<?php

return [
    'location'   => env('UPLOAD_FILE', public_path('uploads/temp/')),
];