<?php

/**
 * The purpose for this file is to write all kind of Queues used in all over the project.
 * Like if we've put user register process in the queue then we shall put that name here
 * and fetch it from here.
 * Benefit: We've all the queue names used all over the project listed here, it would be easy
 * to reconfigure supervisor queue configuration for all the queues.
 *
 * TODO:
 * Whoever in team have created queues so far, please update your code and fetch the queue name from here.
 *
 * - Muhammad Danish
 *
 * Note:
 * For sample purpose you guys can follow buy_k_coin example.
 * */

return [
	'default' => 'default',
	'WithdrawAmount' => 'WithdrawAmount',
	'PayPalWithdrawStatus' => 'PayPalWithdrawStatus',
	'PostProject' => 'PostProject',
	'PostProjectSkillNotification' => 'PostProjectSkillNotification',
	'ApprovePayPal' => 'ApprovePayPal',
    'SharedOfficeSearchJson' => 'SharedOfficeSearchJson',
    'SendPushyNotifications' => 'SendPushyNotifications',
];
