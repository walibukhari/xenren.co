<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMigrationSharedOfficeSettingsAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shared_office_settings', function (Blueprint $table) {
            $table->integer('status_cancel_type')->nullable();
            $table->integer('status_book_type')->nullable();
            $table->integer('cancel_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shared_office_settings', function (Blueprint $table) {
            //
        });
    }
}
