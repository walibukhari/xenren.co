<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectMilestonesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project_milestones', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('project_id');
			$table->double('amount');
			$table->string('name');
			$table->longtext('description');
			$table->string('status')->nullable();
			$table->datetime('due_date');
			$table->datetime('auto_release')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('project_milestones');
	}
}
