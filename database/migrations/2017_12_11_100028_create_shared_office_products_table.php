<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSharedOfficeProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shared_office_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('office_id');
            $table->char('category_id', 255);
            $table->char('number', 255)->nullable();
            $table->text('qr');
            $table->integer('x')->nullable();
            $table->integer('y')->nullable();
            $table->char('status_id', 255);
            $table->integer('month_price')->nullable();
            $table->integer('time_price');
            $table->text('remark');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shared_office_products');
    }
}
