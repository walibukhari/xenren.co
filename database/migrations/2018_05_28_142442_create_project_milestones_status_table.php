<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectMilestonesStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_milestones_status', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->integer('milestone_id');
            $table->string('status')->default(1)->comment('1=> in progress,2=> confirm,3=> rejected,4=> requested');
            $table->longtext('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_milestones_status');
    }
}
