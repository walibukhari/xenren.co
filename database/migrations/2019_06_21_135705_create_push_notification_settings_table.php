<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePushNotificationSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('push_notification_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->double('low_fixed_price');
            $table->double('high_fixed_price');
            $table->double('low_hourly_price');
            $table->double('high_hourly_price');
            $table->double('low_atleast_spend');
            $table->double('high_atleast_spend');
            $table->integer('package_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('push_notification_settings');
    }
}
