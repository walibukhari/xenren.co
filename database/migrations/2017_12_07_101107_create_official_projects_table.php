<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficialProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('official_projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->char('lang', 255);
            $table->char('title', 255);
            $table->text('description');
            $table->dateTime('recruit_start');
            $table->dateTime('recruit_end');
            $table->dateTime('completion');
            $table->text('location_address');
            $table->char('city', 255)->nullable();
            $table->decimal('location_lat', 11, 8)->nullable();
            $table->decimal('location_lng', 11, 8)->nullable();
            $table->integer('budget_switch')->nullable();
            $table->decimal('budget_from', 15, 2)->nullable();
            $table->decimal('budget_to', 15, 2)->nullable();
            $table->integer('stock_total')->nullable();
            $table->integer('stock_share')->nullable();
            $table->text('stock_description')->nullable();
            $table->integer('funding_stage')->nullable();
            $table->text('contract_file')->nullable();
            $table->text('icon')->nullable();
            $table->text('banner')->nullable();
            $table->integer('design_type')->nullable();
            $table->text('out_cover')->nullable();
            $table->text('in_cover')->nullable();
            $table->integer('project_level')->nullable();
            $table->integer('project_type')->nullable();
            $table->integer('share_has_reward')->nullable();
            $table->decimal('share_reward', 15, 2)->nullable();
            $table->decimal('reward', 15, 2)->nullable();
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('official_projects');
    }
}
