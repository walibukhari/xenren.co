<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuestRegistersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('guest_registers', function (Blueprint $table) {
			$table->increments('id');
			$table->text('email')->nullable();
			$table->char('phone_number')->nullable();
			$table->text('password');
			$table->string('about')->nullable();
			$table->string('skills')->nullable();
			$table->text('language')->nullable();
			$table->integer('refer_by');
			$table->text('invite_code')->nullable();
			$table->integer('code');
			$table->text('key');
			$table->integer('status');
			$table->string('image')->nullable();
			$table->string('city_id')->nullable();
			$table->string('hint')->nullable();
			$table->longText('real_name')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('guest_registers');
	}
}
