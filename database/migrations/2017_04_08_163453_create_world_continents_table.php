<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorldContinentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('world_continents', function(Blueprint $table)
		{
			$table->increments('id')->comment('Auto increase ID');
			$table->string('name')->default('')->index('uniq_continent')->comment('Continent Name');
			$table->string('code')->default('')->comment('Continent Code');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('world_continents');
	}

}
