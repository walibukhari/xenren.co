<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('email')->unique();
			$table->string('password')->nullable();
			$table->string('face_image')->nullable();
			$table->integer('initial_display')->nullable();
			$table->integer('search_order')->nullable();
			$table->string('phone_number')->nullable();
			$table->text('email_change_code')->nullable();
			$table->text('password_change_code')->nullable();
			$table->text('real_name')->nullable();
			$table->string('about_me')->nullable();
			$table->string('id_card_no')->nullable();
			$table->integer('gender')->nullable();
			$table->date('date_of_birth')->nullable();
			$table->text('address')->nullable();
			$table->decimal('hourly_pay')->nullable();
			$table->text('handphone_no')->nullable();
			$table->text('skype_id')->nullable();
			$table->text('wechat_id')->nullable();
			$table->text('qq_id')->nullable();
			$table->integer('is_contact_allow_view')->nullable();
			$table->integer('is_freeze')->nullable();
			$table->text('id_image_front')->nullable();
			$table->text('id_image_back')->nullable();
			$table->integer('del_flg')->nullable();
			$table->integer('is_validated')->nullable();
			$table->string('nick_name')->nullable();
			$table->text('img_avatar')->nullable();
			$table->integer('bank_id')->nullable();
			$table->string('bank_account_no')->nullable();
			$table->string('bank_name')->nullable();
			$table->text('bank_address')->nullable();
			$table->integer('upline_id')->nullable();
			$table->integer('did_first_sale')->nullable();
			$table->string('experience')->nullable();
			$table->integer('job_position_id')->nullable();
			$table->string('title')->nullable();
			$table->integer('is_tailor')->nullable();
			$table->integer('account_balance')->nullable();
			$table->text('comment_rating')->nullable();
			$table->string('qq_open_id')->nullable();
			$table->string('weibo_open_id')->nullable();
			$table->string('weixin_open_id')->nullable();
			$table->string('facebook_id')->nullable();
			$table->integer('unread_message')->nullable();
			$table->integer('is_ban_submit_keyword')->nullable();
			$table->integer('country_id')->nullable();
			$table->integer('city_id')->nullable();
			$table->integer('currency')->nullable();
			$table->decimal('xen_coin')->nullable();
			$table->integer('is_agent')->nullable();
			$table->integer('language_preference')->nullable();
			$table->integer('status')->nullable();
			$table->integer('is_first_time_skill_submit')->nullable();
			$table->integer('email_notification')->nullable();
			$table->integer('wechat_notification')->nullable();
			$table->integer('close_notice')->nullable();
			$table->string('hint')->nullable();
			$table->string('stripe_id')->nullable();
			$table->string('card_brand')->nullable();
			$table->string('card_plast_four')->nullable();
			$table->string('user_link')->nullable();
			$table->longText('device_token')->nullable();
			$table->longText('device_os')->nullable();
			$table->timestamp('email_verified_at')->nullable();
			$table->timestamp('trial_ends_at')->nullable();
			$table->timestamp('last_login_at')->nullable();
			$table->rememberToken();
			$table->timestamps();
		});
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}
}
