<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTransactionTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transaction', function (Blueprint $table) {
			$table->renameColumn('balance_before', 'performed_by_balance_before');
			$table->renameColumn('balance_after', 'performed_by_balance_after');
			$table->integer('performed_to_balance_before')->nullable();
			$table->integer('performed_to_balance_after')->nullable();
		});
		
		if (Schema::hasColumn('transaction', 'balance_before')){
			Schema::table('transaction', function (Blueprint $table) {
				$table->dropColumn('balance_before');
			});
		}
		
		if (Schema::hasColumn('transaction', 'balance_after')){
			Schema::table('transaction', function (Blueprint $table) {
				$table->dropColumn('balance_after');
			});
		}
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}
}
