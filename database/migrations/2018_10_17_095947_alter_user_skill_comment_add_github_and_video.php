<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserSkillCommentAddGithubAndVideo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('user_skill_comment', function ($table) {
		    $table->increments('id');
		    $table->integer('user_id')->nullable();
		    $table->integer('staff_id')->nullable();
		    $table->integer('official_project_id')->nullable();
		    $table->text('comment')->nullable();
		    $table->integer('attitude_rate')->nullable();
		    $table->text('attitude_comment')->nullable();
		    $table->decimal('score_attitude')->nullable();
		    $table->decimal('score_overall')->nullable();
		    $table->integer('is_official')->nullable();
		    $table->text('github_link')->nullable();
		    $table->string('video_name')->nullable();
		    $table->string('video_thumb')->nullable();
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
