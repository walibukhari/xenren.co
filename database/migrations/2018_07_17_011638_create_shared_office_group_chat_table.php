<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSharedOfficeGroupChatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shared_office_group_chat', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('office_id');
            $table->integer('sender_user_id');
            $table->integer('sender_staff_id')->nullable();
            $table->text('message');
            $table->tinyInteger('is_seen')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shared_office_group_chat');

    }
}
