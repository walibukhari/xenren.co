<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type');
            $table->integer('from_who_to_who');
            $table->integer('reviewer_user_id');
            $table->integer('reviewer_staff_id')->nullable();
            $table->integer('reviewee_user_id');
            $table->integer('reviewee_staff_id')->nullable();
            $table->integer('project_id');
            $table->integer('reason');
            $table->integer('recommendation');
            $table->integer('proficiency');
            $table->integer('payment_on_time')->nullable();
            $table->integer('communication');
            $table->integer('availability');
            $table->integer('trustworthy')->nullable();
            $table->integer('clear_requirement')->nullable();
            $table->integer('skils')->nullable();
            $table->integer('quality_work')->nullable();
            $table->integer('adherence_to_schedule')->nullable();
            $table->integer('cooperation')->nullable();
            $table->decimal('total_score', 8, 2);
            $table->text('experience');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
