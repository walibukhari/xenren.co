<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSharedOfficeBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shared_office_booking', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('office_id');
            $table->integer('user_id');
            $table->string('client_email');
            $table->string('full_name');
            $table->string('phone_no');
            $table->string('check_in');
            $table->string('check_out');
            $table->string('no_of_rooms');
            $table->string('no_of_persons');
            $table->string('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shared_office_booking');
    }
}
