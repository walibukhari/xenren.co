<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersInboxMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_inbox_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inbox_id');
            $table->integer('from_user_id')->nullable();
            $table->integer('to_user_id')->nullable();
            $table->integer('from_staff_id')->nullable();
            $table->integer('to_staff_id')->nullable();
            $table->integer('project_id');
            $table->integer('is_read')->nullable();
            $table->longText('custom_message')->nullable();
            $table->decimal('type')->nullable();
            $table->decimal('quote_price', 10, 2)->nullable();
            $table->integer('pay_type')->nullable();
            $table->text('file')->nullable();
            $table->string('daily_update')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_inbox_messages');
    }
}
