<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('room_id')->nullable();
            $table->integer('sender_user_id')->nullable();
            $table->integer('sender_staff_id')->nullable();
            $table->integer('receiver_user_id')->nullable();
            $table->integer('receiver_staff_id')->nullable();
            $table->text('message')->nullable();
            $table->text('file')->nullable();
            $table->char('is_seen', 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_messages');
    }
}
