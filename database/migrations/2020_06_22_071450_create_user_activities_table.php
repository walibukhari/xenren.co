<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('username');
            $table->string('email');
            $table->string('skype')->nullable();
            $table->string('line')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('we_chat')->nullable();
            $table->string('performed_activity')->nullable();
            $table->double('amount')->nullable();
            $table->string('project_name')->nullable();
            $table->string('project_type')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('search')->nullable();
            $table->string('received_offer')->nullable();
            $table->string('send_offer')->nullable();
            $table->string('request_contact_info')->nullable();
            $table->string('send_message')->nullable();
            $table->string('receive_message')->nullable();
            $table->string('register_user')->nullable();
            $table->bigInteger('register_user_verify_code')->nullable();
            $table->dateTime('login_time')->nullable();
            $table->dateTime('logout_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_activities');
    }
}
