<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserInboxAddDailyUpdateField extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_inbox', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('category_id')->nullable();
			$table->integer('project_id')->nullable();
			$table->integer('from_user_id')->nullable();
			$table->integer('to_user_id')->nullable();
			$table->integer('from_staff_id')->nullable();
			$table->integer('to_staff_id')->nullable();
			$table->integer('type')->nullable();
			$table->integer('is_trash')->nullable();
			$table->integer('daily_update')->nullable();
			$table->integer('category');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users_inbox');
	}
}
