<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSharedOfficeImagesAddOfficeIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shared_office_images', function (Blueprint $table) {
            $table->index(['office_id', 'priority_images']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shared_office_images', function (Blueprint $table) {
            $table->dropIndex(['office_id', 'priority_images']);
        });
    }
}
