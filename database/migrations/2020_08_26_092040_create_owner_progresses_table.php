<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOwnerProgressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owner_progresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('staff_id');
            $table->bigInteger('manage_bookings')->default(0);
            $table->bigInteger('owner_profile')->default(0);
            $table->bigInteger('office_check_in')->default(0);
            $table->bigInteger('office_info')->default(0);
            $table->bigInteger('confirm_bookings')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owner_progresses');
    }
}
