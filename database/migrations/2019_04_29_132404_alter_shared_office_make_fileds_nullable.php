<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSharedOfficeMakeFiledsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('shared_offices', function (Blueprint $table) {
		    $table->string('saturday_opening_time')->nullable()->change();
		    $table->string('saturday_closing_time')->nullable()->change();
		    $table->string('sunday_opening_time')->nullable()->change();
		    $table->string('sunday_closing_time')->nullable()->change();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
