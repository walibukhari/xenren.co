<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSkillCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_skill_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('staff_id');
            $table->integer('official_project_id');
            $table->text('comment');
            $table->integer('attitude_rate');
            $table->text('attitude_comment');
            $table->decimal('score_attitude', 5, 2);
            $table->decimal('score_overall', 5, 2);
            $table->integer('is_official');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_skill_comments');
    }
}
