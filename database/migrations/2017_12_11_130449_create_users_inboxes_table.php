<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersInboxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_inboxes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category');
            $table->integer('project_id');
            $table->integer('from_user_id');
            $table->integer('to_user_id');
            $table->integer('from_staff_id');
            $table->integer('to_staff_id');
            $table->integer('type');
            $table->integer('is_trash');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_inboxes');
    }
}
