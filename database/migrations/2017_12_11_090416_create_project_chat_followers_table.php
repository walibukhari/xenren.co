<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectChatFollowersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_chat_followers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_chat_room_id')->nullable();
            $table->integer('user_id');
            $table->integer('is_kick')->nullable();
            $table->integer('thumb_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_chat_followers');
    }
}
