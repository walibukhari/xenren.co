<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSharedOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shared_offices', function (Blueprint $table) {
            $table->increments('id');
            $table->char('office_name');
            $table->integer('office_size_x')->nullable();
            $table->integer('office_size_y')->nullable();
            $table->char('location');
            $table->text('description');
            $table->string('number')->nullable();
            $table->text('qr')->nullable()->nullable();
            $table->char('image')->nullable();
            $table->tinyInteger('pic_size');
            $table->integer('staff_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shared_offices');
    }
}
