<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faqs', function (Blueprint $table) {
            $table->increments('id');
            $table->char('title_cn', 255);
            $table->text('text_cn');
            $table->char('title_en', 255);
            $table->text('text_en');
            $table->integer('parent_id');
            $table->integer('position');
            $table->integer('flags');
            $table->integer('_lft');
            $table->integer('_rgt');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faqs');
    }
}
