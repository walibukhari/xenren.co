<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorldCountriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('world_countries', function(Blueprint $table)
		{
			$table->increments('id')->comment('Auto increase ID');
			$table->integer('continent_id')->unsigned()->comment('Continent ID');
			$table->string('name')->default('')->comment('Country Common Name');
			$table->string('full_name')->nullable()->comment('Country Fullname');
			$table->string('capital')->nullable()->comment('Capital Common Name');
			$table->string('code')->nullable()->comment('ISO3166-1-Alpha-2');
			$table->string('code_alpha3')->nullable()->comment('ISO3166-1-Alpha-3');
			$table->string('emoji')->nullable()->comment('Country Emoji');
			$table->boolean('has_division')->default(0)->comment('Has Division');
			$table->string('currency_code')->nullable()->comment('iso_4217_code');
			$table->string('currency_name')->nullable()->comment('iso_4217_name');
			$table->string('tld')->nullable()->comment('Top level domain');
			$table->string('callingcode')->nullable()->comment('Calling prefix');
			$table->unique(['continent_id','name'], 'uniq_country');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('world_countries');
	}

}
