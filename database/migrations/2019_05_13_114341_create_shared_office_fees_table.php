<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSharedOfficeFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shared_office_fees', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("type");
            $table->integer("office_id");
            $table->longText("no_of_peoples");
            $table->longText('hourly_price');
            $table->longText("daily_price");
            $table->longText("weekly_price");
            $table->longText("monthly_price");
            $table->integer("is_available")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shared_office_fees');
    }
}
