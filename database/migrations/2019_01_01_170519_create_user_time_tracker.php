<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTimeTracker extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_time_track', function (Blueprint $table) {
            $table->increments('id');
            $table->text('memo');
            $table->time('start_time');
            $table->time('end_time')->nullable();
            $table->integer('user_id');
            $table->time('total_time')->nullable();
            $table->integer('project_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_time_track');
    }
}
