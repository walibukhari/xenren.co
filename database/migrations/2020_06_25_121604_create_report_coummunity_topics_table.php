<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportCoummunityTopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_coummunity_topics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('comment');
            $table->string('topic_name');
            $table->string('discussion_name');
            $table->integer('discussion_id');
            $table->string('report_to');
            $table->string('report_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_coummunity_topics');
    }
}
