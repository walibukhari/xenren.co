<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSharedOfficeImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('shared_office_images', function (Blueprint $table) {
		    $table->increments('id');
		    $table->integer('office_id');
		    $table->string('image')->nullable();
		    $table->string('pic_size')->nullable();
		    $table->string('alt')->nullable();
		    $table->string('order_image')->nullable();
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shared_office_images');
    }
}
