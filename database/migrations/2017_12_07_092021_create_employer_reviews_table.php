<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployerReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employer_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reviewer_id');
            $table->integer('project_id');
            $table->integer('reason');
            $table->integer('recommendation_val');
            $table->integer('proficiency_val');
            $table->integer('skills');
            $table->integer('quality_work');
            $table->integer('availability');
            $table->integer('adherence_to_schedule');
            $table->integer('communication');
            $table->integer('cooperation');
            $table->text('experience');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employer_reviews');
    }
}
