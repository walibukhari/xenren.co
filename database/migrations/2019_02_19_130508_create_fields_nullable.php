<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
	         /* $table->text('email_change_code')->nullable()->change();
	          $table->text('password_change_code')->nullable()->change();
	          $table->string('id_card_no')->nullable()->change();
	          $table->text('address')->nullable()->change();
	          $table->string('handphone_no')->nullable()->change();
	          $table->text('id_image_front')->nullable()->change();
	          $table->text('id_image_back')->nullable()->change();
	          $table->string('img_avatar')->nullable()->change();
	          $table->string('nick_name')->nullable()->change();
	          $table->string('bank_id')->nullable()->change();
	          $table->string('bank_account_no')->nullable()->change();
	          $table->string('bank_address')->nullable()->change();
	          $table->string('upline_id')->nullable()->change();
	          $table->integer('did_first_sale')->nullable()->change();
	          $table->text('comment_rating')->nullable()->change();*/
        });

        Schema::table('project_milestones', function (Blueprint $table) {
	          //$table->dateTime('auto_release')->nullable();
        });

        Schema::table('transaction', function (Blueprint $table) {
	          /*$table->float('performed_by_balance_before')->nullable();
	          $table->float('performed_by_balance_after')->nullable();
	          $table->string('currency')->nullable();*/
        });

        Schema::table('guest_registers', function (Blueprint $table) {
	          //$table->float('invite_code')->nullable()->change();
        });

        Schema::table('shared_office_products', function (Blueprint $table) {
	          //$table->float('month_price')->change()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('fields_nullable');
    }
}
