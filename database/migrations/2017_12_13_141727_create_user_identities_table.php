<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserIdentitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_identity', function (Blueprint $table) {
            $table->increments('id');
            $table->text('key');
            $table->text('real_name')->nullable();
            $table->char('id_card_no')->nullable();
            $table->text('id_image')->nullable();
            $table->text('hold_id_image');
            $table->integer('gender');
            $table->dateTime('date_of_birth');
            $table->text('address');
            $table->integer('country_id');
            $table->char('handphone_no');
            $table->integer('status');
            $table->integer('user_id');
            $table->integer('is_submit');
            $table->text('reject_reason');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_identity');
    }
}
