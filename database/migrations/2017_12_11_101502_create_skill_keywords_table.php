<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkillKeywordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skill_keywords', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->char('name_cn')->nullable();
            $table->char('name_en')->nullable();
            $table->char('reference_link', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skill_keywords');
    }
}
