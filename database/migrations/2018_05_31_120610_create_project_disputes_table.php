<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectDisputesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_disputes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->integer('milestone_id')
                ->nullable()
                ->comment('will be filled only if the dispute type is 2 (milestone dispute)');
            $table->double('amount')
                ->nullable()
                ->comment('will be filled only if the dispute type is 1 (manual dispute)');
            $table->string('type')->default(1)->comment(
                '1=> manual dispute,
                 2=> milestone dispute'
            );
            $table->string('status')->default(1)->comment(
                '1=> in progress,
                 2=> confirm by freelancer,
                 3=> rejected by freelancer'
            );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_disputes');
    }
}
