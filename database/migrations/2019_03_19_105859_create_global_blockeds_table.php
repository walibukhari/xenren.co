<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGlobalBlockedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_contact_privacies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id");
            $table->integer("qq")->nullable();
            $table->integer("weChat")->nullable();
            $table->integer("skype")->nullable();
            $table->integer("phone")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_contact_privacies');
    }
}
