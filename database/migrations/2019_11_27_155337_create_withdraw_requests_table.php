<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWithdrawRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdraw_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bank_name');
            $table->string('iban_number');
            $table->longText('card_number');
            $table->string('branch_name');
            $table->double('branch_code');
            $table->string('branch_address');
            $table->double('payment_request');
            $table->double('total_balance');
            $table->integer('staff_id');
            $table->integer('office_id');
            $table->integer('transaction_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdraw_requests');
    }
}
