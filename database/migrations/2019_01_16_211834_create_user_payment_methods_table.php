<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('user_payment_methods', function (Blueprint $table) {
		    $table->increments('id');
		    $table->integer('user_id');
		    $table->string('payment_method');
		    $table->string('name');
		    $table->string('email');
		    $table->string('phone');
		    $table->integer('status')->default(0);
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_payment_methods');
    }
}
