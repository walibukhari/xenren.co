<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSharedOfficeAddLatLngFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('shared_offices', function (Blueprint $table) {
		    $table->integer('city_id')->nullable();
		    $table->integer('state_id')->nullable();
		    $table->integer('continent_id')->nullable();
		    $table->double('lat')->nullable();
		    $table->double('lng')->nullable();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
