<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectDisputesCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('project_disputes_comments', function (Blueprint $table) {
		    $table->increments('id');
		    $table->string('project_dispute_id');
		    $table->integer('user_id');
		    $table->string('comment');
		    $table->softDeletes();
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_disputes_comments');
    }
}
