<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSharedOfficeFinancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shared_office_finances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->integer('office_id');
            $table->integer('user_id');
            $table->string('status_finance', 255);
            $table->string('user', 255);
            $table->string('cost', 255);
            $table->dateTime('start_time');
            $table->dateTime('end_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shared_office_finances');
    }
}
