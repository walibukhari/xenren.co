<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->increments('id');
            $table->char('username', 255);
            $table->char('email', 255);
            $table->text('name');
            $table->char('password', 255);
            $table->char('remember_token', 255);
            $table->integer('staff_type');
            $table->integer('reviewee_staff_id');
            $table->integer('permission_group_id')->nullable();
            $table->char('title', 255);
            $table->char('img_avatar', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
