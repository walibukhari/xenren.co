<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectFileDisputesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_filed_disputes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaction_id');
            $table->integer('status')->comment('0=> in progress, 1=> accept, 2=> reject');
            $table->string('comment')->nullable();
            $table->string('attachments')->nullable()->comment('in case of file dispute, freelancer uploaded files will be saved here in comma separated string');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_filed_disputes');
    }
}
