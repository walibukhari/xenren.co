<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectDisputeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('project_disputes', function (Blueprint $table) {
		    $table->increments('id');
		    $table->string('project_id');
		    $table->string('milestone_id')->nullable();
		    $table->string('transaction_id')->nullable();
		    $table->string('status');
		    $table->string('admin_comment')->nullable();
		    $table->string('created_by');
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_disputes');
    }
}
