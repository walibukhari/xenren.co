<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableMondayTimeOpenLongText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shared_offices', function (Blueprint $table) {
            $table->longText('monday_opening_time')->nullable()->change();
            $table->longText('monday_closing_time')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shared_offices', function (Blueprint $table) {
            //
        });
    }
}
