<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockedUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blocked_users', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('qq')->nullable()->comment('0: Inactive, 1: Platform Active, 2: All Active');
            $table->tinyInteger('skype')->nullable()->comment('0: Inactive, 1: Platform Active, 2: All Active');
            $table->tinyInteger('wechat')->nullable()->comment('0: Inactive, 1: Platform Active, 2: All Active');
            $table->tinyInteger('phone')->nullable()->comment('0: Inactive, 1: Platform Active, 2: All Active');
            $table->integer('from')->nullable()->comment('User Id who blocked');
            $table->integer('to')->nullable()->comment('User Id to block');
            $table->integer('block_chat')->nullable()->comment('0: No, 1: Blocked');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blocked_users');
    }
}
