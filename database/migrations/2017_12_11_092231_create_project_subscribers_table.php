<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectSubscribersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_subscribers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('official_project_id');
            $table->integer('common_project_id');
            $table->integer('user_id');
            $table->integer('status');
            $table->char('notify_by', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_subscribers');
    }
}
