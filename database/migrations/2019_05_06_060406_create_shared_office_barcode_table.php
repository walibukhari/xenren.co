<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSharedOfficeBarcodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shared_office_barcode', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shared_office_id');
            $table->integer('type');
            $table->integer('category')->comment('Open or Private');
            $table->text('barcode');
            $table->text('code');
            $table->text('password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shared_office_barcode');
    }
}
