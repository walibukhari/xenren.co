<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTransactionAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table("transaction", function ($table) {
		    $table->double("balance_before")->nullable();
		    $table->double("balance_after")->nullable();
		    $table->integer("performed_by")->nullable();
		    $table->integer("performed_to")->nullable();
		    $table->integer("currency")->nullable();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
