<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectChatMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_chat_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_chat_room_id');
            $table->integer('sender_user_id');
            $table->integer('sender_staff_id');
            $table->text('message');
            $table->text('file');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_chat_messages');
    }
}
