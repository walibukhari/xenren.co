<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type');
            $table->integer('user_id');
            $table->integer('staff_id')->nullable();
            $table->string('name');
            $table->integer('category_id')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('language_id')->nullable();
            $table->longText('description')->nullable();
            $table->string('attachment')->nullable();
            $table->integer('pay_type')->nullable();
            $table->decimal('reference_price', 10, 2)->nullable();
            $table->integer('time_commitment')->nullable();
            $table->integer('freelance_type')->nullable();
            $table->integer('status')->nullable();
            $table->integer('is_read')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project');
    }
}
