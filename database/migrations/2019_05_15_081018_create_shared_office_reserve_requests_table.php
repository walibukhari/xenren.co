<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSharedOfficeReserveRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shared_office_reserve_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("office_id");
            $table->integer("type");
            $table->longText("price");
            $table->string("duration_type");
            $table->string("office_manager_email");
            $table->string("name");
            $table->double("phone_number");
            $table->dateTime("check_in_date");
            $table->dateTime("check_out_date");
            $table->integer("no_of_rooms");
            $table->integer("no_of_peoples");
            $table->string("remarks");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shared_office_reserve_requests');
    }
}
