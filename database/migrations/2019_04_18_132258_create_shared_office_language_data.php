<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSharedOfficeLanguageData extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('shared_office_languages', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('shared_offices_id');
			$table->string('office_name_cn');
			$table->text('office_description_cn');
			$table->string('office_space_cn');
			$table->string('contact_name_cn');
			$table->string('contact_phone_cn');
			$table->string('contact_email_cn');
			$table->text('contact_address_cn');
			$table->timestamps();
		});
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('shared_office_languages');
	}
}
