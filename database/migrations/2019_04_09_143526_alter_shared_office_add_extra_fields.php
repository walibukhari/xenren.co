<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSharedOfficeAddExtraFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('shared_offices', function (Blueprint $table) {
		    $table->string('contact_name')->nullable();
		    $table->string('contact_phone')->nullable();
		    $table->string('contact_email')->nullable();
		    $table->string('contact_address')->nullable();
		    
		    $table->string('monday_opening_time')->nullable();
		    $table->string('monday_closing_time')->nullable();
		    
		    $table->string('saturday_opening_time')->nullable();
		    $table->string('saturday_closing_time')->nullable();
		    
		    $table->string('sunday_opening_time')->nullable();
		    $table->string('sunday_closing_time')->nullable();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
