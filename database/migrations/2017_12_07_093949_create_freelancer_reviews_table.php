<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFreelancerReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('freelancer_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reviewer_id');
            $table->integer('project_id');
            $table->integer('reason');
            $table->integer('recommendation');
            $table->integer('proficiency');
            $table->integer('skills');
            $table->integer('quality_work');
            $table->integer('availability');
            $table->integer('adherence_to_schedule');
            $table->integer('communication');
            $table->integer('cooperation');
            $table->text('description');
            $table->integer('feedback_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('freelancer_reviews');
    }
}
