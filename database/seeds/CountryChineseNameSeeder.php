<?php

use App\Models\Countries;
use Illuminate\Database\Seeder;

class CountryChineseNameSeeder extends Seeder
{
    /**
     * Run this seeds after seeder "CountryTableSeeder.php" finish run.
     * https://
     *
     * @return void
     */
    public function run()
    {

        $countries = Countries::all();
        foreach( $countries as $country )
        {
            switch( $country->name )
            {
                case "Afghanistan":
                    $country->name_cn = "阿富汗";
                    break;
                case "Åland Islands":
                    $country->name_cn = "奥兰";
                    break;
                case "Albania":
                    $country->name_cn = "阿尔巴尼亚";
                    break;
                case "Algeria":
                    $country->name_cn = "阿尔及利亚";
                    break;
                case "American Samoa":
                    $country->name_cn = "美属萨摩亚";
                    break;
                case "Andorra":
                    $country->name_cn = "安道尔";
                    break;
                case "Angola":
                    $country->name_cn = "安哥拉";
                    break;
                case "Anguilla":
                    $country->name_cn = "安圭拉";
                    break;
                case "Antarctica":
                    $country->name_cn = "南极洲";
                    break;
                case "Antigua and Barbuda":
                    $country->name_cn = "安提瓜和巴布达";
                    break;
                case "Argentina":
                    $country->name_cn = "阿根廷";
                    break;
                case "Armenia":
                    $country->name_cn = "亚美尼亚";
                    break;
                case "Aruba":
                    $country->name_cn = "阿路巴";
                    break;
                case "Australia":
                    $country->name_cn = "澳大利亚";
                    break;
                case "Austria":
                    $country->name_cn = "奥地利";
                    break;
                case "Azerbaijan":
                    $country->name_cn = "亚塞拜然";
                    break;
                case "The Bahamas":
                    $country->name_cn = "巴哈马";
                    break;
                case "Bahrain":
                    $country->name_cn = "巴林";
                    break;
                case "Bangladesh":
                    $country->name_cn = "孟加拉";
                    break;
                case "Barbados":
                    $country->name_cn = "巴贝多";
                    break;
                case "Belarus":
                    $country->name_cn = "白俄罗斯";
                    break;
                case "Belgium":
                    $country->name_cn = "比利时";
                    break;
                case "Belize":
                    $country->name_cn = "贝里斯";
                    break;
                case "Benin":
                    $country->name_cn = "贝南";
                    break;
                case "Bermuda":
                    $country->name_cn = "百慕达";
                    break;
                case "Bhutan":
                    $country->name_cn = "不丹";
                    break;
                case "Bolivia":
                    $country->name_cn = "玻利维亚";
                    break;
                case "Bonaire":
                    $country->name_cn = "波奈";
                    break;
                case "Bosnia and Herzegovina":
                    $country->name_cn = "波希尼亚及赫塞哥维那";
                    break;
                case "Botswana":
                    $country->name_cn = "波札那";
                    break;
                case "Bouvet Island":
                    $country->name_cn = "布韦岛";
                    break;
                case "Brazil":
                    $country->name_cn = "巴西";
                    break;
                case "British Indian Ocean Territory":
                    $country->name_cn = "英属印度洋领地";
                    break;
                case "United States Minor Outlying Islands":
                    $country->name_cn = "美国本土外小岛屿";
                    break;
                case "Virgin Islands (British)":
                    $country->name_cn = "英属维尔京群岛";
                    break;
                case "Virgin Islands (U.S.)":
                    $country->name_cn = "美属维京群岛";
                    break;
                case "Brunei":
                    $country->name_cn = "汶莱";
                    break;
                case "Bulgaria":
                    $country->name_cn = "保加利亚";
                    break;
                case "Burkina Faso":
                    $country->name_cn = "有吉纳法索";
                    break;
                case "Burundi":
                    $country->name_cn = "蒲隆地";
                    break;
                case "Cambodia":
                    $country->name_cn = "柬埔寨";
                    break;
                case "Cameroon":
                    $country->name_cn = "喀麦隆";
                    break;
                case "Canada":
                    $country->name_cn = "加拿大";
                    break;
                case "Cape Verde":
                    $country->name_cn = "维德角岛";
                    break;
                case "Cayman Islands":
                    $country->name_cn = "开曼群岛";
                    break;
                case "Central African Republic":
                    $country->name_cn = "中非共和国";
                    break;
                case "Chad":
                    $country->name_cn = "查德";
                    break;
                case "Chile":
                    $country->name_cn = "智利";
                    break;
                case "China":
                    $country->name_cn = "中国大陆";
                    break;
                case "Christmas Island":
                    $country->name_cn = "圣诞岛";
                    break;
                case "Cocos (Keeling) Islands":
                    $country->name_cn = "库克群岛";
                    break;
                case "Colombia":
                    $country->name_cn = "哥伦比亚";
                    break;
                case "Comoros":
                    $country->name_cn = "科摩罗";
                    break;
                case "Republic of the Congo":
                    $country->name_cn = "刚果";
                    break;
                case "Democratic Republic of the Congo":
                    $country->name_cn = "刚果";
                    break;
                case "Cook Islands":
                    $country->name_cn = "科克群岛";
                    break;
                case "Costa Rica":
                    $country->name_cn = "哥斯大黎加";
                    break;
                case "Croatia":
                    $country->name_cn = "克罗埃西亚";
                    break;
                case "Cuba":
                    $country->name_cn = "古巴";
                    break;
                case "Curaçao":
                    $country->name_cn = "库拉索";
                    break;
                case "Cyprus":
                    $country->name_cn = "塞浦路斯";
                    break;
                case "Czech Republic":
                    $country->name_cn = "捷克";
                    break;
                case "Denmark":
                    $country->name_cn = "丹麦";
                    break;
                case "Djibouti":
                    $country->name_cn = "吉布提";
                    break;
                case "Dominica":
                    $country->name_cn = "多米尼克";
                    break;
                case "Dominican Republic":
                    $country->name_cn = "多明尼加";
                    break;
                case "Ecuador":
                    $country->name_cn = "厄瓜多尔";
                    break;
                case "Egypt":
                    $country->name_cn = "埃及";
                    break;
                case "El Salvador":
                    $country->name_cn = "萨尔瓦多";
                    break;
                case "Equatorial Guinea":
                    $country->name_cn = "几内亚";
                    break;
                case "Eritrea":
                    $country->name_cn = "厄利垂亚";
                    break;
                case "Estonia":
                    $country->name_cn = "爱沙尼亚";
                    break;
                case "Ethiopia":
                    $country->name_cn = "衣索匹亚";
                    break;
                case "Falkland Islands":
                    $country->name_cn = "福克兰群岛";
                    break;
                case "Faroe Islands":
                    $country->name_cn = "法罗群岛";
                    break;
                case "Fiji":
                    $country->name_cn = "斐济";
                    break;
                case "Finland":
                    $country->name_cn = "芬兰";
                    break;
                case "France":
                    $country->name_cn = "法国";
                    break;
                case "French Guiana":
                    $country->name_cn = "法属圭亚那";
                    break;
                case "French Polynesia":
                    $country->name_cn = "法属玻里尼西亚";
                    break;
                case "French Southern and Antarctic Lands":
                    $country->name_cn = "法国南半球及南极属地";
                    break;
                case "Gabon":
                    $country->name_cn = "加彭";
                    break;
                case "The Gambia":
                    $country->name_cn = "冈比亚";
                    break;
                case "Georgia":
                    $country->name_cn = "乔治亚";
                    break;
                case "Germany":
                    $country->name_cn = "德国";
                    break;
                case "Ghana":
                    $country->name_cn = "迦纳";
                    break;
                case "Gibraltar":
                    $country->name_cn = "直布罗陀";
                    break;
                case "Greece":
                    $country->name_cn = "希腊";
                    break;
                case "Greenland":
                    $country->name_cn = "格陵兰";
                    break;
                case "Grenada":
                    $country->name_cn = "格瑞那达";
                    break;
                case "Guadeloupe":
                    $country->name_cn = "瓜德罗普";
                    break;
                case "Guam":
                    $country->name_cn = "关岛";
                    break;
                case "Guatemala":
                    $country->name_cn = "瓜地马拉";
                    break;
                case "Guernsey":
                    $country->name_cn = "根西";
                    break;
                case "Guinea":
                    $country->name_cn = "几内亚";
                    break;
                case "Guinea-Bissau":
                    $country->name_cn = "几内亚";
                    break;
                case "Guyana":
                    $country->name_cn = "盖亚那";
                    break;
                case "Haiti":
                    $country->name_cn = "海地";
                    break;
                case "Heard Island and McDonald Islands":
                    $country->name_cn = "赫德岛和麦克唐纳群岛";
                    break;
                case "Holy See":
                    $country->name_cn = "梵蒂冈";
                    break;
                case "Honduras":
                    $country->name_cn = "宏都拉斯";
                    break;
                case "Hong Kong":
                    $country->name_cn = "香港";
                    break;
                case "Hungary":
                    $country->name_cn = "匈牙利";
                    break;
                case "Iceland":
                    $country->name_cn = "冰岛";
                    break;
                case "India":
                    $country->name_cn = "印度";
                    break;
                case "Indonesia":
                    $country->name_cn = "印尼";
                    break;
                case "Ivory Coast":
                    $country->name_cn = "科特迪瓦";
                    break;
                case "Iran":
                    $country->name_cn = "伊朗";
                    break;
                case "Iraq":
                    $country->name_cn = "伊拉克";
                    break;
                case "Republic of Ireland":
                    $country->name_cn = "爱尔兰";
                    break;
                case "Isle of Man":
                    $country->name_cn = "曼岛";
                    break;
                case "Israel":
                    $country->name_cn = "以色列";
                    break;
                case "Italy":
                    $country->name_cn = "义大利";
                    break;
                case "Jamaica":
                    $country->name_cn = "牙买加";
                    break;
                case "Japan":
                    $country->name_cn = "日本";
                    break;
                case "Jersey":
                    $country->name_cn = "泽西";
                    break;
                case "Jordan":
                    $country->name_cn = "約旦";
                    break;
                case "Kazakhstan":
                    $country->name_cn = "哈萨克";
                    break;
                case "Kenya":
                    $country->name_cn = "肯亚";
                    break;
                case "Kiribati":
                    $country->name_cn = "基里巴斯共和国";
                    break;
                case "Korea":
                    $country->name_cn = "韩国";
                    break;
                case "Kuwait":
                    $country->name_cn = "科威特";
                    break;
                case "Kyrgyzstan":
                    $country->name_cn = "吉尔吉斯斯坦";
                    break;
                case "Laos":
                    $country->name_cn = "寮国";
                    break;
                case "Latvia":
                    $country->name_cn = "拉脱维亚";
                    break;
                case "Lebanon":
                    $country->name_cn = "黎巴嫩";
                    break;
                case "Lesotho":
                    $country->name_cn = "奥塞梯语";
                    break;
                case "Leone":
                    $country->name_cn = "塞拉利昂";
                    break;
                case "Liberia":
                    $country->name_cn = "利比亚";
                    break;
                case "Libya":
                    $country->name_cn = "利比亚";
                    break;
                case "Liechtenstein":
                    $country->name_cn = "列支敦士登";
                    break;
                case "Lithuania":
                    $country->name_cn = "语系-立陶宛语";
                    break;
                case "Luxembourg":
                    $country->name_cn = "卢森堡";
                    break;
                case "Macau":
                    $country->name_cn = "澳门";
                    break;
                case "Republic of Macedonia":
                    $country->name_cn = "马其顿";
                    break;
                case "Madagascar":
                    $country->name_cn = "马达加斯加";
                    break;
                case "Malawi":
                    $country->name_cn = "马拉威";
                    break;
                case "Malaysia":
                    $country->name_cn = "马来西亚";
                    break;
                case "Maldives":
                    $country->name_cn = "马尔地夫";
                    break;
                case "Mali":
                    $country->name_cn = "马利";
                    break;
                case "Malta":
                    $country->name_cn = "马尔他";
                    break;
                case "Marshall Islands":
                    $country->name_cn = "马绍尔群岛";
                    break;
                case "Martinique":
                    $country->name_cn = "马提尼克";
                    break;
                case "Mauritania":
                    $country->name_cn = "茅利塔尼亚";
                    break;
                case "Mauritius":
                    $country->name_cn = "模里西斯";
                    break;
                case "Mayotte":
                    $country->name_cn = "马约特";
                    break;
                case "Mexico":
                    $country->name_cn = "墨西哥";
                    break;
                case "Federated States of Micronesia":
                    $country->name_cn = "密克罗尼西亚";
                    break;
                case "Moldova":
                    $country->name_cn = "摩尔多瓦";
                    break;
                case "Monaco":
                    $country->name_cn = "摩纳哥";
                    break;
                case "Mongolia":
                    $country->name_cn = "蒙古";
                    break;
                case "Montenegro":
                    $country->name_cn = "蒙特内哥罗";
                    break;
                case "Montserrat":
                    $country->name_cn = "蒙塞拉特岛";
                    break;
                case "Morocco":
                    $country->name_cn = "摩洛哥";
                    break;
                case "Mozambique":
                    $country->name_cn = "莫三鼻克";
                    break;
                case "Myanmar":
                    $country->name_cn = "缅甸";
                    break;
                case "Namibia":
                    $country->name_cn = "纳米比亚";
                    break;
                case "Nauru":
                    $country->name_cn = "诺鲁";
                    break;
                case "Nepal":
                    $country->name_cn = "尼泊尔";
                    break;
                case "Netherlands":
                    $country->name_cn = "荷兰";
                    break;
                case "New Caledonia":
                    $country->name_cn = "新喀里多尼亚";
                    break;
                case "New Zealand":
                    $country->name_cn = "纽西兰";
                    break;
                case "Nicaragua":
                    $country->name_cn = "尼加拉瓜";
                    break;
                case "Niger":
                    $country->name_cn = "尼日";
                    break;
                case "Nigeria":
                    $country->name_cn = "奈及利亚";
                    break;
                case "Niue":
                    $country->name_cn = "纽埃";
                    break;
                case "Norfolk Island":
                    $country->name_cn = "诺福克岛";
                    break;
                case "North Korea":
                    $country->name_cn = "朝鲜民主主义人民共和国";
                    break;
                case "Northern Mariana Islands":
                    $country->name_cn = "北马里亚纳群岛";
                    break;
                case "Norway":
                    $country->name_cn = "挪威";
                    break;
                case "Oman":
                    $country->name_cn = "阿曼";
                    break;
                case "Pakistan":
                    $country->name_cn = "巴基斯坦";
                    break;
                case "Palau":
                    $country->name_cn = "帕劳";
                    break;
                case "Palestine":
                    $country->name_cn = "巴勒斯坦国";
                    break;
                case "Panama":
                    $country->name_cn = "巴拿马";
                    break;
                case "Papua New Guinea":
                    $country->name_cn = "巴布亚纽几内亚";
                    break;
                case "Paraguay":
                    $country->name_cn = "巴拉圭";
                    break;
                case "Peru":
                    $country->name_cn = "秘鲁";
                    break;
                case "Philippines":
                    $country->name_cn = "菲律宾";
                    break;
                case "Pitcairn Islands":
                    $country->name_cn = "皮特凯恩群岛";
                    break;
                case "Poland":
                    $country->name_cn = "波兰";
                    break;
                case "Portugal":
                    $country->name_cn = "葡萄牙";
                    break;
                case "Puerto Rico":
                    $country->name_cn = "波多黎各";
                    break;
                case "Qatar":
                    $country->name_cn = "卡达";
                    break;
                case "Republic of Kosovo":
                    $country->name_cn = "科索沃";
                    break;
                case "Réunion":
                    $country->name_cn = "留尼汪";
                    break;
                case "Romania":
                    $country->name_cn = "罗马尼亚";
                    break;
                case "Russia":
                    $country->name_cn = "俄罗斯";
                    break;
                case "Rwanda":
                    $country->name_cn = "卢安达";
                    break;
                case "Saint Barthélemy":
                    $country->name_cn = "圣巴泰勒米";
                    break;
                case "Saint Helena":
                    $country->name_cn = "圣赫勒拿";
                    break;
                case "Saint Kitts and Nevis":
                    $country->name_cn = "圣克里斯多福及尼维斯";
                    break;
                case "Saint Lucia":
                    $country->name_cn = "圣露西亚";
                    break;
                case "Saint Martin":
                    $country->name_cn = "圣马丁岛";
                    break;
                case "Saint Pierre and Miquelon":
                    $country->name_cn = "圣皮埃尔和密克隆群岛";
                    break;
                case "Saint Vincent and the Grenadines":
                    $country->name_cn = "圣文森及格瑞那丁";
                    break;
                case "Samoa":
                    $country->name_cn = "西南太平洋的群岛";
                    break;
                case "San Marino":
                    $country->name_cn = "圣马力诺芬兰文";
                    break;
                case "São Tomé and Príncipe":
                    $country->name_cn = "圣多美及普林西比";
                    break;
                case "Saudi Arabia":
                    $country->name_cn = "沙乌地阿拉伯";
                    break;
                case "Senegal":
                    $country->name_cn = "塞内加尔";
                    break;
                case "Serbia":
                    $country->name_cn = "塞尔维亚";
                    break;
                case "Seychelles":
                    $country->name_cn = "塞席尔";
                    break;
                case "Sierra Leone":
                    $country->name_cn = "狮子山";
                    break;
                case "Singapore":
                    $country->name_cn = "新加坡";
                    break;
                case "Sint Maarten":
                    $country->name_cn = "荷属圣马丁";
                    break;
                case "Slovakia":
                    $country->name_cn = "斯洛伐克";
                    break;
                case "Slovenia":
                    $country->name_cn = "斯洛维尼亚";
                    break;
                case "Solomon Islands":
                    $country->name_cn = "索罗门群岛";
                    break;
                case "Somalia":
                    $country->name_cn = "索马利亚";
                    break;
                case "South Africa":
                    $country->name_cn = "南非";
                    break;
                case "South Georgia":
                    $country->name_cn = "南乔治亚和南桑威奇群岛";
                    break;
                case "South Korea":
                    $country->name_cn = "大韩民国";
                    break;
                case "South Sudan":
                    $country->name_cn = "南苏丹";
                    break;
                case "Spain":
                    $country->name_cn = "西班牙";
                    break;
                case "Sri Lanka":
                    $country->name_cn = "斯里兰卡";
                    break;
                case "Sudan":
                    $country->name_cn = "苏丹";
                    break;
                case "Suriname":
                    $country->name_cn = "苏利南";
                    break;
                case "Svalbard and Jan Mayen":
                    $country->name_cn = "斯瓦巴和扬马延";
                    break;
                case "Swaziland":
                    $country->name_cn = "史瓦济兰";
                    break;
                case "Sweden":
                    $country->name_cn = "瑞典";
                    break;
                case "Switzerland":
                    $country->name_cn = "瑞士";
                    break;
                case "Syria":
                    $country->name_cn = "叙利亚";
                    break;
                case "Taiwan":
                    $country->name_cn = "台湾";
                    break;
                case "Tajikistan":
                    $country->name_cn = "塔吉克斯坦";
                    break;
                case "Tanzania":
                    $country->name_cn = "坦尚尼亚";
                    break;
                case "Thailand":
                    $country->name_cn = "泰国";
                    break;
                case "East Timor":
                    $country->name_cn = "东帝汶";
                    break;
                case "Togo":
                    $country->name_cn = "多哥";
                    break;
                case "Tokelau":
                    $country->name_cn = "托克劳";
                    break;
                case "Tonga":
                    $country->name_cn = "东加王国";
                    break;
                case "Trinidad and Tobago":
                    $country->name_cn = "千里达及托贝哥";
                    break;
                case "Tunisia":
                    $country->name_cn = "突尼西亚";
                    break;
                case "Turkey":
                    $country->name_cn = "土耳其";
                    break;
                case "Turkmenistan":
                    $country->name_cn = "土库曼";
                    break;
                case "Turks and Caicos Islands":
                    $country->name_cn = "特克斯和凯科斯群岛";
                    break;
                case "Tuvalu":
                    $country->name_cn = "吐瓦鲁";
                    break;
                case "Uganda":
                    $country->name_cn = "乌干达";
                    break;
                case "Ukraine":
                    $country->name_cn = "乌克兰";
                    break;
                case "United Arab Emirates":
                    $country->name_cn = "阿拉伯联合大公国";
                    break;
                case "United Kingdom":
                    $country->name_cn = "英国";
                    break;
                case "United States":
                    $country->name_cn = "美国";
                    break;
                case "Uruguay":
                    $country->name_cn = "乌拉圭";
                    break;
                case "Uzbekistan":
                    $country->name_cn = "乌兹别克斯坦";
                    break;
                case "Vanuatu":
                    $country->name_cn = "瓦努阿图";
                    break;
                case "Venezuela":
                    $country->name_cn = "委内瑞拉";
                    break;
                case "Vietnam":
                    $country->name_cn = "越南";
                    break;
                case "Wallis and Futuna":
                    $country->name_cn = "瓦利斯和富图纳";
                    break;
                case "Western Sahara":
                    $country->name_cn = "西撒哈拉";
                    break;
                case "Yemen":
                    $country->name_cn = "叶门";
                    break;
                case "Zambia":
                    $country->name_cn = "尚比亚";
                    break;
                case "Zimbabwe":
                    $country->name_cn = "辛巴威";
                    break;
            }

            $country->save();
        }
    }
}
