<?php

use App\Models\LevelTranslation;
use Illuminate\Database\Seeder;

class levels extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        //
//    'skill_level_1' => 'Rookie',
//    'skill_level_2' => 'Expert',
//    'skill_level_3' => 'PRO',
//    'skill_level_4' => 'GURU',
//    'skill_level_5' => 'Godlike',
//    'skill_level_6' => 'Legends',
        $level = new LevelTranslation();
        $level->code_name = 'skill_level_1';
        $level->name_cn = '初级';
        $level->name_en = 'Rookie';
        $level->min_experience = 0;
        $level->save();

        $level2 = new LevelTranslation();
        $level2->code_name = 'skill_level_2';
        $level2->name_en = 'Expert';
        $level2->name_cn = '专业';
        $level2->min_experience = 10;
        $level2->save();

        $level3 = new LevelTranslation();
        $level3->code_name = 'skill_level_3';
        $level3->name_en = 'PRO';
        $level3->name_cn = '高手';
        $level3->min_experience = 100;
        $level3->save();


        $level4 = new LevelTranslation();
        $level4->code_name = 'skill_level_4';
        $level4->name_en = 'GURU';
        $level4->name_cn = '达人';
        $level4->min_experience = 1000;
        $level4->save();

        $level5 = new LevelTranslation();
        $level5->code_name = 'skill_level_5';
        $level5->name_en = 'Godlike';
        $level5->name_cn = '神人';
        $level5->min_experience = 10000;
        $level5->save();


        $level6 = new LevelTranslation();
        $level6->code_name = 'skill_level_6';
        $level6->name_cn = '传奇';
        $level6->name_en = 'Legends';
        $level6->min_experience = 100000;
        $level6->save();
    }
}
