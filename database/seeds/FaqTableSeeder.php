<?php

use Illuminate\Database\Seeder;

class FaqTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        \App\Models\Faq::truncate();
        $faq = new \App\Models\Faq();
        $faq->title_en = 'faq';
        $faq->title_cn = 'faq';
        $faq->parent_id = 0;
        $faq->save();

        $categoy = new \App\Models\Faq();
        $categoy->title_en = 'buyer';
        $categoy->title_cn = 'buyer';
        $categoy->parent_id = $faq->id;
        $categoy->save();

        $seller = new \App\Models\Faq();
        $seller->title_en = 'Seller';
        $seller->title_cn = 'Seller';
        $seller->parent_id = $faq->id;
        $seller->save();

        $popular_question = new \App\Models\Faq();
        $popular_question->title_en = 'Popular Question';
        $popular_question->title_cn = 'Popular Question';
        $popular_question->parent_id = 0;
        $popular_question->save();

        $suggusted_articles = new \App\Models\Faq();
        $suggusted_articles->title_en = 'Suggested articles';
        $suggusted_articles->title_cn = 'Suggested articles';
        $suggusted_articles->parent_id = 0;
        $suggusted_articles->save();

        //seller main faq
        $getting_started = new \App\Models\Faq();
        $getting_started->title_en = 'Getting Started';
        $getting_started->title_cn = 'Getting Started';
        $getting_started->parent_id = $seller->id;
        $getting_started->save();

        $make_payment = new \App\Models\Faq();
        $make_payment->title_en = 'Make Payments';
        $make_payment->title_cn = 'Make Payments';
        $make_payment->parent_id = $seller->id;
        $make_payment->save();

        $hire = new \App\Models\Faq();
        $hire->title_en = 'Hire';
        $hire->title_cn = 'Hire';
        $hire->parent_id = $seller->id;
        $hire->save();

        $work_with_client2 = new \App\Models\Faq();
        $work_with_client2->title_en = 'Work With Clients';
        $work_with_client2->title_cn = 'Work With Clients';
        $work_with_client2->parent_id = $seller->id;
        $work_with_client2->save();

        $account2 = new \App\Models\Faq();
        $account2->title_en = 'Account';
        $account2->title_cn = 'Account';
        $account2->parent_id = $seller->id;
        $account2->save();

        $app2 = new \App\Models\Faq();
        $app2->title_en = 'Apps & Messages';
        $app2->title_cn = 'Apps & Messages';
        $app2->parent_id = $seller->id;
        $app2->save();

//        buyer main faq
        $getting_started2 = new \App\Models\Faq();
        $getting_started2->title_en = 'Getting Started';
        $getting_started2->title_cn = 'Getting Started';
        $getting_started2->parent_id = $categoy->id;
        $getting_started2->save();

        $payment_protection = new \App\Models\Faq();
        $payment_protection->title_en = 'Legends Lair Payments protection';
        $payment_protection->title_cn = 'Legends Lair Payments protection';
        $payment_protection->parent_id = $getting_started2->id;
        $payment_protection->save();

        $as_freelancer = new \App\Models\Faq();
        $as_freelancer->title_en = 'Getting started as Freelancer';
        $as_freelancer->title_cn = 'Getting started as Freelancer';
        $as_freelancer->parent_id = $getting_started2->id;
        $as_freelancer->save();

        $as_client = new \App\Models\Faq();
        $as_client->title_en = 'how do i get started as a client?';
        $as_client->title_cn = 'how do i get started as a client?';
        $as_client->parent_id = $getting_started2->id;
        $as_client->save();

        $job_score = new \App\Models\Faq();
        $job_score->title_en = 'what is a job score?';
        $job_score->title_cn = 'what is a job score?';
        $job_score->parent_id = $getting_started2->id;
        $job_score->save();




        $freelancer_profile = new \App\Models\Faq();
        $freelancer_profile->title_en = 'Freelancer Profile';
        $freelancer_profile->title_cn = 'Freelancer Profile';
        $freelancer_profile->parent_id = $categoy->id;
        $freelancer_profile->save();

        $create_complete = new \App\Models\Faq();
        $create_complete->title_en = 'Create a 100% complete';
        $create_complete->title_cn = 'Create a 100% complete';
        $create_complete->parent_id = $freelancer_profile->id;
        $create_complete->save();

        $profile = new \App\Models\Faq();
        $profile->title_en = 'Freelancer profile';
        $profile->title_cn = 'Freelancer profile';
        $profile->parent_id = $freelancer_profile->id;
        $profile->save();

        $profile_visibility = new \App\Models\Faq();
        $profile_visibility->title_en = 'Set your profile visibility';
        $profile_visibility->title_cn = 'Set your profile visibility';
        $profile_visibility->parent_id = $freelancer_profile->id;
        $profile_visibility->save();

        $become_top = new \App\Models\Faq();
        $become_top->title_en = 'Become top rated';
        $become_top->title_cn = 'Become top rated';
        $become_top->parent_id = $freelancer_profile->id;
        $become_top->save();

        $become_top = new \App\Models\Faq();
        $become_top->title_en = 'Become top rated';
        $become_top->title_cn = 'Become top rated';
        $become_top->parent_id = $freelancer_profile->id;
        $become_top->save();


        $get_payments = new \App\Models\Faq();
        $get_payments->title_en = 'Get Payments';
        $get_payments->title_cn = 'Get Payments';
        $get_payments->parent_id = $categoy->id;
        $get_payments->save();

        $delay_payments = new \App\Models\Faq();
        $delay_payments->title_en = 'Can Holyday or outages delay my payments';
        $delay_payments->title_cn = 'Can Holyday or outages delay my payments';
        $delay_payments->parent_id = $get_payments->id;
        $delay_payments->save();

        $hourly_contracts = new \App\Models\Faq();
        $hourly_contracts->title_en = 'Get paid for my hourly contracts';
        $hourly_contracts->title_cn = 'Get paid for my hourly contracts';
        $hourly_contracts->parent_id = $get_payments->id;
        $hourly_contracts->save();

        $fixed = new \App\Models\Faq();
        $fixed->title_en = 'Get paid for my fixed-price contracts';
        $fixed->title_cn = 'Get paid for my fixed-price contracts';
        $fixed->parent_id = $get_payments->id;
        $fixed->save();


        $access_earnings = new \App\Models\Faq();
        $access_earnings->title_en = 'Access Your Earnings';
        $access_earnings->title_cn = 'Access Your Earnings';
        $access_earnings->parent_id = $get_payments->id;
        $access_earnings->save();


        $work_with_client = new \App\Models\Faq();
        $work_with_client->title_en = 'Work With Clients';
        $work_with_client->title_cn = 'Work With Clients';
        $work_with_client->parent_id = $categoy->id;
        $work_with_client->save();

        $account = new \App\Models\Faq();
        $account->title_en = 'Account';
        $account->title_cn = 'Account';
        $account->parent_id = $categoy->id;
        $account->save();

        $app = new \App\Models\Faq();
        $app->title_en = 'Apps & Messages';
        $app->title_cn = 'Apps & Messages';
        $app->parent_id = $categoy->id;
        $app->save();


    }
}
