<?php

use App\Models\Countries;
use App\Models\CountryLanguage;
use App\Models\Language;
use GuzzleHttp\Client;
use Illuminate\Database\Seeder;

class CountryLanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new Client();
        $res = $client->request('get', 'http://restcountries.eu/rest/v1/all', []);
//        $res = $client->request('get', 'http://data.okfn.org/data/core/language-codes/r/language-codes.json', []);
        $contents = \GuzzleHttp\json_decode($res->getBody()->getContents());
        $data = [];
        foreach ($contents as $content) {
            $country_id = Countries::where('name', $content->name)->first()->id;
            foreach ($content->languages as $lang) {
                $data[] = [
                    'country_id' => $country_id,
                    'language_id' => Language::where('alpha2', $lang)->first()->id,
                ];
            }

        }
        CountryLanguage::insert($data);
    }
}
