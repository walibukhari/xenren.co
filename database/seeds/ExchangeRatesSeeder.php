<?php

use App\Models\Currency;
use App\Models\ExchangeRate;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExchangeRatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        DB::table('currencies')->truncate();
        Currency::create(array(
            'id' => 1,
            'name_en' => 'USD',
            'name_cn' => '美元',
            'created_by' => '1',
            'updated_by' => null
        ));

        Currency::create(array(
            'id' => 2,
            'name_en' => 'RMB',
            'name_cn' => '人民币',
            'created_by' => 1,
            'updated_by' => null
        ));

        DB::table('exchange_rates')->truncate();
        ExchangeRate::create(array(
            'id' => 1,
            'from_id' => 1,
            'to_id' => 2,
            'rate' => 6.876207,
            'created_by' => 1,
            'updated_by' => null
        ));

        ExchangeRate::create(array(
            'id' => 2,
            'from_id' => 2,
            'to_id' => 1,
            'rate' => 0.145429,
            'created_by' => 1,
            'updated_by' => null
        ));

        \DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
