<?php

use App\Models\Countries;
use App\Models\Language;
use GuzzleHttp\Client;
use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement("SET foreign_key_checks=0");
        DB::table('countries')->truncate();
        DB::table('languages')->truncate();

        $client = new Client();
        $res = $client->request('get', 'http://restcountries.eu/rest/v1/all', []);
//        $res = $client->request('get', 'http://data.okfn.org/data/core/language-codes/r/language-codes.json', []);
        $contents = \GuzzleHttp\json_decode($res->getBody()->getContents());
        $data = [];
        foreach ($contents as $content) {
            $data[] = [
                'code' => $content->alpha2Code,
                'name' => $content->name,
            ];
        }
        Countries::insert($data);

        $client2 = new Client();
//        $res = $client2->request('get', 'https://restcountries.eu/rest/v1/all', []);
        $res2 = $client2->request('get', 'http://data.okfn.org/data/core/language-codes/r/language-codes.json', []);
        $languages = \GuzzleHttp\json_decode($res2->getBody()->getContents());
        $data2 = [];
        foreach ($languages as $lang) {
            $data2[] = [
                'alpha2' => $lang->alpha2,
                'name' => $lang->English,
            ];
        }
        Language::insert($data2);

        DB::statement("SET foreign_key_checks=1");
    }
}
