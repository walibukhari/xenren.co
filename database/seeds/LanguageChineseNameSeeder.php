<?php

use App\Models\Language;
use Illuminate\Database\Seeder;

class LanguageChineseNameSeeder extends Seeder
{
    /**
     * Run this seeds after seeder "CountryTableSeeder.php" finish run.
     * https://zh.wikipedia.org/wiki/ISO_639-1%E4%BB%A3%E7%A0%81%E8%A1%A8
     */
    public function run()
    {
        //add extra language
        $count = Language::where('alpha2', 'zh_HANS')
            ->where('name', 'Simplified Chinese')
            ->count();

        if( $count == 0 )
        {
            $language = new Language();
            $language->alpha2 = "zh_HANS";
            $language->name = "Simplified Chinese";
            $language->save();
        }

        $count = Language::where('alpha2', 'zh_HANT')
            ->where('name', 'Traditional Chinese')
            ->count();

        if( $count == 0 )
        {
            $language = new Language();
            $language->alpha2 = "zh_HANT";
            $language->name = "Traditional Chinese";
            $language->save();
        }

        $languages = Language::all();
        foreach( $languages as $language )
        {
            switch( $language->name )
            {
                case "Afar":
                    $language->name_cn = "阿法尔语";
                    break;
                case "Abkhazian":
                    $language->name_cn = "阿布哈兹语";
                    break;
                case "Avestan":
                    $language->name_cn = "阿维斯陀语";
                    break;
                case "Afrikaans":
                    $language->name_cn = "阿非利堪斯语";
                    break;
                case "Akan":
                    $language->name_cn = "阿坎语";
                    break;
                case "Amharic":
                    $language->name_cn = "阿姆哈拉语";
                    break;
                case "Aragonese":
                    $language->name_cn = "阿拉贡语";
                    break;
                case "Arabic":
                    $language->name_cn = "阿拉伯语";
                    break;
                case "Assamese":
                    $language->name_cn = "阿萨姆语";
                    break;
                case "Avaric":
                    $language->name_cn = "阿瓦尔语";
                    break;
                case "Aymara":
                    $language->name_cn = "艾马拉语";
                    break;
                case "Azerbaijani":
                    $language->name_cn = "阿塞拜疆语";
                    break;
                case "Bashkir":
                    $language->name_cn = "巴什基尔语";
                    break;
                case "Belarusian":
                    $language->name_cn = "白俄罗斯语";
                    break;
                case "Bulgarian":
                    $language->name_cn = "保加利亚语";
                    break;
                case "Bihari languages":
                    $language->name_cn = "比哈尔语";
                    break;
                case "Bislama":
                    $language->name_cn = "比斯拉玛语";
                    break;
                case "Bambara":
                    $language->name_cn = "班巴拉语";
                    break;
                case "Bengali":
                    $language->name_cn = "孟加拉语";
                    break;
                case "Tibetan":
                    $language->name_cn = "藏语";
                    break;
                case "Breton":
                    $language->name_cn = "布列塔尼语";
                    break;
                case "Bosnian":
                    $language->name_cn = "波斯尼亚语";
                    break;
                case "Catalan; Valencian":
                    $language->name_cn = "加泰隆语";
                    break;
                case "Chechen":
                    $language->name_cn = "车臣语";
                    break;
                case "Chamorro":
                    $language->name_cn = "查莫罗语";
                    break;
                case "Corsican":
                    $language->name_cn = "科西嘉语";
                    break;
                case "Cree":
                    $language->name_cn = "克里语";
                    break;
                case "Czech":
                    $language->name_cn = "捷克语";
                    break;
                case "Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic":
                    $language->name_cn = "教会斯拉夫语";
                    break;
                case "Chuvash":
                    $language->name_cn = "楚瓦什语";
                    break;
                case "Welsh":
                    $language->name_cn = "威尔士语";
                    break;
                case "Danish":
                    $language->name_cn = "丹麦语";
                    break;
                case "German":
                    $language->name_cn = "德语";
                    break;
                case "Divehi; Dhivehi; Maldivian":
                    $language->name_cn = "迪维希语";
                    break;
                case "Dzongkha":
                    $language->name_cn = "不丹语";
                    break;
                case "Ewe":
                    $language->name_cn = "埃维语";
                    break;
                case "Greek, Modern (1453-)":
                    $language->name_cn = "现代希腊语";
                    break;
                case "English":
                    $language->name_cn = "英语";
                    break;
                case "Esperanto":
                    $language->name_cn = "世界语";
                    break;
                case "Spanish; Castilian":
                    $language->name_cn = "西班牙语";
                    break;
                case "Estonian":
                    $language->name_cn = "爱沙尼亚语";
                    break;
                case "Basque":
                    $language->name_cn = "巴斯克语";
                    break;
                case "Persian":
                    $language->name_cn = "波斯语";
                    break;
                case "Fulah":
                    $language->name_cn = "富拉语";
                    break;
                case "Finnish":
                    $language->name_cn = "芬兰语";
                    break;
                case "Fijian":
                    $language->name_cn = "斐济语";
                    break;
                case "Faroese":
                    $language->name_cn = "法罗斯语";
                    break;
                case "French":
                    $language->name_cn = "法语";
                    break;
                case "Western Frisian":
                    $language->name_cn = "弗里西亚语";
                    break;
                case "Irish":
                    $language->name_cn = "爱尔兰语";
                    break;
                case "Gaelic; Scottish Gaelic":
                    $language->name_cn = "苏格兰盖尔语";
                    break;
                case "Galician":
                    $language->name_cn = "加利西亚语";
                    break;
                case "Guarani":
                    $language->name_cn = "瓜拉尼语";
                    break;
                case "Gujarati":
                    $language->name_cn = "古吉拉特语";
                    break;
                case "Manx":
                    $language->name_cn = "马恩岛语";
                    break;
                case "Hausa":
                    $language->name_cn = "豪萨语";
                    break;
                case "Hebrew":
                    $language->name_cn = "希伯来语";
                    break;
                case "Hindi":
                    $language->name_cn = "印地语";
                    break;
                case "Hiri Motu":
                    $language->name_cn = "希里莫图语";
                    break;
                case "Croatian":
                    $language->name_cn = "克罗地亚语";
                    break;
                case "Haitian; Haitian Creole":
                    $language->name_cn = "海地克里奥尔语";
                    break;
                case "Hungarian":
                    $language->name_cn = "匈牙利语";
                    break;
                case "Armenian":
                    $language->name_cn = "亚美尼亚语";
                    break;
                case "Herero":
                    $language->name_cn = "赫雷罗语";
                    break;
                case "Interlingua (International Auxiliary Language Association)":
                    $language->name_cn = "国际语A";
                    break;
                case "Indonesian":
                    $language->name_cn = "印尼语";
                    break;
                case "Interlingue; Occidental":
                    $language->name_cn = "国际语E";
                    break;
                case "Igbo":
                    $language->name_cn = "伊博语";
                    break;
                case "Sichuan Yi; Nuosu":
                    $language->name_cn = "四川彝语";
                    break;
                case "Inupiaq":
                    $language->name_cn = "依努庇克语";
                    break;
                case "Ido":
                    $language->name_cn = "伊多语";
                    break;
                case "Icelandic":
                    $language->name_cn = "冰岛语";
                    break;
                case "Italian":
                    $language->name_cn = "意大利语";
                    break;
                case "Inuktitut":
                    $language->name_cn = "伊努伊特语";
                    break;
                case "Japanese":
                    $language->name_cn = "日语";
                    break;
                case "Javanese":
                    $language->name_cn = "爪哇语";
                    break;
                case "Georgian":
                    $language->name_cn = "格鲁吉亚语";
                    break;
                case "Kongo":
                    $language->name_cn = "刚果语";
                    break;
                case "Kikuyu; Gikuyu":
                    $language->name_cn = "基库尤语";
                    break;
                case "Kuanyama; Kwanyama":
                    $language->name_cn = "宽亚玛语";
                    break;
                case "Kazakh":
                    $language->name_cn = "哈萨克语";
                    break;
                case "Kalaallisut; Greenlandic":
                    $language->name_cn = "格陵兰语";
                    break;
                case "Central Khmer":
                    $language->name_cn = "高棉语";
                    break;
                case "Kannada":
                    $language->name_cn = "坎纳达语";
                    break;
                case "Korean":
                    $language->name_cn = "朝鲜语";
                    break;
                case "Kanuri":
                    $language->name_cn = "卡努里语";
                    break;
                case "Kashmiri":
                    $language->name_cn = "克什米尔语";
                    break;
                case "Kurdish":
                    $language->name_cn = "库尔德语";
                    break;
                case "Komi":
                    $language->name_cn = "科米语";
                    break;
                case "Cornish":
                    $language->name_cn = "康沃尔语";
                    break;
                case "Kirghiz; Kyrgyz":
                    $language->name_cn = "吉尔吉斯语";
                    break;
                case "Latin":
                    $language->name_cn = "拉丁语";
                    break;
                case "Luxembourgish; Letzeburgesch":
                    $language->name_cn = "卢森堡语";
                    break;
                case "Ganda":
                    $language->name_cn = "干达语";
                    break;
                case "Limburgan; Limburger; Limburgish":
                    $language->name_cn = "林堡语";
                    break;
                case "Lingala":
                    $language->name_cn = "林加拉语";
                    break;
                case "Lao":
                    $language->name_cn = "老挝语";
                    break;
                case "Lithuanian":
                    $language->name_cn = "立陶宛语";
                    break;
                case "Luba-Katanga":
                    $language->name_cn = "卢巴-加丹加语";
                    break;
                case "Latvian":
                    $language->name_cn = "拉脱维亚语";
                    break;
                case "Malagasy":
                    $language->name_cn = "马达加斯加语";
                    break;
                case "Marshallese":
                    $language->name_cn = "马绍尔语";
                    break;
                case "Maori":
                    $language->name_cn = "毛利语";
                    break;
                case "Macedonian":
                    $language->name_cn = " 	马其顿语";
                    break;
                case "Malayalam":
                    $language->name_cn = "马拉亚拉姆语";
                    break;
                case "Mongolian":
                    $language->name_cn = "蒙古语";
                    break;
                case "Marathi":
                    $language->name_cn = "马拉提语";
                    break;
                case "Malay":
                    $language->name_cn = "马来语";
                    break;
                case "Maltese":
                    $language->name_cn = "马耳他语";
                    break;
                case "Burmese":
                    $language->name_cn = "缅甸语";
                    break;
                case "Nauru":
                    $language->name_cn = "瑙鲁语";
                    break;
                case "Bokmål, Norwegian; Norwegian Bokmål":
                    $language->name_cn = "挪威布克莫尔语";
                    break;
                case "Ndebele, North; North Ndebele":
                    $language->name_cn = "北恩德贝勒语";
                    break;
                case "Nepali":
                    $language->name_cn = "尼泊尔语";
                    break;
                case "Ndonga":
                    $language->name_cn = "恩敦加语";
                    break;
                case "Dutch; Flemish":
                    $language->name_cn = "荷兰语";
                    break;
                case "Norwegian Nynorsk; Nynorsk, Norwegian":
                    $language->name_cn = "新挪威语";
                    break;
                case "Norwegian":
                    $language->name_cn = "挪威语";
                    break;
                case "Ndebele, South; South Ndebele":
                    $language->name_cn = "南恩德贝勒语";
                    break;
                case "Navajo; Navaho":
                    $language->name_cn = "纳瓦霍语";
                    break;
                case "Chichewa; Chewa; Nyanja":
                    $language->name_cn = "尼扬贾语";
                    break;
                case "Occitan (post 1500); Provençal":
                    $language->name_cn = "奥克西唐语";
                    break;
                case "Ojibwa":
                    $language->name_cn = "奥吉布瓦语";
                    break;
                case "Oromo":
                    $language->name_cn = "阿芳·奥洛莫语";
                    break;
                case "Oriya":
                    $language->name_cn = "奥利亚语";
                    break;
                case "Ossetian; Ossetic":
                    $language->name_cn = "奥塞梯语";
                    break;
                case "Panjabi; Punjabi":
                    $language->name_cn = "旁遮普语";
                    break;
                case "Pali":
                    $language->name_cn = "巴利语";
                    break;
                case "Polish":
                    $language->name_cn = "波兰语";
                    break;
                case "Pushto; Pashto":
                    $language->name_cn = "普什图语";
                    break;
                case "Portuguese":
                    $language->name_cn = "葡萄牙语";
                    break;
                case "Quechua":
                    $language->name_cn = "凯楚亚语";
                    break;
                case "Romansh":
                    $language->name_cn = "利托-罗曼语";
                    break;
                case "Rundi":
                    $language->name_cn = "基隆迪语";
                    break;
                case "Romanian; Moldavian; Moldovan":
                    $language->name_cn = "罗马尼亚语";
                    break;
                case "Russian":
                    $language->name_cn = "俄语";
                    break;
                case "Kinyarwanda":
                    $language->name_cn = "基尼阿万达语";
                    break;
                case "Sanskrit":
                    $language->name_cn = "梵语";
                    break;
                case "Sardinian":
                    $language->name_cn = "撒丁语";
                    break;
                case "Sindhi":
                    $language->name_cn = "信德语";
                    break;
                case "Northern Sami":
                    $language->name_cn = "北萨米语";
                    break;
                case "Sango":
                    $language->name_cn = "桑戈语";
                    break;
                case "Sinhala; Sinhalese":
                    $language->name_cn = "僧加罗语";
                    break;
                case "Slovak":
                    $language->name_cn = "斯洛伐克语";
                    break;
                case "Slovenian":
                    $language->name_cn = "斯洛文尼亚语";
                    break;
                case "Samoan":
                    $language->name_cn = "萨摩亚语";
                    break;
                case "Shona":
                    $language->name_cn = "绍纳语";
                    break;
                case "Somali":
                    $language->name_cn = "索马里语";
                    break;
                case "Albanian":
                    $language->name_cn = "阿尔巴尼亚语";
                    break;
                case "Serbian":
                    $language->name_cn = "塞尔维亚语";
                    break;
                case "Swati":
                    $language->name_cn = "塞斯瓦替语";
                    break;
                case "Sotho, Southern":
                    $language->name_cn = "塞索托语";
                    break;
                case "Sundanese":
                    $language->name_cn = "巽他语";
                    break;
                case "Swedish":
                    $language->name_cn = "瑞典语";
                    break;
                case "Swahili":
                    $language->name_cn = "斯瓦希里语";
                    break;
                case "Tamil":
                    $language->name_cn = "泰米尔语";
                    break;
                case "Telugu":
                    $language->name_cn = "泰卢固语";
                    break;
                case "Tajik":
                    $language->name_cn = "塔吉克语";
                    break;
                case "Thai":
                    $language->name_cn = "傣语";
                    break;
                case "Tigrinya":
                    $language->name_cn = "提格里尼亚语";
                    break;
                case "Turkmen":
                    $language->name_cn = "土库曼语";
                    break;
                case "Tagalog":
                    $language->name_cn = "他加禄语";
                    break;
                case "Tswana":
                    $language->name_cn = "塞茨瓦纳语";
                    break;
                case "Tonga (Tonga Islands)":
                    $language->name_cn = "汤加语";
                    break;
                case "Turkish":
                    $language->name_cn = "土耳其语";
                    break;
                case "Tsonga":
                    $language->name_cn = "宗加语";
                    break;
                case "Tatar":
                    $language->name_cn = "塔塔尔语";
                    break;
                case "Twi":
                    $language->name_cn = "特威语";
                    break;
                case "Tahitian":
                    $language->name_cn = "塔希提语";
                    break;
                case "Uighur; Uyghur":
                    $language->name_cn = "维吾尔语";
                    break;
                case "Ukrainian":
                    $language->name_cn = "乌克兰语";
                    break;
                case "Urdu":
                    $language->name_cn = "乌尔都语";
                    break;
                case "Uzbek":
                    $language->name_cn = "乌兹别克语";
                    break;
                case "Venda":
                    $language->name_cn = "文达语";
                    break;
                case "Vietnamese":
                    $language->name_cn = "越南语";
                    break;
                case "Volapük":
                    $language->name_cn = "沃拉普克语";
                    break;
                case "Walloon":
                    $language->name_cn = "沃伦语";
                    break;
                case "Wolof":
                    $language->name_cn = "沃洛夫语";
                    break;
                case "Xhosa":
                    $language->name_cn = "科萨语";
                    break;
                case "Yiddish":
                    $language->name_cn = "依地语";
                    break;
                case "Yoruba":
                    $language->name_cn = "约鲁巴语";
                    break;
                case "Zhuang; Chuang":
                    $language->name_cn = "壮语";
                    break;
                case "Chinese":
                    $language->name_cn = "中文";
                    break;
                case "Zulu":
                    $language->name_cn = "祖鲁语";
                    break;
                case "Simplified Chinese":
                    $language->name_cn = "简体中文";
                    break;
                case "Traditional Chinese":
                    $language->name_cn = "繁体中文";
                    break;
            }

            $language->save();
        }

    }
}
