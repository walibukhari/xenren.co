<?php

use App\Models\SharedOfficeProductCategory;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class SeedSharedOfficeProductCategoryTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    SharedOfficeProductCategory::truncate();
	    SharedOfficeProductCategory::insert([
		    [
			    'name' => '1. Seat',
			    'name_ch' => '1. 座位',
			    'created_at' => Carbon::now(),
			    'updated_at' => Carbon::now()
		    ],
		    [
			    'name' => '2. Meeting Room',
			    'name_ch' => '2. 会议室',
			    'created_at' => Carbon::now(),
			    'updated_at' => Carbon::now()
		    ],
		    [
			    'name' => '3. Vending Machine',
			    'name_ch' => '3. 自动售货机',
			    'created_at' => Carbon::now(),
			    'updated_at' => Carbon::now()
		    ],
		    [
			    'name' => '4. Coffee Machine',
			    'name_ch' => '4. 咖啡机',
			    'created_at' => Carbon::now(),
			    'updated_at' => Carbon::now()
		    ],
		    [
			    'name' => '5. VR Room',
			    'name_ch' => '5. VR室',
			    'created_at' => Carbon::now(),
			    'updated_at' => Carbon::now()
		    ],
		    [
			    'name' => '6. Door',
			    'name_ch' => '6. 门',
			    'created_at' => Carbon::now(),
			    'updated_at' => Carbon::now()
		    ],
		    [
			    'name' => '7. Phone Room',
			    'name_ch' => '7. 电话室',
			    'created_at' => Carbon::now(),
			    'updated_at' => Carbon::now()
		    ],
		    [
			    'name' => '8. Dining Room',
			    'name_ch' => '8. 餐厅',
			    'created_at' => Carbon::now(),
			    'updated_at' => Carbon::now()
		    ],
		    [
			    'name' => '9. Table',
			    'name_ch' => '9. 表',
			    'created_at' => Carbon::now(),
			    'updated_at' => Carbon::now()
		    ],
		    [
			    'name' => '10. Wall 1',
			    'name_ch' => '10. 墙1',
			    'created_at' => Carbon::now(),
			    'updated_at' => Carbon::now()
		    ],
		    [
			    'name' => '11. Wall 2',
			    'name_ch' => '11. 墙2',
			    'created_at' => Carbon::now(),
			    'updated_at' => Carbon::now()
		    ],
		    [
			    'name' => '12. Wall 3',
			    'name_ch' => '12. 墙3',
			    'created_at' => Carbon::now(),
			    'updated_at' => Carbon::now()
		    ],
		    [
			    'name' => '13. Wall 5',
			    'name_ch' => '13. 墙5',
			    'created_at' => Carbon::now(),
			    'updated_at' => Carbon::now()
		    ]
	    ]);
    }
}
