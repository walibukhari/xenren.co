<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class AdminInUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::find(1);

        if (!$user) {
            //create new
            $user = new User();
            $user->id = 1;
        }

        $user->email = "admin@admin.com";
        $user->password = bcrypt("qweasd");
        $user->real_name = "Admin";
        $user->about_me = "It is all about admin";
        $user->id_card_no = "1234567890";
        $user->gender = 1;
        $user->date_of_birth = "1/1/1980";
        $user->address = "Fake Addres Admin";
        $user->handphone_no = "1112223333";
        $user->skype_id = "admin_skype";
        $user->wechat_id = "admin_wechat";
        $user->qq_id = "admin_qq";
        $user->nick_name = "Admin";
        $user->save();
    }
}
