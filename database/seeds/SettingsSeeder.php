<?php

use Illuminate\Database\Seeder;
use App\Models\Settings;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hourly_on_off = Settings::where('key','=',Settings::HOURLY_ON_OFF)->first();
        if(empty($hourly_on_off)) {
            Settings::create([
                'key' => 'hourly_on_off',
                'value' => 0
            ]);
        }

        $release_time = Settings::where('key','=',Settings::SET_RELEASE_TIME)->first();
        if(empty($release_time)) {
            Settings::create([
                'key' => 'release_time',
                'value' => 0
            ]);
        }

        $release_time_un_verified = Settings::where('key','=',Settings::SET_RELEASE_TIME_UNVERIFIED)->first();
        if(empty($release_time_un_verified)) {
            Settings::create([
                'key' => 'release_time_un_verified',
                'value' => 0
            ]);
        }
    }
}
