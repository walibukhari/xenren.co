<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	   \Schema::disableForeignKeyConstraints();
         $this->call(WorldContinentsTableSeeder::class);
         $this->call(WorldContinentsLocaleTableSeeder::class);
         $this->call(WorldCountriesTableSeeder::class);
         $this->call(WorldCountriesLocaleTableSeeder::class);
         $this->call(WorldDivisionsTableSeeder::class);
         $this->call(WorldDivisionsLocaleTableSeeder::class);
         $this->call(WorldCitiesTableSeeder::class);
         $this->call(WorldCitiesLocaleTableSeeder::class);
         $this->call(SettingsSeeder::class);
	      \Schema::enableForeignKeyConstraints();
    }
}
