-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 24, 2018 at 11:52 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin_xenren`
--

-- --------------------------------------------------------

--
-- Table structure for table `shared_office_shfacilitie`
--

DROP TABLE IF EXISTS `shared_office_shfacilitie`;
CREATE TABLE IF NOT EXISTS `shared_office_shfacilitie` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `shared_office_id` int(10) NOT NULL,
  `shfacilitie_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shared_office_shfacilitie_shared_office_id_foreign` (`shared_office_id`) USING BTREE,
  KEY `shared_office_shfacilitie_shfacilitie_id_foreign` (`shfacilitie_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
