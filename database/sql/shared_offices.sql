-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 22, 2017 at 05:02 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin_xenren`
--

-- --------------------------------------------------------

--
-- Table structure for table `shared_offices`
--

DROP TABLE IF EXISTS `shared_offices`;
CREATE TABLE IF NOT EXISTS `shared_offices` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `office_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `qr` text CHARACTER SET utf8,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pic_size` tinyint(4) NOT NULL COMMENT '1-1366 X 768, 2-683 X 768 ',
  `staff_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sharedoffice_staff_id_foreign` (`staff_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shared_offices`
--

INSERT INTO `shared_offices` (`id`, `office_name`, `location`, `description`, `qr`, `image`, `pic_size`, `staff_id`, `created_at`, `updated_at`) VALUES
(10, 'Skyoffice', 'Belgrade', 'sdfsdfs xcxc', 'https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=143498184', 'uploads/sharedoffice/news_E4nCSRsaqqmZEAyV3oXsdFxkiGO2ZX8ivfzyuuOI..jpg', 1, 1, '2017-11-05 17:29:17', '2017-11-06 22:17:53'),
(11, 'SkyofficeNo1', 'fdfa', 'dadasdfas', 'https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=143498184', 'uploads/sharedoffice/sahredoffice_dF0X2MUViJiZrnG2SIXFJZqOQq6kSnTGuByoxTMv..jpg', 1, 1, '2017-11-12 17:18:21', '2017-11-15 14:44:02');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
