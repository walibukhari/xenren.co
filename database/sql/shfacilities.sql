-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 24, 2018 at 11:49 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin_xenren`
--

-- --------------------------------------------------------

--
-- Table structure for table `shfacilities`
--

DROP TABLE IF EXISTS `shfacilities`;
CREATE TABLE IF NOT EXISTS `shfacilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `alt` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `order_image` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shfacilities`
--

INSERT INTO `shfacilities` (`id`, `title`, `description`, `alt`, `order_image`, `created_at`, `updated_at`) VALUES
(1, 'WiFi', 'wireless is free in shared office', NULL, 1, NULL, NULL),
(2, 'Printer', 'Printing device for printing documents', NULL, 2, NULL, NULL),
(3, 'Lunch', NULL, NULL, 3, NULL, NULL),
(4, 'Vending Machine', NULL, NULL, 4, NULL, NULL),
(5, 'Vr Facility', NULL, NULL, 5, NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
