Update the socialiteproviders to latest version need laravel 5.4 but project only laravel 5.2
API for wechat have a bit adjustment due to not include the open id into parameter

Step Modify:
1. copy /public/wechat_login/AbstractProvider.php to overwrite /vendor/laravel/socialite/src/Two/AbstractProvider.php
2. copy /public/wechat_login/Provider.php to overwrite /vendor/socialiteproviders/weixin-web/src/Provider.php
3. add dummy function "getUserByTokenAndOpenId($token, $openId)"
    to /vendor/socialiteproviders/qq/src/Provider.php
    to /vendor/socialiteproviders/weibo/src/Provider.php
    to /vendor/socialiteproviders/weixin/src/Provider.php