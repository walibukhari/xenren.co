$.widget("ui.autocomplete", $.ui.autocomplete, {
    options : $.extend({}, this.options, {
        multiselect: false
    }),
    _create: function(){
        this._super();

        var self = this,
            o = self.options;

        if (o.multiselect) {
            console.log('multiselect true');

            self.selectedItems = {};
            self.multiselect = $("<div></div>")
                .addClass("ui-autocomplete-multiselect ui-state-default ui-widget")
                .css("width", self.element.width())
                .insertBefore(self.element)
                .append(self.element)
                .bind("click.autocomplete", function(){
                    self.element.focus();
                });
            $('.ui-autocomplete-multiselect').css('border-top-left-radius','25px');
            $('.ui-autocomplete-multiselect').css('border-top-right-radius','25px');
            $('.ui-autocomplete-multiselect').css('border-bottom-left-radius','25px');
            $('.ui-autocomplete-multiselect').css('border-bottom-right-radius','25px');

            var fontSize = parseInt(self.element.css("fontSize"), 10);
            function autoSize(e){
                var $this = $(this);
                $this.width(1).width(this.scrollWidth+fontSize-1);
            };
            $('.ui-autocomplete-multiselect').css('border-top-left-radius','25px');
            $('.ui-autocomplete-multiselect').css('border-top-right-radius','25px');
            $('.ui-autocomplete-multiselect').css('border-bottom-left-radius','25px');
            $('.ui-autocomplete-multiselect').css('border-bottom-right-radius','25px');
            $('#autocomplete').attr('placeholder','Search');
            $('#autocomplete').on('keypress keyup keydown', function(e){
                var value = $("#autocomplete").val();
                if(value === '')
                {
                    $('.ui-autocomplete-multiselect').css('border-top-left-radius','25px');
                    $('.ui-autocomplete-multiselect').css('border-top-right-radius','25px');
                    $('.ui-autocomplete-multiselect').css('border-bottom-left-radius','25px');
                    $('.ui-autocomplete-multiselect').css('border-bottom-right-radius','25px');
                }
            });
            jQuery.ui.autocomplete.prototype._resizeMenu = function () {
                var ul = this.menu.element;
                ul.outerWidth(this.element.outerWidth());
            }

            $('.btn-searchjqueryauto').click(function(){
                $('#searchIc').hide();
                $('#searchIcLdr').show();

                var idD = [];
                window.skillIdData.map(function (d) {
                    idD.push(d.id);
                });
                var skillIdList = idD.join();
                console.log(skillIdList);
                console.log('skillIdList');
                if( skillIdList == null )
                {
                    var postVal = window.skillIdData[0].id;
                    console.log('postVal');
                    console.log(postVal);
                    console.log('postVal');
                    $(this).attr('data-skill-id-list', postVal);
                    skillIdList = $(this).attr('data-skill-id-list')
                }

                var searchType = window.localStorage.getItem('selectBtn');
                // if( searchType == null )
                // {
                //     $('#search-type').show();
                //     $('#search-type').text(lang.please_select_freelancer_project_cofounder);
                //
                //     // alertError(lang.please_choose_search_type);
                //     return true;
                // }
                // else
                // {
                $('#search-type').text("");
                $('#search-type').hide();
                // }
                if( searchType == 'find-project' )
                {
                    window.location.href = url.jobs + '?project_type=1&order=1&type=1&skill_id_list=' + skillIdList;
                }
                else if( searchType == 'find-freelancer' )
                {
                    window.location.href = url.findExperts + '?skill_id_list=' + skillIdList;
                }
                else if( searchType == 'find-co-founder' )
                {
                    window.location.href = url.findExperts + '?skill_id_list=' + skillIdList + '&status_id=1';
                }
                else if(searchType == null) {
                    window.location.href = url.findExperts + '?skill_id_list=' + skillIdList;
                }
            });

            window.skillIdData = [];
            o.select = function(e, ui) {
                $('.ui-autocomplete-multiselect').css('border-top-left-radius','25px');
                $('.ui-autocomplete-multiselect').css('border-top-right-radius','25px');
                $('.ui-autocomplete-multiselect').css('border-bottom-left-radius','25px');
                $('.ui-autocomplete-multiselect').css('border-bottom-right-radius','25px');
                var length = $("div.ui-autocomplete-multiselect").children().length;
                if(length <=6) {
                    console.log('ui.item.value');
                    var getItem = sessionStorage.getItem('item');
                    console.log('getItem');
                    console.log(getItem);
                    if(getItem == 'no-found') {
                        console.log('not push');
                        return false;
                    } else {
                        console.log('push item');
                        window.skillIdData.push(ui.item);
                        self.selectedItems[ui.item.label] = ui.item;
                        self._value("");
                        $("<div></div>")
                            .addClass("ui-autocomplete-multiselect-item")
                            .text(ui.item.value)
                            .append(
                                $("<span></span>")
                                    .addClass("ui-icon ui-icon-close")
                                    .click(function () {
                                        var item = $(this).parent();
                                        delete self.selectedItems[item.text()];
                                        item.remove();
                                        $("#warnings").html('');
                                        console.log("remove item");
                                        $('.ui-autocomplete-multiselect').css('border-top-left-radius', '25px');
                                        $('.ui-autocomplete-multiselect').css('border-top-right-radius', '25px');
                                        $('.ui-autocomplete-multiselect').css('border-bottom-left-radius', '25px');
                                        $('.ui-autocomplete-multiselect').css('border-bottom-right-radius', '25px');
                                        var totalWidth = -1;
                                        $('.ui-autocomplete-multiselect-item').each(function (index) {
                                            var inputwidth = $('#autocomplete').width();
                                            totalWidth += parseInt($(this).width(), 10);
                                        });
                                        console.log(totalWidth);
                                        if (totalWidth < 400) {
                                            console.log("-1111");
                                            $("div.ui-autocomplete-multiselect").css('min-height', '53px');
                                            $('#autocomplete').removeClass('dropdownInput');
                                            $('#autocomplete').addClass('autoooComplete');
                                        }
                                    })
                            )
                            .insertBefore(self.element);
                        var width = $("div.ui-autocomplete-multiselect").width();
                        var countDiv = $("div.ui-autocomplete-multiselect > div").size();
                        var countDivWidth = $("div.ui-autocomplete-multiselect > div").width();
                        var totalWidth = 0;
                        $('#autocomplete').attr('placeholder', 'search...');
                        $('.ui-autocomplete-multiselect-item').each(function (index) {
                            var inputwidth = $('#autocomplete').width();
                            totalWidth += parseInt($(this).width(), 10);
                        });
                        if (totalWidth >= 300 || totalWidth >= 380 || totalWidth > 380 || totalWidth === 380 && countDiv >= 4) {
                            '<br>'
                            console.log("working fine");
                            $("div.ui-autocomplete-multiselect").css('min-height', '100px');
                            $('.ui-autocomplete-multiselect-item').css('margin-bottom', '10px');
                            $('#autocomplete').removeClass('autoooComplete');
                            $('#autocomplete').addClass('dropdownInput');
                        }
                        return false;
                    }
                } else {
                    $('.ui-autocomplete-multiselect').css('border-top-left-radius','25px');
                    $('.ui-autocomplete-multiselect').css('border-top-right-radius','25px');
                    $('.ui-autocomplete-multiselect').css('border-bottom-left-radius','25px');
                    $('.ui-autocomplete-multiselect').css('border-bottom-right-radius','25px');
                    console.log("length is greater then 5");
                    $("#warnings").html("<span style='color:red;'>Max skills reached</span>");
                    return false;
                }
            }
        }

        return this;
    }
});
