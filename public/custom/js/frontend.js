$(document).ready(function(){

    var width = $(window).width();
    $.ajax({
        url: "/home/storeScreenWidth",
        data : {screenwidth:width}
    }).done(function() {
        //alert('Successfully Send : ' + width);
    });

    if( width > 1300){
        var margin = ( width - 1300) / 3;
        $('.page-header.navbar.navbar-fixed-top').css('margin-right', margin).css('margin-left', margin);
        $('body').attr('style', 'margin-right: ' + margin + 'px !important; margin-left: ' + margin + 'px !important;' );
    }else{
        $('.page-header.navbar.navbar-fixed-top').css('margin-right', 0).css('margin-left', 0);
        $('body').attr('style', 'margin-right: ' + 0 + 'px !important; margin-left: ' + 0 + 'px !important;' );
    }

    $( window ).resize(function() {
        var width = $(window).width();
        if( width > 1300){
            var margin = ( width - 1300) / 3;
            $('.page-header.navbar.navbar-fixed-top').css('margin-right', margin).css('margin-left', margin);
            $('body').attr('style', 'margin-right: ' + margin + 'px !important; margin-left: ' + margin + 'px !important;' );
        }else{
            //mean not need margin
            width = 1300;

            $('.page-header.navbar.navbar-fixed-top').css('margin-right', 0).css('margin-left', 0);
            $('body').attr('style', 'margin-right: ' + 0 + 'px !important; margin-left: ' + 0 + 'px !important;' );
        }

        $.ajax({
            url: "/home/storeScreenWidth",
            data : {screenwidth:width}
        }).done(function() {
            //alert('Successfully Send : ' + width);
        });
    });

    //$('#btn-change-status-submit').on('click', function () {
    //    var status = $('#status').val();
    //    var url = $(this).data('url');
    //    var token = $('input[name="_token"]').val();
    //    $.ajax({
    //        url: url,
    //        method: 'post',
    //        data: {'_token': token, 'status': status},
    //    dataType: 'json',
    //        beforeSend: function () {
    //    },
    //    success: function (resp) {
    //        console.log(resp);
    //        // if (resp.status == 'success') {
    //        $('#modalAddUserStatus').modal('hide');
    //        //     window.location.reload();
    //        // } else {
    //        //     alertError(resp.msg);
    //        // }
    //    },
    //    error: function (resp) {
    //        alert('error');
    //    }
    //});
//});

    //$('.status-box-label').on('click', function() {
    //    $('.check').toggleClass('check');
    //    $(this).toggleClass('check');
    //});

    $(window).scroll(function() {
        //var scrollTop = $(window).scrollTop();
        //var windowHeight = $(window).height();
        //var documentHeight = $(document).height();
        var footer_height = $(".page-footer").outerHeight() + 100;
        if($(window).scrollTop() >= 100)
        {
            $('.page-sidebar-wrapper').addClass("floating-menu");
        }
        else
        {
            $('.page-sidebar-wrapper').removeClass("floating-menu");
        }

        if($(window).scrollTop() + $(window).height() > $(document).height() - footer_height) {
            $(".page-sidebar-wrapper").css({top: 'unset', bottom: 0});
            $('.page-sidebar-wrapper').removeClass("floating-menu");
        }
        else
        {
            $(".page-sidebar-wrapper").css({top: '60px', bottom: 'unset', 'padding-top':' unset'});
        }
    });

    $('#main-menu-jobs').click(function(){
        var url = $(this).data('href');
        window.location.href = url;
    });

    $('#main-menu-find-experts').click(function(){
        var url = $(this).data('href');
        window.location.href = url;
    });

    var msHeaderSearchBar = $('#msHeaderSearchBar').magicSuggest({
        allowFreeEntries: false,
        allowDuplicates: false,
        hideTrigger: true,
        placeholder: g_lang.search,
        data: headerSearchSkillList,
        method: 'get',
        highlight: true,
        //noSuggestionText: noSuggestionText,
        renderer: function(data){
            if( data.description == null )
            {
                return '<b>' + data.name + '</b>' ;
            }
            else
            {
                var name = data.name.split("|");

                return '<b>' + name[0] + '</b> | <span class="search-desc">' + data.description + '</span>';
            }

        },
        selectionRenderer: function(data){
            result = data.name;
            name = result.split("|")[0];
            return name;
        }

    });


    $(msHeaderSearchBar).on(
        'selectionchange', function(e, cb, s){
            $('.btn-search').attr('data-skill-id-list', cb.getValue());
        }
    );

    $(msHeaderSearchBar).on('keyup', function(e, m, v){
        if(msHeaderSearchBar.getRawValue().length > 0){
            $('#s1').show();
            $('#s2').hide();
        }
        else {
            $('#s1').hide();
            $('#s2').show();
        }
    });

    $('.header-search-bar').click(function(){
        var skillList =  $(this).data('skill-id-list');
        var baseUrl = $(this).data('base-url');
        var redirectUrl  = baseUrl + '?&skill_id_list=' + skillList;
        window.location.href = redirectUrl;
    });

    // $('#imgAvatar').click(function(){
    //     window.location.href = g_url.user_center;
    // });

});