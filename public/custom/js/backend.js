// Show image preview of the file to be uploaded
function imageFileInputChange(element) {
    var reader = new FileReader();
    reader.onload = function (e) {
        var target = element.getAttribute('target');
        $('#' + target).attr('src', e.target.result);
    };
    reader.readAsDataURL(element.files[0]);
}

