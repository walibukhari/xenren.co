window.listgeocoder ='';
window.listmap = '';
window.markers = [];
window.selectedPlace = [];
var marker = false;
function initListMap(){
	//The center location of our map.
	var centerOfMap = new google.maps.LatLng(52.357971, -6.516758);

	//Map options.
	var options = {
		center: centerOfMap, //Set center.
		zoom: 7, //The zoom value.
		mapTypeId: 'roadmap'
	};

	//Create the map object.
	var listmap = new google.maps.Map(document.getElementById('listMap'), options);

	//Listen for any clicks on the map.
	google.maps.event.addListener(listmap, 'click', function(event) {

		removeAllMarkersFromMap(window.markers, marker);

		var listgeocoder = new google.maps.Geocoder;
		var latlng = {lat: parseFloat(event.latLng.lat()), lng: parseFloat(event.latLng.lng())};
		listgeocoder.geocode({'location': latlng}, function(place, status) {
			if (status === 'OK') {
				pickRequiredInformation(place[0]);
			} else {
				window.alert('Geocoder failed due to: ' + status);
			}
		});
		//Get the location that the user clicked.
		var clickedLocation = event.latLng;
		//If the marker hasn't been added.
		if(marker === false){
			//Create the marker.
			marker = new google.maps.Marker({
				position: clickedLocation,
				title: 'place.name',
				map: listmap,
				draggable: true //make it draggable
			});
			//Listen for drag events!
			google.maps.event.addListener(marker, 'dragend', function(event){
				// markerLocation();
			});
		} else{
			//Marker has already been added, so just change its location.
			marker.setPosition(clickedLocation);
		}
		//Get the marker's location.
		// markerLocation();
	});

	// Create the search box and link it to the UI element.
	var input = document.getElementById('pac-input');
	var searchBox = new google.maps.places.SearchBox(input);
	// map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	// Bias the SearchBox results towards current map's viewport.
	listmap.addListener('bounds_changed', function() {
		// console.log('bounds_changed');
		// console.log(listmap.getBounds());
		searchBox.setBounds(listmap.getBounds());
	});

	// Listen for the event fired when the user selects a prediction and retrieve
	// more details for that place.
	searchBox.addListener('places_changed', function() {
		// console.log('place searched markers');
		// console.log(window.markers);

		google.maps.Map.prototype.markers = new Array();

		google.maps.Map.prototype.addMarker = function(marker) {
			this.markers[this.markers.length] = marker;
		};

		google.maps.Map.prototype.getMarkers = function() {
			return this.markers;
		};

		google.maps.Map.prototype.clearMarkers = function() {
			for(var i=0; i<this.markers.length; i++){
				this.markers[i].setMap(null);
			}
			this.markers = new Array();
		};

		var places = searchBox.getPlaces();

		if (places.length == 0) {
			return;
		}

		// Clear out the old markers.
		window.markers.forEach(function(marker) {
			// console.log('in loop of search box')
			// console.log(marker)
			marker.setMap(null);
		});
		window.markers = [];

		// For each place, get the icon, name and location.
		var bounds = new google.maps.LatLngBounds();
		places.forEach(function(place) {
			if (!place.geometry) {
				return;
			}
			var icon = {
				url: place.icon,
				size: new google.maps.Size(71, 71),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(17, 34),
				scaledSize: new google.maps.Size(25, 25)
			};

			// Create a marker for each place.
			window.markers.push(new google.maps.Marker({
				map: listmap,
				title: place.name,
				position: place.geometry.location,
				draggable: true
			}));

			if (place.geometry.viewport) {
				// Only geocodes have viewport.
				bounds.union(place.geometry.viewport);
			} else {
				bounds.extend(place.geometry.location);
			}
			// console.log('place.geometry.location');
			pickRequiredInformation(place);
		});
		listmap.fitBounds(bounds);
	});

}

function removeAllMarkersFromMap(markers, marker) {
	// Clear out the old markers.
	markers.forEach(function(marker) {
		marker.setMap(null);
	});
	markers = [];
}

function pickRequiredInformation(place) {
	window.selectedPlace = [];
	var city = [];
	var state = [];
	var country = [];
	window.selectedPlace = [];
	place.address_components.forEach(function(v){
		if(v.types[0] == 'country') {
			country = {
				'long_name': v.long_name,
				'short_name': v.short_name
			};
		}

		if(v.types[0] == 'administrative_area_level_1') {
			state = {
				'long_name': v.long_name,
				'short_name': v.short_name
			};
		}

		if(v.types[0] == 'locality') {
			city = {
				'long_name': v.long_name,
				'short_name': v.short_name
			};
		}
	});
	var placeName = place.name ? place.name : place.formatted_address;
	window.selectedPlace.push({
		'address': place.address_components,
		'formatted_address': place.formatted_address,
		'name': place.name,
		'city': city,
		'country': country,
		'state': state,
		'lat': place.geometry.location.lat(),
		'lng': place.geometry.location.lng()
	});
	$('#pac-input').val(placeName);
}
google.maps.event.addDomListener(window, 'load', initListMap);
