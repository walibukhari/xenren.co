jQuery(
    function ($) {
        $('.btn-delete').click(function(e){
            var url = $(this).data('url');
            var inboxId = $(this).data('inbox-id');

            $.ajax({
                url: url,
                dataType: 'json',
                data: {inboxId: inboxId},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if(result.status == 'success') {
                        alertSuccess(result.msg);
                        $('#inbox-' + inboxId).hide();
                    }else{
                        alertError(result.msg);
                    }
                }
            });
        });

        $('.btn-undelete').click(function(e){
            var url = $(this).data('url');
            var inboxId = $(this).data('inbox-id');

            $.ajax({
                url: url,
                dataType: 'json',
                data: {inboxId: inboxId},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if(result.status == 'success') {
                        alertSuccess(result.msg);
                        $('#inbox-' + inboxId).hide();
                    }else{
                        alertError(result.msg);
                    }
                }
            });
        });

        $('.acceptContactInfoRequest').click(function(){
            var receiverId = $(this).data('receiver-id');
            var inboxId = $(this).data('inbox-id');
            var url = $(this).data('url');
            $.ajax({
                url: url,
                dataType: 'json',
                data: {receiverId: receiverId, inboxId: inboxId},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if(result.status == 'success') {
                        alertSuccess(result.msg);
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }else{
                        alertError(result.msg);
                    }
                }
            });
        });

        $('.rejectContactInfoRequest').click(function(){
            var receiverId = $(this).data('receiver-id');
            var inboxId = $(this).data('inbox-id');
            var url = $(this).data('url');
            $.ajax({
                url: url,
                dataType: 'json',
                data: {receiverId: receiverId, inboxId: inboxId},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if(result.status == 'success') {
                        alertSuccess(result.msg);
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }else{
                        alertError(result.msg);
                    }
                }
            });
        });

        $('.rejectInvite').click(function(){
            var receiverId = $(this).data('receiver-id');
            var inboxId = $(this).data('inbox-id');
            var url = $(this).data('url');
            $.ajax({
                url: url,
                dataType: 'json',
                data: {receiverId: receiverId, inboxId: inboxId},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if(result.status == 'success') {
                        alertSuccess(result.msg);
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }else{
                        alertError(result.msg);
                    }
                }
            });
        });

        $('.acceptSendOffer').click(function(){
            var receiverId = $(this).data('receiver-id');
            var inboxId = $(this).data('inbox-id');
            var url = $(this).data('url');
            $.ajax({
                url: url,
                dataType: 'json',
                data: {receiverId: receiverId, inboxId: inboxId},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if(result.status == 'success') {
                        alertSuccess(result.msg);
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }else{
                        alertError(result.msg);
                    }
                }
            });
        });

        $('.rejectSendOffer').click(function(){
            var receiverId = $(this).data('receiver-id');
            var inboxId = $(this).data('inbox-id');
            var url = $(this).data('url');
            $.ajax({
                url: url,
                dataType: 'json',
                data: {receiverId: receiverId, inboxId: inboxId},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if(result.status == 'success') {
                        alertSuccess(result.msg);
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }else{
                        alertError(result.msg);
                    }
                }
            });
        });

        $('.acceptJob').click(function(){
            var projectId = $(this).data('project-id');
            var inboxId = $(this).data('inbox-id');
            var url = $(this).data('url');
            $.ajax({
                url: url,
                dataType: 'json',
                data: {projectId: projectId, inboxId: inboxId},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if(result.status == 'success') {
                        alertSuccess(result.msg);
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }else{
                        alertError(result.msg);
                    }
                }
            });
        });

        $('.rejectJob').click(function(){
            var projectId = $(this).data('project-id');
            var inboxId = $(this).data('inbox-id');
            var url = $(this).data('url');
            $.ajax({
                url: url,
                dataType: 'json',
                data: {projectId: projectId, inboxId: inboxId},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if(result.status == 'success') {
                        alertSuccess(result.msg);
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }else{
                        alertError(result.msg);
                    }
                }
            });
        });

        $('.btnSendMessage').click(function(){
            var receiverId = $(this).data('receiver-id');
            var inboxId = $(this).data('inbox-id');
            var url = $(this).data('url');
            var message = $('.tbxMessage').val();
            $.ajax({
                url: url,
                dataType: 'json',
                data: {receiverId: receiverId, inboxId: inboxId, message: message},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if(result.status == 'success') {
                        $('.tbxMessage').val('');
                        alertSuccess(result.msg);
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }else{
                        alertError(result.msg);
                    }
                }
            });
        });

        $('.tbxMessage').keypress(function(e) {
            if ((e.keyCode || e.which) == 13) {
                $('.btnSendMessage').click();
            }
        });
    }
);