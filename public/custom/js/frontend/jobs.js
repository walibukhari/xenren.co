jQuery(
    function ($) {

        // Create cookie for filter on off
        function createCookie(name,value,days) {
            var expires = "";
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days*24*60*60*1000));
                expires = "; expires=" + date.toUTCString();
            }
            document.cookie = name + "=" + value + expires + "; path=/";
        }

        function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for(var i=0;i < ca.length;i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') 
                    c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) == 0) 
                    return c.substring(nameEQ.length,c.length);
            }
            return null;
        }

        function eraseCookie(name) {
            createCookie(name,"",-1);
        }

        if(readCookie('filter') == null){
            createCookie('filter', 1, 365);
        }

        if(readCookie('filter') == 0){
            $('.tab-content-searchBox').hide();
            $('.filter .fa').removeClass('fa-caret-up');
            $('.filter .fa').addClass('fa-caret-down');
        }
        else if(readCookie('filter') == 1) {
            $('.tab-content-searchBox').show();
            $('.filter .fa').removeClass('fa-caret-down');
            $('.filter .fa').addClass('fa-caret-up');
        }



        $('body').on('click', 'div[id^="btn-close-"]', function () {
            var id = parseInt(this.id.replace("btn-close-", ""));

            $.ajax({
                url: '/jobs/delete',
                dataType: 'json',
                data: {'id': id},
                method: 'get',
                beforeSend: function () {
                },
                error: function () {
                    alert('Unable to hide/delete right now, please try again later');
                },
                success: function (result) {
                    if(result.status == 'OK') {
                        $('.div-job-' + id).hide();
                    }
                }
            });
        });

        //$('.bs-filter-keyword').selectpicker({
        //    style: 'btn-default',
        //    size: 4
        //});
        //
        //$('.bs-filter-value').selectpicker({
        //    style: 'btn-default',
        //    size: 4
        //});

        $('button[id^="btn-join-discussion-"]').click(function(){
            var redirect = $(this).data('redirect');
            window.location.href = redirect;
        });

        //$('button[id^="btn-make-offer-"]').click(function(){
        //    var projectId = parseInt(this.id.replace("btn-make-offer-", ""));
        //    $('.modal-project-id').val(projectId);
        //
        //    var questions = JSON.parse($('.question-' + projectId).val());
        //
        //    $('.question-section').html('');
        //    for(var i in questions) {
        //        var result = '<div class="form-group">' +
        //                        '<label>Question ' + ( parseInt(i) + 1 )+ ' : ' + questions[i].question + ' </label>' +
        //                        '<input class="form-control" name="answer[]" value="" type="text">' +
        //                        '<input type="hidden" class="form-control" name="questionId[]" value="' + questions[i].id + '" type="text">' +
        //                    '</div>';
        //        $('.question-section').append(result);
        //    }
        //
        //    $('#make-offer-form').resetForm();
        //
        //    $('#tbxQQId').val($('.qq-id').val());
        //    $('#tbxWechatId').val($('.wechat-id').val());
        //    $('#tbxSkypeId').val($('.skype-id').val());
        //    $('#tbxHandphoneNo').val($('.handphone-no').val());
        //
        //    $('#model-alert-container').html('');
        //});
        //
        //$('#btn-submit').click(function(){
        //    $('#make-offer-form').submit();
        //});
        //
        //$('#make-offer-form').ajaxForm({
        //    beforeSubmit: function() {
        //        App.blockUI({
        //            target: '#make-offer-form',
        //            overlayColor: 'none',
        //            centerY: true,
        //            boxed: true
        //        });
        //    },
        //    success: function(resp) {
        //        if (resp.status == 'success') {
        //            App.alert({
        //                type: 'success',
        //                icon: 'check',
        //                message: resp.msg,
        //                place: 'append',
        //                closeInSeconds: 3,
        //                container: '#model-alert-container',
        //                reset: true,
        //                focus: true
        //            });
        //
        //            App.unblockUI('#make-offer-form');
        //            $('#modalMakeOffer').scrollTop(0);
        //
        //            setTimeout(function() {
        //                $('.modal').modal('hide');
        //                $('#make-offer-form').resetForm();
        //            }, 3 * 1000);
        //        }else{
        //            App.alert({
        //                type: 'danger',
        //                icon: 'warning',
        //                message: resp.msg,
        //                container: '#model-alert-container',
        //                reset: true,
        //                focus: true
        //            });
        //
        //            App.unblockUI('#make-offer-form');
        //            $('#modalMakeOffer').scrollTop(0);
        //        }
        //    },
        //    error: function(response, statusText, xhr, formElm){
        //        if (response.status == 422) {
        //            $.each(response.responseJSON, function(i) {
        //                $.each(response.responseJSON[i], function(key, value) {
        //                    App.alert({
        //                        type: 'danger',
        //                        icon: 'warning',
        //                        message: value,
        //                        container: '#model-alert-container',
        //                        reset: true,
        //                        focus: true
        //                    });
        //                });
        //            });
        //        }
        //
        //        App.unblockUI('#make-offer-form');
        //        $('#modalMakeOffer').scrollTop(0);
        //    }
        //});

        $('.btn-login').click(function(){
            var url = $(this).data('redirect');
            window.location.href = url;
        });

        //$('#ckbPayType').bootstrapSwitch();
        //
        //$('#ckbPayType').on('switchChange.bootstrapSwitch', function (event, state) {
        //    //true = hourly pay, false = fixed price
        //    if( state == true )
        //    {
        //        var hourly_pay = $("#hourly_pay").val();
        //        $('#price').val(hourly_pay);
        //        $('#pay_type').val("1");
        //    }
        //    else
        //    {
        //        $('#price').val("0.00");
        //        $('#pay_type').val("2");
        //    }
        //
        //});

        //$('.btn-add-fav-job').click(function(){
        //    var url = $(this).data('url');
        //    var projectId = $(this).data('project-id');
        //    var targetEle = $(this);
        //
        //    $.ajax({
        //        url: url,
        //        dataType: 'json',
        //        data: {projectId: projectId},
        //        method: 'POST',
        //        error: function () {
        //            alert(lang.unable_to_request);
        //        },
        //        success: function (result) {
        //            if (result.status == 'OK') {
        //                targetEle.addClass('active');
        //                alertSuccess(result.message);
        //            } else if (result.status == 'Error') {
        //                alertError(result.message);
        //            }
        //        }
        //    });
        //});

        

        $('.filter').click(function(){
            if ($('.tab-content-searchBox').css('display') == 'none')
            {
                $('.tab-content-searchBox').slideDown(1000);
                $('.filter .fa').removeClass('fa-caret-down');
                $('.filter .fa').addClass('fa-caret-up');
                createCookie('filter', 1, 365);
            }
            else
            {
                $('.tab-content-searchBox').slideUp(1000);
                $('.filter .fa').removeClass('fa-caret-up');
                $('.filter .fa').addClass('fa-caret-down');
                createCookie('filter', 0, 365);
            }
        });

        $('#slcKeyword').change(function(){
            if( $(this).val() == "1" )
            {
                //country
                console.log('country select');

                $.ajax({
                    url: url.getCountries,
                    dataType: 'json',
                    data: {},
                    method: 'POST',
                    error: function () {
                        alert(lang.unable_to_request);
                    },
                    success: function (result) {
                        if (result.status == 'OK') {
                            //clear
                            $('#slcValue')
                                .find('option')
                                .remove()
                                .end();

                            //add items
                            $.each( result.data, function( key, value ) {
                                $('#slcValue')
                                    .append($('<option>', { value : key })
                                        .text(value));
                            });
                        }
                    }
                });
            }
            else if( $(this).val() == "2" )
            {
                //language
                console.log('language select');

                $.ajax({
                    url: url.getLanguages,
                    dataType: 'json',
                    data: {},
                    method: 'POST',
                    error: function () {
                        alert(lang.unable_to_request);
                    },
                    success: function (result) {
                        if (result.status == 'OK') {
                            //clear
                            $('#slcValue')
                                .find('option')
                                .remove()
                                .end();

                            //add items
                            $.each( result.data, function( key, value ) {
                                $('#slcValue')
                                    .append($('<option>', { value : key })
                                        .text(value));
                            });
                        }
                    }
                });
            }
            else if( $(this).val() == "3" )
            {
                //hourly range
                console.log('hourly range select');

                $.ajax({
                    url: url.getHourlyRanges,
                    dataType: 'json',
                    data: {},
                    method: 'POST',
                    error: function () {
                        alert(lang.unable_to_request);
                    },
                    success: function (result) {
                        if (result.status == 'OK') {
                            //clear
                            $('#slcValue')
                                .find('option')
                                .remove()
                                .end();

                            //add items
                            $.each( result.data, function( key, value ) {
                                $('#slcValue')
                                    .append($('<option>', { value : key })
                                        .text(value));
                            });
                        }
                    }
                });
            }
        });

        $('.add-filter-con').click(function(){

            if( $('#slcKeyword').val() == 1 )
            {
                var countryId = $('#slcValue').val();
                var countryName = $("#slcValue option[value='" + countryId + "']").text();
                $('input[name="hidCountry"]').val(countryId);

                var result = "<b>" + lang.country + "</b>：" + countryName;
                $('.div-country .value').html(result);
                $('#btn-close-country').show();
            }
            else if ( $('#slcKeyword').val() == 2 )
            {
                var languageId = $('#slcValue').val();
                var languageName = $("#slcValue option[value='" + languageId + "']").text();
                $('input[name="hidLanguage"]').val(languageId);

                var result =  "<b>" + lang.language + "</b>：" + languageName;
                $('.div-language .value').html(result);
                $('#btn-close-language').show();
            }
            else if( $('#slcKeyword').val() == 3 )
            {
                var hourlyRangeId = $('#slcValue').val();
                var hourlyRangeName = $("#slcValue option[value='" + hourlyRangeId + "']").text();
                $('input[name="hidHourlyRange"]').val(hourlyRangeId);

                var result =  "<b>" + lang.hourly_range + "</b>：" + hourlyRangeName;
                $('.div-hourly-range .value').html(result);
                $('#btn-close-hourly-range').show();
            }

            setDoneButtonUrl();
        });

        $('#btn-close-country').click(function(){
            //remove the display
            $('.div-country .value').html("");
            $('#btn-close-country').hide();
            $('input[name="hidCountry"]').val("");

            //rebuild the url
            setDoneButtonUrl();
        });

        $('#btn-close-language').click(function(){
            //remove the display
            $('.div-language .value').html("");
            $('#btn-close-language').hide();
            $('input[name="hidLanguage"]').val("");

            //rebuild the url
            setDoneButtonUrl();
        });

        $('#btn-close-hourly-range').click(function(){
            //remove the display
            $('.div-hourly-range .value').html("");
            $('#btn-close-hourly-range').hide();
            $('input[name="hidHourlyRange"]').val("");

            //rebuild the url
            setDoneButtonUrl();
        });

        //$("#slcCountry").change(function() {
        //    $('input[name="hidCountry"]').val($("#slcCountry").val());
        //    setDoneButtonUrl();
        //});
        //
        //$("#slcLanguage").change(function() {
        //    $('input[name="hidLanguage"]').val($("#slcLanguage").val());
        //    setDoneButtonUrl();
        //});
        //
        //$("#slcBudget").change(function() {
        //    $('input[name="hidHourlyRange"]').val($("#slcBudget").val());
        //    setDoneButtonUrl();
        //});

        function setDoneButtonUrl()
        {
            var baseUrl = $('#btn-done').data('base-url');
            var hidCountry = parseInt($('input[name="hidCountry"]').val());
            var hidLanguage = parseInt($('input[name="hidLanguage"]').val());
            var hidHourlyRange = parseInt($('input[name="hidHourlyRange"]').val());

            var advanceFilterParam = "advance_filter=";
            if( hidCountry > 0 )
            {
                advanceFilterParam += ",1|" + hidCountry;
            }

            if( hidLanguage > 0 )
            {
                advanceFilterParam += ",2|" + hidLanguage;
            }

            if( hidHourlyRange > 0 )
            {
                advanceFilterParam += ",3|" + hidHourlyRange;
            }

            //http://lgdlair.com/jobs?project_type=2&type=1&order=4&advance_filter=1|49,2|183
            var url = baseUrl + advanceFilterParam;

            var skills = $('#skill_id_list').val();
            if( skills != "" )
            {
                url = url + "&skill_id_list=" + skills;
            }

            $('#btn-done').attr('data-redirect-url', url);
        }

        var skill_id_list = $('#skill_id_list').val();
        var selectedSkillList = [];
        if(skill_id_list != ""){
            selectedSkillList = skill_id_list.split(",")
        }

        var ms = $('#msSkills').magicSuggest({
            allowFreeEntries: false,
            allowDuplicates: false,
            hideTrigger: true,
            placeholder: g_lang.search,
            data: skillList,
            method: 'get',
            highlight: true,
            value: selectedSkillList,
            renderer: function(data){
                if( data.description == null )
                {
                    return '<b>' + data.name + '</b>' ;
                }
                else
                {
                    var name = data.name.split("|");

                    return '<b>' + name[0] + '</b> | <span class="search-desc">' + data.description + '</span>';
                }

            },
            selectionRenderer: function(data){
                result = data.name;
                name = result.split("|")[0];
                return name;
            }
        });

        $(ms).on('selectionchange', function(e, cb, s){
                $('#skill_id_list').val(cb.getValue());
                setDoneButtonUrl();
        });

        // $('.btn-search').click(function(){
        $('body').on('click', 'svg[id^="btn-done"]', function(){
            var redirectUrl = $(this).data('redirect-url');
            window.location.href = redirectUrl;
        });

        //language
        $('.language .item').click(function(){
            var id = $(this).data('id');
            var name = $(this).data('name');

            $('#language_name').html(name);
            $('#slcLanguage').val(id);
            $('input[name="hidLanguage"]').val(id);
            setDoneButtonUrl();
        });

        //country
        $('.country .item').click(function(){
            var id = $(this).data('id');
            var name = $(this).data('name');

            $('#country_name').html(name);
            $('#slcCountry').val(id);
            $('input[name="hidCountry"]').val(id);
            setDoneButtonUrl();
        });

        //hourly range
        $('.hourly-range .item').click(function(){
            var id = $(this).data('id');
            var name = $(this).data('name');

            $('#hourly_range_name').html(name);
            $('#slcBudget').val(id);
            $('input[name="hidHourlyRange"]').val(id);
            setDoneButtonUrl();
        });

        //get the description width and the image not allow bigger than div description width
        var divDescriptionWidth = $('.description').width();
        console.log( " DivDescriptionWidth : " + divDescriptionWidth );
        $('.description img').each(function( index, element ) {
            console.log( "Image Width : " + element.width );
            if( element.width > divDescriptionWidth )
            {
                $(element).attr('style', 'width:100%; height:100%;')
            }
        });

        $('.scroll-div').slimScroll({});
    }
);
