jQuery(
    function ($) {

        var addFavour = url.addFavouriteJob;
        var removeFavour = url.removeFavouriteJob;

        $('.btn-add-fav-job').click(function(){
            var url = $(this).data('url');
            var projectId = $(this).data('project-id');
            var targetEle = $(this);

            $.ajax({
                url: url,
                dataType: 'json',
                data: {projectId: projectId},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if (result.status == 'OK') {

                        if(targetEle.hasClass('active')){
                            targetEle.removeClass('active');
                            targetEle.data('url', addFavour);
                            
                        }
                        else {
                            targetEle.addClass('active');
                            targetEle.data('url', removeFavour);
                        }

                        alertSuccess(result.message);
                    } else if (result.status == 'Error') {
                        alertError(result.message);
                    }
                }
            });
        });


        // $('#modalThumbdownJob').on('show.bs.modal', function(e){
            $('.btn-thumb-down').click(function(){
                var url = $(this).data('url');
                // alert($(this).attr('class'));
                var projectType = $(this).data('project-type');
                var projectId = $(this).data('project-id');
                if( projectType == 1)
                    var targetEle = $(this).parent().parent().parent().parent().parent().parent();
                else
                    var targetEle = $(this).parent().parent().parent().parent().parent();
                
                $.ajax({
                    url: url,
                    dataType: 'json',
                    data: {projectId: projectId, projectType: projectType},
                    method: 'POST',
                    error: function () {
                        alert(lang.unable_to_request);
                        $('.btn-thumb-down').off('click');
                    },
                    success: function (result) {
                        if (result.status == 'OK') {
                            targetEle.addClass('inactive');
                            alertSuccess(result.message);
                            $('.btn-thumb-down').off('click');
                        } else if (result.status == 'Error') {
                            alertError(result.message);
                            $('.btn-thumb-down').off('click');
                        }
                    }
                });
            });

        // });
    });

        $('.check-job-div-main').click(function(e){
            console.log($(this));
            console.log('===');
            console.log(e.target.className);
            console.log(typeof e.target.className);
            console.log('===');
            var id = ($(this).data('project-id'));
            var classN =  $(this).attr('class');
            // alert(classN);
            var className = (e.target.className);
            // alert(className);
            if (
                className == "svg-inline--fa fa-heart fa-w-18 pull-right" ||
                className == "svg-inline--fa fa-thumbs-down fa-w-16 pull-left" ||
                className == "btn btn-green-bg-white-text" ||
                className == "set-image--image" ||
                className == "per-text openDialog" ||
                className == "per-text  openDialog award-yellow-per" ||
                className == "per-text  openDialog award-Red-per" ||
                className == "per-text  openDialog award-Red-per" ||
                className == "set-per-text openDialog" ||
                className == "set-per-text openDialog award-yellow-per" ||
                className == "set-per-text openDialog award-Red-per" ||
                className == "setper-text openDialog" ||
                className == "btn-add-fav-job" ||
                className == "fa fa-heart pull-right" ||
                className == "btn-thumb-down btn-add-down-job-"+id ||
                className == "fa fa-thumbs-down pull-left" ||
                className == "btn-thumb-down btn-add-down-job-"+id+"inactive" ||
                className == "btn-add-fav-job active" ||
                typeof e.target.className == "object"

            ) {
                return;
            }
            var url = $(this).data('reurl');
            if(url) {
                window.location.href = url;
            }
    }
);