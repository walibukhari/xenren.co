$(function(){
    $('#cancelMilestone').on('click', function(e){
        e.preventDefault();
        $('#milestones_amount').val('')
        $('#milestones_name').val('')
        $('#milestones_description').val('')
        $('#milestones_duedate').val('')
    });

    $('#confirmMilestone').on('click', function(e){
        e.preventDefault();
        var form = new FormData();
        var amount = $('#milestones_amount').val();
        var name = $('#milestones_name').val();
        var description = $('#milestones_description').val();
        var datetime = $('#milestones_duedate').val();
        var applicant_id = $('#applicant_id').val();

        form.append("amount", amount);
        form.append("name", name);
        form.append("description", description);
        form.append("d_date", datetime);
        form.append("applicant_id", applicant_id);

        var settings = {
            "url": "/createMilestone/"+window.ProjectId,
            "method": "POST",
            "data": form,
            "headers": {
                "Cache-Control": "no-cache"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data"
        };


        $.ajax(settings).done(function(response){
            var response = JSON.parse(response)
            if(response.status == 'failure') {
                alertError(response.message);
            } else {
                alertSuccess(response.message);
                setTimeout(function(){
                    window.location.reload();
                }, 300)
            }
        })
    });

    $('#confirmMileStone').on('click', function(e){
        
        $('.confirmationMod').modal('hide');
        $('.password_show').modal('show');
    });
    $('#confirmMileStonepass').on('click', function(e){
        var password = $('#password').val();
        
        e.preventDefault();
        var form = new FormData();
        // var comment = $('#reason').val();

        form.append("comment", "");
        form.append("password", password);
        form.append("milestone_id", window.milestoneId);

        var settings = {
            "url": "/acceptMilestone/"+window.ProjectId,
            "method": "POST",
            "data": form,
            "headers": {
                "Cache-Control": "no-cache"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data"
        };

        if(password){
        $.ajax(settings).done(function(response){
           
            var response = JSON.parse(response)
            if(response.status == 'failure' || response.status == 'error') {
                alertError(response.message);
            } else {
                alertSuccess(response.message);
                setTimeout(function(){
                    window.location.reload();
                }, 300)
            }
        });
    }
    });

    $('#rejectMileStoneBtn').on('click', function(e){
        e.preventDefault();
        $('#rejectMileStoneModal').modal('show');
        // $('.reasonTextArea').toggle(1000);
        // $('#submitRejectMileStone').toggle();
        // $('#confirmMileStonebtn').toggle();
        // $('#confirmMileStone').toggle();
    });

    $('.submitRejectMileStone').on('click', function(e){
        e.preventDefault();
        var form = new FormData();
        var comment = $('#cancelCurrentMilestone').val();
        console.log(comment);
        form.append("comment", comment);

        var settings = {
            "url": "/rejectMilestone/"+window.ProjectId,
            "method": "POST",
            "data": form,
            "headers": {
                "Cache-Control": "no-cache"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data"
        }


        $.ajax(settings).done(function(response){
            var response = JSON.parse(response)
            if(response.status == 'failure') {
                alertError(response.message);
            } else {
                alertSuccess(response.message);
                setTimeout(function(){
                    window.location.reload();
                }, 300)
            }
        });

    })

    $('.changeStatus').on('change', function(e){
        e.preventDefault();
        var form = new FormData();
        var comment = $('#reason').val();
        var status = $('#changeStatus').val();
        var milestone = $(this).data('milestone');

        form.append("comment", comment);
        form.append("status", status);
        form.append("milestone", milestone);

        var settings = {
            "url": "/changeStatus/"+window.ProjectId,
            "method": "POST",
            "data": form,
            "headers": {
                "Cache-Control": "no-cache"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data"
        }


        $.ajax(settings).done(function(response){
            var response = JSON.parse(response)
            if(response.status == 'failure') {
                alertError(response.message);
            } else {
                alertSuccess(response.message);
                setTimeout(function(){
                    window.location.reload();
                }, 300)
            }
        });

    })


    $('input[type=radio][name=autoMilestone]').change(function() {
        $('#milestoneAmount').html(($(this).data('amount')))
    });

    $('#confirmManualMilestone').on('click', function(){
        var amount = parseFloat($('#manualAmount').val());
        if(amount > window.total) {
            alertError('you cant dispute more then $'+window.total);
        }

        var form = new FormData();
        form.append("amount", amount);
        form.append("type", 1);
        var settings = {
            "url": "/createDispute/"+window.ProjectId,
            "method": "POST",
            "data": form,
            "headers": {
                "Cache-Control": "no-cache"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data"
        }
        $.ajax(settings).done(function(response){
            var response = JSON.parse(response)
            if(response.status == 'failure') {
                alertError(response.message);
            } else {
                alertSuccess(response.message);
                setTimeout(function(){
                    window.location.reload();
                }, 300)
            }
        })
    });

    $('#confirmAutoMilestone').on('click', function(){
        var milestone = $('input:radio[name="autoMilestone"]:checked').val();
        if(!milestone) {
            alertError('Please select milestone to dispute');
        }

        var form = new FormData();
        form.append("milestone_id", milestone);
        form.append("type", 2);
        var settings = {
            "url": "/createDispute/"+window.ProjectId,
            "method": "POST",
            "data": form,
            "headers": {
                "Cache-Control": "no-cache"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data"
        }
        $.ajax(settings).done(function(response){
            var response = JSON.parse(response)
            if(response.status == 'failure') {
                alertError(response.message);
            } else {
                alertSuccess(response.message);
                setTimeout(function(){
                    window.location.reload();
                }, 300)
            }
        })
    });


    $('.disputeMilestoneBtn').on('click', function() {
        var milestone = $(this).data('milestone');
        window.milestone = $(this).data('milestone');
        $('#refundRequestModal').modal('show');
        // $('.disputeModal').modal('show');
        // $('#dispute-explain-'+milestone).slideDown();
        // $('#dispute-milestone-btn-'+milestone).slideUp();
    });

    $('.rejectDisputeMilestoneBtn').on('click', function() {
        var milestone = $(this).data('milestone');

        $('#dispute-explain-'+milestone).slideUp();
        $('#dispute-milestone-btn-'+milestone).slideDown();
    });

    $('.confirmDisputeMilestoneBtn').on('click', function(){
        var milestone = window.milestone;
        if(!milestone) {
            alertError('Please select milestone to dispute');
            return;
        }

        var reason = $('.refundcomment').val();
        if(!reason) {
            alertError('Please enter reason for dispute!');
            return;
        }

        var fileInput = document.getElementById('screenshot');
        var file = fileInput.files[0];

        var form = new FormData();
        form.append("milestone_id", milestone);
        form.append("reason", reason);
        form.append("type", 2);
        form.append("screenshot", file);
        var settings = {
            "url": "/createDispute/"+window.ProjectId,
            "method": "POST",
            "data": form,
            "headers": {
                "Cache-Control": "no-cache"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data"
        }
        $.ajax(settings).done(function(response){
            var response = JSON.parse(response)
            if(response.status == 'failure') {
                alertError(response.message);
            } else {
                alertSuccess(response.message);
                setTimeout(function(){
                    window.location.reload();
                }, 300)
            }
        });
    });


    $('.refundMilestoneBtn').on('click', function() {
        var milestone = $(this).data('milestone');
        $('#refundModal').modal('show');
        window.milestone = $(this).data('milestone')
        // $('#refund-explain-'+milestone).slideDown();
        // $('#refund-milestone-btn-'+milestone).slideUp();
    });

    $('.rejectRefundMilestoneBtn').on('click', function() {
        var milestone = $(this).data('milestone');

        $('#refund-explain-'+milestone).slideUp();
        $('#refund-milestone-btn-'+milestone).slideDown();
    });

    $('.confirmRefundMilestoneBtn').on('click', function(){
        var milestone = window.milestone ;
        if(!milestone) {
            alertError('Please select milestone to dispute');
            return;
        }

        var reason = $('.refundcomment').val();
        if(!reason) {
            alertError('Please enter reason for refund!');
            return;
        }

        var form = new FormData();
        form.append("milestone_id", window.milestone);
        form.append("reason", reason);
        form.append("type", 2);
        var settings = {
            "url": "/refundDispute/"+window.ProjectId,
            "method": "POST",
            "data": form,
            "headers": {
                "Cache-Control": "no-cache"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data"
        };
        $.ajax(settings).done(function(response){
            var response = JSON.parse(response)
            if(response.status == 'failure') {
                alertError(response.message);
            } else {
                alertSuccess(response.message);
                setTimeout(function(){
                    window.location.reload();
                }, 300)
            }
        });
    });


    $('.refundDisputeMilestoneBtn').on('click', function(){
        window.milestone = $(this).data('milestone');
        console.log(window.milestone);
        $('#refundRequestFromEmployerModal').modal('show');
    });

    $('.rejectRefundRequestBtn').on('click', function(){
        window.milestone = $(this).data('milestone');
        console.log(window.milestone);
        $('#rejectRefundFreelancer').modal('show');
    });

    $('.confirmRejectRefundFreelancer').on('click', function(){
        var reason = $('#rejectRefundFreelancerComment').val();

          var form = new FormData();
          form.append("milestone_id", window.milestone);
          form.append("message", reason);

          var settings = {
            "url": "/rejectRefundRequest/"+window.milestone,
            "method": "POST",
            "data": form,
            "headers": {
              "Cache-Control": "no-cache"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data"
          };
          $.ajax(settings).done(function(response){
            var response = JSON.parse(response)
            if(response.status == 'failure') {
              alertError(response.message);
            } else {
              alertSuccess(response.message);
              setTimeout(function(){
                window.location.reload();
              }, 300)
            }
          });

    });

    $('.fileDisputeEmployer').on('click', function () {
	    window.transactionid = $(this).data('transactionid');
	    window.projectid = $(this).data('project');
	    window.milestone = $(this).data('milestoneid');
	    console.log(window.transactionid);
	    console.log(window.projectid);
	    console.log(window.milestone);
	    $('.disputeModalEmployer').modal('show');
    })

    $('.confirmRequestFromEmployerMilestoneBtn').on('click', function(){
        var milestone = window.milestone;
        if(!milestone) {
            alertError('Please select milestone to dispute');
            return;
        }

        var form = new FormData();
        form.append("milestone_id", milestone);
        form.append("type", 2);
        var settings = {
            "url": "/refundDispute/"+window.ProjectId,
            "method": "POST",
            "data": form,
            "headers": {
                "Cache-Control": "no-cache"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data"
        };
        $.ajax(settings).done(function(response){
            var response = JSON.parse(response)
            if(response.status == 'failure') {
                alertError(response.message);
            } else {
                alertSuccess(response.message);
                setTimeout(function(){
                    window.location.reload();
                }, 300)
            }
        });
    });


    $('#endContract').on('click', function(){
        var reason = $('#reason_to_end').val();
        var freelancer = $('#freelancer').val();
        var form = new FormData();
        form.append("reason_to_end", reason);
        form.append("applicant_id", $(this).data('user'));
        var settings = {
            "url": "/endContract/postMessage/"+window.ProjectId,
            "method": "POST",
            "data": form,
            "headers": {
                "Cache-Control": "no-cache"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data"
        };
        $.ajax(settings).done(function(response){
            var response = JSON.parse(response);
            if(response.status == 'failure') {
                alertError(response.message);
            } 
            else {
                if (response.status=='success' && freelancer === undefined) {
                alertSuccess(response.message);
                    setTimeout(function () {
                        window.location.href = '/endContract/employer/' + window.applicantId+'-'+window.ProjectId;
                    }, 300)
                }
            }
        });
    });

    $('.daily_update').on('change', function(){
        var daily_update = 0;
        if($(this).is(":checked")) {
            daily_update = 1;
        }
        var form = new FormData();
        // alert($(this).data('user'));
        form.append("daily_update", daily_update);
        form.append("applicant_id", $(this).data('user'));
        var settings = {
            "url": "/dailyUpdate/"+window.ProjectId,
            "method": "POST",
            "data": form,
            "headers": {
                "Cache-Control": "no-cache"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data"
        }
        $.ajax(settings).done(function(response){
            var response = JSON.parse(response)
            if(response.status == 'failure') {
                alertError(response.message);
            } else {
                alertSuccess(response.message);
                setTimeout(function(){
                    // window.location.reload();
                }, 3000)
            }
        });
    });

    $('.requestDisputeAgain').on('click', function(){
        var form = new FormData();
        form.append("dispute_id", $(this).data('disputeid'));
        form.append("status", 1);
        var settings = {
            "url": "/updateDisputeStatus",
            "method": "POST",
            "data": form,
            "headers": {
                "Cache-Control": "no-cache"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data"
        }
        $.ajax(settings).done(function(response){
            var response = JSON.parse(response)
            if(response.status == 'failure') {
                alertError(response.message);
            } else {
                alertSuccess(response.message);
                setTimeout(function(){
                    window.location.reload();
                }, 3000)
            }
        });
    })

    $('.switchFixed').on('click', function(){
        var form = new FormData();
        form.append('status', '2');
        var settings = {
            "url": "/switchFixed/"+window.ProjectId,
            "method": "POST",
            "data": form,
            "headers": {
                "Cache-Control": "no-cache"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data"
        }
        $.ajax(settings).done(function(response){
            var response = JSON.parse(response)
            if(response.status == 'failure') {
                alertError(response.message);
            } else {
                alertSuccess(response.message);
                setTimeout(function(){
                    window.location.reload();
                }, 3000)
            }
        });
    })

    $('.switchHourly').on('click', function(){
        var form = new FormData();
        form.append('status', '1');
        var settings = {
            "url": "/switchFixed/"+window.ProjectId,
            "method": "POST",
            "data": form,
            "headers": {
                "Cache-Control": "no-cache"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data"
        }
        $.ajax(settings).done(function(response){
            var response = JSON.parse(response)
            if(response.status == 'failure') {
                alertError(response.message);
            } else {
                alertSuccess(response.message);
                setTimeout(function(){
                    window.location.reload();
                }, 3000)
            }
        });
    })

    $('#confirmMilestoneFreelancer').on('click', function(e){
        
        e.preventDefault();
        var form = new FormData();
        var amount = $('#milestones_amount').val();
        var name = $('#milestones_name').val();
        var description = $('#milestones_description').val();
        // var datetime = $('#milestones_duedate').val();

        form.append("_token", $('#_token').val());
        form.append("amount", amount);
        form.append("milestone_id", name);
        form.append("description", description);
        // form.append("d_date", datetime);

        var settings = {
            "url": "/confirmMilestone/"+window.ProjectId,
            "method": "POST",
            "data": form,
            "headers": {
                "Cache-Control": "no-cache"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data"
        }


        $.ajax(settings).done(function(response){
            var response = JSON.parse(response)
            if(response.status == 'failure') {
                alertError(response.message);
                setTimeout(function(){
                    // window.location.reload();
                }, 300);
            } else {
                alertSuccess(response.message);
                setTimeout(function(){
                    // window.location.reload();
                }, 300);
            }
        });
    });


    $('#milestones_amount').val($('select#milestones_name').find(':selected').data('amount'));
    $('select#milestones_name').on('change', function(){
        $('#milestones_amount').val($(this).find(':selected').data('amount'));
    });

    $('.confirmMilestoneEmployer').on('click', function(e){
        window.name = $(this).data('name');
        window.amount = $(this).data('amount');
        window.milestoneId = $(this).data('milestoneid');
        window.dueDate = $(this).data('duedate');
        window.type = $(this).data('type');
        window.status = $(this).data('status');
        window.transactionId = $(this).data('transactionid');

        console.log(window.name);
        console.log(window.amount);
        console.log(window.milestoneId);
        console.log(window.dueDate);
        console.log(window.type);
        console.log(window.status);
        console.log(window.transactionId);

        $('#acceptRequestingMilestone').modal('show');
    });
    $('.rejectMilestoneEmployerBtn').on('click', function(){
        window.name = $(this).data('name');
        window.amount = $(this).data('amount');
        window.milestoneid = $(this).data('milestoneid');
        window.duedate = $(this).data('duedate');
        window.type = $(this).data('type');
        window.status = $(this).data('status');
        window.transactionid = $(this).data('transactionid');
        window.dueDate = "";
        // console.log('window.transactionid' + window.transactionid);
	    console.log($(this).data());
	    $('#myModal').modal('show');
    });

    $('.confirmMilestoneEmployerSubmit').on('click', function(e){
        e.preventDefault();
        var form = new FormData();
        var name = window.name;
        var amount = window.amount;
        var milestoneId = window.milestoneId;
        var dueDate = window.dueDate;
        var type = window.type;
        var status = window.status;
        var transactionId = window.transactionId;

        form.append("_token", $("input[name=_token]").val());
        form.append("amount", amount);
        form.append("name", name);
        form.append("milestone_id", milestoneId);
        form.append("due_date", dueDate);
        form.append("type", type);
        form.append("status",window.status);

        var settings = {
            "url": "/updateTransaction/"+transactionId,
            "method": "POST",
            "data": form,
            "headers": {
                "Cache-Control": "no-cache"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data"
        }


        $.ajax(settings).done(function(response){
            var response = JSON.parse(response);
            if(response.status == 'failure') {
                alertError(response.message);
                setTimeout(function(){
                    // window.location.reload();
                }, 300);
            } else {
                alertSuccess(response.message);
                setTimeout(function(){
                    window.location.reload();
                }, 300);
            }
        });
    });

    $('.rejectMilestoneEmployer').on('click', function(e){
        e.preventDefault();
        var form = new FormData();
        var name = window.name;
        var amount = window.amount;
        var milestoneId = window.milestoneid;
        var dueDate = window.dueDate;
        var type = window.type;
        var status = window.status;
        var transactionId = window.transactionid;
        var comment = $('.comment').val();
        console.log('transactionId: '+transactionId);
        console.log('transactionId: '+window.transactionid);
        form.append("_token", $("input[name=_token]").val());
        form.append("amount", amount);
        form.append("name", name);
        form.append("milestone_id", milestoneId);
        form.append("due_date", dueDate);
        form.append("type", type);
        form.append("status", status);
        form.append("comment", comment);

          var settings = {
            "url": "/updateTransaction/"+transactionId,
            "method": "POST",
            "data": form,
            "headers": {
              "Cache-Control": "no-cache"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data"
          }


          $.ajax(settings).done(function(response){
            var response = JSON.parse(response)
            if(response.status == 'failure') {
              alertError(response.message);
            } else {
              alertSuccess(response.message);
              setTimeout(function(){
                window.location.reload();
              }, 3000);
            }
          });
    });

    $('.updateMilestoneDispute').click(function (e){
        e.preventDefault();
        var form = new FormData();
        var id = $(this).data('id');
        var status = $(this).data('status');

        form.append("dispute_id", id);
        form.append("status", status);

        var settings = {
            "url": "/updateDisputeStatus",
            "method": "POST",
            "data": form,
            "headers": {
                "Cache-Control": "no-cache"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data"
        };


        $.ajax(settings).done(function(response){
            var response = JSON.parse(response);
            if(response.status == 'failure') {
                alertError(response.message);
            } else {
                alertSuccess(response.message);
                setTimeout(function(){
                    window.location.reload();
                }, 300);
            }
        });
    });

    $('.fileDispute').on('click', function () {
        $('#comment').val('');
        window.transactionid = $(this).data('transactionid');
        window.milestone = $(this).data('milestone');
        window.project = $(this).data('project');
        console.log(window.transactionid);
        console.log(window.milestone);
        console.log(window.project);
        $('.disputeModal').modal('show');
    });

    $('.fileDisputeEmployerToAdmin').on('click', function () {
	    var form = new FormData();
	    var fileInput = document.getElementById('screenshotEmployer');

	    var file = fileInput.files;
	    form.append("transaction_id", window.transactionid);
	    form.append("project_id", window.projectid);
	    form.append("milestone_id", window.milestone);
	    form.append("attachments_length", file.length);
	    form.append("comment", $('.disputeComment').val());
	    for (var i=0; i<file.length; i++) {
		    console.log(file[i]);
		    form.append("attachment_"+i, file[i]);
	    }
	    var settings = {
		    "url": "/fileDisputeToAdminEmployer",
		    "method": "POST",
		    "data": form,
		    "headers": {
			    "Cache-Control": "no-cache"
		    },
		    "processData": false,
		    "contentType": false,
		    "mimeType": "multipart/form-data"
	    };


	    $.ajax(settings).done(function(response){
		    var response = JSON.parse(response);
		    if(response.status == 'failure') {
			    alertError(response.message);
		    } else {
			    alertSuccess(response.message);
			    setTimeout(function(){
				    // window.location.reload();
			    }, 300);
		    }
	    });
    });

    $('.fileDisputeToAdmin').on('click', function () {
        var form = new FormData();
        var id = 123;
        var status = 213;

        var fileInput = document.getElementById('screenshot');

        var file = fileInput.files;
        form.append("transaction_id", window.transactionid);
        form.append("milestone_id", window.milestone);
        form.append("project_id", window.project);
        form.append("attachments_length", file.length);
        form.append("comment", $('#comment').val());
        for (var i=0; i<file.length; i++) {
            console.log(file[i]);
            form.append("attachment_"+i, file[i]);
        }
        var settings = {
            "url": "/fileDisputeToAdmin",
            "method": "POST",
            "data": form,
            "headers": {
                "Cache-Control": "no-cache"
            },
            "processData": false,
            "contentType": false,
            "mimeType": "multipart/form-data"
        };


        $.ajax(settings).done(function(response){
            var response = JSON.parse(response);
            if(response.status == 'failure') {
                alertError(response.message);
            } else {
                alertSuccess(response.message);
                setTimeout(function(){
                    window.location.reload();
                }, 300);
            }
        });
    });

    $('.cancelDispute').on('click', function(){
	    $('#comment').val('');
	    window.transactionid = $(this).data('transactionid');
	    window.milestone = $(this).data('milestone');
	    window.project = $(this).data('project');
	    console.log(window.transactionid);
	    console.log(window.milestone);
	    console.log(window.project);
	    $('#cancelDisputeModel').modal('show');
    })

  $('.confirmCancelDisputeFreelancer').on('click', function(){
	  console.log(window.transactionid);
	  console.log(window.milestone);
	  console.log(window.project);
	  var form = new FormData();
	  form.append("transaction_id", window.transactionid);
	  form.append("milestone_id", window.milestone);
	  form.append("project_id", window.project);
	  var settings = {
		  "url": "/cancelDisputeFreelancer",
		  "method": "POST",
		  "data": form,
		  "headers": {
			  "Cache-Control": "no-cache"
		  },
		  "processData": false,
		  "contentType": false,
		  "mimeType": "multipart/form-data"
	  };
	  $.ajax(settings).done(function(response){
		  var response = JSON.parse(response);
		  if(response.status == 'failure') {
			  alertError(response.message);
		  } else {
			  alertSuccess(response.message);
			  setTimeout(function(){
				  window.location.reload();
			  }, 300);
		  }
	  });
  })

});
