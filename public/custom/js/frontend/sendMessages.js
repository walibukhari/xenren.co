$('#btnSubmit').click(function(){
    var formMessage = $('#send-message-form').serialize();
    var user_id = $('#user_id').val();
    var url = "/sendPrivateMessage/"+user_id;
    var token = '{!! csrf_token()  !!}';

    $.ajax({
        type:'post',
        url:url,
        data:formMessage,
        headers:{
            Authorization: token
        },

        success:function(data){
            console.log(data);
            toastr.success('Message Sent');
            document.getElementById('msg').value = "";
            $('#sendPrivateMessage').modal('hide');
        }, error: function(error){
            console.log(error);
        }
    });
});