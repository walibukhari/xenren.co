jQuery(
    function ($) {
        //load messages
        setTimeout(function(){
            var url = $('.conversation').data('url');
            $.ajax({
                url: url,
                dataType: 'json',
                method: 'get',
                beforeSend: function() {
                },
                success: function(result) {
                    if(result.status == 'OK'){
                        $('#content-chat').html('<ul class="chats"></ul>');
                        $.each(result.chatLog, function(i, e) {
                            var changeDate = '';
                            if(e.changeDate == 1){
                                // changeDate = '<li><div class="strike"><span>'+e.date+'</span></div></li>';
                            }
                            var image = '';
                            if(e.isImage == 1){
                                image = '<br><a href="/' + e.file + '" data-lightbox="image-' + e.id + '"><img src="/' + e.file + '" class="img-thumbnail" width="75px" data-lightbox="image-' + e.id + '"></a>'
                            }
                            var audio = '';
                            if(e.isAudio == 1){
                                audio = '<br><audio controls><source src="/'+e.file+'" type="audio/ogg"><source src="'+e.file+'" type="audio/mp3">Your browser does not support the audio element.</audio>'
                            }
                            $('#content-chat ul').append(changeDate+'<li class="'+e.status+'">'+
                            '<img class="avatar" alt="" src="'+e.avatar+'" />'+
                            '<div class="message">'+
                            '<a href="javascript:void(0);" class="name"> '+e.email+' </a>'+'<br>'+
                            '<i class="datetime"> '+e.time+'</i>'+
                            '<p class="body"> '+ e.content +' '+image+''+audio+'</p>'+
                            '</div>'+
                            '</li>');
                        });

                        //always scroll to bottom
                        var scrollHeight =  $('#content-chat').prop('scrollHeight');
                        $("#content-chat").slimScroll({ scrollTo: scrollHeight });

                    }
                }
                //,
                //error: function(a, b) {
                //    if (a.status != 422) {
                //        alert(lang.unknown_error);
                //    }
                //}
            });
        }, 1000);

        //send messages
        $('.chat-form form').submit(function(e){
            e.preventDefault();
            var url = $(this).attr('action');
            var data = $(this).serialize();
            $.ajax({
                url: url,
                data: data,
                dataType: 'json',
                method: 'post',
                statusCode: {
                    //422: function (resp) {
                    //    var html = '';
                    //    var json = $.parseJSON(resp.responseText);
                    //
                    //    $.each(json, function(index, value) {
                    //        $.each(value, function(i, msg) {
                    //            alertError(msg, 'test');
                    //        })
                    //    });
                    //}
                },
                beforeSend: function() {
                    $('.chat-form .error-msg').html('');
                },
                success: function(result) {
                    if(result.status == "OK"){
                        $('#content-chat ul').append('<li class="in 6">'+
                        '<img class="avatar" alt="" src="'+result.avatar+'" />'+
                        '<div class="message">'+
                        '<a href="javascript:void(0);" class="name"> '+result.realName+' </a>'+'<br>'+
                        '<i class="datetime"> ' + result.time + '</i>'+
                        '<p class="body"> '+ result.message +' </p>'+
                        '</div>'+
                        '</li>');
                        $('.chat-form .message').val('');

                        //always scroll to bottom
                        var scrollHeight =  $('#content-chat').prop('scrollHeight');
                        $("#content-chat").slimScroll({ scrollTo: scrollHeight });
                    }
                },
                error: function(a, b) {
                    if (a.status != 422) {
                        alert(lang.unknown_error);
                    }
                }
            });
            return false;
        });

        //wait admin reply
        $('.btn-wait-admin-reply').click(function(e){
            e.preventDefault();
            var url = $(this).data('url');
            var feedback_id = $(this).data('id');
            $.ajax({
                url: url,
                data: { feedback_id: feedback_id },
                dataType: 'json',
                method: 'post',
                success: function(result) {
                    if(result.status == "OK"){
                        $('.wait-admin-reply-' + feedback_id).html(lang.wait_admin_reply);
                    }
                },
                error: function(a, b) {
                    if (a.status != 422) {
                        alert(lang.unknown_error);
                    }
                }
            });
        });
    }
);