jQuery(
	function ($) {
		$('.btn-check-detail').click(function(){
			var url = $(this).data('url');
			window.location.href = url;
		});
		
		$('.btn-remove-fav-job').click(function(){
			var url = $(this).data('url');
			var projectId = $(this).data('project-id');
			var targetEle = $(this);
			
			$.ajax({
				url: url,
				dataType: 'json',
				data: {projectId: projectId},
				method: 'POST',
				error: function () {
					alert(lang.unable_to_request);
				},
				success: function (result) {
					console.log("result");
					console.log(result);
					console.log("result");
					if (result.status === 'OK') {
						$('#project-id-'+ projectId).hide();
						alertSuccess(result.message);
						$('.set-fa-fa-fa-starsss').addClass('fav-add-active');
					}else if(result.status === 'ok'){
						$('#project-id-'+ projectId).hide();
						alertSuccess(result.message);
						$('.set-fa-fa-fa-starsss').removeClass('fav-add-active');

					} else if (result.status === 'Error') {
						alertError(result.message);
					}
				}
			});
		});
		
		$('.icon-link').on('click', function(){
			var success   = true,
				range     = document.createRange(),
				selection;
			var tmpElem = $('<div>');
			tmpElem.css({
				position: "absolute",
				left:     "-1000px",
				top:      "-1000px",
			});
			// Add the input value to the temp element.
			tmpElem.text(window.location.href);
			$("body").append(tmpElem);
			// Select temp element.
			range.selectNodeContents(tmpElem.get(0));
			selection = window.getSelection ();
			selection.removeAllRanges ();
			selection.addRange (range);
			// Lets copy.
			try {
				success = document.execCommand ("copy", false, null);
			}
			catch (e) {
				copyToClipboardFF(window.location.href);
			}
			if (success) {
				alertSuccess ("Link copied!");
				// remove temp element.
				tmpElem.remove();
			}
		});
	}
);