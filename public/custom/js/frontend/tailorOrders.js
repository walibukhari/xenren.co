jQuery(
    function ($) {
        $('.bs-filter-keyword').selectpicker({
            style: 'btn-default',
            size: 4
        });

        $('.bs-filter-value').selectpicker({
            style: 'btn-default',
            size: 4
        });
    }
);
