jQuery(
	function ($) {
		$('#add-project-form').makeAjaxForm({
			submitBtn: '#project-submit-btn',
			alertContainer: '#project-alert-container',
			beforeFunction: validate,
			afterSuccessFunction: function (response, $el, $this) {
				if( response.status == 'success')
				{
					console.log(response.data.country_id);
					var url = '/'+window.session.value+'/Coworkers?skill_id_list=' + response.data.skill_id_list + '&success_post=true&country_id='+response.data.country_id;
					window.location.href = url;
				}
				else{
					if(response.message == 'personal_info'){
						console.log($('#pay_type').val());
						$('#modalPersonalInfo').modal('show');
						response.data.avatar ? $('.avatar-g').removeClass('hide') : $('.avatar-r').removeClass('hide') ;
						response.data.clear_budget ? $('.clear_budget-g').removeClass('hide') : $('.clear_budget-r').removeClass('hide') ;
						response.data.verify_account ? $('.verify_account-g').removeClass('hide') : $('.verify_account-r').removeClass('hide') ;
						response.data.skill_info ? $('.skill_info-g').removeClass('hide') : $('.skill_info-r').removeClass('hide') ;
					}
					$(".ajaxloader").hide();
				}
			}
		});

		$('#modal_post_project').click(function (){
			$('#modalPersonalInfo').modal('hide');
			$('#continue_posting').val('1');
			$('#project-submit-btn').trigger('click');
		});

		$('.modal_draft_project').click(function (){
			$('#modalPersonalInfo').modal('hide');
			$('#continue_posting').val('1');
			$('#is_draft').val('1');
			$('#project-submit-btn').trigger('click');
		});

		//$("#tbxDescription").keyup(function () {
		//    updateCountTbxDescription();
		//});
		//$("#tbxDescription").keypress(function () {
		//    updateCountTbxDescription();
		//});
		//$("#tbxDescription").keydown(function () {
		//    updateCountTbxDescription();
		//});

		var language = $('html').attr('lang');
		var summernoteLanguage = '';
		if( language == "cn" )
		{
			summernoteLanguage = "zh-CN";
		}
		else
		{
			summernoteLanguage = "en-US";
		}

		$('#snDescription').summernote({
			placeholder: lang.example_job_description,
			lang: summernoteLanguage,
			height: 300,
			toolbar:[
				['style',['bold','fontsize']],
				['insert', ['link', 'picture','codeview']],
			],
			callbacks: {
				onChange: function(contents, $editable) {
					var stripHtmlTag = contents.replace(/<(?:.|\n)*?>/gm, '');

					$('#tbxDescription').val(contents);
					updateCountTbxDescription();

					if(stripHtmlTag.length > 0 )
					{
						if($('.note-editor').hasClass('error-border') && $('#tbxDescription').val() != '' ){
							$('.note-editor').removeClass('error-border');
							$("#description-require").hide();
						}
					}
					else
					{
						$('.note-editor').addClass('error-border');
						$("#description-require").show();
					}
				},
				onImageUpload: function(files) {
					for (var i = 0; i < files.length; i++) {
						send(files[i]);
					}
				}
			}
		});

		function send(file)
		{
			if (file.type.includes('image'))
			{
				var name = file.name.split(".");
				name = name[0];
				var data = new FormData();
				data.append('file', file);
				$.ajax({
					url: url.uploadDescriptionImage,
					type: 'POST',
					contentType: false,
					cache: false,
					processData: false,
					dataType: 'JSON',
					data: data,
					success: function (resp) {
						if(resp.status == "success")
						{
							$('#snDescription').summernote('insertImage', resp.data['url']);
						}
						else
						{
							alert(resp.msg);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						alert(textStatus + " " + errorThrown);
					}
				});
			}
		}

		$('ul.subcategory li').click(function(e)
		{
			var categoryId  = parseInt($(this).attr("catId"));
			var categoryName = $(this).attr("catName");
			$('#category_id').val(categoryId);
			$('#category_name').html(categoryName);
			$('#category_full_name').val(categoryName.trim());

			//allow category dropdown menu to close now
			$('#category_menu_allow_close').val(1);

			if($('#slcCategory').hasClass('error-border')){
				$('#slcCategory').removeClass('error-border');
				$("#category-require").hide();
			}
		});

		//set the label with ajax postback and refresh problem
		var categoryFullname = $('#category_full_name').val();
		if(categoryFullname == ""){
			$('#category_name').html(lang.please_select);
		}else{
			$('#category_name').html(categoryFullname);
		}

		//reset menu and not allow to close until level 2 category being click
		$('.category.dropdown').click(function(e){
			$('#category_menu_allow_close').val(0);
		});

		$('li.category').click(function(e)
		{
			var category_menu_allow_close = $('#category_menu_allow_close').val();
			if(category_menu_allow_close == 0){
				e.stopPropagation();
			}else{

			}
		});

		$('ul.project li').click(function(e)
		{
			var projectId  = parseInt($(this).attr("projId"));
			var projectName = $(this).attr("projName");
			$('#modal_pre_project_id').val(projectId);
			$('#modal_pre_project_name').html(projectName);
			$('#modal_project_id').html(projectId);
			$('#modal_project_name').html(projectName);
		});

		$('ul.country li').click(function(){
			var countryId = parseInt($(this).data('id'));
			var countryName = $(this).data('name');
			$('#country_name').html(countryName);
			$('#country_id').val(countryId);
		});

		$('ul.language li').click(function(){
			var languageId = parseInt($(this).data('id'));
			var languageName = $(this).data('name');
			$('#language_name').html(languageName);
			$('#language_id').val(languageId);
		});

		//Question
		$('.btn-add-more-question').click(function(){

			//only allow 8 question field
			$totalQuestionBox = $('div[name*="div-question-box"]').length;
			if( $totalQuestionBox == 8 ){
				return true;
			}

			//random int
			var item = Math.floor(1000 + Math.random() * 9000);

			var result = '<div class="col-md-12" style="padding:0px;"><div class="form-group div-question-top" id="question-' + item + '" name="div-question-box">' +
				'<div class="col-md-8 col-xs-10 div-question">' +
				'<textarea class="form-control" placeholder="" maxlength="256" rows="3" id="tbxQuestion-' + item + '" name="question[]" cols="50"></textarea>'+
				'<span class="pull-right">' +
				'<span id="noOfCharQuestion-' + item + '">256</span> Characters Left </span>' +
				'</br>' +
				'<span id="question-require-' + item + '" class="error-message" style="display:none;"><i class="fa fa-exclamation-circle"></i> ' + lang.field_required+ '</span>' +
				'</div>' +
				'<div class="col-md-4 col-xs-2 div-close-question">' +
				'<button id="btn-close-question-' + item + '" type="button" class="close pull-left">' +
				'&times;' +
				'</button>' +
				'</div>' +
				'</div></div>';
			$('.section-question').append(result);
		});
		$("#tbxName").blur(function(){
			if($(this).hasClass('error-border') && $(this).val() != '' ){
				$(this).removeClass('error-border');
				$("#project-name-require").hide();
			}
		});

		//$("#tbxDescription").blur(function(){
		//    if($(this).hasClass('error-border') && $(this).val() != '' ){
		//        $(this).removeClass('error-border');
		//        $("#description-require").hide();
		//    }
		//});

		$(".note-editor").blur(function(){
			if($(this).hasClass('error-border') && $('#tbxDescription').val() != '' ){
				$(this).removeClass('error-border');
				$("#description-require").hide();
			}
		});

		$("#reference_price").blur(function(){
			if($(this).hasClass('error-border') && $(this).val() != '' ){
				$(this).removeClass('error-border');
				$("#fixed-price-require").hide();
			}
		});
		$("input[name='time_commitment']").click(function(){
			if( $("input[name='time_commitment']").parent('.pay-type-custom').find('.pay-type-label').hasClass('error-border')){
				$("input[name='time_commitment']").parent('.pay-type-custom').find('.pay-type-label').removeClass('error-border');
				$("#time-commitment-require").hide();
			}
		});

		$("input[name='time_duration']").click(function(){
			if( $("input[name='time_duration']").parent('.pay-type-custom').find('.pay-type-label').hasClass('error-border')){
				$("input[name='time_duration']").parent('.pay-type-custom').find('.pay-type-label').removeClass('error-border');
				$("#time-commitment-require").hide();
			}
		});

		$("div[name='rdFreelanceType']").click(function(){
			$("div[name='rdFreelanceType']").attr('style', '');
			$("#freelance-type-require").hide();
		});

		$(document.body)
			.on("click", "button[id ^='btn-close-question-']", function(){
				var questionId = parseInt(this.id.replace("btn-close-question-", ""));
				$('#question-' + questionId).remove();
			});

		$(document.body)
			.on("keypress", "textarea[id ^='tbxQuestion-']", updateCountTbxQuestion)
			.on("keyup", "textarea[id ^='tbxQuestion-']", updateCountTbxQuestion)
			.on("keydown", "textarea[id ^='tbxQuestion-']", updateCountTbxQuestion)
			.on("blur", "textarea[id ^='tbxQuestion-']", function(e){
				if($(this).hasClass('error-border') && $(this).val() != ''){
					$(this).removeClass('error-border');
					$(this).parent().children('.error-message').hide();
				}
			});

		$('input[id^="pay-type-"]').click(function() {

			if($('#pay-type-1').is(':checked') ){

				$('.div-price').show();
				$('#label-div-price-hour').show();
				;
				$('#label-div-price-fix').hide();

			}else if( $('#pay-type-2').is(':checked') ){

				$('.div-price').show();

				$('#label-div-price-hour').hide();
				$('#label-div-price-fix').show();

			}else if( $('#pay-type-3').is(':checked') ){

				$('.div-price').hide();
			}

			$('#reference_price').val('');

			if($("input[name='pay_type']").parent('.pay-type-custom').find('.pay-type-label').hasClass("error-border")){
				$("input[name='pay_type']").parent('.pay-type-custom').find('.pay-type-label').removeClass("error-border");
				$("#pay-require").hide();
			}
		});

		var i = 0;
		$(document.body)
			.on("click", ".attachment-no-file", function(){
				var count = $('.upload-file').length;
				if( count == 8 )
				{
					alertError(lang.max_upload_eight_documents);
					return true;
				}

				$('.file-attachments').append(
					$('<input/>').attr('type', 'file').attr('name', 'fileUpload[]').attr('id', 'fileupload_' + i).attr('style', 'display:none')
				);
				$('#fileupload_' + i).trigger('click');
			});

		$(document.body)
			.on("change", "input[id^='fileupload_']", function(){
				var file = $('#fileupload_' + i)[0].files[0];
				var fileName = "" + $('#fileupload_' + i).val();
				var fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
				var itemId = i;

				$.ajax({
					url: url.getInputFile,
					dataType: 'json',
					data: {'fileName': fileName, 'itemId': itemId},
					method: 'POST',
					beforeSend: function () {
					},
					error: function () {
						alert(lang.unable_to_request);
					},
					success: function (response) {
						if ( response.status == "OK" )
						{
							$('#btn-add-new-file').remove();
							$('#attachments-sect').append(response.contents);
						}
					}
				});

				i++;
			});

		$(document.body)
			.on("click", "span[id^='btn-delete-']", function(){
				var itemId = parseInt(this.id.replace("btn-delete-", ""));
				$('#fu-item-' + itemId).remove();
				$('#fileupload_' + itemId).remove();
			});

		$(document.body)
			.on("click", "span[id^='btn-remove-existing-']", function(){
				var projectFileId = parseInt(this.id.replace("btn-remove-existing-", ""));
				$('#fu-item-' + projectFileId).remove();
				$('#fileupload_' + projectFileId).remove();

				var input = '<input type="hidden" name="delete_file[]" value="' + projectFileId + '" />';
				$('#add-project-form').append(input);
			});
	}
);

function updateCountTbxDescription (e) {
	var qText = $("#tbxDescription").val();

	if(qText.length <= 5000) {
		$("#noOfCharDescription").html(5000 - qText.length );
	} else {
		jQuery("#noOfCharDescription").html(0);
		$("#tbxDescription").val(qText.substring(0,5000));
	}
}

function updateCountTbxQuestion (e) {
	var questionId = parseInt(this.id.replace("tbxQuestion-", ""));

	var qText = $("#tbxQuestion-" + questionId ).val();
	console.log('in text change');
	console.log(qText);
	if(qText.length <= 256) {
		console.log(256 - qText.length);
		var len = 256 - qText.length;
		var html = `<span> ${len} </span>`;
		console.log(html);
		$("#noOfCharQuestion-" + questionId ).html(html);
	} else {
		jQuery("#noOfCharQuestion-" + questionId).html(0);
		$("#tbxQuestion-" + questionId).val(qText.substring(0,256));
	}
}

function checkPersonalInfo() {
	$.ajax({
		url: '/checkPersonalInfo',
		type: 'get',
		success: function (res){
			console.log(res);
		},
		error: function (err){
			console.log(err);
		}
	});
	return false;
}

function validate(){
	$(".ajaxloader").show();
	errName = lang.please_input_project_name;
	errCategoryId = lang.please_choose_category;
	errDescription = lang.please_input_description;
	errSkillId = lang.please_choose_skill;
	errPayType = lang.please_choose_pay_type;
	errReferencePrice = lang.please_input_reference_price;
	errTimeCommitment = lang.please_choose_commitment_time;
	errTimeDuration = lang.please_choose_time_duration;
	errFreelanceType = lang.please_choose_freelance_type;
	errQuestion = lang.please_input_question;
	err = g_lang.error;

	var errors = [];
	var scrollToError = "";
	$.each($('textarea[name="question[]"]'), function () {
		if ($(this).val() == '') {
			$(this).addClass('error-border');
			$(this).parent().children('.error-message').show();
			errors.push(errQuestion);
			scrollToError = $(this).name;
		}
	});
	// if ($('textarea[name="question[]"]').val() == '') {
	//     errors.push(errQuestion);
	//     $("textarea[name='question[]']").addClass('error-border');
	//     scrollToError = "question";
	//     $("textarea[name='question[]']").parent().children('.error-message').show();
	// }

	if (!$('input[name="freelance_type"]').is(':checked')) {
		errors.push(errFreelanceType);
		$("div[name='rdFreelanceType']").attr('style', 'border:1px solid red !important;');
		$("#freelance-type-require").show();
		scrollToError = "freelance_type";
	}

	if (!$('input[name="time_commitment"]').is(':checked')) {
		errors.push(errTimeCommitment);
		$("input[name='time_commitment']").parent('.pay-type-custom').find('.pay-type-label').addClass('error-border');
		$("#time-commitment-require").show();
		scrollToError = "time_commitment";
	}

	if (!$('input[name="time_duration"]').is(':checked')) {
		errors.push(errTimeDuration);
		$("input[name='time_duration']").parent('.pay-type-custom').find('.pay-type-label').addClass('error-border');
		$("#time-commitment-require").show();
		scrollToError = "time_duration";
	}

	if ($('input[name="reference_price"]').val() == '' &&
		(
			$('#pay-type-1').is(':checked') ||
			$('#pay-type-2').is(':checked')
		)
	) {
		errors.push(errReferencePrice);
		$("#reference_price").addClass('error-border');
		$("#fixed-price-require").show();
		scrollToError = "reference_price";
	}

	if (!$('input[name="pay_type"]').is(':checked')) {
		$("input[name='pay_type']").parent('.pay-type-custom').find('.pay-type-label').addClass('error-border');
		$("#pay-require").show();
		errors.push(errPayType);
		scrollToError = "pay_type";
	}

	if ($('input[name="skill_id_list"]').val() == 0) {
		errors.push(errSkillId);
		$("#magicsuggest").attr('style', 'border-color:red !important;');
		$('#magicsuggest').addClass('error-border');
		$("#skill-require").show();
		scrollToError = "skill";
	}

	//if ($('textarea[name="description"]').val() == '') {
	//    errors.push(errDescription);
	//    $("#tbxDescription").addClass('error-border');
	//    $("#description-require").show();
	//    scrollToError = "description";
	//}

	if ($('input[name="description"]').val() == '') {
		errors.push(errDescription);
		$(".note-editor.note-frame").addClass('error-border');
		$("#description-require").show();
		scrollToError = "description";
	}

	if ($('input[name="category_id"]').val() == 0) {
		errors.push(errCategoryId);
		$("#slcCategory").addClass('error-border');
		$("#category-require").show();
		scrollToError = "category";
	}

	if ($('input[name="name"]').val() == '') {
		errors.push(errName);
		$("#tbxName").addClass('error-border');
		$("#project-name-require").show();
		scrollToError = "name";
	}

	//reset the error
	$("#tbxName").attr('style', '');
	$("#slcCountry").attr('style', '');
	$("#slcLanguage").attr('style', '');
	$("#slcCategory").attr('style', '');
	$("#tbxDescription").attr('style', '');
	// $("#magicsuggest").attr('style', '');
	$("input[name='pay_type']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
	if ($('#pay-type-1').is(':checked')) {
		$("input[name='pay_type'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
	}
	else if ($('#pay-type-2').is(':checked')) {
		$("input[name='pay_type'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
	}
	else if ($('#pay-type-3').is(':checked')) {
		$("input[name='pay_type'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
	}

	$("#reference_price").attr('style', '');
	$("input[name='time_commitment']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
	if ($('#time-commitment-1').is(':checked')) {
		$("input[name='time_commitment'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
	}
	else if ($('#time-commitment-2').is(':checked')) {
		$("input[name='time_commitment'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
	}
	else if ($('#time-commitment-3').is(':checked')) {
		$("input[name='time_commitment'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
	}

	$("input[name='time_duration']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
	if ($('#time_duration-1').is(':checked')) {
		$("input[name='time_duration'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
	}
	else if ($('#time_duration-2').is(':checked')) {
		$("input[name='time_duration'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
	}
	else if ($('#time_duration-3').is(':checked')) {
		$("input[name='time_duration'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
	}else if ($('#time_duration-4').is(':checked')) {
		$("input[name='time_duration'][value='4']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
	}else if ($('#time_duration-5').is(':checked')) {
		$("input[name='time_duration'][value='5']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
	}

	// $("div[name='rdFreelanceType']").attr('style', '');
	$("textarea[name='question[]']").attr('style', '');

	switch (scrollToError) {
		case "name":
			App.scrollTo($('#tbxName'));
			break;
		case "category":
			App.scrollTo($('#slcCategory'));
			break;
		case "description":
			//App.scrollTo($('#tbxDescription'));
			App.scrollTo($('.note-editor'));
			break;
		case "skill":
			App.scrollTo($('#magicsuggest'));
			break;
		case "pay_type":
			App.scrollTo($("input[name='pay_type']"));
			break;
		case "reference_price":
			App.scrollTo($('#reference_price'));
			break;
		case "time_commitment":
			App.scrollTo($("input[name='time_commitment']"));
			break;
		case "time_duration":
			App.scrollTo($("input[name='time_duration']"));
			break;
		case "freelance_type":
			App.scrollTo($("div[name='rdFreelanceType']"));
			break;
		case "question":
			App.scrollTo($("textarea[name='question[]']"));
			break;

	}

	if (scrollToError == "") {
		return true;
	}
	else {
		$(".ajaxloader").hide();
		return false;
	}

}

var showError = function (val) {
	if (val instanceof Array) {
		$(val).each(function (index, value) {
			showError(value);
		});
	} else {
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"positionClass": "toast-top-right",
			"onclick": null,
			"showDuration": "1000",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}

		toastr["error"](val, err);
	}
};
