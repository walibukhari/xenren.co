jQuery(
    function ($) {
        //skill
        var selectedSkillList = [];
        if (skill_id_list != "") {
            selectedSkillList = skill_id_list.split(",")
        }
        var noSuggestionText = "<a href='javascript:;' data-toggle='modal' data-target='#modelAddNewSkillKeyword'>" + lang.help_us_improve_skill_keyword + "</a>";
        var msSkills = $('#msSkills').magicSuggest({
            allowFreeEntries: true,
            allowDuplicates: false,
            placeholder: g_lang.search,
            data: skillList,
            value: selectedSkillList,
            method: 'get',
            highlight: true,
            noSuggestionText: noSuggestionText,
            renderer: function(data){
                if( data.description == null )
                {
                    return '<b>' + data.name + '</b>' ;
                }
                else
                {
                    var name = data.name.split("|");

                    return '<b>' + name[0] + '</b> | <span class="search-desc">' + data.description + '</span>';
                }

            },
            selectionRenderer: function(data){
                result = data.name;
                name = result.split("|")[0];
                return name;
            }
        });
        $(msSkills).on(
            'selectionchange', function (e, cb, s) {
                $('#skill_id_list').val(cb.getValue());
            }
        );
    }
);

