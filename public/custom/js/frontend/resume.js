jQuery(
    function ($) {
        $('.send-request-for-contact-info').click(function () {
            var receiverId = $(this).data('receiver-id');
            $.ajax({
                url: '/findExperts/sendRequestForContactInfo',
                dataType: 'json',
                data: {receiverId: receiverId},
                method: 'POST',
                error: function (err) {
	                console.log(err);
	                alertError('Unable to request right now, please try again later');
                },
                success: function (result) {
	                console.log('result');
	                if (result.status == 'OK') {
                        alertSuccess(result.message);
                    } else if (result.status == 'Error') {
                        alertError(result.message);
                    } else{
                        alertError(result.message);
                    }
                }
            });
        });

        //collapse event
        $(".collapse").on('show.bs.collapse', function(){
            var isUserContactShow = false;
            var ipAddress = $(this).data('ip-address');
            var targetUserId = $(this).data('target-user-id');
            var userId = $(this).data('user-id');

            $.ajax({
                url: url.api_isAllowToCheckContact,
                dataType: 'json',
                data: {
                    ipAddress: ipAddress,
                    userId: userId,
                    targetUserId: targetUserId
                },
                method: 'post',
                async: false,
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if(result.status == 'success')
                    {
                        //allow to show
                        isUserContactShow = true;
                    }
                    else
                    {
                        //disallow to show
                        alertInfo(result.msg);
                        isUserContactShow = false;
                    }
                }
            });

            return isUserContactShow;
        });
    }
);