jQuery(
    function ($) {
        //template
        var template = '<div class="row">' +
                            '<label class="col-md-12 p-t-40">' +
                                lang.new_skill_keywords +
                            '</label>' +
                            '<div class="col-md-12">' +
                                '<input class="form-control" name="tbxSkillKeyword[]" value="" type="text">' +
                            '</div>' +
                        '</div>' +
                        '<div class="row">' +
                            '<label class="col-md-12 p-t-20">' +
                                lang.please_use_wiki_or_baidu_support_your_keyword +
                            '</label>' +
                            '<div class="col-md-12">' +
                                '<input class="form-control" name="tbxReferenceUrl[]" value="" type="text">' +
                            '</div>' +
                        '</div>';


        $('#btnAddMore').click(function()
        {
            var errorExist = false;
            $( "input[ name = 'tbxSkillKeyword[]']" ).each(function( index )
            {
                console.log( index + ": " + $( this ).val() );
                if(  $( this ).val() == "" )
                {
                    alert(lang.one_of_skill_input_empty);
                    errorExist = true;
                }
            });

            if( !errorExist)
            {
                var count = parseInt( $('#hidInputCount').val() );
                if( count == 10 )
                {
                    alert(lang.maximum_ten_skill_keywords_allow_to_submit);
                    return false;
                }
                else
                {
                    $('.add-more-section').append(template);
                    $('#hidInputCount').val(count + 1);
                }
            }

        });

        //submit the keyword
        $('#add-new-skill-keyword-form').makeAjaxForm({
          
            submitBtn: '#btn-submit-new-skill-keyword',
            alertContainer : '#add-new-skill-keyword-errors',
            clearForm: true,

            afterSuccessFunction: function (response, $el, $this) {
                if( response.status == 'success'){
                    App.alert({
                        type: 'success',
                        icon: 'check',
                        message: response.msg,
                        place: 'append',
                        container: '#body-alert-container',
                        reset: true,
                        focus: true
                    });
                    setTimeout(function(){
                        $('#modelAddNewSkillKeyword').modal('toggle');
                    }, 3000);
                }else{
                    App.alert({
                        type: 'danger',
                        icon: 'warning',
                        message: response.msg,
                        container: '#add-new-skill-keyword-errors',
                        reset: true,
                        focus: true
                    });
                }

                App.unblockUI('#add-new-skill-keyword-form');
            }
        });
    }
);

