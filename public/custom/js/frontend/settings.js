$(document).ready(function(){
    if(user_email_notification == email_notification)
    {
        $('#emailNotification').addClass('btn-active2');
    }

    if(user_wechat_notification == wechat_notification)
    {
        $('#wechatNotification').addClass('btn-active2');
    }

    if(user_ui_preference == user_ui_preference_both) {
        $('#asBoth').addClass('btn-active2');
    }

    if(checked_in_space == checked_in_COWORK) {
        $('#checked_in_cowork_space').addClass('btn-active2');
    }

    if(user_push_notification == push_notification) {
        $('#push_notification').addClass('btn-active2');
    }

    if(user_close_account != close_account) {
        $('#close_account').addClass('btn-active2');
    }

    if(user_ui_preference == user_ui_preference_freelancer) {
        $('#asFreelance').addClass('btn-active2');
        $('.jobsHide').hide();
        $('.receivedProjects').hide();
        $('.setNB1').css('left','19%');
    }

    if(user_ui_preference == user_ui_preference_employer) {
        $('#asEmployee').addClass('btn-active2');
        $('.FindExpertHide').hide();
        $('.awardedProjects').hide();
    }

});

function store(match){
    if(match === 'email') {
        var email_element = document.getElementById("emailNotification");
        var email_notification =  email_element.classList.toggle('btn-active2');

        if(email_notification == true) {
            var x = 1+match;
        } else {
            var x= 0+match;
        }
    } else if(match === 'wechat') {
        var wechat_element = document.getElementById("wechatNotification");
        var wechat_notification =  wechat_element.classList.toggle('btn-active2');
        if(wechat_notification == true) {
            var x = 1+match;
        } else {
            var x = 0+match;
        }
    } else if(match === 'employee') {
        var  employee_element = document.getElementById("asEmployee");
        var  as_employee = employee_element.classList.toggle('btn-active2');

        if(as_employee == true) {
            var x = 2+match;

        }
    } else if(match === 'freelance') {
        var  freelance_element = document.getElementById("asFreelance");
        var  as_freelance = freelance_element.classList.toggle('btn-active2');

        if(as_freelance == true) {
            var x = 1+match;

        }
    } else if(match === 'uipreference') {
        var  uipreference_element = document.getElementById("asBoth");
        var  as_both = uipreference_element.classList.toggle('btn-active2');

        if(as_both == true) {
            var x = 0+match;
        }
    } else if(match === 'checked_in_cowork_space') {
        var  checked_cowork_space_element = document.getElementById("checked_in_cowork_space");
        var  as_cowork_space = checked_cowork_space_element.classList.toggle('btn-active2');

        if(as_cowork_space == true) {
            var x = 1+match;
        } else {
            var x = 0+match;
        }
    } else if (match === 'push_notification') {
        var push_element = document.getElementById("push_notification");
        var push_notification =  push_element.classList.toggle('btn-active2');

        if(push_notification == true) {
            var x = 1+match;
        } else {
            var x= 0+match;
        }
    } else if (match === 'close_account') {
        var close_element = document.getElementById("close_account");
        var close_account =  close_element.classList.toggle('btn-active2');

        if(close_account == true) {
            var x = 1+match;
        } else {
            var x= 0+match;
        }
    }
    $.ajax({
        type:'POST',
        url:'/data/'+x,
        data:x,
        success:function(data){
            console.log(data);
            window.location.reload();
        }
    });

}
