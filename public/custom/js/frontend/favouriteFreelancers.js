jQuery(
    function ($) {
        $('.btn-check-detail').click(function(){
            var url = $(this).data('url');
            window.location.href = url;
        });

        $('.btn-remove-fav-freelancer').click(function(){
            var url = $(this).data('url');
            var freelancerId = $(this).data('freelancer-id');
            var targetEle = $(this);

            $.ajax({
                url: url,
                dataType: 'json',
                data: {freelancerId: freelancerId},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if (result.status == 'OK') {
                        $('#freelancer-'+ freelancerId).hide();
                        alertSuccess(result.message);
                    } else if (result.status == 'Error') {
                        alertError(result.message);
                    }
                }
            });
        });
    }
);