jQuery(
    function ($) {

        $('button[id^="btn-make-offer-"]').click(function(){
            var projectId = parseInt(this.id.replace("btn-make-offer-", ""));
            $('.modal-project-id').val(projectId);

            var questions = JSON.parse($('.question-' + projectId).val());

            $('.question-section').html('');
            for(var i in questions) {
                var result = '<div class="form-group">' +
                    '<label>Question ' + ( parseInt(i) + 1 )+ ' : ' + questions[i].question + ' </label>' +
                    '<input class="form-control" name="answer[]" value="" type="text">' +
                    '<input type="hidden" class="form-control" name="questionId[]" value="' + questions[i].id + '" type="text">' +
                    '</div>';
                $('.question-section').append(result);
            }

            $('#make-offer-form').resetForm();

            $('#tbxQQId').val($('.qq-id').val());
            $('#tbxWechatId').val($('.wechat-id').val());
            $('#tbxSkypeId').val($('.skype-id').val());
            $('#tbxHandphoneNo').val($('.handphone-no').val());

            $('#model-alert-container').html('');
        });

        $('#btn-submit').click(function(){
            $('#make-offer-form').submit();
        });

        $('#make-offer-form').ajaxForm({
            beforeSubmit: function() {
                App.blockUI({
                    target: '#make-offer-form',
                    overlayColor: 'none',
                    centerY: true,
                    boxed: true
                });
            },
            success: function(resp) {
                if (resp.status == 'success') {
                    App.alert({
                        type: 'success',
                        icon: 'check',
                        message: resp.msg,
                        place: 'append',
                        closeInSeconds: 3,
                        container: '#model-alert-container',
                        reset: true,
                        focus: true
                    });

                    App.unblockUI('#make-offer-form');
                    $('#modalMakeOffer').scrollTop(0);

                    setTimeout(function() {
                        $('.modal').modal('hide');
                        $('#make-offer-form').resetForm();
                        location.reload();
                    }, 3 * 1000);
                }else{
                    App.alert({
                        type: 'danger',
                        icon: 'warning',
                        message: resp.msg,
                        container: '#model-alert-container',
                        reset: true,
                        focus: true
                    });

                    App.unblockUI('#make-offer-form');
                    $('#modalMakeOffer').scrollTop(0);
                }
            },
            error: function(response, statusText, xhr, formElm){
                if (response.status == 422) {
                    $.each(response.responseJSON, function(i) {
                        $.each(response.responseJSON[i], function(key, value) {
                            App.alert({
                                type: 'danger',
                                icon: 'warning',
                                message: value,
                                container: '#model-alert-container',
                                reset: true,
                                focus: true
                            });
                        });
                    });
                }

                App.unblockUI('#make-offer-form');
                $('#modalMakeOffer').scrollTop(0);
            }
        });


        // jquery send messages functions

        //load messages
        setTimeout(function(){
            var url = $('.project-conversation').data('url');
            $.ajax({
                url: url,
                dataType: 'json',
                method: 'get',
                beforeSend: function() {
                },
                success: function(result) {
                    if(result.status == 'OK'){
                        $('#content-chat').html('<ul class="chats"></ul>');
                        $.each(result.chatLog, function(i, e) {
                            var changeDate = '';
                            if(e.changeDate == 1){
                                // changeDate = '<li><div class="strike"><span>'+e.date+'</span></div></li>';
                            }
                            var image = '';
                            if(e.isImage == 1){
                                image = '<br><a href="/' + e.file + '" data-lightbox="image-' + e.id + '"><img src="/' + e.file + '" class"img-thumbnail" width="75px" data-lightbox="image-' + e.id + '"></a>'
                            }
                            var audio = '';
                            if(e.isAudio == 1){
                                audio = '<br><audio controls><source src="/'+e.file+'" type="audio/ogg"><source src="'+e.file+'" type="audio/mp3">Your browser does not support the audio element.</audio>'
                            }
                            $('#content-chat ul').append(changeDate+'<li class="'+e.status+'">'+
                                                                        '<img class="avatar" alt="" src="'+e.avatar+'" />'+
                                                                        '<div class="message">'+
                                                                            '<a href="javascript:void(0);" class="name"> '+e.email+' </a>'+
                                                                            '<p class="datetime"> '+e.time+'</p>'+
                                                                            '<p class="body"> '+ e.content +' '+image+''+audio+'</p>'+
                                                                        '</div>'+
                                                                    '</li>');
                        });

                        //always scroll to bottom
                        var scrollHeight =  $('#content-chat').prop('scrollHeight');
                        $("#content-chat").slimScroll({ scrollTo: scrollHeight });

                    }
                },
                error: function(a, b) {
                    if (a.status != 422) {
                        alert('Unknown error occured, please try again later');
                    }
                }
            });

            //var urlFile = $('.order-conversation').data('urlfile');
            //$.ajax({
            //    url: urlFile,
            //    dataType: 'json',
            //    method: 'get',
            //    beforeSend: function() {
            //    },
            //    success: function(result) {
            //        console.log(result);
            //        if(result.status == 'OK' && result.count > 0){
            //            $('#file-content').html('');
            //            $('.file-upload .slimScrollDiv').css('height', '300px');
            //            $('#content-file').css('height', '300px');
            //            $.each(result.chatLog, function(i, e) {
            //                $('.file-list').append(
            //                    '<li>' +
            //                    '<div class="file-logo pull-left">' +
            //                    '<img alt="file-logo" src="/images/file-logo.png">' +
            //                    '</div>' +
            //                    '<div class="file-info text-left">' +
            //                    'filename : '+e.content+'<br/>' +
            //                    '<span class="size-datetime">'+e.size+' - '+e.time+'</span><br>' +
            //                    '<a href="/'+e.file+'" class="download-file">下载纸样</a>' +
            //                    '</div>' +
            //                    '</li>');
            //            });
            //        }
            //    },
            //    error: function(a, b) {
            //        if (a.status != 422) {
            //            alert('Unknown error occured, please try again later');
            //        }
            //    }
            //});
        }, 1000);

         //upload image modal show
        $('#chat-message-img').click(function(e){
            e.preventDefault();
            var url = $(this).data('url');
            $('#add-order-file-chat').attr('action',url);
            $('#modalAddFileChat').modal('show');
        });
         //file send modal show
        $('#chat-message-file').click(function(e){
            e.preventDefault();
            var url = $(this).data('url');
            $('#add-order-file-chat').attr('action',url);
            $('#modalAddFileChat').modal('show');
        });
         //send messages
        $('.chat-form form').submit(function(e){
            e.preventDefault();
            var url = $(this).attr('action');
            var data = $(this).serialize();
            $.ajax({
                url: url,
                data: data,
                dataType: 'json',
                method: 'post',
                statusCode: {
                    422: function (resp) {
                        var html = '';
                        var json = $.parseJSON(resp.responseText);

                        $.each(json, function(index, value) {
                            $.each(value, function(i, msg) {
                                alertError(msg, 'test');
                            })
                        });

                        //$('.chat-form .error-msg').html(html);
                    }
                },
                beforeSend: function() {
                    $('.chat-form .error-msg').html('');
                },
                success: function(result) {
                    if(result.status == "OK"){
                        $('#content-chat ul').append('<li class="in 4">'+
                                                        '<img class="avatar" alt="" src="'+result.avatar+'" />'+
                                                        '<div class="message">'+
                                                            //'<span class="arrow"> </span>'+
                                                            '<a href="javascript:void(0);" class="name"> '+result.realName+' </a>'+
                                                            '<p class="datetime"> ' + result.time + '</p>'+
                                                            '<p class="body"> '+ result.message +' </p>'+
                                                        '</div>'+
                                                    '</li>');
                        $('.chat-form .message').val('');

                        //always scroll to bottom
                        var scrollHeight =  $('#content-chat').prop('scrollHeight');
                        $("#content-chat").slimScroll({ scrollTo: scrollHeight });
                    }
                },
                error: function(a, b) {
                    if (a.status != 422) {
                        alert('Unknown error occured, please try again later');
                    }
                }
            });
            return false;
        });

        $('#invite-helpdesk').click(function(e){
            e.preventDefault();
            var url = $(this).data('url');
            $.ajax({
                url: url,
                dataType: 'json',
                method: 'get',
                statusCode: {
                    422: function (resp) {
                        var html = '';
                        var json = $.parseJSON(resp.responseText);

                        $.each(json, function(index, value) {
                            $.each(value, function(i, msg) {
                                alertError(msg, 'test');
                            });
                        });

                        //$('.chat-form .error-msg').html(html);
                    }
                },
                beforeSend: function() {
                    $('.chat-form .error-msg').html('');
                },
                success: function(result) {
                    if(result.status == "OK"){
                        $('#content-chat ul').append('<li class="in 5">'+
                                                        '<img class="avatar" alt="" src="'+result.avatar+'" />'+
                                                        '<div class="message">'+
                                                            '<span class="arrow"> </span>'+
                                                            '<a href="javascript:void(0);" class="name"> '+result.realName+' </a>'+
                                                            '<span class="body"> '+ result.message +' </span>'+
                                                        '</div>'+
                                                        '<div class="div-datetime">' +
                                                            '<span class="datetime"> '+result.time+'</span>'+
                                                        '</div>'+
                                                    '</li>');
                        alertSuccess('Invitation sent', 'clear');
                    }
                },
                error: function(a, b) {
                    if (a.status != 422) {
                        alert('Unknown error occured, please try again later');
                    }
                }
            });
            return false;
        });

        $('#add-project-file-chat').submit(function(e){
            e.preventDefault();
            var url = $(this).attr('action');
            var data = new FormData( this );
            console.log(data);
            $.ajax({
                url: url,
                data: data,
                dataType: 'json',
                processData: false,
                contentType: false,
                method: 'post',
                statusCode: {
                    422: function (resp) {
                        var html = '';
                        var json = $.parseJSON(resp.responseText);

                        $.each(json, function(index, value) {
                            $.each(value, function(i, msg) {
                                alertError(msg, 'test');
                            })
                        });

                        //$('.chat-form .error-msg').html(html);
                    }
                },
                beforeSend: function() {
                    $('.chat-form .error-msg').html('');
                },
                success: function(result) {
                    if(result.status == "OK"){
                        alertSuccess('Upload success', 'test');
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }
                },
                error: function(a, b) {
                    if (a.status != 422) {
                        alert('Unknown error occured, please try again later');
                    }
                }
            });
            return false;
        });

        $('.btn-chat-room').click(function(){
            $('.chat-section').show();
            $('.freelance-detail-section').hide();
            $('.btn-freelance-detail').removeClass('active');
            $('.btn-chat-room').addClass('active');
        });

        $('.btn-freelance-detail').click(function(){
            $('.chat-section').hide();
            $('.freelance-detail-section').show();
            $('.btn-freelance-detail').addClass('active');
            $('.btn-chat-room').removeClass('active');
        });

        $('body').on('click', 'button[id^="btn-choose-"]', function () {
            var userId = parseInt(this.id.replace("btn-choose-", ""));
            var projectId =  parseInt($('#projectId').val());
            var url = $('#choose_url').val();

            $.ajax({
                url: url,
                dataType: 'json',
                method: 'get',
                data: { userId: userId, projectId: projectId },
                beforeSend: function() {
                },
                success: function(result) {
                    if(result.status == 'success'){
                        window.location.href = '/joinDiscussion/getProject/' + projectId;
                    }
                },
                error: function(a, b) {
                    if (a.status != 422) {
                        alert('Unknown error occured, please try again later');
                    }
                }
            });
        });


    }
);