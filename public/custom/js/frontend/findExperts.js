var selectText = lang.select
jQuery(
    function ($) {
	    try {
		    $(".read-more-target").hide();
		    $('.clear-filter').click(function () {
                //remove all existing values
			    $('#tbxName').val("");
			    $('#skill_id_list').val("").trigger('change');

			    $('#job_position_name_label').text(selectText);
                $('#job_position_name').val("").trigger('change');
                $('#slcJobPosition').val("0");

                $('#hourly_range_name_label').text(selectText);
                $('#hourly_range_name').val("").trigger('change');
			    $('#slcHourlyRange').val("0");

                $('#review_type_name_label').text(selectText);
                $('#review_type_name').val("").trigger('change');
			    $('#slcReviewType').val("0");

                $('#country_name_label').text(selectText);
                $('#country_name').val("").trigger('change');
			    $('#slcCountry').val("0");

                $('#tbxJobTitle').val("").trigger('change');
			    $('#hidLanguageId').val("").trigger('change');

			    var count = $('.ms-close-btn').length;
			    for (var i = 0; i < count; i++) {
				    $('.ms-close-btn').trigger('click');
			    }
		    });

		    $('.send-search').click(function () {
				var session = window.session.value;
				var base_url = session+'/Coworkers';
			    var name = $('#tbxName').val();
			    var skills = $('#skill_id_list').val();
			    var job_position_id = $('#slcJobPosition').val();
                var job_position_name = $('#job_position_name').val();
			    var hourly_range_id = $('#slcHourlyRange').val();
                var hourly_range_name = $('#hourly_range_name').val();
			    var review_type_id = $('#slcReviewType').val();
                var review_type_name = $('#review_type_name').val();
			    var job_title = $('#tbxJobTitle').val();
			    var country_id = $('#slcCountry').val();
                var country_name = $('#country_name').val();
			    var language_id = $('#hidLanguageId').val();

			    var redirectUrl = base_url + '?';
			    if (name == '' ||
				    job_position_id == 0 ||
				    skills == "" ||
				    hourly_range_id == 0 ||
				    review_type_id == 0 ||
				    job_title == "" ||
				    country_id == 0 ||
				    language_id == 0
			    ) {
                    window.location.href = '/'+redirectUrl;
                }

			    if (name != "") {
                    redirectUrl = redirectUrl + '&name=' + name;
                }

			    if (skills != "") {
                    redirectUrl = redirectUrl + '&skill_id_list=' + skills;
                }

			    if (job_position_id != 0) {
                    redirectUrl = redirectUrl + '&job_position_id=' + job_position_id + '&job_position_name=' + job_position_name;
                }

			    if (hourly_range_id != 0) {
                    redirectUrl = redirectUrl + '&hourly_range_id=' + hourly_range_id + '&hourly_range_name=' + hourly_range_name;
                }

			    if (review_type_id != 0) {
                    redirectUrl = redirectUrl + '&review_type_id=' + review_type_id + '&review_type_name=' + review_type_name;
                }

			    if (job_title != "") {
                    redirectUrl = redirectUrl + '&job_title=' + job_title;
                }

			    if (country_id != 0) {
                    redirectUrl = redirectUrl + '&country_id=' + country_id + '&country_name=' + country_name;
                }

			    if (language_id != 0) {
                    redirectUrl = redirectUrl + '&language_id=' + language_id;
                }

			    window.location.href = '/'+redirectUrl;
		    });

		    $('button[id^="btn-invite-"]').click(function () {
			    var receiverId = parseInt(this.id.replace("btn-invite-", ""));
			    $('#receiverId').val(receiverId);
		    });

		    $('#btn-submit-invite').click(function () {
			    var projectId = $('#slcProjectId').val();
			    var receiverId = $('#receiverId').val();

			    $.ajax({
				    url: '/findExperts/invite',
				    dataType: 'json',
				    data: {projectId: projectId, receiverId: receiverId},
				    method: 'get',
				    beforeSend: function () {
				    },
				    error: function () {
					    alert(lang.unable_to_request);
				    },
				    success: function (result) {
					    if (result.status == 'OK') {
						    $('#modalInvite').modal('toggle');
					    }
				    }
			    });
		    });

		    $('.btn-login').click(function () {
			    var url = $(this).data('url');
			    window.location.href = url;
		    });

		    function formatCity(city) {
			    if (!city.id) {
				    return city.text;
			    }
			    return city.text;
		    }

		    language = [];
		    api_city_url = [];
		    try {
			    $("#slcCity").select2({
				    language: language,
				    placeholder: lang.select_city,
				    theme: "bootstrap",
				    ajax: {
					    url: api_city_url,
					    dataType: 'json',

					    data: function (params) {
						    return {
							    q: params.term,
						    };
					    },
					    processResults: function (data, params) {
						    // parse the results into the format expected by Select2
						    // since we are using custom formatting functions we do not need to
						    // alter the remote JSON data, except to indicate that infinite
						    // scrolling can be used
						    return {
							    results: data,
						    };
					    },
					    cache: true
				    },
                   // escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
				    minimumInputLength: 2,
				    templateResult: formatCity, // omitted for brevity, see the source of this page
                   // templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
			    });
		    } catch (e) {
			    console.log(e);
		    }
		    //solve the ajaxpostback problem
		    var skill_id_list = $('#skill_id_list').val();
		    var selectedSkillList = [];
		    try {
			    if (skill_id_list != "") {
				    selectedSkillList = skill_id_list.split(",")
			    }
		    } catch (e) {
			    console.log(e);
		    }

            var ms = $('#magicsuggest').magicSuggest({
			    allowFreeEntries: false,
			    allowDuplicates: false,
			    hideTrigger: false,
			    placeholder: lang.enter,
			    data: skillList,
			    value: selectedSkillList,
			    method: 'get',
			    highlight: true,
			    renderer: function (data) {
				    if (data.description == null) {
					    return '<b>' + data.name + '</b>';
				    } else {
					    var name = data.name.split("|");
					    return '<b>' + name[0] + '</b> | <span class="search-desc">' + data.description + '</span>';
				    }
			    },
			    selectionRenderer: function (data) {
				    result = data.name;
				    name = result.split("|")[0];
				    return name;
			    }
		    });

		    $(ms).on(
			    'selectionchange', function (e, cb, s) {
				    $('#skill_id_list').val(cb.getValue()).trigger('change');
			    }
		    );

		    $('.send-request-for-contact-info').click(function () {
			    var receiverId = $(this).data('receiver-id');
			    $.ajax({
				    url: '/findExperts/sendRequestForContactInfo',
				    dataType: 'json',
				    data: {receiverId: receiverId},
				    method: 'POST',
				    error: function () {
					    console.log('lang.unable_to_request');
					    console.log(lang.unable_to_request);

				    },
				    success: function (result) {
					    if (result.status == 'OK') {
						    alertSuccess(result.message);
					    } else if (result.status == 'Error') {
						    alertError(result.message);
					    } else if (result.status == 'Notice') {
						    alertInfo(result.message);
					    }
				    }
			    });
		    });

		    var addFavourFree = url.addFavouriteFreelancer;
		    var removeFavourFree = url.removeFavouriteFreelancer;
		    $('.btn-add-fav-freelancer').click(function () {
			    var url = $(this).data('url');
			    var freelancerId = $(this).data('freelancer-id');
			    var targetEle = $(this);

			    $.ajax({
				    url: url,
				    dataType: 'json',
				    data: {freelancerId: freelancerId},
				    method: 'POST',
				    error: function () {
					    alert(lang.unable_to_request);
				    },
				    success: function (result) {
					    if (result.status == 'OK') {
						    if (targetEle.hasClass('activeFree')) {
							    targetEle.removeClass('activeFree');
							    targetEle.data('url', addFavourFree);
							    targetEle.children('i').removeClass('heart-red');
						    }
						    else {
							    targetEle.addClass('activeFree');
							    targetEle.data('url', removeFavourFree);
							    targetEle.children('i').addClass('heart-red');
						    }

						    alertSuccess(result.message);
					    } else if (result.status == 'Error') {
						    alertError(result.message);
					    }
				    }
			    });
		    });

		    if (showAdvanceFilterSection) {
			    $('.advance-filter-collapse').trigger('click');
		    }

		    //country language
		    var language_id = $('#hidLanguageId').val();
		    var selectedlang = [];
		    if (language_id != "") {
			    selectedlang = language_id.split(",")
		    }
		    var lms = $('#languageInput').magicSuggest({
			    allowFreeEntries: false,
			    allowDuplicates: false,
			    placeholder: lang.make_selection,
			    data: languages,
			    maxSelection: 1,
			    maxSelectionRenderer: function (v) {
				    return lang.only_choose_one;
			    },
			    value: selectedlang,
			    method: 'get',
			    renderer: function (data) {
				    return data.name;
			    },
			    selectionRenderer: function (data) {
				    return data.name;
			    }
		    });
		    $(lms).on('selectionchange', function (e, cb, s) {
			    $('#hidLanguageId').val(cb.getValue()).trigger('change');
		    });

		    //collapse event
		    $(".collapse").on('show.bs.collapse', function () {
			    var isUserContactShow = false;
			    var ipAddress = $(this).data('ip-address');
			    var targetUserId = $(this).data('target-user-id');
			    var userId = $(this).data('user-id');

			    $.ajax({
				    url: url.api_isAllowToCheckContact,
				    dataType: 'json',
				    data: {
					    ipAddress: ipAddress,
					    userId: userId,
					    targetUserId: targetUserId
				    },
				    method: 'post',
				    async: false,
				    error: function () {
					    alert(lang.unable_to_request);
				    },
				    success: function (result) {
					    if (result.status == 'success') {
						    //allow to show
						    isUserContactShow = true;
					    }
					    else {
						    //disallow to show
						    alertInfo(result.msg);
						    isUserContactShow = false;
					    }
				    }
			    });

			    return isUserContactShow;
		    });

		    $('.sectAboutMe').readmore({
			    moreLink: '<a href="javascript:;" class="f-c-green2" >' + lang.read_more + '</a>',
			    lessLink: '<a href="javascript:;" class="f-c-green2" >' + lang.read_less + '</a>',
			    speed: 500,
			    collapsedHeight: 35
		    });

		    var tar = $('.button-list');
		    $('.myDiv').click(function(){

		    })
		    $('.btn-check-detail').click(function (e) {
                var className = (e.target.className)
                if (className == "setFEPPBtn btn btn-success find-experts-success pull-right" ||
				    className == "btn find-experts-offer pull-right setFEPPBtn" ||
				    className == "setFEPPBtn btn find-experts-offer find-experts-check pull-right" ||
				    className == "btn find-experts-offer find-experts-check pull-right collapsed" ||
				    className == "fa fa-envelope" ||
				    className == "btn btn-success pull-right invite-freelance-gray" ||
				    className == "md-click-circle md-click-animate" ||
				    className == "btn find-experts-offer find-experts-request send-request-for-contact-info pull-right" ||
				    className == "fa fa-heart pull-right" ||
					className == "fa fa-heart pull-right heart-red" ||
					className == "fa fa-heart pull-right  heart-red" ||
					className == "btn-add-fav-freelancer setHeartactiveFree" ||
					className == "fa fa-heart pull-right " ||
					className == "btn-add-fav-freelancer setHeart" ||
					className == "setFEPPBtn123 btn pull-right" ||
					className == "setFEPPBtn123 fa fa-close" ||
					className == "contactInfoModal modal fade"
			    ) {
				    return;
			    }

			    var url = $(this).data('url');
                if(url)
    			    window.location.href = url;
		    });

		    //job position
		    $('.job-position .item').click(function () {
			    var id = $(this).data('id');
			    var name = $(this).data('name');

			    $('#job_position_name_label').html(name);
                $('#job_position_name').val(name).trigger('change');
			    $('#slcJobPosition').val(id);
		    });

		    //hourly range
		    $('.hourly-range .item').click(function () {
			    var id = $(this).data('id');
			    var name = $(this).data('name');

			    $('#hourly_range_name_label').html(name);
                $('#hourly_range_name').val(name).trigger('change');
			    $('#slcHourlyRange').val(id);
		    });

		    //review type
		    $('.review-type .item').click(function () {
			    var id = $(this).data('id');
			    var name = $(this).data('name');

			    $('#review_type_name_label').html(name);
                $('#review_type_name').val(name).trigger('change');
			    $('#slcReviewType').val(id);
		    });

		    //country
		    $('.country .item').click(function () {
			    var id = $(this).data('id');
			    var name = $(this).data('name');

			    $('#country_name_label').html(name);
                $('#country_name').val(name).trigger('change');
			    $('#slcCountry').val(id);
		    });

	    } catch (e) {
		    console.log(e);
	    }
    }
);
