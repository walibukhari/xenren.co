jQuery(
    function ($) {

        function split( val ) {
            return val.split( /,\s*/ );
        }
        function extractLast( term ) {
            return split( term ).pop();
        }

        // console.log(skillList);

        window.skillIdData = [];
        $( "#autocomplete" ).autocomplete({
            multiselect: true,
            // source: skillList,
            source: function (request, response) {
                console.log(extractLast(request.term));
                term = extractLast(request.term);
                console.log('term checking');
                console.log(term);
                console.log('term checking');

                // search by skill
                var skillSet = $.map(skillList, function (obj, key) {

                    var name = obj.name.split('|')[0].toUpperCase();
                    var resp = null
                    if (name.indexOf(term.toUpperCase()) !== -1) {
                        // var  ul = $(ul).wrap("<div class='col-md-5 search-main setHPSMAIN'></div>");
                        $('#autocomplete').css('border-top-left-radius','25px');
                        $('#autocomplete').css('border-top-right-radius','25px');
                        $('#autocomplete').css('border-bottom-left-radius','0px');
                        $('#autocomplete').css('border-bottom-right-radius','0px');
                        resp = {
                            label: obj.name + " (" + obj.age + ")", // Label for Display
                            value: obj.name.split('|')[0], // Value
                            id:obj.id
                        };
                    }
                    return resp;
                });

                // search by desc
                var skillSetByDesc = $.map(skillList, function (obj, key) {

                    var name = obj.name.split('|')[1].toUpperCase();

                    var resp = null
                    if (name.indexOf(term.toUpperCase()) !== -1) {
                        // var  ul = $(ul).wrap("<div class='col-md-5 search-main setHPSMAIN'></div>");
                        $('#autocomplete').css('border-top-left-radius','25px');
                        $('#autocomplete').css('border-top-right-radius','25px');
                        $('#autocomplete').css('border-bottom-left-radius','0px');
                        $('#autocomplete').css('border-bottom-right-radius','0px');
                        resp = {
                            label: obj.name + " (" + obj.age + ")", // Label for Display
                            value: obj.name.split('|')[0], // Value
                            id:obj.id
                        };
                    }

                    return resp;
                });

                // merge
                // skillSet = skillSet.concat(skillSetByDesc);

                // unique only
                function onlyUnique(value, index, self) {
                    return self.indexOf(value) === index;
                }
                skillSet = skillSet.filter( onlyUnique );

                if (!skillSet.length) {
                    sessionStorage.setItem('item','no-found');
                    console.log('item not found');
                    response({
                        label: "<a href=\"javascript:;\" style=\"height: 50px;color:red;display:flex;justify-content:center;\" data-toggle=\"modal\" data-target=\"#modelAddNewSkillKeyword\">Didn't found the keyword? Please help us improve.</a>",
                    });
                } else {
                    sessionStorage.setItem('item','found');
                    response(skillSet);
                }
            },
            focus: function() {
                return false;
            },
            select: function( event, ui ) {
                var terms = split( this.value );
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push( ui.item.value);
                // add placeholder to get the comma-and-space at the end
                terms.push( "" );
                console.log(terms);
                this.value = terms.join( ", " );
                $('#autocomplete').css('border-top-left-radius','25px');
                $('#autocomplete').css('border-top-right-radius','25px');
                $('#autocomplete').css('border-bottom-left-radius','25px');
                $('#autocomplete').css('border-bottom-right-radius','25px');
                return false;
            }
        });

        $.ui.autocomplete.prototype._renderItem = function (ul, item ) {
                $('.ui-autocomplete-multiselect').css('border-top-left-radius','25px');
                $('.ui-autocomplete-multiselect').css('border-top-right-radius','25px');
                $('.ui-autocomplete-multiselect').css('border-bottom-left-radius','0px');
                $('.ui-autocomplete-multiselect').css('border-bottom-right-radius','0px');
                // item.label = item.label.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong class='highlightSearched'>"+item.value+"</strong>");
                console.log('item.label');
                console.log(item);
                item.label = item.label.split('(undefined)')[0];
                return $("<li></li>")
                    .data("item.autocomplete", item.label)
                    .append("<a style='margin:0px !important;border:0px !important;width: 93% !important;\n" +
                        "    display: inline-block;'>" + item.label + "</a> <span style='\n" +
                        "    float: right;\n" +
                        "    position: absolute;" +
                        "    right:2px;" +
                        "    margin:0px !important;" +
                        "    border:0px !important;" +
                        "    color: #5ec329;\n" +
                        "    font-size: 24px;\n" +
                        "    font-weight: 600;\n'> + </span>")
                    .appendTo(ul);
        };

        var divWidth = $("div.ui-autocomplete-multiselect").width();
        var seTWidth = document.getElementById("ui-id-1").width = divWidth;
        $( document ).ready( function(){
            setMaxWidth();
            $( window ).bind( "resize", setMaxWidth ); //Remove this if it's not needed. It will react when window changes size.
            function setMaxWidth() {
                if (window.matchMedia("(max-width: 2000px)").matches && window.matchMedia("(min-width: 992px)").matches) {
                    $('.search-main').css('padding','0px');
                    $("#ui-id-1").wrap("<div class='col-md-5 setAutoComplete' />");
                } else if(window.matchMedia("(max-width: 991px)").matches && window.matchMedia("(min-width: 769px)").matches){
                    $('.search-main').css('padding-left','15px');
                    $('.search-main').css('padding-right','15px');
                    $("#ui-id-1").wrap("<div class='col-md-5 adujustAutoComplete' />");
                } else if(window.matchMedia("(max-width: 768px)").matches && window.matchMedia("(min-width: 668px)").matches){
                    $('.search-main').css('padding-left','15px');
                    $('.search-main').css('padding-right','15px');
                    $("#ui-id-1").wrap("<div class='col-md-5 audjustAutoComplete1' />");
                } else if(window.matchMedia("(max-width: 669px)").matches && window.matchMedia("(min-width: 450px)").matches){
                    $('.search-main').css('padding-left','15px');
                    $('.search-main').css('padding-right','15px');
                    $("#ui-id-1").wrap("<div class='col-md-5 audjustAutoComplete' />");
                } else if(window.matchMedia("(max-width: 414px)").matches && window.matchMedia("(min-width: 412px)").matches){
                    $('.search-main').css('padding-left','15px');
                    $('.search-main').css('padding-right','15px');
                    $("#ui-id-1").wrap("<div class='col-md-5 audjustAutoComplete2' />");
                } else if(window.matchMedia("(max-width: 411px)").matches && window.matchMedia("(min-width: 400px)").matches){
                    $('.search-main').css('padding-left','15px');
                    $('.search-main').css('padding-right','15px');
                    $("#ui-id-1").wrap("<div class='col-md-5 audjustAutoComplete3' />");
                } else if(window.matchMedia("(max-width: 375px)").matches && window.matchMedia("(min-width: 361px)").matches){
                    $('.search-main').css('padding-left','15px');
                    $('.search-main').css('padding-right','15px');
                    $("#ui-id-1").wrap("<div class='col-md-5 audjustAutoComplete4' />");
                } else if(window.matchMedia("(max-width: 360px)").matches && window.matchMedia("(min-width: 325px)").matches){
                    $('.search-main').css('padding-left','15px');
                    $('.search-main').css('padding-right','15px');
                    $("#ui-id-1").wrap("<div class='col-md-5 audjustAutoComplete5' />");
                } else if(window.matchMedia("(max-width: 320px)").matches){
                    $('.search-main').css('padding-left','15px');
                    $('.search-main').css('padding-right','15px');
                    $("#ui-id-1").wrap("<div class='col-md-5 audjustAutoComplete6' />");
                }
                $( "#ui-id-1" ).css( "maxWidth",  "100%" );
                $( "#ui-id-1" ).css( "minWidth",  "100%" );
                document.getElementById("ui-id-1").style.position = "absolute";
                document.getElementById("ui-id-1").style.left = "auto";
            }
        });


        $width = $(window).width()
        if( $width >= 1170 )
        {
            $('.small-justify-block img').width(180);
            $('.big-justify-block img').width(372);

        }
        else
        {
            $oneSection = Math.round( ($width - 200) / 6);

            $('.small-justify-block img').width($oneSection - 6);
            $('.big-justify-block img').width(($oneSection * 2) - 6);
        }

        $( window ).resize(function() {
            $width = $(window).width()
            if( $width >= 1170 )
            {
                $('.small-justify-block img').width(180);
                $('.big-justify-block img').width(372);

            }
            else
            {
                $oneSection = Math.round( ($width - 200) / 6);

                $('.small-justify-block img').width($oneSection - 6);
                $('.big-justify-block img').width(($oneSection * 2) - 6);
            }
        });

        var a = window.localStorage.getItem('selectBtn');
        if(a == null){
            // $(".search-type-btn-default[data-search-type='find-freelancer']").addClass('search-type-btn-color');
        }else{
            $(".search-type-btn-default[data-search-type='"+a+"']").addClass('search-type-btn-color');
        }

        var a = window.localStorage.getItem('selectBtn');
        if(a == null){
            // $(".search-type-btn-default[data-search-type='find-freelancer']").addClass('search-type-btn-color');
        }else{
            $(".search-type-btn-default[data-search-type='"+a+"']").addClass('search-type-btn-color');
        }

        $('body').click(function(){
            $('.ui-autocomplete-multiselect').css('border-top-left-radius','25px');
            $('.ui-autocomplete-multiselect').css('border-top-right-radius','25px');
            $('.ui-autocomplete-multiselect').css('border-bottom-left-radius','25px');
            $('.ui-autocomplete-multiselect').css('border-bottom-right-radius','25px');
        }, function () {
            $('.ui-autocomplete-multiselect').css('border-top-left-radius','25px');
            $('.ui-autocomplete-multiselect').css('border-top-right-radius','25px');
            $('.ui-autocomplete-multiselect').css('border-bottom-left-radius','25px');
            $('.ui-autocomplete-multiselect').css('border-bottom-right-radius','25px');
        });

        $('.search-type-text').click(function(){
            $('#search-type').hide();
            $('.search-type-text').each(function(index){
                $( this ).removeClass('search-type-btn-color').addClass('search-type-btn-default');
            });

            var setBtn = $(this).attr('data-search-type');
            window.localStorage.setItem('selectBtn', setBtn);

            $(this).addClass('search-type-btn-color');
            $searchType = $(this).data('search-type');
            $('.btn-search').attr('data-search-type', $searchType);
        });
        $('.btn-search').click(function(){
            var skillIdList = $(this).attr('data-skill-id-list');
            console.log('skillIdList');
            console.log(skillIdList);
            console.log('skillIdList');
            if( skillIdList == null )
            {
                var postVal = window.filteredData[0].id;
                console.log('postVal');
                console.log(postVal);
                console.log('postVal');
                $(this).attr('data-skill-id-list', postVal);
                skillIdList = $(this).attr('data-skill-id-list')
                // return;
                // alertError(lang.please_select_skills);
                // return true;
            }

            var searchType = $(this).data('search-type');
            // if( searchType == null )
            // {
            //     $('#search-type').show();
            //     $('#search-type').text(lang.please_select_freelancer_project_cofounder);
            //
            //     // alertError(lang.please_choose_search_type);
            //     return true;
            // }
            // else
            // {
                $('#search-type').text("");
                $('#search-type').hide();
            // }

            if( searchType == 'find-project' )
            {

                window.location.href = url.jobs + '?project_type=1&order=1&type=1&skill_id_list=' + skillIdList;
            }
            else if( searchType == 'find-freelancer' )
            {
                window.location.href = url.findExperts + '?skill_id_list=' + skillIdList;
            }
            else if( searchType == 'find-co-founder' )
            {
                window.location.href = url.findExperts + '?skill_id_list=' + skillIdList + '&status_id=1';
            }
            else if(searchType == null) {
                window.location.href = url.findExperts + '?skill_id_list=' + skillIdList;
            }
        });

        if( is_guest == false )
        {
            var noSuggestionText = "<a href='javascript:;' data-toggle='modal' data-target='#modelAddNewSkillKeyword'>" + lang.help_us_improve_skill_keyword + "</a>";
        }
        else
        {
            var noSuggestionText = "<a href='" + url.login + "' >" + lang.help_us_improve_skill_keyword + "</a>";
        }

        window.filteredData = [];
        // setTimeout(function(){
        //     window.filteredData = [];
        //     setInterval(function(){
        //         var val = $('.ms-sel-ctn').children()[0].value;
        //         if(!val || val == '') {
        //             window.filteredData = [];
        //         } else {
        //             ms.expand();
        //         }
        //     }, 100)
        // }, 3000);

        var ms = $('#msSearchBar').magicSuggest({
            allowFreeEntries: false,
            allowDuplicates: false,
            hideTrigger: true,
            placeholder: g_lang.search,
            data: skillList,
            method: 'get',
            highlight: true,
            // selectFirst: true,
            noSuggestionText: noSuggestionText,
            renderer: function(data, i){
                if(window.filteredData.length > 4) {
                    window.filteredData = [];
                }
                window.filteredData.push(data);
                // $('.ms-res-ctn , .dropdown-menu').css('display','none');
                if( data.description == null )
                {
                    return '<b>' + data.name + '</b>';
                    // return '<b>' + data.name + '</b>' + '<i class="fa fa-plus pull-right add-more"></i>';
                }
                else
                {
                    var name = data.name.split("|");
                    return '<b>' + name[0] + '</b> | <span class="search-desc">' + data.description + '<i class="fa fa-plus search-plus "></i></span>';
                    // return '<b>' + name[0] + '</b> | <span class="search-desc">' + data.description + '</span>' + '<i class="fa fa-plus pull-right add-more"></i>';
                }

            },
            selectionRenderer: function(data){
                result = data.name;
                name = result.split("|")[0];
                return name;
            }
        });

        $('.search-type-text').mouseenter(function(){
            if( $(this).hasClass('find-freelancer-btn') ) {
                $(".ms-sel-ctn").find("input").attr("placeholder", lang.freelancer_require_what_skill);
            }
            else if( $(this).hasClass('find-project-btn') ) {
                $(".ms-sel-ctn").find("input").attr("placeholder", lang.what_skill_you_have);
            }
            else if( $(this).hasClass('find-co-founder-btn') ) {
                $(".ms-sel-ctn").find("input").attr("placeholder", lang.co_founder_require_what_skill);
            }
            else {
                $(".ms-sel-ctn").find("input").attr("placeholder", g_lang.search);
            }
        });

        $('.search-type-text').mouseleave(function(){
            $(".ms-sel-ctn").find("input").attr("placeholder", g_lang.search);
        });

        $('.search-type-text').mouseenter(function(){
            if( $(this).hasClass('find-freelancer-btn') ) {
                $("#autocomplete").attr("placeholder", lang.freelancer_require_what_skill);
            }
            else if( $(this).hasClass('find-project-btn') ) {
                $("#autocomplete").attr("placeholder", lang.what_skill_you_have);
            }
            else if( $(this).hasClass('find-co-founder-btn') ) {
                $("#autocomplete").attr("placeholder", lang.co_founder_require_what_skill);
            }
            else {
                $("#autocomplete").attr("placeholder", g_lang.search);
            }
        });

        $('.search-type-text').mouseleave(function(){
            $("#autocomplete").attr("placeholder", g_lang.search);
        });



        $(ms).on(
            'selectionchange', function(e, cb, s){
                $('.btn-search').attr('data-skill-id-list', cb.getValue());
                $("#msSearchBar").css('border-bottom-left-radius','25px');
                $("#msSearchBar").css('border-bottom-right-radius','25px');
            }
        );

        $(ms).on('keydown', function(e,m,v){
            $('.ms-res-ctn').addClass('displayBlock');
            $("#msSearchBar").css('border-bottom-left-radius','14px');
            $("#msSearchBar").css('border-bottom-right-radius','14px');
            if(v.keyCode === 13 || v.which  === 13 ){
                $("#msSearchBar").css('border-bottom-left-radius','25px');
                $("#msSearchBar").css('border-bottom-right-radius','25px');
                var skillIdList = $('.btn-search').data('skill-id-list');
                // if( skillIdList == null )
                // {
                //     return true;
                // }

                var searchType = $('.btn-search').data('search-type');
                if( searchType == null )
                {
                    return true;
                }

                $('.btn-search').trigger('click');
            }
        });

        $('body').click(function () {
            $('#autocomplete').css('border-top-left-radius','25px');
            $('#autocomplete').css('border-top-right-radius','25px');
            $('#autocomplete').css('border-bottom-left-radius','25px');
            $('#autocomplete').css('border-bottom-right-radius','25px');

            $("#msSearchBar").css('border-bottom-left-radius','25px');
            $("#msSearchBar").css('border-bottom-right-radius','25px');
        });


        var userId = $('#user-id').val();
        checkFirstTimeFilling(userId);

        $('#changeStatusMenu').click(function(){
            if( userId > 0 )
            {
                $('#modalChangeUserStatus').modal('show');
            }
            else
            {
                window.location.href = url.login;
            }
        });

        $('.btnSkip').click(function(){
            var userId = $('#user-id').val();
            $.ajax({
                url: url.api_skipAddUserSkill,
                dataType: 'json',
                data: {
                    userId: userId
                },
                method: 'post',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if(result.status == 'success')
                    {
                        $('#modalChangeUserSkill').modal('hide');
                    }
                }
            });
        });

        $('.btnChangeSkill').click(function(){
            var userId = $('#user-id').val();
            var value = $('#skill_id_list').val();
            $.ajax({
                url: url.personalInformationChange,
                dataType: 'json',
                data: {
                    column: 'skill',
                    val: value
                },
                method: 'post',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if(result.status == 'success')
                    {
                        $('#modalChangeUserSkill').modal('hide');
                    }
                }
            });
        });
    },
    //user change Status js
    function ($) {
        //submit the keyword
        // $('#add-user-status-form').makeAjaxForm({
        //     inModal: true,
        //     closeModal: true,
        //     loadingText: '',
        //     submitBtn: '#btn-change-status-submit',
        //     alertContainer : '#add-user-status-errors',
        //     afterSuccessFunction: function (response, $el, $this) {
        //         if( response.status == 'success'){
        //             $('#modalChangeUserStatus').modal('toggle');
        //
        //             var path = window.location.pathname;
        //             var page = path.split("/").pop();
        //             //console.log( page );
        //
        //             if( page == "userCenter" )
        //             {
        //                 window.location.reload();
        //             }
        //         }
        //         else
        //         {
        //             App.alert({
        //                 type: 'danger',
        //                 icon: 'warning',
        //                 message: response.msg,
        //                 container: '#add-user-status-errors',
        //                 reset: true,
        //                 focus: true
        //             });
        //         }
        //
        //         App.unblockUI('#add-new-skill-keyword-form');
        //     }
        // });

        $('.status-box-label').on('click', function() {
            if ( $( this ).hasClass( "check" ) )
            {
                $(this).removeClass('check');
            }
            else
            {
                $(this).addClass('check');
            }

            var statusIdList = "";
            $(".status-box-label.check").each(function( index ) {
                if( index >= 1)
                {
                    statusIdList += ",";
                }
                statusIdList += $(this).data('id')
            });
            //console.log( " statusIdList: " +  statusIdList);
            $('#status_id_list').val(statusIdList)
        });


    }
    );
