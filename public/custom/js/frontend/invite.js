jQuery(
    function ($) {
        $('#btn-invite-friend').click(function(){
            var registerUrl = $(this).data('register-url');

            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val(registerUrl).select();
            document.execCommand("copy");
            $temp.remove();

            alertSuccess(lang.already_copy_and_use_in_paste)
        });
    }
);