jQuery(
    function ($) {

        $('.frontCard').on('click', function(){
            var a = window.confirm('are you sure you want to remove this image?');
            if(a) {
	            $('.frontCardDefaultImage').show();
                $('.frontCardDefaultImage').css('display','inline-block');
	            $('.frontCardImage').hide();
            }
        });

        $('.backCard').on('click', function(){
            var a = window.confirm('are you sure you want to remove this image?');
            if(a) {
	            $('.backCardDefaultImage').show();
	            $('.backCardImage').hide();
            }
        });

        var languages = $('html').attr('lang');
        var a = new Date();
        a.setYear(a.getFullYear() - 100);
        if(languages == 'en') {
            $('.input-group.date').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                startView: 2,
                startDate: a,
            });
        }
        else{
            $('.input-group.date').datepicker({
                format: "yyyy-mm-dd",
                autoclose: true,
                language: "zh-CN",
                startView: 2,
                startDate: a,
            });
        }

        $('#resubmit-identity').on('click', function () {
            $('#resubmit-container').remove();
            $('#submit_form').show();
        });

        $('#identity-submit-btn').click(function(){
            var real_name = $('#real_name').val();
            var id_card_no = $('#id_card_no').val();
            var date_of_birth = $('#date_of_birth').val();
            var address = $('#address').val();
            var handphone_no = $('#handphone_no').val();
            var country = $('#country').val();
            if(real_name === '')
            {
                $('.blockUI').hide();
                $('#real_name').addClass('errorColor');
                $('#id_card_no').removeClass('errorColor');
                $('#address').removeClass('errorColor');
                $('#date_of_birth').removeClass('errorColor');
                $('#handphone_no').removeClass('errorColor');
                $('#country').removeClass('errorColor');
                toastr.error('real name field is required');
            } else if(id_card_no === '')
            {
                $('#real_name').removeClass('errorColor');
                $('.blockUI').hide();
                $('#address').removeClass('errorColor');
                $('#country').removeClass('errorColor');
                $('#date_of_birth').removeClass('errorColor');
                $('#handphone_no').removeClass('errorColor');
                $('#id_card_no').addClass('errorColor');
                toastr.error('id card no is required');
            } else if(date_of_birth === '') {
                $('#real_name').removeClass('errorColor');
                $('#id_card_no').removeClass('errorColor');
                $('#country').removeClass('errorColor');
                $('.blockUI').hide();
                $('#address').removeClass('errorColor');
                $('#handphone_no').removeClass('errorColor');
                $('#date_of_birth').addClass('errorColor');
                toastr.error('date of birth is required');
            } else if (address === ''){
                $('#real_name').removeClass('errorColor');
                $('#country').removeClass('errorColor');
                $('#id_card_no').removeClass('errorColor');
                $('#date_of_birth').removeClass('errorColor');
                $('#handphone_no').removeClass('errorColor');
                $('.blockUI').hide();
                $('#address').addClass('errorColor');
                toastr.error('address field is required');
            } else if(handphone_no === ''){
                $('#handphone_no').addClass('errorColor');
                $('.blockUI').hide();
                $('#id_card_no').removeClass('errorColor');
                $('#address').removeClass('errorColor');
                $('#real_name').removeClass('errorColor');
                $('#date_of_birth').removeClass('errorColor');
                $('#country').removeClass('errorColor');
                toastr.error('hand phone  field is required');
            } else if(country === ''){
                $('#id_card_no').removeClass('errorColor');
                $('#address').removeClass('errorColor');
                $('#real_name').removeClass('errorColor');
                $('#date_of_birth').removeClass('errorColor');
                $('.blockUI').hide();
                $('#handphone_no').removeClass('errorColor');
                $('#country').addClass('errorColor');
                toastr.error('country field is required');
            } else {
                $('#id_card_no').removeClass('errorColor');
                $('#address').removeClass('errorColor');
                $('#real_name').removeClass('errorColor');
                $('#date_of_birth').removeClass('errorColor');
                $('#handphone_no').removeClass('errorColor');
                $('#country').removeClass('errorColor');
                $('#submit_form').submit();
            }
        });

        $('#identity-submit-btn').click(function () {
            $('.idSpanIdentityAuth').hide();
            $('.lds-ring').show();
        });

        $('#submit_form').makeAjaxForm({
            submitBtn: '#identity-submit-btn',
            alertContainer: '#body-alert-container',
            successFunction: function(resp){
                console.log(resp);
                $('.success').hide();
                $('.divSuccess').show();
                $('.txtSuccess').html(window.successMessage);
                $("html, body").animate({ scrollTop: 0 }, "slow");
                setTimeout(function(){
                    window.location.reload();
                }, 3000);
            },
            errorFunction: function(response, $el, $this) {
                $('.idSpanIdentityAuth').show();
                $('.lds-ring').hide();
                $('.blockUI').hide();
                if(response.responseJSON && response.responseJSON.errors)
                {
                    let err = response.responseJSON.errors;
                    Object.keys(err).forEach(function (key) {
                        console.log("errororor");
                        console.log(key, err[key][0]);
                        console.log("errororor");
                    });
                }
            }
        });


        function formatRepo(state) {
            if (!state.id) {
                return state.text;
            }
            var $state = $(
                '<span><img src="images/Flags-Icon-Set/24x24/' + state.code + '.png" class="img-flag" /> ' + state.text + '</span>'
            );
            return $state;
        }

        $("#country").select2({
            placeholder: countryPlaceHolder,
            language: "zh-CN",
            theme: "bootstrap",
	          minimumInputLength: 3,
            ajax: {
                url: url.api_countries,
                dataType: 'json',

                data: function (params) {
                    return {
                        q: params.term,
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
	                console.log(data);
	                return {
                        results: data,
                    };
                },
                cache: true
            },
//                    escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            templateResult: formatRepo, // omitted for brevity, see the source of this page
//                    templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });

        $('#real_name').blur(function(){
            var value = $(this).val();
            var id = $('#user_identity_id').val();

            $.ajax({
                url: url.api_identity_authentication_change,
                dataType: 'json',
                data: {'column': 'real_name', 'value': value, 'id': id},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (response) {
                    if ( response.status == "success" )
                    {
                        //alert('update username success');
                    }
                }
            });
        });

        $('#id_card_no').blur(function(){
            var value = $(this).val();
            var id = $('#user_identity_id').val();

            $.ajax({
                url: url.api_identity_authentication_change,
                dataType: 'json',
                data: {'column': 'id_card_no', 'value': value, 'id': id},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (response) {
                    if ( response.status == "success" )
                    {
//                      alert('update id card no success');
                    }
                }
            });
        });

        $('input[type=radio][name=gender]').change(function(){
            var value = $(this).val();
            var id = $('#user_identity_id').val();

            $.ajax({
                url: url.api_identity_authentication_change,
                dataType: 'json',
                data: {'column': 'gender', 'value': value, 'id': id},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (response) {
                    if ( response.status == "success" )
                    {
//                      alert('update gender success');
                    }
                }
            });
        });

        var previous_date_of_birth = "";
        $('#date_of_birth').change(function(){
            var value = $(this).val();
            var id = $('#user_identity_id').val();

            if( previous_date_of_birth == value)
            {
                return true;
            }
            else
            {
                previous_date_of_birth = value;
            }

            $.ajax({
                url: url.api_identity_authentication_change,
                dataType: 'json',
                data: {'column': 'date_of_birth', 'value': value, 'id': id},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (response) {
                    if ( response.status == "success" )
                    {
                        //alert('update date_of_birth success');
                    }
                }
            });
        });

        $('#address').blur(function(){
            var value = $(this).val();
            var id = $('#user_identity_id').val();

            $.ajax({
                url: url.api_identity_authentication_change,
                dataType: 'json',
                data: {'column': 'address', 'value': value, 'id': id},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (response) {
                    if ( response.status == "success" ) {
//                      alert('update address success');
                    }
                }
            });
        });

        $('#country').change(function(){
            var value = $(this).val();
            var id = $('#user_identity_id').val();

            $.ajax({
                url: url.api_identity_authentication_change,
                dataType: 'json',
                data: {'column': 'country', 'value': value, 'id': id},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (response) {
                    if ( response.status == "success" )
                    {
                        //alert('update country success');
                    }
                }
            });
        });

        $('#handphone_no').blur(function(){
            var value = $(this).val();
            var id = $('#user_identity_id').val();

            $.ajax({
                url: url.api_identity_authentication_change,
                dataType: 'json',
                data: {'column': 'handphone_no', 'value': value, 'id': id},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (response) {
                    if ( response.status == "success" )
                    {
//                      alert('update handphone_no success');
                    }
                }
            });
        });

        $('input[name=id_image]').change(function(){
            var value = $(this).val();
            var id = $('#user_identity_id').val();

            var image = $(this)[0].files[0];
            var formData = new FormData();
            formData.append('id_image', image);
            formData.append('column', 'id_image');
            formData.append('id', id);
	        $('.frontCardDefaultImage').hide();
	        $('.frontCardImage').show();

            $.ajax({
                url: url.api_identity_authentication_change,
                dataType: 'json',
                data: formData,
                method: 'POST',
                processData: false,
                contentType: false,
                //enctype: 'multipart/form-data',
                error: function () {
                    toastr.error('Something went wrong while uploading your image, please try again alter');
                },
                success: function (response) {
                    if ( response.status == "success" )
                    {
                        window.location.reload();
                        //alert('update image success');
                    }
                }
            });
        });

        $('input[name=hold_id_image]').change(function(){
            var value = $(this).val();
            var id = $('#user_identity_id').val();

            var image = $(this)[0].files[0];
            var formData = new FormData();
            formData.append('hold_id_image', image);
            formData.append('column', 'hold_id_image');
            formData.append('id', id);
	        $('.backCardDefaultImage').hide();
	        $('.backCardImage').show();
            $.ajax({
                url: url.api_identity_authentication_change,
                dataType: 'json',
                data: formData,
                method: 'POST',
                processData: false,
                contentType: false,
                //enctype: 'multipart/form-data',
                error: function () {
                    toastr.error('Something went wrong while uploading your image, please try again alter');
                },
                success: function (response) {
                    if ( response.status == "success" )
                    {
                        window.location.reload();
                        //alert('update image success');
                    }
                }
            });
        });
    }
);

function validate(){
    var languages = $('html').attr('lang');
    if(languages == 'en') {
        errRealName = 'Please input real name';
        errIdentityCardNo = 'Please input identity card number';
        errIdentityCardLength = 'Identity card number must 18 numbers';
        errGender ='Please select gender';
        errBirthDate = 'Please select birth date';
        errAddress = 'Please input address';
        errHandphone = 'Please input handphone number';
        errBackIDCard = 'Please upload back of your ID card';
        errFrontIDCard = 'Please upload front of your ID card';
        err = 'ERROR';
    }else{
        errRealName = '请输入真实姓名';
        errIdentityCardNo = '请输入身份证号码';
        errIdentityCardLength = '身份证号码应该18个号码';
        errGender ='请选择性别';
        errBirthDate = '请选择出生日期';
        errAddress = '请输入地址';
        errHandphone = '请输入手机号码';
        errBackIDCard = '请上传身份证背面图';
        errFrontIDCard = '请上传身份证正面图';
        err = '错误';
    }

    var errors = [];
    if ($('input[name="real_name"]').val() == '') {
        errors.push(errRealName);
    }
    if ($('input[name="id_card_no"]').val() == '') {
        errors.push(errIdentityCardNo);
    }

    var id_card_no = $('input[name="id_card_no"]').val();
    if (!($('input[name="id_card_no"]').val().length == '18' && !isNaN(id_card_no))) {
        errors.push(errIdentityCardLength);
    }
    if (!$('input[name="gender"]').is(':checked')) {
        errors.push(errGender);
    }
    if ($('input[name="date_of_birth"]').val() == '') {
        errors.push(errBirthDate);
    }
    if ($('input[name="address"]').val() == '') {
        errors.push(errAddress);
    }
    if ($('input[name="handphone_no"]').val() == '') {
        errors.push(errHandphone);
    }
    if ( typeof $('.img_front_id img').attr('src') === 'undefined') {
        errors.push(errFrontIDCard);
    }
    if ( typeof $('.img_back_id img').attr('src') === 'undefined') {
        errors.push(errBackIDCard);
    }

    if (errors.length) {
        toastr.clear();
        showError(errors);
        App.scrollTo($('.page-title'));
        return false;
    } else {
        return true;
    }
}

var showError = function (val) {
    if (val instanceof Array) {
        $(val).each(function (index, value) {
            showError(value);
        });
    } else {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "1000",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        toastr["error"](val, err);
    }
};
