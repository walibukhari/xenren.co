jQuery(
    function ($) {
        $("a[name='job-position']").click(function(){
            var id = $(this).data('id');

            $( "a[name='job-position']" ).each(function( index ) {
                var itemId = $( this ).data('id');
                if( itemId == id )
                {
                    $(this).parent().addClass('active');
                    $("#job_position_id").val(itemId)
                }
                else
                {
                    $(this).parent().removeClass('active');
                }
            });
        });

        var i = 0;
        $(document.body)
            .on("click", "#btn-add-new-file", function(){
                $('.file-attachments').append(
                    $('<input/>').attr('type', 'file').attr('name', 'file_upload[]').attr('id', 'fileupload_' + i).attr('style', 'display:none')
                );
                $('#fileupload_' + i).trigger('click');
            });

        $(document.body)
            .on("change", "input[id^='fileupload_']", function(){
                var file = $('#fileupload_' + i)[0].files[0];
                var fileName = "" + $('#fileupload_' + i).val();
                var fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
                var itemId = i;
                var _this = this;

                fileLimit = 1024 * 1024; //1 Mb
                if( this.files[0].size > fileLimit )
                {
                    //reset the html file element
                    var $el = $("#fileupload_" + itemId );
                    $el.wrap('<form>').closest('form').get(0).reset();
                    $el.unwrap();

                    alertError(lang.image_file_too_big);
                    return false;
                }

                $.ajax({
                    url: url.getThumbnail,
                    dataType: 'json',
                    data: {'fileName': fileName, 'itemId': itemId},
                    method: 'POST',
                    beforeSend: function () {
                    },
                    error: function () {
                        alert(lang.unable_to_request);
                    },
                    success: function (response) {
                        if ( response.status == "OK" )
                        {
                            $('#btn-add-new-file').remove();
                            $('#attachments-sect').append(response.contents);
                            readURL(_this, itemId);
                        }
                    }
                });

                i++;
            });

        function readURL(input, id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.img-' + id).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        $('#btn-submit').click(function(){
            $('.idSpanPortfolioSubmit').hide();
            $('.lds-ring-portfolio').show();
        });
        $('#create-portfolio-form').makeAjaxForm({
            submitBtn: '#btn-submit',
            alertContainer: '#portfolio-alert-container',
            afterSuccessFunction: function (response, $el, $this) {
                if( response.status == 'success'){
                    $('.idSpanPortfolioSubmit').show();
                    $('.lds-ring-portfolio').hide();
                    $('#create-portfolio-form').resetForm();
                    $('.file-attachments').html('');
                    $('.upload-file').remove();

                    $("#portfolio-list").append(response.data.contents)
                    toastr.success('Portfolio Created Successfully');
                    setTimeout(() => {
                        window.location.reload();
                    },1500);
                    //reset dropzone
                    $('#file-sect').html("");
                    arrUploadFile = {};
                    myDropzone.removeAllFiles(true);

                }else{
                    console.log(response.data.error);
                    $('.idSpanPortfolioSubmit').show();
                    $('.lds-ring-portfolio').hide();
                    toastr.error(response.data.error)
                }

                App.unblockUI('#create-portfolio-form');
            }
        });

        $(document.body)
            .on("click", ".btn-delete-portfolio", function(){
                var id = $(this).data('id');

                $.ajax({
                    url: url.delete,
                    dataType: 'json',
                    data: {'id': id},
                    method: 'POST',
                    error: function () {
                        toastr.success(lang.unable_to_request);
                    },
                    success: function (response) {
                        if ( response.status == "success" )
                        {
                            $('#portfolio-sect-' + id ).hide();
                            toastr.success('Portfolio Deleted');
                            setTimeout(() => {
                                window.location.reload();
                            },1500);
                        }
                    }
                });
            });

    }
);
