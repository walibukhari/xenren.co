jQuery(
    function ($) {
        $('#myId').click(function(){
            $('.idSpanSaveChange').hide();
            $('.lds-ring').show();
        });
        $('#create-feedback-form').makeAjaxForm({
            submitBtn: '#btn-submit',
            alertContainer: '#alert-container',
            redirectTo: url.feedbackList,
            errorFunction: function(response, $el, $this) {
                if(response.responseJSON && response.responseJSON.errors)
                {
                    $('.idSpanSaveChange').show();
                    $('.lds-ring').hide();
                    let err = response.responseJSON.errors;
                    Object.keys(err).forEach(function (key) {
                        toastr.error(err[key][0]);
                    });
                }
            }
        });

        function readURL(input, id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.img-' + id).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input[id^='fileUploadImage-']").change(function(){
            var id = parseInt(this.id.replace("fileUploadImage-", ""));

            fileLimit = 1024 * 1024; //1 Mb
            if( this.files[0].size > fileLimit )
            {
                //reset the html file element
                var $el = $("#fileUploadImage-" + id );
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();

                alertError(lang.image_file_too_big);
                return false;
            }

            if(this.files[0].type != 'image/png' && this.files[0].type != 'image/jpg' && this.files[0].type != 'image/gif' && this.files[0].type != 'image/jpeg' )
            {
                alert(lang.invalid_file_type_for_feedback);
                return false;
            }

            readURL(this, id);

            $("#div-img-" + id ).show();
            $(".lbl-fileupload-" + id).hide();

            //open another slot for upload
            $('#div-upload-' + (id + 1) ).show();
        });

        $("div[id^='div-img-']").click(function(e) {
            var id = parseInt(this.id.replace("div-img-", ""));
            $(".uploadImage-" + id).trigger('click');
        });
    }
);
