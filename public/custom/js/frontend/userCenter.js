jQuery(
    function ($) {

        $('.kv-fa').rating({
            defaultCaption: '{rating}',
            starCaptions: function (rating) {
                return rating;
            },
            starCaptionClasses: function(val) {
                return 'star-caption';
            },
            filledStar: '<i class="fa fa-star"></i>',
            emptyStar: '<i class="fa fa-star-o"></i>'
        });

        $('.ttl-evaluation').rating({
            defaultCaption: '{rating}',
            starCaptions: function (rating) {
                return ' ';
            },
            starCaptionClasses: function(val) {
                return 'star-caption';
            },
            filledStar: '<i class="fa fa-star"></i>',
            emptyStar: '<i class="fa fa-star-o"></i>'
        });

        $('.personal-rated-val').rating({
            defaultCaption: '{rating}',
            starCaptions: function (rating) {
                return ' ';
            },
            starCaptionClasses: function(val) {
                return 'star-caption';
            },
            filledStar: '<span style="color:#777777"><i class="fa fa-star"></i></span>',
            emptyStar: '<span style="color:#cccccc"><i class="fa fa-star"></i></span>'
        });

        $('.user-rated-val').rating({
            defaultCaption: '{rating}',
            starCaptions: function (rating) {
                return ' ';
            },
            starCaptionClasses: function(val) {
                return 'star-caption';
            },
            filledStar: '<span style="color:#f4b55e"><i class="fa fa-star"></i></span>',
            emptyStar: '<span style="color:#cccccc"><i class="fa fa-star"></i></span>'
        });

        ratingHistogram();

        $('.job-remark').tooltip();

    }
);

function ratingHistogram(){
    $('.bar span').hide();
    $('#bar-five').animate({
        width: '100%'}, 1000);
    $('#bar-four').animate({
        width: '35%'}, 1000);
    $('#bar-three').animate({
        width: '20%'}, 1000);
    $('#bar-two').animate({
        width: '15%'}, 1000);
    $('#bar-one').animate({
        width: '30%'}, 1000);

    setTimeout(function() {
        $('.bar span').fadeIn('slow');
    }, 1000);
}

// popup
$(".img-popup").click(function(){
    var img_attr = $(this).parent().find(".img-popup-full").attr("src");
    $("#img-popup-tmp img").attr("src", img_attr);
    $("#img-popup-tmp").modal("show");
});
// popup
$(".img-popup-thumb").click(function(){
    var img_attr = $(this).attr("src");
    $("#img-popup-tmp img").attr("src", img_attr);
    $("#img-popup-tmp").modal("show");
});