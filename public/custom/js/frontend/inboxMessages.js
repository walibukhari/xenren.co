jQuery(
    function ($) {
        $('.btn-delete').click(function(e){
            var url = $(this).data('url');
            var inboxId = $(this).data('inbox-id');

            $.ajax({
                url: url,
                dataType: 'json',
                data: {inboxId: inboxId},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if(result.status == 'success') {
                        alertSuccess(result.msg);
                        $('#inbox-' + inboxId).hide();
                    }else{
                        alertError(result.msg);
                    }
                }
            });
        });

        $('.btn-undelete').click(function(e){
            var url = $(this).data('url');
            var inboxId = $(this).data('inbox-id');

            $.ajax({
                url: url,
                dataType: 'json',
                data: {inboxId: inboxId},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if(result.status == 'success') {
                        alertSuccess(result.msg);
                        $('#inbox-' + inboxId).hide();
                    }else{
                        alertError(result.msg);
                    }
                }
            });
        });

        $('.acceptContactInfoRequest').click(function(){
            var receiverId = $(this).data('receiver-id');
            var inboxId = $(this).data('inbox-id');
            var url = $(this).data('url');
            $.ajax({
                url: url,
                dataType: 'json',
                data: {receiverId: receiverId, inboxId: inboxId},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if(result.status == 'success') {
                        alertSuccess(result.msg);
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }else{
                        alertError(result.msg);
                    }
                }
            });
        });

        $('.rejectContactInfoRequest').click(function(){
            var receiverId = $(this).data('receiver-id');
            var inboxId = $(this).data('inbox-id');
            var url = $(this).data('url');
            $.ajax({
                url: url,
                dataType: 'json',
                data: {receiverId: receiverId, inboxId: inboxId},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if(result.status == 'success') {
                        alertSuccess(result.msg);
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }else{
                        alertError(result.msg);
                    }
                }
            });
        });

        $('.rejectInvite').click(function(){
            var receiverId = $(this).data('receiver-id');
            var inboxId = $(this).data('inbox-id');
            var url = $(this).data('url');
            $.ajax({
                url: url,
                dataType: 'json',
                data: {receiverId: receiverId, inboxId: inboxId},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if(result.status == 'success') {
                        alertSuccess(result.msg);
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }else{
                        alertError(result.msg);
                    }
                }
            });
        });

        $('.acceptSendOffer').click(function(){
            var receiverId = $(this).data('receiver-id');
            var inboxId = $(this).data('inbox-id');
            var projectPayType = $(this).data('project-type');
            var url = $(this).data('url');
            $.ajax({
                url: url,
                dataType: 'json',
                data: {receiverId: receiverId, inboxId: inboxId , projectPayType: projectPayType},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if(result.status == 'success') {
                        alertSuccess(result.msg);
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }else{
                        alertError(result.msg);
                    }
                }
            });
        });

        $('.rejectSendOffer').click(function(){
            var receiverId = $(this).data('receiver-id');
            var inboxId = $(this).data('inbox-id');
            var url = $(this).data('url');
            $.ajax({
                url: url,
                dataType: 'json',
                data: {receiverId: receiverId, inboxId: inboxId},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if(result.status == 'success') {
                        alertSuccess(result.msg);
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }else{
                        alertError(result.msg);
                    }
                }
            });
        });

        $('.acceptJob').click(function(){
            var projectId = $(this).data('project-id');
            var inboxId = $(this).data('inbox-id');
            var url = $(this).data('url');
            $.ajax({
                url: url,
                dataType: 'json',
                data: {projectId: projectId, inboxId: inboxId},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if(result.status == 'success') {
                        alertSuccess(result.msg);
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }else{
                        alertError(result.msg);
                    }
                }
            });
        });

        $('.rejectJob').click(function(){
            var projectId = $(this).data('project-id');
            var inboxId = $(this).data('inbox-id');
            var url = $(this).data('url');
            $.ajax({
                url: url,
                dataType: 'json',
                data: {projectId: projectId, inboxId: inboxId},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if(result.status == 'success') {
                        alertSuccess(result.msg);
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }else{
                        alertError(result.msg);
                    }
                }
            });
        });

        $('.btnSendMessage').click(function(){
            var receiverId = $(this).data('receiver-id');
            var inboxId = $(this).data('inbox-id');
            var url = $(this).data('url');
            var message = $('.tbxMessage').val();
            $.ajax({
                url: url,
                dataType: 'json',
                data: {receiverId: receiverId, inboxId: inboxId, message: message},
                method: 'POST',
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (result) {
                    if(result.status == 'success') {
                        $('.tbxMessage').val('');
                        alertSuccess(result.msg);
                        fetchInboxMessages(inboxId);
                        // setTimeout(function(){
                        //     location.reload();
                        // },1000);
                    }else{
                        alertError(result.msg);
                    }
                }
            });
        });

        $('.tbxMessage').keypress(function(e) {
            if ((e.keyCode || e.which) == 13) {
                $('.btnSendMessage').click();
            }
        });


        function fetchInboxMessages(inboxId) {
            var form = new FormData();
            form.append("message", $('#message').val());

            var settings = {
                "url": "/privateChat?channel=private_chat&inobxId="+inboxId,
                "method": "GET",
                "data": form,
                "processData": false,
                "contentType": false,
                "mimeType": "multipart/form-data",
            }

            $.ajax(settings).done(function (response) {
                console.log(response);
                $('.inbox-message-section').html(response);
            });
        }

        //refresh the inbox message every 5 sec and retrieve new incoming message
        // window.setInterval(function(){
        //     var lastMessageId = $('#last-message-id').val();
        //     var inbox_id = inboxId;
        //     $.ajax({
        //         url: url.get_new_inbox_message,
        //         dataType: 'json',
        //         data: {
        //             inbox_id: inbox_id,
        //             last_message_id: lastMessageId
        //         },
        //         method: 'POST',
        //         //error: function () {
        //         //    alert(lang.unable_to_request);
        //         //},
        //         success: function (response) {
        //             if(response.status == 'success') {
        //                 if( response.result != '' )
        //                 {
        //                     $('.inbox-message-section').append(response.result);
        //                     $('#last-message-id').val(response.last_message_id);
        //                 }
        //             }
        //         }
        //     });
        // }, 5000);

        //scroll box
        $('.inbox-message-section').slimScroll({
            height: '460px',
            start: 'bottom'
        });
    }
);