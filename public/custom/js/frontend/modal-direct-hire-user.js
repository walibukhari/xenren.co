String.prototype.capitalize = function(){
    return this.charAt(0).toUpperCase() + this.slice(1);
}


$(document).ready(function(){
    $(".btn-direct-hire-user").on('click', function(){
        var receiver = $(this).data('user');
        $("#direct-hire-user-form").find(".modal-title").append(receiver.capitalize());
    });

    $('button[id^="btn-direct-hire-"]').click(function(){
        var receiverId = parseInt(this.id.replace("btn-direct-hire-", ""));
        $('#hire-receiverId').val(receiverId);
    });
});


