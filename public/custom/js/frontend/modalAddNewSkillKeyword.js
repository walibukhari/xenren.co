jQuery(
    function ($) {
        $('#btnInputNewSkillKeywordOk').click(function(){
            var tbxNewSkillKeywords = $('#tbxNewSkillKeywords').val();
            var res = tbxNewSkillKeywords.split(",");

            $('.div-keyword').show();
            $('.keyword-section').html('');
            $.each(res, function( index, value ) {
                var elements =  '<div class="col-md-3 control-label">' + value + '</div>' +
                    '<input name="lblkeyword[]" value="' + value + '" type="hidden">' +
                    '<div class="col-md-8" style="padding-bottom:10px;">' +
                    '<input class="form-control" name="tbxReferenceUrl[]" value="" type="text">' +
                    '</div>';

                $('.keyword-section').append(elements);
            });
        });

        //reset
        $('#modelAddNewSkillKeyword').on('hidden.bs.modal', function () {
            $('#tbxNewSkillKeywords').val('');
            $('.div-keyword').hide();
        })

        //submit the keyword
        $('#add-new-skill-keyword-form').makeAjaxForm({
            inModal: true,
            closeModal: true,
            submitBtn: '#btn-submit-new-skill-keyword',
            alertContainer : '#add-new-skill-keyword-errors',
            afterSuccessFunction: function (response, $el, $this) {
                if( response.status == 'success'){
                    App.alert({
                        type: 'success',
                        icon: 'check',
                        message: response.msg,
                        place: 'append',
                        container: '#body-alert-container',
                        reset: true,
                        focus: true
                    });

                    $('#modelAddNewSkillKeyword').modal('toggle');
                }else{
                    App.alert({
                        type: 'danger',
                        icon: 'warning',
                        message: response.msg,
                        container: '#add-new-skill-keyword-errors',
                        reset: true,
                        focus: true
                    });
                }

                App.unblockUI('#add-new-skill-keyword-form');
            }
        });

    }
);
