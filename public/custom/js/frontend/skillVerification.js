jQuery(
	function ($) {
		$('#btnSearch').click(function(){
			
			data = window.searchSkills;
			var result = [];
			$.each(data, function(i, e) {
				if ($.inArray(e, result) == -1) result.push(e);
			});
			result = result.join();
			result = result.replace(' ', '');
			var keyword = result;
			if( keyword == "")
			{
				alertError(lang.search_keyword_can_not_empty);
				return false;
			}

			var base_url = '/'+window.session+'/skillVerification';
			var redirectUrl = '';
			redirectUrl = base_url + '?keyword=' + keyword;
			window.location.href = redirectUrl;
		});
		
		$('button[id^="btn-join-discussion-"]').click(function(){
			var redirect = $(this).data('redirect');
			window.location.href = redirect;
		});
	}
);
