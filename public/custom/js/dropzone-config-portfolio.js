// Disable the auto init. So we can modify settings first. We will manually initialize it later.
Dropzone.autoDiscover = false;

// Manually init dropzone on our element.
var myDropzone = new Dropzone("#portfolio-dropzone", {
    url: '/uploadFile',
    paramName: "file",      // The name that will be used to transfer the file
    maxFilesize: 5,         // MB
    maxFiles: 3,
    parallelUploads: 3,     //limits number of files processed to reduce stress on server
    addRemoveLinks: false,
    method: 'post',
    filesizeBase: 1024,
    dictRemoveFile: lang.remove,
    dictFileTooBig: lang.image_is_bigger_than_5mb,
    dictMaxFilesExceeded: lang.you_can_not_upload_any_more_files,
    dictDefaultMessage: lang.drop_files_here_to_upload + '<div class="dz-icon"><img src="\\images\\upload.png" width="40px" height="40px"/></div>',
    dictCancelUpload: lang.cancel_upload,
    dictInvalidFileType: lang.invalid_file_type_for_post_project,
    acceptedFiles:'image/*, application/pdf, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/msword, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, audio/mpeg3, audio/x-mpeg-3, video/mpeg, video/x-mpeg, audio/mpeg, video/mp4, application/mp4',
    init: function() {
        this.on("success", function(file, response) {
            // do stuff here.
            //alertSuccess("Image Upload Successful");
            $('<input>', {
                'type' : 'hidden',
                'name' : 'hidUploadedFile[]',
                'value' : response.filename
            }).appendTo('#file-sect');

            addAddFileSymbol();

        });

        this.on("removedfile", function(file) {
            $('.add-file-symbol').remove();

            $.ajax({
                type: 'POST',
                url: 'uploadFile/delete',
                data: {id: file.name, _token: $('#csrf-token').val()},
                dataType: 'html',
                success: function(data){
                    var rep = JSON.parse(data);
                    if(rep.code == 200)
                    {
                        //alertSuccess("Remove Image Successful");
                    }

                }
            });

            addAddFileSymbol();
        });

        this.on("addedfile", function(file) {
            $('.add-file-symbol').remove();

            // Create the remove button
            var removeButton = Dropzone.createElement("<a href='javascript:;' class='btn red btn-sm btn-block'>" + lang.remove + "</a>");

            // Capture the Dropzone instance as closure.
            var _this = this;

            // Listen to the click event
            removeButton.addEventListener("click", function(e) {
                // Make sure the button click doesn't submit the form:
                e.preventDefault();
                e.stopPropagation();

                // Remove the file preview.
                _this.removeFile(file);
                // If you want to the delete the file on the server as well,
                // you can do the AJAX request here.
            });

            // Add the button to the file preview element.
            file.previewElement.appendChild(removeButton);
        });

    },
    sending: function(file, xhr, formData) {
        //formData.append("filesize", file.size);
        formData.append("_token", $('[name=_token]').val());    // Laravel expect the token post value to be named _token by default
        formData.append("type", fileUploadFeatureType);
    },
    accept: function(file, done) {
        getFileTypeIcon(file.type);
        done();
    },
});

$.each(arrUploadFile, function( index, value ) {
    var mockFile = {name: value.name, size: value.size, type: value.type };
    myDropzone.emit("addedfile", mockFile);
    myDropzone.createThumbnailFromUrl(mockFile, '/uploads/temp/' + value.name);
    myDropzone.emit("complete", mockFile);
    getFileTypeIcon(value.type);

    $('<input>', {
        'type' : 'hidden',
        'name' : 'hidUploadedFile[]',
        'value' : value.name
    }).appendTo('#file-sect');
});

function getFileTypeIcon($fileType)
{
    var item = $('.dropzone .dz-preview.dz-file-preview:last');
    var thumbnail = $('.dropzone .dz-preview.dz-file-preview .dz-image:last');

    var fileTypeIcon = "";
    switch ($fileType) {
        case 'application/pdf':
            fileTypeIcon = "images/pdf.png";
            break;
        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
        case 'application/msword':
            fileTypeIcon = "images/doc.png";
            break;
        case 'application/vnd.ms-excel':
        case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
            fileTypeIcon = "images/xls.png";
            break;
        case 'audio/mpeg3':
        case 'audio/x-mpeg-3':
        case 'video/mpeg':
        case 'video/x-mpeg':
        case 'audio/mpeg':
            fileTypeIcon = "images/mp3.png";
            break;
        case 'video/mp4':
        case 'application/mp4':
            fileTypeIcon = "images/mp4.png";
            break;
    }

    if( fileTypeIcon != "" )
    {
        item.removeClass('dz-file-preview').addClass('dz-image-preview');
        var imageIcon = '<div class="file-type-icon"><img data-dz-thumbnail="" src="' + fileTypeIcon + '" width="90px"></div>';
        thumbnail.html(imageIcon);
    }
}

function addAddFileSymbol()
{
    $('.dz-preview.dz-error').hide();

    var count = $('.dz-preview.dz-success').length;
    if( count == 0 || count == 3 )
    {
        $('.add-file-symbol').hide();

        if( count == 0 )
        {
            $('.dz-default.dz-message').show();
        }
    }
    else
    {
        var countAddFileSymbol = $('.add-file-symbol').length;
        if( countAddFileSymbol == 0 )
        {
            var addFileSymbol = '<div class="add-file-symbol dz-preview" style="cursor:pointer;display:none">' +
                '<img class="fa fa-plus" src="/images/addmoreimage.png" style="height: 120px;cursor: pointer;">' +
                '</div>';
            $('#portfolio-dropzone').append(addFileSymbol);

            $('.add-file-symbol').show();

            $('.dz-default.dz-message').hide();
        }
    }

    $('.add-file-symbol').click(function() {
        $('#portfolio-dropzone').trigger('click');
    });

}
