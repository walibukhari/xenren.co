revapi = jQuery('.rev-interactive').revolution({
    sliderType: "standard",
    sliderLayout: "fullscreen",
    dottedOverlay: "none",
    delay: 9000,
    responsiveLevels: [1240, 1024, 778, 480],
    visibilityLevels: [1240, 1024, 778, 480],
    gridwidth: [1240, 1024, 778, 480],
    gridheight: [600, 600, 500, 400],
    lazyType: "smart",
    shadow: 0,
    spinner: "off",
    stopLoop: "off",
    autoHeight: "off",
    fullScreenAutoWidth: "off",
    fullScreenAlignForce: "off",
    fullScreenOffsetContainer: "",
    fullScreenOffset: "",
    hideSliderAtLimit: 0,
    hideCaptionAtLimit: 0,
    hideAllCaptionAtLilmit: 0,
    debugMode: false,
    parallax: {
        type: "on",
        levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85],
        origo: "enterpoint",
        speed: 400,
        bgparallax: "on",
        disable_onmobile: "off"
    },
    fallbacks: {
        simplifyAll: "off",
        nextSlideOnWindowFocus: "off",
        disableFocusListener: false,
    }
});


particlesJS("particles-js", {
    "particles": {
        "number": {
            "value":223, "density": {
                "enable": true, "value_area": 800
            }
        }
        , "color": {
            "value": "#ffffff"
        }
        , "shape": {
            "type":"circle", "stroke": {
                "width": 0, "color": "#000000"
            }
            , "polygon": {
                "nb_sides": 5
            }
            , "image": {
                "src": "img/github.svg", "width": 100, "height": 100
            }
        }
        , "opacity": {
            "value":1, "random":true, "anim": {
                "enable": true, "speed": 1, "opacity_min": 0, "sync": false
            }
        }
        , "size": {
            "value":2, "random":true, "anim": {
                "enable": false, "speed": 4, "size_min": 0.3, "sync": false
            }
        }
        , "line_linked": {
            "enable": false, "distance": 150, "color": "#ffffff", "opacity": 0.4, "width": 1
        }
        , "move": {
            "enable":true, "speed":2.5, "direction":"top", "random":true, "straight":false, "out_mode":"out", "bounce":false, "attract": {
                "enable": false, "rotateX": 600, "rotateY": 600
            }
        }
    }
    , "interactivity": {
        "detect_on":"canvas", "events": {
            "onhover": {
                "enable": true, "mode": "bubble"
            }
            , "onclick": {
                "enable": true, "mode": "repulse"
            }
            , "resize":true
        }
        , "modes": {
            "grab": {
                "distance":400, "line_linked": {
                    "opacity": 1
                }
            }
            , "bubble": {
                "distance": 250, "size": 0, "duration": 2, "opacity": 0, "speed": 3
            }
            , "repulse": {
                "distance": 400, "duration": 0.4
            }
            , "push": {
                "particles_nb": 4
            }
            , "remove": {
                "particles_nb": 2
            }
        }
    }
    , "retina_detect":true
}

);