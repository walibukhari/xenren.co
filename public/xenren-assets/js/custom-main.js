$(document).ready(function(){
    var xenren_path = $('.xenren_path').val();
    $('.explore').hover(
        function() {
            $('.explore-span').addClass('explore-span-white');
        }, function() {
            $('.explore-span').removeClass('explore-span-white');
        }
    );
    $('.btn-style-3').hover(
        function() {
            $(this).find('i').addClass('i-hover');
        }, function() {
            $(this).find('i').removeClass('i-hover');
        }
    );
   /* $('.team-member').hover(
        function() {
            $(this).find('.member-info').css('background','#5ec329');
            $(this).find('.member-title').css('color','#2f4a5b');
        }, function() {
            $(this).find('.member-info').css('background','#2f4a5b');
            $(this).find('.member-title').css('color','#5ec329');
        }
    );*/
    $('.services-secure').hover(
        function() {
            $(this).find('.content').show();
            var src = $(this).find('img').data('src');
            $(this).find('img').attr('src',xenren_path+'images/icon/'+src+'-hover.png');
        }, function() {
            $(this).find('.content').hide();
            var src = $(this).find('img').data('src');
            $(this).find('img').attr('src',xenren_path+'images/icon/'+src+'.png');
        }
    );
});