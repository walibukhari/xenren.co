jQuery(
    function ($) {
        //template
        var template = '<div class="row">' +
                            '<label class="col-md-12 p-t-40">' +
                                lang.new_skill_keywords +
                            '</label>' +
                            '<div class="col-md-12">' +
                                '<input class="form-control" name="tbxSkillKeyword[]" value="" type="text">' +
                            '</div>' +
                        '</div>' +
                        '<div class="row">' +
                            '<label class="col-md-12 p-t-20">' +
                                lang.please_use_wiki_or_baidu_support_your_keyword +
                            '</label>' +
                            '<div class="col-md-12">' +
                                '<input class="form-control" name="tbxReferenceUrl[]" value="" type="text">' +
                            '</div>' +
                        '</div>';


        $('#btnAddMore').click(function()
        {
            var errorExist = false;
            $( "input[ name = 'tbxSkillKeyword[]']" ).each(function( index )
            {
                console.log( index + ": " + $( this ).val() );
                if(  $( this ).val() == "" )
                {
                    alert(lang.one_of_skill_input_empty);
                    errorExist = true;
                }
            });

            if( !errorExist)
            {
                var count = parseInt( $('#hidInputCount').val() );
                if( count == 10 )
                {
                    alert(lang.maximum_ten_skill_keywords_allow_to_submit);
                    return false;
                }
                else
                {
                    $('.add-more-section').append(template);
                    $('#hidInputCount').val(count + 1);
                }
            }

        });

        //submit the keyword
        $('#add-new-skill-keyword-form').makeAjaxForm({
          
            submitBtn: '#btn-submit-new-skill-keyword',
            alertContainer : '#add-new-skill-keyword-errors',
            clearForm: true,

            afterSuccessFunction: function (response, $el, $this) {
                if( response.status == 'success'){
                    App.alert({
                        type: 'success',
                        icon: 'check',
                        message: response.msg,
                        place: 'append',
                        container: '#body-alert-container',
                        reset: true,
                        focus: true
                    });
                    setTimeout(function(){
                        $('#modelAddNewSkillKeyword').modal('toggle');
                    }, 3000);
                }else{
                    App.alert({
                        type: 'danger',
                        icon: 'warning',
                        message: response.msg,
                        container: '#add-new-skill-keyword-errors',
                        reset: true,
                        focus: true
                    });
                }

                App.unblockUI('#add-new-skill-keyword-form');
            }
        });
    }
);


jQuery(
	function ($) {
		$('#add-project-form').makeAjaxForm({
			submitBtn: '#project-submit-btn',
			alertContainer: '#project-alert-container',
			beforeFunction: validate,
			afterSuccessFunction: function (response, $el, $this) {
				if( response.status == 'success')
				{
					console.log(response.data.country_id);
					var url = '/'+window.session.value+'/Coworkers?skill_id_list=' + response.data.skill_id_list + '&success_post=true&country_id='+response.data.country_id;
					window.location.href = url;
				}
				else{
					if(response.message == 'personal_info'){
						console.log($('#pay_type').val());
						$('#modalPersonalInfo').modal('show');
						response.data.avatar ? $('.avatar-g').removeClass('hide') : $('.avatar-r').removeClass('hide') ;
						response.data.clear_budget ? $('.clear_budget-g').removeClass('hide') : $('.clear_budget-r').removeClass('hide') ;
						response.data.verify_account ? $('.verify_account-g').removeClass('hide') : $('.verify_account-r').removeClass('hide') ;
						response.data.skill_info ? $('.skill_info-g').removeClass('hide') : $('.skill_info-r').removeClass('hide') ;
					}
					$(".ajaxloader").hide();
				}
			}
		});

		$('#modal_post_project').click(function (){
			$('#modalPersonalInfo').modal('hide');
			$('#continue_posting').val('1');
			$('#project-submit-btn').trigger('click');
		});

		$('.modal_draft_project').click(function (){
			$('#modalPersonalInfo').modal('hide');
			$('#continue_posting').val('1');
			$('#is_draft').val('1');
			$('#project-submit-btn').trigger('click');
		});

		//$("#tbxDescription").keyup(function () {
		//    updateCountTbxDescription();
		//});
		//$("#tbxDescription").keypress(function () {
		//    updateCountTbxDescription();
		//});
		//$("#tbxDescription").keydown(function () {
		//    updateCountTbxDescription();
		//});

		var language = $('html').attr('lang');
		var summernoteLanguage = '';
		if( language == "cn" )
		{
			summernoteLanguage = "zh-CN";
		}
		else
		{
			summernoteLanguage = "en-US";
		}

		$('#snDescription').summernote({
			placeholder: lang.example_job_description,
			lang: summernoteLanguage,
			height: 300,
			toolbar:[
				['style',['bold','fontsize']],
				['insert', ['link', 'picture','codeview']],
			],
			callbacks: {
				onChange: function(contents, $editable) {
					var stripHtmlTag = contents.replace(/<(?:.|\n)*?>/gm, '');

					$('#tbxDescription').val(contents);
					updateCountTbxDescription();

					if(stripHtmlTag.length > 0 )
					{
						if($('.note-editor').hasClass('error-border') && $('#tbxDescription').val() != '' ){
							$('.note-editor').removeClass('error-border');
							$("#description-require").hide();
						}
					}
					else
					{
						$('.note-editor').addClass('error-border');
						$("#description-require").show();
					}
				},
				onImageUpload: function(files) {
					for (var i = 0; i < files.length; i++) {
						send(files[i]);
					}
				}
			}
		});

		function send(file)
		{
			if (file.type.includes('image'))
			{
				var name = file.name.split(".");
				name = name[0];
				var data = new FormData();
				data.append('file', file);
				$.ajax({
					url: url.uploadDescriptionImage,
					type: 'POST',
					contentType: false,
					cache: false,
					processData: false,
					dataType: 'JSON',
					data: data,
					success: function (resp) {
						if(resp.status == "success")
						{
							$('#snDescription').summernote('insertImage', resp.data['url']);
						}
						else
						{
							alert(resp.msg);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						alert(textStatus + " " + errorThrown);
					}
				});
			}
		}

		$('ul.subcategory li').click(function(e)
		{
			var categoryId  = parseInt($(this).attr("catId"));
			var categoryName = $(this).attr("catName");
			$('#category_id').val(categoryId);
			$('#category_name').html(categoryName);
			$('#category_full_name').val(categoryName.trim());

			//allow category dropdown menu to close now
			$('#category_menu_allow_close').val(1);

			if($('#slcCategory').hasClass('error-border')){
				$('#slcCategory').removeClass('error-border');
				$("#category-require").hide();
			}
		});

		//set the label with ajax postback and refresh problem
		var categoryFullname = $('#category_full_name').val();
		if(categoryFullname == ""){
			$('#category_name').html(lang.please_select);
		}else{
			$('#category_name').html(categoryFullname);
		}

		//reset menu and not allow to close until level 2 category being click
		$('.category.dropdown').click(function(e){
			$('#category_menu_allow_close').val(0);
		});

		$('li.category').click(function(e)
		{
			var category_menu_allow_close = $('#category_menu_allow_close').val();
			if(category_menu_allow_close == 0){
				e.stopPropagation();
			}else{

			}
		});

		$('ul.project li').click(function(e)
		{
			var projectId  = parseInt($(this).attr("projId"));
			var projectName = $(this).attr("projName");
			$('#modal_pre_project_id').val(projectId);
			$('#modal_pre_project_name').html(projectName);
			$('#modal_project_id').html(projectId);
			$('#modal_project_name').html(projectName);
		});

		$('ul.country li').click(function(){
			var countryId = parseInt($(this).data('id'));
			var countryName = $(this).data('name');
			$('#country_name').html(countryName);
			$('#country_id').val(countryId);
		});

		$('ul.language li').click(function(){
			var languageId = parseInt($(this).data('id'));
			var languageName = $(this).data('name');
			$('#language_name').html(languageName);
			$('#language_id').val(languageId);
		});

		//Question
		$('.btn-add-more-question').click(function(){

			//only allow 8 question field
			$totalQuestionBox = $('div[name*="div-question-box"]').length;
			if( $totalQuestionBox == 8 ){
				return true;
			}

			//random int
			var item = Math.floor(1000 + Math.random() * 9000);

			var result = '<div class="col-md-12" style="padding:0px;"><div class="form-group div-question-top" id="question-' + item + '" name="div-question-box">' +
				'<div class="col-md-8 col-xs-10 div-question">' +
				'<textarea class="form-control" placeholder="" maxlength="256" rows="3" id="tbxQuestion-' + item + '" name="question[]" cols="50"></textarea>'+
				'<span class="pull-right">' +
				'<span id="noOfCharQuestion-' + item + '">256</span> Characters Left </span>' +
				'</br>' +
				'<span id="question-require-' + item + '" class="error-message" style="display:none;"><i class="fa fa-exclamation-circle"></i> ' + lang.field_required+ '</span>' +
				'</div>' +
				'<div class="col-md-4 col-xs-2 div-close-question">' +
				'<button id="btn-close-question-' + item + '" type="button" class="close pull-left">' +
				'&times;' +
				'</button>' +
				'</div>' +
				'</div></div>';
			$('.section-question').append(result);
		});
		$("#tbxName").blur(function(){
			if($(this).hasClass('error-border') && $(this).val() != '' ){
				$(this).removeClass('error-border');
				$("#project-name-require").hide();
			}
		});

		//$("#tbxDescription").blur(function(){
		//    if($(this).hasClass('error-border') && $(this).val() != '' ){
		//        $(this).removeClass('error-border');
		//        $("#description-require").hide();
		//    }
		//});

		$(".note-editor").blur(function(){
			if($(this).hasClass('error-border') && $('#tbxDescription').val() != '' ){
				$(this).removeClass('error-border');
				$("#description-require").hide();
			}
		});

		$("#reference_price").blur(function(){
			if($(this).hasClass('error-border') && $(this).val() != '' ){
				$(this).removeClass('error-border');
				$("#fixed-price-require").hide();
			}
		});
		$("input[name='time_commitment']").click(function(){
			if( $("input[name='time_commitment']").parent('.pay-type-custom').find('.pay-type-label').hasClass('error-border')){
				$("input[name='time_commitment']").parent('.pay-type-custom').find('.pay-type-label').removeClass('error-border');
				$("#time-commitment-require").hide();
			}
		});

		$("input[name='time_duration']").click(function(){
			if( $("input[name='time_duration']").parent('.pay-type-custom').find('.pay-type-label').hasClass('error-border')){
				$("input[name='time_duration']").parent('.pay-type-custom').find('.pay-type-label').removeClass('error-border');
				$("#time-commitment-require").hide();
			}
		});

		$("div[name='rdFreelanceType']").click(function(){
			$("div[name='rdFreelanceType']").attr('style', '');
			$("#freelance-type-require").hide();
		});

		$(document.body)
			.on("click", "button[id ^='btn-close-question-']", function(){
				var questionId = parseInt(this.id.replace("btn-close-question-", ""));
				$('#question-' + questionId).remove();
			});

		$(document.body)
			.on("keypress", "textarea[id ^='tbxQuestion-']", updateCountTbxQuestion)
			.on("keyup", "textarea[id ^='tbxQuestion-']", updateCountTbxQuestion)
			.on("keydown", "textarea[id ^='tbxQuestion-']", updateCountTbxQuestion)
			.on("blur", "textarea[id ^='tbxQuestion-']", function(e){
				if($(this).hasClass('error-border') && $(this).val() != ''){
					$(this).removeClass('error-border');
					$(this).parent().children('.error-message').hide();
				}
			});

		$('input[id^="pay-type-"]').click(function() {

			if($('#pay-type-1').is(':checked') ){

				$('.div-price').show();
				$('#label-div-price-hour').show();
				;
				$('#label-div-price-fix').hide();

			}else if( $('#pay-type-2').is(':checked') ){

				$('.div-price').show();

				$('#label-div-price-hour').hide();
				$('#label-div-price-fix').show();

			}else if( $('#pay-type-3').is(':checked') ){

				$('.div-price').hide();
			}

			$('#reference_price').val('');

			if($("input[name='pay_type']").parent('.pay-type-custom').find('.pay-type-label').hasClass("error-border")){
				$("input[name='pay_type']").parent('.pay-type-custom').find('.pay-type-label').removeClass("error-border");
				$("#pay-require").hide();
			}
		});

		var i = 0;
		$(document.body)
			.on("click", ".attachment-no-file", function(){
				var count = $('.upload-file').length;
				if( count == 8 )
				{
					alertError(lang.max_upload_eight_documents);
					return true;
				}

				$('.file-attachments').append(
					$('<input/>').attr('type', 'file').attr('name', 'fileUpload[]').attr('id', 'fileupload_' + i).attr('style', 'display:none')
				);
				$('#fileupload_' + i).trigger('click');
			});

		$(document.body)
			.on("change", "input[id^='fileupload_']", function(){
				var file = $('#fileupload_' + i)[0].files[0];
				var fileName = "" + $('#fileupload_' + i).val();
				var fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
				var itemId = i;

				$.ajax({
					url: url.getInputFile,
					dataType: 'json',
					data: {'fileName': fileName, 'itemId': itemId},
					method: 'POST',
					beforeSend: function () {
					},
					error: function () {
						alert(lang.unable_to_request);
					},
					success: function (response) {
						if ( response.status == "OK" )
						{
							$('#btn-add-new-file').remove();
							$('#attachments-sect').append(response.contents);
						}
					}
				});

				i++;
			});

		$(document.body)
			.on("click", "span[id^='btn-delete-']", function(){
				var itemId = parseInt(this.id.replace("btn-delete-", ""));
				$('#fu-item-' + itemId).remove();
				$('#fileupload_' + itemId).remove();
			});

		$(document.body)
			.on("click", "span[id^='btn-remove-existing-']", function(){
				var projectFileId = parseInt(this.id.replace("btn-remove-existing-", ""));
				$('#fu-item-' + projectFileId).remove();
				$('#fileupload_' + projectFileId).remove();

				var input = '<input type="hidden" name="delete_file[]" value="' + projectFileId + '" />';
				$('#add-project-form').append(input);
			});
	}
);

function updateCountTbxDescription (e) {
	var qText = $("#tbxDescription").val();

	if(qText.length <= 5000) {
		$("#noOfCharDescription").html(5000 - qText.length );
	} else {
		jQuery("#noOfCharDescription").html(0);
		$("#tbxDescription").val(qText.substring(0,5000));
	}
}

function updateCountTbxQuestion (e) {
	var questionId = parseInt(this.id.replace("tbxQuestion-", ""));

	var qText = $("#tbxQuestion-" + questionId ).val();
	console.log('in text change');
	console.log(qText);
	if(qText.length <= 256) {
		console.log(256 - qText.length);
		var len = 256 - qText.length;
		var html = `<span> ${len} </span>`;
		console.log(html);
		$("#noOfCharQuestion-" + questionId ).html(html);
	} else {
		jQuery("#noOfCharQuestion-" + questionId).html(0);
		$("#tbxQuestion-" + questionId).val(qText.substring(0,256));
	}
}

function checkPersonalInfo() {
	$.ajax({
		url: '/checkPersonalInfo',
		type: 'get',
		success: function (res){
			console.log(res);
		},
		error: function (err){
			console.log(err);
		}
	});
	return false;
}

function validate(){
	$(".ajaxloader").show();
	errName = lang.please_input_project_name;
	errCategoryId = lang.please_choose_category;
	errDescription = lang.please_input_description;
	errSkillId = lang.please_choose_skill;
	errPayType = lang.please_choose_pay_type;
	errReferencePrice = lang.please_input_reference_price;
	errTimeCommitment = lang.please_choose_commitment_time;
	errTimeDuration = lang.please_choose_time_duration;
	errFreelanceType = lang.please_choose_freelance_type;
	errQuestion = lang.please_input_question;
	err = g_lang.error;

	var errors = [];
	var scrollToError = "";
	$.each($('textarea[name="question[]"]'), function () {
		if ($(this).val() == '') {
			$(this).addClass('error-border');
			$(this).parent().children('.error-message').show();
			errors.push(errQuestion);
			scrollToError = $(this).name;
		}
	});
	// if ($('textarea[name="question[]"]').val() == '') {
	//     errors.push(errQuestion);
	//     $("textarea[name='question[]']").addClass('error-border');
	//     scrollToError = "question";
	//     $("textarea[name='question[]']").parent().children('.error-message').show();
	// }

	if (!$('input[name="freelance_type"]').is(':checked')) {
		errors.push(errFreelanceType);
		$("div[name='rdFreelanceType']").attr('style', 'border:1px solid red !important;');
		$("#freelance-type-require").show();
		scrollToError = "freelance_type";
	}

	if (!$('input[name="time_commitment"]').is(':checked')) {
		errors.push(errTimeCommitment);
		$("input[name='time_commitment']").parent('.pay-type-custom').find('.pay-type-label').addClass('error-border');
		$("#time-commitment-require").show();
		scrollToError = "time_commitment";
	}

	if (!$('input[name="time_duration"]').is(':checked')) {
		errors.push(errTimeDuration);
		$("input[name='time_duration']").parent('.pay-type-custom').find('.pay-type-label').addClass('error-border');
		$("#time-commitment-require").show();
		scrollToError = "time_duration";
	}

	if ($('input[name="reference_price"]').val() == '' &&
		(
			$('#pay-type-1').is(':checked') ||
			$('#pay-type-2').is(':checked')
		)
	) {
		errors.push(errReferencePrice);
		$("#reference_price").addClass('error-border');
		$("#fixed-price-require").show();
		scrollToError = "reference_price";
	}

	if (!$('input[name="pay_type"]').is(':checked')) {
		$("input[name='pay_type']").parent('.pay-type-custom').find('.pay-type-label').addClass('error-border');
		$("#pay-require").show();
		errors.push(errPayType);
		scrollToError = "pay_type";
	}

	if ($('input[name="skill_id_list"]').val() == 0) {
		errors.push(errSkillId);
		$("#magicsuggest").attr('style', 'border-color:red !important;');
		$('#magicsuggest').addClass('error-border');
		$("#skill-require").show();
		scrollToError = "skill";
	}

	//if ($('textarea[name="description"]').val() == '') {
	//    errors.push(errDescription);
	//    $("#tbxDescription").addClass('error-border');
	//    $("#description-require").show();
	//    scrollToError = "description";
	//}

	if ($('input[name="description"]').val() == '') {
		errors.push(errDescription);
		$(".note-editor.note-frame").addClass('error-border');
		$("#description-require").show();
		scrollToError = "description";
	}

	if ($('input[name="category_id"]').val() == 0) {
		errors.push(errCategoryId);
		$("#slcCategory").addClass('error-border');
		$("#category-require").show();
		scrollToError = "category";
	}

	if ($('input[name="name"]').val() == '') {
		errors.push(errName);
		$("#tbxName").addClass('error-border');
		$("#project-name-require").show();
		scrollToError = "name";
	}

	//reset the error
	$("#tbxName").attr('style', '');
	$("#slcCountry").attr('style', '');
	$("#slcLanguage").attr('style', '');
	$("#slcCategory").attr('style', '');
	$("#tbxDescription").attr('style', '');
	// $("#magicsuggest").attr('style', '');
	$("input[name='pay_type']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
	if ($('#pay-type-1').is(':checked')) {
		$("input[name='pay_type'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
	}
	else if ($('#pay-type-2').is(':checked')) {
		$("input[name='pay_type'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
	}
	else if ($('#pay-type-3').is(':checked')) {
		$("input[name='pay_type'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
	}

	$("#reference_price").attr('style', '');
	$("input[name='time_commitment']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
	if ($('#time-commitment-1').is(':checked')) {
		$("input[name='time_commitment'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
	}
	else if ($('#time-commitment-2').is(':checked')) {
		$("input[name='time_commitment'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
	}
	else if ($('#time-commitment-3').is(':checked')) {
		$("input[name='time_commitment'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
	}

	$("input[name='time_duration']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
	if ($('#time_duration-1').is(':checked')) {
		$("input[name='time_duration'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
	}
	else if ($('#time_duration-2').is(':checked')) {
		$("input[name='time_duration'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
	}
	else if ($('#time_duration-3').is(':checked')) {
		$("input[name='time_duration'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
	}else if ($('#time_duration-4').is(':checked')) {
		$("input[name='time_duration'][value='4']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
	}else if ($('#time_duration-5').is(':checked')) {
		$("input[name='time_duration'][value='5']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
	}

	// $("div[name='rdFreelanceType']").attr('style', '');
	$("textarea[name='question[]']").attr('style', '');

	switch (scrollToError) {
		case "name":
			App.scrollTo($('#tbxName'));
			break;
		case "category":
			App.scrollTo($('#slcCategory'));
			break;
		case "description":
			//App.scrollTo($('#tbxDescription'));
			App.scrollTo($('.note-editor'));
			break;
		case "skill":
			App.scrollTo($('#magicsuggest'));
			break;
		case "pay_type":
			App.scrollTo($("input[name='pay_type']"));
			break;
		case "reference_price":
			App.scrollTo($('#reference_price'));
			break;
		case "time_commitment":
			App.scrollTo($("input[name='time_commitment']"));
			break;
		case "time_duration":
			App.scrollTo($("input[name='time_duration']"));
			break;
		case "freelance_type":
			App.scrollTo($("div[name='rdFreelanceType']"));
			break;
		case "question":
			App.scrollTo($("textarea[name='question[]']"));
			break;

	}

	if (scrollToError == "") {
		return true;
	}
	else {
		$(".ajaxloader").hide();
		return false;
	}

}

var showError = function (val) {
	if (val instanceof Array) {
		$(val).each(function (index, value) {
			showError(value);
		});
	} else {
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"positionClass": "toast-top-right",
			"onclick": null,
			"showDuration": "1000",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}

		toastr["error"](val, err);
	}
};

(function ($) {
  $.extend($.summernote.lang, {
    'zh-CN': {
      font: {
        bold: '粗体',
        italic: '斜体',
        underline: '下划线',
        clear: '清除格式',
        height: '行高',
        name: '字体',
        strikethrough: '删除线',
        subscript: '下标',
        superscript: '上标',
        size: '字号'
      },
      image: {
        image: '图片',
        insert: '插入图片',
        resizeFull: '缩放至 100%',
        resizeHalf: '缩放至 50%',
        resizeQuarter: '缩放至 25%',
        floatLeft: '靠左浮动',
        floatRight: '靠右浮动',
        floatNone: '取消浮动',
        shapeRounded: '形状: 圆角',
        shapeCircle: '形状: 圆',
        shapeThumbnail: '形状: 缩略图',
        shapeNone: '形状: 无',
        dragImageHere: '将图片拖拽至此处',
        selectFromFiles: '从本地上传',
        maximumFileSize: '文件大小最大值',
        maximumFileSizeError: '文件大小超出最大值。',
        url: '图片地址',
        remove: '移除图片'
      },
      video: {
        video: '视频',
        videoLink: '视频链接',
        insert: '插入视频',
        url: '视频地址',
        providers: '(优酷, 腾讯, Instagram, DailyMotion, Youtube等)'
      },
      link: {
        link: '链接',
        insert: '插入链接',
        unlink: '去除链接',
        edit: '编辑链接',
        textToDisplay: '显示文本',
        url: '链接地址',
        openInNewWindow: '在新窗口打开'
      },
      table: {
        table: '表格'
      },
      hr: {
        insert: '水平线'
      },
      style: {
        style: '样式',
        p: '普通',
        blockquote: '引用',
        pre: '代码',
        h1: '标题 1',
        h2: '标题 2',
        h3: '标题 3',
        h4: '标题 4',
        h5: '标题 5',
        h6: '标题 6'
      },
      lists: {
        unordered: '无序列表',
        ordered: '有序列表'
      },
      options: {
        help: '帮助',
        fullscreen: '全屏',
        codeview: '源代码'
      },
      paragraph: {
        paragraph: '段落',
        outdent: '减少缩进',
        indent: '增加缩进',
        left: '左对齐',
        center: '居中对齐',
        right: '右对齐',
        justify: '两端对齐'
      },
      color: {
        recent: '最近使用',
        more: '更多',
        background: '背景',
        foreground: '前景',
        transparent: '透明',
        setTransparent: '透明',
        reset: '重置',
        resetToDefault: '默认'
      },
      shortcut: {
        shortcuts: '快捷键',
        close: '关闭',
        textFormatting: '文本格式',
        action: '动作',
        paragraphFormatting: '段落格式',
        documentStyle: '文档样式',
        extraKeys: '额外按键'
      },
      history: {
        undo: '撤销',
        redo: '重做'
      },
      help: {
        insertParagraph: '插入段落',
        undo: '撤销',
        redo: '重做',
        tab: '增加缩进',
        untab: '减少缩进',
        bold: '粗体',
        italic: '斜体',
        underline: '下划线',
        strikethrough: '删除线',
        removeFormat: '清除格式',
        justifyLeft: '左对齐',
        justifyCenter: '居中对齐',
        justifyRight: '右对齐',
        justifyFull: '两端对齐',
        insertUnorderedList: '无序列表',
        insertOrderedList: '有序列表',
        outdent: '减少缩进',
        indent: '增加缩进',
        formatPara: '设置选中内容样式为 普通',
        formatH1: '设置选中内容样式为 标题1',
        formatH2: '设置选中内容样式为 标题2',
        formatH3: '设置选中内容样式为 标题3',
        formatH4: '设置选中内容样式为 标题4',
        formatH5: '设置选中内容样式为 标题5',
        formatH6: '设置选中内容样式为 标题6',
        insertHorizontalRule: '插入水平线',
        'linkDialog.show': '显示链接对话框'
      }
    }
  });
})(jQuery);

/*! Summernote v0.8.7 | (c) 2013- Alan Hong and other contributors | MIT license */

!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):"object"==typeof module&&module.exports?module.exports=a(require("jquery")):a(window.jQuery)}(function($){"use strict";var isSupportAmd="function"==typeof define&&define.amd,isFontInstalled=function(a){var b="Comic Sans MS"===a?"Courier New":"Comic Sans MS",c=$("<div>").css({position:"absolute",left:"-9999px",top:"-9999px",fontSize:"200px"}).text("mmmmmmmmmwwwwwww").appendTo(document.body),d=c.css("fontFamily",b).width(),e=c.css("fontFamily",a+","+b).width();return c.remove(),d!==e},userAgent=navigator.userAgent,isMSIE=/MSIE|Trident/i.test(userAgent),browserVersion;if(isMSIE){var matches=/MSIE (\d+[.]\d+)/.exec(userAgent);matches&&(browserVersion=parseFloat(matches[1])),matches=/Trident\/.*rv:([0-9]{1,}[\.0-9]{0,})/.exec(userAgent),matches&&(browserVersion=parseFloat(matches[1]))}var isEdge=/Edge\/\d+/.test(userAgent),hasCodeMirror=!!window.CodeMirror;if(!hasCodeMirror&&isSupportAmd&&"undefined"!=typeof require)if(void 0!==require.resolve)try{require.resolve("codemirror"),hasCodeMirror=!0}catch(a){}else void 0!==eval("require").specified&&(hasCodeMirror=eval("require").specified("codemirror"));var isSupportTouch="ontouchstart"in window||navigator.MaxTouchPoints>0||navigator.msMaxTouchPoints>0,agent={isMac:navigator.appVersion.indexOf("Mac")>-1,isMSIE:isMSIE,isEdge:isEdge,isFF:!isEdge&&/firefox/i.test(userAgent),isPhantom:/PhantomJS/i.test(userAgent),isWebkit:!isEdge&&/webkit/i.test(userAgent),isChrome:!isEdge&&/chrome/i.test(userAgent),isSafari:!isEdge&&/safari/i.test(userAgent),browserVersion:browserVersion,jqueryVersion:parseFloat($.fn.jquery),isSupportAmd:isSupportAmd,isSupportTouch:isSupportTouch,hasCodeMirror:hasCodeMirror,isFontInstalled:isFontInstalled,isW3CRangeSupport:!!document.createRange},func=function(){var a=function(a){return function(b){return a===b}},b=function(a,b){return a===b},c=function(a){return function(b,c){return b[a]===c[a]}},d=function(){return!0},e=function(){return!1},f=function(a){return function(){return!a.apply(a,arguments)}},g=function(a,b){return function(c){return a(c)&&b(c)}},h=0;return{eq:a,eq2:b,peq2:c,ok:d,fail:e,self:function(a){return a},not:f,and:g,invoke:function(a,b){return function(){return a[b].apply(a,arguments)}},uniqueId:function(a){var b=++h+"";return a?a+b:b},rect2bnd:function(a){var b=$(document);return{top:a.top+b.scrollTop(),left:a.left+b.scrollLeft(),width:a.right-a.left,height:a.bottom-a.top}},invertObject:function(a){var b={};for(var c in a)a.hasOwnProperty(c)&&(b[a[c]]=c);return b},namespaceToCamel:function(a,b){return(b=b||"")+a.split(".").map(function(a){return a.substring(0,1).toUpperCase()+a.substring(1)}).join("")},debounce:function(a,b,c){var d;return function(){var e=this,f=arguments,g=function(){d=null,c||a.apply(e,f)},h=c&&!d;clearTimeout(d),d=setTimeout(g,b),h&&a.apply(e,f)}}}}(),list=function(){var a=function(a){return a[0]},b=function(a){return a[a.length-1]},c=function(a){return a.slice(0,a.length-1)},d=function(a){return a.slice(1)},e=function(a,b){for(var c=0,d=a.length;c<d;c++){var e=a[c];if(b(e))return e}},f=function(a,b){for(var c=0,d=a.length;c<d;c++)if(!b(a[c]))return!1;return!0},g=function(a,b){return $.inArray(b,a)},h=function(a,b){return-1!==g(a,b)},i=function(a,b){return b=b||func.self,a.reduce(function(a,c){return a+b(c)},0)},j=function(a){for(var b=[],c=-1,d=a.length;++c<d;)b[c]=a[c];return b},k=function(a){return!a||!a.length},l=function(c,e){return c.length?d(c).reduce(function(a,c){var d=b(a);return e(b(d),c)?d[d.length]=c:a[a.length]=[c],a},[[a(c)]]):[]},m=function(a){for(var b=[],c=0,d=a.length;c<d;c++)a[c]&&b.push(a[c]);return b},n=function(a){for(var b=[],c=0,d=a.length;c<d;c++)h(b,a[c])||b.push(a[c]);return b},o=function(a,b){var c=g(a,b);return-1===c?null:a[c+1]};return{head:a,last:b,initial:c,tail:d,prev:function(a,b){var c=g(a,b);return-1===c?null:a[c-1]},next:o,find:e,contains:h,all:f,sum:i,from:j,isEmpty:k,clusterBy:l,compact:m,unique:n}}(),NBSP_CHAR=String.fromCharCode(160),ZERO_WIDTH_NBSP_CHAR="\ufeff",dom=function(){var a=function(a){return a&&$(a).hasClass("note-editable")},b=function(a){return a&&$(a).hasClass("note-control-sizing")},c=function(a){return a=a.toUpperCase(),function(b){return b&&b.nodeName.toUpperCase()===a}},d=function(a){return a&&3===a.nodeType},e=function(a){return a&&1===a.nodeType},f=function(a){return a&&/^BR|^IMG|^HR|^IFRAME|^BUTTON|^INPUT/.test(a.nodeName.toUpperCase())},g=function(b){return!a(b)&&(b&&/^DIV|^P|^LI|^H[1-7]/.test(b.nodeName.toUpperCase()))},h=function(a){return a&&/^H[1-7]/.test(a.nodeName.toUpperCase())},i=c("PRE"),j=c("LI"),k=function(a){return g(a)&&!j(a)},l=c("TABLE"),m=c("DATA"),n=function(a){return!(s(a)||o(a)||p(a)||g(a)||l(a)||r(a)||m(a))},o=function(a){return a&&/^UL|^OL/.test(a.nodeName.toUpperCase())},p=c("HR"),q=function(a){return a&&/^TD|^TH/.test(a.nodeName.toUpperCase())},r=c("BLOCKQUOTE"),s=function(b){return q(b)||r(b)||a(b)},t=c("A"),u=function(a){return n(a)&&!!D(a,g)},v=function(a){return n(a)&&!D(a,g)},w=c("BODY"),x=function(a,b){return a.nextSibling===b||a.previousSibling===b},y=function(a,b){b=b||func.ok;var c=[];return a.previousSibling&&b(a.previousSibling)&&c.push(a.previousSibling),c.push(a),a.nextSibling&&b(a.nextSibling)&&c.push(a.nextSibling),c},z=agent.isMSIE&&agent.browserVersion<11?"&nbsp;":"<br>",A=function(a){return d(a)?a.nodeValue.length:a?a.childNodes.length:0},B=function(a){var b=A(a);return 0===b||(!d(a)&&1===b&&a.innerHTML===z||!(!list.all(a.childNodes,d)||""!==a.innerHTML))},C=function(a){f(a)||A(a)||(a.innerHTML=z)},D=function(b,c){for(;b;){if(c(b))return b;if(a(b))break;b=b.parentNode}return null},E=function(b,c){for(b=b.parentNode;b&&1===A(b);){if(c(b))return b;if(a(b))break;b=b.parentNode}return null},F=function(b,c){c=c||func.fail;var d=[];return D(b,function(b){return a(b)||d.push(b),c(b)}),d},G=function(a,b){var c=F(a);return list.last(c.filter(b))},H=function(a,b){for(var c=F(a),d=b;d;d=d.parentNode)if($.inArray(d,c)>-1)return d;return null},I=function(a,b){b=b||func.fail;for(var c=[];a&&!b(a);)c.push(a),a=a.previousSibling;return c},J=function(a,b){b=b||func.fail;for(var c=[];a&&!b(a);)c.push(a),a=a.nextSibling;return c},K=function(a,b){var c=[];return b=b||func.ok,function d(e){a!==e&&b(e)&&c.push(e);for(var f=0,g=e.childNodes.length;f<g;f++)d(e.childNodes[f])}(a),c},L=function(a,b){var c=a.parentNode,d=$("<"+b+">")[0];return c.insertBefore(d,a),d.appendChild(a),d},M=function(a,b){var c=b.nextSibling,d=b.parentNode;return c?d.insertBefore(a,c):d.appendChild(a),a},N=function(a,b){return $.each(b,function(b,c){a.appendChild(c)}),a},O=function(a){return 0===a.offset},P=function(a){return a.offset===A(a.node)},Q=function(a){return O(a)||P(a)},R=function(a,b){for(;a&&a!==b;){if(0!==V(a))return!1;a=a.parentNode}return!0},S=function(a,b){if(!b)return!1;for(;a&&a!==b;){if(V(a)!==A(a.parentNode)-1)return!1;a=a.parentNode}return!0},T=function(a,b){return O(a)&&R(a.node,b)},U=function(a,b){return P(a)&&S(a.node,b)},V=function(a){for(var b=0;a=a.previousSibling;)b+=1;return b},W=function(a){return!!(a&&a.childNodes&&a.childNodes.length)},X=function(b,c){var d,e;if(0===b.offset){if(a(b.node))return null;d=b.node.parentNode,e=V(b.node)}else W(b.node)?(d=b.node.childNodes[b.offset-1],e=A(d)):(d=b.node,e=c?0:b.offset-1);return{node:d,offset:e}},Y=function(b,c){var d,e;if(A(b.node)===b.offset){if(a(b.node))return null;d=b.node.parentNode,e=V(b.node)+1}else W(b.node)?(d=b.node.childNodes[b.offset],e=0):(d=b.node,e=c?A(b.node):b.offset+1);return{node:d,offset:e}},Z=function(a,b){return a.node===b.node&&a.offset===b.offset},_=function(a){if(d(a.node)||!W(a.node)||B(a.node))return!0;var b=a.node.childNodes[a.offset-1],c=a.node.childNodes[a.offset];return!(b&&!f(b)||c&&!f(c))},aa=function(a,b){for(;a;){if(b(a))return a;a=X(a)}return null},ba=function(a,b){for(;a;){if(b(a))return a;a=Y(a)}return null},ca=function(a){if(!d(a.node))return!1;var b=a.node.nodeValue.charAt(a.offset-1);return b&&" "!==b&&b!==NBSP_CHAR},da=function(a,b,c,d){for(var e=a;e&&(c(e),!Z(e,b));){var f=d&&a.node!==e.node&&b.node!==e.node;e=Y(e,f)}},ea=function(a,b){return F(b,func.eq(a)).map(V).reverse()},fa=function(a,b){for(var c=a,d=0,e=b.length;d<e;d++)c=c.childNodes.length<=b[d]?c.childNodes[c.childNodes.length-1]:c.childNodes[b[d]];return c},ga=function(a,b){var c=b&&b.isSkipPaddingBlankHTML,e=b&&b.isNotSplitEdgePoint;if(Q(a)&&(d(a.node)||e)){if(O(a))return a.node;if(P(a))return a.node.nextSibling}if(d(a.node))return a.node.splitText(a.offset);var f=a.node.childNodes[a.offset],g=M(a.node.cloneNode(!1),a.node);return N(g,J(f)),c||(C(a.node),C(g)),g},ha=function(a,b,c){var d=F(b.node,func.eq(a));return d.length?1===d.length?ga(b,c):d.reduce(function(a,d){return a===b.node&&(a=ga(b,c)),ga({node:d,offset:a?dom.position(a):A(d)},c)}):null},ia=function(a,b){var c,d,e=b?g:s,f=F(a.node,e),h=list.last(f)||a.node;e(h)?(c=f[f.length-2],d=h):(c=h,d=c.parentNode);var i=c&&ha(c,a,{isSkipPaddingBlankHTML:b,isNotSplitEdgePoint:b});return i||d!==a.node||(i=a.node.childNodes[a.offset]),{rightNode:i,container:d}},ja=function(a){return document.createElement(a)},ka=function(a){return document.createTextNode(a)},la=function(a,b){if(a&&a.parentNode){if(a.removeNode)return a.removeNode(b);var c=a.parentNode;if(!b){var d,e,f=[];for(d=0,e=a.childNodes.length;d<e;d++)f.push(a.childNodes[d]);for(d=0,e=f.length;d<e;d++)c.insertBefore(f[d],a)}c.removeChild(a)}},ma=function(b,c){for(;b&&!a(b)&&c(b);){var d=b.parentNode;la(b),b=d}},na=function(a,b){if(a.nodeName.toUpperCase()===b.toUpperCase())return a;var c=ja(b);return a.style.cssText&&(c.style.cssText=a.style.cssText),N(c,list.from(a.childNodes)),M(c,a),la(a),c},oa=c("TEXTAREA"),pa=function(a,b){var c=oa(a[0])?a.val():a.html();return b?c.replace(/[\n\r]/g,""):c},qa=function(a,b){var c=pa(a);if(b){var d=/<(\/?)(\b(?!!)[^>\s]*)(.*?)(\s*\/?>)/g;c=c.replace(d,function(a,b,c){c=c.toUpperCase();var d=/^DIV|^TD|^TH|^P|^LI|^H[1-7]/.test(c)&&!!b,e=/^BLOCKQUOTE|^TABLE|^TBODY|^TR|^HR|^UL|^OL/.test(c);return a+(d||e?"\n":"")}),c=$.trim(c)}return c},ra=function(a){var b=$(a),c=b.offset(),d=b.outerHeight(!0);return{left:c.left,top:c.top+d}},sa=function(a,b){Object.keys(b).forEach(function(c){a.on(c,b[c])})},ta=function(a,b){Object.keys(b).forEach(function(c){a.off(c,b[c])})},ua=function(a){return a&&!dom.isText(a)&&list.contains(a.classList,"note-styletag")};return{NBSP_CHAR:NBSP_CHAR,ZERO_WIDTH_NBSP_CHAR:ZERO_WIDTH_NBSP_CHAR,blank:z,emptyPara:"<p>"+z+"</p>",makePredByNodeName:c,isEditable:a,isControlSizing:b,isText:d,isElement:e,isVoid:f,isPara:g,isPurePara:k,isHeading:h,isInline:n,isBlock:func.not(n),isBodyInline:v,isBody:w,isParaInline:u,isPre:i,isList:o,isTable:l,isData:m,isCell:q,isBlockquote:r,isBodyContainer:s,isAnchor:t,isDiv:c("DIV"),isLi:j,isBR:c("BR"),isSpan:c("SPAN"),isB:c("B"),isU:c("U"),isS:c("S"),isI:c("I"),isImg:c("IMG"),isTextarea:oa,isEmpty:B,isEmptyAnchor:func.and(t,B),isClosestSibling:x,withClosestSiblings:y,nodeLength:A,isLeftEdgePoint:O,isRightEdgePoint:P,isEdgePoint:Q,isLeftEdgeOf:R,isRightEdgeOf:S,isLeftEdgePointOf:T,isRightEdgePointOf:U,prevPoint:X,nextPoint:Y,isSamePoint:Z,isVisiblePoint:_,prevPointUntil:aa,nextPointUntil:ba,isCharPoint:ca,walkPoint:da,ancestor:D,singleChildAncestor:E,listAncestor:F,lastAncestor:G,listNext:J,listPrev:I,listDescendant:K,commonAncestor:H,wrap:L,insertAfter:M,appendChildNodes:N,position:V,hasChildren:W,makeOffsetPath:ea,fromOffsetPath:fa,splitTree:ha,splitPoint:ia,create:ja,createText:ka,remove:la,removeWhile:ma,replace:na,html:qa,value:pa,posFromPlaceholder:ra,attachEvents:sa,detachEvents:ta,isCustomStyleTag:ua}}(),Context=function(a,b){var c=this,d=$.summernote.ui;return this.memos={},this.modules={},this.layoutInfo={},this.options=b,this.initialize=function(){return this.layoutInfo=d.createLayout(a,b),this._initialize(),a.hide(),this},this.destroy=function(){this._destroy(),a.removeData("summernote"),d.removeLayout(a,this.layoutInfo)},this.reset=function(){var a=c.isDisabled();this.code(dom.emptyPara),this._destroy(),this._initialize(),a&&c.disable()},this._initialize=function(){var a=$.extend({},this.options.buttons);Object.keys(a).forEach(function(b){c.memo("button."+b,a[b])});var b=$.extend({},this.options.modules,$.summernote.plugins||{});Object.keys(b).forEach(function(a){c.module(a,b[a],!0)}),Object.keys(this.modules).forEach(function(a){c.initializeModule(a)})},this._destroy=function(){Object.keys(this.modules).reverse().forEach(function(a){c.removeModule(a)}),Object.keys(this.memos).forEach(function(a){c.removeMemo(a)}),this.triggerEvent("destroy",this)},this.code=function(b){var c=this.invoke("codeview.isActivated");if(void 0===b)return this.invoke("codeview.sync"),c?this.layoutInfo.codable.val():this.layoutInfo.editable.html();c?this.layoutInfo.codable.val(b):this.layoutInfo.editable.html(b),a.val(b),this.triggerEvent("change",b)},this.isDisabled=function(){return"false"===this.layoutInfo.editable.attr("contenteditable")},this.enable=function(){this.layoutInfo.editable.attr("contenteditable",!0),this.invoke("toolbar.activate",!0),this.triggerEvent("disable",!1)},this.disable=function(){this.invoke("codeview.isActivated")&&this.invoke("codeview.deactivate"),this.layoutInfo.editable.attr("contenteditable",!1),this.invoke("toolbar.deactivate",!0),this.triggerEvent("disable",!0)},this.triggerEvent=function(){var b=list.head(arguments),c=list.tail(list.from(arguments)),d=this.options.callbacks[func.namespaceToCamel(b,"on")];d&&d.apply(a[0],c),a.trigger("summernote."+b,c)},this.initializeModule=function(b){var c=this.modules[b];c.shouldInitialize=c.shouldInitialize||func.ok,c.shouldInitialize()&&(c.initialize&&c.initialize(),c.events&&dom.attachEvents(a,c.events))},this.module=function(a,b,c){if(1===arguments.length)return this.modules[a];this.modules[a]=new b(this),c||this.initializeModule(a)},this.removeModule=function(b){var c=this.modules[b];c.shouldInitialize()&&(c.events&&dom.detachEvents(a,c.events),c.destroy&&c.destroy()),delete this.modules[b]},this.memo=function(a,b){if(1===arguments.length)return this.memos[a];this.memos[a]=b},this.removeMemo=function(a){this.memos[a]&&this.memos[a].destroy&&this.memos[a].destroy(),delete this.memos[a]},this.createInvokeHandlerAndUpdateState=function(a,b){return function(d){c.createInvokeHandler(a,b)(d),c.invoke("buttons.updateCurrentStyle")}},this.createInvokeHandler=function(a,b){return function(d){d.preventDefault();var e=$(d.target);c.invoke(a,b||e.closest("[data-value]").data("value"),e)}},this.invoke=function(){var a=list.head(arguments),b=list.tail(list.from(arguments)),c=a.split("."),d=c.length>1,e=d&&list.head(c),f=d?list.last(c):list.head(c),g=this.modules[e||"editor"];return!e&&this[f]?this[f].apply(this,b):g&&g[f]&&g.shouldInitialize()?g[f].apply(g,b):void 0},this.initialize()};$.fn.extend({summernote:function(){var a=$.type(list.head(arguments)),b="string"===a,c="object"===a,d=c?list.head(arguments):{};d=$.extend({},$.summernote.options,d),d.langInfo=$.extend(!0,{},$.summernote.lang["en-US"],$.summernote.lang[d.lang]),d.icons=$.extend(!0,{},$.summernote.options.icons,d.icons),d.tooltip="auto"===d.tooltip?!agent.isSupportTouch:d.tooltip,this.each(function(a,b){var c=$(b);if(!c.data("summernote")){var e=new Context(c,d);c.data("summernote",e),c.data("summernote").triggerEvent("init",e.layoutInfo)}});var e=this.first();if(e.length){var f=e.data("summernote");if(b)return f.invoke.apply(f,list.from(arguments));d.focus&&f.invoke("editor.focus")}return this}});var Renderer=function(a,b,c,d){this.render=function(e){var f=$(a);if(c&&c.contents&&f.html(c.contents),c&&c.className&&f.addClass(c.className),c&&c.data&&$.each(c.data,function(a,b){f.attr("data-"+a,b)}),c&&c.click&&f.on("click",c.click),b){var g=f.find(".note-children-container");b.forEach(function(a){a.render(g.length?g:f)})}return d&&d(f,c),c&&c.callback&&c.callback(f),e&&e.append(f),f}},renderer={create:function(a,b){return function(){var c=$.isArray(arguments[0])?arguments[0]:[],d="object"==typeof arguments[1]?arguments[1]:arguments[0];return d&&d.children&&(c=d.children),new Renderer(a,c,d,b)}}},editor=renderer.create('<div class="note-editor note-frame panel panel-default"/>'),toolbar=renderer.create('<div class="note-toolbar panel-heading"/>'),editingArea=renderer.create('<div class="note-editing-area"/>'),codable=renderer.create('<textarea class="note-codable"/>'),editable=renderer.create('<div class="note-editable panel-body" contentEditable="true"/>'),statusbar=renderer.create(['<div class="note-statusbar">','  <div class="note-resizebar">','    <div class="note-icon-bar"/>','    <div class="note-icon-bar"/>','    <div class="note-icon-bar"/>',"  </div>","</div>"].join("")),airEditor=renderer.create('<div class="note-editor"/>'),airEditable=renderer.create('<div class="note-editable" contentEditable="true"/>'),buttonGroup=renderer.create('<div class="note-btn-group btn-group">'),dropdown=renderer.create('<div class="dropdown-menu">',function(a,b){var c=$.isArray(b.items)?b.items.map(function(a){var c="string"==typeof a?a:a.value||"",d=b.template?b.template(a):a,e="object"==typeof a?a.option:void 0;return'<li><a href="#" data-value="'+c+'"'+(void 0!==e?' data-option="'+e+'"':"")+">"+d+"</a></li>"}).join(""):b.items;a.html(c)}),dropdownCheck=renderer.create('<div class="dropdown-menu note-check">',function(a,b){var c=$.isArray(b.items)?b.items.map(function(a){var c="string"==typeof a?a:a.value||"",d=b.template?b.template(a):a;return'<li><a href="#" data-value="'+c+'">'+icon(b.checkClassName)+" "+d+"</a></li>"}).join(""):b.items;a.html(c)}),palette=renderer.create('<div class="note-color-palette"/>',function(a,b){for(var c=[],d=0,e=b.colors.length;d<e;d++){for(var f=b.eventName,g=b.colors[d],h=[],i=0,j=g.length;i<j;i++){var k=g[i];h.push(['<button type="button" class="note-color-btn"','style="background-color:',k,'" ','data-event="',f,'" ','data-value="',k,'" ','title="',k,'" ','data-toggle="button" tabindex="-1"></button>'].join(""))}c.push('<div class="note-color-row">'+h.join("")+"</div>")}a.html(c.join("")),b.tooltip&&a.find(".note-color-btn").tooltip({container:"body",trigger:"hover",placement:"bottom"})}),dialog=renderer.create('<div class="modal" aria-hidden="false" tabindex="-1"/>',function(a,b){b.fade&&a.addClass("fade"),a.html(['<div class="modal-dialog">','  <div class="modal-content">',b.title?'    <div class="modal-header">      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>      <h4 class="modal-title">'+b.title+"</h4>    </div>":"",'    <div class="modal-body">'+b.body+"</div>",b.footer?'    <div class="modal-footer">'+b.footer+"</div>":"","  </div>","</div>"].join(""))}),popover=renderer.create(['<div class="note-popover popover in">','  <div class="arrow"/>','  <div class="popover-content note-children-container"/>',"</div>"].join(""),function(a,b){var c=void 0!==b.direction?b.direction:"bottom";a.addClass(c),b.hideArrow&&a.find(".arrow").hide()}),icon=function(a,b){return"<"+(b=b||"i")+' class="'+a+'"/>'},ui={editor:editor,toolbar:toolbar,editingArea:editingArea,codable:codable,editable:editable,statusbar:statusbar,airEditor:airEditor,airEditable:airEditable,buttonGroup:buttonGroup,dropdown:dropdown,dropdownCheck:dropdownCheck,palette:palette,dialog:dialog,popover:popover,icon:icon,options:{},button:function(a,b){return renderer.create('<button type="button" class="note-btn btn btn-default btn-sm" tabindex="-1">',function(a,b){b&&b.tooltip&&self.options.tooltip&&a.attr({title:b.tooltip}).tooltip({container:"body",trigger:"hover",placement:"bottom"})})(a,b)},toggleBtn:function(a,b){a.toggleClass("disabled",!b),a.attr("disabled",!b)},toggleBtnActive:function(a,b){a.toggleClass("active",b)},onDialogShown:function(a,b){a.one("shown.bs.modal",b)},onDialogHidden:function(a,b){a.one("hidden.bs.modal",b)},showDialog:function(a){a.modal("show")},hideDialog:function(a){a.modal("hide")},createLayout:function(a,b){self.options=b;var c=(b.airMode?ui.airEditor([ui.editingArea([ui.airEditable()])]):ui.editor([ui.toolbar(),ui.editingArea([ui.codable(),ui.editable()]),ui.statusbar()])).render();return c.insertAfter(a),{note:a,editor:c,toolbar:c.find(".note-toolbar"),editingArea:c.find(".note-editing-area"),editable:c.find(".note-editable"),codable:c.find(".note-codable"),statusbar:c.find(".note-statusbar")}},removeLayout:function(a,b){a.html(b.editable.html()),b.editor.remove(),a.show()}};$.summernote=$.summernote||{lang:{}},$.extend($.summernote.lang,{"en-US":{font:{bold:"Bold",italic:"Italic",underline:"Underline",clear:"Remove Font Style",height:"Line Height",name:"Font Family",strikethrough:"Strikethrough",subscript:"Subscript",superscript:"Superscript",size:"Font Size"},image:{image:"Picture",insert:"Insert Image",resizeFull:"Resize Full",resizeHalf:"Resize Half",resizeQuarter:"Resize Quarter",floatLeft:"Float Left",floatRight:"Float Right",floatNone:"Float None",shapeRounded:"Shape: Rounded",shapeCircle:"Shape: Circle",shapeThumbnail:"Shape: Thumbnail",shapeNone:"Shape: None",dragImageHere:"Drag image or text here",dropImage:"Drop image or Text",selectFromFiles:"Select from files",maximumFileSize:"Maximum file size",maximumFileSizeError:"Maximum file size exceeded.",url:"Image URL",remove:"Remove Image"},video:{video:"Video",videoLink:"Video Link",insert:"Insert Video",url:"Video URL?",providers:"(YouTube, Vimeo, Vine, Instagram, DailyMotion or Youku)"},link:{link:"Link",insert:"Insert Link",unlink:"Unlink",edit:"Edit",textToDisplay:"Text to display",url:"To what URL should this link go?",openInNewWindow:"Open in new window"},table:{table:"Table",addRowAbove:"Add row above",addRowBelow:"Add row below",addColLeft:"Add column left",addColRight:"Add column right",delRow:"Delete row",delCol:"Delete column",delTable:"Delete table"},hr:{insert:"Insert Horizontal Rule"},style:{style:"Style",p:"Normal",blockquote:"Quote",pre:"Code",h1:"Header 1",h2:"Header 2",h3:"Header 3",h4:"Header 4",h5:"Header 5",h6:"Header 6"},lists:{unordered:"Unordered list",ordered:"Ordered list"},options:{help:"Help",fullscreen:"Full Screen",codeview:"Code View"},paragraph:{paragraph:"Paragraph",outdent:"Outdent",indent:"Indent",left:"Align left",center:"Align center",right:"Align right",justify:"Justify full"},color:{recent:"Recent Color",more:"More Color",background:"Background Color",foreground:"Foreground Color",transparent:"Transparent",setTransparent:"Set transparent",reset:"Reset",resetToDefault:"Reset to default"},shortcut:{shortcuts:"Keyboard shortcuts",close:"Close",textFormatting:"Text formatting",action:"Action",paragraphFormatting:"Paragraph formatting",documentStyle:"Document Style",extraKeys:"Extra keys"},help:{insertParagraph:"Insert Paragraph",undo:"Undoes the last command",redo:"Redoes the last command",tab:"Tab",untab:"Untab",bold:"Set a bold style",italic:"Set a italic style",underline:"Set a underline style",strikethrough:"Set a strikethrough style",removeFormat:"Clean a style",justifyLeft:"Set left align",justifyCenter:"Set center align",justifyRight:"Set right align",justifyFull:"Set full align",insertUnorderedList:"Toggle unordered list",insertOrderedList:"Toggle ordered list",outdent:"Outdent on current paragraph",indent:"Indent on current paragraph",formatPara:"Change current block's format as a paragraph(P tag)",formatH1:"Change current block's format as H1",formatH2:"Change current block's format as H2",formatH3:"Change current block's format as H3",formatH4:"Change current block's format as H4",formatH5:"Change current block's format as H5",formatH6:"Change current block's format as H6",insertHorizontalRule:"Insert horizontal rule","linkDialog.show":"Show Link Dialog"},history:{undo:"Undo",redo:"Redo"},specialChar:{specialChar:"SPECIAL CHARACTERS",select:"Select Special characters"}}});var key=function(){var a={BACKSPACE:8,TAB:9,ENTER:13,SPACE:32,DELETE:46,LEFT:37,UP:38,RIGHT:39,DOWN:40,NUM0:48,NUM1:49,NUM2:50,NUM3:51,NUM4:52,NUM5:53,NUM6:54,NUM7:55,NUM8:56,B:66,E:69,I:73,J:74,K:75,L:76,R:82,S:83,U:85,V:86,Y:89,Z:90,SLASH:191,LEFTBRACKET:219,BACKSLASH:220,RIGHTBRACKET:221};return{isEdit:function(b){return list.contains([a.BACKSPACE,a.TAB,a.ENTER,a.SPACE,a.DELETE],b)},isMove:function(b){return list.contains([a.LEFT,a.UP,a.RIGHT,a.DOWN],b)},nameFromCode:func.invertObject(a),code:a}}(),range=function(){var a=function(a,b){var c,d,e=a.parentElement(),f=document.body.createTextRange(),g=list.from(e.childNodes);for(c=0;c<g.length;c++)if(!dom.isText(g[c])){if(f.moveToElementText(g[c]),f.compareEndPoints("StartToStart",a)>=0)break;d=g[c]}if(0!==c&&dom.isText(g[c-1])){var h=document.body.createTextRange(),i=null;h.moveToElementText(d||e),h.collapse(!d),i=d?d.nextSibling:e.firstChild;var j=a.duplicate();j.setEndPoint("StartToStart",h);for(var k=j.text.replace(/[\r\n]/g,"").length;k>i.nodeValue.length&&i.nextSibling;)k-=i.nodeValue.length,i=i.nextSibling;i.nodeValue;b&&i.nextSibling&&dom.isText(i.nextSibling)&&k===i.nodeValue.length&&(k-=i.nodeValue.length,i=i.nextSibling),e=i,c=k}return{cont:e,offset:c}},b=function(a){var b=function(a,c){var d,e;if(dom.isText(a)){var f=dom.listPrev(a,func.not(dom.isText)),g=list.last(f).previousSibling;d=g||a.parentNode,c+=list.sum(list.tail(f),dom.nodeLength),e=!g}else{if(d=a.childNodes[c]||a,dom.isText(d))return b(d,0);c=0,e=!1}return{node:d,collapseToStart:e,offset:c}},c=document.body.createTextRange(),d=b(a.node,a.offset);return c.moveToElementText(d.node),c.collapse(d.collapseToStart),c.moveStart("character",d.offset),c},c=function(a,d,e,f){this.sc=a,this.so=d,this.ec=e,this.eo=f;var g=function(){if(agent.isW3CRangeSupport){var c=document.createRange();return c.setStart(a,d),c.setEnd(e,f),c}var g=b({node:a,offset:d});return g.setEndPoint("EndToEnd",b({node:e,offset:f})),g};this.getPoints=function(){return{sc:a,so:d,ec:e,eo:f}},this.getStartPoint=function(){return{node:a,offset:d}},this.getEndPoint=function(){return{node:e,offset:f}},this.select=function(){var a=g();if(agent.isW3CRangeSupport){var b=document.getSelection();b.rangeCount>0&&b.removeAllRanges(),b.addRange(a)}else a.select();return this},this.scrollIntoView=function(a){var b=$(a).height();return a.scrollTop+b<this.sc.offsetTop&&(a.scrollTop+=Math.abs(a.scrollTop+b-this.sc.offsetTop)),this},this.normalize=function(){var a=function(a,b){if(dom.isVisiblePoint(a)&&!dom.isEdgePoint(a)||dom.isVisiblePoint(a)&&dom.isRightEdgePoint(a)&&!b||dom.isVisiblePoint(a)&&dom.isLeftEdgePoint(a)&&b||dom.isVisiblePoint(a)&&dom.isBlock(a.node)&&dom.isEmpty(a.node))return a;var c=dom.ancestor(a.node,dom.isBlock);if((dom.isLeftEdgePointOf(a,c)||dom.isVoid(dom.prevPoint(a).node))&&!b||(dom.isRightEdgePointOf(a,c)||dom.isVoid(dom.nextPoint(a).node))&&b){if(dom.isVisiblePoint(a))return a;b=!b}return(b?dom.nextPointUntil(dom.nextPoint(a),dom.isVisiblePoint):dom.prevPointUntil(dom.prevPoint(a),dom.isVisiblePoint))||a},b=a(this.getEndPoint(),!1),d=this.isCollapsed()?b:a(this.getStartPoint(),!0);return new c(d.node,d.offset,b.node,b.offset)},this.nodes=function(a,b){a=a||func.ok;var c=b&&b.includeAncestor,d=b&&b.fullyContains,e=this.getStartPoint(),f=this.getEndPoint(),g=[],h=[];return dom.walkPoint(e,f,function(b){if(!dom.isEditable(b.node)){var e;d?(dom.isLeftEdgePoint(b)&&h.push(b.node),dom.isRightEdgePoint(b)&&list.contains(h,b.node)&&(e=b.node)):e=c?dom.ancestor(b.node,a):b.node,e&&a(e)&&g.push(e)}},!0),list.unique(g)},this.commonAncestor=function(){return dom.commonAncestor(a,e)},this.expand=function(b){var g=dom.ancestor(a,b),h=dom.ancestor(e,b);if(!g&&!h)return new c(a,d,e,f);var i=this.getPoints();return g&&(i.sc=g,i.so=0),h&&(i.ec=h,i.eo=dom.nodeLength(h)),new c(i.sc,i.so,i.ec,i.eo)},this.collapse=function(b){return b?new c(a,d,a,d):new c(e,f,e,f)},this.splitText=function(){var b=a===e,g=this.getPoints();return dom.isText(e)&&!dom.isEdgePoint(this.getEndPoint())&&e.splitText(f),dom.isText(a)&&!dom.isEdgePoint(this.getStartPoint())&&(g.sc=a.splitText(d),g.so=0,b&&(g.ec=g.sc,g.eo=f-d)),new c(g.sc,g.so,g.ec,g.eo)},this.deleteContents=function(){if(this.isCollapsed())return this;var a=this.splitText(),b=a.nodes(null,{fullyContains:!0}),d=dom.prevPointUntil(a.getStartPoint(),function(a){return!list.contains(b,a.node)}),e=[];return $.each(b,function(a,b){var c=b.parentNode;d.node!==c&&1===dom.nodeLength(c)&&e.push(c),dom.remove(b,!1)}),$.each(e,function(a,b){dom.remove(b,!1)}),new c(d.node,d.offset,d.node,d.offset).normalize()};var h=function(b){return function(){var c=dom.ancestor(a,b);return!!c&&c===dom.ancestor(e,b)}};this.isOnEditable=h(dom.isEditable),this.isOnList=h(dom.isList),this.isOnAnchor=h(dom.isAnchor),this.isOnCell=h(dom.isCell),this.isOnData=h(dom.isData),this.isLeftEdgeOf=function(a){if(!dom.isLeftEdgePoint(this.getStartPoint()))return!1;var b=dom.ancestor(this.sc,a);return b&&dom.isLeftEdgeOf(this.sc,b)},this.isCollapsed=function(){return a===e&&d===f},this.wrapBodyInlineWithPara=function(){if(dom.isBodyContainer(a)&&dom.isEmpty(a))return a.innerHTML=dom.emptyPara,new c(a.firstChild,0,a.firstChild,0);var b=this.normalize();if(dom.isParaInline(a)||dom.isPara(a))return b;var d;if(dom.isInline(b.sc)){var e=dom.listAncestor(b.sc,func.not(dom.isInline));d=list.last(e),dom.isInline(d)||(d=e[e.length-2]||b.sc.childNodes[b.so])}else d=b.sc.childNodes[b.so>0?b.so-1:0];var f=dom.listPrev(d,dom.isParaInline).reverse();if(f=f.concat(dom.listNext(d.nextSibling,dom.isParaInline)),f.length){var g=dom.wrap(list.head(f),"p");dom.appendChildNodes(g,list.tail(f))}return this.normalize()},this.insertNode=function(a){var b=this.wrapBodyInlineWithPara().deleteContents(),c=dom.splitPoint(b.getStartPoint(),dom.isInline(a));return c.rightNode?c.rightNode.parentNode.insertBefore(a,c.rightNode):c.container.appendChild(a),a},this.pasteHTML=function(a){var b=$("<div></div>").html(a)[0],c=list.from(b.childNodes),d=this.wrapBodyInlineWithPara().deleteContents();return c.reverse().map(function(a){return d.insertNode(a)}).reverse()},this.toString=function(){var a=g();return agent.isW3CRangeSupport?a.toString():a.text},this.getWordRange=function(a){var b=this.getEndPoint();if(!dom.isCharPoint(b))return this;var d=dom.prevPointUntil(b,function(a){return!dom.isCharPoint(a)});return a&&(b=dom.nextPointUntil(b,function(a){return!dom.isCharPoint(a)})),new c(d.node,d.offset,b.node,b.offset)},this.bookmark=function(b){return{s:{path:dom.makeOffsetPath(b,a),offset:d},e:{path:dom.makeOffsetPath(b,e),offset:f}}},this.paraBookmark=function(b){return{s:{path:list.tail(dom.makeOffsetPath(list.head(b),a)),offset:d},e:{path:list.tail(dom.makeOffsetPath(list.last(b),e)),offset:f}}},this.getClientRects=function(){return g().getClientRects()}};return{create:function(a,b,d,e){if(4===arguments.length)return new c(a,b,d,e);if(2===arguments.length)return d=a,e=b,new c(a,b,d,e);var f=this.createFromSelection();return f||1!==arguments.length?f:(f=this.createFromNode(arguments[0]),f.collapse(dom.emptyPara===arguments[0].innerHTML))},createFromSelection:function(){var b,d,e,f;if(agent.isW3CRangeSupport){var g=document.getSelection();if(!g||0===g.rangeCount)return null;if(dom.isBody(g.anchorNode))return null;var h=g.getRangeAt(0);b=h.startContainer,d=h.startOffset,e=h.endContainer,f=h.endOffset}else{var i=document.selection.createRange(),j=i.duplicate();j.collapse(!1);var k=i;k.collapse(!0);var l=a(k,!0),m=a(j,!1);dom.isText(l.node)&&dom.isLeftEdgePoint(l)&&dom.isTextNode(m.node)&&dom.isRightEdgePoint(m)&&m.node.nextSibling===l.node&&(l=m),b=l.cont,d=l.offset,e=m.cont,f=m.offset}return new c(b,d,e,f)},createFromNode:function(a){var b=a,c=0,d=a,e=dom.nodeLength(d);return dom.isVoid(b)&&(c=dom.listPrev(b).length-1,b=b.parentNode),dom.isBR(d)?(e=dom.listPrev(d).length-1,d=d.parentNode):dom.isVoid(d)&&(e=dom.listPrev(d).length,d=d.parentNode),this.create(b,c,d,e)},createFromNodeBefore:function(a){return this.createFromNode(a).collapse(!0)},createFromNodeAfter:function(a){return this.createFromNode(a).collapse()},createFromBookmark:function(a,b){
var d=dom.fromOffsetPath(a,b.s.path),e=b.s.offset,f=dom.fromOffsetPath(a,b.e.path),g=b.e.offset;return new c(d,e,f,g)},createFromParaBookmark:function(a,b){var d=a.s.offset,e=a.e.offset,f=dom.fromOffsetPath(list.head(b),a.s.path),g=dom.fromOffsetPath(list.last(b),a.e.path);return new c(f,d,g,e)}}}(),async=function(){return{readFileAsDataURL:function(a){return $.Deferred(function(b){$.extend(new FileReader,{onload:function(a){var c=a.target.result;b.resolve(c)},onerror:function(){b.reject(this)}}).readAsDataURL(a)}).promise()},createImage:function(a){return $.Deferred(function(b){var c=$("<img>");c.one("load",function(){c.off("error abort"),b.resolve(c)}).one("error abort",function(){c.off("load").detach(),b.reject(c)}).css({display:"none"}).appendTo(document.body).attr("src",a)}).promise()}}}(),History=function(a){var b=[],c=-1,d=a[0],e=function(){var b=range.create(d),c={s:{path:[],offset:0},e:{path:[],offset:0}};return{contents:a.html(),bookmark:b?b.bookmark(d):c}},f=function(b){null!==b.contents&&a.html(b.contents),null!==b.bookmark&&range.createFromBookmark(d,b.bookmark).select()};this.rewind=function(){a.html()!==b[c].contents&&this.recordUndo(),c=0,f(b[c])},this.reset=function(){b=[],c=-1,a.html(""),this.recordUndo()},this.undo=function(){a.html()!==b[c].contents&&this.recordUndo(),0<c&&(c--,f(b[c]))},this.redo=function(){b.length-1>c&&(c++,f(b[c]))},this.recordUndo=function(){c++,b.length>c&&(b=b.slice(0,c)),b.push(e())}},Style=function(){var a=function(a,b){if(agent.jqueryVersion<1.9){var c={};return $.each(b,function(b,d){c[d]=a.css(d)}),c}return a.css.call(a,b)};this.fromNode=function(b){var c=["font-family","font-size","text-align","list-style-type","line-height"],d=a(b,c)||{};return d["font-size"]=parseInt(d["font-size"],10),d},this.stylePara=function(a,b){$.each(a.nodes(dom.isPara,{includeAncestor:!0}),function(a,c){$(c).css(b)})},this.styleNodes=function(a,b){a=a.splitText();var c=b&&b.nodeName||"SPAN",d=!(!b||!b.expandClosestSibling),e=!(!b||!b.onlyPartialContains);if(a.isCollapsed())return[a.insertNode(dom.create(c))];var f=dom.makePredByNodeName(c),g=a.nodes(dom.isText,{fullyContains:!0}).map(function(a){return dom.singleChildAncestor(a,f)||dom.wrap(a,c)});if(d){if(e){var h=a.nodes();f=func.and(f,function(a){return list.contains(h,a)})}return g.map(function(a){var b=dom.withClosestSiblings(a,f),c=list.head(b),d=list.tail(b);return $.each(d,function(a,b){dom.appendChildNodes(c,b.childNodes),dom.remove(b)}),list.head(b)})}return g},this.current=function(a){var b=$(dom.isElement(a.sc)?a.sc:a.sc.parentNode),c=this.fromNode(b);try{c=$.extend(c,{"font-bold":document.queryCommandState("bold")?"bold":"normal","font-italic":document.queryCommandState("italic")?"italic":"normal","font-underline":document.queryCommandState("underline")?"underline":"normal","font-subscript":document.queryCommandState("subscript")?"subscript":"normal","font-superscript":document.queryCommandState("superscript")?"superscript":"normal","font-strikethrough":document.queryCommandState("strikethrough")?"strikethrough":"normal","font-family":document.queryCommandValue("fontname")||c["font-family"]})}catch(a){}if(a.isOnList()){var d=["circle","disc","disc-leading-zero","square"],e=$.inArray(c["list-style-type"],d)>-1;c["list-style"]=e?"unordered":"ordered"}else c["list-style"]="none";var f=dom.ancestor(a.sc,dom.isPara);if(f&&f.style["line-height"])c["line-height"]=f.style.lineHeight;else{var g=parseInt(c["line-height"],10)/parseInt(c["font-size"],10);c["line-height"]=g.toFixed(1)}return c.anchor=a.isOnAnchor()&&dom.ancestor(a.sc,dom.isAnchor),c.ancestors=dom.listAncestor(a.sc,dom.isEditable),c.range=a,c}},Bullet=function(){var a=this;this.insertOrderedList=function(a){this.toggleList("OL",a)},this.insertUnorderedList=function(a){this.toggleList("UL",a)},this.indent=function(a){var b=this,c=range.create(a).wrapBodyInlineWithPara(),d=c.nodes(dom.isPara,{includeAncestor:!0}),e=list.clusterBy(d,func.peq2("parentNode"));$.each(e,function(a,c){var d=list.head(c);dom.isLi(d)?b.wrapList(c,d.parentNode.nodeName):$.each(c,function(a,b){$(b).css("marginLeft",function(a,b){return(parseInt(b,10)||0)+25})})}),c.select()},this.outdent=function(a){var b=this,c=range.create(a).wrapBodyInlineWithPara(),d=c.nodes(dom.isPara,{includeAncestor:!0}),e=list.clusterBy(d,func.peq2("parentNode"));$.each(e,function(a,c){var d=list.head(c);dom.isLi(d)?b.releaseList([c]):$.each(c,function(a,b){$(b).css("marginLeft",function(a,b){return b=parseInt(b,10)||0,b>25?b-25:""})})}),c.select()},this.toggleList=function(b,c){var d=range.create(c).wrapBodyInlineWithPara(),e=d.nodes(dom.isPara,{includeAncestor:!0}),f=d.paraBookmark(e),g=list.clusterBy(e,func.peq2("parentNode"));if(list.find(e,dom.isPurePara)){var h=[];$.each(g,function(c,d){h=h.concat(a.wrapList(d,b))}),e=h}else{var i=d.nodes(dom.isList,{includeAncestor:!0}).filter(function(a){return!$.nodeName(a,b)});i.length?$.each(i,function(a,c){dom.replace(c,b)}):e=this.releaseList(g,!0)}range.createFromParaBookmark(f,e).select()},this.wrapList=function(a,b){var c=list.head(a),d=list.last(a),e=dom.isList(c.previousSibling)&&c.previousSibling,f=dom.isList(d.nextSibling)&&d.nextSibling,g=e||dom.insertAfter(dom.create(b||"UL"),d);return a=a.map(function(a){return dom.isPurePara(a)?dom.replace(a,"LI"):a}),dom.appendChildNodes(g,a),f&&(dom.appendChildNodes(g,list.from(f.childNodes)),dom.remove(f)),a},this.releaseList=function(a,b){var c=[];return $.each(a,function(a,d){var e=list.head(d),f=list.last(d),g=b?dom.lastAncestor(e,dom.isList):e.parentNode,h=g.childNodes.length>1?dom.splitTree(g,{node:f.parentNode,offset:dom.position(f)+1},{isSkipPaddingBlankHTML:!0}):null,i=dom.splitTree(g,{node:e.parentNode,offset:dom.position(e)},{isSkipPaddingBlankHTML:!0});d=b?dom.listDescendant(i,dom.isLi):list.from(i.childNodes).filter(dom.isLi),!b&&dom.isList(g.parentNode)||(d=d.map(function(a){return dom.replace(a,"P")})),$.each(list.from(d).reverse(),function(a,b){dom.insertAfter(b,g)});var j=list.compact([g,i,h]);$.each(j,function(a,b){var c=[b].concat(dom.listDescendant(b,dom.isList));$.each(c.reverse(),function(a,b){dom.nodeLength(b)||dom.remove(b,!0)})}),c=c.concat(d)}),c}},Typing=function(){var a=new Bullet;this.insertTab=function(a,b){var c=dom.createText(new Array(b+1).join(dom.NBSP_CHAR));a=a.deleteContents(),a.insertNode(c,!0),a=range.create(c,b),a.select()},this.insertParagraph=function(b){var c=range.create(b);c=c.deleteContents(),c=c.wrapBodyInlineWithPara();var d,e=dom.ancestor(c.sc,dom.isPara);if(e){if(dom.isEmpty(e)&&dom.isLi(e))return void a.toggleList(e.parentNode.nodeName);if(dom.isEmpty(e)&&dom.isPara(e)&&dom.isBlockquote(e.parentNode))dom.insertAfter(e,e.parentNode),d=e;else{d=dom.splitTree(e,c.getStartPoint());var f=dom.listDescendant(e,dom.isEmptyAnchor);f=f.concat(dom.listDescendant(d,dom.isEmptyAnchor)),$.each(f,function(a,b){dom.remove(b)}),(dom.isHeading(d)||dom.isPre(d)||dom.isCustomStyleTag(d))&&dom.isEmpty(d)&&(d=dom.replace(d,"p"))}}else{var g=c.sc.childNodes[c.so];d=$(dom.emptyPara)[0],g?c.sc.insertBefore(d,g):c.sc.appendChild(d)}range.create(d,0).normalize().select().scrollIntoView(b)}},TableResultAction=function(a,b,c,d){function e(){return!a||!a.tagName||"td"!==a.tagName.toLowerCase()&&"th"!==a.tagName.toLowerCase()?void console.error("Impossible to identify start Cell point.",a):(n.colPos=a.cellIndex,a.parentElement&&a.parentElement.tagName&&"tr"===a.parentElement.tagName.toLowerCase()?void(n.rowPos=a.parentElement.rowIndex):void console.error("Impossible to identify start Row point.",a))}function f(a,b,c,d,e,f,g){var h={baseRow:c,baseCell:d,isRowSpan:e,isColSpan:f,isVirtual:g};o[a]||(o[a]=[]),o[a][b]=h}function g(a,b,c,d){return{baseCell:a.baseCell,action:b,virtualTable:{rowIndex:c,cellIndex:d}}}function h(a,b){if(!o[a])return b;if(!o[a][b])return b;for(var c=b;o[a][c];)if(c++,!o[a][c])return c}function i(a,b){var c=h(a.rowIndex,b.cellIndex),d=b.colSpan>1,e=b.rowSpan>1,g=a.rowIndex===n.rowPos&&b.cellIndex===n.colPos;f(a.rowIndex,c,a,b,e,d,!1);var i=b.attributes.rowSpan?parseInt(b.attributes.rowSpan.value,10):0;if(i>1)for(var k=1;k<i;k++){var l=a.rowIndex+k;j(l,c,b,g),f(l,c,a,b,!0,d,!0)}var m=b.attributes.colSpan?parseInt(b.attributes.colSpan.value,10):0;if(m>1)for(var o=1;o<m;o++){var p=h(a.rowIndex,c+o);j(a.rowIndex,p,b,g),f(a.rowIndex,p,a,b,e,!0,!0)}}function j(a,b,c,d){a===n.rowPos&&n.colPos>=c.cellIndex&&c.cellIndex<=b&&!d&&n.colPos++}function k(){for(var a=d.rows,b=0;b<a.length;b++)for(var c=a[b].cells,e=0;e<c.length;e++)i(a[b],c[e])}function l(a){switch(b){case TableResultAction.where.Column:if(a.isColSpan)return TableResultAction.resultAction.SubtractSpanCount;break;case TableResultAction.where.Row:if(!a.isVirtual&&a.isRowSpan)return TableResultAction.resultAction.AddCell;if(a.isRowSpan)return TableResultAction.resultAction.SubtractSpanCount}return TableResultAction.resultAction.RemoveCell}function m(a){switch(b){case TableResultAction.where.Column:if(a.isColSpan)return TableResultAction.resultAction.SumSpanCount;if(a.isRowSpan&&a.isVirtual)return TableResultAction.resultAction.Ignore;break;case TableResultAction.where.Row:if(a.isRowSpan)return TableResultAction.resultAction.SumSpanCount;if(a.isColSpan&&a.isVirtual)return TableResultAction.resultAction.Ignore}return TableResultAction.resultAction.AddCell}var n={colPos:0,rowPos:0},o=[],p=[];this.getActionList=function(){for(var a=b===TableResultAction.where.Row?n.rowPos:-1,d=b===TableResultAction.where.Column?n.colPos:-1,e=0,f=!0;f;){var h=a>=0?a:e,i=d>=0?d:e,j=o[h];if(!j)return f=!1,p;var k=j[i];if(!k)return f=!1,p;var q=TableResultAction.resultAction.Ignore;switch(c){case TableResultAction.requestAction.Add:q=m(k);break;case TableResultAction.requestAction.Delete:q=l(k)}p.push(g(k,q,h,i)),e++}return p},function(){e(),k()}()};TableResultAction.where={Row:0,Column:1},TableResultAction.requestAction={Add:0,Delete:1},TableResultAction.resultAction={Ignore:0,SubtractSpanCount:1,RemoveCell:2,AddCell:3,SumSpanCount:4};var Table=function(){this.tab=function(a,b){var c=dom.ancestor(a.commonAncestor(),dom.isCell),d=dom.ancestor(c,dom.isTable),e=dom.listDescendant(d,dom.isCell),f=list[b?"prev":"next"](e,c);f&&range.create(f,0).select()},this.addRow=function(a,b){for(var c=dom.ancestor(a.commonAncestor(),dom.isCell),d=$(c).closest("tr"),e=this.recoverAttributes(d),f=$("<tr"+e+"></tr>"),g=new TableResultAction(c,TableResultAction.where.Row,TableResultAction.requestAction.Add,$(d).closest("table")[0]),h=g.getActionList(),i=0;i<h.length;i++){var j=h[i],k=this.recoverAttributes(j.baseCell);switch(j.action){case TableResultAction.resultAction.AddCell:f.append("<td"+k+">"+dom.blank+"</td>");break;case TableResultAction.resultAction.SumSpanCount:if("top"===b){if((j.baseCell.parent?j.baseCell.closest("tr").rowIndex:0)<=d[0].rowIndex){var l=$("<div></div>").append($("<td"+k+">"+dom.blank+"</td>").removeAttr("rowspan")).html();f.append(l);break}}var m=parseInt(j.baseCell.rowSpan,10);m++,j.baseCell.setAttribute("rowSpan",m)}}if("top"===b)d.before(f);else{if(c.rowSpan>1){var n=d[0].rowIndex+(c.rowSpan-2);return void $($(d).parent().find("tr")[n]).after($(f))}d.after(f)}},this.addCol=function(a,b){var c=dom.ancestor(a.commonAncestor(),dom.isCell),d=$(c).closest("tr");$(d).siblings().push(d);for(var e=new TableResultAction(c,TableResultAction.where.Column,TableResultAction.requestAction.Add,$(d).closest("table")[0]),f=e.getActionList(),g=0;g<f.length;g++){var h=f[g],i=this.recoverAttributes(h.baseCell);switch(h.action){case TableResultAction.resultAction.AddCell:"right"===b?$(h.baseCell).after("<td"+i+">"+dom.blank+"</td>"):$(h.baseCell).before("<td"+i+">"+dom.blank+"</td>");break;case TableResultAction.resultAction.SumSpanCount:if("right"===b){var j=parseInt(h.baseCell.colSpan,10);j++,h.baseCell.setAttribute("colSpan",j)}else $(h.baseCell).before("<td"+i+">"+dom.blank+"</td>")}}},this.recoverAttributes=function(a){var b="";if(!a)return b;for(var c=a.attributes||[],d=0;d<c.length;d++)"id"!==c[d].name.toLowerCase()&&c[d].specified&&(b+=" "+c[d].name+"='"+c[d].value+"'");return b},this.deleteRow=function(a){for(var b=dom.ancestor(a.commonAncestor(),dom.isCell),c=$(b).closest("tr"),d=c.children("td, th").index($(b)),e=c[0].rowIndex,f=new TableResultAction(b,TableResultAction.where.Row,TableResultAction.requestAction.Delete,$(c).closest("table")[0]),g=f.getActionList(),h=0;h<g.length;h++)if(g[h]){var i=g[h].baseCell,j=g[h].virtualTable,k=i.rowSpan&&i.rowSpan>1,l=k?parseInt(i.rowSpan,10):0;switch(g[h].action){case TableResultAction.resultAction.Ignore:continue;case TableResultAction.resultAction.AddCell:var m=c.next("tr")[0];if(!m)continue;var n=c[0].cells[d];k&&(l>2?(l--,m.insertBefore(n,m.cells[d]),m.cells[d].setAttribute("rowSpan",l),m.cells[d].innerHTML=""):2===l&&(m.insertBefore(n,m.cells[d]),m.cells[d].removeAttribute("rowSpan"),m.cells[d].innerHTML=""));continue;case TableResultAction.resultAction.SubtractSpanCount:k&&(l>2?(l--,i.setAttribute("rowSpan",l),j.rowIndex!==e&&i.cellIndex===d&&(i.innerHTML="")):2===l&&(i.removeAttribute("rowSpan"),j.rowIndex!==e&&i.cellIndex===d&&(i.innerHTML="")));continue;case TableResultAction.resultAction.RemoveCell:continue}}c.remove()},this.deleteCol=function(a){for(var b=dom.ancestor(a.commonAncestor(),dom.isCell),c=$(b).closest("tr"),d=c.children("td, th").index($(b)),e=new TableResultAction(b,TableResultAction.where.Column,TableResultAction.requestAction.Delete,$(c).closest("table")[0]),f=e.getActionList(),g=0;g<f.length;g++)if(f[g])switch(f[g].action){case TableResultAction.resultAction.Ignore:continue;case TableResultAction.resultAction.SubtractSpanCount:var h=f[g].baseCell,i=h.colSpan&&h.colSpan>1;if(i){var j=h.colSpan?parseInt(h.colSpan,10):0;j>2?(j--,h.setAttribute("colSpan",j),h.cellIndex===d&&(h.innerHTML="")):2===j&&(h.removeAttribute("colSpan"),h.cellIndex===d&&(h.innerHTML=""))}continue;case TableResultAction.resultAction.RemoveCell:dom.remove(f[g].baseCell,!0);continue}},this.createTable=function(a,b,c){for(var d,e=[],f=0;f<a;f++)e.push("<td>"+dom.blank+"</td>");d=e.join("");for(var g,h=[],i=0;i<b;i++)h.push("<tr>"+d+"</tr>");g=h.join("");var j=$("<table>"+g+"</table>");return c&&c.tableClassName&&j.addClass(c.tableClassName),j[0]},this.deleteTable=function(a){var b=dom.ancestor(a.commonAncestor(),dom.isCell);$(b).closest("table").remove()}},KEY_BOGUS="bogus",Editor=function(a){var b=this,c=a.layoutInfo.note,d=a.layoutInfo.editor,e=a.layoutInfo.editable,f=a.options,g=f.langInfo,h=e[0],i=null,j=new Style,k=new Table,l=new Typing,m=new Bullet,n=new History(e);this.initialize=function(){e.on("keydown",function(c){c.keyCode===key.code.ENTER&&a.triggerEvent("enter",c),a.triggerEvent("keydown",c),c.isDefaultPrevented()||(f.shortcuts?b.handleKeyMap(c):b.preventDefaultEditableShortCuts(c))}).on("keyup",function(b){a.triggerEvent("keyup",b)}).on("focus",function(b){a.triggerEvent("focus",b)}).on("blur",function(b){a.triggerEvent("blur",b)}).on("mousedown",function(b){a.triggerEvent("mousedown",b)}).on("mouseup",function(b){a.triggerEvent("mouseup",b)}).on("scroll",function(b){a.triggerEvent("scroll",b)}).on("paste",function(b){a.triggerEvent("paste",b)}),e.html(dom.html(c)||dom.emptyPara);var g=agent.isMSIE?"DOMCharacterDataModified DOMSubtreeModified DOMNodeInserted":"input";e.on(g,func.debounce(function(){a.triggerEvent("change",e.html())},250)),d.on("focusin",function(b){a.triggerEvent("focusin",b)}).on("focusout",function(b){a.triggerEvent("focusout",b)}),f.airMode||(f.width&&d.outerWidth(f.width),f.height&&e.outerHeight(f.height),f.maxHeight&&e.css("max-height",f.maxHeight),f.minHeight&&e.css("min-height",f.minHeight)),n.recordUndo()},this.destroy=function(){e.off()},this.handleKeyMap=function(b){var c=f.keyMap[agent.isMac?"mac":"pc"],d=[];b.metaKey&&d.push("CMD"),b.ctrlKey&&!b.altKey&&d.push("CTRL"),b.shiftKey&&d.push("SHIFT");var e=key.nameFromCode[b.keyCode];e&&d.push(e);var g=c[d.join("+")];g?(b.preventDefault(),a.invoke(g)):key.isEdit(b.keyCode)&&this.afterCommand()},this.preventDefaultEditableShortCuts=function(a){(a.ctrlKey||a.metaKey)&&list.contains([66,73,85],a.keyCode)&&a.preventDefault()},this.createRange=function(){return this.focus(),range.create(h)},this.saveRange=function(a){i=this.createRange(),a&&i.collapse().select()},this.restoreRange=function(){i&&(i.select(),this.focus())},this.saveTarget=function(a){e.data("target",a)},this.clearTarget=function(){e.removeData("target")},this.restoreTarget=function(){return e.data("target")},this.currentStyle=function(){var a=range.create();return a&&(a=a.normalize()),a?j.current(a):j.fromNode(e)},this.styleFromNode=function(a){return j.fromNode(a)},this.undo=function(){a.triggerEvent("before.command",e.html()),n.undo(),a.triggerEvent("change",e.html())},a.memo("help.undo",g.help.undo),this.redo=function(){a.triggerEvent("before.command",e.html()),n.redo(),a.triggerEvent("change",e.html())},a.memo("help.redo",g.help.redo);for(var o=this.beforeCommand=function(){a.triggerEvent("before.command",e.html()),b.focus()},p=this.afterCommand=function(b){n.recordUndo(),b||a.triggerEvent("change",e.html())},q=["bold","italic","underline","strikethrough","superscript","subscript","justifyLeft","justifyCenter","justifyRight","justifyFull","formatBlock","removeFormat","backColor","foreColor","fontName"],r=0,s=q.length;r<s;r++)this[q[r]]=function(a){return function(b){o(),document.execCommand(a,!1,b),p(!0)}}(q[r]),a.memo("help."+q[r],g.help[q[r]]);this.tab=function(){var a=this.createRange();a.isCollapsed()&&a.isOnCell()?k.tab(a):(o(),l.insertTab(a,f.tabSize),p())},a.memo("help.tab",g.help.tab),this.untab=function(){var a=this.createRange();a.isCollapsed()&&a.isOnCell()&&k.tab(a,!0)},a.memo("help.untab",g.help.untab),this.wrapCommand=function(a){return function(){o(),a.apply(b,arguments),p()}},this.insertParagraph=this.wrapCommand(function(){l.insertParagraph(h)}),a.memo("help.insertParagraph",g.help.insertParagraph),this.insertOrderedList=this.wrapCommand(function(){m.insertOrderedList(h)}),a.memo("help.insertOrderedList",g.help.insertOrderedList),this.insertUnorderedList=this.wrapCommand(function(){m.insertUnorderedList(h)}),a.memo("help.insertUnorderedList",g.help.insertUnorderedList),this.indent=this.wrapCommand(function(){m.indent(h)}),a.memo("help.indent",g.help.indent),this.outdent=this.wrapCommand(function(){m.outdent(h)}),a.memo("help.outdent",g.help.outdent),this.insertImage=function(b,c){return async.createImage(b,c).then(function(a){o(),"function"==typeof c?c(a):("string"==typeof c&&a.attr("data-filename",c),a.css("width",Math.min(e.width(),a.width()))),a.show(),range.create(h).insertNode(a[0]),range.createFromNodeAfter(a[0]).select(),p()}).fail(function(b){a.triggerEvent("image.upload.error",b)})},this.insertImages=function(c){$.each(c,function(c,d){var e=d.name;f.maximumImageFileSize&&f.maximumImageFileSize<d.size?a.triggerEvent("image.upload.error",g.image.maximumFileSizeError):async.readFileAsDataURL(d).then(function(a){return b.insertImage(a,e)}).fail(function(){a.triggerEvent("image.upload.error")})})},this.insertImagesOrCallback=function(b){f.callbacks.onImageUpload?a.triggerEvent("image.upload",b):this.insertImages(b)},this.insertNode=this.wrapCommand(function(a){this.createRange().insertNode(a),range.createFromNodeAfter(a).select()}),this.insertText=this.wrapCommand(function(a){var b=this.createRange(),c=b.insertNode(dom.createText(a));range.create(c,dom.nodeLength(c)).select()}),this.getSelectedText=function(){var a=this.createRange();return a.isOnAnchor()&&(a=range.createFromNode(dom.ancestor(a.sc,dom.isAnchor))),a.toString()},this.pasteHTML=this.wrapCommand(function(a){var b=this.createRange().pasteHTML(a);range.createFromNodeAfter(list.last(b)).select()}),this.formatBlock=this.wrapCommand(function(b,c){var d=a.options.callbacks.onApplyCustomStyle;d?d.call(this,c,a,this.onFormatBlock):this.onFormatBlock(b)}),this.onFormatBlock=function(a){a=agent.isMSIE?"<"+a+">":a,document.execCommand("FormatBlock",!1,a)},this.formatPara=function(){this.formatBlock("P")},a.memo("help.formatPara",g.help.formatPara);for(var r=1;r<=6;r++)this["formatH"+r]=function(a){return function(){this.formatBlock("H"+a)}}(r),a.memo("help.formatH"+r,g.help["formatH"+r]);this.fontSize=function(a){var b=this.createRange();if(b&&b.isCollapsed()){var c=j.styleNodes(b),d=list.head(c);$(c).css({"font-size":a+"px"}),d&&!dom.nodeLength(d)&&(d.innerHTML=dom.ZERO_WIDTH_NBSP_CHAR,range.createFromNodeAfter(d.firstChild).select(),e.data(KEY_BOGUS,d))}else o(),$(j.styleNodes(b)).css({"font-size":a+"px"}),p()},this.insertHorizontalRule=this.wrapCommand(function(){var a=this.createRange().insertNode(dom.create("HR"));a.nextSibling&&range.create(a.nextSibling,0).normalize().select()}),a.memo("help.insertHorizontalRule",g.help.insertHorizontalRule),this.removeBogus=function(){var a=e.data(KEY_BOGUS);if(a){var b=list.find(list.from(a.childNodes),dom.isText),c=b.nodeValue.indexOf(dom.ZERO_WIDTH_NBSP_CHAR);-1!==c&&b.deleteData(c,1),dom.isEmpty(a)&&dom.remove(a),e.removeData(KEY_BOGUS)}},this.lineHeight=this.wrapCommand(function(a){j.stylePara(this.createRange(),{lineHeight:a})}),this.unlink=function(){var a=this.createRange();if(a.isOnAnchor()){var b=dom.ancestor(a.sc,dom.isAnchor);a=range.createFromNode(b),a.select(),o(),document.execCommand("unlink"),p()}},this.createLink=this.wrapCommand(function(a){var b=a.url,c=a.text,d=a.isNewWindow,e=a.range||this.createRange(),g=e.toString()!==c;"string"==typeof b&&(b=b.trim()),b=f.onCreateLink?f.onCreateLink(b):/^[A-Za-z][A-Za-z0-9+-.]*\:[\/\/]?/.test(b)?b:"http://"+b;var h=[];if(g){e=e.deleteContents();var i=e.insertNode($("<A>"+c+"</A>")[0]);h.push(i)}else h=j.styleNodes(e,{nodeName:"A",expandClosestSibling:!0,onlyPartialContains:!0});$.each(h,function(a,c){$(c).attr("href",b),d?$(c).attr("target","_blank"):$(c).removeAttr("target")});var k=range.createFromNodeBefore(list.head(h)),l=k.getStartPoint(),m=range.createFromNodeAfter(list.last(h)),n=m.getEndPoint();range.create(l.node,l.offset,n.node,n.offset).select()}),this.getLinkInfo=function(){var a=this.createRange().expand(dom.isAnchor),b=$(list.head(a.nodes(dom.isAnchor))),c={range:a,text:a.toString(),url:b.length?b.attr("href"):""};return b.length&&(c.isNewWindow="_blank"===b.attr("target")),c},this.color=this.wrapCommand(function(a){var b=a.foreColor,c=a.backColor;b&&document.execCommand("foreColor",!1,b),c&&document.execCommand("backColor",!1,c)}),this.insertTable=this.wrapCommand(function(a){var b=a.split("x");this.createRange().deleteContents().insertNode(k.createTable(b[0],b[1],f))}),this.addRow=function(a){var b=this.createRange(e);b.isCollapsed()&&b.isOnCell()&&(o(),k.addRow(b,a),p())},this.addCol=function(a){var b=this.createRange(e);b.isCollapsed()&&b.isOnCell()&&(o(),k.addCol(b,a),p())},this.deleteRow=function(){var a=this.createRange(e);a.isCollapsed()&&a.isOnCell()&&(o(),k.deleteRow(a),p())},this.deleteCol=function(){var a=this.createRange(e);a.isCollapsed()&&a.isOnCell()&&(o(),k.deleteCol(a),p())},this.deleteTable=function(){var a=this.createRange(e);a.isCollapsed()&&a.isOnCell()&&(o(),k.deleteTable(a),p())},this.floatMe=this.wrapCommand(function(a){var b=$(this.restoreTarget());b.toggleClass("note-float-left","left"===a),b.toggleClass("note-float-right","right"===a),b.css("float",a)}),this.resize=this.wrapCommand(function(a){$(this.restoreTarget()).css({width:100*a+"%",height:""})}),this.resizeTo=function(a,b,c){var d;if(c){var e=a.y/a.x,f=b.data("ratio");d={width:f>e?a.x:a.y/f,height:f>e?a.x*f:a.y}}else d={width:a.x,height:a.y};b.css(d)},this.removeMedia=this.wrapCommand(function(){var b=$(this.restoreTarget()).detach();a.triggerEvent("media.delete",b,e)}),this.hasFocus=function(){return e.is(":focus")},this.focus=function(){this.hasFocus()||e.focus()},this.isEmpty=function(){return dom.isEmpty(e[0])||dom.emptyPara===e.html()},this.empty=function(){a.invoke("code",dom.emptyPara)}},Clipboard=function(a){var b=this,c=a.layoutInfo.editable;this.events={"summernote.keydown":function(c,d){b.needKeydownHook()&&(d.ctrlKey||d.metaKey)&&d.keyCode===key.code.V&&(a.invoke("editor.saveRange"),b.$paste.focus(),setTimeout(function(){b.pasteByHook()},0))}},this.needKeydownHook=function(){return agent.isMSIE&&agent.browserVersion>10||agent.isFF},this.initialize=function(){this.needKeydownHook()?(this.$paste=$('<div tabindex="-1" />').attr("contenteditable",!0).css({position:"absolute",left:-1e5,opacity:0}),c.before(this.$paste),this.$paste.on("paste",function(b){a.triggerEvent("paste",b)})):c.on("paste",this.pasteByEvent)},this.destroy=function(){this.needKeydownHook()&&(this.$paste.remove(),this.$paste=null)},this.pasteByHook=function(){var b=this.$paste[0].firstChild,c=b&&b.src;if(dom.isImg(b)&&0===c.indexOf("data:")){for(var d=atob(b.src.split(",")[1]),e=new Uint8Array(d.length),f=0;f<d.length;f++)e[f]=d.charCodeAt(f);var g=new Blob([e],{type:"image/png"});g.name="clipboard.png",a.invoke("editor.restoreRange"),a.invoke("editor.focus"),a.invoke("editor.insertImagesOrCallback",[g])}else{var h=$("<div />").html(this.$paste.html()).html();a.invoke("editor.restoreRange"),a.invoke("editor.focus"),h&&a.invoke("editor.pasteHTML",h)}this.$paste.empty()},this.pasteByEvent=function(b){var c=b.originalEvent.clipboardData;if(c&&c.items&&c.items.length){var d=list.head(c.items);"file"===d.kind&&-1!==d.type.indexOf("image/")&&a.invoke("editor.insertImagesOrCallback",[d.getAsFile()]),a.invoke("editor.afterCommand")}}},Dropzone=function(a){var b=$(document),c=a.layoutInfo.editor,d=a.layoutInfo.editable,e=a.options,f=e.langInfo,g={},h=$(['<div class="note-dropzone">','  <div class="note-dropzone-message"/>',"</div>"].join("")).prependTo(c),i=function(){Object.keys(g).forEach(function(a){b.off(a.substr(2).toLowerCase(),g[a])}),g={}};this.initialize=function(){e.disableDragAndDrop?(g.onDrop=function(a){a.preventDefault()},b.on("drop",g.onDrop)):this.attachDragAndDropEvent()},this.attachDragAndDropEvent=function(){var e=$(),i=h.find(".note-dropzone-message");g.onDragenter=function(b){var d=a.invoke("codeview.isActivated"),g=c.width()>0&&c.height()>0;d||e.length||!g||(c.addClass("dragover"),h.width(c.width()),h.height(c.height()),i.text(f.image.dragImageHere)),e=e.add(b.target)},g.onDragleave=function(a){e=e.not(a.target),e.length||c.removeClass("dragover")},g.onDrop=function(){e=$(),c.removeClass("dragover")},b.on("dragenter",g.onDragenter).on("dragleave",g.onDragleave).on("drop",g.onDrop),h.on("dragenter",function(){h.addClass("hover"),i.text(f.image.dropImage)}).on("dragleave",function(){h.removeClass("hover"),i.text(f.image.dragImageHere)}),h.on("drop",function(b){var c=b.originalEvent.dataTransfer;c&&c.files&&c.files.length?(b.preventDefault(),d.focus(),a.invoke("editor.insertImagesOrCallback",c.files)):$.each(c.types,function(b,d){var e=c.getData(d);d.toLowerCase().indexOf("text")>-1?a.invoke("editor.pasteHTML",e):$(e).each(function(){a.invoke("editor.insertNode",this)})})}).on("dragover",!1)},this.destroy=function(){i()}},CodeMirror;agent.hasCodeMirror&&(agent.isSupportAmd?require(["codemirror"],function(a){CodeMirror=a}):CodeMirror=window.CodeMirror);var Codeview=function(a){var b=a.layoutInfo.editor,c=a.layoutInfo.editable,d=a.layoutInfo.codable,e=a.options;this.sync=function(){this.isActivated()&&agent.hasCodeMirror&&d.data("cmEditor").save()},this.isActivated=function(){return b.hasClass("codeview")},this.toggle=function(){this.isActivated()?this.deactivate():this.activate(),a.triggerEvent("codeview.toggled")},this.activate=function(){if(d.val(dom.html(c,e.prettifyHtml)),d.height(c.height()),a.invoke("toolbar.updateCodeview",!0),b.addClass("codeview"),d.focus(),agent.hasCodeMirror){var f=CodeMirror.fromTextArea(d[0],e.codemirror);if(e.codemirror.tern){var g=new CodeMirror.TernServer(e.codemirror.tern);f.ternServer=g,f.on("cursorActivity",function(a){g.updateArgHints(a)})}f.setSize(null,c.outerHeight()),d.data("cmEditor",f)}},this.deactivate=function(){if(agent.hasCodeMirror){var f=d.data("cmEditor");d.val(f.getValue()),f.toTextArea()}var g=dom.value(d,e.prettifyHtml)||dom.emptyPara,h=c.html()!==g;c.html(g),c.height(e.height?d.height():"auto"),b.removeClass("codeview"),h&&a.triggerEvent("change",c.html(),c),c.focus(),a.invoke("toolbar.updateCodeview",!1)},this.destroy=function(){this.isActivated()&&this.deactivate()}},EDITABLE_PADDING=24,Statusbar=function(a){var b=$(document),c=a.layoutInfo.statusbar,d=a.layoutInfo.editable,e=a.options;this.initialize=function(){if(e.airMode||e.disableResizeEditor)return void this.destroy();c.on("mousedown",function(a){a.preventDefault(),a.stopPropagation();var c=d.offset().top-b.scrollTop(),f=function(a){var b=a.clientY-(c+EDITABLE_PADDING);b=e.minheight>0?Math.max(b,e.minheight):b,b=e.maxHeight>0?Math.min(b,e.maxHeight):b,d.height(b)};b.on("mousemove",f).one("mouseup",function(){b.off("mousemove",f)})})},this.destroy=function(){c.off(),c.remove()}},Fullscreen=function(a){var b=this,c=a.layoutInfo.editor,d=a.layoutInfo.toolbar,e=a.layoutInfo.editable,f=a.layoutInfo.codable,g=$(window),h=$("html, body");this.resizeTo=function(a){e.css("height",a.h),f.css("height",a.h),f.data("cmeditor")&&f.data("cmeditor").setsize(null,a.h)},this.onResize=function(){b.resizeTo({h:g.height()-d.outerHeight()})},this.toggle=function(){c.toggleClass("fullscreen"),this.isFullscreen()?(e.data("orgHeight",e.css("height")),g.on("resize",this.onResize).trigger("resize"),h.css("overflow","hidden")):(g.off("resize",this.onResize),this.resizeTo({h:e.data("orgHeight")}),h.css("overflow","visible")),a.invoke("toolbar.updateFullscreen",this.isFullscreen())},this.isFullscreen=function(){return c.hasClass("fullscreen")}},Handle=function(a){var b=this,c=$(document),d=a.layoutInfo.editingArea,e=a.options;this.events={"summernote.mousedown":function(a,c){b.update(c.target)&&c.preventDefault()},"summernote.keyup summernote.scroll summernote.change summernote.dialog.shown":function(){b.update()},"summernote.disable":function(){b.hide()},"summernote.codeview.toggled":function(){b.update()}},this.initialize=function(){this.$handle=$(['<div class="note-handle">','<div class="note-control-selection">','<div class="note-control-selection-bg"></div>','<div class="note-control-holder note-control-nw"></div>','<div class="note-control-holder note-control-ne"></div>','<div class="note-control-holder note-control-sw"></div>','<div class="',e.disableResizeImage?"note-control-holder":"note-control-sizing",' note-control-se"></div>',e.disableResizeImage?"":'<div class="note-control-selection-info"></div>',"</div>","</div>"].join("")).prependTo(d),this.$handle.on("mousedown",function(d){if(dom.isControlSizing(d.target)){d.preventDefault(),d.stopPropagation();var e=b.$handle.find(".note-control-selection").data("target"),f=e.offset(),g=c.scrollTop(),h=function(c){a.invoke("editor.resizeTo",{x:c.clientX-f.left,y:c.clientY-(f.top-g)},e,!c.shiftKey),b.update(e[0])};c.on("mousemove",h).one("mouseup",function(b){b.preventDefault(),c.off("mousemove",h),a.invoke("editor.afterCommand")}),e.data("ratio")||e.data("ratio",e.height()/e.width())}}),this.$handle.on("wheel",function(a){a.preventDefault(),b.update()})},this.destroy=function(){this.$handle.remove()},this.update=function(b){if(a.isDisabled())return!1;var c=dom.isImg(b),d=this.$handle.find(".note-control-selection");if(a.invoke("imagePopover.update",b),c){var e=$(b),f=e.position(),g={left:f.left+parseInt(e.css("marginLeft"),10),top:f.top+parseInt(e.css("marginTop"),10)},h={w:e.outerWidth(!1),h:e.outerHeight(!1)};d.css({display:"block",left:g.left,top:g.top,width:h.w,height:h.h}).data("target",e);var i=h.w+"x"+h.h;d.find(".note-control-selection-info").text(i),a.invoke("editor.saveTarget",b)}else this.hide();return c},this.hide=function(){a.invoke("editor.clearTarget"),this.$handle.children().hide()}},AutoLink=function(a){var b=this,c=/^([A-Za-z][A-Za-z0-9+-.]*\:[\/\/]?|mailto:[A-Z0-9._%+-]+@)?(www\.)?(.+)$/i;this.events={"summernote.keyup":function(a,c){c.isDefaultPrevented()||b.handleKeyup(c)},"summernote.keydown":function(a,c){b.handleKeydown(c)}},
this.initialize=function(){this.lastWordRange=null},this.destroy=function(){this.lastWordRange=null},this.replace=function(){if(this.lastWordRange){var b=this.lastWordRange.toString(),d=b.match(c);if(d&&(d[1]||d[2])){var e=d[1]?b:"http://"+b,f=$("<a />").html(b).attr("href",e)[0];this.lastWordRange.insertNode(f),this.lastWordRange=null,a.invoke("editor.focus")}}},this.handleKeydown=function(b){if(list.contains([key.code.ENTER,key.code.SPACE],b.keyCode)){var c=a.invoke("editor.createRange").getWordRange();this.lastWordRange=c}},this.handleKeyup=function(a){list.contains([key.code.ENTER,key.code.SPACE],a.keyCode)&&this.replace()}},AutoSync=function(a){var b=a.layoutInfo.note;this.events={"summernote.change":function(){b.val(a.invoke("code"))}},this.shouldInitialize=function(){return dom.isTextarea(b[0])}},Placeholder=function(a){var b=this,c=a.layoutInfo.editingArea,d=a.options;this.events={"summernote.init summernote.change":function(){b.update()},"summernote.codeview.toggled":function(){b.update()}},this.shouldInitialize=function(){return!!d.placeholder},this.initialize=function(){this.$placeholder=$('<div class="note-placeholder">'),this.$placeholder.on("click",function(){a.invoke("focus")}).text(d.placeholder).prependTo(c)},this.destroy=function(){this.$placeholder.remove()},this.update=function(){var b=!a.invoke("codeview.isActivated")&&a.invoke("editor.isEmpty");this.$placeholder.toggle(b)}},Buttons=function(a){var b=this,c=$.summernote.ui,d=a.layoutInfo.toolbar,e=a.options,f=e.langInfo,g=func.invertObject(e.keyMap[agent.isMac?"mac":"pc"]),h=this.representShortcut=function(a){var b=g[a];return e.shortcuts&&b?(agent.isMac&&(b=b.replace("CMD","⌘").replace("SHIFT","⇧"))," ("+(b=b.replace("BACKSLASH","\\").replace("SLASH","/").replace("LEFTBRACKET","[").replace("RIGHTBRACKET","]"))+")"):""};this.initialize=function(){this.addToolbarButtons(),this.addImagePopoverButtons(),this.addLinkPopoverButtons(),this.addTablePopoverButtons(),this.fontInstalledMap={}},this.destroy=function(){delete this.fontInstalledMap},this.isFontInstalled=function(a){return b.fontInstalledMap.hasOwnProperty(a)||(b.fontInstalledMap[a]=agent.isFontInstalled(a)||list.contains(e.fontNamesIgnoreCheck,a)),b.fontInstalledMap[a]},this.addToolbarButtons=function(){a.memo("button.style",function(){return c.buttonGroup([c.button({className:"dropdown-toggle",contents:c.icon(e.icons.magic)+" "+c.icon(e.icons.caret,"span"),tooltip:f.style.style,data:{toggle:"dropdown"}}),c.dropdown({className:"dropdown-style",items:a.options.styleTags,template:function(a){"string"==typeof a&&(a={tag:a,title:f.style.hasOwnProperty(a)?f.style[a]:a});var b=a.tag,c=a.title;return"<"+b+(a.style?' style="'+a.style+'" ':"")+(a.className?' class="'+a.className+'"':"")+">"+c+"</"+b+">"},click:a.createInvokeHandler("editor.formatBlock")})]).render()}),a.memo("button.bold",function(){return c.button({className:"note-btn-bold",contents:c.icon(e.icons.bold),tooltip:f.font.bold+h("bold"),click:a.createInvokeHandlerAndUpdateState("editor.bold")}).render()}),a.memo("button.italic",function(){return c.button({className:"note-btn-italic",contents:c.icon(e.icons.italic),tooltip:f.font.italic+h("italic"),click:a.createInvokeHandlerAndUpdateState("editor.italic")}).render()}),a.memo("button.underline",function(){return c.button({className:"note-btn-underline",contents:c.icon(e.icons.underline),tooltip:f.font.underline+h("underline"),click:a.createInvokeHandlerAndUpdateState("editor.underline")}).render()}),a.memo("button.clear",function(){return c.button({contents:c.icon(e.icons.eraser),tooltip:f.font.clear+h("removeFormat"),click:a.createInvokeHandler("editor.removeFormat")}).render()}),a.memo("button.strikethrough",function(){return c.button({className:"note-btn-strikethrough",contents:c.icon(e.icons.strikethrough),tooltip:f.font.strikethrough+h("strikethrough"),click:a.createInvokeHandlerAndUpdateState("editor.strikethrough")}).render()}),a.memo("button.superscript",function(){return c.button({className:"note-btn-superscript",contents:c.icon(e.icons.superscript),tooltip:f.font.superscript,click:a.createInvokeHandlerAndUpdateState("editor.superscript")}).render()}),a.memo("button.subscript",function(){return c.button({className:"note-btn-subscript",contents:c.icon(e.icons.subscript),tooltip:f.font.subscript,click:a.createInvokeHandlerAndUpdateState("editor.subscript")}).render()}),a.memo("button.fontname",function(){return c.buttonGroup([c.button({className:"dropdown-toggle",contents:'<span class="note-current-fontname"/> '+c.icon(e.icons.caret,"span"),tooltip:f.font.name,data:{toggle:"dropdown"}}),c.dropdownCheck({className:"dropdown-fontname",checkClassName:e.icons.menuCheck,items:e.fontNames.filter(b.isFontInstalled),template:function(a){return'<span style="font-family:'+a+'">'+a+"</span>"},click:a.createInvokeHandlerAndUpdateState("editor.fontName")})]).render()}),a.memo("button.fontsize",function(){return c.buttonGroup([c.button({className:"dropdown-toggle",contents:'<span class="note-current-fontsize"/>'+c.icon(e.icons.caret,"span"),tooltip:f.font.size,data:{toggle:"dropdown"}}),c.dropdownCheck({className:"dropdown-fontsize",checkClassName:e.icons.menuCheck,items:e.fontSizes,click:a.createInvokeHandler("editor.fontSize")})]).render()}),a.memo("button.color",function(){return c.buttonGroup({className:"note-color",children:[c.button({className:"note-current-color-button",contents:c.icon(e.icons.font+" note-recent-color"),tooltip:f.color.recent,click:function(b){var c=$(b.currentTarget);a.invoke("editor.color",{backColor:c.attr("data-backColor"),foreColor:c.attr("data-foreColor")})},callback:function(a){a.find(".note-recent-color").css("background-color","#FFFF00"),a.attr("data-backColor","#FFFF00")}}),c.button({className:"dropdown-toggle",contents:c.icon(e.icons.caret,"span"),tooltip:f.color.more,data:{toggle:"dropdown"}}),c.dropdown({items:["<li>",'<div class="btn-group">','  <div class="note-palette-title">'+f.color.background+"</div>","  <div>",'    <button type="button" class="note-color-reset btn btn-default" data-event="backColor" data-value="inherit">',f.color.transparent,"    </button>","  </div>",'  <div class="note-holder" data-event="backColor"/>',"</div>",'<div class="btn-group">','  <div class="note-palette-title">'+f.color.foreground+"</div>","  <div>",'    <button type="button" class="note-color-reset btn btn-default" data-event="removeFormat" data-value="foreColor">',f.color.resetToDefault,"    </button>","  </div>",'  <div class="note-holder" data-event="foreColor"/>',"</div>","</li>"].join(""),callback:function(a){a.find(".note-holder").each(function(){var a=$(this);a.append(c.palette({colors:e.colors,eventName:a.data("event"),tooltip:e.tooltip}).render())})},click:function(b){var c=$(b.target),d=c.data("event"),e=c.data("value");if(d&&e){var f="backColor"===d?"background-color":"color",g=c.closest(".note-color").find(".note-recent-color"),h=c.closest(".note-color").find(".note-current-color-button");g.css(f,e),h.attr("data-"+d,e),a.invoke("editor."+d,e)}}})]}).render()}),a.memo("button.ul",function(){return c.button({contents:c.icon(e.icons.unorderedlist),tooltip:f.lists.unordered+h("insertUnorderedList"),click:a.createInvokeHandler("editor.insertUnorderedList")}).render()}),a.memo("button.ol",function(){return c.button({contents:c.icon(e.icons.orderedlist),tooltip:f.lists.ordered+h("insertOrderedList"),click:a.createInvokeHandler("editor.insertOrderedList")}).render()});var d=c.button({contents:c.icon(e.icons.alignLeft),tooltip:f.paragraph.left+h("justifyLeft"),click:a.createInvokeHandler("editor.justifyLeft")}),g=c.button({contents:c.icon(e.icons.alignCenter),tooltip:f.paragraph.center+h("justifyCenter"),click:a.createInvokeHandler("editor.justifyCenter")}),i=c.button({contents:c.icon(e.icons.alignRight),tooltip:f.paragraph.right+h("justifyRight"),click:a.createInvokeHandler("editor.justifyRight")}),j=c.button({contents:c.icon(e.icons.alignJustify),tooltip:f.paragraph.justify+h("justifyFull"),click:a.createInvokeHandler("editor.justifyFull")}),k=c.button({contents:c.icon(e.icons.outdent),tooltip:f.paragraph.outdent+h("outdent"),click:a.createInvokeHandler("editor.outdent")}),l=c.button({contents:c.icon(e.icons.indent),tooltip:f.paragraph.indent+h("indent"),click:a.createInvokeHandler("editor.indent")});a.memo("button.justifyLeft",func.invoke(d,"render")),a.memo("button.justifyCenter",func.invoke(g,"render")),a.memo("button.justifyRight",func.invoke(i,"render")),a.memo("button.justifyFull",func.invoke(j,"render")),a.memo("button.outdent",func.invoke(k,"render")),a.memo("button.indent",func.invoke(l,"render")),a.memo("button.paragraph",function(){return c.buttonGroup([c.button({className:"dropdown-toggle",contents:c.icon(e.icons.alignLeft)+" "+c.icon(e.icons.caret,"span"),tooltip:f.paragraph.paragraph,data:{toggle:"dropdown"}}),c.dropdown([c.buttonGroup({className:"note-align",children:[d,g,i,j]}),c.buttonGroup({className:"note-list",children:[k,l]})])]).render()}),a.memo("button.height",function(){return c.buttonGroup([c.button({className:"dropdown-toggle",contents:c.icon(e.icons.textHeight)+" "+c.icon(e.icons.caret,"span"),tooltip:f.font.height,data:{toggle:"dropdown"}}),c.dropdownCheck({items:e.lineHeights,checkClassName:e.icons.menuCheck,className:"dropdown-line-height",click:a.createInvokeHandler("editor.lineHeight")})]).render()}),a.memo("button.table",function(){return c.buttonGroup([c.button({className:"dropdown-toggle",contents:c.icon(e.icons.table)+" "+c.icon(e.icons.caret,"span"),tooltip:f.table.table,data:{toggle:"dropdown"}}),c.dropdown({className:"note-table",items:['<div class="note-dimension-picker">','  <div class="note-dimension-picker-mousecatcher" data-event="insertTable" data-value="1x1"/>','  <div class="note-dimension-picker-highlighted"/>','  <div class="note-dimension-picker-unhighlighted"/>',"</div>",'<div class="note-dimension-display">1 x 1</div>'].join("")})],{callback:function(c){c.find(".note-dimension-picker-mousecatcher").css({width:e.insertTableMaxSize.col+"em",height:e.insertTableMaxSize.row+"em"}).mousedown(a.createInvokeHandler("editor.insertTable")).on("mousemove",b.tableMoveHandler)}}).render()}),a.memo("button.link",function(){return c.button({contents:c.icon(e.icons.link),tooltip:f.link.link+h("linkDialog.show"),click:a.createInvokeHandler("linkDialog.show")}).render()}),a.memo("button.picture",function(){return c.button({contents:c.icon(e.icons.picture),tooltip:f.image.image,click:a.createInvokeHandler("imageDialog.show")}).render()}),a.memo("button.video",function(){return c.button({contents:c.icon(e.icons.video),tooltip:f.video.video,click:a.createInvokeHandler("videoDialog.show")}).render()}),a.memo("button.hr",function(){return c.button({contents:c.icon(e.icons.minus),tooltip:f.hr.insert+h("insertHorizontalRule"),click:a.createInvokeHandler("editor.insertHorizontalRule")}).render()}),a.memo("button.fullscreen",function(){return c.button({className:"btn-fullscreen",contents:c.icon(e.icons.arrowsAlt),tooltip:f.options.fullscreen,click:a.createInvokeHandler("fullscreen.toggle")}).render()}),a.memo("button.codeview",function(){return c.button({className:"btn-codeview",contents:c.icon(e.icons.code),tooltip:f.options.codeview,click:a.createInvokeHandler("codeview.toggle")}).render()}),a.memo("button.redo",function(){return c.button({contents:c.icon(e.icons.redo),tooltip:f.history.redo+h("redo"),click:a.createInvokeHandler("editor.redo")}).render()}),a.memo("button.undo",function(){return c.button({contents:c.icon(e.icons.undo),tooltip:f.history.undo+h("undo"),click:a.createInvokeHandler("editor.undo")}).render()}),a.memo("button.help",function(){return c.button({contents:c.icon(e.icons.question),tooltip:f.options.help,click:a.createInvokeHandler("helpDialog.show")}).render()})},this.addImagePopoverButtons=function(){a.memo("button.imageSize100",function(){return c.button({contents:'<span class="note-fontsize-10">100%</span>',tooltip:f.image.resizeFull,click:a.createInvokeHandler("editor.resize","1")}).render()}),a.memo("button.imageSize50",function(){return c.button({contents:'<span class="note-fontsize-10">50%</span>',tooltip:f.image.resizeHalf,click:a.createInvokeHandler("editor.resize","0.5")}).render()}),a.memo("button.imageSize25",function(){return c.button({contents:'<span class="note-fontsize-10">25%</span>',tooltip:f.image.resizeQuarter,click:a.createInvokeHandler("editor.resize","0.25")}).render()}),a.memo("button.floatLeft",function(){return c.button({contents:c.icon(e.icons.alignLeft),tooltip:f.image.floatLeft,click:a.createInvokeHandler("editor.floatMe","left")}).render()}),a.memo("button.floatRight",function(){return c.button({contents:c.icon(e.icons.alignRight),tooltip:f.image.floatRight,click:a.createInvokeHandler("editor.floatMe","right")}).render()}),a.memo("button.floatNone",function(){return c.button({contents:c.icon(e.icons.alignJustify),tooltip:f.image.floatNone,click:a.createInvokeHandler("editor.floatMe","none")}).render()}),a.memo("button.removeMedia",function(){return c.button({contents:c.icon(e.icons.trash),tooltip:f.image.remove,click:a.createInvokeHandler("editor.removeMedia")}).render()})},this.addLinkPopoverButtons=function(){a.memo("button.linkDialogShow",function(){return c.button({contents:c.icon(e.icons.link),tooltip:f.link.edit,click:a.createInvokeHandler("linkDialog.show")}).render()}),a.memo("button.unlink",function(){return c.button({contents:c.icon(e.icons.unlink),tooltip:f.link.unlink,click:a.createInvokeHandler("editor.unlink")}).render()})},this.addTablePopoverButtons=function(){a.memo("button.addRowUp",function(){return c.button({className:"btn-md",contents:c.icon(e.icons.rowAbove),tooltip:f.table.addRowAbove,click:a.createInvokeHandler("editor.addRow","top")}).render()}),a.memo("button.addRowDown",function(){return c.button({className:"btn-md",contents:c.icon(e.icons.rowBelow),tooltip:f.table.addRowBelow,click:a.createInvokeHandler("editor.addRow","bottom")}).render()}),a.memo("button.addColLeft",function(){return c.button({className:"btn-md",contents:c.icon(e.icons.colBefore),tooltip:f.table.addColLeft,click:a.createInvokeHandler("editor.addCol","left")}).render()}),a.memo("button.addColRight",function(){return c.button({className:"btn-md",contents:c.icon(e.icons.colAfter),tooltip:f.table.addColRight,click:a.createInvokeHandler("editor.addCol","right")}).render()}),a.memo("button.deleteRow",function(){return c.button({className:"btn-md",contents:c.icon(e.icons.rowRemove),tooltip:f.table.delRow,click:a.createInvokeHandler("editor.deleteRow")}).render()}),a.memo("button.deleteCol",function(){return c.button({className:"btn-md",contents:c.icon(e.icons.colRemove),tooltip:f.table.delCol,click:a.createInvokeHandler("editor.deleteCol")}).render()}),a.memo("button.deleteTable",function(){return c.button({className:"btn-md",contents:c.icon(e.icons.trash),tooltip:f.table.delTable,click:a.createInvokeHandler("editor.deleteTable")}).render()})},this.build=function(b,d){for(var e=0,f=d.length;e<f;e++){for(var g=d[e],h=g[0],i=g[1],j=c.buttonGroup({className:"note-"+h}).render(),k=0,l=i.length;k<l;k++){var m=a.memo("button."+i[k]);m&&j.append("function"==typeof m?m(a):m)}j.appendTo(b)}},this.updateCurrentStyle=function(){var c=a.invoke("editor.currentStyle");if(this.updateBtnStates({".note-btn-bold":function(){return"bold"===c["font-bold"]},".note-btn-italic":function(){return"italic"===c["font-italic"]},".note-btn-underline":function(){return"underline"===c["font-underline"]},".note-btn-subscript":function(){return"subscript"===c["font-subscript"]},".note-btn-superscript":function(){return"superscript"===c["font-superscript"]},".note-btn-strikethrough":function(){return"strikethrough"===c["font-strikethrough"]}}),c["font-family"]){var e=c["font-family"].split(",").map(function(a){return a.replace(/[\'\"]/g,"").replace(/\s+$/,"").replace(/^\s+/,"")}),f=list.find(e,b.isFontInstalled);d.find(".dropdown-fontname li a").each(function(){var a=$(this).data("value")+""==f+"";this.className=a?"checked":""}),d.find(".note-current-fontname").text(f)}if(c["font-size"]){var g=c["font-size"];d.find(".dropdown-fontsize li a").each(function(){var a=$(this).data("value")+""==g+"";this.className=a?"checked":""}),d.find(".note-current-fontsize").text(g)}if(c["line-height"]){var h=c["line-height"];d.find(".dropdown-line-height li a").each(function(){var a=$(this).data("value")+""==h+"";this.className=a?"checked":""})}},this.updateBtnStates=function(a){$.each(a,function(a,b){c.toggleBtnActive(d.find(a),b())})},this.tableMoveHandler=function(a){var b,c=$(a.target.parentNode),d=c.next(),f=c.find(".note-dimension-picker-mousecatcher"),g=c.find(".note-dimension-picker-highlighted"),h=c.find(".note-dimension-picker-unhighlighted");if(void 0===a.offsetX){var i=$(a.target).offset();b={x:a.pageX-i.left,y:a.pageY-i.top}}else b={x:a.offsetX,y:a.offsetY};var j={c:Math.ceil(b.x/18)||1,r:Math.ceil(b.y/18)||1};g.css({width:j.c+"em",height:j.r+"em"}),f.data("value",j.c+"x"+j.r),3<j.c&&j.c<e.insertTableMaxSize.col&&h.css({width:j.c+1+"em"}),3<j.r&&j.r<e.insertTableMaxSize.row&&h.css({height:j.r+1+"em"}),d.html(j.c+" x "+j.r)}},Toolbar=function(a){var b=$.summernote.ui,c=a.layoutInfo.note,d=a.layoutInfo.toolbar,e=a.options;this.shouldInitialize=function(){return!e.airMode},this.initialize=function(){e.toolbar=e.toolbar||[],e.toolbar.length?a.invoke("buttons.build",d,e.toolbar):d.hide(),e.toolbarContainer&&d.appendTo(e.toolbarContainer),c.on("summernote.keyup summernote.mouseup summernote.change",function(){a.invoke("buttons.updateCurrentStyle")}),a.invoke("buttons.updateCurrentStyle")},this.destroy=function(){d.children().remove()},this.updateFullscreen=function(a){b.toggleBtnActive(d.find(".btn-fullscreen"),a)},this.updateCodeview=function(a){b.toggleBtnActive(d.find(".btn-codeview"),a),a?this.deactivate():this.activate()},this.activate=function(a){var c=d.find("button");a||(c=c.not(".btn-codeview")),b.toggleBtn(c,!0)},this.deactivate=function(a){var c=d.find("button");a||(c=c.not(".btn-codeview")),b.toggleBtn(c,!1)}},LinkDialog=function(a){var b=this,c=$.summernote.ui,d=a.layoutInfo.editor,e=a.options,f=e.langInfo;this.initialize=function(){var a=e.dialogsInBody?$(document.body):d,b='<div class="form-group"><label>'+f.link.textToDisplay+'</label><input class="note-link-text form-control" type="text" /></div><div class="form-group"><label>'+f.link.url+'</label><input class="note-link-url form-control" type="text" value="http://" /></div>'+(e.disableLinkTarget?"":'<div class="checkbox"><label for="sn-checkbox-open-in-new-window"><input type="checkbox" id="sn-checkbox-open-in-new-window" checked />'+f.link.openInNewWindow+"</label></div>"),g='<button href="#" class="btn btn-primary note-link-btn disabled" disabled>'+f.link.insert+"</button>";this.$dialog=c.dialog({className:"link-dialog",title:f.link.insert,fade:e.dialogsFade,body:b,footer:g}).render().appendTo(a)},this.destroy=function(){c.hideDialog(this.$dialog),this.$dialog.remove()},this.bindEnterKey=function(a,b){a.on("keypress",function(a){a.keyCode===key.code.ENTER&&b.trigger("click")})},this.toggleLinkBtn=function(a,b,d){c.toggleBtn(a,b.val()&&d.val())},this.showLinkDialog=function(d){return $.Deferred(function(e){var f=b.$dialog.find(".note-link-text"),g=b.$dialog.find(".note-link-url"),h=b.$dialog.find(".note-link-btn"),i=b.$dialog.find("input[type=checkbox]");c.onDialogShown(b.$dialog,function(){a.triggerEvent("dialog.shown"),d.url||(d.url=d.text),f.val(d.text);var c=function(){b.toggleLinkBtn(h,f,g),d.text=f.val()};f.on("input",c).on("paste",function(){setTimeout(c,0)});var j=function(){b.toggleLinkBtn(h,f,g),d.text||f.val(g.val())};g.on("input",j).on("paste",function(){setTimeout(j,0)}).val(d.url).trigger("focus"),b.toggleLinkBtn(h,f,g),b.bindEnterKey(g,h),b.bindEnterKey(f,h);var k=void 0!==d.isNewWindow?d.isNewWindow:a.options.linkTargetBlank;i.prop("checked",k),h.one("click",function(a){a.preventDefault(),e.resolve({range:d.range,url:g.val(),text:f.val(),isNewWindow:i.is(":checked")}),b.$dialog.modal("hide")})}),c.onDialogHidden(b.$dialog,function(){f.off("input paste keypress"),g.off("input paste keypress"),h.off("click"),"pending"===e.state()&&e.reject()}),c.showDialog(b.$dialog)}).promise()},this.show=function(){var b=a.invoke("editor.getLinkInfo");a.invoke("editor.saveRange"),this.showLinkDialog(b).then(function(b){a.invoke("editor.restoreRange"),a.invoke("editor.createLink",b)}).fail(function(){a.invoke("editor.restoreRange")})},a.memo("help.linkDialog.show",e.langInfo.help["linkDialog.show"])},LinkPopover=function(a){var b=this,c=$.summernote.ui,d=a.options;this.events={"summernote.keyup summernote.mouseup summernote.change summernote.scroll":function(){b.update()},"summernote.disable summernote.dialog.shown":function(){b.hide()}},this.shouldInitialize=function(){return!list.isEmpty(d.popover.link)},this.initialize=function(){this.$popover=c.popover({className:"note-link-popover",callback:function(a){a.find(".popover-content").prepend('<span><a target="_blank"></a>&nbsp;</span>')}}).render().appendTo("body");var b=this.$popover.find(".popover-content");a.invoke("buttons.build",b,d.popover.link)},this.destroy=function(){this.$popover.remove()},this.update=function(){if(!a.invoke("editor.hasFocus"))return void this.hide();var b=a.invoke("editor.createRange");if(b.isCollapsed()&&b.isOnAnchor()){var c=dom.ancestor(b.sc,dom.isAnchor),d=$(c).attr("href");this.$popover.find("a").attr("href",d).html(d);var e=dom.posFromPlaceholder(c);this.$popover.css({display:"block",left:e.left,top:e.top})}else this.hide()},this.hide=function(){this.$popover.hide()}},ImageDialog=function(a){var b=this,c=$.summernote.ui,d=a.layoutInfo.editor,e=a.options,f=e.langInfo;this.initialize=function(){var a=e.dialogsInBody?$(document.body):d,b="";if(e.maximumImageFileSize){var g=Math.floor(Math.log(e.maximumImageFileSize)/Math.log(1024)),h=1*(e.maximumImageFileSize/Math.pow(1024,g)).toFixed(2)+" "+" KMGTP"[g]+"B";b="<small>"+f.image.maximumFileSize+" : "+h+"</small>"}var i='<div class="form-group note-group-select-from-files"><label>'+f.image.selectFromFiles+'</label><input class="note-image-input form-control" type="file" name="files" accept="image/*" multiple="multiple" />'+b+'</div><div class="form-group note-group-image-url" style="overflow:auto;"><label>'+f.image.url+'</label><input class="note-image-url form-control col-md-12" type="text" /></div>',j='<button href="#" class="btn btn-primary note-image-btn disabled" disabled>'+f.image.insert+"</button>";this.$dialog=c.dialog({title:f.image.insert,fade:e.dialogsFade,body:i,footer:j}).render().appendTo(a)},this.destroy=function(){c.hideDialog(this.$dialog),this.$dialog.remove()},this.bindEnterKey=function(a,b){a.on("keypress",function(a){a.keyCode===key.code.ENTER&&b.trigger("click")})},this.show=function(){a.invoke("editor.saveRange"),this.showImageDialog().then(function(d){c.hideDialog(b.$dialog),a.invoke("editor.restoreRange"),"string"==typeof d?a.invoke("editor.insertImage",d):a.invoke("editor.insertImagesOrCallback",d)}).fail(function(){a.invoke("editor.restoreRange")})},this.showImageDialog=function(){return $.Deferred(function(d){var e=b.$dialog.find(".note-image-input"),f=b.$dialog.find(".note-image-url"),g=b.$dialog.find(".note-image-btn");c.onDialogShown(b.$dialog,function(){a.triggerEvent("dialog.shown"),e.replaceWith(e.clone().on("change",function(){d.resolve(this.files||this.value)}).val("")),g.click(function(a){a.preventDefault(),d.resolve(f.val())}),f.on("keyup paste",function(){var a=f.val();c.toggleBtn(g,a)}).val("").trigger("focus"),b.bindEnterKey(f,g)}),c.onDialogHidden(b.$dialog,function(){e.off("change"),f.off("keyup paste keypress"),g.off("click"),"pending"===d.state()&&d.reject()}),c.showDialog(b.$dialog)})}},ImagePopover=function(a){var b=this,c=$.summernote.ui,d=a.layoutInfo.editable,e=d[0],f=a.options;this.events={"summernote.disable":function(){b.hide()}},this.shouldInitialize=function(){return!list.isEmpty(f.popover.image)},this.initialize=function(){this.$popover=c.popover({className:"note-image-popover"}).render().appendTo("body");var b=this.$popover.find(".popover-content");a.invoke("buttons.build",b,f.popover.image)},this.destroy=function(){this.$popover.remove()},this.update=function(a){if(dom.isImg(a)){var b=dom.posFromPlaceholder(a),c=dom.posFromPlaceholder(e);this.$popover.css({display:"block",left:b.left,top:Math.min(b.top,c.top)})}else this.hide()},this.hide=function(){this.$popover.hide()}},TablePopover=function(a){var b=this,c=$.summernote.ui,d=a.options;this.events={"summernote.mousedown":function(a,c){b.update(c.target)},"summernote.keyup summernote.scroll summernote.change":function(){b.update()},"summernote.disable":function(){b.hide()}},this.shouldInitialize=function(){return!list.isEmpty(d.popover.table)},this.initialize=function(){this.$popover=c.popover({className:"note-table-popover"}).render().appendTo("body");var b=this.$popover.find(".popover-content");a.invoke("buttons.build",b,d.popover.table),agent.isFF&&document.execCommand("enableInlineTableEditing",!1,!1)},this.destroy=function(){this.$popover.remove()},this.update=function(b){if(a.isDisabled())return!1;var c=dom.isCell(b);if(c){var d=dom.posFromPlaceholder(b);this.$popover.css({display:"block",left:d.left,top:d.top})}else this.hide();return c},this.hide=function(){this.$popover.hide()}},VideoDialog=function(a){var b=this,c=$.summernote.ui,d=a.layoutInfo.editor,e=a.options,f=e.langInfo;this.initialize=function(){var a=e.dialogsInBody?$(document.body):d,b='<div class="form-group row-fluid"><label>'+f.video.url+' <small class="text-muted">'+f.video.providers+'</small></label><input class="note-video-url form-control span12" type="text" /></div>',g='<button href="#" class="btn btn-primary note-video-btn disabled" disabled>'+f.video.insert+"</button>";this.$dialog=c.dialog({title:f.video.insert,fade:e.dialogsFade,body:b,footer:g}).render().appendTo(a)},this.destroy=function(){c.hideDialog(this.$dialog),this.$dialog.remove()},this.bindEnterKey=function(a,b){a.on("keypress",function(a){a.keyCode===key.code.ENTER&&b.trigger("click")})},this.createVideoNode=function(a){var b,c=/^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/,d=a.match(c),e=/(?:www\.|\/\/)instagram\.com\/p\/(.[a-zA-Z0-9_-]*)/,f=a.match(e),g=/\/\/vine\.co\/v\/([a-zA-Z0-9]+)/,h=a.match(g),i=/\/\/(player\.)?vimeo\.com\/([a-z]*\/)*(\d+)[?]?.*/,j=a.match(i),k=/.+dailymotion.com\/(video|hub)\/([^_]+)[^#]*(#video=([^_&]+))?/,l=a.match(k),m=/\/\/v\.youku\.com\/v_show\/id_(\w+)=*\.html/,n=a.match(m),o=/\/\/v\.qq\.com.*?vid=(.+)/,p=a.match(o),q=/\/\/v\.qq\.com\/x?\/?(page|cover).*?\/([^\/]+)\.html\??.*/,r=a.match(q),s=/^.+.(mp4|m4v)$/,t=a.match(s),u=/^.+.(ogg|ogv)$/,v=a.match(u),w=/^.+.(webm)$/,x=a.match(w);if(d&&11===d[1].length){var y=d[1];b=$("<iframe>").attr("frameborder",0).attr("src","//www.youtube.com/embed/"+y).attr("width","640").attr("height","360")}else if(f&&f[0].length)b=$("<iframe>").attr("frameborder",0).attr("src","https://instagram.com/p/"+f[1]+"/embed/").attr("width","612").attr("height","710").attr("scrolling","no").attr("allowtransparency","true");else if(h&&h[0].length)b=$("<iframe>").attr("frameborder",0).attr("src",h[0]+"/embed/simple").attr("width","600").attr("height","600").attr("class","vine-embed");else if(j&&j[3].length)b=$("<iframe webkitallowfullscreen mozallowfullscreen allowfullscreen>").attr("frameborder",0).attr("src","//player.vimeo.com/video/"+j[3]).attr("width","640").attr("height","360");else if(l&&l[2].length)b=$("<iframe>").attr("frameborder",0).attr("src","//www.dailymotion.com/embed/video/"+l[2]).attr("width","640").attr("height","360");else if(n&&n[1].length)b=$("<iframe webkitallowfullscreen mozallowfullscreen allowfullscreen>").attr("frameborder",0).attr("height","498").attr("width","510").attr("src","//player.youku.com/embed/"+n[1]);else if(p&&p[1].length||r&&r[2].length){var z=p&&p[1].length?p[1]:r[2];b=$("<iframe webkitallowfullscreen mozallowfullscreen allowfullscreen>").attr("frameborder",0).attr("height","310").attr("width","500").attr("src","http://v.qq.com/iframe/player.html?vid="+z+"&amp;auto=0")}else{if(!(t||v||x))return!1;b=$("<video controls>").attr("src",a).attr("width","640").attr("height","360")}return b.addClass("note-video-clip"),b[0]},this.show=function(){var d=a.invoke("editor.getSelectedText");a.invoke("editor.saveRange"),this.showVideoDialog(d).then(function(d){c.hideDialog(b.$dialog),a.invoke("editor.restoreRange");var e=b.createVideoNode(d);e&&a.invoke("editor.insertNode",e)}).fail(function(){a.invoke("editor.restoreRange")})},this.showVideoDialog=function(d){return $.Deferred(function(e){var f=b.$dialog.find(".note-video-url"),g=b.$dialog.find(".note-video-btn");c.onDialogShown(b.$dialog,function(){a.triggerEvent("dialog.shown"),f.val(d).on("input",function(){c.toggleBtn(g,f.val())}).trigger("focus"),g.click(function(a){a.preventDefault(),e.resolve(f.val())}),b.bindEnterKey(f,g)}),c.onDialogHidden(b.$dialog,function(){f.off("input"),g.off("click"),"pending"===e.state()&&e.reject()}),c.showDialog(b.$dialog)})}},HelpDialog=function(a){var b=this,c=$.summernote.ui,d=a.layoutInfo.editor,e=a.options,f=e.langInfo;this.createShortCutList=function(){var b=e.keyMap[agent.isMac?"mac":"pc"];return Object.keys(b).map(function(c){var d=b[c],e=$('<div><div class="help-list-item"/></div>');return e.append($("<label><kbd>"+c+"</kdb></label>").css({width:180,"margin-right":10})).append($("<span/>").html(a.memo("help."+d)||d)),e.html()}).join("")},this.initialize=function(){var a=e.dialogsInBody?$(document.body):d,b=['<p class="text-center">','<a href="http://summernote.org/" target="_blank">Summernote 0.8.7</a> · ','<a href="https://github.com/summernote/summernote" target="_blank">Project</a> · ','<a href="https://github.com/summernote/summernote/issues" target="_blank">Issues</a>',"</p>"].join("");this.$dialog=c.dialog({title:f.options.help,fade:e.dialogsFade,body:this.createShortCutList(),footer:b,callback:function(a){a.find(".modal-body").css({"max-height":300,overflow:"scroll"})}}).render().appendTo(a)},this.destroy=function(){c.hideDialog(this.$dialog),this.$dialog.remove()},this.showHelpDialog=function(){return $.Deferred(function(d){c.onDialogShown(b.$dialog,function(){a.triggerEvent("dialog.shown"),d.resolve()}),c.showDialog(b.$dialog)}).promise()},this.show=function(){a.invoke("editor.saveRange"),this.showHelpDialog().then(function(){a.invoke("editor.restoreRange")})}},AirPopover=function(a){var b=this,c=$.summernote.ui,d=a.options;this.events={"summernote.keyup summernote.mouseup summernote.scroll":function(){b.update()},"summernote.disable summernote.change summernote.dialog.shown":function(){b.hide()},"summernote.focusout":function(a,c){agent.isFF||c.relatedTarget&&dom.ancestor(c.relatedTarget,func.eq(b.$popover[0]))||b.hide()}},this.shouldInitialize=function(){return d.airMode&&!list.isEmpty(d.popover.air)},this.initialize=function(){this.$popover=c.popover({className:"note-air-popover"}).render().appendTo("body");var b=this.$popover.find(".popover-content");a.invoke("buttons.build",b,d.popover.air)},this.destroy=function(){this.$popover.remove()},this.update=function(){var b=a.invoke("editor.currentStyle");if(b.range&&!b.range.isCollapsed()){var c=list.last(b.range.getClientRects());if(c){var d=func.rect2bnd(c);this.$popover.css({display:"block",left:Math.max(d.left+d.width/2,0)-20,top:d.top+d.height})}}else this.hide()},this.hide=function(){this.$popover.hide()}},HintPopover=function(a){var b=this,c=$.summernote.ui,d=a.options.hint||[],e=a.options.hintDirection||"bottom",f=$.isArray(d)?d:[d];this.events={"summernote.keyup":function(a,c){c.isDefaultPrevented()||b.handleKeyup(c)},"summernote.keydown":function(a,c){b.handleKeydown(c)},"summernote.disable summernote.dialog.shown":function(){b.hide()}},this.shouldInitialize=function(){return f.length>0},this.initialize=function(){this.lastWordRange=null,
this.$popover=c.popover({className:"note-hint-popover",hideArrow:!0,direction:""}).render().appendTo("body"),this.$popover.hide(),this.$content=this.$popover.find(".popover-content"),this.$content.on("click",".note-hint-item",function(){b.$content.find(".active").removeClass("active"),$(this).addClass("active"),b.replace()})},this.destroy=function(){this.$popover.remove()},this.selectItem=function(a){this.$content.find(".active").removeClass("active"),a.addClass("active"),this.$content[0].scrollTop=a[0].offsetTop-this.$content.innerHeight()/2},this.moveDown=function(){var a=this.$content.find(".note-hint-item.active"),b=a.next();if(b.length)this.selectItem(b);else{var c=a.parent().next();c.length||(c=this.$content.find(".note-hint-group").first()),this.selectItem(c.find(".note-hint-item").first())}},this.moveUp=function(){var a=this.$content.find(".note-hint-item.active"),b=a.prev();if(b.length)this.selectItem(b);else{var c=a.parent().prev();c.length||(c=this.$content.find(".note-hint-group").last()),this.selectItem(c.find(".note-hint-item").last())}},this.replace=function(){var b=this.$content.find(".note-hint-item.active");if(b.length){var c=this.nodeFromItem(b);this.lastWordRange.insertNode(c),range.createFromNode(c).collapse().select(),this.lastWordRange=null,this.hide(),a.triggerEvent("change",a.layoutInfo.editable.html(),a.layoutInfo.editable),a.invoke("editor.focus")}},this.nodeFromItem=function(a){var b=f[a.data("index")],c=a.data("item"),d=b.content?b.content(c):c;return"string"==typeof d&&(d=dom.createText(d)),d},this.createItemTemplates=function(a,b){var c=f[a];return b.map(function(b,d){var e=$('<div class="note-hint-item"/>');return e.append(c.template?c.template(b):b+""),e.data({index:a,item:b}),0===a&&0===d&&e.addClass("active"),e})},this.handleKeydown=function(a){this.$popover.is(":visible")&&(a.keyCode===key.code.ENTER?(a.preventDefault(),this.replace()):a.keyCode===key.code.UP?(a.preventDefault(),this.moveUp()):a.keyCode===key.code.DOWN&&(a.preventDefault(),this.moveDown()))},this.searchKeyword=function(a,b,c){var d=f[a];if(d&&d.match.test(b)&&d.search){var e=d.match.exec(b);d.search(e[1],c)}else c()},this.createGroup=function(a,c){var d=$('<div class="note-hint-group note-hint-group-'+a+'"/>');return this.searchKeyword(a,c,function(c){c=c||[],c.length&&(d.html(b.createItemTemplates(a,c)),b.show())}),d},this.handleKeyup=function(c){if(list.contains([key.code.ENTER,key.code.UP,key.code.DOWN],c.keyCode)){if(c.keyCode===key.code.ENTER&&this.$popover.is(":visible"))return}else{var d=a.invoke("editor.createRange").getWordRange(),g=d.toString();if(f.length&&g){this.$content.empty();var h=func.rect2bnd(list.last(d.getClientRects()));h&&(this.$popover.hide(),this.lastWordRange=d,f.forEach(function(a,c){a.match.test(g)&&b.createGroup(c,g).appendTo(b.$content)}),"top"===e?this.$popover.css({left:h.left,top:h.top-this.$popover.outerHeight()-5}):this.$popover.css({left:h.left,top:h.top+h.height+5}))}else this.hide()}},this.show=function(){this.$popover.show()},this.hide=function(){this.$popover.hide()}};$.summernote=$.extend($.summernote,{version:"0.8.7",ui:ui,dom:dom,plugins:{},options:{modules:{editor:Editor,clipboard:Clipboard,dropzone:Dropzone,codeview:Codeview,statusbar:Statusbar,fullscreen:Fullscreen,handle:Handle,hintPopover:HintPopover,autoLink:AutoLink,autoSync:AutoSync,placeholder:Placeholder,buttons:Buttons,toolbar:Toolbar,linkDialog:LinkDialog,linkPopover:LinkPopover,imageDialog:ImageDialog,imagePopover:ImagePopover,tablePopover:TablePopover,videoDialog:VideoDialog,helpDialog:HelpDialog,airPopover:AirPopover},buttons:{},lang:"en-US",toolbar:[["style",["style"]],["font",["bold","underline","clear"]],["fontname",["fontname"]],["color",["color"]],["para",["ul","ol","paragraph"]],["table",["table"]],["insert",["link","picture","video"]],["view",["fullscreen","codeview","help"]]],popover:{image:[["imagesize",["imageSize100","imageSize50","imageSize25"]],["float",["floatLeft","floatRight","floatNone"]],["remove",["removeMedia"]]],link:[["link",["linkDialogShow","unlink"]]],table:[["add",["addRowDown","addRowUp","addColLeft","addColRight"]],["delete",["deleteRow","deleteCol","deleteTable"]]],air:[["color",["color"]],["font",["bold","underline","clear"]],["para",["ul","paragraph"]],["table",["table"]],["insert",["link","picture"]]]},airMode:!1,width:null,height:null,linkTargetBlank:!0,focus:!1,tabSize:4,styleWithSpan:!0,shortcuts:!0,textareaAutoSync:!0,direction:null,tooltip:"auto",styleTags:["p","blockquote","pre","h1","h2","h3","h4","h5","h6"],fontNames:["Arial","Arial Black","Comic Sans MS","Courier New","Helvetica Neue","Helvetica","Impact","Lucida Grande","Tahoma","Times New Roman","Verdana"],fontSizes:["8","9","10","11","12","14","18","24","36"],colors:[["#000000","#424242","#636363","#9C9C94","#CEC6CE","#EFEFEF","#F7F7F7","#FFFFFF"],["#FF0000","#FF9C00","#FFFF00","#00FF00","#00FFFF","#0000FF","#9C00FF","#FF00FF"],["#F7C6CE","#FFE7CE","#FFEFC6","#D6EFD6","#CEDEE7","#CEE7F7","#D6D6E7","#E7D6DE"],["#E79C9C","#FFC69C","#FFE79C","#B5D6A5","#A5C6CE","#9CC6EF","#B5A5D6","#D6A5BD"],["#E76363","#F7AD6B","#FFD663","#94BD7B","#73A5AD","#6BADDE","#8C7BC6","#C67BA5"],["#CE0000","#E79439","#EFC631","#6BA54A","#4A7B8C","#3984C6","#634AA5","#A54A7B"],["#9C0000","#B56308","#BD9400","#397B21","#104A5A","#085294","#311873","#731842"],["#630000","#7B3900","#846300","#295218","#083139","#003163","#21104A","#4A1031"]],lineHeights:["1.0","1.2","1.4","1.5","1.6","1.8","2.0","3.0"],tableClassName:"table table-bordered",insertTableMaxSize:{col:10,row:10},dialogsInBody:!1,dialogsFade:!1,maximumImageFileSize:null,callbacks:{onInit:null,onFocus:null,onBlur:null,onEnter:null,onKeyup:null,onKeydown:null,onImageUpload:null,onImageUploadError:null},codemirror:{mode:"text/html",htmlMode:!0,lineNumbers:!0},keyMap:{pc:{ENTER:"insertParagraph","CTRL+Z":"undo","CTRL+Y":"redo",TAB:"tab","SHIFT+TAB":"untab","CTRL+B":"bold","CTRL+I":"italic","CTRL+U":"underline","CTRL+SHIFT+S":"strikethrough","CTRL+BACKSLASH":"removeFormat","CTRL+SHIFT+L":"justifyLeft","CTRL+SHIFT+E":"justifyCenter","CTRL+SHIFT+R":"justifyRight","CTRL+SHIFT+J":"justifyFull","CTRL+SHIFT+NUM7":"insertUnorderedList","CTRL+SHIFT+NUM8":"insertOrderedList","CTRL+LEFTBRACKET":"outdent","CTRL+RIGHTBRACKET":"indent","CTRL+NUM0":"formatPara","CTRL+NUM1":"formatH1","CTRL+NUM2":"formatH2","CTRL+NUM3":"formatH3","CTRL+NUM4":"formatH4","CTRL+NUM5":"formatH5","CTRL+NUM6":"formatH6","CTRL+ENTER":"insertHorizontalRule","CTRL+K":"linkDialog.show"},mac:{ENTER:"insertParagraph","CMD+Z":"undo","CMD+SHIFT+Z":"redo",TAB:"tab","SHIFT+TAB":"untab","CMD+B":"bold","CMD+I":"italic","CMD+U":"underline","CMD+SHIFT+S":"strikethrough","CMD+BACKSLASH":"removeFormat","CMD+SHIFT+L":"justifyLeft","CMD+SHIFT+E":"justifyCenter","CMD+SHIFT+R":"justifyRight","CMD+SHIFT+J":"justifyFull","CMD+SHIFT+NUM7":"insertUnorderedList","CMD+SHIFT+NUM8":"insertOrderedList","CMD+LEFTBRACKET":"outdent","CMD+RIGHTBRACKET":"indent","CMD+NUM0":"formatPara","CMD+NUM1":"formatH1","CMD+NUM2":"formatH2","CMD+NUM3":"formatH3","CMD+NUM4":"formatH4","CMD+NUM5":"formatH5","CMD+NUM6":"formatH6","CMD+ENTER":"insertHorizontalRule","CMD+K":"linkDialog.show"}},icons:{align:"note-icon-align",alignCenter:"note-icon-align-center",alignJustify:"note-icon-align-justify",alignLeft:"note-icon-align-left",alignRight:"note-icon-align-right",rowBelow:"note-icon-row-below",colBefore:"note-icon-col-before",colAfter:"note-icon-col-after",rowAbove:"note-icon-row-above",rowRemove:"note-icon-row-remove",colRemove:"note-icon-col-remove",indent:"note-icon-align-indent",outdent:"note-icon-align-outdent",arrowsAlt:"note-icon-arrows-alt",bold:"note-icon-bold",caret:"note-icon-caret",circle:"note-icon-circle",close:"note-icon-close",code:"note-icon-code",eraser:"note-icon-eraser",font:"note-icon-font",frame:"note-icon-frame",italic:"note-icon-italic",link:"note-icon-link",unlink:"note-icon-chain-broken",magic:"note-icon-magic",menuCheck:"note-icon-check",minus:"note-icon-minus",orderedlist:"note-icon-orderedlist",pencil:"note-icon-pencil",picture:"note-icon-picture",question:"note-icon-question",redo:"note-icon-redo",square:"note-icon-square",strikethrough:"note-icon-strikethrough",subscript:"note-icon-subscript",superscript:"note-icon-superscript",table:"note-icon-table",textHeight:"note-icon-text-height",trash:"note-icon-trash",underline:"note-icon-underline",undo:"note-icon-undo",unorderedlist:"note-icon-unorderedlist",video:"note-icon-video"}}})});
/* ===========================================================
 * Bootstrap: fileinput.js v3.1.3
 * http://jasny.github.com/bootstrap/javascript/#fileinput
 * ===========================================================
 * Copyright 2012-2014 Arnold Daniels
 *
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */

+function ($) { "use strict";

  var isIE = window.navigator.appName == 'Microsoft Internet Explorer'

  // FILEUPLOAD PUBLIC CLASS DEFINITION
  // =================================

  var Fileinput = function (element, options) {
    this.$element = $(element)
    
    this.$input = this.$element.find(':file')
    if (this.$input.length === 0) return

    this.name = this.$input.attr('name') || options.name

    this.$hidden = this.$element.find('input[type=hidden][name="' + this.name + '"]')
    if (this.$hidden.length === 0) {
      this.$hidden = $('<input type="hidden">').insertBefore(this.$input)
    }

    this.$preview = this.$element.find('.fileinput-preview')
    var height = this.$preview.css('height')
    if (this.$preview.css('display') !== 'inline' && height !== '0px' && height !== 'none') {
      this.$preview.css('line-height', height)
    }
        
    this.original = {
      exists: this.$element.hasClass('fileinput-exists'),
      preview: this.$preview.html(),
      hiddenVal: this.$hidden.val()
    }
    
    this.listen()
  }
  
  Fileinput.prototype.listen = function() {
    this.$input.on('change.bs.fileinput', $.proxy(this.change, this))
    $(this.$input[0].form).on('reset.bs.fileinput', $.proxy(this.reset, this))
    
    this.$element.find('[data-trigger="fileinput"]').on('click.bs.fileinput', $.proxy(this.trigger, this))
    this.$element.find('[data-dismiss="fileinput"]').on('click.bs.fileinput', $.proxy(this.clear, this))
  },

  Fileinput.prototype.change = function(e) {
    var files = e.target.files === undefined ? (e.target && e.target.value ? [{ name: e.target.value.replace(/^.+\\/, '')}] : []) : e.target.files
    
    e.stopPropagation()

    if (files.length === 0) {
      this.clear()
      return
    }

    this.$hidden.val('')
    this.$hidden.attr('name', '')
    this.$input.attr('name', this.name)

    var file = files[0]

    if (this.$preview.length > 0 && (typeof file.type !== "undefined" ? file.type.match(/^image\/(gif|png|jpeg)$/) : file.name.match(/\.(gif|png|jpe?g)$/i)) && typeof FileReader !== "undefined") {
      var reader = new FileReader()
      var preview = this.$preview
      var element = this.$element

      reader.onload = function(re) {
        var $img = $('<img>')
        $img[0].src = re.target.result
        files[0].result = re.target.result
        
        element.find('.fileinput-filename').text(file.name)
        
        // if parent has max-height, using `(max-)height: 100%` on child doesn't take padding and border into account
        if (preview.css('max-height') != 'none') $img.css('max-height', parseInt(preview.css('max-height'), 10) - parseInt(preview.css('padding-top'), 10) - parseInt(preview.css('padding-bottom'), 10)  - parseInt(preview.css('border-top'), 10) - parseInt(preview.css('border-bottom'), 10))
        
        preview.html($img)
        element.addClass('fileinput-exists').removeClass('fileinput-new')

        element.trigger('change.bs.fileinput', files)
      }

      reader.readAsDataURL(file)
    } else {
      this.$element.find('.fileinput-filename').text(file.name)
      this.$preview.text(file.name)
      
      this.$element.addClass('fileinput-exists').removeClass('fileinput-new')
      
      this.$element.trigger('change.bs.fileinput')
    }
  },

  Fileinput.prototype.clear = function(e) {
    if (e) e.preventDefault()
    
    this.$hidden.val('')
    this.$hidden.attr('name', this.name)
    this.$input.attr('name', '')

    //ie8+ doesn't support changing the value of input with type=file so clone instead
    if (isIE) { 
      var inputClone = this.$input.clone(true);
      this.$input.after(inputClone);
      this.$input.remove();
      this.$input = inputClone;
    } else {
      this.$input.val('')
    }

    this.$preview.html('')
    this.$element.find('.fileinput-filename').text('')
    this.$element.addClass('fileinput-new').removeClass('fileinput-exists')
    
    if (e !== undefined) {
      this.$input.trigger('change')
      this.$element.trigger('clear.bs.fileinput')
    }
  },

  Fileinput.prototype.reset = function() {
    this.clear()

    this.$hidden.val(this.original.hiddenVal)
    this.$preview.html(this.original.preview)
    this.$element.find('.fileinput-filename').text('')

    if (this.original.exists) this.$element.addClass('fileinput-exists').removeClass('fileinput-new')
     else this.$element.addClass('fileinput-new').removeClass('fileinput-exists')
    
    this.$element.trigger('reset.bs.fileinput')
  },

  Fileinput.prototype.trigger = function(e) {
    this.$input.trigger('click')
    e.preventDefault()
  }

  
  // FILEUPLOAD PLUGIN DEFINITION
  // ===========================

  var old = $.fn.fileinput
  
  $.fn.fileinput = function (options) {
    return this.each(function () {
      var $this = $(this),
          data = $this.data('bs.fileinput')
      if (!data) $this.data('bs.fileinput', (data = new Fileinput(this, options)))
      if (typeof options == 'string') data[options]()
    })
  }

  $.fn.fileinput.Constructor = Fileinput


  // FILEINPUT NO CONFLICT
  // ====================

  $.fn.fileinput.noConflict = function () {
    $.fn.fileinput = old
    return this
  }


  // FILEUPLOAD DATA-API
  // ==================

  $(document).on('click.fileinput.data-api', '[data-provides="fileinput"]', function (e) {
    var $this = $(this)
    if ($this.data('bs.fileinput')) return
    $this.fileinput($this.data())
      
    var $target = $(e.target).closest('[data-dismiss="fileinput"],[data-trigger="fileinput"]');
    if ($target.length > 0) {
      e.preventDefault()
      $target.trigger('click.bs.fileinput')
    }
  })

}(window.jQuery);
(function ($) {
  "use strict";

  var defaultOptions = {
    tagClass: function(item) {
      return 'label label-info';
    },
    itemValue: function(item) {
      return item ? item.toString() : item;
    },
    itemText: function(item) {
      return this.itemValue(item);
    },
    freeInput: true,
    addOnBlur: true,
    maxTags: undefined,
    maxChars: undefined,
    confirmKeys: [13, 44],
    onTagExists: function(item, $tag) {
      $tag.hide().fadeIn();
    },
    trimValue: false,
    allowDuplicates: false
  };

  /**
   * Constructor function
   */
  function TagsInput(element, options) {
    this.itemsArray = [];

    this.$element = $(element);
    this.$element.hide();

    this.isSelect = (element.tagName === 'SELECT');
    this.multiple = (this.isSelect && element.hasAttribute('multiple'));
    this.objectItems = options && options.itemValue;
    this.placeholderText = element.hasAttribute('placeholder') ? this.$element.attr('placeholder') : '';
    this.inputSize = Math.max(1, this.placeholderText.length);

    this.$container = $('<div class="bootstrap-tagsinput"></div>');
    this.$input = $('<input type="text" placeholder="' + this.placeholderText + '"/>').appendTo(this.$container);

    this.$element.after(this.$container);

    var inputWidth = (this.inputSize < 3 ? 3 : this.inputSize) + "em";
    this.$input.get(0).style.cssText = "width: " + inputWidth + " !important;";
    this.build(options);
  }

  TagsInput.prototype = {
    constructor: TagsInput,

    /**
     * Adds the given item as a new tag. Pass true to dontPushVal to prevent
     * updating the elements val()
     */
    add: function(item, dontPushVal) {
      var self = this;

      if (self.options.maxTags && self.itemsArray.length >= self.options.maxTags)
        return;

      // Ignore falsey values, except false
      if (item !== false && !item)
        return;

      // Trim value
      if (typeof item === "string" && self.options.trimValue) {
        item = $.trim(item);
      }

      // Throw an error when trying to add an object while the itemValue option was not set
      if (typeof item === "object" && !self.objectItems)
        throw("Can't add objects when itemValue option is not set");

      // Ignore strings only containg whitespace
      if (item.toString().match(/^\s*$/))
        return;

      // If SELECT but not multiple, remove current tag
      if (self.isSelect && !self.multiple && self.itemsArray.length > 0)
        self.remove(self.itemsArray[0]);

      if (typeof item === "string" && this.$element[0].tagName === 'INPUT') {
        var items = item.split(',');
        if (items.length > 1) {
          for (var i = 0; i < items.length; i++) {
            this.add(items[i], true);
          }

          if (!dontPushVal)
            self.pushVal();
          return;
        }
      }

      var itemValue = self.options.itemValue(item),
          itemText = self.options.itemText(item),
          tagClass = self.options.tagClass(item);

      // Ignore items allready added
      var existing = $.grep(self.itemsArray, function(item) { return self.options.itemValue(item) === itemValue; } )[0];
      if (existing && !self.options.allowDuplicates) {
        // Invoke onTagExists
        if (self.options.onTagExists) {
          var $existingTag = $(".tag", self.$container).filter(function() { return $(this).data("item") === existing; });
          self.options.onTagExists(item, $existingTag);
        }
        return;
      }

      // if length greater than limit
      if (self.items().toString().length + item.length + 1 > self.options.maxInputLength)
        return;

      // raise beforeItemAdd arg
      var beforeItemAddEvent = $.Event('beforeItemAdd', { item: item, cancel: false });
      self.$element.trigger(beforeItemAddEvent);
      if (beforeItemAddEvent.cancel)
        return;

      // register item in internal array and map
      self.itemsArray.push(item);

      // add a tag element
      var $tag = $('<span class="tag ' + htmlEncode(tagClass) + '">' + htmlEncode(itemText) + '<span data-role="remove"></span></span>');
      $tag.data('item', item);
      self.findInputWrapper().before($tag);
      $tag.after(' ');

      // add <option /> if item represents a value not present in one of the <select />'s options
      if (self.isSelect && !$('option[value="' + encodeURIComponent(itemValue) + '"]',self.$element)[0]) {
        var $option = $('<option selected>' + htmlEncode(itemText) + '</option>');
        $option.data('item', item);
        $option.attr('value', itemValue);
        self.$element.append($option);
      }

      if (!dontPushVal)
        self.pushVal();

      // Add class when reached maxTags
      if (self.options.maxTags === self.itemsArray.length || self.items().toString().length === self.options.maxInputLength)
        self.$container.addClass('bootstrap-tagsinput-max');

      self.$element.trigger($.Event('itemAdded', { item: item }));
    },

    /**
     * Removes the given item. Pass true to dontPushVal to prevent updating the
     * elements val()
     */
    remove: function(item, dontPushVal) {
      var self = this;

      if (self.objectItems) {
        if (typeof item === "object")
          item = $.grep(self.itemsArray, function(other) { return self.options.itemValue(other) ==  self.options.itemValue(item); } );
        else
          item = $.grep(self.itemsArray, function(other) { return self.options.itemValue(other) ==  item; } );

        item = item[item.length-1];
      }

      if (item) {
        var beforeItemRemoveEvent = $.Event('beforeItemRemove', { item: item, cancel: false });
        self.$element.trigger(beforeItemRemoveEvent);
        if (beforeItemRemoveEvent.cancel)
          return;

        $('.tag', self.$container).filter(function() { return $(this).data('item') === item; }).remove();
        $('option', self.$element).filter(function() { return $(this).data('item') === item; }).remove();
        if($.inArray(item, self.itemsArray) !== -1)
          self.itemsArray.splice($.inArray(item, self.itemsArray), 1);
      }

      if (!dontPushVal)
        self.pushVal();

      // Remove class when reached maxTags
      if (self.options.maxTags > self.itemsArray.length)
        self.$container.removeClass('bootstrap-tagsinput-max');

      self.$element.trigger($.Event('itemRemoved',  { item: item }));
    },

    /**
     * Removes all items
     */
    removeAll: function() {
      var self = this;

      $('.tag', self.$container).remove();
      $('option', self.$element).remove();

      while(self.itemsArray.length > 0)
        self.itemsArray.pop();

      self.pushVal();
    },

    /**
     * Refreshes the tags so they match the text/value of their corresponding
     * item.
     */
    refresh: function() {
      var self = this;
      $('.tag', self.$container).each(function() {
        var $tag = $(this),
            item = $tag.data('item'),
            itemValue = self.options.itemValue(item),
            itemText = self.options.itemText(item),
            tagClass = self.options.tagClass(item);

          // Update tag's class and inner text
          $tag.attr('class', null);
          $tag.addClass('tag ' + htmlEncode(tagClass));
          $tag.contents().filter(function() {
            return this.nodeType == 3;
          })[0].nodeValue = htmlEncode(itemText);

          if (self.isSelect) {
            var option = $('option', self.$element).filter(function() { return $(this).data('item') === item; });
            option.attr('value', itemValue);
          }
      });
    },

    /**
     * Returns the items added as tags
     */
    items: function() {
      return this.itemsArray;
    },

    /**
     * Assembly value by retrieving the value of each item, and set it on the
     * element.
     */
    pushVal: function() {
      var self = this,
          val = $.map(self.items(), function(item) {
            return self.options.itemValue(item).toString();
          });

      self.$element.val(val, true).trigger('change');
    },

    /**
     * Initializes the tags input behaviour on the element
     */
    build: function(options) {
      var self = this;

      self.options = $.extend({}, defaultOptions, options);
      // When itemValue is set, freeInput should always be false
      if (self.objectItems)
        self.options.freeInput = false;

      makeOptionItemFunction(self.options, 'itemValue');
      makeOptionItemFunction(self.options, 'itemText');
      makeOptionFunction(self.options, 'tagClass');
      
      // Typeahead Bootstrap version 2.3.2
      if (self.options.typeahead) {
        var typeahead = self.options.typeahead || {};

        makeOptionFunction(typeahead, 'source');

        self.$input.typeahead($.extend({}, typeahead, {
          source: function (query, process) {
            function processItems(items) {
              var texts = [];

              for (var i = 0; i < items.length; i++) {
                var text = self.options.itemText(items[i]);
                map[text] = items[i];
                texts.push(text);
              }
              process(texts);
            }

            this.map = {};
            var map = this.map,
                data = typeahead.source(query);

            if ($.isFunction(data.success)) {
              // support for Angular callbacks
              data.success(processItems);
            } else if ($.isFunction(data.then)) {
              // support for Angular promises
              data.then(processItems);
            } else {
              // support for functions and jquery promises
              $.when(data)
               .then(processItems);
            }
          },
          updater: function (text) {
            self.add(this.map[text]);
          },
          matcher: function (text) {
            return (text.toLowerCase().indexOf(this.query.trim().toLowerCase()) !== -1);
          },
          sorter: function (texts) {
            return texts.sort();
          },
          highlighter: function (text) {
            var regex = new RegExp( '(' + this.query + ')', 'gi' );
            return text.replace( regex, "<strong>$1</strong>" );
          }
        }));
      }

      // typeahead.js
      if (self.options.typeaheadjs) {
          var typeaheadjs = self.options.typeaheadjs || {};
          
          self.$input.typeahead(null, typeaheadjs).on('typeahead:selected', $.proxy(function (obj, datum) {
            if (typeaheadjs.valueKey)
              self.add(datum[typeaheadjs.valueKey]);
            else
              self.add(datum);
            self.$input.typeahead('val', '');
          }, self));
      }

      self.$container.on('click', $.proxy(function(event) {
        if (! self.$element.attr('disabled')) {
          self.$input.removeAttr('disabled');
        }
        self.$input.focus();
      }, self));

        if (self.options.addOnBlur && self.options.freeInput) {
          self.$input.on('focusout', $.proxy(function(event) {
              // HACK: only process on focusout when no typeahead opened, to
              //       avoid adding the typeahead text as tag
              if ($('.typeahead, .twitter-typeahead', self.$container).length === 0) {
                self.add(self.$input.val());
                self.$input.val('');
              }
          }, self));
        }
        

      self.$container.on('keydown', 'input', $.proxy(function(event) {
        var $input = $(event.target),
            $inputWrapper = self.findInputWrapper();

        if (self.$element.attr('disabled')) {
          self.$input.attr('disabled', 'disabled');
          return;
        }

        switch (event.which) {
          // BACKSPACE
          case 8:
            if (doGetCaretPosition($input[0]) === 0) {
              var prev = $inputWrapper.prev();
              if (prev) {
                self.remove(prev.data('item'));
              }
            }
            break;

          // DELETE
          case 46:
            if (doGetCaretPosition($input[0]) === 0) {
              var next = $inputWrapper.next();
              if (next) {
                self.remove(next.data('item'));
              }
            }
            break;

          // LEFT ARROW
          case 37:
            // Try to move the input before the previous tag
            var $prevTag = $inputWrapper.prev();
            if ($input.val().length === 0 && $prevTag[0]) {
              $prevTag.before($inputWrapper);
              $input.focus();
            }
            break;
          // RIGHT ARROW
          case 39:
            // Try to move the input after the next tag
            var $nextTag = $inputWrapper.next();
            if ($input.val().length === 0 && $nextTag[0]) {
              $nextTag.after($inputWrapper);
              $input.focus();
            }
            break;
         default:
             // ignore
         }

        // Reset internal input's size
        var textLength = $input.val().length,
            wordSpace = Math.ceil(textLength / 5),
            size = textLength + wordSpace + 1;
        $input.attr('size', Math.max(this.inputSize, $input.val().length));
      }, self));

      self.$container.on('keypress', 'input', $.proxy(function(event) {
         var $input = $(event.target);

         if (self.$element.attr('disabled')) {
            self.$input.attr('disabled', 'disabled');
            return;
         }

         var text = $input.val(),
         maxLengthReached = self.options.maxChars && text.length >= self.options.maxChars;
         if (self.options.freeInput && (keyCombinationInList(event, self.options.confirmKeys) || maxLengthReached)) {
            self.add(maxLengthReached ? text.substr(0, self.options.maxChars) : text);
            $input.val('');
            event.preventDefault();
         }

         // Reset internal input's size
         var textLength = $input.val().length,
            wordSpace = Math.ceil(textLength / 5),
            size = textLength + wordSpace + 1;
         $input.attr('size', Math.max(this.inputSize, $input.val().length));
      }, self));

      // Remove icon clicked
      self.$container.on('click', '[data-role=remove]', $.proxy(function(event) {
        if (self.$element.attr('disabled')) {
          return;
        }
        self.remove($(event.target).closest('.tag').data('item'));
      }, self));

      // Only add existing value as tags when using strings as tags
      if (self.options.itemValue === defaultOptions.itemValue) {
        if (self.$element[0].tagName === 'INPUT') {
            self.add(self.$element.val());
        } else {
          $('option', self.$element).each(function() {
            self.add($(this).attr('value'), true);
          });
        }
      }
    },

    /**
     * Removes all tagsinput behaviour and unregsiter all event handlers
     */
    destroy: function() {
      var self = this;

      // Unbind events
      self.$container.off('keypress', 'input');
      self.$container.off('click', '[role=remove]');

      self.$container.remove();
      self.$element.removeData('tagsinput');
      self.$element.show();
    },

    /**
     * Sets focus on the tagsinput
     */
    focus: function() {
      this.$input.focus();
    },

    /**
     * Returns the internal input element
     */
    input: function() {
      return this.$input;
    },

    /**
     * Returns the element which is wrapped around the internal input. This
     * is normally the $container, but typeahead.js moves the $input element.
     */
    findInputWrapper: function() {
      var elt = this.$input[0],
          container = this.$container[0];
      while(elt && elt.parentNode !== container)
        elt = elt.parentNode;

      return $(elt);
    }
  };

  /**
   * Register JQuery plugin
   */
  $.fn.tagsinput = function(arg1, arg2) {
    var results = [];

    this.each(function() {
      var tagsinput = $(this).data('tagsinput');
      // Initialize a new tags input
      if (!tagsinput) {
          tagsinput = new TagsInput(this, arg1);
          $(this).data('tagsinput', tagsinput);
          results.push(tagsinput);

          if (this.tagName === 'SELECT') {
              $('option', $(this)).attr('selected', 'selected');
          }

          // Init tags from $(this).val()
          $(this).val($(this).val());
      } else if (!arg1 && !arg2) {
          // tagsinput already exists
          // no function, trying to init
          results.push(tagsinput);
      } else if(tagsinput[arg1] !== undefined) {
          // Invoke function on existing tags input
          var retVal = tagsinput[arg1](arg2);
          if (retVal !== undefined)
              results.push(retVal);
      }
    });

    if ( typeof arg1 == 'string') {
      // Return the results from the invoked function calls
      return results.length > 1 ? results : results[0];
    } else {
      return results;
    }
  };

  $.fn.tagsinput.Constructor = TagsInput;

  /**
   * Most options support both a string or number as well as a function as
   * option value. This function makes sure that the option with the given
   * key in the given options is wrapped in a function
   */
  function makeOptionItemFunction(options, key) {
    if (typeof options[key] !== 'function') {
      var propertyName = options[key];
      options[key] = function(item) { return item[propertyName]; };
    }
  }
  function makeOptionFunction(options, key) {
    if (typeof options[key] !== 'function') {
      var value = options[key];
      options[key] = function() { return value; };
    }
  }
  /**
   * HtmlEncodes the given value
   */
  var htmlEncodeContainer = $('<div />');
  function htmlEncode(value) {
    if (value) {
      return htmlEncodeContainer.text(value).html();
    } else {
      return '';
    }
  }

  /**
   * Returns the position of the caret in the given input field
   * http://flightschool.acylt.com/devnotes/caret-position-woes/
   */
  function doGetCaretPosition(oField) {
    var iCaretPos = 0;
    if (document.selection) {
      oField.focus ();
      var oSel = document.selection.createRange();
      oSel.moveStart ('character', -oField.value.length);
      iCaretPos = oSel.text.length;
    } else if (oField.selectionStart || oField.selectionStart == '0') {
      iCaretPos = oField.selectionStart;
    }
    return (iCaretPos);
  }

  /**
    * Returns boolean indicates whether user has pressed an expected key combination. 
    * @param object keyPressEvent: JavaScript event object, refer
    *     http://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
    * @param object lookupList: expected key combinations, as in:
    *     [13, {which: 188, shiftKey: true}]
    */
  function keyCombinationInList(keyPressEvent, lookupList) {
      var found = false;
      $.each(lookupList, function (index, keyCombination) {
          if (typeof (keyCombination) === 'number' && keyPressEvent.which === keyCombination) {
              found = true;
              return false;
          }

          if (keyPressEvent.which === keyCombination.which) {
              var alt = !keyCombination.hasOwnProperty('altKey') || keyPressEvent.altKey === keyCombination.altKey,
                  shift = !keyCombination.hasOwnProperty('shiftKey') || keyPressEvent.shiftKey === keyCombination.shiftKey,
                  ctrl = !keyCombination.hasOwnProperty('ctrlKey') || keyPressEvent.ctrlKey === keyCombination.ctrlKey;
              if (alt && shift && ctrl) {
                  found = true;
                  return false;
              }
          }
      });

      return found;
  }

  /**
   * Initialize tagsinput behaviour on inputs and selects which have
   * data-role=tagsinput
   */
  $(function() {
    $("input[data-role=tagsinput], select[multiple][data-role=tagsinput]").tagsinput();
  });
})(window.jQuery);

/*
 *  Bootstrap TouchSpin - v3.0.1
 *  A mobile and touch friendly input spinner component for Bootstrap 3.
 *  http://www.virtuosoft.eu/code/bootstrap-touchspin/
 *
 *  Made by István Ujj-Mészáros
 *  Under Apache License v2.0 License
 */
(function($) {
  'use strict';

  var _currentSpinnerId = 0;

  function _scopedEventName(name, id) {
    return name + '.touchspin_' + id;
  }

  function _scopeEventNames(names, id) {
    return $.map(names, function(name) {
      return _scopedEventName(name, id);
    });
  }

  $.fn.TouchSpin = function(options) {

    if (options === 'destroy') {
      this.each(function() {
        var originalinput = $(this),
            originalinput_data = originalinput.data();
        $(document).off(_scopeEventNames([
          'mouseup',
          'touchend',
          'touchcancel',
          'mousemove',
          'touchmove',
          'scroll',
          'scrollstart'], originalinput_data.spinnerid).join(' '));
      });
      return;
    }

    var defaults = {
      min: 0,
      max: 100,
      initval: '',
      step: 1,
      decimals: 0,
      stepinterval: 100,
      forcestepdivisibility: 'round', // none | floor | round | ceil
      stepintervaldelay: 500,
      verticalbuttons: false,
      verticalupclass: 'glyphicon glyphicon-chevron-up',
      verticaldownclass: 'glyphicon glyphicon-chevron-down',
      prefix: '',
      postfix: '',
      prefix_extraclass: '',
      postfix_extraclass: '',
      booster: true,
      boostat: 10,
      maxboostedstep: false,
      mousewheel: true,
      buttondown_class: 'btn btn-default',
      buttonup_class: 'btn btn-default',
	  buttondown_txt: '-',
	  buttonup_txt: '+'
    };

    var attributeMap = {
      min: 'min',
      max: 'max',
      initval: 'init-val',
      step: 'step',
      decimals: 'decimals',
      stepinterval: 'step-interval',
      verticalbuttons: 'vertical-buttons',
      verticalupclass: 'vertical-up-class',
      verticaldownclass: 'vertical-down-class',
      forcestepdivisibility: 'force-step-divisibility',
      stepintervaldelay: 'step-interval-delay',
      prefix: 'prefix',
      postfix: 'postfix',
      prefix_extraclass: 'prefix-extra-class',
      postfix_extraclass: 'postfix-extra-class',
      booster: 'booster',
      boostat: 'boostat',
      maxboostedstep: 'max-boosted-step',
      mousewheel: 'mouse-wheel',
      buttondown_class: 'button-down-class',
      buttonup_class: 'button-up-class',
	  buttondown_txt: 'button-down-txt',
	  buttonup_txt: 'button-up-txt'
    };

    return this.each(function() {

      var settings,
          originalinput = $(this),
          originalinput_data = originalinput.data(),
          container,
          elements,
          value,
          downSpinTimer,
          upSpinTimer,
          downDelayTimeout,
          upDelayTimeout,
          spincount = 0,
          spinning = false;

      init();


      function init() {
        if (originalinput.data('alreadyinitialized')) {
          return;
        }

        originalinput.data('alreadyinitialized', true);
        _currentSpinnerId += 1;
        originalinput.data('spinnerid', _currentSpinnerId);


        if (!originalinput.is('input')) {
          console.log('Must be an input.');
          return;
        }

        _initSettings();
        _setInitval();
        _checkValue();
        _buildHtml();
        _initElements();
        _hideEmptyPrefixPostfix();
        _bindEvents();
        _bindEventsInterface();
        elements.input.css('display', 'block');
      }

      function _setInitval() {
        if (settings.initval !== '' && originalinput.val() === '') {
          originalinput.val(settings.initval);
        }
      }

      function changeSettings(newsettings) {
        _updateSettings(newsettings);
        _checkValue();

        var value = elements.input.val();

        if (value !== '') {
          value = Number(elements.input.val());
          elements.input.val(value.toFixed(settings.decimals));
        }
      }

      function _initSettings() {
        settings = $.extend({}, defaults, originalinput_data, _parseAttributes(), options);
      }

      function _parseAttributes() {
        var data = {};
        $.each(attributeMap, function(key, value) {
          var attrName = 'bts-' + value + '';
          if (originalinput.is('[data-' + attrName + ']')) {
            data[key] = originalinput.data(attrName);
          }
        });
        return data;
      }

      function _updateSettings(newsettings) {
        settings = $.extend({}, settings, newsettings);
      }

      function _buildHtml() {
        var initval = originalinput.val(),
            parentelement = originalinput.parent();

        if (initval !== '') {
          initval = Number(initval).toFixed(settings.decimals);
        }

        originalinput.data('initvalue', initval).val(initval);
        originalinput.addClass('form-control');

        if (parentelement.hasClass('input-group')) {
          _advanceInputGroup(parentelement);
        }
        else {
          _buildInputGroup();
        }
      }

      function _advanceInputGroup(parentelement) {
        parentelement.addClass('bootstrap-touchspin');

        var prev = originalinput.prev(),
            next = originalinput.next();

        var downhtml,
            uphtml,
            prefixhtml = '<span class="input-group-addon bootstrap-touchspin-prefix">' + settings.prefix + '</span>',
            postfixhtml = '<span class="input-group-addon bootstrap-touchspin-postfix">' + settings.postfix + '</span>';

        if (prev.hasClass('input-group-btn')) {
          downhtml = '<button class="' + settings.buttondown_class + ' bootstrap-touchspin-down" type="button">' + settings.buttondown_txt + '</button>';
          prev.append(downhtml);
        }
        else {
          downhtml = '<span class="input-group-btn"><button class="' + settings.buttondown_class + ' bootstrap-touchspin-down" type="button">' + settings.buttondown_txt + '</button></span>';
          $(downhtml).insertBefore(originalinput);
        }

        if (next.hasClass('input-group-btn')) {
          uphtml = '<button class="' + settings.buttonup_class + ' bootstrap-touchspin-up" type="button">' + settings.buttonup_txt + '</button>';
          next.prepend(uphtml);
        }
        else {
          uphtml = '<span class="input-group-btn"><button class="' + settings.buttonup_class + ' bootstrap-touchspin-up" type="button">' + settings.buttonup_txt + '</button></span>';
          $(uphtml).insertAfter(originalinput);
        }

        $(prefixhtml).insertBefore(originalinput);
        $(postfixhtml).insertAfter(originalinput);

        container = parentelement;
      }

      function _buildInputGroup() {
        var html;

        if (settings.verticalbuttons) {
          html = '<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix">' + settings.prefix + '</span><span class="input-group-addon bootstrap-touchspin-postfix">' + settings.postfix + '</span><span class="input-group-btn-vertical"><button class="' + settings.buttondown_class + ' bootstrap-touchspin-up" type="button"><i class="' + settings.verticalupclass + '"></i></button><button class="' + settings.buttonup_class + ' bootstrap-touchspin-down" type="button"><i class="' + settings.verticaldownclass + '"></i></button></span></div>';
        }
        else {
          html = '<div class="input-group bootstrap-touchspin"><span class="input-group-btn"><button class="' + settings.buttondown_class + ' bootstrap-touchspin-down" type="button">' + settings.buttondown_txt + '</button></span><span class="input-group-addon bootstrap-touchspin-prefix">' + settings.prefix + '</span><span class="input-group-addon bootstrap-touchspin-postfix">' + settings.postfix + '</span><span class="input-group-btn"><button class="' + settings.buttonup_class + ' bootstrap-touchspin-up" type="button">' + settings.buttonup_txt + '</button></span></div>';
        }

        container = $(html).insertBefore(originalinput);

        $('.bootstrap-touchspin-prefix', container).after(originalinput);

        if (originalinput.hasClass('input-sm')) {
          container.addClass('input-group-sm');
        }
        else if (originalinput.hasClass('input-lg')) {
          container.addClass('input-group-lg');
        }
      }

      function _initElements() {
        elements = {
          down: $('.bootstrap-touchspin-down', container),
          up: $('.bootstrap-touchspin-up', container),
          input: $('input', container),
          prefix: $('.bootstrap-touchspin-prefix', container).addClass(settings.prefix_extraclass),
          postfix: $('.bootstrap-touchspin-postfix', container).addClass(settings.postfix_extraclass)
        };
      }

      function _hideEmptyPrefixPostfix() {
        if (settings.prefix === '') {
          elements.prefix.hide();
        }

        if (settings.postfix === '') {
          elements.postfix.hide();
        }
      }

      function _bindEvents() {
        originalinput.on('keydown', function(ev) {
          var code = ev.keyCode || ev.which;

          if (code === 38) {
            if (spinning !== 'up') {
              upOnce();
              startUpSpin();
            }
            ev.preventDefault();
          }
          else if (code === 40) {
            if (spinning !== 'down') {
              downOnce();
              startDownSpin();
            }
            ev.preventDefault();
          }
        });

        originalinput.on('keyup', function(ev) {
          var code = ev.keyCode || ev.which;

          if (code === 38) {
            stopSpin();
          }
          else if (code === 40) {
            stopSpin();
          }
        });

        originalinput.on('blur', function() {
          _checkValue();
        });

        elements.down.on('keydown', function(ev) {
          var code = ev.keyCode || ev.which;

          if (code === 32 || code === 13) {
            if (spinning !== 'down') {
              downOnce();
              startDownSpin();
            }
            ev.preventDefault();
          }
        });

        elements.down.on('keyup', function(ev) {
          var code = ev.keyCode || ev.which;

          if (code === 32 || code === 13) {
            stopSpin();
          }
        });

        elements.up.on('keydown', function(ev) {
          var code = ev.keyCode || ev.which;

          if (code === 32 || code === 13) {
            if (spinning !== 'up') {
              upOnce();
              startUpSpin();
            }
            ev.preventDefault();
          }
        });

        elements.up.on('keyup', function(ev) {
          var code = ev.keyCode || ev.which;

          if (code === 32 || code === 13) {
            stopSpin();
          }
        });

        elements.down.on('mousedown.touchspin', function(ev) {
          elements.down.off('touchstart.touchspin');  // android 4 workaround

          if (originalinput.is(':disabled')) {
            return;
          }

          downOnce();
          startDownSpin();

          ev.preventDefault();
          ev.stopPropagation();
        });

        elements.down.on('touchstart.touchspin', function(ev) {
          elements.down.off('mousedown.touchspin');  // android 4 workaround

          if (originalinput.is(':disabled')) {
            return;
          }

          downOnce();
          startDownSpin();

          ev.preventDefault();
          ev.stopPropagation();
        });

        elements.up.on('mousedown.touchspin', function(ev) {
          elements.up.off('touchstart.touchspin');  // android 4 workaround

          if (originalinput.is(':disabled')) {
            return;
          }

          upOnce();
          startUpSpin();

          ev.preventDefault();
          ev.stopPropagation();
        });

        elements.up.on('touchstart.touchspin', function(ev) {
          elements.up.off('mousedown.touchspin');  // android 4 workaround

          if (originalinput.is(':disabled')) {
            return;
          }

          upOnce();
          startUpSpin();

          ev.preventDefault();
          ev.stopPropagation();
        });

        elements.up.on('mouseout touchleave touchend touchcancel', function(ev) {
          if (!spinning) {
            return;
          }

          ev.stopPropagation();
          stopSpin();
        });

        elements.down.on('mouseout touchleave touchend touchcancel', function(ev) {
          if (!spinning) {
            return;
          }

          ev.stopPropagation();
          stopSpin();
        });

        elements.down.on('mousemove touchmove', function(ev) {
          if (!spinning) {
            return;
          }

          ev.stopPropagation();
          ev.preventDefault();
        });

        elements.up.on('mousemove touchmove', function(ev) {
          if (!spinning) {
            return;
          }

          ev.stopPropagation();
          ev.preventDefault();
        });

        $(document).on(_scopeEventNames(['mouseup', 'touchend', 'touchcancel'], _currentSpinnerId).join(' '), function(ev) {
          if (!spinning) {
            return;
          }

          ev.preventDefault();
          stopSpin();
        });

        $(document).on(_scopeEventNames(['mousemove', 'touchmove', 'scroll', 'scrollstart'], _currentSpinnerId).join(' '), function(ev) {
          if (!spinning) {
            return;
          }

          ev.preventDefault();
          stopSpin();
        });

        originalinput.on('mousewheel DOMMouseScroll', function(ev) {
          if (!settings.mousewheel || !originalinput.is(':focus')) {
            return;
          }

          var delta = ev.originalEvent.wheelDelta || -ev.originalEvent.deltaY || -ev.originalEvent.detail;

          ev.stopPropagation();
          ev.preventDefault();

          if (delta < 0) {
            downOnce();
          }
          else {
            upOnce();
          }
        });
      }

      function _bindEventsInterface() {
        originalinput.on('touchspin.uponce', function() {
          stopSpin();
          upOnce();
        });

        originalinput.on('touchspin.downonce', function() {
          stopSpin();
          downOnce();
        });

        originalinput.on('touchspin.startupspin', function() {
          startUpSpin();
        });

        originalinput.on('touchspin.startdownspin', function() {
          startDownSpin();
        });

        originalinput.on('touchspin.stopspin', function() {
          stopSpin();
        });

        originalinput.on('touchspin.updatesettings', function(e, newsettings) {
          changeSettings(newsettings);
        });
      }

      function _forcestepdivisibility(value) {
        switch (settings.forcestepdivisibility) {
          case 'round':
            return (Math.round(value / settings.step) * settings.step).toFixed(settings.decimals);
          case 'floor':
            return (Math.floor(value / settings.step) * settings.step).toFixed(settings.decimals);
          case 'ceil':
            return (Math.ceil(value / settings.step) * settings.step).toFixed(settings.decimals);
          default:
            return value;
        }
      }

      function _checkValue() {
        var val, parsedval, returnval;

        val = originalinput.val();

        if (val === '') {
          return;
        }

        if (settings.decimals > 0 && val === '.') {
          return;
        }

        parsedval = parseFloat(val);

        if (isNaN(parsedval)) {
          parsedval = 0;
        }

        returnval = parsedval;

        if (parsedval.toString() !== val) {
          returnval = parsedval;
        }

        if (parsedval < settings.min) {
          returnval = settings.min;
        }

        if (parsedval > settings.max) {
          returnval = settings.max;
        }

        returnval = _forcestepdivisibility(returnval);

        if (Number(val).toString() !== returnval.toString()) {
          originalinput.val(returnval);
          originalinput.trigger('change');
        }
      }

      function _getBoostedStep() {
        if (!settings.booster) {
          return settings.step;
        }
        else {
          var boosted = Math.pow(2, Math.floor(spincount / settings.boostat)) * settings.step;

          if (settings.maxboostedstep) {
            if (boosted > settings.maxboostedstep) {
              boosted = settings.maxboostedstep;
              value = Math.round((value / boosted)) * boosted;
            }
          }

          return Math.max(settings.step, boosted);
        }
      }

      function upOnce() {
        _checkValue();

        value = parseFloat(elements.input.val());
        if (isNaN(value)) {
          value = 0;
        }

        var initvalue = value,
            boostedstep = _getBoostedStep();

        value = value + boostedstep;

        if (value > settings.max) {
          value = settings.max;
          originalinput.trigger('touchspin.on.max');
          stopSpin();
        }

        elements.input.val(Number(value).toFixed(settings.decimals));

        if (initvalue !== value) {
          originalinput.trigger('change');
        }
      }

      function downOnce() {
        _checkValue();

        value = parseFloat(elements.input.val());
        if (isNaN(value)) {
          value = 0;
        }

        var initvalue = value,
            boostedstep = _getBoostedStep();

        value = value - boostedstep;

        if (value < settings.min) {
          value = settings.min;
          originalinput.trigger('touchspin.on.min');
          stopSpin();
        }

        elements.input.val(value.toFixed(settings.decimals));

        if (initvalue !== value) {
          originalinput.trigger('change');
        }
      }

      function startDownSpin() {
        stopSpin();

        spincount = 0;
        spinning = 'down';

        originalinput.trigger('touchspin.on.startspin');
        originalinput.trigger('touchspin.on.startdownspin');

        downDelayTimeout = setTimeout(function() {
          downSpinTimer = setInterval(function() {
            spincount++;
            downOnce();
          }, settings.stepinterval);
        }, settings.stepintervaldelay);
      }

      function startUpSpin() {
        stopSpin();

        spincount = 0;
        spinning = 'up';

        originalinput.trigger('touchspin.on.startspin');
        originalinput.trigger('touchspin.on.startupspin');

        upDelayTimeout = setTimeout(function() {
          upSpinTimer = setInterval(function() {
            spincount++;
            upOnce();
          }, settings.stepinterval);
        }, settings.stepintervaldelay);
      }

      function stopSpin() {
        clearTimeout(downDelayTimeout);
        clearTimeout(upDelayTimeout);
        clearInterval(downSpinTimer);
        clearInterval(upSpinTimer);

        switch (spinning) {
          case 'up':
            originalinput.trigger('touchspin.on.stopupspin');
            originalinput.trigger('touchspin.on.stopspin');
            break;
          case 'down':
            originalinput.trigger('touchspin.on.stopdownspin');
            originalinput.trigger('touchspin.on.stopspin');
            break;
        }

        spincount = 0;
        spinning = false;
      }

    });

  };

})(jQuery);

/*!
 * jQuery twitter bootstrap wizard plugin
 * Examples and documentation at: http://github.com/VinceG/twitter-bootstrap-wizard
 * version 1.0
 * Requires jQuery v1.3.2 or later
 * Supports Bootstrap 2.2.x, 2.3.x, 3.0
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 * Authors: Vadim Vincent Gabriel (http://vadimg.com), Jason Gill (www.gilluminate.com)
 */
(function(e){var k=function(d,g){d=e(d);var a=this,b=e.extend({},e.fn.bootstrapWizard.defaults,g),f=null,c=null;this.rebindClick=function(b,a){b.unbind("click",a).bind("click",a)};this.fixNavigationButtons=function(){f.length||(c.find("a:first").tab("show"),f=c.find('li:has([data-toggle="tab"]):first'));e(b.previousSelector,d).toggleClass("disabled",a.firstIndex()>=a.currentIndex());e(b.nextSelector,d).toggleClass("disabled",a.currentIndex()>=a.navigationLength());a.rebindClick(e(b.nextSelector,d),
a.next);a.rebindClick(e(b.previousSelector,d),a.previous);a.rebindClick(e(b.lastSelector,d),a.last);a.rebindClick(e(b.firstSelector,d),a.first);if(b.onTabShow&&"function"===typeof b.onTabShow&&!1===b.onTabShow(f,c,a.currentIndex()))return!1};this.next=function(h){if(d.hasClass("last")||b.onNext&&"function"===typeof b.onNext&&!1===b.onNext(f,c,a.nextIndex()))return!1;$index=a.nextIndex();$index>a.navigationLength()||c.find('li:has([data-toggle="tab"]):eq('+$index+") a").tab("show")};this.previous=
function(h){if(d.hasClass("first")||b.onPrevious&&"function"===typeof b.onPrevious&&!1===b.onPrevious(f,c,a.previousIndex()))return!1;$index=a.previousIndex();0>$index||c.find('li:has([data-toggle="tab"]):eq('+$index+") a").tab("show")};this.first=function(h){if(b.onFirst&&"function"===typeof b.onFirst&&!1===b.onFirst(f,c,a.firstIndex())||d.hasClass("disabled"))return!1;c.find('li:has([data-toggle="tab"]):eq(0) a').tab("show")};this.last=function(h){if(b.onLast&&"function"===typeof b.onLast&&!1===
b.onLast(f,c,a.lastIndex())||d.hasClass("disabled"))return!1;c.find('li:has([data-toggle="tab"]):eq('+a.navigationLength()+") a").tab("show")};this.currentIndex=function(){return c.find('li:has([data-toggle="tab"])').index(f)};this.firstIndex=function(){return 0};this.lastIndex=function(){return a.navigationLength()};this.getIndex=function(a){return c.find('li:has([data-toggle="tab"])').index(a)};this.nextIndex=function(){return c.find('li:has([data-toggle="tab"])').index(f)+1};this.previousIndex=
function(){return c.find('li:has([data-toggle="tab"])').index(f)-1};this.navigationLength=function(){return c.find('li:has([data-toggle="tab"])').length-1};this.activeTab=function(){return f};this.nextTab=function(){return c.find('li:has([data-toggle="tab"]):eq('+(a.currentIndex()+1)+")").length?c.find('li:has([data-toggle="tab"]):eq('+(a.currentIndex()+1)+")"):null};this.previousTab=function(){return 0>=a.currentIndex()?null:c.find('li:has([data-toggle="tab"]):eq('+parseInt(a.currentIndex()-1)+")")};
this.show=function(a){return d.find('li:has([data-toggle="tab"]):eq('+a+") a").tab("show")};this.disable=function(a){c.find('li:has([data-toggle="tab"]):eq('+a+")").addClass("disabled")};this.enable=function(a){c.find('li:has([data-toggle="tab"]):eq('+a+")").removeClass("disabled")};this.hide=function(a){c.find('li:has([data-toggle="tab"]):eq('+a+")").hide()};this.display=function(a){c.find('li:has([data-toggle="tab"]):eq('+a+")").show()};this.remove=function(a){var b="undefined"!=typeof a[1]?a[1]:
!1;a=c.find('li:has([data-toggle="tab"]):eq('+a[0]+")");b&&(b=a.find("a").attr("href"),e(b).remove());a.remove()};c=d.find("ul:first",d);f=c.find('li:has([data-toggle="tab"]).active',d);c.hasClass(b.tabClass)||c.addClass(b.tabClass);if(b.onInit&&"function"===typeof b.onInit)b.onInit(f,c,0);if(b.onShow&&"function"===typeof b.onShow)b.onShow(f,c,a.nextIndex());a.fixNavigationButtons();e('a[data-toggle="tab"]',c).on("click",function(d){d=c.find('li:has([data-toggle="tab"])').index(e(d.currentTarget).parent('li:has([data-toggle="tab"])'));
if(b.onTabClick&&"function"===typeof b.onTabClick&&!1===b.onTabClick(f,c,a.currentIndex(),d))return!1});e('a[data-toggle="tab"]',c).on("shown shown.bs.tab",function(d){$element=e(d.target).parent();d=c.find('li:has([data-toggle="tab"])').index($element);if($element.hasClass("disabled")||b.onTabChange&&"function"===typeof b.onTabChange&&!1===b.onTabChange(f,c,a.currentIndex(),d))return!1;f=$element;a.fixNavigationButtons()})};e.fn.bootstrapWizard=function(d){if("string"==typeof d){var g=Array.prototype.slice.call(arguments,
1);1===g.length&&g.toString();return this.data("bootstrapWizard")[d](g)}return this.each(function(a){a=e(this);if(!a.data("bootstrapWizard")){var b=new k(a,d);a.data("bootstrapWizard",b)}})};e.fn.bootstrapWizard.defaults={tabClass:"nav nav-pills",nextSelector:".wizard li.next",previousSelector:".wizard li.previous",firstSelector:".wizard li.first",lastSelector:".wizard li.last",onShow:null,onInit:null,onNext:null,onPrevious:null,onLast:null,onFirst:null,onTabChange:null,onTabClick:null,onTabShow:null}})(jQuery);

/*! jQuery Validation Plugin - v1.14.0 - 6/30/2015
 * http://jqueryvalidation.org/
 * Copyright (c) 2015 Jörn Zaefferer; Licensed MIT */
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a(jQuery)}(function(a){a.extend(a.fn,{validate:function(b){if(!this.length)return void(b&&b.debug&&window.console&&console.warn("Nothing selected, can't validate, returning nothing."));var c=a.data(this[0],"validator");return c?c:(this.attr("novalidate","novalidate"),c=new a.validator(b,this[0]),a.data(this[0],"validator",c),c.settings.onsubmit&&(this.on("click.validate",":submit",function(b){c.settings.submitHandler&&(c.submitButton=b.target),a(this).hasClass("cancel")&&(c.cancelSubmit=!0),void 0!==a(this).attr("formnovalidate")&&(c.cancelSubmit=!0)}),this.on("submit.validate",function(b){function d(){var d,e;return c.settings.submitHandler?(c.submitButton&&(d=a("<input type='hidden'/>").attr("name",c.submitButton.name).val(a(c.submitButton).val()).appendTo(c.currentForm)),e=c.settings.submitHandler.call(c,c.currentForm,b),c.submitButton&&d.remove(),void 0!==e?e:!1):!0}return c.settings.debug&&b.preventDefault(),c.cancelSubmit?(c.cancelSubmit=!1,d()):c.form()?c.pendingRequest?(c.formSubmitted=!0,!1):d():(c.focusInvalid(),!1)})),c)},valid:function(){var b,c,d;return a(this[0]).is("form")?b=this.validate().form():(d=[],b=!0,c=a(this[0].form).validate(),this.each(function(){b=c.element(this)&&b,d=d.concat(c.errorList)}),c.errorList=d),b},rules:function(b,c){var d,e,f,g,h,i,j=this[0];if(b)switch(d=a.data(j.form,"validator").settings,e=d.rules,f=a.validator.staticRules(j),b){case"add":a.extend(f,a.validator.normalizeRule(c)),delete f.messages,e[j.name]=f,c.messages&&(d.messages[j.name]=a.extend(d.messages[j.name],c.messages));break;case"remove":return c?(i={},a.each(c.split(/\s/),function(b,c){i[c]=f[c],delete f[c],"required"===c&&a(j).removeAttr("aria-required")}),i):(delete e[j.name],f)}return g=a.validator.normalizeRules(a.extend({},a.validator.classRules(j),a.validator.attributeRules(j),a.validator.dataRules(j),a.validator.staticRules(j)),j),g.required&&(h=g.required,delete g.required,g=a.extend({required:h},g),a(j).attr("aria-required","true")),g.remote&&(h=g.remote,delete g.remote,g=a.extend(g,{remote:h})),g}}),a.extend(a.expr[":"],{blank:function(b){return!a.trim(""+a(b).val())},filled:function(b){return!!a.trim(""+a(b).val())},unchecked:function(b){return!a(b).prop("checked")}}),a.validator=function(b,c){this.settings=a.extend(!0,{},a.validator.defaults,b),this.currentForm=c,this.init()},a.validator.format=function(b,c){return 1===arguments.length?function(){var c=a.makeArray(arguments);return c.unshift(b),a.validator.format.apply(this,c)}:(arguments.length>2&&c.constructor!==Array&&(c=a.makeArray(arguments).slice(1)),c.constructor!==Array&&(c=[c]),a.each(c,function(a,c){b=b.replace(new RegExp("\\{"+a+"\\}","g"),function(){return c})}),b)},a.extend(a.validator,{defaults:{messages:{},groups:{},rules:{},errorClass:"error",validClass:"valid",errorElement:"label",focusCleanup:!1,focusInvalid:!0,errorContainer:a([]),errorLabelContainer:a([]),onsubmit:!0,ignore:":hidden",ignoreTitle:!1,onfocusin:function(a){this.lastActive=a,this.settings.focusCleanup&&(this.settings.unhighlight&&this.settings.unhighlight.call(this,a,this.settings.errorClass,this.settings.validClass),this.hideThese(this.errorsFor(a)))},onfocusout:function(a){this.checkable(a)||!(a.name in this.submitted)&&this.optional(a)||this.element(a)},onkeyup:function(b,c){var d=[16,17,18,20,35,36,37,38,39,40,45,144,225];9===c.which&&""===this.elementValue(b)||-1!==a.inArray(c.keyCode,d)||(b.name in this.submitted||b===this.lastElement)&&this.element(b)},onclick:function(a){a.name in this.submitted?this.element(a):a.parentNode.name in this.submitted&&this.element(a.parentNode)},highlight:function(b,c,d){"radio"===b.type?this.findByName(b.name).addClass(c).removeClass(d):a(b).addClass(c).removeClass(d)},unhighlight:function(b,c,d){"radio"===b.type?this.findByName(b.name).removeClass(c).addClass(d):a(b).removeClass(c).addClass(d)}},setDefaults:function(b){a.extend(a.validator.defaults,b)},messages:{required:"This field is required.",remote:"Please fix this field.",email:"Please enter a valid email address.",url:"Please enter a valid URL.",date:"Please enter a valid date.",dateISO:"Please enter a valid date ( ISO ).",number:"Please enter a valid number.",digits:"Please enter only digits.",creditcard:"Please enter a valid credit card number.",equalTo:"Please enter the same value again.",maxlength:a.validator.format("Please enter no more than {0} characters."),minlength:a.validator.format("Please enter at least {0} characters."),rangelength:a.validator.format("Please enter a value between {0} and {1} characters long."),range:a.validator.format("Please enter a value between {0} and {1}."),max:a.validator.format("Please enter a value less than or equal to {0}."),min:a.validator.format("Please enter a value greater than or equal to {0}.")},autoCreateRanges:!1,prototype:{init:function(){function b(b){var c=a.data(this.form,"validator"),d="on"+b.type.replace(/^validate/,""),e=c.settings;e[d]&&!a(this).is(e.ignore)&&e[d].call(c,this,b)}this.labelContainer=a(this.settings.errorLabelContainer),this.errorContext=this.labelContainer.length&&this.labelContainer||a(this.currentForm),this.containers=a(this.settings.errorContainer).add(this.settings.errorLabelContainer),this.submitted={},this.valueCache={},this.pendingRequest=0,this.pending={},this.invalid={},this.reset();var c,d=this.groups={};a.each(this.settings.groups,function(b,c){"string"==typeof c&&(c=c.split(/\s/)),a.each(c,function(a,c){d[c]=b})}),c=this.settings.rules,a.each(c,function(b,d){c[b]=a.validator.normalizeRule(d)}),a(this.currentForm).on("focusin.validate focusout.validate keyup.validate",":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'], [type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], [type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'], [type='radio'], [type='checkbox']",b).on("click.validate","select, option, [type='radio'], [type='checkbox']",b),this.settings.invalidHandler&&a(this.currentForm).on("invalid-form.validate",this.settings.invalidHandler),a(this.currentForm).find("[required], [data-rule-required], .required").attr("aria-required","true")},form:function(){return this.checkForm(),a.extend(this.submitted,this.errorMap),this.invalid=a.extend({},this.errorMap),this.valid()||a(this.currentForm).triggerHandler("invalid-form",[this]),this.showErrors(),this.valid()},checkForm:function(){this.prepareForm();for(var a=0,b=this.currentElements=this.elements();b[a];a++)this.check(b[a]);return this.valid()},element:function(b){var c=this.clean(b),d=this.validationTargetFor(c),e=!0;return this.lastElement=d,void 0===d?delete this.invalid[c.name]:(this.prepareElement(d),this.currentElements=a(d),e=this.check(d)!==!1,e?delete this.invalid[d.name]:this.invalid[d.name]=!0),a(b).attr("aria-invalid",!e),this.numberOfInvalids()||(this.toHide=this.toHide.add(this.containers)),this.showErrors(),e},showErrors:function(b){if(b){a.extend(this.errorMap,b),this.errorList=[];for(var c in b)this.errorList.push({message:b[c],element:this.findByName(c)[0]});this.successList=a.grep(this.successList,function(a){return!(a.name in b)})}this.settings.showErrors?this.settings.showErrors.call(this,this.errorMap,this.errorList):this.defaultShowErrors()},resetForm:function(){a.fn.resetForm&&a(this.currentForm).resetForm(),this.submitted={},this.lastElement=null,this.prepareForm(),this.hideErrors();var b,c=this.elements().removeData("previousValue").removeAttr("aria-invalid");if(this.settings.unhighlight)for(b=0;c[b];b++)this.settings.unhighlight.call(this,c[b],this.settings.errorClass,"");else c.removeClass(this.settings.errorClass)},numberOfInvalids:function(){return this.objectLength(this.invalid)},objectLength:function(a){var b,c=0;for(b in a)c++;return c},hideErrors:function(){this.hideThese(this.toHide)},hideThese:function(a){a.not(this.containers).text(""),this.addWrapper(a).hide()},valid:function(){return 0===this.size()},size:function(){return this.errorList.length},focusInvalid:function(){if(this.settings.focusInvalid)try{a(this.findLastActive()||this.errorList.length&&this.errorList[0].element||[]).filter(":visible").focus().trigger("focusin")}catch(b){}},findLastActive:function(){var b=this.lastActive;return b&&1===a.grep(this.errorList,function(a){return a.element.name===b.name}).length&&b},elements:function(){var b=this,c={};return a(this.currentForm).find("input, select, textarea").not(":submit, :reset, :image, :disabled").not(this.settings.ignore).filter(function(){return!this.name&&b.settings.debug&&window.console&&console.error("%o has no name assigned",this),this.name in c||!b.objectLength(a(this).rules())?!1:(c[this.name]=!0,!0)})},clean:function(b){return a(b)[0]},errors:function(){var b=this.settings.errorClass.split(" ").join(".");return a(this.settings.errorElement+"."+b,this.errorContext)},reset:function(){this.successList=[],this.errorList=[],this.errorMap={},this.toShow=a([]),this.toHide=a([]),this.currentElements=a([])},prepareForm:function(){this.reset(),this.toHide=this.errors().add(this.containers)},prepareElement:function(a){this.reset(),this.toHide=this.errorsFor(a)},elementValue:function(b){var c,d=a(b),e=b.type;return"radio"===e||"checkbox"===e?this.findByName(b.name).filter(":checked").val():"number"===e&&"undefined"!=typeof b.validity?b.validity.badInput?!1:d.val():(c=d.val(),"string"==typeof c?c.replace(/\r/g,""):c)},check:function(b){b=this.validationTargetFor(this.clean(b));var c,d,e,f=a(b).rules(),g=a.map(f,function(a,b){return b}).length,h=!1,i=this.elementValue(b);for(d in f){e={method:d,parameters:f[d]};try{if(c=a.validator.methods[d].call(this,i,b,e.parameters),"dependency-mismatch"===c&&1===g){h=!0;continue}if(h=!1,"pending"===c)return void(this.toHide=this.toHide.not(this.errorsFor(b)));if(!c)return this.formatAndAdd(b,e),!1}catch(j){throw this.settings.debug&&window.console&&console.log("Exception occurred when checking element "+b.id+", check the '"+e.method+"' method.",j),j instanceof TypeError&&(j.message+=".  Exception occurred when checking element "+b.id+", check the '"+e.method+"' method."),j}}if(!h)return this.objectLength(f)&&this.successList.push(b),!0},customDataMessage:function(b,c){return a(b).data("msg"+c.charAt(0).toUpperCase()+c.substring(1).toLowerCase())||a(b).data("msg")},customMessage:function(a,b){var c=this.settings.messages[a];return c&&(c.constructor===String?c:c[b])},findDefined:function(){for(var a=0;a<arguments.length;a++)if(void 0!==arguments[a])return arguments[a];return void 0},defaultMessage:function(b,c){return this.findDefined(this.customMessage(b.name,c),this.customDataMessage(b,c),!this.settings.ignoreTitle&&b.title||void 0,a.validator.messages[c],"<strong>Warning: No message defined for "+b.name+"</strong>")},formatAndAdd:function(b,c){var d=this.defaultMessage(b,c.method),e=/\$?\{(\d+)\}/g;"function"==typeof d?d=d.call(this,c.parameters,b):e.test(d)&&(d=a.validator.format(d.replace(e,"{$1}"),c.parameters)),this.errorList.push({message:d,element:b,method:c.method}),this.errorMap[b.name]=d,this.submitted[b.name]=d},addWrapper:function(a){return this.settings.wrapper&&(a=a.add(a.parent(this.settings.wrapper))),a},defaultShowErrors:function(){var a,b,c;for(a=0;this.errorList[a];a++)c=this.errorList[a],this.settings.highlight&&this.settings.highlight.call(this,c.element,this.settings.errorClass,this.settings.validClass),this.showLabel(c.element,c.message);if(this.errorList.length&&(this.toShow=this.toShow.add(this.containers)),this.settings.success)for(a=0;this.successList[a];a++)this.showLabel(this.successList[a]);if(this.settings.unhighlight)for(a=0,b=this.validElements();b[a];a++)this.settings.unhighlight.call(this,b[a],this.settings.errorClass,this.settings.validClass);this.toHide=this.toHide.not(this.toShow),this.hideErrors(),this.addWrapper(this.toShow).show()},validElements:function(){return this.currentElements.not(this.invalidElements())},invalidElements:function(){return a(this.errorList).map(function(){return this.element})},showLabel:function(b,c){var d,e,f,g=this.errorsFor(b),h=this.idOrName(b),i=a(b).attr("aria-describedby");g.length?(g.removeClass(this.settings.validClass).addClass(this.settings.errorClass),g.html(c)):(g=a("<"+this.settings.errorElement+">").attr("id",h+"-error").addClass(this.settings.errorClass).html(c||""),d=g,this.settings.wrapper&&(d=g.hide().show().wrap("<"+this.settings.wrapper+"/>").parent()),this.labelContainer.length?this.labelContainer.append(d):this.settings.errorPlacement?this.settings.errorPlacement(d,a(b)):d.insertAfter(b),g.is("label")?g.attr("for",h):0===g.parents("label[for='"+h+"']").length&&(f=g.attr("id").replace(/(:|\.|\[|\]|\$)/g,"\\$1"),i?i.match(new RegExp("\\b"+f+"\\b"))||(i+=" "+f):i=f,a(b).attr("aria-describedby",i),e=this.groups[b.name],e&&a.each(this.groups,function(b,c){c===e&&a("[name='"+b+"']",this.currentForm).attr("aria-describedby",g.attr("id"))}))),!c&&this.settings.success&&(g.text(""),"string"==typeof this.settings.success?g.addClass(this.settings.success):this.settings.success(g,b)),this.toShow=this.toShow.add(g)},errorsFor:function(b){var c=this.idOrName(b),d=a(b).attr("aria-describedby"),e="label[for='"+c+"'], label[for='"+c+"'] *";return d&&(e=e+", #"+d.replace(/\s+/g,", #")),this.errors().filter(e)},idOrName:function(a){return this.groups[a.name]||(this.checkable(a)?a.name:a.id||a.name)},validationTargetFor:function(b){return this.checkable(b)&&(b=this.findByName(b.name)),a(b).not(this.settings.ignore)[0]},checkable:function(a){return/radio|checkbox/i.test(a.type)},findByName:function(b){return a(this.currentForm).find("[name='"+b+"']")},getLength:function(b,c){switch(c.nodeName.toLowerCase()){case"select":return a("option:selected",c).length;case"input":if(this.checkable(c))return this.findByName(c.name).filter(":checked").length}return b.length},depend:function(a,b){return this.dependTypes[typeof a]?this.dependTypes[typeof a](a,b):!0},dependTypes:{"boolean":function(a){return a},string:function(b,c){return!!a(b,c.form).length},"function":function(a,b){return a(b)}},optional:function(b){var c=this.elementValue(b);return!a.validator.methods.required.call(this,c,b)&&"dependency-mismatch"},startRequest:function(a){this.pending[a.name]||(this.pendingRequest++,this.pending[a.name]=!0)},stopRequest:function(b,c){this.pendingRequest--,this.pendingRequest<0&&(this.pendingRequest=0),delete this.pending[b.name],c&&0===this.pendingRequest&&this.formSubmitted&&this.form()?(a(this.currentForm).submit(),this.formSubmitted=!1):!c&&0===this.pendingRequest&&this.formSubmitted&&(a(this.currentForm).triggerHandler("invalid-form",[this]),this.formSubmitted=!1)},previousValue:function(b){return a.data(b,"previousValue")||a.data(b,"previousValue",{old:null,valid:!0,message:this.defaultMessage(b,"remote")})},destroy:function(){this.resetForm(),a(this.currentForm).off(".validate").removeData("validator")}},classRuleSettings:{required:{required:!0},email:{email:!0},url:{url:!0},date:{date:!0},dateISO:{dateISO:!0},number:{number:!0},digits:{digits:!0},creditcard:{creditcard:!0}},addClassRules:function(b,c){b.constructor===String?this.classRuleSettings[b]=c:a.extend(this.classRuleSettings,b)},classRules:function(b){var c={},d=a(b).attr("class");return d&&a.each(d.split(" "),function(){this in a.validator.classRuleSettings&&a.extend(c,a.validator.classRuleSettings[this])}),c},normalizeAttributeRule:function(a,b,c,d){/min|max/.test(c)&&(null===b||/number|range|text/.test(b))&&(d=Number(d),isNaN(d)&&(d=void 0)),d||0===d?a[c]=d:b===c&&"range"!==b&&(a[c]=!0)},attributeRules:function(b){var c,d,e={},f=a(b),g=b.getAttribute("type");for(c in a.validator.methods)"required"===c?(d=b.getAttribute(c),""===d&&(d=!0),d=!!d):d=f.attr(c),this.normalizeAttributeRule(e,g,c,d);return e.maxlength&&/-1|2147483647|524288/.test(e.maxlength)&&delete e.maxlength,e},dataRules:function(b){var c,d,e={},f=a(b),g=b.getAttribute("type");for(c in a.validator.methods)d=f.data("rule"+c.charAt(0).toUpperCase()+c.substring(1).toLowerCase()),this.normalizeAttributeRule(e,g,c,d);return e},staticRules:function(b){var c={},d=a.data(b.form,"validator");return d.settings.rules&&(c=a.validator.normalizeRule(d.settings.rules[b.name])||{}),c},normalizeRules:function(b,c){return a.each(b,function(d,e){if(e===!1)return void delete b[d];if(e.param||e.depends){var f=!0;switch(typeof e.depends){case"string":f=!!a(e.depends,c.form).length;break;case"function":f=e.depends.call(c,c)}f?b[d]=void 0!==e.param?e.param:!0:delete b[d]}}),a.each(b,function(d,e){b[d]=a.isFunction(e)?e(c):e}),a.each(["minlength","maxlength"],function(){b[this]&&(b[this]=Number(b[this]))}),a.each(["rangelength","range"],function(){var c;b[this]&&(a.isArray(b[this])?b[this]=[Number(b[this][0]),Number(b[this][1])]:"string"==typeof b[this]&&(c=b[this].replace(/[\[\]]/g,"").split(/[\s,]+/),b[this]=[Number(c[0]),Number(c[1])]))}),a.validator.autoCreateRanges&&(null!=b.min&&null!=b.max&&(b.range=[b.min,b.max],delete b.min,delete b.max),null!=b.minlength&&null!=b.maxlength&&(b.rangelength=[b.minlength,b.maxlength],delete b.minlength,delete b.maxlength)),b},normalizeRule:function(b){if("string"==typeof b){var c={};a.each(b.split(/\s/),function(){c[this]=!0}),b=c}return b},addMethod:function(b,c,d){a.validator.methods[b]=c,a.validator.messages[b]=void 0!==d?d:a.validator.messages[b],c.length<3&&a.validator.addClassRules(b,a.validator.normalizeRule(b))},methods:{required:function(b,c,d){if(!this.depend(d,c))return"dependency-mismatch";if("select"===c.nodeName.toLowerCase()){var e=a(c).val();return e&&e.length>0}return this.checkable(c)?this.getLength(b,c)>0:b.length>0},email:function(a,b){return this.optional(b)||/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(a)},url:function(a,b){return this.optional(b)||/^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(a)},date:function(a,b){return this.optional(b)||!/Invalid|NaN/.test(new Date(a).toString())},dateISO:function(a,b){return this.optional(b)||/^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(a)},number:function(a,b){return this.optional(b)||/^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(a)},digits:function(a,b){return this.optional(b)||/^\d+$/.test(a)},creditcard:function(a,b){if(this.optional(b))return"dependency-mismatch";if(/[^0-9 \-]+/.test(a))return!1;var c,d,e=0,f=0,g=!1;if(a=a.replace(/\D/g,""),a.length<13||a.length>19)return!1;for(c=a.length-1;c>=0;c--)d=a.charAt(c),f=parseInt(d,10),g&&(f*=2)>9&&(f-=9),e+=f,g=!g;return e%10===0},minlength:function(b,c,d){var e=a.isArray(b)?b.length:this.getLength(b,c);return this.optional(c)||e>=d},maxlength:function(b,c,d){var e=a.isArray(b)?b.length:this.getLength(b,c);return this.optional(c)||d>=e},rangelength:function(b,c,d){var e=a.isArray(b)?b.length:this.getLength(b,c);return this.optional(c)||e>=d[0]&&e<=d[1]},min:function(a,b,c){return this.optional(b)||a>=c},max:function(a,b,c){return this.optional(b)||c>=a},range:function(a,b,c){return this.optional(b)||a>=c[0]&&a<=c[1]},equalTo:function(b,c,d){var e=a(d);return this.settings.onfocusout&&e.off(".validate-equalTo").on("blur.validate-equalTo",function(){a(c).valid()}),b===e.val()},remote:function(b,c,d){if(this.optional(c))return"dependency-mismatch";var e,f,g=this.previousValue(c);return this.settings.messages[c.name]||(this.settings.messages[c.name]={}),g.originalMessage=this.settings.messages[c.name].remote,this.settings.messages[c.name].remote=g.message,d="string"==typeof d&&{url:d}||d,g.old===b?g.valid:(g.old=b,e=this,this.startRequest(c),f={},f[c.name]=b,a.ajax(a.extend(!0,{mode:"abort",port:"validate"+c.name,dataType:"json",data:f,context:e.currentForm,success:function(d){var f,h,i,j=d===!0||"true"===d;e.settings.messages[c.name].remote=g.originalMessage,j?(i=e.formSubmitted,e.prepareElement(c),e.formSubmitted=i,e.successList.push(c),delete e.invalid[c.name],e.showErrors()):(f={},h=d||e.defaultMessage(c,"remote"),f[c.name]=g.message=a.isFunction(h)?h(b):h,e.invalid[c.name]=!0,e.showErrors(f)),g.valid=j,e.stopRequest(c,j)}},d)),"pending")}}});var b,c={};a.ajaxPrefilter?a.ajaxPrefilter(function(a,b,d){var e=a.port;"abort"===a.mode&&(c[e]&&c[e].abort(),c[e]=d)}):(b=a.ajax,a.ajax=function(d){var e=("mode"in d?d:a.ajaxSettings).mode,f=("port"in d?d:a.ajaxSettings).port;return"abort"===e?(c[f]&&c[f].abort(),c[f]=b.apply(this,arguments),c[f]):b.apply(this,arguments)})});