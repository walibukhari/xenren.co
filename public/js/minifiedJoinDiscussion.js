jQuery(
	function ($) {
		
		//$('button[id^="btn-make-offer-"]').click(function(){
		//    var projectId = parseInt(this.id.replace("btn-make-offer-", ""));
		//    $('.modal-project-id').val(projectId);
		//
		//    var questions = JSON.parse($('.question-' + projectId).val());
		//
		//    $('.question-section').html('');
		//    for(var i in questions) {
		//        var result = '<div class="form-group">' +
		//            '<label>Question ' + ( parseInt(i) + 1 )+ ' : ' + questions[i].question + ' </label>' +
		//            '<input class="form-control" name="answer[]" value="" type="text">' +
		//            '<input type="hidden" class="form-control" name="questionId[]" value="' + questions[i].id + '" type="text">' +
		//            '</div>';
		//        $('.question-section').append(result);
		//    }
		//
		//    $('#make-offer-form').resetForm();
		//
		//    $('#tbxQQId').val($('.qq-id').val());
		//    $('#tbxWechatId').val($('.wechat-id').val());
		//    $('#tbxSkypeId').val($('.skype-id').val());
		//    $('#tbxHandphoneNo').val($('.handphone-no').val());
		//
		//    $('#model-alert-container').html('');
		//});
		//
		//$('#btn-submit').click(function(){
		//    $('#make-offer-form').submit();
		//});
		//
		//$('#make-offer-form').ajaxForm({
		//    beforeSubmit: function() {
		//        App.blockUI({
		//            target: '#make-offer-form',
		//            overlayColor: 'none',
		//            centerY: true,
		//            boxed: true
		//        });
		//    },
		//    success: function(resp) {
		//        if (resp.status == 'success') {
		//            App.alert({
		//                type: 'success',
		//                icon: 'check',
		//                message: resp.msg,
		//                place: 'append',
		//                closeInSeconds: 3,
		//                container: '#model-alert-container',
		//                reset: true,
		//                focus: true
		//            });
		//
		//            App.unblockUI('#make-offer-form');
		//            $('#modalMakeOffer').scrollTop(0);
		//
		//            setTimeout(function() {
		//                $('.modal').modal('hide');
		//                $('#make-offer-form').resetForm();
		//                location.reload();
		//            }, 3 * 1000);
		//        }else{
		//            App.alert({
		//                type: 'danger',
		//                icon: 'warning',
		//                message: resp.msg,
		//                container: '#model-alert-container',
		//                reset: true,
		//                focus: true
		//            });
		//
		//            App.unblockUI('#make-offer-form');
		//            $('#modalMakeOffer').scrollTop(0);
		//        }
		//    },
		//    error: function(response, statusText, xhr, formElm){
		//        if (response.status == 422) {
		//            $.each(response.responseJSON, function(i) {
		//                $.each(response.responseJSON[i], function(key, value) {
		//                    App.alert({
		//                        type: 'danger',
		//                        icon: 'warning',
		//                        message: value,
		//                        container: '#model-alert-container',
		//                        reset: true,
		//                        focus: true
		//                    });
		//                });
		//            });
		//        }
		//
		//        App.unblockUI('#make-offer-form');
		//        $('#modalMakeOffer').scrollTop(0);
		//    }
		//});
		//
		//$('#ckbPayType').on('switchChange.bootstrapSwitch', function (event, state) {
		//    //true = hourly pay, false = fixed price
		//    if (state == true) {
		//        var hourly_pay = $("#hourly_pay").val();
		//        $('#price').val(hourly_pay);
		//        $('#pay_type').val("1");
		//    }
		//    else {
		//        $('#price').val("0.00");
		//        $('#pay_type').val("2");
		//    }
		//
		//});
		// jquery send messages functions
		
		//load messages
		setTimeout(function(){
			var url = $('.project-conversation').data('url');
			$.ajax({
				url: url,
				dataType: 'json',
				method: 'get',
				beforeSend: function() {
				},
				success: function(result) {
					if(result.status == 'OK'){
						if( result.chatLog.length == 0)
						{
							var chatRoomEmptyIcon =     "<ul class='chats'>" +
								"<li>" +
								"<div class='no-chat-room-messege-sect text-center'>" +
								"<p class='top-text'>" +
								lang.chat_room_is_empty +
								"</p>" +
								"<div class='no-chat-room-img'>" +
								"<img src='\\images\\no-chat-room-message.png'>" +
								"</div>" +
								"<div class='download-text'>" +
								"<div class='recommended'>" +
								"<img src='\\images\\trello.png'>" +
								"<p>" +
								lang.recommended_management_tool_for_project +
								"</p>" +
								"<a target='_blank' class='download' href='https://trello.com/' >" +
								"www.trello.com"+
								"</a>" +
								"</div>" +
								"</div>" +
								"<div class='download-text'>" +
								"<div class='recommended'>" +
								"<img src='\\images\\print-screen.png'>" +
								"<p>" +
								lang.recommended_screenshot_tool_for_project +
								"</p>" +
								"<a target='_blank' class='download setDownload' href='http://app.prntscr.com/en/download.html' >" +
								"www.prntscr.com"+
								"</a>" +
								"</div>" +
								"</div>" +
								"</div>" +
								"</li>" +
								"</ul>"
							$('#content-chat').html(chatRoomEmptyIcon)
						}
						else
						{
							$('#content-chat').html('<ul class="chats"></ul>');
							$.each(result.chatLog, function (i, e) {
								var changeDate = '';
								if (e.changeDate == 1) {
									// changeDate = '<li><div class="strike"><span>'+e.date+'</span></div></li>';
								}
								var image = '';
								if (e.isImage == 1) {
									image = '<br><a href="/' + e.file + '" data-lightbox="image-' + e.id + '"><img src="/' + e.file + '" class"img-thumbnail" width="75px" data-lightbox="image-' + e.id + '"></a>'
								}
								var audio = '';
								if (e.isAudio == 1) {
									audio = '<br><audio controls><source src="/' + e.file + '" type="audio/ogg"><source src="' + e.file + '" type="audio/mp3">Your browser does not support the audio element.</audio>'
								}
								// $('#content-chat ul').append(changeDate + '<li class="' + e.status + '">' +
								$('#content-chat ul').append(changeDate + '<li class="in 8">' +
									"<img class='avatar setAvater Image_highlight_sliver Image_highlight_brown Image_highlight_gold'  src=" + e.avatar + " />" +
									'<div class="message">' +
									'<a href="javascript:void(0);" class="name setName" onClick="window.location.href=\'/resume/getDetails/'+e.id+'\'"> ' + e.email + ' </a>' + '<br>'+
									'<i class="datetime setDateTime"> ' + e.time + '</i>' +
									'<p class="body setMessageBody"> ' + e.content + ' ' + image + '' + audio + '</p>' +
									'</div>' +
									'</li>');
							});
							
							//always scroll to bottom
							var scrollHeight = $('#content-chat').prop('scrollHeight');
							$("#content-chat").slimScroll({scrollTo: scrollHeight});
						}
						
					}
				},
				error: function(a, b) {
					if (a.status != 422) {
						alert('Unknown error occured, please try again later');
					}
				}
			});
			
			//var urlFile = $('.order-conversation').data('urlfile');
			//$.ajax({
			//    url: urlFile,
			//    dataType: 'json',
			//    method: 'get',
			//    beforeSend: function() {
			//    },
			//    success: function(result) {
			//        console.log(result);
			//        if(result.status == 'OK' && result.count > 0){
			//            $('#file-content').html('');
			//            $('.file-upload .slimScrollDiv').css('height', '300px');
			//            $('#content-file').css('height', '300px');
			//            $.each(result.chatLog, function(i, e) {
			//                $('.file-list').append(
			//                    '<li>' +
			//                    '<div class="file-logo pull-left">' +
			//                    '<img alt="file-logo" src="/images/file-logo.png">' +
			//                    '</div>' +
			//                    '<div class="file-info text-left">' +
			//                    'filename : '+e.content+'<br/>' +
			//                    '<span class="size-datetime">'+e.size+' - '+e.time+'</span><br>' +
			//                    '<a href="/'+e.file+'" class="download-file">下载纸样</a>' +
			//                    '</div>' +
			//                    '</li>');
			//            });
			//        }
			//    },
			//    error: function(a, b) {
			//        if (a.status != 422) {
			//            alert('Unknown error occured, please try again later');
			//        }
			//    }
			//});
		}, 1000);
		
		//upload image modal show
		$('#chat-message-img').click(function(e){
			e.preventDefault();
			var url = $(this).data('url');
			$('#add-order-file-chat').attr('action',url);
			$('#modalAddFileChat').modal('show');
		});
		//file send modal show
		$('#chat-message-file').click(function(e){
			e.preventDefault();
			var url = $(this).data('url');
			$('#add-order-file-chat').attr('action',url);
			$('#modalAddFileChat').modal('show');
		});
		//send messages
		$('.chat-form form').submit(function(e){
			e.preventDefault();
			var url = $(this).attr('action');
			var data = $(this).serialize();
            console.log('============================');
            console.log(url);
            var edit = ($('.sendMessage').attr('data-edit'));
            if(edit == 'true') {
            	var id = $('.sendMessage').attr('data-id');
            	var chatroomid = $('.sendMessage').attr('data-chatroomid');
                console.log('edit case here don\'t send new message now please');
                url = '/updateLastMessage?id='+id;
            }
            $.ajax({
				url: url,
				data: data,
				dataType: 'json',
				method: 'post',
				statusCode: {
					422: function (resp) {
						var html = '';
						var json = $.parseJSON(resp.responseText);

						$.each(json, function(index, value) {
							$.each(value, function(i, msg) {
								alertError(msg, 'test');
							})
						});

						//$('.chat-form .error-msg').html(html);
					}
				},
				beforeSend: function() {
					$('.chat-form .error-msg').html('');
				},
				success: function(result) {
					if(result.status == "OK"){
						$('#content-chat ul').append('<li class="in 9">'+
							'<img class="avatar" alt="test" src="'+result.avatar+'" />'+
							'<div class="message">'+
							//'<span class="arrow"> </span>'+
							// '<a href="javascript:void(0);" class="name"> '+result.realName+' </a>'+
							'<a href="javascript:void(0);" class="name"> You </a>'+
							'<p class="datetime"> ' + result.time + '</p>'+
							'<p class="body"> '+ result.message +' </p>'+
							'</div>'+
							'</li>');
						$('.chat-form .message').val('');

						//always scroll to bottom
						var scrollHeight =  $('#content-chat').prop('scrollHeight');
						$("#content-chat").slimScroll({ scrollTo: scrollHeight });
					} else {
                        $.ajax({
                            url: '/projectChat/pullProjectChat/'+chatroomid,
                            dataType: 'json',
                            method: 'get',
                            beforeSend: function() {
                            },
                            success: function(result) {
                                if(result.status == 'OK'){
									$('#content-chat').html('<ul class="chats"></ul>');
									$.each(result.chatLog, function (i, e) {
										var changeDate = '';
										if (e.changeDate == 1) {
											// changeDate = '<li><div class="strike"><span>'+e.date+'</span></div></li>';
										}
										var image = '';
										if (e.isImage == 1) {
											image = '<br><a href="/' + e.file + '" data-lightbox="image-' + e.id + '"><img src="/' + e.file + '" class"img-thumbnail" width="75px" data-lightbox="image-' + e.id + '"></a>'
										}
										var audio = '';
										if (e.isAudio == 1) {
											audio = '<br><audio controls><source src="/' + e.file + '" type="audio/ogg"><source src="' + e.file + '" type="audio/mp3">Your browser does not support the audio element.</audio>'
										}
										// $('#content-chat ul').append(changeDate + '<li class="' + e.status + '">' +
										$('#content-chat ul').append(changeDate + '<li class="in 8">' +
											"<img class='avatar setAvater Image_highlight_sliver Image_highlight_brown Image_highlight_gold'  src=" + e.avatar + " />" +
											'<div class="message">' +
											'<a href="javascript:void(0);" class="name setName" onClick="window.location.href=\'/resume/getDetails/'+e.id+'\'"> ' + e.email + ' </a>' + '<br>'+
											'<i class="datetime setDateTime"> ' + e.time + '</i>' +
											'<p class="body setMessageBody"> ' + e.content + ' ' + image + '' + audio + '</p>' +
											'</div>' +
											'</li>');
									});

									//always scroll to bottom
									var scrollHeight = $('#content-chat').prop('scrollHeight');
									$("#content-chat").slimScroll({scrollTo: scrollHeight});
									$('#message').val('');
                                }
                            },
                            error: function(a, b) {
                                if (a.status != 422) {
                                    alert('Unknown error occured, please try again later');
                                }
                            }
                        });
					}
				},
				error: function(a, b) {
					if (a.status != 422) {
						alert('Unknown error occured, please try again later');
					}
				}
			});
			return false;
		});
		
		//$('#invite-helpdesk').click(function(e){
		//    e.preventDefault();
		//    var url = $(this).data('url');
		//    $.ajax({
		//        url: url,
		//        dataType: 'json',
		//        method: 'get',
		//        statusCode: {
		//            422: function (resp) {
		//                var html = '';
		//                var json = $.parseJSON(resp.responseText);
		//
		//                $.each(json, function(index, value) {
		//                    $.each(value, function(i, msg) {
		//                        alertError(msg, 'test');
		//                    })
		//                });
		//
		//                //$('.chat-form .error-msg').html(html);
		//            }
		//        },
		//        beforeSend: function() {
		//            $('.chat-form .error-msg').html('');
		//        },
		//        success: function(result) {
		//            if(result.status == "OK"){
		//                $('#content-chat ul').append('<li class="in">'+
		//                                                '<img class="avatar" alt="" src="'+result.avatar+'" />'+
		//                                                '<div class="message">'+
		//                                                    '<span class="arrow"> </span>'+
		//                                                    '<a href="javascript:void(0);" class="name"> '+result.realName+' </a>'+
		//                                                    '<span class="body"> '+ result.message +' </span>'+
		//                                                '</div>'+
		//                                                '<div class="div-datetime">' +
		//                                                    '<span class="datetime"> '+result.time+'</span>'+
		//                                                '</div>'+
		//                                            '</li>');
		//                alertSuccess('Invitation sent', 'clear');
		//            }
		//        },
		//        error: function(a, b) {
		//            if (a.status != 422) {
		//                alert('Unknown error occured, please try again later');
		//            }
		//        }
		//    });
		//    return false;
		//});
		
		$('#add-project-file-chat').submit(function(e){
			e.preventDefault();
			var url = $(this).attr('action');
			var data = new FormData( this );
			console.log(data);
			$.ajax({
				url: url,
				data: data,
				dataType: 'json',
				processData: false,
				contentType: false,
				method: 'post',
				statusCode: {
					422: function (resp) {
						var html = '';
						var json = $.parseJSON(resp.responseText);
						
						$.each(json, function(index, value) {
							$.each(value, function(i, msg) {
								alertError(msg, 'test');
							})
						});
						
						//$('.chat-form .error-msg').html(html);
					}
				},
				beforeSend: function() {
					$('.chat-form .error-msg').html('');
				},
				success: function(result) {
					if(result.status == "OK"){
						$('#content-chat ul').append(result.contents);
						
						//close modal
						$('#modalAddFileChat').modal('toggle');
						
						//always scroll to bottom
						var scrollHeight =  $('#content-chat').prop('scrollHeight');
						$("#content-chat").slimScroll({ scrollTo: scrollHeight });
					}
				},
				error: function(a, b) {
					if (a.status != 422) {
						alert('Unknown error occured, please try again later');
					}
				}
			});
			return false;
		});
		
		$('.btn-chat-room').click(function(){
			$('.chat-section').show();
			$('.freelance-detail-section').hide();
			$('.btn-freelance-detail').removeClass('active');
			$('.btn-chat-room').addClass('active');
		});
		
		$('.btn-freelance-detail').click(function(){
			$('.chat-section').hide();
			$('.freelance-detail-section').show();
			$('.btn-freelance-detail').addClass('active');
			$('.btn-chat-room').removeClass('active');
		});
		
		$('body').on('click', 'button[id^="btn-choose-"]', function () {
			var userId = parseInt(this.id.replace("btn-choose-", ""));
			var projectId =  parseInt($('#projectId').val());
			var url = $('#choose_url').val();
			
			$.ajax({
				url: url,
				dataType: 'json',
				method: 'get',
				data: { userId: userId, projectId: projectId },
				beforeSend: function() {
				},
				success: function(result) {
					if(result.status == 'success'){
						window.location.href = '/joinDiscussion/getProject/' + projectId;
					}
				},
				error: function(a, b) {
					if (a.status != 422) {
						alert('Unknown error occured, please try again later');
					}
				}
			});
		});
		
		//creator reject applicant
		$('.btn-reject').click(function (e) {
			e.preventDefault();
			var url = $(this).data('url');
			$.ajax({
				url: url,
				dataType: 'json',
				method: 'post',
				success: function (result) {
					if (result.status == "success") {
						alertSuccess(lang.submit_successful);
						setTimeout(function () {
							location.reload();
						}, 1000);
					}
				},
				error: function (a, b) {
				}
			});
		});
		
		//creator reject applicant
		$('.btn-award').on('click', function (e) {
			e.preventDefault();
			var url = $(this).data('url');
			alert(url );
			$.ajax({
				url: url,
				dataType: 'json',
				method: 'post',
				success: function (result) {
					if (result.status == "success") {
						alertSuccess(lang.submit_successful);
						setTimeout(function () {
							location.reload();
						}, 1000);
					}
				},
				error: function (a, b) {
				}
			});
		});
		
		//check applicant details
		$('.btn-check-detail').click(function(){
			var url = $(this).data('url');
			window.location.href = url;
		});
		
		//creator thumb up chat room follower
		$('body').on('click', 'button[class^="btn-thumb-up-"]', function (e) {
			e.preventDefault();
			var url = $(this).data('url');
			var id = $(this).data('id');
			$.ajax({
				url: url,
				dataType: 'json',
				method: 'post',
				success: function (result) {
					if (result.status == "success") {
						$('.btn-thumb-up-' + id ).addClass("btn-thumbs-color").removeClass("btn-thumbs-default");
						$('.btn-thumb-down-' + id ).addClass("btn-thumbs-default").removeClass("btn-thumbs-color");
					}
				},
				error: function (a, b) {
				}
			});
		});
		
		//creator thumb down chat room follower
		$('body').on('click', 'button[class^="btn-thumb-down-"]', function (e) {
			e.preventDefault();
			var url = $(this).data('url');
			var id = $(this).data('id');
			$.ajax({
				url: url,
				dataType: 'json',
				method: 'post',
				success: function (result) {
					if (result.status == "success") {
						$('.btn-thumb-up-' + id ).addClass("btn-thumbs-default").removeClass("btn-thumbs-color");
						$('.btn-thumb-down-' + id).addClass("btn-thumbs-color").removeClass("btn-thumbs-default");
					}
				},
				error: function (a, b) {
				}
			});
		});
		
		//quit the discussion
		// $('body').on('click', 'id="btnQuitInterview"', function (e) {
		//     e.preventDefault();
		//     var url = $(this).data('url');
		//     var id = $(this).data('id');
		//     $.ajax({
		//         url: url,
		//         dataType: 'json',
		//         method: 'post',
		//         data: { id: id },
		//         success: function (result) {
		//             if (result.status == "success") {
		//                 $('.chat-follower-' + id).hide();
		//             }
		//         },
		//         error: function (a, b) {
		//         }
		//     });
		// });
		//
		// $('body').on('click',function(e){
		//     e.preventDefault();
		//     $('.bsBox').hide();
		// });
		
		//get the description width and the image not allow bigger than div description width
		var divDescriptionWidth = $('.description').width();
		console.log( " DivDescriptionWidth : " + divDescriptionWidth );
		$('.description img').each(function( index, element ) {
			console.log( "Image Width : " + element.width );
			if( element.width > divDescriptionWidth )
			{
				$(element).attr('style', 'width:100%; height:100%;')
			}
		});
		
		$('.scroll-div').slimScroll({
			height: '100px',
		});
	}
);

jQuery(
	function ($) {
		$('.btn-check-detail').click(function(){
			var url = $(this).data('url');
			window.location.href = url;
		});
		
		$('.btn-remove-fav-job').click(function(){
			var url = $(this).data('url');
			var projectId = $(this).data('project-id');
			var targetEle = $(this);
			
			$.ajax({
				url: url,
				dataType: 'json',
				data: {projectId: projectId},
				method: 'POST',
				error: function () {
					alert(lang.unable_to_request);
				},
				success: function (result) {
					console.log("result");
					console.log(result);
					console.log("result");
					if (result.status === 'OK') {
						$('#project-id-'+ projectId).hide();
						alertSuccess(result.message);
						$('.set-fa-fa-fa-starsss').addClass('fav-add-active');
					}else if(result.status === 'ok'){
						$('#project-id-'+ projectId).hide();
						alertSuccess(result.message);
						$('.set-fa-fa-fa-starsss').removeClass('fav-add-active');

					} else if (result.status === 'Error') {
						alertError(result.message);
					}
				}
			});
		});
		
		$('.icon-link').on('click', function(){
			var success   = true,
				range     = document.createRange(),
				selection;
			var tmpElem = $('<div>');
			tmpElem.css({
				position: "absolute",
				left:     "-1000px",
				top:      "-1000px",
			});
			// Add the input value to the temp element.
			tmpElem.text(window.location.href);
			$("body").append(tmpElem);
			// Select temp element.
			range.selectNodeContents(tmpElem.get(0));
			selection = window.getSelection ();
			selection.removeAllRanges ();
			selection.addRange (range);
			// Lets copy.
			try {
				success = document.execCommand ("copy", false, null);
			}
			catch (e) {
				copyToClipboardFF(window.location.href);
			}
			if (success) {
				alertSuccess ("Link copied!");
				// remove temp element.
				tmpElem.remove();
			}
		});
	}
);
/*
* Author:  Gopal Joshi
* Website: www.sgeek.org
*/

(function($) {
    $.fn.Sswitch = function(element, options ) {
        this.$element = $(this);
        
        this.options = $.extend({}, $.fn.Sswitch.defaults, {
            state: this.$element.is(":checked"),
            disabled: this.$element.is(":disabled"),
            readonly: this.$element.is("[readonly]"),
            parentClass: this.$element.data("parent"),
            onSwitchChange: element.onSwitchChange
        },options);

        this.$container = $("<div>", {
          "class": (function(_this){
                return function(){
                    var classes;
                    classes = [_this.options.parentClass];
                    classes.push(_this.options.state ? "" + _this.options.parentClass + "-on" : "" + _this.options.parentClass + "-off");
                    if (_this.options.disabled) {
                        classes.push("" + _this.options.parentClass + "-disabled");
                    }
                    if (_this.options.readonly) {
                        classes.push("" + _this.options.parentClass + "-readonly");
                    }
                    if (_this.$element.attr("id")) {
                        classes.push("" + _this.options.parentClass + "-id-" + (_this.$element.attr("id")));
                    }
                    return classes.join(" ");
                };
            })(this)()
        });
        this.$label = $("<span>", {
          html: this.options.labelText,
          "class": "" + this.options.parentClass + "-label"
        });
        this.$container = this.$element.wrap(this.$container).parent();
        this.$element.before(this.$label);
        
        return this.$container.on("click", (function(_this) {
            return function(event) {
                event.preventDefault();
                event.stopPropagation();
                if (_this.options.readonly || _this.options.disabled) {
                  return _this.target;
                }
                _this.options.state = !_this.options.state;
                _this.$element.prop("checked", _this.options.state);
                _this.$container.addClass(_this.options.state ? "" + _this.options.parentClass + "-on" : "" + _this.options.parentClass + "-off").removeClass(_this.options.state ? "" + _this.options.parentClass + "-off" : "" + _this.options.parentClass + "-on");
                _this.options.onSwitchChange.call(_this);
                return _this;
            };
        })(this));
        return this.$element;
    },
    $.fn.Sswitch.defaults = {
        text     : 'Default Title',
        fontsize : 10,
        state: true,
        disabled: false,
        readonly: false,
        parentClass: "s-switch",
        onSwitchChange: function() {}
    };
}(jQuery));
jQuery(
    function ($) {
        //template
        var template = '<div class="row">' +
                            '<label class="col-md-12 p-t-40">' +
                                lang.new_skill_keywords +
                            '</label>' +
                            '<div class="col-md-12">' +
                                '<input class="form-control" name="tbxSkillKeyword[]" value="" type="text">' +
                            '</div>' +
                        '</div>' +
                        '<div class="row">' +
                            '<label class="col-md-12 p-t-20">' +
                                lang.please_use_wiki_or_baidu_support_your_keyword +
                            '</label>' +
                            '<div class="col-md-12">' +
                                '<input class="form-control" name="tbxReferenceUrl[]" value="" type="text">' +
                            '</div>' +
                        '</div>';


        $('#btnAddMore').click(function()
        {
            var errorExist = false;
            $( "input[ name = 'tbxSkillKeyword[]']" ).each(function( index )
            {
                console.log( index + ": " + $( this ).val() );
                if(  $( this ).val() == "" )
                {
                    alert(lang.one_of_skill_input_empty);
                    errorExist = true;
                }
            });

            if( !errorExist)
            {
                var count = parseInt( $('#hidInputCount').val() );
                if( count == 10 )
                {
                    alert(lang.maximum_ten_skill_keywords_allow_to_submit);
                    return false;
                }
                else
                {
                    $('.add-more-section').append(template);
                    $('#hidInputCount').val(count + 1);
                }
            }

        });

        //submit the keyword
        $('#add-new-skill-keyword-form').makeAjaxForm({
          
            submitBtn: '#btn-submit-new-skill-keyword',
            alertContainer : '#add-new-skill-keyword-errors',
            clearForm: true,

            afterSuccessFunction: function (response, $el, $this) {
                if( response.status == 'success'){
                    App.alert({
                        type: 'success',
                        icon: 'check',
                        message: response.msg,
                        place: 'append',
                        container: '#body-alert-container',
                        reset: true,
                        focus: true
                    });
                    setTimeout(function(){
                        $('#modelAddNewSkillKeyword').modal('toggle');
                    }, 3000);
                }else{
                    App.alert({
                        type: 'danger',
                        icon: 'warning',
                        message: response.msg,
                        container: '#add-new-skill-keyword-errors',
                        reset: true,
                        focus: true
                    });
                }

                App.unblockUI('#add-new-skill-keyword-form');
            }
        });
    }
);

