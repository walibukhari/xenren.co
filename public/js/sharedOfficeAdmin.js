let vu = new Vue({
    el: '#vueJs',
    data: {
        activeColor:'#EBEBEB',
        productsOrg: [],
        products: [],
        financeProducts: [],
        sharedOfficeRating:[],
        sharedOfficeRatingAverage:0,
        sharedOfficeRatingPercentage:50,
        showLoader: true,
        addUserPanel: false,
        // totalPersons: 10,
        totalAmount: 0,
        financeTotalAmount: 0,
        // personsUsage: [],
        startTimeFilter: 0,
        endTimeFilter: 0,
        personsAdded: [],
        personsAddedObj: [],
        hideShowCheck:false,
        sharedOfficeUsers:[],
        userDetails:'',
        userSkills:[],
        officelS:'',
        clientVerifiedS:'',
        normalS:'',
        messagesSend:'',
        hideMessage:false,
        getOfficeMessages:[],
        getStaffM:[],
        staffMessages:[],
        filename:'',
        hideshowImage:false,
        searchBox:'',
        OriginalData:[],
        checkedSharedOffice:'',
        payment_request:'',
        bank_name:'',
        iban_number:'',
        card_number:'',
        branch_name:'',
        branch_code:'',
        branch_address:'',
    },
    methods: {
        getDetail() {
            this.showLoader = true;
            let self = this;
            setTimeout(function () {
                let date = $('#datetime').val();
                self.$http.get('/admin/sharedOfficeProducts/{{$id}}?date=' + date).then(function (response) {
                    self.showLoader = false;
                    self.products = response.body.data;
                    self.productsOrg = response.body.data;
                }, function (error) {
                    self.showLoader = false;
                });
            }, 100)
        },
        getTodaySharedOfficeRating() {
            let self = this;
            setTimeout(function () {
                let date = new Date().toDateString();
                self.$http.get('/admin/getTodaySharedOfficeRating/{{$id}}?date=' + date).then(function (response) {
                    self.sharedOfficeRating = response.body.data;
                    self.sharedOfficeRatingAverage = response.body.average;
                    self.sharedOfficeRatingPercentage = response.body.ratingPercent[0];
                }, function (error) {
                    //
                });
            }, 100);
        },
        saveUser(cb) {
            let data = new FormData();
            data.append('username', $('#username').val());
            data.append('start_time', $('#start_time').val());
            data.append('end_time', $('#end_time').val());
            data.append('seat_no', $('#seat_no').val());
            data.append('office_id', '{{$id}}');
            let self = this;
            self.$http.post('/admin/sharedOfficeProductsCheckUsers', data).then((response) => {
                if (response.body.status == 'failure') {
                    toastr.error(response.body.message);
                    cb(true)
                } else {
                    toastr.success(response.body.message);
                    cb(null, true)
                }
            }, (response) => {
                cb(true)
            })

        },
        addUser(cb) {
            let self = this;
            var validated = [];
            validated = this.personsAddedObj.filter(function (data) {
                if (data['number'] == self.personsAdded['number'] || data['name'] == self.personsAdded['name']) {
                    return data;
                }
            });
            if (validated.length > 0) {
                toastr.error('Seat/User already selected');
                cb(true);
            } else {
                let headers = new Headers();
                headers.append('Content-Type', 'application/json');
                self.$http.post('/admin/sharedOfficeProductsCheckUsers', {
                    username: self.personsAdded['name'],
                    start_time: self.personsAdded['start_time'],
                    end_time: self.personsAdded['end_time'],
                    seat_no: self.personsAdded['number'],
                    cost: 0,
                    office_id: '{{$id}}'
                }, {
                    headers: headers
                }).then((response) => {
                    if (response.body.status == 'failure') {
                        toastr.error(response.body.message);
                        cb(true)
                    } else {
                        self.personsAddedObj.push((self.personsAdded));


                        self.personsAdded = [];
                        cb(null, true)
                    }
                }, (response) => {
                    cb(true)
                });
            }
        },
        getClass(data) {
            if (data === 0)
                return 'availables';
            if (data === 1)
                return 'inuse';
            if (data === 0)
                return 'booking';
        },
        formatDate(date) {
            var format = 'Do MMMM, h:mm a';
            return moment(date).format(format)
        },
        addUserPanelFnc(showPanel) {
            let self = this;
            if (showPanel == false) {
                // save user
                this.addUser(function (err, success) {
                    if (!err) {
                        self.addUserPanel = showPanel;
                    }
                });
            } else {
                this.addUserPanel = showPanel;
            }
        },
        showPanel() {
            if (this.addUserPanel) {
                return 'display: block';
            } else {
                return 'display: none';
            }
        },
        changeStart(event) {
            this.startTimeFilter = event.target.value;
        },
        changeEnd(event) {
            this.endTimeFilter = event.target.value;
        },
        changeStartTime(val) {
            this.personsAdded.start_time = val;
        },
        changeEndTime(val) {
            this.personsAdded.end_time = val;
        },
        save() {
            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            let formData = new FormData();
            let obj = {};
            let index = 0
            this.personsAddedObj.map(function(d){
                let a =  {
                    end_time: d.end_time,
                    month_price: d.month_price,
                    name: d.name,
                    number: d.number,
                    product_type_name: d.product_type_name,
                    start_time: d.start_time,
                    office_id: '{{$id}}'
                };
                obj[index] = (a);
                index++;
            });

            this.$http.post('/admin/sharedOfficeProductsAddUsers', JSON.stringify(obj),headers).then((response) => {
                if(response.body.status == 'error') {
                    toastr.error(response.body.message);
                } else {
                    toastr.success(response.body.message);
                    this.getDetail();
                    $('#myModal').modal('hide');
                }
            }, (response) => {
                toastr.error(response.body.message);
            });
        },
        convertToJSON(array) {
            let obj = array.map(function(d){
                return {
                    end_time: d.end_time,
                    month_price: d.month_price,
                    name: d.name,
                    number: d.number,
                    product_type_name: d.product_type_name,
                    start_time: d.start_time,
                };
            });
            return obj;
        },
        applyFilter() {
            let dateFilter = null;
            let periorityFilter = null;
            let self = this;
            if (this.startTimeFilter) {
                this.products.office_products = this.products.office_products.filter(function (data) {
                    if (data.startDateTime) {
                        let t1 = (moment(data.startDateTime, "HH:mm:ss"));
                        let st = (moment(self.startTimeFilter, "HH:mm:ss"));
                        let et = (moment(self.endTimeFilter, "HH:mm:ss"));
                        if ((t1.isBefore(et)) && t1.isAfter(st)) {
                            return data;
                        }
                    }
                });
            }
        },
        initializeSelect2() {
            let self = this;
            let select = $('#seats');
            select
                .select2({
                    placeholder: 'Enter Desk/Room and Seat No',
                    theme: 'bootstrap',
                    width: '100%',
                    allowClear: true,
                    data: this.select2data
                })
                .on('change', function () {
                    let id = select.val();
                    let rec = self.productsOrg.office_products.filter(function (data) {
                        if (parseInt(data.id) === parseInt(id)) {
                            return data;
                        }
                    });
                    self.personsAdded.product_type_name = rec[0].product_type_name;
                    self.personsAdded.month_price = rec[0].month_price;
                    self.personsAdded.number = rec[0].number;

                });
            select.val(this.value).trigger('change')
        },
        openSection(id)
        {
            let self = this;
            var link = '/admin/sharedOffice/user/details/'+id;
            self.$http.get(link).then(function(response){
                self.userDetails = response.body;
                self.userSkills = response.body.skills;
                var officalSkills = [];
                var clientVerified = [];
                var normalSkills = [];
                self.userSkills.map(function(q){
                    if(q.experience >= 1)
                    {
                        officalSkills.push(q.skill.name_cn);
                    } else if(q.is_client_verified === 1)
                    {
                        clientVerified.push(q.skill.name_cn);
                    } else if(q && q.skill_id) {
                        normalSkills.push(q.skill.name_cn);
                    }
                });
                self.officelS = officalSkills;
                self.clientVerifiedS = clientVerified;
                self.normalS = normalSkills;
            }, function(error){
            });

            $('.set-custom-width-col').addClass('removeCustomWidth');
            this.hideShowCheck = true;
        },
        getSharedofficeUsers()
        {
            let self = this;
            var office_id = '{{$id}}';
            var link = "/admin/sharedOffice/users/"+office_id;
            self.$http.get(link).then(function(response){
                self.sharedOfficeUsers = response.body.data;
            }, function(error){
            });
        },
        getUserImage(users)
        {
            return users.img_avatar;
        },
        lastLoginTime(users)
        {
            var date = moment(users.last_login_at).fromNow("m");
            var d = date.replace('minutes','min');
            if(d === 'an hour') {
                d = '1 hour';
            }
            if(d === 'a few seconds')
            {
                d = '1 sec';
            }
            if(d === 'a minute')
            {
                d = '1 minute';
            }
            if(d === 'a day')
            {
                d = '1 day';
            }
            if(d === 'a month')
            {
                d = '1 month';
            }
            return d;
        },
        getSharedofficeChat()
        {
            let self = this;
            var office_id = '{{$id}}';
            var link = '/admin/sharedOffice/room/chat/'+office_id;
            self.$http.get(link).then(function(response){
                this.getOfficeMessages = response.body.data;
                this.OriginalData = response.body.data;
            }, function(error){
            });
        },
        getUserMessageImages(messages)
        {
            var users = messages.users;
            var staff = messages.staff_data;
            var user_image = '';
            users.map(function(data){
                user_image = data.img_avatar;
            });
            staff.map(function(data){
                user_image = '/'+data.img_avatar;
            });
            return user_image;
        },
        getUserData(messages)
        {
            var users = messages.users;
            var staff = messages.staff_data;
            var user_data = '';
            users.map(function(data){
                user_data = data.name;
            });
            staff.map(function(data){
                user_data = data.name;
            });
            return user_data;
        },
        getStaffImage(staff)
        {
            var staff_image = '';
            staff.map(function(data){
                staff_image = data.img_avatar;
            });
            return staff_image;
        },
        getStaffData(staff)
        {
            var staff_name = '';
            staff.map(function(data){
                staff_name = data.name;
            });
            return staff_name;
        },
        hitEnter()
        {
            let self = this;
            var sender_staff_id = '{!! \Auth::guard('staff')->user()->id !!}';
            var message = this.messagesSend;
            var office_id = '{{$id}}';
            var link = '/admin/sharedoffice/sendMessage';
            let form = new FormData();
            form.append('office_id',office_id);
            form.append('sender_staff_id',sender_staff_id);
            form.append('message',message);
            if(message === '')
            {
                toastr.error("Please enter your message");
            } else {
                self.$http.post(link,form).then(function(response){
                    self.messagesSend = '';
                }, function(error){
                });
                this.getSharedofficeChat();
                self.hideMessage = true;
            }
        },
        getChatCheck(messages) {
            let user_id = '{{\Auth::guard('staff')->user()->id}}';
            if(parseInt(messages.sender_staff_id) === parseInt(user_id) || parseInt(messages.sender_user_id) === parseInt(user_id))
            {
                return true;
            } else {
                return false
            }
        },
        deleteMessage(id)
        {
            let self = this;
            var link = "/admin/sharedoffice/delete/message/"+id;
            self.$http.get(link).then(function(response){
                if(response.body.success === 'success')
                {
                    setTimeout(this.getSharedofficeChat, 20);
                    toastr.success('message deleted successfully');
                }
            }, function(error){
            });
        },
        fileReader(e)
        {
            let self = this;
            var sender_staff_id = '{!! \Auth::guard('staff')->user()->id !!}';
            var file = $('#file-input')[0].files[0];
            var office_id = '{{$id}}';
            var link = '/admin/sharedoffice/sendMessage';
            let form = new FormData();
            form.append('file',file);
            form.append('office_id',office_id);
            form.append('sender_staff_id',sender_staff_id);
            self.$http.post(link,form).then(function(response){
                $('#loader').show();
                if(response.body.success === 'success')
                {
                    $('#loader').hide();
                    toastr.success('attachment upload successfully');
                }
                this.getSharedofficeChat();
            }, function(error){
                $('#changeColor').addClass('changeColor');
                toastr.error('attachment failed to upload');
            });

            var files = e.target.files;
            var fileReader = new FileReader();
            fileReader.onload = (function(e) {
                var file = e.target;
                var img = $("<img id='loader' src='/images/giphy.gif' style='display:none;height: 72px;position: absolute; z-index: 999;width: 100px;top: 39%;'><span class=\"pip\">" +
                    "<img class=\"imageThumb\" style=\"width:100px;height:70px;position:relative;top:-7px;object-fit: cover;\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>"+
                    "</span>");
                self.hideshowImage = true;
            });
            fileReader.readAsDataURL(file);
        },
        resetFilter() {
            if(this.searchBox.length > 0) {
                this.searchBox = '';
                this.getOfficeMessages = this.OriginalData;
                $('#classChange').addClass('fa-search');
                $('#classChange').removeClass('fa-close');
            }
        },
        searchResults()
        {
            let self = this;
            var text = self.searchBox;
            var arr = [];
            if(text.length > 0)
            {
                $('#classChange').removeClass('fa-search');
                $('#classChange').addClass('fa-close');
                self.OriginalData.map(function(data){
                    if(data.message.includes(text))
                    {
                        arr.push(data);
                    }
                });
                self.getOfficeMessages = arr;
            } else {
                $('#classChange').addClass('fa-search');
                $('#classChange').removeClass('fa-close');
                self.getOfficeMessages = self.OriginalData;
            }
        },
        blocked(id)
        {
            let self = this;
            var action = 1;
            var office_id = '{{$id}}';
            var user_id = id;
            let link = '/admin/sharedoffice/chat/action/'+action;
            let data = new FormData();
            data.append('user_id',user_id);
            data.append('office_id',office_id);
            self.$http.post(link, data).then(function(response){
                if(response.body.success === 'success') {
                    toastr.success('User Blocked');
                }
                if(response.body.error === 'error')
                {
                    if(response.body.data === 1)
                    {
                        toastr.error('User already Blocked');
                    }
                }
            }, function(){
            });
        },
        kicked(id){
            let self = this;
            var action = 0;
            var office_id = '{{$id}}';
            var user_id = id;
            let data = new FormData();
            data.append('user_id',user_id);
            data.append('office_id',office_id);
            let link = '/admin/sharedoffice/chat/action/'+action;
            self.$http.post(link,data).then(function(response){
                if(response.body.success === 'success') {
                    toastr.success('User Kicked');
                }
                if(response.body.error === 'error')
                {
                    if(response.body.data === 0)
                    {
                        toastr.error('User already Kicked');
                    }
                }
            }, function(){
            });
        },
        closeRightSec()
        {
            $('.set-custom-width-col').removeClass('removeCustomWidth');
            $('.set-custom-width-col').addClass('custom-width-slide');
            this.hideShowCheck = false;
        },
        getFinanceDetail(startDate,endDate) {
            startDate = new Date(startDate).toDateString();
            endDate = new Date(endDate).toDateString();
            this.showLoader = true;
            let self = this;
            setTimeout(function () {
                self.$http.get('/admin/sharedOfficeFinances/{{$id}}?startdate=' + startDate+'&enddate='+endDate).then(function (response) {
                    self.showLoader = false;
                    self.financeProducts = response.body.data;
                    console.log('response.body.data');
                    console.log(response.body.data);
                    console.log('self.financeProducts');
                    self.financeTotalAmount = response.body.totalAmount - response.body.totalDeduct;
                    console.log('self.financeTotalAmount');
                    console.log(self.financeTotalAmount);
                }, function (error) {
                    self.showLoader = false;
                });
            }, 100)
        },
        Withdrawal: function(id){
            var link = '/admin/withdraw/request';
            var formRequest = new FormData();
            formRequest.append('csrf_token','{{ csrf_token() }}');
            formRequest.append('payment_request',this.payment_request);
            formRequest.append('bank_name',this.bank_name);
            formRequest.append('iban_number',this.iban_number);
            formRequest.append('card_number',this.card_number);
            formRequest.append('branch_name',this.branch_name);
            formRequest.append('branch_code',this.branch_code);
            formRequest.append('branch_address',this.branch_address);
            formRequest.append('staff_id', '{{ $staff_id }}');
            formRequest.append('office_id', '{{ $id }}');
            if(this.bank_name === '') {
                toastr.error('please enter bank name');
            } else if(this.iban_number === '') {
                toastr.error('please enter iban number');
            } else if(this.card_number === '') {
                toastr.error('please enter card number');
            } else if(this.branch_name === '') {
                toastr.error('please enter branch name');
            } else if(this.branch_code === '') {
                toastr.error('please enter branch code');
            } else if(this.branch_address === '') {
                toastr.error('please enter branch address');
            } else if(this.payment_request === '') {
                toastr.error('please enter request payment');
            } else {
                this.$http.post(link, formRequest).then(function (response) {
                    console.log('request withdraw response');
                    console.log(response);
                    if (response.body.success === true) {
                        toastr.success(response.body.message);
                        setTimeout(function () {
                            window.location.reload();
                        }, 600);
                    }
                    if (response.body.success === false) {
                        toastr.error(response.body.message);
                    }
                }, function (error) {
                    console.log('request withdraw error');
                    console.log(error);
                })
            }
        },
    },
    watch: {
        startTimeFilter: function (val) {
            this.applyFilter();
        },
        endTimeFilter: function (val) {
            this.applyFilter();
        },
        personsAddedObj: function (val) {
            let self = this;
            this.personsAddedObj.map(function (data) {
                self.totalAmount = parseInt(self.totalAmount) + parseInt(data.month_price);
            });
        },

    },
    mounted() {
        this.getFinanceDetail(new Date(),new Date());
        this.getTodaySharedOfficeRating();
        this.getDetail();
        this.getSharedofficeUsers();
        this.getSharedofficeChat();
        this.initializeSelect2();
    }
});
