var element = $('.floating-chat');
// var myStorage = localStorage;
//
// if (!myStorage.getItem('chatID')) {
//     myStorage.setItem('chatID', createUUID());
// }

setTimeout(function() {
    element.addClass('enter');
}, 1000);

// function onMetaAndEnter(event) {
//     if ((event.metaKey || event.ctrlKey) && event.keyCode == 13) {
//         sendNewMessage();
//     }
// }

function closeElement() {
    element.find('.chat').removeClass('enter').hide();
    element.find('>i').show();
    element.removeClass('expand');
    element.find('.header button').off('click', closeElement);
    // element.find('#sendMessage').off('click', sendNewMessage);
    // element.find('.text-box').off('keydown', onMetaAndEnter).prop("disabled", true).blur();
    setTimeout(function() {
        element.find('.chat').removeClass('enter').show()
        element.click(openElement);
    }, 500);
}

function openElement() {
    // var messages = element.find('.messages');
    // var textInput = element.find('.text-box');
    element.find('>i').hide();
    element.addClass('expand');
    element.find('.chat').addClass('enter');
    // var strLength = textInput.val().length * 2;
    // textInput.keydown(onMetaAndEnter).prop("disabled", false).focus();
    element.off('click', openElement);
    element.find('.header button').click(closeElement);
    // element.find('#sendMessage').click(sendNewMessage);
    // messages.scrollTop(messages.prop("scrollHeight"));
}


element.click(openElement);
