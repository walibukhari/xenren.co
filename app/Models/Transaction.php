<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
	protected $table = 'transaction';

	protected $fillable = [
		'name',
		'amount',
		'milestone_id',
		'due_date',
		'comment',
		'performed_by_balance_before',
		'performed_by_balance_after',
		'performed_to_balance_before',
		'performed_to_balance_after',
		'performed_by',
		'performed_to',
		'balance_before',
		'balance_after',
		'type',
		'currency',
		'release_at',
		'status'
	];

	protected $appends = [
		'CurrencyType',
		'CurrencySymbol',
		'PaymentType',
		'StatusString',
		'ProjectOwner'
	];

	const STATUS_REQUESTED = 4; //Requested by Freelancer
	const STATUS_APPROVED = 2;  //Accepted by Employer
	const STATUS_REJECTED = 3;  //Rejected by Employer
	const STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM = 5;
	const STATUS_WAITING_FOR_ADMIN_APPROVAL = 6;
	const STATUS_IN_PROGRESS_TRANSACTION = 7;
	const STATUS_IN_REJECTED_TRANSACTION = 8;
	const STATUS_APPROVED_TRANSACTION = 9;
	const STATUS_INITIAL_SEAT_SCAN = 16;
	const STATUS_IN_PROGRESS_MILESTONE = 10;
	const STATUS_IN_PROGRESS_DISPUTE = 11;
	const STATUS_DISPUTE_APPROVED = 12;
	const STATUS_DISPUTE_REJECTED = 13;
	const STATUS_DISPUTE_CANCELED_BY_FREELANCER = 14;
    const STATUS_IN_PROGRESS_APPROVED_BY_ADMIN_TRANSACTION = 15;
    const STATUS_SHARED_OFFICE_OWNER_STATUS_WITHDRAW_REQUEST_SEND = 0;
    const STATUS_SHARED_OFFICE_OWNER_STATUS_WITHDRAW_REQUEST_APPROVED = 1;
    const STATUS_SHARED_OFFICE_OWNER_STATUS_WITHDRAW_REQUEST_REJECT = 3;

    const TYPE_PROJECT_PAYMENT = 1;
	const TYPE_MILESTONE_PAYMENT = 2;
	const TYPE_SERVICE_FEE = 3;
	const TYPE_TOP_UP = 4;
	const TYPE_INITIAL_SEAT_SCAN = 11;
	const TYPE_MILESTONE_PAYMENT_REFUND = 5;
	const TYPE_SHARED_OFFICE_ACCOUNT_BALANCE = 6;
	const TYPE_ON_HOLD = 7;
	const TYPE_WITHDRAW = 8;
	const TYPE_BONUS = 9;
    const TYPE_SHARED_OFFICE_OWNER_STATUS_WITHDRAW_REQUEST = 10;
    const TYPE_BOOKING_CHARGES = 12;
    const TYPE_BOOKING_CHARGES_FEE = 13;

    const PAYMENT_TYPE_DEBIT = 'debit';
	const PAYMENT_TYPE_CREDIT = 'credit';

	const TYPE = 'shared_office_booking_request';


    public function getCreatedAT($date)
    {
       $date = Carbon::parse($date)->format('d/m/Y');
        return $date;
    }

	public function disputeAgainstMilestone()
	{
		return $this->hasOne(ProjectDispute::class, 'milestone_id', 'milestone_id');
	}

	public function filedTransactionDispute()
	{
		return $this->hasOne(ProjectDispute::class, 'transaction_id', 'id');
	}

	public function transactionDetail()
	{
		return $this->hasOne(TransactionDetail::class, 'transaction_id', 'id');
	}

	public function getStatusStringAttribute()
	{
		if($this->status == self::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM) {
			return 'Approved - Processing';
		}

		if($this->status == self::STATUS_APPROVED || $this->status == self::STATUS_APPROVED_TRANSACTION) {
			return 'Approved';
		}

		if($this->status == self::STATUS_IN_PROGRESS_TRANSACTION) {
			return 'Processing';
		}

		if($this->status == self::STATUS_IN_REJECTED_TRANSACTION) {
			return 'Rejected';
		}

		if($this->status == self::STATUS_IN_PROGRESS_APPROVED_BY_ADMIN_TRANSACTION) {
			return 'Approved - Processing';
		}

		if($this->status == self::STATUS_REJECTED) {
			return 'Rejected';
		}
	}

	public function getPaymentTypeAttribute()
	{
		if($this->type == self::TYPE_TOP_UP) {
			return self::PAYMENT_TYPE_CREDIT;
		}

		if($this->type == self::TYPE_SERVICE_FEE) {
			return self::PAYMENT_TYPE_DEBIT ;
		}

		if($this->performed_by == \Auth::user()->id) {
			return self::PAYMENT_TYPE_DEBIT;
		}

		return self::PAYMENT_TYPE_CREDIT;
	}
	public function getCurrencyTypeAttribute()
	{
		if($this->currency == User::CURRENCY_CNY) {
			return 'CNY';
		}
		return 'USD';
	}

	public function getCurrencySymbolAttribute()
	{
		if((int)$this->currency == User::CURRENCY_CNY) {
			return '¥';
		}
		return '$';
	}

  public function getProjectOwnerAttribute()
  {
      return isset($this->milestone) && isset($this->milestone->project) && isset($this->milestone->project->creator) ? $this->milestone->project->creator : null;
  }

	public function milestone()
	{
		return $this->hasOne(ProjectMilestones::class, 'id', 'milestone_id');
	}

	public function staff(){
	    return $this->hasOne(Staff::class,'id','performed_by');
    }

	public function receiver()
	{
		return $this->hasOne(User::class, 'id', 'performed_to');
	}

	public function sender()
	{
		return $this->hasOne(User::class, 'id', 'performed_by');
	}

	static function getMyTransactionDetail()
	{
		return (self::where([
			['performed_by', '=', \Auth::user()->id],
			['status' , '!=', self::STATUS_REQUESTED]
		])
			->orWhere([
				['performed_to', '=', \Auth::user()->id],
				['status' , '!=', self::STATUS_REQUESTED]
			])
			->with('milestone.project.creator')
      ->orderBy('id', 'desc')
			->get());
	}

}
