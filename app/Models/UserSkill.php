<?php

namespace App\Models;

use Input;

class UserSkill extends BaseModels {
    const SKILL_UNVERIFIED = 1;
    const SKILL_CLIENT_VERIFIED = 2;
    const SKILL_ADMIN_VERIFIED = 3;

    protected $table = 'user_skill';

    protected $fillable = [
        'skill_id',
        'user_id'
    ];

    protected $appends = [
      'sort_by_skill'
    ];

    protected $dates = [];

    public static function boot() {
        parent::boot();
    }

    public function getSortBySkillAttribute()
    {
        return $this->experience > 0 ? $this->experience : $this->is_client_verified;
    }

    public function scopeGetUserSkill($query) {
        return $query;
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function skill() {
        return $this->belongsTo('App\Models\Skill', 'skill_id', 'id');
    }

    public function explainSkillLevel() {
        return \App\Constants::translateSkillLevel($this->experience);
    }

    public function getSkillVerifyStatus(){
        if( $this->experience >= 1 )
        {
            //admin already verified
            return $this::SKILL_ADMIN_VERIFIED;
        }

        if( $this->experience == 0 )
        {
            $isClientVerify = $this->is_client_verified;
            if( $isClientVerify == 1 )
            {
                //common project complete and project creator give skill verification
                return $this::SKILL_CLIENT_VERIFIED;
            }
            else
            {
                //admin didn't give any verification
                return $this::SKILL_UNVERIFIED;
            }
        }
    }
}