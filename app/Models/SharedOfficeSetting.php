<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SharedOfficeSetting extends Model
{
    const BOOK_STATUS_PAY_LATER = 1;
    const BOOK_STATUS_PAY_NOW = 2;
    const CANCEL_WITH_CHARGES = 3;
    const CANCEL_WITH_OUT_CHARGES = 4;

    protected $fillable = [
        'staff_id','book_type' , 'cancel_type' , 'status','status_book_type' , 'status_cancel_type' , 'discount' ,'allow'
    ];

    public function staff() {
        return $this->hasOne(Staff::class,'id','staff_id');
    }
}
