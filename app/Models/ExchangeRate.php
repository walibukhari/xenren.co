<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExchangeRate extends Model
{
    protected $table = 'exchange_rates';

    protected $fillable = [
        'from_id', 'to_id', 'rate', 'created_by', 'updated_by'
    ];

    public function from() {
        return $this->belongsTo('App\Models\Currency', 'from_id', 'id');
    }

    public function to() {
        return $this->belongsTo('App\Models\Currency', 'to_id', 'id');
    }

    static function getExchangeRate($from, $to)
    {
        $rate = ExchangeRate::where(['from_id' => $from, 'to_id' => $to])->first();
        if (!empty($rate))
        {
            return $rate->rate;
        }

        $inverse_rate = ExchangeRate::where(['from_id' => $to, 'to_id' => $from])->first();
        if (!empty($inverse_rate))
        {
            return $inverse_rate->inverse_rate;
        }

        if (empty($rate) && empty($rate))
        {
            return 1;
        }
    }
}
