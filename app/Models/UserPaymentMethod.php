<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPaymentMethod extends Model
{
    protected $table = 'user_payment_methods';
    
    protected $fillable = [
	    'user_id',
	    'payment_method',
	    'name',
	    'email',
	    'phone',
	    'status',
    ];
    
    CONST STATUS_ACTIVE = 0;
    CONST STATUS_INACTIVE = 1;
}
