<?php

namespace App\Models;

use Input;
use App\Models\SkillKeywordSubmit;

class SkillKeyword extends BaseModels {

    protected $table = 'skill_keywords';

    protected $fillable = [
        'user_id', 'name_cn', 'name_en', 'reference_link'
    ];

    protected $dates = [];

    public static function boot() {
        parent::boot();
    }

    public function scopeGetSkillKeyword($query) {
        return $query;
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function scopeGetFilteredResults($query) {
        if (Input::has('filter_id') && Input::get('filter_id') != '') {
            $query->where('id', '=', Input::get('filter_id'));
        }

        if (Input::has('filter_user') && Input::get('filter_user') != '') {
            $user = User::where('name',  Input::get('filter_user'))->first();
            if( $user != null ){
                $query->where('user_id', $user->id );
            }else{
                $query->where('user_id', 0 );
            }
        }

        if (Input::has('filter_name_en') && Input::get('filter_name_en') != '') {
            $query->where('name_en', 'like', '%' . Input::get('filter_name_en') . '%');
        }

        if (Input::has('filter_name_cn') && Input::get('filter_name_cn') != '') {
            $query->where('name_cn', 'like', '%' . Input::get('filter_name_cn') . '%');
        }

        if (Input::has('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::has('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::has('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::has('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }
    }

    public function getSubmitCount(){
        $targetWord = "";
        if( $this->name_cn != "" ){
            $targetWord = $this->name_cn;
        }

        if( $this->name_en != "" ){
            $targetWord = $this->name_en;
        }

        $skillKeywordSubmit =  SkillKeywordSubmit::where('name', $targetWord)->first();

        $count = 0;
        if($skillKeywordSubmit)
        {
            $count = $skillKeywordSubmit->submit_count;
        }
        return $count;
    }
}