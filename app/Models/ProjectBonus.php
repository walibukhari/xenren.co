<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectBonus extends Model
{
    protected $table = 'project_bonuses';

    protected $fillable = [
        'project_id',
        'amount',
        'reason',
        'description',
        'private_note',
    ];
}
