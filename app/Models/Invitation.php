<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Staff;

class Invitation extends BaseModels
{

    protected $table = 'invitations';

    protected $fillable = ['invitor_id','invitee_id'];

    public function invitor() {
        return $this->belongsTo('App\Models\User', 'invitor_id', 'id');
    }

    public function invitee() {
        return $this->belongsTo('App\Models\User', 'invitee_id', 'id');
    }
}
