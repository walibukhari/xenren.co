<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorldStates extends Model
{
    protected $table = 'world_states';
    
    protected $fillable = [
    	'name',
	    'country_id'
    ];
}
