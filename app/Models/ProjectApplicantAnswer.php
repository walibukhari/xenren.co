<?php

namespace App\Models;

class ProjectApplicantAnswer extends BaseModels {

    protected $table = 'project_applicant_answers';

    protected $fillable = [
        'project_applicant_id', 'project_question_id', 'answer'
    ];

    protected $dates = [];

    public static function boot() {
        parent::boot();
    }

    public function scopeGetProjectApplicant($query) {
        return $query;
    }

    public function projectApplicant() {
        return $this->hasOne('App\Models\ProjectApplicant', 'project_applicant_id', 'id');
    }

    public function projectQuestion() {
        return $this->belongsTo('App\Models\ProjectQuestion', 'project_question_id', 'id');
    }
}