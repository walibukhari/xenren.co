<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SharedOfficeProductCategory extends Model
{
    public function categories()
    {
        return $this->hasMany('App\Models\SharedOfficeProducts');
    }
}
