<?php

namespace App\Models;

use App\Constants;
use Carbon;
use DB;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Laravel\Cashier\Billable;
use Tymon\JWTAuth\Contracts\JWTSubject;

//use Webpatser\Countries\Countries;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use Billable;

    // J W T token

    const CONTACT_INFO_NOT_ALLOW_TO_VIEW = 0;
    const CONTACT_INFO_ALLOW_TO_VIEW = 1;

    const CURRENCY_USD = 1;
    const CURRENCY_CNY = 2;

    const HOURLY_RANGE_1_10 = 1;
    const HOURLY_RANGE_11_20 = 2;
    const HOURLY_RANGE_21_30 = 3;
    const HOURLY_RANGE_31_40 = 4;
    const HOURLY_RANGE_41_50 = 5;
    const HOURLY_RANGE_EQUAL_OR_MORE_THAN_51 = 6;

    const LANGUAGE_PREFERENCE_CHINESE = 1;
    const LANGUAGE_PREFERENCE_ENGLISH = 2;

    const STATUS_LOOK_FOR_PROJECTS = 1;
    const STATUS_LOOK_FOR_COFOUNDER = 2;
    const STATUS_LOOK_FOR_FULL_TIME_JOB = 3;
    // const STATUS_LOOK_FOR_PART_TIME_JOB = 3;
    const STATUS_ANY_STATUS = 4;

    const IS_VALIDATED_PENDING = 0;
    const IS_VALIDATED_APPROVED = 1;
    const IS_VALIDATED_REJECTED = 2;

    const GENDER_MALE = 0;
    const GENDER_FEMALE = 1;

    const USER_UI_PREFERENCE_BOTH = 0;
    const USER_EMAIL_NOTIFICATION = 1;
    const USER_WECHAT_NOTIFICATION = 1;
    const USER_UI_PREFERENCE_FREELANCER = 1;
    const USER_UI_PREFERENCE_EMPLOYER = 2;
    const CHECKED_IN_CO_WORK_SPACE = 1;
    const APP_PUSH_NOTIFICATION = 1;
    const CLOSE_MY_ACCOUNT = 0;

    protected $myPublishOrderCount;
    protected $myReceivedOrderCount;
    protected $myProgressiveOrderCount;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'userscol',
        'id_card_no',
        'gender',
        'date_of_birth',
        'address',
        'img_avatar',
        'handphone_no',
        'id_image_front',
        'id_image_back',
        'is_validated',
        'device_token',
        'device_os',
        'name',
        'user_link',
        'facebook_id',
        'face_image',
        'hint',
        'title',
        'about_me',
        'experience',
        'is_contact_allow_view',
        'line_id',
        'free_notifications',
        'web_token',
        'post_project_notify',
        'moveer_address',
        'ip_country_code',
        'account_balance'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'profile_qr_code',
        'total_amount_spent',
        'total_time_spent',
        'total_rating',
        'user_image',
        'is_favorite',
        'last_scanned_office',
        'country_name',
        'country_flag',
        'compressed_image',
    ];

    // J W T token
    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function sharedOfficeSeat()
    {
        return $this->hasOne(SharedOfficeFinance::class, 'user_id', 'id');
    }

    /**
     * TODO:
     * sending empty invoice to prevent crash, test it, this function must be called from stripe
     * library and shouldn't be over-rided.
     */
    public function invoices()
    {
        return collect();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public static function getCurrencyFullName($id)
    {
        if (0 != $id) {
            return WorldCountries::where('id', '=', $id)->first();
        }
    }

    public function getCompressedImageAttribute()
    {
        $url = \URL::to('/');
        $image = str_replace($url, '', $this->img_avatar);
        if (file_exists(public_path().$image)) {
            $a = is_null($image) ? asset('/images/image.png') : asset('/compresses/medium'.str_replace('uploads/', '', $image));
            $noFound = asset('/compresses/medium/images/image.png');
            if ($a == $noFound) {
                return asset('/images/image.png');
            }

            return $a;
        }
    }

    public function getCountryFlagAttribute()
    {
        try {
            $city = WorldCities::with('country')->where('id', '=', $this->city_id)->first();

            return strtoupper($city->country->code);
        } catch (\Exception $e) {
            return '';
        }
    }

    public function userSetting()
    {
        return $this->hasOne(PushNotificationSettings::class, 'user_id', 'id');
    }

    public function getCountryNameAttribute()
    {
        try {
            $country = WorldCountries::where('id', '=', $this->country_id)->first();

            return $country->name;
        } catch (\Exception $e) {
            return 'n/a';
        }
    }

    public function getLastScannedOfficeAttribute()
    {
        return SharedOfficeFinance::with('office')->whereHas('office')->where('user_id', '=', $this->id)->first();
    }

    public function getIsFavoriteAttribute()
    {
        try {
            if (\Auth::user()) {
                $exist = FavouriteFreelancer::where('user_id', '=', \Auth::user()->id)->where('freelancer_id', '=', $this->id)->first();

                return is_null($exist) ? null : true;
            }

            return null;
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function getUserImageAttribute()
    {
        if ('' == $this->img_avatar || null == $this->img_avatar) {
            return asset('images/avatar_xenren.png');
        }
        $fbString = 'https://graph.facebook.com/';
        if (false !== strpos($this->img_avatar, $fbString)) {
            return $this->img_avatar;
        }

        return asset($this->img_avatar);
    }

    public function getImgAvatarAttribute($val)
    {
        if ('' == $val || null == $val) {
            return asset('images/image.png');
        }
        $fbString = 'https://graph.facebook.com/';
        if (false !== strpos($val, $fbString)) {
            return str_replace(asset('/'), '', $val);
        }
        if (false !== strpos($val, 'wx.qlogo.cn')) {
            return str_replace(asset('/'), '', $val);
        }
        $search = asset('/').asset('/');
        $v = str_replace($search, '', $val);

        return asset($v);
    }

    public function getTotalRatingAttribute()
    {
        $total = 0;
        $average = 0;

        return Review::where('reviewee_user_id', $this->id)->avg('total_score');
    }

    public function getTotalAmountSpentAttribute()
    {
        try {
            $amount = SharedOfficeFinance::where('user_id', '=', \Auth::user()->id)->sum('cost');

            return isset($amount) ? $amount : 0;
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function getTotalTimeSpentAttribute()
    {
        try {
            $data = SharedOfficeFinance::where('user_id', '=', \Auth::user()->id)->get();
            $hours = 0;
            foreach ($data as $v) {
                $d1 = isset($v->start_time) ? \Carbon\Carbon::parse($v->start_time) : null;
                $d2 = isset($v->end_time) ? \Carbon\Carbon::parse($v->end_time) : null;
                if (isset($d1, $d2)) {
                    $h = $d2->diffInHours($d1);
                    $hours += $h;
                }
            }

            return $hours;
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function xgetHandphoneNoAttribute($val)
    {
        try {
            if (Auth::user() && $this->id == Auth::user()->id) {
                return $val;
            }
            if (Auth::user()) {
                $blocked = BlockedUser::where('from', '=', $this->id)->where('to', '=', \Auth::user()->id)->first();
                if (!is_null($blocked)) {
                    if (1 == $blocked->phone) {
                        return $val;
                    }

                    return null;
                }
                $privacy = UserContactPrivacy::where('user_id', '=', $this->id)->first();
                if (is_null($privacy)) {
                    return $val;
                }
                if (1 == $privacy->phone) {
                    return $val;
                }

                return null;
            }
            $privacy = UserContactPrivacy::where('user_id', '=', $this->id)->first();
            if (is_null($privacy)) {
                return $val;
            }
            if (1 == $privacy->phone) {
                return $val;
            }

            return null;
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getHandphoneNoAttribute($val)
    {
        if (Auth::user() && Auth::user()->id == $this->id) {
            return $val;
        }
        if (Auth::user()) {
            $phone = UserContactPrivacy::where('user_id', '=', $this->id)->first();
            if (is_null($phone)) {
                return null;
            }
            if (0 == $phone->phone) {
                return null;
            }

            $data = ProjectApplicant::where(function ($q) {
                $q->where('user_id', Auth::user()->id)->orwhere('user_id', '=', $this->id);
            })
                ->where(function ($q) {
                    $q->where('status', '=', ProjectApplicant::STATUS_APPLICANT_ACCEPTED)
                        ->orWhere('status', '=', ProjectApplicant::STATUS_CREATOR_SELECTED)
                    ;
                })
                ->whereHas('project.creator', function ($q) {
                    $q->where('user_id', '=', Auth::user()->id)->orwhere('user_id', '=', $this->id);
                })
                ->first()
            ;
            if (is_null($data)) {
                if (1 == $phone->phone) {
                    return $val;
                }

                return null;
            }

            return $val;
        }
        $phone = UserContactPrivacy::where('user_id', '=', $this->id)->first();
        if (is_null($phone)) {
            return null;
        }
        if (0 == $phone->phone) {
            return null;
        }

        return $val;
    }

    public function getLineIdAttribute($val)
    {
        if (Auth::user() && Auth::user()->id == $this->id) {
            return $val;
        }

        if (Auth::user()) {
            $line = UserContactPrivacy::where('user_id', '=', $this->id)->first();
            if (is_null($line)) {
                return null;
            }
            if (0 == $line->line) {
                return null;
            }

            $data = ProjectApplicant::where(function ($q) {
                $q->where('user_id', Auth::user()->id)->orwhere('user_id', '=', $this->id);
            })
                ->where(function ($q) {
                    $q->where('status', '=', ProjectApplicant::STATUS_APPLICANT_ACCEPTED)
                        ->orWhere('status', '=', ProjectApplicant::STATUS_CREATOR_SELECTED)
                    ;
                })
                ->whereHas('project.creator', function ($q) {
                    $q->where('user_id', '=', Auth::user()->id)->orwhere('user_id', '=', $this->id);
                })
                ->first()
            ;
            if (is_null($data)) {
                if (1 == $line->line) {
                    return $val;
                }

                return null;
            }

            return $val;
        }
        $line = UserContactPrivacy::where('user_id', '=', $this->id)->first();
        if (is_null($line)) {
            return null;
        }
        if (0 == $line->line) {
            return null;
        }

        return $val;
    }

    public function getSkypeIdAttribute($val)
    {
        if (Auth::user() && Auth::user()->id == $this->id) {
            return $val;
        }
        if (Auth::user()) {
            $skype = UserContactPrivacy::where('user_id', '=', $this->id)->first();
            if (is_null($skype)) {
                return null;
            }

            if (0 == $skype->skype) {
                return null;
            }

            $data = ProjectApplicant::where(function ($q) {
                $q->where('user_id', Auth::user()->id)->orwhere('user_id', '=', $this->id);
            })
                ->where(function ($q) {
                    $q->where('status', '=', ProjectApplicant::STATUS_APPLICANT_ACCEPTED)
                        ->orWhere('status', '=', ProjectApplicant::STATUS_CREATOR_SELECTED)
                        ;
                })
                ->whereHas('project.creator', function ($q) {
                    $q->where('user_id', '=', Auth::user()->id)->orwhere('user_id', '=', $this->id);
                })
                ->first()
                ;
            if (is_null($data)) {
                if (1 == $skype->skype) {
                    return $val;
                }

                return null;
            }

            return $val;
        }
        $skype = UserContactPrivacy::where('user_id', '=', $this->id)->first();
        if (is_null($skype)) {
            return null;
        }
        if (0 == $skype->skype) {
            return null;
        }

        return $val;
    }

    public function getQqIdAttribute($val)
    {
        if (Auth::user() && Auth::user()->id == $this->id) {
            return $val;
        }
        if (Auth::user()) {
            $data = ProjectApplicant::where(function ($q) {
                $q->where('user_id', Auth::user()->id)->orwhere('user_id', '=', $this->id);
            })
                ->where(function ($q) {
                    $q->where('status', '=', ProjectApplicant::STATUS_APPLICANT_ACCEPTED)
                        ->orWhere('status', '=', ProjectApplicant::STATUS_CREATOR_SELECTED)
                        ;
                })
                ->whereHas('project.creator', function ($q) {
                    $q->where('user_id', '=', Auth::user()->id)->orwhere('user_id', '=', $this->id);
                })
                ->first()
                ;
            if (is_null($data)) {
                return null;
            }

            return $val;
        }

        return null;
    }

    public function getWechatIdAttribute($val)
    {
        if (Auth::user() && Auth::user()->id == $this->id) {
            return $val;
        }
        if (Auth::user()) {
            $wechat = UserContactPrivacy::where('user_id', '=', $this->id)->first();
            if (is_null($wechat)) {
                return null;
            }

            if (0 == $wechat->weChat) {
                return null;
            }

            $data = ProjectApplicant::where(function ($q) {
                $q->where('user_id', Auth::user()->id)->orwhere('user_id', '=', $this->id);
            })
                ->where(function ($q) {
                    $q->where('status', '=', ProjectApplicant::STATUS_APPLICANT_ACCEPTED)
                        ->orWhere('status', '=', ProjectApplicant::STATUS_CREATOR_SELECTED)
                        ;
                })
                ->whereHas('project.creator', function ($q) {
                    $q->where('user_id', '=', Auth::user()->id)->orwhere('user_id', '=', $this->id);
                })
                ->first()
                ;
            if (is_null($data)) {
                if (1 == $wechat->weChat) {
                    return $val;
                }

                return null;
            }

            return $val;
        }
        $wechat = UserContactPrivacy::where('user_id', '=', $this->id)->first();
        if (is_null($wechat)) {
            return null;
        }
        if (0 == $wechat->weChat) {
            return null;
        }

        return $val;
    }

    public function xgetSkypeIdAttribute($val)
    {
        try {
            if (Auth::user() && $this->id == Auth::user()->id) {
                return $val;
            }
            if (Auth::user()) {
                $blocked = BlockedUser::where('from', '=', $this->id)->where('to', '=', \Auth::user()->id)->first();
                if (!is_null($blocked)) {
                    if (1 == $blocked->skype) {
                        return $val;
                    }

                    return null;
                }
                $privacy = UserContactPrivacy::where('user_id', '=', $this->id)->first();
                if (is_null($privacy)) {
                    return $val;
                }
                if (1 == $privacy->skype) {
                    return $val;
                }

                return null;
            }
            $privacy = UserContactPrivacy::where('user_id', '=', $this->id)->first();
            if (is_null($privacy)) {
                return $val;
            }
            if (1 == $privacy->skype_id) {
                return $val;
            }

            return null;
        } catch (\Exception $e) {
            return null;
        }
    }

    public function xgetWechatIdAttribute($val)
    {
        try {
            if (Auth::user() && $this->id == Auth::user()->id) {
                return $val;
            }
            if (Auth::user()) {
                $blocked = BlockedUser::where('from', '=', $this->id)->where('to', '=', \Auth::user()->id)->first();
                if (!is_null($blocked)) {
                    if (1 == $blocked->weChat) {
                        return $val;
                    }

                    return null;
                }
                $privacy = UserContactPrivacy::where('user_id', '=', $this->id)->first();
                if (is_null($privacy)) {
                    return $val;
                }
                if (1 == $privacy->weChat) {
                    return $val;
                }

                return null;
            }
            $privacy = UserContactPrivacy::where('user_id', '=', $this->id)->first();
            if (is_null($privacy)) {
                return $val;
            }
            if (1 == $privacy->wechat) {
                return $val;
            }

            return null;
        } catch (\Exception $e) {
            return null;
        }
    }

    public function xgetQqIdAttribute($val)
    {
        try {
            if (Auth::user() && $this->id == Auth::user()->id) {
                return $val;
            }
            if (Auth::user()) {
                $blocked = BlockedUser::where('from', '=', $this->id)->where('to', '=', \Auth::user()->id)->first();
                if (!is_null($blocked)) {
                    if (1 == $blocked->qq) {
                        return $val;
                    }

                    return null;
                }
                $privacy = UserContactPrivacy::where('user_id', '=', $this->id)->first();
                if (is_null($privacy)) {
                    return $val;
                }
                if (1 == $privacy->qq) {
                    return $val;
                }

                return null;
            }
            $privacy = UserContactPrivacy::where('user_id', '=', $this->id)->first();
            if (is_null($privacy)) {
                return $val;
            }
            if (1 == $privacy->qq) {
                return $val;
            }

            return null;
        } catch (\Exception $e) {
            return $e->getMessage().' at '.$e->getFile().' on '.$e->getLine();
        }
    }

    public function sharedOfficeDetail()
    {
        return $this->hasMany(SharedOfficeFinance::class, 'user_id', 'id');
    }

    public function bankAccount()
    {
        return $this->hasMany('App\Models\UserBank', 'user_id', 'id');
    }

    public function Receivecoins()
    {
        return $this->hasMany('App\Models\UserCoins', 'receiver_id', 'id');
    }

    public function Sendcoins()
    {
        return $this->hasMany('App\Models\UserCoins', 'sender_id', 'id');
    }

    public function skills()
    {
        return $this->hasMany('App\Models\UserSkill', 'user_id', 'id')->orderBy('id', 'ASC');
    }

    public function languages()
    {
        return $this->hasMany('App\Models\UserLanguage', 'user_id', 'id')->orderBy('id', 'ASC');
    }

    public function projects()
    {
        return $this->hasMany('App\Models\Project', 'user_id', 'id')->orderBy('id', 'ASC');
    }

    public function CompletedProjects()
    {
        return $this->hasMany('App\Models\Project', 'user_id', 'id')->orderBy('id', 'ASC')->where('status', '=', Project::STATUS_COMPLETED);
    }

    public function job_position()
    {
        return $this->hasOne('App\Models\JobPosition', 'id', 'job_position_id');
    }

    public function commentSkills()
    {
        return $this->hasMany('App\Models\UserSkillCommentSkills', 'user_id', 'id');
    }

    public function comment()
    {
        return $this->hasMany('App\Models\UserSkillComment', 'user_id', 'id');
    }

//    public function country()
//    {
//        return $this->hasOne(Countries::class, 'id', 'country_id');
//    }

    public function country()
    {
        return $this->hasOne(WorldCountries::class, 'id', 'country_id');
    }

//    public function city()
//    {
//        return $this->hasOne(CountryCities::class, 'id', 'city_id');
//    }

    public function city()
    {
        return $this->hasOne(WorldCities::class, 'id', 'city_id');
    }

    public function status()
    {
        return $this->hasMany('App\Models\UserStatus', 'user_id', 'id');
    }

    public function getProfileQrCodeAttribute()
    {
        return 'https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=/en/resume/getDetails/'.$this->user_link;
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    // public function setEmailAttribute($value)
    // {
    //     $this->attributes['email'] = trim(strtolower($value));
    // }

    public function scopeGetUsers($query)
    {
        return $query;
    }

    public function scopeGetFilteredResults($query)
    {
        if (Input::has('filter_id') && '' != Input::get('filter_id')) {
            $query->where('id', '=', Input::get('filter_id'));
        }

        if (Input::has('filter_email') && '' != Input::get('filter_email')) {
            $query->where('email', 'like', '%'.Input::get('filter_email').'%');
        }

        if (Input::has('filter_nick_name') && '' != Input::get('filter_nick_name')) {
            $query->where('name', 'like', '%'.Input::get('filter_nick_name').'%');
        }

        if (Input::has('filter_real_name') && '' != Input::get('filter_real_name')) {
            $query->where('name', 'like', '%'.Input::get('filter_real_name').'%');
        }

        if (Input::has('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::has('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }
    }

    public function explainStatus()
    {
        $userStatusList = UserStatus::where('user_id', $this->id)->get();
        $result = '';
        foreach ($userStatusList as $key => $userStatus) {
            if ($key >= 1) {
                $result .= ' | ';
            }

            $result .= User::translateStatus($userStatus->status_id);
        }

        if (0 == count($userStatusList)) {
            $result = ' - ';
        }

        return $result;
    }

    public function getStatusIds()
    {
        $userStatusList = UserStatus::where('user_id', $this->id)->get();

        $result = '';
        foreach ($userStatusList as $key => $userStatus) {
            if ($key >= 1) {
                $result .= ',';
            }

            $result .= $userStatus->status_id;
        }

        if (0 == count($userStatusList)) {
            $result = '';
        }

        return $result;
    }

    public function isStatusSelected($statusId)
    {
        $count = UserStatus::where('user_id', $this->id)
            ->where('status_id', $statusId)
            ->count()
        ;
        $result = false;
        if (1 == $count) {
            $result = true;
        } else {
            $result = false;
        }

        return $result;
    }

    //	public function getRating() {
    //		$users = DB::table('order')
    //		            ->select(DB::raw('avg(order_applicants.rate_quality + order_applicants.rate_communicate + order_applicants.rate_speed) as rating'))
    //					->join('order_applicants', function($join)
    //		        	{
    //		            	$join->on('order.id', '=', 'order_applicants.order_id')
    //				                 ->where('order_applicants.user_id', '=', $this->id);
    //			        })
    //		            ->where('order_applicants.status', '=', 1)
    //		            ->first();
    //		return $users->rating;
    //	}

    //	public function getLevel() {
    //		$users = DB::table('order')
    //		            ->select(DB::raw('sum(order_applicants.rate_quality + order_applicants.rate_communicate + order_applicants.rate_speed) as point'))
    //					->join('order_applicants', function($join)
    //		        	{
    //		            	$join->on('order.id', '=', 'order_applicants.order_id')
    //				                 ->where('order_applicants.user_id', '=', $this->id);
    //			        })
    //		            ->where('order_applicants.status', '=', 1)
    //		            ->first();
//
    //		$level = DB::table('level')
    //		            ->select('badge')
    //		            ->where('level', '>=', (int) $users->point)
    //					->orderBy('level')
    //					->limit(1)
    //		            ->first();
    //		return $level->badge;
    //	}

    public function getMyPublishOrderCount()
    {
//        if ($this->myPublishOrderCount == null) {
//            $this->myPublishOrderCount = Project::where('user_id', '=', $this->id)
//                ->where('is_read', 0)
//                ->count();
//        }
//
//        return $this->myPublishOrderCount;
        return Project::with([
            'applicants',
            'awardedApplicant',
            'projectChatRoom.chatFollower',
            'milestones.milestoneStatus',
            'userInbox',
        ])
            ->where('user_id', '=', \Auth::user()->id)
            ->where('type', '=', Project::PROJECT_TYPE_COMMON)
            ->orderBy('id', 'desc')->count();
    }

    public function getMyReceivedOrderCount()
    {
//        if ($this->myReceivedOrderCount == null) {
//            $this->myReceivedOrderCount = ProjectApplicant::where('user_id', '=', $this->id)
//                ->where('is_read', ProjectApplicant::IS_READ_NO)
//                ->count();
//        }
//
//        return $this->myReceivedOrderCount;
        $progressingProjects = ProjectApplicant::with([
            'project', 'user',
        ])
            ->where('user_id', '=', \Auth::user()->id)
            ->where(function ($q) {
                $q->where('status', '=', ProjectApplicant::STATUS_CREATOR_SELECTED)
                    ->orWhere('status', '=', ProjectApplicant::STATUS_APPLICANT_ACCEPTED)
                ;
            })
            ->orderBy('id', 'desc')->count();

        return $progressingProjects;
    }

    public function getMyProgressiveOrderCount()
    {
        if (null == $this->myProgressiveOrderCount) {
            $this->myProgressiveOrderCount = Project::where('user_id', '=', $this->id)
                ->where('is_read', 1)
                ->where('status', '!=', Project::STATUS_COMPLETED)
                ->with('awardedApplicant')
                ->whereHas('awardedApplicant', function ($q) {
                    $q->where('status', ProjectApplicant::STATUS_CREATOR_SELECTED);
                })
                ->count()
            ;
        }

        return $this->myProgressiveOrderCount;
    }

    public function getCityWithCountry()
    {
        try {
            $city = WorldCities::with('country')->where('id', '=', \Auth::user()->city_id)->first();

            return $city->name.', '.$city->country->name;
        } catch (\Exception $e) {
            return '';
        }
    }

    public function getCountryCode()
    {
        try {
            $city = WorldCities::with('country')->where('id', '=', \Auth::user()->city_id)->first();

            return $city->country->callingcode;
        } catch (\Exception $e) {
            return '';
        }
    }

    public function sharedofficeCityWithCountry()
    {
        try {
            $city = WorldCities::with('country')->where('id', '=', self::getLastScannedOfficeAttribute()->office->city_id)->first();

            return $city->name.', '.$city->country->name;
        } catch (\Exception $e) {
            return '';
        }
    }

    public function sharedofficeRating()
    {
        try {
            return SharedOfficeRating::where('office_id', '=', self::getLastScannedOfficeAttribute()->office->id)->avg('rate');
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function setImgAvatarAttribute($file)
    {
        try {
            if ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
                $img = \Image::make($file);
                $mime = $img->mime();
                $ext = convertMimeToExt($mime);

                $path = 'uploads/user/'.\Auth::guard('users')->user()->id.'/avatar/';
                $filename = generateRandomUniqueName();

                touchFolder($path);

                $img = $img->save($path.$filename.$ext);

                $this->attributes['img_avatar'] = $path.$filename.$ext;
            } else {
                if (filter_var($file, FILTER_VALIDATE_URL)) {
                    //is url type
                    $this->attributes['img_avatar'] = $file;
                } else {
                    $this->attributes['img_avatar'] = $file;
                }
            }
        } catch (\Exception $e) {
        }
    }

    public function getAvatar()
    {
        if ('' == $this->img_avatar || null == $this->img_avatar) {
            return asset('images/avatar_xenren.png');
        }

        return asset($this->img_avatar);
    }

    public function myInbox()
    {
        return $this->hasMany('App\Models\UserInbox', 'to_user_id', 'id')->orderBy('id', 'DESC');
    }

    public function getProjectApplicant($projectId)
    {
        return DB::table('project_applicants')
            ->select('project_applicants.*')
            ->join('project_chat_followers', 'project_applicants.user_id', '=', 'project_chat_followers.user_id')
            ->join('project_chat_rooms', 'project_applicants.project_id', '=', 'project_chat_rooms.project_id')
            ->where('project_applicants.user_id', $this->id)
            ->where('project_applicants.project_id', $projectId)
            ->first()
        ;
    }

    public function getUserSkillList()
    {
        $userSkills = UserSkill::GetUserSkill()->where('user_id', $this->id)->limit(10)->get();
        $list = '';
        foreach ($userSkills as $key => $userSkill) {
            if ('' == $list) {
                $list = $userSkill->skill_id;
            } else {
                $list = $list.','.$userSkill->skill_id;
            }
        }

        return $list;
    }

    public function getUserSkill()
    {
        return UserSkill::GetUserSkill()->where('user_id', $this->id)->get();
    }

    public function getUserLanguageList()
    {
        $userLangs = UserLanguage::where('user_id', $this->id)->get();
        $list = '';
        foreach ($userLangs as $key => $lang) {
            if ('' == $list) {
                $list = $lang->language_id;
            } else {
                $list = $list.','.$lang->language_id;
            }
        }

        return $list;
    }

    public function getResumeQRCode()
    {
        return 'https://api.qrserver.com/v1/create-qr-code/?size=100x100&data='.route('frontend.resume.getdetails', [$this->id, \Session::get('lang')]);
    }

    public function getAgeAttribute()
    {
        //if age equal to current year (due to user not fill the birth of date) make it 0.
        $result = 0;
        $curentYear = Carbon::now()->year;
        if (isset($this->attributes['date_of_birth'], $this->attributes['date_of_birth']->age)) {
            if (Carbon::parse($this->attributes['date_of_birth'])->age != $curentYear) {
                $result = Carbon::parse($this->attributes['date_of_birth'])->age;
            }

            return $result;
        }

        return '';
    }

    public function getCurrencySignAttribute()
    {
        if (null != $this->currency && 1 == $this->currency) {
            return 'USD';
        }
        if (null != $this->currency && 2 == $this->currency) {
            return 'CNY';
        }

        return 'USD';
    }

    public function getExperience()
    {
        if (!isset($this->experience)) {
            return 0;
        }

        return $this->experience;
    }

    public function getLastLogin()
    {
        try {
            $datetime = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->last_login_at);

            return $datetime->format('d-m-Y h:i A');
        } catch (\Exception $e) {
            return 'N/A';
        }
    }

    public function getJobPosition()
    {
        try {
            $result = '';
            if ($this->job_position_id > 0) {
                $jobPosition = JobPosition::find($this->job_position_id);
                $result = is_null($jobPosition) ? 'N/A' : $jobPosition->getName();
            }

            return $result;
        } catch (\Exception $ed) {
            return 'N/A';
        }
    }

    public function getName()
    {
        //if user still not verify, use nick_name
        //if user already identity verification, he or she need use the real name
        //all user need use real name (same with IC and verify by admin) if possible

        if ('' != $this->name) {
//            if ($this->getLatestIdentityStatus() == UserIdentity::STATUS_APPROVED) {
//                $result = $this->real_name;
//            } else {
            $result = $this->name;
//            }
        } elseif ('' != $this->name) {
//            if ($this->getLatestIdentityStatus() == UserIdentity::STATUS_APPROVED) {
//                $result = $this->real_name;
//            } else {
            $result = $this->name;
//            }
        } else {
//            if ($this->nick_name != "") {
//                $result = $this->nick_name;
//            } else {
            $result = $this->name;
//            }
        }

        return $result;
    }

    public function getLatestIdentityStatus()
    {
        $result = 0;
        $identities = $this->identity()->get();
        foreach ($identities as $identity) {
            $result = $identity->status;
        }

        return $result;
    }

    public function identity()
    {
        return $this->hasMany('App\Models\UserIdentity', 'user_id', 'id');
    }

    public function UserIdentity()
    {
        return $this->hasOne('App\Models\UserIdentity', 'user_id', 'id');
    }

    public function getAboutMe()
    {
        //user didn't input description, then default value "User no yet fill"
        if ('' == $this->about_me) {
            $result = trans('common.user_no_yet_fill');
        } else {
            $result = $this->about_me;
        }

        return $result;
    }

    public function getReviewInfo($user_id = false)
    {
        $id = \Auth::user()->id;
        $total = 0;
        $average = Review::where('reviewee_user_id', $id)->avg('total_score');
        $reviews = Review::where('reviewee_user_id', $id)->get();
        $totalCountReview = isset($reviews) && !is_null($reviews) ? count($reviews) : 0;
        $average = isset($average) && $average > 0 ? $average : 0;

        foreach ($reviews as $review) {
            $total += $review->total_score;
        }
        $average = number_format($average, 2, '.', ',');
//        if ($totalCountReview != 0) {
//            $average = number_format($total / $totalCountReview, 2, '.', '');
//        }

        $totalCount1Star = $this->getTotalCountRate(1);
        $totalCount2Star = $this->getTotalCountRate(2);
        $totalCount3Star = $this->getTotalCountRate(3);
        $totalCount4Star = $this->getTotalCountRate(4);
        $totalCount5Star = $this->getTotalCountRate(5);

        if (0 != $totalCount1Star && 0 != $totalCountReview) {
            $pct1Star = number_format($totalCount1Star / $totalCountReview * 20, 2, '.', '');
        } else {
            $pct1Star = 0;
        }

        if (0 != $totalCount2Star && 0 != $totalCountReview) {
            $pct2Star = number_format($totalCount2Star / $totalCountReview * 40, 2, '.', '');
        } else {
            $pct2Star = 0;
        }

        if (0 != $totalCount3Star && 0 != $totalCountReview) {
            $pct3Star = number_format($totalCount3Star / $totalCountReview * 50, 2, '.', '');
        } else {
            $pct3Star = 0;
        }

        if (0 != $totalCount4Star && 0 != $totalCountReview) {
            $pct4Star = number_format($totalCount4Star / $totalCountReview * 70, 2, '.', '');
        } else {
            $pct4Star = 0;
        }

        if (0 != $totalCount5Star && 0 != $totalCountReview) {
            $pct5Star = number_format($totalCount5Star / $totalCountReview * 80, 2, '.', '');
        } else {
            $pct5Star = 0;
        }

        $result = '';
        $rate = round($average);
        $fullStar = $rate;
        $emptyStar = 5 - $rate;
        for ($i = 0; $i < $fullStar; ++$i) {
            $result .= '<i class="fa fa-star" style="padding-left:2px;padding-right:2px"> </i>';
        }
        for ($i = 0; $i < $emptyStar; ++$i) {
            $result .= '<i class="fa fa-star-o" style="padding-left:2px;padding-right:2px"> </i>';
        }
        $ratingStar = $result;
        return [
            'averageRating' => $average,
            'totalCountReview' => $totalCountReview,
            'ratingStar' => $ratingStar,
            'totalCount1Star' => $totalCount1Star,
            'totalCount2Star' => $totalCount2Star,
            'totalCount3Star' => $totalCount3Star,
            'totalCount4Star' => $totalCount4Star,
            'totalCount5Star' => $totalCount5Star,
            'pct1Star' => $pct1Star,
            'pct2Star' => $pct2Star,
            'pct3Star' => $pct3Star,
            'pct4Star' => $pct4Star,
            'pct5Star' => $pct5Star,
        ];
    }

    public function getReviewInfoForAPI($user_id = false)
    {
        $id = \Auth::user()->id;
        $total = 0;
        $average = Review::where('reviewee_user_id', $id)->avg('total_score');
        $reviews = Review::where('reviewee_user_id', $id)->get();
        $totalCountReview = isset($reviews) && !is_null($reviews) ? count($reviews) : 0;
        $average = isset($average) && $average > 0 ? $average : 0;

        foreach ($reviews as $review) {
            $total += $review->total_score;
        }
        $average = number_format($average, 2, '.', ',');
//        if ($totalCountReview != 0) {
//            $average = number_format($total / $totalCountReview, 2, '.', '');
//        }

        $totalCount1Star = $this->getTotalCountRate(1);
        $totalCount2Star = $this->getTotalCountRate(2);
        $totalCount3Star = $this->getTotalCountRate(3);
        $totalCount4Star = $this->getTotalCountRate(4);
        $totalCount5Star = $this->getTotalCountRate(5);

        if (0 != $totalCount1Star && 0 != $totalCountReview) {
            $pct1Star = number_format($totalCount1Star / $totalCountReview * 200, 2, '.', '');
        } else {
            $pct1Star = 0;
        }

        if (0 != $totalCount2Star && 0 != $totalCountReview) {
            $pct2Star = number_format($totalCount2Star / $totalCountReview * 200, 2, '.', '');
        } else {
            $pct2Star = 0;
        }

        if (0 != $totalCount3Star && 0 != $totalCountReview) {
            $pct3Star = number_format($totalCount3Star / $totalCountReview * 200, 2, '.', '');
        } else {
            $pct3Star = 0;
        }

        if (0 != $totalCount4Star && 0 != $totalCountReview) {
            $pct4Star = number_format($totalCount4Star / $totalCountReview * 200, 2, '.', '');
        } else {
            $pct4Star = 0;
        }

        if (0 != $totalCount5Star && 0 != $totalCountReview) {
            $pct5Star = number_format($totalCount5Star / $totalCountReview * 200, 2, '.', '');
        } else {
            $pct5Star = 0;
        }

//        $result = "";
//        $rate = round($average);
//        $fullStar = $rate;
//        $emptyStar = 5 - $rate;
//        for ($i = 0; $i < $fullStar; $i++) {
//            $result .= '<i class="fa fa-star" style="padding-left:2px;padding-right:2px"> </i>';
//        }
//        for ($i = 0; $i < $emptyStar; $i++) {
//            $result .= '<i class="fa fa-star-o" style="padding-left:2px;padding-right:2px"> </i>';
//        }
//        $ratingStar = $result;

        return [
            'averageRating' => $average,
            'totalCountReview' => $totalCountReview,
            //            'ratingStar' => $ratingStar,
            'totalCount1Star' => $totalCount1Star,
            'totalCount2Star' => $totalCount2Star,
            'totalCount3Star' => $totalCount3Star,
            'totalCount4Star' => $totalCount4Star,
            'totalCount5Star' => $totalCount5Star,
            'pct1Star' => $pct1Star,
            'pct2Star' => $pct2Star,
            'pct3Star' => $pct3Star,
            'pct4Star' => $pct4Star,
            'pct5Star' => $pct5Star,
            'id' => $id,
        ];
    }

    public function getTotalCountRate($totalStar)
    {
        $result = 0;

        $betweenValue = null;
        switch ($totalStar) {
            case 1:
                $betweenValue = [0.01, 1.00];

                break;
            case 2:
                $betweenValue = [1.01, 2.00];

                break;
            case 3:
                $betweenValue = [2.01, 3.00];

                break;
            case 4:
                $betweenValue = [3.01, 4.00];

                break;
            case 5:
                $betweenValue = [4.01, 5.00];

                break;
            default:
                return '-';

                break;
        }

        $reviews = Review::where('reviewee_user_id', $this->id)
            ->whereBetween('total_score', $betweenValue)
            ->get()
        ;

        return count($reviews);
    }

    public function isFavouriteFreelance()
    {
        $count = FavouriteFreelancer::where('user_id', \Auth::guard('users')->user()->id)
            ->where('freelancer_id', $this->id)
            ->count()
        ;

        if ($count >= 1) {
            return true;
        }

        return false;
    }

    public function getHourlyPay()
    {
        //DB store value in USD / CNY base on currency column
        //if cn language and currency is USD, convert USD to CNY
        //if cn language and currency is CNY, not need convert, straight retrieve
        //if en language and currency is USD, not need convert, straight retrieve
        //if en language and currency is CNY, convert CNY to USD

        if ('en' == app()->getLocale()) {
            if (Constants::USD == $this->currency || null == $this->currency || 0 == $this->currency) {
                return '$'.number_format($this->hourly_pay, 2, '.', ',').' /'.trans('common.hr');
            }
            if (Constants::CNY == $this->currency) {
                $exchangeRate = ExchangeRate::getExchangeRate(Constants::CNY, Constants::USD);
                $value = $this->hourly_pay * $exchangeRate;

                return '$'.number_format($value, 2, '.', ',').' /'.trans('common.hr');
            }
        } elseif ('cn' == app()->getLocale()) {
            if (Constants::USD == $this->currency || null == $this->currency || 0 == $this->currency) {
                $exchangeRate = ExchangeRate::getExchangeRate(Constants::USD, Constants::CNY);
                $value = $this->hourly_pay * $exchangeRate;

                return '¥'.number_format($value, 2, '.', ',').' /'.trans('common.hr');
            }
            if (Constants::CNY == $this->currency) {
                return '¥'.number_format($this->hourly_pay, 2, '.', ',').' /'.trans('common.hr');
            }
        }
    }

    public function getHourlyPay_v2($isRaw = false)
    {
        $returnNumber = 0;
        if ('en' == app()->getLocale()) {
            if (Constants::USD == $this->currency || null == $this->currency || 0 == $this->currency) {
                $returnNumber = $this->hourly_pay;
            }
            if (Constants::CNY == $this->currency) {
                $exchangeRate = ExchangeRate::getExchangeRate(Constants::CNY, Constants::USD);
                $value = $this->hourly_pay * $exchangeRate;

                $returnNumber = $value;
            }
        } elseif ('cn' == app()->getLocale()) {
            if (Constants::USD == $this->currency || null == $this->currency || 0 == $this->currency) {
                $exchangeRate = ExchangeRate::getExchangeRate(Constants::USD, Constants::CNY);
                $value = $this->hourly_pay * $exchangeRate;

                $returnNumber = $value; number_format($value, 2, '.', ',');
            }
            if (Constants::CNY == $this->currency) {
                $returnNumber = $this->hourly_pay;
            }
        }

        if ($isRaw) {
            return $returnNumber;
        }

        return number_format($returnNumber, 2, '.', ',');

    }

    public function getRatingStar()
    {
        $total = 0;
        $average = 0;
        $reviews = Review::where('reviewee_user_id', $this->id)->get();
        $totalCountReview = count($reviews);
        foreach ($reviews as $review) {
            $total += $review->total_score;
        }

        if (0 != $totalCountReview) {
            $average = number_format($total / $totalCountReview, 2, '.', '');
        }

        $result = '';
        $rate = round($average);
        $fullStar = $rate;
        $emptyStar = 5 - $rate;
        for ($i = 0; $i < $fullStar; ++$i) {
            $result .= '<i class="fa fa-star" style="padding-left:2px;padding-right:2px"> </i>';
        }
        for ($i = 0; $i < $emptyStar; ++$i) {
            $result .= '<i class="far fa-star" style="padding-left:2px;padding-right:2px"> </i>';
        }

        return $result;
    }

    public function isJobInvitationReceived($projectID)
    {
        $count = UserInbox::where('category', UserInbox::CATEGORY_NORMAL)
            ->where('project_id', $projectID)
            ->where('type', UserInbox::TYPE_PROJECT_INVITE_YOU)
            ->where('to_user_id', $this->id)
            ->count()
        ;

        if (0 == $count) {
            return false;
        }

        return true;
    }

    public function isSkillVerifiedByAdmin()
    {
        $userSkill = UserSkill::where('user_id', $this->id)
            ->where('experience', '>=', 1)
            ->first()
        ;

        if ($userSkill) {
            return true;
        }

        return false;
    }

    public static function getStatusLists($empty = false)
    {
        $arr = [];
//        if ($arr === true) {
//            $arr[] = 'User Status';
//        }
        $arr = [
            [
                'id' => static::STATUS_LOOK_FOR_PROJECTS,
                'name' => trans('common.looking_for_projects'),
            ],
            [
                'id' => static::STATUS_LOOK_FOR_COFOUNDER,
                'name' => trans('common.looking_for_co_founders'),
            ],
            [
                'id' => static::STATUS_LOOK_FOR_FULL_TIME_JOB,
                'name' => trans('common.looking_for_job'),
            ],
            [
                'id' => static::STATUS_ANY_STATUS,
                'name' => trans('common.any_status'),
            ],
        ];
//        $arr[static::STATUS_LOOK_FOR_PROJECTS] = trans('common.looking_for_projects');
//        $arr[static::STATUS_LOOK_FOR_COFOUNDER] = trans('common.looking_for_co_founders');
//        $arr[static::STATUS_LOOK_FOR_FULL_TIME_JOB] = trans('common.looking_for_job');
//        $arr[static::STATUS_ANY_STATUS] = trans('common.any_status');

        return $arr;
    }

    public static function translateStatus($id)
    {
        switch ($id) {
            case static::STATUS_LOOK_FOR_PROJECTS:
                return trans('common.looking_for_projects');

                break;
            case static::STATUS_LOOK_FOR_COFOUNDER:
                return trans('common.looking_for_co_founders');

                break;
            case static::STATUS_LOOK_FOR_FULL_TIME_JOB:
                return trans('common.looking_for_full_time_job');

                break;
            // case static::STATUS_LOOK_FOR_PART_TIME_JOB: return trans('common.looking_for_part_time_job'); break;
            case static::STATUS_ANY_STATUS:
                return trans('common.any_status');

                break;
            default:
                return '-';

                break;
        }
    }

    public function isContactInfoExist()
    {
        //email is a must so that can send email notification
        if (null == $this->email || '' == $this->email) {
            return false;
        }

        //if web, handphone, skype, wechat and qq empty, will pop up form
        if ((null == $this->handphone_no || '' == $this->handphone_no) &&
            (null == $this->skype_id || '' == $this->skype_id) &&
            (null == $this->wechat_id || '' == $this->wechat_id) &&
            (null == $this->qq_id || '' == $this->qq_id)
        ) {
            return false;
        }

        return true;
    }

    public static function getCurrencyName($id)
    {
        $users = WorldCountries::where('id', '=', $id)->first();

        return $users->currency_code;
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'performed_to', 'id');
    }

    public function sharedOfficeChatAction()
    {
        return $this->hasOne(SharedOfficeChatAction::class, 'user_id', 'id');
    }

    public function userCoinAddress()
    {
        return $this->hasOne(UserCoinAddress::class, 'user_id', 'id');
    }

    public function portfolios()
    {
        return $this->hasMany(Portfolio::class, 'user_id', 'id');
    }

    public function getPortfolioImage($portfolio)
    {
        $file = '';
        foreach ($portfolio as $port) {
            if (isset($port->files[0])) {
                if ($port->files[0]->file) {
                    $folder = public_path().'/uploads/portfolio/'.$port->files[0]->id;
                    if (!file_exists($folder)) {
                        $file = $port->files[0]->file;
                    } else {
                        $file = '/images/default_portfolio.png';
                    }
                }
            }
        }

        return $file;
    }
}
