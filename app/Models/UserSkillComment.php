<?php

namespace App\Models;

use Input;

class UserSkillComment extends BaseModels {

    protected $table = 'user_skill_comment';

    protected $fillable = [
        'user_id', 'comment', 'staff_id', 'is_official',
        'attitude_rate', 'attitude_comment', 'score_attitude', 'score_overall',
        'location_address', 'location_lat', 'location_lng',
    ];

    public static $rules = [
        'user_id' => 'required|exists:users,id',
        'comment' => 'required',
        'staff_id' => 'exists:staff,id',
        'official_project_id' => 'exists:official_projects,id',
        'is_official' => 'required|in:0,1',
        'attitude_rate' => 'required|in:0,1',
        'attitude_comment' => '',
        'score_attitude' => 'min:0|max:5',
        'score_overall' => 'min:0|max:5',
        'location_address' => '',
        'location_lat' => '',
        'location_lng' => '',
    ];

    protected $dates = [];

    public static function boot() {
        parent::boot();
    }

    public function scopeGetRecords($query) {
        return $query;
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function staff() {
        return $this->belongsTo('App\Models\Staff', 'staff_id', 'id');
    }

    public function officialProjects() {
        return $this->belongsTo('App\Models\OfficialProject', 'official_project_id', 'id');
    }

    public function skills() {
        return $this->hasMany('App\Models\UserSkillCommentSkills', 'comment_id', 'id');
    }

    public function files() {
        return $this->hasMany('App\Models\UserSkillCommentFiles', 'comment_id', 'id');
    }

    public function setCommentAttribute($value) {
        $this->attributes['comment'] = trim($value);
    }

    public function scopeGetFilteredResults($query) {
        if (Input::has('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::has('filter_comment') && Input::get('filter_comment') != '') {
            $query->where('comment', 'like', '%' . Input::get('filter_comment') . '%');
        }

        if (Input::has('filter_score_overall_from') && Input::get('filter_score_overall_from') != '') {
            $query->where('score_overall', '>=', Input::get('filter_score_overall_from'));
        }

        if (Input::has('filter_score_overall_to') && Input::get('filter_score_overall_to') != '') {
            $query->where('score_overall', '<=', Input::get('filter_score_overall_to'));
        }

        if (Input::has('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::has('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::has('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::has('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }
    }

    public function getLocationLatWithDefault() {
        return $this->location_lat ? $this->location_lat : 39.989628;
    }

    public function getLocationLngWithDefault() {
        return $this->location_lng ? $this->location_lng : 116.480983;
    }

}