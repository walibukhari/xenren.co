<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WithdrawRequest extends Model
{
    protected $fillable = [
        'bank_name',
        'iban_number',
        'card_number',
        'branch_name',
        'branch_code',
        'branch_address',
        'payment_request',
        'total_balance',
        'staff_id',
        'office_id',
        'transaction_id',
        'country_id',
        'remarks',
        'amount',
    ];

    const STATUS_APPROVED = 1;
    const STATUS_REJECTED = 3;

    public function staff()
    {
        return $this->hasOne(Staff::class,'id','staff_id');
    }
    public function office()
    {
        return $this->hasOne(SharedOffice::class,'id','office_id');
    }
    public function transaction()
    {
        return $this->hasOne(Transaction::class,'id','transaction_id');
    }
}
