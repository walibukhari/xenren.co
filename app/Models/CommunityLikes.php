<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommunityLikes extends Model
{
    protected $fillable = [
      'reply_id','likes','user_id'
    ];
}
