<?php

namespace App\Models;

use Input;

class UserSkillCommentFiles extends BaseModels {

    protected $table = 'user_skill_comment_files';

    protected $fillable = [
        'user_id', 'comment_id', 'path',
    ];

    public static $rules = [
        'user_id' => 'required|exists:users,id',
        'comment_id' => 'required|exists:user_skill_comment,id',
        'path' => 'required',
    ];

    protected $dates = [];

    public static function boot() {
        parent::boot();
    }

    public function scopeGetRecords($query) {
        return $query;
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function comment() {
        return $this->belongsTo('App\Models\UserSkillComment', 'comment_id', 'id');
    }

    public function setPathAttribute($file) {
        $allowed_ext = ['png', 'jpg', 'jpeg', 'bmp', 'gif', 'pdf', 'xls', 'xlsx', 'mp3', 'mp4', 'docs', 'docx'];
        if ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
            $ext = $file->getClientOriginalExtension(); // getting extension
            if (in_array($ext, $allowed_ext)) {
                $filename = generateRandomUniqueName();

                $now = new \Carbon\Carbon();
                $path = 'uploads/skillcomment/' . $now->format('Y/m') . '/';

                touchFolder($path);

                $file->move($path, $filename . '.' . $ext);

                $this->attributes['path'] = $path . $filename . '.' . $ext;
            } else {
                throw new \Exception('不允许的文件格式，允许的文件格式有：' . implode(', ', $allowed_ext));
            }
        }
    }

    public function scopeGetFilteredResults($query) {
        if (Input::has('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::has('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::has('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::has('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::has('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }
    }

    public function getFileTypeImage()
    {
        $result = "";

        if( in_array( substr(strrchr($this->path,'.'),1), ['jpg', 'jpeg']) )
        {
            $result = "/images/jpg.png";
        }
        else if( in_array( substr(strrchr($this->path,'.'),1), ['gif']) )
        {
            $result = "/images/gif.png";
        }
        else if( in_array( substr(strrchr($this->path,'.'),1), ['png']) )
        {
            $result = "/images/png.png";
        }
        else if( in_array( substr(strrchr($this->path,'.'),1), ['bmp']) )
        {
            $result = "/images/bmp.png";
        }
        else if( in_array( substr(strrchr($this->path,'.'),1), ['doc', 'docx']) )
        {
            $result = "/images/doc.png";
        }
        else if ( in_array( substr(strrchr($this->path,'.'),1), ['pdf']) )
        {
            $result = "/images/pdf.png";
        }
        else if ( in_array( substr(strrchr($this->path,'.'),1), ['xls', 'xlsx']) )
        {
            $result = "/images/xls.png";
        }
        else if ( in_array( substr(strrchr($this->path,'.'),1), ['mp3']) )
        {
            $result = "/images/mp3.png";
        }
        else if ( in_array( substr(strrchr($this->path,'.'),1), ['mp4']) )
        {
            $result = "/images/mp4.png";
        }

        return $result;
    }

    public function getShortFilename()
    {
        $result = "";
        $fullFilename = basename($this->path);

        if( strlen($fullFilename) >= 5 )
        {
            $partOfFile = substr($fullFilename, 0, 5);

            //get extension
            $array = explode('.', $this->path);
            $extension = end($array);

            $result = $partOfFile . '...' . '.' . $extension;
        }
        else
        {
            $result = $fullFilename;
        }

        return $result;
    }
}