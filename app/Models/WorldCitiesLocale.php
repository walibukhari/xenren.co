<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorldCitiesLocale extends Model
{
	protected $table = 'world_cities_locale';
}
