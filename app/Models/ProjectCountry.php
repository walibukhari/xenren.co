<?php

namespace App\Models;

use Input;
use App\Models\Skill;

class ProjectCountry extends BaseModels {

    public $rules = [
        'project_id' => 'required|exists:projects,id',
        'country_id' => 'required|exists:countries,id',
    ];

    protected $table = 'project_countries';

    protected $fillable = [
        'project_id', 'country_id', ''
    ];

    protected $dates = [];


    public function project() {
        return $this->belongsTo('App\Models\Project', 'project_id', 'id');
    }

    public function country() {
        return $this->belongsTo('App\Models\WorldCountries', 'country_id', 'id');
    }

}