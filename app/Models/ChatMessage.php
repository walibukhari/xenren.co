<?php

namespace App\Models;

use Input;

class ChatMessage extends BaseModels {

    protected $table = 'chat_messages';

    public static function getRecentChat(){
    	$userId = \Auth::guard('users')->user()->id;
    	$recents = static::GetChatMessage()
        		->orWhere(function ($query) use ($userId){
	                $query->where('receiver_user_id',$userId);
	            })
        		->groupBy(['sender_user_id'])
        		->orderBy('created_at','desc')
                ->get();
        $lists = array();
        foreach ($recents as $key => $var) {
        	$email 		= '';
        	$userLink 	= 0;
        	if($var->receiver_user_id == $userId){
        		$email 		= $var->SendBy()->first()->email;
        		$userLink 	= $var->sender_user_id;
        	}else{
        		$email = $var->ReceiveBy()->first()->email;
        		$userLink 	= $var->receiver_user_id;
        	}
        	$name = explode('@', $email);
        	if(count($name) >= 2){
        		$name = $name[0];
        	}else{
        		$name = $email;
        	}
        	$lists[$userLink]['email'] 			= $name;
            $lists[$userLink]['last_chat'] 		= $var->message;
        	$lists[$userLink]['time'] 			= $var->created_at;
        }
        return $lists;
    }

    public function room()
    {
        return $this->belongsTo('App\Models\ChatRoom', 'room_id', 'id');
    }

    public function sender_user()
    {
        return $this->belongsTo('App\Models\User', 'sender_user_id', 'id');
    }

    public function sender_admin()
    {
        return $this->belongsTo('App\Models\Staff', 'sender_staff_id', 'id');
    }

    public function receiver_user()
    {
        return $this->belongsTo('App\Models\User', 'receiver_user_id', 'id');
    }

    public function receiver_admin()
    {
        return $this->belongsTo('App\Models\Staff', 'receiver_staff_id', 'id');
    }

    public function scopeGetChatMessage($query)
    {
        return $query;
    }

    public function setFileAttribute($file)
    {
        if ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
            if($file->guessClientExtension() == 'dwg'){
                $ext = $file->guessClientExtension();
                $c = new \Carbon\Carbon();
                $path = 'uploads/conversation/' . \Auth::guard('users')->user()->id . '/' . $c->format('Y/m/');
                $filename = 'back_' . generateRandomUniqueName();

                touchFolder($path);

                $file->move($path, $filename.'.'.$ext);
                $this->attributes['file'] = $path. $filename.'.'.$ext;
            }else{
                $img = \Image::make($file);
                $mime = $img->mime();
                $ext = convertMimeToExt($mime);

                $c = new \Carbon\Carbon();
                $idPath = \Auth::guard('users')->check() ? \Auth::guard('users')->user()->id : 'staff';
                $path = 'uploads/conversation/' . $idPath . '/' . $c->format('Y/m/');
                $filename = 'back_' . generateRandomUniqueName();

                touchFolder($path);

                $img = $img->save($path . $filename . $ext);

                $this->attributes['file'] = $path . $filename . $ext;
            }
        }
    }

    public function getFile() {
        if ($this->file != null && $this->file != '') {
            return asset($this->file);
        } else {
            return null;
        }
    }

    public static function pullChatMessage($chatRoomId = null, $whoInput){
        $filter = array();
        $filter['chatRoomId'] = $chatRoomId;


        $_recents = static::GetChatMessage()
            ->where(function ($query) use ($filter){
                $query->where('room_id',$filter['chatRoomId']);
            })
            ->orderBy('created_at','desc')
            ->take(20)
            ->get();

        $recents = array();
        foreach( $_recents as $recent)
        {
            array_unshift($recents, $recent);
        }

        $lists = array();
        $i = 0;
        foreach ($recents as $key => $var) {
            $status     = 'out';
            if( $whoInput == 'user' && $var->sender_user_id == \Auth::guard('users')->user()->id){
                $status = 'in';
            }
            if(is_numeric($var->sender_staff_id) && $var->sender_staff_id > 0){
                $admin = Staff::find($var->sender_staff_id);
                if( $whoInput == 'staff' && $var->sender_staff_id == \Auth::guard('staff')->user()->id ){
                    $status             = 'in';
                }else{
                    $status             = 'out';
                }
                $name                   = $admin->name;
                $lists[$i]['avatar']    = $admin->getAvatar();
            }else{
                if($var->sender_user()->first()->name != ''){
                    $name = $var->sender_user()->first()->getName();
                }else{
                    $email      = $var->sender_user()->first()->email;
                    $name       = explode('@', $email);
                    if(count($name) >= 2){
                        $name = $name[0];
                    }else{
                        $name = $email;
                    }
                }
                $lists[$i]['avatar']          = $var->sender_user()->first()->getAvatar();
            }

            $lists[$i]['email']           = $name;
            $lists[$i]['content']         = $var->message;
            $lists[$i]['time']            = date('d M, Y H:i',strtotime($var->created_at));
            $lists[$i]['status']          = $status;
            $lists[$i]['date']            = date('Y-m-d',strtotime($var->created_at));
            $lists[$i]['file']            = $var->file;
            //check is image
            $isImage = 0;
            if(file_exists($var->file)){
                if(strpos($var->file, '.txt') !== false || strpos($var->file, '.doc') !== false){
                    $lists[$i]['content']         = str_replace('--file upload: ', '', '<a href="' . $var->getFile(). '">' . $var->message . '<a>');
                    $isImage = 0;
                }else{
                    $a = getimagesize($var->file);
                    $image_type = $a[2];
                    if(in_array($image_type , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP)))
                    {
                        $lists[$i]['content']         = str_replace('--file upload: ', '', $var->message);
                        $isImage = 1;
                    }
                }
            }
            $lists[$i]['isImage']          = $isImage;
            //check is audio
            $isAudio = 0;
            if(file_exists($var->file)){
                $allowed = array(
                    'audio/mpeg', 'audio/x-mpeg', 'audio/mpeg3', 'audio/x-mpeg-3', 'audio/aiff',
                    'audio/mid', 'audio/x-aiff', 'audio/x-mpequrl','audio/midi', 'audio/x-mid',
                    'audio/x-midi','audio/wav','audio/x-wav','audio/xm','audio/x-aac','audio/basic',
                    'audio/flac','audio/mp4','audio/x-matroska','audio/ogg','audio/s3m','audio/x-ms-wax',
                    'audio/xm','application/ogg','application/mp3','audio/mp3'
                );

                // check REAL MIME type
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $type = finfo_file($finfo, $var->file);
                finfo_close($finfo);

                // check to see if REAL MIME type is inside $allowed array
                if( in_array($type, $allowed) ) {
                    $isAudio = 1;
                }
            }
            $lists[$i]['isAudio']         = $isAudio;

            if($i == 0){
                $lists[$i]['changeDate']  = 1;
            }else{
                if($lists[$i]['date'] != $lists[$i-1]['date'])
                    $lists[$i]['changeDate']  = 1;
                else
                    $lists[$i]['changeDate']  = 0;
            }
            $i++;
        }
        return [
            'lists'         => $lists
        ];
    }

}