<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCanceledDispute extends Model
{
    protected $table = 'user_canceled_disputes_count';
    
    protected $fillable = [
	    'user_id',
	    'transaction_id',
	    'project_id',
    ];
}
