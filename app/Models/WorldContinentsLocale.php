<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorldContinentsLocale extends Model
{
	protected $table = 'world_continents_locale';
}
