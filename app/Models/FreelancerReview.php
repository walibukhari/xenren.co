<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FreelancerReview extends Model
{
    //
    protected $table = 'freelancer_reviews';
}
