<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportedUser extends Model
{
    protected $table = 'reported_users';

    protected $fillable = [
        'reported_by',
        'reported_user_id',
        'reason'
    ];

    public function reportUser($request)
    {
        $input = [
            'reported_by' => \Auth::user()->id,
            'reported_user_id' => isset($request->user_id) ? $request->user_id : '',
            'reason' => isset($request->reasson) ? $request->reasson : '',
        ];

        $input = array_filter($input, 'strlen');
        return $this->create($input);
    }
}
