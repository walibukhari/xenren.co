<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ProjectDispute extends Model
{
		/**
		 * Business Flow:
		 * - In case of disputes for already released milestone "milesone_id" will be filled in,
		 * because we're creating* dispute for milestone
		 * - In case of Requesting Milestone "transaction_id" will be filled because we're
		 * creating dispute for a non milestone request by employer
		*/
	
    protected $table = 'project_disputes';

    protected $fillable = [
        'project_id',
        'milestone_id',
        'transaction_id',
        'created_by',
        'status'
    ];

    const STATUS_INPROGRESS = 0;
    const STATUS_CONFIRM = 1;
    const STATUS_REJECTED= 2;
    const STATUS_REFUNDED = 3;
    const STATUS_CANCEL_REQUEST_BY_EMPLOYER = 4;
    const STATUS_CANCELED_BY_FREELANCER = 5;

    const TYPE_MANUAL_DISPUTE = 1;
    const TYPE_MILESTONE_DISPUTE = 2;
	
		public function setCreatedByAttribute()
		{
			if(Auth::user()) {
				return $this->attributes['created_by'] = Auth::user()->id;
			}
		}
    
    public function milestone()
    {
        return $this->hasOne(ProjectMilestones::class, 'id', 'milestone_id');
    }

    /**
     * @param $input
     * @return static
     */
    public function createDispute($input)
    {
        return $this->create($input);
    }
}
