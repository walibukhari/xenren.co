<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectMilestoneStauts extends Model
{
    protected $table = 'project_milestones_status';

    protected $fillable = [
        'project_id',
        'milestone_id',
        'status',
        'comment'
    ];

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'milestone_id', 'milestone_id');
    }

    public function project()
    {
        return $this->hasOne(Project::class, 'id', 'project_id');
    }

    public function milestone()
    {
        return $this->hasOne(ProjectMilestones::class, 'id', 'milestone_id');
    }

    public function createMileStoneStatus($request)
    {
        return $this->create($request->all());
    }

    public function updateMileStoneStatus($request, $updateMileStoneStatus)
    {
        if(isset($request->milestone)) {
            return $this
                ->where('project_id', '=', $updateMileStoneStatus)
                ->where('milestone_id', '=', $request->milestone)
                ->update([
                    'status' => $request->status,
                    'comment' => isset($request->comment)? $request->comment : 'n/a'
                ]);
        } else {
            return $this
                ->where('project_id', '=', $updateMileStoneStatus)
                ->where('status', '=', 1)
                ->update([
                    'status' => $request->status,
                    'comment' => isset($request->comment)? $request->comment : 'n/a'
                ]);
        }
    }
}
