<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SharedOfficesBulletin extends Model
{
    protected $table = 'shared_offices_bulletin';
    
    protected $fillable = [
	    'shared_offices_id',
			'bulletin_id'
    ];
	
		public function bulletinDetail()
		{
			return $this->hasOne(Bulletin::class, 'id', 'bulletin_id');
		}
}
