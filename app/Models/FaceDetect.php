<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaceDetect extends Model
{

    const STATUS_FACE_DETECT_TRUE = 1;
    const STATUS_FACE_DETECT_FALSE = 2;

    protected $fillable = [
      'user_id' , 'face_status' ,'face_response'
    ];

    public static function createFaceData($status , $userId , $response){
        $check = FaceDetect::where('user_id','=',$userId)->first();
        if($status == self::STATUS_FACE_DETECT_TRUE) {
            if(is_null($check)) {
                self::create([
                    'user_id' => $userId,
                    'face_response' => json_encode($response),
                    'face_status' => $status
                ]);
            } else {
                self::where('user_id','=',$userId)->update([
                    'user_id' => $userId,
                    'face_response' => json_encode($response),
                    'face_status' => $status
                ]);
            }
        } else {
            if(is_null($check)) {
                self::create([
                    'user_id' => $userId,
                    'face_response' => json_encode($response),
                    'face_status' => $status
                ]);
            } else {
                self::where('user_id','=',$userId)->update([
                    'user_id' => $userId,
                    'face_response' => json_encode($response),
                    'face_status' => $status
                ]);
            }
        }
    }
}
