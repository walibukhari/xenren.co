<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PushNotificationSettings extends Model
{
    const WEB_NOTIFICATION_ON = 1;
    const WEB_NOTIFICATION_OFF = 0;
    CONST FREE_NOTIFICATIONS = 30;
    protected $fillable = [
        'user_id',
        'low_fixed_price',
        'high_fixed_price',
        'low_hourly_price',
        'high_hourly_price',
        'low_atleast_spend',
        'high_atleast_spend',
        'package_id',
        'country_type',
        'active',
        '$user->userSetting',
    ];

    public function users()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

    public function keywords()
    {
        return $this->hasMany(PushNotificationKeywordSettings::class, 'push_notification_settings_id','id');
    }

    public function language()
    {
        return $this->hasMany(PushNotificationLanguageSettings::class, 'push_notification_settings_id','id');
    }

    public function country()
    {
        return $this->hasMany(PushNotificationCountrySettings::class, 'push_notification_settings_id','id');
    }

    /**
     * @param $obj
     * @return mixed
     */
    public function createSettings($obj)
    {
        return $this->create([
            'user_id' => $obj['user_id'],
            'low_fixed_price' => $obj['low_fixed_price'],
            'high_fixed_price' => $obj['high_fixed_price'],
            'low_hourly_price' => $obj['low_hourly_price'],
            'high_hourly_price' => $obj['high_hourly_price'],
            'low_atleast_spend' => $obj['low_atleast_spend'],
            'high_atleast_spend' => $obj['high_atleast_spend'],
            'country_type' => $obj['country_type'],
            'package_id' => $obj['package_id'],
            'active' =>  $obj['active']
        ]);
    }

    /**
     * @param $obj
     * @param $id
     * @return mixed
     */
    public function updateSettings($obj, $id)
    {
        return $this->where('id', '=', $id)->update([
            'user_id' => $obj['user_id'],
            'low_fixed_price' => $obj['low_fixed_price'],
            'high_fixed_price' => $obj['high_fixed_price'],
            'low_hourly_price' => $obj['low_hourly_price'],
            'high_hourly_price' => $obj['high_hourly_price'],
            'low_atleast_spend' => $obj['low_atleast_spend'],
            'high_atleast_spend' => $obj['high_atleast_spend'],
            'country_type' => $obj['country_type'],
            'package_id' => $obj['package_id'],
            'active' =>  $obj['active']
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteSettings($id)
    {
        return $this->where('id', '=', $id)->delete();
    }

    /**
     * @return mixed
     */
    public function getSettings()
    {
        return $this->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getSingleSettings($id)
    {
        return $this->where('id', '=', $id)->first();
    }

    /**
     * @param $userId
     * @return mixed
     * @internal param $id
     */
    public function getUserSettings($userId)
    {
        return $this->where('user_id', '=', $userId)->with('keywords.skills', 'language.languageName',  'country.countryName')->first();
//        return $this->where('user_id', '=', $userId)->with('keyword.skills', 'language.languageName', 'country.countryName')->first();
    }
}
