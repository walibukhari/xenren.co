<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommunityRead extends Model
{

    const STATUS_READ_AS = 1;
    const STATUS_READ_AS_SINGLE_TOPIC = 2;

    protected $fillable = [
      'user_id','topic_id','read_all','un_read_all','read_as','status','d_id'
    ];
}
