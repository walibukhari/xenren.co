<?php

namespace App\Models;

class ProjectQuestion extends BaseModels {

    protected $table = 'project_question';

    protected $fillable = [
        'project_id', 'question', ''
    ];

    protected $dates = [];

    public static function boot() {
        parent::boot();
    }

    public function scopeGetProjectQuestion($query) {
        return $query;
    }

    public function project() {
        return $this->belongsTo('App\Models\Project', 'project_id', 'id');
    }

    public function applicantAnswer() {
        return $this->hasMany(ProjectApplicantAnswer::class, 'project_question_id', 'id');
    }

}