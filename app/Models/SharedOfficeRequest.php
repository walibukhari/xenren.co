<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class SharedOfficeRequest extends Model
{
    use SoftDeletes;
    protected $table = 'shared_office_request';

    protected $fillable = [
        'company_name',
        'location',
        'image',
        'full_name',
        'phone_number',
        'email',
        'status',
        'is_email_approved'
    ];

    protected $dates = ['deleted_at'];

    const SHARED_OFFICE_REQUEST_STATUS = 0;
    const SHARED_OFFICE_REQUESTS_STATUS = 1;
    const SHARED_OFFICE_PERSON_EMAIL_VERIFIED_STATUS = 2;
    const SHARED_OFFICE_PERSON_EMAIL_UN_VERIFIED_STATUS = 0;

}
