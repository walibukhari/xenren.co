<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'currencies';

    protected $fillable = [
        'name_en', 'name_cn', 'created_by', 'updated_by'
    ];

    public static function getCurrencyLists($empty = false) {
        $arr = array();
        if ($arr === true) {
            $arr[] = 'Currency';
        }

        if( $empty )
        {
            $arr[''] = trans('common.all');
        }

        $currencies = Currency::all();
        foreach( $currencies as $currency)
        {
            $field = 'name_' . app()->getLocale();
            $arr[$currency->id] = $currency->$field;
        }

        return $arr;
    }

    public function getName() {
        $field = 'name_' . app()->getLocale();

        return $this->$field;
    }
}
