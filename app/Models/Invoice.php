<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
      'invoice_number' ,'staff_id' ,'user_id','date','amount'
    ];
}
