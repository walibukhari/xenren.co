<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TimeZone extends Model
{
    protected $fillable = [
      'value','abbr','offset','isdst','text','utc'
    ];
}
