<?php

namespace App\Models;


class UserToken extends BaseModels {
    protected $table = 'users_token';

    protected $fillable = [
        'user_id', 'token', 'is_login'
    ];

    //protected $dates = ['expired_time'];

    public static function boot() {
        parent::boot();
    }
}