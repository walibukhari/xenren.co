<?php

namespace App\Models;

use Input;
use App\Models\Skill;

class ProjectSkill extends BaseModels {

    public $rules = [
        'project_id' => 'required|exists:projects,id',
        'skill_id' => 'required|exists:skill,id',
    ];
    protected $table = 'project_skill';
    protected $fillable = [
        'project_id', 'skill_id', 'created_at'
    ];
    protected $dates = [];
    public $timestamps = true;
    public static function boot() {
        parent::boot();
    }

    public function scopeGetProjectSkill($query) {
        return $query;
    }

    public function project() {
        return $this->belongsTo('App\Models\Project', 'project_id', 'id');
    }

    public function skill() {
        return $this->belongsTo('App\Models\Skill', 'skill_id', 'id');
    }

    public function getSkillName($id){
        $model = Skill::find($id);
        return $model->name_en;
    }
}
