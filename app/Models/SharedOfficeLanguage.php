<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SharedOfficeLanguage extends Model
{
    protected $table = 'shared_office_languages';
    
    protected $fillable = [
	    'shared_offices_id',
	    'office_name_cn',
	    'office_description_cn',
	    'office_space_cn',
	    'contact_name_cn',
	    'contact_phone_cn',
	    'contact_email_cn',
	    'contact_address_cn',
    ];
}
