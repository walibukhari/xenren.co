<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Staff;

class News extends BaseModels
{

    protected $table = 'news';

    protected $fillable = ['title','sub_title', 'link', 'image', 'language', 'pic_size'];

    public static function boot() {
        parent::boot();
    }

    public function stuff() {
        return $this->belongsTo('App\Models\Staff', 'staff_id', 'id');
    }

    public function scopeGetNews($query){
        return $query;
    }

    public function createdAt(){
        $date = strtotime( $this->created_at );
        return date( 'M d, Y', $date );
    }

//    public function getStaffName(){
//        $model = Staff::GetStaff()->where('id', $this->staff_id);
//        return $model->name;
//    }
}
