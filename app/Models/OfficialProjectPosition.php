<?php

namespace App\Models;

use Input;
use App\Models\Skill;

class OfficialProjectPosition extends BaseModels {

    protected $table = 'official_project_positions';

    protected $fillable = [
        'official_project_id', 'job_position_id',
        'needed', 'description',
    ];

    public $rules = [
        'official_project_id' => 'required|exists:official_projects,id',
        'job_position_id' => 'required|exists:job_position,id',
        'needed' => 'required|min:1',
        'description' => '',
    ];

    protected $dates = [];

    public static function boot() {
        parent::boot();
    }

    public function scopeGetProjectSkill($query) {
        return $query;
    }

    public function officialProject() {
        return $this->belongsTo('App\Models\OfficialProject', 'official_project_id', 'id');
    }

    public function jobPosition() {
        return $this->belongsTo('App\Models\JobPosition', 'job_position_id', 'id');
    }
}