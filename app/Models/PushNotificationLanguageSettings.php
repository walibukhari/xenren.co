<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PushNotificationLanguageSettings extends Model
{
    protected $table  = 'push_notification_language_settings';

    protected $fillable = [
        'push_notification_settings_id',
        'language_id',
    ];

    public function languageName()
    {
        return $this->hasOne(Language::class, 'id', 'language_id');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecordBySettingId($id)
    {
        return $this->where('push_notification_settings_id', '=', $id)->delete();
    }

    /**
     * @param $obj
     * @return mixed
     */
    public function createRecord($obj)
    {
        return $this->create([
            'push_notification_settings_id' => $obj['push_notification_settings_id'],
            'language_id' => $obj['language_id'],
        ]);
    }

    /**
     * @param $obj
     * @param $id
     * @return mixed
     */
    public function updateRecord($obj, $id)
    {
        return $this->where('id', '=', $id)->update([
            'push_notification_settings_id' => $obj['push_notification_settings_id'],
            'language_id' => $obj['language_id'],
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        return $this->where('id', '=', $id)->delete();
    }

    public function getRecord()
    {
        return $this->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getSingleRecord($id)
    {
        return $this->where('id', '=', $id)->first();
    }
}
