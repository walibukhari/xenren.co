<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class Portfolio extends Model
{
    protected $table = 'portfolios';

    const PORTFOLIO_TYPE_DESIGNER = 1;

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function files(){
        return $this->hasMany('App\Models\PortfolioFile', 'portfolio_id', 'id');
    }

    public function getUrl()
    {
        return $this->addHttp($this->url);
    }

    private function addHttp($url) {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }
        return $url;
    }

}
