<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SharedOfficeCategory extends Model
{

    const SOFTWARE = 1;
    const DESIGNER = 2;
    const MARKETER = 3;
    const FILM_HUB = 4;
    const HARDWARE_HUB = 5;
    const DRAWING = 6;
    const MUSIC_HUB = 7;

    protected $fillable = [
        'name' , 'image' , 'type'
    ];

    public static function types()
    {
        return collect([
            '0' => [
                'id' => '1',
                'name' => 'SOFTWARE'
            ],
            '1' => ['id' => '2', 'name' => 'DESIGNER'],
            '2' => ['id' => '3', 'name' => 'MARKETER'],
            '3' => ['name' => 'FILM HUB','id' => '4'],
            '4' => ['name' => 'HARDWARE HUB','id' => '5'],
            '5' => ['name' => 'DRAWING','id' => '6'],
            '6' => ['name' => 'MUSIC HUB','id' => '7']
        ]);
    }

    public function getTypeName($type){
        $name =  self::types()->where('id','=',$type)->first();
        if(isset($name)) {
            return $name['name'];
        } else {
            return '--';
        }
    }
}
