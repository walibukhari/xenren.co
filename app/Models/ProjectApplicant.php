<?php

namespace App\Models;

use App\Constants;
use Illuminate\Support\Facades\Input;

class ProjectApplicant extends BaseModels {

    const STATUS_NEW = 1;
    const STATUS_APPLY = 2;
    const STATUS_CREATOR_SELECTED = 3;
    const STATUS_CREATOR_REJECTED = 4;
    const STATUS_APPLICANT_ACCEPTED = 5;
    const STATUS_APPLICANT_REJECTED = 6;
    const STATUS_COMPLETED = 7;
    const STATUS_INTERVIEW = 8;

    const PAY_TYPE_UNKNOWN = 0;
    const PAY_TYPE_HOURLY_PAY = 1;
    const PAY_TYPE_FIXED_PRICE = 2;

    const DISPLAY_ORDER_PROGRESSING = 1;
    const DISPLAY_ORDER_PUBLISHED = 2;
    const DISPLAY_ORDER_COMPLETED = 3;
    const DISPLAY_ORDER_OTHER = 4;

    const IS_READ_NO = 0;
    const IS_READ_YES = 1;

    protected $table = 'project_applicants';

    protected $fillable = [
        'user_id', 'project_id', 'status', 'quote_price', 'pay_type', 'about_me', 'display_order',
        'title', 'description', 'reference_price', 'reference_price_type', 'skill_id_string', 'daily_updates', 'hourly_rate'
    ];

    protected $dates = [];

    public function getDailyUpdateAttribute($val)
    {
        return isset($val) && $val == 1 ? 'Yes': 'No';
    }

    public static function boot() {
        parent::boot();
    }

    public function scopeGetProjectApplicant($query) {
        return $query;
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function userInbox() {
        return $this->hasMany('App\Models\UserInbox', 'project_id', 'project_id');
    }

    public function awardedUserInbox() {
        return $this->hasOne('App\Models\UserInbox', 'project_id', 'project_id');
    }

    public function project() {
        return $this->belongsTo('App\Models\Project', 'project_id', 'id');
    }

    public function TimeTracker() {
        return $this->hasMany(TimeTrack::class, 'project_id', 'project_id');
    }

    public function scopeGetFilteredResults($query)
    {
        if (Input::has('filter_id') && Input::get('filter_id') != '') {
            $query->where('id', Input::get('filter_id'));
        }

        if (Input::has('filter_username') && Input::get('filter_username') != '') {
            $query
                ->join('users', function ($join) {
                    $join->on('users.id', '=', 'project_applicants.user_id')
                        ->where('users.name', 'like', '%' . Input::get('filter_username') . '%');
                });
        }

        if (Input::has('filter_quote_price') && Input::get('filter_quote_price') != '') {
            $query->where('quote_price', Input::get('filter_quote_price'));
        }

        if (Input::has('filter_pay_type') && Input::get('filter_pay_type') != '') {
            if (Input::get('filter_pay_type') == 1 || Input::get('filter_pay_type') == 2) {
                $query->where('pay_type', Input::get('filter_pay_type'));
            }
        }

        if (Input::has('filter_status') && Input::get('filter_status') != -1) {
            if (Input::get('filter_status') == 0 ||
                Input::get( 'filter_status') == $this::STATUS_APPLY ||
                Input::get('filter_status') == $this::STATUS_CREATOR_SELECTED ||
                Input::get('filter_status') == $this::STATUS_CREATOR_REJECTED ||
                Input::get('filter_status') == $this::STATUS_APPLICANT_ACCEPTED ||
                Input::get('filter_status') == $this::STATUS_APPLICANT_REJECTED ||
                Input::get('filter_status') == $this::STATUS_COMPLETED
            ) {
                $query->where('status', Input::get('filter_status'));
            }
        }

        if (Input::has('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::has('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

    }

    public function getStatus()
    {
        switch ($this->status) {
            case static::STATUS_APPLY:
                return trans('common.apply');
                break;
            case static::STATUS_CREATOR_SELECTED:
                return trans('common.creator_selected');
                break;
            case static::STATUS_CREATOR_REJECTED:
                return trans('common.project_creator_reject');
                break;
            case static::STATUS_APPLICANT_ACCEPTED:
                return trans('common.applicant_accept');
                break;
            case static::STATUS_APPLICANT_REJECTED:
                return trans('common.applicant_reject');
                break;
            case static::STATUS_COMPLETED:
                return trans('common.completed');
                break;
            default:
                return '-';
                break;
        }
    }

    public function getPayType()
    {
        switch ($this->pay_type) {
            case static::PAY_TYPE_HOURLY_PAY:
                return trans('common.pay_hourly');
                break;
            case static::PAY_TYPE_FIXED_PRICE:
                return trans('common.fixed_price');
                break;
            default:
                return '-';
                break;
        }
    }

    public function getAnswer($question_id)
    {
        $result = " - " . trans('common.no_answer') . " - ";

        $model = ProjectApplicantAnswer::where('project_applicant_id', $this->id)
            ->where('project_question_id', $question_id)->first();

        if ($model) {
            $result = $model->answer;
        }

        return $result;
    }

    public function getQuotePrice()
    {
        //DB store value in USD
        //if cn language, convert USD to CNY
        //if en language, not need convert, straight retrieve

        if( app()->getLocale() == "en" )
        {
            return '$' . number_format($this->quote_price, 2, '.', ',');
        }
        else
        {
            $exchangeRate = ExchangeRate::getExchangeRate(Constants::USD, Constants::CNY);
            $value = $this->quote_price * $exchangeRate;
            return ' ¥' . number_format($value, 2, '.', ',');
        }
    }
}