<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    const TOPIC_TYPE_OFFICAL = 1;
    const TOPIC_TYPE_DISCUSSION = 2;

    protected $fillable = [
        'topic_name',
        'topic_description',
        'topic_type'
    ];

    public function read(){
        return $this->hasOne(CommunityRead::class,'topic_id','id');
    }

    public static function createTopics($request){
        return Topic::create([
            'topic_name' => $request['topic_name'],
            'topic_description' => $request['topic_description'],
            'topic_type' => $request['topic_type'],
        ]);
    }

    public static function getTopicType($type) {
        if($type == self::TOPIC_TYPE_OFFICAL) {
            return 'Xenren Offical';
        } else {
            return 'Community Discussion';
        }
    }
}
