<?php

namespace App\Models;

use Input;

class SkillKeywordGroup extends BaseModels {

    protected $table = 'skill_keyword_groups';

    protected $fillable = [
        'skill_id', 'group'
    ];

    public function skill() {
        return $this->belongsTo('App\Models\Skill', 'skill_id', 'id');
    }


}