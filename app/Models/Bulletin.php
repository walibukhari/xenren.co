<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bulletin extends Model
{
	protected $table = 'bulletin';
	
	protected $fillable = [
		'text',
		'text_cn',
		'status',
	];
	
	protected $appends = [
		'status_name'
	];
	
	CONST STATUS_INACTIVE = 0;
	CONST STATUS_ACTIVE = 1;
	
	public function getStatusNameAttribute()
	{
		$status = $this->status;
		if($status == self::STATUS_ACTIVE) {
			return 'Active';
		}
		if($status == self::STATUS_INACTIVE) {
			return 'In Active';
		}
		return '--';
	}
	
	public function getBulletin()
	{
		return $this->get();
	}
}
