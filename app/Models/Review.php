<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    const RATE_PAYMENT_ON_TIME = 1;
    const RATE_COMMUNICATION = 2;
    const RATE_AVAILABILITY = 3;
    const RATE_TRUSTWORTHY = 4;
    const RATE_CLEAR_REQUIREMENT = 5;
    const RATE_SKILL = 6;
    const RATE_QUALITY_OF_WORK = 7;
    const RATE_ADHERENCE_TO_SCHEDULE = 8;
    const RATE_COOPERATION = 9;

    const REASON_PROJECT_COMPLETED = 1;
    const REASON_NO_RESPONSE = 2;
    const REASON_PROJECT_CANCELLED = 3;
    const REASON_TERMS_AND_CONDITION_CHANGED = 4;
    const REASON_WORK_COMPLETED = 5;
    const REASON_INSUFFICIENT_SKILLS = 6;

    const LP_DIFFICULT_TO_UNDERSTAND = 1;
    const LP_ACCEPTABLE = 2;
    const LP_FLUENT = 3;
    const LP_I_DIDNT_SPEAK = 4;

    const TYPE_FREELANCER_REVIEW = 1;
    const TYPE_EMPLOYER_REVIEW = 2;

    const FWTW_USER_TO_USER = 1;
    const FWTW_USER_TO_ADMIN = 2;
    const FWTW_ADMIN_TO_USER = 3;

    public function reviewer_user() {
        return $this->belongsTo('App\Models\User', 'reviewer_user_id', 'id');
    }

    public function reviewee_user() {
        return $this->belongsTo('App\Models\User', 'reviewee_user_id', 'id');
    }

    public function reviewer_staff() {
        return $this->belongsTo('App\Models\Staff', 'reviewer_staff_id', 'id');
    }

    public function reviewee_staff() {
        return $this->belongsTo('App\Models\Staff', 'reviewee_staff_id', 'id');
    }

    public function project() {
        return $this->belongsTo('App\Models\Project', 'project_id', 'id');
    }

    public function reviewSkills() {
        return $this->hasMany('App\Models\ReviewSkill', 'review_id', 'id');
    }

    public static function getEmployerReasonLists($empty = false) {
        $arr = array();
        if ($arr === true) {
            $arr[] = 'Employer Reason';
        }
        $arr[static::REASON_PROJECT_COMPLETED] = trans('common.project_completed');
        $arr[static::REASON_NO_RESPONSE] = trans('common.no_response');
        $arr[static::REASON_PROJECT_CANCELLED] = trans('common.project_cancelled');
        $arr[static::REASON_TERMS_AND_CONDITION_CHANGED] = trans('common.terms_and_condition_changed');

        return $arr;
    }

    public static function getFreelancerReasonLists($empty = false) {
        $arr = array();
        if ($arr === true) {
            $arr[] = 'Freelancer Reason';
        }
        $arr[static::REASON_WORK_COMPLETED] = trans('common.work_completed');
        $arr[static::REASON_INSUFFICIENT_SKILLS] = trans('common.insufficient_skills');
        $arr[static::REASON_PROJECT_CANCELLED] = trans('common.project_cancelled');

        return $arr;
    }

    public static function getEnglishProficiencyLists($empty = false) {
        $arr = array();
        if ($arr === true) {
            $arr[] = 'English Proficiency';
        }
        $arr[static::LP_DIFFICULT_TO_UNDERSTAND] = trans('common.difficult_to_understand');
        $arr[static::LP_ACCEPTABLE] = trans('common.acceptable');
        $arr[static::LP_FLUENT] = trans('common.fluent');
        $arr[static::LP_I_DIDNT_SPEAK] = trans('common.i_didnt_speak_to', ['who' => trans('common.client')]);

        return $arr;
    }

    public static function getEmployerRateCategoryLists($empty = false) {
        $arr = array();
        if ($arr === true) {
            $arr[] = 'Employer Rate Category';
        }
        $arr[static::RATE_PAYMENT_ON_TIME] = trans('common.payment_on_time');
        $arr[static::RATE_COMMUNICATION] = trans('common.communication');
        $arr[static::RATE_AVAILABILITY] = trans('common.availability');
        $arr[static::RATE_TRUSTWORTHY] = trans('common.trustworthy');
        $arr[static::RATE_CLEAR_REQUIREMENT] = trans('common.clear_requirement');

        return $arr;
    }

    public static function getFreelancerRateCategoryLists($empty = false) {
        $arr = array();
        if ($arr === true) {
            $arr[] = 'Freelancer Rate Category';
        }
        $arr[static::RATE_SKILL] = trans('common.skill');
        $arr[static::RATE_QUALITY_OF_WORK] = trans('common.quality_of_work');
        $arr[static::RATE_AVAILABILITY] = trans('common.availability');
        $arr[static::RATE_ADHERENCE_TO_SCHEDULE] = trans('common.adherence_to_schedule');
        $arr[static::RATE_COMMUNICATION] = trans('common.communication');
        $arr[static::RATE_COOPERATION] = trans('common.cooperation');

        return $arr;
    }

    public function getYellowRateStar()
    {
        $result = "";
        $rate = ceil($this->total_score);
        $fullStar = $rate;
        $emptyStar = 5 - $rate;

        for ($i = 0; $i < $fullStar; $i++)
        {
            $result .= '<span class="fa star fa-star" style="padding-left:2px;padding-right:2px"></span>';
        }

        for ($i = 0; $i < $emptyStar; $i++)
        {
            $result .= '<span class="fa star fa-star-o" style="padding-left:2px;padding-right:2px"></span>';
        }

        return $result;
    }

}
