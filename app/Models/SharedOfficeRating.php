<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SharedOfficeRating extends Model
{
    protected $table = 'shared_office_rating';

    protected $fillable = [
        'id', 'office_id', 'user_id', 'rate', 'comment', 'helpful_count'
    ];
    protected $appends = [
        'avg'
    ];

    public function rater()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

    public function officeRating()
    {
        return $this->belongsTo(SharedOffice::class,'office_id','id');
    }

    public function getAvgAttribute()
    {
        return SharedOfficeRating::where('office_id','=',$this->office_id)->avg('rate');

    }
}
