<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportCoummunityTopic extends Model
{
    const ALREADY_REPORTED = 1;
    protected $fillable = [
      'comment','topic_name','discussion_name','discussion_id','report_to','report_by','discussion_id','replay_id'
    ];

    public function reportToUsername($to){
        $user = User::where('id','=',$to)->first();
        return $user->name;
    }

    public function reportByUsername($by){
        $user = User::where('id','=',$by)->first();
        return $user->name;
    }
}
