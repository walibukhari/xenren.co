<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ImageModel extends Model
{
		use SoftDeletes;
	
    protected $table = 'image_string';

    protected $fillable = [
        'id',
        'text',
        'user_time_tracker_id',
        'activity_level',
	      'hourly_rate'
    ];
	
		public function trackerDetail()
		{
			return $this->hasOne(TimeTrack::class, 'id', 'user_time_tracker_id');
		}
}
