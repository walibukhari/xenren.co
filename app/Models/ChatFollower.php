<?php

namespace App\Models;

use App\Models\BaseModels;
use App\Models\ChatRoom;
use App\Models\User;

class ChatFollower extends BaseModels
{
    //

    protected $table = 'chat_followers';

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function staff()
    {
        return $this->belongsTo('App\Models\Staff', 'staff_id', 'id');
    }

    public function room()
    {
        return $this->belongsTo('App\Models\ChatRoom', 'room_id', 'id');

    }
}
