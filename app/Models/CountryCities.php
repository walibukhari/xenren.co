<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CountryCities extends Model
{
    protected $table = 'country_cities';

    protected $fillable = [
        'id', 'name', 'lat', 'lon', 'state_id'
    ];

    public function state()
    {
        return $this->hasOne(States::class, 'id', 'state_id');
    }
}
