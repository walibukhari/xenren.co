<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PushNotificationKeywordSettings extends Model
{
    protected $table  = 'push_notification_keywords_settings';

    protected $fillable = [
        'push_notification_settings_id',
        'keyword_id',
    ];

    public function skills()
    {
        return $this->hasOne(Skill::class, 'id', 'keyword_id');
    }

    /**
     * @param $obj
     * @return mixed
     */
    public function createRecord($obj)
    {
        return $this->create([
            'push_notification_settings_id' => $obj['push_notification_settings_id'],
            'keyword_id' => $obj['keyword_id'],
        ]);
    }

    /**
     * @param $obj
     * @param $id
     * @return mixed
     */
    public function updateRecord($obj, $id)
    {
        return $this->where('id', '=', $id)->update([
            'push_notification_settings_id' => $obj['push_notification_settings_id'],
            'keyword_id' => $obj['keyword_id'],
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        return $this->where('id', '=', $id)->delete();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecordBySettingId($id)
    {
        return $this->where('push_notification_settings_id', '=', $id)->delete();
    }

    /**
     * @return mixed
     */
    public function getRecord()
    {
        return $this->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getSingleRecord($id)
    {
        return $this->where('id', '=', $id)->first();
    }
}
