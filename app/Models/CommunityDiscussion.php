<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Topic;

class CommunityDiscussion extends Model
{
    const NOT_NOTIFY_ME = 0;
    const NOTIFY_ME = 1;
    const READ_AS_FALSE = 0;
    const READ_AS_TRUE = 1;
    const MARK_AS_ALL_NEW = 0;
    const MARK_AS_ALL_READ = 3;
    const SUBSCRIBE = 4;
    const UN_SUBSCRIBE = 8;
    const SUBSCRIBE_RSS_FEED = 9;
    const BOOKMARK = 5;
    const UN_BOOKMARK = 7;
    const EMAIL_TO_FRIEND = 6;
    const STATUS_STOP = 0;
    const STATUS_START = 1;

    protected $fillable = [
        'name' , 'description' , 'file' , 'filename' , 'notify_me' , 'book_mark' , 'topic_id' , 'topic_type' , 'user_id' , 'read_as' ,'status'
    ];

    public function getPostTopicType($type){
        if($type == Topic::TOPIC_TYPE_DISCUSSION) {
            return 'Discussion';
        } else {
            return 'Xenren Offical';
        }
    }


    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

    public function readD(){
        return $this->hasOne(CommunityRead::class,'d_id','id');
    }

    public function getTopicName($topic){
        $topic = Topic::where('id','=',$topic)->first();
        return $topic->topic_name;
    }

    public function getTopicType($type){
        if($type == Topic::TOPIC_TYPE_OFFICAL) {
            return 'Xenren Offical';
        } else {
            return 'Community Discussion';
        }
    }

    public function subscribe(){
        return $this->hasOne(CommunitySubscribe::class,'discussion_id','id')->where('user_id','=',\Auth::user()->id);
    }

    public function replies(){
        return $this->hasOne(CommunityReply::class,'discuss_id','id');
    }

    public function topicReplies(){
        return $this->hasMany(CommunityReply::class,'discuss_id','id');
    }

    public function topicLikes(){
        return $this->hasMany(CommunityTopicLikes::class,'topic_id','id');
    }

    public function getLikesCount($likes){
       $count = [];
       foreach ($likes as $topics) {
            if($topics->likes > 0) {
                array_push($count,$topics->likes);
            }
        }
       return count($count);
    }

    public function topic(){
        return $this->hasOne(Topic::class,'id','topic_id');
    }
}
