<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SharedOfficeFinanceRevision extends Model
{
    protected $table = 'shared_office_finances_revision';

    public function financetable()
    {
        return $this->belongsTo('App\Models\SharedOfficeFinance', 'transaction_id', 'id');
    }
}
