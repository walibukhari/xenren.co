<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCoinAddress extends Model
{
    protected $table = 'user_coin_address';

    protected $fillable = ['user_id', 'address'];

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
