<?php

namespace App\Models;

use Input;

class PermissionGroupPermission extends BaseModels {
    protected $table = 'permission_groups_permissions';

    protected $fillable = [
        'permission_tag'
    ];

    protected $dates = ['deleted_at'];

    protected $rules = array(
        'permission_group_id' => 'required|exists:operator_groups,id',
        'permission_tag' => 'required|isPermissionTag',
    );

    public static function boot() {
        parent::boot();
    }

    public function permissionGroup() {
        return $this->belongsTo('App\Models\PermissionGroup', 'permission_group_id', 'id');
    }

    public function setPermissionTagAttribute($value) {
        $this->attributes['permission_tag'] = strtolower($value);
    }

    public static function getJsonPermission()
    {
        // check path if not exist
        $path = storage_path('app/json');
        if (!file_exists($path)) {
            mkdir($path);
        }

        // create file if not exist
        $file = $path . '/permissions.json';
        if (!file_exists($file)) {
            file_put_contents($file, json_encode(array()));
        }

        // get list permission
        return (array) json_decode(file_get_contents($file));
    }

    //All the permissions for backend should set at here
    public static function getPermissionsLists() {
        $listPermission = self::getJsonPermission();
        $arr = array();
        foreach ($listPermission as $permission) {
            $arr[$permission] = trans('permissions.' . $permission);
        }

        // $arr = array();

        // $arr['manage_site_setting'] = trans('permissions.manage_site_setting');
        // $arr['manage_staff'] = trans('permissions.manage_staff');
        // $arr['manage_user'] = trans('permissions.manage_user');
        // $arr['manage_permission_groups'] = trans('permissions.manage_permission_groups');
        // $arr['manage_category'] = trans('permissions.manage_category');
        // $arr['manage_bank'] = trans('permissions.manage_bank');
        // $arr['manage_raw_material'] = trans('permissions.manage_raw_material');
        // $arr['manage_official_project'] = trans('permissions.manage_official_project');
        // $arr['manage_skill_review'] = trans('permissions.manage_skill_review');
        // $arr['manage_job_position'] = trans('permissions.manage_job_position');
        // $arr['manage_projects'] = trans('permissions.manage_projects');
        // $arr['manage_sharedoffice'] = trans('permissions.manage_sharedoffice');
        // $arr['manage_bullet_ins'] = trans('permissions.manage_bullet_ins');
        // $arr['manage_sharedofficeproducts'] = trans('permissions.manage_sharedofficeproducts');
        // $arr['manage_sharedofficebarcodes'] = trans('permissions.manage_sharedofficebarcodes');
        // $arr['manage_badwords'] = trans('permissions.manage_badwords');
        // $arr['manage_payments'] = trans('permissions.manage_payments');
        // $arr['manage_sharedofficechat'] = trans('permissions.manage_payments');
        // $arr['manage_sharedoffice_working_sheet'] = trans('permissions.working_sheet');
        // $arr['manage_article'] = trans('permissions.manage_article');

        return $arr;
    }

    //All the permissions for backend should set at here
    public static function getPermissionsListsOwner() {
        $arr = array();

        $arr['manage_staff'] = trans('permissions.manage_staff');
        // $arr['manage_permission_groups'] = trans('permissions.manage_permission_groups');
        $arr['manage_sharedoffice_working_sheet'] = trans('permissions.manage_sharedoffice_working_sheet');
        $arr['manage_sharedoffice'] = trans('permissions.manage_sharedoffice');
        $arr['manage_sharedofficebarcodes'] = trans('permissions.manage_sharedofficebarcodes');
        $arr['manage_sharedofficeproducts'] = trans('permissions.manage_sharedofficeproducts');
        $arr['manage_sharedoffice_dashboard'] = trans('permissions.manage_sharedoffice_dashboard');
        $arr['manage_sharedoffice_checkin'] = trans('permissions.manage_sharedoffice_checkin');
        $arr['manage_sharedoffice_owner_request'] = trans('permissions.manage_sharedoffice_owner_request');
        $arr['manage_sharedoffice_images'] = trans('permissions.manage_sharedoffice_images');

        return $arr;
    }

    public static function isValidPermission($permission_tag) {
        $permission_tag = strtolower($permission_tag);

        if (array_key_exists($permission_tag, static::getPermissionsLists())) {
            return true;
        } else {
            return false;
        }
    }

}
