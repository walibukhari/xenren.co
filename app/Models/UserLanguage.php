<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLanguage extends Model
{
    //
    protected $appends = ['country_flag'];

    protected $fillable = [
      'user_id',
      'language_id',
    ];

    public function getCountryFlagAttribute()
    {

        $code = isset($this->language->country->code) ? $this->language->country->code : NULL;
        if($code){
            return \URL::to('/').'/images/Flags-Icon-Set/24x24/'.$code.'.png';
        } else {
            return '';
        }

    }

    public function language()
    {
        return $this->belongsTo('App\Models\CountryLanguage', 'language_id', 'id');
    }

    public function languageDetail()
    {
        return $this->belongsTo('App\Models\Language', 'language_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
