<?php

namespace App\Models;

use Input;

class UserSkillCommentSkills extends BaseModels {

    protected $table = 'user_skill_comment_skills';

    protected $fillable = [
        'user_id', 'comment_id', 'skill_id', 'score', 'action_type', 'experience',
    ];

    public static $rules = [
        'user_id' => 'required|exists:users,id',
        'comment_id' => 'required|exists:user_skill_comment,id',
        'skill_id' => 'required|exists:skill,id',
        'score' => 'min:0|max:5',
        'action_type' => 'required|in:0,1',//0 mean plus 1 mean deduct
        'experience' => 'isNumber',
    ];

    protected $dates = [];

    public static function boot() {
        parent::boot();
    }

    public function scopeGetRecords($query) {
        return $query;
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function comment() {
        return $this->belongsTo('App\Models\UserSkillComment', 'comment_id', 'id');
    }

    public function skill() {
        return $this->belongsTo('App\Models\Skill', 'skill_id', 'id');
    }

    public function scopeGetFilteredResults($query) {
        if (Input::has('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::has('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::has('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::has('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::has('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }
    }

}