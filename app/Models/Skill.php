<?php

namespace App\Models;

use Input;
use Illuminate\Database\Eloquent\SoftDeletes;

class Skill extends BaseModels {
    const TAG_COLOR_WHITE = 1;
    const TAG_COLOR_GOLD = 2;

    const SEARCH_TYPE_WHITE = 1;
    const SEARCH_TYPE_GOLD = 2;

    use SoftDeletes;

    protected $table = 'skill';

    protected static $skillLists = null;

    protected $fillable = [
        'name_cn', 'name_en', 'reference_link_en', 'reference_link_cn', 'description', 'tag_color'
    ];

    protected $dates = ['deleted_at'];

    public static function boot() {
        parent::boot();
    }

    public function scopeGetSkill($query) {
        return $query;
    }

    public function getName() {
        $field = 'name_' . app()->getLocale();

        return $this->$field;
    }

    public function getNames()
    {
        if( $this->reference_link_cn != "" ) {
            $name_cn = '<a href="' . $this->reference_link_cn . '" target="_blank" >' . $this->name_cn . '</a>';
        }else{
            $name_cn = $this->name_cn;
        }

        if( $this->reference_link_en != "" ) {
            $name_en = '<a href="' . $this->reference_link_en . '"  target="_blank" >' . $this->name_en . '</a>';
        }else{
            $name_en = $this->name_en;
        }

        return $name_cn . ' / '. $name_en;
    }

    public function getDescription()
    {
        $field = 'description_' . app()->getLocale();

        return $this->$field;
    }

    public function setNameCnAttribute($value) {
        $this->attributes['name_cn'] = trim($value);
    }

    public function setNameEnAttribute($value) {
        $this->attributes['name_en'] = trim($value);
    }

    public function explainStatus() {
        return $this->deleted_at == null ? trans('common.active') : trans('common.inactive');
    }

    public function scopeGetFilteredResults($query) {
        if (Input::has('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::has('filter_name') && Input::get('filter_name') != '') {
            $query->where('name_cn', 'like', '%' . Input::get('filter_name') . '%')
                ->orWhere('name_en', 'like', '%' . Input::get('filter_name') . '%');
        }

        if (Input::has('filter_status') && Input::get('filter_status') != '' && Input::get('filter_status') > 0) {
            if (Input::get('filter_status') == 1) {
                $query->whereNull('deleted_at');
            } else {
                $query->whereNotNull('deleted_at');
            }
        }

        if (Input::has('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::has('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::has('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::has('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }
    }

    public function getTagColor()
    {
        if( $this->tag_color == $this::TAG_COLOR_WHITE )
        {
            return "<div class='tag-white'></div>";
        }
        else if( $this->tag_color == $this::TAG_COLOR_GOLD )
        {
            return "<div class='tag-gold'></div>";
        }
    }

    public static function getSearchSkillData($searchType)
    {
        if( $searchType == 1 ) //SEARCH_TYPE_WHITE
        {
            $skills = Skill::where('tag_color', Skill::TAG_COLOR_WHITE)
                ->get();
        }
        else if(  $searchType == 2 ) //SEARCH_TYPE_GOLD
        {
            $skills = Skill::where('tag_color', Skill::TAG_COLOR_GOLD)
                ->get();
        }

        $arr = array();
        $nameField = 'name_' . app()->getLocale();
        $descriptionField = 'description_' . app()->getLocale();
        // foreach ($skills as $skill) {
        //     $item = array("id" => $skill->id, "name" => $skill->$nameField);
        //     array_push($arr, $item);
        // }

        foreach ($skills as $skill) {
            if( $skill->$descriptionField != '' )
            {
                $item = array("id" => $skill->id, "name" => $skill->$nameField . ' | ' . $skill->$descriptionField, "description" => $skill->$descriptionField);
                array_push($arr, $item);
            }
        }

        return json_encode($arr);
    }
}