<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectMilestones extends Model
{
    const STATUS_INPROGRESS = 1;
    const STATUS_CONFIRMED = 2;

    const STATUS_REJECTED = 3;
    const STATUS_REQUESTED = 4;
    const STATUS_DISPUTED = 5; //EMPLOYER REQUESTED THE MILESTONE

    const STATUS_REFUNDED = 6;  //FREELANCER REFUNDED TO EMPLOYER

    const STATUS_CANCEL_REQUEST_BY_EMPLOYER = 7;
	  const STATUS_FREELANCER_REJECTED_REFUND = 8;  //FREELANCER REJECTED REFUNDED TO EMPLOYER

    protected $tabel = 'project_milestones';

    protected $fillable = [
        'project_id',
        'applicant_id',
        'amount',
        'name',
        'description',
        'due_date',
        'auto_release'
    ];

		public function applicant()
		{
			return $this->hasOne(User::class, 'id', 'applicant_id');
		}

    public function project()
    {
        return $this->hasOne(Project::class, 'id', 'project_id');
    }

    public function milestoneStatus()
    {
        return $this->hasOne(ProjectMilestoneStauts::class, 'milestone_id', 'id');
    }

    public function activeMilestoneStatus()
    {
        return $this->hasOne(ProjectMilestoneStauts::class, 'milestone_id', 'id')->status(1);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'milestone_id', 'id');
    }

    public function transaction()
    {
        return $this->hasOne(Transaction::class, 'milestone_id', 'id');
    }

    public function createMilestone($request)
    {
        return $this
            ->create($request->all());
    }

    public function updateMilestone($request, $id)
    {
        $input = $request->all();
        $input = array_filter($input, 'strlen');
        return $this
            ->where('project_id', '=', $id)
            ->update($input);
    }

		public function filedDisputes()
		{
			return $this->hasOne(ProjectFileDisputes::class, 'milestone_id', 'id');
		}

    public function getProjectMilestoens($project_id, $applicantId=false)
    {
        $query =  $this
            ->with([
                'project',
                'milestoneStatus'
            ])
            ->whereHas('milestoneStatus' , function($q){
                $q->where('status', '=', 1);
            })
            ->where('project_id', '=', $project_id);

        $query->when($applicantId, function ($q)use($applicantId){
        	  $q->where('applicant_id', '=', $applicantId);
        });

        return $query->get();
    }

    public function getStatus($status){
		    if($status == self::STATUS_INPROGRESS) {
                return 'In Progress';
            } else if($status == self::STATUS_CONFIRMED) {
                return 'Confirmed';
            } else if($status == self::STATUS_REJECTED) {
                return 'Rejected';
            } else if($status == self::STATUS_REQUESTED) {
                return 'Requested';
            } else if($status == self::STATUS_REFUNDED) {
                return 'Refunded';
            } else if($status == self::STATUS_FREELANCER_REJECTED_REFUND) {
                return 'Freelancer Reject Refund';
            } else {
                return $status;
            }
    }

}
