<?php

namespace App\Models;

use App\Models\BaseModels;
use App\Models\User;

class ChatRoom extends BaseModels
{
    const TYPE_FEEDBACKS = 1;

    protected $table = 'chat_rooms';

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function feedback()
    {
        return $this->belongsTo('App\Models\Feedback', 'feedback_id', 'id');
    }
}
