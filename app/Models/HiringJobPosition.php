<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HiringJobPosition extends Model
{
    protected $fillable = [
        'job_positions'
    ];
}
