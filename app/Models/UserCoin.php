<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCoin extends Model
{
    protected $table = 'user_coin';

    protected $fillable = ['project_id', 'user_id', 'coin_type', 'amount'];

    public function project() {
        return $this->belongsTo('App\Models\Project', 'project_id', 'id');
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
