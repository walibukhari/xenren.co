<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectDisputesComment extends Model
{
		use SoftDeletes;
	
    protected $table = 'project_disputes_comments';
    
    protected $fillable = [
    	'project_dispute_id',
    	'user_id',
    	'comment',
    ];
}
