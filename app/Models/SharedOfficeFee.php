<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SharedOfficeFee extends Model
{
    protected $fillable = [
        'type',
        'office_id',
        'no_of_peoples',
        'hourly_price',
        'daily_price',
        'weekly_price',
        'monthly_price',
        'is_available',
    ];

    const HOT_DESK_TYPE = 1;
    const DEDICATED_DESK_TYPE = 2;
    const OFFICE_DESK_TYPE = 3;

    public function officeFee()
    {
        return $this->belongsTo(SharedOffice::class,'office_id','id');
    }

}
