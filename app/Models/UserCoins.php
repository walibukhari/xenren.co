<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCoins extends Model
{
    //''
	
	protected $table = 'user_coins';

    protected $fillable = ['coin_type_id','coin_type', 'receiver_id', 'admin_id','sender_id','number_of_coin'];

    public static function boot() {
        parent::boot();
    }
    
    public function users() {
        //return $this->belongsTo('App\Models\Users', 'user_id', 'id');
        return $this->belongsTo('App\Models\Users', 'receiver_id', 'id');
    }
}
