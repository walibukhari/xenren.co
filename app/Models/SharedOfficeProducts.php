<?php

namespace App\Models;
use Carbon\Carbon;
use Input;
use Illuminate\Database\Eloquent\Model;
use DB;

class SharedOfficeProducts extends Model
{
    // TODO change this
    const CATEGORY_HOT_DESK = 1;
    const CATEGORY_DEDICATED_DESK = 2;
    const CATEGORY_MEETING_ROOM = 3;
    const CATEGORY_VENDING_MACHINE = 4;
    const CATEGORY_COFFEE_MACHINE = 5;
    const CATEGORY_VR_ROOM = 6;
    const CATEGORY_DOOR = 7;
    const CATEGORY_PHONE_ROOM = 8;
    const CATEGORY_DINNING_ROOM= 9;
    const CATEGORY_TABLE = 10;
    const CATEGORY_WALL = 11;

    const STATUS_USING = 1;
    const STATUS_BOOKED = 2;
    const STATUS_EMPTY = 3;

    protected $fillable = [
	    'qr',
	    'category_id',
	    'office_id',
	    'x',
	    'y',
	    'number',
	    'status_id',
	    'month_price',
	    'time_price',
        'weekly_price',
        'hourly_price',
        'daily_price',
        'no_of_peoples',
        'availability_id',
        'type',
	    'remark',
//	    'start_date',
    ];

    protected $appends = [
      'product_type_name'
    ];

//    public function getStartDateAttribute()
//    {
//        return Carbon::parse($this->start_time)->toDateString();
//    }

    public function getProductImage($categoryId) {
        $getImage = SharedOfficeProductCategory::where('id','=',$categoryId)->first();
        return '/'.$getImage->image;
    }

    public function getProductName($categoryId) {
        $getImage = SharedOfficeProductCategory::where('id','=',$categoryId)->first();
        $getImage = explode('.',$getImage->name);
        return $getImage[1];
    }

    public function getStatusName($status){
        if($status == self::STATUS_USING) {
                return 'Using';
        } else if($status == self::STATUS_BOOKED) {
            return 'Booked';
        } else {
            return 'Empty';
        }
    }

    public function getProductTypeNameAttribute()
    {
        $name= '';
        if($this->category_id == self::CATEGORY_DEDICATED_DESK) {
            $name = 'DEDICATED DESK';
        }
        if($this->category_id == self::CATEGORY_HOT_DESK) {
            $name = 'HOT DESK';
        }
        if($this->category_id == self::CATEGORY_MEETING_ROOM) {
            $name = 'MEETING ROOM';
        }
        if($this->category_id == self::CATEGORY_VENDING_MACHINE) {
            $name = 'VENDING MACHINE';
        }
        if($this->category_id == self::CATEGORY_COFFEE_MACHINE) {
            $name = 'COFFEE MACHINE';
        }
        if($this->category_id == self::CATEGORY_VR_ROOM) {
            $name = 'VR ROOM';
        }
        if($this->category_id == self::CATEGORY_DOOR) {
            $name = 'DOOR';
        }
        if($this->category_id == self::CATEGORY_PHONE_ROOM) {
            $name = 'PHONE ROOM';
        }
        if($this->category_id == self::CATEGORY_DINNING_ROOM) {
            $name = 'DINNING ROOM';
        }
        if($this->category_id == self::CATEGORY_TABLE) {
            $name = 'TABLE';
        }
        if($this->category_id == self::CATEGORY_WALL) {
            $name = 'WALL';
        }
        return $name;
    }

    public function productstatus()
    {
        return $this->belongsTo('App\Models\SharedOfficeProductStatus', 'status_id', 'id');
    }

    public function office()
    {
        return $this->belongsTo('App\Models\SharedOffice', 'office_id', 'id');
    }

    public function productcategory()
    {
        return $this->belongsTo('App\Models\SharedOfficeProductCategory', 'category_id', 'id');
    }

    public function finance()
    {
        return $this->hasMany('App\Models\SharedOfficeFinance', 'product_id', 'id');
    }

    public function scopeGetFilteredResults($query) {

        if (Input::has('filter_id') && Input::get('filter_id') != '') {
            $query->where('id', '=', Input::get('filter_id'));
        }

        if (Input::has('filter_officeid') && Input::get('filter_officeid') != '') {
            $query->where('office_id', '=', Input::get('filter_officeid'));
        }

        if (Input::has('filter_categories') && Input::get('filter_categories') != '') {


            $query
                ->join('shared_office_product_categories', function($join)
                {
                    // ->select('shared_office_product_categories.name')
                    $join->on('shared_office_products.category_id', '=', 'shared_office_product_categories.id')
                        ->where('shared_office_product_categories.name', 'like', '%' . Input::get('filter_categories') . '%');
                });


        }

        /*
         SELECT * FROM `shared_office_products` inner JOIN shared_office_product_categories on shared_office_product_categories.id=shared_office_products.category_id where shared_office_product_categories.name like '%sea%'
         */


        /*
                if (Input::has('filter_categories') && Input::get('filter_categories') != '') {
                    $query->where('title_cn', 'like', '%' . Input::get('filter_categories') . '%')
                        ->orWhere('title_en', 'like', '%' . Input::get('filter_categories') . '%');
                }

                        if (Input::has('filter_created_after')) {
                            $query->where('created_at', '>=', Input::get('filter_created_after'));
                        }

                        if (Input::has('filter_created_before')) {
                            $query->where('created_at', '<=', Input::get('filter_created_before'));
                        }

                        if (Input::has('filter_updated_after')) {
                            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
                        }

                        if (Input::has('filter_updated_before')) {
                            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
                        }
                */
    }

    public function availability()
    {
        return $this->belongsTo(SharedOfficeProductAvailability::class, 'availability_id');
    }
}
