<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SharedOfficeReserveRequest extends Model
{
    protected $fillable = [
        'type',
        'office_id',
        'price',
        'duration_type',
        'office_manager_email',
        'name',
        'phone_number',
        'check_in_date',
        'check_out_date',
        'no_of_rooms',
        'no_of_peoples',
        'remarks',
    ];
}
