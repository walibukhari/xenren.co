<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table = 'languages';

    protected $fillable = [
        'alpha2', 'name', 'name_cn'
    ];

    public static function getLanguageLists($empty = false) {
        $arr = array();
        if ($arr === true) {
            $arr[] = 'Language';
        }

        if( $empty )
        {
            $arr[''] = trans('common.all');
        }

        $languages = Language::all();
        foreach( $languages as $language )
        {
            if( app()->getLocale() == "en" )
            {
                $arr[$language->id] = $language->name;
            }
            else
            {
                $arr[$language->id] = $language->name_cn;
            }

        }

        return $arr;
    }

    public function getName()
    {
        if( app()->getLocale() == "en" )
        {
            return $this->name;
        }
        else
        {
            return $this->name_cn;
        }
    }

    public static function getJsonLanguages()
    {
        $arr = array();
        $languages = Language::all();
        foreach( $languages as $language)
        {
            if( app()->getLocale() == "en" )
            {
                $languageName = $language->name;
            }
            else
            {
                $languageName = $language->name_cn;
            }

            $item = array("id"=>$language->id, "name"=>$languageName);
            array_push($arr, $item);
        }

        return json_encode($arr);
    }
}
