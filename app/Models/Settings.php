<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    const HOURLY_ON_OFF = 'hourly_on_off';
    const SET_RELEASE_TIME = 'release_time';
    const SET_RELEASE_TIME_UNVERIFIED = 'release_time_un_verified';

    protected $fillable = [
        'key',
        'value'
    ];
}
