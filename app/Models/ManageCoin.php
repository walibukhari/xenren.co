<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ManageCoin extends Model
{
    //''
	
	protected $table = 'coins';

    protected $fillable = ['coins','coins_type', 'per_coin_amount', 'remark'];

    public static function boot() {
        parent::boot();
    }
    /*
    public function user() {
        return $this->belongsTo('App\Models\Users', 'user_id', 'id');
    }*/
}
