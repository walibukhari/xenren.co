<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TwilioCode extends Model
{
    protected $fillable = [
        'phone_number' , 'code' , 'seat_id' , 'office_id'
    ];
}
