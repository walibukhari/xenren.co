<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorldCountriesLocale extends Model
{
	protected $table = 'world_countries_locale';
}
