<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorldDivisionsLocale extends Model
{
	protected $table = 'world_divisions_locale';
}
