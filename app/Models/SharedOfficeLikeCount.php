<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SharedOfficeLikeCount extends Model
{
    protected $table = 'shared_office_like_counts';
    
    protected $fillable = [
	    'user_id',
	    'shared_office_id',
    ];
}
