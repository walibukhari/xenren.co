<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommunityTopicLikes extends Model
{
    protected $fillable = [
      'topic_id','likes','user_id'
    ];
}
