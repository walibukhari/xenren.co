<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ThumbdownJob extends Model
{
    //
    protected $table = 'thumbdown_jobs';

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function project() {
        return $this->belongsTo('App\Models\Project', 'project_id', 'id');
    }
}
