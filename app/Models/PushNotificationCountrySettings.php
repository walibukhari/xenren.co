<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PushNotificationCountrySettings extends Model
{
    protected $table  = 'push_notification_country_settings';

    protected $fillable = [
        'push_notification_settings_id',
        'country_id',
    ];

    public function countryName()
    {
        return $this->hasOne(WorldCountries::class, 'id', 'country_id');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecordBySettingId($id)
    {
        return $this->where('push_notification_settings_id', '=', $id)->delete();
    }

    /**
     * @param $obj
     * @return mixed
     */
    public function createRecord($obj)
    {
        return $this->create([
            'push_notification_settings_id' => $obj['push_notification_settings_id'],
            'country_id' => $obj['country_id'],
        ]);
    }

    /**
     * @param $obj
     * @param $id
     * @return mixed
     */
    public function updateRecord($obj, $id)
    {
        return $this->where('id', '=', $id)->update([
            'push_notification_settings_id' => $obj['push_notification_settings_id'],
            'country_id' => $obj['country_id'],
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        return $this->where('id', '=', $id)->delete();
    }

    /**
     * @return mixed
     */
    public function getRecord()
    {
        return $this->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getSingleRecord($id)
    {
        return $this->where('id', '=', $id)->first();
    }
}
