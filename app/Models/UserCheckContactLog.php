<?php

namespace App\Models;

use Input;
use App\Models\Skill;

class UserCheckContactLog extends BaseModels {

    public $rules = [
        'project_id' => 'required|exists:projects,id',
        'skill_id' => 'required|exists:skill,id',
    ];
    protected $table = 'user_check_contact_logs';

    protected $fillable = [
        'ip_address', 'user_id', 'target_user_id'
    ];

    protected $dates = [];

    public static function boot() {
        parent::boot();
    }

    public function scopeGetProjectSkill($query) {
        return $query;
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function targetUser() {
        return $this->belongsTo('App\Models\User', 'target_user_id', 'id');
    }

}