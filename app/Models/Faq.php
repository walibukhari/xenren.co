<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Input;
use Kalnoy\Nestedset\NodeTrait;

class Faq extends BaseModels
{
    //
    use NodeTrait;
    protected static $faqLists = null;
    protected $table = 'faqs';

    const CO_WORK_FAQ = 1;
    const FREELANCE_FAQ = 2;
    const EMPLOYEE_FAQ = 3;
    const CO_WORK_OWNER_FAQ = 4;

    /*
        public function parent()
        {
            return $this->belongsTo(Faq::class, 'parent_id')->where('parent_id', 0)->with('parent');
        }

        public function children()
        {
            return $this->hasMany(Faq::class, 'parent_id')->with('children');
        }
    */

    public function children()
    {
        return $this->hasMany(Faq::class, 'parent_id')->with('children');
    }

    public function getTypeName($faq) {
        return $faq;
    }

    public function scopeGetFaq($query)
    {
        return $query;
    }

    public function getTitle()
    {
        $field = 'title_' . app()->getLocale();

        return $this->$field;
    }

    public function getText()
    {
        $field = 'text_' . app()->getLocale();

        return substr(strip_tags($this->$field), 0, 350);
    }

    public function getContent()
    {
        $field = 'text_' . app()->getLocale();

        return $this->$field;
    }

    public function getParent()
    {
        $field = 'title_' . app()->getLocale();

        return ($this->parent_id != 0) ? Faq::find($this->parent_id)->$field : null;
    }

    public function scopeGetFilteredResults($query)
    {
        if (Input::has('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::has('filter_title') && Input::get('filter_title') != '') {
            $query->where('title_cn', 'like', '%' . Input::get('title_name') . '%')
                ->orWhere('title_en', 'like', '%' . Input::get('title_name') . '%');
        }

        if (Input::has('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::has('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::has('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::has('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }
    }
}
