<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shfacilitie extends Model
{
    public function sharedoffices()
    {
        return $this->belongsToMany('App\Models\SharedOffice');
    }
}
