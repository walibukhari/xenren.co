<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OwnerProgress extends Model
{
    protected $fillable = [
        'staff_id','manage_bookings' , 'owner_profile' , 'office_check_in' , 'office_info','confirm_bookings'
    ];

    public static function setProgress($staff,$permission){
        $check = self::where('staff_id','=',$staff->id)->first();
        if($permission == 'manage_sharedoffice_checkin') {
            if(is_null($check)) {
                self::create([
                    'staff_id' => $staff->id,
                    'office_check_in' => 1
                ]);
            } else {
                self::where('staff_id','=',$staff->id)->increment('office_check_in', 1);
            }
        }
        if($permission == 'manager_confirm_bookings') {
            if(is_null($check)) {
                self::create([
                    'staff_id' => $staff->id,
                    'confirm_bookings' => 1
                ]);
            } else {
                self::where('staff_id','=',$staff->id)->increment('confirm_bookings',1);
            }
        }
        if($permission == 'manage_sharedoffice_notifications') {
            if(is_null($check)) {
                self::create([
                    'staff_id' => $staff->id,
                    'manage_bookings' => 1
                ]);
            } else {
                self::where('staff_id','=',$staff->id)->increment('manage_bookings', 1);
            }
        }
        if($permission == 'manage_sharedoffice') {
            if(is_null($check)) {
                self::create([
                    'staff_id' => $staff->id,
                    'office_info' => 1
                ]);
            } else {
                self::where('staff_id','=',$staff->id)->increment('office_info', 1);
            }
        }
//        if($permission == 'manage_sharedofficebarcodes') {
//        }
//        if($permission == 'manage_sharedoffice_owner_request') {
//        }
//        if($permission == 'manage_sharedofficeproducts') {
//        }
//        if($permission == 'manage_sharedoffice_invoices') {
//        }
//        if($permission == 'manage_staff') {
//        }
//        if($permission == 'manage_sharedoffice_setting') {
//        }
    }
}
