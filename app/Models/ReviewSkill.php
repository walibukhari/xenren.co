<?php

namespace App\Models;

use Input;
use App\Models\Skill;

class ReviewSkill extends BaseModels {

    public $rules = [
        'review_id' => 'required|exists:reviews,id',
        'skill_id' => 'required|exists:skill,id',
    ];
    protected $table = 'review_skills';
    protected $fillable = [
        'review_id', 'skill_id', ''
    ];
    protected $dates = [];

    public static function boot() {
        parent::boot();
    }

    public function review() {
        return $this->belongsTo('App\Models\Review', 'review_id', 'id');
    }

    public function skill() {
        return $this->belongsTo('App\Models\Skill', 'skill_id', 'id');
    }

}