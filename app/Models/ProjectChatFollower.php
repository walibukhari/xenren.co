<?php

namespace App\Models;

class ProjectChatFollower extends BaseModels {
    const THUMB_STATUS_THUMB_UP = 1;
    const THUMB_STATUS_UNSET = 2;
    const THUMB_STATUS_THUMB_DOWN = 3;

    protected $table = 'project_chat_followers';

    protected $fillable = [
        'project_chat_room_id', 'user_id', 'is_kick', 'thumb_status'
    ];

    protected $dates = [];

    public static function boot() {
        parent::boot();
    }

    public function scopeGetProjectChatFollower($query) {
        return $query;
    }

    public function projectChatRoom() {
        return $this->belongsTo('App\Models\ProjectChatRoom', 'project_chat_room_id', 'id');
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function getQuotePrice($projectId)
    {
        $userId = $this->user->id;
        $projectApplicant = ProjectApplicant::where('user_id', $userId)
            ->where('project_id', $projectId)
            ->first();
        return $projectApplicant->getQuotePrice();
    }
}