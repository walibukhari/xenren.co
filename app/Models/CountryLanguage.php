<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class CountryLanguage extends Model
{
    //

    public function country()
    {
        return $this->belongsTo(WorldCountries::class, 'country_id', 'id');
    }

    public function language()
    {
        return $this->belongsTo(Language::class, 'language_id', 'id');
    }

    public function getAttributeLangname()
    {
        return $this->language->name;
    }

    public function getAttributeCountry()
    {
        return $this->country->name;
    }

    public function scopeGetFilteredResults($query) {
        if (Input::has('filter_id') && Input::get('filter_id') != '') {
            $query->where('id', '=', Input::get('filter_id'));
        }

        if (Input::has('filter_country') && Input::get('filter_country') != '') {
            $query->where('country_id', '=', Input::get('filter_country'));
        }

        if (Input::has('filter_language') && Input::get('filter_language') != '') {
            $query->where('language_id', '=', Input::get('filter_language'));
        }

    }

    public function translateCountry() {
        if( app()->getLocale() == "en" )
        {
            return $this->country->name;
        }
        else
        {
            return $this->country->name_cn;
        }
    }

    public function translateLanguage() {
        if( app()->getLocale() == "en" )
        {
            return $this->language->name;
        }
        else
        {
            return $this->language->name_cn;
        }
    }

    public static function getCountryLanguages() {
        $languages = array();
        foreach (CountryLanguage::all() as $lang) {
            if (app()->getlocale() == 'cn') {
                $name = (($lang->language->name_cn != null) || !empty($lang->language->name_cn)) ? $lang->language->name_cn : $lang->language->name;
            } else {
                $name = $lang->language->name;

            }
            $language = array(
                'id' => $lang->id,
                'name' => $name,
                'code' => isset($lang->country->code) ? $lang->country->code : 'cn',
            );
            array_push($languages, $language);
        }

        return $languages;
    }

}
