<?php

namespace App\Models;

use Input;

class UserIdentity extends BaseModels {

	const STATUS_PENDING = 0;
	const STATUS_APPROVED = 1;
	const STATUS_REJECTED = 2;
	const STATUS_NEW = 3;

    const GENDER_MALE = 0;
    const GENDER_FEMALE = 1;

    protected $table = 'user_identity';

    protected $fillable = [
        'id_image',
        'hold_id_image'
    ];

		protected $appends = [
			'status_text',
			'gender_text'
		];

    public static function boot() {
        parent::boot();
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function country() {
        return $this->belongsTo('App\Models\WorldCountries', 'country_id', 'id');
    }
		/**
		 * get user's gender
		 */
		function getGenderTextAttribute() {
			if ( $this->gender === $this::GENDER_MALE ) {
				return trans('member.male');
			} else if ($this->gender === $this::GENDER_FEMALE ) {
				return trans('member.female');
			} else {
				return "Invalid gender";
			}
		}

		public function getStatusTextAttribute()
		{
			if($this->status == 0) {
				return 'Pending';
			}
			if($this->status == 1) {
				return 'Approved';
			}
			if($this->status == 2) {
				return 'Rejected';
			}
			return $this->status;
		}


	function getOwnerEmail() {
		return $this->user->email;
	}

	public function scopeGetFilteredResults($query) {
        if (Input::has('filter_email') && Input::get('filter_email') != '') {
            $query
            	->join('users', function($join)
		        {
		            $join->on('users.id', '=', 'user_identity.user_id')
		                 ->where('users.email', 'like', '%' . Input::get('filter_email') . '%');
		        });
        }
    }

	public function getIdImageAttribute($val) {
		if(!is_null($val) &&  $val != '') {
			return asset($val);
		} else {
			return null;
		}
	}

	public function getIdImage() {
		if (!is_null($this->id_image) && $this->id_image != '') {
			return asset($this->id_image);
		} else {
			return null;
		}
	}

	public function setIdImageAttribute($file)
	{
		if ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
			$img = \Image::make($file);
			$mime = $img->mime();
			$ext = convertMimeToExt($mime);

			$path = 'uploads/user/' . $this->user_id . '/identity/';
			$filename = generateRandomUniqueName();

			touchFolder($path);

			$img = $img->save($path . $filename . $ext);

			$this->attributes['id_image'] = $path . $filename . $ext;
		}
	}

    public function getHoldIdImageAttribute($val) {
        if (is_null($val) || $val == '') {
            return null;
        } else {
            return asset($val);
        }
    }

    public function getHoldIdImage() {
        if ($this->hold_id_image != null && $this->hold_id_image != '') {
            return asset($this->hold_id_image);
        } else {
            return null;
        }
    }

    public function setHoldIdImageAttribute($file)
    {
        if ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
            $img = \Image::make($file);
            $mime = $img->mime();
            $ext = convertMimeToExt($mime);

            $path = 'uploads/user/' . $this->user_id . '/identity/';
            $filename = generateRandomUniqueName();

            touchFolder($path);

            $img = $img->save($path . $filename . $ext);

            $this->attributes['hold_id_image'] = $path . $filename . $ext;
        }
    }

	public function isNullStatus() {
		return is_null($this->status);
	}

	public function isZero() {
		return ($this->status == 0);
	}

	public function isPending() {
		return $this->status == static::STATUS_PENDING && $this->status !== null ? true : false;
	}

	public function isApproved() {
		return $this->status == static::STATUS_APPROVED && $this->status !== null ? true : false;
	}

	public function isRejected() {
		return $this->status == static::STATUS_REJECTED && $this->status !== null ? true : false;
	}
}
