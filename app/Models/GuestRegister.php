<?php

namespace App\Models;

use Input;
use App\Models\Skill;

class GuestRegister extends BaseModels {

    public $rules = [
        'email' => 'required',
        'code' => 'required',
    ];

    protected $table = 'guest_registers';

    protected $fillable = [
        'email', 'code', 'real_name'
    ];

    protected $dates = [];

    public function getSkillsAttributes($val)
    {
        return explode(',',$val);
    }


}