<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommunitySubscribe extends Model
{
    protected $fillable = [
        'user_id','topic_id','discussion_id','subscribe'
    ];
}
