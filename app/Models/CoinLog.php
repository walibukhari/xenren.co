<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Staff;

class CoinLog extends BaseModels
{

    protected $table = 'coin_logs';

    protected $fillable = ['user_id','amount', 'current_balance', 'remark'];

    public static function boot() {
        parent::boot();
    }

    public function user() {
        return $this->belongsTo('App\Models\Users', 'user_id', 'id');
    }

}
