<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectFileDisputes extends Model
{
    protected $table = 'project_disputes';

    protected $fillable = [
        'project_id',
        'milestone_id',
        'transaction_id',
        'status',
        'admin_comment',
        'created_by',
    ];

    public function getAttachmentsAttribute($v) {
        $record = collect(explode(',', $v));
	      $record = $record->map(function($v){
            return 	asset('/disputes/').'/'.$v;
        });
	      return $record;
    }
    
    protected $appends = [
    	'status_name'
    ];
    
    const STATUS_FILE_DISPUTE_IN_PROGRESS = 0;
    const STATUS_FILE_DISPUTE_IN_APPROVED = 1;
    const STATUS_FILE_DISPUTE_IN_REJECTED = 2;
		const STATUS_CANCELED_BY_FREELANCER = 5;
	
	const TYPE_REPORTED_BY_FREELANCERS = 1;
    const TYPE_REPORTED_BY_EMPLOYERS = 2;

		public function getStatusNameAttribute()
		{
			if($this->status == self::STATUS_FILE_DISPUTE_IN_PROGRESS) {
				return '<span style="color: black" >In Progress<span>';
			}
			if($this->status == self::STATUS_FILE_DISPUTE_IN_APPROVED) {
				return '<span style="color: darkgreen">Approved</span>';
			}
			if($this->status == self::STATUS_FILE_DISPUTE_IN_REJECTED) {
				return '<span style="color: darkred">Rejected</span>';
			}
		}
    
    public function transaction()
    {
        return $this->hasOne(Transaction::class, 'id','transaction_id');
    }
    
    public function milestones()
    {
        return $this->hasOne(ProjectMilestones::class, 'id','milestone_id');
    }
    
    public function project()
    {
        return $this->hasOne(Project::class, 'id','project_id');
    }
    
    /**
     * Because the transaction dispute will be created only be freelancer, then created_by will be
     * the freelancer working on it.
     * */
    public function applicant()
    {
        return $this->hasOne(User::class, 'id','created_by');
    }
    
    public function applicantComment()
    {
        return $this->hasMany(ProjectDisputesComment::class, 'project_dispute_id','id');
    }
	
		public function comments()
		{
			return $this->hasMany(ProjectDisputesComment::class, 'project_dispute_id', 'id');
		}
	
		public function userAttachments()
		{
			return $this->hasMany(ProjectDisputesAttachments::class, 'project_dispute_id', 'id');
		}
}
