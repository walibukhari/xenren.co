<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorldDivisions extends Model
{
	protected $table = 'world_divisions';
	
	public function country()
	{
		return $this->hasOne(WorldCountries::class, 'id', 'country_id');
	}
}
