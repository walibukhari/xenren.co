<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;

class ProjectFile extends Model
{
    protected $table = 'project_files';

    public function project() {
        return $this->belongsTo('App\Models\Project', 'project_id', 'id');
    }

    public function getFile() {
        if ($this->file != null && $this->file != '') {
            return asset($this->file);
        } else {
            return null;
        }
    }

//    public function setFileAttribute($file)
//    {
//        $allowed_ext = ['png', 'jpg', 'jpeg', 'bmp', 'gif', 'pdf', 'xls', 'xlsx', 'mp3', 'mp4', 'docs', 'docx'];
//        if ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
//            $ext = $file->getClientOriginalExtension(); // getting extension
//            if (in_array($ext, $allowed_ext)) {
//                $filename = generateRandomUniqueName();
//
//                $path = 'uploads/project/'  . $this->project->id . '/';
//
//                touchFolder($path);
//
//                $file->move($path, $filename . '.' . $ext);
//
//                $this->attributes['file'] = $path . $filename . '.' . $ext;
//            } else {
//                throw new \Exception(trans('common.file_format_not_allow') . implode(', ', $allowed_ext));
//            }
//        }
//    }

    public function setFileAttribute($filename)
    {
        $ext = substr(strrchr($filename,'.'),1);
        $newFilename = generateRandomUniqueName();
        $path = 'uploads/project/'  . $this->project->id . '/';
        touchFolder($path);
        $srcPath = Config::get('upload-files.location') .$filename;
        $destPath = $path . $newFilename . '.' . $ext;
        File::isDirectory($path) or File::makeDirectory($path, 777, true, true);
        $r = File::copy($srcPath, public_path().'/'.$destPath);
        \Log::info($r);
        $this->attributes['file'] = $destPath;
    }

    public function getFileTypeImage()
    {
        $result = "";

        if( in_array( substr(strrchr($this->file,'.'),1), ['jpg', 'jpeg']) )
        {
            $result = "/images/jpg.png";
        }
        else if( in_array( substr(strrchr($this->file,'.'),1), ['gif']) )
        {
            $result = "/images/gif.png";
        }
        else if( in_array( substr(strrchr($this->file,'.'),1), ['png']) )
        {
            $result = "/images/png.png";
        }
        else if( in_array( substr(strrchr($this->file,'.'),1), ['bmp']) )
        {
            $result = "/images/bmp.png";
        }
        else if( in_array( substr(strrchr($this->file,'.'),1), ['doc', 'docx']) )
        {
            $result = "/images/doc.png";
        }
        else if ( in_array( substr(strrchr($this->file,'.'),1), ['pdf']) )
        {
            $result = "/images/pdf.png";
        }
        else if ( in_array( substr(strrchr($this->file,'.'),1), ['xls', 'xlsx']) )
        {
            $result = "/images/xls.png";
        }
        else if ( in_array( substr(strrchr($this->file,'.'),1), ['mp3']) )
        {
            $result = "/images/mp3.png";
        }
        else if ( in_array( substr(strrchr($this->file,'.'),1), ['mp4']) )
        {
            $result = "/images/mp4.png";
        }

        return $result;
    }

    public function getShortFilename()
    {
        $result = "";
        $fullFilename = basename($this->file);

        if( strlen($fullFilename) >= 5 )
        {
            $partOfFile = substr($fullFilename, 0, 5);

            //get extension
            $array = explode('.', $this->file);
            $extension = end($array);

            $result = $partOfFile . '...' . '.' . $extension;
        }
        else
        {
            $result = $fullFilename;
        }

        return $result;
    }
}
