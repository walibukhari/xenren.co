<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SharedOfficeImages extends Model
{
    const TYPE_IMAGE = 'image';
    const TYPE_VIDEO = 'video';
    protected $fillable = [
        'office_id',
        'image',
        'pic_size',
        'priority_images',
        'video'
    ];

    protected $appends = [
        'compressed_image'
    ];

    public function office()
    {
        return $this->belongsTo('App\Models\SharedOffice', 'office_id', 'id');
    }

    public function getImageAttribute($val)
    {
        return asset($val);
    }

    public function getCompressedImageAttribute($val)
    {
        return str_replace('https://www.xenren.co/uploads/sharedoffice/', '', asset('compresses/sharedOffice/'.$this->image));
    }
}
