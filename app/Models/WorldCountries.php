<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Middleware\SharedOffice;

class WorldCountries extends Model
{
    protected $table = 'world_countries';

    protected $fillable = [
        'continent_id',
        'name',
        'full_name',
        'capital',
        'code',
        'code_alpha3',
        'emoji',
        'has_division',
        'currency_code',
        'currency_name',
        'tld',
        'callingcode',
    ];

    public $timestamps = false;

    public function getCodeAttribute($val)
    {
        return strtoupper($val);
    }

    public function locale()
    {
        return $this->hasOne(WorldCountriesLocale::class, 'country_id', 'id');
    }

    public function cities()
    {
        return $this->hasMany(WorldCities::class, 'country_id', 'id');
    }

    public function continent()
    {
        return $this->hasOne(WorldContinents::class, 'id', 'continent_id');
    }

    public static function getCountryLists()
    {
        $arr = self::select(['id', 'name'])->get();
        return $arr;
    }

    public function getName()
    {
        return $this->name;
    }

    public function sharedoffices()
    {
        return $this->hasOne(SharedOffice::class, 'state_id');
    }
}
