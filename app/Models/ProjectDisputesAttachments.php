<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectDisputesAttachments extends Model
{
	
	use SoftDeletes;
	
	protected $table = 'project_disputes_attachments';
	
	protected $fillable = [
		'project_dispute_id',
		'user_id',
		'attachment'
	];
}
