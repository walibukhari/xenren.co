<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coins extends Model
{
    const UPLOAD_FOLDER = 'uploads/coins';
    protected $table = 'coins';

    protected $fillable = ['name_en','name_cn', 'contract_address', 'symbol', 'img', 'decimal'];

    public static function getAllCoinFormated()
    {
    	$coinList = self::all();
        $name = 'name_'.\App::getLocale();
        $coins = [];
        foreach ($coinList as $coin) {
            $coins[$coin->id]['name'] = $coin->$name;
            $coins[$coin->id]['contract'] = $coin->contract_address;
            $coins[$coin->id]['symbol'] = $coin->symbol;
            $coins[$coin->id]['img'] = $coin->img;
            $coins[$coin->id]['decimal'] = $coin->decimal;
        }

        return $coins;
    }

    public function getImgAttribute($value)
    {
    	return !empty($value) ? asset(self::UPLOAD_FOLDER).'/'.$value : null;
    }
}
