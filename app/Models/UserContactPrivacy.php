<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContactPrivacy extends Model
{
    protected $fillable = [
        'user_id','qq','weChat','skype','phone','line',
    ];
}
