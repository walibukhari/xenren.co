<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class Feedback extends Model
{
    const PRIORITY_LOW = 1;
    const PRIORITY_MEDIUM = 2;
    const PRIORITY_HIGH = 3;

    const STATUS_PENDING = 1;
    const STATUS_RUNNING = 2;
    const STATUS_RESOLVED = 3;
    const STATUS_WAIT_USER_REPLY = 4;
    const STATUS_WAIT_ADMIN_REPLY = 5;

    protected $table = 'feedbacks';

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function files(){
        return $this->hasMany('App\Models\FeedbackFile', 'feedback_id', 'id');
    }

    public static function getPriorityLists($empty = false) {
        $arr = array();
        if ($arr === true) {
            $arr[] = 'Priority';
        }

        if( $empty )
        {
            $arr[''] = trans('common.all');
        }
        $arr[static::PRIORITY_LOW] = trans('common.low');
        $arr[static::PRIORITY_MEDIUM] = trans('common.medium');
        $arr[static::PRIORITY_HIGH] = trans('common.high');

        return $arr;
    }

    public static function getStatusLists($empty = false) {
        $arr = array();
        if ($arr === true) {
            $arr[] = 'Status';
        }

        if( $empty )
        {
            $arr[''] = trans('common.all');
        }
        $arr[static::STATUS_PENDING] = trans('common.pending');
        $arr[static::STATUS_RUNNING] = trans('common.running');
        $arr[static::STATUS_RESOLVED] = trans('common.resolved');
        $arr[static::STATUS_WAIT_USER_REPLY] = trans('common.wait_user_reply');
        $arr[static::STATUS_WAIT_ADMIN_REPLY] = trans('common.wait_admin_reply');

        return $arr;
    }

    public function translateStatus() {
        switch ($this->status) {
            case static::STATUS_PENDING: return trans('common.pending'); break;
            case static::STATUS_RUNNING: return trans('common.running'); break;
            case static::STATUS_RESOLVED: return trans('common.resolved'); break;
            case static::STATUS_WAIT_USER_REPLY: return trans('common.wait_user_reply'); break;
            case static::STATUS_WAIT_ADMIN_REPLY: return trans('common.wait_admin_reply'); break;
            default: return '-'; break;
        }
    }

    public function translatePriority() {
        switch ($this->priority) {
            case static::PRIORITY_LOW: return trans('common.low'); break;
            case static::PRIORITY_MEDIUM: return trans('common.medium'); break;
            case static::PRIORITY_HIGH: return trans('common.high'); break;
            default: return '-'; break;
        }
    }

    public function scopeGetFilteredResults($query) {
        if (Input::has('filter_id') && Input::get('filter_id') != '') {
            $query->where('id', '=', Input::get('filter_id'));
        }

        if (Input::has('filter_priority') && Input::get('filter_priority') != '') {
            $query->where('priority', '=', Input::get('filter_priority'));
        }

        if (Input::has('filter_title') && Input::get('filter_title') != '') {
            $query->where('title', 'like', '%' . Input::get('filter_title') . '%');
        }

        if (Input::has('filter_status') && Input::get('filter_status') != '') {
            $query->where('status', '=', Input::get('filter_status'));
        }

        if (Input::has('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::has('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }
    }

}
