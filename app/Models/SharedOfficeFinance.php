<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SharedOfficeFinance extends Model
{
    protected $fillable = [
        'product_id',
        'office_id',
        'user',
        'cost',
        'start_time',
        'user_id',
        'x',
        'y',
        'type',
        'expected_end_time',
        'scan_type'
    ];

    const TYPE_NORMAL = 0;
    const TYPE_OTHER_BAR_CODES = 1;

    const SCAN_TYPE_HOURLY = 1;
    const SCAN_TYPE_DAILY = 2;
    const SCAN_TYPE_WEEKLY = 3;
    const SCAN_TYPE_MONTHLY = 4;

    public function productfinance()
    {
        return $this->belongsTo('App\Models\SharedOfficeProducts', 'product_id', 'id');
    }

    public function product()
    {
        return $this->hasOne(SharedOfficeProducts::class, 'number', 'product_id');
    }

    public function financerevision()
    {
        return $this->hasMany('App\Models\SharedOfficeFinanceRevision');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function userStatus()
    {
        return $this->hasOne(UserStatus::class, 'user_id', 'user_id');
    }

    public function office()
    {
        return $this->hasOne(SharedOffice::class, 'id', 'office_id');
    }
}
