<?php

namespace App\Models;

use App\Constants;
use Carbon\Carbon;
use Input;

class OfficialProject extends BaseModels {

    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;

    const DESIGN_TYPE_BANNER_WITHOUT_ICON_TOGETHER = 1;
    const DESIGN_TYPE_BANNER_WITH_ICON_TOGETHER = 2;

    const funding_stage_seed = 1;
    const funding_stage_angle = 2;
    const funding_stage_pre_a = 3;
    const funding_stage_a = 4;
    const funding_stage_b = 5;
    const funding_stage_c = 6;

    public static $rules = [
//        'lang' => 'required|in:en,cn',
        'title' => 'required|min:1|max:255',
        'description' => '',
        'recruit_start' => 'required|date',
        'recruit_end' => 'required|date|after:recruit_start',
        'completion' => 'required',
        'location_address' => '',
        'location_lat' => '',
        'location_lng' => '',
        'budget_switch' => 'required|in:0,1',
        'budget_from' => 'isFund',
        'budget_to' => 'isFund',
        'stock_total' => '',
        'stock_share' => '',
        'stock_description' => '',
        'funding_stage' => 'required|in:1,2,3,4,5,6',
        'contract_file' => 'mimes:jpeg,jpg,png,excel,msword',
        'icon' => 'mimes:jpeg,jpg,png',
        'banner' => 'mimes:jpeg,jpg,png',
        'design_type' => 'required',
        'out_cover' => 'mimes:jpeg,jpg,png',
        'in_cover' => 'mimes:jpeg,jpg,png',
        'project_level' => 'required|isProjectLevel',
        'project_type' => 'required|isProjectType',
        'share_has_reward' => 'required|in:0,1',
        'share_reward' => 'isFund',
        'reward' => 'isFund',
        'status' => 'required|in:0,1',
    ];
    protected static $projectLists = null;
    protected $table = 'official_projects';
    protected $fillable = [
        'project_id', 'lang', 'title', 'description',
        'recruit_start', 'recruit_end', 'completion',
        'location_address', 'city', 'location_lat', 'location_lng',
        'budget_switch', 'budget_from', 'budget_to',
        'stock_total', 'stock_share', 'stock_description', 'funding_stage',
        'contract_file', 'icon', 'banner', 'design_type', 'out_cover', 'in_cover',
        'project_level', 'project_type',
        'share_has_reward', 'share_reward', 'reward',
        'status',
    ];
    protected $dates = [];

    public static function boot() {
        parent::boot();
    }

		public function getSlugByTitle()
		{
			return strtolower(str_replace(' ', '-', $this->title));
		}

    public function scopeGetRecords($query) {
        return $query;
    }

    public function setContractFileAttribute($file) {
        if ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
            $mime = $file->getClientOriginalExtension();

            $now = new \Carbon\Carbon();
            $path = 'officialproject/contract/' . $now->format('Y/m/');
            $filename = generateRandomUniqueName();

            touchFolder($path);

            $file = $file->move($path, $filename . '.' . $mime);

            $this->attributes['contract_file'] = $path . $filename . '.' . $mime;
        }
    }

    public function setIconAttribute($file) {
        if ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
            $mime = $file->getClientOriginalExtension();

            $now = new \Carbon\Carbon();
            $path = 'officialproject/' . $now->format('Y/m/');
            $filename = generateRandomUniqueName();

            touchFolder($path);
            $file->move($path, $filename . '.' . $mime);
            $this->attributes['icon'] = $path . $filename . '.' . $mime;
        }
    }

    public function getIcon() {
        if ($this->icon == '' || $this->icon == null) {
            asset('/images/logo_3.png') ;
        } else {
            return asset($this->icon);
        }
    }

    public function getfundingStage() {
        if($this->funding_stage == self::funding_stage_a) {
        	return 'A';
        }
        if($this->funding_stage == self::funding_stage_b) {
	        return 'B';
        }
        if($this->funding_stage == self::funding_stage_c) {
	        return 'C';
        }
        if($this->funding_stage == self::funding_stage_pre_a) {
	        return 'Pre-A';
        }
        if($this->funding_stage == self::funding_stage_angle) {
	        return 'Angle';
        }
        if($this->funding_stage == self::funding_stage_seed) {
	        return 'Seed';
        }
    }

    public function setBannerAttribute($file) {
        if ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
            $mime = $file->getClientOriginalExtension();

            $now = new \Carbon\Carbon();
            $path = 'officialproject/' . $now->format('Y/m/');
            $filename = generateRandomUniqueName();

            touchFolder($path);
            $file->move($path, $filename . '.' . $mime);
            $this->attributes['banner'] = $path . $filename . '.' . $mime;
        }
    }

    public function getBanner() {
        if ($this->banner == '' || $this->banner == null) {
            return '';
        } else {
            return asset($this->banner);
        }
    }

    public function setOutCoverAttribute($file) {
        if ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
            $mime = $file->getClientOriginalExtension();

            $now = new \Carbon\Carbon();
            $path = 'officialproject/cover/' . $now->format('Y/m/');
            $filename = generateRandomUniqueName();

            touchFolder($path);

            $file = $file->move($path, $filename . '.' . $mime);

            $this->attributes['out_cover'] = $path . $filename . '.' . $mime;
        }
    }

    public function getOutCover() {
        if ($this->out_cover == '' || $this->out_cover == null) {
            return '';
        } else {
            return asset($this->out_cover);
        }
    }

    public function setInCoverAttribute($file) {
        if ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
            $mime = $file->getClientOriginalExtension();

            $now = new \Carbon\Carbon();
            $path = 'officialproject/cover/' . $now->format('Y/m/');
            $filename = generateRandomUniqueName();

            touchFolder($path);

            $file = $file->move($path, $filename . '.' . $mime);

            $this->attributes['in_cover'] = $path . $filename . '.' . $mime;
        }
    }

    public function getInCover() {
        if ($this->in_cover == '' || $this->in_cover == null) {
            return '';
        } else {
            return asset($this->in_cover);
        }
    }

    public function setStockTotalAttribute($value)
    {
        if( app()->getLocale() == 'en' )
        {
            //do noting due to alrady store in usd
            $this->attributes['stock_total'] = $value;
        }
        else if( app()->getLocale() == 'cn' )
        {
            //all the value store in DB is USD currency, so need to convert
            $exchangeRate = ExchangeRate::getExchangeRate(Constants::CNY, Constants::USD);
            $this->attributes['stock_total'] = $exchangeRate * $value;
        }
    }

    public function getStockTotal()
    {
        if( app()->getLocale() == 'en' )
        {
            return "$" . number_format($this->stock_total, 0, '', ',');
        }
        else
        {
            //all the value store in DB is USD currency, so need to convert
            if( $this->stock_total != null )
            {
                $exchangeRate = ExchangeRate::getExchangeRate(Constants::USD, Constants::CNY);
                $totalShareValue = number_format($exchangeRate * $this->stock_total, 0, '', ',');
                return "¥ " . $totalShareValue;
            }
            else
            {
                return "¥ 0";
            }
        }

    }

    public function setBudgetFromAttribute($value)
    {
    	  if(isset($value) && $value != '') {
		      if (app()->getLocale() == 'en') {
			      //do noting due to alrady store in usd
			      $this->attributes['budget_from'] = $value;
		      } else if (app()->getLocale() == 'cn') {
			      //all the value store in DB is USD currency, so need to convert
			      $exchangeRate = ExchangeRate::getExchangeRate(Constants::CNY, Constants::USD);
			      $this->attributes['budget_from'] = $exchangeRate * $value;
		      }
	      }
    }

    public function getBudgetFrom()
    {
        if( app()->getLocale() == 'en' )
        {
            return "$" . number_format($this->budget_from, 0, '', ',');
        }
        else
        {
            //all the value store in DB is USD currency, so need to convert
            if( $this->budget_from != null )
            {
                $exchangeRate = ExchangeRate::getExchangeRate(Constants::USD, Constants::CNY);
                $budgetFrom = number_format($exchangeRate * $this->budget_from, 0, '', ',');
                return "¥" . $budgetFrom;
            }
            else
            {
                return "¥ 0";
            }
        }
    }

    public function setBudgetToAttribute($value)
    {
	    if(isset($value) && $value != '') {
		    if (app()->getLocale() == 'en') {
			    //do noting due to alrady store in usd
			    $this->attributes['budget_to'] = $value;
		    } else if (app()->getLocale() == 'cn') {
			    //all the value store in DB is USD currency, so need to convert
			    $exchangeRate = ExchangeRate::getExchangeRate(Constants::CNY, Constants::USD);
			    $this->attributes['budget_to'] = $exchangeRate * $value;
		    }
	    }
    }

    public function getBudgetTo()
    {
        if( app()->getLocale() == 'en' )
        {
            return "$" . number_format($this->budget_to, 0, '', ',');
        }
        else
        {
            //all the value store in DB is USD currency, so need to convert
            if( $this->budget_to != null )
            {
                $exchangeRate = ExchangeRate::getExchangeRate(Constants::USD, Constants::CNY);
                $budgetTo = number_format($exchangeRate * $this->budget_to, 0, '', ',');
                return "¥" . $budgetTo;
            }
            else
            {
                return "¥ 0";
            }
        }
    }

    public function getBudgetRange()
    {
        return round($this->budget_from ). ' - ' . round($this->budget_to);
    }

    public function scopeGetFilteredResults($query) {
        if (Input::has('filter_id') && Input::get('filter_id') != '') {
            $query->where('id', '=', Input::get('filter_id'));
        }

        if (Input::has('filter_language') && Input::get('filter_language') != '') {
            $languageId = Input::get('filter_language');
            $query = $query->whereHas('project.language', function ($query) use ( $languageId ) {
                $query->where('language_id', $languageId);
            });
        }

        if (Input::has('filter_country') && Input::get('filter_country') != '') {
            $countryId = Input::get('filter_country');
            $query = $query->whereHas('project.country', function ($query) use ( $countryId ) {
                $query->where('country_id', $countryId);
            });
        }

        if (Input::has('filter_name') && Input::get('filter_name') != '') {
            $query->where('name', 'like', '%' . Input::get('filter_name') . '%');
        }

        if (Input::has('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::has('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::has('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::has('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }
    }

    public function explainLang() {
        return \App\Constants::translateLang($this->lang);
    }

    public function explainProjectLevel() {
        return \App\Constants::translateProjectLevel($this->project_level);
    }

    public function explainProjectType() {
        return \App\Constants::translateProjectType($this->project_type);
    }

    public function projectPositions() {
        return $this->hasMany('App\Models\OfficialProjectPosition', 'official_project_id', 'id');
    }

    public function projectImages() {
        return $this->hasMany('App\Models\OfficialProjectImage', 'official_project_id', 'id');
    }

    public function project()
    {
        return $this->belongsTo('App\Models\Project', 'project_id', 'id');
    }

    public function explainStatus() {
        return $this->status == 1 ? trans('common.published') : trans('common.draft');
    }

    public function getLocationLatWithDefault() {
        return $this->location_lat ? $this->location_lat : 39.989628;
    }

    public function getLocationLngWithDefault() {
        return $this->location_lng ? $this->location_lng : 116.480983;
    }

    public function getLocationLngLngWithDefault() {
        $lat =  $this->location_lng ? $this->location_lng : 116.480983;
        $lng = $this->location_lng ? $this->location_lng : 116.480983;
				return $lat . ' - ' . $lng;
    }

    public function getRecruitEndDate()
    {
        return date('Y-m-d',strtotime($this->recruit_end));
    }

    public function getTimeFrame()
    {
        $startDate = new Carbon($this->recruit_start);
        $endDate = new Carbon($this->recruit_end);

        $diffDay = $endDate->diff($startDate)->days;
        return $diffDay;
    }

    public function haveApprovedApplicant() {
        foreach ($this->applicants as $key => $var) {
            if ($var->status == ProjectApplicant::STATUS_ACCEPTED) {
                return true;
            }
        }

        return false;
    }

    public function getApprovedApplicant() {
        foreach ($this->applicants as $key => $var) {
            if ($var->status == ProjectApplicant::STATUS_ACCEPTED) {
                return $var->user_id;
            }
        }
    }

    public static function getDesignTypes($empty = false)
    {
        $arr = array();
        if ($arr === true) {
            $arr[] = 'Design Type';
        }
        $arr[static::DESIGN_TYPE_BANNER_WITHOUT_ICON_TOGETHER] = trans('common.banner_without_icon_together');
        $arr[static::DESIGN_TYPE_BANNER_WITH_ICON_TOGETHER] = trans('common.banner_with_icon_together');

        return $arr;
    }
}
