<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SharedOfficeGroupChat extends Model
{
    CONST ATTACHMNET = 1;

    protected $table = 'shared_office_group_chat';

    protected $fillable = [
        'office_id',
        'sender_user_id',
        'sender_staff_id',
        'message',
        'is_seen',
        'deleted_at',
        'file',
        'attachment',
    ];
    use SoftDeletes;
    protected $softDelete = true;

    public function users()
    {
        return $this->hasMany(User::class,'id','sender_user_id');
    }

    public function staffData()
    {
        return $this->hasMany(Staff::class,'id','sender_staff_id');
    }

    public function officeDetail()
    {
        return $this->hasOne(SharedOffice::class, 'id', 'office_id');
    }

    public function totalMembers()
    {
        return $this->hasMany(SharedOfficeGroupChat::class, 'office_id', 'office_id');
    }

    public function staff()
    {
        return $this->hasOne(Staff::class, 'id', 'sender_id');
    }

    public function saveMessage($request)
    {
        $input = [
            'office_id' => isset($request['office_id']) ? $request['office_id'] : NULL,
            'sender_user_id' => \Auth::user()->id,
            'sender_staff_id' => isset($request['sender_staff_id']) ? $request['sender_staff_id'] : NULL,
            'message' => isset($request['message']) ? $request['message'] : NULL,
            'is_seen' => isset($request['is_seen']) ? $request['is_seen'] : NULL,
        ];

        $input = array_filter($input, 'strlen');
        $result = $this->create($input);
        return $result;

    }

    public function getMessagesByOfficeId($office_id)
    {
        return $this
            ->with('totalMembers','users','staffData')
            ->where('office_id', '=', $office_id)
            ->get();
    }
}
