<?php

namespace App\Models;

use Input;
use App\Models\Skill;

class OfficialProjectImage extends BaseModels {

    protected $table = 'official_project_images';

    protected $fillable = [
        'official_project_id', 'image'
    ];

    public $rules = [
        'official_project_id' => 'required|exists:official_projects,id',
        'image' => 'required|mimes:jpg,jpeg,png,bmp,gif',
    ];

    protected $dates = [];

    public static function boot() {
        parent::boot();
    }

    public function scopeGetProjectSkill($query) {
        return $query;
    }

    public function officialProject() {
        return $this->belongsTo('App\Models\OfficialProject', 'official_project_id', 'id');
    }

    public function setImageAttribute($file) {
        if ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
            $mime = $file->getClientOriginalExtension();

            $now = new \Carbon\Carbon();
            $path = 'officialproject/images/' . $now->format('Y/m/');
            $filename = generateRandomUniqueName();

            touchFolder($path);

            $file = $file->move($path, $filename . '.' . $mime);

            $this->attributes['image'] = $path . $filename . '.' . $mime;
        }
    }

    public function getImage() {
        if ($this->image != null && $this->image != '') {
            return asset($this->image);
        } else {
            return '';
        }
    }
}