<?php

namespace App\Models;

use Input;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Api\SharedOffice\SharedOfficeController;
use Illuminate\Database\Eloquent\SoftDeletes;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageManager;

class SharedOffice extends Model
{

    use SoftDeletes;

    const BOOK_TYPE_PAY_LATER = 1;
    const BOOK_TYPE_PAY_NOW = 2;
    const MANAGE_BOOKING_NOT_ALLOW = 0;
    const MANAGE_BOOKING_ALLOW = 1;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'office_name',
        'location',
        'description',
        'office_manager',
        'qr',
        'image',
        'pic_size',
        'staff_id',
        'created_at',
        'updated_at',
        'office_size_x',
        'office_size_y',
        'number',
        'city_id',
        'state_id',
        'continent_id',
        'lat',
        'lng',
        'contact_name',
        'contact_phone',
        'contact_email',
        'contact_address',
        'monday_opening_time',
        'monday_closing_time',
        'saturday_opening_time',
        'saturday_closing_time',
        'sunday_opening_time',
        'sunday_closing_time',
        'office_space',
        'version_english',
        'version_chinese',
        'active',
        'likes',
        'verify_info_to_scan',
        'require_deposit',
        'currency_id',
        'time_zone',
        'seatsData'
    ];

    protected $appends = [
        'total_members',
        'meeting_room_price',
        'total_hot_desk',
        'total_meeting_room',
        'checked_in_users',
        'city_id',
        'state_id',
        'min_seat_price',
        'max_seat_price',
        'total_dedicated_desks',
        'city_name',
        'country_name',
        'rating',
        'compressed_image',
        'discount'
    ];

    public function getDiscountAttribute()
    {
        $d = SharedOfficeSetting::where('staff_id', '=', $this->staff_id)->first();
        if(is_null($d)){
            return 0;
        } else {
            return $d->discount;
        }
    }

    public function officeSettings(){
        return $this->hasOne(SharedOfficeSetting::class,'staff_id','staff_id');
    }


    public function bookings()
    {
        return $this->hasMany(SharedOfficeBooking::class, 'office_id', 'id');
    }

    public function currency() {
        return $this->hasOne(WorldCountries::class,'id','currency_id');
    }

    public function owner()
    {
        return $this->hasOne(Staff::class, 'id', 'staff_id');
    }

    function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public function getLocationAttribute($val)
    {
        return $val;
//        return self::clean($val);
    }

    public function getRatingAttribute()
    {
        try {
            $rating = SharedOfficeRating::where('office_id', '=', $this->id)->avg('rate');
            return ceil($rating);
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function getImageAttribute($val)
    {
        return is_null($val) ? '/images/logo_3.png' : $val;
    }

    public function getCompressedImageAttribute()
    {
        return is_null($this->image) ? asset('/images/logo_3.png') : asset('/compresses/sharedOffice/'.str_replace('uploads/sharedoffice/','',$this->image));
    }

    public function SharedOfficeLanguage()
    {
        return $this->hasOne(SharedOfficeProducts::class, 'office_id', 'id');
    }

    public function SharedOfficeLanguageData()
    {
        return $this->hasOne(SharedOfficeLanguage::class, 'shared_offices_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo(WorldCities::class, 'city_id');
    }

    public function country()
    {
        return $this->belongsTo(WorldCountries::class, 'state_id');
    }

    public function getCityNameAttribute()
    {
        try {
            return $this->city->name;
        } catch (\Exception $e) {
            return '';
        }
    }

    public function getCountryNameAttribute()
    {
        try {
            return $this->country->name;
        } catch (\Exception $e) {
            return '';
        }
    }

    public function getMaxSeatPriceAttribute()
    {
        $p  = SharedOfficeProducts::where('office_id', '=', $this->id)->orderBy('month_price', 'desc')->first();
        if (is_null($p)) {
            return 0;
        } else {
            return $p->month_price;
        }
    }

    public function getMinSeatPriceAttribute()
    {
        $p  = SharedOfficeProducts::where('office_id', '=', $this->id)->orderBy('month_price', 'asc')->first();
        if (is_null($p)) {
            return 0;
        } else {
            return $p->month_price;
        }
    }

    public function getTotalDedicatedDesksAttribute()
    {
        $p = SharedOfficeProducts::where('office_id', '=', $this->id)->where('category_id', '=', SharedOfficeProducts::CATEGORY_DEDICATED_DESK)->orderBy('month_price', 'asc')->count();
        if ($p < 1) {
            return 0;
        }
        return $p;
    }

    public function bulletins()
    {
        return $this->hasMany(SharedOfficesBulletin::class, 'shared_offices_id', 'id');
    }

    public function getCityIdAttribute($val)
    {
        return $val;
    }

    public function getStateIdAttribute($val)
    {
        return $val;
    }

    public function getCheckedInUsersAttribute()
    {
        try {
            return SharedOfficeController::get_check_in_users($this->id);
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function getTotalMembersAttribute()
    {
        return $this->totalMembersInOffice->count();
    }

    public function getTotalMeetingRoomAttribute()
    {
        $count = SharedOfficeProducts::where('office_id', '=', $this->id)->where('category_id', '=', SharedOfficeProducts::CATEGORY_MEETING_ROOM)->count();
        if ($count < 1) {
            return 0;
        }
        return $count;
    }

    public function getTotalHotDeskAttribute()
    {
        $count = SharedOfficeProducts::where('office_id', '=', $this->id)->where('category_id', '=', SharedOfficeProducts::CATEGORY_HOT_DESK)->count();
        if ($count < 1) {
            return 0;
        }
        return $count;
    }

    public function getMeetingRoomPriceAttribute()
    {
        try {
            if (isset($this->meetingPrice)) {
                $time_price = $this->meetingPrice->first()->time_price;
                unset($this->meetingPrice);
                return $time_price;
            } else {
                return "";
            }
        } catch (\Exception $e) {
            return "";
        }
    }

    // protected $table = 'sharedOffices';
    public function products()
    {
        return $this->belongsTo('App\Models\SharedOfficeProducts');
    }

    // protected $table = 'sharedOffices';
    public function officeProducts()
    {
        return $this->hasMany('App\Models\SharedOfficeProducts', 'office_id', 'id');
    }

    public function images()
    {
        return $this->belongsTo('App\Models\SharedOfficeImages');
    }

    public function officeImages()
    {
        return $this->hasMany(SharedOfficeImages::class, 'office_id', 'id');
    }

    public function shfacilities()
    {
        return $this->belongsToMany('App\Models\Shfacilitie');
    }

    public function isMember()
    {
        return $this->hasOne(SharedOfficeFinance::class, 'office_id', 'id');
    }

    public function totalMember()
    {
        return $this->hasOne(SharedOfficeFinance::class, 'office_id', 'id');
    }

    public function totalMembersInOffice()
    {
        return $this->hasMany(SharedOfficeFinance::class, 'office_id', 'id');
    }

    public function totalOfficeMember()
    {
        return $this->hasMany(SharedOfficeGroupChat::class, 'office_id', 'id');
    }

    public function seatPrice()
    {
        return $this->hasOne(SharedOfficeProducts::class, 'office_id', 'id');
    }

    public function meetingPrice()
    {
        return $this->hasOne(SharedOfficeProducts::class, 'office_id', 'id')
            ->where('category_id', '=', SharedOfficeProducts::CATEGORY_MEETING_ROOM);
    }

    public function sharedOfficeRating()
    {
        return $this->hasMany(SharedOfficeRating::class, 'office_id', 'id');
    }

    public function sharedOfficeFee()
    {
        return $this->hasMany(SharedOfficeFee::class, 'office_id', 'id');
    }

    public function sharedOfficeLikeCount()
    {
        return $this->hasMany(SharedOfficeLikeCount::class, 'shared_office_id', 'id');
    }
    public function product()
    {
        return $this->hasOne(SharedOfficeProducts::class, 'office_id', 'id');
    }
}
