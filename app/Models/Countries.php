<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    protected $table = 'countries';

    protected $fillable = [
        'id', 'code', 'name', 'name_cn'
    ];
	
		public function getCodeAttribute($val)
		{
			return strtoupper($val);
		}
			
    public static function getCountryLists($empty = false) {
//        $arr = array();
//        if ($arr === true) {
//            $arr[] = 'Country';
//            $arrid[]='id';
//            $arrName[]='name';
//        }
//
//        if( $empty )
//        {
//            $arr[''] = trans('common.all');
//        }
//
//        $countries = Countries::all();
//        foreach( $countries as $country)
//        {
//            if( app()->getLocale() == "en" )
//            {
////                $arr['id'] = $country->id;
//                $arr[$country->id] = $country->getName();
//            }
//            else
//            {
//                $arr[$country->id] = $country->name_cn;
//            }
//
//        }
        $arr = self::select(['id', 'name'])->get();
        return $arr;
    }

    public function getName()
    {
        if( app()->getLocale() == "en" )
        {
            return $this->name;
        }
        else
        {
            return $this->name_cn;
        }
    }

    public static function getJsonCountries()
    {
        $arr = array();
        $countries = Countries::all();
        foreach( $countries as $country)
        {
            if( app()->getLocale() == "en" )
            {
                $countryName = $country->name;
            }
            else
            {
                $countryName = $country->name_cn;
            }

            $item = array("id"=>$country->id, "name"=>$countryName);
            array_push($arr, $item);
        }

        return json_encode($arr);
    }

    public static function getCountryCodeLists($empty = false) {
//        $arr = array();
//        if ($arr === true) {
//            $arr[] = 'CountryCode';
//        }
//
//        if( $empty )
//        {
//            $arr[''] = trans('common.all');
//        }
//
//        $countries = Countries::all();
//        foreach( $countries as $country)
//        {
//            $arr[$country->id] = $country->code;
//
//        }
        $arr = self::select(['id', 'code'])->get();
        return $arr;
    }
}
