<?php

namespace App\Models;

class UploadFile extends BaseModels {

    const TYPE_PORTFOLIO = 1;
    const TYPE_POST_PROJECT = 2;

    protected $table = 'upload_files';

    public static $rules = [
        'file' => 'required|mimes:png,gif,jpeg,jpg,bmp,pdf,mp3,xls,xlsx,mp4,docs,docx'
    ];

    public static $messages = [
        'file.mimes' => 'Uploaded file is not in allow format',
        'file.required' => 'File is required'
    ];

    public function getFileType()
    {
        $result = "";

        if( in_array( substr(strrchr($this->filename,'.'),1), ['jpg', 'jpeg']) )
        {
            $result = "";
        }
        else if( in_array( substr(strrchr($this->filename,'.'),1), ['gif']) )
        {
            $result = "";
        }
        else if( in_array( substr(strrchr($this->filename,'.'),1), ['png']) )
        {
            $result = "";
        }
        else if( in_array( substr(strrchr($this->filename,'.'),1), ['bmp']) )
        {
            $result = "";
        }
        else if( in_array( substr(strrchr($this->filename,'.'),1), ['doc', 'docx']) )
        {
            $result = "application/msword";
        }
        else if ( in_array( substr(strrchr($this->filename,'.'),1), ['pdf']) )
        {
            $result = "application/pdf";
        }
        else if ( in_array( substr(strrchr($this->filename,'.'),1), ['xls', 'xlsx']) )
        {
            $result = "application/vnd.ms-excel";
        }
        else if ( in_array( substr(strrchr($this->filename,'.'),1), ['mp3']) )
        {
            $result = "audio/mpeg3";
        }
        else if ( in_array( substr(strrchr($this->filename,'.'),1), ['mp4']) )
        {
            $result = "video/mp4";
        }

        return $result;
    }
}