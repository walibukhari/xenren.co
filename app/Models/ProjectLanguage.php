<?php

namespace App\Models;

use Input;
use App\Models\Skill;

class ProjectLanguage extends BaseModels {

    public $rules = [
        'project_id' => 'required|exists:projects,id',
        'language_id' => 'required|exists:languages,id',
    ];

    protected $table = 'project_languages';

    protected $fillable = [
        'project_id', 'language_id', ''
    ];

    protected $dates = [];


    public function project() {
        return $this->belongsTo('App\Models\Project', 'project_id', 'id');
    }

    public function language() {
        return $this->belongsTo('App\Models\Language', 'language_id', 'id');
    }

}