<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SharedOfficeRatingHelpfulCount extends Model
{
    protected $table = 'shared_office_rating_helpful_count';
    
    protected $fillable = [
	    'user_id',
	    'shared_office_rating_id',
    ];
}
