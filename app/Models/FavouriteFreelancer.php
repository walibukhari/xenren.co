<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FavouriteFreelancer extends Model
{
    protected $table = 'favourite_freelancers';

    protected $fillable = [
    	'user_id',
	    'freelancer_id'
    ];
    
    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function freelancer() {
        return $this->belongsTo('App\Models\User', 'freelancer_id', 'id');
    }


}
