<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    protected $table = 'transaction_detail';

    protected $fillable = [
    	'transaction_id',
    	'payment_method',
    	'raw',
    ];

    CONST PAYMENT_METHOD_PAYPAL = 'paypal';
    CONST PAYMENT_METHOD_STRIPE = 'stripe';
    CONST PAYMENT_METHOD_REVENUE_MONSTER = 'revene_monster';
}
