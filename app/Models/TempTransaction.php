<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TempTransaction extends Model
{
    protected $fillable = [
        'order_id','amount','user_id','staff_id'
    ];

    public  function  user() {
        return $this->hasOne(User::class,'id','user_id');
    }

    public  function  staff() {
        return $this->hasOne(Staff::class,'id','staff_id');
    }
}
