<?php

namespace App\Models;

use Input;

class SkillKeywordSubmit extends BaseModels {

    protected $table = 'skill_keyword_submits';

    protected $fillable = [
        'name', 'submit_count'
    ];

    protected $dates = [];

    public static function boot() {
        parent::boot();
    }

    public function scopeGetSkillKeywordSubmit($query) {
        return $query;
    }

}