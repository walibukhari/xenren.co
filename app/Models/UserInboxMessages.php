<?php

namespace App\Models;

use Auth;
use Illuminate\Support\Facades\Session;
use Input;
use Carbon\Carbon;

class UserInboxMessages extends BaseModels {

	const TYPE_PROJECT_APPLICANT_SELECTED = 1;
	const TYPE_PROJECT_NEW_MESSAGE = 2;
	const TYPE_PROJECT_CONFIRMED_COMPLETED = 3;
	const TYPE_PROJECT_SUCCESSFUL_POST_JOB = 4;
	const TYPE_PROJECT_PEOPLE_COME_IN = 5;
	const TYPE_PROJECT_INVITE_YOU = 6;
	const TYPE_PROJECT_ASK_FOR_CONTACT = 7;
	const TYPE_PROJECT_APPLICANT_ACCEPTED = 8;
	const TYPE_PROJECT_APPLICANT_REJECTED = 9;
	const TYPE_PROJECT_SEND_OFFER = 10;
	const TYPE_PROJECT_AGREE_SEND_CONTACT = 11;
	const TYPE_PROJECT_REJECT_SEND_CONTACT = 12;
	const TYPE_PROJECT_INVITE_ACCEPT = 13;
	const TYPE_PROJECT_INVITE_REJECT = 14;
	const TYPE_PROJECT_ACCEPT_SEND_OFFER = 15;
	const TYPE_PROJECT_REJECT_SEND_OFFER = 16;
	const TYPE_PROJECT_FILE_UPLOAD = 17;
	const TYPE_CHAT_FROM_APP = 17;
	CONST TYPE_PROJECT_RECEIVE_OFFER = 19;
	CONST TYPE_PROJECT_SKILL_NOTIFICATION = 20;

	const PAY_TYPE_HOURLY_PAY = 1;
	const PAY_TYPE_FIXED_PRICE = 2;

	protected $table = 'users_inbox_messages';

	protected $fillable = [
		'inbox_id',
		'from_user_id',
		'to_user_id',
		'from_staff_id',
		'to_staff_id',
		'project_id',
		'is_read',
		'type',
		'custom_message',
		'quote_price',
		'pay_type',
		'file',
		'updated_at',
		'created_at',
		'daily_update',
        'response_time'
	];

	protected $dates = [];

	public static function boot() {
		parent::boot();
	}

	public function getQuotePrice($from , $to){
	    $data = self::where('from_user_id','=',$to )->where('to_user_id','=',$from)->first();
	    return $data;
    }

	public function getFromUserData($fromUserId) {
	    $user = User::where('id','=',$fromUserId)->first();
	    return $user;
    }

	public function inbox() {
		return $this->belongsTo('App\Models\UserInbox', 'inbox_id', 'id');
	}

	public function project() {
		return $this->belongsTo('App\Models\Project', 'project_id', 'id');
	}

	public function fromUser() {
		return $this->belongsTo('App\Models\User', 'from_user_id', 'id');
	}

	public function toUser() {
		return $this->belongsTo('App\Models\User', 'to_user_id', 'id');
	}

	public function isRead() {
		return $this->is_read == 1 ? true : false;
	}

	public function setTranslateVariableAttribute($value) {
		$this->attributes['translate_variable'] = json_encode($value);
	}

	public function getTranslateVariableAttribute($value) {
		return json_decode($this->attributes['translate_variable'], true);
	}

	public function getHref() {
        $locale = getLocale();
        $inboxId = isset($this->inbox_id) ? $this->inbox_id : 0;
        $projectId = isset($this->project_id) ? $this->project_id : 0;
        $projectName = Project::where('id','=',$projectId)->first();
        if(is_null($projectName)) {
            $projectName = 0;
        } else {
            $projectName = $projectName->name;
        }
        $projectName = isset($projectName) ? str_replace(' ', '-', $projectName): 0;
		try {
			switch ($this->type) {
				case static::TYPE_PROJECT_APPLICANT_SELECTED:
					return route('frontend.joindiscussion.getproject', [$locale,$projectName, $projectId]);
					break;
				case static::TYPE_PROJECT_NEW_MESSAGE:
					return route('frontend.inbox.messages', [$this->inbox_id]);
					break;
				case static::TYPE_PROJECT_CONFIRMED_COMPLETED:
					return route('frontend.joindiscussion.getproject', [$locale,$projectName, $projectId]);
					break;
				case static::TYPE_PROJECT_SUCCESSFUL_POST_JOB:
					return route('frontend.joindiscussion.getproject', [$locale,$projectName, $projectId]);
					break;
				case static::TYPE_PROJECT_PEOPLE_COME_IN:
					return route('frontend.joindiscussion.getproject', [$locale,$projectName, $projectId]);
					break;
				case static::TYPE_PROJECT_INVITE_YOU:
					return route('frontend.inbox.messages', [$this->inbox_id]);
					break;
				case static::TYPE_PROJECT_ASK_FOR_CONTACT:
					return route('frontend.inbox.messages', [$this->inbox_id]);
					break;
				case static::TYPE_PROJECT_SEND_OFFER:
					return route('frontend.joindiscussion.getproject', [$locale,$projectName, $projectId]);
					break;
				case static::TYPE_PROJECT_AGREE_SEND_CONTACT:
					return route('frontend.joindiscussion.getproject', [$locale,$projectName, $projectId]);
					break;
				case static::TYPE_PROJECT_REJECT_SEND_CONTACT:
					return route('frontend.joindiscussion.getproject', [$locale,$projectName, $projectId]);
					break;
				case static::TYPE_PROJECT_INVITE_ACCEPT:
					return route('frontend.joindiscussion.getproject', [$locale,$projectName, $projectId]);
					break;
				case static::TYPE_PROJECT_INVITE_REJECT:
					return route('frontend.joindiscussion.getproject', [$locale,$projectName, $projectId]);
					break;
				case static::TYPE_PROJECT_ACCEPT_SEND_OFFER:
					return route('frontend.joindiscussion.getproject', [$locale,$projectName, $projectId]);
					break;
				case static::TYPE_PROJECT_REJECT_SEND_OFFER:
					return route('frontend.joindiscussion.getproject', [$locale,$projectName, $projectId]);
					break;
				case static::TYPE_PROJECT_APPLICANT_ACCEPTED:
					return route('frontend.joindiscussion.getproject', [$locale,$projectName, $projectId]);
					break;
				case static::TYPE_PROJECT_APPLICANT_REJECTED:
					return route('frontend.joindiscussion.getproject', [$locale,$projectName, $projectId]);
					break;
				case static::TYPE_PROJECT_FILE_UPLOAD:
					return route('frontend.joindiscussion.getproject', [$locale,$projectName, $projectId]);
					break;
				case static::TYPE_PROJECT_RECEIVE_OFFER:
					return route('frontend.inbox.messages', [$inboxId]);
					break;
				default:
					return route("frontend.inbox",[$locale]);
					break;
			}
		}catch (\Exception $e) {
			return '';
		}
	}

	public function getTitle() {
		switch ($this->type) {
			case static::TYPE_PROJECT_APPLICANT_SELECTED:
				return trans('common.you_have_been_selected_for_project', ['projectId' => $this->project_id]);
				break;
			case static::TYPE_PROJECT_NEW_MESSAGE:
				return truncateSentence($this->custom_message, 150);
				break;
			case static::TYPE_PROJECT_CONFIRMED_COMPLETED:
				return trans('common.project_completed');
				break;
			case static::TYPE_PROJECT_SUCCESSFUL_POST_JOB:
				return trans('common.successful_post_job');
				break;
			case static::TYPE_PROJECT_PEOPLE_COME_IN:
				return trans('common.new_user_join_discussion_room');
				break;
			case static::TYPE_PROJECT_INVITE_YOU:
				return trans('common.invite_you_to_project');
				break;
			case static::TYPE_PROJECT_ASK_FOR_CONTACT:
				return trans('common.user_ask_for_contact');
				break;
			case static::TYPE_PROJECT_SEND_OFFER:
				return trans('common.send_offer');
				break;
			case static::TYPE_PROJECT_AGREE_SEND_CONTACT:
				return trans('common.accept_send_contact');
				break;
			case static::TYPE_PROJECT_REJECT_SEND_CONTACT:
				return trans('common.reject_send_contact');
				break;
			case static::TYPE_PROJECT_INVITE_ACCEPT:
				return trans('common.invite_accept');
				break;
			case static::TYPE_PROJECT_INVITE_REJECT:
				return trans('common.invite_reject');
				break;
			case static::TYPE_PROJECT_ACCEPT_SEND_OFFER:
				return trans('common.accept_send_offer');
				break;
			case static::TYPE_PROJECT_REJECT_SEND_OFFER:
				return trans('common.reject_send_offer');
				break;
			case static::TYPE_PROJECT_APPLICANT_ACCEPTED:
				return trans('common.success_accept_job');
				break;
			case static::TYPE_PROJECT_APPLICANT_REJECTED:
				return trans('common.success_reject_job');
				break;
			case static::TYPE_PROJECT_FILE_UPLOAD:
				return trans('common.file_upload');
				break;
			case static::TYPE_PROJECT_RECEIVE_OFFER:
				return trans('common.received_offer');
				break;
			default:
				return '-';
				break;
		}
	}

	public function getHtmlClass() {
		switch ($this->type) {
			case static::TYPE_PROJECT_APPLICANT_SELECTED:
				return 'alert-info';
				break;
			case static::TYPE_PROJECT_NEW_MESSAGE:
			case static::TYPE_PROJECT_SUCCESSFUL_POST_JOB:
			case static::TYPE_PROJECT_PEOPLE_COME_IN:
			case static::TYPE_PROJECT_INVITE_YOU:
			case static::TYPE_PROJECT_ASK_FOR_CONTACT:
				return 'alert-success';
				break;
			case static::TYPE_PROJECT_CONFIRMED_COMPLETED:
				return 'alert-warning';
				break;
			default:
				return 'alert-default';
				break;
		}
	}

	public function getGotoLink() {
		switch ($this->type) {
			case static::TYPE_PROJECT_APPLICANT_SELECTED:
				if ($this->order_id != null) {
					return route('frontend.mypublishorderdetails', $this->order_id);
				} else {
					return 'javascript:;';
				}
				break;
			case static::TYPE_PROJECT_NEW_MESSAGE:
				if ($this->order_id != null) {
					return route('frontend.mypublishorderdetails', $this->order_id);
				} else {
					return 'javascript:;';
				}
				break;
			case static::TYPE_PROJECT_CONFIRMED_COMPLETED:
				if ($this->order_id != null) {
					return route('frontend.mypublishorderdetails', $this->order_id);
				} else {
					return 'javascript:;';
				}
				break;
			default:
				return 'javascript:;';
				break;
		}
	}

	public function getCreatedDate() {
		$language = Session::get('lang');
		if( $language == null )
		{
			session(['lang' => 'cn']);
			$language = 'cn';
		}

		if ($language == "cn") {
			Carbon::setLocale('zh');
		} else {
			Carbon::setLocale('en');
		}
		$humanDate = $this->created_at->diffForHumans();

		return $humanDate;
	}

	public function getCustomMessage()
	{
		switch ($this->type) {
//            case static::TYPE_PROJECT_APPLICANT_SELECTED;
//                $acceptUrl = route('frontend.inbox.acceptjob', ['project_id' => $this->project_id]);
//                $rejectUrl = route('frontend.inbox.rejectjob', ['project_id' => $this->project_id]);
//
//                return trans('common.congrat_to_selected_user') .
//                    "<br/> <a href=' " . $acceptUrl . " '>[ " . trans('common.accept') . " ]</a> or <a href='" . $rejectUrl . "'>[ " . trans('common.reject') . " ]</a>";
			default:
				return $this->custom_message;
				break;
		}
	}

	public function getSenderInfo()
	{
		if( isset($this->from_user_id) && $this->from_user_id >= 0 )
		{
			$user = User::find($this->from_user_id);
			$name = $user->getName();
			$avatar = $user->getAvatar();
		}
		else if( isset($this->from_staff_id) && $this->from_staff_id >= 0 )
		{
			$staff = Staff::find($this->from_staff_id);
			$name = $staff->name;
			$avatar = $staff->getAvatar();
		}

		$result = array(
			'name' => $name,
			'avatar' => $avatar
		);

		return $result;
	}

	public function setFileAttribute($file)
	{
		if ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
			if($file->guessClientExtension() == 'dwg'){
				$ext = $file->guessClientExtension();
				$c = new \Carbon\Carbon();
				$path = 'uploads/inbox/' . Auth::guard('users')->user()->id . '/' . $c->format('Y/m/');
				$filename = 'back_' . generateRandomUniqueName();

				touchFolder($path);

				$file->move($path, $filename.'.'.$ext);
				$this->attributes['file'] = $path. $filename.'.'.$ext;
			}else{
				$img = \Image::make($file);
				$mime = $img->mime();
				$ext = convertMimeToExt($mime);

				$c = new \Carbon\Carbon();
				$idPath = Auth::guard('users')->check() ? Auth::guard('users')->user()->id : 'staff';
				$path = 'uploads/inbox/' . $idPath . '/' . $c->format('Y/m/');
				$filename = 'back_' . generateRandomUniqueName();

				touchFolder($path);

				$img = $img->save($path . $filename . $ext);

				$this->attributes['file'] = $path . $filename . $ext;
			}
		}
	}

//	public function getUserPrivateChatWithOtherUser($otherUserId)
//	{
//		return $this
//			->where('to_user_id', '=', 401)
//			->orwhere('to_user_id', '=', $otherUserId)
//			->where('from_user_id', '=', 401)
//			->orwhere('from_user_id', '=', $otherUserId)
////			->select(['id', 'custom_message', 'inbox_id', 'is_read', 'type'])
//			->get();
//	}
//
	public function getFile() {
		if ($this->file != null && $this->file != '') {
			return asset($this->file);
		} else {
			return null;
		}
	}
}
