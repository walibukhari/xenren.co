<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class OfficialProjectNews extends Model
{
    const TYPE_UPDATE = 1;
    const TYPE_HOT_NEWS = 2;
    const TYPE_STORY = 3;

    protected $table = 'official_project_news';

    public function project() {
        return $this->belongsTo('App\Models\Project', 'project_id', 'id');
    }

    public static function getTypeList($empty = false) {
        $arr = array();
        if ($arr === true) {
            $arr[] = 'Type';
        }

        if( $empty )
        {
            $arr[''] = trans('common.all');
        }
        $arr[static::TYPE_UPDATE] = trans('common.update');
        $arr[static::TYPE_HOT_NEWS] = trans('common.hot_news');
        $arr[static::TYPE_STORY] = trans('common.story');

        return $arr;
    }

    public function translateType() {
        switch ($this->type) {
            case static::TYPE_UPDATE: return trans('common.update'); break;
            case static::TYPE_HOT_NEWS: return trans('common.hot_news'); break;
            case static::TYPE_STORY: return trans('common.story'); break;
            default: return '-'; break;
        }
    }

    public function scopeGetFilteredResults($query) {
        if (Input::has('filter_id') && Input::get('filter_id') != '') {
            $query->where('id', '=', Input::get('filter_id'));
        }

        if (Input::has('filter_type') && Input::get('filter_type') != '') {
            $query->where('type', Input::get('filter_type') );
        }

        if (Input::has('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::has('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }
    }

    public function getDate()
    {
        $datetime = Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at);
        $string = $datetime->format('d F Y');
        return $string;
    }

    public function getTypeSymbol()
    {
        if($this->type == OfficialProjectNews::TYPE_UPDATE)
        {
            $result = '<i class="fa fa-repeat repeat pull-right" aria-hidden="true"></i>';
        }
        else if($this->type == OfficialProjectNews::TYPE_HOT_NEWS)
        {
            $result = '<img src="/images/icons/ic_hot.png" width="12px" class="pull-right">';
        }
        else if($this->type == OfficialProjectNews::TYPE_STORY)
        {
            $result = '<span class="fa fa-star star pull-right"></span>';
        }
        else
        {
            $result = "Unknown Type";
        }

        return $result;
    }
}
