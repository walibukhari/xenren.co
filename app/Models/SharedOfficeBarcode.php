<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SharedOfficeBarcode extends Model
{
    protected $table = 'shared_office_barcode';

    protected $fillable = [
        'shared_office_id',
        'type',
        'category',
        'barcode',
        'code',
        'password',
        'bid',
        'active',
    ];

    protected $appends = [
        'google_barcode'
    ];

    CONST SHARED_OFFICE_BARCODE_BID = 1;
    CONST SHARED_OFFICE_BARCODE_BID_ZERO = 0;
    CONST SHARED_OFFICE_BARCODE_ACTIVE = 1;
    CONST SHARED_OFFICE_BARCODE_INACTIVE = 0;
    CONST BARCODE_TYPE= 1;
    CONST BARCODE_TYPE_TWO = 2;
    CONST BARCODE_TYPE_THREE = 3;
    CONST BARCODE_CATEGORY_TYPE = 1;

    public function sharedOffice()
    {
        return $this->hasOne(SharedOffice::class, 'id', 'shared_office_id');
    }

    public static function getBarCodes($officeId=null)
    {
        if(is_null($officeId)) {
            return self::with('sharedOffice')
                ->whereHas('sharedOffice')
                ->paginate(10);
        } else {
            return self::with('sharedOffice')
                ->where('shared_office_id', $officeId)
                ->whereHas('sharedOffice')
                ->paginate();
        }
    }

    public function getGoogleBarcodeAttribute($val)
    {
        return 'https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=office:'.$this->shared_office_id.'|type:'.$this->type.'|category:'.$this->category;
    }
}
