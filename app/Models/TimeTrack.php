<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TimeTrack extends Model
{
	protected $table = 'user_time_track';

	protected $fillable = [
		'id',
		'start_time',
		'memo',
		'end_time',
		'user_id',
		'total_time',
		'project_id',
		'project_name',
		'auto_end',
	];

	protected $appends = [
		'date',
        'total_time_elapsed',
        'total_time_elapsed_in_minute',
	];

    public function getTotalTimeElapsedAttribute()
    {
        $startTime = is_null($this->end_time) ? Carbon::now() : Carbon::parse($this->start_time);
        $endTime = is_null($this->end_time) ? Carbon::now() : Carbon::parse($this->end_time);
        return $startTime->diffInSeconds($endTime);
	}

    public function getTotalTimeElapsedInMinuteAttribute()
    {
        $startTime = is_null($this->end_time) ? Carbon::now() : Carbon::parse($this->start_time);
        $endTime = is_null($this->end_time) ? Carbon::now() : Carbon::parse($this->end_time);
        return $startTime->diffInMinutes($endTime);
	}

	public function getDateAttribute()
	{
		return Carbon::parse($this->created_at)->toDateString();
	}

	public function project()
	{
		return $this->hasOne(Project::class, 'id', 'project_id');
	}

	public function pictures()
	{
		return $this->hasMany(ImageModel::class, 'user_time_tracker_id', 'id');
	}

	public function projectApplicant()
	{
		return $this->hasOne(ProjectApplicant::class, 'project_id', 'project_id');
	}

}
