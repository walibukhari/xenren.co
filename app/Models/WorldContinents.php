<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorldContinents extends Model
{
	protected $table = 'world_continents';

	protected $fillable = [
		'name',
		'code',
        'usd_amount'
	];
}
