<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BadWordsFilter extends Model
{
    protected $table = 'bad_words_filters';

    protected $fillable = [
      'user_id',
      'data'
    ];

    protected $casts = [
        'data' => 'array',
    ];

    const TYPE_PROJECT = 1;
    const TYPE_PROFILE = 2;

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function project()
    {
        return $this->hasOne(Project::class, 'id', 'user_id');
    }

    public function getTypeName($id)
    {
        if($id == self::TYPE_PROFILE) {
            return 'User';
        }

        if($id == self::TYPE_PROJECT) {
            return 'Project';
        }
    }

}
