<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SharedOfficeBooking extends Model
{
    use SoftDeletes;
    protected $table = 'shared_office_booking';

    const IS_READ = 1;
    const IS_NOT_READ = 0;

    CONST STATUS_REQUEST = 0;
    CONST STATUS_BOOKED = 1;
    CONST STATUS_CANCELED = 2;
    CONST STATUS_CANCELED_CLIENT_SIDE = 3;

    protected $fillable = [
        'id',
        'office_id',
        'user_id',
        'client_email',
        'full_name',
        'phone_no',
        'check_in',
        'check_out',
        'no_of_rooms',
        'no_of_persons',
        'remarks',
        'is_read',
        'category_id',
        'status',
    ];

    protected $appends = [
        'category_name'
    ];

    public function getUserSeatNumber($category_id , $office_id){
        $getPreviousSeatData = SharedOfficeProducts::where('category_id','=',$category_id )->where('office_id','=',$office_id)->first();
        if(!is_null($getPreviousSeatData)) {
            return $getPreviousSeatData->number;
        }
    }

    public function getTimePrice($category_id , $office_id){
        $getTimePrice = SharedOfficeProducts::where('category_id','=',$category_id )->where('office_id','=',$office_id)->first();
        if(!is_null($getTimePrice)) {
            return $getTimePrice->time_price;
        }
    }

    public function getCurrantStatusName($seat) {
        $getStatus = SharedOfficeProducts::where('number','=',$seat)->first();
        if(!is_null($getStatus)) {
            if($getStatus->status_id == SharedOfficeProducts::STATUS_USING) {
                return 'Using';
            } else if($getStatus->status_id == SharedOfficeProducts::STATUS_BOOKED) {
                return 'Booked';
            } else {
                return 'Empty';
            }
        }
    }

    public function getStatusName($status){
        if($status == self::STATUS_REQUEST) {
            return 'Pending';
        } else if($status == self::STATUS_BOOKED) {
            return 'Booked';
        } else if($status == self::STATUS_CANCELED) {
            return 'Canceled By Owner';
        } else if($status == self::STATUS_CANCELED_CLIENT_SIDE) {
            return 'You Cancel Request';
        }
    }

    public function getRoomData($id){
        return $id;
    }
    public function getTotalFreelancer(){
        $total = self::where('status','=',self::STATUS_BOOKED)->get();
        return count($total);
    }

    public function getCategoryNameAttribute()
    {
        $d = SharedOfficeProductCategory::where('id', '=', $this->category_id)->first();
        if(!is_null($d)){
            $name = $d->name;
            $name = explode('.', $name);
            return $name[1];
        } else {
            return '' ;
        }
    }

    public function office()
    {
        return $this->belongsTo('App\Models\SharedOffice', 'office_id', 'id');
    }

    public  function products(){
        return $this->hasOne(SharedOfficeProducts::class,'category_id','category_id');
    }

    public function users()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
