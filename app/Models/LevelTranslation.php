<?php

namespace App\Models;

use Input;

class LevelTranslation extends BaseModels
{


    protected static $levelTranslationLists = null;
    protected $table = 'level_translations';
    protected $fillable = [
        'code_name', 'experience', 'name_cn', 'name_en'
    ];

    public static function boot()
    {
        parent::boot();
//        static::saving(function ($m) {
//            $m->tag = snake_case($m->name_en);
//        });
    }

    public function scopeGetLevelTranslation($query)
    {
        return $query;
    }

    public function getName()
    {
        $field = 'name_' . app()->getLocale();

        return $this->$field;
    }

    public function getNames()
    {
        $name_cn = $this->name_cn;
        $name_en = $this->name_en;
        return $name_cn . ' / ' . $name_en;
    }

    public function setNameCnAttribute($value)
    {
        $this->attributes['name_cn'] = trim($value);
    }

    public function setNameEnAttribute($value)
    {
        $this->attributes['name_en'] = trim($value);
    }

    public function scopeGetFilteredResults($query)
    {
        if (Input::has('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::has('filter_name') && Input::get('filter_name') != '') {
            $query->where('name_cn', 'like', '%' . Input::get('filter_name') . '%')
                ->orWhere('name_en', 'like', '%' . Input::get('filter_name') . '%');
        }

        if (Input::has('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::has('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::has('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::has('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }
    }

}