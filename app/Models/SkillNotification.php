<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SkillNotification extends Model
{
    protected $table = 'skill_notification_record';

    protected $fillable = [
        'project_id',
        'user_id',
        'skills_id',
    ];
}
