<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SharedOfficeProductAvailability extends Model
{
    protected $table = 'shared_office_product_availability';

    protected $fillable = [
        'office_id',
        'category_id',
        'availability'
    ];

    public function product()
    {
        return $this->hasMany(SharedOfficeProducts::class, 'availability_id');
    }
}
