<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoinProject extends Model
{
    protected $table = 'coin_projects';

    protected $fillable = ['project_id','coin_type'];

    public function project() {
        return $this->belongsTo('App\Models\Project', 'project_id', 'id');
    }
}
