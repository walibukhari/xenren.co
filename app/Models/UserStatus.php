<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserStatus extends Model
{
    protected $table = 'user_status';

    protected $appends = ['user_status'];


    public function getUserStatusAttribute()
    {
        if($this->attributes['status_id'] == 1){
            return "Look for Projects";
        } else if ($this->attributes['status_id'] == 2){
            return "Look for CoFounder";
        } else if ($this->attributes['status_id'] == 3){
            return "Look for Full Time Job";
        } else if ($this->attributes['status_id'] == 4){
            return "Any Status";
        } else {
            return "";
        }
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
