<?php

namespace App\Models;

use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\Model;

class UserXencoinDetail extends Model
{

    protected $table = 'user_xencoin_detail';
    protected $fillable = [
        'user_id',
        'upline_id',
        'type',
        'status'
    ];

    const GENERATE_LINK = 'link';
    const INVITE_LINK = 'inviteLink';
    const QR_LINK = 'qr';
    const INVITED_STATUS = 0;
    const SIGNED_UP = 1;
    const USER_VERIFIED = 2;

    public function createRecord($obj){
        return $this->create([
            'user_id' =>$obj['user_id'],
            'upline_id' =>$obj['upline_id'],
            'type' =>$obj['type'],
            'status' =>$obj['status']
        ]);
    }
    public function updateRecord($obj, $id){
        return $this->where('id', '=', $id)->update([
            'user_id' =>$obj['user_id'],
            'upline_id' =>$obj['upline_id'],
            'type' =>$obj['type'],
            'status' =>$obj['status']
        ]);
    }
    public function deleteRecord($id)
    {
        return $this->where('id', '=', $id)->delete();
    }
    public function getRecord()
    {
        return $this->get();
    }

}
