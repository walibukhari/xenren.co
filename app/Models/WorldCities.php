<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Middleware\SharedOffice;

class WorldCities extends Model
{
    protected $table = 'world_cities';

    protected $fillable = [
        'country_id',
        'division_id',
        'name',
        'full_name',
        'code',
        'state_id'
    ];

    public $timestamps = false;

    public function getCodeAttribute($val)
    {
        return strtoupper($val);
    }

    public function locale()
    {
        return $this->hasOne(WorldCitiesLocale::class, 'city_id', 'id');
    }

    public function state()
    {
        return $this->hasOne(WorldDivisions::class, 'country_id', 'country_id');
    }

    public function country()
    {
        return $this->hasOne(WorldCountries::class, 'id', 'country_id');
    }

    public function sharedoffices()
    {
        return $this->hasOne(SharedOffice::class, 'city_id');
    }
}
