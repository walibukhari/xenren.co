<?php

namespace App\Models;

use App\Constants;
use Carbon\Carbon;
use Input;

class UserInbox extends BaseModels {

    const TYPE_PROJECT_APPLICANT_SELECTED = 1;
    const TYPE_PROJECT_NEW_MESSAGE = 2;
    const TYPE_PROJECT_CONFIRMED_COMPLETED = 3;
    const TYPE_PROJECT_SUCCESSFUL_POST_JOB = 4;
    const TYPE_PROJECT_PEOPLE_COME_IN = 5;
    const TYPE_PROJECT_INVITE_YOU = 6;
    const TYPE_PROJECT_ASK_FOR_CONTACT = 7;
    const TYPE_PROJECT_APPLICANT_ACCEPTED = 8;
    const TYPE_PROJECT_APPLICANT_REJECTED = 9;
    const TYPE_PROJECT_SEND_OFFER = 10;
		const TYPE_PROJECT_AGREE_SEND_CONTACT = 11;
		const TYPE_PROJECT_REJECT_SEND_CONTACT = 12;
		const TYPE_PROJECT_INVITE_ACCEPT = 13;
		const TYPE_PROJECT_INVITE_REJECT = 14;
		const TYPE_PROJECT_ACCEPT_SEND_OFFER = 15;
		const TYPE_PROJECT_REJECT_SEND_OFFER = 16;
		const TYPE_CHAT_FROM_APP = 18;
		const TYPE_PROJECT_RECEIVE_OFFER = 19;
		const TYPE_PROJECT_SKILL_NOTIFICATION = 20;

		const ACTION_MARK_AS_READ = 1;
    const ACTION_DELETE = 2;
    const ACTION_UNDELETE = 3;
    const ACTION_REMOVE = 4;

    const CATEGORY_ALL = 0;
    const CATEGORY_NORMAL = 1;
    const CATEGORY_SYSTEM = 2;

    protected $latestMessage = null;

    protected $table = 'users_inbox';

    protected $fillable = [
        'category',
        'project_id',
        'from_user_id',
        'to_user_id',
        'from_staff_id',
        'to_staff_id',
        'type',
        'is_trash',
        'daily_update'
    ];

    protected $dates = [];

    public static function boot() {
        parent::boot();
    }

    public function project() {
        return $this->belongsTo('App\Models\Project', 'project_id', 'id');
    }

    public function fromUser() {
        return $this->belongsTo('App\Models\User', 'from_user_id', 'id');
    }

    public function toUser() {
        return $this->belongsTo('App\Models\User', 'to_user_id', 'id');
    }

    public function fromStaff()
    {
        return $this->belongsTo('App\Models\Staff', 'from_staff_id', 'id');
    }

    public function toStaff() {
        return $this->belongsTo('App\Models\Staff', 'to_staff_id', 'id');
    }

    public function messages() {
        return $this->hasMany('App\Models\UserInboxMessages', 'inbox_id', 'id')->orderBy('updated_at', 'DESC');
    }

    public function firstMessage() {
        return $this->hasOne('App\Models\UserInboxMessages', 'inbox_id', 'id')->orderBy('id', 'ASC');
    }

    public function getTitle() {
        return $this->getLatestMessage()->getTitle();
//        switch ($this->type) {
//            case static::TYPE_ORDER_APPLICANT_SELECTED:
//                return trans('order.you_have_been_selected_for_order', ['orderId' => $this->order->order_id]);
//            break;
//            case static::TYPE_ORDER_NEW_MESSAGE:
//                return trans('order.order_have_new_message', ['email' => $this->fromUser->email, 'name' => $this->fromUser->nick_name]);
//            break;
//            case static::TYPE_ORDER_CONFIRMED_COMPLETED:
//                return trans('order.order_has_been_completed_by_creator', ['orderId' => $this->order->order_id]);
//            break;
//        }
    }

    private function getLatestMessage()
    {
        if ($this->latestMessage == null) {
            $this->latestMessage = UserInboxMessages::where('inbox_id', '=', $this->id)
                ->orderBy('updated_at', 'DESC')
                ->first();
        }

        return $this->latestMessage;
    }

    public function getHtmlClass() {
        switch ($this->type) {
            case static::TYPE_PROJECT_APPLICANT_SELECTED:
                return 'panel-info';
                break;
            case static::TYPE_PROJECT_NEW_MESSAGE:
                return 'panel-success';
                break;
            case static::TYPE_PROJECT_CONFIRMED_COMPLETED:
                return 'panel-warning';
                break;
            default:
                return 'panel-default';
                break;
        }
    }

    public static function getInboxActionLists($empty = false) {
        $arr = array();
        if ($arr === true) {
            $arr[] = 'Inbox Actions';
        }
        $arr[static::ACTION_MARK_AS_READ] = trans('common.mark_as_read');
        $arr[static::ACTION_DELETE] = trans('common.delete');

        return $arr;
    }

    public static function getDeleteActionLists($empty = false) {
        $arr = array();
        if ($arr === true) {
            $arr[] = 'Delete Actions';
        }
        $arr[static::ACTION_MARK_AS_READ] = trans('common.mark_as_read');
        $arr[static::ACTION_UNDELETE] = trans('common.undelete');
        $arr[static::ACTION_REMOVE] = trans('common.remove');

        return $arr;
    }

    public function getLatestInboxMessageDate()
    {
        return $this->getLatestMessage()->getCreatedDate();
    }

    public function getLatestInboxMessageSender()
    {
        return $this->getLatestMessage()->fromUser();
    }

    public function getLatestInboxMessageSenderName()
    {
        if(!empty($this->getLatestMessage()->from_user_id)) {
            $user = User::find($this->getLatestMessage()->from_user_id);
            $name = $user->getName();
            return $name;
        }
        else {
            return trans('common.admin');
        }
    }

    public function getLatestInboxMessageIsRead($isUser, $authId)
    {
    	try {
		    //$isUser (true = user, false = staff)
		    //$authId (if $isUser true, this mean userId, else mean staffId)

		    if ($isUser) {
			    if ($this->getLatestMessage()->to_user_id == $authId) {
				    return $this->getLatestMessage()->is_read;
			    } else if ($this->getLatestMessage()->from_user_id == $authId) {
				    return 1;
			    }
		    } else if (!$isUser) {
			    if ($this->getLatestMessage()->to_staff_id == $authId) {
				    return $this->getLatestMessage()->is_read;
			    } else if ($this->getLatestMessage()->from_staff_id == $authId) {
				    return 1;
			    }
		    }
	    }catch (\Exception $e) {
    		dump($e->getMessage());
    		dump($e->getFile());
    		dump($e->getLine());
    		dump($this);
    		dump($this->getLatestMessage());
    		dd($e);
    		return 1;
	    }
    }

    public function isNormalMessage()
    {
        $result = false;
        if( $this->category == $this::CATEGORY_NORMAL && $this->is_trash == 0 )
        {
            $result = true;
        }
        return $result;
    }

    public function isSystemMessage()
    {
        $result = false;
        if( $this->category == $this::CATEGORY_SYSTEM && $this->is_trash == 0 )
        {
            $result = true;
        }
        return $result;
    }

    public function isTrash()
    {
        $result = false;
        if( $this->is_trash == 1 )
        {
            $result = true;
        }
        return $result;
    }

    public function getSenderInfo()
    {
        if( isset($this->from_user_id) && $this->from_user_id >= 0 )
        {
            $user = User::find($this->from_user_id);
            $name = $user->getName();
            $avatar = $user->getAvatar();
            $id = $user->id;
        }
        else if( isset($this->from_staff_id) && $this->from_staff_id >= 0 )
        {
            $staff = Staff::find($this->from_staff_id);
            $name = $staff->name;
            $avatar = $staff->getAvatar();
	          $id = $staff->id;
        }

        $result = array(
            'name' => $name,
            'avatar' => $avatar,
            'id' => $id,
        );

        return $result;
    }

    public function getReceiverInfo()
    {
        if( isset($this->to_user_id) && $this->to_user_id >= 0 )
        {
            $user = User::where('id','=',$this->to_user_id)->first();
            if(!is_null($user)) {
	            $name = $user->getName();
	            $avatar = $user->getAvatar();
            } else {
	            $name = '--';
	            $avatar = '--';
            }
        }
        else if( isset($this->from_staff_id) && $this->from_staff_id >= 0 )
        {
            $staff = Staff::where('id','=',$this->from_staff_id)->first();
            if(!is_null($staff)) {
	            $name = $staff->name;
	            $avatar = $staff->getAvatar();
            } else {
	            $name = '--';
	            $avatar = '--';
            }
        }

        $result = array(
            'name' => $name,
            'avatar' => $avatar
        );

        return $result;
    }

    public function getReceiver($isUser, $senderId)
    {
        $user_id = 0;
        $staff_id = 0;

        if( $isUser )
        {
            $user_id = $senderId;
        }
        else
        {
            $staff_id = $senderId;
        }

        if( $this->project_id != null )
        {
            $project = Project::find($this->project_id);
            if(!is_null($project)) {
                if ($project->type == Project::PROJECT_TYPE_COMMON) {
                    if ($this->category == UserInbox::CATEGORY_NORMAL) {
                        //user to user
                        if ($this->from_user_id != 0 && $this->to_user_id != 0) {
                            if ($this->from_user_id == $user_id) {
                                $receiver_id = $this->to_user_id;
                            } else if ($this->to_user_id == $user_id) {
                                $receiver_id = $this->from_user_id;
                            }

                            $user = User::find($receiver_id);
                            $name = $user->getName();
                            $avatar = $user->getAvatar();
                            $this_isUser = true;
                        }
                    } else if ($this->category == UserInbox::CATEGORY_SYSTEM) {
                        if ($this->from_staff_id != 0 && $this->to_user_id != 0) {
                            //admin to user
                            if ($this->from_staff_id == $staff_id) {
                                $receiver_id = $this->to_user_id;
                                $user = User::find($receiver_id);
                                $name = $user->getName();
                                $avatar = $user->getAvatar();
                                $this_isUser = true;
                            } else if ($this->to_user_id == $user_id) {
                                $receiver_id = $this->from_staff_id;
                                $staff = Staff::find($receiver_id);
                                $name = $staff->name;
                                $avatar = $staff->getAvatar();
                                $this_isUser = false;
                            }
                        } else if ($this->from_user_id != 0 && $this->to_user_id != 0) {
                            //user to user
                            //project complete notification
                            if ($this->from_user_id == $user_id) {
                                $receiver_id = $this->to_user_id;
                            } else if ($this->to_user_id == $user_id) {
                                $receiver_id = $this->from_user_id;
                            }

                            $user = User::find($receiver_id);
                            $name = $user->getName();
                            $avatar = $user->getAvatar();
                            $this_isUser = true;
                        }
                    }

                } else if ($project->type == Project::PROJECT_TYPE_OFFICIAL) {
                    //admin to user or user to admin
                    if ($this->from_staff_id != 0 && $this->to_user_id != 0) {
                        if ($this->from_staff_id == $staff_id) {
                            $receiver_id = $this->to_user_id;
                            $user = User::find($receiver_id);
                            $name = $user->getName();
                            $avatar = $user->getAvatar();
                            $this_isUser = true;
                        } else if ($this->to_user_id == $user_id) {
                            $receiver_id = $this->from_staff_id;
                            $staff = Staff::find($receiver_id);
                            $name = $staff->name;
                            $avatar = $staff->getAvatar();
                            $this_isUser = false;
                        }
                    } else if ($this->from_user_id != 0 && $this->to_staff_id != 0) {
                        if ($this->from_user_id == $user_id) {
                            $receiver_id = $this->to_staff_id;
                            $staff = Staff::find($receiver_id);
                            $name = $staff->name;
                            $avatar = $staff->getAvatar();
                            $this_isUser = false;
                        } else if ($this->to_staff_id == $user_id) {
                            $receiver_id = $this->from_user_id;
                            $user = User::find($receiver_id);
                            $name = $user->getName();
                            $avatar = $user->getAvatar();
                            $this_isUser = true;
                        }
                    }

                }
            } else {
                return 'N/A';
            }
        }
        else
        {

            if( $this->from_user_id != 0 && $this->to_user_id != 0 )
            {
                //user ask for contact not need to give project id
                //only user to user
                if( $this->from_user_id == $user_id )
                {
                    $receiver_id = $this->to_user_id;
                }
                else if($this->to_user_id == $user_id)
                {
                    $receiver_id = $this->from_user_id;
                }

                $user = User::find($receiver_id);
                $name = $user->getName();
                $avatar = $user->getAvatar();
                $this_isUser = true;
            }
            else if( $this->from_staff_id != 0 && $this->to_user_id != 0 )
            {
                //admin send message to user from admin panel manage user
                $receiver_id = $this->to_user_id;
                $user = User::find($receiver_id);
                $name = $user->getName();
                $avatar = $user->getAvatar();
                $this_isUser = true;
            }

        }


        $result = array(
            'name' => $name,
            'avatar' => $avatar,
            'id' => $receiver_id,
            'isUser' => $this_isUser
        );

        return $result;
    }

    public function getMyMessages()
    {
        return $this->where('from_user_id', '=', \Auth::user()->id)->orWhere('to_user_id', '=', \Auth::user()->id)->get();
    }

}
