<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserOtherTypeBarcodeScans extends Model
{
    protected $table = 'user_other_type_barcode_scans';

    protected $fillable = [
        'user_id',
        'shared_office_barcode_id'
    ];

    public function sharedOfficeBarCode() {
        return $this->hasOne('App\Models\SharedOfficeBarcode', 'id', 'shared_office_barcode_id');
    }
}
