<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SharedOfficeChatAction extends Model
{
    CONST KICKED_USER_IN_CHAT = 0;
    CONST BLOCKED_USER_IN_CHAT = 1;

    protected $fillable = [
        'user_id',
        'office_id',
        'action',
    ];
}
