<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeedbackFile extends Model
{
    protected $table = 'feedback_files';

    public function feedback() {
        return $this->belongsTo('App\Models\Feedback', 'feedback_id', 'id');
    }

    public function getFile() {
        if ($this->file != null && $this->file != '') {
            return asset($this->file);
        } else {
            return null;
        }
    }

    public function setFileAttribute($file)
    {
        if ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
            $img = \Image::make($file);
            $mime = $img->mime();
            $ext = convertMimeToExt($mime);

            $path = 'uploads/feedback/' . $this->feedback->id . '/';
            $filename = generateRandomUniqueName();

            touchFolder($path);

            $img = $img->save($path . $filename . $ext);

            $this->attributes['file'] = $path . $filename . $ext;
        }
    }
}
