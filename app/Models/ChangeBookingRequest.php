<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChangeBookingRequest extends Model
{
    const STATUS_DEFAULT = 0;
    const STATUS_CONFIRM = 1;
    const STATUS_CANCEL = 2;

    protected $fillable = [
        'book_id' , 'office_id' , 'seat_id' , 'user_id' , 'check_in' , 'check_out' , 'status' , 'currant_seat' , 'change_seat'
    ];

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

    public function office(){
        return $this->hasOne(SharedOffice::class,'id','office_id');
    }

    public function product(){
        return $this->hasOne(SharedOfficeProducts::class,'id','seat_id');
    }

    public function booking(){
        return $this->hasOne(SharedOfficeBooking::class,'id','book_id');
    }
}
