<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserActivity extends Model
{
    protected $fillable = [
        'user_id',
        'username',
        'email',
        'we_chat',
        'line',
        'skype',
        'phone_number',
        'performed_activity',
        'amount',
        'project_name',
        'project_type',
        'country',
        'city',
        'search',
        'received_offer',
        'send_offer',
        'request_contact_info',
        'send_message',
        'receive_message',
        'register_user',
        'logout_time',
        'register_user_verify_code',
        'login_time',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
