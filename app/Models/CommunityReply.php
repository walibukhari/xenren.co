<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommunityReply extends Model
{
    protected $fillable = [
      'topic_id','topic_type','user_id','comment','likes','notify_me','discuss_id','reply_id'
    ];

    public function getQuotes($r_id) {
        $r_data = self::where('reply_id','=',$r_id)->first();
        if($r_data->discuss_id == $r_id) {
            $data = CommunityDiscussion::where('id','=',$r_data->discuss_id)->first();
            $data = $data->description;
        } else {
            $data = self::where('id','=',$r_data->reply_id)->first();
            $data = $data->comment;
        }
        return $data;
    }

    public function topic(){
        return $this->hasOne(Topic::class,'id','topic_id');
    }

    public function discussion(){
        return $this->hasOne(CommunityDiscussion::class,'id','discuss_id');
    }

    public function like(){
        return $this->hasMany(CommunityLikes::class,'reply_id','id');
    }

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

    public function getLikesCount($likes){
        $count = [];
        foreach ($likes as $topics) {
            if($topics->likes > 0) {
                array_push($count,$topics->likes);
            }
        }
        return count($count);
    }

    public  function getUser($userId,$reply_id) {
        return CommunityLikes::where('user_id','=',$userId)->where('reply_id','=',$reply_id)->first();
    }
}
