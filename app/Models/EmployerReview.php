<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployerReview extends Model
{
    const RATE_PAYMENT_ON_TIME = 1;
    const RATE_COMMUNICATION = 2;
    const RATE_AVAILABILITY = 3;
    const RATE_TRUSTWORTHY = 4;
    const RATE_CLEAR_REQUIREMENT = 5;

    const REASON_PROJECT_COMPLETED = 1;
    const REASON_NO_RESPONSE = 2;
    const REASON_PROJECT_CANCELLED = 3;
    const REASON_TERMS_AND_CONDITION_CHANGED = 4;

    const LP_DIFFICULT_TO_UNDERSTAND = 1;
    const LP_ACCEPTABLE = 2;
    const LP_FLUENT = 3;
    const LP_I_DIDNT_SPEAK = 4;

    public static function getReasonLists($empty = false) {
        $arr = array();
        if ($arr === true) {
            $arr[] = 'End Contract Reason';
        }
        $arr[static::REASON_PROJECT_COMPLETED] = trans('common.project_completed');
        $arr[static::REASON_NO_RESPONSE] = trans('common.no_response');
        $arr[static::REASON_PROJECT_CANCELLED] = trans('common.project_cancelled');
        $arr[static::REASON_TERMS_AND_CONDITION_CHANGED] = trans('common.terms_and_condition_changed');

        return $arr;
    }

    public static function getEnglishProficiencyLists($empty = false) {
        $arr = array();
        if ($arr === true) {
            $arr[] = 'English Proficiency';
        }
        $arr[static::LP_DIFFICULT_TO_UNDERSTAND] = trans('common.difficult_to_understand');
        $arr[static::LP_ACCEPTABLE] = trans('common.acceptable');
        $arr[static::LP_FLUENT] = trans('common.fluent');
        $arr[static::LP_I_DIDNT_SPEAK] = trans('common.i_didnt_speak_to', ['who' => trans('common.client')]);

        return $arr;
    }

    public static function getRateCategoryLists($empty = false) {
        $arr = array();
        if ($arr === true) {
            $arr[] = 'Rate Category';
        }
        $arr[static::RATE_PAYMENT_ON_TIME] = trans('common.payment_on_time');
        $arr[static::RATE_COMMUNICATION] = trans('common.communication');
        $arr[static::RATE_AVAILABILITY] = trans('common.availability');
        $arr[static::RATE_TRUSTWORTHY] = trans('common.trustworthy');
        $arr[static::RATE_CLEAR_REQUIREMENT] = trans('common.clear_requirement');

        return $arr;
    }
}
