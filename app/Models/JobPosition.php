<?php

namespace App\Models;

use Input;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobPosition extends BaseModels {
    protected $table = 'job_position';

    protected static $jobPositionLists = null;

    protected $fillable = [
        'name_cn', 'name_en', 'remark_cn', 'remark_en'
    ];

    protected $dates = [];

    public static function boot() {
        parent::boot();
    }

    public function scopeGetJobPosition($query) {
        return $query;
    }

    public function getName() {
        $field = 'name_' . app()->getLocale();

        return $this->$field;
    }

    public function getNames()
    {
        return $this->name_cn . ' / ' . $this->name_en;
    }

    public function getRemark() {
        $field = 'remark_' . app()->getLocale();

        return $this->$field;
    }

    public function getRemarks()
    {
        return $this->remark_cn . ' / ' . $this->remark_en;
    }

    public function scopeGetFilteredResults($query) {
        if (Input::has('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::has('filter_name') && Input::get('filter_name') != '') {
            $query->where('name_cn', 'like', '%' . Input::get('filter_name') . '%')
                ->orWhere('name_en', 'like', '%' . Input::get('filter_name') . '%');
        }

        if (Input::has('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::has('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::has('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::has('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }
    }

}
