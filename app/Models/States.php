<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class States extends Model
{
    protected $table = 'country_states';

    protected $fillable = [
        'id',
        'name',
        'country_id'
    ];

    public function country()
    {
        return $this->hasOne(Countries::class, 'id', 'country_id');
    }
}
