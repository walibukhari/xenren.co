<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlockedUser extends Model
{
    protected $table = 'blocked_users';

    protected $fillable = [
        'qq',
        'skype',
        'wechat',
        'phone',
        'from',
        'to',
        'block_chat',
        'is_blocked',
    ];

    const Inactive = 0;
    const Platform_Active = 1;
    const All_Active = 2;

    const Block_Chat_True = 1;
    const Block_Chat_False = 0;

    public function toUser()
    {
        return $this->hasOne('App\Models\User', 'id', 'to');
    }

    public function HideContactInfo($request, $id)
    {

        $input = [
            'qq' => isset($request->platform['qq']) && $request->platform['qq'] == 1 ? self::Platform_Active : (isset($request->all_users['qq']) && $request->all_users['qq'] == 1 ? self::All_Active : self::Inactive),
            'skype' => isset($request->platform['skype']) && $request->platform['skype'] == 1 ? self::Platform_Active : (isset($request->all_users['skype']) && $request->all_users['skype'] == 1 ? self::All_Active : self::Inactive),
            'wechat' => isset($request->platform['wechat']) && $request->platform['wechat'] == 1 ? self::Platform_Active : (isset($request->all_users['wechat']) && $request->all_users['wechat'] == 1 ? self::All_Active : self::Inactive),
            'phone' => isset($request->platform['phone']) && $request->platform['phone'] == 1 ? self::Platform_Active : (isset($request->all_users['phone']) && $request->all_users['phone'] == 1 ? self::All_Active : self::Inactive),
            'from' => \Auth::user()->id,
            'to' => $id,
            'block_chat' => self::Block_Chat_False,
        ];
        $input = array_filter($input, 'strlen');
        return $this->create($input);
    }

    public function BlockUserFromChat($request)
    {
        $input = [
            'from' => \Auth::user()->id,
            'to' => $request->user_id,
            'block_chat' => self::Block_Chat_True,
        ];
        $input = array_filter($input, 'strlen');
        return $this->create($input);
    }

    public function GetMyBlockedUsers()
    {
        return $this
            ->where('from', '=', \Auth::user()->id)
            ->whereNotNull('qq')
            ->select(['id', 'to', 'from', 'phone', 'qq', 'skype', 'wechat'])
            ->with('toUser')
            ->get();
    }

    public function GetMyBlockedContactInfoForAllUsers()
    {
        return $this
            ->where('from', '=', \Auth::user()->id)
            ->where('qq', '=', self::All_Active)
            ->orWhere('skype', '=', self::All_Active)
            ->orWhere('phone', '=', self::All_Active)
            ->orWhere('wechat', '=', self::All_Active)
            ->get();
    }

    public function blockSingleUser($request, $userid)
    {
        $inputData = [
            'qq' => $request->qq,
            'skype' => $request->skype,
            'wechat' => $request->wechat,
            'phone' => $request->phone,
            'from' => \Auth::user()->id,
            'to' => $userid,
            'block_chat' => $request->block_chat,
            'is_blocked' => 1
        ];
        return $this->create($inputData);
    }



}
