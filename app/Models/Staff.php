<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Validator;
use Input;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Staff extends BaseModels implements
    JWTSubject,
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword ,SoftDeletes;

    const STAFF_TYPE_ADMIN = 1;
    const STAFF_TYPE_STAFF = 2;
    const STAFF_TYPE_IS_OPERATOR = 1;
    const Shared_OFFICE_TITLE = 'Shared Office Owner';
    const STAFF_ACTIVE_PUSH_NOTIFICATION = 1;
    const STAFF_DE_ACTIVE_PUSH_NOTIFICATION = 0;

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    protected static $userPermissions = null;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'staff';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'title', 'img_avatar','balance','device_token','device_os','country_id', 'office_id','phone_number'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['deleted_at'];

    public static function boot() {
        parent::boot();
    }

    public static function defaultUserType() {
        return static::STAFF_TYPE_STAFF;
    }

    public function getDefaultUserType() {
        return static::defaultUserType();
    }

    public function permissionGroup() {
        return $this->hasOne('App\Models\PermissionGroup', 'id', 'permission_group_id');
    }

    public function setPasswordAttribute($value) {
        $this->attributes['password'] = bcrypt($value);
    }

    public function setEmailAttribute($value) {
        $this->attributes['email'] = trim(strtolower($value));
    }

    public function setUsernameAttribute($value) {
        $this->attributes['username'] = trim(strtolower($value));
    }

    public function isAdmin() {
        return $this->staff_type == static::STAFF_TYPE_ADMIN;
    }

    public function isStaff() {
        return $this->staff_type == static::STAFF_TYPE_ADMIN || $this->staff_type == static::STAFF_TYPE_STAFF;
    }

    public function scopeGetStaff($query) {
        return $query->where('staff_type', '=', static::getDefaultUserType());
    }

    public function getPermissions() {
        if (static::$userPermissions === null && static::$userPermissions !== false) {
            $this->load(['permissionGroup', 'permissionGroup.permissions']);
            if ($this->permission_group_id != null && $this->permissionGroup) {
                $lists = PermissionGroupPermission::getPermissionsLists();
                $arr = array();

                foreach ($this->permissionGroup->permissions as $key => $var) {
                    if (isset($lists[$var->permission_tag])) {
                        $arr[$var->permission_tag] = $lists[$var->permission_tag];
                    } else {
                        $arr[$var->permission_tag] = sprintf('Unknown Permission %s', $var->permission_tag);
                    }
                }

                static::$userPermissions = $arr;
            } else {
                static::$userPermissions = false;
            }
        }

        return static::$userPermissions;
    }

    public function hasPermission($permission) {
        $permission = strtolower($permission);

        if ($this->isAdmin()) {
            return true;
        }

        if (!$this->isStaff()) {
            return false;
        }

        $my_permissions = $this->getPermissions();

        if ($my_permissions == false || $my_permissions == null) {
            return false;
        }

        if (isset($my_permissions[$permission])) {
            return true;
        }

        return false;
    }


    public static function getCurrencyName($id){
        $staff = WorldCountries::where('id','=',$id)->first();
        return $staff->currency_code;
    }

    public function scopeGetFilteredResults($query) {
        if (Input::has('filter_row_id') && Input::get('filter_row_id') != '') {
            $query->where('id', '=', Input::get('filter_row_id'));
        }

        if (Input::has('filter_username') && Input::get('filter_username') != '') {
            $query->where('username', 'like', "%" . Input::get('filter_username') . "%");
        }

        if (Input::has('filter_name') && Input::get('filter_name') != '') {
            $query->where('name', 'like', "%" . Input::get('filter_name') . "%");
        }

        if (Input::has('filter_email') && Input::get('filter_email') != '') {
            $query->where('email', 'like', "%" . Input::get('filter_email') . "%");
        }

        if (Input::has('filter_group_name') && Input::get('filter_group_name') != '') {
            $query->whereHas('permissionGroup', function ($q) {
                $q->where('group_name', 'like', '%' . Input::get('filter_group_name') . '%');
            });
        }

        if (Input::has('filter_created_after') && Input::get('filter_created_after') != '') {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::has('filter_created_before') && Input::get('filter_created_before') != '') {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::has('filter_updated_after') && Input::get('filter_updated_after') != '') {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::has('filter_updated_before') && Input::get('filter_updated_before') != '') {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }

        if (Input::has('filter_status') && Input::get('filter_status') != '') {
            if (Input::get('filter_status') == 1) {
                $query->where('deleted_at', '=', NULL);
            } else if (Input::get('filter_status') == 2) {
                $query->whereNotNull('deleted_at');
            }
        }

        return $query;
    }

    public function office() {
        return $this->hasOne(SharedOffice::class,'staff_id','id');
    }

    public function officeStaff() {
        return $this->hasOne(SharedOffice::class,'id','office_id');
    }

    public function getAvatar() {
        if ($this->img_avatar == '' || $this->img_avatar == null) {
            return asset('images/avatar_xenren.png');
        } else {
            return asset($this->img_avatar);
        }
    }
}
