<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Input;

class Sliders extends BaseModels
{

    protected $table = 'sliders';

    protected $fillable = ['title','sub_title', 'link', 'image'];

    public static function boot() {
        parent::boot();
    }

    public function stuff() {
        return $this->belongsTo('App\Models\Staff', 'staff_id', 'id');
    }
}
