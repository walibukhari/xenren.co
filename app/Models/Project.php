<?php

namespace App\Models;


use App\Constants;
use Carbon\Carbon;
use function foo\func;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Input;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends BaseModels
{
    use SoftDeletes;
    const STATUS_PUBLISHED = 1;
    const STATUS_SELECTED = 2;
    const STATUS_HIRED = 3; //useless bcos can hire more than one applicant for each project
    const STATUS_COMPLETED = 4;
    const STATUS_CANCELLED = 5;
    const STATUS_DRAFT = 6;

    const PROJECT_TYPE_COMMON = 1;
    const PROJECT_TYPE_OFFICIAL = 2;

    const PAY_TYPE_HOURLY_PAY = 1;
    const PAY_TYPE_FIXED_PRICE = 2;
    const PAY_TYPE_MAKE_OFFER = 3;

    const Keyword_Country = 1;
    const Keyword_Language = 2;
    const Keyword_Hourly_Range = 3;

    const HOURLY_RANGE_1_10 = 1;
    const HOURLY_RANGE_11_20 = 2;
    const HOURLY_RANGE_21_30 = 3;
    const HOURLY_RANGE_31_40 = 4;
    const HOURLY_RANGE_41_50 = 5;
    const HOURLY_RANGE_EQUAL_OR_MORE_THAN_51 = 6;

    const IS_READ_NO = 0;
    const IS_READ_YES = 1;

    const FREELANCER_TYPE_ANYBODY = 1;
    const FREELANCER_TYPE_LOGIN_USER = 2;
    const FREELANCER_TYPE_INVITED_USER = 3;

    const JOB_LIKED = 1;
    const JOB_DISLIKED = 2;
    const JOB_NONE = 3;

    protected static $projectLists = null;
    protected $table = 'project';
    protected $fillable = [
        'user_id',
        'name',
        'category_id',
        'country_id',
        'language_id',
        'description',
        'attachment',
        'pay_type',
        'time_commitment',
        'freelance_type',
        'question',
        'daily_update',
        'reason_to_end',
        'nearby_place',
        'weekly_limit',
        'country_id',
        'duration',
    ];

    protected $dates = ['created_at', 'updated_at'];

    public function getProjectType($type) {
        if($type == self::PAY_TYPE_FIXED_PRICE) {
            return 'Fixed Price';
        } else if($type == self::PAY_TYPE_HOURLY_PAY) {
            return 'Hourly Price';
        } else {
            return 'Make Offer';
        }
    }

    public static function boot()
    {
        parent::boot();
    }

    public function scopeGetProject($query)
    {
        return $query;
    }

    public function scopeGetUserProject($query)
    {
        return $query->where('user_id', '=', \Auth::guard('users')->user()->id);
    }

    public function review()
    {
        return $this->hasOne(Review::class, 'project_id', 'id');
    }

    public function setAttachmentAttribute($file)
    {
        if ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
            $mime = $file->getClientOriginalExtension();

            $path = 'uploads/project/' . $this->id . '/';
            $filename = generateRandomUniqueName();

            touchFolder($path);

            $file = $file->move($path, $filename . '.' . $mime);

            $this->attributes['attachment'] = $path . $filename . '.' . $mime;
        }
    }

    public function getAttachment()
    {
        if ($this->attachment == '' || $this->attachment == null) {
            return '';
        } else {
            return asset($this->attachment);
        }
    }

    public function cleanTheLink($val)
    {
        $a =  str_replace("/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s",' ',$val);
        $a = iconv("UTF-8", "ASCII//TRANSLIT", $a);
        $a = str_replace('?' , '', $a);
        $a = str_replace('>' , '', $a);
        $a = str_replace('<' , '', $a);
        $a = str_replace('^' , '', $a);
        $a = str_replace("’", ' ', $a);
        $a = str_replace("-", ' ', $a);
        $a = str_replace(":", ' ', $a);
        $a = str_replace("$", ' ', $a);
        $a = str_replace("/", ' ', $a);
        $a =  str_replace("%", ' ', $a);
        $a =  str_replace("@", ' ', $a);
        $a =  str_replace("#", ' ', $a);
        $a =  str_replace("&", ' ', $a);
        $a =  str_replace("*", ' ', $a);
        $a =  str_replace("(", ' ', $a);
        $a =  str_replace(")", ' ', $a);
        $a =  str_replace("-", ' ', $a);
        $a =  str_replace("+", ' ', $a);
        $a =  str_replace("=", ' ', $a);
        $a =  str_replace("{", ' ', $a);
        $a =  str_replace("}", ' ', $a);
        $a =  str_replace("[", ' ', $a);
        $a =  str_replace(",", ' ', $a);
        $a =  str_replace("]", ' ', $a);
        $a =  str_replace("|", ' ', $a);
        $a =  str_replace("'", ' ', $a);
        $a =  str_replace(":", ' ', $a);
        $a =  str_replace(";", ' ', $a);
        $a =  str_replace("!", ' ', $a);
        $a =  str_replace(')' , '', $a);
        $a =  str_replace('.' , '', $a);
        $a = trim($a);
        return preg_replace('!\s+!', '-', $a);
    }

    public function getSlug()
    {
        return self::cleanTheLink(strtolower(str_replace(' ', '-', $this->name)));
    }

    public function timeTracked()
    {
        return $this->hasMany(TimeTrack::class, 'project_id', 'id');
    }

    public function scopeGetFilteredResults($query)
    {
        if (Input::has('filter_id') && Input::get('filter_id') != '') {
            $query->where('id', '=', Input::get('filter_id'));
        }

        if (Input::has('filter_name') && Input::get('filter_name') != '') {
            $query->where('name', 'like', '%' . Input::get('filter_name') . '%');
        }

        if (Input::has('filter_created_after')) {
            $query->where('created_at', '>=', Input::get('filter_created_after'));
        }

        if (Input::has('filter_created_before')) {
            $query->where('created_at', '<=', Input::get('filter_created_before'));
        }

        if (Input::has('filter_updated_after')) {
            $query->where('updated_at', '>=', Input::get('filter_updated_after'));
        }

        if (Input::has('filter_updated_before')) {
            $query->where('updated_at', '<=', Input::get('filter_updated_before'));
        }
    }

    public function userInbox()
    {
        return $this->hasMany('App\Models\UserInbox', 'project_id', 'id');
    }

    public function creator()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function explainPayType()
    {
        return \App\Constants::translatePayType($this->pay_type);
    }

    public function projectSkills()
    {
        return $this->hasMany('App\Models\ProjectSkill', 'project_id', 'id');
    }

    public function coinProject()
    {
        return $this->hasOne('App\Models\CoinProject', 'project_id', 'id');
    }

    public function projectQuestions()
    {
        return $this->hasMany('App\Models\ProjectQuestion', 'project_id', 'id');
    }

    public function projectCountries()
    {
        return $this->hasMany('App\Models\ProjectCountry', 'project_id', 'id');
    }

    public function projectLocation()
    {
        return $this->hasOne(CountryCities::class, 'id', 'nearby_place');
    }

    public function getLocation()
    {
        $loc = WorldCountries::where('id', '=', $this->country_id)->first();
        if(!is_null($loc)) {
            return $loc->name;
        } else {
            return 'N/A';
        }
    }

    public function getValue()
    {
        $op = OfficialProject::where('project_id', '=', $this->id)->first();
        if(!is_null($op)) {
            return $op->stock_total;
        } else {
            return 'N/A';
        }
    }

    public function getContract()
    {
        $op = OfficialProject::where('project_id', '=', $this->id)->first();
        return asset($op->contract_file);
    }

    public function getStage()
    {
        $op = OfficialProject::where('project_id', '=', $this->id)->first();
        if(!is_null($op)) {
            $stage = $op->funding_stage;
            if($stage == OfficialProject::funding_stage_seed) {
                return 'seed';
            }
            if($stage == OfficialProject::funding_stage_angle) {
                return 'angle';
            }
            if($stage == OfficialProject::funding_stage_pre_a) {
                return 'pre a';
            }
            if($stage == OfficialProject::funding_stage_a) {
                return 'stage a';
            }
            if($stage == OfficialProject::funding_stage_b) {
                return 'stage b';
            }
            if($stage == OfficialProject::funding_stage_c) {
                return 'stage c';
            }

        } else {
            return 'N/A';
        }
    }

    public function projectLanguages()
    {
        return $this->hasMany('App\Models\ProjectLanguage', 'project_id', 'id');
    }

    public function applicants()
    {
        return $this->hasMany('App\Models\ProjectApplicant', 'project_id', 'id');
    }

    public function awardedApplicant()
    {
        return $this->hasOne('App\Models\ProjectApplicant', 'project_id', 'id');
    }


    public function projectChatRoom()
    {
        return $this->hasOne('App\Models\ProjectChatRoom', 'project_id', 'id');
    }

    public function officialProject()
    {
        return $this->hasOne('App\Models\OfficialProject', 'project_id', 'id');
    }

    public function thumbDown()
    {
        return $this->hasOne(ThumbdownJob::class, 'project_id', 'id');
    }

    public function projectFiles()
    {
        return $this->hasMany('App\Models\ProjectFile', 'project_id', 'id');
    }


    public function favoriteJob()
    {
        return $this->hasOne(FavouriteJob::class, 'project_id', 'id');
    }

    public function haveApprovedApplicant()
    {
        foreach ($this->applicants as $key => $var) {
            if ($var->status == ProjectApplicant::STATUS_APPLICANT_ACCEPTED) {
                return true;
            }
        }

        return false;
    }

    public function getApprovedApplicant()
    {
        foreach ($this->applicants as $key => $var) {
            if ($var->status == ProjectApplicant::STATUS_APPLICANT_ACCEPTED) {
                return $var->user_id;
            }
        }
    }

    public function firstOrSecondMilestone()
    {
        $projects = self::where('user_id', '=', Auth::user()->id)->with([
            'milestones.milestoneStatus' => function($q){
                $q->where('project_milestones_status.status', '=', ProjectMilestones::STATUS_CONFIRMED);
            }
        ])
            ->whereHas('milestones.milestoneStatus', function($q){
                $q->where('project_milestones_status.status', '=', ProjectMilestones::STATUS_CONFIRMED);
            })
            ->get();
        $milestones = collect();
        foreach ($projects as $k => $v) {
            foreach ($v->milestones as $k1 => $v1) {
                $milestones->push($v1);
            }
        }

        if(isset($milestones) && $milestones->count() > 2) {
            return false;
        } else {
            return true;
        }
    }

    public function getApplicantsCount()
    {
        $result = 0;
        $result = count($this->applicants);

        //-1 is because project creator
        $result = $result - 1;
        if ($result == -1) {
            $result = 0;
        }
        return $result;
    }

    public function getIntervieweesCount()
    {
        $result = 0;

        if ($this->projectChatRoom != null) {
            $result = count($this->projectChatRoom->chatFollower()->get());

            //-1 is because project creator
            $result = $result - 1;
            if ($result == -1) {
                $result = 0;
            }
        }


        return $result;
    }

    public function getHiredApplicantsCount()
    {
        $result = 0;
        foreach ($this->applicants as $key => $var) {
            if ($var->status == ProjectApplicant::STATUS_APPLICANT_ACCEPTED) {
                $result += 1;
            }
        }

        return $result;
    }

    public function orders()
    {
        return $this->hasOne('App\MyOrder');
    }

    public function getProjectCreatedInfo()
    {
        $result = "";
        $language = Session::get('lang');
        if ($language == "cn") {
            Carbon::setLocale('zh');
        } else if ($language == "en") {
            Carbon::setLocale('en');
        } else {
            Carbon::setLocale('zh');
        }
        $humanDateTime = $this->created_at->diffForHumans();

        if ($this->type == Project::PROJECT_TYPE_COMMON) {
            if (\Auth::guard('users')->check() && \Auth::guard('users')->user()->id == $this->user_id) {
                $who = trans('common.you');
            } else {
                $who = $this->creator->getName();
            }
        } else {
            $who = trans('common.admin');
        }

        $result = trans('common.posted_by', [
            'who' => $who,
            'humanDateTime' => $humanDateTime
        ]);

        return $result;
    }

    public function getCreatedAt()
    {
        $language = Session::get('lang');
        if ($language == null) {
            session(['lang' => 'cn']);
            $language = 'cn';
        }

        if ($language == "cn") {
            Carbon::setLocale('zh');
        } else {
            Carbon::setLocale('en');
        }
        $humanDate = $this->created_at->diffForHumans();

        $result = trans('common.posted_at_humanDate', [
            'humanDate' => $humanDate
        ]);

        return $result;
    }

    public function getCreatedAt_v2()
    {
        $language = Session::get('lang');
        if ($language == null) {
            session(['lang' => 'cn']);
            $language = 'cn';
        }

        if ($language == "cn") {
            Carbon::setLocale('zh');
        } else {
            Carbon::setLocale('en');
        }
        $humanDate = $this->created_at->diffForHumans();

        return $humanDate;
    }

    public function getByCreator()
    {
        if ($this->type == Project::PROJECT_TYPE_COMMON) {
            if (\Auth::guard('users')->check() && \Auth::guard('users')->user()->id == $this->user_id) {
                $creator = trans('common.you');
            } else {
                $creator = $this->creator->getName();
            }
        } else {
            $creator = trans('common.admin');
        }

        $result = trans('common.by_creator', [
            'creator' => $creator
        ]);
        return $result;
    }

    public function translateStatus()
    {
        switch ($this->status) {
            case static::STATUS_PUBLISHED:
                return trans('common.published');
            case static::STATUS_SELECTED:
                return trans('common.selected');
            case static::STATUS_HIRED:
                return trans('common.hired');
            case static::STATUS_COMPLETED:
                return trans('common.completed');
            case static::STATUS_CANCELLED:
                return trans('common.cancelled');
            default:
                return '';
        }
    }

    public static function isMyFavouriteJob($project_id)
    {
        // TODO: Comment this
        $user_id = Auth::check() ? Auth::user()->id : 2;

        $count = FavouriteJob::where('user_id', $user_id)
            ->where('project_id', $project_id)
            ->count();

        if ($count >= 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function isMyThumbdownJob($project_id)
    {
        // TODO: Comment this
        $user_id = Auth::check() ? Auth::user()->id : 2;

        $count = ThumbdownJob::where('user_id', $user_id)
            ->where('project_id', $project_id)
            ->count();

        if ($count >= 1) {
            return true;
        } else {
            return false;
        }
    }

    public function isYourFavouriteJob()
    {
        if(\Auth::guard('users')->check())
        {
            $count = FavouriteJob::where('user_id', \Auth::guard('users')->user()->id)
                ->where('project_id', $this->id)
                ->count();
        }
        else {
            $count = FavouriteJob::where('user_id', '')
                ->where('project_id', $this->id)
                ->count();
        }

        if ($count >= 1) {
            return true;
        } else {
            return false;
        }
    }

    public function isYourThumbdownJob()
    {
        $count = ThumbdownJob::where('user_id', \Auth::guard('users')->user()->id)
            ->where('project_id', $this->id)
            ->count();

        if ($count >= 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function getOfficialProjectKeywords($empty = false)
    {
        $arr = array();
        if ($arr === true) {
            $arr[] = 'Official Project Keywords';
        }
        $arr[static::Keyword_Country] = trans('common.country');
        $arr[static::Keyword_Language] = trans('common.language_2');

        return $arr;
    }

    public static function getCommonProjectKeywords($empty = false)
    {
        $arr = array();
        if ($arr === true) {
            $arr[] = 'Common Project Keywords';
        }
        $arr[static::Keyword_Country] = trans('common.country');
        $arr[static::Keyword_Language] = trans('common.language_2');
        $arr[static::Keyword_Hourly_Range] = trans('common.hourly_range');

        return $arr;
    }

    public static function translateHourlyRange($value)
    {
        if (app()->getLocale() == "en") {
            $currency = trans("common.usd");
        } else {
            $currency = trans("common.rmb");
        }

        switch ($value) {
            case static::HOURLY_RANGE_1_10:
                $result = "1 - 10 " . $currency;
                break;
            case static::HOURLY_RANGE_11_20:
                $result = "11 - 20 " . $currency;
                break;
            case static::HOURLY_RANGE_21_30:
                $result = "21 - 30 " . $currency;
                break;
            case static::HOURLY_RANGE_31_40:
                $result = "31 - 40 " . $currency;
                break;
            case static::HOURLY_RANGE_41_50:
                $result = "41 - 50 " . $currency;
                break;
            case static::HOURLY_RANGE_EQUAL_OR_MORE_THAN_51:
                $result = "51+ " . $currency;
                break;
        }

        return $result;
    }

    public function getReferencePrice()
    {
        //DB store value in USD
        //if cn language, convert USD to CNY
        //if en language, not need convert, straight retrieve

        if (app()->getLocale() == "en") {
            return '$' . number_format($this->reference_price, 2, '.', ',');
        } else {
            $exchangeRate = ExchangeRate::getExchangeRate(Constants::USD, Constants::CNY);
            $value = $this->reference_price * $exchangeRate;
            return ' ¥' . number_format($value, 2, '.', ',');
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function milestones()
    {
        return $this->hasMany('App\Models\ProjectMilestones', 'project_id', 'id');
    }

    public function disputes()
    {
        return $this->hasMany(ProjectDispute::class, 'project_id', 'id');
    }

    /**
     * @param $projectId
     * @return mixed
     */
    public function getProjectDetailById($projectId, $userId=false)
    {
        \DB::enableQueryLog();
        $query = $this
            ->with([
                'awardedApplicant' => function ($q) {
//                    $q->where('status', '=', 3);
                },
                'awardedApplicant.user.languages.languageDetail',
                'awardedApplicant.user.skills.skill',
                'milestones.milestoneStatus',
                'milestones.filedDisputes',
                'milestones.transaction' => function($q){
                    $q->where('type', '=', Transaction::TYPE_MILESTONE_PAYMENT);
                },
                'disputes.milestone.transactions'
            ])
            ->where('id', '=', $projectId);
//
        $query->when($userId, function($q)use($userId, $projectId){
//	        	if(\Auth::user()->id != Project::where('id', '=', $projectId)->first()->user_id) {
            $q->with(['milestones' => function ($q1) use ($q, $userId) {
                $q1->where('applicant_id', '=', $userId);
            }]);
            $q->with(['awardedApplicant' => function ($q1) use ($q, $userId) {
                $q1->where('user_id', '=', $userId);
            }]);
//		        }
        });

        \Log::info(']]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]');
        \Log::info(\DB::getQueryLog());
        \Log::info(']]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]');
        return   $query->first();
    }

    /**
     * @return mixed
     */
    public function getMyProject()
    {
        return $this
            ->with([
                'applicants',
                'awardedApplicant' => function ($q) {
//                    $q->where('status', '=', 3);
                },
                'projectChatRoom.chatFollower',
                'milestones.milestoneStatus'
            ])
            ->where('user_id', '=', \Auth::user()->id)
            ->where('type', '=', self::PROJECT_TYPE_COMMON)
            ->orderBy('id', 'desc')
            ->get();
    }


    /**
     * @return mixed
     */
    public function getMyProgressiveProjects()
    {
        return $this
            ->with([
                'applicants',
                'awardedApplicant',
                'projectChatRoom.chatFollower',
                'milestones.milestoneStatus'
            ])
            ->whereHas('awardedApplicant', function ($q) {
                $q->where('status', '=', ProjectApplicant::STATUS_CREATOR_SELECTED);
            })
            ->where('user_id', '=', \Auth::user()->id)
            ->where('type', '=', self::PROJECT_TYPE_COMMON)
            ->where('status', '!=', self::STATUS_COMPLETED)
            ->orderBy('id', 'desc')
            ->get();
    }
}
