<?php

namespace App\Models;

class ProjectChatRoom extends BaseModels {
    protected $table = 'project_chat_rooms';

    protected $fillable = [
        'project_id'
    ];

    protected $dates = [];

    public static function boot() {
        parent::boot();
    }

    public function scopeGetProjectChatRoom($query) {
        return $query;
    }

    public function project() {
        return $this->belongsTo('App\Models\Project', 'project_id', 'id');
    }

    public function chatFollower()
    {
        return $this->hasMany('App\Models\ProjectChatFollower', 'project_chat_room_id', 'id');
    }
}