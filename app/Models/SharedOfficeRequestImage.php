<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SharedOfficeRequestImage extends Model
{
    //

    protected $fillable = [
        'shared_office_request_id',
        'images'
    ];
}
