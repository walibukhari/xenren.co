<?php


namespace App\Traits;


use App\Models\SharedOffice;
use App\Models\Staff;
use App\Models\TempTransaction;
use App\Models\Transaction;
use App\Services\GeneralService;
use Illuminate\Support\Facades\Auth;
use RevenueMonster\SDK\Exceptions\ApiException;
use RevenueMonster\SDK\Exceptions\ValidationException;
use RevenueMonster\SDK\Request\QRPay;
use RevenueMonster\SDK\Request\WebPayment;
use RevenueMonster\SDK\RevenueMonster;

trait RevenueMonsterTrait
{
    protected $rm;

    public function __construct()
    {
        $this->rm = new RevenueMonster([
            'clientId' => '1573107227524768921',
            'clientSecret' => 'olLXugBNyRTgDLOMaMFBalLGBjRYtdOW',
            'privateKey' => file_get_contents(public_path('rm-live.pem')),
            'isSandbox' => false,
        ]);
    }

    public function generatePaymentLink($request)
    {
        $amount = str_replace(',','.',$request->amount);
        return $this->web($amount,$request);
    }

    public function qr()
    {
        try {
            $qrPay = new QRPay();
            $qrPay->currencyType = 'MYR';
            $qrPay->amount = 100*0.1;
            $qrPay->isPreFillAmount = true;
            $qrPay->order->title = '服务费';
            $qrPay->order->detail = 'testing';
            $qrPay->method = [];
            $qrPay->redirectUrl = 'https://www.xenren.co/revenue-monster/redirect/?user_id='.Auth::user()->id;
            $qrPay->storeId = '1571728609803307378';
            $qrPay->type = 'DYNAMIC';
            $response = $this->rm->payment->qrPay($qrPay);
            dd($response);
        } catch(ApiException $e) {
            echo "statusCode : {$e->getCode()}, errorCode : {$e->getErrorCode()}, errorMessage : {$e->getMessage()}";
        } catch(Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getStaff($id){
        $officeId = SharedOffice::where('id','=',$id)->first();
        $staff = Staff::where('id','=',$officeId->staff_id)->first();
        return $staff;
    }

    public function web($amount,$request)
    {
        $staff = $this->getStaff($request->office_id);
        try {
            $amount = 100*(double)$amount;
            $wp = new WebPayment;
            $wp->order->id = GeneralService::generateRandomStringWithAlPhabet(10);
            $wp->order->title = 'Testing Web Payment';
            $wp->order->currencyType = 'MYR';
            $wp->order->amount = $amount;
            $wp->order->detail = '';
            $wp->order->additionalData = '';
            $wp->storeId = "1571728609803307378";
            $wp->redirectUrl = 'https://www.xenren.co/revenue-monster/redirect';
            $wp->notifyUrl = 'https://www.xenren.co/revenue-monster/notify';
//            $wp->method = ["BOOST_MY"];

            $response = $this->rm->payment->createWebPayment($wp);
//            echo $response->checkoutId; // Checkout ID
//            echo $response->url; // Payment gateway url
            if($request->type != '' && $request->type == Transaction::TYPE) {
                $data = TempTransaction::create([
                    'order_id' => $wp->order->id,
                    'staff_id' => $staff->id,
                    'user_id' => 0,
                    'amount' => $amount,
                ]);
            } else {
                $data = TempTransaction::create([
                    'order_id' => $wp->order->id,
                    'user_id' => Auth::user()->id,
                    'amount' => $amount,
                ]);
            }
            return $response;
        } catch(ApiException $e) {
            echo "statusCode : {$e->getCode()}, errorCode : {$e->getErrorCode()}, errorMessage : {$e->getMessage()}";
        } catch(ValidationException $e) {
            var_dump($e->getMessage());
        }  catch(Exception $e) {
            echo $e->getMessage();
        }
    }
}
