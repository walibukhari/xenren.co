<?php

namespace App\Traits;
use Illuminate\Support\Facades\Redis;

trait PredisTrait {
	
	public function pushToRedis($event, $data, $channel)
	{
		$data = collect([
			'event' => $event,
			'data' => $data
		]);
		\Log::info($data);
		return Redis::publish($channel, $data);
	}
	
}