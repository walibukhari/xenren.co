<?php
/**
 * Created by PhpStorm.
 * User: ahsanhussain
 * Date: 4/5/19
 * Time: 4:07 PM
 * Skype: ahsan.mughal2
 */

namespace App\Traits;


use App\Libraries\Pushy\PushyAPI;
use App\Models\User;

trait sendPushNotification
{
    public function sendNotifications($receiverId,$message,$body){
        $users = User::where('id','=',$receiverId)->select([
            'id', 'web_token', 'post_project_notify','device_token'
        ])->where('post_project_notify', '=', 1)->get();
        $usersTokens = array();
        $usersDeviceToken = array();
        foreach ($users as $user) {
            array_push($usersTokens, $user->web_token);
            array_push($usersDeviceToken, $user->device_token);
        }
        $data = array(
            "title" => "Xenren", // Notification title
            'message' => $message,
            'body' => $body,
            "image" => asset('images/Favicon.jpg')
        );
        $options = array(
            'notification' => array(
                'badge' => 0,
                'sound' => 'ping.aiff',
                'body' => $body
            )
        );
        PushyAPI::sendPushMobileNotification($data, $usersDeviceToken, $options);
        PushyAPI::sendPushNotification($data, $usersTokens, $options);
    }

}
