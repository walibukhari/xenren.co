<?php

namespace App\Traits;

trait YahooWeatherTrait {
    
    protected $url;
    protected $app_id;
    protected $consumer_key;
    protected $consumer_secret;
    protected $lat;
    protected $long;
    
	public function __construct()
    {
        $this->url = 'https://weather-ydn-yql.media.yahoo.com/forecastrss';
        $this->app_id = 'ONY1j44q';
        $this->consumer_key = 'dj0yJmk9VFcyVlBaS0JoZE9LJmQ9WVdrOVQwNVpNV28wTkhFbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PTZl';
        $this->consumer_secret = '7ee0aa0a1eac1766c8162372c9db2a6684e8894d';
    }

    public function getWeatherByYahoo($lat,$long)
    {
        $this->lat = $lat;
        $this->long = $long;

        $query = array(
            'lat' => $this->lat,
            'lon' => $this->long,
            'u' =>'c',
            'format' => 'json',
        );
        $oauth = array(
            'oauth_consumer_key' => $this->consumer_key,
            'oauth_nonce' => uniqid(mt_rand(1, 1000)),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_timestamp' => time(),
            'oauth_version' => '1.0'
        );

        $base_info = $this->buildBaseString($this->url, 'GET', array_merge($query, $oauth));
        
        $composite_key = rawurlencode($this->consumer_secret) . '&';
        
        $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
        
        $oauth['oauth_signature'] = $oauth_signature;
        
        $header = array(
            $this->buildAuthorizationHeader($oauth),
            'X-Yahoo-App-Id: ' . $this->app_id
        );
        
        $options = array(
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_HEADER => false,
            CURLOPT_URL => $this->url . '?' . http_build_query($query),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false
        );
        
        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);
        curl_close($ch);
        $return_data = json_decode($response);
        return $return_data;
    }
	
    public function buildBaseString($baseURI, $method, $params) {
        $r = array();
        ksort($params);
        foreach($params as $key => $value) {
            $r[] = "$key=" . rawurlencode($value);
        }
        return $method . "&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
    }

    public function buildAuthorizationHeader($oauth) {
            $r = 'Authorization: OAuth ';
            $values = array();
            foreach($oauth as $key=>$value) {
                $values[] = "$key=\"" . rawurlencode($value) . "\"";
            }
            $r .= implode(', ', $values);
            return $r;
        }

}