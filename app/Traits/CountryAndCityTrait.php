<?php
/**
 * Created by PhpStorm.
 * User: ahsanhussain
 * Date: 4/5/19
 * Time: 4:07 PM
 * Skype: ahsan.mughal2
 */

namespace App\Traits;


use App\Models\WorldCities;
use App\Models\WorldContinents;
use App\Models\WorldCountries;

trait CountryAndCityTrait
{
	
	protected $continents = [
		"AD" => "Europe",
		"AE" => "Asia",
		"AF" => "Asia",
		"AG" => "North America",
		"AI" => "North America",
		"AL" => "Europe",
		"AM" => "Asia",
		"AN" => "North America",
		"AO" => "Africa",
		"AQ" => "Antarctica",
		"AR" => "South America",
		"AS" => "Australia",
		"AT" => "Europe",
		"AU" => "Australia",
		"AW" => "North America",
		"AZ" => "Asia",
		"BA" => "Europe",
		"BB" => "North America",
		"BD" => "Asia",
		"BE" => "Europe",
		"BF" => "Africa",
		"BG" => "Europe",
		"BH" => "Asia",
		"BI" => "Africa",
		"BJ" => "Africa",
		"BM" => "North America",
		"BN" => "Asia",
		"BO" => "South America",
		"BR" => "South America",
		"BS" => "North America",
		"BT" => "Asia",
		"BW" => "Africa",
		"BY" => "Europe",
		"BZ" => "North America",
		"CA" => "North America",
		"CC" => "Asia",
		"CD" => "Africa",
		"CF" => "Africa",
		"CG" => "Africa",
		"CH" => "Europe",
		"CI" => "Africa",
		"CK" => "Australia",
		"CL" => "South America",
		"CM" => "Africa",
		"CN" => "Asia",
		"CO" => "South America",
		"CR" => "North America",
		"CU" => "North America",
		"CV" => "Africa",
		"CX" => "Asia",
		"CY" => "Asia",
		"CZ" => "Europe",
		"DE" => "Europe",
		"DJ" => "Africa",
		"DK" => "Europe",
		"DM" => "North America",
		"DO" => "North America",
		"DZ" => "Africa",
		"EC" => "South America",
		"EE" => "Europe",
		"EG" => "Africa",
		"EH" => "Africa",
		"ER" => "Africa",
		"ES" => "Europe",
		"ET" => "Africa",
		"FI" => "Europe",
		"FJ" => "Australia",
		"FK" => "South America",
		"FM" => "Australia",
		"FO" => "Europe",
		"FR" => "Europe",
		"GA" => "Africa",
		"GB" => "Europe",
		"GD" => "North America",
		"GE" => "Asia",
		"GF" => "South America",
		"GG" => "Europe",
		"GH" => "Africa",
		"GI" => "Europe",
		"GL" => "North America",
		"GM" => "Africa",
		"GN" => "Africa",
		"GP" => "North America",
		"GQ" => "Africa",
		"GR" => "Europe",
		"GS" => "Antarctica",
		"GT" => "North America",
		"GU" => "Australia",
		"GW" => "Africa",
		"GY" => "South America",
		"HK" => "Asia",
		"HN" => "North America",
		"HR" => "Europe",
		"HT" => "North America",
		"HU" => "Europe",
		"ID" => "Asia",
		"IE" => "Europe",
		"IL" => "Asia",
		"IM" => "Europe",
		"IN" => "Asia",
		"IO" => "Asia",
		"IQ" => "Asia",
		"IR" => "Asia",
		"IS" => "Europe",
		"IT" => "Europe",
		"JE" => "Europe",
		"JM" => "North America",
		"JO" => "Asia",
		"JP" => "Asia",
		"KE" => "Africa",
		"KG" => "Asia",
		"KH" => "Asia",
		"KI" => "Australia",
		"KM" => "Africa",
		"KN" => "North America",
		"KP" => "Asia",
		"KR" => "Asia",
		"KW" => "Asia",
		"KY" => "North America",
		"KZ" => "Asia",
		"LA" => "Asia",
		"LB" => "Asia",
		"LC" => "North America",
		"LI" => "Europe",
		"LK" => "Asia",
		"LR" => "Africa",
		"LS" => "Africa",
		"LT" => "Europe",
		"LU" => "Europe",
		"LV" => "Europe",
		"LY" => "Africa",
		"MA" => "Africa",
		"MC" => "Europe",
		"MD" => "Europe",
		"ME" => "Europe",
		"MG" => "Africa",
		"MH" => "Australia",
		"MK" => "Europe",
		"ML" => "Africa",
		"MM" => "Asia",
		"MN" => "Asia",
		"MO" => "Asia",
		"MP" => "Australia",
		"MQ" => "North America",
		"MR" => "Africa",
		"MS" => "North America",
		"MT" => "Europe",
		"MU" => "Africa",
		"MV" => "Asia",
		"MW" => "Africa",
		"MX" => "North America",
		"MY" => "Asia",
		"MZ" => "Africa",
		"NA" => "Africa",
		"NC" => "Australia",
		"NE" => "Africa",
		"NF" => "Australia",
		"NG" => "Africa",
		"NI" => "North America",
		"NL" => "Europe",
		"NO" => "Europe",
		"NP" => "Asia",
		"NR" => "Australia",
		"NU" => "Australia",
		"NZ" => "Australia",
		"OM" => "Asia",
		"PA" => "North America",
		"PE" => "South America",
		"PF" => "Australia",
		"PG" => "Australia",
		"PH" => "Asia",
		"PK" => "Asia",
		"PL" => "Europe",
		"PM" => "North America",
		"PN" => "Australia",
		"PR" => "North America",
		"PS" => "Asia",
		"PT" => "Europe",
		"PW" => "Australia",
		"PY" => "South America",
		"QA" => "Asia",
		"RE" => "Africa",
		"RO" => "Europe",
		"RS" => "Europe",
		"RU" => "Europe",
		"RW" => "Africa",
		"SA" => "Asia",
		"SB" => "Australia",
		"SC" => "Africa",
		"SD" => "Africa",
		"SE" => "Europe",
		"SG" => "Asia",
		"SH" => "Africa",
		"SI" => "Europe",
		"SJ" => "Europe",
		"SK" => "Europe",
		"SL" => "Africa",
		"SM" => "Europe",
		"SN" => "Africa",
		"SO" => "Africa",
		"SR" => "South America",
		"ST" => "Africa",
		"SV" => "North America",
		"SY" => "Asia",
		"SZ" => "Africa",
		"TC" => "North America",
		"TD" => "Africa",
		"TF" => "Antarctica",
		"TG" => "Africa",
		"TH" => "Asia",
		"TJ" => "Asia",
		"TK" => "Australia",
		"TM" => "Asia",
		"TN" => "Africa",
		"TO" => "Australia",
		"TR" => "Asia",
		"TT" => "North America",
		"TV" => "Australia",
		"TW" => "Asia",
		"TZ" => "Africa",
		"UA" => "Europe",
		"UG" => "Africa",
		"US" => "North America",
		"UY" => "South America",
		"UZ" => "Asia",
		"VC" => "North America",
		"VE" => "South America",
		"VG" => "North America",
		"VI" => "North America",
		"VN" => "Asia",
		"VU" => "Australia",
		"WF" => "Australia",
		"WS" => "Australia",
		"YE" => "Asia",
		"YT" => "Africa",
		"ZA" => "Africa",
		"ZM" => "Africa",
		"ZW" => "Africa"
	];
	
	public function validateContinent($continent)
	{
		$c = WorldContinents::where('name', 'like', '%'.$continent.'%')->first();
		return $c;
	}
	
	/**
	 * @param $countryName
	 * @return mixed
	 */
	public function validateCountry($countryName)
	{
	    try {
            $continent = $this->validateContinent($this->continents[$countryName->short_name]);
            $exist = WorldCountries::where('name', 'like', '%' . $countryName->long_name . '%')->first();
            if (!is_null($exist)) {
                return $exist;
            } else {
                $obj = WorldCountries::create([
                    'continent_id', '=', $continent->id,
                    'name', '=', $countryName->short_name,
                    'full_name', '=', $countryName->long_name,
                ])->first();
                return $obj;
            }
        } catch (\Exception $e) {
	        dd($e);
            return false;
        }
	}
	
	public function validateCity($city, $country)
	{
	    try {
            $name = isset($city->short_long_name) ? $city->short_long_name : $city->long_name;
            $exist = WorldCities::where('name', 'like', '%' . $name)->first();
            if (!is_null($exist)) {
                return $exist;
            } else {
                return WorldCities::create([
                    'country_id' => $country->id,
                    'division_id' => 0,
                    'name' => $city->short_name,
                    'full_name' => $name,
                    'code' => $city->short_name,
                    'state_id' => 0
                ]);
            }
        } catch (\Exception $e) {
            return false;
        }
	}

    /**
     * @param $cityName
     * @return \Illuminate\Support\Collection
     */
    public function getCityDetail($cityName)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://getcitydetails.geobytes.com/GetCityDetails?fqcn=".urlencode($cityName),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return collect([
                'status' => 'error',
                'message' => $err
            ]);
        } else {
            return collect([
                'status' => 'success',
                'data' => json_decode($response)
            ]);
        }
	}
	
}