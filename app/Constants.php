<?php

namespace App;

class Constants {
    const MATERIAL_1 = 1;
    const MATERIAL_2 = 2;
    const MATERIAL_3 = 3;

    const BODY_SHAPE_1 = 1;
    const BODY_SHAPE_2 = 2;
    const BODY_SHAPE_3 = 3;

    const PAY_TYPE_HOURLY_PAY = 1;
    const PAY_TYPE_FIXED_PRICE = 2;
    const PAY_TYPE_MAKE_OFFER = 3;

    const TIME_COMMITMENT_1 = 1;
    const TIME_COMMITMENT_2 = 2;
    const TIME_COMMITMENT_3 = 3;

    const FREELANCE_TYPE_1 = 1;
    const FREELANCE_TYPE_2 = 2;
    const FREELANCE_TYPE_3 = 3;

//    const USER_STATUS_LK_COFOUNDERS = 1;
//    const USER_STATUS_LK_JOB = 2;
//    const USER_STATUS_LK_PROJECT = 3;
//    const USER_STATUS_AL_JOB = 4;
//    const USER_STATUS_AL_PROJECT = 5;
//    const USER_STATUS_AL_STARTUP = 6;
//    const USER_STATUS_NO_STATUS = 7;

    const FUNDING_STAGE_SEED = 1;
    const FUNDING_STAGE_ANGLE = 2;
    const FUNDING_STAGE_PREA = 3;
    const FUNDING_STAGE_A = 4;
    const FUNDING_STAGE_B = 5;
    const FUNDING_STAGE_C = 6;

    const USD = 1;
    const CNY = 2;

    const REVIEW_TYPE_OFFICIAL = 1;
    const REVIEW_TYPE_COMMON = 2;

    const LAST_ACTIVITY_WITHIN_ONE_WEEK = 1;
    const LAST_ACTIVITY_WITHIN_ONE_MONTH = 2;
    const LAST_ACTIVITY_WITHIN_THREE_MONTHS = 3;

//    const PROJECT_TYPE_COMMON = 1;
//    const PROJECT_TYPE_OFFICIAL = 2;

    const ADMIN_ID = 1;

    const LAST_SEVEN_DAYS = 1;
    const LAST_FOURTEEN_DAYS = 2;
    const LAST_THIRTY_DAYS = 3;

    public static function getMaterialLists($empty = false) {
        $arr = array();
        if ($arr === true) {
            $arr[] = trans('order.material');
        }
        $arr[static::MATERIAL_1] = trans('order.thick');
        $arr[static::MATERIAL_2] = trans('order.middle');
        $arr[static::MATERIAL_3] = trans('order.thin');

        return $arr;
    }

    public static function translateMaterial($id) {
        switch ($id) {
            case static::MATERIAL_1: return trans('order.thick'); break;
            case static::MATERIAL_2: return trans('order.middle'); break;
            case static::MATERIAL_3: return trans('order.thin'); break;
            default: return '-'; break;
        }
    }

    public static function getBodyShapeLists($empty = false) {
        $arr = array();
        if ($arr === true) {
            $arr[] = trans('order.material');
        }
        $arr[static::BODY_SHAPE_1] = trans('order.asia');
        $arr[static::BODY_SHAPE_2] = trans('order.europe');
        $arr[static::BODY_SHAPE_3] = trans('order.america');

        return $arr;
    }

    public static function translateBodyShape($id) {
        switch ($id) {
            case static::BODY_SHAPE_1: return trans('order.asia'); break;
            case static::BODY_SHAPE_2: return trans('order.europe'); break;
            case static::BODY_SHAPE_3: return trans('order.america'); break;
            default: return '-'; break;
        }
    }

    public static function getPayTypeLists($empty = false) {
        $arr = array();
        if ($arr === true) {
            $arr[] = 'Pay Type';
        }
        $arr[static::PAY_TYPE_HOURLY_PAY] = trans('common.pay_by_the_hour');
        $arr[static::PAY_TYPE_FIXED_PRICE] = trans('common.pay_a_fixed_price');
        $arr[static::PAY_TYPE_MAKE_OFFER] = trans('common.make_offer');

        return $arr;
    }

    public static function translatePayType($id) {
        switch ($id) {
            case static::PAY_TYPE_HOURLY_PAY: return trans('common.pay_hourly'); break;
            case static::PAY_TYPE_FIXED_PRICE: return trans('common.fixed_price'); break;
            case static::PAY_TYPE_MAKE_OFFER: return trans('common.request_quotation'); break;
            default: return '-'; break;
        }
    }

    public static function getTimeCommitmentLists($empty = false) {
        $arr = array();
        if ($arr === true) {
            $arr[] = 'Time Commitment';
        }
        $arr[static::TIME_COMMITMENT_1] = 'More than 30 hrs/week';
        $arr[static::TIME_COMMITMENT_2] = 'Less than 30 hrs/week';
        $arr[static::TIME_COMMITMENT_3] = 'I don\'t know yet';

        return $arr;
    }

    public static function translateTimeCommitment($id) {
        switch ($id) {
            case static::TIME_COMMITMENT_1: return 'More than 30 hrs/week'; break;
            case static::TIME_COMMITMENT_2: return 'Less than 30 hrs/week'; break;
            case static::TIME_COMMITMENT_3: return 'I don\'t know yet'; break;
            default: return '-'; break;
        }
    }

    public static function getFreelanceTypeLists($empty = false) {
        $arr = array();
        if ($arr === true) {
            $arr[] = 'Freelance Type';
        }
        $arr[static::FREELANCE_TYPE_1] = 'Anyone';
        $arr[static::FREELANCE_TYPE_2] = 'Only Xenren users';
        $arr[static::FREELANCE_TYPE_3] = 'Only freelances I invited';

        return $arr;
    }

    public static function translateFreelanceType($id) {
        switch ($id) {
            case static::FREELANCE_TYPE_1: return 'Anyone'; break;
            case static::FREELANCE_TYPE_2: return 'Only Xenren users'; break;
            case static::FREELANCE_TYPE_3: return 'Only freelances I invited'; break;
            default: return '-'; break;
        }
    }

//    public static function getUserStatusLists($empty = false) {
//        $arr = array();
//        if ($arr === true) {
//            $arr[] = 'User Status';
//        }
//        $arr[static::USER_STATUS_LK_COFOUNDERS] = trans('`common.looking_for_co_founders');
//        $arr[static::USER_STATUS_LK_JOB] = trans('common.looking_for_job');
//        $arr[static::USER_STATUS_LK_PROJECT] = trans('common.looking_for_project');
//        $arr[static::USER_STATUS_AL_JOB] = trans('common.already_in_job');
//        $arr[static::USER_STATUS_AL_PROJECT] =  trans('common.already_in_project');
//        $arr[static::USER_STATUS_AL_STARTUP] = trans('common.already_in_startup');
//        $arr[static::USER_STATUS_NO_STATUS] = trans('common.no_status');
//
//        return $arr;
//    }
//
//    public static function translateUserStatus($id) {
//        switch ($id) {
//            case static::USER_STATUS_LK_COFOUNDERS: return trans('common.looking_for_co_founders'); break;
//            case static::USER_STATUS_LK_JOB: return trans('common.looking_for_job'); break;
//            case static::USER_STATUS_LK_PROJECT: return trans('common.looking_for_project'); break;
//            case static::USER_STATUS_AL_JOB: return trans('common.already_in_job'); break;
//            case static::USER_STATUS_AL_PROJECT: return trans('common.already_in_project'); break;
//            case static::USER_STATUS_AL_STARTUP: return trans('common.already_in_startup'); break;
//            case static::USER_STATUS_NO_STATUS: return trans('common.no_status'); break;
//            default: return '-'; break;
//        }
//    }

    public static function translateProjectLevel($level) {
        $arr = static::getProjectLevels();
        if (isset($arr[$level])) {
            return $arr[$level];
        } else {
            return 'Unknown';
        }
    }

    public static function getProjectLevels($empty = false)
    {
        $arr = array();
        if ($empty === true) {
            $arr[''] = trans('common.all');
        }
        $arr[1] = trans('common.project_level_1');
        $arr[2] = trans('common.project_level_2');
        $arr[3] = trans('common.project_level_3');
        $arr[4] = trans('common.project_level_4');
        $arr[5] = trans('common.project_level_5');
        $arr[6] = trans('common.project_level_6');

        return $arr;
    }

    public static function translateProjectType($type) {
        $arr = static::getProjectTypes();
        if (isset($arr[$type])) {
            return $arr[$type];
        } else {
            return 'Unknown';
        }
    }

    public static function getProjectTypes($empty = false)
    {
        $arr = array();
        if ($empty === true) {
            $arr[''] = trans('common.all');
        }
        $arr[1] = trans('common.project_type_1');
        $arr[2] = trans('common.project_type_2');
        $arr[3] = trans('common.project_type_3');
        return $arr;
    }

    public static function translateLang($lang) {
        $arr = static::getLangs();
        if (isset($arr[$lang])) {
            return $arr[$lang];
        } else {
            return 'Unknown';
        }
    }

    public static function getLangs($empty = false)
    {
        $arr = array();
        if ($empty === true) {
            $arr[''] = trans('common.all');
        }
        $arr['en'] = trans('common.lang_en');
        $arr['cn'] = trans('common.lang_cn');

        return $arr;
    }

    public static function translateSkillLevel($experience) {
        $arr = static::getSkillLevel();
        $id = 1;
        if ($experience >= 100000) {
            $id = 6;
        } else if ($experience >= 10000) {
            $id = 5;
        } else if ($experience >= 1000) {
            $id = 4;
        } else if ($experience >= 100) {
            $id = 3;
        } else if ($experience >= 10) {
            $id = 2;
        }

        if (isset($arr[$id])) {
            return $arr[$id];
        } else {
            return 'Unknown';
        }
    }

    public static function getSkillLevel($empty = false)
    {
        $arr = array();
        if ($empty === true) {
            $arr[] = trans('common.all');
        }

        $arr[1] = trans('common.skill_level_1');
        $arr[2] = trans('common.skill_level_2');
        $arr[3] = trans('common.skill_level_3');
        $arr[4] = trans('common.skill_level_4');
        $arr[5] = trans('common.skill_level_5');
        $arr[6] = trans('common.skill_level_6');

        return $arr;
    }

    public static function getSkillScoreSelects() {
        return [
            '0' => '0',
            '0.5' => '0.5',
            '1' => '1',
            '1.5' => '1.5',
            '2' => '2',
            '2.5' => '2.5',
            '3' => '3',
            '3.5' => '3.5',
            '4' => '4',
            '4.5' => '4.5',
            '5' => '5',
        ];
    }

    public static function getScoreOverallHtml($value)
    {
        $result = "";

        $fullStarCount = floor($value);
        $halfStarCount = ceil($value - $fullStarCount) ;
        $noStarCount = 5 - ceil($value);

        for($i = 0; $i < $fullStarCount; $i++ )
        {
            $result = $result . "<span class='fa star fa-star'></span>";
        }

        for($i = 0; $i < $halfStarCount; $i++ )
        {
            $result = $result . "<span class='fa star fa-star-half-o'></span>";
        }

        for($i = 0; $i < $noStarCount; $i++ )
        {
            $result = $result . "<span class='fa star fa-star-o'></span>";
        }

        return $result;
    }

    public static function getFundingStageLists($empty = false) {
        $arr = array();
        if ($arr === true) {
            $arr[] = 'Funding Stage';
        }
        $arr[static::FUNDING_STAGE_SEED] = trans('officialprojects.种子投资');
        $arr[static::FUNDING_STAGE_ANGLE] = trans('officialprojects.天使投资');
        $arr[static::FUNDING_STAGE_PREA] = trans('officialprojects.A轮前');
        $arr[static::FUNDING_STAGE_A] = trans('officialprojects.A轮');
        $arr[static::FUNDING_STAGE_B] = trans('officialprojects.B轮');
        $arr[static::FUNDING_STAGE_C] = trans('officialprojects.C轮');

        return $arr;
    }

    public static function translateFundingStage($id) {
        switch ($id) {
            case static::FUNDING_STAGE_SEED: return trans('officialprojects.种子投资'); break;
            case static::FUNDING_STAGE_ANGLE: return trans('officialprojects.天使投资'); break;
            case static::FUNDING_STAGE_PREA: return trans('officialprojects.A轮前'); break;
            case static::FUNDING_STAGE_A: return trans('officialprojects.A轮'); break;
            case static::FUNDING_STAGE_B: return trans('officialprojects.B轮'); break;
            case static::FUNDING_STAGE_C: return trans('officialprojects.C轮'); break;
            default: return '-'; break;
        }
    }

    public static function currency($id)
    {
        switch ($id) {
            case static::USD:
                return trans('common.usd');
                break;
            case static::CNY:
                return trans('common.cny');
                break;
            default:
                return '';
                break;
        }
    }

    public static function getReviewTypeLists($empty = false){
        $arr = array();
        if ($arr === true) {
            $arr[] = trans('common.review_type');
        }

        $arr[static::REVIEW_TYPE_OFFICIAL] = trans('common.official_review');
        $arr[static::REVIEW_TYPE_COMMON] = trans('common.common_review');

        return $arr;
    }

    public static function translateReviewType($id) {
        switch ($id) {
            case static::REVIEW_TYPE_OFFICIAL: return trans('common.official_review'); break;
            case static::REVIEW_TYPE_COMMON: return trans('common.common_review'); break;
            default: return '-'; break;
        }
    }

    public static function getLastActivityLists($empty = false) {
        $arr = array();
        if ($arr === true) {
            $arr[] = trans('common.last_activities');
        }

        $arr[static::LAST_ACTIVITY_WITHIN_ONE_WEEK] =trans('common.within_one_week');
        $arr[static::LAST_ACTIVITY_WITHIN_ONE_MONTH] = trans('common.within_one_month');
        $arr[static::LAST_ACTIVITY_WITHIN_THREE_MONTHS] = trans('common.within_three_months');

        return $arr;
    }

    public static function getLastNumberDaysList($empty = false)
    {
        $arr = array();
        if ($arr === true) {
            $arr[] = trans('common.last_number_days');
        }

        $arr[static::LAST_SEVEN_DAYS] = trans('common.last_seven_days');
        $arr[static::LAST_FOURTEEN_DAYS] = trans('common.last_fourteen_days');
        $arr[static::LAST_THIRTY_DAYS] = trans('common.last_thirty_days');

        return $arr;
    }

    public static function getHourlyRange() {
        $arr = array();
        if( app()->getLocale() == "en" )
        {
            $currency = trans("common.usd");
        }
        else
        {
            $currency = trans("common.rmb");
        }

        $arr[1] = "1 - 10 " . $currency;
        $arr[2] = "11 - 20 " . $currency;
        $arr[3] = "21 - 30 " . $currency;
        $arr[4] = "31 - 40 " . $currency;
        $arr[5] = "41 - 50 " . $currency;
        $arr[6] = "51+" . $currency;

        return $arr;
    }
    public static function getHourlyRanges()
    {
        $arr = array();
        if( app()->getLocale() == "en" )
        {
            $currency = trans("common.usd");
        }
        else
        {
            $currency = trans("common.rmb");
        }

        $arr[] = self::get_hourly_range_object(1, "1 - 10 " . $currency);
        $arr[] = self::get_hourly_range_object(2,"11 - 20 " . $currency);
        $arr[] = self::get_hourly_range_object(3,"21 - 30 " . $currency);
        $arr[] = self::get_hourly_range_object(4,"31 - 40 " . $currency);
        $arr[] = self::get_hourly_range_object(5,"41 - 50 " . $currency);
        $arr[] = self::get_hourly_range_object(6,"51+" . $currency);

        return $arr;
    }

    private static function get_hourly_range_object($id, $name) {
        return [
            "id" => $id,
            "name" => $name
        ];
    }
}