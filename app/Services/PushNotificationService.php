<?php

namespace App\Services;

use Log;
use App\Libraries\Pushy\PushyAPI;

class PushNotificationService
{
	
		static public function sendPush($deviceToken, $data, $message='Xenren')
		{
			// Payload data you want to send to devices
//			$data = array('message' => 'Hello World From Backend!');

			// The recipient device tokens
			$to = array($deviceToken);

			// Optionally, send to a publish/subscribe topic instead
			// $to = '/topics/news';

			// Optional push notification options (such as iOS notification fields)
			$options = array(
				'notification' => array(
					'badge' => 1,
					'sound' => 'ping.aiff',
					'body'  => $message
				)
			);

			// Send it with Pushy
			return PushyAPI::sendPushNotification($data, $to, $options);
		}
    /**
     * This function sends the notifications to the app side
     *
     * @param $deviceToken
     * @param $data
     * @return bool
     */
    static public function xxsendPush($deviceToken, $data)
    {
        $apiKey = config('app.FIRE_BASE_API_KEY');
        $registration_ids = $deviceToken;

        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';

        $fcmMsg = array(
            'body' => $data['body'],
            'title' => $data['title']
        );

        $fields = array(
            'to' => $registration_ids,
            'notification' => $fcmMsg,
            'data' => $data
        );

        $headers = array(
            'Authorization: key=' .$apiKey,
            'Content-Type: application/json'
        );

        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        Log::info($fields);
        Log::info('************************** cron result **************************');
        Log::info($registration_ids);
        Log::info($result);
        // Close connection
        curl_close($ch);
        return true;

    }
    
    /**
     * This function sends the notifications to the app side
     *
     * @param $deviceToken
     * @param $data
     * @return bool
     */
    static public function xsendPush($deviceToken, $data)
    {
        $apiKey = config('app.FIRE_BASE_API_KEY');
        $registration_ids = $deviceToken;

        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';

        $fcmMsg = array(
            'body' => $data['body'],
            'title' => $data['title']
        );

        $fields = array(
            'to' => $registration_ids,
            'notification' => $fcmMsg,
            'data' => $data
        );

        $headers = array(
            'Authorization: key=' .$apiKey,
            'Content-Type: application/json'
        );

        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        Log::info($fields);
        Log::info('************************** cron result **************************');
        Log::info($registration_ids);
        Log::info($result);
        // Close connection
        curl_close($ch);
        return true;

    }

}