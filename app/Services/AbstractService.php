<?php

namespace App\Services;

class AbstractService
{
    private $ouput;

    public function info($message, $customOutput = null)
    {
        if (!is_null($customOutput)) {
            return $customOutput->info($message);
        }
        if (!is_null($this->ouput)) {
            return $this->ouput->info($message);
        }

        return $message;
    }

    public function error($message, $customOutput = null)
    {
        return $this->baseOutput('error', $message, $customOutput);
    }

    public function baseOutput($type, $message, $customOutput)
    {
        if (!is_null($customOutput)) {
            return $customOutput->{$type}($message);
        }
        if (!is_null($this->ouput)) {
            return $this->ouput->{$type}($message);
        }

        return $message;
    }

    public function setOutput($output)
    {
        $this->ouput = $output;

        return $this;
    }

    /**
     * Get the value of ouput.
     */
    public function getOuput()
    {
        return $this->ouput;
    }

    public function createOrGetJsonFile(string $fileName): string
    {
        $fullPathFile = $fileName;
        if (!file_exists($fullPathFile)) {
            file_put_contents($fullPathFile, '');
        }

        return file_get_contents($fullPathFile);
    }
}
