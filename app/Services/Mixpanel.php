<?php

namespace App\Services;

use Base_MixpanelBase;
use Producers_MixpanelPeople;
use Producers_MixpanelEvents;

class Mixpanel extends Base_MixpanelBase {
    /**
     * An instance of the MixpanelPeople class (used to create/update profiles)
     * @var Producers_MixpanelPeople
     */
    public $people;


    /**
     * An instance of the MixpanelEvents class
     * @var Producers_MixpanelEvents
     */
    private $_events;


    /**
     * Instances' list of the Mixpanel class (for singleton use, splitted by token)
     * @var Mixpanel[]
     */
    private static $_instances = array();


    /**
     * Instantiates a new Mixpanel instance.
     * @param $token
     * @param array $options
     */
    public function __construct($token, $options = array()) {
        parent::__construct($options);
        $this->people = new Producers_MixpanelPeople($token, $options);
        $this->_events = new Producers_MixpanelEvents($token, $options);
    }


    /**
     * Returns a singleton instance of Mixpanel
     * @param $token
     * @param array $options
     * @return Mixpanel
     */
    public static function getInstance($token, $options = array()) {
        if(!isset(self::$_instances[$token])) {
            self::$_instances[$token] = new Mixpanel($token, $options);
        }
        return self::$_instances[$token];
    }


    /**
     * Add an array representing a message to be sent to Mixpanel to the in-memory queue.
     * @param array $message
     */
    public function enqueue($message = array()) {
        $this->_events->enqueue($message);
    }


    /**
     * Add an array representing a list of messages to be sent to Mixpanel to a queue.
     * @param array $messages
     */
    public function enqueueAll($messages = array()) {
        $this->_events->enqueueAll($messages);
    }


    /**
     * Flush the events queue
     * @param int $desired_batch_size
     */
    public function flush($desired_batch_size = 50) {
        $this->_events->flush($desired_batch_size);
    }


    /**
     * Empty the events queue
     */
    public function reset() {
        $this->_events->reset();
    }


    /**
     * Identify the user you want to associate to tracked events. The $anon_id must be UUID v4 format and not already merged to an $identified_id.
     * All identify calls with a new and valid $anon_id will trigger a track $identify event, and merge to the $identified_id.
     * @param string|int $user_id
     * @param string|int $anon_id
     */
    public function identify($user_id, $anon_id = null) {
        $this->_events->identify($user_id, $anon_id);
    }

    /**
     * Track an event defined by $event associated with metadata defined by $properties
     * @param string $event
     * @param array $properties
     */
    public function track($event, $properties = array()) {
        $this->_events->track($event, $properties);
    }


    /**
     * Register a property to be sent with every event.
     *
     * If the property has already been registered, it will be
     * overwritten. NOTE: Registered properties are only persisted for the life of the Mixpanel class instance.
     * @param string $property
     * @param mixed $value
     */
    public function register($property, $value) {
        $this->_events->register($property, $value);
    }


    /**
     * Register multiple properties to be sent with every event.
     *
     * If any of the properties have already been registered,
     * they will be overwritten. NOTE: Registered properties are only persisted for the life of the Mixpanel class
     * instance.
     * @param array $props_and_vals
     */
    public function registerAll($props_and_vals = array()) {
        $this->_events->registerAll($props_and_vals);
    }


    /**
     * Register a property to be sent with every event.
     *
     * If the property has already been registered, it will NOT be
     * overwritten. NOTE: Registered properties are only persisted for the life of the Mixpanel class instance.
     * @param $property
     * @param $value
     */
    public function registerOnce($property, $value) {
        $this->_events->registerOnce($property, $value);
    }


    /**
     * Register multiple properties to be sent with every event.
     *
     * If any of the properties have already been registered,
     * they will NOT be overwritten. NOTE: Registered properties are only persisted for the life of the Mixpanel class
     * instance.
     * @param array $props_and_vals
     */
    public function registerAllOnce($props_and_vals = array()) {
        $this->_events->registerAllOnce($props_and_vals);
    }


    /**
     * Un-register an property to be sent with every event.
     * @param string $property
     */
    public function unregister($property) {
        $this->_events->unregister($property);
    }


    /**
     * Un-register a list of properties to be sent with every event.
     * @param array $properties
     */
    public function unregisterAll($properties) {
        $this->_events->unregisterAll($properties);
    }


    /**
     * Get a property that is set to be sent with every event
     * @param string $property
     * @return mixed
     */
    public function getProperty($property)
    {
        return $this->_events->getProperty($property);
    }


    /**
     * An alias to be merged with the distinct_id. Each alias can only map to one distinct_id.
     * This is helpful when you want to associate a generated id (such as a session id) to a user id or username.
     * @param string|int $distinct_id
     * @param string|int $alias
     */
    public function createAlias($distinct_id, $alias) {
        $this->_events->createAlias($distinct_id, $alias);
    }
}
