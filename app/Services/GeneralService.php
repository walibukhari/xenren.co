<?php

namespace App\Services;

use App\Models\Project;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

/**
 * Created by PhpStorm.
 * User: ahsan
 * Date: 20/06/18
 * Time: 09:48 AM
 */
class GeneralService
{
    public function uploadFile($request, $path, $fileName)
    {
        if (!$path)
            return false;

        try {
            $dt = Carbon::parse(Carbon::Now());
            $timeStamp = $dt->timestamp;
            $destinationPath = public_path() . '/' .$path;
            $file = Input::file($fileName);
            $extension = Input::file($fileName)->getClientOriginalExtension();
            $originalName = Input::file($fileName)->getClientOriginalName();
            $fileOriginalName = Input::file($fileName)->getClientOriginalName();
            $fileOriginalName = str_replace('.' . $extension, "", $fileOriginalName);
            $fileName = $timeStamp . '__' . $fileOriginalName . '.' . $extension;
            $file->move($destinationPath, $fileName);
            return collect([
                'status' => 'success',
                'icon' => $fileName,
                'fileName' => $originalName
            ]);
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function calculateScore($project)
    {
        $skillScore = self::countSkillScore($project);
        $locationScore = self::calculateLocationScore($project);
        $budgetScore = self::calculateBudgetScore($project);
        $verifyScore = \Auth::user()->is_validated > 0 ? 10:0;  //TODO: Need to rewamp on base of face detection package
        $hiringScore = Project::where('user_id', '=', \Auth::user()->id)->count() > 10 ? 10 : 0;
        $howLongScore = self::calculateHowLongScore($project);
        $avatarScopre = self::calculateAvatarScore();
        $personalInformationScore = self::calculateInformationScore();
        $applicantScore = self::calculateApplicantScore();

        \Log::info('========= SCORES ===============');
        \Log::info($skillScore);
        \Log::info($locationScore);
        \Log::info($budgetScore);
        \Log::info($verifyScore);
        \Log::info($hiringScore);
        \Log::info($howLongScore);
        \Log::info($avatarScopre);
        \Log::info($personalInformationScore);
        \Log::info($applicantScore);
        \Log::info('========= SCORES ===============');

        $score = $skillScore +  $locationScore + $budgetScore + $verifyScore + $hiringScore + $howLongScore + $avatarScopre + $personalInformationScore + $applicantScore;
        return collect([
            'score' => $score,
            'detail' => collect([
                'skillScore' => $skillScore,
                'locationScore' => $locationScore,
                'budgetScore' => $budgetScore,
                'verifyScore' => $verifyScore,
                'hiringScore' => $hiringScore,
                'howLongScore' => $howLongScore,
                'avatarScopre' => $avatarScopre,
                'personalInformationScore' => $personalInformationScore,
                'applicantScore' => $applicantScore
            ])
        ]);
    }

    public function calculateInformationScore()
    {
        if(
            isset(\Auth::user()->nick_name) && \Auth::user()->nick_name != '' &&
            isset(\Auth::user()->about_me) && \Auth::user()->about_me != '' &&
            isset(\Auth::user()->experience) && \Auth::user()->experience != '' &&
            isset(\Auth::user()->job_position_id) && \Auth::user()->job_position_id != '' &&
            isset(\Auth::user()->title) && \Auth::user()->title != '' &&
            isset(\Auth::user()->country_id) && \Auth::user()->country_id != '' &&
            isset(\Auth::user()->city_id) && \Auth::user()->city_id != '' &&
            isset(\Auth::user()->address) && \Auth::user()->address != '' &&
            isset(\Auth::user()->language_preference) && \Auth::user()->language_preference != ''
        ) {
            return 10;
        } else {
            return 0;
        }
    }

    public function calculateApplicantScore()
    {
        if(
            isset(\Auth::user()->qq_id) && \Auth::user()->qq_id != '' ||
            isset(\Auth::user()->wechat_id) && \Auth::user()->wechat_id != '' ||
            isset(\Auth::user()->skype_id) && \Auth::user()->skype_id != '' ||
            isset(\Auth::user()->handphone_no) && \Auth::user()->handphone_no != ''
        ) {
            return 10;
        } else {
            return 0;
        }
    }

    public function calculateAvatarScore()
    {
        if(isset(\Auth::user()->img_avatar) && \Auth::user()->img_avatar != '') {
            return 10;
        } else {
            return 0;
        }
    }

    public function calculateHowLongScore($project)
    {
        $projectDate = Carbon::parse($project->created_at);
        $diff = $projectDate->diffInDays(Carbon::now());
        return $diff >= 10 ? 10 : 0;
    }

    public function calculateBudgetScore($project)
    {
        if($project->pay_type == Project::PAY_TYPE_HOURLY_PAY) {
            if($project->reference_price*(1/100) * (\Auth::user()->hourly_pay) >= 40) {
                $budgetScore = 10;
            } else {
                $budgetScore = 0;
            }
        } else {
            $budgetScore = 10;
        }
        return $budgetScore ;
    }

    public function calculateLocationScore($project)
    {
        if(is_null($project->nearby_place)) {
            if(\Auth::user()->city_Iid) {
                $locationScore = 10;
            } else {
                $locationScore = 10;
            }
        } else {
            $locationScore = 10;
            if(\Auth::user()->city_Iid) {
                $locationScore = 0;
                if($project->nearby_place == \Auth::user()->city_Iid) {
                    $locationScore = 10;
                } else {
                    $locationScore = 0;
                }
            } else {
                $locationScore = 0;
            }
        }

        return $locationScore;
    }

    /**
     * ******************************** CALCULATE SKILL SCORE FUNCTIONS ****************************************
     *
     * User Skill ALGO:
     *	experience = 1 ==> Golden Skill with point 20
     *	is_client_verified = 1 ==> Green Skill with point 15
     *	is_client_verified = 0 ==> white Skill with point 10
     *
     * */

    /**
     * @param $project
     * @return int
     */
    public function countSkillScore($project)
    {
        $userSkills = Auth::user()->getUserSkill();
        $projectSkills = $project->projectSkills;
        $existingSkills = self::skillExist($projectSkills, $userSkills);
        $skillScore = self::calculateSkill($existingSkills);
        return $skillScore;
    }

    /**
     * @param $projectSkills
     * @param $userSkills
     * @return array
     */
    public function skillExist($projectSkills, $userSkills)
    {
        $list = array();
        foreach( $projectSkills as $key => $val ){
            foreach ($userSkills as $k => $v) {
                if($v->skill_id == $val->skill_id) {
                    array_push($list, $v);
                }
            }
        }
        return $list;
    }

    /**
     * @param $userSkills
     * @return int
     */
    public function calculateSkill($userSkills)
    {
        if (count($userSkills) > 0) {
            $whiteSkills = array();
            $greenSkills = array();
            $goldSkills = array();
            foreach ($userSkills as $k => $v) {
                if ($v->experience > 0) { //GOLD SKILL
                    array_push($goldSkills, $v);
                }
                if ($v->is_client_verified > 0) { //GREEN SKILL
                    array_push($greenSkills, $v);
                }
                if ($v->is_client_verified < 1) { //GREEN SKILL
                    array_push($whiteSkills, $v);
                }
            }

            if (count($goldSkills) >= count($greenSkills)) {
                return 20;
            }
            if (count($greenSkills) >= count($whiteSkills)) {
                return 15;
            }
            return 10;
        } else {
            return 0;
        }
    }

    public static function time2str($ts)
    {
        if (!ctype_digit($ts)) {
            $ts = strtotime($ts);
        }
        $diff = time() - $ts;
        if ($diff == 0) {
            return 'now';
        } elseif ($diff > 0) {
            $day_diff = floor($diff / 86400);
            if ($day_diff == 0) {
                if ($diff < 60) return 'just now';
                if ($diff < 120) return '1 minute ago';
                if ($diff < 3600) return floor($diff / 60) . ' minutes ago';
                if ($diff < 7200) return '1 hour ago';
                if ($diff < 86400) return floor($diff / 3600) . ' hours ago';
            }
            if ($day_diff == 1) {
                return 'Yesterday';
            }
            if ($day_diff < 7) {
                return $day_diff . ' days ago';
            }
            if ($day_diff < 31) {
                return ceil($day_diff / 7) . ' weeks ago';
            }
            if ($day_diff < 60) {
                return 'last month';
            }
            return date('F d', $ts) . " at " . date('h:i a', $ts);
        } else {
            $diff = abs($diff);
            $day_diff = floor($diff / 86400);
            if ($day_diff == 0) {
                if ($diff < 120) {
                    return 'in a minute';
                }
                if ($diff < 3600) {
                    return 'in ' . floor($diff / 60) . ' minutes';
                }
                if ($diff < 7200) {
                    return 'in an hour';
                }
                if ($diff < 86400) {
                    return 'in ' . floor($diff / 3600) . ' hours';
                }
            }
            if ($day_diff == 1) {
                return 'Tomorrow';
            }
            if ($day_diff < 4) {
                return date('l', $ts);
            }
            if ($day_diff < 7 + (7 - date('w'))) {
                return 'next week';
            }
            if (ceil($day_diff / 7) < 4) {
                return 'in ' . ceil($day_diff / 7) . ' weeks';
            }
            if (date('n', $ts) == date('n') + 1) {
                return 'next month';
            }
            return date('F d', $ts) . " at " . date('h:i a', $ts);
        }
    }

    static function cleanTheLink($val)
    {
        $a =  str_replace("/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s",' ',$val);
        $a = iconv("UTF-8", "ASCII//TRANSLIT", $a);
        $a = str_replace('?' , '', $a);
        $a = str_replace('>' , '', $a);
        $a = str_replace('<' , '', $a);
        $a = str_replace('^' , '', $a);
        $a = str_replace("’", ' ', $a);
        $a = str_replace("-", ' ', $a);
        $a = str_replace(":", ' ', $a);
        $a = str_replace("$", ' ', $a);
        $a = str_replace("/", ' ', $a);
        $a =  str_replace("%", ' ', $a);
        $a =  str_replace("@", ' ', $a);
        $a =  str_replace("#", ' ', $a);
        $a =  str_replace("&", ' ', $a);
        $a =  str_replace("*", ' ', $a);
        $a =  str_replace("(", ' ', $a);
        $a =  str_replace(")", ' ', $a);
        $a =  str_replace("-", ' ', $a);
        $a =  str_replace("+", ' ', $a);
        $a =  str_replace("=", ' ', $a);
        $a =  str_replace("{", ' ', $a);
        $a =  str_replace("}", ' ', $a);
        $a =  str_replace("[", ' ', $a);
        $a =  str_replace(",", ' ', $a);
        $a =  str_replace("]", ' ', $a);
        $a =  str_replace("|", ' ', $a);
        $a =  str_replace("'", ' ', $a);
        $a =  str_replace(":", ' ', $a);
        $a =  str_replace(";", ' ', $a);
        $a =  str_replace("!", ' ', $a);
        $a =  str_replace(')' , '', $a);
        $a =  str_replace('.' , '', $a);
        $a = str_replace(' ' , '-', $a);
        $a = strtolower(trim($a));
        return preg_replace('!\s+!', ' ', $a);
    }


    static function generateRandomString($length = 1) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $dt = Carbon::parse(Carbon::Now());
        $timeStamp = $dt->timestamp;
        return $timeStamp.'_'.$randomString;
    }

    static function  generateRandomStringWithAlPhabet($length = 1) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $dt = Carbon::parse(Carbon::Now());
        $timeStamp = $dt->timestamp;
        return $timeStamp.'_'.$randomString;
    }

    static function calculateServiceFee($amount)
    {
        return (2.75 * ($amount))/100;
    }

    static function truncateString($str, $chars, $to_space, $replacement="...") {
        if($chars > strlen($str))  return $str;

        $str = substr($str, 0, $chars);
        $space_pos = strrpos($str, " ");
        if($to_space && $space_pos >= 0)
            $str = substr($str, 0, strrpos($str, " "));

        return($str . $replacement);
    }

    static function payPalAvailableCountries($country) {
        $obj = array(
            'albania',
            'algeria',
            'andorra',
            'angola',
            'anguilla',
            'antigua',
            'argentina',
            'armenia',
            'aruba',
            'australia',
            'austria',
            'azerbaijan',
            'bahamas',
            'bahrain',
            'barbados',
            'belarus',
            'belgium',
            'belize',
            'benin',
            'bermuda',
            'bhutan',
            'bolivia',
            'bonsia',
            'botswana',
            'brazil',
            'british',
            'brunei',
            'bulgaria',
            'burkina',
            'burundi',
            'cambodia',
            'cameroon',
            'canada',
            'cape verde',
            'cayman islands',
            'chad',
            'chile',
            'china',
            'colombia',
            'comoros',
            'congo - brazzaville',
            'congo - kinshasa',
            'cook islands',
            'costa rica',
            'cÔte d’ivoire',
            'croatia',
            'cyprus',
            'czech republic',
            'denmark',
            'djibouti',
            'dominica',
            'dominican republic',
            'ecuador',
            'egypt',
            'el salvador',
            'eritrea',
            'estonia',
            'ethiopia',
            'falkland islands',
            'faroe islands',
            'fiji',
            'finland',
            'france',
            'french guiana',
            'french polynesia',
            'gabon',
            'gambia',
            'georgia',
            'germany',
            'gibraltar',
            'greece',
            'greenland',
            'grenada',
            'guadeloupe',
            'guatemala',
            'guinea',
            'guinea-bissau',
            'guyana',
            'honduras',
            'hong kong sar china',
            'hungary',
            'iceland',
            'india',
            'indonesia',
            'ireland',
            'israel',
            'italy',
            'jamaica',
            'japan',
            'jordan',
            'kazakhstan',
            'kenya',
            'kiribati',
            'kuwait',
            'kyrgyzstan',
            'laos',
            'latvia',
            'lesotho',
            'liechtenstein',
            'lithuania',
            'luxembourg',
            'macedonia',
            'madagascar',
            'malawi',
            'malaysia',
            'maldives',
            'mali',
            'malta',
            'marshall islands',
            'martinique',
            'mauritania',
            'mauritius',
            'mayotte',
            'mexico',
            'micronesia',
            'moldova',
            'monaco',
            'mongolia',
            'montenegro',
            'montserrat',
            'morocco',
            'mozambique',
            'namibia',
            'nauru',
            'nepal',
            'netherlands',
            'new caledonia',
            'new zealand',
            'nicaragua',
            'niger',
            'nigeria',
            'niue',
            'norfolk island',
            'norway',
            'oman',
            'palau',
            'panama',
            'papua new guinea',
            'paraguay',
            'peru',
            'philippines',
            'pitcairn islands',
            'poland',
            'portugal',
            'qatar',
            'rÉunion',
            'romania',
            'russia',
            'rwanda',
            'samoa',
            'san marino',
            'sÃo tomÉ & prÍncipe',
            'saudi arabia',
            'senegal',
            'serbia',
            'seychelles',
            'sierra leone',
            'singapore',
            'slovakia',
            'slovenia',
            'solomon islands',
            'somalia',
            'south africa',
            'south korea',
            'spain',
            'sri lanka',
            'st. helena',
            'st. kitts & nevis',
            'st. lucia',
            'st. pierre & miquelon',
            'st. vincent & grenadines',
            'suriname',
            'svalbard & jan mayen',
            'swaziland',
            'sweden',
            'switzerland',
            'taiwan',
            'tajikistan',
            'tanzania',
            'thailand',
            'togo',
            'tonga',
            'trinidad & tobago',
            'tunisia',
            'turkmenistan',
            'turks & caicos islands',
            'tuvalu',
            'uganda',
            'ukraine',
            'united arab emirates',
            'united kingdom',
            'united states',
            'uruguay',
            'vanuatu',
            'vatican city',
            'venezuela',
            'vietnam',
            'wallis & futuna',
            'yemen',
            'zambia',
            'zimbabwe',
        );
        if (in_array($country, $obj)) {
            return true;
        } else {
            return false;
        }
    }

    static function stripeAvailableCountries($country) {
        $obj = array(
            'Albania',
            'Algeria',
            'Andorra',
            'Angola',
            'Anguilla',
            'Antigua & Barbuda',
            'Argentina',
            'Armenia',
            'Aruba',
            'Australia',
            'Austria',
            'Azerbaijan',
            'Bahamas',
            'Bahrain',
            'Barbados',
            'Belarus',
            'Belgium',
            'Belize',
            'Benin',
            'Bermuda',
            'Bhutan',
            'Bolivia',
            'Bosnia & Herzegovina',
            'Botswana',
            'Brazil',
            'British Virgin Islands',
            'Brunei',
            'Bulgaria',
            'Burkina Faso',
            'Burundi',
            'Cambodia',
            'Cameroon',
            'Canada',
            'Cape Verde',
            'Cayman Islands',
            'Chad',
            'Chile',
            'China',
            'Colombia',
            'Comoros',
            'Congo',
            'Congo - Brazzaville',
            'Congo - Kinshasa',
            'Cook Islands',
            'Costa Rica',
            'Côte D’Ivoire',
            'Croatia',
            'Cyprus',
            'Czech Republic',
            'Denmark',
            'Djibouti',
            'Dominica',
            'Dominican Republic',
            'Ecuador',
            'Egypt',
            'El Salvador',
            'Eritrea',
            'Estonia',
            'Ethiopia',
            'Falkland Islands',
            'Faroe Islands',
            'Fiji',
            'Finland',
            'France',
            'French Guiana',
            'French Polynesia',
            'Gabon',
            'Gambia',
            'Georgia',
            'Germany',
            'Gibraltar',
            'Greece',
            'Greenland',
            'Grenada',
            'Guadeloupe',
            'Guatemala',
            'Guinea	Gn',
            'Guinea-Bissau',
            'Guyana',
            'Honduras',
            'Hong Kong Sar China',
            'Hungary',
            'Iceland',
            'India',
            'Indonesia',
            'Ireland',
            'Israel',
            'Italy',
            'Jamaica',
            'Japan',
            'Jordan',
            'Kazakhstan',
            'Kenya',
            'Kiribati',
            'Kuwait',
            'Kyrgyzstan',
            'Laos',
            'Latvia',
            'Lesotho',
            'Liechtenstein',
            'Lithuania',
            'Luxembourg',
            'Macedonia',
            'Madagascar',
            'Malawi',
            'Malaysia',
            'Maldives',
            'Mali',
            'Malta',
            'Marshall Islands',
            'Martinique',
            'Mauritania',
            'Mauritius',
            'Mayotte',
            'Mexico',
            'Micronesia',
            'Moldova',
            'Monaco',
            'Mongolia',
            'Montenegro',
            'Montserrat',
            'Morocco',
            'Mozambique',
            'Namibia',
            'Nauru',
            'Nepal',
            'Netherlands',
            'New Caledonia',
            'New Zealand',
            'Nicaragua',
            'Niger',
            'Nigeria',
            'Niue',
            'Norfolk Island',
            'Norway',
            'Oman',
            'Palau',
            'Panama',
            'Papua New Guinea',
            'Paraguay',
            'Peru',
            'Philippines',
            'Pitcairn Islands',
            'Poland',
            'Portugal',
            'Qatar',
            'Réunion',
            'Romania',
            'Russia',
            'Rwanda',
            'Samoa',
            'San Marino',
            'São Tomé & Príncipe',
            'Saudi Arabia',
            'Senegal',
            'Serbia',
            'Seychelles',
            'Sierra Leone',
            'Singapore',
            'Slovakia',
            'Slovenia',
            'Solomon Islands',
            'Somalia',
            'South Africa',
            'South Korea',
            'Spain',
            'Sri Lanka',
            'St. Helena',
            'St. Kitts & Nevis',
            'St. Lucia',
            'St. Pierre & Miquelon',
            'St. Vincent & Grenadines',
            'Suriname',
            'Svalbard & Jan Mayen',
            'Swaziland',
            'Sweden',
            'Switzerland',
            'Taiwan',
            'Tajikistan',
            'Tanzania',
            'Thailand',
            'Togo',
            'Tonga',
            'Trinidad & Tobago',
            'Tunisia',
            'Turkmenistan',
            'Turks & Caicos Islands',
            'Tuvalu',
            'Uganda',
            'Ukraine',
            'United Arab Emirates',
            'United Kingdom',
            'United States',
            'Uruguay',
            'Vanuatu',
            'Vatican City',
            'Venezuela',
            'Vietnam',
            'Wallis & Futuna',
            'Yemen',
            'Zambia',
            'Zimba',
        );
        if (in_array($country, $obj)) {
            return true;
        } else {
            return false;
        }
    }
}
