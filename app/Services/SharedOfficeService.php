<?php

declare(strict_types=1);

namespace App\Services;

use App\Http\Controllers\Services\SharedOfficeController;
use App\Models\SharedOffice;

class SharedOfficeService extends AbstractService
{
    public function updateJsonList(array $data): bool
    {
        $isExist = false;

        try {
            $sharedOffices = json_decode($this->createOrGetJsonFile(base_path('public/jsons/homepage.json')), true);
            if (!is_null($sharedOffices)) {
                foreach ($sharedOffices as $key => $value) {
                    if ($data['id'] == $value['id']) {
                        $isExist = true;
                        $fetchOfficeInfo = $this->fetchOfficeInfoForJsonList($data);

                        $sharedOffices[$key] = $fetchOfficeInfo;
                    }
                }
            }
            if (false === $isExist) {
                $fetchOfficeInfo = $this->fetchOfficeInfoForJsonList($data);
                $sharedOffices[] = $fetchOfficeInfo;
            }
            $dataTransformer = collect($sharedOffices);
            file_put_contents(public_path('jsons/homepage.json'), $dataTransformer->toJson(JSON_PRETTY_PRINT));
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());

            return false;
        }

        return true;
    }

    public function deleteJsonList(array $data): bool
    {
        try {
            $sharedOffices = json_decode($this->createOrGetJsonFile(base_path('public/jsons/homepage.json')), true);
            \Log::info('Current shared office : '.count($sharedOffices));
            if (!is_null($sharedOffices)) {
                foreach ($sharedOffices as $key => $value) {
                    if ($data['id'] == $value['id']) {
                        unset($sharedOffices[$key]);
                    }
                }
            }
            $dataTransformer = collect($sharedOffices);
            \Log::info('Current shared office (After delete): '.count($dataTransformer));
            file_put_contents(public_path('jsons/homepage.json'), $dataTransformer->toJson(JSON_PRETTY_PRINT));
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());

            return false;
        }

        return true;
    }

    private function getOfficeDetail($officeId)
    {
        $soc = new SharedOfficeController();

        return $soc->officeDetailsBtnget($officeId)['office_products'];
    }

    private function fetchOfficeInfoForJsonList(array $office): array
    {
        $sharedoffice = SharedOffice::with('totalOfficeMember', 'city', 'city.country', 'country', 'sharedOfficeRating', 'SharedOfficeLanguageData', 'sharedOfficeLikeCount', 'shfacilities', 'officeImages');
        $sharedoffice->selectRaw('shared_offices.id,  shared_offices.city_id , shared_offices.state_id ,office_space,office_size_x,office_size_y,office_space,
            contact_phone,monday_opening_time,monday_closing_time,saturday_opening_time,saturday_closing_time,sunday_opening_time,sunday_closing_time,
            continent_id,shared_offices.lat,shared_offices.lng,office_name,image,description,location,version_english,version_chinese,created_at,
            updated_at');
        $sharedoffice->where('id', '=', $office['id']);
        $data = $sharedoffice->first();

        $detail = $this->getOfficeDetail($office['id']);
        if (!$data) {
            $data = $office;
        } else {
            $data = $data->toArray();
        }
        $data['products'] = $detail;

        return $data;
    }
}
