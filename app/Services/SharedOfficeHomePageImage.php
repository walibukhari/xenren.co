<?php

namespace App\Services;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class SharedOfficeHomePageImage implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->resize(800, 500);
    }
}
