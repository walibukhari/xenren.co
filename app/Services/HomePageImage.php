<?php

namespace App\Services;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class HomePageImage implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->resize(1302, 719.03);
    }
}
