<?php

namespace App\Services;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class SharedOfficeImage implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->resize(170, 128);
    }
}
