<?php

namespace App\Logic\UploadFile;

use App\Models\Portfolio;
use App\Models\PortfolioFile;
use App\Models\ProjectFile;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManager;
use App\Models\UploadFile;


class UploadFileRepository
{
    public function upload( $form_data )
    {

        $validator = Validator::make($form_data, UploadFile::$rules, UploadFile::$messages);

        if ($validator->fails()) {

            return Response::json([
                'error' => true,
                'message' => $validator->messages()->first(),
                'code' => 400
            ], 400);

        }

        $uploadFile = $form_data['file'];

        $originalName = $uploadFile->getClientOriginalName();
        $extension = $uploadFile->getClientOriginalExtension();
        $originalNameWithoutExt = substr($originalName, 0, strlen($originalName) - strlen($extension) - 1);

        $filename = $this->sanitize($originalNameWithoutExt);
        $allowed_filename = $this->createUniqueFilename( $filename, $extension );

//        $uploadSuccess1 = $this->original( $uploadFile, $allowed_filename );

        if ($uploadFile instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
            $uploadSuccess1 = $uploadFile->move(Config::get('upload-files.location'), $allowed_filename);
        }

        if( !$uploadSuccess1 ) {

            return Response::json([
                'error' => true,
                'message' => 'Server error while uploading',
                'code' => 500
            ], 500);

        }

        if( $form_data['type'] == UploadFile::TYPE_POST_PROJECT )
        {
            $fileUploadFeatureType = UploadFile::TYPE_POST_PROJECT;
        }
        else
        {
            $fileUploadFeatureType = UploadFile::TYPE_PORTFOLIO;
        }

        $sessionUploadFile = new UploadFile;
        $sessionUploadFile->user_id       = \Auth::guard('users')->user()->id;
        $sessionUploadFile->type          = $fileUploadFeatureType;
        $sessionUploadFile->filename      = $allowed_filename;
        $sessionUploadFile->original_name = $originalName;
        $sessionUploadFile->save();

        return Response::json([
            'error' => false,
            'code'  => 200,
            'filename' => $allowed_filename
        ], 200);

    }

    public function createUniqueFilename( $filename, $extension )
    {
        $full_size_dir = Config::get('upload-files.location');
        $full_image_path = $full_size_dir . $filename . '.' . $extension;

        if ( File::exists( $full_image_path ) )
        {
            // Generate token for image
            $imageToken = substr(sha1(mt_rand()), 0, 5);
            return $filename . '-' . $imageToken . '.' . $extension;
        }

        return $filename . '.' . $extension;
    }

//    /**
//     * Optimize Original Upload File
//     */
//    public function original( $photo, $filename )
//    {
//        $manager = new ImageManager();
//        $uploadFile = $manager->make( $photo )->save(Config::get('upload-files.location') . $filename );
//
//        return $uploadFile;
//    }

    /**
     * Delete File From Session folder, based on server created filename
     */
    public function delete( $filename )
    {

        $file_location_dir = Config::get('upload-files.location');

        $filename = str_replace(" ","-", $filename);
        $sessionUploadFile = UploadFile::where('filename', 'like', $filename)->first();


        if(empty($sessionUploadFile))
        {
            return Response::json([
                'error' => true,
                'code'  => 400
            ], 400);

        }

        $full_path1 = $file_location_dir . $sessionUploadFile->filename;

        if ( File::exists( $full_path1 ) )
        {
            File::delete( $full_path1 );
        }

        if( !empty($sessionUploadFile))
        {
            $sessionUploadFile->delete();
        }

        return Response::json([
            'error' => false,
            'code'  => 200
        ], 200);
    }

    function sanitize($string, $force_lowercase = true, $anal = false)
    {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
            "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
            "â€”", "â€“", ",", "<", ".", ">", "/", "?");
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;

        return ($force_lowercase) ?
            (function_exists('mb_strtolower')) ?
                mb_strtolower($clean, 'UTF-8') :
                strtolower($clean) :
            $clean;
    }

    public function copy( $filename, $type, $id )
    {
        try
        {
            if( $type == UploadFile::TYPE_PORTFOLIO )
            {
                $portfolioFile = new PortfolioFile();
                $portfolioFile->portfolio_id = $id;
                $portfolioFile->file = $filename;
                $portfolioFile->save();
            }
            else if( $type == UploadFile::TYPE_POST_PROJECT )
            {
                $projectFile = new ProjectFile();
                $projectFile->project_id = $id;
                $projectFile->file = $filename;
                $projectFile->save();
            }
        }
        catch(\Exception $e)
        {
            return Response::json([
                'error' => true,
                'message'  => $e->getMessage() . ' || ' . $e->getFile() . ' || ' . $e->getLine()
            ], 400);
            return Response::json([
                'error' => true,
                'code'  => 400
            ], 400);
        }

        return Response::json([
            'error' => false,
            'code'  => 200
        ], 200);
    }
}