<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SharedOfficeMainImage extends Model
{
    protected $fillable = [
      'main_image'
    ];
}
