<?php

namespace App\Console\Commands;

use App\Models\GuestRegister;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RemoveGuestRegisters extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove:guest-register';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      GuestRegister::truncate();
    }
}
