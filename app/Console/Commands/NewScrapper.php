<?php

namespace App\Console\Commands;

use App\Services\Scrapper\CoWorkerScrapperService;
use Illuminate\Console\Command;
use Illuminated\Console\Loggable;
use Illuminated\Console\WithoutOverlapping;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NewScrapper extends Command
{
    use Loggable;
    use WithoutOverlapping;
    public $output;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cowork:scrapper {site?}';
    protected $service;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrap all data from defined coworking space web, currently only support coworker.com';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
        $this->service = new CoWorkerScrapperService($this);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '512M');
        $this->service->continuableScrapByCountry();
    }

    public function info($string, $verbosity = null)
    {
        parent::info($string, $verbosity);
        $this->logInfo($string);
    }

    public function error($string, $verbosity = null)
    {
        parent::error($string, $verbosity);
        $this->logError($string);
    }

    protected function initialize(InputInterface $inputInterface, OutputInterface $outputInterface)
    {
        $this->initializeMutex();
        $this->initializeLogging();
        parent::initialize($inputInterface, $outputInterface);
    }
}
