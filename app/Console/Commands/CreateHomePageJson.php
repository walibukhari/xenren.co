<?php

namespace App\Console\Commands;

use App\Http\Controllers\Services\SharedOfficeController;
use App\Models\SharedOffice;
use Illuminate\Console\Command;

class CreateHomePageJson extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CreateHomePageJson';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate json file from shared office data (it will needed for some logic)';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '512M');
        try {
            // empty homepage json
            file_put_contents(public_path('jsons/homepage.json'), json_encode([]));

            $language = '';
            $sharedoffice = SharedOffice::with('totalOfficeMember', 'city', 'city.country', 'country', 'sharedOfficeRating', 'SharedOfficeLanguageData', 'sharedOfficeLikeCount', 'shfacilities', 'officeImages');
            $sharedoffice->selectRaw('shared_offices.id,  shared_offices.city_id , shared_offices.state_id ,office_space,office_size_x,office_size_y,office_space,
                contact_phone,monday_opening_time,monday_closing_time,saturday_opening_time,saturday_closing_time,sunday_opening_time,sunday_closing_time,
                continent_id,shared_offices.lat,shared_offices.lng,office_name,image,description,location,version_english,version_chinese,created_at,
                updated_at');
            $sharedoffice->where('active', '=', 1);
            $sharedoffice->where('image', '!=', '');
            $sharedoffice->where('deleted_at', '=', null);
            $sharedoffice = $sharedoffice->paginate(1000);
            $newarr = collect();
            $loader = $this->output->createProgressBar(count($sharedoffice));
            foreach ($sharedoffice as $k => $v) {
                $soc = new SharedOfficeController();
                $pro = $soc->officeDetailsBtnget($v->id);
                if($pro["office_products"] !== '') {
                    $officeProducts = $pro["office_products"];
                    $total_hot_desk = $pro["total_hot_desk"];
                    $total_dedicated_desks = $pro["total_dedicated_desks"];
                    $total_meeting_room = $pro["total_meeting_room"];
                    $v["products"] = $pro["office_products"];
                }

                // if($total_dedicated_desks > 0 && $total_hot_desk > 0 && $total_meeting_room > 0){
                //     if($v->min_seat_price > 0) {
                $newarr->push($v);
                //     }
                // }
//                $sharedoffice[$k]['pro'] = $pro['office_products'];
                $loader->advance();
            }
            $loader->finish();

            file_put_contents(public_path('jsons/homepage.json'), $newarr->toJson(JSON_PRETTY_PRINT));
        } catch (\Exception $e) {
            var_dump($e->getLine());
            dd($e->getMessage());

            return collect([
                'status' => 'error',
                'message' => $e->getMessage().' at '.$e->getLine(),
            ]);
        }
    }
}
