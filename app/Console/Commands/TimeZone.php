<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TimeZone extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'timezone:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = public_path() . "/timezone/timezone.json";
        $zones = json_decode(file_get_contents($path), true);
        if ($zones) {
            foreach ($zones as $k => $v) {
                $this->output->progressStart(count($v));
                foreach ($v as $data) {
                    \App\Models\TimeZone::create([
                        'value' => $data['value'],
                        'abbr' => $data['abbr'],
                        'offset' => $data['offset'],
                        'isdst' => $data['isdst'],
                        'text' => $data['text'],
                        'utc' => self::utc($data['utc']),
                    ]);
                    $this->output->progressAdvance();
                }
            }
            $this->output->progressFinish();
        }
    }

    public function utc($utc)
    {
        if(count($utc) > 1)
        {
            return collect($utc);
        } else {
            return collect($utc);
        }
    }
}
