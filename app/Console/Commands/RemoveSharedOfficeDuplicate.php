<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\SharedOffice;

class RemoveSharedOfficeDuplicate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SharedOffice:remove-duplicate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to remove duplicate based on it\'s name and location and keep the latest update';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('============= Running remove duplicate shared office =============');
        $listSharedOffice = SharedOffice::get();
        $bar = $this->output->createProgressBar(count($listSharedOffice));
        try {
            \DB::beginTransaction();
            $count = 1;
            foreach ($listSharedOffice as $sharedOffice) {
                $findDuplicate = SharedOffice::where('office_name', $sharedOffice->office_name)
                    ->where(\DB::raw('TRIM(location)'), trim($sharedOffice->location))
                    ->orderBy('id', 'asc')
                    ->get();

                if (count($findDuplicate) > 1 && $findDuplicate->last()->id != $sharedOffice->id) {
                    $this->info(' found duplicate id : '.$sharedOffice->id.' # total duplicate : '.$count);
                    $count++;

                    $sharedOffice->delete();
                }

                $bar->advance();
            }

            \DB::commit();
        } catch (Exception $e) {
            \DB::rollback();
            $this->info('error : ' . $e->getMessage());
        }

        $bar->finish();

        $this->info('============= End remove duplicate shared office =============');
    }
}
