<?php

namespace App\Console\Commands;

use App\Models\SharedOfficeFinance;
use Illuminate\Console\Command;

class RemoveChatUserAfterSevenDays extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove-sharedoffice-chat-users-after-seven-days';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It will remove shared offices chat users after seven days of joining chat';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
    }
}
