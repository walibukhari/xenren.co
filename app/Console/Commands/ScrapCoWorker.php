<?php

namespace App\Console\Commands;

use App\Models\SharedOffice;
use App\Models\SharedOfficeImages;
use App\Models\WorldCities;
use App\Models\WorldCountries;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Goutte\Client;

class ScrapCoWorker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrap:coworker';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $client = new Client();
            $cities = WorldCities::with('country')->orderBy('id', 'desc')->get();
            foreach ($cities as $k => $v) {
                try {
                    $city = (str_replace(' ', '-', strtolower($v->name)));
                    $country = (str_replace(' ', '-', strtolower($v->country->name)));
                    $this->info('ID: ' . $v->id);
                    $this->info('City: ' . $city);
                    $this->info('Country: ' . $country);
                    $url = 'https://www.coworker.com/search/' . $city . '/' . $country;
                    $this->info('Searching: ' . $url);
                    $crawler = $client->request('GET', $url);
                    $crawler->filter('#result_list')->each(function ($node) use ($country, $city) {
                        $node->filter('li')->each(function ($node1) use ($country, $city) {

                            $links1 = $node1->filter('.search_border_main > a ')->each(function ($node) use ($country, $city) {
                                $href = $node->extract(array('href'));
                                $link = self::filterLink($href);

                                $client = new Client();
                                $crawler = $client->request('GET', $link);
                                $bannerImages = array();
                                $name = '';
                                $officeTiming = array();
                                $latlng = array();

                                $name = $crawler->filter('.kohub_space_headings')->each(function ($innerNode) {
                                    return ($innerNode->filter('h1')->text());
                                });
                                $office = SharedOffice::where('office_name', '=', $name[0])->first();
                                if(is_null($office)) {
                                    $bannerImages = $crawler->filter('.coworker-banner-panel-21-11-18 > .container-fluid')->each(function ($innerNode) {
                                        //Crawling Banner Images
                                        $bannerImages1 = array();
                                        $bannerImages1 = $innerNode->filter('.coworker-banner-outer-21-11-18')->each(function ($innerNode1) {
                                            $bannerImages2 = array();
                                            $bannerImages2 = $innerNode1->filter('.pad_none > a')->each(function ($innerNode2) {
                                                $imageName = self::saveImage($innerNode2->filter('div > img')->eq(0)->attr('src'));
                                                return $imageName;
                                            });
                                            return $bannerImages2;
                                        });
                                        return $bannerImages1;
                                    });

                                    $latlng = $crawler->filter('.space_content_map')->each(function ($innerNode) {
                                        $q = self::getQueryParameters($innerNode->filter('iframe')->eq(0)->attr('src'));
                                        return (explode(',', $q['q']));
                                    });


                                    $officeTiming = $crawler->filter('.open_timings_con')->each(function ($innerNode) {
                                        return $innerNode->text();
                                    });

                                    $address = ($crawler->filter('.muchroom_mail')->text());
                                    $description = ($crawler->filter('#p_description')->text());
                                    $bannerImages = $bannerImages[0][0];
                                    try {
//                                    if (is_null($office)) {
                                        \DB::beginTransaction();
                                        $sharedoffice = SharedOffice::create([
                                            'office_name' => $name[0],
                                            'location' => $address,
                                            'description' => $description,
                                            'office_manager' => '',
                                            'qr' => 'dummy',
                                            'image' => $bannerImages[0],
                                            'pic_size' => 1,
                                            'staff_id' => 1,
                                            'office_size_x' => 0,
                                            'office_size_y' => 0,
                                            'number' => 0,
                                            'city_id' => 0,
                                            'state_id' => 0,
                                            'continent_id' => 0,
                                            'lat' => $latlng[0][0],
                                            'lng' => $latlng[0][1],
                                            'contact_name' => 0,
                                            'contact_phone' => 0,
                                            'contact_email' => 0,
                                            'contact_address' => 0,
                                            'monday_opening_time' => self::pluckOfficeTiming($officeTiming, 0)[0],
                                            'monday_closing_time' => self::pluckOfficeTiming($officeTiming, 0)[1],
                                            'saturday_opening_time' => self::pluckOfficeTiming($officeTiming, 1),
                                            'saturday_closing_time' => self::pluckOfficeTiming($officeTiming, 1),
                                            'sunday_opening_time' => self::pluckOfficeTiming($officeTiming, 2),
                                            'sunday_closing_time' => self::pluckOfficeTiming($officeTiming, 2),
                                            'office_space' => 0,
                                            'version_english' => 1,
                                            'version_chinese' => 0,
                                            'active' => 0,
                                            'likes' => 0,
                                        ]);
                                        $sharedoffice = SharedOffice::find($sharedoffice->id);
                                        $sharedoffice->qr = "https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=office:" . $sharedoffice->id;
                                        $sharedoffice->save();
                                        $index = 0;
                                        foreach ($bannerImages as $k) {
                                            if ($index > 0) {
                                                $img = SharedOfficeImages::create([
                                                    'office_id' => $sharedoffice->id,
                                                    'image' => $k,
                                                    'pic_size' => 1
                                                ]);
                                            }
                                            $index++;
                                        }

                                        \DB::commit();
//                                    }
                                    } catch (\Exception $e) {
                                        $this->error($e->getMessage());
                                        \DB::rollback();
//                                        dd($e->getMessage());
                                    }
                                }
                            });

                            $links = $node1->filter('.csSearchWrapper > a ')->each(function ($node) {
                                $href = $node->extract(array('href'));
                                $link = ($href[0]);
                                $this->info('going in ' . $link);
                                $client = new Client();
                                $crawler = $client->request('GET', $link);
                                $bannerImages = array();
                                $name = '';
                                $officeTiming = array();
                                $latlng = array();

                                $name = $crawler->filter('.kohub_space_headings')->each(function ($innerNode) {
                                    return ($innerNode->filter('h1')->text());
                                });
                                $office = SharedOffice::where('office_name', '=', $name[0])->first();
                                if(is_null($office)) {
                                    $bannerImages = $crawler->filter('.coworker-banner-panel-21-11-18 > .container-fluid')->each(function ($innerNode) {
                                        //Crawling Banner Images
                                        $bannerImages1 = array();
                                        $bannerImages1 = $innerNode->filter('.coworker-banner-outer-21-11-18')->each(function ($innerNode1) {
                                            $bannerImages2 = array();
                                            $bannerImages2 = $innerNode1->filter('.pad_none > a')->each(function ($innerNode2) {
                                                $imageName = self::saveImage($innerNode2->filter('div > img')->eq(0)->attr('src'));
                                                return $imageName;
                                            });
                                            return $bannerImages2;
                                        });
                                        return $bannerImages1;
                                    });

                                    $latlng = $crawler->filter('.space_content_map')->each(function ($innerNode) {
                                        $q = self::getQueryParameters($innerNode->filter('iframe')->eq(0)->attr('src'));
                                        return (explode(',', $q['q']));
                                    });


                                    $officeTiming = $crawler->filter('.open_timings_con')->each(function ($innerNode) {
                                        return $innerNode->text();
                                    });

                                    $address = ($crawler->filter('.muchroom_mail')->text());
                                    $description = ($crawler->filter('#p_description')->text());
                                    $bannerImages = $bannerImages[0][0];
                                    try {
//                                    if (is_null($office)) {
                                        \DB::beginTransaction();
                                        $sharedoffice = SharedOffice::create([
                                            'office_name' => $name[0],
                                            'location' => $address,
                                            'description' => $description,
                                            'office_manager' => '',
                                            'qr' => 'dummy',
                                            'image' => $bannerImages[0],
                                            'pic_size' => 1,
                                            'staff_id' => 1,
                                            'office_size_x' => 0,
                                            'office_size_y' => 0,
                                            'number' => 0,
                                            'city_id' => 0,
                                            'state_id' => 0,
                                            'continent_id' => 0,
                                            'lat' => $latlng[0][0],
                                            'lng' => $latlng[0][1],
                                            'contact_name' => 0,
                                            'contact_phone' => 0,
                                            'contact_email' => 0,
                                            'contact_address' => 0,
                                            'monday_opening_time' => self::pluckOfficeTiming($officeTiming, 0)[0],
                                            'monday_closing_time' => self::pluckOfficeTiming($officeTiming, 0)[1],
                                            'saturday_opening_time' => self::pluckOfficeTiming($officeTiming, 1),
                                            'saturday_closing_time' => self::pluckOfficeTiming($officeTiming, 1),
                                            'sunday_opening_time' => self::pluckOfficeTiming($officeTiming, 2),
                                            'sunday_closing_time' => self::pluckOfficeTiming($officeTiming, 2),
                                            'office_space' => 0,
                                            'version_english' => 1,
                                            'version_chinese' => 0,
                                            'active' => 0,
                                            'likes' => 0,
                                        ]);
                                        $sharedoffice = SharedOffice::find($sharedoffice->id);
                                        $sharedoffice->qr = "https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=office:" . $sharedoffice->id;
                                        $sharedoffice->save();

                                        $index = 0;
                                        foreach ($bannerImages as $k) {
                                            if ($index > 0) {
                                                SharedOfficeImages::create([
                                                    'office_id' => $sharedoffice->id,
                                                    'image' => $k,
                                                    'pic_size' => 1
                                                ]);
                                            }
                                            $index++;
                                        }

                                        \DB::commit();
//                                    }
                                    } catch (\Exception $e) {
                                        $this->error($e->getMessage());
                                        \DB::rollback();
//                                        dd($e->getMessage());
                                    }
                                }
                            });
                        });
                    });
                } catch (\Exception $e) {
                    $this->error($e->getMessage());
//                    dd($e);
                }
            }
        } catch (\Exception $e) {
            $this->info($e->getMessage());
//            dd($e);
        }
    }

    public function getQueryParameters($url)
    {
        $query_str = parse_url($url, PHP_URL_QUERY);
        parse_str($query_str, $query_params);
        return ($query_params);
    }

    public function pluckOfficeTiming($obj,$index) //0 = normal days, 1 = sat, 2 = sunday
    {
        try {
            if ($index == 0) {
                $timing = $obj[0];
                $timing = explode('-', $timing);
                return collect([
                    Carbon::parse(trim($timing[0])),
                    Carbon::parse(trim($timing[1])),
                ]);
            }
            return trim($obj[$index]) == 'Closed' ? 'OFF DAY' : $obj[$index];
        } catch (\Exception $e) {
            dump($obj);
            if ($index == 0) {
                return collect([
                    '24 Hours',
                    '24 Hours',
                ]);
            }
            return '24 Hours';
        }
    }

    public function saveImage($url)
    {
        $this->info('Saving: '.$url);
        $extension = 'png';

        if (strpos($url, 'jpg') !== false) { $extension = '.jpg'; }
        $ts = self::quickRandom();

        $ch = curl_init($url);
        $fp = fopen(public_path('uploads/sharedoffice/'.$ts.$extension), 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        return 'uploads/sharedoffice/'.$ts.$extension;
    }

    public static function quickRandom($length = 5)
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $ts = Carbon::now()->timestamp;
        $string =  substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
        return $string .'_'.$ts;
    }

    public function filterLink($href)
    {
        $link = $href[0];
        $link = explode('/', $link);
        $link = 'https://www.coworker.com/'.(str_replace('_', '/', $link[count($link)-1], $link[count($link)-1]));
        $this->info('going in '. $link);
        return $link;
    }
}
