<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;
use League\Flysystem\Sftp\SftpAdapter;
use Symfony\Component\Process\Process;

class MysqlBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mysql:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup mysql data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        Log::useDailyFiles(storage_path().'/logs/MysqlBackup.log');
//        Log::info('=== Start Backup Schedule Log===');

        $dir = Storage::directories();
        if (!array_has($dir, 'mysql_backups')) {
            Storage::makeDirectory('mysql_backups');
        }

        $db = env('DB_DATABASE');
        $db_user = env('DB_USERNAME');
        $db_pwd = env('DB_PASSWORD');
        $domain = env('DOMAIN');
        $filename = 'mysql_'.$db.'_'.$domain.strftime("_%d_%b_%Y_time_%H_%M_%S.sql",time());
        $file = '';
        $command = '';
        if( $domain == "www.xenren.co")
        {
            //unix
            $file = storage_path('app/mysql_backups/' . $filename);
            $command = 'mysqldump -u ' . $db_user . ' -p'. $db_pwd . ' ' . $db . ' > ' . $file;
        }
        else if($domain == "xenrencc.dev") //change to your local domain accordingly
        {
            //window
            //need mysqldump full path
            $file = storage_path('app\\mysql_backups\\' . $filename);
            $command = 'C:\xampp\mysql\bin\mysqldump -u ' . $db_user . ' -p'. $db_pwd . ' ' . $db . ' > ' . $file;
        }

//        Log::info('$domain : ' . $domain);
//        Log::info('$file : ' . $file);
//        Log::info('$command : ' . $command);

        $backup = new Process($command);
        $backup->run();

        if( $domain == "www.xenren.co")
        {
            //upload the xenren.co production file to test server
            //store at location /home/xenren_backup/production
            $adapter = new SftpAdapter([
                'host' => '128.199.231.203',
                'port' => 22,
                'username' => 'root',
                'password' => 'Je46f~;hSzD^2j18v6{T',
                'root' => '/home/xenren_db_backup/production',
                'timeout' => 10,
                'directoryPerm' => 0755
            ]);

            $filesystem = new Filesystem($adapter);
            $filesystem->put($filename, file_get_contents($file));
        }

//        Log::info('=== End Backup Schedule Log===');
    }
}
