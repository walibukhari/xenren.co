<?php

namespace App\Console\Commands;

use App\Models\SharedOffice;
use Illuminate\Console\Command;

class RemoveSharedOfficesDuplicateRecords extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove-duplicates:sharedOffices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will remove duplicate records identified by office name in shared offices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->confirm('Do you wish to continue?')) {
            $duplicateRecords = SharedOffice::select('office_name')
            ->selectRaw('count(`office_name`) as `occurences`')
            ->where('deleted_at',null)
            ->groupBy('office_name')
            ->having('occurences', '>', 1)
            ->get();
            if(count($duplicateRecords)){
                $dontDeleteThisRow = SharedOffice::where('office_name', $duplicateRecords[0]->office_name)->first();
                SharedOffice::where('office_name', $duplicateRecords[0]->office_name)->where('id', '!=', $dontDeleteThisRow->id)->delete();
                $this->info('Duplicates Shared Offices Records removed');
            }else{
                $this->info('No Duplicates Shared Offices Records found');
            }

        }
        
    }
}
