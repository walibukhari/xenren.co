<?php

namespace App\Console\Commands;

use App\Models\SharedOffice;
use Illuminate\Console\Command;

class SharedOfficeChangeQrCode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run:shared_office_change_qr_code';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $locale = app()->getLocale();
        $sharedOffices = SharedOffice::with('officeProducts')->get();
        $loader = $this->output->createProgressBar(count($sharedOffices));
        foreach ($sharedOffices as $offices) {
            if(count($offices->officeProducts) > 0) {
                foreach($offices->officeProducts as $products) {
                    $seatNo = $products->number;
                    $officeID = $products->office_id;
                    $qr = "https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=https://www.xenren.co/".$locale."/seat/". $seatNo ."/office/".$officeID;
                    $products->update([
                       'qr' => $qr
                    ]);
                }
            }
            $loader->advance();
        }
        $loader->finish();
    }
}
