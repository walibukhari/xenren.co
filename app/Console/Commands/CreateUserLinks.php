<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Services\GeneralService;
use Illuminate\Console\Command;

class CreateUserLinks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'User:UserLinks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	try {
    		$this->info('--- Fixing Names ---');
		    $users = \DB::select('SELECT * FROM `users` WHERE `name` = \'\' ORDER BY `id` DESC');
		    $len= count($users);
		    $this->info('--- Total Users With NO REAL NAME AND NICK NAME = '.$len.' ---');
		    $bar = $this->output->createProgressBar($len);
		    foreach ($users as $k => $v) {
		      User::where('id', '=', $v->id)->update([
		      	'name' => $v->nick_name
		      ]);
			    $bar->advance();
		    }
		    $bar->finish();
		    $this->info('.');
		
		    $this->info('--- REMOVING ALL USER LINKS ---');
		    \DB::table('users')->update(array('user_link' => null));
		    $this->info('--- CREATING USER LINKS ---');
		    $users = User::get();
		    $len = count($users);
		    $bar = $this->output->createProgressBar($len);
		    foreach ($users as $k => $v) {
			    $exist = User::where('name', '=', $v->name)->get();
			    $newUserLink = GeneralService::cleanTheLink($v->name);
			    if (count($exist) > 0) {
				    $n = GeneralService::cleanTheLink($v->name);
				    $newUserLink = ($n) .'-'. ((int)($v->id) + 1);
			    }
			    if(is_null($v->name) || $v->name == '') {
				    User::where('id', '=', $v->id)->update([
					    'user_link' => $v->id
				    ]);
			    } else {
				    User::where('id', '=', $v->id)->update([
					    'user_link' => $newUserLink
				    ]);
			    }
			    $bar->advance();
		    }
		    $bar->finish();
		    $this->info('.');
	    } catch (\Exception $e) {
    		dd($e->getMessage());
	    }
    }
}
