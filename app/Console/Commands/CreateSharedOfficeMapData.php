<?php

namespace App\Console\Commands;

use App\Models\SharedOffice;
use App\Models\SharedOfficeProducts;
use App\Models\SharedOfficeRating;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CreateSharedOfficeMapData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Create:SharedOfficeMapData';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        file_put_contents(public_path('jsons/sharedOffices.json'), "");
        $sharedoffice = DB::table('shared_offices')
            ->selectRaw("
                        DISTINCT(shared_offices.id),
                        shared_offices.id,
                        shared_offices.active,
                        shared_offices.lat,
                        shared_offices.lng,
                        office_name,
                        image,
                        location,
                        version_english,
                        version_chinese,
                        office_name_cn,
                        world_cities.name AS city_name,
                        world_countries.name AS country_name")
            ->where('active', '=', 1)
            ->leftJoin('shared_office_languages', 'shared_offices.id', '=', 'shared_office_languages.shared_offices_id')
            ->leftJoin('world_cities', 'shared_offices.city_id', '=', 'world_cities.id')
            ->leftJoin('world_countries', 'shared_offices.state_id', '=', 'world_countries.id')
            ->leftJoin('shared_office_products', 'shared_offices.id', '=', 'shared_office_products.office_id');

        $sharedoffice = $sharedoffice->limit(3000)->get();

        for ($i = 0; $i < count($sharedoffice); $i++) {
            $sharedoffice[$i]->location = addcslashes($sharedoffice[$i]->location, '"\\/');
            $sharedoffice[$i]->avg = (float)SharedOfficeRating::where('office_id', '=', $sharedoffice[$i]->id)->avg('rate');
            $sharedoffice[$i]->shared_office_rating = SharedOfficeRating::where('office_id', '=', $sharedoffice[$i]->id)->get();
            $sharedoffice[$i]->shfacilities = DB::table('shared_office_shfacilitie')->where('shared_office_id', '=', $sharedoffice[$i]->id)->get();
            $sharedoffice[$i]->min_seat_price = SharedOfficeProducts::where('office_id', '=', $sharedoffice[$i]->id)->orderBy('month_price', 'asc')->first();
        }
        $array = $sharedoffice->toArray();
        $sharedoffice = array_chunk($array, 100);
        $newJsonString = json_encode($sharedoffice, JSON_PRETTY_PRINT);
        file_put_contents(public_path('jsons/sharedOffices.json'), stripslashes($newJsonString));
    }
}
