<?php

namespace App\Console\Commands;

use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\SharedOfficeRequest;

class DeleteOldSharedOfficeRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deleteoldsharedofficerequest:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('shared_office_request')->where('status', '!=', SharedOfficeRequest::SHARED_OFFICE_PERSON_EMAIL_VERIFIED_STATUS)
            ->whereDate('created_at', Carbon::now()->subDays(7))
            ->delete();
    }
}
