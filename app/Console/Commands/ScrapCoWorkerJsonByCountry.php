<?php

namespace App\Console\Commands;

use App\Models\SharedOffice;
use App\Models\SharedOfficeImages;
use App\Models\SharedOfficeProducts;
use App\Models\WorldCities;
use App\Models\WorldContinents;
use App\Models\WorldCountries;
use App\Traits\CountryAndCityTrait;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Goutte\Client;
use phpDocumentor\Reflection\Types\Self_;
use function Clue\StreamFilter\fun;
use Illuminate\Support\Facades\Storage;

class ScrapCoWorkerJsonByCountry extends Command
{
    use CountryAndCityTrait;
    protected $country;
    /**
     * The name and signature of the console command.
     * TODO: Please run this command after  scrap:coworker
     *
     * @var string
     */
    protected $signature = 'scrap:coworker-json-by-country {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getContinent($name)
    {
        if (strpos(strtolower(trim($name)), 'asia') !== false) {
            return 'Asia';
        }
        if (strpos(strtolower(trim($name)), 'europe') !== false) {
            return 'Europe';
        }
        if (strpos(strtolower(trim($name)), 'africa') !== false) {
            return 'Africa';
        }
        if (strpos(strtolower(trim($name)), 'oceania') !== false) {
            return 'Oceania';
        }
        if (strpos(strtolower(trim($name)), 'antarctica') !== false) {
            return 'Antarctica';
        }
        if (strpos(strtolower(trim($name)), 'north America') !== false) {
            return 'North America';
        }
        if (strpos(strtolower(trim($name)), 'south America') !== false) {
            return 'South America';
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $name = (trim(implode(' ', preg_split('/(?=[A-Z])/',trim($this->argument('name'))))));
            $this->info('***************** GOING TO SCRAP JSON NOW ***********************');
            $response = self::scrapFromLink();
            $response = collect($response);
            $response = ($response->where('country', $name));

            $country = WorldCountries::where('name', $name)->first();
            $cities = !is_null($country) ? WorldCities::where('country_id', $country->id)->get() : [];
            if (!empty($cities)) {
                try {
                    \DB::beginTransaction();
                    foreach ($cities as $city) {
                        $sharedOffice = SharedOffice::where('city_id', $city->id);
                        if ($sharedOffice->count() > 0) {
                            foreach ($sharedOffice->get() as $office) {
                                // Delete product
                                $sharedOfficeProduct = SharedOfficeProducts::where('office_id', $office->id);
                                $countSharedOfficeProduct = $sharedOfficeProduct->count();
                                if ($countSharedOfficeProduct > 0) {
                                    $this->info('Deleting product '.$countSharedOfficeProduct);
                                    $sharedOfficeProduct->delete();
                                }

                                // remove existing img
                                $sharedOfficeImage = SharedOfficeImages::where('office_id', '=', $office->id);
                                if ($sharedOfficeImage->count() > 0) {
                                    foreach ($sharedOfficeImage->get() as $image) {
                                        $path = public_path( str_replace(asset('/'), '', $image->image) );
                                        \Log::info('deleting : ' . $path);
                                        if (file_exists($path)) {
                                            unlink($path);
                                        }
                                        $image->delete();
                                    }
                                }
                            }

                            $this->info('Deleting '.$city->name.', '.$sharedOffice->count().' Shared Office');
                            $sharedOffice->delete();
                        }
                    }

                    \DB::commit();
                } catch (Exception $e) {
                    \DB::rollback();
                    $this->info('error: '.$e->getMessage());
                }
            }

            $this->info('----');

            $bar = $this->output->createProgressBar(count($response));
            foreach ($response as $k => $v) {
                try {
                    $city = WorldCities::where('name', 'like', '%' . trim($v['city']) . '%')->with('country')->first();
                    if (is_null($city)) {
                        $detail = self::getCityDetail($v['city']);
                        if ($detail['status'] == 'success') {
                            $detail = $detail['data'];
                            $city = WorldCities::where('name', 'like', '%' . trim($detail->geobytescity) . '%')->first();
                            $country = WorldCountries::where('name', 'like', '%' . trim($detail->geobytescountry) . '%')->first();
                            if (is_null($country)) {
                                $continents = WorldContinents::where('name', 'like', '%' . trim(self::getContinent($detail->geobytesmapreference)) . '%')->first();
                                $country = WorldCountries::create([
                                    'continent_id' => $continents->id,
                                    'name' => $detail->geobytescountry,
                                    'full_name' => $detail->geobytescountry,
                                    'capital' => $detail->geobytescapital,
                                    'code' => $detail->geobytesinternet,
                                    'code_alpha3' => $detail->geobytesinternet,
                                    'emoji' => $detail->geobytesinternet,
                                    'has_division' => 0,
                                    'currency_code' => $detail->geobytescurrencycode,
                                    'currency_name' => $detail->geobytescurrencycode,
                                    'tld' => '.' . strtolower($detail->geobytesinternet),
                                    'callingcode' => '',
                                ]);
                            }
                            if (is_null($city)) {
                                $city = WorldCities::create([
                                    'country_id' => $country->id,
                                    'division_id' => 0,
                                    'name' => $detail->geobytesipaddress,
                                    'full_name' => $detail->geobytesipaddress,
                                    'code' => $detail->geobytesipaddress,
                                    'state_id' => 0
                                ]);
                            }
                            $cityId = $city->id;
                            $countryid = $country->id;
                        } else {
                            $cityId = 0;
                            $countryid = 0;
                        }
                    } else {
                        $country = $city->country;
                        $cityId = $city->id;
                        $countryid = $country->id;
                    }

                    $link = 'https://www.coworker.com/' . $v["coworkspace_url"];
                    $this->info('-');
                    $this->info('going in ' . $link);

                    $client = new Client();
                    $crawler = $client->request('GET', $link);
                    $bannerImages = array();
                    $name = '';
                    $officeTiming = array();
                    $latlng = array();

                    $name = $crawler->filter('.kohub_space_headings')->each(function ($innerNode) {
                        return ($innerNode->filter('h1')->text());
                    });

                    if (count($name) == 0) {
                        $this->info('Data not found!');
                        $bar->advance();
                        continue;
                    }

                    $name[0] = $v['name'];

                    $closedOffice = SharedOffice::where('office_name', '=', $v['name'].' - CLOSED');
                    if (!empty($closedOffice->get())) {
                        $closedOffice->delete();
                    }

                    $office = SharedOffice::where('office_name', '=', $v['name'])
                        ->with('officeProducts')
                        ->first();
//                    if (is_null($office)) {
                    $bannerImages = $crawler->filter('.coworker-banner-panel-21-11-18 > .container-fluid')->each(function ($innerNode) {
                        //Crawling Banner Images
                        $bannerImages1 = array();
                        $bannerImages1 = $innerNode->filter('.coworker-banner-outer-21-11-18')->each(function ($innerNode1) {
                            $bannerImages2 = array();
                            $bannerImages2 = $innerNode1->filter('.pad_none > a')->each(function ($innerNode2) {
                                $imageName = self::saveImage($innerNode2->filter('div > img')->eq(0)->attr('src'));
                                return $imageName;
                            });
                            return $bannerImages2;
                        });
                        return $bannerImages1;
                    });

                    $latlng = $crawler->filter('.space_content_map')->each(function ($innerNode) {
                        $q = self::getQueryParameters($innerNode->filter('iframe')->eq(0)->attr('src'));
                        return (explode(',', $q['q']));
                    });


                    $contact = $crawler->filter('.add-phone-number-con')->each(function ($innerNode) {
                        return ($innerNode->filter('a')->text());
                    });
                    $this->info('-');
                    $contact = (count($contact)) > 0 ? $contact[0] : '';


                    $officeTiming = $crawler->filter('.open_timings_con')->each(function ($innerNode) {
                        return $innerNode->text();
                    });

                    $address = $crawler->filter('.muchroom_mail')->count() > 0 ? ($crawler->filter('.muchroom_mail')->text()) : '';
                    $description = $crawler->filter('#p_description')->count() > 0 ? ($crawler->filter('#p_description')->text()) : '';
                    $bannerImages = isset($bannerImages[0][0]) ? $bannerImages[0][0] : [];
                    if ($crawler->filter('.space_shadow_div2 > .space-new-contact-bar200719 > .space-new-contact-top200719 > strong')->count() > 0) {
                        $officeAmount = ($crawler->filter('.space_shadow_div2 > .space-new-contact-bar200719 > .space-new-contact-top200719 > strong')->text());
                        $officeAmount = (double)preg_replace("/[^0-9.]/", "", $officeAmount);
                    }

                    // remove duplicate office
                    $duplicateOffice = SharedOffice::where('office_name', '=', $v['name'])
                        ->where('location', '=', $address);
                    $duplicateOffice = !is_null($office) ? $duplicateOffice->where('id', '!=', $office->id) : $duplicateOffice;
                    if ($duplicateOffice->count() > 1) {
                        $this->info('----');
                        $this->info('Deleting duplicate');
                        $duplicateOffice->delete();
                    }

                    $hotDesks = $crawler->filter('.tab-content > #hot_desk > div > .Open_Plan_table > table > .tbody_section > tr > td')->each(function ($data) {
                        return ($data->text());
                    });

                    $h_desks = array();
                    if(count($hotDesks) > 0 && strlen($hotDesks[0]) < 5) {
                        $hotDesks = (array_chunk($hotDesks, 5));
                        foreach ($hotDesks as $k1 => $v1) {
                            $type = self::checkWhichPriceTypeExist($v1[1]);
                            if($type == 'Day') {
                                array_push($h_desks, array(
                                    'category_id' => SharedOfficeProducts::CATEGORY_HOT_DESK,
                                    'no_of_peoples' => ($v1[0]),
                                    'daily_price' => round(self::pluckPrice($v1[1], $v1[2], 1), 2),
                                ));
                            } else if($type == 'Month') {
                                array_push($h_desks, array(
                                    'category_id' => SharedOfficeProducts::CATEGORY_HOT_DESK,
                                    'no_of_peoples' => ($v1[0]),
                                    'month_price' => round(self::pluckPrice($v1[1], $v1[2], 2), 2),
                                ));
                            } else if($type == 'Week') {
                                array_push($h_desks, array(
                                    'category_id' => SharedOfficeProducts::CATEGORY_HOT_DESK,
                                    'no_of_peoples' => ($v1[0]),
                                    'weekly_price' => round(self::pluckPrice($v1[1], $v1[2], 4), 2),
                                ));
                            } else if($type == 'Hour') {
                                array_push($h_desks, array(
                                    'category_id' => SharedOfficeProducts::CATEGORY_HOT_DESK,
                                    'no_of_peoples' => ($v1[0]),
                                    'hourly_price' => round(self::pluckPrice($v1[1], $v1[2], 3), 2),
                                ));
                            }
                        }
                    }


                    $dedicatedDesks = $crawler->filter('.tab-content > #dedicated_desk > div > .Open_Plan_table > table > .tbody_section > tr > td')->each(function ($data) {
                        return ($data->text());
                    });

                    $d_desks = array();
                    if(count($dedicatedDesks) > 0 && strlen($dedicatedDesks[0]) < 5) {
                        $dedicatedDesks = (array_chunk($dedicatedDesks, 5));
                        foreach ($dedicatedDesks as $k1 => $v1) {
                            $type = self::checkWhichPriceTypeExist($v1[1]);
                            if($type == 'Day') {
                                array_push($d_desks, array(
                                    'category_id' => SharedOfficeProducts::CATEGORY_DEDICATED_DESK,
                                    'no_of_peoples' => ($v1[0]),
                                    'daily_price' => self::pluckPrice($v1[1], $v1[2], 1),

                                ));
                            } else if($type == 'Month') {
                                array_push($d_desks, array(
                                    'category_id' => SharedOfficeProducts::CATEGORY_DEDICATED_DESK,
                                    'no_of_peoples' => ($v1[0]),
                                    'month_price' => self::pluckPrice($v1[1], $v1[2], 2),
                                ));
                            } else if($type == 'Hour') {
                                array_push($d_desks, array(
                                    'category_id' => SharedOfficeProducts::CATEGORY_DEDICATED_DESK,
                                    'no_of_peoples' => ($v1[0]),
                                    'hourly_price' => self::pluckPrice($v1[1], $v1[2], 3),
                                ));
                            } else if($type == 'Week') {
                                array_push($d_desks, array(
                                    'category_id' => SharedOfficeProducts::CATEGORY_DEDICATED_DESK,
                                    'no_of_peoples' => ($v1[0]),
                                    'weekly_price' => self::pluckPrice($v1[1], $v1[2], 4),
                                ));
                            }
                        }
                    }


                    $officeDesk = $crawler->filter('.tab-content > #private_office > div > .Open_Plan_table > table > .tbody_section > tr > td')->each(function ($data) {
                        return ($data->text());
                    });
                    $office_desk = array();
                    if(count($officeDesk) > 0 && strlen($officeDesk[0]) < 5) {
                        $officeDesk = (array_chunk($officeDesk, 5));
                        foreach ($officeDesk as $k1 => $v1) {
                            $type = self::checkWhichPriceTypeExist($v1[1]);
                            if($type == 'Day') {
                                array_push($office_desk, array(
                                    'category_id' => SharedOfficeProducts::CATEGORY_MEETING_ROOM,
                                    'no_of_peoples' => ($v1[0]),
                                    'daily_price' => self::pluckPrice($v1[1], $v1[2], 1),
                                ));
                            } else if($type == 'Month') {
                                array_push($office_desk, array(
                                    'category_id' => SharedOfficeProducts::CATEGORY_MEETING_ROOM,
                                    'no_of_peoples' => ($v1[0]),
                                    'month_price' => self::pluckPrice($v1[1], $v1[2], 2),
                                ));
                            } else if($type == 'Hour') {
                                array_push($office_desk, array(
                                    'category_id' => SharedOfficeProducts::CATEGORY_MEETING_ROOM,
                                    'no_of_peoples' => ($v1[0]),
                                    'hourly_price' => self::pluckPrice($v1[1], $v1[2], 3),
                                ));
                            } else if($type == 'Week') {
                                array_push($office_desk, array(
                                    'category_id' => SharedOfficeProducts::CATEGORY_MEETING_ROOM,
                                    'no_of_peoples' => ($v1[0]),
                                    'weekly_price' => self::pluckPrice($v1[1], $v1[2], 4),
                                ));
                            }
                        }
                    }

                    $feeData = array_merge($h_desks, $d_desks, $office_desk);
                    $continentID = WorldCountries::where('id','=',$countryid)->first();
                    try {
                        if (is_null($office)) {
                            // dump('is null country id');
                            // dump($countryid);
                            \DB::beginTransaction();
                            $sharedoffice = SharedOffice::create([
                                'office_name' => $name[0],
                                'location' => $address,
                                'description' => $description,
                                'office_manager' => '',
                                'qr' => 'dummy',
                                'image' => count($bannerImages) > 0 ? $bannerImages[0] : '',
                                'pic_size' => 1,
                                'staff_id' => 1,
                                'office_size_x' => 0,
                                'office_size_y' => 0,
                                'number' => 0,
                                'city_id' => isset($cityId) ? $cityId : 0,
                                'state_id' => isset($countryid) ? $countryid : 0,
                                'currency_id' => isset($countryid) ? $countryid : 0,
                                'continent_id' => isset($continentID->continent_id) ? $continentID->continent_id : 0,
                                'lat' => $latlng[0][0],
                                'lng' => $latlng[0][1],
                                'contact_name' => 0,
                                'contact_phone' => $contact,
                                'contact_email' => 0,
                                'contact_address' => 0,
                                'monday_opening_time' => self::pluckOfficeTiming($officeTiming, 0)[0],
                                'monday_closing_time' => self::pluckOfficeTiming($officeTiming, 0)[1],
                                'saturday_opening_time' => self::pluckOfficeTiming($officeTiming, 1),
                                'saturday_closing_time' => self::pluckOfficeTiming($officeTiming, 1),
                                'sunday_opening_time' => self::pluckOfficeTiming($officeTiming, 2),
                                'sunday_closing_time' => self::pluckOfficeTiming($officeTiming, 2),
                                'verify_info_to_scan' => 0,
                                'require_deposit' => 1,
                                'office_space' => 0,
                                'version_english' => 1,
                                'version_chinese' => 0,
                                'active' => 1,
                                'likes' => 0,
                            ]);
                            $sharedoffice = SharedOffice::find($sharedoffice->id);
                            $sharedoffice->qr = "https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=office:" . $sharedoffice->id;
                            $sharedoffice->save();
                            foreach ($feeData as $key => $val) {
                                $sharedOfficeProduct = SharedOfficeProducts::create([
                                    'office_id' => $sharedoffice->id,
                                    'qr' => $sharedoffice->qr,
                                    'remark' => 'n/a',
                                    'category_id' => $val['category_id'],
                                    'no_of_peoples' => $val['no_of_peoples'],
                                    'month_price' => isset($val['month_price']) && $val['month_price'] != '' ? $val['month_price'] : 0,
                                    'hourly_price' => isset($val['hourly_price']) && $val['hourly_price'] != '' ? $val['hourly_price'] : 0,
                                    'weekly_price' => isset($val['weekly_price']) && $val['weekly_price'] != '' ? $val['weekly_price'] : 0,
                                    'daily_price' => isset($val['daily_price']) && $val['daily_price'] != '' ? $val['daily_price'] : 0,
                                    'type' => $val['category_id'],
                                    'time_price' => isset($officeAmount) && $officeAmount != '' ? $officeAmount : '',
                                    'number' => $key,
                                    'status_id' => SharedOfficeProducts::STATUS_EMPTY
                                ]);
                                self::updateSharedOfficeProducts($sharedOfficeProduct->id);
                            }
                            $index = 0;
                            foreach ($bannerImages as $k) {
                                if ($index > 0) {
                                    SharedOfficeImages::create([
                                        'office_id' => $sharedoffice->id,
                                        'image' => $k,
                                        'pic_size' => 1
                                    ]);
                                }
                                $index++;
                            }
                            \DB::commit();
                            $bar->advance();
                        } else {
                            // dump('is not null country id');
                            // dump($countryid);
                            // dump('$office->officeProducts->id');
                            \DB::beginTransaction();
                            $sharedoffice = SharedOffice::where('id', '=', $office->id)->first();
                            $sharedoffice->update([
                                'office_name' => $name[0],
                                'location' => $address,
                                'description' => $description,
                                'office_manager' => '',
                                'qr' => 'dummy',
                                'image' => count($bannerImages) > 0 ? $bannerImages[0] : '',
                                'pic_size' => 1,
                                'staff_id' => 1,
                                'office_size_x' => 0,
                                'office_size_y' => 0,
                                'number' => 0,
                                'city_id' => isset($cityId) ? $cityId : 0,
                                'state_id' => isset($countryid) ? $countryid : $office->state_id,
                                'currency_id' => isset($countryid) ? $countryid : $office->state_id,
                                'continent_id' => isset($continentID->continent_id) ? $continentID->continent_id : 0,
                                'lat' => $latlng[0][0],
                                'lng' => $latlng[0][1],
                                'contact_name' => 0,
                                'contact_phone' => $contact,
                                'contact_email' => 0,
                                'contact_address' => 0,
                                'monday_opening_time' => self::pluckOfficeTiming($officeTiming, 0)[0],
                                'monday_closing_time' => self::pluckOfficeTiming($officeTiming, 0)[1],
                                'saturday_opening_time' => self::pluckOfficeTiming($officeTiming, 1),
                                'saturday_closing_time' => self::pluckOfficeTiming($officeTiming, 1),
                                'sunday_opening_time' => self::pluckOfficeTiming($officeTiming, 2),
                                'sunday_closing_time' => self::pluckOfficeTiming($officeTiming, 2),
                                'verify_info_to_scan' => 0,
                                'require_deposit' => 1,
                                'office_space' => 0,
                                'version_english' => 1,
                                'version_chinese' => 0,
                                'active' => 1,
                                'likes' => 0,
                            ]);
                            $sharedoffice = SharedOffice::find($office->id);
                            $sharedoffice->qr = "https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=office:" . $sharedoffice->id;
                            $sharedoffice->save();

                            // Delete product
                            $sharedOfficeProduct = SharedOfficeProducts::where('office_id', $sharedoffice->id);
                            if (!empty($sharedOfficeProduct->get())) {
                                $this->info('Deleting product');
                                $sharedOfficeProduct->delete();
                            }

                            foreach ($feeData as $key => $val) {
                                $sharedOfficeProduct = SharedOfficeProducts::create([
                                    'office_id' => $sharedoffice->id,
                                    'qr' => $sharedoffice->qr,
                                    'remark' => 'n/a',
                                    'category_id' => $val['category_id'],
                                    'no_of_peoples' => $val['no_of_peoples'],
                                    'month_price' => isset($val['month_price']) && $val['month_price'] != '' ? $val['month_price'] : 0,
                                    'hourly_price' => isset($val['hourly_price']) && $val['hourly_price'] != '' ? $val['hourly_price'] : 0,
                                    'weekly_price' => isset($val['weekly_price']) && $val['weekly_price'] != '' ? $val['weekly_price'] : 0,
                                    'daily_price' => isset($val['daily_price']) && $val['daily_price'] != '' ? $val['daily_price'] : 0,
                                    'type' => $val['category_id'],
                                    'time_price' => isset($officeAmount) && $officeAmount != '' ? $officeAmount : '',
                                    'number' => $key,
                                    'status_id' => SharedOfficeProducts::STATUS_EMPTY
                                ]);
                                self::updateSharedOfficeProducts($sharedOfficeProduct->id);
                            }

                            // remove existing img
                            $sharedOfficeImage = SharedOfficeImages::where('office_id', '=', $sharedoffice->id);
                            if ($sharedOfficeImage->count() > 0) {
                                foreach ($sharedOfficeImage->get() as $image) {
                                    $path = public_path( str_replace(asset('/'), '', $image->image) );
                                    \Log::info('deleting : ' . $path);
                                    if (file_exists($path)) {
                                        unlink($path);
                                    }
                                    $image->delete();
                                }
                            }

                            $index = 0;
                            foreach ($bannerImages as $k) {
                                if ($index > 0) {
                                    SharedOfficeImages::create([
                                        'office_id' => $office->id,
                                        'image' => $k,
                                        'pic_size' => 1
                                    ]);
                                }
                                $index++;
                            }

                            \DB::commit();
                            $bar->advance();
                        }
                    } catch (\Exception $e) {
                        \DB::rollback();
                        dump($e->getMessage());
                        dump($e->getLine());
                        dd($e->getFile());
                    }
//                    }
                } catch (\Exception $e) {
                    $this->info('Error found :');
                    $this->info($e->getMessage());
                    // dd($e->getTrace());

                    $bar->advance();
                }
            }
            $bar->finish();
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    public function checkWhichPriceTypeExist($priceType){
        $type = str_replace(' ','', preg_replace('/\d/', '', $priceType ));
        return $type;
    }

    public function pluckPrice($duration, $price, $type)
    {
        $duration = self::getDurationType($duration);
        $price = (double)preg_replace("/[^0-9.]/", "", $price);
        if ($type == 1) {
            if ($duration == 1) {
                return $price;
            }
            if ($duration == 2) {
                return $price / 30;
            }
            if ($duration == 3) {
                return ($price * 24);
            }
            if ($duration == 4) {
                return $price * 24;
            }
        }
        if ($type == 2) {
            if ($duration == 1) {
                return $price / 30;
            }
            if ($duration == 2) {
                return $price;
            }
            if ($duration == 3) {
                return ($price * 24) * 30;
            }
            if ($duration == 4) {
                return $price * 24;
            }
        }
        if ($type == 3) {
            if ($duration == 1) {
                return ($price * 7);
            }
            if ($duration == 2) {
                return $price * 30;
            }
            if ($duration == 3) {
                return ($price);
            }
            if ($duration == 4) {
                return $price * 7;
            }
        }
        if ($type == 4) {
            if ($duration == 1) {
                return $price / 7;
            }
            if ($duration == 2) {
                return $price * 4;
            }
            if ($duration == 3) {
                return ($price * 7) * 24;
            }
            if ($duration == 4) {
                return $price;
            }
        }
    }

    public function getDurationType($duration)
    {
        if (strpos($duration, "Day")) {
            return 1;
        }
        if (strpos($duration, "Month")) {
            return 2;
        }
        if (strpos($duration, "Hour")) {
            return 3;
        }
        if (strpos($duration, "Week")) {
            return 4;
        }
    }

    public function updateSharedOfficeProducts($id)
    {
        $sharedOfficeProduct = SharedOfficeProducts::where('id', '=', $id)->first();
        $sharedOfficeProduct = $sharedOfficeProduct != null ? $sharedOfficeProduct->update([
            'number' => $id
        ]) : null;
    }

    public function cleanData($data)
    {
        return str_replace('PKR', '', $data);
    }

    public function scrapFromLink()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.coworker.com/cloudfront/app/search/results.json",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Postman-Token: bce9ad66-9346-4f14-af59-82da71bd1158",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, true);
        }
    }

    public function pluckOfficeTiming($obj, $index) //0 = normal days, 1 = sat, 2 = sunday
    {
        try {
            if ($index == 0) {
                $timing = $obj[0];
                $timing = explode('-', $timing);
                return collect([
                    Carbon::parse(trim($timing[0])),
                    Carbon::parse(trim($timing[1])),
                ]);
            }
            return trim($obj[$index]) == 'Closed' ? 'OFF DAY' : $obj[$index];
        } catch (\Exception $e) {
            // dump($obj);
            if ($index == 0) {
                return collect([
                    '24 Hours',
                    '24 Hours',
                ]);
            }
            return '24 Hours';
        }
    }

    public function getQueryParameters($url)
    {
        $query_str = parse_url($url, PHP_URL_QUERY);
        parse_str($query_str, $query_params);
        return ($query_params);
    }

    public function saveImage($url)
    {
        $this->info('Saving: ' . $url);
        $extension = 'png';

        if (strpos($url, 'jpg') !== false) {
            $extension = '.jpg';
        }
        $ts = self::quickRandom();

        if (!file_exists(public_path('uploads'))) {
            mkdir(public_path('uploads'));
        }

        if (!file_exists(public_path('uploads/sharedoffice'))) {
            mkdir(public_path('uploads/sharedoffice'));
        }

        $ch = curl_init($url);
        $fp = fopen(public_path('uploads/sharedoffice/' . $ts . $extension), 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        return 'uploads/sharedoffice/' . $ts . $extension;
    }

    public static function quickRandom($length = 5)
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $ts = Carbon::now()->timestamp;
        $string = substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
        return $string . '_' . $ts;
    }

    public function filterLink($href)
    {
        $link = $href[0];
        $link = explode('/', $link);
        $link = 'https://www.coworker.com/' . (str_replace('_', '/', $link[count($link) - 1], $link[count($link) - 1]));
        $this->info('going in ' . $link);
        return $link;
    }
}
