<?php

namespace App\Console\Commands;

use App\Http\Controllers\Services\SharedOfficeController;
use App\Models\SharedOfficeFinance;
use Carbon\Carbon;
use Illuminate\Console\Command;

class freeUpSeats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SharedOffice:freeSeats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make shared office seats free if using time is greater than or equal to 16 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->freeSeats();
    }



    public function freeSeats()
    {
        $FreeSeats = new SharedOfficeFinance();
        $_sharedOfficeController = new SharedOfficeController();
        $data = $FreeSeats->with('productfinance')
            ->whereHas('productfinance', function ($q) {
                $q->where('status_id', '=', 1);
            })
            ->whereNull('end_time')
            ->get();
        $bar = $this->output->createProgressBar(count($data));
        foreach ($data as $key => $finance) {
            $startTime = $finance->start_time;
            $currentTime = Carbon::now();
            $calculateHours = $currentTime->diffInHours($startTime);
            if($calculateHours >= 16){
                $request = new \Illuminate\Http\Request();
                $result = $_sharedOfficeController->sharedofficeStopTimer($request, $finance->product_id, $finance->user_id);

                if($result['status'] != 'success' ){
                    $this->info('**************** FREE SEATS CRON ERROR ****************');
                    $this->info($result);
                    $this->info('*******************************************************');

                    \Log::ERROR('**************** FREE SEATS CRON ERROR ****************');
                    \Log::ERROR($result);
                    \Log::ERROR('*******************************************************');
                }
            }
            $bar->advance();
        }
        $bar->finish();
    }
}
