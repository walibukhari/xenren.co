<?php

namespace App\Console\Commands;

use App\Models\ProjectDispute;
use App\Models\ProjectMilestones;
use App\Models\ProjectMilestoneStauts;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ReleaseFundsAfterSpecificTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Command:ReleaseFundsAfterSpecificTime';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $record = ProjectMilestones::where('status', '=', ProjectMilestones::STATUS_CANCEL_REQUEST_BY_EMPLOYER)->where('created_at', '<', Carbon::now()->subHour(72)->toDateTimeString())->get();

        foreach ($record as $k => $v) {
            ProjectMilestoneStauts::where('milestone_id', '=', $v->milestone_id)->update([
                'status' => ProjectMilestones::STATUS_CONFIRMED
            ]);
        }

        return true;
    }
}
