<?php

namespace App\Console\Commands;

use App\Http\Controllers\CronJobController;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class DeleteObsoleteViewContactLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deleteobsoleteviewcontactlogs:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'User View Contact Log if more than 3 days will be auto delete.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        CronJobController::deleteObsoleteViewContactLogs();
    }
}
