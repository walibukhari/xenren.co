<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;
use League\Flysystem\Sftp\SftpAdapter;
use Symfony\Component\Process\Process;

class DeleteMysqlBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deletemysqlbackup:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete mysql backup data after 72 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        Log::useDailyFiles(storage_path().'/logs/DeleteMysqlBackup.log');
        Log::info('=== Start Delete Mysql Backup ===');

        $domain = env('DOMAIN');

        //get the specific directory
        if( $domain == "www.xenren.co")
        {
            //unix
            $directory = storage_path('app/mysql_backups/');
        }
        else if($domain == "xenrencc.dev") //change to your local domain accordingly
        {
            //window
            //need mysqldump full path
            $directory = storage_path('app\\mysql_backups\\');
        }

        //check the file whether already 72 hour
        $files = File::allFiles($directory);
        foreach ($files as $file)
        {
            $timestamp = File::lastModified($file);
            $fileCreatedDate = Carbon::createFromTimestamp($timestamp);
            $now = Carbon::now();
            $differentHour = $now->diffInHours($fileCreatedDate);

            //delete those old file
            if( $differentHour >= 72)
            {
                Log::info('$fileCreatedDate : ' . $fileCreatedDate);
                Log::info('$now : ' . $now);
                Log::info('$differentHour : ' . $differentHour);
                Log::info('$file : ' . $file);

                @unlink($file);
            }
        }



        Log::info('=== End Delete Mysql Backup ===');
    }
}
