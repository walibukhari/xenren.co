<?php

namespace App\Console\Commands;

use App\Http\Controllers\LocationsController;
use App\Models\Countries;
use App\Models\CountryCities;
use App\Models\States;
use Illuminate\Console\Command;

class CreateLocationsDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:create-locations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        self::infoLog('Renaming Old Tables');
        LocationsController::renameOldTable();
        self::infoLog('Fetching Countries');
        $countries = self::getCountries();
        self::infoLog('Creating Countries Table');
        LocationsController::createCountriesTable();
        self::infoLog('Inserting Countries Table');

        foreach ($countries as $k => $v) {
            $bar = $this->output->createProgressBar(count($v));
            foreach ($v as $k1 => $v1) {
                $a = \DB::table('x_countries')->where('code', '=', $v1['sortname'])->first();
                Countries::create([
                    'id' => $v1['id'],
                    'code' => $v1['sortname'],
                    'name' => $v1['name'],
                    'name_cn' => isset($a->name_cn) ? $a->name_cn : ''
                ]);
                $bar->advance();
            }
            $bar->finish();
        }

        self::infoLog('Fetching States');
        $states = self::getStates();
        self::infoLog('Create States Table');
        LocationsController::createStatesTable();
        self::infoLog('Inserting States Table');

        foreach ($states as $k => $v) {
            $bar = $this->output->createProgressBar(count($v));
            foreach ($v as $k1 => $v1) {
                States::create([
                    'name' => $v1['name'],
                    'country_id' => $v1['country_id']
                ]);
                $bar->advance();
            }
            $bar->finish();
        }

        self::infoLog('Fetching Cities');
        $cities = self::getCities();
        self::infoLog('Create Cities Table');
        LocationsController::createCitiesTable();
        self::infoLog('Inserting Cities Table');

        foreach ($cities as $k => $v) {
            $bar = $this->output->createProgressBar(count($v));
            foreach ($v as $k1 => $v1) {
                $a = \DB::table('x_country_cities')->where('name', '=', $v1['name'])->first();
                CountryCities::create([
                    'name' => $v1['name'],
                    'lat' => isset($a->lat) ? $a->lat : '',
                    'lon' => isset($a->lon) ? $a->lon : '',
                    'state_id' => $v1['state_id']
                ]);

                $bar->advance();
            }
            $bar->finish();
        }

        self::infoLog('Database Created..!');
        return true;
    }

    public function getCountries()
    {
        $path = public_path() . "/jsons/countries.json"; // ie: /var/www/laravel/app/storage/json/filename.json
        $countries = json_decode(file_get_contents($path), true);
        return $countries;
    }
    public function getCities()
    {
        $path = public_path() . "/jsons/cities.json"; // ie: /var/www/laravel/app/storage/json/filename.json
        $cities = json_decode(file_get_contents($path), true);
        return $cities;
    }
    public function getStates()
    {
        $path = public_path() . "/jsons/states.json"; // ie: /var/www/laravel/app/storage/json/filename.json
        $states = json_decode(file_get_contents($path), true);
        return $states;
    }

    public function infoLog($msg)
    {
        $this->info('|*********************************'.$msg.'*********************************|');
    }
}
