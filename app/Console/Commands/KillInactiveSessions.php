<?php

namespace App\Console\Commands;

use App\Models\ImageModel;
use App\Models\TimeTrack;
use Carbon\Carbon;
use Illuminate\Console\Command;

class KillInactiveSessions extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'kill:inactive-sessions';
	
	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';
	
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		try {
			$sessions = TimeTrack::where('end_time', '=', null)->orWhere('end_time', '=', '0000-00-00 00:00:00')->get();
			foreach ($sessions as $k => $v) {
				$img = ImageModel::where('user_time_tracker_id', '=', $v->id)->orderBy('id', 'desc')->first();
				if(!is_null($img)) {
					
					$lastImageTime = isset($img->created_at) ? Carbon::parse($img->created_at) : Carbon::now()->subMinute(10);
					$now = Carbon::now();
					$diff = $now->diffInSeconds($lastImageTime);
					
					if ($diff > 320) { //5 minutes 20 seconds
						TimeTrack::where('id', '=', $v->id)->update([
							'end_time' => $v->start_time,
							'auto_end' => 1
						]);
						
//						if (!isset($v->end_time) || is_null($v->end_time) || $v->end_time == '0000-00-00 00:00:00') {
//							TimeTrack::where('id', '=', $v->id)->update([
//								'end_time' => $v->start_time,
//								'auto_end' => 1
//							]);
//						}
					}
					
				}
			}
		}catch (\Exception $e) {
			dd($e->getLine());
		}
	}
}
