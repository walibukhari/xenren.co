<?php

namespace App\Console\Commands;

use App\Models\SharedOfficeProducts;
use Illuminate\Console\Command;

class GenerateUniqueSeatNumberCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GenerateUniqueSeatNumber:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Fetching All Shared Office Products!');
        $allItems = SharedOfficeProducts::get();
        $this->info($allItems->count() . ' Products found!');
        $this->output->progressStart($allItems->count());

        foreach($allItems as $item){
            $number = $this->getUniqueRandomNumber();
            $item->number = $number;
            $item->qr = 'https://api.qrserver.com/v1/create-qr-code/?size=160x170&data='.$number;
            $item->save();
            $this->output->progressAdvance();
        }
        $this->output->progressFinish();
        $this->info('All Products Updated!');
    }

    public function generateNum()
    {
        return mt_rand(1,1000);
    }

    public function getUniqueRandomNumber()
    {
        $num = self::generateNum();
        $items = SharedOfficeProducts::where('number', '=', $num)->get();
        if(isset($items)) {
            return $num;
        }
        self::getUniqueRandomNumber();
    }
}
