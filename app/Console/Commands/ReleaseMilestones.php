<?php

namespace App\Console\Commands;

use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ReleaseMilestones extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'release-milestone:today';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $transactions = Transaction::where('release_at', '=', null)->get();
        foreach ($transactions as $k => $v) {
            $releaseDate = Carbon::parse($v->created_at)->addDays(config('milestones.system_hold_days'));
            Transaction::where('id', '=', $v->id)->update([
               'release_at' => $releaseDate
            ]);
        }
        self::releaseMilestonesScheduledForToday();
    }

    public function releaseMilestonesScheduledForToday()
    {
        $transactions = Transaction::with('milestone')->whereBetween('release_at',[
            Carbon::today()->startOfYear(), Carbon::today()->endOfDay()
        ])
            ->where('status', '=', Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM)
            ->where(function ($q){
                $q->where('type', '=', Transaction::TYPE_MILESTONE_PAYMENT)
                    ->orWhere('type', '=', Transaction::TYPE_SERVICE_FEE)
                    ->orWhere('type', '=', Transaction::TYPE_BONUS);
            })
            ->get();

        foreach ($transactions as $k => $v) {
            \DB::beginTransaction();

            $v->update([
                'status' => Transaction::STATUS_APPROVED
            ]);

            if(isset($v->milestone)) {
                if ($v->type == Transaction::TYPE_SERVICE_FEE) {
                    User::where('id', '=', $v->milestone['applicant_id'])->decrement('account_balance', $v->amount);
                } else {
                    User::where('id', '=', $v->milestone['applicant_id'])->increment('account_balance', $v->amount);
                }
            } else {
                if ($v->type == Transaction::TYPE_SERVICE_FEE) {
                    self::log('decrementing'.$v->amount);
                    User::where('id', '=', $v->performed_to)->decrement('account_balance', $v->amount);
                } else {
                    self::log('incrementing'.$v->amount);
                    User::where('id', '=', $v->performed_to)->increment('account_balance', $v->amount);
                }
            }
            \DB::commit();
        }
        self::log(' *- *- *-*- * -*- *- *- *- *- *- *- *- *-* - *-* - *- *- *- *-*- * -*- *- *- *- *- *- *- *- *-* - *-* - *-* -');
    }

    protected function log($log)
    {
        \Log::channel('release-daily-milestone')->info($log);
    }
}
