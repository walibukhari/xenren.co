<?php

namespace App\Console\Commands;

use App\Models\SharedOffice;
use App\Models\SharedOfficeImages;
use App\Models\WorldCities;
use App\Models\WorldContinents;
use App\Models\WorldCountries;
use App\Traits\CountryAndCityTrait;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Goutte\Client;
use phpDocumentor\Reflection\Types\Self_;

class ScrapCoWorkerJson extends Command
{
    use CountryAndCityTrait;

    /**
     * The name and signature of the console command.
     * TODO: Please run this command after  scrap:coworker
     *
     * @var string
     */
    protected $signature = 'scrap:coworker-json';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getContinent($name)
    {
        if (strpos(strtolower(trim($name)), 'asia') !== false) {
            return 'Asia';
        }
        if (strpos(strtolower(trim($name)), 'europe') !== false) {
            return 'Europe';
        }
        if (strpos(strtolower(trim($name)), 'africa') !== false) {
            return 'Africa';
        }
        if (strpos(strtolower(trim($name)), 'oceania') !== false) {
            return 'Oceania';
        }
        if (strpos(strtolower(trim($name)), 'antarctica') !== false) {
            return 'Antarctica';
        }
        if (strpos(strtolower(trim($name)), 'north America') !== false) {
            return 'North America';
        }
        if (strpos(strtolower(trim($name)), 'south America') !== false) {
            return 'South America';
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->info('***************** GOING TO SCRAP JSON NOW ***********************');
            $response = self::scrapFromLink();
            $bar = $this->output->createProgressBar(count($response));
            foreach ($response as $k => $v) {
                try {
                    $city = WorldCities::where('name', 'like', '%' . trim($v['city']) . '%')->with('country')->first();
                    if(is_null($city)) {
                        $detail = self::getCityDetail($v['city']);
                        if ($detail['status'] == 'success') {
                            $detail = $detail['data'];
                            $city = WorldCities::where('name', 'like', '%' . trim($detail->geobytescity) . '%')->first();
                            $country = WorldCountries::where('name', 'like', '%' . trim($detail->geobytescountry) . '%')->first();
                            if (is_null($country)) {
                                $continents = WorldContinents::where('name', 'like', '%' . trim(self::getContinent($detail->geobytesmapreference)) . '%')->first();
                                $country = WorldCountries::create([
                                    'continent_id' => $continents->id,
                                    'name' => $detail->geobytescountry,
                                    'full_name' => $detail->geobytescountry,
                                    'capital' => $detail->geobytescapital,
                                    'code' => $detail->geobytesinternet,
                                    'code_alpha3' => $detail->geobytesinternet,
                                    'emoji' => $detail->geobytesinternet,
                                    'has_division' => 0,
                                    'currency_code' => $detail->geobytescurrencycode,
                                    'currency_name' => $detail->geobytescurrencycode,
                                    'tld' => '.' . strtolower($detail->geobytesinternet),
                                    'callingcode' => '',
                                ]);
                            }
                            if (is_null($city)) {
                                $city = WorldCities::create([
                                    'country_id' => $country->id,
                                    'division_id' => 0,
                                    'name' => $detail->geobytesipaddress,
                                    'full_name' => $detail->geobytesipaddress,
                                    'code' => $detail->geobytesipaddress,
                                    'state_id' => 0
                                ]);
                            }
                            $cityId = $city->id;
                            $countryid = $country->id;
                        } else {
                            $cityId = 0;
                            $countryid = 0;
                        }
                    } else{
                        $country = $city->country;
                        $cityId = $city->id;
                        $countryid = $country->id;
                    }

                    $link = 'https://www.coworker.com/' . $v["coworkspace_url"];
                    $this->info('-');
                    $this->info('going in ' . $link);

                    $client = new Client();
                    $crawler = $client->request('GET', $link);
                    $bannerImages = array();
                    $name = '';
                    $officeTiming = array();
                    $latlng = array();

                    $name = $crawler->filter('.kohub_space_headings')->each(function ($innerNode) {
                        return ($innerNode->filter('h1')->text());
                    });
                    $office = SharedOffice::where('office_name', '=', $v['name'])->first();
//                    if (is_null($office)) {
                    $bannerImages = $crawler->filter('.coworker-banner-panel-21-11-18 > .container-fluid')->each(function ($innerNode) {
                        //Crawling Banner Images
                        $bannerImages1 = array();
                        $bannerImages1 = $innerNode->filter('.coworker-banner-outer-21-11-18')->each(function ($innerNode1) {
                            $bannerImages2 = array();
                            $bannerImages2 = $innerNode1->filter('.pad_none > a')->each(function ($innerNode2) {
                                $imageName = self::saveImage($innerNode2->filter('div > img')->eq(0)->attr('src'));
                                return $imageName;
                            });
                            return $bannerImages2;
                        });
                        return $bannerImages1;
                    });

                    $latlng = $crawler->filter('.space_content_map')->each(function ($innerNode) {
                        $q = self::getQueryParameters($innerNode->filter('iframe')->eq(0)->attr('src'));
                        return (explode(',', $q['q']));
                    });


                    $contact = $crawler->filter('.add-phone-number-con')->each(function ($innerNode) {
                        return ($innerNode->filter('a')->text());
                    });
                    dump($contact);
                    $this->info('-');
                    $contact = (count($contact)) > 0 ? $contact[0] : '';


                    $officeTiming = $crawler->filter('.open_timings_con')->each(function ($innerNode) {
                        return $innerNode->text();
                    });

                    $address = ($crawler->filter('.muchroom_mail')->text());
                    $description = ($crawler->filter('#p_description')->text());
                    $bannerImages = $bannerImages[0][0];
                    try {
                        if (is_null($office)) {
                            \DB::beginTransaction();
                            $sharedoffice = SharedOffice::create([
                                'office_name' => $name[0],
                                'location' => $address,
                                'description' => $description,
                                'office_manager' => '',
                                'qr' => 'dummy',
                                'image' => $bannerImages[0],
                                'pic_size' => 1,
                                'staff_id' => 1,
                                'office_size_x' => 0,
                                'office_size_y' => 0,
                                'number' => 0,
                                'city_id' => isset($cityId) ? $cityId : 0,
                                'state_id' => isset($countryid) ? $countryid : 0,
                                'continent_id' => 0,
                                'lat' => $latlng[0][0],
                                'lng' => $latlng[0][1],
                                'contact_name' => 0,
                                'contact_phone' => $contact,
                                'contact_email' => 0,
                                'contact_address' => 0,
                                'monday_opening_time' => self::pluckOfficeTiming($officeTiming, 0)[0],
                                'monday_closing_time' => self::pluckOfficeTiming($officeTiming, 0)[1],
                                'saturday_opening_time' => self::pluckOfficeTiming($officeTiming, 1),
                                'saturday_closing_time' => self::pluckOfficeTiming($officeTiming, 1),
                                'sunday_opening_time' => self::pluckOfficeTiming($officeTiming, 2),
                                'sunday_closing_time' => self::pluckOfficeTiming($officeTiming, 2),
                                'verify_info_to_scan' => 0,
                                'require_deposit' => 1,
                                'office_space' => 0,
                                'version_english' => 1,
                                'version_chinese' => 0,
                                'active' => 1,
                                'likes' => 0,
                            ]);
                            $sharedoffice = SharedOffice::find($sharedoffice->id);
                            $sharedoffice->qr = "https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=office:" . $sharedoffice->id;
                            $sharedoffice->save();

                            $index = 0;
                            foreach ($bannerImages as $k) {
                                if ($index > 0) {
                                    SharedOfficeImages::create([
                                        'office_id' => $sharedoffice->id,
                                        'image' => $k,
                                        'pic_size' => 1
                                    ]);
                                }
                                $index++;
                            }
                            \DB::commit();
                            $bar->advance();
                        } else {
                            \DB::beginTransaction();
                            $sharedoffice = SharedOffice::where('id', '=', $office->id)->update([
                                'office_name' => $name[0],
                                'location' => $address,
                                'description' => $description,
                                'office_manager' => '',
                                'qr' => 'dummy',
                                'image' => $bannerImages[0],
                                'pic_size' => 1,
                                'staff_id' => 1,
                                'office_size_x' => 0,
                                'office_size_y' => 0,
                                'number' => 0,
                                'city_id' => isset($cityId) ? $cityId : 0,
                                'state_id' => isset($countryid) ? $countryid : 0,
                                'continent_id' => 0,
                                'lat' => $latlng[0][0],
                                'lng' => $latlng[0][1],
                                'contact_name' => 0,
                                'contact_phone' => $contact,
                                'contact_email' => 0,
                                'contact_address' => 0,
                                'monday_opening_time' => self::pluckOfficeTiming($officeTiming, 0)[0],
                                'monday_closing_time' => self::pluckOfficeTiming($officeTiming, 0)[1],
                                'saturday_opening_time' => self::pluckOfficeTiming($officeTiming, 1),
                                'saturday_closing_time' => self::pluckOfficeTiming($officeTiming, 1),
                                'sunday_opening_time' => self::pluckOfficeTiming($officeTiming, 2),
                                'sunday_closing_time' => self::pluckOfficeTiming($officeTiming, 2),
                                'verify_info_to_scan' => 0,
                                'require_deposit' => 1,
                                'office_space' => 0,
                                'version_english' => 1,
                                'version_chinese' => 0,
                                'active' => 1,
                                'likes' => 0,
                            ]);
                            $sharedoffice = SharedOffice::find($office->id);
                            $sharedoffice->qr = "https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=office:" . $sharedoffice->id;
                            $sharedoffice->save();

                            SharedOfficeImages::where('office_id', '=', $sharedoffice->id)->delete();
                            $index = 0;
                            foreach ($bannerImages as $k) {
                                if ($index > 0) {
                                    SharedOfficeImages::create([
                                        'office_id' => $office->id,
                                        'image' => $k,
                                        'pic_size' => 1
                                    ]);
                                }
                                $index++;
                            }

                            \DB::commit();
                            $bar->advance();
                        }
                    } catch (\Exception $e) {
                        \DB::rollback();
                        dump($e->getMessage());
                        dump($e->getLine());
                        dd($e->getFile());
                    }
//                    }
                } catch (\Exception $e) {
                    $this->info($e->getMessage());
                }
            }
            $bar->finish();
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    public function scrapFromLink()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.coworker.com/cloudfront/app/search/results.json",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Postman-Token: bce9ad66-9346-4f14-af59-82da71bd1158",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, true);
        }
    }

    public function pluckOfficeTiming($obj, $index) //0 = normal days, 1 = sat, 2 = sunday
    {
        try {
            if ($index == 0) {
                $timing = $obj[0];
                $timing = explode('-', $timing);
                return collect([
                    Carbon::parse(trim($timing[0])),
                    Carbon::parse(trim($timing[1])),
                ]);
            }
            return trim($obj[$index]) == 'Closed' ? 'OFF DAY' : $obj[$index];
        } catch (\Exception $e) {
            dump($obj);
            if ($index == 0) {
                return collect([
                    '24 Hours',
                    '24 Hours',
                ]);
            }
            return '24 Hours';
        }
    }

    public function getQueryParameters($url)
    {
        $query_str = parse_url($url, PHP_URL_QUERY);
        parse_str($query_str, $query_params);
        return ($query_params);
    }

    public function saveImage($url)
    {
        $this->info('Saving: ' . $url);
        $extension = 'png';

        if (strpos($url, 'jpg') !== false) {
            $extension = '.jpg';
        }
        $ts = self::quickRandom();

        $ch = curl_init($url);
        $fp = fopen(public_path('uploads/sharedoffice/' . $ts . $extension), 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        return 'uploads/sharedoffice/' . $ts . $extension;
    }

    public static function quickRandom($length = 5)
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $ts = Carbon::now()->timestamp;
        $string = substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
        return $string . '_' . $ts;
    }

    public function filterLink($href)
    {
        $link = $href[0];
        $link = explode('/', $link);
        $link = 'https://www.coworker.com/' . (str_replace('_', '/', $link[count($link) - 1], $link[count($link) - 1]));
        $this->info('going in ' . $link);
        return $link;
    }
}
