<?php

namespace App\Console\Commands;

use App\Models\ProjectSkill;
use App\Models\Skill;
use Illuminate\Console\Command;

class ProjectScript extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:removeDuplicateSkills';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $skills = Skill::get();
        try {
            foreach ($skills as $key => $val) {
                $count = 0;
                $duplicateSkills = Skill::where('name_en', '=', $val->name_en)->get();
                foreach ($duplicateSkills as $skillsKey => $skillsVal) {
                    if ($count == 0) {
                        // At first attempt we do nothing bcz first skill is always original
                        $count = $count + 1;
                    } else {
                        $project = ProjectSkill::where('skill_id', '=', $skillsVal->id)->get();
                        \DB::beginTransaction();

                        // change project duplicate id with original skill
                        if (count($project) > 0) {
                            foreach ($project as $projectKey => $projectVal) {
                                $projectVal->skill_id = $val->id;
                                $projectVal->save();
                            }
                        }

                        // Also maintain Skill record to delete duplicate skills
                        $skill = Skill::where('id', '=', $skillsVal->id)->first();
                        if (!is_null($skill)) {
                            $skill->delete();
                        }

                        \DB::commit();
                    }
                }
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            dd($e->getMessage());
        }
    }
}
