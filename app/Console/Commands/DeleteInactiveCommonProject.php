<?php

namespace App\Console\Commands;

use App\Http\Controllers\CronJobController;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class DeleteInactiveCommonProject extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deleteinactivecommonproject:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Common project if more than 30 days and didn\'t hired anyone will be auto delete.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        CronJobController::deleteInactiveCommonProject();
    }
}
