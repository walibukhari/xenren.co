<?php

namespace App\Console\Commands;

use App\Jobs\SharedOfficeSearchJson;
use App\Models\SharedOffice;
use Illuminate\Console\Command;

class GenerateSharedOfficeSearch extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'generate:sharedofficesearch';
	
	/**
	 * The console command description.
	 *
     *
	 * @var string
	 */
	protected $description = 'Command description';
	
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$file_path = self::getFileName('en');
		$exist = self::unlinkIfExist($file_path);

		$file_path = self::getFileName('cn');
		$exist = self::unlinkIfExist($file_path);


		$data = SharedOffice::where('active', '=', 1)->get();
        $loader = $this->output->createProgressBar(count($data));
		foreach ($data as $k => $v) {
			dispatch(new SharedOfficeSearchJson($v));
			dispatch(new SharedOfficeSearchJson($v, 'cn'));
			$loader->advance();
		}
        $loader->finish();
	}
	
	public function unlinkIfExist($file_path)
	{
        if(file_exists($file_path)) {
            try {
                unlink($file_path);
            } catch (\Exception $e) {
                dd($e);
            }
        }
	}

    public function getFileName($lang)
    {
        return public_path() . "/SharedOfficesJson/sharedoffices_".$lang.".json";
	}
}
