<?php

namespace App\Console\Commands;

use App\Models\Portfolio;
use App\Models\ProjectApplicant;
use App\Models\User;
use App\Models\UserIdentity;
use App\Models\UserSkill;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class UsersInitialDisplay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'usersintialdisplay:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rearrange the users display priority in find expert page';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        Log::useDailyFiles(storage_path().'/logs/UsersInitialDisplay.log');
        Log::info('=== Start Users Initial Display Log===');

        $users = User::all();
        foreach( $users as $user )
        {
            $user->initial_display = getDisplayPriorityPoint($user);
            $user->save();
        }

        Log::info('=== End Users Initial Display Log===');
    }
}
