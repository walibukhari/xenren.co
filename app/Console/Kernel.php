<?php

namespace App\Console;

use App\Console\Commands\CreateHomePageJson;
use App\Console\Commands\SharedOfficeChangeQrCode;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\TimeZone::class,
        \App\Console\Commands\AdminVerifySharedOfficeRequests::class,
        CreateHomePageJson::class,
        SharedOfficeChangeQrCode::class,
    ];

    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->command('verify_shared_office_requests:run')
            ->weekly()
        ;

        $schedule->command('deleteinactivecommonproject:run')
            ->daily()
        ;

        $schedule->command('deleteobsoleteviewcontactlogs:run')
            ->daily()
        ;

        $schedule->command('mysql:backup')->dailyAt('8:00');
        $schedule->command('mysql:backup')->dailyAt('20:00');
//        $schedule->command('mysql:backup')->everyMinute();

        $schedule->command('deletemysqlbackup:run')->daily();
//        $schedule->command('deletemysqlbackup:run')->everyMinute();

        $schedule->command('usersintialdisplay:run')->hourly();
        $schedule->command('Command:ReleaseFundsAfterSpecificTime')->hourly();
//        $schedule->command('usersintialdisplay:run')->everyMinute();

        $schedule->command('SharedOffice:freeSeats')->hourly();
        $schedule->command('release-milestone:today')->daily();

        $schedule->command('kill:inactive-sessions')->everyMinute();
        $schedule->command('User:UserLinks')->everyMinute();
        $schedule->command('remove:guest-register')->everyFifteenMinutes();

        $schedule->command('cowork:scrapper')->everyFifteenMinutes()->withoutOverlapping();

        $schedule->command('generate:sharedofficesearch')->dailyAt('12:00')->withoutOverlapping();
        $schedule->command('CreateHomePageJson')->dailyAt('02:00')->withoutOverlapping();

        $schedule->command('Create:SharedOfficeMapData')->twiceDaily();
        $schedule->command('run:shared_office_change_qr_code')->twiceDaily();

        //	    $schedule->command('scrap:coworker-json')->everyFiveMinutes()->withoutOverlapping();

        $schedule->command('deleteoldsharedofficerequest:run')
            ->daily()
        ;
    }

    /**
     * Register the commands for the application.
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
