<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFeedback extends Model
{
    protected  $table='user_feedback';
    protected  $fillable=['username','email','feedback_text'];
}
