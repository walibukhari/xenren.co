<?php

namespace App\Libraries\PayPal_PHP_SDK;

use Illuminate\Support\Facades\Auth;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
define('PP_CONFIG_PATH', base_path('config'));

trait PayPalTrait {
	
	protected $apiContext;
	protected $payment;
	
	public function __construct()
	{
		$this->apiContext = new ApiContext(
			new OAuthTokenCredential(
//				'AR1588l0QCdM1SZjHi_fwhg8Gz9kPJoTR97E56Yt2SIr1hzB_w7corhc3ZGRgjWPQSbWcYzeANBiBUnl',     // ClientID
//				'EBcoKvFYaEGGI3oHVCL7JcYI0H0MbN_Fr9FZiUgVjZjEPY3WuwsiHiT6TCYbBWPWOEAyH1dUmdA8d2iU'
				'AZ1DmwT_UQomFoN0rS1bQuzi_nt-DCk2XaLvdzba3Q-ZtBeWy-OtGOPw7SaiaSvnCbs2sfwZqFlB92JY',     // ClientID
				'EIFDQqI5gCkF3zSU0_qw52nZvcicKPJ7jsaura2lwggT6nzqyF-MHWwgllzCcJkPg1CneXKkdzCRGL-v'      // ClientSecret
			)
		);
	}

    /**
     * Create Payment Functions
     * @param $request
     * @return null|string
     */
	
	public function createPayment($request)
	{
		$payment = $this->addItems($request);
		try {
			$payment->create($this->apiContext);
			return $payment->getApprovalLink();
		}
		catch (\PayPal\Exception\PayPalConnectionException $ex) {
			// This will print the detailed information on the exception.
			//REALLY HELPFUL FOR DEBUGGING
			echo $ex->getData();
		}
	}
	
	public function executePayment($request)
	{
		/** Get the payment ID before session clear **/
		$payment_id = $request->paymentId;
		$token = $request->token;
		if (empty($payment_id) || empty($token)) {
			dd('no data');
		}
		$payment = self::getPaymentDetail($payment_id);

		$execution = new PaymentExecution();
		$execution->setPayerId($request->PayerID);
		/**Execute the payment **/
		$result = $payment->execute($execution, $this->apiContext);
		
		return collect([
			'status' => $result->getState(),
			'result' => collect([
				'transactions' => collect([
					'amount' => $result->getTransactions()[0]->amount->getDetails()->subtotal,
					'tax' =>$result->getTransactions()[0]->amount->getDetails()->tax
				]),
				'id' => $result->getId(),
				'intent' => $result->getIntent(),
				'state' => $result->getState(),
				'cart' => $result->getCart(),
				'payer' => $result->getPayer()->getPayerInfo(),
				'payee' => $result->getPayee(),
				'result' => $result
			]),
		]);
	}
	
	public function getPaymentDetail($payment_id)
	{
		return Payment::get($payment_id, $this->apiContext);
	}
	
	public function calculateTax($amount)
	{
		return ($amount * 2.75) / 100;
	}
	
	public function addItems($request) {
		$payer = new Payer();
		$payer->setPaymentMethod('paypal');
		
		$amount = $request->amount;
		
		$item1 = new Item();
		$item1->setName('Topup For employer')
			->setCurrency('USD')
			->setQuantity(1)
//			->setSku("123123") // Similar to `item_number` in Classic API
			->setPrice($amount);
		
		$tax = self::calculateTax($amount);
		$itemList = new ItemList();
		$itemList->setItems(array($item1));
		
		$details = new Details();
		$details
			->setTax($tax)
			->setSubtotal($amount);
		
		$total = $request->amount + $tax;
		
		$amount = new Amount();
		$amount->setCurrency("USD")
			->setTotal($total)
			->setDetails($details);
		
		$transaction = new Transaction();
		$transaction->setAmount($amount)
			->setItemList($itemList)
			->setDescription("Payment description")
			->setInvoiceNumber(uniqid());
		
		$redirectUrls = new RedirectUrls();
		$redirectUrls->setReturnUrl("https://www.xenren.co/approve-paypal-payment")
			->setCancelUrl("https://www.xenren.co/cancel-paypal-payment");
		
		$payment = new Payment();
		$payment->setIntent('sale')
			->setPayer($payer)
			->setTransactions(array($transaction))
			->setRedirectUrls($redirectUrls);
		return $payment;
		
	}

    /**
     * Send Payment Functions
     * @param $request
     * @return \Illuminate\Support\Collection
     */
	
	public function withdraw($request)
	{
		$payouts = new \PayPal\Api\Payout();
		
		/*{
		"sender_batch_header":{
		"sender_batch_id":"2014021801",
		"email_subject":"You have a Payout!"
		},
		"items":[
		{
		"recipient_type":"EMAIL",
		"amount":{
					"value":"1.0",
          "currency":"USD"
    },
    "note":"Thanks for your patronage!",
    "sender_item_id":"2014031400023",
    "receiver":"shirt-supplier-one@mail.com"
    }
		  ]
		}*/
		
		$senderBatchHeader = new \PayPal\Api\PayoutSenderBatchHeader();
		$senderBatchHeader->setSenderBatchId(uniqid())
			->setEmailSubject("You have a Payout!");
		$senderItem = new \PayPal\Api\PayoutItem();
		$senderItem->setRecipientType('Email')
			->setNote('Thanks for your patronage!')
			->setReceiver($request->email)
//			->setSenderItemId("2014031400023")
			->setAmount(new \PayPal\Api\Currency('{
                        "value":'.$request->amount.',
                        "currency":"USD"
                    }'));
		
		$payouts->setSenderBatchHeader($senderBatchHeader)
			->addItem($senderItem);
		
		
		try {
			$output = $payouts->createSynchronous($this->apiContext);
		} catch (\Exception $ex) {
			return collect([
			    'status' => 'error',
                'message' => $ex->getMessage()
            ]);
		}
		return collect([
			"status" => 'success',
			"payout_batch_id" => $output->getBatchHeader()->getPayoutBatchId(),
			"batch_status" => $output->getBatchHeader()->getBatchStatus(),
			"sender_batch_id" => $output->getBatchHeader()->getSenderBatchHeader()->getSenderBatchId(),
			"email_subject" => $output->getBatchHeader()->getSenderBatchHeader()->getEmailSubject(),
		]);
		
	}
	
	public function getWithdrawDetail($batchId)
	{
	    //TODO: change followings
		return \PayPal\Api\Payout::get($batchId, new ApiContext(
			new OAuthTokenCredential(
				'AR1588l0QCdM1SZjHi_fwhg8Gz9kPJoTR97E56Yt2SIr1hzB_w7corhc3ZGRgjWPQSbWcYzeANBiBUnl',     // ClientID
				'EBcoKvFYaEGGI3oHVCL7JcYI0H0MbN_Fr9FZiUgVjZjEPY3WuwsiHiT6TCYbBWPWOEAyH1dUmdA8d2iU'      // ClientSecret
//				'AXUJW--mrMa7Qr4rd9wWYxdZQgVAZ-_p3L3_qAPrUsKZtxVDJeWy-Kd8D5AqtWde6GurbuagLGu3hPvk',     // ClientID
//				'EOKSMSFiS67p8P-LXd5T9yInJ6zjNHWqRUex_ziUPyH7JrYfcMokkSdhdrT6XjovLc_jieThFx3kwp40'      // ClientSecret
			)
		));
	}
	
}