<?php

namespace App\Libraries\Pushy;

class PushyAPI {

	static public function sendPushNotification($data, $to, $options) {
		// Insert your Secret API Key here
		$apiKey = "787dbee1de02e6eb1861531c468ec3d2b6421ba69c48dacf1422c9b8b6b36211";

		// Default post data to provided options or empty array
		$post = $options ?: array();

		// Set notification payload and recipients
		$post['to'] = $to;
		$post['data'] = $data;

		// Set Content-Type header since we're sending JSON
		$headers = array(
			'Content-Type: application/json'
		);

		// Initialize curl handle
		$ch = curl_init();

		// Set URL to Pushy endpoint
		curl_setopt($ch, CURLOPT_URL, 'https://api.pushy.me/push?api_key=' . $apiKey);

		// Set request method to POST
		curl_setopt($ch, CURLOPT_POST, true);

		// Set our custom headers
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		// Get the response back as string instead of printing it
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		// Set post data as JSON
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post, JSON_UNESCAPED_UNICODE));

		// Actually send the push
		$result = curl_exec($ch);

		// Display errors
		if (curl_errno($ch)) {
			$error = curl_error($ch);
			return collect([
				'status' => 'error',
				'message' => $error
			]);
		}

		// Close curl handle
		curl_close($ch);
		return collect([
			'status' => 'success',
			'data' => $result
		]);
		// Debug API response
		//echo $result;
	}
    static public function sendBookingNotification($data, $to, $options) {
        // Insert your Secret API Key here
        $apiKey = "c539cd9b642a1ec0c321d5634f27e0b01109be1b6bb9ef78460f30f091666b8c";

        // Default post data to provided options or empty array
        $post = $options ?: array();

        // Set notification payload and recipients
        $post['to'] = $to;
        $post['data'] = $data;

        // Set Content-Type header since we're sending JSON
        $headers = array(
            'Content-Type: application/json'
        );

        // Initialize curl handle
        $ch = curl_init();

        // Set URL to Pushy endpoint
        curl_setopt($ch, CURLOPT_URL, 'https://api.pushy.me/push?api_key=' . $apiKey);

        // Set request method to POST
        curl_setopt($ch, CURLOPT_POST, true);

        // Set our custom headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // Get the response back as string instead of printing it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Set post data as JSON
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post, JSON_UNESCAPED_UNICODE));

        // Actually send the push
        $result = curl_exec($ch);

        // Display errors
        if (curl_errno($ch)) {
            $error = curl_error($ch);
            return collect([
                'status' => 'error',
                'message' => $error
            ]);
        }

        // Close curl handle
        curl_close($ch);
        return collect([
            'status' => 'success',
            'data' => $result
        ]);
        // Debug API response
        //echo $result;
    }

    static public function sendPushMobileNotification($data, $to, $options) {
        // Insert your Secret API Key here
        $apiKey = "5c6d098c25afbbd89006a0f1f2cdc579299b2e87e2975d883e9fbcb9df9afbfd";

        // Default post data to provided options or empty array
        $post = $options ?: array();

        // Set notification payload and recipients
        $post['to'] = $to;
        $post['data'] = $data;

        // Set Content-Type header since we're sending JSON
        $headers = array(
            'Content-Type: application/json'
        );

        // Initialize curl handle
        $ch = curl_init();

        // Set URL to Pushy endpoint
        curl_setopt($ch, CURLOPT_URL, 'https://api.pushy.me/push?api_key=' . $apiKey);

        // Set request method to POST
        curl_setopt($ch, CURLOPT_POST, true);

        // Set our custom headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // Get the response back as string instead of printing it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Set post data as JSON
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post, JSON_UNESCAPED_UNICODE));

        // Actually send the push
        $result = curl_exec($ch);

        // Display errors
        if (curl_errno($ch)) {
            $error = curl_error($ch);
            return collect([
                'status' => 'error',
                'message' => $error
            ]);
        }

        // Close curl handle
        curl_close($ch);
        return collect([
            'status' => 'success',
            'data' => $result
        ]);
        // Debug API response
        //echo $result;
    }

}
