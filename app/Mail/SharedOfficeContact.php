<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SharedOfficeContact extends Mailable
{
	use Queueable, SerializesModels;
	public $requestObj;
	
	/**
	 * Create a new message instance.
	 *
	 * @param $request
	 */
	public function __construct($request)
	{
		$this->requestObj = $request;
	}
	
	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this->from('support@xenren.co', 'Xenren')
			->view('mails.sharedOfficeContactBlade');
	}
}
