<?php

namespace App\Http\ViewComposers;

use App\Models\Skill;
use App;
use Illuminate\Contracts\View\View;

class GlobalComposer {

    protected $_G;

    public function __construct()
    {

        if(!App::runningInConsole()){
            try {
                $actions = \Route::current()->action;
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                app('sentry')->captureException($e);
            }
        }

        try {
            $this->_G = array();

            if (isset($actions) && isset($actions['xenren_path']) && $actions['xenren_path']) {
                $this->_G['xenren_path'] = 'xenren-assets/';
            }

            if (isset($actions) && isset($actions['disable_sidemenu'])) {
                $this->_G['disable_sidemenu'] = $actions['disable_sidemenu'];
            }

            if ((\Route::current()) !== null) {
                $this->_G['route_name'] = \Route::current()->getName();
            }

            if (\Auth::guard('users')->check()) {
                $this->_G['user'] = \Auth::guard('users')->user();
            } else {
                $this->_G['user'] = null;
            }

            if (preg_match('/micromessenger/i', strtolower(request()->header('User-Agent')))) {
                $this->_G['is_weixin_browser'] = true;
            }

            $headerSearchSkillList = Skill::getSearchSkillData(Skill::SEARCH_TYPE_WHITE);
            $this->_G['headerSearchSkillList'] = $headerSearchSkillList;
        } catch (\Exception $e){
            \Log::error($e->getMessage());
            app('sentry')->captureException($e);
        }
    }

    public function compose(View $view)
    {
        $view->with('_G', $this->_G);
    }

}