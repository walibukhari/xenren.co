<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;
use Response;
use Auth;

class ProjectCreateFormRequest extends FormRequest
{
    protected $rules = [
        'name' => 'required|min:1|max:500',
        'category_id' => 'required|exists:category,id',
        'description' => 'required|min:1|max:5000',
        'pay_type' => 'required|min:1|max:3|isNumber',
        'time_commitment' => 'required|min:1|max:3|isNumber',
        'freelance_type' => 'required|min:1|max:3|isNumber',
        'question' => 'min:1|max:500',
    ];

    public function rules()
    {
        $rules = $this->rules;

        return $rules;
    }

    public function authorize()
    {
        return Auth::guard('users')->check();
    }

    // OPTIONAL OVERRIDE
    public function forbiddenResponse()
    {
        return Response::make('Permission denied!', 403);
    }

}