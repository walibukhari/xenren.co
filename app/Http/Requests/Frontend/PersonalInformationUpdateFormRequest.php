<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;
use Response;
use Auth;

class PersonalInformationUpdateFormRequest extends FormRequest
{
    protected $rules = [
        'name' => 'required|alpha_space',
        'avatar' => '',
        'about_me' => 'required',
        'experience' => 'numeric',
        'position' => '',
        'sub_position' => '',
        'address' => '',
        'hourly_pay' => 'numeric',
        'email' => 'required',
        'qq_id' => '',
        'wechat_id' => '',
        'skype_id' => '',
        'handphone_no' => 'numeric',
        'country_id' => '',
        'city_id' => '',
        'currency' => ''
    ];

    public function rules()
    {
        $rules = $this->rules;

        return $rules;
    }

    public function authorize()
    {
        return Auth::guard('users')->check();
    }

    // OPTIONAL OVERRIDE
    public function forbiddenResponse()
    {
        return Response::make('Permission denied!', 403);
    }

    // OPTIONAL OVERRIDE
//    public function response()
//    {
//        // If you want to customize what happens on a failed validation,
//        // override this method.
//        // See what it does natively here:
//        // https://github.com/laravel/framework/blob/master/src/Illuminate/Foundation/Http/FormRequest.php
//    }
}