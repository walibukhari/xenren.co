<?php

namespace App\Http\Requests\Frontend;

use App\Http\Requests\Request;

class CommentCreatePost extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'video' => 'nullable|mimetypes:video/avi,video/mpeg,video/webm,video/quicktime|max:30240'
        ];
    }
}
