<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;
use Response;
use Auth;

class FeedbackCreateFormRequest extends FormRequest
{
    protected $rules = [
        'priority' => 'required|min:1|max:3',
        'title' => 'required|min:1|max:500',
        'description' => 'required|min:1|max:5000',
        'fileUploadImage' => '',
    ];

    public function rules()
    {
        $rules = $this->rules;

        return $rules;
    }

    public function authorize()
    {
        return Auth::guard('users')->check();
    }

    // OPTIONAL OVERRIDE
    public function forbiddenResponse()
    {
        return Response::make('Permission denied!', 403);
    }

}