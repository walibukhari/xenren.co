<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;
use Response;
use Auth;

class PortfolioCreateFormRequest extends FormRequest
{
    protected $rules = [
        'job_position_id' => 'required|exists:job_position,id',
        'title' => 'required|min:1|max:500',
        'url' => 'required',
//        'hidUploadedFile' => 'required'
    ];

    public function rules()
    {
        $rules = $this->rules;

        return $rules;
    }

    public function authorize()
    {
        return Auth::guard('users')->check();
    }

    // OPTIONAL OVERRIDE
    public function forbiddenResponse()
    {
        return Response::make('Permission denied!', 403);
    }

}