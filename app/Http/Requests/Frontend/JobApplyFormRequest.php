<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;
use Response;
use Auth;

class JobApplyFormRequest extends FormRequest
{
    protected $rules = [
        'project_id' => 'required|min:1|max:500',
        'price' => 'required|min:1|numeric',
//        'about_me' => 'required|min:1|max:500', (cant use because english is calculate by word while chinese is calculate by character)
        'pay_type' => 'required'
//        'questionId' => 'required|exists:project_question,id'
    ];

    public function rules()
    {
        $rules = $this->rules;

        return $rules;
    }

    public function authorize()
    {
        return Auth::guard('users')->check();
    }

    // OPTIONAL OVERRIDE
    public function forbiddenResponse()
    {
        return Response::make('Permission denied!', 403);
    }

}