<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Response;
use Auth;

class RegisterUserFormRequest extends FormRequest
{
    protected $rules = [
        'name' => 'required',
        'last_name' => 'required',
        'phone_or_email' => 'required|email',
        'password' => 'required|min:8|confirmed',
        'password_confirmation' => 'required',
        'remember' => 'required',
//        'CaptchaCode' => 'required|valid_captcha',
    ];

    public function rules()
    {
        $rules = $this->rules;

        return $rules;
    }
    
    public function messages()
    {
	    return [
                'name.required' => trans('register.name_required'),
                'last_name.required' => trans('register.lastname_required'),
	    	'phone_or_email.required' => trans('register.email_required'),
	    	'phone_or_email.email' => trans('register.validate_email'),
                'password.required' => trans('register.password_required'),
                'password_confirmation.required' => trans('register.password_confirmation_required'),
                'remember.required' => trans('register.remember_required'),
	    ];
    }
	
	public function authorize()
    {
        return Auth::guest();
    }

    // OPTIONAL OVERRIDE
    public function forbiddenResponse()
    {
        return Response::make('Permission denied!', 403);
    }

    // OPTIONAL OVERRIDE
//    public function response()
//    {
//        // If you want to customize what happens on a failed validation,
//        // override this method.
//        // See what it does natively here:
//        // https://github.com/laravel/framework/blob/master/src/Illuminate/Foundation/Http/FormRequest.php
//    }
}