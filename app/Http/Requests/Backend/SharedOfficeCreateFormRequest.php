<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;
use Response;
use Auth;

class SharedOfficeCreateFormRequest extends FormRequest
{
    protected $rules = [];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guard('staff')->check() && Auth::guard('staff')->user()->hasPermission('manage_sharedoffice');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = $this->rules;

        return [
            'rules' => $rules,

            'office_name' => 'required_without:office_name_cn',
            'office_name_cn' => 'required_without:office_name',
            'description' => 'required_without:description_cn',
            'description_cn' => 'required_without:description',
            'office_space' => 'required_without:office_space_cn',
            'office_space_cn' => 'required_without:office_space',
            'contact_name' => 'required_without:contact_name_cn',
            'contact_name_cn' => 'required_without:contact_name',
            'contact_phone' => 'required_without:contact_phone_cn',
            'contact_phone_cn' => 'required_without:contact_phone',
            'contact_email' => 'required_without:contact_email_cn',
            'contact_email_cn' => 'required_without:contact_email',
            'contact_address' => 'required_without:contact_address_cn',
            'contact_address_cn' => 'required_without:contact_address',

            'office_manager' => 'required',
            'image' => 'required',
            'pic_size' => 'required',
            'bulletins' => 'required',
            'shfacilities' => 'required',
            'location' => 'required',
            'office_size_x' => 'required',
            'office_size_y' => 'required',
            'current_location' => 'required',
            'monday_opening_time' => 'required',
            'monday_closing_time' => 'required',
//            'saturday_opening_time' => 'required',
//            'saturday_closing_time' => 'required',
//            'sunday_opening_time' => 'required',
//            'sunday_closing_time' => 'required',
        ];
    }

    // OPTIONAL OVERRIDE
    public function forbiddenResponse()
    {
        return Response::make('Permission denied!', 403);
    }

}
