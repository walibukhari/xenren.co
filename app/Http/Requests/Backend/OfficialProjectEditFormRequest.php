<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;
use Response;
use Auth;
use App\Models\OfficialProject;

class OfficialProjectEditFormRequest extends FormRequest
{
    public function rules()
    {
        return OfficialProject::$rules;
    }

    public function authorize()
    {
        return Auth::guard('staff')->check() && Auth::guard('staff')->user()->hasPermission('manage_official_project');
    }
}