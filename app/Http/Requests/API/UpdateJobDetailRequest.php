<?php

namespace App\Http\Requests\API;

use App\Http\Requests\Request;

class UpdateJobDetailRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'experience_year' => 'optional|integer',
            'job_position' => 'optional|integer',
            'job_title' => 'optional|string',
            'skills' => 'optional|string',
            'currency' => 'optional|integer',
        ];
    }
}
