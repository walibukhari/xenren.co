<?php

namespace App\Http\Controllers;

use App\Constants;
use App\Traits\sendPushNotification;
// use App\Models\Countries;
use App\Libraries\Pushy\PushyAPI;
use App\Models\CountryLanguage;
use App\Models\ExchangeRate;
use App\Models\JobPosition;
use App\Models\Language;
use App\Models\Project;
use App\Models\Skill;
use App\Models\User;
use App\Models\UserInbox;
use App\Models\UserInboxMessages;
use App\Models\WorldCountries;
use Auth;
use Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class FindExpertsController extends Controller
{
    use sendPushNotification;

    public function findExpertsNew(Request $request)
    {
        return view('frontend.findExperts.indexNew');
    }

    public function index(Request $request)
    {
        //for resume user page and redirect back
        $request->flash();
        Session::put('redirect_url', route('frontend.findexperts'));
        Session::put('redirect_url_name', trans('common.back_to_find_expert'));
        $skillList = Skill::getSearchSkillData(Skill::SEARCH_TYPE_WHITE);

        $jobPositions = JobPosition::GetJobPosition()->get();
        $statusList = User::getStatusLists();
        $hourlyRanges = $this::getHourlyRanges();
        $languages = json_encode(CountryLanguage::getCountryLanguages());
        $countries = WorldCountries::get()->sortBy('name');
        $countriesCode = WorldCountries::get();

        $filter = [
            'name' => $request->name,
            'skill_id_list' => $request->skill_id_list,
            'job_position_id' => $request->job_position_id,
            'job_position_name' => $request->job_position_name,
            'hourly_range_id' => $request->hourly_range_id,
            'hourly_range_name' => $request->hourly_range_name,
            'status_id' => $request->status_id,
            'review_type_id' => $request->review_type_id,
            'job_title' => $request->job_title,
            'last_activities_id' => $request->last_activities_id,
            'country_id' => $request->country_id,
            'country_name' => $request->country_name,
            'language_id' => $request->language_id,
        ];
        $users = User::select('name', 'id', 'job_position_id', 'checked_in_cowork_space', 'last_login_at', 'user_link', 'img_avatar', 'hourly_pay', 'currency', 'skype_id', 'wechat_id', 'line_id', 'handphone_no')->with('portfolios.files', 'job_position', 'skills.skill', 'comment', 'languages', 'projects', 'transactions');

        //admin impersonate to user (use in message inbox)
        //not need to show if user account is freeze

        // get user detail
        if (\Auth::guard('users')->check()) {
            $users = $users->where('id', '!=', \Auth::guard('users')->user()->id);
        } else {
            $users->where('id', '!=', 1);
        }

        // processing name
        if ($request->filled('name')) {
            $users = $users->where('name', 'LIKE', "%{$request->name}%");
            // ->orWhere('name', 'LIKE', "%$request->name%");
        }

        // processing job position
        if ($request->filled('job_position_id')) {
            $users = $users->where('job_position_id', $request->job_position_id);
            $jobPosition = JobPosition::where('id', $request->job_position_id)->first();
        }

        // processing hourly range
        if ($request->filled('hourly_range_id')) {
            switch ($request->hourly_range_id) {
                case '1':
                    $startValue = 1;
                    $endValue = 10;

                    break;
                case '2':
                    $startValue = 11;
                    $endValue = 20;

                    break;
                case '3':
                    $startValue = 21;
                    $endValue = 30;

                    break;
                case '4':
                    $startValue = 31;
                    $endValue = 40;

                    break;
                case '5':
                    $startValue = 41;
                    $endValue = 50;

                    break;
                case '6':
                    $startValue = 51;
                    $endValue = 20000;

                    break;
            }

            if ('cn' == app()->getLocale()) {
                $exchangeRate = ExchangeRate::getExchangeRate(Constants::CNY, Constants::USD);
                $startValue = $startValue * $exchangeRate;
                $endValue = $endValue * $exchangeRate;
            }

            $users = $users->whereBetween('hourly_pay', [$startValue, $endValue]);
            $hourlyRangeName = $this::translateHourlyRange($request->hourly_range_id);
        }

        // processing review type
        if ($request->has('review_type_id')) {
            $review_type_id = $request->review_type_id;

            if (Constants::REVIEW_TYPE_OFFICIAL == $review_type_id) {
                $users = $users->whereHas('comment', function ($query) {
                    $query->where('official_project_id', '>', 0);
                });
            }
        }

        // processing job title
        if ($request->filled('job_title')) {
            $users = $users->where('title', 'LIKE', '%'.$request->job_title.'%');
        }

        // processing last activities
        if ($request->filled('last_activities_id')) {
            $last_activities_id = $request->last_activities_id;

            if (Constants::LAST_ACTIVITY_WITHIN_ONE_WEEK == $last_activities_id) {
                $oneWeek = Carbon::now()->subWeek();
                $users = $users->where('last_login_at', '>=', $oneWeek);
            } elseif (Constants::LAST_ACTIVITY_WITHIN_ONE_MONTH == $last_activities_id) {
                $oneMonth = Carbon::now()->subMonth(1);
                $users = $users->where('last_login_at', '>=', $oneMonth);
            } elseif (Constants::LAST_ACTIVITY_WITHIN_THREE_MONTHS == $last_activities_id) {
                $threeMonths = Carbon::now()->subMonth(3);
                $users = $users->where('last_login_at', '>=', $threeMonths);
            }
        }

        // processing country
        if ($request->filled('country_id')) {
            $users = $users->where('country_id', $request->country_id);
            $country = WorldCountries::where('id', $request->country_id)->first();
        }

        // processing language
        if ($request->filled('language_id')) {
            $language_id = $request->language_id;
            $users = $users->whereHas('languages', function ($query) use ($language_id) {
                $query->where('language_id', $language_id);
            });
        }

        // processing expert status
        if ($request->filled('status_id')) {
            $status = $request->status_id;
            if (User::STATUS_LOOK_FOR_COFOUNDER == $status || User::STATUS_LOOK_FOR_FULL_TIME_JOB == $status) {
                $users = $users->where('status', $request->status_id);
            }
            //any status allow
        }

        // processing skill
        if ($request->filled('skill_id_list')) {
            $array = explode(',', $request->skill_id_list);
            $skills = Skill::whereIn('id', $array)->get();
            foreach ($skills as $skill) {
                $idArray[] = $skill->id;
            }

            DB::table('users')->update(['search_order' => 0]);
            //any of the skill option
            if (isset($idArray)) {
                $users = $users->whereHas('skills', function ($query) use ($idArray) {
                    $query->whereIn('skill_id', $idArray);
                });
            } else {
                $users = $users->whereHas('skills', function ($query) {
                    $query->whereIn('skill_id', []); // where idArray variable empty
                });
            }
            //calculate user skill
            //white skill - each skill give 1 mark
            //green skill - each skill give 10 mark
            //gold skill - each skill give 100 mark

            //update search_order
            foreach ($users->get() as $searchUser) {
                try {
                    $searchUser->search_order = getDisplayPriorityPoint($searchUser);
                    $searchUser->save();
                } catch (\Exception $e) {
                }
            }
        }

        //removing account which are freeze
        $users = $users->where('is_freeze', 0)->where('name', '!=', null);
        $userCount = $users->count();
        $paginateNumber = 20;

        if ($userCount / $paginateNumber > 100) {
            $paginateNumber = $userCount / 100;
        }

        // trigger order by initial (opening page) or order from search
        // and also order by user skills count
        if (
            null == $filter['name'] &&
            null == $filter['skill_id_list'] &&
            null == $filter['job_position_id'] &&
            null == $filter['job_position_name'] &&
            null == $filter['hourly_range_id'] &&
            null == $filter['hourly_range_name'] &&
            null == $filter['status_id'] &&
            null == $filter['review_type_id'] &&
            null == $filter['job_title'] &&
            null == $filter['last_activities_id'] &&
            null == $filter['country_id'] &&
            null == $filter['country_name'] &&
            null == $filter['language_id']
        ) {
            $users = $users->withCount('skills')->orderBy('skills_count', 'desc')
                ->orderBy('initial_display', 'desc')
                ->orderBy('last_login_at', 'desc')
                ->get()
            ;
        } else {
            $users = $users->orderBy('search_order', 'desc')
                ->withCount('skills')->orderBy('skills_count', 'desc')
                ->orderBy('last_login_at', 'desc')
                ->get()
            ;
        }

        if (Auth::guard('users')->check()) {
            $projects = Project::GetProject()
                ->where('user_id', Auth::guard('users')->user()->id)
                ->get()
            ;
        } else {
            $projects = null;
        }

        $invitationProject = null;
        if ($request->has('invitation_project_id')) {
            $invitationProject = Project::find($request->invitation_project_id);
        }

        // trigger advanced filter
        if (
            null != $filter['review_type_id'] ||
            null != $filter['job_title'] ||
            null != $filter['last_activities_id'] ||
            null != $filter['country_id'] ||
            null != $filter['language_id'] ||
            null != $filter['status_id']
        ) {
            $showAdvanceFilterSection = 'true';
        } else {
            $showAdvanceFilterSection = 'false';
        }

        // sort user skills
        foreach ($users as $user) {
            $user->skills = $user->skills
                ->sortByDesc('is_client_verified')
                ->sortByDesc('experience')
            ;
        }

        $users = self::moveOtherToEnd($users);
        $users = (self::paginate($users, 15, $request->page ?: 1));
        $aboutMe = [];

        foreach ($users as $user) {
            $aboutMe[$user->id] = '';
            $display = '';
            if (preg_match_all('/./u', $user->about_me, $matches) > 75) {
                for ($i = 0; $i < 75; ++$i) {
                    $display = $display.$matches[0][$i];
                }

                $aboutMe[$user->id] = $display;
            }
        }
        $isSuccessPost = $request->success_post;

        return view('frontend.findExperts.index', [
            'users' => $users,
            'filter' => $filter,
            'skillList' => $skillList,
            'jobPositions' => $jobPositions,
            'statusList' => $statusList,
            'projects' => $projects,
            'invitationProject' => $invitationProject,
            'count' => $userCount,
            'languages' => $languages,
            'countries' => $countries,
            'hourlyRanges' => $hourlyRanges,
            'showAdvanceFilterSection' => $showAdvanceFilterSection,
            'countriesCode' => $countriesCode,
            'isSuccessPost' => $isSuccessPost,
            'aboutMe' => $aboutMe,
        ]);
    }

    public function moveOtherToEnd($collection)
    {
        // move the item with the name == 'Other' to the end of the collection
        return $collection->reject(function ($value) {
            return $value['img_avatar'] == \URL::to('/').'/images/image.png';
        })->merge($collection->filter(function ($value) {
            return $value['img_avatar'] == \URL::to('/').'/images/image.png';
        }));
    }

    public function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);

        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage);
    }

    public function indexPost(Request $request)
    {
        $skills = Skill::GetSkill()->get();
        $jobPositions = JobPosition::GetJobPosition()->get();
        $statusList = Constants::getUserStatusLists();

        $filter = [];
        $filter['skills'] = null;
        $filter['job_position'] = null;
        $filter['status'] = null;
        $filter['name'] = null;

        $search_url = route('frontend.findexperts', ['lang' => \Session::get('lang')]).'?';

        if ($request->has('name') || '' != $request->name) {
            $filter['name'] = $request->name;
        }

        if (null != $filter['name']) {
            $search_url .= 'name='.$filter['name'].'&';
        }

        $users = User::GetUsers()->paginate(15);

        return view('frontend.findExperts.index', [
            'users' => $users,
            'skills' => $skills,
            'jobPositions' => $jobPositions,
            'statusList' => $statusList,
        ]);
    }

    public function sendRequestForContactInfo(Request $request)
    {
        $receiverId = $request->receiverId;

        $sender = \Auth::guard('users')->user();

        //check whether message already send before
        $count = UserInbox::where('from_user_id', $sender->id)
            ->where('to_user_id', $receiverId)
            ->where('type', UserInbox::TYPE_PROJECT_ASK_FOR_CONTACT)
            ->count()
        ;

        $users = User::where('id','=',$receiverId)->select([
            'id', 'web_token', 'post_project_notify','device_token'
        ])->where('post_project_notify', '=', 1)->get();
        $usersTokens = array();
        foreach ($users as $user) {
            array_push($usersTokens, $user->device_token);
        }
        $data = array(
            "title" => "Xenren", // Notification title
            'message' => 'Request Contact Info...!',
            'body' => 'some one send you request for your contact info',
            "image" => asset('images/Favicon.jpg')
        );
        $options = array(
            'notification' => array(
                'badge' => 0,
                'sound' => 'ping.aiff',
                'body' => 'some one send you request for your contact info'
            )
        );
        PushyAPI::sendPushNotification($data,$usersTokens,$options);
        if ($count > 0) {
            return response()->json([
                'status' => 'Notice',
                'message' => trans('common.request_for_contact_already_send'),
            ]);
        }

        //add user inbox notification
        $userInbox = new UserInbox();
        $userInbox->category = UserInbox::CATEGORY_NORMAL;
        $userInbox->project_id = null;
        $userInbox->from_user_id = $sender->id;
        $userInbox->to_user_id = $receiverId;
        $userInbox->type = UserInbox::TYPE_PROJECT_ASK_FOR_CONTACT;
        $userInbox->is_trash = 0;
        $userInbox->save();

        //add user inbox message
        $userInboxMessage = new UserInboxMessages();
        $userInboxMessage->inbox_id = $userInbox->id;
        $userInboxMessage->from_user_id = $sender->id;
        $userInboxMessage->to_user_id = $receiverId;
        $userInboxMessage->project_id = null;
        $userInboxMessage->is_read = 0;
        $userInboxMessage->type = UserInbox::TYPE_PROJECT_ASK_FOR_CONTACT;

        // $url = route('frontend.inbox.acceptrequestforcontactinfo');
        // $receiverId = $sender->id;
        $message = trans('common.i_want_to_know_your_contact');
        // $message = trans('common.request_contact_info_description', ['url' => $url, 'receiver-id' => $receiverId]);
        $userInboxMessage->custom_message = $message;
        $userInboxMessage->save();
        $this->sendEmail($receiverId,$sender);
        return response()->json([
            'status' => 'OK',
            'message' => trans('common.send_request_for_contact'),
        ]);
    }

    public function sendEmail($receiverId,$sender){
        $senderEmail = 'support@xenren.co';
        $senderName = $sender->name;
        $receiver = User::where('id','=',$receiverId)->first();

        Mail::send('mails.sendEmailForContactInfo', ['sender' =>$senderName ], function ($message) use ($senderEmail, $senderName, $receiver) {
            $message->from($senderEmail, $senderName);
            $message->to($receiver->email, $receiver->first_name.' '.$receiver->last_name)->subject('Request Contact Info !...');
        });
    }

    public function inviteToWork($userId, $invitationProjectId)
    {
        $user = User::find($userId);
        $projects = Project::where('user_id', \Auth::user()->id)->get();

        return view('frontend.includes.modalInvite', [
            'user' => $user,
            'projects' => $projects,
            'invitationProjectId' => $invitationProjectId,
        ]);
    }

    public function inviteToWorkPost(Request $request)
    {
        $projectId = $request->projectId;
        $receiverId = $request->receiverId;
        $senderMessage = $request->senderMessage;

        $sender = \Auth::guard('users')->user();
        $receiver = User::where('id', $receiverId)->first();
        $project = Project::where('id', $projectId)->first();

        $url = route('frontend.joindiscussion.getproject', [\Session::get('lang'), $project->name, $project->id]);

        $exist = UserInbox::where('category', '=', UserInbox::CATEGORY_NORMAL)
            ->where('project_id', '=', $project->id)
            ->where('from_user_id', '=', $sender->id)
            ->where('to_user_id', '=', $receiverId)->first();

        if (!is_null($exist)) {
            return response()->json([
                'status' => 'Error',
                'msg' => 'Invitation already sent...!',
            ]);
        }

        if (!(null == $receiver->email || '' == $receiver->email)) {
            $senderEmail = 'support@xenren.co';
            $senderName = $sender->getName();

            Mail::send('mails.invite', ['project' => $project, 'url' => $url, 'sender' => $sender, 'senderMessage' => $senderMessage], function ($message) use ($senderEmail, $senderName, $receiver) {
                $message->from($senderEmail, $senderName);
                $message->to($receiver->email, $receiver->first_name.' '.$receiver->last_name)->subject(trans('common.project_invite').'!');
            });
        }

        //add user inbox notification
        $userInbox = new UserInbox();
        $userInbox->category = UserInbox::CATEGORY_NORMAL;
        $userInbox->project_id = $project->id;
        $userInbox->from_user_id = $sender->id;
        $userInbox->to_user_id = $receiverId;
        $userInbox->from_staff_id = null;
        $userInbox->to_staff_id = null;
        $userInbox->type = UserInbox::TYPE_PROJECT_INVITE_YOU;
        $userInbox->is_trash = 0;
        $userInbox->save();

        //add user inbox message
        $userInboxMessage = new UserInboxMessages();
        $userInboxMessage->inbox_id = $userInbox->id;
        $userInboxMessage->from_user_id = $sender->id;
        $userInboxMessage->to_user_id = $receiverId;
        $userInboxMessage->from_staff_id = null;
        $userInboxMessage->to_staff_id = null;
        $userInboxMessage->project_id = $project->id;
        $userInboxMessage->is_read = 0;
        $userInboxMessage->type = UserInbox::TYPE_PROJECT_INVITE_YOU;

        $userInboxMessage->custom_message = $senderMessage;
        $userInboxMessage->save();

        return response()->json([
            'status' => 'OK',
            'receiverId' => $receiverId,
        ]);
    }

    public function sendOffer($userId)
    {
        $user = User::find($userId);
        $projects = Project::where('user_id', \Auth::user()->id)->get();

        return view('frontend.includes.modalSendOffer', [
            'user' => $user,
            'projects' => $projects,
        ]);
    }

    public function sendOfferPost(Request $request)
    {
        $projectId = $request->projectId;
        $receiver_Id = $request->receiverId;
        $payTypeId = $request->pay_type;
        $amount = $request->amount;
        $senderMessage = $request->senderMessage;

        $sender = \Auth::guard('users')->user();
        $receiver = User::where('id', $receiver_Id)->first();
        $project = Project::where('id', $projectId)->first();

        if (!$project) {
            return response()->json([
                'status' => 'Error',
                'message' => trans('common.project_no_exist'),
            ]);
        }

        if (Constants::PAY_TYPE_FIXED_PRICE == $payTypeId) {
            $pay_type = trans('common.fixed_price');
        } elseif (Constants::PAY_TYPE_HOURLY_PAY == $payTypeId) {
            $pay_type = trans('common.pay_hourly');
        }

        $url = route('frontend.joindiscussion.getproject', ['lang' => \Session::get('lang'), $project->name, $project->id]);

        $senderEmail = 'support@xenren.co';
        $senderName = $sender->getName();

        $previousOffer = UserInbox::where('project_id', '=', $project->id)
            ->where('category', '=', UserInbox::CATEGORY_NORMAL)
            ->where('from_user_id', '=', $sender->id)
            ->where('to_user_id', '=', $receiver_Id)->first();

        if (!is_null($previousOffer)) {
            return response()->json([
                'status' => 'Error',
                'message' => 'Offer already sent...!',
            ]);
        }

        Mail::send('mails.sendOffer', [
            'project' => $project,
            'url' => $url,
            'sender' => $sender,
            'senderMessage' => $senderMessage,
            'pay_type' => $pay_type,
            'amount' => $amount,
        ], function ($message) use ($senderEmail, $senderName, $receiver) {
            $message->from($senderEmail, $senderName);
            $message->to($receiver->email, $receiver->first_name.' '.$receiver->last_name)->subject(trans('common.send_offer').'!');
        });

        //add user inbox notification
        $userInbox = new UserInbox();
        $userInbox->category = UserInbox::CATEGORY_NORMAL;
        $userInbox->project_id = $project->id;
        $userInbox->from_user_id = $sender->id;
        $userInbox->to_user_id = $receiver_Id;
        $userInbox->from_staff_id = null;
        $userInbox->to_staff_id = null;
        $userInbox->daily_update = $request->daily_update;
        $userInbox->type = UserInbox::TYPE_PROJECT_SEND_OFFER;
        $userInbox->is_trash = 0;
        $userInbox->save();

        //add user inbox message
        $userInboxMessage = new UserInboxMessages();
        $userInboxMessage->inbox_id = $userInbox->id;
        $userInboxMessage->from_user_id = $sender->id;
        $userInboxMessage->to_user_id = $receiver_Id;
        $userInboxMessage->from_staff_id = null;
        $userInboxMessage->to_staff_id = null;
        $userInboxMessage->project_id = $project->id;
        $userInboxMessage->is_read = 0;
        $userInboxMessage->type = UserInbox::TYPE_PROJECT_SEND_OFFER;
        $userInboxMessage->quote_price = $amount;
        $userInboxMessage->pay_type = $payTypeId;

        // $message = trans('common.you_have_been_offer') . "<br/><br/>".
        // trans('common.project_name') . " : " . $project->name . '<br/>' .
        // trans('common.sender') . " : " . $sender->getName() . ' <br/>' .
        // trans('common.pay_type') . " : " . $pay_type . '<br/>' .
        // trans('common.amount') . " : " . $amount . '<br/><br/>' .
        // trans('common.message') . " : <br/>" .
        // $senderMessage . '<br/><br/>' .
        // "<a href='" . $url . "'>[ " . trans('common.check') . " ]</a>";// .
        // "<a id='link-ignore-" . $project->id . "'>[ " . trans('common.ignore') . " ]</a>";
        $userInboxMessage->custom_message = $senderMessage;
        $userInboxMessage->save();

        return response()->json([
            'status' => 'OK',
        ]);
    }

    public static function getHourlyRanges()
    {
        $arr = [];
        if ('en' == app()->getLocale()) {
            $currency = trans('common.usd');
        } else {
            $currency = trans('common.rmb');
        }

        $arr[1] = '1 - 10 '.$currency;
        $arr[2] = '11 - 20 '.$currency;
        $arr[3] = '21 - 30 '.$currency;
        $arr[4] = '31 - 40 '.$currency;
        $arr[5] = '41 - 50 '.$currency;
        $arr[6] = '51+'.$currency;

        return $arr;
    }

    public static function translateHourlyRange($id)
    {
        if ('en' == app()->getLocale()) {
            $currency = trans('common.usd');
        } else {
            $currency = trans('common.rmb');
        }

        switch ($id) {
            case 1:
                return '1 - 10 '.$currency;

                break;
            case 2:
                return '11 - 20 '.$currency;

                break;
            case 3:
                return '21 - 30 '.$currency;

                break;
            case 4:
                return '31 - 40 '.$currency;

                break;
            case 5:
                return '41 - 50 '.$currency;

                break;
            case 6:
                return '51+'.$currency;

                break;
            default:
                return '-';

                break;
        }
    }
}
