<?php

namespace App\Http\Controllers;

use App;
use App\Http\Requests;
use App\Models\Countries;
use App\Models\CountryCities;
use App\Models\Faq;
use App\Models\Language;
use App\Models\User;
use Excel;
use File;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Session;

class EtcController extends Controller
{
    public function etc()
    {
        Faq::fixTree();
        $faq = Faq::find(1);

//        $nodes = $faq->descendants->toTree($faq);
//
        $array = array();

        $field = 'title_' . app()->getLocale();
        foreach ($faq->children as $nodes) {

            $parent = '';
            foreach ($nodes->getAncestors() as $p) {
                $parent .= $p->$field . ' - ';
            }
            array_push($array, [$nodes->id => $parent . $nodes->title_cn]);
            foreach ($nodes->children as $node) {

                $parent = '';
                foreach ($node->getAncestors() as $p) {
                    $parent .= $p->$field . ' - ';
                }
                array_push($array, [$node->id => $parent . $node->$field]);
                foreach ($node->children as $node1) {

                    $parent = '';
                    foreach ($node1->getAncestors() as $p) {
                        $parent .= $p->$field . ' - ';
                    }
                    array_push($array, [$node1->id => $parent . $node1->$field]);

                }
            }

        }
    }
    public function getCountries()
    {
//        $client = new Client();
//        $res = $client->request('get', url('countriesToCities.json'), []);
////        $res = $client->request('get', 'http://data.okfn.org/data/core/language-codes/r/language-codes.json', []);
//
////        dd(\GuzzleHttp\json_decode($res->getBody()->getContents()));
//        echo $res;
//        echo \GuzzleHttp\json_encode($user->languages);
    }
    public function setLang(Request $request, $lang) {
//        if (in_array($lang, config('app.locales'))) {
            session([
            	'lang' => $lang
            ]);
//        } else {
//            //Set back to default lang
//            Session::put('lang', config('app.fallback_locale'));
//        }

        // $redirect = $request->get('redirect');
        $redirect = url()->previous();
        $uri_segments = explode('/', $redirect);
        $uri_segments[3] = Session::get('lang');
        $redirect = implode('/',$uri_segments);
        // dd( implode('/',$uri_segments));

        // $redirect = str_replace($request->segment(1));
        // dd($redirect);
        // dd(url()->previous());
        // $redirect = $redirect.'/'.Session::get('lang');

        if( \Auth::check())
        {
            //if user login
            //check user language preference, then load the selected language in preference
            $user = \Auth::guard('users')->user();
            if( $lang == "cn")
            {
                $user->language_preference = User::LANGUAGE_PREFERENCE_CHINESE;
            }
            else
            {
                $user->language_preference = User::LANGUAGE_PREFERENCE_ENGLISH;
            }
            $user->save();
        }

        if (isset($redirect) && $redirect != '' && $redirect != null) {
            return redirect($redirect);
        } else {
            return redirect()->back();
        }
    }

    public function adminLang($lang)
    {
//        if (in_array($lang, config('app.locales'))) {
            Session::put('lang', $lang);
            App::setLocale($lang);
//        } else {
//            //Set back to default lang
//            Session::put('lang', config('app.fallback_locale'));
//        }

        return redirect()->back();
//        $redirect = $request->get('redirect');
//
//        if (isset($redirect) && $redirect != '' && $redirect != null) {
//            return redirect()->to($redirect);
//        } else {
//            return redirect()->to('/admin');
//        }
    }

}


