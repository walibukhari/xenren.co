<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\FrontendController;
use App\Http\Requests\Frontend\PortfolioCreateFormRequest;
use App\Logic\UploadFile\UploadFileRepository;
use App\Models\ChatRoom;
use App\Models\ChatFollower;
use App\Models\Feedback;
use App\Models\Portfolio;
use App\Models\PortfolioFile;
use App\Models\UploadFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;


class PortfolioController extends FrontendController
{
    public function __construct(UploadFileRepository $uploadFileRepository)
    {
        $this->uploadFile = $uploadFileRepository;
    }

    public function index(Request $request)
    {
        //get all the feedback list
        $feedbacks = Feedback::where('user_id', \Auth::id())
            ->get();

        $feedback_id = $request->get('feedback_id');
        if ($feedback_id == 0) {
            //get the latest feedback
            $feedback = Feedback::where('user_id', \Auth::id())
                ->orderBy('created_at', 'desc')
                ->first();
        } else {
            $feedback = Feedback::find($feedback_id);
        }

        if ($feedback != null) {
            //check chat room exist or not
            $chatRoom = ChatRoom::where('type', ChatRoom::TYPE_FEEDBACKS)
                ->where('user_id', $feedback->user_id)
                ->where('feedback_id', $feedback->id)
                ->first();

            //create a chat room if the chat room not exist
            if (!isset($chatRoom)) {
                $chatRoom = new ChatRoom();
                $chatRoom->type = ChatRoom::TYPE_FEEDBACKS;
                $chatRoom->user_id = $feedback->user_id;
                $chatRoom->feedback_id = $feedback->id;
                $chatRoom->save();
            }

            //chat room follower
            $chatFollower = ChatFollower::where('user_id', \Auth::guard('users')->user()->id)
                ->where('room_id', $chatRoom->id)
                ->first();

            //no user exist then add the login user as follower
            if ($chatFollower == null) {
                $chatFollower = new ChatFollower();
                $chatFollower->user_id = \Auth::guard('users')->user()->id;
                $chatFollower->room_id = $chatRoom->id;
                $chatFollower->save();
            }

            $chatFollowers = ChatFollower::with(['user'])
                ->where('room_id', $chatRoom->id)
                ->get();
        } else {
            $chatRoom = null;
            $chatFollowers = null;
        }

        return view('frontend.feedback.index', [
            'feedbacks' => $feedbacks,
            'feedback' => $feedback,
            'chatRoom' => $chatRoom,
            'chatFollowers' => $chatFollowers,
        ]);
    }

    public function getThumbnail(Request $request)
    {
        $fileName = $request->get('fileName');
        $itemId = $request->get('itemId');

        $shortFileName = $this->getShortFilename($fileName);
        $fileType = $this->getFileTypeImage($fileName);

        //get html content
        $view = View::make('frontend.userCenter.thumbnail', [
            'imageFileType' => $fileType['img'],
            'shortFileName' => $shortFileName,
            'itemId' => $itemId,
            'isImage' => $fileType['isImage']
        ]);
        $contents = $view->render();

        return response()->json([
            'status' => 'OK',
            'contents' => $contents
        ]);
    }

    public function createPost(Request $request)
    {
        if($request->has('hidUploadedFile') == false) {
            return makeJSONResponse(false, trans('common.error'), ['error' => 'Please Upload Image']);
        } else if($request->get('title') == '') {
            return makeJSONResponse(false, trans('common.error'), ['error' => 'Please Enter Title']);
        } elseif($request->get('url') == '') {
            return makeJSONResponse(false, trans('common.error'), ['error' => 'Please Enter URL']);
        } else {
            try {
                DB::beginTransaction();
                $portfolio = new Portfolio();
                $portfolio->user_id = Auth::guard('users')->user()->id;
                $portfolio->job_position_id = $request->get('job_position_id');
                $portfolio->title = $request->get('title');
                $portfolio->url = $request->get('url');
                $portfolio->save();

//            if( $request->hasFile('file_upload') )
//            {
//                $fileUploadImages = Input::file('file_upload');
//                foreach ($fileUploadImages as $fileUploadImage) {
//                    $portfolioFile = new PortfolioFile();
//                    $portfolioFile->portfolio_id = $portfolio->id;
//                    $portfolioFile->file = $fileUploadImage;
//                    $portfolioFile->save();
//                }
//            }

                $hidUploadedFiles = $request->get('hidUploadedFile');
                if ($request->has('hidUploadedFile')) {
                    foreach ($hidUploadedFiles as $file) {

                        //copy from file uploaded location to portfolio pic location
                        //add record to DB
                        $oldFile = public_path() . '/uploads/temp/' . $file;
                        $folder = public_path() . '/uploads/portfolio/' . $portfolio->id;
                        $newFile = public_path() . '/uploads/portfolio/' . $portfolio->id . '/' . $file;

//                try {
                        if (!file_exists($folder))
                            \File::makeDirectory($folder);
//                } catch (\Exception $e){
//                	dd($portfolio);
//                }
                        \File::move($oldFile, $newFile);

                        $this->uploadFile->copy($file, UploadFile::TYPE_PORTFOLIO, $portfolio->id);

                        //delete the record from uploaded location
                        //remove record from DB
                        $this->uploadFile->delete($file);
                        $portfolioFile = new PortfolioFile();
                        $portfolioFile->portfolio_id = $portfolio->id;
                        $portfolioFile->thumbnail_file = '/uploads/portfolio/' . $portfolio->id . '/' . $file;
                        $portfolioFile->file = '/uploads/portfolio/' . $portfolio->id . '/' . $file;
                        $portfolioFile->save();
                    }
                }
                DB::commit();

                //only display similar job position new item
                if (Auth::guard('users')->user()->job_position_id == $portfolio->job_position_id) {
                    //get html content
                    $view = View::make('frontend.userCenter.portfolioItem', [
                        'portfolio' => $portfolio
                    ]);
                    $contents = $view->render();
                } else {
                    $contents = "";
                }

                return makeJSONResponse(true, trans('common.success'), ['contents' => $contents]);
            } catch (\Exception $e) {
                DB::rollback();
                return makeJSONResponse(false, $e->getMessage());
            }
        }
    }

    public function delete(Request $request)
    {
        try {
            DB::beginTransaction();
            $id = $request->get('id');

            $portfolio = Portfolio::with('files')->find($id);
            $delete_files_paths = array();
            $portfolioFiles = $portfolio->files;
            foreach ($portfolioFiles as $portfolioFile) {
                $delete_files_paths[] = $portfolioFile->file;
            }

            $portfolio->delete();

            DB::commit();
            //Delete image after commit, make sure no image get deleted but row still exists
            if ($delete_files_paths && count($delete_files_paths) > 0) {
                foreach ($delete_files_paths as $key => $var) {
                    @unlink(public_path($var));
                }
            }

            return makeJSONResponse(true, trans('common.delete_success'));

        } catch (\Exception $e) {
            DB::rollback();
            return makeJSONResponse(false, $e->getMessage());
        }

    }

    private function getFileTypeImage($fileName)
    {
        if (in_array(substr(strrchr($fileName, '.'), 1), ['jpg', 'jpeg'])) {
            $img = "images/jpg.png";
            $isImage = true;
        } else if (in_array(substr(strrchr($fileName, '.'), 1), ['gif'])) {
            $img = "images/gif.png";
            $isImage = true;
        } else if (in_array(substr(strrchr($fileName, '.'), 1), ['png'])) {
            $img = "images/png.png";
            $isImage = true;
        } else if (in_array(substr(strrchr($fileName, '.'), 1), ['bmp'])) {
            $img = "images/bmp.png";
            $isImage = true;
        } else if (in_array(substr(strrchr($fileName, '.'), 1), ['doc', 'docx'])) {
            $img = "images/doc.png";
            $isImage = false;
        } else if (in_array(substr(strrchr($fileName, '.'), 1), ['pdf'])) {
            $img = "images/pdf.png";
            $isImage = false;
        } else if (in_array(substr(strrchr($fileName, '.'), 1), ['xls', 'xlsx'])) {
            $img = "images/xls.png";
            $isImage = false;
        } else if (in_array(substr(strrchr($fileName, '.'), 1), ['mp3'])) {
            $img = "images/mp3.png";
            $isImage = false;
        } else if (in_array(substr(strrchr($fileName, '.'), 1), ['mp4'])) {
            $img = "images/mp4.png";
            $isImage = false;
        }

        return array(
            'img' => $img,
            'isImage' => $isImage
        );
    }

    private function getShortFilename($fileName)
    {
        //get extension
        $array = explode('.', $fileName);
        $extension = end($array);

        $fileNameWithoutExt = str_replace('.' . $extension, "", $fileName);

        if (strlen($fileNameWithoutExt) > 5) {
            $partOfFile = substr($fileNameWithoutExt, 0, 5);


            $result = $partOfFile . '...' . '.' . $extension;
        } else {
            $result = $fileName;
        }

        return $result;
    }
}
