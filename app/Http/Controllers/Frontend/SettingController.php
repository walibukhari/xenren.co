<?php

namespace App\Http\Controllers\Frontend;

use App\Models\User;
use Carbon;
use App\Http\Controllers\FrontendController;
use Illuminate\Http\Request;

class SettingController extends FrontendController
{
    public function index(Request $request)
    {
        return view('frontend.setting.index', [
            'lang' => \Session::get('lang'),
        ]);

    }

    public function getUser(){
        return  User::where('id','=',\Auth::user()->id)->first();
    }

    public function settingPost($data)
    {

        if($data === '1email' || $data === '0email') {
            $a = preg_split('#(?<=\d)(?=[a-z])#i', $data);
            $user = \Auth::guard('users')->user();
            $user->email_notification = $a[0];
            $user->save();
        } else if($data === '1wechat' || $data === '0wechat') {
            $a = preg_split('#(?<=\d)(?=[a-z])#i', $data);
            $user = \Auth::guard('users')->user();
            $user->wechat_notification = $a[0];
            $user->save();
        } else if($data === '2employee') {
            $a = preg_split('#(?<=\d)(?=[a-z])#i', $data);
            $user = \Auth::guard('users')->user();
            $user->uipreference = $a[0];
            $user->save();
        } else if($data === '1freelance') {
            $a = preg_split('#(?<=\d)(?=[a-z])#i', $data);
            $user = \Auth::guard('users')->user();
            $user->uipreference = $a[0];
            $user = \Auth::guard('users')->user();

            $user->save();
        } else if($data === '0uipreference') {
            $a = preg_split('#(?<=\d)(?=[a-z])#i', $data);
            $user = \Auth::guard('users')->user();
            $user->uipreference = $a[0];
            $user->save();
        } else if($data === '1checked_in_cowork_space' || $data === '0checked_in_cowork_space') {
            $a = preg_split('#(?<=\d)(?=[a-z])#i', $data);
            $user = \Auth::guard('users')->user();
            $user->checked_in_cowork_space = $a[0];
            $user->save();
        } else if($data === '1push_notification' || $data === '0push_notification') {
            $a = preg_split('#(?<=\d)(?=[a-z])#i', $data);
            $user = \Auth::guard('users')->user();
            $user->push_notification = $a[0];
            $user->save();
        } else if($data === '1close_account' || $data === '0close_account') {
            $a = preg_split('#(?<=\d)(?=[a-z])#i', $data);
            $user = \Auth::guard('users')->user();
            $user->close_account = $a[0];
            $user->save();
        }
        return makeResponse(trans('common.edit_success'));
    }
}
