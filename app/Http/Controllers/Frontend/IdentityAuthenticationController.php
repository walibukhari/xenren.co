<?php

namespace App\Http\Controllers\Frontend;

use Carbon;
use App\Http\Controllers\FrontendController;
use App\Models\User;
use Input;
use Validator;
use Illuminate\Http\Request;
use Auth;
use Image;
use File;
use App\Models\UserIdentity;
use App\Http\Requests\Frontend\UserIdentityCreateFormRequest;

class IdentityAuthenticationController extends FrontendController
{
    public function index(Request $request)
    {
        $identity = UserIdentity::with('country')->where('user_id', '=', \Auth::guard('users')->user()->id)
            ->orderBy('id', 'DESC')
            ->first();

        if(!$identity)
        {
            $identity = new UserIdentity();
            $identity->user_id = \Auth::guard('users')->user()->id;
            $identity->key = generateStrongPassword(32, false, 'lud');
            $identity->save();
        }

        $targetLink = route('frontend.uploadidcard', ['key' => $identity->key ]);
        $qrLink = 'https://api.qrserver.com/v1/create-qr-code/?size=200x150&data=' . $targetLink;

        return view('frontend.identityAuthentication.index', [
            'identity' => $identity,
            'qrLink' => $qrLink
        ]);
    }

    public function create(UserIdentityCreateFormRequest $request)
    {

//        try {
//            $identity = UserIdentity::with('country')
//                ->whereIn('status', [UserIdentity::STATUS_PENDING])
//                ->where('user_id', '=', \Auth::guard('users')->user()->id)
//                ->orderBy('id', 'DESC')
//                ->first();
//
//            if ($identity) {
//                return makeResponse(trans('member.waiting_approval'), true);
//            }
//
//            $model = new UserIdentity();
//            $model->user_id = \Auth::guard('users')->user()->id;
//            $model->real_name = $request->get('real_name');
//            $model->id_card_no = $request->get('id_card_no');
//            $model->gender = $request->get('gender');
//            if(app()->getLocale() == "cn") {
//                $input_date_of_birth = convertCNDatePicker($request->get('date_of_birth'));
//            }else{
//                $input_date_of_birth = $request->get('date_of_birth');
//            }
//            $model->date_of_birth = $input_date_of_birth;
//            $model->address = $request->get('address');
//            $model->country_id = $request->get('country_id');
//            $model->handphone_no = $request->get('handphone_no');
//            if ($request->hasFile('id_image')) {
//                $model->id_image = $request->file('id_image');
//            }
//
//            $model->save();
//            addSuccess(trans('member.successfully_submit_authentication'));
//            return makeResponse(trans('member.successfully_submit_authentication'));
//        } catch (\Exception $e) {
//            return makeResponse($e->getMessage(), true);
//        }
    }

    public function delete(Request $request) {
	      $userIdentity = UserIdentity::where('user_id', '=', \Auth::guard('users')->user()->id)->first();
	      if($userIdentity->status == UserIdentity::STATUS_PENDING) {
		      $identity = UserIdentity::where('user_id', '=', \Auth::guard('users')->user()->id)->update([
		      	'status' => null,
			      'is_submit' => null
		      ]);
		        addSuccess(trans('member.successfully_delete_authentication'));
	      } else {
		      $identity = UserIdentity::where('user_id', '=', \Auth::guard('users')->user()->id)
			      ->whereIn('status', [
				      UserIdentity::STATUS_APPROVED
			      ]);
		
		      if ($identity->count()) {
			      foreach ($identity->get() as $key => $var) {
				      $var->delete();
			      }
			      addSuccess(trans('member.successfully_delete_authentication'));
			
		      } else {
			      addError(trans('member.authentication_not_found'));
		      }
	      }
        return redirect()->route('frontend.identityauthentication');
    }

//    public function change(Request $request)
//    {
//        $column = $request->get('column');
//        $value = $request->get('value');
//        $id = $request->get('id');
//        $userIdentity = UserIdentity::find($id);
//
//        switch($column)
//        {
//            case "real_name":
//                $userIdentity->real_name = $value;
//                break;
//            case "id_card_no":
//                $userIdentity->id_card_no = $value;
//                break;
//            case "gender":
//                $userIdentity->gender = $value;
//                break;
//            case "date_of_birth":
//                $userIdentity->date_of_birth = $value;
//                break;
//            case "address":
//                $userIdentity->address = $value;
//                break;
//            case "country":
//                $userIdentity->country_id = $value;
//                break;
//            case "handphone_no":
//                $userIdentity->handphone_no = $value;
//                break;
//            case "id_image":
//                if ($request->hasFile('id_image')) {
//                    $userIdentity->id_image = $request->file('id_image');
//                }
//                break;
//        }
//        $userIdentity->save();
//        return makeJSONResponse(true, 'success');
//    }

//    public function submit(UserIdentityCreateFormRequest $request)
//    {
//        $id = $request->get('user_identity_id');
//        $identity = UserIdentity::find($id);
//        try {
//
//            if ($identity->status == UserIdentity::STATUS_PENDING && $identity->is_submit == 1) {
//                return makeResponse(trans('member.waiting_approval'), true);
//            }
//
//            if( $identity->id_image == '' || $identity->id_image == null )
//            {
//                return makeResponse(trans('validation.required', ['attribute' => trans('common.id_image')]), true);
//            }
//
//            $identity->user_id = \Auth::guard('users')->user()->id;
//            $identity->status = UserIdentity::STATUS_PENDING;
//            $identity->is_submit = 1;
//            $identity->save();
//
//            addSuccess(trans('member.successfully_submit_authentication'));
//            return makeResponse(trans('member.successfully_submit_authentication'));
//        } catch (\Exception $e) {
//            return makeResponse($e->getMessage(), true);
//        }
//    }

}