<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\ProjectApplicant;
use App\Models\ProjectDispute;
use App\Models\ProjectDisputesAttachments;
use App\Models\ProjectDisputesComment;
use App\Models\ProjectFileDisputes;
use App\Models\ProjectMilestones;
use App\Models\ProjectMilestoneStauts;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserCanceledDispute;
use App\Services\GeneralService;
use App\Services\GeneralServices;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use DB;
use App\Http\Requests;

class ProjectDisputeController extends Controller
{
    public function createDispute(Request $request, $projectId)
    {
        if($request->type == 1) { // create manual dispute
            $dispute = self::manaulDispute($request, $projectId);
        } else { // create dispute against milestone
            $dispute = self::milestoneDispute($request, $projectId);
        }

        return $dispute;
    }

    public function detail($id)
    {
        $detail = ProjectFileDisputes::where('id', '=', $id)
	        ->with([
	        	'applicant',
	        	'project.creator',
	        	'milestones.applicant',
	        	'transaction',
	        ])->first();
	      
        $amount = isset($detail->milestones) ? $detail->milestones->amount : $detail->transaction->amount;
		    $dueDate = Carbon::parse($detail->created_at)->addDays(14);
		
		    $employer = $detail->project->creator;
		    $applicant = isset($detail->milestones) ? $detail->milestones->applicant : $detail->applicant;
				
			    
	      $applicantAttachments = ProjectDisputesAttachments::where('project_dispute_id', '=', $detail->id)
		      ->where('user_id', '=', $applicant->id)
		      ->get();
	      
	      $employerAttachments = ProjectDisputesAttachments::where('project_dispute_id', '=', $detail->id)
		      ->where('user_id', '=', $employer->id)
		      ->get();
	
	      $employerComment = ProjectDisputesComment::where('project_dispute_id', '=', $detail->id)
		      ->where('user_id', '=', $employer->id)
		      ->orderBy('id', 'desc')
		      ->first();
	
	      $applicantComment = ProjectDisputesComment::where('project_dispute_id', '=', $detail->id)
		      ->where('user_id', '=', $applicant->id)
		      ->orderBy('id', 'desc')
		      ->first();
	      
        return view('frontend.evidence.index', [
            'detail' => $detail,
            'amount' => $amount,
            'projectName' => $detail->project->name,
            'dueDate' => $dueDate,
            'applicant' => $applicant,
            'employer' => $employer,
            'employerComment' => $employerComment,
            'applicantComment' => $applicantComment,
            'applicantAttachments' => $applicantAttachments,
            'employerAttachments' => $employerAttachments,
	          'adminComment' => isset($detail->admin_comment) ? $detail->admin_comment : false
        ]);
    }
	
		public function updateDisputeComment(Request $request)
		{
			$count = ProjectDisputesComment::where('project_dispute_id', '=', $request->dispute_id)
				->where('user_id', '=', Auth::user()->id)->withTrashed()->get()->count();
			
			if($count > 3) {
				return collect([
					'status' => 'error',
					'message' => 'You can only update comment only three times...!'
				]);
			}
			
			$attachments = null;
			$arr = null;
			if($request->attachments_length > 0) {
				$arr = array();
				for ($i = 0; $i < $request->attachments_length; $i++) {
					$icon = self::uploadFile($request, 'uploads/disputes', 'attachment_' . $i);
					array_push($arr, $icon['icon']);
				}
			}
			$attachments = $arr;
			
			\DB::transaction(function() use($attachments, $request) {
				if(!is_null($attachments)) {
					foreach ($attachments as $k => $v) {
						ProjectDisputesAttachments::create([
							'project_dispute_id' => $request->dispute_id,
							'user_id' => Auth::user()->id,
							'attachment' => $v
						]);
					}
				}
				ProjectDisputesComment::where('project_dispute_id', '=', $request->dispute_id)
					->where('user_id', '=', Auth::user()->id)
					->delete();
				ProjectDisputesComment::
				create([
					'comment' => $request->comment,
					'project_dispute_id' =>  $request->dispute_id,
					'user_id' => Auth::user()->id
				]);
			});
			
			return collect([
				'status' => 'success',
				'message' => 'Updated...!'
			]);
		}
	
		public function updateDisputeAttachment(Request $request)
		{
			$attachment = ProjectDisputesAttachments::where('project_dispute_id', '=', $request->dispute_id)
			->where('user_id', '=', Auth::user()->id)
			->where('id', '=', $request->attachment_id)->first();
			if(is_null($attachment)) {
				return collect([
					'status' => 'error',
					'message' => 'File not found...!'
				]);
			}
			$attachment = ProjectDisputesAttachments::where('project_dispute_id', '=', $request->dispute_id)
				->where('user_id', '=', Auth::user()->id)
				->where('id', '=', $request->attachment_id)->delete();
			
			return collect([
				'status' => 'success',
				'message' => 'Attachment Removed...!'
			]);
		}
		
    public function manaulDispute($request, $projectId)
    {
        $_project_dispute = new ProjectDispute();
        $input = [
            'amount' => $request->amount,
            'project_id' => $projectId,
            'type' => ProjectDispute::TYPE_MANUAL_DISPUTE,
            'status' => 1,
        ];
        return collect([
            'status' => 'success',
            'data' => $_project_dispute->createDispute($input)
        ]);
    }

    public function milestoneDispute($request, $projectId)
    {
        $_project_dispute = new ProjectDispute();
        $d = $_project_dispute
            ->where('milestone_id', '=', $request->milestone_id)
            ->where('project_id', '=', $projectId)
            ->first();
        if(isset($d)) {
            return collect([
                'status' => 'failure',
                'message' => 'Dispute for this milestone is already created...!'
            ]);
        }

        if(!isset($request->reason)) {
            return collect([
                'status' => 'failure',
                'message' => 'Reason for dispute is required...!'
            ]);
        }
        $input = [
            'milestone_id' => $request->milestone_id,
            'project_id' => $projectId,
            'type' => ProjectDispute::TYPE_MILESTONE_DISPUTE,
            'status' => 1,
        ];

        $milestoneStatus = ProjectMilestoneStauts::where('milestone_id', '=', $request->milestone_id)
            ->where('project_id', '=', $projectId)->first();
        if(is_null($milestoneStatus)) {
            return collect([
                'status' => 'failure',
                'message' => 'Milestone not found...!'
            ]);
        }

        $milestoneStatus->update([
            'status' => ProjectMilestones::STATUS_DISPUTED,
            'comment' => $request->input('reason', '')
        ]);

        return collect([
            'status' => 'success',
            'data' => []
        ]);
    }
	
		public function rejectRefundDispute(Request $request, $milestoneId)
		{
			ProjectMilestoneStauts::where('milestone_id', '=', $milestoneId)->where('status', '=', ProjectMilestones::STATUS_DISPUTED)->update([
				'status' => ProjectMilestones::STATUS_FREELANCER_REJECTED_REFUND,
				'comment' => isset($request->message) ? $request->message : ''
			]);
			return collect([
				'status' => 'success',
				'data' => 'Milestone refund request refunded'
			]);
		}
    
    public function refundDispute(Request $request, $projectId)
    {
        $milestoneStatus = ProjectMilestoneStauts::with('milestone.project')->where('milestone_id', '=', $request->milestone_id)
            ->where('project_id', '=', $projectId)->first();
//        $d = ProjectDispute::where('milestone_id', '=', $request->milestone_id)
//            ->where('project_id', '=', $projectId)
//            ->first();
//
//        if(is_null($milestoneStatus) || is_null($d)) {
//            return collect([
//                'status' => 'failure',
//                'message' => 'Milestone dispute not found...!'
//            ]);
//        }
//
//        $d->update(['status' => ProjectDispute::STATUS_REFUNDED]);

        $milestoneStatus->update([
            'status' => ProjectMilestones::STATUS_REFUNDED,
            'comment' => $request->input('reason', '')
        ]);
	      $milestoneAmount = $milestoneStatus->milestone->amount;
	      
	      Transaction::where('milestone_id', '=', $milestoneStatus->milestone->id)
		                 ->where('status', '=', Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM)
	                   ->update([
	                      'name' => 'Milestone Refunded',
			                  'status' => Transaction::STATUS_APPROVED,
			                  'type' => Transaction::TYPE_MILESTONE_PAYMENT_REFUND
	                   ]);
	      
//		    $data = [
//			    'name' => 'Milestone Refund',
//			    'amount' => $milestoneStatus->milestone->amount - GeneralService::calculateServiceFee($milestoneAmount),
//			    'milestone_id' => $milestoneStatus->milestone->id,
//			    'type' => Transaction::TYPE_MILESTONE_PAYMENT_REFUND,
//			    'balance_before' => \Auth::user()->account_balance,
//			    'balance_after' => (\Auth::user()->account_balance) - $milestoneAmount - GeneralService::calculateServiceFee($milestoneAmount),
//			    'status' => Transaction::STATUS_APPROVED,
//			    'comment' => '',
//			    'performed_by' => \Auth::user()->id,
//			    'performed_to' => $milestoneStatus->milestone->project->user_id
//		    ];
//		    Transaction::create($data);
		    
//		    User::where('id', '=', \Auth::user()->id)->decrement('account_balance', ($milestoneAmount - (GeneralService::calculateServiceFee($milestoneAmount)) ));
		    User::where('id', '=', $milestoneStatus->milestone->project->user_id)->increment('account_balance', $milestoneAmount);
		
	
	    return collect([
            'status' => 'success'
        ]);
    }


    public function updateDisputeStatus(Request $request)
    {
        $_project_dispute = new ProjectDispute();
        $_project_dispute->where('id', '=', $request->dispute_id)->update([
            'status' => $request->status
        ]);
        return collect([
            'status' => 'success',
            'message' => 'Updated dispute..!'
        ]);
    }

    public function endContract(Request $request,$projectId)
    {
       ProjectApplicant::where('user_id', '=', $request->applicant_id)->where('project_id', '=', $projectId)->update([
        'status' => ProjectApplicant::STATUS_COMPLETED
      ]);
	DB::enableQueryLog();
	    $isEmployerExist = ProjectApplicant::where('project_id', '=', $projectId)
		    ->where('status', '=', ProjectApplicant::STATUS_APPLICANT_ACCEPTED)
		    ->first();
			

			
	    $isEmployerExist1 = ProjectApplicant::where('project_id', '=', $projectId)
		    ->Where('status', '=', ProjectApplicant::STATUS_CREATOR_SELECTED)
			->first();
			
			if(is_null($isEmployerExist) && is_null($isEmployerExist1)) {
				$_project = new Project();
				$_project->where('id', '=', $projectId)->update([
					'status' => Project::STATUS_COMPLETED,
					'reason_to_end' => $request->reason_to_end
				]);
			}
      return collect([
          'status' => 'success',
          'message' => 'Contract Ended...!'
      ]);
    }

    public function dailyUpdate(Request $request, $projectId)
    {
        $_project = new Project();
        $_project->where('id', '=', $projectId)->update([
            'daily_update' => $request->daily_update,
        ]);
        ProjectApplicant::where('project_id', '=', $projectId)->where('user_id', '=', $request->applicant_id)->update([
	        'daily_update' => $request->daily_update,
        ]);
        return collect([
            'status' => 'success',
            'message' => 'Contract Updated...!'
        ]);
    }

    public function fileDisputeEmployer(Request $request) {
	    $arr = array();
	    for ($i=0; $i<$request->attachments_length; $i++) {
		    $icon = self::uploadFile($request, 'uploads/disputes', 'attachment_'.$i);
		    array_push($arr, $icon['icon']);
	    }
	    $attachments = $arr;
	    try {
	    	$dispute = ProjectDispute::where( 'project_id' , '=' , $request->project_id)->where('milestone_id' , '=' , $request->milestone_id)->first();
	    	if(!is_null($dispute)) {
	    		return collect([
	    			'status' => 'failure',
				    'message' => 'Dispute already exist.'
			    ]);
		    }
		    $dispute = \DB::transaction(function () use ($request, $attachments) {
			    $dispute = ProjectDispute::create([
				    'project_id' => $request->project_id,
				    'milestone_id' => $request->milestone_id,
				    'created_by' => Auth::user()->id,
				    'status' => ProjectDispute::STATUS_INPROGRESS
			    ]);
			    ProjectDisputesComment::create([
				    'project_dispute_id' => $dispute->id,
				    'user_id' => Auth::user()->id,
				    'comment' => $request->comment,
			    ]);
			    if ($request->attachments_length > 0) {
				    foreach ($attachments as $k => $v) {
					    ProjectDisputesAttachments::create([
						    'project_dispute_id' => $dispute->id,
						    'user_id' => Auth::user()->id,
						    'attachment' => $v
					    ]);
				    }
			    }
			    return $dispute;
		    });
	    } catch (\Exception $e){
	    	dd($e);
	    }
//	    if(!is_null(ProjectFileDisputes::where('transaction_id', '=', $request->transaction_id)->first())) {
//		    $record = ProjectFileDisputes::where('transaction_id', '=', $request->transaction_id)->update([
//			    'attachments' => $attachments,
//			    'comment' => $request->comment,
//			    'status' => ProjectFileDisputes::STATUS_FILE_DISPUTE_IN_PROGRESS,
//			    'type' => ProjectFileDisputes::TYPE_REPORTED_BY_EMPLOYERS
//		    ]);
//	    } else {
//		    $record = ProjectFileDisputes::create([
//			    'attachments' => $attachments,
//			    'comment' => $request->comment,
//			    'transaction_id'  => $request->transaction_id,
//			    'status' => ProjectFileDisputes::STATUS_FILE_DISPUTE_IN_PROGRESS,
//			    'type' => ProjectFileDisputes::TYPE_REPORTED_BY_EMPLOYERS
//		    ]);
//	    }
	    return collect([
		    'status' => 'success',
		    'message' => 'Filed Dispute',
		    'data' => $dispute
	    ]);
    }
	
		public function cancelDisputeFreelancer(Request $request)
		{
				$dispute = ProjectDispute::where( 'project_id' , '=' , $request->project_id)->where('transaction_id' , '=' , $request->transaction_id)->first();
				ProjectDispute::where( 'project_id' , '=' , $request->project_id)->where('transaction_id' , '=' , $request->transaction_id)->update([
					'status' => ProjectDispute::STATUS_CANCELED_BY_FREELANCER
				]);
				Transaction::where('id', '=', $request->transaction_id)->update([
					'status' => Transaction::STATUS_DISPUTE_CANCELED_BY_FREELANCER
				]);
				return collect([
					'status' => 'success',
					'message' => 'dispute canceled...!'
				]);
		}
    
    public function fileDispute(Request $request)
    {
      $arr = array();
      for ($i=0; $i<$request->attachments_length; $i++) {
          $icon = (self::uploadFile($request, 'uploads/disputes', 'attachment_'.$i));
          array_push($arr, $icon['icon']);
      }
	    $attachments = $arr;
	    try {
		    $dispute = ProjectDispute::where( 'project_id' , '=' , $request->project_id)->where('transaction_id' , '=' , $request->transaction_id)->first();
		    if(!is_null($dispute)) {
			    if(($dispute->status == ProjectDispute::STATUS_CANCELED_BY_FREELANCER)) {
				    return collect([
					    'status' => 'failure',
					    'message' => 'you can\'t create dispute for previously canceled dispute..!'
				    ]);
			    }
			    return collect([
				    'status' => 'failure',
				    'message' => 'Dispute already exist.'
			    ]);
		    }
		    
		    $dispute = \DB::transaction(function () use ($request, $attachments) {
			    UserCanceledDispute::create([
				    'user_id' => Auth::user()->id,
				    'transaction_id'=> $request->transaction_id,
				    'project_id' => $request->project_id,
			    ]);
			    $dispute = ProjectDispute::create([
				    'project_id' => $request->project_id,
				    'transaction_id' => $request->transaction_id,
				    'created_by' => Auth::user()->id,
				    'status' => ProjectDispute::STATUS_INPROGRESS
			    ]);
			    ProjectDisputesComment::create([
				    'project_dispute_id' => $dispute->id,
				    'user_id' => Auth::user()->id,
				    'comment' => $request->comment,
			    ]);
			    Transaction::where('id', '=', $request->transaction_id)->update([
			    	'status' => Transaction::STATUS_IN_PROGRESS_DISPUTE
			    ]);
			    if ($request->attachments_length > 0) {
				    foreach ($attachments as $k => $v) {
					    ProjectDisputesAttachments::create([
						    'project_dispute_id' => $dispute->id,
						    'user_id' => Auth::user()->id,
						    'attachment' => $v
					    ]);
				    }
			    }
			    return $dispute;
		    });
	    } catch (\Exception $e){
		    return collect($e);
	    }

//        $data = ProjectDispute::where('milestone_id', '=', $request->milestone_id)->first();

//        if(!is_null($data)) {
//        	if(!is_null(ProjectFileDisputes::where('transaction_id', '=', $request->transaction_id)->first())) {
//		        $record = ProjectFileDisputes::where('transaction_id', '=', $request->transaction_id)->update([
//			        'attachments' => $attachments,
//			        'comment' => $request->comment,
//			        'status' => ProjectFileDisputes::STATUS_FILE_DISPUTE_IN_PROGRESS
//		        ]);
//	        } else {
//		        $record = ProjectFileDisputes::create([
//			        'attachments' => $attachments,
//			        'comment' => $request->comment,
//			        'transaction_id'  => $request->transaction_id,
//			        'status' => ProjectFileDisputes::STATUS_FILE_DISPUTE_IN_PROGRESS,
//		        ]);
//	        }
//        } else {
//            $record = ProjectFileDisputes::create([
//                'attachments' => $attachments,
//                'comment' => $request->comment,
//                'transaction_id'  => $request->transaction_id,
//                'status' => ProjectFileDisputes::STATUS_FILE_DISPUTE_IN_PROGRESS,
//            ]);
//        }

        return collect([
            'status' => 'success',
            'message' => 'Filed Dispute',
            'data' => $dispute
        ]);
    }

    public function uploadFile($request, $path, $fileName)
    {
        if (!$path)
            return false;

        try {
            $dt = Carbon::parse(Carbon::Now());
            $timeStamp = $dt->timestamp;
            $destinationPath = public_path() . '/' .$path;
            $file = Input::file($fileName);
            $extension = Input::file($fileName)->getClientOriginalExtension();
            $originalName = Input::file($fileName)->getClientOriginalName();
            $fileOriginalName = Input::file($fileName)->getClientOriginalName();
            $fileOriginalName = str_replace('.' . $extension, "", $fileOriginalName);
            $fileName = $timeStamp . '__' . $fileOriginalName . '.' . $extension;
            $file->move($destinationPath, $fileName);
            return collect([
                'status' => 'success',
                'icon' => $fileName,
                'fileName' => $originalName
            ]);
        } catch (Exception $e) {
            return $e;
        }
    }
}
