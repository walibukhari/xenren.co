<?php

namespace App\Http\Controllers\Frontend;

use App\Libraries\PayPal_PHP_SDK\PayPalTrait;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\User;
use App\Services\GeneralService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Stripe\Stripe;

class StripeController extends Controller
{
    use PayPalTrait;
    // function __construct()
    // {
    // 	Stripe::setApiKey(config('services.stripe.secret'));
    // }

    public function invoices()
    {
        $user = User::where('id', '=', Auth::user()->id)->first();
        $user->invoiceFor('TopUp Fee', 5000);
        $invoices = $user->invoices();
        dump($invoices);
        foreach ($invoices as $k => $invoice) {
            dump($invoice->date()->toFormattedDateString());
            dump( $invoice->total() );
            dump( $invoice->id );
        }
        dd(1);
    }

    public function index(Request $request)
    {
        $data = json_decode($request->data);
        \Log::info('stripe');
        \Log::info($request->all());
        $user = User::where('id', '=', Auth::user()->id)->first();

        if (!$user->hasStripeId()) {
            // $customer = $this->createStripeCustomer($data->id);
            $user->createAsStripeCustomer([
                'name' => $user->name,
                'source' => $data->id,
                'description' => $user->email,
            ]);
        }

        // else
        // {
        // 	$customer = \Stripe\Customer::retrieve(Auth::user()->stripe_id);
        // }
        $fee = round(GeneralService::calculateServiceFee($request->amount));
        $amount = $request->amount + $fee;
        $charge = $this->createStripeCharge($amount, 'Topup Fee For Account', $user);
        if(!is_array($charge)) {
            return $charge;
        }

        $balance = $user->account_balance;
        $currency_name = User::getCurrencyName(\Auth::user()->country_id);
        $data = [
            'name' => 'Topup Amount',
            'amount' => $amount,
            'milestone_id' => 0,
            'type' => Transaction::TYPE_TOP_UP,
            'balance_before' => $balance,
            'balance_after' => $balance + $request->amount,
            'performed_by_balance_before' => $balance,
            'performed_by_balance_after' => $balance + $request->amount,
            'performed_to_balance_before' => $balance,
            'performed_to_balance_after' => $balance + $request->amount,
            'performed_by' => \Auth::user()->id,
            'performed_to' => \Auth::user()->id,
            'status' => Transaction::STATUS_IN_PROGRESS_TRANSACTION,
            'comment' => $charge['id'],
            'currency' => $currency_name,
        ];
        $tr = Transaction::create($data);
        TransactionDetail::create([
            'transaction_id' => $tr->id,
            'payment_method' => TransactionDetail::PAYMENT_METHOD_STRIPE,
            'raw' => json_encode($charge)
        ]);
        $data = [
            'name' => 'Topup',
            'amount' => $fee,
            'milestone_id' => 0,
            'type' => Transaction::TYPE_SERVICE_FEE,
            'balance_before' => $balance,
            'balance_after' => $balance + $request->amount,
            'performed_by_balance_before' => $balance,
            'performed_by_balance_after' => $balance + $request->amount,
            'performed_to_balance_before' => $balance,
            'performed_to_balance_after' => $balance + $request->amount,
            'performed_by' => \Auth::user()->id,
            'performed_to' => \Auth::user()->id,
            'status' => Transaction::STATUS_REQUESTED,
            'comment' => $charge['id'],
            'currency' => $currency_name
        ];
        Transaction::create($data);
    }

    // public function isStripeCustomer()
    // {
    // 	return \Auth::user() && \App\Models\User::where('id', \Auth::user()->id)->whereNotNull('stripe_id')->first();
    // }

    public function createStripeCharge($product_price, $product_name, $user)
    {
        try {
//			$charge = \Stripe\Charge::create(array(
//				"amount" => $product_price,
//				"currency" => "USD",
//				"customer" => $customer->id,
//				"description" => $product_name
//			));
            // $user = User::where('id', '=', Auth::user()->id)->first();
            $charge = $user->invoiceFor($product_name, $product_price * 100);
        } catch(\Stripe\Error\Card $e) {
            \Log::info('Stripe error: '.$e->getMessage());
            return redirect()
                ->route('home')
                ->with('error', 'Your credit card was been declined. Please try again or contact us.');
        }

        return $charge;
    }

    // public function createStripeCustomer($token)
    // {
    // 	Stripe::setApiKey(config('services.stripe.secret'));

    // 	$customer = \Stripe\Customer::create(array(
    //         "name" => \Auth::user()->name,
    //         "email" => \Auth::user()->email,
    // 		"description" => \Auth::user()->email,
    // 		"source" => $token
    // 	));

    // 	\Auth::user()->stripe_id = $customer->id;
    // 	\Auth::user()->save();

    // 	return $customer;
    // }

}
