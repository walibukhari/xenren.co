<?php

namespace App\Http\Controllers\Frontend;

use App\Constants;
use App\Models\JobPosition;
use App\Models\OfficialProjectNews;
use App\Models\Portfolio;
use App\Models\ProjectDispute;
use App\Models\Review;
use App\Models\Transaction;
use App\Models\UserCanceledDispute;
use App\Models\UserInbox;
use App\Models\UserInboxMessages;
use App\Models\UserSkillComment;
use Carbon;
use App\Http\Controllers\FrontendController;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;

use Auth;
use App\Models\Project;
use App\Models\ProjectChatRoom;
use App\Models\ProjectChatFollower;
use App\Models\ProjectApplicant;
use App\Models\Skill;
use App\Models\Countries;
use App\Models\CountryCities;
use App\Models\WorldCities;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;


class JoinDiscussionController extends FrontendController
{
    public function index()
    {

    }

    public function getProject($lang, $slug, $id)
    {
        $project = Project::with(['projectChatRoom', 'projectSkills', 'projectFiles'])->find($id);
        //for resume user page and redirect back
        $userInvited = preg_replace('/[0-9]/', '', $id);
        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

        if (!$project) {
            return view('errors.404');
        } else {
            Session::put('redirect_url', route('frontend.joindiscussion.getproject', ['slug' => $project->name, 'id' => $id]));
            Session::put('redirect_url_name', trans('common.back_to_join_discussion'));

            if ($project->freelance_type == Project::FREELANCER_TYPE_INVITED_USER) {
                if ($userInvited != "pvt") {
                    return view('errors.404');
                }
            }
        }

        //redis init
        Redis::publish('rooms', json_encode(['room' => 'default_room']));
        Redis::publish('room', $id);
        //Project Chat Room

        $projectChatRoom = ProjectChatRoom::GetProjectChatRoom()
            ->where('project_id', $project->id)
            ->first();

        //create a chat room if the chat room not exist
        if (!isset($projectChatRoom)) {
            $projectChatRoom = new ProjectChatRoom();
            $projectChatRoom->project_id = $project->id;
            $projectChatRoom->save();
        }

        //chat room follower
        if (\Auth::guard('users')->check()) {

            $projectChatFollower = ProjectChatFollower::GetProjectChatFollower()
                ->where('user_id', \Auth::guard('users')->user()->id)
                ->where('project_chat_room_id', $projectChatRoom->id)
                ->first();
        } else {
            $projectChatFollower = ProjectChatFollower::GetProjectChatFollower();
        }

        $isNewComer = false;
        if ($projectChatFollower == null) {
            $projectChatFollower = new ProjectChatFollower();
            $projectChatFollower->project_chat_room_id = $projectChatRoom->id;
            $projectChatFollower->user_id = \Auth::guard('users')->user()->id;
            $projectChatFollower->is_kick = 0;
            $projectChatFollower->save();

            $isNewComer = true;
        }

        $projectChatFollowers = ProjectChatFollower::with(['user'])
            ->where('project_chat_room_id', $projectChatRoom->id)
            ->where('user_id', '!=', null)
            ->orderBy('thumb_status', 'asc')
            ->get();

        $user = \Auth::guard('users')->user();

        $candidateAlreadySelected = $project->haveApprovedApplicant();
        $candidateUserId = $project->getApprovedApplicant();

        $projectApplicants = ProjectApplicant::with(['user'])
            ->where('user_id', '>', 0)//because have user = null when new job post for published status
            ->where('project_id', $project->id)
            ->paginate(10);

        $skill_id_list = "";
        foreach ($project->projectSkills as $key => $projectSkill) {
            if ($key == 0) {
                $skill_id_list = $projectSkill->skill_id;
            } else {
                $skill_id_list = $skill_id_list . "," . $projectSkill->skill_id;
            }
        }

        if ($isNewComer == true) {
            //check whether user inbox exist or not
            $userInbox = UserInbox::where('project_id', $project->id)
                ->where('type', UserInbox::TYPE_PROJECT_PEOPLE_COME_IN)
                ->first();

            if (!$userInbox) {
                //system message notification
                $userInbox = new UserInbox();
                $userInbox->category = UserInbox::CATEGORY_SYSTEM;
                $userInbox->project_id = $project->id;

                if ($project->type == Project::PROJECT_TYPE_COMMON) {
                    $userInbox->from_user_id = null;
                    $userInbox->to_user_id = $project->user_id;
                    $userInbox->from_staff_id = 1;
                    $userInbox->to_staff_id = null;
                } else if ($project->type == Project::PROJECT_TYPE_OFFICIAL) {
                    $userInbox->from_user_id = null;
                    $userInbox->to_user_id = null;
                    $userInbox->from_staff_id = 1;
                    $userInbox->to_staff_id = $project->staff_id;
                }

                $userInbox->type = UserInbox::TYPE_PROJECT_PEOPLE_COME_IN;
                $userInbox->is_trash = 0;
                $userInbox->save();
            }

            //add user inbox message
            $userInboxMessage = new UserInboxMessages();
            $userInboxMessage->inbox_id = $userInbox->id;
            if ($project->type == Project::PROJECT_TYPE_COMMON) {
                $userInboxMessage->from_user_id = null;
                $userInboxMessage->to_user_id = $project->user_id;
                $userInboxMessage->from_staff_id = 1;
                $userInboxMessage->to_staff_id = null;
            } else if ($project->type == Project::PROJECT_TYPE_OFFICIAL) {
                $userInboxMessage->from_user_id = null;
                $userInboxMessage->to_user_id = null;
                $userInboxMessage->from_staff_id = 1;
                $userInboxMessage->to_staff_id = $project->staff_id;
            }

            $userInboxMessage->project_id = $project->id;
            $userInboxMessage->is_read = 0;
            $userInboxMessage->type = UserInboxMessages::TYPE_PROJECT_PEOPLE_COME_IN;

            $message = trans('common.user_come_in', ['nickname' => \Auth::guard('users')->user()->getName()]);
            $userInboxMessage->custom_message = $message;
            $userInboxMessage->save();
        }

        $officialProjectNewsList = OfficialProjectNews::where('project_id', $project->id)
            ->orderBy('created_at', 'desc')
            ->limit(4)
            ->get();

        if (\Auth::guard('users')->check()) {
            //make the project as read if you are project creator
            if ($project->is_read == Project::IS_READ_NO && $project->user_id == \Auth::guard('users')->user()->id) {
                $project->is_read = Project::IS_READ_YES;
                $project->save();
            }
        }

        //make the project applicant read if you are project applicant
        if (\Auth::guard('users')->check()) {
            $applicant = ProjectApplicant::where('user_id', \Auth::guard('users')->user()->id)
                ->where('project_id', $project->id)
                ->first();
        } else {
            $applicant = ProjectApplicant::where('user_id', '')
                ->where('project_id', $project->id)
                ->first();
        }
        if (!empty($applicant)) {
            $applicant->is_read = ProjectApplicant::IS_READ_YES;
            $applicant->save();
        } else {
            $applicant = ProjectApplicant::where('user_id', null)
                ->where('project_id', $project->id)
                ->first();
        }

        $postSkill = [];
        if (!empty($applicant->skill_id_string)) {
            $postSkill = Skill::whereIn('id', explode(",", $applicant->skill_id_string))
                ->get();
        }
        if (\Auth::guard('users')->check()) {
            if (\Auth::guard('users')->user()->is_agent == 1) {
                $skillList = Skill::getSearchSkillData(Skill::TAG_COLOR_GOLD);
            } else {
                $skillList = Skill::getSearchSkillData(Skill::TAG_COLOR_WHITE);
            }
        } else {
            $skillList = Skill::getSearchSkillData(Skill::TAG_COLOR_GOLD);
        }

        $pay_types = Constants::getPayTypeLists();
        if (empty($applicant->reference_price_type))
            $type = $project->pay_type;
        else
            $type = $applicant->reference_price_type;

        $post_type = 'project';
        $explode = explode('img', $project->description);
        $replace = preg_replace
        ('/^.|.><.|.br.|.><.|.style.|.><.|.width.|.><.|.<".|.757px;".|.><.|.">.|.p>/', '', $explode);
        $firstIndex = array_shift($replace);
        $link = str_replace("\"<", "", str_replace('src="', '', $replace));
        $getPath = isset($link) ? $link : null;

        $redirectBackUrl = route('frontend.jobs.index', ['lang' => \Session::get('lang')]);
        $redirectBackName = trans('common.back_to_job_listing');
        $link = asset($lang.'/joinDiscussion/getProject/' . $slug . '/' . $id);
        $qr_code = "https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=" . $link;
        //project owner chat first
        $getOwnerfirst = array();
        foreach ($projectChatFollowers as $projectOwner) {
            if ($projectOwner->user_id == $project->user_id) {
                array_push($getOwnerfirst, $projectOwner);
            }
        }

        return view('frontend.joinDiscussion.index', [
            'project' => $project,
            'getOwnerfirst' => $getOwnerfirst,
            'qr_code' => $qr_code,
            'redirectUrl' => $redirectBackUrl,
            'redirectBackName' => $redirectBackName,
            'projectChatRoomId' => $projectChatRoom->id,
            'projectChatFollowers' => $projectChatFollowers,
            'user' => $user,
            'projectApplicants' => $projectApplicants,
            'applicant' => $applicant,
            'path' => $getPath,
            'candidateAlreadySelected' => $candidateAlreadySelected,
            'candidateUserId' => $candidateUserId,
            'skill_id_list' => $skill_id_list,
            'officialProjectNewsList' => $officialProjectNewsList,
            'postSkill' => $postSkill,

            'post_type' => $post_type,
            'pay_types' => $pay_types,
            'type' => $type,
            'skillList' => $skillList,
        ]);
    }

    public function jobDetail($id, $applicantId, Request $request)
    {

//    	dump(Carbon\Carbon::parse(Carbon\Carbon::now())->startOfWeek()->addDay(0)->format('D d'));
//    	dd(Carbon\Carbon::parse(Carbon\Carbon::now())->startOfWeek()->format('D'));
        //for resume user page and redirect back

        $userInvited = preg_replace('/[0-9]/', '', $id);
        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

        $project = Project::with([
            'timeTracked' => function ($q) {
                $q->where('created_at', '<=', Carbon\Carbon::now()->endOfWeek())->where('user_id', '=', \Auth::user()->id);
            },
            'timeTracked.pictures',
            'projectChatRoom',
            'projectSkills',
            'projectFiles',
            'milestones' => function ($q) use ($applicantId) {
                $q->where('applicant_id', '=', $applicantId);
                $q->orderBy('updated_at', '=', 'desc');
            },
            'milestones.filedDisputes',
            'milestones.transaction' => function ($q) {
                $q->where('type', '!=', Transaction::TYPE_SERVICE_FEE);
                $q->orderBy('updated_at', '=', 'desc');
            },
            'milestones.transaction.filedTransactionDispute',
            'milestones.milestoneStatus',
            'creator.country',
            'creator.city',
        ])->find($id);

        if (!$project) {
            return view('errors.404');
        } else {
            Session::put('redirect_url', route('frontend.joindiscussion.getproject', ['slug' => $project->name, 'id' => $id]));
            Session::put('redirect_url_name', trans('common.back_to_join_discussion'));
            if ($project->freelance_type == Project::FREELANCER_TYPE_INVITED_USER) {
                if ($userInvited != "pvt") {
                    return view('errors.404');
                }
            }
        }

        //redis init
        Redis::publish('rooms', json_encode(['room' => 'default_room']));
        Redis::publish('room', $id);
        //Project Chat Room

        $projectChatRoom = ProjectChatRoom::GetProjectChatRoom()
            ->where('project_id', $project->id)
            ->first();

        //create a chat room if the chat room not exist
        if (!isset($projectChatRoom)) {
            $projectChatRoom = new ProjectChatRoom();
            $projectChatRoom->project_id = $project->id;
            $projectChatRoom->save();
        }

        //chat room follower
        $projectChatFollower = ProjectChatFollower::GetProjectChatFollower()
            ->where('user_id', \Auth::guard('users')->user()->id)
            ->where('project_chat_room_id', $projectChatRoom->id)
            ->first();

        $isNewComer = false;
        if ($projectChatFollower == null) {
            $projectChatFollower = new ProjectChatFollower();
            $projectChatFollower->project_chat_room_id = $projectChatRoom->id;
            $projectChatFollower->user_id = \Auth::guard('users')->user()->id;
            $projectChatFollower->is_kick = 0;
            $projectChatFollower->save();

            $isNewComer = true;
        }

        $projectChatFollowers = ProjectChatFollower::with(['user'])
            ->where('project_chat_room_id', $projectChatRoom->id)
            ->where('user_id', '!=', null)
            ->orderBy('thumb_status', 'asc')
            ->get();

        $user = \Auth::guard('users')->user();

        $candidateAlreadySelected = $project->haveApprovedApplicant();
        $candidateUserId = $project->getApprovedApplicant();

        $projectApplicants = ProjectApplicant::with(['user'])
            ->where('user_id', '>', 0)//because have user = null when new job post for published status
            ->where('project_id', $project->id)
            ->paginate(10);

        $skill_id_list = "";
        foreach ($project->projectSkills as $key => $projectSkill) {
            if ($key == 0) {
                $skill_id_list = $projectSkill->skill_id;
            } else {
                $skill_id_list = $skill_id_list . "," . $projectSkill->skill_id;
            }
        }

        if ($isNewComer == true) {
            //check whether user inbox exist or not
            $userInbox = UserInbox::where('project_id', $project->id)
                ->where('type', UserInbox::TYPE_PROJECT_PEOPLE_COME_IN)
                ->first();

            if (!$userInbox) {
                //system message notification
                $userInbox = new UserInbox();
                $userInbox->category = UserInbox::CATEGORY_SYSTEM;
                $userInbox->project_id = $project->id;

                if ($project->type == Project::PROJECT_TYPE_COMMON) {
                    $userInbox->from_user_id = null;
                    $userInbox->to_user_id = $project->user_id;
                    $userInbox->from_staff_id = 1;
                    $userInbox->to_staff_id = null;
                } else if ($project->type == Project::PROJECT_TYPE_OFFICIAL) {
                    $userInbox->from_user_id = null;
                    $userInbox->to_user_id = null;
                    $userInbox->from_staff_id = 1;
                    $userInbox->to_staff_id = $project->staff_id;
                }

                $userInbox->type = UserInbox::TYPE_PROJECT_PEOPLE_COME_IN;
                $userInbox->is_trash = 0;
                $userInbox->save();
            }

            //add user inbox message
            $userInboxMessage = new UserInboxMessages();
            $userInboxMessage->inbox_id = $userInbox->id;
            if ($project->type == Project::PROJECT_TYPE_COMMON) {
                $userInboxMessage->from_user_id = null;
                $userInboxMessage->to_user_id = $project->user_id;
                $userInboxMessage->from_staff_id = 1;
                $userInboxMessage->to_staff_id = null;
            } else if ($project->type == Project::PROJECT_TYPE_OFFICIAL) {
                $userInboxMessage->from_user_id = null;
                $userInboxMessage->to_user_id = null;
                $userInboxMessage->from_staff_id = 1;
                $userInboxMessage->to_staff_id = $project->staff_id;
            }

            $userInboxMessage->project_id = $project->id;
            $userInboxMessage->is_read = 0;
            $userInboxMessage->type = UserInboxMessages::TYPE_PROJECT_PEOPLE_COME_IN;

            $message = trans('common.user_come_in', ['nickname' => \Auth::guard('users')->user()->getName()]);
            $userInboxMessage->custom_message = $message;
            $userInboxMessage->save();
        }

        $officialProjectNewsList = OfficialProjectNews::where('project_id', $project->id)
            ->orderBy('created_at', 'desc')
            ->limit(4)
            ->get();

        //make the project as read if you are project creator
        if ($project->is_read == Project::IS_READ_NO && $project->user_id == \Auth::guard('users')->user()->id) {
            $project->is_read = Project::IS_READ_YES;
            $project->save();
        }

        //make the project applicant read if you are project applicant
        $applicant = ProjectApplicant::where('user_id', \Auth::guard('users')->user()->id)
            ->where('project_id', $project->id)
            ->first();
        if (!empty($applicant)) {
            $applicant->is_read = ProjectApplicant::IS_READ_YES;
            $applicant->save();
        } else {
            $applicant = ProjectApplicant::where('user_id', null)
                ->where('project_id', $project->id)
                ->first();
        }

        $postSkill = [];
        if (!empty($applicant->skill_id_string)) {
            $postSkill = Skill::whereIn('id', explode(",", $applicant->skill_id_string))
                ->get();
        }

        if (\Auth::guard('users')->user()->is_agent == 1) {
            $skillList = Skill::getSearchSkillData(Skill::TAG_COLOR_GOLD);
        } else {
            $skillList = Skill::getSearchSkillData(Skill::TAG_COLOR_WHITE);

        }

        $pay_types = Constants::getPayTypeLists();
        if (empty($applicant->reference_price_type))
            $type = $project->pay_type;
        else
            $type = $applicant->reference_price_type;

        $post_type = 'project';

        $projectDisputes = ProjectDispute::where('project_id', '=', $id)->whereIn('status', [ProjectDispute::STATUS_INPROGRESS, ProjectDispute::STATUS_CONFIRM])->with('milestone')->get();

        if ($project->type == Project::PROJECT_TYPE_OFFICIAL) {
            $filterDate = isset($request->date) ? Carbon\Carbon::parse($request->date) : (Carbon\Carbon::now());
            $filterDateSOM = Carbon\Carbon::parse($request->date)->startOfMonth();
            $filterDateEOM = Carbon\Carbon::parse($request->date)->endOfMonth();
            $filterDateSOW = Carbon\Carbon::parse($request->date)->startOfWeek();
            $filterDateEOW = Carbon\Carbon::parse($request->date)->endOfWeek();

            $trackerTime = $project->timeTracked;
            $recordedTime = ($trackerTime->groupBy('date'));
            $totalTimeLogged = 0;
            $totalTimeLoggedLastWeek = 0;
            $totalEarning = 0;

            $firstWeekHours = 0;
            $firstWeekEarning = 0;

            $secondWeekHours = 0;
            $secondWeekEarning = 0;

            $thirdWeekHours = 0;
            $thirdWeekEarning = 0;

            $fourthWeekHours = 0;
            $fourthWeekEarning = 0;

            $fifthWeekHours = 0;
            $fifthWeekEarning = 0;

            foreach ($recordedTime as $k => $v) {
                foreach ($v as $k1 => $v1) {
                    foreach ($v1->pictures as $k2 => $v2) {
                        $hourlyRate = $v2->hourly_rate;
                        $perMinute = $hourlyRate / 60;
                        $fiveMinutesPrice = $perMinute * 5;
                        $totalEarning += $fiveMinutesPrice;
                        $start = Carbon\Carbon::parse($v2->created_at);
                        if ($start->weekOfMonth == 1) {
                            $firstWeekEarning += $fiveMinutesPrice;
                        }
                        if ($start->weekOfMonth == 2) {
                            $secondWeekEarning += $fiveMinutesPrice;
                        }

                        if ($start->weekOfMonth == 3) {
                            $thirdWeekEarning += $fiveMinutesPrice;
                        }
                        if ($start->weekOfMonth == 4) {
                            $fourthWeekEarning += $fiveMinutesPrice;
                        }
                        if ($start->weekOfMonth == 5) {
                            $fifthWeekEarning += $fiveMinutesPrice;
                        }
                    }
                    $startTime = isset($v1->start_time) ? Carbon\Carbon::parse($v1->start_time) : Carbon\Carbon::now();
                    $endTime = isset($v1->end_time) ? Carbon\Carbon::parse($v1->end_time) : Carbon\Carbon::now();
                    $totalTimeDiff = $endTime->diffInSeconds($startTime);
                    $totalTimeDiffHours = $endTime->diffInHours($startTime);
                    if ($v1->start_time >= $filterDateSOM && $v1->start_time <= $filterDateEOM) {
                        $totalTimeLogged += $totalTimeDiffHours;
                    }

                    if ($v1->start_time >= $filterDateSOW && $v1->start_time <= $filterDateEOW) {
                        $totalTimeLoggedLastWeek += $totalTimeDiff;
                    }

                    if ($startTime >= Carbon\Carbon::parse($request->date)->startOfMonth() && $startTime <= Carbon\Carbon::parse($request->date)->endOfMonth()) {
                        $startTime = isset($v1->start_time) ? Carbon\Carbon::parse($v1->start_time) : Carbon\Carbon::now();
                        $endTime = isset($v1->end_time) ? Carbon\Carbon::parse($v1->end_time) : Carbon\Carbon::now();
                        $hours = $endTime->diffInSeconds($startTime);

                        if ($startTime->weekOfMonth == 1) {
                            $firstWeekHours += $hours;
                        }
                        if ($startTime->weekOfMonth == 2) {
                            $secondWeekHours += $hours;
                        }

                        if ($startTime->weekOfMonth == 3) {
                            $thirdWeekHours += $hours;
                        }
                        if ($startTime->weekOfMonth == 4) {
                            $fourthWeekHours += $hours;
                        }
                        if ($startTime->weekOfMonth == 5) {
                            $fifthWeekHours += $hours;
                        }
                    }

                }
            }
            $date = isset($request->date) ? Carbon\Carbon::parse($request->date) : Carbon\Carbon::now();
            $totalHours = $firstWeekHours + $secondWeekHours + $thirdWeekHours + $fourthWeekHours + $fifthWeekHours;
            $totalEarning = $firstWeekEarning + $secondWeekEarning + $thirdWeekEarning + $fourthWeekEarning + $fifthWeekEarning;
            return view('frontend.officialProject.myReceiveOrder.index', [
                'project' => $project,
                'projectChatRoomId' => $projectChatRoom->id,
                'projectChatFollowers' => $projectChatFollowers,
                'user' => $user,
                'projectApplicants' => $projectApplicants,
                'applicant' => $applicant,
                'candidateAlreadySelected' => $candidateAlreadySelected,
                'candidateUserId' => $candidateUserId,
                'skill_id_list' => $skill_id_list,
                'officialProjectNewsList' => $officialProjectNewsList,
                'postSkill' => $postSkill,
                'post_type' => $post_type,
                'pay_types' => $pay_types,
                'type' => $type,
                'skillList' => $skillList,
                'project_disputes' => $projectDisputes,
                'recordedTime' => $recordedTime,
                'trackerTime' => $trackerTime,
                'totalTimeLoggedLastWeek' => gmdate("H:i", $totalTimeLoggedLastWeek),
                'totalTimeLoggedHours' => $totalTimeLogged,
                'totalEarning' => round($totalEarning, 2),
                'date' => $date,
                'date1' => isset($request->date) ? Carbon\Carbon::parse($request->date)->subDays(1) : Carbon\Carbon::now(),
                'filters' => isset($request->date) ? true : false,
                'weekOfMonth' => Carbon\Carbon::parse($request->date)->weekOfMonth,
                'firstWeekHours' => gmdate('H:i', $firstWeekHours),
                'secondWeekHours' => gmdate('H:i', $secondWeekHours),
                'thirdWeekHours' => gmdate('H:i', $thirdWeekHours),
                'fourthWeekHours' => gmdate('H:i', $fourthWeekHours),
                'fifthWeekHours' => gmdate('H:i', $fifthWeekHours),
                'totalHours' => gmdate('H:i', $totalHours),
                'firstWeekEarning' => $firstWeekEarning,
                'secondWeekEarning' => $secondWeekEarning,
                'thirdWeekEarning' => $thirdWeekEarning,
                'fourthWeekEarning' => $fourthWeekEarning,
                'fifthWeekEarning' => $fifthWeekEarning,
            ]);
        } else {
//				    if($project->type == Project::PAY_TYPE_HOURLY_PAY) {
//					    	return view('frontend.commonHourlyProject.myReceiveOrder.index', [
//									'project' => $project,
//									'projectChatRoomId' => $projectChatRoom->id,
//									'projectChatFollowers' => $projectChatFollowers,
//									'user' => $user,
//									'projectApplicants' => $projectApplicants,
//									'applicant' => $applicant,
//									'candidateAlreadySelected' => $candidateAlreadySelected,
//									'candidateUserId' => $candidateUserId,
//									'skill_id_list' => $skill_id_list,
//									'officialProjectNewsList' => $officialProjectNewsList,
//									'postSkill' => $postSkill,
//									'post_type' => $post_type,
//									'pay_types' => $pay_types,
//									'type' => $type,
//									'skillList' => $skillList,
//									'project_disputes' => $projectDisputes
//								]);
//				    }
//				    else {
            return view('frontend.myReceiveOrder.jobDetail', [
                'project' => $project,
                'disputedCount' => UserCanceledDispute::where('user_id', '=', Auth::user()->id)->where('project_id', '=', $project->id)->count(),
                'projectChatRoomId' => $projectChatRoom->id,
                'projectChatFollowers' => $projectChatFollowers,
                'user' => $user,
                'projectApplicants' => $projectApplicants,
                'applicant' => $applicant,
                'candidateAlreadySelected' => $candidateAlreadySelected,
                'candidateUserId' => $candidateUserId,
                'skill_id_list' => $skill_id_list,
                'officialProjectNewsList' => $officialProjectNewsList,
                'postSkill' => $postSkill,
                'post_type' => $post_type,
                'pay_types' => $pay_types,
                'type' => $type,
                'skillList' => $skillList,
                'project_disputes' => $projectDisputes
            ]);
        }
//				}
    }

    public function chooseCandidate(Request $request)
    {
        if ($request->has('projectId') && $request->get('projectId') > 0) {
            $projectId = $request->get('projectId');
        } else {
            return makeJSONResponse(false, 'No Project ID');
        }

        if ($request->has('userId') && $request->get('userId') > 0) {
            $userId = $request->get('userId');
        } else {
            return makeJSONResponse(false, 'No User ID');
        }

        $model = ProjectApplicant::where('project_id', $projectId)
            ->where('user_id', $userId)
            ->first();
        $model->status = 1;
        $model->save();

        return makeJSONResponse(true, 'Candidate Choose Successfully');
    }

    public function awardJob($applicantId)
    {
        $model = ProjectApplicant::with(['project', 'user'])->find($applicantId);

        if ($model) {
            //Check if project have accepted applicant or not
            $exists = ProjectApplicant::where('project_id', '=', $model->project_id)
                ->where('status', '=', ProjectApplicant::STATUS_CREATOR_SELECTED)
                ->count();

//            if ($exists) {
//                return makeJSONResponse(false, trans('common.project_already_have_applicant'));
//            }


            $model->status = ProjectApplicant::STATUS_CREATOR_SELECTED;
            $model->is_read = ProjectApplicant::IS_READ_NO;

            $currentProject = ProjectApplicant::where('project_id', '=', $model->project_id)
                ->first();

            // $model->title = $currentProject->title;
            // $model->description = $currentProject->description;
            // $model->reference_price = $currentProject->reference_price;
            // $model->reference_price_type = $currentProject->reference_price_type;
            // $model->skill_id_string = $currentProject->skill_id_string;

            $project = $model->project;
            $project->status = Project::STATUS_SELECTED;

            //Here we send user inbox
            $ui = new UserInbox();
            $ui->category = UserInbox::CATEGORY_NORMAL;
            $ui->project_id = $project->id;
            $ui->from_user_id = $project->user_id;
            $ui->to_user_id = $model->user_id;
            $ui->from_staff_id = null;
            $ui->to_staff_id = null;
            $ui->type = UserInbox::TYPE_PROJECT_APPLICANT_SELECTED;
            $ui->is_trash = 0;

            //Here we create user inbox message
            $uim = new UserInboxMessages();
            $uim->inbox_id = $ui->id;
            $uim->from_user_id = $project->user_id;
            $uim->to_user_id = $model->user_id;
            $uim->from_staff_id = null;
            $uim->to_staff_id = null;
            $uim->project_id = $project->id;
            $uim->is_read = 0;
            $uim->type = UserInboxMessages::TYPE_PROJECT_APPLICANT_SELECTED;
            $uim->custom_message = null;
            $uim->quote_price = null;
            $uim->pay_type = null;

            try {
                \DB::beginTransaction();

                if (!$model->save()) {
                    return makeJSONResponse(false, trans('common.unknown_error'));
                }

                if (!$project->save()) {
                    return makeJSONResponse(false, trans('common.unknown_error'));
                }

                $ui->project_id = $project->id;
                if (!$ui->save()) {
                    return makeJSONResponse(false, trans('common.unknown_error'));
                }

                $uim->project_id = $project->id;
                $uim->inbox_id = $ui->id;
                if (!$uim->save()) {
                    return makeJSONResponse(false, trans('common.unknown_error'));
                }

                //Update the applicant unread message
                $applicant = $model->user;
                $applicant->unread_message = UserInboxMessages::where('to_user_id', '=', $applicant->id)
                    ->where('is_read', '=', 0)
                    ->count();
                if (!$applicant->save()) {
                    return makeJSONResponse(false, trans('common.unknown_error'));
                }

                \DB::commit();
                return makeJSONResponse(true, trans('common.submit_successful'));
            } catch (\Exception $e) {
                \DB::rollback();
                return makeJSONResponse(false, $e->getLine());
            }

        } else {
            return makeJSONResponse(false, trans('common.project_applicant_not_found'));
        }
    }

    public function reject($applicantId)
    {
        $model = ProjectApplicant::find($applicantId);
        $model->status = ProjectApplicant::STATUS_CREATOR_REJECTED;
        $model->is_read = ProjectApplicant::IS_READ_NO;

        try {
            if ($model->save()) {
                return makeJSONResponse(true, trans('common.submit_successful'));

            } else {
                return makeJSONResponse(false, trans('common.err_happen'));
            }
        } catch (\Exception $e) {
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function sendMessage($applicantId)
    {
        $projectApplicant = ProjectApplicant::with('user')->find($applicantId);

        return view('frontend.joinDiscussion.sendMessage', [
            'projectApplicant' => $projectApplicant
        ]);
    }

    public function sendMessagePost($applicantId, Request $request)
    {
        $projectApplicant = ProjectApplicant::with('user')->where('id', $applicantId)->first();

        try {
            \DB::beginTransaction();

            //add user inbox notification
            $userInbox = new UserInbox();
            $userInbox->category = UserInbox::CATEGORY_NORMAL;
            $userInbox->project_id = $projectApplicant->project_id;
            $userInbox->from_user_id = Auth::id();
            $userInbox->from_staff_id = null;
            $userInbox->to_staff_id = null;
            $userInbox->to_user_id = $projectApplicant->user_id;
            $userInbox->type = UserInbox::TYPE_PROJECT_NEW_MESSAGE;
            $userInbox->is_trash = 0;
            $userInbox->save();

            //add user inbox message
            $userInboxMessage = new UserInboxMessages();
            $userInboxMessage->inbox_id = $userInbox->id;
            $userInboxMessage->from_user_id = Auth::id();
            $userInboxMessage->to_user_id = $projectApplicant->user_id;
            $userInboxMessage->from_staff_id = null;
            $userInboxMessage->to_staff_id = null;
            $userInboxMessage->project_id = $projectApplicant->project_id;
            $userInboxMessage->is_read = 0;
            $userInboxMessage->type = UserInbox::TYPE_PROJECT_NEW_MESSAGE;

            $message = $request->get('message');
            $userInboxMessage->custom_message = $message;
            $userInboxMessage->quote_price = null;
            $userInboxMessage->pay_type = null;
            $userInboxMessage->file = null;

            if ($userInboxMessage->save()) {

                \DB::commit();
                return makeResponse(trans('common.submit_successful'));

            } else {
                \DB::rollback();
                throw new \Exception(trans('common.err_happen'));
            }
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse($e->getMessage(), true);
        }
    }

    public function thumbUp($projectChatFollowerId)
    {
        $projectChatFollower = ProjectChatFollower::find($projectChatFollowerId);
        $projectChatFollower->thumb_status = ProjectChatFollower::THUMB_STATUS_THUMB_UP;

        try {
            if ($projectChatFollower->save()) {
                return makeJSONResponse(true, trans('common.submit_successful'));

            } else {
                return makeJSONResponse(false, trans('common.err_happen'));
            }
        } catch (\Exception $e) {
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function thumbDown($projectChatFollowerId)
    {
        $projectChatFollower = ProjectChatFollower::find($projectChatFollowerId);
        $projectChatFollower->thumb_status = ProjectChatFollower::THUMB_STATUS_THUMB_DOWN;

        try {
            if ($projectChatFollower->save()) {
                return makeJSONResponse(true, trans('common.submit_successful'));

            } else {
                return makeJSONResponse(false, trans('common.err_happen'));
            }
        } catch (\Exception $e) {
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    /**
     * @param $applicantId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function applicantDetail($applicantId)
    {
        $projectApplicant = ProjectApplicant::with('user')->find($applicantId);

        $adminReviews = UserSkillComment::with(['files', 'staff', 'skills', 'skills.skill'])
            ->where('user_id', '=', $projectApplicant->user->id)
            ->get();

        $reviews = Review::with('reviewer_user', 'reviewer_staff', 'reviewee_user', 'project')
            ->where('reviewee_user_id', $projectApplicant->user->id)
            ->get();

        $job_positions = JobPosition::all();

        $portfolios = Portfolio::with('files')
            ->where('user_id', $projectApplicant->user->id)
            ->where('job_position_id', $projectApplicant->user->job_position_id)
            ->get();

        $projectChatRoom = ProjectChatRoom::where('project_id', $projectApplicant->project_id)->first();

        $projectChatFollower = ProjectChatFollower::where('project_chat_room_id', $projectChatRoom->id)
            ->where('user_id', $projectApplicant->user->id)
            ->first();

        return view('frontend.joinDiscussion.userMakeOfferDetail', [
            'projectApplicant' => $projectApplicant,
            'reviews' => $reviews,
            'job_positions' => $job_positions,
            'portfolios' => $portfolios,
            'adminReviews' => $adminReviews,
            'projectChatFollower' => $projectChatFollower
        ]);
    }

    public function news($project_id)
    {
        $officialProjectNewsList = OfficialProjectNews::where('project_id', $project_id)
            ->orderBy('created_at', 'desc')
            ->get();

        $project = Project::find($project_id);

        return view('frontend.joinDiscussion.news', [
            'officialProjectNewsList' => $officialProjectNewsList,
            'project' => $project
        ]);
    }

    public function quitProjectDiscussion(Request $request)
    {
        $projectChatFollowerId = $request->get('id');
        $projectChatFollower = ProjectChatFollower::find($projectChatFollowerId);

        try {
            if ($projectChatFollower->delete()) {
                return makeJSONResponse(true, trans('common.submit_successful'));
            } else {
                return makeJSONResponse(false, trans('common.err_happen'));
            }
        } catch (\Exception $e) {
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function getProjectApplicant($id, $slug)
    {
        //for resume user page and redirect back
        $userInvited = preg_replace('/[0-9]/', '', $id);
        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

        //$applicant = ProjectApplicant::find($id);
        //$projectId = isset($applicant->project_id) ? $applicant->project_id : 0;
        $project = Project::with(['projectChatRoom', 'projectSkills', 'projectFiles', 'awardedApplicant'])->find($id);
        if (!$project) {
            return view('errors.404');
        } else {

            Session::put('redirect_url', route('frontend.joindiscussion.getproject', ['slug' => $project->name, 'id' => $id]));
            Session::put('redirect_url_name', trans('common.back_to_join_discussion'));
            if ($project->freelance_type == Project::FREELANCER_TYPE_INVITED_USER) {
                if ($userInvited != "pvt") {
                    return view('errors.404');
                }
            }
        }

        //redis init
        Redis::publish('rooms', json_encode(['room' => 'default_room']));
        Redis::publish('room', $id);
        //Project Chat Room

        $projectChatRoom = ProjectChatRoom::GetProjectChatRoom()
            ->where('project_id', $project->id)
            ->first();

        //create a chat room if the chat room not exist
        if (!isset($projectChatRoom)) {
            $projectChatRoom = new ProjectChatRoom();
            $projectChatRoom->project_id = $project->id;
            $projectChatRoom->save();
        }

        //chat room follower
        $projectChatFollower = ProjectChatFollower::GetProjectChatFollower()
            ->where('user_id', \Auth::guard('users')->user()->id)
            ->where('project_chat_room_id', $projectChatRoom->id)
            ->first();

        $isNewComer = false;
        if ($projectChatFollower == null) {
            $projectChatFollower = new ProjectChatFollower();
            $projectChatFollower->project_chat_room_id = $projectChatRoom->id;
            $projectChatFollower->user_id = \Auth::guard('users')->user()->id;
            $projectChatFollower->is_kick = 0;
            $projectChatFollower->save();

            $isNewComer = true;
        }

        $projectChatFollowers = ProjectChatFollower::with(['user'])
            ->where('project_chat_room_id', $projectChatRoom->id)
            ->where('user_id', '!=', null)
            ->orderBy('thumb_status', 'asc')
            ->get();

        $user = \Auth::guard('users')->user();

        $candidateAlreadySelected = $project->haveApprovedApplicant();
        $candidateUserId = $project->getApprovedApplicant();

        $projectApplicants = ProjectApplicant::with(['user'])
            ->where('user_id', '>', 0)//because have user = null when new job post for published status
            ->where('project_id', $project->id)
            ->paginate(10);

        $skill_id_list = "";
        foreach ($project->projectSkills as $key => $projectSkill) {
            if ($key == 0) {
                $skill_id_list = $projectSkill->skill_id;
            } else {
                $skill_id_list = $skill_id_list . "," . $projectSkill->skill_id;
            }
        }

        if ($isNewComer == true) {
            //check whether user inbox exist or not
            $userInbox = UserInbox::where('project_id', $project->id)
                ->where('type', UserInbox::TYPE_PROJECT_PEOPLE_COME_IN)
                ->first();

            if (!$userInbox) {
                //system message notification
                $userInbox = new UserInbox();
                $userInbox->category = UserInbox::CATEGORY_SYSTEM;
                $userInbox->project_id = $project->id;

                if ($project->type == Project::PROJECT_TYPE_COMMON) {
                    $userInbox->from_user_id = null;
                    $userInbox->to_user_id = $project->user_id;
                    $userInbox->from_staff_id = 1;
                    $userInbox->to_staff_id = null;
                } else if ($project->type == Project::PROJECT_TYPE_OFFICIAL) {
                    $userInbox->from_user_id = null;
                    $userInbox->to_user_id = null;
                    $userInbox->from_staff_id = 1;
                    $userInbox->to_staff_id = $project->staff_id;
                }

                $userInbox->type = UserInbox::TYPE_PROJECT_PEOPLE_COME_IN;
                $userInbox->is_trash = 0;
                $userInbox->save();
            }

            //add user inbox message
            $userInboxMessage = new UserInboxMessages();
            $userInboxMessage->inbox_id = $userInbox->id;
            if ($project->type == Project::PROJECT_TYPE_COMMON) {
                $userInboxMessage->from_user_id = null;
                $userInboxMessage->to_user_id = $project->user_id;
                $userInboxMessage->from_staff_id = 1;
                $userInboxMessage->to_staff_id = null;
            } else if ($project->type == Project::PROJECT_TYPE_OFFICIAL) {
                $userInboxMessage->from_user_id = null;
                $userInboxMessage->to_user_id = null;
                $userInboxMessage->from_staff_id = 1;
                $userInboxMessage->to_staff_id = $project->staff_id;
            }

            $userInboxMessage->project_id = $project->id;
            $userInboxMessage->is_read = 0;
            $userInboxMessage->type = UserInboxMessages::TYPE_PROJECT_PEOPLE_COME_IN;

            $message = trans('common.user_come_in', ['nickname' => \Auth::guard('users')->user()->getName()]);
            $userInboxMessage->custom_message = $message;
            $userInboxMessage->save();
        }

        $officialProjectNewsList = OfficialProjectNews::where('project_id', $project->id)
            ->orderBy('created_at', 'desc')
            ->limit(4)
            ->get();

        //make the project as read if you are project creator
        if ($project->is_read == Project::IS_READ_NO && $project->user_id == \Auth::guard('users')->user()->id) {
            $project->is_read = Project::IS_READ_YES;
            $project->save();
        }

        //make the project applicant read if you are project applicant
        $applicant = ProjectApplicant::where('user_id', \Auth::guard('users')->user()->id)
            ->where('project_id', $project->id)
            ->first();
        if (!empty($applicant)) {
            $applicant->is_read = ProjectApplicant::IS_READ_YES;
            $applicant->save();
        } else {
            $applicant = ProjectApplicant::find($id);
        }

        $postSkill = [];
        if (!empty($applicant->skill_id_string)) {
            $postSkill = Skill::whereIn('id', explode(",", $applicant->skill_id_string))
                ->get();
        }

        if (\Auth::guard('users')->user()->is_agent == 1) {
            $skillList = Skill::getSearchSkillData(Skill::TAG_COLOR_GOLD);
        } else {
            $skillList = Skill::getSearchSkillData(Skill::TAG_COLOR_WHITE);

        }

        $pay_types = Constants::getPayTypeLists();
        if (empty($applicant->reference_price_type))
            $type = $project->pay_type;
        else
            $type = $applicant->reference_price_type;

        $post_type = 'project';
        $explode = explode('img', $project->description);
        $replace = preg_replace
        ('/^.|.><.|.br.|.><.|.style.|.><.|.width.|.><.|.<".|.757px;".|.><.|.">.|.p>/', '', $explode);
        $firstIndex = array_shift($replace);
        $link = str_replace("\"<", "", str_replace('src="', '', $replace));
        $getPath = isset($link) ? $link : null;

        $redirectBackUrl = route('frontend.jobs.index');
        $redirectBackName = trans('common.back_to_job_listing');
        $link = asset('joinDiscussion/getProject/' . $slug . '/' . $id);
        $qr_code = "https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=" . $link;

        //project owner chat first
        $getOwnerfirst = array();
        foreach ($projectChatFollowers as $projectOwner) {
            if ($projectOwner->user_id == $project->user_id) {
                array_push($getOwnerfirst, $projectOwner);
            }
        }

        return view('frontend.joinDiscussion.index', [
            'project' => $project,
            'qr_code' => $qr_code,
            'getOwnerfirst' => $getOwnerfirst,
            'redirectUrl' => $redirectBackUrl,
            'redirectBackName' => $redirectBackName,
            'projectChatRoomId' => $projectChatRoom->id,
            'projectChatFollowers' => $projectChatFollowers,
            'user' => $user,
            'projectApplicants' => $projectApplicants,
            'path' => $getPath,
            'applicant' => $applicant,
            'candidateAlreadySelected' => $candidateAlreadySelected,
            'candidateUserId' => $candidateUserId,
            'skill_id_list' => $skill_id_list,
            'officialProjectNewsList' => $officialProjectNewsList,
            'postSkill' => $postSkill,
            'path' => $getPath,
            'post_type' => $post_type,
            'pay_types' => $pay_types,
            'type' => $type,
            'skillList' => $skillList,
        ]);
    }

    public function change(Request $request)
    {
        // dd($request->all());
        $column = $request->column;
        $id = $request->id;
        $user_id = \Auth::guard('users')->user()->id;
        $project_applicant = ProjectApplicant::find($id);
        if ($column == 'title') {
            $project_applicant->title = $request->get('val');
            $project_applicant->save();
            return makeJSONResponse(true, 'success');
        } elseif ($column == 'description') {
            //dd($request->get('val'));
            $project_applicant->description = $request->get('val');
            $project_applicant->save();
            return makeJSONResponse(true, 'success');
        } elseif ($column == 'reference_price') {
            $project_applicant->reference_price = $request->get('val');
            $project_applicant->reference_price_type = $request->reference_price_type;
            $project_applicant->save();
            return makeJSONResponse(true, 'success');
        } elseif ($column == 'skill_id_string') {
            $project_applicant->skill_id_string = $request->get('val');
            $project_applicant->save();
            return makeJSONResponse(true, 'success');
        }

        return makeJSONResponse(true, 'success');
    }

}
