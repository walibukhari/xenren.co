<?php

namespace App\Http\Controllers\Frontend;

use App\Jobs\SharedOfficeContact;
use App\Libraries\MobileDetect\Mobile_Detect;
use App\Models\SharedOfficeBooking;
use App\Models\SharedOfficeCategory;
use App\Models\SharedOfficeFinance;
use App\Models\SharedOfficeImages;
use App\Models\SharedOfficeProductCategory;
use App\Models\SharedOfficeProducts;
use App\Models\SharedOfficeRating;
use App\Models\SharedOfficeRequest;
use App\Models\SharedOffice;
use App\Models\SharedOfficeSetting;
use App\Models\Staff;
use App\Models\Transaction;
use App\Models\User;
use App\Models\WorldCities;
use App\Models\WorldContinents;
use App\Models\WorldCountries;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use function GuzzleHttp\is_host_in_noproxy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use App\Models\SharedOfficeRequestImage;
use App\Http\Requests\Frontend\resetPasswordRequest;
use function GuzzleHttp\Psr7\str;

class SharedOfficeController extends Controller
{

    public function searchPage(Request $request, $lang, $countryName, $cityName = null)
    {
        $filterType = isset($request->type) ? $request->type : '';
        $continents = array('Asia', 'Europe', 'Europe', 'Africa', 'Oceania', 'Antarctica', 'North America', 'South America');
        $check = (in_array(str_replace('-', ' ', $countryName), $continents));

        $data = [
            'country' => '',
            'location' => '',
            'city' => '',
            'countCountry' => '',
            'countCity' => '',
            'filterType' => $filterType,
            'rating' => isset($request->rating) ? $request->rating : ''
        ];

        if ($check == true) {
            $continentName = str_replace('-', ' ', $countryName);
            $continent = WorldContinents::where('name', '=', $continentName)->first();
            $sharedOffices = collect(json_decode(file_get_contents(base_path('public/jsons/homepage.json')), true));
            $arr = [];
            foreach ($sharedOffices as $sharedOffice) {
                if($sharedOffice['country']['continent_id'] == $continent->id) {
                    array_push($arr,$sharedOffice);
                }
            }
            $data = [
                'country' => '',
                'location' => $continentName,
                'city' => '',
                'continent' => $continent->name,
                'countContinent' => count($arr),
                'countCountry' => '',
                'countCity' => '',
                'filterType' => $filterType,
                'rating' => isset($request->rating) ? $request->rating : ''
            ];
            return view("frontend.coWorks.search_new", $data);
        }

        if (!$cityName) {
            $location = str_replace(' ', '-', $countryName);
            $country = WorldCountries::with('continent')->where('name', 'like', '%' . $countryName . '%')->first();
            $country_name = str_replace("-", " ", $countryName);
            $country = WorldCountries::with('continent')->where('name', 'like', '%' . $country_name . '%')->first();
            if (is_null($country)) {
                $data['country'] = $countryName;
                $data['location'] = $location;
            } else {
                $countContinent = SharedOffice::where('continent_id', '=', $country->continent_id)
                    ->where('active', '=', 1)
                    ->count();
                $countCountry = $this->countCountry($country->name);
                //check filter type against country
                if ($request->type && $request->type != '' && $request->type != 'all-spaces') {
                    $id = self::getFilterType($request->type);
                    $countCountry->whereHas('product', function ($query) use ($id) {
                        $query->where('category_id', '=', $id);
                    });
                }
                $data['country'] = $country->name;
                $data['location'] = $location;
                $data['countContinent'] = $countContinent;
                $data['countCountry'] = $countCountry;
            }
        } else {
            $country_name = str_replace("-", " ", $countryName);
            $city_name = str_replace("-", " ", $cityName);
            $location = $country_name . ',' . str_replace(' ', '-', $city_name);
            $countryName = $country_name;
            $country_name = str_replace("-", " ", $countryName);
            $city = WorldCities::with('country')->where('name', 'like', '%' . $city_name . '%')->first();
            $country = WorldCountries::with('continent')->where('name', 'like', '%' . $country_name . '%')->first();
            if (is_null($city)) {
                $data['country'] = $countryName;
                $data['city'] = $cityName;
                $data['location'] = $location;
            } else {
                $countryId = isset($country->id) ? $country->id : $city->country->name;
                $countCity = SharedOffice::where('city_id', '=', $city->id)
                    ->where('state_id', '=', $countryId);
                //check filter type against city
                if ($request->type && $request->type != '' && $request->type != 'all-spaces') {
                    $id = self::getFilterType($request->type);
                    $countCity->whereHas('product', function ($query) use ($id) {
                        $query->where('category_id', '=', $id);
                    });
                }
                $countCity = $countCity->count();
                $countCountry = $this->countCountry($country->name);
                $data['country'] = isset($country->name) ? $country->name : $city->country->name;
                $data['city'] = $city->name;
                $data['countCountry'] = $countCountry;
                $data['countCity'] = $countCity;
                $data['location'] = $location;
            }
        }

        if ($request->text) {
            $sharedOffice = SharedOffice::where('office_name', 'like', '%' . $request->text . '%')->orwhere('location', 'like', '%' . $request->text . '%')->first();
            if (!is_null($sharedOffice)) {
                $replace = str_replace('-', ',', $sharedOffice->location);
                $explode = explode(',', $replace);
                $city_name = str_replace("-", " ", $explode[0]);
                $country_name = str_replace("-", " ", $explode[1]);
                $city = WorldCities::with('country')->where('name', 'like', '%' . $city_name . '%')->first();
                $country = WorldCountries::with('continent')->where('name', 'like', '%' . $country_name . '%')->first();
                if (!is_null($city)) {
                    $countryId = isset($country->id) ? $country->id : $city->country->name;
                    $countCity = SharedOffice::where('city_id', '=', $city->id)
                        ->where('state_id', '=', $countryId)
                        ->count();
                    $countCountry = $this->countCountry($country->name);
                    $data['country'] = isset($country->name) ? $country->name : $city->country->name;
                    $data['city'] = $city->name;
                    $data['countCountry'] = $countCountry;
                    $data['countCity'] = $countCity;
                    $data['location'] = $location;
                }
            }
        }
        return view("frontend.coWorks.search_new", $data);
    }

    public function countCountry($countryName) {
        $countCountry = SharedOffice::get();
        $countCountry = $countCountry->filter(function($value, $key) use ($countryName){
            $d = $value['location'];
            if(strpos($d, $countryName) !== false) {
                return $value;
            }
        });
        $countCountry = $countCountry->count();
        return $countCountry;
    }

    public function getFilterType($type)
    {
        if ($type == 'hot-desk') {
            $id = SharedOfficeProducts::CATEGORY_HOT_DESK;
        } elseif ($type == 'dedicated-desk') {
            $id = SharedOfficeProducts::CATEGORY_DEDICATED_DESK;
        } elseif ($type == 'meeting-room') {
            $id = SharedOfficeProducts::CATEGORY_MEETING_ROOM;
        }
        return $id;
    }

    public function officeDetail(Request $request, $lang, $countryName, $cityName, $officeName, $officeId)
    {
        $officeImages = SharedOfficeImages::where('office_id','=',$officeId)->get();

        $office_video = '';
        foreach ($officeImages as $images) {
            if(isset($images->video)) {
                $office_video = $images->video;
            }
        }
        $officeData = SharedOffice::where('id','=',$officeId)->with('officeProducts')->first();
        $category_data = '';
        if(isset($officeData->category_id)) {
            $category_data = SharedOfficeCategory::where('id','=',$officeData->category_id)->first();
        }
        if(isset($officeData) && count($officeData->officeProducts) > 0) {
            foreach($officeData->officeProducts as $products) {
                $name = $products->getProductName($products->category_id);
                $facilities[] = collect([
                    'id' => $products->category_id,
                    'name' => $name
                ]);
            }
        } else {
            $facilities = [];
        }
        $facilities = collect($facilities)->unique('name');
        $sharedOffice = SharedOffice::where('id', '=', $officeId)->first();
        $countryId = isset($sharedOffice->state_id) ? $sharedOffice->state_id : '';
        $country_name = WorldCountries::where('id', '=', $countryId)->select('name')->first();
        $cityId = isset($sharedOffice->city_id) ? $sharedOffice->city_id : '';
        $city = WorldCities::with('country')->where('id', '=', $cityId)->first();
        $countCountry = SharedOffice::where('state_id', '=', $countryId)
            ->where('active', '=', 1)
            ->count();
        $countCity = SharedOffice::where('city_id', '=', $cityId)
            ->where('state_id', '=', $countryId)
            ->where('active', '=', 1)
            ->count();
        if(isset($sharedOffice)) {
            $officeImage = asset($sharedOffice->image);
            $officeOwnerSettings = SharedOfficeSetting::where('staff_id', '=', $sharedOffice->staff_id)->first();
        }
        $detect = new Mobile_Detect();
        $ip = \Request::ip();
        $availableCountry = $this->checkCountry($ip);
        return view("frontend.coWorks.officeDetails", [
            'officeData' => $officeData,
            'category_data' => $category_data,
            'office_id' => $officeId,
            'officeImages' => isset($officeImages) ? $officeImages : '',
            'OwnerSettings' => isset($officeOwnerSettings) ? $officeOwnerSettings : '',
            'facilities' => $facilities,
            'countCountry' => $countCountry,
            'countCity' => $countCity,
            'city' => isset($city->name) ? $city->name : '',
            'country' => isset($country_name->name) ? $country_name->name : '',
            'officeName' => isset($sharedOffice) ? $sharedOffice->office_name :'',
            'officeImage' => isset($officeImage) ? $officeImage : 'https://www.xenren.co/images/aCowork_space.png',
            'office_video' => $office_video,
            'is_mobile' => $detect->isMobile(),
            'available_country' => isset($availableCountry) ? $availableCountry : ''
        ]);
    }
    public function checkCountry($ip){
        $ipData = userLocation($ip);
        $Country = false;
        if($ipData['status'] == true) {
            if($ipData['response']->country == 'Malaysia') {
                $Country = true;
                return collect([
                    'is_available_country' => $Country,
                    'country_name' => $ipData['response']->country,
                    'code' => $ipData['response']->country_code
                ]);
            } else {
                return collect([
                    'is_available_country' => $Country,
                    'country_name' => $ipData['response']->country,
                    'code' => $ipData['response']->country_code
                ]);
            }
        }
    }

    public function getBookingData(){
        $user_id = \Auth::guard('users')->user()->id;
        $bookings = SharedOfficeBooking::with('office.shfacilities','office.officeSettings')->where('user_id','=',$user_id)
            ->withTrashed()
            ->orderBy('id','desc')
            ->get();
        return view('frontend.coWorks.userBookingRequests.bookingRequests',[
            'bookings' => $bookings
        ]);
    }

    public function changeBookingRequest($officeId){
        $user_id = \Auth::guard('users')->user()->id;
        $booking = SharedOfficeBooking::where('office_id','=',$officeId)->with('office.shfacilities','office.officeProducts','products')->where('user_id','=',$user_id)
            ->withTrashed()
            ->first();
        return collect([
           'status' => true,
           'data' => $booking
        ]);
    }

    public function requestCancel($id){
        SharedOfficeBooking::where('id', '=', $id)->update([
            'status' => SharedOfficeBooking::STATUS_CANCELED_CLIENT_SIDE
        ]);
        SharedOfficeBooking::where('id', '=', $id)->delete();
        return collect([
            'status' => 'success',
            'message' => 'Booking canceled...!'
        ]);
    }

    public function cancelBooking($office_id,$id,$category_id){
        $sharedOffice = SharedOffice::where('id','=',$office_id)->with('owner','officeSettings')->first();
        $user = \Auth::user();
        $checkProducts = SharedOfficeProducts::where('office_id','=',$office_id)->where('category_id','=',$category_id)->first();

        if($sharedOffice->officeSettings->cancel_type == SharedOfficeSetting::CANCEL_WITH_CHARGES) {
            $balance = $user->account_balance - $checkProducts->time_price;
            $user->update([
               'account_balance' => $balance
            ]);
            // 0.25 percent to admin
            $deduct_amount = (0.25/100)*$checkProducts->time_price;
            $admin_b = Staff::where('staff_type','=',Staff::STAFF_TYPE_ADMIN)->first();
            Staff::where('staff_type','=',Staff::STAFF_TYPE_ADMIN)->update([
               'balance' => $deduct_amount
            ]);
            $admin_a = Staff::where('staff_type','=',Staff::STAFF_TYPE_ADMIN)->first();

            // funds transfer to staff
            $staff_balance_b = $sharedOffice->owner->balance + $checkProducts->time_price;
            $staff_balance = $staff_balance_b-$deduct_amount;
            $sharedOffice->owner->update([
                'balance' => $staff_balance
            ]);

            $transaction = Transaction::create([
                'name' => 'Booking Charges',
                'amount' => $checkProducts->time_price,
                'milestone_id' => 0,
                'due_date' => Carbon::now()->addYear(1),
                'comment' => $checkProducts->time_price . ' Booking Charges',
                'performed_by_balance_before' => Auth::user()->account_balance,
                'performed_by_balance_after' => $balance,
                'performed_to_balance_before' => Auth::user()->account_balance,
                'performed_to_balance_after' => $balance,
                'performed_by' => Auth::user()->id,
                'performed_to' => Auth::user()->id,
                'type' => Transaction::TYPE_BOOKING_CHARGES,
                'currency' => 'USD',
                'status' => Transaction::STATUS_APPROVED_TRANSACTION,
            ]);

            // admin
            $transaction = Transaction::create([
                'name' => 'Booking Charges',
                'amount' => $deduct_amount,
                'milestone_id' => 0,
                'due_date' => Carbon::now()->addYear(1),
                'comment' => $checkProducts->time_price . ' Transaction Fee 2.5%',
                'performed_by_balance_before' => $admin_b->balance,
                'performed_by_balance_after' => $admin_a->balance,
                'performed_to_balance_before' => $admin_b->balance,
                'performed_to_balance_after' => $admin_a->balance,
                'performed_by' => $admin_a,
                'performed_to' => Auth::user()->id,
                'type' => Transaction::TYPE_BOOKING_CHARGES_FEE,
                'currency' => 'USD',
                'status' => Transaction::STATUS_APPROVED_TRANSACTION,
            ]);
        }



        SharedOfficeBooking::where('id','=',$id)->update([
            'status' => SharedOfficeBooking::STATUS_CANCELED
        ]);
        SharedOfficeBooking::where('id','=',$id)->delete();
        $checkProducts->update([
           'status_id' => SharedOfficeProducts::STATUS_EMPTY
        ]);
        return collect([
           'status' => true,
           'message' => 'seat cancel success'
        ]);
    }

    public function bookingDetails($lang,$id){
        $getDetails = SharedOfficeBooking::with('office.shfacilities','products')->where('id','=',$id)
            ->withTrashed()
            ->first();
        return view('frontend.coWorks.userBookingRequests.detail',[
            'booking_detail' => $getDetails
        ]);
    }

    public function index()
    {
        // $offices = SharedOffice::with(['shfacilities', 'country', 'city'])->get();
        $detect = new Mobile_Detect();
        return view("frontend.coWorks.index", [
            // 'offices' => $offices,
            'is_mobile' => $detect->isMobile()
        ]);
    }

    public function mapJson()
    {
        $map = file_get_contents(public_path('jsons/sharedOffices.json'));
        $map = json_decode($map);
        return $map;
    }

    public function mapJsonApp()
    {
        return \Cache::get('sh_office_hp', function () {
            $map = file_get_contents(public_path('jsons/sharedOffices.json'));
            $map = json_decode($map);
            return $map;
        });
    }

    public function signIn()
    {
        return view("frontend.coWorks.login");
    }

    public function resetPassword()
    {
        return view("frontend.coWorks.forgotPassword");
    }

    public function forgotPassword(Request $request)
    {
        $getEmail = Staff::where('email', '=', $request->email)->first();
        if (is_null($getEmail)) {
            return collect([
                'error' => 'email does not exit please enter correct email'
            ]);
        } else {
            $lang = getLocale();
            $link = route('sharedoffice.setPassword',[$lang,$getEmail->id]);
            $senderEmail = "support@xenren.co";
            $senderName = 'Xenren Confirmation Message';
            $receiverEmail = $getEmail->email;

            $request->session()->put('id', $getEmail->id);
            $request->session()->put('email', $getEmail->email);

            Mail::send('mails.setStaffPasswordConfirmationMail', array('email' => $getEmail->email, 'link' => $link), function ($message) use ($senderEmail, $senderName, $receiverEmail) {
                $message->from($senderEmail, $senderName);
                $message->to($receiverEmail)
                    ->subject('reset password confirmation email');
            });
            return collect([
                'success' => 'we will send you email check you email',
            ]);
        }
    }

    public function setPassword($lang, $id)
    {
        $staff = Staff::where('id','=',$id)->first();
        return view("frontend.coWorks.setPassword", [
            'id' => $staff->id,
            'email' => $staff->email
        ]);
    }

    public function setPasswordPost(resetPasswordRequest $request)
    {
        $update = Staff::where('id', '=', $request->id)->update([
            'password' => bcrypt($request->password),
        ]);

        return collect([
            'success' => 'password updated successfully'
        ]);
    }

    public function signUp()
    {
        return view("frontend.coWorks.register");
    }

    public function message(Request $request, SharedOffice $shareOffice)
    {
//        dispatch(new SharedOfficeContact($request->all()));
        return collect([
            'status' => 'success',
            'data' => $request->all(),
            'message' => 'Message sent..!'
        ]);
    }

    public function sharedOfficeRequestImages(Request $request)
    {
    }

    public function sharedOfficeRequest(Request $request)
    {
        $check = Staff::where('email', '=', $request['email'])->first();
        if (!is_null($check)) {
            return collect([
                'status' => false,
                'message' => 'Email Already Exist'
            ]);
        }
        if ($request->hasFile('file_name')) {
            $fileName = $request->file('file_name')->getClientOriginalName();
            $path = 'uploads/listSpaceImages';
            $save = $request->file('file_name')->move($path, $fileName);
        }

        $sharedOfficeRequest = SharedOfficeRequest::create([
            'company_name' => $request->company_name,
            'location' => $request->location,
            'full_name' => $request->full_name,
            'phone_number' => $request->phone_number,
            'email' => $request->email,
            'image' => $fileName,
            'status' => SharedOfficeRequest::SHARED_OFFICE_REQUEST_STATUS,
            'is_email_approved' => SharedOfficeRequest::SHARED_OFFICE_PERSON_EMAIL_UN_VERIFIED_STATUS,
        ]);
        for ($i = 0; $i < $request->counter; $i++) {
            if ($request->hasFile('image_' . $i)) {
                $file_Name = $request->file('image_' . $i)->getClientOriginalName();
                $path = 'uploads/listSpaceImages';
                $save = $request->file('image_' . $i)->move($path, $file_Name);
            }
            SharedOfficeRequestImage::create([
                'shared_office_request_id' => $sharedOfficeRequest->id,
                'images' => $file_Name
            ]);
        }
        $this->createStaff($request->all());
        $this->sendEmailAdmin($sharedOfficeRequest->id, $fileName, $request->all());
        $this->sendEmailCreator($sharedOfficeRequest->id, $fileName, $request->all());
    }

    public function createStaff($request)
    {
        \Log::info('CREATE STAFF NOW');
        \Log::info('CREATE STAFF NOW');
        \Log::info('CREATE STAFF NOW');
        \Log::info('CREATE STAFF NOW');
        \Log::info('CREATE STAFF NOW');
        \Log::info('CREATE STAFF NOW');
        \Log::info('CREATE STAFF NOW');
        \Log::info($request['password']);

        $staff = new Staff();
        $staff->username = $request['full_name'];
        $staff->name = $request['full_name'];
        $staff->email = $request['email'];
        $staff->password = ($request['password']);
        $staff->staff_type = Staff::STAFF_TYPE_STAFF;
        $staff->title = Staff::Shared_OFFICE_TITLE;
        $staff->permission_group_id = Staff::STAFF_TYPE_ADMIN;
        $staff->img_avatar = asset('images/default-m.jpg');
        $staff->save();
    }

    public function sendEmailAdmin($requestId, $fileName, $request)
    {
        $senderEmail = "support@xenren.co";
        Mail::send(
            'mails.sendEmailXenrenCoListSpace',
            array(
                'company_name' => $request['company_name'],
                'location' => $request['location'],
                'full_name' => $request['full_name'],
                'phone_number' => $request['phone_number'],
                'email' => $request['email'],
                'image' => $fileName
            ),
            function ($message) use ($senderEmail) {
                $message->from($senderEmail);
                $message->to($senderEmail)
                    ->subject(trans('common.list_your_space'));
            }
        );
    }

    public function sendEmailCreator($requestId, $fileName, $request)
    {
        $creatorEmail = $request['email'];
        $link = url('/office/creator/email-verification', [$requestId]);
        Mail::send(
            'mails.sendEmailXenrenCoCreatorListSpace',
            array(
                'company_name' => $request['company_name'],
                'location' => $request['location'],
                'full_name' => $request['full_name'],
                'phone_number' => $request['phone_number'],
                'email' => $request['email'],
                'password' => $request['password'],
                'link' => $link,
                'image' => $fileName
            ),
            function ($message) use ($creatorEmail) {
                $message->from('support@xenren.co');
                $message->to($creatorEmail)
                    ->subject(trans('common.list_your_space'));
            }
        );
    }

    public function verifyCreatorEmail($id)
    {
        SharedOfficeRequest::where('id', '=', $id)->update([
            'status' => SharedOfficeRequest::SHARED_OFFICE_PERSON_EMAIL_VERIFIED_STATUS
        ]);
        return redirect('/')->with('verified', 'Email Verified Success');
    }

    public function getSharedOfficeRating($office_id)
    {
        $check = SharedOfficeRating::where('office_id', '=', $office_id)->where('user_id', '=', \Auth::user()->id)->first();
        return $check;
    }

    public function sharedOfficeCoworkCheck($userId, $officeId)
    {
        $usedBefore = SharedOfficeFinance::where('user_id', '=', $userId)->where('office_id', '=', $officeId)->first();
        if (is_null($usedBefore)) {
            return collect([
                'status' => 'true',
                'message' => 'You can\'t post review if you\'ve never joined the office before...!'
            ]);
        }
    }

    public function sharedOfficeRating(Request $request)
    {
        $usedBefore = SharedOfficeFinance::where('user_id', '=', $request->user_id)->where('office_id', '=', $request->office_id)->first();
        if (is_null($usedBefore)) {
            return collect([
                'status' => 'error',
                'message' => 'You can\'t post review if you\'ve never joined the office before...!'
            ]);
        }
        $check = SharedOfficeRating::where('user_id', '=', $request->user_id)->where('office_id', '=', $request->office_id)->first();
        if (is_null($check)) {
            SharedOfficeRating::create([
                'office_id' => $request->office_id,
                'user_id' => $request->user_id,
                'rate' => $request->reviewsStars,
                'comment' => $request->comment,
                'helpful_count' => 0
            ]);
        } else {
            SharedOfficeRating::where('user_id', '=', $request->user_id)->where('office_id', '=', $request->office_id)->update([
                'office_id' => $request->office_id,
                'user_id' => $request->user_id,
                'rate' => isset($request->reviewsStars) ? $request->reviewsStars : $check->rate,
                'comment' => isset($request->comment) ? $request->comment : $check->comment,
                'helpful_count' => 0
            ]);
        }
        return collect([
            'status' => 'success',
        ]);
    }
}
