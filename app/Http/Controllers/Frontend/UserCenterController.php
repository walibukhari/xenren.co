<?php

namespace App\Http\Controllers\Frontend;

use App\Constants;
use App\Jobs\AutoSendEmail;
use App\Models\CountryLanguage;
use App\Models\FaceDetect as Face;
use App\Models\Portfolio;
use App\Models\Review;
use App\Models\Skill;
use App\Models\SkillKeyword;
use App\Models\TransactionDetail;
use App\Models\UploadFile;
use App\Models\User;
use App\Models\UserIdentity;
use App\Models\UserPaymentMethod;
use Carbon;
use App\Http\Controllers\FrontendController;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use App\Models\UserSkill;
use App\Models\JobPosition;
use App\Models\UserSkillComment;
use Auth;
use App\Models\WorldCountries;
use Illuminate\Support\Facades\Config;
use Lang;

class UserCenterController extends FrontendController
{
    use DispatchesJobs;

    public function afterregister()
    {
//        $user_id = \Auth::user()->id;
//        $email = \Auth::user()->email;
//        $name = \Auth::user()->name;
//        $this->dispatch(new AutoSendEmail($user_id,$email,$name));
        return view("auth.afterRegister");
    }

	public function addPayPal(Request $request)
	{
		$data = UserPaymentMethod::where('user_id' , '=' , \Auth::user()->id)->where('payment_method' ,'=', TransactionDetail::PAYMENT_METHOD_PAYPAL)->first();
		if(is_null($data)){
			UserPaymentMethod::create([
				'user_id' => \Auth::user()->id,
				'payment_method' => TransactionDetail::PAYMENT_METHOD_PAYPAL,
				'name' => $request->Name,
				'email' => $request->Email,
				'phone' => $request->Phone,
			]);
		} else {
			UserPaymentMethod::where('user_id' , '=' , \Auth::user()->id)->where('payment_method' ,'=', TransactionDetail::PAYMENT_METHOD_PAYPAL)
				->update([
				'user_id' => \Auth::user()->id,
				'payment_method' => TransactionDetail::PAYMENT_METHOD_PAYPAL,
				'name' => $request->Name,
				'email' => $request->Email,
				'phone' => $request->Phone,
			]);
		}
		return redirect()->back();
	}

	public function index(Request $request)
	{
		try {
            $user = \Auth::guard('users')->user();
            if(isset($request->job_position)) {
                $user_job_position_id = $request->job_position;
            } else {
                $user_job_position_id = $user->job_position_id;
            }
			$userSkills = UserSkill::with('skill')->where('user_id', $user->id)->get();
			$userSkillsKeywords = SkillKeyword::where('user_id', $user->id)->get();

			$jobPosition = JobPosition::GetJobPosition()->where('id', $user->job_position_id)->first();
			$adminReviews = UserSkillComment::with(['files', 'staff', 'skills', 'skills.skill'])->where('user_id', '=', $user->id);

			$languages = array();
			foreach (CountryLanguage::all() as $lang) {
				if (Lang::locale() == 'cn') {
					$name = (($lang->language->name_cn != null) || !empty($lang->language->name_cn)) ? $lang->language->name_cn : $lang->language->name;
				} else {
					$name = $lang->language->name;
				}

				$language = array(
					'id' => $lang->id,
					'text' => $name,
					'name' => $name,
					'code' => isset($lang->country->code) ? $lang->country->code : 'cn',
				);
                array_push($languages,$language);
            }

            $langData = collect($languages)->unique('name');
			$unique = array();
			foreach($langData as $lang) {
                if (Lang::locale() == 'cn') {
                    $name = $lang['name_cn'];
                } else {
                    $name = $lang['name'];
                }
                $data = array(
                    'id' => $lang['id'],
                    'text' => $name,
                    'name' => $name,
                    'code' => isset($lang->country->code) ? $lang->country->code : 'cn',
                );
                array_push($unique,$data);
            }
            $langList = json_encode($unique);

			$skillList = Skill::getSearchSkillData(Skill::SEARCH_TYPE_WHITE);

			$userStatus = User::getStatusLists();

			$userIdentity = UserIdentity::where('user_id', $user->id)
				->orderBy('id', 'desc')
				->first();

			$reviews = Review::with('reviewer_user', 'reviewer_staff', 'reviewee_user', 'project')
				->where('reviewee_user_id', Auth::id())
				->get();
			$reviewInfo = $user->getReviewInfo();
			$job_positions = JobPosition::all();
			$portfolios = Portfolio::with('files')
				->where('user_id', \Auth::guard('users')->user()->id)
				->where('job_position_id', $user_job_position_id)
				->get();
			$uploadFiles = UploadFile::where('user_id', \Auth::guard('users')->user()->id)
				->where('type', UploadFile::TYPE_PORTFOLIO)
				->get();


			$result = [];
			foreach ($uploadFiles as $file) {
				$obj['name'] = $file->filename;
				$obj['size'] = filesize(Config::get('upload-files.location') . $file->filename);
				$obj['type'] = $file->getFileType();
				$result[] = $obj;
			}

			$jsonUploadFile = "{}";
			if (count($uploadFiles) != 0) {
				$jsonUploadFile = json_encode($result);
			}

			$position = JobPosition::all();

			$getCurrencyCode = WorldCountries::orderBy('currency_code', 'asc')->get();
            $getCurrencyCode = $getCurrencyCode->unique('currency_code')->all();
			$getCode = array();
			foreach ($getCurrencyCode as $code){
			        array_push($getCode,$code);
            }
            $faceDetect = Face::where('user_id','=',\Auth::guard('users')->user()->id)->first();
            $jobData = JobPosition::where('id', '=', $user_job_position_id)->first();
            $name = isset($jobData) ? $jobData->name_en : '';
			return view('frontend.userCenter.mock-index', [
                'faceDetect' => isset($faceDetect) ? $faceDetect->face_status  : '',
                'user' => $user,
				'userSkills' => $userSkills,
				'userSkillsKeywords' => $userSkillsKeywords,
				'jobPosition' => $jobPosition,
				'userStatus' => $userStatus,
				'skillList' => $skillList,
				'adminReviews' => $adminReviews->get(),
				'langList' => $langList,
				'userIdentity' => $userIdentity,
				'reviews' => $reviews,
				'reviewInfo' => $reviewInfo,
				'job_positions' => $job_positions,
				'portfolios' => $portfolios,
				'jsonUploadFile' => $jsonUploadFile,
                'position' => $position,
                'currency_code' => $getCode,
                'user_job_position_id' => $user_job_position_id,
                'name' => $name
			]);
		} catch (\Exception $e) {
			dd($e);
		}
	}

	public function getCurrency(Request $request)
    {
        $curl = curl_init();
//        https://free.currconv.com/api/v7/convert?q=USD_PHP&compact=ultra&apiKey=5936149c17cbe2186434
        $q = strtoupper($request->from) ."_".strtoupper($request->to);
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://free.currconv.com/api/v7/convert?q=".$q."&compact=ultra&apiKey=5936149c17cbe2186434",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Postman-Token: b40806c9-4c2b-4139-9b6e-d8690b520148",
                "cache-control: no-cache"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }

	public function newIndex()
	{
		return view('frontend.userCenter.newIndex');
	}

	public function closeNotice()
	{
		$user = \Auth::guard('users')->user();
		$user->close_notice = $user->close_notice + 1;
		$user->save();

		return makeJSONResponse(true, 'Successful');
	}

	public function updatePositions($id)
    {
        $user_id = \Auth::user()->id;
        $update = User::where('id','=',$user_id)->update([
           'job_position_id' => $id
        ]);
        return response()->json($update);
    }

	public function userCenterHover()
	{
		return view('frontend.userCenter.newIndexHover');
	}

	public function getUsers()
    {
        return User::where('id','=',\Auth::user()->id)->first();
    }
}
