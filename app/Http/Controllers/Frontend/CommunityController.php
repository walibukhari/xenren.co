<?php

namespace App\Http\Controllers\Frontend;

use App\Models\CommunityDiscussion;
use App\Models\CommunityLikes;
use App\Models\CommunityRead;
use App\Models\CommunityReply;
use App\Models\CommunitySubscribe;
use App\Models\CommunityTopicLikes;
use App\Models\ReportCoummunityTopic;
use App\Models\Topic;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class CommunityController extends Controller
{

    public function reportCommunityMessage($lang,$id)
    {
        if(!isset($id)) {
            return redirect($lang,'/community/home');
        }
        $discussion = CommunityDiscussion::where('id','=',$id)->with('user','topic')->first();
        return view('frontend.community.reportCommunityMessage',[
            'data' => $discussion
        ]);
    }

    public function reportCommunityReplyMessage($lang,$id)
    {
        if(!isset($id)) {
            return redirect($lang,'/community/home');
        }
        $replyDiscussion = CommunityReply::where('id','=',$id)->with('user','topic','discussion')->first();
        return view('frontend.community.reportCommunityReplyMessage',[
            'data' => $replyDiscussion
        ]);
    }

    public function reportToAdmin(Request $request) {
        if(isset($request->replay_id)) {
            $check = ReportCoummunityTopic::where('replay_id', '=', $request->replay_id)->where('report_to', '=', $request->report_to)->where('report_by', '=', $request->report_by)->first();
            if (is_null($check)) {
                ReportCoummunityTopic::create([
                    'report_to' => $request->report_to,
                    'report_by' => $request->report_by,
                    'topic_name' => $request->topic_name,
                    'discussion_name' => $request->discussion_name,
                    'discussion_id' => $request->discussion_id,
                    'comment' => $request->description,
                    'replay_id' => $request->replay_id
                ]);
                $message = 'Report Send To Admin Successfully';
            } else {
                $message = 'Already Reported';
            }
        } else {
            $check = ReportCoummunityTopic::where('replay_id', '=', $request->discussion_id)->where('report_to', '=', $request->report_to)->where('report_by', '=', $request->report_by)->first();
            if (is_null($check)) {
                ReportCoummunityTopic::create([
                    'report_to' => $request->report_to,
                    'report_by' => $request->report_by,
                    'topic_name' => $request->topic_name,
                    'discussion_name' => $request->discussion_name,
                    'discussion_id' => $request->discussion_id,
                    'comment' => $request->description,
                    'replay_id' => isset($request->replay_id) ? $request->replay_id : $request->discussion_id
                ]);
                $message = 'Report Send To Admin Successfully';
            } else {
                $message = 'Already Reported';
            }

        }

        return back()->with('success',$message);
    }

    public function index()
    {
        $topics = Topic::all();
        return view('frontend.community.index', [
            'topics' => $topics
        ]);
    }

    public function xenrenOffical($lang, $val, $id)
    {
        if(\Auth::user()) {
            $read = '';
            $single = '';
            $getDiscussion = CommunityDiscussion::where('topic_type', '=', Topic::TOPIC_TYPE_OFFICAL)->where('topic_id', '=', $id)->with('user', 'topicLikes', 'topicReplies', 'topic.read', 'readD')->get();
            foreach ($getDiscussion as $k => $d) {
                /* read all or mark as new all  read as feature individually user */
                if (isset($d->topic->read)) {
                    $read = $d->topic->read->where('user_id', '=', \Auth::user()->id)->first();
                }
                if (isset($d->readD)) {
                    $single = $d->readD;
                }
            }
            return view('frontend.community.offical.index', [
                'topic_id' => $id,
                'lang' => $lang,
                'topic_name' => $val,
                'getDiscussion' => $getDiscussion,
                'read' => $read,
                'single' => $single,
            ]);
        } else {
            return redirect('/login');
        }
    }

    public function communityDiscussion($lang, $val, $id)
    {
        if(\Auth::user()) {
            $read = '';
            $single = '';
            $getDiscussion = CommunityDiscussion::where('topic_type', '=', Topic::TOPIC_TYPE_DISCUSSION)->where('topic_id', '=', $id)->with('user', 'topicLikes', 'topicReplies', 'topic.read', 'readD')->get();
            foreach ($getDiscussion as $k => $d) {
                /* read all or mark as new all  read as feature individually user */
                if (isset($d->topic->read)) {
                    $read = $d->topic->read->where('user_id', '=', \Auth::user()->id)->first();
                }
                if (isset($d->readD)) {
                    $single = $d->readD;
                }
            }
            return view('frontend.community.discussion.index', [
                'topic_id' => $id,
                'lang' => $lang,
                'topic_name' => $val,
                'getDiscussion' => $getDiscussion,
                'read' => $read,
                'single' => $single,
            ]);
        } else {
            return redirect('/login');
        }
    }

    public function communityForums($lang, $topic_name)
    {
        $getTopic = Topic::where('topic_name', '=', cleanName($topic_name))->first();
        $topic_type = Topic::getTopicType($getTopic->topic_type);
        return view('frontend.community.fourm.fourm', [
            'topic_type' => $topic_type,
            'type' => $getTopic->topic_type,
            'topic_name' => $topic_name,
            'topic_id' => $getTopic->id,
            'lang' => $lang
        ]);
    }

    public function submitQuestions(Request $request)
    {
        if($request->description == '') {
            return back()->with('error_message','Description Field is required');
        }
        if($request->name == '') {
            return back()->with('error_message','Name Field is required');
        }
        if ($request->hasFile('file')) {
            dd('file');
        }
        if ($request->hasFile('filename')) {
            dd('filename');
        }
        if ($request->notify_me == 'on') {
            $notify_me = CommunityDiscussion::NOTIFY_ME;
        } else {
            $notify_me = CommunityDiscussion::NOT_NOTIFY_ME;
        }
        $communityDiscussion = CommunityDiscussion::create([
            'name' => $request->name,
            'description' => $request->description,
            'notify_me' => $notify_me,
            'topic_id' => $request->topic_id,
            'topic_type' => $request->topic_type,
            'user_id' => \Auth::user()->id
        ]);
        CommunityTopicLikes::create([
            'topic_id' => $communityDiscussion->id,
            'likes' => 0,
            'user_id' => \Auth::user()->id
        ]);
        $userId = \Auth::user()->id;
        $topic_id = $request->topic_id;
        $job = (new \App\Jobs\SendPushyNotifications($userId, $topic_id))->onQueue(config('jobs-queue-names.SendPushyNotifications'));
        return back()->with('success', 'Question Added Successfully');
    }

    public function communityReply($lang, $topic_type, $topic_name, $topic_id, $d_id)
    {
        if(!isset($d_id)) {
            return redirect($lang.'/community/home');
        }
        if(!\Auth::check()) {
            return redirect($lang.'/login');
        }
        $getTopic = Topic::where('id', '=', $topic_id)->first();
        $topic_type = Topic::getTopicType($getTopic->topic_type);
        $topic_type = str_replace(' ', '-', $topic_type);
        $check = CommunityRead::where('user_id','=',\Auth::user()->id)->where('topic_id','=',$topic_id)->where('d_id','=',$d_id)->first();
        if(is_null($check)) {
            CommunityRead::create([
                'topic_id' => $topic_id,
                'd_id' => $d_id,
                'user_id' => \Auth::user()->id,
                'read_as' => CommunityRead::STATUS_READ_AS,
                'status' => CommunityRead::STATUS_READ_AS_SINGLE_TOPIC,
                'read_all' => '',
                'un_read_all' => ''
            ]);
        } else {
            CommunityRead::where('user_id','=',\Auth::user()->id)->where('topic_id','=',$topic_id)->update([
                'read_as' => CommunityRead::STATUS_READ_AS,
                'status' => CommunityRead::STATUS_READ_AS_SINGLE_TOPIC,
                'read_all' => '',
                'un_read_all' => ''
            ]);
        }
        $getDiscussion = CommunityDiscussion::where('id', '=', $d_id)->with('user', 'topicLikes','subscribe')->first();
        $checkLike = CommunityTopicLikes::where('user_id', '=', \Auth::user()->id)->where('topic_id', '=', $getDiscussion->id)->first();
        $getReplies = CommunityReply::where('discuss_id', '=', $getDiscussion->id)->with('like', 'user')->get();
        return view('frontend.community.reply.index', [
            'lang' => $lang,
            'topic_name' => $getTopic->topic_name,
            'topic_type' => $topic_type,
            'topic_id' => $topic_id,
            'getDiscussion' => $getDiscussion,
            'getReplies' => $getReplies,
            'checkLike' => $checkLike,
        ]);
    }

    public function communityReplyForums($lang, $topic_name, $topic_id, $d_id, $type , $r_id)
    {
        if ($type == 'd_comment') {
            $replyTo = CommunityDiscussion::where('id', '=', $d_id)->with('topicLikes', 'user')->first();
            $checkLike = CommunityTopicLikes::where('user_id', '=', \Auth::user()->id)->where('topic_id', '=', $d_id)->first();
        } else {
            $replyTo = CommunityReply::where('id', '=', $d_id)->with('like', 'user')->first();
            $checkLike = CommunityLikes::where('user_id', '=', \Auth::user()->id)->where('reply_id', '=', $d_id)->first();
        }
        return view('frontend.community.reply.replay', [
            'lang' => $lang,
            'd_name' => $topic_name,
            'topic_id' => $topic_id,
            'd_id' => $d_id,
            'replyTo' => $replyTo,
            'type' => $type,
            'checkLike' => $checkLike,
            'r_id' => $r_id
        ]);
    }

    public function sendNotifyEmail($discussion, $replyMessage)
    {
        //sender email
        $senderEmail = "support@xenren.co";
        $senderName = "Support Team";
        $user = User::where('id', '=', $discussion->user_id)->first();
        $lang = getLocale();
        $communityLink = route('frontend.Community',[$lang]);

        Mail::send('mails.notify_email', array('link' => $communityLink ,'comment' => $replyMessage->comment, 'name' => \Auth::user()->name), function ($message) use ($senderEmail, $senderName, $user) {
            $message->from($senderEmail, $senderName);
            $message->to($user->email, $user->first_name . ' ' . $user->last_name)
                ->subject(trans('common.edit_email'));
        });
    }

    public function submitAnswer(Request $request)
    {
        $topic = Topic::where('id', '=', $request->topic_id)->first();
        $getDiscussion = CommunityDiscussion::where('id', '=', $request->d_id)->first();
        $name = str_replace(' ','-',$getDiscussion->name);
        $url = '/'.getLocale().'/community/'.$request->d_name.'/'.$name.'/t-id/'.$request->topic_id.'/d-id/'.$request->d_id;
        $communityReply = CommunityReply::create([
            'topic_id' => $topic->id,
            'discuss_id' => $request->d_id,
            'topic_type' => $topic->topic_type,
            'user_id' => \Auth::user()->id,
            'comment' => $request->description,
            'likes' => 0,
            'notify_me' => $request->notify_me,
            'reply_id' => $request->r_id
        ]);
        if (isset($getDiscussion) && $getDiscussion->notify_me == CommunityDiscussion::NOTIFY_ME) {
            $this->sendNotifyEmail($getDiscussion, $communityReply);
        }
        CommunityLikes::create([
            'reply_id' => $communityReply->id,
            'likes' => 0,
            'user_id' => \Auth::user()->id
        ]);
        return redirect($url);
    }

    public function submitLikes($reply_id)
    {
        $checkLikes = CommunityLikes::where('user_id', '=', \Auth::user()->id)->where('reply_id', '=', $reply_id)->first();
        if (is_null($checkLikes)) {
            CommunityLikes::create([
                'reply_id' => $reply_id,
                'user_id' => \Auth::user()->id,
                'likes' => 1
            ]);
            return back();
        } else {
            CommunityLikes::where('user_id', '=', \Auth::user()->id)->update([
                'likes' => 1
            ]);
            return back();
        }
    }

    public function topicLikes($topic_id)
    {
        $checkLikes = CommunityTopicLikes::where('user_id', '=', \Auth::user()->id)->where('topic_id', '=', $topic_id)->first();
        if (is_null($checkLikes)) {
            CommunityTopicLikes::create([
                'topic_id' => $topic_id,
                'user_id' => \Auth::user()->id,
                'likes' => 1
            ]);
            return back();
        } else {
            CommunityTopicLikes::where('user_id', '=', \Auth::user()->id)->update([
                'likes' => 1
            ]);
            return back();
        }
    }

    public function feature($id, $type)
    {
        $topic = CommunityDiscussion::where('topic_id', '=', $id)->get();
        $checkExists = CommunityRead::where('user_id','=',\Auth::user()->id)->where('topic_id','=',$id)->first();
        if ($type == CommunityDiscussion::MARK_AS_ALL_NEW) {
            $feature_type = CommunityDiscussion::MARK_AS_ALL_NEW;
            if(!is_null($checkExists)) {
                foreach ($topic as $k => $readAs) {
                    if($k == 0) {
                        CommunityRead::where('user_id', '=', \Auth::user()->id)->update([
                            'user_id' => \Auth::user()->id,
                            'topic_id' => $readAs->topic_id,
                            'read_all' => $feature_type,
                            'read_as' => '',
                            'status' => '',
                            'd_id' => ''
                        ]);
                    }
                }
            } else {
                foreach ($topic as $k => $readAs) {
                    if($k == 0) {
                        CommunityRead::create([
                            'user_id' => \Auth::user()->id,
                            'topic_id' => $readAs->topic_id,
                            'un_read_all' => $feature_type
                        ]);
                    }
                }
            }
        }
        if ($type == CommunityDiscussion::MARK_AS_ALL_READ) {
            $feature_type = CommunityDiscussion::MARK_AS_ALL_READ;
            if(!is_null($checkExists)) {
                foreach ($topic as $k => $readAs) {
                    if($k == 0) {
                        CommunityRead::where('user_id', '=', \Auth::user()->id)->update([
                            'user_id' => \Auth::user()->id,
                            'topic_id' => $readAs->topic_id,
                            'read_all' => $feature_type,
                            'read_as' => '',
                            'status' => '',
                            'd_id' => ''
                        ]);
                    }
                }
            } else {
                foreach ($topic as $k => $readAs) {
                    if($k == 0) {
                        CommunityRead::create([
                            'user_id' => \Auth::user()->id,
                            'topic_id' => $readAs->topic_id,
                            'read_all' => $feature_type
                        ]);
                    }
                }
            }
        }
        if($type == CommunityDiscussion::SUBSCRIBE_RSS_FEED) {
            $feature_type = CommunityDiscussion::SUBSCRIBE_RSS_FEED;
            $check = CommunitySubscribe::where('user_id','=',\Auth::user()->id)->where('topic_id','=',$id)->where('subscribe','=',$feature_type)->first();
            if(is_null($check)) {
                CommunitySubscribe::create([
                    'user_id' => \Auth::user()->id,
                    'topic_id' => $id,
                    'discussion_id' => 0,
                    'subscribe' => $feature_type
                ]);
                $message = 'Subscribe Rss Feed Successfully';
            } else {
                $message = 'You Already Subscribe Rss Feed Successfully';
            }
        }
        return back();
    }

    public function replyFeatureS($id, $type)
    {
        $topic_d = CommunityDiscussion::where('id', '=', $id)->first();
        if ($type == CommunityDiscussion::BOOKMARK) {
            $feature_type = CommunityDiscussion::BOOKMARK;
            CommunityDiscussion::where('id', '=', $topic_d->id)->update([
                'book_mark' => $feature_type
            ]);
            return back();
        }
        if ($type == CommunityDiscussion::UN_BOOKMARK) {
            $feature_type = CommunityDiscussion::UN_BOOKMARK;
            CommunityDiscussion::where('id', '=', $topic_d->id)->update([
                'book_mark' => $feature_type
            ]);
            return back();
        }
        if ($type == CommunityDiscussion::SUBSCRIBE) {
            $feature_type = CommunityDiscussion::SUBSCRIBE;
            $checkUserSubs = CommunitySubscribe::where('user_id','=',\Auth::user()->id)->where('topic_id','=',$topic_d->topic_id)->where('discussion_id','=',$topic_d->id)->first();
            if(isset($checkUserSubs) && $checkUserSubs->subscribe == CommunityDiscussion::UN_SUBSCRIBE) {
                $checkUserSubs->update([
                    'subscribe' => $feature_type
                ]);
                $message = 'Subscribe This Topic Successfully';
            } else {
                if (is_null($checkUserSubs)) {
                    CommunitySubscribe::create([
                        'user_id' => \Auth::user()->id,
                        'topic_id' => $topic_d->topic_id,
                        'discussion_id' => $topic_d->id,
                        'subscribe' => $feature_type
                    ]);
                    $message = 'Subscribe This Topic Successfully';
                } else {
                    $message = 'Already Subscribe This Topic';
                }
            }
            return back()->with('success_message',$message);
        }
        if($type == CommunityDiscussion::UN_SUBSCRIBE) {
            $feature_type = CommunityDiscussion::UN_SUBSCRIBE;
            CommunitySubscribe::where('user_id','=',\Auth::user()->id)->where('topic_id','=',$topic_d->topic_id)->where('discussion_id','=',$topic_d->id)->update([
                'subscribe' => $feature_type
            ]);
            $message = 'Un Subscribe This Topic Successfully';
            return back()->with('success_message',$message);
        }
        if (isset($feature_type)) {
            CommunityDiscussion::where('id', '=', $topic_d->id)->update([
                'read_as' => $feature_type
            ]);
        }
        return back();
    }

    public function emailToFriend($lang, $id)
    {
        $topic_d = CommunityDiscussion::where('id', '=', $id)->first();
        if(is_null($topic_d)){
            return back();
        }
        $user = User::where('id', '=', $topic_d->user_id)->first();
        return view('frontend.community.email_to_friend', [
            'user' => $user,
        ]);
    }

    public function inviteEmailToFriend($lang, $id)
    {
        $topic_d = CommunityDiscussion::where('topic_id', '=', $id)->first();
        $user = User::where('id', '=', $topic_d->user_id)->first();
        return view('frontend.community.invite_to_friend', [
            'user' => $user,
        ]);
    }

    public function sendEmailToFriend(Request $request)
    {
        //sender email
        $senderEmail = "support@xenren.co";
        $senderName = "Support Team";
        $friend_email = $request->friend_email;
        $subject = $request->subject;
        $lang = getLocale();
        $communityLink = route('frontend.Community',[$lang]);
        $username = \Auth::user()->name;
        Mail::send('mails.notify_email', array('link' => $communityLink, 'comment' => $request->description, 'name' => $username, 'send_to' => $request->email), function ($message) use ($senderEmail, $senderName, $friend_email, $subject) {
            $message->from($senderEmail, $senderName);
            $message->to($friend_email)
                ->subject($subject);
        });

        return back()->with('success', 'Email Send Successfully');
    }

    public function inviteEmailToFriendPost(Request $request)
    {
        //sender email
        $senderEmail = "support@xenren.co";
        $senderName = "Support Team";
        $friend_email = $request->friend_email;
        $subject = $request->subject;
        $lang = getLocale();
        $communityLink = route('frontend.Community',[$lang]);
        $username = \Auth::user()->name;
        Mail::send('mails.invite_email_community', array('link' => $communityLink,'comment' => $request->description, 'name' => $username, 'send_to' => $friend_email), function ($message) use ($senderEmail, $senderName, $friend_email, $subject) {
            $message->from($senderEmail, $senderName);
            $message->to($friend_email)
                ->subject($subject);
        });

        return back()->with('success', 'Email Send Successfully');
    }
}
