<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\FrontendController;
use App\Models\FavouriteFreelancer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class FavouriteFreelancerController extends FrontendController
{
    public function index()
    {
        $favouriteFreelancers = FavouriteFreelancer::where('user_id', \Auth::id())
            ->paginate(10);

        $totalCount = FavouriteFreelancer::where('user_id', \Auth::id())
            ->count();

        return view('frontend.favouriteFreelancers.index', [
            'favouriteFreelancers' => $favouriteFreelancers,
            'totalCount' => $totalCount
        ]);
    }


    public function addFavouriteFreelancer(Request $request)
    {
        $freelancerId = $request->get('freelancerId');

        //check whether exist or not
        $count = FavouriteFreelancer::where('user_id', Auth::id())
            ->where('freelancer_id', $freelancerId)
            ->count();
        if ($count >= 1) {
            return response()->json([
                'status' => 'Error',
                'message' => trans('common.freelancer_already_exist_in_list')
            ]);
        }

        $favouriteFreelancer = new FavouriteFreelancer();
        $favouriteFreelancer->user_id = Auth::id();
        $favouriteFreelancer->freelancer_id = $freelancerId;
        $favouriteFreelancer->save();

        return response()->json([
            'status' => 'OK',
            'message' => trans('common.freelancer_added_into_list')
        ]);
    }

    public function removeFavouriteFreelancer(Request $request)
    {
        $freelancerId = $request->get('freelancerId');

        //check whether exist or not
        $count = FavouriteFreelancer::where('user_id', Auth::id())
            ->where('freelancer_id', $freelancerId)
            ->count();

        if ($count >= 1) {
            FavouriteFreelancer::where('user_id', Auth::id())
                ->where('freelancer_id', $freelancerId)
                ->first()->delete();

            return response()->json([
                'status' => 'OK',
                'message' => trans('common.freelancer_remove_from_list_success')
            ]);
        } else {
            return response()->json([
                'status' => 'Error',
                'message' => trans('common.freelancer_no_exist_in_list')
            ]);
        }
    }
}
