<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\FrontendController;
use App\Http\Requests\Frontend\UserBankAccountCreateFormRequest;
use App\Models\ProjectApplicant;
use App\Models\ProjectMilestones;
use App\Models\TimeTrack;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserBank;
use App\Models\UserPaymentMethod;
use App\Models\WorldCountries;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MyAccountController extends FrontendController
{
    public function index(Request $request)
    {
        $transactions = Transaction::where(function ($q) {
            $q->where('status', '=', Transaction::STATUS_APPROVED_TRANSACTION)
                ->orWhere('status', Transaction::STATUS_IN_REJECTED_TRANSACTION)
                ->orWhere('status', Transaction::STATUS_REJECTED)
                ->orWhere('status', Transaction::STATUS_IN_PROGRESS_TRANSACTION)
                    ;
        })
            ->where(function ($q) {
                $q->where('type', '=', Transaction::TYPE_TOP_UP)->orWhere('type', '=', Transaction::TYPE_WITHDRAW);
            })
            ->where('created_at', '>', Carbon::now()->subDay(30))
            ->where(function ($q) {
                $q->where('performed_by', '=', Auth::user()->id)->orWhere('performed_to', '=', Auth::user()->id);
            })
            ->with('transactionDetail')
            ->orderBy('id', 'desc')
            ->limit(10)
            ->get()
            ;
        $paymentMethodDetails = UserPaymentMethod::where('user_id', '=', \Illuminate\Support\Facades\Auth::user()->id)->get();
        $ip = \Request::ip();
        $availableCountry = $this->checkCountry($ip);
        $userID = \Auth::user()->id;

        $amountEarned = Transaction::where(function ($q) {
            $q->where('status', Transaction::STATUS_APPROVED_TRANSACTION)
                ->orWhere('status', Transaction::STATUS_APPROVED)
                ;
        })->where('performed_to', '=', \Auth::user()->id)->sum('amount');
        $amountPaid = Transaction::where(function ($q) {
            $q->where('status', Transaction::STATUS_APPROVED_TRANSACTION)
                ->orWhere('status', Transaction::STATUS_APPROVED)
                ;
        })->where('performed_by', '=', \Auth::user()->id)->sum('amount');
        $amountWithdrawal = Transaction::where('status', '=', Transaction::TYPE_WITHDRAW)
            ->where('performed_by', '=', \Auth::user()->id)->sum('amount');
        $amountPending = Transaction::where('status', '=', Transaction::STATUS_IN_PROGRESS_TRANSACTION)
            ->where('performed_by', '=', \Auth::user()->id)->sum('amount');

        // calculate progress amount
        $onGoingProject = ProjectApplicant::where('user_id', \Auth::user()->id)
            ->where('status', '!=', ProjectApplicant::STATUS_COMPLETED)->get();
        $amountProgress = 0;
        foreach ($onGoingProject as $project) {
            $timeTrack = TimeTrack::selectRaw('SUM(TIMESTAMPDIFF(SECOND, created_at, updated_at)) as total_time')
                ->where('project_id', $project->project_id)
                ->where('user_id', \Auth::user()->id)
                ->first()
                ;

            $totalTime = !is_null($timeTrack->total_time) ? (int) $timeTrack->total_time : 0;
            $totalDollar = ProjectApplicant::PAY_TYPE_HOURLY_PAY == $project->pay_type ? $project->quote_price * floor($totalTime / 3600) : $project->quote_price;

            $amountProgress += $totalDollar;
        }

        // old logic
        // $amountProgress = ProjectMilestones::with(['project','milestoneStatus'])
        //     ->whereHas('project', function($q){
        //         $q->where('user_id', '=', \Auth::user()->id);
        //     })->whereHas('milestoneStatus', function($q){
        //         $q->where('status', '=', ProjectMilestones::STATUS_INPROGRESS);
        //     })->sum('amount');

        $checkUserCurrencyExist = $this->userCurrencyExist($userID);
        if (true != $checkUserCurrencyExist) {
            $currencies = $this->getAllCurrencies();
        }
        $userLocation = userLocation($request->ip());
        if (isset($userLocation['response'])) {
            $country = $userLocation['response']->country;
            $userOriginalLocation = $country;
        }

        return view('frontend.myAccount.index', [
            'invoices' => $transactions,
            'userCurrencyExist' => $checkUserCurrencyExist,
            'currencies' => isset($currencies) ? $currencies : '',
            'paymentMethodDetails' => $paymentMethodDetails,
            'available_country' => $availableCountry,
            'amountEarned' => $amountEarned,
            'amountPaid' => $amountPaid,
            'amountWithdrawal' => $amountWithdrawal,
            'amountPending' => $amountPending,
            'amountProgress' => $amountProgress,
            'userOriginalLocation' => isset($userOriginalLocation) ? $userOriginalLocation : '',
        ]);
    }

    public function userCurrencyExist($userID)
    {
        $user = User::where('id', '=', $userID)->first();
        if (is_null($user->country_id)) {
            return false;
        }

        return true;
    }

    public function getAllCurrencies()
    {
        $currency = WorldCountries::select(['id', 'name', 'currency_code', 'currency_name'])
            ->get()
            ->sortby('currency_name')
            ;

        return collect($currency)->unique('currency_code');
    }

    public function checkCountry($ip)
    {
        $ipData = json_decode(file_get_contents(
            'http://www.geoplugin.net/json.gp?ip='.$ip
        ));
        $Country = false;
        if ($ipData) {
            if ('Malaysia' == $ipData->geoplugin_countryName) {
                $Country = true;

                return collect([
                    'is_available_country' => $Country,
                    'country_name' => $ipData->geoplugin_countryName,
                    'code' => $ipData->geoplugin_currencyCode,
                ]);
            }

            return collect([
                'is_available_country' => $Country,
                'country_name' => $ipData->geoplugin_countryName,
                'code' => $ipData->geoplugin_currencyCode,
            ]);
        }
    }

    public function create(UserBankAccountCreateFormRequest $request)
    {
        $bank = new UserBank();
        $bank->user_id = Auth::guard('users')->user()->id;
        $bank->bank_id = $request->bank_id;
        $bank->account_name = $request->get('account_name');
        $bank->account_number = $request->get('account_number');
        $bank->account_address = $request->get('account_address');

        try {
            $bank->save();
            addSuccess(trans('member.bank_account_successfully_add'));

            return makeResponse(trans('member.bank_account_successfully_add'));
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), false);
        }
    }

    public function delete(Request $request, $id)
    {
        $model = UserBank::find($id);

        if ($model) {
            if ($model->user_id == Auth::guard('users')->user()->id) {
                $model->delete();
                addSuccess(trans('member.bankcard_successfully_delete'));

                return makeResponse(trans('member.bankcard_successfully_delete'));
            }

            return makeResponse(trans('member.bankcard_not_belongs_to_you'), true);
        }

        return makeResponse(trans('member.bankcard_not_found'), true);
    }
}
