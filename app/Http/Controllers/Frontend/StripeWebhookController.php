<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use Laravel\Cashier\Http\Controllers\WebhookController as CashierController;
use App\Libraries\PayPal_PHP_SDK\PayPalTrait;

class StripeWebhookController extends CashierController
{
    use PayPalTrait;

    /**
     * Handle invoice payment succeeded.
     *
     * @param  array  $payload
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handleInvoicePaymentSucceeded($payload)
    {
        \Log::info('Stripe webhook');
        \Log::info($payload);

        $type = $payload['type'];
        $data = $payload['data'];

        // if invoice payment success
        if ($type == 'invoice.payment_succeeded') {
	        \Log::info('Payment success');
	        $invoiceId = $data['object']['id'];
	        \DB::beginTransaction();
	        try {
	        	// set status to approve
	        	$topUpTransaction = Transaction::where('comment', $invoiceId)->where('type', Transaction::TYPE_TOP_UP)->first();
	        	$topUpTransaction->update([
	        		'status' => Transaction::STATUS_APPROVED_TRANSACTION
	        	]);

	        	// set topup status to approve
		        $topUpFeeTransaction = Transaction::where('comment', $invoiceId)->where('type', Transaction::TYPE_SERVICE_FEE)->first();
		        $topUpFeeTransaction->update([
		        	'status' => Transaction::STATUS_APPROVED
		        ]);

		        // calculate tax
		        $amount = $topUpTransaction->balance_after - $topUpTransaction->balance_before;
		        $tax = $this->calculateTax($amount);
		        $amount = $amount - $tax;

		        // increase balance
		        $user = User::where('id', $topUpTransaction->performed_by)->first();
		        $user->increment('account_balance', $amount);

	        	\DB::commit();
	        } catch (Exception $e) {
	        	\DB::rollback();
		        \Log::info('Error: '.$e->getMessage());
	        }
        }

        if ($type == 'invoice.payment_failed') {
        	\Log::info('Payment failed');
	        $invoiceId = $data['object']['id'];
	        \DB::beginTransaction();
	        try {
	        	// set status to approve
	        	$topUpTransaction = Transaction::where('comment', $invoiceId)->where('type', Transaction::TYPE_TOP_UP)->first();
	        	$topUpTransaction->update([
	        		'status' => Transaction::STATUS_IN_REJECTED_TRANSACTION
	        	]);

	        	// set topup status to approve
		        $topUpFeeTransaction = Transaction::where('comment', $invoiceId)->where('type', Transaction::TYPE_SERVICE_FEE)->first();
		        $topUpFeeTransaction->update([
		        	'status' => Transaction::STATUS_REJECTED
		        ]);

	        	\DB::commit();
	        } catch (Exception $e) {
	        	\DB::rollback();
		        \Log::info('Error: '.$e->getMessage());
	        }
        }
    }
}
