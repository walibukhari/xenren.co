<?php

namespace App\Http\Controllers\Frontend;

use App\Models\OfficialProject;
use App\Models\Project;
use App\Models\Skill;
use Carbon;
use App\Http\Controllers\FrontendController;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\User;
use App\Models\UserSkill;

class SkillVerificationController extends FrontendController
{
    public function index(Request $request)
    {
        $keyword = $request->get('keyword');
        $projectType = $request->get('project_type');
        if( $projectType == null )
        {
            $projectType = Project::PROJECT_TYPE_OFFICIAL;
        }

        $jobs = null;
        if( $keyword == null )
        {
            $keyword = "";
        }
        else
        {
            $searchSkillNames = explode(',', $keyword);
            $searchSkillIDs = array();
            foreach( $searchSkillNames as $searchSkillName)
            {
                $skill = Skill::where('tag_color', Skill::TAG_COLOR_GOLD)
                    ->where(function($query) use ($searchSkillName){
                        $query->where('name_en', $searchSkillName);
                        $query->orWhere('name_cn', $searchSkillName);
                    })
                    ->first();

                if($skill)
                {
                    array_push($searchSkillIDs, $skill->id);
                }
            }

            if( $projectType == Project::PROJECT_TYPE_OFFICIAL)
            {
                $jobs = OfficialProject::with(['projectPositions', 'projectImages', 'project', 'project.projectSkills']);

                $jobs->whereHas('project.projectSkills', function ($query) use ($searchSkillIDs) {
                    $query->whereIn('skill_id', $searchSkillIDs);
                });

                $jobs->orderBy('created_at', 'DESC');

                if( $jobs->count() == 0 )
                {
                    //when official project no exist any, show common project
                    $projectType = Project::PROJECT_TYPE_COMMON;

                    $jobs = Project::with(['creator', 'projectQuestions', 'projectSkills'])
                        ->where('type', Project::PROJECT_TYPE_COMMON);

                    $jobs->whereHas('projectSkills', function ($query) use ($searchSkillIDs) {
                        $query->whereIn('skill_id', $searchSkillIDs);
                    });

                    $jobs->orderBy('created_at', 'DESC');
                }
            }
            else if( $projectType == Project::PROJECT_TYPE_COMMON)
            {
                $jobs = Project::with(['creator', 'projectQuestions', 'projectSkills'])
                    ->where('type', Project::PROJECT_TYPE_COMMON);

                $jobs->whereHas('projectSkills', function ($query) use ($searchSkillIDs) {
                    $query->whereIn('skill_id', $searchSkillIDs);
                });

                $jobs->orderBy('created_at', 'DESC');
            }

            $jobs = $jobs->get();

        }
	
	      $skillList = Skill::getSearchSkillData(Skill::SEARCH_TYPE_WHITE);
        return view('frontend.skillVerification.index', [
            'keyword' => $keyword,
            'jobs' => $jobs,
            'projectType' => $projectType,
	          'skillList' => $skillList,
        ]);
    }

    public function update(Request $request) {
        $categories = explode(',', $request->get('skills'));

        try {
            \DB::beginTransaction();

            $user = \Auth::guard('users')->user();

            if (count($categories) > 0 && count($categories) <= 6) {
                //Delete the user current skill set
                \DB::table('user_skill')->where('user_id', '=', $user->id)->delete();

                //Loop and insert the categories
                foreach ($categories as $key => $var) {
                    $category = Category::find($var);
                    if ($category->parent_id == null) {
                        throw new \Exception(trans('order.cannot_set_first_level_as_skill'));
                    }

                    if ($category->children->count()) {
                        throw new \Exception(trans('order.only_last_level_category_is_skill'));
                    }

                    $c = new UserSkill();
                    $c->user_id = $user->id;
                    $c->category_id = $category->id;

                    if (!$c->save()) {
                        throw new \Exception(trans('common.unknown_error'));
                    }
                }
            }

            $user->experience = $request->has('working_experience') && isNumber($request->get('working_experience')) ? $request->get('working_experience') : 0;

            if (!$user->save()) {
                return makeJSONResponse(trans('common.unknown_error'), true);
            }
            \DB::commit();
            return makeResponse(trans('order.successfully_update_skill'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeJSONResponse($e->getMessage(), true);
        }
    }
}