<?php
namespace App\Http\Controllers\Frontend;

use App\Constants;
use App\Http\Controllers\FrontendController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;



class TransactionDetailsController extends FrontendController
{
    public function index()
    {
        return view('frontend.transactionDetails.index');
    }
}
