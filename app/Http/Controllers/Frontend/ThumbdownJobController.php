<?php
namespace App\Http\Controllers\Frontend;

use App\Constants;
use App\Http\Controllers\FrontendController;
use App\Http\Requests\Frontend\JobApplyFormRequest;
use App\Models\ThumbdownJob;
use App\Models\Project;
use App\Models\ProjectApplicant;
use App\Models\ProjectApplicantAnswer;
use App\Models\ProjectChatFollower;
use App\Models\ProjectChatRoom;
use App\Models\User;
use App\Models\UserInboxMessages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;


class ThumbdownJobController extends FrontendController
{
    public function thumbsDownJob(Request $request){
        $projectId = $request->get('projectId');
        $projectType = $request->get('projectType');

        //check whether exist or not
        $count = ThumbdownJob::where('user_id', Auth::id())
            ->where('project_id', $projectId)
            ->where('project_type', $projectType)
            ->count();

        if( $count >= 1 )
        {
            return response()->json([
                'status'        => 'Error',
                'message'       => trans('common.error')
            ]);
        }

        $thumbdownJob = new ThumbdownJob();
        $thumbdownJob->user_id = Auth::id();
        $thumbdownJob->project_id = $projectId;
        $thumbdownJob->project_type = $projectType;
        $thumbdownJob->save();

        return response()->json([
            'status'        => 'OK',
            'message'       => trans('common.job_thumbed_down')
        ]);
    }
}
