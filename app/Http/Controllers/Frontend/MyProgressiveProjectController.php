<?php

namespace App\Http\Controllers\Frontend;

use App\Constants;
use App\Models\Project;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MyProgressiveProjectController extends Controller
{
    public function index(Request $request)
    {
        $_project = new Project();
        $projects = $_project->getMyProgressiveProjects();
        $lastNumberDayOptions = Constants::getLastNumberDaysList();

        if(isset($request->lastNumberDays) && $request->lastNumberDays != 0) {
            $projects = self::daysFilter($projects, $request->lastNumberDays);
        }

        if(isset($request->status)) {
            $projects = self::statusFilter($projects, $request->status);
        }

        return view('frontend.myProgressiveOrder.index', [
            'projects' => $projects,
            'status' => isset($request->status) ? $request->status : 'all',
            'lastNumberDayOptions' => $lastNumberDayOptions,
            'lastNumberDay' => $request->lastNumberDays
        ]);
    }
}
