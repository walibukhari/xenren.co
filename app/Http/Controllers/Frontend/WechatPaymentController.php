<?php

namespace App\Http\Controllers\Frontend;

use Carbon;
use App\Http\Controllers\FrontendController;
use Illuminate\Http\Request;

use \App\Libraries\Wechat2\lib\WxPayConfig;
use \App\Libraries\Wechat2\lib\data\WxPayNativePay;
use \App\Libraries\Wechat2\lib\data\WxPayUnifiedOrder;



class WechatPaymentController extends FrontendController {

    protected $wechat;

    function __construct() 
    {
        // $this->wechat = new WxPayUnifiedOrder();
    }

    public function qr_scan(Request $request, $amount = 0) {
    	$notify = new WxPayNativePay();
		$input = new WxPayUnifiedOrder();
		$input->SetBody("test QR pay for xenren");
		$input->SetAttach("test QR pay for xenren");
		$input->SetOut_trade_no(WxPayConfig::MCHID.date("YmdHis"));
		$input->SetTotal_fee($amount);
		$input->SetTime_start(date("YmdHis"));
		$input->SetTime_expire(date("YmdHis", time() + 600));
		$input->SetGoods_tag("test");
		$input->SetNotify_url("http://www.xenren.co/wechat/notify");
		$input->SetTrade_type("NATIVE");
		$input->SetProduct_id("123456789");
		$result = $notify->GetPayUrl($input);
		$url2 = $result["code_url"];
		return view('frontend.wechat.qr-create', [
            'url2' => $url2
        ]);
    }

    public function notify() {
    	echo 'thank you';
    }
}