<?php

namespace App\Http\Controllers\Frontend;


use App\Libraries\Pushy\PushyAPI;
use App\Logic\UploadFile\UploadFileRepository;
use App\Models\Countries;
use App\Models\CountryCities;
use App\Models\ExchangeRate;
use App\Models\Language;
use App\Models\Settings;
use App\Models\UploadFile;
use App\Models\User;
use App\Models\WorldCountries;
use Illuminate\Http\Request;
use App\Constants;
use App\Http\Controllers\FrontendController;
use App\Http\Requests\Frontend\ProjectCreateFormRequest;

use App\Models\Category;
use App\Models\Skill;
use App\Models\Project;
use App\Models\ProjectSkill;
use App\Models\SkillKeyword;
use App\Models\SkillKeywordSubmit;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class PostProjectController extends FrontendController
{
    public function __construct(UploadFileRepository $uploadFileRepository)
    {
        $this->uploadFile = $uploadFileRepository;
    }

    public function index(Request $request)
    {
        //for wechat autologin and redirect back
        Session::put('redirect_url', route('frontend.postproject'));

        $categories = Category::whereNull('parent_id')->get();

        if (\Auth::guard('users')->user()->is_agent == 1) {
            $skillList = Skill::getSearchSkillData(Skill::TAG_COLOR_GOLD);
        } else {
            $skillList = Skill::getSearchSkillData(Skill::TAG_COLOR_WHITE);
        }

        $countryList = WorldCountries::get();
        $languageList = Language::getJsonLanguages();

        $projectList = Project::GetUserProject()->get();

        $pay_types = Constants::getPayTypeLists();

        $uploadFiles = UploadFile::where('user_id', \Auth::guard('users')->user()->id)
            ->where('type', UploadFile::TYPE_POST_PROJECT)
            ->get();

        $result[] = '';
        foreach ($uploadFiles as $file) {
            $obj['name'] = $file->filename;
            try {
                $obj['size'] = filesize(Config::get('upload-files.location') . $file->filename);
            } catch (\Exception $e) {
                $obj['size'] = 0;
            }
            $obj['type'] = $file->getFileType();
            $result[] = $obj;
        }

        $jsonUploadFile = "{}";
        if (count($uploadFiles) != 0) {
            $jsonUploadFile = json_encode($result);
        }
        $draft = Project::where('user_id', '=', \Auth::user()->id)->where('status', '=', Project::STATUS_DRAFT)->select(['id'])->first();

        $adminSetting = getAdminSetting(Settings::HOURLY_ON_OFF);
        return view('frontend.postProject.index', [
            'categories' => $categories,
            'skillList' => $skillList,
            'countryList' => $countryList,
            'languageList' => $languageList,
            'projectList' => $projectList,
            'pay_types' => $pay_types,
            'jsonUploadFile' => $result,
            'draft'=> $draft,
            'adminSetting' => $adminSetting
        ] );
    }

    public function createDraft($request)
    {
        if ($request->continue_posting == '0') {
            $personal_info = $this->checkPersonalInfo($request);
            if ($personal_info['status'] == 'error') {
                return collect([
                    'status' => 'error',
                    'message' => 'personal_info',
                    'data' => $personal_info['data']
                ]);
            }
        }

//        \DB::beginTransaction();
        $project_id = $request->get('pre_project_id');
        $project = Project::GetProject()->find($project_id);
        $mode = "";
        $countryId = '';

        if (!$project) {
            //create
            $project = new Project();
            $mode = "create";
        } else {
            //Update
            $mode = "update";
        }

        try {
            if (isset($request->nearby_place)) {
                $country = CountryCities::where('id', '=', $request->nearby_place)->with('state.country')->first();
                $countryId = $country->state->country->id;
            }
        } catch (\Exception $e) {
            $countryId = '';
        }
        $job = (new \App\Jobs\CreateDraftProject($request->all(), \Auth::guard('users')->user()->id, $mode, Project::STATUS_DRAFT))->onQueue(config('jobs-queue-names.PostProject'));
        dispatch($job);
        $data = collect([
            'projectName' => $request->name,
            'type' => 'desktop'
        ]);


        \Log::info('Post Project Observer For Push Notification Called');

        $users = User::select([
            'id', 'web_token', 'post_project_notify'
        ])->where('post_project_notify', '=', 1)->get();

        $usersTokens = array();
        foreach ($users as $user) {
            array_push($usersTokens, $user->web_token);
        }

        $data = array(
            "title" => "Xenren", // Notification title
            'message' => $request->name,
            'body' => 'Project Posted...!'
        );
        $options = array(
            'notification' => array(
                'badge' => 0,
                'sound' => 'ping.aiff',
                'body' => "New project posted on Xenren"
            )
        );
        try {

            if ($mode == "create") {
                return makeJSONResponse(true, trans('common.successfully_add_new_skill_keywords'), [
//                    'project_id' => $project->id,
                    'skill_id_list' => $request->get('skill_id_list'),
                    'country_id' => $countryId
                ]);
            } else {
                //Delete image after commit, make sure no image get deleted but row still exists
//                if ($delete_files_paths && count($delete_files_paths) > 0) {
//                    foreach ($delete_files_paths as $key => $var) {
//                        @unlink(public_path($var));
//                    }
//                }
                return makeResponse(trans('common.project_success_updated'));
            }
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse($e->getMessage(), false);
        }

    }

    public function create(ProjectCreateFormRequest $request)
    {
        if (isset($request->is_draft) && $request->is_draft == 1) {
            return self::createDraft($request);
        }
        if ($request->continue_posting == '0') {
            $personal_info = $this->checkPersonalInfo($request);
            if ($personal_info['status'] == 'error') {
                return collect([
                    'status' => 'error',
                    'message' => 'personal_info',
                    'data' => $personal_info['data']
                ]);
            }
        }

//        \DB::beginTransaction();
        $project_id = $request->get('pre_project_id');
        $project = Project::GetProject()->find($project_id);
        $mode = "";
        $countryId = '';

        if (!$project) {
            //create
            $project = new Project();
            $mode = "create";
        } else {
            //Update
            $mode = "update";
        }

        try {
            if (isset($request->nearby_place)) {
                $country = CountryCities::where('id', '=', $request->nearby_place)->with('state.country')->first();
                $countryId = $country->state->country->id;
            }
        } catch (\Exception $e) {
            $countryId = '';
        }
        $job = (new \App\Jobs\CreatePostProject($request->all(), \Auth::guard('users')->user()->id, $mode))->onQueue(config('jobs-queue-names.PostProject'));
        dispatch($job);
        $data = collect([
            'projectName' => $request->name,
            'type' => 'desktop'
        ]);


        \Log::info('Post Project Observer For Push Notification Called');

        $users = User::select([
            'id', 'web_token', 'post_project_notify'
        ])->where('post_project_notify', '=', 1)->get();

        $usersTokens = array();
        foreach ($users as $user) {
            array_push($usersTokens, $user->web_token);
        }
        $icon = asset('images/Favicon.jpg');
        $data = array(
            "title" => "Xenren", // Notification title
            'message' => 'New Project Posted'.' '.$request->name,
            'body' => 'Project Posted...!',
            "image" => $icon
        );
        $options = array(
            'notification' => array(
                'badge' => 1,
                'sound' => 'ping.aiff',
                'body' => "New project posted on Xenren"
            )
        );
        PushyAPI::sendPushNotification($data, $usersTokens, $options);
        if (isset($request->is_draft_loaded) && $request->is_draft_loaded == 1) {
            Project::where('user_id', '=', \Auth::user()->id)->where('status', '=', Project::STATUS_DRAFT)->delete();
        }
//        $project->type = Project::PROJECT_TYPE_COMMON;
//        $project->user_id = \Auth::guard('users')->user()->id;
//        $project->name = $request->get('name');
//        $project->category_id = $request->get('category_id');
//        $project->description = $request->get('description');
//        $project->pay_type = $request->get('pay_type');
//        $project->time_commitment = $request->get('time_commitment');
//        $project->freelance_type = $request->get('freelance_type');
//        $project->nearby_place = $request->get('nearby_place');
//        $project->status = Project::STATUS_PUBLISHED;
//
//        //always convert the price to usd
//        if( app()->getLocale() == "en" )
//        {
//            $project->reference_price = $request->get('reference_price');
//        }
//        elseif( app()->getLocale() == "cn" )
//        {
//            $price = $request->get('reference_price');
//            $exchangeRate = ExchangeRate::getExchangeRate(Constants::CNY, Constants::USD);
//            $value = $price * $exchangeRate;
//            $project->reference_price = $value;
//        }

        try {
//            $project->is_read = Project::IS_READ_NO;
//            $project->save();
//
//            $delete_file = $request->get('delete_file');
//            $delete_files_paths = array();
//            if($mode == "update")
//            {
//                if ($delete_file && count($delete_file) > 0) {
//                    foreach ($delete_file as $key => $var) {
//                        $projectFile = ProjectFile::find($var);
//                        if ($projectFile) {
//                            $delete_images_paths[] = $projectFile->file;
//                            $projectFile->delete();
//                        }
//                    }
//                }
//            }

//            $hidUploadedFiles = $request->get('hidUploadedFile');
//            if( $hidUploadedFiles != null)
//            {
//                foreach ($hidUploadedFiles as $file) {
//                    //copy from file uploaded location to portfolio pic location
//                    //add record to DB
//                    $this->uploadFile->copy($file, UploadFile::TYPE_POST_PROJECT, $project->id);
//
//                    //delete the record from uploaded location
//                    //remove record from DB
//                    $this->uploadFile->delete($file);
//                }
//            }z

//            if($mode == "update"){
//                ProjectSkill::where('project_id', $project->id)->delete();
//                ProjectCountry::where('project_id', $project->id)->delete();
//                ProjectLanguage::where('project_id', $project->id)->delete();
//            }
//
//            $skillIDList = explode(",", $request->get('skill_id_list'));
//            $i_skills = array();
//            foreach($skillIDList as $skillID){
//                $i_skills[]=[
//                  'project_id' => $project->id,
//                  'skill_id'  => trim($skillID),
//                  'created_at' => date('Y-m-d H:i:s')
//                ];
//            }
//            if(!empty($i_skills)){
//                ProjectSkill::insert($i_skills);
//            }
//            $countryIDList = explode(",", $request->get('country_id_list'));
//            $i_countries = array();
//            foreach($countryIDList as $countryID){
//                if( $countryID == "" )
//                {
//                    continue;
//                }
//                $i_countries[]=[
//                  'project_id' => $project->id,
//                  'country_id'  => trim($countryID),
//                  'created_at' => date('Y-m-d H:i:s')
//                ];
//            }
//            if(!empty($i_countries)){
//                ProjectCountry::insert($i_countries);
//            }
//            $languageIDList = explode(",", $request->get('language_id_list'));
//            $i_languages = array();
//            foreach($languageIDList as $languageID){
//                if( $languageID == "" )
//                {
//                    continue;
//                }
//                $i_languages[] = [
//                  'project_id' => $project->id,
//                  'language_id'  => trim($languageID),
//                  'created_at' => date('Y-m-d H:i:s')
//                ];
//            }
//            if(!empty($i_languages)){
//                ProjectLanguage::insert($i_languages);
//            }
//            if( $project->id >= 1){
//                //clear all question before save a new set
//
//                $questions = $request->get('question');
//                if( $questions != null ){
//                    ProjectQuestion::GetProjectQuestion()->where('project_id', $project->id)->delete();
//                    $i_questions = array();
//                    foreach($questions as $key => $question){
//                        if( $question != '' ) {
//                            $i_questions[] = [
//                                'project_id' => $project->id,
//                                'question'  => trim($question),
//                                'created_at' => date('Y-m-d H:i:s')
//                            ];
//                        }
//                    }
//                    if(!empty($i_questions)){
//                        ProjectQuestion::insert($i_questions);
//                    }
//                }
//
//                //Create Project Chat Room
//                $projectChatRoom = ProjectChatRoom::GetProjectChatRoom()
//                    ->where('project_id', $project->id)
//                    ->first();
//                if( $projectChatRoom == null){
//                    $projectChatRoom = new ProjectChatRoom();
//                    $projectChatRoom->project_id = $project->id;
//                    $projectChatRoom->save();
//                }
//
//                //Project Owner become first chat room follower
//                $projectChatFollower = ProjectChatFollower::GetProjectChatFollower()
//                    ->where('user_id', \Auth::guard('users')->user()->id )
//                    ->first();
//
//                if( $projectChatFollower == null){
//                    $projectChatFollower = new ProjectChatFollower();
//                    $projectChatFollower->project_chat_room_id = $projectChatRoom->id;
//                    $projectChatFollower->user_id = \Auth::guard('users')->user()->id;
//                    $projectChatFollower->is_kick = 0;
//                    $projectChatFollower->save();
//                }
//
//                if( $mode == 'create' ){
//                    //add user inbox notification
//                    $userInbox =  new UserInbox();
//                    $userInbox->category = UserInbox::CATEGORY_SYSTEM;
//                    $userInbox->project_id =  $project->id;
//                    $userInbox->from_user_id = null;
//                    $userInbox->to_user_id = \Auth::guard('users')->user()->id;
//                    $userInbox->from_staff_id = 1;
//                    $userInbox->to_staff_id = null;
//                    $userInbox->type = UserInbox::TYPE_PROJECT_SUCCESSFUL_POST_JOB;
//                    $userInbox->is_trash = 0;
//                    $userInbox->save();
//
//                    //add user inbox message
//                    $userInboxMessage = new UserInboxMessages();
//                    $userInboxMessage->inbox_id = $userInbox->id;
//                    $userInboxMessage->from_user_id = null;
//                    $userInboxMessage->to_user_id = \Auth::guard('users')->user()->id;
//                    $userInboxMessage->from_staff_id = 1;
//                    $userInboxMessage->to_staff_id = null;
//                    $userInboxMessage->project_id = $project->id;
//                    $userInboxMessage->is_read = 0;
//                    $userInboxMessage->type = UserInboxMessages::TYPE_PROJECT_SUCCESSFUL_POST_JOB;
//
//                    $message = trans('common.you_success_post_project') . "<br/>" .
//                        trans('common.project_name') . " : <a href='" . route('frontend.joindiscussion.getproject', ['slug' => $project->name,'id' => $project->id ] ) . "'>" . $project->name . "</a>";
//                    $userInboxMessage->custom_message = $message;
//                    $userInboxMessage->save();
//                }
//
//            }

//            \DB::commit();
            if ($mode == "create") {
                //email notification to those who have related skill tag
//                $this->sendNewProjectNotification($project);

                return makeJSONResponse(true, trans('common.successfully_add_new_skill_keywords'), [
//                    'project_id' => $project->id,
                    'skill_id_list' => $request->get('skill_id_list'),
                    'country_id' => $countryId
                ]);
//                return makeResponse(trans('common.project_success_added'));
            } else {
                //Delete image after commit, make sure no image get deleted but row still exists
//                if ($delete_files_paths && count($delete_files_paths) > 0) {
//                    foreach ($delete_files_paths as $key => $var) {
//                        @unlink(public_path($var));
//                    }
//                }
                return makeResponse(trans('common.project_success_updated'));
            }
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse($e->getMessage(), false);
        }

    }

    public function getProjectInfo(Request $request)
    {
        if ($request->has('id') && $request->get('id') != '') {
            $project = Project::with('projectQuestions', 'projectFiles', 'projectCountries', 'projectLanguages')->find($request->get('id'));

            $projectSkillList = ProjectSkill::GetProjectSkill()->where('project_id', '=', $project->id)->get();
            $skillIdList = array();
            foreach ($projectSkillList as $projectSkill) {
                $item = $projectSkill->skill_id;
                array_push($skillIdList, $item);
            }

            $countryIdList = array();
            foreach ($project->projectCountries as $projectCountry) {
                $item = $projectCountry->country_id;
                array_push($countryIdList, $item);
            }

            $languageIdList = array();
            foreach ($project->projectLanguages as $projectLanguage) {
                $item = $projectLanguage->language_id;
                array_push($languageIdList, $item);
            }

            $category = Category::find($project->category_id);
            $parentCategory = Category::find($category->parent_id);
            $categoryName = $parentCategory->getTitle() . ' > ' . $category->getTitle();

            //get html content
            $view = View::make('frontend.postProject.fileList', [
                'projectFiles' => $project->projectFiles,
            ]);
            $attachmentContent = $view->render();

            //convert the price
            if (app()->getLocale() == "en") {
                //not need convert
            } else if (app()->getLocale() == "cn") {
                //convert form usd to cny
                $price = $project->reference_price;
                $exchangeRate = ExchangeRate::getExchangeRate(Constants::USD, Constants::CNY);
                $project->reference_price = $price * $exchangeRate;
            }

            if ($project) {
                try {
                    return makeJSONResponse(true, 'Success', [
                        'project' => $project,
                        'skillIdList' => $skillIdList,
                        'countryIdList' => $countryIdList,
                        'languageIdList' => $languageIdList,
                        'categoryName' => $categoryName,
                        'attachmentContent' => $attachmentContent,
                    ]);
                } catch (\Exception $e) {
                    return makeJSONResponse(false, $e->getMessage());
                }
            } else {
                return makeJSONResponse(false, trans('common.unknown_error'));
            }
        } else {
            return makeJSONResponse(false, trans('common.unknown_error'));
        }

    }

    public function addNewSkillKeyword(Request $request)
    {
        \DB::beginTransaction();
        try {
            if ($request->has('locale') && $request->get('locale') != '') {
                $locale = $request->get('locale');
            }

//            if( $request->has('lblkeyword') && $request->get('lblkeyword') != null ){
//                $keywords = $request->get('lblkeyword');
//                $ttlCountKeyword = count($keywords);
//                foreach( $keywords as $key => $keyword ){
//                    if( $keyword == '' ){
//                        return makeJSONResponse(false, trans('common.keyword_can_not_empty'));
//                    }
//                }
//            }
//
//            if( $request->has('tbxReferenceUrl') && $request->get('tbxReferenceUrl') != null ){
//                $referenceUrls = $request->get('tbxReferenceUrl');
//                $ttlCountReferenceUrl = count($referenceUrls);
////                foreach( $referenceUrls as $key => $referenceUrl ){
////                    if( $referenceUrl == '' ){
////                        return makeJSONResponse(false, trans('common.reference_url_can_not_empty'));
////                    }
////                }
//            }
//
//            if( isset($ttlCountReferenceUrl) && $ttlCountKeyword != $ttlCountReferenceUrl ){
//                return makeJSONResponse(false, trans('common.keyword_and_reference_url_count_not_match'));
//            }
//            else
//            {
//                //user without click ok button to fill the url for each keywords, so take original input and split it out
//                if( $request->has('tbxNewSkillKeywords') && $request->get('tbxNewSkillKeywords') != null )
//                {
//                    $keywords = explode(",", $request->get('tbxNewSkillKeywords') );
//                }
//            }

//          //check whether the keyword already in skill table
            $keywords = $request->get('tbxSkillKeyword');
            $referenceUrls = $request->get('tbxReferenceUrl');

            //check whether user is allow to submit keyword
            if (\Auth::guard("users")->user()->is_ban_submit_keyword == 1) {
                return makeJSONResponse(false, trans('common.you_are_ban_from_submit_any_skill_keywords'));
            }

            $emptyKeyword = true;

            foreach ($keywords as $key => $keyword) {
                if ($keyword != "") {
                    $emptyKeyword = false;
                    break;
                }
            }
            if ($emptyKeyword) {
                return makeJSONResponse(false, trans('common.keyword_can_not_empty'));
            }

            foreach ($keywords as $key => $keyword) {
                if ($keyword == "") {
                    //     return makeJSONResponse(false, trans('common.keyword_can_not_empty'));
                    continue;
                }

                $skillKeyword = new SkillKeyword();
                $skillKeyword->user_id = \Auth::guard("users")->user()->id;
                if ($locale == "cn") {
                    $skillKeyword->name_cn = $keyword;
                    $skillKeyword->name_en = "";
                } else {
                    $skillKeyword->name_cn = "";
                    $skillKeyword->name_en = $keyword;
                }
                $skillKeyword->reference_link = isset($referenceUrls) ? $referenceUrls[$key] : '';
                $skillKeyword->save();

                $skillKeywordSubmit = SkillKeywordSubmit::GetSkillKeywordSubmit()
                    ->where('name', $keyword)->first();
                if ($skillKeywordSubmit) {
                    $skillKeywordSubmit->submit_count = $skillKeywordSubmit->submit_count + 1;
                    $skillKeywordSubmit->save();
                } else {
                    $skillKeywordSubmit = new SkillKeywordSubmit();
                    $skillKeywordSubmit->name = $keyword;
                    $skillKeywordSubmit->submit_count = 1;
                    $skillKeywordSubmit->save();
                }

            }

            $user = User::find(\Auth::guard("users")->user()->id);
            $user->is_first_time_skill_submit = 1;
            $user->save();

            \DB::commit();
            return makeJSONResponse(true, trans('common.successfully_add_new_skill_keywords'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeJSONResponse(false, $e->getMessage());
        }

    }

    public function getInputFile(Request $request)
    {
        $fileName = $request->get('fileName');
        $itemId = $request->get('itemId');

        $shortFileName = $this->getShortFilename($fileName);
        $imageFileType = $this->getFileTypeImage($fileName);

        //get html content
        $view = View::make('frontend.postProject.fileItem', [
            'shortFileName' => $shortFileName,
            'imageFileType' => $imageFileType,
            'itemId' => $itemId
        ]);
        $contents = $view->render();

        return response()->json([
            'status' => 'OK',
            'contents' => $contents
        ]);
    }

    private function getFileTypeImage($fileName)
    {
        $result = "";

        if (in_array(substr(strrchr($fileName, '.'), 1), ['jpg', 'jpeg'])) {
            $result = "images/jpg.png";
        } else if (in_array(substr(strrchr($fileName, '.'), 1), ['gif'])) {
            $result = "images/gif.png";
        } else if (in_array(substr(strrchr($fileName, '.'), 1), ['png'])) {
            $result = "images/png.png";
        } else if (in_array(substr(strrchr($fileName, '.'), 1), ['bmp'])) {
            $result = "images/bmp.png";
        } else if (in_array(substr(strrchr($fileName, '.'), 1), ['doc', 'docx'])) {
            $result = "images/doc.png";
        } else if (in_array(substr(strrchr($fileName, '.'), 1), ['pdf'])) {
            $result = "images/pdf.png";
        } else if (in_array(substr(strrchr($fileName, '.'), 1), ['xls', 'xlsx'])) {
            $result = "images/xls.png";
        } else if (in_array(substr(strrchr($fileName, '.'), 1), ['mp3'])) {
            $result = "images/mp3.png";
        } else if (in_array(substr(strrchr($fileName, '.'), 1), ['mp4'])) {
            $result = "images/mp4.png";
        }

        return $result;
    }

    private function getShortFilename($fileName)
    {
        //get extension
        $array = explode('.', $fileName);
        $extension = end($array);

        $fileNameWithoutExt = str_replace('.' . $extension, "", $fileName);

        if (strlen($fileNameWithoutExt) > 5) {
            $partOfFile = substr($fileNameWithoutExt, 0, 5);


            $result = $partOfFile . '...' . '.' . $extension;
        } else {
            $result = $fileName;
        }

        return $result;
    }

    public function sendNewProjectNotification(Project $project)
    {
        //get the user who have following skill
        $projectId = $project->id;
        $projectName = $project->name;

        $projectSkills = ProjectSkill::where('project_id', $projectId)
            ->get();
        $skillIds = array();
        foreach ($projectSkills as $projectSkill) {
            array_push($skillIds, $projectSkill->skill_id);
        }

        //select user who match the project skill
        $users = User::with('skills')->where('id', '!=', \Auth::guard('users')->user()->id);
        $users = $users->whereHas('skills', function ($query) use ($skillIds) {
            $query
                ->whereIn('skill_id', $skillIds);
        })
            ->where('email_notification', 1) //allow send email notification
            ->get();

        //sender email
        $senderEmail = "support@xenren.co";
        $senderName = "Support Team";

        $emails = array();
        foreach ($users as $user) {
            if ($user->email != "" || $user->email != null) {
                array_push($emails, $user->email);
            }

        }
        $projectLink = route('frontend.joindiscussion.getproject', ['slug' => $projectName, 'id' => $projectId]);

        Mail::send('mails.skillMatchEmailNotification', array(
            'projectLink' => $projectLink,
            'projectName' => $projectName
        ), function ($message) use ($senderEmail, $senderName, $emails) {
            $message->from($senderEmail, $senderName);
            $message->to($emails)
                ->subject(trans('common.new_project_notification'));
        });
    }

    public function uploadDescriptionImage(Request $request)
    {
        $url = "";
        $allowed = array('png', 'jpg', 'gif', 'jpeg');
        $fileExist = $request->hasFile('file');
        if ($fileExist) {
            $file = $request->file('file');
            if ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
                $mime = $file->getClientOriginalExtension();
                if (!in_array(strtolower($mime), $allowed)) {
                    return makeJSONResponse(false, trans('common.invalid_file_type_for_image'));
                }

                $path = 'uploads/project/description/';
                $filename = generateRandomUniqueName();

                touchFolder($path);
                $file->move($path, $filename . '.' . $mime);

                //../to prevent image cant retrieve from join discussion and post project page
                $url = '/' . $path . $filename . '.' . $mime;
            }
        }

        return makeJSONResponse(true, '', ['url' => $url]);

    }

    public function checkPersonalInfo($request)
    {
        $id = \Auth::user()->id;
        $user = User::with('skills')->where('id', '=', $id)->first();
        if (isset($user->skills) && count($user->skills) > 0) {
            $skill_info = true;
        } else {
            $skill_info = false;
        }
        $verify_account = isset($user->is_validated) && $user->is_validated != 0 ? true : false;
        $clear_budget = isset($request->reference_price) && $request->reference_price != 0 ? true : false;
        $avatar = isset($user->img_avatar) && $user->img_avatar != '' ? true : false;
        if ($skill_info && $avatar && $clear_budget && $verify_account) {
            return collect([
                'status' => 'success'
            ]);
        }

        return collect([
            'status' => 'error',
            'data' => [
                'skill_info' => $skill_info,
                'verify_account' => $verify_account,
                'clear_budget' => $clear_budget,
                'avatar' => $avatar,
            ]
        ]);
        return $user->with('skills');
    }

    // public function editPostProject($project_id) {
    //     $project = Project::with('projectQuestions', 'projectFiles', 'projectCountries', 'projectLanguages')
    //         ->find($project_id)
    //     ;

    //     $projectSkillList = ProjectSkill::GetProjectSkill()->where('project_id', '=', $project->id)->get();

    //     $skillIdList = array();
    //     foreach($projectSkillList as $projectSkill){
    //         $item = $projectSkill->skill_id;
    //         array_push($skillIdList, $item);
    //     }

    //     $countryIdList = array();
    //     foreach($project->projectCountries as $projectCountry){
    //         $item = $projectCountry->country_id;
    //         array_push($countryIdList, $item);
    //     }

    //     $languageIdList = array();
    //     foreach($project->projectLanguages as $projectLanguage){
    //         $item = $projectLanguage->language_id;
    //         array_push($languageIdList, $item);
    //     }

    //     $category = Category::find($project->category_id);
    //     $parentCategory = Category::find($category->parent_id);
    //     $categoryName = $parentCategory->getTitle() . ' > ' .  $category->getTitle();

    //     //get html content
    //     $view = View::make('frontend.postProject.fileList', [
    //         'projectFiles' => $project->projectFiles,
    //     ]);
    //     $attachmentContent = $view->render();

    //     //convert the price
    //     if( app()->getLocale() == "en" )
    //     {
    //         //not need convert
    //     }
    //     else if(  app()->getLocale() == "cn" )
    //     {
    //         //convert form usd to cny
    //         $price = $project->reference_price;
    //         $exchangeRate = ExchangeRate::getExchangeRate(Constants::USD, Constants::CNY);
    //         $project->reference_price = $price * $exchangeRate;
    //     }

    //     $categories = Category::whereNull('parent_id')->get();

    //     if( \Auth::guard('users')->user()->is_agent == 1 )
    //     {
    //         $skillList = Skill::getSearchSkillData(Skill::TAG_COLOR_GOLD);
    //     }
    //     else
    //     {
    //         $skillList = Skill::getSearchSkillData(Skill::TAG_COLOR_WHITE);
    //     }

    //     $pay_types = Constants::getPayTypeLists();

    //     $uploadFiles = ProjectFile::where('project_id', $project_id)
    //         ->get();

    //     $path = [];
    //     foreach( $uploadFiles as $file )
    //     {
    //         $path= explode("/",$file->file);
    //         $test = sizeof($path);
    //         $obj['name'] = $path[$test - 1];
    //         $obj['size'] = filesize( $file->file );
    //         $obj['type'] = $file->getFileTypeImage();
    //         $result[] = $obj;
    //     }

    //     $jsonUploadFile = "{}";
    //     if( count($uploadFiles) != 0 )
    //     {
    //         $jsonUploadFile = json_encode($result);
    //     }
    //     // dd($jsonUploadFile);
    //     $countries = Countries::getCountryLists();

    //     $languages = Language::getLanguageLists();


    //     $countryList = Countries::getJsonCountries();
    //     $languageList = Language::getJsonLanguages();

    //     $skillIdListString = "";
    //     $countryIdListString = "";
    //     $languageIdListString = "";

    //     foreach($skillIdList as $key=>$skill){
    //         if($key != (sizeof($skillIdList)-1))
    //             $skillIdListString = $skillIdListString.$skill.",";
    //         else
    //             $skillIdListString = $skillIdListString.$skill;
    //     }

    //     foreach($countryIdList as $key=>$skill){
    //         if($key != (sizeof($countryIdList)-1))
    //             $countryIdListString = $countryIdListString.$skill.",";
    //         else
    //             $countryIdListString = $countryIdListString.$skill;
    //     }

    //     foreach($languageIdList as $key=>$skill){
    //         if($key != (sizeof($languageIdList)-1))
    //             $languageIdListString = $languageIdListString.$skill.",";
    //         else
    //             $languageIdListString = $languageIdListString.$skill;
    //     }

    //     return view('frontend.postProject.edit', [
    //         'project' => $project,
    //         'skillIdList'=>$skillIdList,
    //         'countryIdList'=>$countryIdList,
    //         'languageIdList'=>$languageIdList,
    //         'categoryName'=>$categoryName,
    //         'attachmentContent'=>$attachmentContent,

    //         'categories' => $categories,
    //         'pay_types' => $pay_types,
    //         'jsonUploadFile' => $jsonUploadFile,
    //         'countries' => $countries,
    //         'languages' => $languages,
    //         'skillList' => $skillList,
    //         'countryList' => $countryList,
    //         'languageList' => $languageList,
    //         'skillIdListString' => $skillIdListString,
    //         'countryIdListString' => $countryIdListString,
    //         'languageIdListString' => $languageIdListString,
    //     ] );
    // }

}
