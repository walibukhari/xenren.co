<?php

namespace App\Http\Controllers\Frontend;

use App\Models\ChatRoom;
use Carbon;
use App\Http\Controllers\FrontendController;
use App\Http\Requests\Frontend\ChatMessageCreateFormRequest;
use App\Http\Requests\Frontend\ChatMessageCreateImageFormRequest;
use Illuminate\Http\Request;
use App\Models\ChatMessage;
use Illuminate\Support\Facades\View;

class ChatController extends FrontendController
{
    public function postChat(ChatMessageCreateFormRequest $request)
    {
        $ChatMessage = new ChatMessage();
        $ChatMessage->sender_user_id = \Auth::guard('users')->user()->id;
        $ChatMessage->room_id = $request->get('chat_room_id');
        $ChatMessage->message = $request->get('message');

        try {
            $ChatMessage->save();
            $realName = \Auth::guard('users')->user()->getName();
            $avatar = \Auth::guard('users')->user()->getAvatar();

            return response()->json([
                'status' => 'OK',
                'message' => $ChatMessage->message,
                'time' => date('d M, Y H:i', strtotime($ChatMessage->created_at)),
                'realName' => $realName,
                'avatar' => $avatar
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => 'not-OK',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function chatImage($chat_room_id)
    {
        return view('frontend.feedback.modalAddFileChat', [
            'chat_room_id' => $chat_room_id
        ]);
    }

    public function chatImagePost(ChatMessageCreateImageFormRequest $request)
    {
        $ChatMessage = new ChatMessage();
        $ChatMessage->sender_user_id = \Auth::guard('users')->user()->id;
        $ChatMessage->room_id = $request->get('chat_room_id');
        $ChatMessage->message = $request->get('message');
        $ChatMessage->file = $request->file('file');

        try {
            $ChatMessage->save();

            //get html content for chat row
            $view = View::make('frontend.feedback.chatItem', [
                'message' => $ChatMessage->message,
                'time' => date('d M, Y H:i', strtotime($ChatMessage->created_at)),
                'realName' => \Auth::guard('users')->user()->getName(),
                'avatar' => \Auth::guard('users')->user()->getAvatar(),
                'file' => $ChatMessage->getFile()
            ]);
            $contents = $view->render();

            return response()->json([
                'status' => 'OK',
                'message' => trans('common.submit_successful'),
                'contents' => $contents
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => 'not-OK',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function pullChat($chatRoomId = null)
    {
        $chatLog = ChatMessage::pullChatMessage($chatRoomId, 'user');
        return response()->json([
            'status' => 'OK',
            'chatLog' => $chatLog['lists']
        ]);
    }
}