<?php

namespace App\Http\Controllers\Frontend;

use App\Constants;
use App\Models\Project;
use App\Models\ProjectApplicant;
use App\Models\UserInbox;
use App\Models\UserInboxMessages;
use App\MyOrder;
use Carbon;
use App\Http\Controllers\FrontendController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response as Response;
use Illuminate\Support\Facades\Auth;

class MyReceiveOrderController extends FrontendController
{
    public function index(Request $request)
    {
        $progressingProjects = ProjectApplicant::with([
            'project','user'
        ])
            ->where('user_id', '=', \Auth::user()->id)
	          ->where(function($q){
		          $q->where('status', '=', ProjectApplicant::STATUS_CREATOR_SELECTED)
			          ->orWhere('status', '=', ProjectApplicant::STATUS_APPLICANT_ACCEPTED);
	          })
            ->orderBy('id','desc');

        $countAwarded = isset($request->viewAllAwarded) ? $progressingProjects->get()->count() : 10;
        $progressingProjects = $progressingProjects->paginate($countAwarded);

        $offers = UserInbox::with('project','project.projectSkills','project.projectSkills.skill','toUser','messages')
            ->where('to_user_id','=',Auth::id())
            ->where('type','=',UserInbox::TYPE_PROJECT_SEND_OFFER)
            ->whereDoesntHave('messages',function($q){
                $q->where('type','=',UserInboxMessages::TYPE_PROJECT_REJECT_SEND_OFFER);
            });

        $count = isseT($request->viewAll) ? $offers->count() : 10;
        $offers = $offers->paginate($count);

        return view('frontend.myReceiveOrder.index', [
            'offers' => $offers,
            'viewAll' => isset($request->viewAll) ? true : false,
            'viewAllProgressing' => isset($request->viewAllProgressing) ? true : false,
            'progressingProjects'=> $progressingProjects
        ]);
    }

    public function removeOffer($project_id, $user_id)
    {
        $projectApplicant = ProjectApplicant::where('project_id', $project_id)
            ->where('user_id', $user_id)
            ->first();


        $projectApplicant->delete();


        return redirect()->route('frontend.myreceiveorder');
    }
}