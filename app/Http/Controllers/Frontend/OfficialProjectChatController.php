<?php

namespace App\Http\Controllers\Frontend;

use App\Models\OfficialProjectChatMessage;
use Carbon;
use App\Http\Controllers\FrontendController;
use App\Http\Requests\Frontend\UserChatMessageCreateFormRequest;
use App\Http\Requests\Frontend\OfficialProjectCreateChatMessageFormRequest;
use App\Http\Requests\Frontend\OfficialProjectCreateChatMessageFileFormRequest;
use App\Http\Requests\Frontend\UserChatMessageCreateProjectImageFormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;


class OfficialProjectChatController extends FrontendController
{

    public static function pullChat($userRelative = null)
    {
        $chatLog = OfficialProjectChatMessage::pullChatMessage($userRelative);
        return response()->json([
            'status' => 'OK',
            'receiverId' => $chatLog['receiverId'],
            'realName' => $chatLog['realName'],
            'chatLog' => $chatLog['lists']
        ]);
    }

    public function index(Request $request)
    {
        $userToInvite = \App\Models\User::GetUsers()
            ->where('id', '!=', \Auth::guard('users')->user()->id)->get();
        $tmpUsers = array();
        foreach ($userToInvite as $key => $var) {
            $realName = $var->email;
            $userRel = explode('@', $var->email);
            if (count($userRel) >= 2) {
                $realName = $userRel[0];
            }
            $tmpUsers[$var->id]['name'] = $realName;
        }
        //dummy for invite user

        $recentChat = OfficialProjectChatMessage::getRecentChat();
        return view('frontend.myChat.index', [
            'recentChat' => $recentChat,
            'userToInvite' => $tmpUsers
        ]);
    }

    public function postChat(UserChatMessageCreateFormRequest $request)
    {
        $OfficialProjectChatMessage = new OfficialProjectChatMessage();
        $OfficialProjectChatMessage->sender_user_id = \Auth::guard('users')->user()->id;
        $OfficialProjectChatMessage->receiver_user_id = $request->get('receiver');
        $OfficialProjectChatMessage->message = $request->get('message');

        try {
            $OfficialProjectChatMessage->save();
            $userRel = \Auth::guard('users')->user()->email;
            $realName = $userRel;
            $userRel = explode('@', $userRel);
            if (count($userRel) >= 2) {
                $realName = $userRel[0];
            }
            return response()->json([
                'status' => 'OK',
                'message' => $OfficialProjectChatMessage->message,
                'time' => $OfficialProjectChatMessage->created_at,
                'realName' => $realName
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => 'not-OK',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function checkNewChat()
    {
        $chatNew = OfficialProjectChatMessage::pullChatMessageNew();
        return response()->json([
            'status' => 'OK'
        ]);
    }

    public function pullProjectChat($projectChatRoomId = null)
    {
        $chatLog = OfficialProjectChatMessage::pullChatProjectMessage($projectChatRoomId);
        return response()->json([
            'status' => 'OK',
            'chatLog' => $chatLog['lists']
        ]);
    }

    public function postProjectChat(OfficialProjectCreateChatMessageFormRequest $request)
    {
        $OfficialProjectChatMessage = new OfficialProjectChatMessage();
        $OfficialProjectChatMessage->sender_user_id = \Auth::guard('users')->user()->id;
        $OfficialProjectChatMessage->chat_room_id = $request->get('officialProjectChatRoomId');
        $OfficialProjectChatMessage->message = $request->get('message');

        try {
            \DB::beginTransaction();

//            //Here we create user inbox message
//            $order = Order::with(['orderInbox'])->JoinApprovedApplicant()->find($request->get('projectId'));
//            if (\Auth::guard('users')->user()->id == $order->creator_id) {
//                $ui = UserInbox::where('to_user_id', '=', $order->applicant_user_id);
//            } else {
//                $ui = UserInbox::where('to_user_id', '=', $order->creator_id);
//            }
//
//            if ($ui->exists()) {
//                $ui = $order->orderInbox;
//
//                $uim_exists = UserInboxMessages::where('from_user_id', '=', \Auth::guard('users')->user()->id)->where('type', '=', UserInboxMessages::TYPE_ORDER_NEW_MESSAGE)->where('project_id', '=', $order->id);
//
//                if ($uim_exists->exists()) {
//                    $uim = $uim_exists->first();
//                    $uim->updated_at = new \Carbon\Carbon();
//                } else {
//                    $uim = new UserInboxMessages();
//                    $uim->updated_at = new \Carbon\Carbon();
//                    $uim->from_user_id = \Auth::guard('users')->user()->id;
//                    if (\Auth::guard('users')->user()->id == $order->creator_id) {
//                        $uim->to_user_id = $order->applicant_user_id;
//                    } else {
//                        $uim->to_user_id = $order->creator_id;
//                    }
//                    $uim->type = UserInboxMessages::TYPE_ORDER_NEW_MESSAGE;
//                }
//            } else {
//                //Here we send user inbox
//                $ui = new UserInbox();
//                $ui->from_user_id = \Auth::guard('users')->user()->id;
//                if (\Auth::guard('users')->user()->id == $order->creator_id) {
//                    $ui->to_user_id = $order->applicant_user_id;
//                } else {
//                    $ui->to_user_id = $order->creator_id;
//                }
//                $ui->type = UserInbox::TYPE_ORDER_APPLICANT_SELECTED;
//
//                //Here we create user inbox message
//                $uim = new UserInboxMessages();
//                $uim->from_user_id = \Auth::guard('users')->user()->id;
//                if (\Auth::guard('users')->user()->id == $order->creator_id) {
//                    $uim->to_user_id = $order->applicant_user_id;
//                } else {
//                    $uim->to_user_id = $order->creator_id;
//                }
//                $uim->type = UserInboxMessages::TYPE_ORDER_NEW_MESSAGE;
//            }
//
//            $u = User::find($uim->to_user_id);
//
//            if (!$u) {
//                throw new \Exception(trans('common.unknown_error'));
//            }
//
//            $u->unread_message = UserInboxMessages::where('to_user_id', '=', $u->id)->where('is_read', '=', 0)->count() + 1;
//            if (!$u->save()) {
//                throw new \Exception(trans('common.unknown_error'));
//            }
//
//            $ui->project_id = $order->id;
//            if (!$ui->save()) {
//                throw new \Exception(trans('common.unknown_error'));
//            }
//
//            $uim->is_read = 0;
//            $uim->project_id = $order->id;
//            $uim->inbox_id = $ui->id;
//            if (!$uim->save()) {
//                throw new \Exception(trans('common.unknown_error'));
//            }

            $OfficialProjectChatMessage->save();

            $this->redisPublish($OfficialProjectChatMessage->id);

            if (\Auth::guard('users')->user()->name != '') {
                $name = \Auth::guard('users')->user()->name;
            } else {
                $email = \Auth::guard('users')->user()->email;
                $name = explode('@', $email);
                if (count($name) >= 2) {
                    $name = $name[0];
                } else {
                    $name = $email;
                }
            }
            $avatar = \Auth::guard('users')->user()->getAvatar();
            \DB::commit();
            return response()->json([
                'status' => 'OK',
                'message' => $OfficialProjectChatMessage->message,
                'time' => date('d M, Y H:i', strtotime($OfficialProjectChatMessage->created_at)),
                'realName' => $name,
                'avatar' => $avatar
            ]);
        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([
                'status' => 'not-OK',
                'message' => $e->getMessage()
            ]);
        }
    }

    function redisPublish($id)
    {
        $message = OfficialProjectChatMessage::find($id);
        $chat['project_chat_room_id'] = $message->chat_room_id;
        if ($message->SendBy()->first()->name != '') {
            $name = $message->SendBy()->first()->name;
        } else {
            $email = $message->SendBy()->first()->email;
            $name = explode('@', $email);
            if (count($name) >= 2) {
                $name = $name[0];
            } else {
                $name = $email;
            }
        }
        $chat['id'] = $message->id;
        $chat['sender_user_id'] = $message->sender_user_id;
        $chat['name'] = $name;
        $chat['avatar'] = $message->sendBy()->first()->getAvatar();
        $chat['email'] = $name;
        $chat['message'] = $message->message;
        $chat['content'] = $message->message;
        $chat['time'] = date('d M, Y H:i', strtotime($message->created_at));
        $chat['status'] = "out";
        $chat['sender_user_id'] = $message->sender_user_id;
        $chat['date'] = date('Y-m-d', strtotime($message->created_at));
        $chat['file'] = $message->file;
        $isImage = 0;

        if (file_exists($message->file)) {
            if (strpos($message->file, '.txt') !== false || strpos($message->file, '.doc') !== false) {
                $chat['content'] = str_replace('--file upload: ', '', '<a href="' . $message->getFile() . '">' . $message->message . '<a>');
                $isImage = 0;
            } else {
                $a = getimagesize($message->file);
                $image_type = $a[2];
                if (in_array($image_type, array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG, IMAGETYPE_BMP))) {
                    $chat['content'] = str_replace('--file upload: ', '', $message->message);
                    $isImage = 1;
                }
            }
        }
        $chat['isImage'] = $isImage;

        $isAudio = 0;
        if (file_exists($message->file)) {
            $allowed = array(
                'audio/mpeg', 'audio/x-mpeg', 'audio/mpeg3', 'audio/x-mpeg-3', 'audio/aiff',
                'audio/mid', 'audio/x-aiff', 'audio/x-mpequrl', 'audio/midi', 'audio/x-mid',
                'audio/x-midi', 'audio/wav', 'audio/x-wav', 'audio/xm', 'audio/x-aac', 'audio/basic',
                'audio/flac', 'audio/mp4', 'audio/x-matroska', 'audio/ogg', 'audio/s3m', 'audio/x-ms-wax',
                'audio/xm', 'application/ogg', 'application/mp3', 'audio/mp3'
            );

            // check REAL MIME type
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $type = finfo_file($finfo, $message->file);
            finfo_close($finfo);

            // check to see if REAL MIME type is inside $allowed array
            if (in_array($type, $allowed)) {
                $isAudio = 1;
            }
        }
        $chat['isAudio'] = $isAudio;
        Redis::publish('default_room', \GuzzleHttp\json_encode($chat));
    }

    public function postInviteHelpdeskChat($projectChatRoomId = null)
    {
        $OfficialProjectChatMessage = new OfficialProjectChatMessage();
        $OfficialProjectChatMessage->sender_user_id = \Auth::guard('users')->user()->id;
        $OfficialProjectChatMessage->receiver_staff_id = 1;
        $OfficialProjectChatMessage->chat_room_id = $projectChatRoomId;

//        if(\Auth::guard('users')->user()->real_name != ''){
//            $name = \Auth::guard('users')->user()->real_name;
//        }else if(\Auth::guard('users')->user()->nick_name != ''){
//            $name = \Auth::guard('users')->user()->real_name;
//        }else{
//            $email      = \Auth::guard('users')->user()->email;
//            $name       = explode('@', $email);
//            if(count($name) >= 2){
//                $name = $name[0];
//            }else{
//                $name = $email;
//            }
//        }

        if (\Auth::guard('users')->user()->name != '') {
            $name = \Auth::guard('users')->user()->name;
        } else {
            $email = \Auth::guard('users')->user()->email;
            $name = explode('@', $email);
            if (count($name) >= 2) {
                $name = $name[0];
            } else {
                $name = $email;
            }
        }

        $OfficialProjectChatMessage->message = '--' . $name . ' invite help desk to this conversation--';

        try {
            $OfficialProjectChatMessage->save();
            $avatar = \Auth::guard('users')->user()->getAvatar();

            return response()->json([
                'status' => 'OK',
                'message' => $OfficialProjectChatMessage->message,
                'time' => date('d M, Y H:i', strtotime($OfficialProjectChatMessage->created_at)),
                'realName' => $name,
                'avatar' => $avatar
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => 'not-OK',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function postProjectChatFile(OfficialProjectCreateChatMessageFileFormRequest $request)
    {
        $OfficialProjectChatMessage = new OfficialProjectChatMessage();
        $OfficialProjectChatMessage->sender_user_id = \Auth::guard('users')->user()->id;
        $OfficialProjectChatMessage->chat_room_id = $request->get('officialProjectChatRoomId');
        $OfficialProjectChatMessage->message = '--file upload: ' . $request->get('message');
        $OfficialProjectChatMessage->file = $request->file('file');

        try {
            $OfficialProjectChatMessage->save();

            $this->redisPublish($OfficialProjectChatMessage->id);

            return response()->json([
                'status' => 'OK'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => 'not-OK',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function pullProjectChatFile($projectId = null)
    {
        $chatLog = OfficialProjectChatMessage::pullChatOrderMessageFile($projectId);
        return response()->json([
            'status' => 'OK',
            'chatLog' => $chatLog['lists'],
            'count' => $chatLog['count']
        ]);
    }

    public function postProjectChatImage(UserChatMessageCreateProjectImageFormRequest $request)
    {
        $OfficialProjectChatMessage = new OfficialProjectChatMessage();
        $OfficialProjectChatMessage->sender_user_id = \Auth::guard('users')->user()->id;
        $OfficialProjectChatMessage->chat_room_id = $request->get('projectChatRoomId');
        $OfficialProjectChatMessage->message = $request->get('message');
        $OfficialProjectChatMessage->file = $request->file('file');

        try {
            $OfficialProjectChatMessage->save();
            $this->redisPublish($OfficialProjectChatMessage->id);
            return response()->json([
                'status' => 'OK'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => 'not-OK',
                'message' => $e->getMessage()
            ]);
        }
    }
}
