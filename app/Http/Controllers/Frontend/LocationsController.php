<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Countries;
use App\Models\CountryCities;
use App\Models\States;
use App\Models\WorldCities;
use App\Models\WorldCountries;
use App\Models\WorldDivisions;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LocationsController extends Controller
{

    public function index(Request $request)
    {
        try {
            $countries = WorldCountries::where('name', 'like', '%' . $request->q . '%')->select('id')->get();
            $countryStates = WorldDivisions::whereIn('country_id', $countries)->select('id')->get();

            $query = WorldCities::with('state', 'country')->where('name', 'like', '%' . $request->q . '%');
            //            if(isset($countryStates) && count($countryStates) > 0) {
            //            	$query->orWhereIn('state_id', $countryStates);
            //            }
            $data = $query->orderByRaw('CHAR_LENGTH(name)')->get();
            $arr = array();
            foreach ($data as $k => $v) {
                array_push($arr, array(
                    'id' => $v->id,
                    'text' => $v->name . ', ' . $v->country->name
                ));
            }
            return collect([
                'results' => $arr
            ]);
        } catch (\Exception $e) {
            dd($e);
        }
    }


    public function xindex(Request $request)
    {
        try {
            $countries = Countries::where('name', 'like', '%' . $request->q . '%')->select('id')->get();
            $countryStates = States::whereIn('country_id', $countries)->select('id')->get();

            $query = CountryCities::with('state.country')->where('name', 'like', '%' . $request->q . '%');
            if (isset($countryStates) && count($countryStates) > 0) {
                $query->orWhereIn('state_id', $countryStates);
            }
            $data = $query->orderByRaw('CHAR_LENGTH(name)')->get();
            $arr = array();
            foreach ($data as $k => $v) {
                array_push($arr, array(
                    'id' => $v->id,
                    'text' => $v->name . ', ' . $v->state->name . ', ' . $v->state->country->name
                ));
            }
            return collect([
                'results' => $arr
            ]);
        } catch (\Exception $e) {
            dd($e);
        }
    }
}
