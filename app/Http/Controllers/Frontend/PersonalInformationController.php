<?php

namespace App\Http\Controllers\Frontend;

use App\Jobs\ValidateFaceDetect;
use App\Models\Countries;
use App\Models\CountryLanguage;
use App\Models\SkillKeyword;
use App\Models\SkillKeywordSubmit;
use App\Models\User;
use App\Models\UserLanguage;
use App\Models\UserSkillCommentSkills;
use App\Models\UserStatus;
use App\Models\UserCoinAddress;
use App\Models\WorldCities;
use App\Models\SharedOffice;
use App\Services\GeneralService;
use Carbon;
use App\Http\Controllers\FrontendController;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;

use App\Models\Skill;
use App\Models\UserSkill;
use App\Models\JobPosition;

use App\Http\Requests\Frontend\PersonalInformationUpdateFormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;
use Lang;
use App\Models\FaceDetect as Face;
use App\Constants;

class PersonalInformationController extends FrontendController
{
    use DispatchesJobs;

    public function index(Request $request)
    {
        try {
            $userSkills = UserSkill::with('skill')
                ->where('user_id', \Auth::guard('users')->user()->id)
                ->get();
            $userSelectedSkillArr = array();
            foreach ($userSkills as $userSkill) {
                $is_gold_tag = $userSkill->experience >= 1 ? 1 : 0;
                $item = array(
                    "id" => $userSkill->skill->id,
                    "name" => $userSkill->skill->getName(),
                    "is_gold_tag" => $is_gold_tag,
                    "is_green_tag" => $userSkill->is_client_verified
                );
                array_push($userSelectedSkillArr, $item);
            }

            $skillList = Skill::GetSkill()
                ->where('tag_color', Skill::TAG_COLOR_WHITE)
                ->get();
            $arr = array();
            foreach ($skillList as $skill) {
                $item = array(
                    "id" => $skill->id,
                    "name" => \Lang::locale() == 'cn' ? $skill->name_cn : $skill->name_en
                );
                array_push($arr, $item);
            }
            $arr = collect($arr)->unique('name')->all();
            $languages = array();
            $allLang = CountryLanguage::whereHas('country')->get();
            foreach ($allLang as $lang) {
                if (Lang::locale() == 'cn') {
                    $name = (($lang->language->name_cn != null) || !empty($lang->language->name_cn)) ? $lang->language->name_cn : $lang->language->name;
                } else {
                    $name = $lang->language->name;

                }
                $language = array(
                    'id' => $lang->id,
                    'name' => $name,
                    'code' => isset($lang->country->code) ? $lang->country->code : 'cn',
                );
                array_push($languages, $language);
            }

            // get coin address
            $moveerAddress = UserCoinAddress::where('user_id', \Auth::guard('users')->user()->id)
                ->first();
            $moveerAddress = !is_null($moveerAddress) ? $moveerAddress->address : null;

            $languages = collect($languages);
            $languages = $languages->unique('name')->values();
            $langList = json_encode($languages);
            $skillList = json_encode($arr);
            $userSelectedSkillArr = json_encode($userSelectedSkillArr);
            $jobPositions = JobPosition::GetJobPosition()->get();
//        $country = Countries::pluck('id','name')->prepend('selections');
            $faceDetect = Face::where('user_id', '=', \Auth::guard('users')->user()->id)->first();
            return view('frontend.personalInformation.index', [
                'skillList' => $skillList,
                'userSelectedSkillArr' => $userSelectedSkillArr,
                'langList' => $langList,
                'jobPositions' => $jobPositions,
                'faceDetect' => isset($faceDetect) ? $faceDetect->face_status : '',
                'moveerAddress' => $moveerAddress
            ]);
        } catch (\Exception $e) {
            dd($e);
        }

    }

    public function deleteLanguage($id){
        $user_id = \Auth::user()->id;
        UserLanguage::where('id', $id)->delete();
        $newUserLanguages = UserLanguage::where('user_id', $user_id)->get();
        $view = View::make('frontend.userCenter.ucLanguage', ['languages' => $newUserLanguages]);
        $contents = (string)$view;
        return makeJsonResponse(true, 'success', ['language_sect_value' => $contents]);
    }

    public function change(Request $request)
    {
        $column = $request->column;
        $user_id = \Auth::guard('users')->user()->id;
        if ($column == 'skill') {

            UserSkill::where('user_id', $user_id)->delete();
            $skillIDList = explode(",", $request->get('val'));
            foreach ($skillIDList as $skillID) {
                $check = Skill::find($skillID);
                if ($check) {
                    //due to when update the user skill, all the skill delete
                    //so need to reinsert the past experience
                    $totalExperience = UserSkillCommentSkills::where('user_id', Auth::guard('users')->user()->id)
                        ->where('skill_id', $skillID)
                        ->sum('experience');

                    if ($totalExperience == null) {
                        $totalExperience = 0;
                    }

                    $userSkill = new UserSkill();
                    $userSkill->user_id = $user_id;
                    $userSkill->skill_id = trim($skillID);
                    $userSkill->experience = $totalExperience;
                    $userSkill->save();
                } else {
                    $skill = new SkillKeyword();
                    $skill->name_en = $skillID;
                    $skill->user_id = $user_id;
                    $skill->save();
                }
            }

            $user = User::find($user_id);
            $user->is_first_time_skill_submit = 1; //mean already submit skill
            $user->save();
        } elseif ($column == 'language') {
//            UserLanguage::where('user_id', $user_id)->delete();
            $language_id = explode(',', $request->get('val'));
            foreach ($language_id as $id) {
                if (isset($id) && $id != '') {
                    $userLanguage = new UserLanguage();
                    $userLanguage->language_id = trim($id);
                    $userLanguage->user_id = $user_id;
                    $userLanguage->save();

                }
            }
            $newUserLanguages = UserLanguage::where('user_id', $user_id)->get();
            $view = View::make('frontend.userCenter.ucLanguage', ['languages' => $newUserLanguages]);
            $contents = (string)$view;

            return makeJsonResponse(true, 'success', ['language_sect_value' => $contents]);
        } elseif ($column == "info") {
            $model = \Auth::guard('users')->user();
//			$model->qq_id = $request->get('qq_id');
            $model->line_id = $request->get('line_id');
            $model->wechat_id = $request->get('wechat_id');
            $model->skype_id = $request->get('skype_id');
            $model->handphone_no = $request->get('handphone_no');
            $model->save();
        } elseif ($column == "hourly_pay") {
            if (!isset($request->hourly_pay) || $request->hourly_pay != '') {
                return makeJsonResponse(true, trans('common.no-record'));
            }
            $model = \Auth::guard('users')->user();
            $model->hourly_pay = str_replace('/ hr', '', $request->get('hourly_pay'));
            $model->currency = $request->get('currency');
            $model->save();

        } elseif ($column == "status") {
            UserStatus::where('user_id', \Auth::user()->id)->delete();
            if ($request->project) {
                $userStatus = new UserStatus();
                $userStatus->user_id = $user_id;
                $userStatus->status_id = 1;
                $userStatus->save();
            }
            if ($request->coFounder) {
                $userStatus = new UserStatus();
                $userStatus->user_id = $user_id;
                $userStatus->status_id = 2;
                $userStatus->save();
            }
            if ($request->job) {
                $userStatus = new UserStatus();
                $userStatus->user_id = $user_id;
                $userStatus->status_id = 3;
                $userStatus->save();
            }
            return redirect()->back();
        } elseif ($column == "date_of_birth") {
            User::where('id', \Auth::user()->id)->update([
                'date_of_birth' => $request->val
            ]);
        } elseif ($column == "name") {
            $user = User::find(\Auth::user()->id);
            $user->update([
                'name' => $request->val
            ]);
        } elseif ($column == "title") {
            \Log::info('TITLE UPDATE');
            $user = User::find(\Auth::user()->id);
            \Log::info($user);
            $d = $user->update([
                'title' => $request->val
            ]);
            \Log::info($d);
        } elseif ($column == "experience") {
            $user = User::find(\Auth::user()->id);
            \Log::info($user);
            $d = $user->update([
                'experience' => $request->val
            ]);
            \Log::info($d);
        } else {
            $model = \Auth::guard('users')->user();
            $model->$column = $request->get('val');
            $model->save();
        }
//        return redirect()->back();
        return makeJSONResponse(true, 'success');
    }

    public function update(Request $request)
    {

        if(is_null(\Auth::user()->ip_country_code)) {
            if(is_null($request->get('city_id'))) {
                return makeResponse('Please Select City Country', true);
            }
            $getCId = WorldCities::where('id', '=', $request->get('city_id'))->with('country')->first();
            $ip_country_code = $getCId->country->code;
        } else {
            $ip_country_code = \Auth::user()->ip_country_code;
        }
        try {
            $skillIDList = explode(",", $request->get('skill_id_list'));
            if (count($skillIDList) > 10) {
                return makeResponse('Maximum skills must be under 10', true);
            }
            $model = \Auth::guard('users')->user();
            if ($request->hasFile('avatar')) {
                $extension = $request->file('avatar')->getClientOriginalExtension();
                $size = $request->file('avatar')->getSize();

                if (in_array(strtolower($extension), ['png', 'jpg', 'jpeg', 'gif']) && $size <= 800000) {
                    $model->img_avatar = $request->file('avatar');
                } else {
                    return makeResponse(trans('member.avatar_image_type'), true);
                }

            }

            //check email address is it already exist in DB
            $email = $request->get('email');
            $count = User::where('email', $email)->count();
            if ($count > 1) {
                return makeResponse(trans('common.email_has_been_registered'), true);
            }

            $handphone_no = $request->get('callingCode') . $request->get('handphone_no');
            $model->name = $request->get('nick_name') . ' ' . $request->get('last_name');
            $model->about_me = $request->get('about_me');
            $model->experience = $request->get('experience');
            $model->job_position_id = $request->get('job_position_id');
            $model->title = $request->get('title');
            $model->address = $request->get('address');
            $model->hourly_pay = $request->get('hourly_pay');
            $model->email = $request->get('email');
//			$model->qq_id = $request->get('qq_id');
            $model->line_id = $request->get('line_id');
            $model->wechat_id = $request->get('wechat_id');
            $model->skype_id = $request->get('skype_id');
            $model->handphone_no = $handphone_no;
            $model->country_id = $request->get('country_id');
            $model->city_id = $request->get('city_id');
            $model->currency = $request->get('currency');
            $model->ip_country_code = $ip_country_code;
            $model->latitude = $request->get('latitude');
            $model->longitude = $request->get('longitude');
            $model->meet_up_place_id = $request->get('meet_up_place');

            // find coin and update or create
            $getAddress = UserCoinAddress::where('user_id', $model->id)->first();
            if (is_null($getAddress)) {
                $address = new UserCoinAddress();
                $address->user_id = $model->id;
                $address->address = $request->get('moveer_address');
                $address->save();
            } else {
                $getAddress->address = $request->get('moveer_address');
                $getAddress->save();
            }

            //user didn't input, then put 2.8 /USD HOUR as default
            if ($model->hourly_pay == 0 || $model->hourly_pay == "") {
                $model->hourly_pay = 2.8;
                $model->currency = User::CURRENCY_USD;
            }

            $model->save();

            if ($model->id >= 1) {
                UserSkill::where('user_id', $model->id)->delete();
                $skillIDList = explode(",", $request->get('skill_id_list'));
                foreach ($skillIDList as $skillID) {
                    $check = Skill::find($skillID);
                    if ($check) {
                        //due to when update the user skill, all the skill delete
                        //so need to reinsert the past experience
                        $totalExperience = UserSkillCommentSkills::where('user_id', Auth::guard('users')->user()->id)
                            ->where('skill_id', $skillID)
                            ->sum('experience');

                        if ($totalExperience == null) {
                            $totalExperience = 0;
                        }

                        $userSkill = new UserSkill();
                        $userSkill->user_id = $model->id;
                        $userSkill->skill_id = trim($skillID);
                        $userSkill->experience = $totalExperience;
                        $userSkill->save();
                    } else {
                        $skill = new SkillKeyword();
                        $skill->name_en = $skillID;
                        $skill->user_id = $model->id;
                        $skill->save();
                    }
                }

                UserLanguage::where('user_id', $model->id)->delete();
                $lang_ids = isset($request->lang_id_list) ? explode(",", $request->get('lang_id_list')) : array();
                foreach ($lang_ids as $lang) {
                    $userLanguage = new UserLanguage();
                    $userLanguage->user_id = $model->id;
                    $userLanguage->language_id = trim($lang);
                    $userLanguage->save();
                }
            }
            $path = str_replace(asset('/'), "", Auth::user()->img_avatar);
            \Log::info($path);
            $response = $this->detectHumanFace(url($path));
            $user = $model;
            $data = json_decode($response);
            if (isset($data->faces[0])) {
                Face::createFaceData(Face::STATUS_FACE_DETECT_TRUE, $user->id, $data->faces[0]);
            } else {
                Face::createFaceData(Face::STATUS_FACE_DETECT_FALSE, $user->id, $data);
            }
            addSuccess(trans('member.successfully_update_personal_information'));
            return makeResponse(trans('member.successfully_update_personal_information'));
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function detectHumanFace($image)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://faceplusplus-faceplusplus.p.rapidapi.com/facepp/v3/detect?image_url=" . $image,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "x-rapidapi-host: faceplusplus-faceplusplus.p.rapidapi.com",
                "x-rapidapi-key: 138cb4582cmsh22edf357340ddcbp18dbbbjsne7d149c34fa5"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }

    public function updateAvatar(Request $request)
    {
        ini_set('max_execution_time', 0);
        ini_set('upload_max_filesize', 100000000);
        $userId = $request->get("user_id");
        $user = User::find($userId);

        if ($request->hasFile('avatar')) {
            $extension = $request->file('avatar')->getClientOriginalExtension();
            $size = $request->file('avatar')->getSize();

            if (in_array(strtolower($extension), ['png', 'jpg', 'jpeg', 'gif']) && $size <= 800000) {
                $user->img_avatar = $request->file('avatar');
            } else {
                return collect([
                    'status' => false,
                    'message' => trans('member.avatar_image_type')
                ]);
            }

//	        try {
//		        $generalService = new GeneralService();
//		        $icon = $generalService->uploadFile($request, 'temp', 'avatar');
//		        $imagePath = (public_path() . '/temp/' . $icon['icon']);
//		        Image::make($imagePath)->encode('jpg')->save((public_path() . '/temp/as.jpg'));
//		        $crop_params = FaceDetect::extract((public_path() . '/temp/as.jpg'))->face;
//		        if (count($crop_params) < 1) {
//			        return collect( [
//				        'status' => false,
//				        'message' => 'We cant detect face in your image, Please use your real image...!'
//			        ]);
//		        }
//	        }catch (\Exception $e) {
//		        return collect( [
//			        'status' => false,
//			        'message' => 'Not a valid file'
//		        ]);
//	        }

        }
        $user->save();

        $this->dispatch(new ValidateFaceDetect($user->img_avatar, $user->id));
        return collect([
            'status' => 'success',
            'message' => trans('member.successfully_update_personal_information')
        ]);

    }

//    public function addStatus(Request $request){
//        try {
//            $model = \Auth::guard('users')->user();
//            $model->status = $request->get('status');
//            $model->save();
//
//            return makeJSONResponse(true, 'Successful add status');
//        } catch (\Exception $e) {
//            return makeJSONResponse(false, $e->getMessage());
//        }
//    }

    public function updateContactView(Request $request)
    {
        $userId = $request->get("user_id");
        $isContactAllowView = $request->get('isContactAllowView');

        try {
            $user = User::find($userId);
            $user->is_contact_allow_view = $isContactAllowView;
            $user->save();

            return makeJSONResponse(true, 'Successful');
        } catch (\Exception $e) {
            return makeJSONResponse(false, $e->getMessage());
        }

    }

    public function sendEditEmailVerification(Request $request)
    {
        $userId = $request->get('userId');
        $user = User::find($userId);

        //sender email
        $senderEmail = "support@xenren.co";
        $senderName = "Support Team";

        //update user table to allow email address edit
        $code = generateRandomUniqueName(20);
        $user->email_change_code = $code;
        $user->save();

        $edit_email_link = route('frontend.changeemail') . '?code=' . $code;

        Mail::send('mails.editEmailVerification', array('edit_email_link' => $edit_email_link), function ($message) use ($senderEmail, $senderName, $user) {
            $message->from($senderEmail, $senderName);
            $message->to($user->email, $user->first_name . ' ' . $user->last_name)
                ->subject(trans('common.edit_email'));
        });

        return makeJSONResponse(true, 'success', ['msg' => trans('common.email_successful_send')]);
    }

    public function changeEmail(Request $request)
    {
        $code = $request->get('code');
        $user = User::where('email_change_code', $code)->first();

        return view('frontend.personalInformation.changeEmail', [
            'user' => $user
        ]);
    }

    public function changeEmailPost(Request $request)
    {
        $userId = $request->get('userId');
        $user = User::find($userId);
        if ($user) {
            $user->email = $request->get('newEmail');
            $user->email_change_code = "";
            $user->save();

            return redirect()->route('frontend.usercenter');
        }

    }

    public function sendEditPasswordVerification(Request $request)
    {
        $userId = $request->get('userId');
        $user = User::find($userId);

        //sender email
        $senderEmail = "support@xenren.co";
        $senderName = "Support Team";

        //update user table to allow email address edit
        $code = generateRandomUniqueName(20);
        $user->password_change_code = $code;
        $user->save();

        $edit_password_link = route('frontend.changepassword') . '?code=' . $code;

        Mail::send('mails.editPasswordVerification', array(
            'edit_password_link' => $edit_password_link,
            'getUserHint' => collect([
                'hint' => Auth::user()->hint
            ])
        ), function ($message) use ($senderEmail, $senderName, $user) {
            $message->from($senderEmail, $senderName);
            $message->to($user->email, $user->first_name . ' ' . $user->last_name)
                ->subject(trans('common.edit_password'));
        });

        return makeJSONResponse(true, 'success', ['msg' => trans('common.email_successful_send')]);
    }

    public function changePassword(Request $request)
    {
        $code = $request->get('code');
        $user = User::where('password_change_code', $code)->first();

        return view('frontend.personalInformation.changePassword', [
            'user' => $user
        ]);
    }

    public function changePasswordPost(Request $request)
    {
        $userId = $request->get('userId');
        $user = User::find($userId);
        $oldPassword = $request->get('oldPassword');
        $newPassword = $request->get('newPassword');
        if ($newPassword == "" || $newPassword == null) {
            return makeResponse(trans('common.new_password_required'), true);
        }

        if (Hash::check($oldPassword, $user->password)) {
            if ($user) {
                $user->password = $newPassword;
                $user->password_change_code = "";
                $user->save();

                return redirect()->route('frontend.usercenter');
            }
        } else {
            return makeResponse(trans('common.incorrect_password'), true);
        }
    }

    public function getSharedOffice()
    {
        $user = Auth::guard('users')->user();
        $user = User::where('id', $user->id)->first();

        $haversine = "(
            6371 * acos (
                cos ( radians('".$user->latitude."') )
                * cos( radians( lat ) )
                * cos( radians( lng ) - radians('".$user->longitude."') )
                + sin ( radians('".$user->latitude."') )
                * sin( radians( lat ) )
            )
        )";

        $select = "max(shared_offices.id) as id, shared_offices.office_name , ".$haversine." AS distance";

        $sharedOffices = SharedOffice::selectRaw($select)
            ->whereRaw($haversine." <= 60")
            ->groupBy('office_name', 'lat', 'lng')
            ->orderByRaw($haversine." asc ")
            ->get();

        return collect([
            'status' => 'success',
            'message' => '',
            'data' => $sharedOffices
        ]);
    }
}
