<?php

namespace App\Http\Controllers\Frontend;

use App\Constants;
use App\Http\Controllers\FrontendController;
use App\Http\Requests\Frontend\JobApplyFormRequest;
use App\Models\ExchangeRate;
use App\Models\FavouriteJob;
use App\Models\Project;
use App\Models\ProjectApplicant;
use App\Models\ProjectApplicantAnswer;
use App\Models\ProjectChatFollower;
use App\Models\ProjectChatRoom;
use App\Models\User;
use App\Models\UserInbox;
use App\Models\UserInboxMessages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;


class FavouriteJobController extends FrontendController
{
    public function index()
    {
        $favouriteJobs = FavouriteJob::where('user_id', \Auth::id())
            ->paginate(10);

        $totalCount = FavouriteJob::where('user_id', \Auth::id())
            ->count();

        return view('frontend.favouriteJobs.index', [
            'favouriteJobs' => $favouriteJobs,
            'totalCount' => $totalCount
        ]);
    }

    public function addFavouriteJob(Request $request)
    {
        $projectId = $request->get('projectId');

        //check whether exist or not
        $count = FavouriteJob::where('user_id', Auth::id())
            ->where('project_id', $projectId)
            ->count();
        if ($count >= 1) {
            return response()->json([
                'status' => 'Error',
                'message' => trans('common.job_already_exist_in_list')
            ]);
        }

        $favouriteJob = new FavouriteJob();
        $favouriteJob->user_id = Auth::id();
        $favouriteJob->project_id = $projectId;
        $favouriteJob->save();

        return response()->json([
            'status' => 'OK',
            'message' => trans('common.job_added_into_list')
        ]);
    }

    public function removeFavouriteJob(Request $request)
    {
        $projectId = $request->get('projectId');

        //check whether exist or not
        $count = FavouriteJob::where('user_id', Auth::id())
            ->where('project_id', $projectId)
            ->count();

        if ($count >= 1) {
            FavouriteJob::where('user_id', Auth::id())
                ->where('project_id', $projectId)
                ->first()->delete();

            return response()->json([
                'status' => 'ok',
                'message' => trans('common.job_remove_from_list_success')
            ]);
        } else {
            return response()->json([
                'status' => 'Error',
                'message' => trans('common.job_no_exist_in_list')
            ]);
        }
    }

    public function makeOffer($project_id, $user_id, $inbox_id = 0)
    {
        $user = User::find($user_id);
        $project = Project::find($project_id);

        return view('frontend.includes.modalMakeOfferv2', [
            'user' => $user,
            'project' => $project,
            'inbox_id' => $inbox_id,
            'is_official' => $project->type == Project::PROJECT_TYPE_OFFICIAL ? true : false
        ]);
    }

    public function makeOfferPost(JobApplyFormRequest $request)
    {
        try {
            $userId = \Auth::guard('users')->user()->id;
            $projectId = $request->get('project_id');
            $inboxId = $request->get('inbox_id');

            $about_me = $request->get('about_me');
            $partOfString = truncateSentence($about_me, 100);
            $apiKey = '194c102703f8d300c5694b5fd7d88fd1';

//            $url = 'http://ws.detectlanguage.com/0.2/detect?q=' . $partOfString . '&key=' . $apiKey;
//            $client = new \GuzzleHttp\Client();
//            $res = $client->request('POST', $url);
//            $detectLanguage = json_decode($res->getBody());

            $language = 'en';
//            if ($detectLanguage == null || $detectLanguage->data->detections[0]->language == '') {
//                return makeResponse(trans('common.api_fail_detect_language'), true);
//            } else {
//                $language = $detectLanguage->data->detections[0]->language;
//            }

            if ($language == "en") {
                if (str_word_count($about_me) > 500) {
                    return makeResponse(trans('validation.max.string', ['attribute' => trans('validation.attributes.about_me'), 'max' => '500']), true);
                }
            } else if ($language == "zh-Hant" || $language == "zh") {
                if (strlen(utf8_decode($about_me)) > 500) {
                    return makeResponse(trans('validation.max.string', ['attribute' => trans('validation.attributes.about_me'), 'max' => '500']), true);
                }
            } else {
                if (str_word_count($about_me) > 500) {
                    return makeResponse(trans('validation.max.string', ['attribute' => trans('validation.attributes.about_me'), 'max' => '500']), true);
                }
            }

            $project = Project::find($projectId);
            if ($inboxId > 0) {
                //check exist or not
                $count = UserInboxMessages::where('from_user_id', \Auth::guard('users')->user()->id)
                    ->where('to_user_id', $project->user_id)
                    ->where('inbox_id', $inboxId)
                    ->where(function ($query) {
                        $query->where('type', UserInboxMessages::TYPE_PROJECT_INVITE_ACCEPT);
                        $query->orWhere('type', UserInboxMessages::TYPE_PROJECT_INVITE_REJECT);
                    })
                    ->count();

                if ($count > 0) {
                    return makeResponse(trans('common.invite_request_already_reply'), true);
                }

                $uim = new UserInboxMessages();
                $uim->inbox_id = $inboxId;
                $uim->from_user_id = \Auth::guard('users')->user()->id;
                $uim->to_user_id = $project->user_id;
                $uim->from_staff_id = null;
                $uim->to_staff_id = null;
                $uim->project_id = $projectId;
                $uim->is_read = 0;
                $uim->type = UserInboxMessages::TYPE_PROJECT_INVITE_ACCEPT;
                $uim->save();
            } else {
                $inbox = UserInbox::where([
                    'category' => UserInbox::CATEGORY_NORMAL,
                    'project_id' => $projectId,
                    'from_user_id' => Auth::user()->id,
                    'to_user_id' => $project->user_id,
                ])->first();
                if (is_null($inbox)) {
                    $inbox = UserInbox::create([
                        'category' => UserInbox::CATEGORY_NORMAL,
                        'project_id' => $projectId,
                        'from_user_id' => Auth::user()->id,
                        'to_user_id' => $project->user_id,
                        'type' => UserInbox::TYPE_PROJECT_RECEIVE_OFFER,
                        'is_trash' => 0,
                        'daily_update' => $request->daily_update
                    ]);
                    UserInboxMessages::create([
                        'inbox_id' => $inbox->id,
                        'from_user_id' => Auth::user()->id,
                        'to_user_id' => $project->user_id,
                        'project_id' => $projectId,
                        'is_read' => 0,
                        'type' => UserInboxMessages::TYPE_PROJECT_RECEIVE_OFFER,
                        'quote_price' => $request->price,
                        'pay_type' => $request->pay_type,
                        'daily_update' => $request->daily_update,
                        'custom_message' => $request->about_me,
                    ]);
                } /*else {
		            return makeResponse(trans('jobs.you_already_apply_this_job_before'), true);
	            }*/
            }

            if ($request->get('line_id') == '' && $request->get('wechat_id') == '' &&
                $request->get('skype_id') == '' && $request->get('handphone_no') == '') {
                return makeResponse(trans('common.at_least_one_contact_info'), true);
            }

            $count = ProjectApplicant::GetProjectApplicant()
                ->where('project_id', $projectId)
                ->where('user_id', $userId)->count();

            if ($count >= 1) {
                return makeResponse(trans('jobs.you_already_apply_this_job_before'), true);
            }

            if (Project::GetProject()
                ->where('user_id', $userId)
                ->where('id', $projectId)
                ->exists()
            ) {
                return makeResponse(trans('jobs.you_are_not_allow_to_apply_because_you_are_project_creator'), true);
            }

            \DB::beginTransaction();

            //all the price save in usd
            //if language is en, no convert and straight save
            //if language is cn, covert to usd and save into db
            if (app()->getLocale() == "cn") {
                $exchangeRate = ExchangeRate::getExchangeRate(Constants::CNY, Constants::USD);
                $quote_price = $request->get('price') * $exchangeRate;
            } else {
                $quote_price = $request->get('price');
            }

            $model = new ProjectApplicant;
            $model->user_id = $userId;
            $model->project_id = $request->get('project_id');
            $model->daily_update = $request->get('daily_update');
            $model->status = ProjectApplicant::STATUS_APPLY; //Apply
            $model->quote_price = $quote_price; //$request->get('price');
            $model->pay_type = $request->get('pay_type');
            $model->about_me = $request->get('about_me');
            $model->save();
            $projectApplicant = $model;
            $newProjectApplicantId = $model->id;

            //when make offer, freelancer need to answer employer question
            if ($newProjectApplicantId >= 1) {
                if ($request->has('answer')) {
                    $questionIds = $request->get('questionId');
                    $answers = $request->get('answer');
                    foreach ($answers as $key => $answer) {
                        if ($answer != '') {
                            $model = new ProjectApplicantAnswer;
                            $model->project_applicant_id = $newProjectApplicantId;
                            $model->project_question_id = $questionIds[$key];
                            $model->answer = $answer;
                            $model->save();
                        }
                    }
                }
            }

            $model = \Auth::guard('users')->user();
            $Update = false;
            if ($model->line_id != $request->get('line_id')) {
                $model->line_id = $request->get('line_id');
                $Update = true;
            }

            if ($model->wechat_id != $request->get('wechat_id')) {
                $model->wechat_id = $request->get('wechat_id');
                $Update = true;
            }

            if ($model->skype_id != $request->get('skype_id')) {
                $model->skype_id = $request->get('skype_id');
                $Update = true;
            }

            if ($model->handphone_no != $request->get('handphone_no')) {
                $model->handphone_no = $request->get('handphone_no');
                $Update = true;
            }

            if ($model->about_me != $request->get('about_me')) {
                $model->about_me = $request->get('about_me');
                $Update = true;
            }

            if ($model->hourly_pay != $request->get('price') && (int)$request->get('pay_type') == ProjectApplicant::PAY_TYPE_HOURLY_PAY) {
                $model->hourly_pay = $request->get('price');
                $Update = true;
            }

            if ($Update == true) {
                $model->save();
            }

            //Project Chat Room
            $projectChatRoom = ProjectChatRoom::GetProjectChatRoom()
                ->where('project_id', $request->get('project_id'))
                ->first();

            //chat room follower
            $projectChatFollower = ProjectChatFollower::GetProjectChatFollower()
                ->where('user_id', \Auth::guard('users')->user()->id)
                ->where('project_chat_room_id', $projectChatRoom->id)
                ->first();

            if ($projectChatFollower == null) {
                $projectChatFollower = new ProjectChatFollower();
                $projectChatFollower->project_chat_room_id = $projectChatRoom->id;
                $projectChatFollower->user_id = \Auth::guard('users')->user()->id;
                $projectChatFollower->is_kick = 0;
                $projectChatFollower->save();
            }

            //update badge count
            $project = Project::with('creator')->where('id', '=', $projectId)->first();
            $project->is_read = Project::IS_READ_NO;
            $project->save();

            User::where('id', '=', $userId)->update([
                'line_id' => $request->get('line_id'),
                'wechat_id' => $request->get('wechat_id'),
                'skype_id' => $request->get('skype_id'),
                'handphone_no' => $request->get('handphone_no')
            ]);

            \DB::commit();

            $this->sendMakeOfferNotification($project, Auth::user());

            return response()->json([
                'status' => 'OK',
                'message' => trans('common.successfully_apply')
            ]);
//            return makeResponse(trans('common.successfully_apply'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(array(
                $e->getMessage(),
                $e->getLine(),
                $e->getFile(),
            ), true);
        }
    }

    public static function sendMakeOfferNotification(Project $project, $projectApplicant)
    {
        if (isset($project->creator) && isset($project->creator->email_notification) && $project->creator->email_notification == 1) {
            $projectId = $project->id;
            $projectName = $project->name;
            $projectApplicantName = $projectApplicant->getName();

            //sender email
            $senderEmail = "support@xenren.co";
            $senderName = "Support Team";

            $email = $project->creator->email;;
            $projectLink = route('frontend.joindiscussion.getproject', ['slug' => $projectName, 'id' => $projectId]);

            Mail::send('mails.makeOfferEmailNotification', array(
                'projectLink' => $projectLink,
                'projectName' => $projectName,
                'projectApplicantName' => $projectApplicantName
            ), function ($message) use ($senderEmail, $senderName, $email) {
                $message->from($senderEmail, $senderName);
                $message->to($email)
                    ->subject(trans('common.make_offer_notification'));
            });
        }
    }
}
