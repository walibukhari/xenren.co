<?php
namespace App\Http\Controllers\Frontend;

use App\Constants;
use App\Http\Controllers\FrontendController;
use App\Models\Transaction;
use App\Services\GeneralService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;



class TransactionHistoryController extends FrontendController
{
	public function index(Request $request)
	{
		$data = Transaction::getMyTransactionDetail();
		$transaction = collect();
		$balance = 0.00;
		$clients = collect();
		foreach ($data as $k => $v) {
		    if(isset($v->milestone) && isset($v->milestone->project) && isset($v->milestone->project->creator) && isset($v->milestone->project->creator->id) || $v->type == Transaction::TYPE_BONUS || $v->type == Transaction::TYPE_BOOKING_CHARGES) {
                if (!is_null($v->milestone)) {
                    $clients->push(collect([
                        'id' => $v->milestone->project->creator->id,
                        'text' => ucfirst($v->milestone->project->creator->name)
                    ]));
                };
                if ($v->type == Transaction::TYPE_MILESTONE_PAYMENT_REFUND && \Auth::user()->id == $v->performed_to) {
                    $balance = round($balance + $v->amount + GeneralService::calculateServiceFee($v->amount), 2);
                } else {
                    if ($v->payment_type == 'credit') {
                        $balance = round($balance + $v->amount, 2);
                    } else {
                        $balance = round($balance - $v->amount, 2);
                    }
                }

                if ($v['type'] == Transaction::TYPE_MILESTONE_PAYMENT_REFUND && isset($v->milestone->applicant_id)) {
                    //refund made by freelancer
                    if ($v->performed_by == Auth::user()->id) {
                        $amount = $v->amount;
                    } else {
                        $tax = GeneralService::calculateServiceFee($v->amount);
                        $amount = $v->amount + $tax;
                        $amount = round($amount);
                    }

                } else {
                    $amount = $v->amount;
                }

                if($v['type'] == Transaction::TYPE_BOOKING_CHARGES) {
                    $transaction->push(collect([
                        'name' => $v->receiver->name,
                        'image' => $v->receiver->img_avatar,
                        'date' => $v->created_at,
                        'status' => $v->status,
                        'status_string' => $v->status_string,
                        'category' => $v->type,
                        'id' => $v->id,
                        'payment' => $v->amount,
                        'balance' => $balance,
                        'payment_type' => $v->payment_type,
                        'currency_symbol' => $v->currency_symbol,
                        'type' => $v->type,
                        'created_at' => $v->created_at,
                        'created_at_formatted' => Carbon::parse($v->created_at)->format('Y-m-d'),
                        'amount' => $amount,
                        'comment' => $v->comment,
                        'release_at' => $v->release_at,
                        'project_owner' => isset($v->project_owner->id) ? $v->project_owner->id : 0
                    ]));
                }

                if($v['type'] == Transaction::TYPE_BONUS) {
                    $transaction->push(collect([
                        'name' => $v->receiver->name,
                        'image' => $v->receiver->img_avatar,
                        'date' => $v->created_at,
                        'status' => $v->status,
                        'status_string' => $v->status_string,
                        'category' => $v->type,
                        'id' => $v->id,
                        'payment' => $v->amount,
                        'balance' => $balance,
                        'payment_type' => $v->payment_type,
                        'currency_symbol' => $v->currency_symbol,
                        'type' => $v->type,
                        'created_at' => $v->created_at,
                        'created_at_formatted' => Carbon::parse($v->created_at)->format('Y-m-d'),
                        'amount' => $amount,
                        'comment' => $v->comment,
                        'release_at' => $v->release_at,
                        'project_owner' => isset($v->project_owner->id) ? $v->project_owner->id : 0
                    ]));
                }
                if ($v['type'] == Transaction::TYPE_MILESTONE_PAYMENT) {
                    if ($v['type'] == Transaction::TYPE_MILESTONE_PAYMENT && $v['performed_by'] == Auth::user()->id) {
                        $transaction->push(collect([
                            'name' => $v->receiver->name,
                            'image' => $v->receiver->img_avatar,
                            'date' => $v->created_at,
                            'status' => $v->status,
                            'status_string' => $v->status_string,
                            'category' => $v->type,
                            'id' => $v->id,
                            'payment' => $v->amount,
                            'balance' => $balance,
                            'payment_type' => $v->payment_type,
                            'currency_symbol' => $v->currency_symbol,
                            'type' => $v->type,
                            'created_at' => $v->created_at,
                            'created_at_formatted' => Carbon::parse($v->created_at)->format('Y-m-d'),
                            'amount' => $amount,
                            'comment' => $v->comment,
                            'release_at' => $v->release_at,
                            'project_owner' => isset($v->project_owner->id) ? $v->project_owner->id : 0
                        ]));
                    } else if ($v['status'] == Transaction::STATUS_APPROVED || $v['status'] == Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM) {
                        $transaction->push(collect([
                            'name' => $v->receiver->name,
                            'image' => $v->receiver->img_avatar,
                            'date' => $v->created_at,
                            'status' => $v->status,
                            'status_string' => $v->status_string,
                            'category' => $v->type,
                            'id' => $v->id,
                            'payment' => $v->amount,
                            'balance' => $balance,
                            'payment_type' => $v->payment_type,
                            'currency_symbol' => $v->currency_symbol,
                            'type' => $v->type,
                            'created_at' => $v->created_at,
                            'created_at_formatted' => Carbon::parse($v->created_at)->format('Y-m-d'),
                            'amount' => $amount,
                            'comment' => $v->comment,
                            'release_at' => $v->release_at,
                            'project_owner' => isset($v->project_owner->id) ? $v->project_owner->id : 0
                        ]));
                    }
                } else {
                    $transaction->push(collect([
                        'name' => $v->receiver->name,
                        'image' => $v->receiver->img_avatar,
                        'date' => $v->created_at,
                        'status' => $v->status,
                        'status_string' => $v->status_string,
                        'category' => $v->type,
                        'id' => $v->id,
                        'payment' => $v->amount,
                        'balance' => $balance,
                        'payment_type' => $v->payment_type,
                        'currency_symbol' => $v->currency_symbol,
                        'type' => $v->type,
                        'created_at' => $v->created_at,
                        'created_at_formatted' => Carbon::parse($v->created_at)->format('Y-m-d'),
                        'amount' => $amount,
                        'comment' => $v->comment,
                        'release_at' => $v->release_at,
                        'project_owner' => isset($v->project_owner->id) ? $v->project_owner->id : 0
                    ]));
                }
            }
		}
		$transaction = $transaction->sortByDesc('id')->all();
		$transaction = collect($transaction)->unique('type');
		$clients = $clients->unique()->all();

		if($request->has('employer') && $request->employer != '') {
			$transaction = $transaction->where('project_owner',  $request->employer);
		}

		if($request->has('id') && $request->id != '') {
			$transaction = $transaction->where('id',  $request->id);
		}

		if($request->has('date') && $request->date != '') {
			$date = (Carbon::parse($request->date)->format('Y-m-d'));
			$transaction = $transaction->where('created_at_formatted', $date);
		}

		if($request->has('status') && $request->status != '') {
			$transaction = $transaction->where('status', $request->status);
		}

		if($request->has('category') && $request->category != '') {
			$transaction = $transaction->where('type', $request->category);
		}

		return view('frontend.transactionHistory.new-index',[
			'data' => $transaction,
			'clients' => json_encode($clients)
		]);
	}

	public function downloadTransactionCSV()
	{
		$data = Transaction::getMyTransactionDetail();
		$transaction = collect();
		$balance = 0.00;
		foreach ($data as $k => $v) {
			if($v->type == Transaction::TYPE_MILESTONE_PAYMENT_REFUND && \Auth::user()->id == $v->performed_to) {
				$balance = round($balance + $v->amount + GeneralService::calculateServiceFee($v->amount), 2);
			} else {
				if ($v->payment_type == 'credit') {
					$balance = round($balance + $v->amount, 2);
				} else {
					$balance = round($balance - $v->amount, 2);
				}
			}

			$amount = 0;
			if($v['type'] == Transaction::TYPE_MILESTONE_PAYMENT_REFUND && isset($v->milestone->applicant_id)) {
				//refund made by freelancer
				if($v->performed_by == Auth::user()->id) {
					$amount = $v->amount;
				} else {
					$tax =GeneralService::calculateServiceFee($v->amount);
					$amount = $v->amount + $tax;
					$amount = round($amount);
				}

			} else {
				$amount = $v->amount;
			}
			if($v['type'] == Transaction::TYPE_MILESTONE_PAYMENT) {
				if ($v['type'] == Transaction::TYPE_MILESTONE_PAYMENT && $v['performed_by'] == Auth::user()->id) {
					$transaction->push(collect([
						'name' => $v->receiver->name,
						'date' => $v->created_at,
						'status_string' => $v->status_string,
						'payment' => $v->amount,
						'payment_type' => $v->payment_type,
						'currency_symbol' => $v->currency_symbol,
						'amount' => $amount,
						'comment' => $v->comment,
						'release_at' => $v->release_at
					]));
				} else if($v['status'] == Transaction::STATUS_APPROVED || $v['status'] == Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM) {
					$transaction->push(collect([
						'name' => $v->receiver->name,
						'date' => $v->created_at,
						'status_string' => $v->status_string,
						'payment' => $v->amount,
						'payment_type' => $v->payment_type,
						'currency_symbol' => $v->currency_symbol,
						'amount' => $amount,
						'comment' => $v->comment,
						'release_at' => $v->release_at
					]));
				}
			} else {
				$transaction->push(collect([
					'name' => $v->receiver->name,
					'date' => $v->created_at,
					'status_string' => $v->status_string,
					'payment' => $v->amount,
					'payment_type' => $v->payment_type,
					'currency_symbol' => $v->currency_symbol,
					'amount' => $amount,
					'comment' => $v->comment,
					'release_at' => $v->release_at
				]));
			}
		}
		$transaction = $transaction->sortByDesc('id')->toArray();
		return \Excel::create(Carbon::now()->toDateTimeString().' export', function($excel) use ($transaction) {
			$excel->sheet('mySheet', function($sheet) use ($transaction) {
				$sheet->fromArray($transaction);
			});
		})->download('csv');

	}
}
