<?php

namespace App\Http\Controllers\Frontend;

use App\Models\UserInbox;
use App\Models\UserInboxMessages;
use Carbon;
use App\Http\Controllers\FrontendController;
use Illuminate\Http\Request;

use Auth;
use App\Models\User;
use App\Models\UserContactPrivacy;

class ModalController extends FrontendController
{
    public function setUsername($user_id){
        $user = User::where('id', $user_id)
            ->first();

        return view('frontend.includes.modalSetUsername',[
            'user' => $user
        ]);
    }

    public function setUsernamePost(Request $request){
        $userId = $request->get('user_id');
        $username = $request->get('username');

        try
        {
            $user = User::find($userId);
            $user->name = $username;
            $user->save();

            return response()->json([
                'status'    	=> 'OK',
                'username'      => $username,
                'user_id'       => $userId
            ]);
        }
        catch(\Exception $e)
        {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function sendPrivateMessage($userId)
    {
        $user = User::find($userId);

        $sender = \Auth::guard('users')->user();
        $receiver = $user;
        $userInbox = null;
        $inboxMessages = null;

        $inbox1 = UserInbox::where('from_user_id', $sender->id)
            ->where('to_user_id', $receiver->id)
            ->where('type', UserInbox::TYPE_PROJECT_NEW_MESSAGE)
            ->where('is_trash', 0)
            ->first();

        $inbox2 = UserInbox::where('from_user_id', $receiver->id)
            ->where('to_user_id', $sender->id)
            ->where('type', UserInbox::TYPE_PROJECT_NEW_MESSAGE)
            ->where('is_trash', 0)
            ->first();

        if ($inbox1)
        {
            $userInbox = $inbox1;
        }

        if ($inbox2)
        {
            $userInbox = $inbox2;
        }

        if ($userInbox)
        {
            $inboxMessages = UserInboxMessages::where('inbox_id', $userInbox->id)
                ->get();
        }

        return view('frontend.includes.modalSendPrivateMessage', [
            'user' => $user,
            'userInbox' => $userInbox,
            'inboxMessages' => $inboxMessages
        ]);
    }

    public function sendPrivateMessagePost($userId, Request $request)
    {

        try {
            \DB::beginTransaction();

            $userInboxId = $request->get('user_inbox_id');
            if( $userInboxId > 0 )
            {
                $userInbox = UserInbox::find($userInboxId);
            }
            else
            {
                //add user inbox notification
                $userInbox = new UserInbox();
                $userInbox->category = UserInbox::CATEGORY_NORMAL;
                $userInbox->project_id = null;
                $userInbox->from_user_id = Auth::id();
                $userInbox->from_staff_id = null;
                $userInbox->to_staff_id = null;
                $userInbox->to_user_id = $userId;
                $userInbox->type = UserInbox::TYPE_PROJECT_NEW_MESSAGE;
                $userInbox->is_trash = 0;
                $userInbox->save();
            }

            //add user inbox message
            $userInboxMessage = new UserInboxMessages();
            $userInboxMessage->inbox_id = $userInbox->id;
            $userInboxMessage->from_user_id = Auth::id();
            $userInboxMessage->to_user_id = $userId;
            $userInboxMessage->from_staff_id = null;
            $userInboxMessage->to_staff_id = null;
            $userInboxMessage->project_id = null;
            $userInboxMessage->is_read = 0;
            $userInboxMessage->type = UserInbox::TYPE_PROJECT_NEW_MESSAGE;

            $message = $request->get('message');
            $userInboxMessage->custom_message = $message;
            $userInboxMessage->quote_price = null;
            $userInboxMessage->pay_type = null;
            $userInboxMessage->file = null;

            $senderName = \Auth::user()->name;
            $email = \Auth::user()->email;

            $senderEmail = "support@xenren.co";

            $receiver = User::find($userId);
            \Mail::send('mails.sendMessageEmail', array( 'sender_name' => $senderName ,'sender_email' => $email , 'sender_message' => $request->message), function($message) use ( $senderEmail, $senderName, $receiver) {
                $message->from($senderEmail, $senderName);
                $message->to($receiver->email, $receiver->name)
                    ->subject(trans('common.message'));
            });

            if ($userInboxMessage->save()) {

                \DB::commit();
                return makeResponse(trans('common.submit_successful'));

            } else {
                \DB::rollback();
                throw new \Exception(trans('common.err_happen'));
            }
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse($e->getMessage(), true);
        }
    }

    public function setContactInfo($user_id){
        $user = User::where('id', $user_id)
            ->first();

        return view('frontend.includes.modalSetContactInfo',[
            'user' => $user
        ]);
    }

    public function setContactInfoPost(Request $request){
        $userId = \Auth::user() && \Auth::user()->id ? \Auth::user()->id : $request->get('user_id');
        $email = \Auth::user() && \Auth::user()->email ? \Auth::user()->email :$request->get('email');
        try
        {
            $user = User::find($userId);
            if( $email != "" )
            {
                if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    // valid address
                    $user->email = $email;
                }
                else {
                    // invalid address
                    return makeResponse(trans('validation.email', ['attribute' => trans('common.email')]), true);
                }
            }
            else
            {
                //must fill input
                return makeResponse(trans('validation.required', ['attribute' => trans('common.email')]), true);
            }

            if($request->get('handphone_no'))
            {
                $user->handphone_no = $request->get('handphone_no');
            }
            if($request->get('skype_id'))
            {
                $user->skype_id = $request->get('skype_id');
            }
            if($request->get('wechat_id'))
            {
                $user->wechat_id = $request->get('wechat_id');
            }
//            if($request->get('qq_id'))
//            {
//                $user->qq_id = $request->get('qq_id');
//            }
            if($request->get('line_id'))
            {
                $user->line_id = $request->get('line_id');
            }
            if($request->get('is_contact_allow_view'))
            {
                $user->is_contact_allow_view = $request->get('is_contact_allow_view');
            }
            $user->save();

            return response()->json([
                'status'    	=> 'OK',
                'user_id'       => $userId
            ]);
        }
        catch(\Exception $e)
        {
            return makeResponse($e->getMessage(), true);
        }
    }///;


//    public function userContactQQ($data)
//    {
//        $check = UserContactPrivacy::where('user_id','=',\Auth::user()->id)->first();
//        if(is_null($check)) {
//            UserContactPrivacy::create([
//                'user_id' => \Auth::user()->id,
//                'qq' => $data,
//            ]);
//        } else {
//	          $check->update([
//                'user_id' => \Auth::user()->id,
//                'qq' => $data,
//            ]);
//        }
//    }

    public function userContactline($data)
    {
        $check = UserContactPrivacy::where('user_id','=',\Auth::user()->id)->first();
        if(is_null($check)) {
            UserContactPrivacy::create([
                'user_id' => \Auth::user()->id,
                'line' => $data,
            ]);
        } else {
	          $check->update([
                'user_id' => \Auth::user()->id,
                'line' => $data,
            ]);
        }
    }

    public function userContactWeChat($data)
    {
        $check = UserContactPrivacy::where('user_id','=',\Auth::user()->id)->first();
        if(is_null($check)) {
           UserContactPrivacy::create([
                'user_id' => \Auth::user()->id,
                'weChat' => $data,
            ]);
        } else {
	          $check->update([
                'weChat' => $data,
            ]);
        }
    }

    public function userContactSkype($data)
    {
        $check = UserContactPrivacy::where('user_id','=',\Auth::user()->id)->first();
        if(is_null($check)) {
            UserContactPrivacy::create([
                'user_id' => \Auth::user()->id,
                'skype' => $data,
            ]);
        } else {
	        $check->update([
                'skype' => $data,
            ]);
        }
    }

    public function userContactPhoneii($data)
    {
        $check = UserContactPrivacy::where('user_id','=',\Auth::user()->id)->first();
        if(is_null($check)) {
            UserContactPrivacy::create([
                'user_id' => \Auth::user()->id,
                'phone' => $data,
            ]);
        } else {
            $check->update([
                'phone' => $data,
            ]);
        }
    }

    public function getContacts(){
      return UserContactPrivacy::where('user_id','=',\Auth::user()->id)->first();
    }

}