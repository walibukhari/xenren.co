<?php

namespace App\Http\Controllers\Frontend;

use App\Constants;
use App\FreelancerReview;
use App\Http\Controllers\FrontendController;
use App\Models\EmployerReview;
use App\Models\Project;
use App\Models\ProjectApplicant;
use App\Models\ProjectSkill;
use App\Models\Review;
use App\Models\ReviewSkill;
use App\Models\User;
use App\Models\UserInbox;
use App\Models\UserInboxMessages;
use App\Models\UserSkill;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EndContractController extends FrontendController
{
    public function getFreelancerForm($projectApplicant_id)
    {
        $projectApplicant = ProjectApplicant::with('user', 'project')->find($projectApplicant_id);
        $project = Project::with('creator', 'officialProject', 'review')->find($projectApplicant->project->id);

        if (Auth::id() == $projectApplicant->user->id) {
            if ($project->type == Project::PROJECT_TYPE_COMMON) {
                $who = $project->creator->getName();
            } else if ($project->type == Project::PROJECT_TYPE_OFFICIAL) {
                $who = User::find(1)->getName();
            }

            $projectName = $project->name;

            $payType = Constants::translatePayType($project->pay_type);
            $currency = Constants::currency(Constants::USD);

            $payType = trans('common.pay_type_display', [
                'type' => $payType,
                'currency' => $currency,
                'amount' => $project->reference_price
            ]);

            $reasons = Review::getEmployerReasonLists();
            $englishProficiencies = Review::getEnglishProficiencyLists();
            $rateCategories = Review::getEmployerRateCategoryLists();

            return view('frontend.endContract.endContractFreelancer', [
                'projectApplicant' => $projectApplicant,
                'project' => $project,
                'who' => $who,
                'projectName' => $projectName,
                'payType' => $payType,
                'reasons' => $reasons,
                'englishProficiencies' => $englishProficiencies,
                'rateCategories' => $rateCategories
            ]);
        }
    }

    public function postFreelancerForm(Request $request)
    {

        $this->validate($request, [
            'reason' => 'required|numeric',
            'recommendation' => 'required|numeric',
            'english_proficiency' => 'required|string',
            'rate_type_1' => 'required|numeric',
            'rate_type_2' => 'required|numeric',
            'rate_type_3' => 'required|numeric',
            'rate_type_4' => 'required|numeric',
            'rate_type_5' => 'required|numeric',
            'total_score' => '',
            'experience' => '',
        ]);

        try {
            DB::beginTransaction();

            $projectApplicantId = $request->input('projectApplicantId');
            $projectId = $request->input('projectId');
            $reason = $request->input('reason');
            $recommendation = $request->input('recommendation');
            $proficiency = $request->input('english_proficiency');
            $payment_on_time = $request->input('rate_type_' . Review::RATE_PAYMENT_ON_TIME);
            $communication = $request->input('rate_type_' . Review::RATE_COMMUNICATION);
            $availability = $request->input('rate_type_' . Review::RATE_AVAILABILITY);
            $trustworthy = $request->input('rate_type_' . Review::RATE_TRUSTWORTHY);
            $clear_requirement = $request->input('rate_type_' . Review::RATE_CLEAR_REQUIREMENT);
            $total_score = $request->input('total_score');
            $experience = $request->input('experience');

            $project = Project::find($projectId);

            $review = new Review();
            $review->type = Review::TYPE_EMPLOYER_REVIEW;
            if ($project->type == Project::PROJECT_TYPE_COMMON) {
                $review->from_who_to_who = Review::FWTW_USER_TO_USER;
            } else if ($project->type == Project::PROJECT_TYPE_OFFICIAL) {
                $review->from_who_to_who = Review::FWTW_USER_TO_ADMIN;
            }

            $projectApplicant = ProjectApplicant::find($projectApplicantId);

            if ($project->type == Project::PROJECT_TYPE_COMMON) {
                $review->reviewer_user_id = \Auth::id();
                $review->reviewer_staff_id = null;
                $review->reviewee_user_id = $project->creator->id;
                $review->reviewee_staff_id = null;
            } else {
                $review->reviewer_user_id = \Auth::id();
                $review->reviewer_staff_id = null;
                $review->reviewee_user_id = null;
                $review->reviewee_staff_id = $project->creator->id;
            }

            $review->project_id = $projectId;
            $review->reason = $reason;
            $review->recommendation = $recommendation;
            $review->proficiency = $proficiency;
            $review->payment_on_time = $payment_on_time;
            $review->communication = $communication;
            $review->availability = $availability;
            $review->trustworthy = $trustworthy;
            $review->clear_requirement = $clear_requirement;
            $review->total_score = $total_score;
            $review->experience = $experience;

            $data = Review::where('project_id', '=', $projectId)->where('reviewee_user_id', '=', $review->reviewee_user_id)->where('reviewer_user_id', '=', \Auth::id())->get();
            if (isset($data) && count($data) > 0) {
                return makeResponse('Employer and employee only can take one time review for each project.', true);
            }

            $review->save();
            $projectApplicant->status = ProjectApplicant::STATUS_COMPLETED;
            $projectApplicant->display_order = ProjectApplicant::DISPLAY_ORDER_COMPLETED;
            $projectApplicant->is_read = ProjectApplicant::IS_READ_NO;
            $projectApplicant->save();

            $ui = new UserInbox();
            $ui->category = UserInbox::CATEGORY_NORMAL;
            $ui->project_id = $project->id;

            if ($project->type == Project::PROJECT_TYPE_COMMON) {
                $ui->from_user_id = \Auth::id();
                $ui->to_user_id = $project->creator->id;
                $ui->from_staff_id = null;
                $ui->to_staff_id = null;
            } else {
                $ui->from_user_id = \Auth::id();
                $ui->to_user_id = null;
                $ui->from_staff_id = null;
                $ui->to_staff_id = $project->creator->id;
            }

            $ui->type = UserInbox::TYPE_PROJECT_CONFIRMED_COMPLETED;
            $ui->is_trash = 0;
            $ui->save();

            $uim = new UserInboxMessages();
            $uim->inbox_id = $ui->id;
            if ($project->type == Project::PROJECT_TYPE_COMMON) {
                $uim->from_user_id = \Auth::id();
                $uim->to_user_id = $project->creator->id;
                $uim->from_staff_id = null;
                $uim->to_staff_id = null;
            } else {
                $uim->from_user_id = \Auth::id();
                $uim->to_user_id = null;
                $uim->from_staff_id = null;
                $uim->to_staff_id = $project->creator->id;
            }

            $uim->project_id = $projectId;
            $uim->is_read = 0;
            $uim->type = UserInboxMessages::TYPE_PROJECT_CONFIRMED_COMPLETED;
            $uim->custom_message = null;
            $uim->quote_price = null;
            $uim->pay_type = null;
            $uim->save();

            $project = Project::find($projectId);
            $project->is_read = Project::IS_READ_NO;
            $project->save();

            DB::commit();
            return makeResponse(trans('common.review_submit_success'), false);

        } catch (\Exception $e) {

            DB::rollback();
            return makeResponse($e->getMessage(), true);
        }

    }

    public function getEmployerForm($projectApplicant_id)
    {
        $projectIdAndApplicatntId = explode('-', $projectApplicant_id);
        $projectApplicant_id = $projectIdAndApplicatntId[0];
        $project_id = $projectIdAndApplicatntId[1];

        $projectApplicant = ProjectApplicant::with('user', 'project')->where('user_id', '=', $projectApplicant_id)->where('project_id', $project_id)->first();
        $project = $projectApplicant->project;

        if (Auth::id() == $project->user_id) {

            $user = $projectApplicant->user;
            $who = $user->getName();
            $projectName = $project->name;

            $payType = Constants::translatePayType($project->pay_type);
            $currency = Constants::currency(Constants::USD);

            $payType = trans('common.pay_type_display', [
                'type' => $payType,
                'currency' => $currency,
                'amount' => $project->reference_price
            ]);

            $reasons = Review::getFreelancerReasonLists();
            $englishProficiencies = Review::getEnglishProficiencyLists();
            $rateCategories = Review::getFreelancerRateCategoryLists();

            $projectSkills = ProjectSkill::where('project_id', $project->id)
                ->get();

            return view('frontend.endContract.endContractEmployer', [
                'projectApplicant' => $projectApplicant,
                'project' => $project,
                'who' => $who,
                'projectName' => $projectName,
                'payType' => $payType,
                'reasons' => $reasons,
                'englishProficiencies' => $englishProficiencies,
                'rateCategories' => $rateCategories,
                'projectSkills' => $projectSkills
            ]);
        }
    }

    public function postEmployerForm(Request $request)
    {
        $this->validate($request, [
            'reason' => 'required|numeric',
            'review_skills' => 'required',
            'recommendation' => 'required|numeric',
            'english_proficiency' => 'required|string',
            'rate_type_' . Review::RATE_SKILL => 'required|numeric',
            'rate_type_' . Review::RATE_QUALITY_OF_WORK => 'required|numeric',
            'rate_type_' . Review::RATE_AVAILABILITY => 'required|numeric',
            'rate_type_' . Review::RATE_ADHERENCE_TO_SCHEDULE => 'required|numeric',
            'rate_type_' . Review::RATE_COMMUNICATION => 'required|numeric',
            'rate_type_' . Review::RATE_COOPERATION => 'required|numeric',
            'total_score' => '',
            'experience' => 'required|string',
        ]);

        try {
            DB::beginTransaction();

            $projectApplicantId = $request->input('projectApplicantId');
            $projectId = $request->input('projectId');
            $reason = $request->input('reason');
            $recommendation = $request->input('recommendation');
            $proficiency = $request->input('english_proficiency');
            $skill = $request->input('rate_type_' . Review::RATE_SKILL);
            $quality_of_work = $request->input('rate_type_' . Review::RATE_QUALITY_OF_WORK);
            $availability = $request->input('rate_type_' . Review::RATE_AVAILABILITY);
            $adherence_to_schedule = $request->input('rate_type_' . Review::RATE_ADHERENCE_TO_SCHEDULE);
            $communication = $request->input('rate_type_' . Review::RATE_COMMUNICATION);
            $cooperation = $request->input('rate_type_' . Review::RATE_COOPERATION);
            $total_score = $request->input('total_score');
            $experience = $request->input('experience');
            $reviewSkills = $request->input('review_skills');
            $projectApplicant = ProjectApplicant::find($projectApplicantId);

            //check whether record already exist or not
            $count = Review::where('reviewer_user_id', Auth::id())
                ->where('reviewee_user_id', $projectApplicant->user->id)
                ->where('project_id', $projectId)
                ->count();
            if ($count >= 1) {
                return makeResponse(trans('common.only_can_take_one_time_review'), true);
            }

            $review = new Review();
            $review->type = Review::TYPE_FREELANCER_REVIEW;
            $review->from_who_to_who = Review::FWTW_USER_TO_USER;
            $review->reviewer_user_id = Auth::id();
            $review->reviewer_staff_id = null;
            $review->reviewee_user_id = $projectApplicant->user->id;
            $review->reviewee_staff_id = null;
            $review->project_id = $projectId;
            $review->reason = $reason;
            $review->recommendation = $recommendation;
            $review->proficiency = $proficiency;
            $review->skils = $skill;
            $review->quality_work = $quality_of_work;
            $review->availability = $availability;
            $review->adherence_to_schedule = $adherence_to_schedule;
            $review->communication = $communication;
            $review->cooperation = $cooperation;
            $review->total_score = $total_score;
            $review->experience = $experience;
            $review->save();

            $reviewSkillList = explode(',', $reviewSkills);
            foreach ($reviewSkillList as $skill_id) {
                $reviewSkill = new ReviewSkill();
                $reviewSkill->review_id = $review->id;
                $reviewSkill->skill_id = $skill_id;
                $reviewSkill->save();
            }

            $projectApplicant->status = ProjectApplicant::STATUS_COMPLETED;
            $projectApplicant->display_order = ProjectApplicant::DISPLAY_ORDER_COMPLETED;
            $projectApplicant->is_read = ProjectApplicant::IS_READ_NO;
            $projectApplicant->save();

            $ui = new UserInbox();
            $ui->category = UserInbox::CATEGORY_SYSTEM;
            $ui->project_id = $projectId;
            $ui->from_user_id = Auth::id();
            $ui->to_user_id = $projectApplicant->user->id;
            $ui->from_staff_id = null;
            $ui->to_staff_id = null;
            $ui->type = UserInbox::TYPE_PROJECT_CONFIRMED_COMPLETED;
            $ui->is_trash = 0;
            $ui->save();

            $uim = new UserInboxMessages();
            $uim->inbox_id = $ui->id;
            $uim->from_user_id = Auth::id();
            $uim->to_user_id = $projectApplicant->user->id;
            $uim->from_staff_id = null;
            $uim->to_staff_id = null;
            $uim->project_id = $projectId;
            $uim->is_read = 0;
            $uim->type = UserInboxMessages::TYPE_PROJECT_CONFIRMED_COMPLETED;
            $uim->custom_message = null;
            $uim->quote_price = null;
            $uim->pay_type = null;
            $uim->save();

            $project = Project::find($projectId);
            $project->is_read = Project::IS_READ_NO;
            $project->save();

            $reviewSkillIDs = explode(',', $reviewSkills);
            foreach ($reviewSkillIDs as $reviewSkillID) {
                $userSkill = UserSkill::where('user_id', $projectApplicant->user->id)
                    ->where('skill_id', $reviewSkillID)
                    ->first();

                if (!is_null($userSkill)) {
                    $userSkill->is_client_verified = 1;
                    $userSkill->save();
                } else {
                    //if user don't have this skill in personal info
                    //client review response good, auto add new skill
                    $addNewUserSkill = new UserSkill();
                    $addNewUserSkill->user_id = $projectApplicant->user->id;
                    $addNewUserSkill->skill_id = $reviewSkillID;
                    $addNewUserSkill->experience = 0; //this one for admin review
                    $addNewUserSkill->is_client_verified = 1;
                    $addNewUserSkill->save();
                }
            }

            DB::commit();
            return makeResponse(trans('common.review_submit_success'), false);

        } catch (\Exception $e) {

            DB::rollback();
            return makeResponse($e->getMessage(), true);
        }
    }
}
