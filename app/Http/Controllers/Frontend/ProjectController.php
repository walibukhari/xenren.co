<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Project;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    public function switchFixed(Request $request, $projectId)
    {
        Project::where('id', '=', $projectId)->update([
            'pay_type' => $request->status
        ]);
        return collect([
            'status' => 'success',
            'message' => 'Project Updated..!'
        ]);
    }
}
