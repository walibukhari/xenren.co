<?php

namespace App\Http\Controllers\Frontend;

use App\Models\ProjectChatRoom;
use App\Traits\PredisTrait;
use Carbon;
use App\Http\Controllers\FrontendController;
use App\Http\Requests\Frontend\UserChatMessageCreateFormRequest;
use App\Http\Requests\Frontend\UserChatMessageCreateProjectFormRequest;
use App\Http\Requests\Frontend\UserChatMessageCreateProjectFileFormRequest;
use App\Http\Requests\Frontend\UserChatMessageCreateProjectImageFormRequest;
use Illuminate\Http\Request;
use App\Models\ProjectChatMessage;
use App\Models\UserInbox;
use App\Models\UserInboxMessages;
use App\Models\User;
use App\Models\Project;
use App\Models\ProjectApplicant;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\View;

class ProjectChatController extends FrontendController
{
    use PredisTrait;

    public static function pullChat($userRelative = null)
    {
        $chatLog = ProjectChatMessage::pullChatMessage($userRelative);
        return response()->json([
            'status' => 'OK',
            'receiverId' => $chatLog['receiverId'],
            'realName' => $chatLog['realName'],
            'chatLog' => $chatLog['lists']
        ]);
    }

    public function index(Request $request)
    {
        $userToInvite = \App\Models\User::GetUsers()
            ->where('id', '!=', \Auth::guard('users')->user()->id)->get();
        $tmpUsers = array();
        foreach ($userToInvite as $key => $var) {
            $realName = $var->email;
            $userRel = explode('@', $var->email);
            if (count($userRel) >= 2) {
                $realName = $userRel[0];
            }
            $tmpUsers[$var->id]['name'] = $realName;
        }
        //dummy for invite user

        $recentChat = ProjectChatMessage::getRecentChat();
        return view('frontend.myChat.index', [
            'recentChat' => $recentChat,
            'userToInvite' => $tmpUsers
        ]);
    }

    public function postChat(UserChatMessageCreateFormRequest $request)
    {
        $ProjectChatMessage = new ProjectChatMessage();
        $ProjectChatMessage->sender_user_id = \Auth::guard('users')->user()->id;
        $ProjectChatMessage->receiver_user_id = $request->get('receiver');
        $ProjectChatMessage->message = $request->get('message');

        try {
            $ProjectChatMessage->save();
            $userRel = \Auth::guard('users')->user()->email;
            $realName = $userRel;
            $userRel = explode('@', $userRel);
            if (count($userRel) >= 2) {
                $realName = $userRel[0];
            }
            return response()->json([
                'status' => 'OK',
                'message' => $ProjectChatMessage->message,
                'time' => $ProjectChatMessage->created_at,
                'realName' => $realName
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => 'not-OK',
                'message' => $e->getMessage()
            ]);
        }
    }

    public static function pullProjectChat($projectChatRoomId = null)
    {
        $chatLog = ProjectChatMessage::pullChatProjectMessage($projectChatRoomId);
        return response()->json([
            'status' => 'OK',
            'chatLog' => $chatLog['lists']
        ]);
    }

    public function postProjectChat(UserChatMessageCreateProjectFormRequest $request)
    {
        $ProjectChatMessage = new ProjectChatMessage();
        $ProjectChatMessage->sender_user_id = \Auth::guard('users')->user()->id;
        $ProjectChatMessage->project_chat_room_id = $request->get('projectChatRoomId');
        $ProjectChatMessage->message = $request->get('message');

        $projectChatRoom = ProjectChatRoom::find($ProjectChatMessage->project_chat_room_id);
        $projectID = $projectChatRoom->project_id;

        //for my publish order is read logic
        $project = Project::find($projectID);
        if ($project->user_id != \Auth::guard('users')->user()->id) {
            $project->is_read = Project::IS_READ_NO;
            $project->save();
        }

        //for my receive order is read logic
        $projectApplicants = ProjectApplicant::where('project_id', $projectID)
            ->get();
        foreach ($projectApplicants as $projectApplicant) {
            $projectApplicant->is_read = ProjectApplicant::IS_READ_NO;
            $projectApplicant->save();
        }

        try {
            \DB::beginTransaction();

//            //Here we create user inbox message
//            $order = Order::with(['orderInbox'])->JoinApprovedApplicant()->find($request->get('projectId'));
//            if (\Auth::guard('users')->user()->id == $order->creator_id) {
//                $ui = UserInbox::where('to_user_id', '=', $order->applicant_user_id);
//            } else {
//                $ui = UserInbox::where('to_user_id', '=', $order->creator_id);
//            }
//
//            if ($ui->exists()) {
//                $ui = $order->orderInbox;
//
//                $uim_exists = UserInboxMessages::where('from_user_id', '=', \Auth::guard('users')->user()->id)->where('type', '=', UserInboxMessages::TYPE_ORDER_NEW_MESSAGE)->where('project_id', '=', $order->id);
//
//                if ($uim_exists->exists()) {
//                    $uim = $uim_exists->first();
//                    $uim->updated_at = new \Carbon\Carbon();
//                } else {
//                    $uim = new UserInboxMessages();
//                    $uim->updated_at = new \Carbon\Carbon();
//                    $uim->from_user_id = \Auth::guard('users')->user()->id;
//                    if (\Auth::guard('users')->user()->id == $order->creator_id) {
//                        $uim->to_user_id = $order->applicant_user_id;
//                    } else {
//                        $uim->to_user_id = $order->creator_id;
//                    }
//                    $uim->type = UserInboxMessages::TYPE_ORDER_NEW_MESSAGE;
//                }
//            } else {
//                //Here we send user inbox
//                $ui = new UserInbox();
//                $ui->from_user_id = \Auth::guard('users')->user()->id;
//                if (\Auth::guard('users')->user()->id == $order->creator_id) {
//                    $ui->to_user_id = $order->applicant_user_id;
//                } else {
//                    $ui->to_user_id = $order->creator_id;
//                }
//                $ui->type = UserInbox::TYPE_ORDER_APPLICANT_SELECTED;
//
//                //Here we create user inbox message
//                $uim = new UserInboxMessages();
//                $uim->from_user_id = \Auth::guard('users')->user()->id;
//                if (\Auth::guard('users')->user()->id == $order->creator_id) {
//                    $uim->to_user_id = $order->applicant_user_id;
//                } else {
//                    $uim->to_user_id = $order->creator_id;
//                }
//                $uim->type = UserInboxMessages::TYPE_ORDER_NEW_MESSAGE;
//            }
//
//            $u = User::find($uim->to_user_id);
//
//            if (!$u) {
//                throw new \Exception(trans('common.unknown_error'));
//            }
//
//            $u->unread_message = UserInboxMessages::where('to_user_id', '=', $u->id)->where('is_read', '=', 0)->count() + 1;
//            if (!$u->save()) {
//                throw new \Exception(trans('common.unknown_error'));
//            }
//
//            $ui->project_id = $order->id;
//            if (!$ui->save()) {
//                throw new \Exception(trans('common.unknown_error'));
//            }
//
//            $uim->is_read = 0;
//            $uim->project_id = $order->id;
//            $uim->inbox_id = $ui->id;
//            if (!$uim->save()) {
//                throw new \Exception(trans('common.unknown_error'));
//            }

            $ProjectChatMessage->save();

            $this->redisPublish($ProjectChatMessage->id);

            if (\Auth::guard('users')->user()->name != '') {
                $name = \Auth::guard('users')->user()->name;
            } else {
                $email = \Auth::guard('users')->user()->email;
                $name = explode('@', $email);
                if (count($name) >= 2) {
                    $name = $name[0];
                } else {
                    $name = $email;
                }
            }
            $avatar = \Auth::guard('users')->user()->getAvatar();
            \DB::commit();
            return response()->json([
                'status' => 'OK',
                'message' => $ProjectChatMessage->message,
                'time' => date('d M, Y H:i', strtotime($ProjectChatMessage->created_at)),
                'realName' => $name,
                'avatar' => $avatar
            ]);
        } catch (Exception $e) {
            \DB::rollback();
            return response()->json([
                'status' => 'not-OK',
                'message' => $e->getMessage()
            ]);
        }
    }

    function redisPublish($id)
    {
        $message = ProjectChatMessage::find($id);
        $chat['project_chat_room_id'] = $message->project_chat_room_id;
        if ($message->SendBy()->first()->name != '') {
            $name = $message->SendBy()->first()->name;
        } else {
            $email = $message->SendBy()->first()->email;
            $name = explode('@', $email);
            if (count($name) >= 2) {
                $name = $name[0];
            } else {
                $name = $email;
            }
        }
        $chat['id'] = $message->id;
        $chat['sender_user_id'] = $message->sender_user_id;
        $chat['name'] = $name;
        $chat['avatar'] = $message->sendBy()->first()->getAvatar();
        $chat['email'] = $name;
        $chat['message'] = $message->message;
        $chat['content'] = $message->message;
        $chat['time'] = date('d M, Y H:i', strtotime($message->created_at));
        $chat['status'] = "out";
        $chat['sender_user_id'] = $message->sender_user_id;
        $chat['date'] = date('Y-m-d', strtotime($message->created_at));
        $chat['file'] = $message->file;
        $isImage = 0;

        if (file_exists($message->file)) {
            if (strpos($message->file, '.txt') !== false || strpos($message->file, '.doc') !== false) {
                $chat['content'] = str_replace('--file upload: ', '', '<a href="' . $message->getFile() . '">' . $message->message . '<a>');
                $isImage = 0;
            } else {
                $a = getimagesize($message->file);
                $image_type = $a[2];
                if (in_array($image_type, array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG, IMAGETYPE_BMP))) {
                    $chat['content'] = str_replace('--file upload: ', '', $message->message);
                    $isImage = 1;
                }
            }
        }
        $chat['isImage'] = $isImage;

        $isAudio = 0;
        if (file_exists($message->file)) {
            $allowed = array(
                'audio/mpeg', 'audio/x-mpeg', 'audio/mpeg3', 'audio/x-mpeg-3', 'audio/aiff',
                'audio/mid', 'audio/x-aiff', 'audio/x-mpequrl', 'audio/midi', 'audio/x-mid',
                'audio/x-midi', 'audio/wav', 'audio/x-wav', 'audio/xm', 'audio/x-aac', 'audio/basic',
                'audio/flac', 'audio/mp4', 'audio/x-matroska', 'audio/ogg', 'audio/s3m', 'audio/x-ms-wax',
                'audio/xm', 'application/ogg', 'application/mp3', 'audio/mp3'
            );

            // check REAL MIME type
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $type = finfo_file($finfo, $message->file);
            finfo_close($finfo);

            // check to see if REAL MIME type is inside $allowed array
            if (in_array($type, $allowed)) {
                $isAudio = 1;
            }
        }
        $chat['isAudio'] = $isAudio;
        $data = collect([
            'chat' => $isAudio,
            'type' => 'chat'
        ]);
        Redis::publish('default_room', \GuzzleHttp\json_encode($data));
    }

    public function postInviteHelpdeskChat($projectChatRoomId = null)
    {
        $ProjectChatMessage = new ProjectChatMessage();
        $ProjectChatMessage->sender_user_id = \Auth::guard('users')->user()->id;
        $ProjectChatMessage->receiver_staff_id = 1;
        $ProjectChatMessage->project_chat_room_id = $projectChatRoomId;

//        if(\Auth::guard('users')->user()->real_name != ''){
//            $name = \Auth::guard('users')->user()->real_name;
//        }else if(\Auth::guard('users')->user()->nick_name != ''){
//            $name = \Auth::guard('users')->user()->real_name;
//        }else{
//            $email      = \Auth::guard('users')->user()->email;
//            $name       = explode('@', $email);
//            if(count($name) >= 2){
//                $name = $name[0];
//            }else{
//                $name = $email;
//            }
//        }

        if (\Auth::guard('users')->user()->name != '') {
            $name = \Auth::guard('users')->user()->name;
        } else {
            $email = \Auth::guard('users')->user()->email;
            $name = explode('@', $email);
            if (count($name) >= 2) {
                $name = $name[0];
            } else {
                $name = $email;
            }
        }

        $ProjectChatMessage->message = '--' . $name . ' invite help desk to this conversation--';

        try {
            $ProjectChatMessage->save();
            $avatar = \Auth::guard('users')->user()->getAvatar();

            return response()->json([
                'status' => 'OK',
                'message' => $ProjectChatMessage->message,
                'time' => date('d M, Y H:i', strtotime($ProjectChatMessage->created_at)),
                'realName' => $name,
                'avatar' => $avatar
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => 'not-OK',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function postProjectChatFile(UserChatMessageCreateProjectFileFormRequest $request)
    {
        $ProjectChatMessage = new ProjectChatMessage();
        $ProjectChatMessage->sender_user_id = \Auth::guard('users')->user()->id;
        $ProjectChatMessage->project_chat_room_id = $request->get('projectChatRoomId');
        $ProjectChatMessage->message = '--file upload: ' . $request->get('message');
        $ProjectChatMessage->file = $request->file('file');

        $projectChatRoom = ProjectChatRoom::find($ProjectChatMessage->project_chat_room_id);
        $projectID = $projectChatRoom->project_id;

        //for my publish order is read logic
        $project = Project::find($projectID);
        if ($project->user_id != \Auth::guard('users')->user()->id) {
            $project->is_read = Project::IS_READ_NO;
            $project->save();
        }

        //for my receive order is read logic
        $projectApplicants = ProjectApplicant::where('project_id', $projectID)
            ->get();
        foreach ($projectApplicants as $projectApplicant) {
            $projectApplicant->is_read = ProjectApplicant::IS_READ_NO;
            $projectApplicant->save();
        }

        try {
            $ProjectChatMessage->save();

            $this->redisPublish($ProjectChatMessage->id);

            //get html content for chat row
            $view = View::make('frontend.joinDiscussion.ucChatItem', [
                'message' => str_replace("--file upload: ", "", $ProjectChatMessage->message),
                'time' => date('d M, Y H:i', strtotime($ProjectChatMessage->created_at)),
                'realName' => \Auth::guard('users')->user()->getName(),
                'avatar' => \Auth::guard('users')->user()->getAvatar(),
                'file' => $ProjectChatMessage->getFile()
            ]);
            $contents = $view->render();

            return response()->json([
                'status' => 'OK',
                'contents' => $contents
            ]);

        } catch (Exception $e) {
            return response()->json([
                'status' => 'not-OK',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function pullProjectChatFile($projectId = null)
    {
        $chatLog = ProjectChatMessage::pullChatOrderMessageFile($projectId);
        return response()->json([
            'status' => 'OK',
            'chatLog' => $chatLog['lists'],
            'count' => $chatLog['count']
        ]);
    }

    public function postProjectChatImage(UserChatMessageCreateProjectImageFormRequest $request)
    {
        $ProjectChatMessage = new ProjectChatMessage();
        $ProjectChatMessage->sender_user_id = \Auth::guard('users')->user()->id;
        $ProjectChatMessage->project_chat_room_id = $request->get('projectChatRoomId');
        $ProjectChatMessage->message = $request->get('message');
        $ProjectChatMessage->file = $request->file('file');

        $projectChatRoom = ProjectChatRoom::find($ProjectChatMessage->project_chat_room_id);
        $projectID = $projectChatRoom->project_id;

        //for my publish order is read logic
        $project = Project::find($projectID);
        if ($project->user_id != \Auth::guard('users')->user()->id) {
            $project->is_read = Project::IS_READ_NO;
            $project->save();
        }

        //for my receive order is read logic
        $projectApplicants = ProjectApplicant::where('project_id', $projectID)
            ->get();
        foreach ($projectApplicants as $projectApplicant) {
            $projectApplicant->is_read = ProjectApplicant::IS_READ_NO;
            $projectApplicant->save();
        }

        try {
            $ProjectChatMessage->save();
            $this->redisPublish($ProjectChatMessage->id);
            return response()->json([
                'status' => 'OK'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => 'not-OK',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function getLastMessage(Request $request)
    {
        $msg = ProjectChatMessage::where('project_chat_room_id', '=', $request->id)
            ->where('sender_user_id', '=', \Auth::user()->id)
            ->select(['id', 'message'])
            ->orderBy('id', 'desc')
            ->first();
        if (!is_null($msg)) {
            return collect([
                'status' => 'success',
                'id' => $msg->id,
                'chatroomid' => $request->id,
                'message' => $msg->message
            ]);
        } else {
            return collect([
                'status' => 'error',
                'message' => 'empty message'
            ]);
        }
    }

    public function updateLastMessage(Request $request)
    {
        $msg = ProjectChatMessage::where('id', '=', $request->id)
            ->update([
                'message' => $request->message
            ]);
        if (!is_null($msg)) {
            return collect([
                'status' => 'success',
            ]);
        } else {
            return collect([
                'status' => 'success',
            ]);
        }
    }

    public function sendMessage(Request $request)
    {
        ProjectChatMessage::create([
            'project_chat_room_id' => $request->projectChatRoomId,
            'sender_user_id' => \Auth::user()->id,
            'message' => $request->message
        ]);
        self::trigger($request);
        return view('frontend.joinDiscussion.ChatItem', [
            'data' => ProjectChatMessage::pullChatProjectMessage($request->projectChatRoomId)['lists']
        ]);
    }

    public function trigger(Request $request)
    {
        $this->pushToRedis('project_chat', $request->all(), 'global-channel');
//        $channel = 'project_chat';
//        $event = $request['event'];
//        $pusher = App::make('pusher');
//        $pusher->trigger($channel,
//            $event,
//            array(
//                'text' => $request['message'] . 'by: ' . \Auth::user()->name,
//                'name' => \Auth::user()->name,
//                'sender_id' => \Auth::user()->id
//            ));
//
//        return collect([
//            'data' => 'success'
//        ]);
    }

    public function pullProjectChats(Request $request)
    {
        return view('frontend.joinDiscussion.ChatItem', [
            'data' => ProjectChatMessage::pullChatProjectMessage($request->projectChatRoomId)['lists']
        ]);
    }
}
