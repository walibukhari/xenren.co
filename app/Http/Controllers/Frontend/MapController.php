<?php

namespace App\Http\Controllers\Frontend;

use App\Models\OfficialProject;
use Carbon;
use App\Http\Controllers\FrontendController;
use Illuminate\Http\Request;

use Auth;
use App\Models\User;

class MapController extends FrontendController
{
    public function index($user_id){
        $user = User::where('id', $user_id)->first();

        return view('frontend.map.index',[
            'address' => $user->address
        ]);
    }

    public function searchByKeyword(){
        return view('frontend.map.searchByKeyword');
    }

    public function searchByAddress(){
        return view('frontend.map.searchByAddress');
    }

    public function officialProjectLocation($official_project_id)
    {
        $officialProject = OfficialProject::where('id', $official_project_id)->first();

        return view('frontend.map.index',[
            'address' => $officialProject->location_address
        ]);
    }
}