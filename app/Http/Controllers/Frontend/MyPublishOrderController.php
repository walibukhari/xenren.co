<?php

namespace App\Http\Controllers\Frontend;

use App\Constants;
use App\Models\Project;
use App\Models\ProjectApplicant;
use App\Models\ProjectMilestones;
use App\Models\ProjectSkill;
use App\Models\Settings;
use App\Models\Skill;
use App\Models\ExchangeRate;
use App\Models\UserInbox;
use Carbon, Session;
use App\Http\Controllers\FrontendController;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use DB;

class MyPublishOrderController extends FrontendController
{


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $milestones = ProjectMilestones::with('milestoneStatus')->get();
        $projects = Project::with([
            'applicants',
            'awardedApplicant',
            'projectChatRoom.chatFollower',
            'milestones.milestoneStatus',
            'userInbox'
        ])
            ->where('user_id', '=', \Auth::user()->id)
            ->where('type', '=', Project::PROJECT_TYPE_COMMON)
            ->orderBy('id', 'desc');
        $count = isset($request->viewAll) ? $projects->get()->count() : 10;
        $projects = $projects->paginate($count, ['*'], 'published_projects');
        $lastNumberDayOptions = Constants::getLastNumberDaysList();

        if (isset($request->lastNumberDays) && $request->lastNumberDays != 0) {
            $projects = self::daysFilter($projects, $request->lastNumberDays);
        }

        if (isset($request->status)) {
            $projects = self::statusFilter($projects, $request->status);
        }

        $countAwarded = isset($request->viewAllAwarded) ? ProjectApplicant::count() : 10;

        $awardedProjects = ProjectApplicant::with([
            'project',
            'user',
            'userInbox' => function ($q) {
                $q->where('type', '=', 10);
            },
            'awardedUserInbox' => function ($q) {
                $q->where('category', '=', UserInbox::CATEGORY_NORMAL);
            },
            'userInbox.firstMessage'
        ])
            ->has('user')
            ->wherehas('project', function ($q) {
                $q->where('user_id', '=', \Auth::user()->id);
            })
            ->wherehas('awardedUserInbox')
            ->orderBy('id', 'desc')
            ->paginate($countAwarded);

        $offerSent = UserInbox::where('type', '=', UserInbox::TYPE_PROJECT_SEND_OFFER)
            ->with(['project', 'toUser', 'firstMessage'])
            ->wherehas('project', function ($q) {
                $q->where('user_id', '=', \Auth::user()->id);
            })
            ->where('project_id', '!=', NULL)->get();

        $pro = array();

        foreach ($awardedProjects as $k => $v) {
            try {
                if (!is_null($v->awardedUserInbox)) {
                    array_push($pro, array(
                        'user' => array(
                            'img_avatar' => $v->user->img_avatar ? $v->user->img_avatar : '/images/avatar_xenren.png',
                            'real_name' => $v->user->name,
                            'id' => $v->user->id
                        ),
                        'awardedUserInboxId' => $v->awardedUserInbox->id,
                        'type' => 0,
                        'status' => $v->status,
                        'pay_type' => $v->pay_type,
                        'project' => array(
                            'name' => $v->project->name,
                            'reference_price' => $v->quote_price,
                            'id' => $v->project->id,
                            'pay_type' => $v->project->pay_type
                        ),
                        'awarded' => true,
                        'id' => $v->project->id,
                    ));
                }
            } catch (\Exception $e) {
                dump($v);
                dd($e);
            }
        }


        foreach ($offerSent as $k => $v) {
            array_push($pro, array(
                'user' => array(
                    'img_avatar' => isset($v->toUser->img_avatar) ? $v->toUser->img_avatar : '/images/avatar_xenren.png',
                    'real_name' => isset($v->toUser->name) ? $v->toUser->name : '',
                    'id' => isset($v->toUser->id) ? $v->toUser->id : ''
                ),
                'type' => 10,
                'status' => 10,
                'pay_type' => $v['firstMessage']['pay_type'],
                'project' => array(
                    'name' => $v->project->name,
                    'reference_price' => $v->firstMessage->quote_price,
//						'reference_price' => isset($v->firstMessage->quote_price) ? $v->firstMessage->quote_price : $v->project->reference_price,
                    'id' => $v->project->id,
                    'pay_type' => $v->project->pay_type
                ),
                'awarded' => false,
                'id' => $v->project->id,
            ));
        }
        $pro = collect($pro);
        $pro = $this->paginate($pro->sortByDesc('id')->toArray(),$request);

//        $pro = $pro->sortByDesc('id');
//
////			$perPage =10;
////			$currentPage = isset($request->page) ? $request->page == 1 ? 0 : $request->page-1 : 0;
////			$slice = $currentPage * $perPage;
////			$pro = $pro->slice($slice);
//        $perPage = 10;
//        $currentPage = isset($request->page) ? $request->page == 1 ? 0 : $request->page - 1 : 0;
//        $slice = $currentPage * $perPage;
//        $pro = $pro->slice($slice);
//
//        $pro = new LengthAwarePaginator(
//            $pro,
//            count($pro),
//            '10',
//            $request->page,
//            [
//                'path' => Paginator::resolveCurrentPath(),
//            ]
//        );

        $adminSettings = getAdminSetting(Settings::HOURLY_ON_OFF);
        return view('frontend.myPublishOrder.index', [
            'milestones' => $milestones,
            'projects' => $projects,
            'status' => isset($request->status) ? $request->status : 'all',
            'lastNumberDayOptions' => $lastNumberDayOptions,
            'lastNumberDay' => $request->lastNumberDays,
            'viewAll' => isset($request->viewAll) ? true : false,
            'viewAllAwarded' => isset($request->viewAllAwarded) ? true : false,
            'awardedProjects' => $pro,
            'adminSettings' => $adminSettings
        ]);
    }

    public function paginate($array,$request)
    {
        $setUrl = $request->url();
        $pageN = $request->page ? $request->page : 1;
        $page = \Input::get('page', $pageN);
        $perPage = 20;
        $offset = ($page * $perPage) - $perPage;
        $paginate =  new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,
            ['path' => $setUrl, 'query' => $request->query()]);
        return $paginate;
    }

    /**
     * @param $projects
     * @param $lastNumberDays
     * @return mixed
     */
    public function daysFilter($projects, $lastNumberDays)
    {
        if ($lastNumberDays == Constants::LAST_SEVEN_DAYS) {
            $week = Carbon::now()->subWeek();
        }
        if ($lastNumberDays == Constants::LAST_FOURTEEN_DAYS) {
            $week = Carbon::now()->subWeek(2);
        }
        if ($lastNumberDays == Constants::LAST_THIRTY_DAYS) {
            $week = Carbon::now()->subMonth(1);
        }
        $filtered = $projects->filter(function ($value, $key) use ($week) {
            return $value->created_at > $week->toDateTimeString();
        });
        return $filtered;
    }

    /**
     * @param $projects
     * @param $status
     * @return mixed
     */
    public function statusFilter($projects, $status)
    {
        $filtered = $projects->filter(function ($value, $key) use ($status) {
            if ($status == 'published') {
                return $value->status == Project::STATUS_PUBLISHED;
            }
            if ($status == 'progressing') {
                return $value->status == Project::STATUS_SELECTED || $value->status == Project::STATUS_HIRED;
            }
            if ($status == 'completed') {
                return $value->status == Project::STATUS_COMPLETED;
            }
            return $value;
        });
        return $filtered;
    }


    public function xindex(Request $request)
    {
        //TODO: Remove this index method after 29 Jun 20418, Refactored it with and keeping this for safe end.
        $filter = array();
        $filter['status'] = null;
        $filter['lastNumberDays'] = null;

        $status = $request->get('status');
        $lastNumberDays = $request->get('lastNumberDays');

        $projects = DB::table('project as p')
            ->select('p.*', 'project_applicants.status as project_status',
                DB::raw("(SELECT COUNT(project_applicants.id) FROM project_applicants
                                WHERE project_applicants.project_id = p.id) as cnt_applicants"),
                DB::raw("(SELECT COUNT(pcf.id) FROM project_chat_followers as pcf JOIN project_chat_rooms as pcr ON pcf.project_chat_room_id = pcr.id
                                WHERE pcr.project_id = p.id) as cnt_project_interviewees"),
                DB::raw("(SELECT COUNT(project_applicants.id) FROM project_applicants
                                WHERE project_applicants.project_id = p.id AND project_applicants.status = " . ProjectApplicant::STATUS_APPLICANT_ACCEPTED . ") as cnt_hired_applicants")
            )
            ->leftjoin('project_applicants', 'project_applicants.project_id', '=', 'p.id')
            ->where([
                ['p.user_id', '=', Auth::id()],
                ['p.type', '=', Project::PROJECT_TYPE_COMMON]
            ]);

        if ($status == 'published') {
            $projects->whereIn('p.status', [Project::STATUS_PUBLISHED]);
        } elseif ($status == 'progressing') {
            $projects->whereIn('p.status', [Project::STATUS_SELECTED, Project::STATUS_HIRED]);
        } elseif ($status == 'completed') {
            $projects->whereIn('p.status', [Project::STATUS_COMPLETED]);
        } else {
            //show all
            $status = 'all';

            $projects->whereIn('p.status', [Project::STATUS_PUBLISHED, Project::STATUS_SELECTED, Project::STATUS_HIRED, Project::STATUS_COMPLETED, Project::STATUS_CANCELLED]);
        }
        if ($status >= 0) {
            $filter['status'] = $status;
        }

        if ($lastNumberDays >= 0) {
            if ($lastNumberDays == Constants::LAST_SEVEN_DAYS) {
                $oneWeek = Carbon::now()->subWeek();
                $projects->where('created_at', '>=', $oneWeek);
            } else if ($lastNumberDays == Constants::LAST_FOURTEEN_DAYS) {
                $twoWeek = Carbon::now()->subWeek(2);
                $projects->where('created_at', '>=', $twoWeek);
            } else if ($lastNumberDays == Constants::LAST_THIRTY_DAYS) {
                $oneMonth = Carbon::now()->subMonth(1);
                $projects->where('created_at', '>=', $oneMonth);
            }

            $filter['lastNumberDays'] = $lastNumberDays;
        }

//        if ($status == 0 || $status == Project:: STATUS_PUBLISHED || $status == Project:: STATUS_SELECTED) {
//            $projectApplicants = $projectApplicants->groupby('project_id')->distinct();
//        }

        $projectCount = count($projects->get());
        $projects = $projects->paginate(10);

        $lastNumberDayOptions = Constants::getLastNumberDaysList();

        return view('frontend.myPublishOrder.index', [
            'projects' => $projects,
            'status' => $status,
            'projectCount' => $projectCount,
            'lastNumberDayOptions' => $lastNumberDayOptions,
            'filter' => $filter
        ]);
    }

    public function orderDetail($id, $userId, Request $request)
    {
        $project = new Project();
        $data = $project->getProjectDetailById($id, $userId);
        $count = 0;
        foreach ($data->milestones as $k => $v) {
            if ($v->milestoneStatus->status == ProjectMilestones::STATUS_INPROGRESS) {
                $count++;
            }
        }
        $currency = self::getCustomerCurrency($request);
        $localCurrencyRate = (getCurrency('usd', strtolower($currency)));

        return view('frontend.fixedPriceEscrowModule.publishedJobMilestone', [
            'data' => $data,
            'completed_milestones_count' => $count,
            'userId' => $userId,
            'local_currency' => $currency,
            'local_currency_rate' => $localCurrencyRate
        ]);
    }

    public function getCustomerCurrency($request)
    {
        try {
            $ip = $request->ip();
            $location = userLocation($ip);
            $location = userLocation('203.6.210.92');
            return (getCurrencyByCountryCode($location['response']->country_code)->currency_code);
        } catch (\Exception $e) {
            return 'usd';
        }
    }

    public function deleteProject($id)
    {
        $project = Project::find($id);
        $project->delete();

        return redirect()->back();
    }

    public function editProject($id)
    {
        $applicant = ProjectApplicant::find($id);
        $applicantId = isset($applicant->project_id) ? $applicant->project_id : false;

        $project = Project::find($id);

        $projectSkillList = ProjectSkill::GetProjectSkill()->where('project_id', '=', $project->id)->get();

        $skillIdList = array();
        foreach ($projectSkillList as $projectSkill) {
            $item = $projectSkill->skill_id;
            array_push($skillIdList, $item);
        }

        //convert the price
        if (app()->getLocale() == "en") {
            //not need convert
        } else if (app()->getLocale() == "cn") {
            //convert form usd to cny
            $price = $project->reference_price;
            $exchangeRate = ExchangeRate::getExchangeRate(Constants::USD, Constants::CNY);
            $project->reference_price = $price * $exchangeRate;

            if (!empty($applicant->reference_price)) {
                $price = $applicant->reference_price;
                $applicant->reference_price = $price * $exchangeRate;
            }

        }

        if (\Auth::guard('users')->user()->is_agent == 1) {
            $skillList = Skill::getSearchSkillData(Skill::TAG_COLOR_GOLD);
        } else {
            $skillList = Skill::getSearchSkillData(Skill::TAG_COLOR_WHITE);
        }

        $pay_types = Constants::getPayTypeLists();

        $skillIdListString = "";

        foreach ($skillIdList as $key => $skill) {
            if ($key != (sizeof($skillIdList) - 1))
                $skillIdListString = $skillIdListString . $skill . ",";
            else
                $skillIdListString = $skillIdListString . $skill;
        }

        if (empty($applicant->reference_price_type))
            $type = $project->pay_type;
        else
            $type = $applicant->reference_price_type;

        return view('frontend.myPublishOrder.edit', [
            'project' => $project,
            'applicant' => $applicant,
            'skillIdList' => $skillIdList,

            'pay_types' => $pay_types,
            'type' => $type,
            'skillList' => $skillList,
            'skillIdListString' => $skillIdListString,
        ]);
    }

    public function postEditProject(Request $request)
    {
        $projectId = $request->get('applicant_id');

        try {
            $project = Project::find($projectId);

            $project->name = $request->get('name');
            $project->description = $request->get('description');
            $project->reference_price = $request->get('reference_price');
            $project->pay_type = $request->get('pay_type');
            $project->save();

            $skills = $request->get('skill_id_list');
            $skills = explode(',', $skills);
            ProjectSkill::where('project_id', '=', $projectId)->delete();
            foreach ($skills as $k => $v) {
                ProjectSkill::create([
                    'project_id' => $projectId,
                    'skill_id' => $v
                ]);
            }

            return makeJSONResponse(true, trans('common.successfully_apply'));
        } catch (\Exception $e) {
            return makeJSONResponse(false, $e->getMessage());
        }
    }
}
