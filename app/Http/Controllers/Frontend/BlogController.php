<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use function App\Http\Controllers\Staff\NewGuid;

class BlogController extends Controller
{
    public function index()
    {
        $article = Article::all();
        return view('frontend.blog.index', [
            'article' => $article
        ]);
    }

    public function detail($lang, $title, $id)
    {
        $title = str_replace('-', ' ', $title);
        $getArticle = Article::where('id', '=', $id)->first();
        $content = $getArticle->article_description;
        $content = preg_replace("/<img[^>]+\>/i", "", $content);
        $content = str_replace('<p>', '', $content);
        $content = str_limit($content, 100);
        return view('frontend.blog.detail', [
            'detail' => $getArticle,
            'content' => $content
        ]);
    }

    public function NewGuid()
    {
        $s = strtoupper(md5(uniqid(rand(), true)));
        $guidText =
            substr($s, 0, 8) . '-' .
            substr($s, 8, 4) . '-' .
            substr($s, 12, 4) . '-' .
            substr($s, 16, 4) . '-' .
            substr($s, 20);
        return $guidText;
    }

    public function uploadImages(Request $request)
    {
        if ($request->hasFile('file')) {
            $file_name = ($this->NewGuid()) . '.png';
            $path = "$file_name";
            move_uploaded_file($_FILES["file"]["tmp_name"], $path);
        }
    }

    public function searchData(Request $request)
    {
        if (is_null($request->searchBlog)) {
            return redirect()->back();
        } else {
            $article = $this->searchInBlog($request->searchBlog);
            return view('frontend.blog.index', [
                'article' => $article,
                'searchValue' => $request->searchBlog
            ]);
        }
    }

    public function searchInBlog($searchData)
    {
        $article = Article::where('title', 'like', '%' . strtolower($searchData) . '%')->get();
        return $article;
    }
}
