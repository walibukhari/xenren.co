<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\FrontendController;
use App\Http\Requests\Frontend\FeedbackCreateFormRequest;
use App\Models\ChatRoom;
use App\Models\ChatFollower;
use App\Models\Feedback;
use App\Models\FeedbackFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;


class FeedbackController extends FrontendController
{
    public function index(Request $request)
    {
        //get all the feedback list
        $feedbacks = Feedback::where('user_id', \Auth::id())
            ->get();

        $feedback_id = $request->get('feedback_id');
        if ($feedback_id == 0) {
            //get the latest feedback
            $feedback = Feedback::where('user_id', \Auth::id())
                ->orderBy('created_at', 'desc')
                ->first();
        } else {
            $feedback = Feedback::find($feedback_id);
        }

        if ($feedback != null) {
            //check chat room exist or not
            $chatRoom = ChatRoom::where('type', ChatRoom::TYPE_FEEDBACKS)
                ->where('user_id', $feedback->user_id)
                ->where('feedback_id', $feedback->id)
                ->first();

            //create a chat room if the chat room not exist
            if (!isset($chatRoom)) {
                $chatRoom = new ChatRoom();
                $chatRoom->type = ChatRoom::TYPE_FEEDBACKS;
                $chatRoom->user_id = $feedback->user_id;
                $chatRoom->feedback_id = $feedback->id;
                $chatRoom->save();
            }

            //chat room follower
            $chatFollower = ChatFollower::where('user_id', \Auth::guard('users')->user()->id)
                ->where('room_id', $chatRoom->id)
                ->first();

            //no user exist then add the login user as follower
            if ($chatFollower == null) {
                $chatFollower = new ChatFollower();
                $chatFollower->user_id = \Auth::guard('users')->user()->id;
                $chatFollower->room_id = $chatRoom->id;
                $chatFollower->save();
            }

            $chatFollowers = ChatFollower::with(['user'])
                ->where('room_id', $chatRoom->id)
                ->get();
        } else {
            $chatRoom = null;
            $chatFollowers = null;
        }

        return view('frontend.feedback.index', [
            'feedbacks' => $feedbacks,
            'feedback' => $feedback,
            'chatRoom' => $chatRoom,
            'chatFollowers' => $chatFollowers,
        ]);
    }

    public function create()
    {
        $priorities = Feedback::getPriorityLists();

        return view('frontend.feedback.create', [
            'priorities' => $priorities
        ]);
    }

    public function createPost(FeedbackCreateFormRequest $request)
    {
        try {
            DB::beginTransaction();
            $feedback = new Feedback();
            $feedback->priority = $request->get('priority');
            $feedback->user_id = Auth::id();
            $feedback->title = $request->get('title');
            $feedback->description = $request->get('description');
            $feedback->status = Feedback::STATUS_PENDING; //all the new feedback is pending until admin join the chat room
            $feedback->save();

            if ($request->hasFile('fileUploadImage')) {
                $fileUploadImages = Input::file('fileUploadImage');
                foreach ($fileUploadImages as $fileUploadImage) {
                    $feedbackFile = new FeedbackFile();
                    $feedbackFile->feedback_id = $feedback->id;
                    $feedbackFile->file = $fileUploadImage;
                    $feedbackFile->save();
                }
            }

            DB::commit();
            return makeResponse(trans('common.success'));
        } catch (\Exception $e) {
            DB::rollback();
            return makeResponse($e->getMessage(), false);
        }
    }

    public function delete(Request $request)
    {
        try {
            $feedback = Feedback::find($request->get('id'));
            $feedback->delete();

            return response()->json([
                'status' => 'OK',
                'message' => trans('common.delete_success')
            ]);
        } catch (\Exception $e) {

            return response()->json([
                'status' => 'Error',
                'message' => $e->getMessage()
            ]);
        }

    }

    public function waitAdminReply(Request $request)
    {
        try {
            $feedback_id = $request->get('feedback_id');
            $feedback = Feedback::find($feedback_id);
            $feedback->status = Feedback::STATUS_WAIT_ADMIN_REPLY;
            $feedback->save();

            return response()->json([
                'status' => 'OK'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'Error',
                'message' => $e->getMessage()
            ]);
        }
    }
}
