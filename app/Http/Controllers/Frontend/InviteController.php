<?php

namespace App\Http\Controllers\Frontend;

use App\Models\CountryLanguage;
use App\Models\Invitation;
//use Carbon;
use App\Http\Controllers\FrontendController;
use App\Models\Language;
use Illuminate\Support\Str;
use App\Models\Skill;
use App\Models\User;
use App\Models\UserLanguage;
use App\Models\UserSkill;
use App\Services\GeneralService;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Auth;
use App\Models\GuestRegister;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Validator;
use App\Models\UserXencoinDetail;
use App\Http\Requests\Auth\UserReferFormRequest;

class InviteController extends FrontendController
{
    protected $refer_id;

    public function index(Request $request)
    {
        $registerUrl = route('logout') . '?inviteCode=' . \Auth::guard('users')->user()->id;
        $qrImageLink = "https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=" . $registerUrl . '-type-qr';
        $invitations = Invitation::where('invitor_id', \Auth::guard('users')->user()->id)->with('invitor', 'invitee')
            ->orderBy('id', 'desc')
            ->get();
        $xenCoin = number_format(\Auth::guard('users')->user()->xen_coin, 0, '', ',');;
        return view('frontend.invite.index', [
            'qrImageLink' => $qrImageLink,
            'registerUrl' => $registerUrl,
            'invitations' => $invitations,
            'xenCoin' => $xenCoin
        ]);
    }

    public function userRefer()
    {
        $skillList = Skill::get();
        $langList = Language::get();
        return view('frontend.newDesign.userRefer', [
            'languages' => $langList,
            'skill' => $skillList,
        ]);
    }

    public function generateLink(UserReferFormRequest $request)
    {
        $currentLoginUser = \Auth::guard('users')->user()->id;
        $name = $request->get('name');
        $about = $request->get('about');
        $skill = $request->get('skill');
        $phone = $request->get('phone');
        $email = $request->get('email');
        $sixDigit = generateStrongPassword(6, false, 'd');
        $key = generateStrongPassword(32, false, 'lud');
        $link = route('frontend.newDesign.inviteFriend', $sixDigit . '?type=link');
        $request->session()->put('generateLink', $link);
        $language = $request->get('language');
        $cityId = $request->city;

        $guestRegister = new GuestRegister();
        if (isset($request->avatar) && $request->avatar != '') {
            $generalService = new GeneralService();
            $avatar = $generalService->uploadFile($request, 'uploads/temp-user', 'avatar');
            $guestRegister->image = isset($avatar['icon']) ? $avatar['icon'] : '';
        }


        \DB::beginTransaction();
        //create new session
        $guestRegister->real_name = $name;
        $guestRegister->about = $about;
        $guestRegister->skills = isset($skill) ? implode(',', $skill) : '';
        $guestRegister->phone_number = $phone;
        $guestRegister->refer_by = $currentLoginUser;
        $guestRegister->key = $key;
        $guestRegister->email = $email;
        $guestRegister->language = isset($language) ? implode(',', $language) : '';
        $guestRegister->code = $sixDigit;
        $guestRegister->city_id = $cityId;
        $guestRegister->status = 1; //active
        $guestRegister->save();

        \DB::commit();
        return collect([
            'status' => 'success',
            'link' => $link
        ]);
//        return makeResponse('Email has been send to your friend...!');
    }

    public function friend_invite(Request $request)
    {
        $id = Auth::guard('users')->user()->id;
        $guest = GuestRegister::where("refer_by", "=", "$id")->first();
        $d = $request->session()->get("guest_user");
        $sL = Skill::get();
        return view("frontend.newDesign.friend_invite", compact("d"));
    }

    public function userReferPost(UserReferFormRequest $request)
    {
        $currentLoginUser = \Auth::guard('users')->user()->id;
        $name = $request->get('name');
        $about = $request->get('about');
        $skill = $request->get('skill');
        $phone = $request->get('phone');
        $email = $request->get('email');
        $sixDigit = generateStrongPassword(6, false, 'd');
        $key = generateStrongPassword(32, false, 'lud');
        $link = route('frontend.newDesign.inviteFriend', $sixDigit);
        $language = $request->get('language');
        $cityId = $request->city;

        $guestRegister = new GuestRegister();
        if (isset($request->avatar) && $request->avatar != '') {
            $generalService = new GeneralService();
            $avatar = $generalService->uploadFile($request, 'uploads/temp-user', 'avatar');
            $guestRegister->image = isset($avatar['icon']) ? $avatar['icon'] : '';
        }

        $exist = User::where('email', '=', $email)->first();
        if (!is_null($exist)) {
            return makeResponse('Email already used...!', true);
        }

        \DB::beginTransaction();
        //create new session
        $guestRegister->real_name = $name;
        $guestRegister->about = $about;
        $guestRegister->skills = isset($skill) ? implode(',', $skill) : '';
        $guestRegister->phone_number = $phone;
        $guestRegister->refer_by = $currentLoginUser;
        $guestRegister->key = $key;
        $guestRegister->email = $email;
        $guestRegister->language = isset($language) ? implode(',', $language) : '';
        $guestRegister->code = $sixDigit;
        $guestRegister->city_id = $cityId;
        $guestRegister->status = 1; //active
        $guestRegister->save();
        //send email
        if (!empty($email)) {
            $this->sendRegisterEmail($email, $link, $request->all());
        }
        \DB::commit();
        $request->session()->put('guest_user', $guestRegister);
        return makeResponse('Email has been sent to your friend...!');
    }

    public function sendRegisterEmail($email, $link, $d)
    {
        $senderEmail = "support@xenren.co";
        $senderName = 'Xenren Invitation Message';
        $receiverEmail = $email;

        Mail::send('frontend.newDesign.friend_invite', array('email' => $email, 'link' => $link, 'd' => $d), function ($message) use ($senderEmail, $senderName, $receiverEmail) {
            $message->from($senderEmail, $senderName);
            $message->to($receiverEmail)
                ->subject('Registration link of Xenren.co');
        });
    }

    public function inviteFriend(Request $request, $id)
    {
        if (Auth::user()) {
            Auth::logout();
        }
        $skillList = Skill::get();
        $langList = Language::get();
        $userData = GuestRegister::where('code', $id)->first();
        session([
            'referral_invite' => $id
        ]);
        $userData['skills'] = explode(',', $userData['skills']);
        $userData['language'] = explode(',', $userData['language']);
        $data = User::where('id', '=', $userData->refer_by)->first();
        if (is_null($data)) {
            return redirect('/');
        }
        $generateLink = $request->session()->get('generateLink');
        $inviteLink = route('frontend.newDesign.inviteFriend', $id);
        $qr = $request->session()->get('qr');
        if ($request->type == UserXencoinDetail::GENERATE_LINK) {
            $link = 0;
        } else if ($qr == UserXencoinDetail::QR_LINK) {
            $link = 1;
        } else {
            $link = 2;
        }

        $this->refer_id = $userData->refer_by;
        return view('frontend.newDesign.inviteFriend', [
            'userData' => $userData,
            'languages' => $langList,
            'skill' => $skillList,
            'user' => $data,
            'type' => $link,
        ]);
    }

    public function inviteFriendPost(UserReferFormRequest $request)
    {
        $name = $request->get('name');
        $about = $request->get('about');
        $phone = isset($request->phone) ? $request->get('phone') : '';
        $email = $request->get('email');
        $skill = $request->get('skill');
        $refer_id = $request->get('refer_id');
        $language = $request->get('language');
        $skype = $request->get('skype');
        $line = $request->get('line');
        $wechat = $request->get('wechat');
        $facebook = $request->get('facebook');

        \DB::beginTransaction();


        $user = new User();
        $user->email = $email;
        $user->phone_number = $phone;
        $user->name = $name;
        $user->skype_id = $skype;
        $user->line_id = $line;
        $user->wechat_id = $wechat;
        $user->about_me = $about;
        $user->upline_id = $refer_id;
        $user->save();

        $xenCoinDetail = new UserXencoinDetail();
        $xenCoinDetail->user_id = $user->id;
        $xenCoinDetail->upline_id = $refer_id;
        $xenCoinDetail->type = $request->type;
        $xenCoinDetail->status = UserXencoinDetail::INVITED_STATUS;
        $xenCoinDetail->save();

        if (isset($request->avatar) && $request->avatar != "") {
            $generalService = new GeneralService();
            $img = $generalService->uploadFile($request, 'uploads/user/' . $user->id . '/', 'avatar');
            $user->img_avatar = 'uploads/user/' . $user->id . '/' . $img['icon'];
        } else {
            $user->img_avatar = str_replace('temp-user/', 'user/' . $user->id . '/', $request['old-avatar']);
        }
        $user->save();
        \DB::commit();
        $request->session()->put('user_id', $user);
        return redirect(route('setPass'));
    }

    public function setPassword(Request $request)
    {
        $user = $request->session()->get('user_id');
        return view('frontend.newDesign.setPassword', [
            'email' => $user->email
        ]);
    }

    public function setPasswordPost(Request $request)
    {
        $user = $request->session()->get('user_id');
        $data = User::where('id', '=', $user->id)->first();
        $email = $request->get('email');
        $password = $request->get('password');
        $reTypepassword = $request->get('reTypePassword');
        $token = Str::random(60);
        User::where('id', '=', $user->id)->update([
            'password' => bcrypt($password),
            'hint' => self::getHint($password),
            'remember_token' => $token
        ]);
        Invitation::create([
            'invitor_id' => $user->upline_id,
            'invitee_id' => $user->id
        ]);
        $this->verificationEmail($email, $token, $user);
//	      \Auth::guard('users')->login($user);
//        $request->session()->forget('user_id');
        return redirect("/acceptInvitation");
    }

    public function acceptInvitation()
    {
        return view('frontend.newDesign.acceptInvitation');
    }

    public function verifyToken($token)
    {
        $usr = explode('-', $token);
        $userId = $usr[1];
        $user = User::where('id', '=', $userId)->first();
        $user->is_validated = 1;
        $user->save();
        \Auth::guard('users')->login($user);
        return redirect('/');
    }

    public function verificationEmail($email, $token, $user)
    {
        $senderEmail = "support@xenren.co";
        $senderName = 'Xenren mail Confirmation Message';
        $receiverEmail = $email;
        $userToken = $token;
        $link = route('home') . '/verify/' . GeneralService::generateRandomStringWithAlPhabet(5) . '-' . $user->id . '-' . GeneralService::generateRandomStringWithAlPhabet(5);
        $user = User::where('remember_token', '=', $userToken)->first();
        Mail::send('frontend.newDesign.success', array('email' => $email, 'link' => $link), function ($message) use ($senderEmail, $senderName, $receiverEmail) {
            $message->from($senderEmail, $senderName);
            $message->to($receiverEmail)
                ->subject('Verification Email');
        });
    }

    public function success()
    {
        return view('frontend.newDesign.success');
    }

    public function getHint($string)
    {
        try {
            $length = strlen($string);
            $co = (int)($length / 2);
            $splittedString = str_split($string, $co);
            return $splittedString[0];
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

}
