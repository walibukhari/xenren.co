<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Project;
use App\Models\ProjectApplicant;
use App\Models\ProjectDispute;
use App\Models\ProjectMilestones;
use App\Models\ProjectMilestoneStauts;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserIdentity;
use App\Models\UserInboxMessages;
use App\Services\GeneralService;
use Illuminate\Http\Request;
use Hash;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ProjectOrderController extends Controller
{
    public function giveBonus(Request $request)
    {
        try {
            $user = User::where('id', '=', Auth::user()->id)->first();
            $applicant = User::where('id', '=', $request->applicant_id)->first();
            if($user->account_balance < $request->amount) {
                return collect([
                    'status' => 'error',
                    'message' => trans('common.insufficient_balance')
                ]);
            }
            \DB::beginTransaction();

            $releaseDate = Carbon::now()->addDays(config('milestones.system_hold_days'));
            $data = [
                'name' => isset($request->reason) ? $request->reason : 'bonus',
                'amount' => $request->amount,
                'milestone_id' => 0,
                'type' => Transaction::TYPE_BONUS,
                'balance_before' => \Auth::user()->account_balance,
                'balance_after' => (\Auth::user()->account_balance) - $request->amount,
                'performed_by' => \Auth::user()->id,
                'performed_to' => $request->applicant_id,
                'status' => Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM,
                'release_at' => $releaseDate,
            ];
            Transaction::create($data);


            $releaseDate = Carbon::now()->addDays(config('milestones.system_hold_days'));
            $data = [
                'milestone_id' => 0,
                'name' => isset($request->reason) ? $request->reason : 'bonus',
                'amount' => GeneralService::calculateServiceFee($request->amount),
                'due_date' => Carbon::now(),
                'comment' => 'Transaction Fee 2.5%',
                'type' => Transaction::TYPE_SERVICE_FEE,
                'status' => Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM,
                'balance_before' => $applicant->account_balance,
                'balance_after' => ($applicant->account_balance) - GeneralService::calculateServiceFee($request->amount),
                'release_at' => $releaseDate,
                'performed_by' => $request->applicant_id,
                'performed_to' => $request->applicant_id,
            ];
            Transaction::create($data);

            User::where('id', '=', Auth::user()->id)->decrement('account_balance', $request->amount);
            \DB::commit();
            return collect([
                'status' => 'success',
            ]);
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function giveBonusP(Request $request, $projectId , $passChk = false)
    {
        if(!$passChk){
            $password = request('password');
            $passChk = $this->checkPassword($password);
            if(!$passChk){
                return collect([
                    'status' => 'error',
                    'message' => 'incorrect password'
                ]);
            }
        }

        if($passChk)
        {
        try {

            $user = User::where('id', '=', Auth::user()->id)->first();
            $applicant = User::where('id', '=', $request->applicant_id)->first();
            if($user->account_balance < $request->amount) {
                return collect([
                    'status' => 'error',
                    'message' => trans('common.insufficient_balance')
                ]);
            }
            if($request->amount && $request->amount>0){
            \DB::beginTransaction();

            $releaseDate = Carbon::now()->addDays(config('milestones.system_hold_days'));

            $data = [
                'name' => $request->name.'_bonus',
                'amount' => $request->amount,
                'milestone_id' => $request->milestone_id,
                'type' => Transaction::TYPE_BONUS,
                'balance_before' => \Auth::user()->account_balance,
                'balance_after' => (\Auth::user()->account_balance) - $request->amount,
                'performed_by' => \Auth::user()->id,
                'performed_to' => $request->applicant_id,
                'status' => Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM,
                'release_at' => $releaseDate,
            ];
            Transaction::create($data);


            $releaseDate = Carbon::now()->addDays(config('milestones.system_hold_days'));
            $data = [
                'milestone_id' => $request->milestone_id,
                'name' => $request->name.'_bonus',
                'amount' => GeneralService::calculateServiceFee($request->amount),
                'due_date' => Carbon::now(),
                'comment' => 'Transaction Fee 2.5%',
                'type' => Transaction::TYPE_SERVICE_FEE,
                'status' => Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM,
                'balance_before' => $applicant->account_balance,
                'balance_after' => ($applicant->account_balance) - GeneralService::calculateServiceFee($request->amount),
                'release_at' => $releaseDate,
                'performed_by' => $request->applicant_id,
                'performed_to' => $request->applicant_id,
            ];
            Transaction::create($data);

            User::where('id', '=', Auth::user()->id)->decrement('account_balance', $request->amount);
            }
            $this->acceptMilestone($request, $projectId , true);
            \DB::commit();
            return collect([
                'status' => 'success',
            ]);
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }
    }

    public function checkAmountNotGreaterThenProjectBudget($request , $projectId){
        $getSendProjectData = ProjectApplicant::where('user_id','=',$request->applicant_id)->where('project_id','=',$projectId)->with('project')->first();
        $mileStoneAmount = $request->amount;
        $projectQuotePrice = $getSendProjectData->quote_price;
        $project = $getSendProjectData->project;
        if ($mileStoneAmount > $projectQuotePrice) {
            return false;
        } else {
            return true;
        }
    }

    public function createMilestone(Request $request, $projectId)
    {
        $check = $this->checkAmountNotGreaterThenProjectBudget($request, $projectId);
        if($check == false){
            return collect([
                'status' => 'failure',
                'message' => 'your amount greater then currant project amount please select different amount'
            ]);
        } else {
            $projApplicantData = ProjectApplicant::where('user_id', $request->applicant_id)->where('project_id', $projectId)->first();
            if ($projApplicantData->status && $projApplicantData->status == 2) {
                return collect([
                    'status' => 'failure',
                    'message' => 'You can not create milestone for pending projects'
                ]);
            }
//        try {
            $_project_milesstones = new ProjectMilestones();
            $_project_milesstones_status = new ProjectMilestoneStauts();
            $mileStones = $_project_milesstones->getProjectMilestoens($projectId, $request->applicant_id);
            if (count($mileStones) > 0) {
                return collect([
                    'status' => 'failure',
                    'message' => trans('common.milestone_in_progress')
                ]);
            }

            if (Auth::user()->account_balance < $request->amount) {
                return collect([
                    'status' => 'failure',
                    'message' => trans('common.insufficient_balance')
                ]);
            }

            $due_date = Carbon::createFromFormat('d M yy - H:i', $request->d_date);
//            dd($due_date->toDateTimeString());
            if ($due_date->lessThan(Carbon::now())) {
                return collect([
                    'status' => 'failure',
                    'message' => trans('common.duedate_greater_current')
                ]);
            }

            $request->request->add(['project_id' => $projectId]);
            $request->request->add(['status' => isset($request->status) ? $request->status : ProjectMilestones::STATUS_INPROGRESS]);
            $request->request->add(['due_date' => $due_date->toDateTimeString()]);

            \DB::transaction(function () use ($request, $_project_milesstones, $_project_milesstones_status) {
                $milestone = $_project_milesstones->createMilestone($request);
                $request->request->add(['milestone_id' => $milestone->id]);
                $_project_milesstones_status->createMileStoneStatus($request);
                Transaction::create([
                    'name' => $request->name,
                    'amount' => $request->amount,
                    'milestone_id' => $milestone->id,
                    'due_date' => $request->due_date,
                    'comment' => $request->description,
                    'balance_before' => Auth::user()->account_balance,
                    'balance_after' => (Auth::user()->account_balance - $request->amount),
                    'performed_by' => Auth::user()->id,
                    'performed_to' => $milestone->applicant_id,
                    'type' => Transaction::TYPE_MILESTONE_PAYMENT,
                    'currency' => 'USD',
                    'status' => Transaction::STATUS_IN_PROGRESS_MILESTONE
                ]);
                Auth::user()->decrement('account_balance', $request->amount);
            });

            $milestones = $_project_milesstones->getProjectMilestoens($projectId);
            return collect([
                'status' => 'success',
                'message' => trans('common.milestone_created')
            ]);
//        } catch (\Exception $e) {
//            return collect([
//                'status' => 'failure',
//                'message' => $e->getMessage() . ' at ' . $e->getFile()
//            ]);
//        }
        }
    }

    public function acceptMilestone(Request $request, $projectId , $passChk = false)
    {
        if(!$passChk){
            $password = request('password');
            $passChk = $this->checkPassword($password);
            if(!$passChk){
                return collect([
                    'status' => 'error',
                    'message' => 'incorrect password'
                ]);
            }
        }

        if($passChk){
        try {
            $_project = new Project();
            $newbieEmployer = $_project->firstOrSecondMilestone();
            $_project_milesstones = new ProjectMilestoneStauts();
            $request->request->add([
                'status' => ProjectMilestones::STATUS_CONFIRMED,
                'milestone' => $request->milestone_id
            ]);
            $currentMilestone =  ProjectMilestoneStauts::where('project_id', '=', $projectId)
                ->where('status', '=', ProjectMilestones::STATUS_INPROGRESS)
                ->with('milestone')
                ->first();
            $applicant = User::where('id','=', $currentMilestone->milestone->applicant_id)->first();
            $currentMilestoneId = $currentMilestone->milestone_id;

//	          if(\Auth::user()->account_balance < $currentMilestone->milestone->amount) {
//	          	return collect([
//	          		'status' => 'failure',
//			          'message' => 'No enough balance'
//		          ]);
//	          }


//            if(isset($transaction)) {
//                /** Removing Partial amount request because we're relesing complete milestone */
//                Transaction::where('milestone_id', '=', $currentMilestoneId)->where('status', '=', Transaction::STATUS_REQUESTED)->delete();
//            }

//		        $data = [
//			        'milestone_id' => $currentMilestoneId,
//			        'name' => $currentMilestone->milestone->name,
//			        'amount' => $currentMilestone->milestone->amount,
//			        'due_date' => $currentMilestone->milestone->due_date,
//			        'comment' => isset($request->comment) ? $request->comment : '',
//			        'type' => Transaction::TYPE_MILESTONE_PAYMENT,
//			        'status' => $newbieEmployer ? Transaction::STATUS_WAITING_FOR_ADMIN_APPROVAL :Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM,
//			        'balance_before' => \Auth::user()->account_balance,
//			        'balance_after' => (\Auth::user()->account_balance) - $currentMilestone->milestone->amount,
//			        'release_at' => Carbon::parse(Carbon::now())->addDays(14),
//			        'performed_by' => \Auth::user()->id,
//			        'performed_to' => $currentMilestone->milestone->applicant_id,
//		        ];
//
//            Transaction::create($data);
            $releaseDate = Carbon::now()->addDays(config('milestones.system_hold_days'));
            $data = [
                'milestone_id' => $currentMilestoneId,
                'name' => $currentMilestone->milestone->name,
                'amount' => GeneralService::calculateServiceFee($currentMilestone->milestone->amount),
                'due_date' => Carbon::now(),
                'comment' => 'Transaction Fee 2.5%',
                'type' => Transaction::TYPE_SERVICE_FEE,
                'status' => Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM,
                'balance_before' => $applicant->account_balance,
                'balance_after' => ($applicant->account_balance) - GeneralService::calculateServiceFee($currentMilestone->milestone->amount),
                'release_at' => $releaseDate,
                'performed_by' => $currentMilestone->milestone->applicant_id,
                'performed_to' => $currentMilestone->milestone->applicant_id,
            ];
            $tr = Transaction::create($data);
            Transaction::where('milestone_id', '=', $currentMilestoneId)->where('status', '=', Transaction::STATUS_IN_PROGRESS_MILESTONE)->update([
                'status' => Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM,
                'release_at' => $releaseDate
            ]);
            $weeks = getReleaseDays('1');
            $weeks2 = getReleaseDays('2');
            if(isset($weeks)) {
                $release_time = Carbon::parse($tr->created_at)->addDay($weeks);
                $this->updateTransactionWithAdminSetReleaseDateVerifiedUsers($release_time, $currentMilestoneId);
            }
            if(isset($weeks2)) {
                $release_time = Carbon::parse($tr->created_at)->addDay($weeks2);
                $this->updateTransactionWithAdminSetReleaseDateUnVerifiedUsers($release_time, $currentMilestoneId);
            }
//            User::where('id', '=', \Auth::user()->id)
//	            ->decrement('account_balance', $currentMilestone->milestone->amount + GeneralService::calculateServiceFee($currentMilestone->milestone->amount));

//	          $amount = $currentMilestone->milestone->amount - GeneralService::calculateServiceFee($currentMilestone->milestone->amount);
//            User::where('id', '=', $currentMilestone->milestone->applicant_id)
//	            ->increment('account_balance', $amount);

//            User::where('id', '=', $currentMilestone->milestone->applicant_id)
//		          ->decrement('account_balance', GeneralService::calculateServiceFee($currentMilestone->milestone->amount));


            $_project_milesstones->updateMileStoneStatus($request, $projectId);

            return collect([
                'status' => 'success',
                'message' => 'Milestone Approved'
            ]);
        }catch (\Exception $e){
            dump($e->getFile());
            dump($e->getMessage());
            dd($e->getLine());
        }
    }
    }

    public function updateTransactionWithAdminSetReleaseDateVerifiedUsers($release_time,$currentMilestoneId)
    {
        $t1 = Transaction::where('milestone_id', '=', $currentMilestoneId)->where('status','=',Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM)->with('receiver.UserIdentity')->get();
        foreach($t1 as $t) {
            if(isset($t->receiver->UserIdentity) && $t->receiver->UserIdentity->status == UserIdentity::STATUS_APPROVED) {
                $t->update([
                    'release_at' => $release_time
                ]);
            }
        }
    }

    public function updateTransactionWithAdminSetReleaseDateUnVerifiedUsers($release_time,$currentMilestoneId)
    {
        $t2 = Transaction::where('milestone_id', '=', $currentMilestoneId)->where('status','=',Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM)->with('receiver.UserIdentity')->get();
        foreach($t2 as $t) {
            if(!isset($t->receiver->UserIdentity) || $t->receiver->UserIdentity->status != UserIdentity::STATUS_APPROVED) {
                $t->update([
                    'release_at' => $release_time
                ]);
            }
        }
    }

    public function rejectMilestone(Request $request, $projectId)
    {
        $hours = false;
        if(!isset($request->milestone)) {
            try {
                $data = ProjectMilestoneStauts::where('project_id', '=', $projectId)->where('status', '=', 1)->first();
                $milestone = ProjectMilestones::where('id', '=', $data->milestone_id)->first();
                $hours = (int)($milestone->created_at->diff(Carbon::now())->format('%h'));
            }catch (\Exception $e) {
                $hours = false;
            }
        }

        $_project_milesstones =  new ProjectMilestoneStauts();
        if(isset($hours) && $hours >= 72) {
            $request->request->add(['status' => ProjectMilestones::STATUS_REJECTED]);
        } else {
            $dueDate = Carbon::parse($milestone->due_date);
            if($dueDate->isPast()) {
                $request->request->add(['status' => ProjectMilestones::STATUS_REJECTED]);
            } else {
                $request->request->add(['status' => ProjectMilestones::STATUS_CANCEL_REQUEST_BY_EMPLOYER]);
            }
        }

        $_project_milesstones->updateMileStoneStatus($request, $projectId);
        return collect([
            'status' => 'success',
            'message' => 'Milestone Rejected'
        ]);
    }

    public function changeStatus(Request $request, $projectId)
    {
        if($request->status == "") {
            return collect([
                'status' => 'error',
                'message' => 'Status not correct..!'
            ]);
        }
        $_project_milesstones =  new ProjectMilestones();
        if($request->status == ProjectMilestones::STATUS_INPROGRESS) {
            $mileStones = $_project_milesstones->getProjectMilestoens($projectId);

            if (count($mileStones) >= 1) {
                return collect([
                    'status' => 'failure',
                    'message' => 'Already a milestone in progress'
                ]);
            }

        }
        \DB::beginTransaction();
        $_project_milesstones = new ProjectMilestoneStauts();
        $_project_milesstones->updateMileStoneStatus($request, $projectId);
        if($request->status == ProjectMilestones::STATUS_REJECTED) {
            $milestone = (ProjectMilestones::with('project')->where('id', '=', $request->milestone)->first());
            Transaction::where('milestone_id', '=', $milestone->id)->where('status', '=', Transaction::STATUS_IN_PROGRESS_MILESTONE)
                ->where('type', '=', Transaction::TYPE_MILESTONE_PAYMENT)->delete();
            User::where('id', '=', $milestone->project->user_id)->increment('account_balance', $milestone->amount);
        }

        \DB::commit();
        return collect([
            'status' => 'success',
            'message' => trans('common.milestone_updated')
        ]);
    }

    public function updateTransaction($transactionId, Request $request)
    {
        /**
         * Employer confirming the partial request here....!
         * */
//        try {
        $newTransaction = \DB::transaction(function () use($request, $transactionId) {
            $_project = new Project();
            $newbieEmployer = $_project->firstOrSecondMilestone();
            $_project_milesstones = new ProjectMilestones();

            $transaction = Transaction::where('id', '=', $transactionId)->first();
            $mileStone = $_project_milesstones->with('project')->where('id', $transaction->milestone_id)->first();

            $mileStoneAmountTransaction = $transaction; //$_transaction->where('id', $transactionId)->first();

            if ($request->status == Transaction::STATUS_APPROVED) {
//		            User::where('id', '=', \Auth::user()->id)->decrement('account_balance', ($transaction->amount));
//		            User::where('id', '=', $mileStone->applicant_id)->increment('account_balance', $transaction->amount - (GeneralService::calculateServiceFee($transaction->amount)));

//                    if($mileStone->amount == $request->amount) {
                /** BECAUSE THE TRANSACTION ENTRY WAS CREATED WHILE REQUESTING MILESTONE */
//                        $_project_milesstones->where('id', $transaction->milestone_id)->update([
//                            'status' => ProjectMilestones::STATUS_CONFIRMED,
//                        ]);
//                        Transaction::create([
//                            'name' => $mileStone->name,
//                            'amount' => $request->amount,
//                            'milestone_id' =>  $mileStone->id,
//                            'due_date' => Carbon::now()->toDateTimeString(),
//                            'comment' => isset($request->comment) ? $request->comment : '',
//                            'type' => Transaction::TYPE_MILESTONE_PAYMENT,
//                            'status' => Transaction::STATUS_APPROVED,
//                        ]);
//                    } else {
                $_project_milesstones_status = new ProjectMilestoneStauts();
                $currentMilestone = $_project_milesstones->getProjectMilestoens($mileStone->project_id, $transaction->applicant_id);

                ProjectMilestones::where('id', '=', $request->milestone_id)->decrement('amount', $request->amount);
                $PM = ProjectMilestones::create([
                    'project_id' => $mileStone->project_id,
                    'amount' => $request->amount,
                    'name' => $mileStone->name,
                    'description' => $mileStone->description,
                    'due_date' => $mileStone->due_date,
                    'auto_release' => $mileStone->auto_release,
                    'applicant_id' => $transaction->performed_by
                ]);
                ProjectMilestoneStauts::create([
                    'project_id' => $mileStone->project_id,
                    'milestone_id' => $PM->id,
                    'status' => ProjectMilestones::STATUS_CONFIRMED,
                    'comment' => $request->comment,
                ]);

                Transaction::where('id', '=', $transactionId)->update([
//							          'status' => $newbieEmployer ? Transaction::STATUS_WAITING_FOR_ADMIN_APPROVAL :Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM,
                    'status' => Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM,
                    'milestone_id' => $PM->id,
                    'comment' => isset($request->comment) ? $request->comment : '',
                    'performed_by' => $mileStone->project->user_id,
                    'performed_to' => $mileStone->applicant_id,
                    'release_at' => Carbon::parse(Carbon::now())->addDays(config('milestones.system_hold_days')),
                ]);
                $applicant = User::where('id', '=', $mileStone->applicant_id)->first();
                $data = [
                    'name' => $transaction->name,
                    'amount' => GeneralService::calculateServiceFee($transaction->amount),
                    'milestone_id' => $PM->id,
                    'type' => Transaction::TYPE_SERVICE_FEE,
                    'balance_before' => $applicant->account_balance,
                    'balance_after' => ($applicant->account_balance - $mileStone->amount),
                    'performed_by' => $mileStone->applicant_id,
                    'performed_to' => $mileStone->applicant_id,
                    'release_at' => Carbon::now()->addDays(config('milestones.system_hold_days')),
                    'status' => Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM,
//							          'status' => $newbieEmployer ? Transaction::STATUS_WAITING_FOR_ADMIN_APPROVAL :Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM,
                ];
                Transaction::create($data);
                Transaction::where('milestone_id', '=', $currentMilestone[0]->id)
                    ->where('status', '=',Transaction::STATUS_IN_PROGRESS_MILESTONE)
                    ->where('type', '=', Transaction::TYPE_MILESTONE_PAYMENT)->decrement('amount', $mileStoneAmountTransaction->amount);
                /** BECAUSE THE TRANSACTION ENTRY WAS CREATED WHILE REQUESTING MILESTONE */
//                        Transaction::create([
//                            'name' => $mileStone->name,
//                            'amount' => $request->amount,
//                            'milestone_id' =>  $PM->id,
//                            'due_date' => Carbon::now()->toDateTimeString(),
//                            'comment' => isset($request->comment) ? $request->comment : '',
//                            'type' => Transaction::TYPE_MILESTONE_PAYMENT,
//                            'status' => Transaction::STATUS_APPROVED,
//                        ]);
//                    }
                if ($mileStone->amount == $mileStoneAmountTransaction->amount) {
                    ProjectMilestoneStauts::where('milestone_id', '=', $mileStone->id)->update([
                        'status' => Transaction::STATUS_APPROVED
                    ]);
                }
            } else if($request->status == Transaction::STATUS_REJECTED) {
//				            ProjectMilestoneStauts::where('milestone_id', '=', $mileStone->id)->update([
//					            'status' => Transaction::STATUS_REJECTED
//				            ]);
                Transaction::where('id', '=', $transactionId)->update([
                    'status' => Transaction::STATUS_REJECTED,
                    'comment' => $request->comment
                ]);
            }

        });

//        } catch (\Exception $e) {
//            return collect([
//                'status' => 'failure',
//                'message' => $e->getLine() . ' at ' . $e->getFile()
//            ]);
//        }

        return collect([
            'status' => 'success',
            'message' => trans('common.milestone_updated')
        ]);
    }

    public function confirmMilestone(Request $request, $projectId)
    {
        try {
            $_project_milesstones = new ProjectMilestones();
            $mileStone = $_project_milesstones->with('project')->where('id', $request->milestone_id)->first();

            if ($request->amount > $mileStone->amount) {
                return collect([
                    'status' => 'failure',
                    'message' => 'Amount cannot be greater than milestone amount '.$mileStone->amount.' ...!, '
                ]);
            }

            $totalSum = Transaction::where('milestone_id' ,'=', $mileStone->id)->where('status', '!=', Transaction::STATUS_IN_PROGRESS_MILESTONE)->sum('amount');

            if($totalSum >= $mileStone->amount) {
                return collect([
                    'status' => 'failure',
                    'message' => 'Already requested amount is equal to milestone amount..!'
                ]);
            }

            $data = [
                'name' => $mileStone->name,
                'amount' => $request->amount,
                'milestone_id' => $mileStone->id,
                'type' => isset($request->type) && $request->type ? $request->type  : Transaction::TYPE_MILESTONE_PAYMENT,
                'balance_before' => \Auth::user()->account_balance,
                'balance_after' => (\Auth::user()->account_balance) - $mileStone->amount,
                'performed_by' => \Auth::user()->id,
                'performed_to' => $mileStone->project->user_id,
                'status' => Transaction::STATUS_REQUESTED
            ];
            Transaction::create($data);

            if($mileStone->project->user_id == Auth::user()->id) { // if the project owner is releasing the milestone
                User::where('id', '=', \Auth::user()->id)
                    ->decrement('account_balance', $mileStone->amount + GeneralService::calculateServiceFee($mileStone->amount));
                User::where('id', '=', \Auth::user()->id)
                    ->decrement('account_balance', GeneralService::calculateServiceFee($mileStone->amount));
            }

            return collect([
                'status' => 'success',
                'message' => 'Milestone Requested...!'
            ]);
        } catch (\Exception $e) {
            return collect([
                'status' => 'failure',
                'message' => array(
                    $e->getMessage(),
                    $e->getFile(),
                    $e->getLine()
                )
            ]);
        }
    }

    public function createRefund(Request $request, $projectId)
    {
        $milestoneStatus = ProjectMilestoneStauts::with(['milestone', 'project'])->where('milestone_id', '=', $request->milestone_id)
            ->where('project_id', '=', $projectId)->first();

        if(is_null($milestoneStatus)) {
            return collect([
                'status' => 'failure',
                'message' => trans('common.milestone_not_found')
            ]);
        }

        if(Auth::user()->account_balance < $milestoneStatus->milestone->amount) {
            return collect([
                'status' => 'failure',
                'message' => trans('common.insufficient_balance')
            ]);
        }

        if(empty($request->input('reason', ''))) {
            return collect([
                'status' => 'failure',
                'message' => trans('common.reason_refund')
            ]);
        }
        $milestone = $milestoneStatus->milestone;
        $project = $milestoneStatus->project;

        $d = ProjectDispute::where('milestone_id', '=', $request->milestone_id)
            ->where('project_id', '=', $projectId)
            ->first();
        if(isset($d)) {
            $d->status = ProjectDispute::STATUS_REFUNDED;
        }

        $milestoneStatus->update([
            'status' => ProjectMilestones::STATUS_REFUNDED,
            'comment' => $request->input('reason', '')
        ]);
        $bal = GeneralService::calculateServiceFee($milestone->amount);
        $data = [
            'milestone_id' => $request->milestone_id,
            'name' => $milestone->name,
            'amount' => ($milestone->amount - $bal),
            'due_date' => $milestone->due_date,
            'comment' => isset($request->comment) ? $request->comment : '',
            'type' => Transaction::TYPE_MILESTONE_PAYMENT_REFUND,
            'status' => Transaction::STATUS_APPROVED,
            'balance_before' => \Auth::user()->account_balance,
            'balance_after' => ((\Auth::user()->account_balance) - $milestone->amount) + GeneralService::calculateServiceFee($milestone->amount),
            'performed_by' => \Auth::user()->id,
            'performed_to' => $project->user_id,
        ];

        Transaction::create($data);
        User::where('id', '=', \Auth::user()->id)->decrement('account_balance', ($milestone->amount - $bal));
        User::where('id', '=', $project->user_id)->increment('account_balance', ($milestone->amount));


        return collect([
            'status' => 'success'
        ]);
    }

    public function checkPassword($password)
    {
        $original_pass = auth()->user()->password;
        if (Hash::check($password , $original_pass)) {
            return true;
        }
        else{
            return false;
        }
    }

}
