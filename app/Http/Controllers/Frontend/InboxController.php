<?php

namespace App\Http\Controllers\Frontend;

use App\Constants;
use App\Libraries\Pushy\PushyAPI;
use App\Models\Project;
use App\Models\ProjectApplicant;
use App\Models\Staff;
use App\Traits\sendPushNotification;
use Carbon;
use App\Http\Controllers\FrontendController;
use Illuminate\Http\Request;
use App\Models\UserInbox;
use App\Models\UserInboxMessages;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class InboxController extends FrontendController
{
    use sendPushNotification;
    public function index(Request $request)
    {
//        \Auth::guard('users')->user()->load(['myInbox', 'myInbox.fromUser', 'myInbox.messages']);
//        $myInbox = \Auth::guard('users')->user()->myInbox;
//
//        return view('frontend.inbox.index', ['myInbox' => $myInbox]);

        $category = $request->get('category');
        $isTrash = $request->get('isTrash');

        if ($isTrash == 0 && $category == 0) {
            $category = UserInbox::CATEGORY_NORMAL;
        }

        $tab = 0;
        if ($category == UserInbox::CATEGORY_NORMAL) {
//            $myInbox = \Auth::guard('users')->user()->myInbox()
            $myInbox = UserInbox::where(function ($query) {
                $query->where('from_user_id', \Auth::id());
                $query->orWhere('to_user_id', \Auth::id());
            })
                ->with('messages')
                ->whereHas('messages')
                ->where('category', UserInbox::CATEGORY_NORMAL)
                ->where('is_trash', 0)
                ->orderBy('id', 'desc')
                ->paginate(10);

            $actions = UserInbox::getInboxActionLists();
            $tab = 1;
        } else if ($category == UserInbox::CATEGORY_SYSTEM) {
//            $myInbox = \Auth::guard('users')->user()->myInbox()
            $myInbox = UserInbox::where(function ($query) {
                $query->where('from_user_id', \Auth::id());
                $query->orWhere('to_user_id', \Auth::id());
            })
                ->with('messages')
                ->whereHas('messages')
                ->where('category', UserInbox::CATEGORY_SYSTEM)
                ->where('is_trash', 0)
                ->orderBy('id', 'desc')
                ->paginate(10);

            $actions = UserInbox::getInboxActionLists();
            $tab = 2;
        } else if ($isTrash == 1) {
//            $myInbox = \Auth::guard('users')->user()->myInbox()
            $myInbox = UserInbox::where(function ($query) {
                $query->where('from_user_id', \Auth::id());
                $query->orWhere('to_user_id', \Auth::id());
            })
                ->with('messages')
                ->whereHas('messages')
                ->where('is_trash', 1)
                ->orderBy('created_at', 'desc')
                ->paginate(10);

            $actions = UserInbox::getDeleteActionLists();
            $tab = 3;
        }

        $response_time = userAverageResponseTime();
        return view('frontend.inbox.index', [
            'myInbox' => $myInbox,
            'actions' => $actions,
            'tab' => $tab,
            'response_time' => $response_time
        ]);
    }

    public function readInbox(Request $request)
    {
        if ($request->has('id') && $request->get('id') != '') {
            $inbox = UserInbox::find($request->get('id'));

            if ($inbox) {
                if ($inbox->to_user_id != \Auth::guard('users')->user()->id) {
                    return makeJSONResponse(false, trans('common.action_no_permission'));
                }

                $user = \Auth::guard('users')->user();

                $unread = UserInboxMessages::where('inbox_id', '=', $inbox->id)->where('to_user_id', '=', $user->id)->where('is_read', '=', 0);
                try {
                    \DB::beginTransaction();
                    $count = $unread->count();
                    if ($count > 0) {
                        $user = User::where('id', '=', \Auth::guard('users')->user()->id)->first();
                        $user->unread_message = $user->unread_message - $count;
                        if ($user->unread_message < 0) {
                            $user->unread_message = 0;
                        }
                    }

                    $user->save();
                    $unread->update(['is_read' => 1]);

                    \DB::commit();
                    return makeJSONResponse(true, 'Success', ['unread' => $user->unread_message]);
                } catch (\Exception $e) {
                    return makeJSONResponse(false, $e->getMessage());
                    \DB::rollback();
                }
            } else {
                return makeJSONResponse(false, trans('common.unknown_error'));
            }
        } else {
            return makeJSONResponse(false, trans('common.unknown_error'));
        }
    }

    public function acceptJob(Request $request)
    {
        $projectId = $request->get('projectId');
        $inboxId = $request->get('inboxId');

        try {
            \DB::beginTransaction();

            //check whether exist or not
            $count = ProjectApplicant::where('project_id', $projectId)
                ->where('user_id', \Auth::guard('users')->user()->id)
                ->where(function ($query) {
                    $query->where('status', ProjectApplicant::STATUS_APPLICANT_ACCEPTED);
                    $query->orWhere('status', ProjectApplicant::STATUS_APPLICANT_REJECTED);
                })
                ->count();

            if ($count > 0) {
                return makeJSONResponse(false, trans('common.applicant_already_reply'));
            }

            $projectApplicant = ProjectApplicant::where('project_id', $projectId)
                ->where('user_id', \Auth::guard('users')->user()->id)
                ->first();
            $projectApplicant->status = ProjectApplicant::STATUS_APPLICANT_ACCEPTED;
            $projectApplicant->display_order = ProjectApplicant::DISPLAY_ORDER_PROGRESSING;
            $projectApplicant->is_read = ProjectApplicant::IS_READ_NO;
            $projectApplicant->save();

            $project = Project::find($projectId);
            $project->status = Project::STATUS_HIRED;
            $project->is_read = Project::IS_READ_NO;
            $project->save();

            $uim = new UserInboxMessages();
            $uim->inbox_id = $inboxId;
            if ($project->type == Project::PROJECT_TYPE_COMMON) {
                $uim->from_user_id = \Auth::guard('users')->user()->id;
                $uim->to_user_id = $project->user_id;
                $uim->from_staff_id = null;
                $uim->to_staff_id = null;
            } else if ($project->type == Project::PROJECT_TYPE_OFFICIAL) {
                $uim->from_user_id = \Auth::guard('users')->user()->id;
                $uim->to_user_id = null;
                $uim->from_staff_id = null;
                $uim->to_staff_id = $project->staff_id;
            }

            $uim->project_id = $project->id;
            $uim->is_read = 0;
            $uim->type = UserInboxMessages::TYPE_PROJECT_APPLICANT_ACCEPTED;
            $uim->custom_message = null;
            $uim->quote_price = null;
            $uim->pay_type = null;
            $uim->save();

            \DB::commit();
            return makeJSONResponse(true, trans('common.success_accept_job'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function rejectJob(Request $request)
    {
        $projectId = $request->get('projectId');
        $inboxId = $request->get('inboxId');

        try {
            \DB::beginTransaction();

            //check whether exist or not
            $count = ProjectApplicant::where('project_id', $projectId)
                ->where('user_id', \Auth::guard('users')->user()->id)
                ->where(function ($query) {
                    $query->where('status', ProjectApplicant::STATUS_APPLICANT_ACCEPTED);
                    $query->orWhere('status', ProjectApplicant::STATUS_APPLICANT_REJECTED);
                })
                ->count();

            if ($count > 0) {
                return makeJSONResponse(false, trans('common.applicant_already_reply'));
            }

            $project = Project::find($projectId);
            $project->status = Project::STATUS_HIRED;
            $project->is_read = Project::IS_READ_NO;
            $project->save();

            $projectApplicant = ProjectApplicant::where('project_id', $projectId)
                ->where('user_id', \Auth::guard('users')->user()->id)
                ->first();
            $projectApplicant->status = ProjectApplicant::STATUS_APPLICANT_REJECTED;
            $projectApplicant->is_read = ProjectApplicant::IS_READ_NO;
            $projectApplicant->save();

            $uim = new UserInboxMessages();
            $uim->inbox_id = $inboxId;

            if ($project->type == Project::PROJECT_TYPE_COMMON) {
                $uim->from_user_id = \Auth::guard('users')->user()->id;
                $uim->to_user_id = $project->user_id;
                $uim->from_staff_id = null;
                $uim->to_staff_id = null;
            } else if ($project->type == Project::PROJECT_TYPE_OFFICIAL) {
                $uim->from_user_id = \Auth::guard('users')->user()->id;
                $uim->to_user_id = null;
                $uim->from_staff_id = null;
                $uim->to_staff_id = $project->staff_id;
            }

            $uim->project_id = $project->id;
            $uim->is_read = 0;
            $uim->type = UserInboxMessages::TYPE_PROJECT_APPLICANT_REJECTED;
            $uim->custom_message = null;
            $uim->quote_price = null;
            $uim->pay_type = null;
            $uim->save();

            \DB::commit();
            return makeJSONResponse(true, trans('common.success_accept_job'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function acceptRequestForContactInfo(Request $request)
    {
        try {
            $receiverId = $request->get('receiverId');
            $inboxId = $request->get('inboxId');

            $user = \Auth::guard('users')->user();
            $receiver = User::find($receiverId);
            $ui = UserInbox::find($inboxId);

            //check exist or not
            $count = UserInboxMessages::where('from_user_id', \Auth::guard('users')->user()->id)
                ->where('to_user_id', $receiver->id)
                ->where('inbox_id', $ui->id)
                ->where(function ($query) {
                    $query->where('type', UserInboxMessages::TYPE_PROJECT_AGREE_SEND_CONTACT);
                    $query->orWhere('type', UserInboxMessages::TYPE_PROJECT_REJECT_SEND_CONTACT);
                })
                ->count();

            if ($count > 0) {
                return makeJSONResponse(false, trans('common.contact_request_already_reply'));
            }

            //send email
            $senderEmail = "support@xenren.co";
            $senderName = $user->getName();

            Mail::send('mails.acceptContactInfo', array('user' => $user), function ($message) use ($senderEmail, $senderName, $receiver) {
                $message->from($senderEmail, $senderName);
                $message->to($receiver->email, $receiver->first_name . ' ' . $receiver->last_name)
                    ->subject(trans('common.contact_info'));
            });

            \DB::beginTransaction();

            $uim = new UserInboxMessages();
            $uim->inbox_id = $ui->id;
            $uim->from_user_id = \Auth::guard('users')->user()->id;
            $uim->to_user_id = $receiver->id;
            $uim->from_staff_id = null;
            $uim->to_staff_id = null;
            $uim->project_id = $ui->project_id;
            $uim->is_read = 0;
            $uim->type = UserInboxMessages::TYPE_PROJECT_AGREE_SEND_CONTACT;
            $uim->save();

            \DB::commit();
            return makeJSONResponse(true, trans('common.success_accept_contact_request'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function rejectRequestForContactInfo(Request $request)
    {
        try {
            $receiverId = $request->get('receiverId');
            $inboxId = $request->get('inboxId');

            $user = \Auth::guard('users')->user();
            $receiver = User::find($receiverId);
            $ui = UserInbox::find($inboxId);

            //check exist or not
            $count = UserInboxMessages::where('from_user_id', \Auth::guard('users')->user()->id)
                ->where('to_user_id', $receiver->id)
                ->where('inbox_id', $ui->id)
                ->where(function ($query) {
                    $query->where('type', UserInboxMessages::TYPE_PROJECT_AGREE_SEND_CONTACT);
                    $query->orWhere('type', UserInboxMessages::TYPE_PROJECT_REJECT_SEND_CONTACT);
                })
                ->count();

            if ($count > 0) {
                return makeJSONResponse(false, trans('common.contact_request_already_reply'));
            }

            //send email
            $senderEmail = "support@xenren.co";
            $senderName = $user->getName();

            Mail::send('mails.rejectContactInfo', array('user' => $user), function ($message) use ($senderEmail, $senderName, $receiver) {
                $message->from($senderEmail, $senderName);
                $message->to($receiver->email, $receiver->first_name . ' ' . $receiver->last_name)
                    ->subject(trans('common.contact_info'));
            });

            \DB::beginTransaction();

            $uim = new UserInboxMessages();
            $uim->inbox_id = $ui->id;
            $uim->from_user_id = \Auth::guard('users')->user()->id;
            $uim->to_user_id = $receiver->id;
            $uim->from_staff_id = null;
            $uim->to_staff_id = null;
            $uim->project_id = $ui->project_id;
            $uim->is_read = 0;
            $uim->type = UserInboxMessages::TYPE_PROJECT_REJECT_SEND_CONTACT;
            $uim->save();

            \DB::commit();
            return makeJSONResponse(true, trans('common.success_reject_contact_request'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function rejectInvite(Request $request)
    {
        try {
            $receiverId = $request->get('receiverId');
            $inboxId = $request->get('inboxId');

            $user = \Auth::guard('users')->user();
            $receiver = User::find($receiverId);
            $ui = UserInbox::find($inboxId);

            //check exist or not
            $count = UserInboxMessages::where('from_user_id', \Auth::guard('users')->user()->id)
                ->where('to_user_id', $receiver->id)
                ->where('inbox_id', $ui->id)
                ->where(function ($query) {
                    $query->where('type', UserInboxMessages::TYPE_PROJECT_INVITE_ACCEPT);
                    $query->orWhere('type', UserInboxMessages::TYPE_PROJECT_INVITE_REJECT);
                })
                ->count();

            if ($count > 0) {
                return makeJSONResponse(false, trans('common.invite_request_already_reply'));
            }

            //send email
            $senderEmail = "support@xenren.co";
            $senderName = $user->getName();

            Mail::send('mails.rejectInvite', array('user' => $user), function ($message) use ($senderEmail, $senderName, $receiver) {
                $message->from($senderEmail, $senderName);
                $message->to($receiver->email, $receiver->first_name . ' ' . $receiver->last_name)
                    ->subject(trans('common.invite_for_work'));
            });

            \DB::beginTransaction();

            $uim = new UserInboxMessages();
            $uim->inbox_id = $ui->id;
            $uim->from_user_id = \Auth::guard('users')->user()->id;
            $uim->to_user_id = $receiver->id;
            $uim->from_staff_id = null;
            $uim->to_staff_id = null;
            $uim->project_id = $ui->project_id;
            $uim->is_read = 0;
            $uim->type = UserInboxMessages::TYPE_PROJECT_INVITE_REJECT;
            $uim->save();

            \DB::commit();
            return makeJSONResponse(true, trans('common.success_reject_invite_for_work'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function acceptReceiveOffer(Request $request)
    {
//	    try {
        \DB::beginTransaction();
        $inboxWithProject = UserInbox::with(['project', 'fromUser', 'toUser', 'firstMessage'])->where('id', $request->inboxId)->first();
        if($inboxWithProject->to_user_id == \Auth::user()->id) {
            $user_id = $inboxWithProject->to_user_id;
        }
        if ($inboxWithProject->type == UserInbox::TYPE_PROJECT_APPLICANT_ACCEPTED) {
            return collect([
                'status' => 'failure',
                'msg' => trans('common.already_awarded')
            ]);
        }
        UserInbox::where('id', $request->inboxId)->update([
            'type' => UserInbox::TYPE_PROJECT_APPLICANT_ACCEPTED
        ]);
        $uim = UserInboxMessages::where('id', $inboxWithProject->firstMessage->id)->first();
        UserInboxMessages::where('id', $inboxWithProject->firstMessage->id)->update([
            'type' => UserInboxMessages::TYPE_PROJECT_APPLICANT_ACCEPTED
        ]);
        ProjectApplicant::where('user_id', '=', $inboxWithProject->fromUser->id)
            ->where('project_id', '=', $inboxWithProject->project_id)
            ->update([
                'status' => ProjectApplicant::STATUS_APPLICANT_ACCEPTED,
            ]);
//		    ProjectApplicant::create([
//			    'user_id' => $inboxWithProject->fromUser->id,
//			    'project_id' => $inboxWithProject->project_id,
//			    'status' => ProjectApplicant::STATUS_APPLICANT_ACCEPTED,
//			    'quote_price' => $inboxWithProject->firstMessage->quote_price,
//			    'about_me' => $inboxWithProject->firstMessage->custom_message,
//			    'display_order' => ProjectApplicant::DISPLAY_ORDER_PROGRESSING,
//			    'is_read' => 0,
//			    'pay_type' => $uim->pay_type,
//			    'daily_update' => $inboxWithProject->daily_update
//		    ]);
        if($inboxWithProject->to_user_id == \Auth::user()->id) {
            //send email to employee
            $senderEmail = "support@xenren.co";
            $senderName = 'Xenren';
            $receiver = $inboxWithProject->toUser;
            try {
                Mail::send('mails.acceptEmployeeSendOffer', array('user' => $inboxWithProject->fromUser), function ($message) use ($senderEmail, $senderName, $receiver) {
                    $message->from($senderEmail, $senderName);
                    $message->to($receiver->email, $receiver->first_name . ' ' . $receiver->last_name)
                        ->subject(trans('You Hire New Freelancer'));
                });
            } catch (\Exception $e) {
                \Log::info('Error while sending email when employer accepted the offer');
            }
        }
            //send email to employer
            $senderEmail = "support@xenren.co";
            $senderName = $inboxWithProject->toUser->getName();
            $receiver = $inboxWithProject->fromUser;

            try {
                Mail::send('mails.acceptSendOffer', array('user' => $inboxWithProject->toUser), function ($message) use ($senderEmail, $senderName, $receiver) {
                    $message->from($senderEmail, $senderName);
                    $message->to($receiver->email, $receiver->first_name . ' ' . $receiver->last_name)
                        ->subject(trans('common.offer_accepted'));
                });
            } catch (\Exception $e) {
                \Log::info('Error while sending email when employer accepted the offer');
            }

        \DB::commit();
        return collect([
            'status' => 'success',
            'msg' => trans('common.job_awarded')
        ]);
//	    } catch (\Exception $e){
//	    	return makeResponse($e, true);
//	    }
    }

    public function acceptSendOffer(Request $request)
    {
        try {
            $receiverId = $request->get('receiverId');
            $inboxId = $request->get('inboxId');
            $projectPayType = $request->get('projectPayType');

            $user = \Auth::guard('users')->user();
            $receiver = User::find($receiverId);
            $ui = UserInbox::find($inboxId);
            $dt = UserInboxMessages::where('inbox_id', '=', $ui->id)->orderBy('id', 'desc')->select(['id', 'quote_price'])->first();

            //check exist or not
            $count = UserInboxMessages::where('from_user_id', \Auth::guard('users')->user()->id)
                ->where('to_user_id', $receiver->id)
                ->where('inbox_id', $ui->id)
                ->where(function ($query) {
                    $query->where('type', UserInboxMessages::TYPE_PROJECT_ACCEPT_SEND_OFFER);
                    $query->orWhere('type', UserInboxMessages::TYPE_PROJECT_REJECT_SEND_OFFER);
                })
                ->count();

            if ($count > 0) {
                return makeJSONResponse(false, trans('common.send_offer_already_reply'));
            }

            //send email
            $senderEmail = "support@xenren.co";
            $senderName = $user->getName();

            Mail::send('mails.acceptSendOffer', array('user' => $user), function ($message) use ($senderEmail, $senderName, $receiver) {
                $message->from($senderEmail, $senderName);
                $message->to($receiver->email, $receiver->first_name . ' ' . $receiver->last_name)
                    ->subject(trans('common.offer_accepted'));
            });

            \DB::beginTransaction();
            $uim = new UserInboxMessages();
            $uim->inbox_id = $ui->id;
            $uim->from_user_id = \Auth::guard('users')->user()->id;
            $uim->to_user_id = $receiver->id;
            $uim->from_staff_id = null;
            $uim->to_staff_id = null;
            $uim->project_id = $ui->project_id;
            $uim->is_read = 0;
            $uim->type = UserInboxMessages::TYPE_PROJECT_ACCEPT_SEND_OFFER;
            $uim->save();

            $ui->type = UserInbox::TYPE_PROJECT_APPLICANT_ACCEPTED;
            $ui->save();

            ProjectApplicant::create([
                'user_id' => $ui->to_user_id,
                'project_id' => $ui->project_id,
                'status' => ProjectApplicant::STATUS_APPLICANT_ACCEPTED,
                'quote_price' => is_null($dt) ? 0 : $dt->quote_price,
                'pay_type' => $projectPayType,
                'about_me' => 'N/A',
                'display_order' => ProjectApplicant::DISPLAY_ORDER_PROGRESSING,
                'is_read' => 0,
                'daily_update' => $ui->daily_update
            ]);

            \DB::commit();
            return makeJSONResponse(true, trans('common.success_accept_send_offer'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function rejectSendOffer(Request $request)
    {
        try {
            $receiverId = $request->get('receiverId');
            $inboxId = $request->get('inboxId');

            $user = \Auth::guard('users')->user();
            $receiver = User::find($receiverId);
            $project = Project::where('user_id', '=', $receiverId)->first();
            $ui = UserInbox::find($inboxId);

            //check exist or not
            $count = UserInboxMessages::where('from_user_id', \Auth::guard('users')->user()->id)
                ->where('to_user_id', $receiver->id)
                ->where('inbox_id', $ui->id)
                ->where(function ($query) {
                    $query->where('type', UserInboxMessages::TYPE_PROJECT_ACCEPT_SEND_OFFER);
                    $query->orWhere('type', UserInboxMessages::TYPE_PROJECT_REJECT_SEND_OFFER);
                })
                ->count();

            if ($count > 0) {
                return makeJSONResponse(false, trans('common.send_offer_already_reply'));
            }

            //send email
            $senderEmail = "support@xenren.co";
            $senderName = $user->getName();

            Mail::send('mails.rejectSendOffer', array('user' => $user, 'receiver_name' => $receiver->name, 'project_name' => $project->name), function ($message) use ($senderEmail, $senderName, $receiver) {
                $message->from($senderEmail, $senderName);
                $message->to($receiver->email, $receiver->name)
                    ->subject(trans('common.offer_rejected'));
            });

            \DB::beginTransaction();

            $uim = new UserInboxMessages();
            $uim->inbox_id = $ui->id;
            $uim->from_user_id = \Auth::guard('users')->user()->id;
            $uim->to_user_id = $receiver->id;
            $uim->from_staff_id = null;
            $uim->to_staff_id = null;
            $uim->project_id = $ui->project_id;
            $uim->is_read = 0;
            $uim->type = UserInboxMessages::TYPE_PROJECT_REJECT_SEND_OFFER;
            $uim->save();

            \DB::commit();
            return makeJSONResponse(true, trans('common.success_reject_send_offer'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeJSONResponse(false, $e->getMessage());
        }
    }

//    // Only For Design
//    public function newIndex(Request $request)
//    {
////        \Auth::guard('users')->user()->load(['myInbox']);
//
//        $category = $request->get('category');
//        $isTrash = $request->get('isTrash');
//
//        if( $isTrash == 0 && $category == 0 )
//        {
//            $category = UserInbox::CATEGORY_NORMAL;
//        }
//
//        $tab = 0;
//        if( $category == UserInbox::CATEGORY_NORMAL )
//        {
////            $myInbox = \Auth::guard('users')->user()->myInbox()
//            $myInbox = UserInbox::where(function($query){
//                    $query->where('from_user_id', \Auth::id());
//                    $query->orWhere('to_user_id', \Auth::id());
//                })
//                ->where('category', UserInbox::CATEGORY_NORMAL)
//                ->where('is_trash', 0)
//                ->orderBy('created_at', 'desc')
//                ->paginate(10);
//
//            $actions = UserInbox::getInboxActionLists();
//            $tab = 1;
//        }
//        else if( $category == UserInbox::CATEGORY_SYSTEM )
//        {
////            $myInbox = \Auth::guard('users')->user()->myInbox()
//            $myInbox = UserInbox::where(function($query){
//                    $query->where('from_user_id', \Auth::id());
//                    $query->orWhere('to_user_id', \Auth::id());
//                })
//                ->where('category', UserInbox::CATEGORY_SYSTEM)
//                ->where('is_trash', 0)
//                ->orderBy('created_at', 'desc')
//                ->paginate(10);
//
//            $actions = UserInbox::getInboxActionLists();
//            $tab = 2;
//        }
//        else if( $isTrash == 1 )
//        {
////            $myInbox = \Auth::guard('users')->user()->myInbox()
//            $myInbox = UserInbox::where(function($query){
//                    $query->where('from_user_id', \Auth::id());
//                    $query->orWhere('to_user_id', \Auth::id());
//                })
//                ->where('is_trash', 1)
//                ->orderBy('created_at', 'desc')
//                ->paginate(10);
//
//            $actions = UserInbox::getDeleteActionLists();
//            $tab = 3;
//        }
//
//
//        return view('frontend.inbox.newIndex', [
//            'myInbox' => $myInbox,
//            'actions' => $actions,
//            'tab' => $tab
//        ]);
//    }

    public function messages($inbox_id)
    {
        $responseTime = userAverageResponseTime();
        $inbox = UserInbox::with('messages')->find($inbox_id);

        $inboxMessages = UserInboxMessages::where('inbox_id', $inbox->id)->with([
            'project.projectSkills.skill',
            'project.projectQuestions',
            'project.projectQuestions.applicantAnswer'
        ])->orderBy('id','ASC')->get();

        if ($inboxMessages) {
            $lastMessageID = $inboxMessages->last()->id;
        } else {
            $lastMessageID = 0;
        }

        $user = \Auth::guard('users')->user();
        foreach ($inboxMessages as $inboxMessage) {
            if ($inboxMessage->to_user_id == $user->id) {
                $inboxMessage->save();
            }
        }
        $is_assigned_project = Project::where('id', $inbox->project_id)
            ->with('awardedApplicant')
            ->whereHas('awardedApplicant', function ($q) use ($user) {
                $q->where('user_id', $user->id);
            })
            ->first();
        $is_awarded = isset($is_assigned_project) && $is_assigned_project->count() > 0 ? true : false;

        $check = UserInboxMessages::where('id', '=', isset($lastMessageID) ? $lastMessageID : null)->first();

        return view('frontend.inbox.messages', [
            'inbox' => $inbox,
            'inboxMessages' => $inboxMessages,
            'lastMessageID' => $lastMessageID,
            'user' => $user,
            'is_awarded' => $is_awarded,
            'checkUserInboxMessages' => $check,
            'response_time' => $responseTime
        ]);
    }

    public function getPrivateChat(Request $request)
    {
        $inbox = UserInbox::with('messages')->find($request->inobxId);

        $inboxMessages = UserInboxMessages::where('inbox_id', $request->inobxId)
            ->get();
        if ($inboxMessages) {
            $lastMessageID = $inboxMessages->last()->id;
        } else {
            $lastMessageID = 0;
        }

        $user = \Auth::guard('users')->user();
        foreach ($inboxMessages as $inboxMessage) {
            if ($inboxMessage->to_user_id == $user->id) {
                $inboxMessage->is_read = 1;
                $inboxMessage->save();
            }
        }

        return view('frontend.inbox.inboxMessageItem', [
            'inbox' => $inbox,
            'inboxMessages' => $inboxMessages,
            'lastMessageID' => $lastMessageID,
            'user' => $user
        ]);
    }

    public function getNewInboxMessagePost(Request $request)
    {
        $lastMessageId = $request->get('last_message_id');
        $inboxId = $request->get('inbox_id');

        $inbox = UserInbox::with('messages')->find($inboxId);
        $inboxMessages = UserInboxMessages::where('inbox_id', $inboxId)
            ->where('id', '>', $lastMessageId)
            ->get();

        if ($inboxMessages->count() != 0) {
            $lastMessageID = $inboxMessages->last()->id;
            $user = \Auth::guard('users')->user();
            foreach ($inboxMessages as $inboxMessage) {
                if ($inboxMessage->to_user_id == $user->id) {
                    $inboxMessage->is_read = 1;
                    $inboxMessage->save();
                }
            }

            $view = View::make('frontend.inbox.inboxMessageItem', [
                'inbox' => $inbox,
                'inboxMessages' => $inboxMessages,
                'lastMessageID' => $lastMessageID,
                'user' => $user
            ]);
            $contents = (string)$view;

            return response()->json([
                'status' => 'success',
                'result' => $contents,
                'last_message_id' => $lastMessageID
            ]);
        } else {
            return response()->json([
                'status' => 'success',
                'result' => ''
            ]);
        }

    }

    public function trash(Request $request)
    {
        $inboxId = $request->get('inboxId');

        try {
            $inbox = UserInbox::find($inboxId);
            $inbox->is_trash = 1;
            $inbox->save();

            return makeJSONResponse(true, trans('common.success_move_to_trash'));

        } catch (\Exception $e) {
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function trashAll(Request $request)
    {
        $inboxIds = $request->get('inboxIds');
        if (!isset($inboxIds) || $inboxIds == '' || count($inboxIds) < 1) {
            return makeJSONResponse(true, trans('common.no-record'));
        }
        try {

            foreach ($inboxIds as $inboxId) {
                $inbox = UserInbox::find($inboxId);
                $inbox->is_trash = 1;
                $inbox->save();
            }

            return makeJSONResponse(true, trans('common.success_move_to_trash'));

        } catch (\Exception $e) {
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function untrashAll(Request $request)
    {
        $inboxIds = $request->get('inboxIds');

        try {

            foreach ($inboxIds as $inboxId) {
                $inbox = UserInbox::find($inboxId);
                $inbox->is_trash = 0;
                $inbox->save();
            }

            return makeJSONResponse(true, trans('common.success_move_to_inbox'));

        } catch (\Exception $e) {
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function untrash(Request $request)
    {
        $inboxId = $request->get('inboxId');

        try {
            $inbox = UserInbox::find($inboxId);
            $inbox->is_trash = 0;
            $inbox->save();

            return makeJSONResponse(true, trans('common.success_move_to_inbox'));

        } catch (\Exception $e) {
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function markAsRead(Request $request)
    {
        $inboxIds = $request->get('inboxIds');
        if (!isset($inboxIds) || $inboxIds == '' || count($inboxIds) < 1) {
            return makeJSONResponse(true, trans('common.no-record'));
        }

        try {

            foreach ($inboxIds as $inboxId) {
                $inbox = UserInbox::find($inboxId);

                $inboxMessages = UserInboxMessages::where('inbox_id', $inbox->id)
                    ->get();

                foreach ($inboxMessages as $inboxMessage) {
                    $inboxMessage->is_read = 1;
                    $inboxMessage->save();
                }
            }

            return makeJSONResponse(true, trans('common.success_mark_as_read'));

        } catch (\Exception $e) {
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function removeAll(Request $request)
    {
        $inboxIds = $request->get('inboxIds');

        try {
            \DB::beginTransaction();
            foreach ($inboxIds as $inboxId) {
                $inbox = UserInbox::find($inboxId);

                $inboxMessages = UserInboxMessages::where('inbox_id', $inbox->id)
                    ->get();

                foreach ($inboxMessages as $inboxMessage) {
                    $inboxMessage->delete();
                }

                $inbox->delete();
            }

            \DB::commit();
            return makeJSONResponse(true, trans('common.success_remove_all'));

        } catch (\Exception $e) {
            \DB::rollback();
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function trigger($inboxId, $channel = 'private_chat')
    {
//        $event = $inboxId;
//        $pusher = \App::make('pusher');
//        $pusher->trigger($channel,
//            $event,
//            array(
//                'text' => 'Message from: ' . \Auth::user()->real_name,
//                'name' => \Auth::user()->real_name,
//                'sender_id' => \Auth::user()->id
//            ));
//
//        return collect([
//            'data' => 'success'
//        ]);
    }


    public function sendMessage(Request $request)
    {
        $inboxId = $request->get('inboxId');
        $receiverId = $request->get('receiverId');
        $message = $request->get('message');

        if ($message == "") {
            return makeJSONResponse(false, trans('common.message_can_not_empty'));
        }

        $ui = UserInbox::find($inboxId);
        ProjectApplicant::where('user_id', '=', $receiverId)->where('project_id', '=', $ui->project_id)->update([
            'status' => ProjectApplicant::STATUS_INTERVIEW,
        ]);
        $type = $ui->category == 1 ? 'private_chat' : 'system_chat';

        $user = \Auth::guard('users')->user();
        if ($ui->project_id != null) {
            $project = Project::find($ui->project_id);

            if ($project->type == Project::PROJECT_TYPE_COMMON) {
                $receiver = User::find($receiverId);
            } else if ($project->type == Project::PROJECT_TYPE_OFFICIAL) {
                $receiver = Staff::find($receiverId);
            }
        } else {
            //ask user for contact dont have project id
            $receiver = User::find($receiverId);
        }


//        try {
            $uim = new UserInboxMessages();
            $uim->inbox_id = $inboxId;

            if ($ui->project_id != null) {
                if ($project->type == Project::PROJECT_TYPE_COMMON) {
                    $uim->from_user_id = $user->id;
                    $uim->to_user_id = $receiver->id;
                    $uim->from_staff_id = null;
                    $uim->to_staff_id = null;
                } else if ($project->type == Project::PROJECT_TYPE_OFFICIAL) {
                    $uim->from_user_id = $user->id;
                    $uim->to_user_id = null;
                    $uim->from_staff_id = null;
                    $uim->to_staff_id = $receiver->id;
                }

                $uim->project_id = $ui->project_id;
            } else {
                //common project
                //user ask for contact
                $uim->from_user_id = $user->id;
                $uim->to_user_id = $receiver->id;
                $uim->from_staff_id = null;
                $uim->to_staff_id = null;
                $uim->project_id = null;
            }
            $uim->is_read = 0;
            $uim->type = UserInboxMessages::TYPE_PROJECT_NEW_MESSAGE;
            $uim->custom_message = $message;
            $uim->quote_price = null;
            $uim->pay_type = null;
            $uim->save();
            $message_p = 'New Message !..';
            $body_p = 'some one send you message';
            $this->sendNotifications($request['receiverId'],$message_p,$body_p);
            $this->updateResponseTime($request->all() , $uim);

            $a = self::trigger($inboxId, $type);

            return makeJSONResponse(true, trans('common.success_send_message'));
//        } catch (\Exception $e) {
//            return makeJSONResponse(false, $e->getMessage());
//        }
    }



    public function updateResponseTime($request , $uim){
        $userSendMessage = UserInboxMessages::where('from_user_id','=',\Auth::user()->id)->where('to_user_id','=',$request['receiverId'])->where('inbox_id','=',$request['inboxId'])->orderBy('id','desc')->first();
        $userSendMessageReply = UserInboxMessages::where('to_user_id','=',$request['receiverId'])->where('from_user_id','=',\Auth::user()->id)->where('inbox_id','=',$request['inboxId'])->orderBy('id','desc')->first();
        $sendUserMessageTime = Carbon\Carbon::parse($userSendMessage->created_at);
        $sendUserMessageReplyTime = Carbon\Carbon::parse($uim->created_at);
        $diff = $sendUserMessageTime->diffInMinutes($sendUserMessageReplyTime);
        if($userSendMessage->is_read == 1) {
            $userSendMessageReply->update([
                'response_time' => $diff,
                'is_read' => 0
            ]);
        } else {
            $userSendMessage->update([
                'response_time' => $userSendMessage->response_time,
                'is_read' => 1
            ]);
        }
        if($userSendMessageReply->is_read == 0) {
            $userSendMessageReply->update([
                'response_time' => $diff,
                'is_read' => 0
            ]);
        } else {
            $userSendMessageReply->update([
                'response_time' => $userSendMessageReply->response_time,
            ]);
        }
    }

    public function chatImage($inbox_id, $receiver_id)
    {
        return view('frontend.inbox.modalAddFileChat', [
            'inbox_id' => $inbox_id,
            'receiver_id' => $receiver_id
        ]);
    }

    public function chatImagePost(Request $request)
    {
        $inboxId = $request->get('inbox-id');
        $receiverId = $request->get('receiver-id');

        $ui = UserInbox::find($inboxId);
        $project = Project::find($ui->project_id);

        $user = \Auth::guard('users')->user();

        //new message don't bind to project
        if ($project == null || $project->type == Project::PROJECT_TYPE_COMMON) {
            $receiver = User::find($receiverId);
        } else if ($project->type == Project::PROJECT_TYPE_OFFICIAL) {
            $receiver = Staff::find($receiverId);
        }

        try {
            if ($request->hasFile('file')) {
                $ui = UserInbox::find($inboxId);

                $uim = new UserInboxMessages();
                $uim->inbox_id = $inboxId;
                if ($project == null || $project->type == Project::PROJECT_TYPE_COMMON) {
                    $uim->from_user_id = $user->id;
                    $uim->to_user_id = $receiver->id;
                    $uim->from_staff_id = null;
                    $uim->to_staff_id = null;
                } else if ($project->type == Project::PROJECT_TYPE_OFFICIAL) {
                    $uim->from_user_id = $user->id;
                    $uim->to_user_id = null;
                    $uim->from_staff_id = null;
                    $uim->to_staff_id = $receiver->id;
                }

                $uim->project_id = $ui->project_id;
                $uim->is_read = 0;
                $uim->type = UserInboxMessages::TYPE_PROJECT_FILE_UPLOAD;
                $uim->custom_message = null;
                $uim->quote_price = null;
                $uim->pay_type = null;
                $uim->file = $request->file('file');
                $uim->save();
            } else {
                return makeJSONResponse(false, trans('common.file_can_not_empty'));
            }

            return makeJSONResponse(true, trans('common.submit_successful'));
        } catch (Exception $e) {
            return makeJSONResponse(false, $e->getMessage());
        }
    }

//    public function chatDetails() {
//        return view('frontend.inbox.chatDetails');
//    }
//
//    public function chatDetailsSendInvitation() {
//        return view('frontend.inbox.sendInvitation');
//    }
//
//    public function chatDetailsSendOffer() {
//        return view('frontend.inbox.sendOffer');
//    }
//
//    public function requestForContactA() {
//        return view('frontend.inbox.requestForContactA');
//    }
//
//    public function requestForContact() {
//        return view('frontend.inbox.requestForContact');
//    }
}
