<?php

namespace App\Http\Controllers\Frontend;

use App\Models\User;
use App\Models\WorldCountries;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserCurrencyController extends Controller
{
    public function saveUserCurrency(Request $request){
        $userID = \Auth::user()->id;
        User::where('id','=',$userID)->update([
           'country_id' => $request->currency
        ]);

        return collect([
           'status' => true,
           'message' => 'currency added successfully'
        ]);
    }
}
