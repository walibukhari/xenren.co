<?php

namespace App\Http\Controllers;

use App\Constants;
use App\Http\Requests\Frontend\SendRequestFormRequest;
use App\Models\Faq;
use App\Models\HiringJobPosition;
use App\Models\News;
use App\Models\Project;
use App\Models\ProjectApplicant;
use App\Models\SharedOffice;
use App\Models\SharedOfficeFinance;
use App\Models\Skill;
use App\Models\SkillKeywordGroup;
use App\Models\TwilioCode;
use App\Models\User;
use App\Models\UserInboxMessages;
use App\UserFeedback;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use RevenueMonster\SDK\Exceptions\ApiException;
use RevenueMonster\SDK\Exceptions\ValidationException;
use RevenueMonster\SDK\Request\QRPay;
use RevenueMonster\SDK\Request\WebPayment;
use RevenueMonster\SDK\RevenueMonster;
use Session;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
    }

    public function choseOption($lang, $seatId, $officeId)
    {
        return view('frontend.option.index', [
            'lang' => $lang,
            'seatId' => $seatId,
            'officeId' => $officeId,
        ]);
    }

    public function gotToPhone($lang, $seatId, $officeId)
    {
        return view('frontend.option.not-exist', [
            'lang' => $lang,
            'seatId' => $seatId,
            'officeId' => $officeId,
        ]);
    }

    public function savePhoneData(Request $request)
    {
        if (!is_null($request->phone_number)) {
            $phone_number = '+'.$request->phone_number;
            $randomSixDigitCode = generateRandomString(6);
            $this->createTwilioEntry($randomSixDigitCode, $phone_number, $request);
            twiloSendSMS($phone_number, $randomSixDigitCode);

            return redirect(getLocale().'/enter-code');
        }

        return back()->with('error', 'some thing went wrong please try again');
    }

    public function enterCode()
    {
        return view('frontend.option.code');
    }

    public function matchCode(Request $request)
    {
        $checkCodeMatch = TwilioCode::where('code', '=', $request->code)->first();
        if (is_null($checkCodeMatch)) {
            return redirect(getLocale().'/enter-phone-number');
        }

        return redirect(getLocale().'/select-seat/'.$request->code);
    }

    public function createTwilioEntry($randomSixDigitCode, $phone_number, $request)
    {
        $checkPhoneNumberExist = TwilioCode::where('phone_number', '=', $phone_number)->first();

        $checkUserExist = User::where('email', '=', str_replace('+', '', $phone_number).'@gmail.com')->first();
        if (is_null($checkUserExist)) {
            $user = User::create([
                'email' => str_replace('+', '', $phone_number).'@gmail.com',
                'password' => bcrypt(str_replace('+', '', $phone_number)),
                'handphone_no' => $phone_number,
            ]);
        } else {
            $user = User::where('id', '=', $checkUserExist->id)->update([
                'password' => bcrypt(str_replace('+', '', $phone_number)),
            ]);
        }
        if (is_null($checkPhoneNumberExist)) {
            TwilioCode::create([
                'phone_number' => $phone_number,
                'code' => $randomSixDigitCode,
                'seat_id' => $request['seat_id'],
                'office_id' => $request['office_id'],
            ]);
        } else {
            TwilioCode::where('phone_number', '=', $phone_number)->update([
                'code' => $randomSixDigitCode,
                'seat_id' => $request['seat_id'],
                'office_id' => $request['office_id'],
            ]);
        }
    }

    public function selectSeat($lang, $code)
    {
        $getData = TwilioCode::where('code', '=', $code)->first();
        $getUser = User::where('email', '=', $getData->phone_number)->first();
        $explode = explode('@', $getUser->email);
        $credtionals = [
            'email' => $getUser->email,
            'password' => $explode[0],
        ];
        $token = $this->getToken($credtionals);
        $getOffice = SharedOffice::where('id', '=', $getData->office_id)->with('officeProducts')->first();
        if (isset($getOffice->officeProducts)) {
            return view('frontend.option.select-seat', [
                'office' => $getOffice,
                'access_token' => $token,
                'office_products' => $getOffice->officeProducts,
                'user' => $getUser,
            ]);
        }

        return abort(403);
    }

    public function getToken($credtionals)
    {
        try {
            if (!$token = JWTAuth::attempt($credtionals)) {
                return response()->json([
                    'success' => 'false',
                    'message' => 'We cant find an account with this credentials. Please make sure you entered the right information and you have verified your email address.',
                ], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['success' => false, 'error' => 'Failed to login, please try again.', 'message' => $e->getMessage()], 500);
        }

        return $token;
    }

    public function selectSeatPost(Request $request)
    {
        if (is_null($request->seat_id)) {
            return back()->with('error', 'please select seat');
        }
        dd($request->all());
    }

    public function userHistory($userId)
    {
        $history = SharedOfficeFinance::where('user_id', '=', $userId)->with('office.product')->get();

        return view('frontend.option.history', [
            'history' => $history,
        ]);
    }

    public function rm()
    {
        $rm = new RevenueMonster([
            'clientId' => '1573107227524768921',
            'clientSecret' => 'olLXugBNyRTgDLOMaMFBalLGBjRYtdOW',
            'privateKey' => file_get_contents(public_path('rm-live.pem')),
            'isSandbox' => false,
        ]);

        // Get merchant profile
//        try {
//            $response = $rm->merchant->profile();
//            dd($response);
//        } catch(ApiException $e) {
//            echo "statusCode : {$e->getCode()}, errorCode : {$e->getErrorCode()}, errorMessage : {$e->getMessage()}";
//            dd($e);
//        } catch(Exception $e) {
//            echo $e->getMessage();
//            dd($e);
//        }

        // Get merchant subscriptions
//        try {
//            $response = $rm->merchant->subscriptions();
//            dd($response);
//        } catch(ApiException $e) {
//            echo "statusCode : {$e->getCode()}, errorCode : {$e->getErrorCode()}, errorMessage : {$e->getMessage()}";
//        } catch(Exception $e) {
//            echo $e->getMessage();
//        }

        // create QR pay
        try {
            $qrPay = new QRPay();
            $qrPay->currencyType = 'MYR';
            $qrPay->amount = 100 * 0.1;
            $qrPay->isPreFillAmount = true;
            $qrPay->order->title = '服务费';
            $qrPay->order->detail = 'testing';
            $qrPay->method = [];
            $qrPay->redirectUrl = 'http://f6264853.ngrok.io/revenue-monster/redirect';
            $qrPay->storeId = '1571728609803307378';
            $qrPay->type = 'DYNAMIC';
            $response = $rm->payment->qrPay($qrPay);
            dump($response);
        } catch (ApiException $e) {
            echo "statusCode : {$e->getCode()}, errorCode : {$e->getErrorCode()}, errorMessage : {$e->getMessage()}";
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        try {
            $wp = new WebPayment();
            $wp->order->id = '10033';
            $wp->order->title = 'Testing Web Payment';
            $wp->order->currencyType = 'MYR';
            $wp->order->amount = 100;
            $wp->order->detail = '';
            $wp->order->additionalData = '';
            $wp->storeId = '1571728609803307378';
            $wp->redirectUrl = 'http://f6264853.ngrok.io/revenue-monster/redirect';
            $wp->notifyUrl = 'http://f6264853.ngrok.io/revenue-monster/notify';
            $wp->layoutVersion = 'v1';
//            $wp->method = ["BOOST_MY"];

            $response = $rm->payment->createWebPayment($wp);
            echo $response->checkoutId; // Checkout ID
            echo $response->url; // Payment gateway url
            dd($response);
        } catch (ApiException $e) {
            echo "statusCode : {$e->getCode()}, errorCode : {$e->getErrorCode()}, errorMessage : {$e->getMessage()}";
        } catch (ValidationException $e) {
            var_dump($e->getMessage());
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function redirect(Request $request)
    {
        \Log::info($request->all());
    }

    public function notify(Request $request)
    {
        \Log::info($request->all());
    }

    public function xenrenSearchFreelancers(Request $request)
    {
        return view('xenrenSearchFreelancers.index');
    }

    public function getExperts(Request $request)
    {
        $filter = [];
        $filter['name'] = null;
        $filter['skill_id_list'] = null;
        $filter['job_position_id'] = null;
        $filter['job_position_name'] = null;
        $filter['hourly_range_id'] = null;
        $filter['hourly_range_name'] = null;
        $filter['status_id'] = null;
        $filter['review_type_id'] = null;
        $filter['review_type_name'] = null;
        $filter['job_title'] = null;
        $filter['last_activities_id'] = null;
        $filter['country_id'] = null;
        $filter['country_name'] = null;
        $filter['language_id'] = null;

        $users = User::with('job_position', 'skills.skill', 'comment', 'languages');

        if ($request->has('name') && '' != $request->get('name')) {
            $users = $users->where('name', 'LIKE', '%'.$request->get('name').'%')
                ->orWhere('name', 'LIKE', '%'.$request->get('name').'%')
            ;
            $filter['name'] = $request->get('name');
        }

        //remove the account which are freeze
        $users = $users->where('is_freeze', 0)->where('name', '!=', null);

        $userCount = $users->count();

        $mode = 'view';
        if (null == $filter['name'] && null == $filter['skill_id_list'] && null == $filter['job_position_id']
            && null == $filter['job_position_name'] && null == $filter['hourly_range_id'] && null == $filter['hourly_range_name']
            && null == $filter['status_id'] && null == $filter['review_type_id'] && null == $filter['review_type_name']
            && null == $filter['job_title'] && null == $filter['last_activities_id'] && null == $filter['country_id']
            && null == $filter['country_name'] && null == $filter['language_id']
        ) {
            $mode = 'view';
        } else {
            $mode = 'search';
        }

        $paginateNumber = 9;
        $count = $users->count();
        if ($count / $paginateNumber > 100) {
            $paginateNumber = $count / 100;
        }

        if ('view' == $mode) {
            $users = $users->orderBy('initial_display', 'desc')
                ->paginate($paginateNumber)
            ;
        } else {
            $users = $users->orderBy('search_order', 'desc')
                ->orderBy('last_login_at', 'Desc')
                ->paginate($paginateNumber)
            ;
        }

        return $users;
    }

    public function setPassword()
    {
        return view('frontend.newDesign.setPassword');
    }

    public function acceptInvitation()
    {
        return view('frontend.newDesign.acceptInvitation');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageName = $_SERVER['REQUEST_URI'];

        return view('home', ['pageName' => $pageName]);
    }

    public function evidence()
    {
        $pageName = $_SERVER['REQUEST_URI'];

        return view('frontend.evidence.index', ['pageName' => $pageName]);
    }

    public function xenrenLanding()
    {
        $lang = Session::get('lang');

        //redirect the page back to original location after login
        $redirectUrl = Session::get('redirect_url');
        if ('' != $redirectUrl) {
            $previousPage = Session::get('previous_page');
            if ('login_page' == $previousPage) {
                Session::put('redirect_url', '');
                Session::put('previous_page', '');

                return Redirect::to($redirectUrl);
            }
        }

        if ('cn' == $lang || '' == $lang || !isset($lang)) {
            $language = 1;
        } else {
            $language = 2;
        }
        $newsList = News::with(['stuff'])->where('language', $language)->get();

        $skillList = Skill::getSearchSkillData(Skill::SEARCH_TYPE_WHITE);

        //check user ip address
        $ip = $_SERVER['REMOTE_ADDR'];

        try {
            $response = geoip(request()->ip());
//            \Log::info('********GEOIP RESPONSE*********');
//            \Log::info($response->iso_code);
//            \Log::info('********GEOIP RESPONSE*********');
            $lowerCase = strtolower($response->iso_code);
            if (null == $lang) {
                if ('tw' == $lowerCase || 'cn' == $lowerCase || 'zh' == $lowerCase) {
                    Session::put('lang', 'cn');
                    App::setLocale('cn');
                } else {
                    Session::put('lang', 'en');
                    App::setLocale('en');
                }
            }
        } catch (\Exception $e) {
            \Log::info('********CURL REQUEST EXCEPTION*********');
            \Log::info($e->getMessage());
            \Log::info('********CURL REQUEST EXCEPTION*********');
        }
//        $ip = "36.255.116.0"; //China
//        $ip = "218.161.47.231"; //Taiwan
//        $ip = "36.255.116.0"; //Hong Kong
//        $ip = "175.141.133.119"; //Malaysia
        // $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));

        if (\Auth::check()) {
            //if user login
            //check user language preference, then load the selected language in preference
            //user language preference is null, then base on ip set the language
            //china, hong kong and taiwan auto set to chinese if null
            //the rest of country set english

            $user = \Auth::guard('users')->user();
            if (null == $user->language_preference) {
//                if( $details->city == "CN" || $details->city == "TW" || $details->city == "HK" )
//                {
//                    $user->language_preference = User::LANGUAGE_PREFERENCE_CHINESE;
//                    $user->save();
//                    $language = 1;
//                    Session::put('lang', "cn");
//                    app()->setLocale("cn");
//
//                }
//                else
//                {
//                    $user->language_preference = User::LANGUAGE_PREFERENCE_ENGLISH;
//                    $user->save();
//                    $language = 2;
//                    Session::put('lang', "en");
//                    app()->setLocale("en");
//                }
            }
        }

        //if user not login set all to chinese
        //do noting

//            if( $details->country == "CN" || $details->country == "TW" || $details->country == "HK" )
//            {
//                $language = 1;
//                Session::put('lang', "cn");
//                app()->setLocale("cn");
//
//            }
//            else
//            {
//                $language = 2;
//                Session::put('lang', "en");
//                app()->setLocale("en");
//            }

        $userStatus = User::getStatusLists();

        $isWechatBrowser = Session::get('is_wechat_browser');
        if (null != $isWechatBrowser && 'true' == $isWechatBrowser) {
            return view('mobileWebLanding.default', [
                'skillList' => $skillList,
            ]);
        }

        return view('home-xenren', [
            'newsList' => $newsList,
            'userStatus' => $userStatus,
            'language' => $language,
            'skillList' => $skillList,
        ]);
    }

    public function storeScreenWidth()
    {
        $screenWidth = $_REQUEST['screenwidth'];
        Session::put('screenWidth', $screenWidth);
    }

    public function faqBuyer(Request $request)
    {
        $searchedArticles = null;
        if ($request->has('search')) {
            $searchWords = $request->search;
            $searchedArticlesQuery = Faq::query()->where(function ($query) use ($searchWords) {
                $terms = array_unique(explode(' ', $searchWords));
                $counter = 0;
                foreach ($terms as $term) {
                    foreach (['title_en', 'title_cn', 'text_en', 'text_cn'] as $key => $value) {
                        if (0 === $counter) {
                            $query->where($value, 'like', "%{$term}%");
                        } else {
                            $query->orWhere($value, 'like', "%{$term}%");
                        }
                        ++$counter;
                    }
                }
            });
            $searchedArticles = $searchedArticlesQuery->limit(10)->get();
            if (count($searchedArticles) > 0) {
                $searchedArticles = $searchedArticles->map(function ($item) use ($searchWords) {
                    foreach (['title_en', 'title_cn', 'text_en', 'text_cn'] as $column) {
                        $terms = array_unique(explode(' ', $searchWords));
                        foreach ($terms as $term) {
                            $item[$column] = preg_replace('/\b('.preg_quote($term).')\b/i', '<strong>$1</strong>', $item[$column]);
                        }
                        $item[$column] = str_limit($item[$column], 150);
                    }

                    return $item;
                });
            }
        }
        $buyer = Faq::find(2);
        $seller = Faq::find(3);
        $suggested = Faq::where('parent_id', '!=', 'null')->orderBy('created_at', 'asc')->paginate(5);
        $pagination = Faq::where('parent_id', '!=', 'null')->orderBy('created_at', 'asc')->get();
        $pagination = (self::paginate($pagination, 15, $request->page ?: 1));

        $locale = app()->getLocale();

        return view(
            'frontend.faq.faq-buyer',
            [
                'searched_articles' => $searchedArticles,
                'locale' => strtolower($locale) ?? '',
                'buyer' => $buyer,
                'seller' => $seller,
                'suggested' => $suggested,
                'pagination' => $pagination,
            ]
        );
    }
    public function faqBuyerFreelancer(Request $request)
    {
        $searchedArticles = null;
        if ($request->has('search')) {
            $searchWords = $request->search;
            $searchedArticlesQuery = Faq::query()->where(function ($query) use ($searchWords) {
                $terms = array_unique(explode(' ', $searchWords));
                $counter = 0;
                foreach ($terms as $term) {
                    foreach (['title_en', 'title_cn', 'text_en', 'text_cn'] as $key => $value) {
                        if (0 === $counter) {
                            $query->where($value, 'like', "%{$term}%");
                        } else {
                            $query->orWhere($value, 'like', "%{$term}%");
                        }
                        ++$counter;
                    }
                }
            });
            $searchedArticles = $searchedArticlesQuery->limit(10)->get();
            if (count($searchedArticles) > 0) {
                $searchedArticles = $searchedArticles->map(function ($item) use ($searchWords) {
                    foreach (['title_en', 'title_cn', 'text_en', 'text_cn'] as $column) {
                        $terms = array_unique(explode(' ', $searchWords));
                        foreach ($terms as $term) {
                            $item[$column] = preg_replace('/\b('.preg_quote($term).')\b/i', '<strong>$1</strong>', $item[$column]);
                        }
                        $item[$column] = str_limit($item[$column], 150);
                    }

                    return $item;
                });
            }
        }
        $buyer = Faq::find(2);
        $seller = Faq::find(3);
        $suggested = Faq::where('type', '=', Faq::FREELANCE_FAQ)->orderBy('created_at', 'asc')->paginate(5);
        $pagination = Faq::where('type', '=', Faq::FREELANCE_FAQ)->orderBy('created_at', 'asc')->get();
        $pagination = (self::paginate($pagination, 15, $request->page ?: 1));

        $locale = app()->getLocale();

        return view(
            'frontend.faq.freelancer.faq-buyer',
            [
                'searched_articles' => $searchedArticles,
                'locale' => strtolower($locale) ?? '',
                'buyer' => $buyer,
                'seller' => $seller,
                'suggested' => $suggested,
                'pagination' => $pagination,
            ]
        );
    }

    public function faqBuyerEmployer(Request $request)
    {
        $searchedArticles = null;
        if ($request->has('search')) {
            $searchWords = $request->search;
            $searchedArticlesQuery = Faq::query()->where(function ($query) use ($searchWords) {
                $terms = array_unique(explode(' ', $searchWords));
                $counter = 0;
                foreach ($terms as $term) {
                    foreach (['title_en', 'title_cn', 'text_en', 'text_cn'] as $key => $value) {
                        if (0 === $counter) {
                            $query->where($value, 'like', "%{$term}%");
                        } else {
                            $query->orWhere($value, 'like', "%{$term}%");
                        }
                        ++$counter;
                    }
                }
            });
            $searchedArticles = $searchedArticlesQuery->limit(10)->get();
            if (count($searchedArticles) > 0) {
                $searchedArticles = $searchedArticles->map(function ($item) use ($searchWords) {
                    foreach (['title_en', 'title_cn', 'text_en', 'text_cn'] as $column) {
                        $terms = array_unique(explode(' ', $searchWords));
                        foreach ($terms as $term) {
                            $item[$column] = preg_replace('/\b('.preg_quote($term).')\b/i', '<strong>$1</strong>', $item[$column]);
                        }
                        $item[$column] = str_limit($item[$column], 150);
                    }

                    return $item;
                });
            }
        }
        $buyer = Faq::find(2);
        $seller = Faq::find(3);
        $suggested = Faq::where('type', '=', Faq::EMPLOYEE_FAQ)->orderBy('created_at', 'asc')->paginate(5);
        $pagination = Faq::where('type', '=', Faq::EMPLOYEE_FAQ)->orderBy('created_at', 'asc')->get();
        $pagination = (self::paginate($pagination, 15, $request->page ?: 1));

        $locale = app()->getLocale();

        return view(
            'frontend.faq.employer.faq-buyer',
            [
                'searched_articles' => $searchedArticles,
                'locale' => strtolower($locale) ?? '',
                'buyer' => $buyer,
                'seller' => $seller,
                'suggested' => $suggested,
                'pagination' => $pagination,
            ]
        );
    }

    public function coWorkfaqSpaceOwner(Request $request)
    {
        $buyer = Faq::find(2);
        $seller = Faq::find(3);
        $suggested = Faq::where('type', '=', Faq::CO_WORK_OWNER_FAQ)->orderBy('created_at', 'asc')->paginate(5);
        $pagination = Faq::where('type', '=', Faq::CO_WORK_OWNER_FAQ)->where('type', '=', Faq::CO_WORK_FAQ)->orderBy('created_at', 'asc')->get();
        $pagination = (self::paginate($pagination, 15, $request->page ?: 1));

        return view(
            'frontend.coworkFaq.coworkFaq',
            [
                'buyer' => $buyer,
                'seller' => $seller,
                'suggested' => $suggested,
                'pagination' => $pagination,
            ]
        );
    }

    public function coWorkfaqUser(Request $request)
    {
        $buyer = Faq::find(2);
        $seller = Faq::find(3);
        $suggested = Faq::where('type', '=', Faq::CO_WORK_FAQ)->orderBy('created_at', 'asc')->paginate(5);
        $pagination = Faq::where('type', '=', Faq::CO_WORK_FAQ)->where('type', '=', Faq::CO_WORK_FAQ)->orderBy('created_at', 'asc')->get();
        $pagination = (self::paginate($pagination, 15, $request->page ?: 1));

        return view(
            'frontend.coworkFaq.user.coworkFaq',
            [
                'buyer' => $buyer,
                'seller' => $seller,
                'suggested' => $suggested,
                'pagination' => $pagination,
            ]
        );
    }

    public function coWorkfaqQuestionMain($id = null, Request $request)
    {
//        $get = Faq::find($id);

//        $anchestro = $get->getAncestors()->where('id',2);

//        dd($anchestro);

        $data['faq'] = Faq::find($id);
        $data['coworker'] = Faq::find($id);
        $data['suggested'] = Faq::where('type', '=', Faq::CO_WORK_OWNER_FAQ)->where('type', '=', Faq::CO_WORK_FAQ)->orderBy('created_at', 'asc')->paginate(5);
        $pagination = Faq::where('type', '=', Faq::CO_WORK_OWNER_FAQ)->where('type', '=', Faq::CO_WORK_FAQ)->orderBy('created_at', 'asc')->get();
        $pagination = (self::paginate($pagination, 15, $request->page ?: 1));
        $data['pagination'] = $pagination;
        $data['id'] = $id;

        return view('frontend.coworkFaq.coworkFaqQuestion', $data);
    }
    public function coWorkfaqQuestionMainUser($id = null, Request $request)
    {
//        $get = Faq::find($id);

//        $anchestro = $get->getAncestors()->where('id',2);

//        dd($anchestro);

        $data['faq'] = Faq::find($id);
        $data['coworker'] = Faq::find($id);
        $data['suggested'] = Faq::where('type', '=', Faq::CO_WORK_FAQ)->where('type', '=', Faq::CO_WORK_FAQ)->orderBy('created_at', 'asc')->paginate(5);
        $pagination = Faq::where('type', '=', Faq::CO_WORK_FAQ)->where('type', '=', Faq::CO_WORK_FAQ)->orderBy('created_at', 'asc')->get();
        $pagination = (self::paginate($pagination, 15, $request->page ?: 1));
        $data['pagination'] = $pagination;
        $data['id'] = $id;

        return view('frontend.coworkFaq.coworkFaqQuestion', $data);
    }

    public function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);

        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage);
    }

    public function faqQuestionMain($id = null, Request $request)
    {
//        $get = Faq::find($id);

//        $anchestro = $get->getAncestors()->where('id',2);

//        dd($anchestro);

        $data['faq'] = Faq::find($id);
        $data['buyer'] = Faq::find(2);
        $data['seller'] = Faq::find(3);
        $data['suggested'] = Faq::where('parent_id', '!=', 'null')->orderBy('created_at', 'asc')->paginate(5);
        $pagination = Faq::where('parent_id', '!=', 'null')->orderBy('created_at', 'asc')->get();
        $pagination = (self::paginate($pagination, 15, $request->page ?: 1));
        $data['pagination'] = $pagination;

        return view('frontend.faq.faq-question-main', $data);
    }
    public function faqQuestionMainFreelancer($id = null, Request $request)
    {
//        $get = Faq::find($id);

//        $anchestro = $get->getAncestors()->where('id',2);

//        dd($anchestro);

        $data['faq'] = Faq::find($id);
        $data['buyer'] = Faq::find(2);
        $data['seller'] = Faq::find(3);
        $data['suggested'] = Faq::where('type', '=', Faq::FREELANCE_FAQ)->orderBy('created_at', 'asc')->paginate(5);
        $pagination = Faq::where('type', '=', Faq::FREELANCE_FAQ)->orderBy('created_at', 'asc')->get();
        $pagination = (self::paginate($pagination, 15, $request->page ?: 1));
        $data['pagination'] = $pagination;

        return view('frontend.faq.freelancer.faq-question-main', $data);
    }
    public function faqQuestionMainEmployer($id = null, Request $request)
    {
//        $get = Faq::find($id);

//        $anchestro = $get->getAncestors()->where('id',2);

//        dd($anchestro);

        $data['faq'] = Faq::find($id);
        $data['buyer'] = Faq::find(2);
        $data['seller'] = Faq::find(3);
        $data['suggested'] = Faq::where('type', '=', Faq::EMPLOYEE_FAQ)->orderBy('created_at', 'asc')->paginate(5);
        $pagination = Faq::where('type', '=', Faq::EMPLOYEE_FAQ)->orderBy('created_at', 'asc')->get();
        $pagination = (self::paginate($pagination, 15, $request->page ?: 1));
        $data['pagination'] = $pagination;

        return view('frontend.faq.employer.faq-question-main', $data);
    }

    public function faqQuestionSub()
    {
        return view('frontend.faq.faq-question-sub');
    }

//    public function endContractEmployer()
//    {
//        return view('frontend.endContract.endContractEmployer');
//    }
//
//    public function endContractFreelancer(){
//        return view('frontend.endContract.endContractFreelancer');
//    }

    public function xenrenLandingNew()
    {
        $lang = Session::get('lang');

        //redirect the page back to original location after login
        $redirectUrl = Session::get('redirect_url');
        if ('' != $redirectUrl) {
            $previousPage = Session::get('previous_page');
            if ('login_page' == $previousPage) {
                Session::put('redirect_url', '');
                Session::put('previous_page', '');

                return Redirect::to($redirectUrl);
            }
        }

        if ('cn' == $lang || '' == $lang || !isset($lang)) {
            $language = 1;
        } else {
            $language = 2;
        }
        $newsList = News::with(['stuff'])->where('language', $language)->get();

        $skillList = Skill::getSearchSkillData(Skill::SEARCH_TYPE_WHITE);

        //check user ip address
        $ip = $_SERVER['REMOTE_ADDR'];

        if (\Auth::check()) {
            $user = \Auth::guard('users')->user();
            if (null == $user->language_preference) {
                if ('CN' == $details->city || 'TW' == $details->city || 'HK' == $details->city) {
                    $user->language_preference = User::LANGUAGE_PREFERENCE_CHINESE;
                    $user->save();
                    $language = 1;
                    Session::put('lang', 'cn');
                    app()->setLocale('cn');
                } else {
                    $user->language_preference = User::LANGUAGE_PREFERENCE_ENGLISH;
                    $user->save();
                    $language = 2;
                    Session::put('lang', 'en');
                    app()->setLocale('en');
                }
            } else {
                if (User::LANGUAGE_PREFERENCE_CHINESE == $user->language_preference) {
                    Session::put('lang', 'cn');
                    app()->setLocale('cn');
                } else {
                    Session::put('lang', 'en');
                    app()->setLocale('en');
                }
            }
        }

        $userStatus = User::getStatusLists();

        $isWechatBrowser = Session::get('is_wechat_browser');
        if (null != $isWechatBrowser && 'true' == $isWechatBrowser) {
            return view('mobileWebLanding.default', [
                'skillList' => $skillList,
            ]);
        }

        return view('home-xenren-new', [
            'newsList' => $newsList,
            'userStatus' => $userStatus,
            'language' => $language,
            'skillList' => $skillList,
        ]);
    }

    public function xenrenLandingNewCN()
    {
        $lang = Session::get('lang');

        //redirect the page back to original location after login
        $redirectUrl = Session::get('redirect_url');
        if ('' != $redirectUrl) {
            $previousPage = Session::get('previous_page');
            if ('login_page' == $previousPage) {
                Session::put('redirect_url', '');
                Session::put('previous_page', '');

                return Redirect::to($redirectUrl);
            }
        }

        if ('cn' == $lang || '' == $lang || !isset($lang)) {
            $language = 1;
        } else {
            $language = 2;
        }
        $newsList = News::with(['stuff'])->where('language', $language)->get();

        $skillList = Skill::getSearchSkillData(Skill::SEARCH_TYPE_WHITE);

        //check user ip address
        $ip = $_SERVER['REMOTE_ADDR'];

        if (\Auth::check()) {
            $user = \Auth::guard('users')->user();
            if (null == $user->language_preference) {
                if ('CN' == $details->city || 'TW' == $details->city || 'HK' == $details->city) {
                    $user->language_preference = User::LANGUAGE_PREFERENCE_CHINESE;
                    $user->save();
                    $language = 1;
                    Session::put('lang', 'cn');
                    app()->setLocale('cn');
                } else {
                    $user->language_preference = User::LANGUAGE_PREFERENCE_ENGLISH;
                    $user->save();
                    $language = 2;
                    Session::put('lang', 'en');
                    app()->setLocale('en');
                }
            } else {
                if (User::LANGUAGE_PREFERENCE_CHINESE == $user->language_preference) {
                    Session::put('lang', 'cn');
                    app()->setLocale('cn');
                } else {
                    Session::put('lang', 'en');
                    app()->setLocale('en');
                }
            }
        }

        $userStatus = User::getStatusLists();

        $isWechatBrowser = Session::get('is_wechat_browser');
        if (null != $isWechatBrowser && 'true' == $isWechatBrowser) {
            return view('mobileWebLanding.default', [
                'skillList' => $skillList,
            ]);
        }

        return view('home-xenren-new-CN', [
            'newsList' => $newsList,
            'userStatus' => $userStatus,
            'language' => $language,
            'skillList' => $skillList,
        ]);
    }

    public function instructionHiring()
    {
        return view('frontend.instructionHiring.index');
    }

    public function privacyPolicy()
    {
        return view('frontend.companyInfo.privacyPolicy');
    }

    public function salesVersion52()
    {
        return view('frontend.companyInfo.sales-Version-5-2');
    }

    public function salesVersion53()
    {
        return view('frontend.companyInfo.sales-Version-5-3');
    }

    public function salesVersion2()
    {
        return view('frontend.companyInfo.sales-Version-2');
    }

    public function termsService()
    {
        return view('frontend.companyInfo.termsService');
    }

    public function aboutUs()
    {
        return view('frontend.companyInfo.aboutUs');
    }

    public function contactUs()
    {
        return view('frontend.companyInfo.contactUs');
    }

    public function cooperatePage()
    {
        return view('frontend.cooperatePage.index');
    }

    public function sentFeedback(Request $request)
    {
        $username = $request->name;
        $email = $request->email;
        $feedbacktext = $request->text;

        $savefeedback = new UserFeedback();
        $savefeedback->username = $username;
        $savefeedback->email = $email;
        $savefeedback->feedback_text = $feedbacktext;
        $savefeedback->save();

        $a = \Mail::send('frontend.cooperatePage.sendfeedback', [
            'request' => $request->all(),
        ], function ($message) use ($email) {
            $message->to('fong@xenren.co');
            $message->from('support@xenren.co');
            $message->subject('User feedback');
        });

        return \redirect()->back()->with('Message', 'Feedback sent');
    }

    public function hiringPage()
    {
        $getPositions = HiringJobPosition::get();

        return view('frontend.companyInfo.hiringPage', [
            'positions' => $getPositions,
        ]);
    }

    public function hiringPage2()
    {
        return view('frontend.companyInfo.hiringPage2');
    }

    public function companyCulture()
    {
        return view('frontend.companyInfo.companyCulture');
    }

    public function chineseCompanyCulture()
    {
        return view('frontend.companyInfo.chineseCompanyCulture');
    }

    public function mobileWebLanding()
    {
        $skillList = Skill::getSearchSkillData(Skill::SEARCH_TYPE_WHITE);

        return view('mobileWebLanding.default', [
            'skillList' => $skillList,
        ]);
    }

    public function sendRequest(SendRequestFormRequest $request)
    {
        dd($request->all());
    }

    public function skillVerification3()
    {
        return view('frontend.skillVerification.skillVerification3');
    }

    public function oneTimeScript()
    {
//        //make all applicants automatic become related project chat follower
//        $projectApplicants = ProjectApplicant::all();
//        foreach ($projectApplicants as $projectApplicant) {
//            $projectId = $projectApplicant->project_id;
//            $userId = $projectApplicant->user_id;
//
//            //get project chat room
//            $projectChatRoom = ProjectChatRoom::where('project_id', $projectId)->first();
//            $projectChatRoomId = $projectChatRoom->id;
//
//            //check whether user for related project exist or not in chat follower list
//            $projectChatFollower = ProjectChatFollower::where('project_chat_room_id', $projectChatRoomId)
//                ->where('user_id', $userId)
//                ->first();
//
//            if (!$projectChatFollower) {
//                $model = new ProjectChatFollower();
//                $model->project_chat_room_id = $projectChatRoomId;
//                $model->user_id = $userId;
//                $model->is_kick = 0;
//                $model->save();
//            }
//
//        }

//        //make all the existing status 0(New) change to new status 1 (Apply)
//        $projectApplicants = ProjectApplicant::all();
//        foreach ($projectApplicants as $projectApplicant) {
//            if( $projectApplicant->status == ProjectApplicant::STATUS_NEW && $projectApplicant->quote_price > 0 )
//            {
//                $projectApplicant->status = ProjectApplicant::STATUS_APPLY;
//                $projectApplicant->save();
//            }
//        }
//
//        //make all project include into project applicants table when new created
//        $projects = Project::where('type', Constants::PROJECT_TYPE_COMMON)
//            ->whereIn('status', [Project:: STATUS_PUBLISHED, Project:: STATUS_SELECTED])
//            ->get();
//
//        foreach ($projects as $project) {
//            //check project already exist in project applicants table
//            $projectApplicant = ProjectApplicant::where('project_id', $project->id)
//                ->where('status', ProjectApplicant::STATUS_NEW)
//                ->where('quote_price', 0.00)
//                ->first();
//
//            //insert just create or published project
//            if (!$projectApplicant) {
//                $model = new ProjectApplicant();
//                $model->user_id = null;
//                $model->project_id =  $project->id;
//                $model->status = ProjectApplicant::STATUS_NEW;
//                $model->quote_price = 0.00;
//                $model->pay_type = ProjectApplicant::PAY_TYPE_UNKNOWN;
//                $model->save();
//            }
//        }

//        $userInboxs = UserInbox::all();
//        foreach( $userInboxs as $userInbox) {
//            if ($userInbox->type == UserInbox::TYPE_PROJECT_SUCCESSFUL_POST_JOB)
//            {
//                if ($userInbox->from_user_id == null && $userInbox->from_staff_id == null) {
//                    $userInbox->from_staff_id = 1;
//                    $userInbox->save();
//                }
//
//                if ($userInbox->category == UserInbox::CATEGORY_NORMAL)
//                {
//                    $userInbox->category = UserInbox::CATEGORY_SYSTEM;
//                    $userInbox->save();
//                }
//            }
//        }
//
//        $userInboxMessages = UserInboxMessages::all();
//        foreach( $userInboxMessages as $userInboxMessage)
//        {
//            if ($userInboxMessage->type == UserInbox::TYPE_PROJECT_SUCCESSFUL_POST_JOB) {
//                if ($userInboxMessage->from_user_id == null && $userInboxMessage->from_staff_id == null) {
//                    $userInboxMessage->from_staff_id = 1;
//                    $userInboxMessage->save();
//                }
//            }
//        }

//        //staff id in project table for type 2 (Official Project) if null set to 1
//        $projects = Project::where('type', Project::PROJECT_TYPE_OFFICIAL)
//            ->get();
//
//        foreach($projects as $project)
//        {
//            if( $project->type == Project::PROJECT_TYPE_OFFICIAL && $project->staff_id == null )
//            {
//                $project->staff_id = 1;
//                $project->save();
//            }
//        }

//        //update display order in project applicant base on status
//        $projectApplicants = ProjectApplicant::all();
//        foreach ( $projectApplicants as $projectApplicants) {
//
//            if( $projectApplicants->status == ProjectApplicant::STATUS_NEW )
//            {
//                $projectApplicants->display_order = 2;
//            }
//            else if( $projectApplicants->status == ProjectApplicant::STATUS_APPLICANT_ACCEPTED )
//            {
//                $projectApplicants->display_order = 1;
//            }
//            else if( $projectApplicants->status == ProjectApplicant::STATUS_COMPLETED )
//            {
//                $projectApplicants->display_order = 3;
//            }
//            else
//            {
//                $projectApplicants->display_order = 4;
//            }
//
//            $projectApplicants->save();
//        }

//        //pairing white and gold tag
//        $skills = Skill::where('tag_color', Skill::TAG_COLOR_WHITE)
//            ->get();
//        foreach( $skills as $skill)
//        {
//            $targetSkills = Skill::where('name_en', $skill->name_en)
//                ->get();
//
//            if( $targetSkills->count() == 2 )
//            {
//                //duplicate the white tag description and link to gold skill tag
//                foreach( $targetSkills as $targetSkill)
//                {
//                    if( $targetSkill->id != $skill->id )
//                    {
//                        //gold tag by update the value same like white tag with similar skill name
//                        $model = Skill::find($targetSkill->id);
//                        $model->name_cn = $skill->name_cn;
//                        $model->reference_link_cn = $skill->reference_link_cn;
//                        $model->description_cn = $skill->description_cn;
//                        $model->name_en = $skill->name_en;
//                        $model->reference_link_en = $skill->reference_link_en;
//                        $model->description_en = $skill->description_en;
//                        $model->tag_color = Skill::TAG_COLOR_GOLD;
//                        $model->save();
//                    }
//                }
//
//            }
//            else if( $targetSkills->count() == 1 )
//            {
//                //create gold tag skill
//                $model = new Skill();
//                $model->name_cn = $skill->name_cn;
//                $model->reference_link_cn = $skill->reference_link_cn;
//                $model->description_cn = $skill->description_cn;
//                $model->name_en = $skill->name_en;
//                $model->reference_link_en = $skill->reference_link_en;
//                $model->description_en = $skill->description_en;
//                $model->tag_color = Skill::TAG_COLOR_GOLD;
//                $model->save();
//            }
//        }
//
//
//
//        //link the similar skill keyword into group
//        $skills = Skill::where('tag_color', Skill::TAG_COLOR_WHITE)
//            ->get();
//
//        foreach( $skills as $skill)
//        {
//            $skillKeywordGroup = SkillKeywordGroup::where('skill_id', $skill->id)
//                ->first();
//            if( $skillKeywordGroup == null )
//            {
//                //skill still not exist in table
//                $similarSkills = Skill::where('name_en', $skill->name_en)
//                    ->get();
//
//                $groupId = DB::table('skill_keyword_groups')->max('group');
//                if( $groupId == null )
//                {
//                    $groupId = 1;
//                }
//                else
//                {
//                    $groupId += 1;
//                }
//
//                foreach( $similarSkills as $similarSkill)
//                {
//                    $skillKeywordGroup = new SkillKeywordGroup();
//                    $skillKeywordGroup->skill_id = $similarSkill->id;
//                    $skillKeywordGroup->group = $groupId;
//                    $skillKeywordGroup->save();
//                }
//            }
//            else
//            {
//                //only one white skill exist in table
//                $SkillKeywordGroups = SkillKeywordGroup::where('group', $skillKeywordGroup->group)
//                    ->get();
//
//                if( $SkillKeywordGroups->count() == 1 )
//                {
//                    $list = Skill::where('name_en', $skill->name_en)->get();
//
//                    foreach( $list as $item )
//                    {
//                        if( $item->id !=  $skill->id)
//                        {
//                            $skillKeywordGroup = new SkillKeywordGroup();
//                            $skillKeywordGroup->skill_id = $item->id;
//                            $skillKeywordGroup->group = $SkillKeywordGroups[0]->group;
//                            $skillKeywordGroup->save();
//                        }
//                    }
//                }
//
//            }
//        }

        //delete duplicate project applicants
        //each project only can have one status 1 (STATUS_NEW)
        $projectApplicants = ProjectApplicant::groupBy('user_id', 'project_id', 'status')->get();
//        $projectApplicants = ProjectApplicant::all();
        foreach ($projectApplicants as $projectApplicant) {
            $targetProjectApplicant = ProjectApplicant::where('user_id', null)
                ->where('project_id', $projectApplicant->project_id)
                ->where('status', ProjectApplicant::STATUS_NEW)
            ;

            if ($targetProjectApplicant->count() > 1) {
                $list = $targetProjectApplicant->get();
                foreach ($list as $key => $item) {
                    if (0 == $key) {
                        //first record remain and the less delete
                        continue;
                    }

                    $item->delete();
                }
            }
        }

        return response()->json([
            'status' => 'success',
            'result' => 'Successful Run',
        ]);
    }

    public function codeConfirmationEmail()
    {
        return view('mails.guestConfirmationCode', [
            'code' => '111-111',
            'email' => 'abc@hotmail.com',
            'link' => 'http://xenrencc.dev/codeConfirmation',
        ]);
    }

    public function testSendEmail()
    {
        //send email
        $senderEmail = 'support@xenren.co';
        $senderName = 'Support Team';

        $user = User::where('email', 'a@yahoo.com')->first();

        Mail::send('mails.acceptContactInfo', ['user' => $user], function ($message) use ($senderEmail, $senderName, $user) {
            $message->from($senderEmail, $senderName);
            $message->to($user->email, $user->first_name.' '.$user->last_name)
                ->subject(trans('common.contact_info'))
            ;
        });
    }

    public function getNewNotificationPost(Request $request)
    {
        if (\Auth::guard('users')->check()) {
            $user = \Auth::guard('users')->user();
            $userInboxMessages = UserInboxMessages::with(['fromUser', 'toUser'])
                ->where('to_user_id', $user->id)
                ->orderBy('id', 'desc')
            ;

            $userInboxMessages = $userInboxMessages->get();
            $pendingNotification = $userInboxMessages->where('is_read', 0)->count();

            $view = View::make('frontend.includes.notification', [
                'pendingNotification' => $pendingNotification,
                'userInboxMessages' => $userInboxMessages,
            ]);

            $contents = (string) $view;

            return response()->json([
                'status' => 'success',
                'result' => $contents,
            ]);
        }

        //return empty when user not login
        return response()->json([
            'status' => 'success',
            'result' => '',
        ]);
    }

    public function updateNotificationToIsRead()
    {
        $user = \Auth::guard('users')->user();
        //update notifucation to is read\
        $unread = UserInboxMessages::where('to_user_id', '=', $user->id)->where('is_read', '=', 0);
        \DB::beginTransaction();
        $unread->update(['is_read' => 1]);
        \DB::commit();
    }

    // public function getOldNotificationPost(Request $request)
    // {

    //     if (\Auth::guard('users')->check()) {

    //         $offset = $request->get('offset');
    //         $user = \Auth::guard('users')->user();

    //         //get old notification

    //          $userInboxMessages = UserInboxMessages::with(['fromUser', 'toUser'])
    //             ->where('to_user_id', $user->id)
    //             ->offset($offset)
    //             ->orderBy('id', 'desc')->get();

    //         $old_notification = [];

    //         if($userInboxMessages){
    //             foreach($userInboxMessages as $key => $userInboxMessage ){
    //                 $old_notification[] = [
    //                     'is_read' => $userInboxMessage->is_read,
    //                     'getAvatar' => $userInboxMessage->fromUser->getAvatar(),
    //                     'getCreatedDate' => $userInboxMessage->getCreatedDate(),
    //                     'getName' => $userInboxMessage->fromUser->getName(),
    //                     'getTitle' => $userInboxMessage->getTitle()
    //                 ];
    //             }
    //         }
    //         return response()->json([
    //             'status'            => 'success',
    //             'result'            => $old_notification,
    //         ]);
    //     }
    // }

    public function publishedJobMilestone()
    {
        return view('frontend.fixedPriceEscrowModule.publishedJobMilestone');
    }

    public function refundNew()
    {
        return view('frontend.fixedPriceEscrowModule.refundNew');
    }

    public function refundEdit()
    {
        return view('frontend.fixedPriceEscrowModule.refundEdit');
    }

    public function endContractEdited()
    {
        return view('frontend.fixedPriceEscrowModule.endContractEdited');
    }

    public function shareVideo()
    {
    }

    public function markAllRead()
    {
        $user = \Auth::guard('users')->user();
        $userInboxMessages = UserInboxMessages::with(['fromUser', 'toUser'])
            ->where('to_user_id', $user->id)
            ->orderBy('id', 'desc')
        ;
        $userInboxMessages = $userInboxMessages->get();
        foreach ($userInboxMessages as $userInboxMessages) {
            $userInboxMessages->where('is_read', '=', '0')->orwhere('is_read', '=', null)->update([
                'is_read' => 1,
            ]);
        }

        return back();
    }
}
