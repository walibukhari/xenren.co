<?php
//https://tuts.codingo.me/laravel-5-1-and-dropzone-js-auto-image-uploads-with-removal-links/
//https://github.com/codingo-me/dropzone-laravel-image-upload
namespace App\Http\Controllers;

use App\Logic\Image\ImageRepository;
use App\Models\PortfolioFile;
use App\Models\UserIdentity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class ImageController extends Controller
{
    protected $images;

    public function __construct(ImageRepository $imageRepository)
    {
        $this->image = $imageRepository;
    }

    public function getUpload()
    {
        return view('frontend.test.upload');
    }

    public function getUpload2()
    {
        return view('frontend.test.upload2');
    }

    public function getUpload3()
    {
        return view('frontend.test.upload3');
    }

    public function getUpload4()
    {
        return view('frontend.test.upload4');
    }

    public function postUpload()
    {
        $photo = Input::all();
        $response = $this->image->upload($photo);
        return $response;

    }

    public function deleteUpload()
    {

        $filename = Input::get('id');

        if(!$filename)
        {
            return 0;
        }

        $response = $this->image->delete( $filename );

        return $response;
    }

    public function testPage()
    {

        echo 'hi';
        // Session::put('lang', null);
//        return view('frontend.test.cropImage');
    }

    public function modalCropImage($type, $id)
    {
        if($type == "portfolio")
        {
            $portfolioFile = PortfolioFile::find($id);
            $filePath = $portfolioFile->file;
        }

        return view('frontend.includes.modalCropImage', [
            'url' => $filePath,
            'type' => $type,
            'id' => $id
        ]);
    }

    public function modalCropImagePost(Request $request)
    {
        $jpeg_quality = 90;

        $x = $request->get('x');
        $y = $request->get('y');
        $w = $request->get('w');
        $h = $request->get('h');

        $src_x = intval($x);
        $src_y = intval($y);
        $src_w = intval($w);
        $src_h = intval($h);

//        $dst_x = intval($x);
//        $dst_y = intval($y);

        $type = $request->get('type');
        $id = $request->get('id');

        if( $type == "portfolio")
        {
            $dst_w = 250;
            $dst_h = 180;

            $portfolioFile = PortfolioFile::find($id);
            $src = public_path() . '/' . $portfolioFile->file;
        }
        else
        {
            $src = public_path() . '/images/team.jpg';
        }

        $image_type = getFileType($src);
        if( $image_type == "jpeg" )
        {
            $src_image = imagecreatefromjpeg($src);
        }
        else if( $image_type == "png" )
        {
            $src_image = imagecreatefrompng($src);
        }
        else if( $image_type == "gif" )
        {
            $src_image = imagecreatefromgif($src);
        }
        else if( $image_type == "bmp" )
        {
            $src_image = imagecreatefrombmp($src);
        }

        $dst_image = ImageCreateTrueColor($dst_w, $dst_h);
        imagecopyresampled($dst_image, $src_image, 0, 0, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);

        $newFilename = generateRandomUniqueName();

        if( $type == "portfolio")
        {
            $path = 'uploads/portfolio/' . $id . '/thumbnail/';
            touchFolder($path);

            $filePath = $path . $newFilename . '.jpg';
            imagejpeg($dst_image, public_path() . '/' . $filePath, $jpeg_quality);

            $portfolioFile->thumbnail_file = $filePath;
            $portfolioFile->save();

            $thumbnailPath = asset($filePath);
        }
        else
        {
            imagejpeg($dst_image, public_path() . '/uploads/cropped/' . $newFilename . '.jpg', $jpeg_quality);
            $thumbnailPath = '';
        }

        return response()->json([
            'status'    	=> 'OK',
            'thumbnailPath' => $thumbnailPath,
            'id'            => $id
        ]);
    }

    public function uploadIdCard($key)
    {
        //for wechat autologin and redirect back
        Session::put('redirect_url', route('frontend.uploadidcard', ['key' => $key ]));

        $identity = UserIdentity::where('key', $key)
            ->first();

        return view('frontend.identityAuthentication.uploadIdCard', [
            'identity' => $identity
        ]);
    }

}