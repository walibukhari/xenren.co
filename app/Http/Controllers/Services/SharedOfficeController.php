<?php

namespace App\Http\Controllers\Services;

use App\Libraries\Pushy\PushyAPI;
use App\Models\SharedOfficeBooking;
use App\Models\SharedOfficeFee;
use App\Models\SharedOfficeLikeCount;
use App\Models\SharedOfficeProductCategory;
use App\Models\SharedOfficeProductAvailability;
use App\Models\SharedOfficeRating;
use App\Models\SharedOfficeRatingHelpfulCount;
use App\Models\SharedOfficeReserveRequest;
use App\Models\Staff;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\WorldCities;
use App\Models\WorldContinents;
use App\Models\WorldCountries;
use App\Services\GeneralService;
use App\SharedOfficeMainImage;
use App\Traits\sendPushNotification;
use Carbon\Carbon;
use function Clue\StreamFilter\fun;
use function GuzzleHttp\Psr7\str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\SharedOffice;
use App\Models\SharedOfficeProducts;
use App\Models\SharedOfficeFinance;
use App\Models\Countries;
use App\Models\SharedOfficeImages;
use App\Models\Shfacilitie;
use App\Models\SharedOfficeFinanceRevision;
use mysql_xdevapi\Collection;
use Session;
use App\Models\User;
use App\Models\CountryCities;
use App\Models\UserCheckContactLog;
use App\Models\UserIdentity;
use App\Models\UserToken;
use App\Models\UserOtherTypeBarcodeScans;
use Illuminate\Pagination\LengthAwarePaginator;
use JWTAuth;
use JWTException;
use Mail;
use Goutte\Client;
use Illuminate\Support\Facades\Cache;

class SharedOfficeController extends Controller
{
    use sendPushNotification;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */

    public function sharedofficeFilterData()
    {
        $hotDeskCount = SharedOfficeProducts::get();
        $payBy = SharedOfficeFee::get();

        return collect([
            'hourly' => $payBy->whereBetween('hourly_price', [1, 100000])->count(),
            'daily' => $payBy->whereBetween('daily_price', [1, 100000])->count(),
            'weekly' => $payBy->whereBetween('weekly_price', [1, 100000])->count(),
            'monthly' => $payBy->whereBetween('monthly_price', [1, 100000])->count(),
            'hotDeskCount' => $hotDeskCount->where('category_id', SharedOfficeProducts::CATEGORY_HOT_DESK)->unique('office_id')->count(),
            'dedicatedDeskCount' => $hotDeskCount->where('category_id', SharedOfficeProducts::CATEGORY_DEDICATED_DESK)->unique('office_id')->count(),
            'meetingRoomCount' => $hotDeskCount->where('category_id', SharedOfficeProducts::CATEGORY_MEETING_ROOM)->unique('office_id')->count()
        ]);
    }

    public  function sharedofficeFilterResults(Request $request){
        if($request->payBy && $request->payType) {
            $type = $request->payType;
            $payby = $request->payBy;
            $sharedOffice = SharedOffice::with(['officeProducts' => function($q) use($type,$payby){
                $q->where('type','=',$type);
                $q->where('category_id','=',$payby);
            }]);
        }
        if($request->priceRangeLow && $request->priceRangeHigh) {

        }

        if($request->distanceRangeLow && $request->distanceRangeHigh) {

        }
        if($request->location) {
            $sharedOffice = SharedOffice::where('location','LIKE','%'.$request->location.'%');
        }
        $sharedOffice = $sharedOffice->where('active','=',1)->paginate(10);
        return response()->json($sharedOffice);
    }

    public function getAllSharedOffices(Request $request)
    {
        if (is_null($request->ip)) {
            $sharedoffice = DB::table('shared_offices')
                ->selectRaw(
                    "
                    shared_offices.id,
                    shared_offices.lat,
                    shared_offices.active,
                    shared_offices.lng,
                    office_name,
                    shared_offices.city_id,
                    image,
                    description,
                    location,
                    version_english,
                    version_chinese,
                    office_name_cn,
                    office_description_cn,
                    shared_office_fees.hourly_price,
                    shared_office_fees.daily_price,
                    shared_office_fees.monthly_price,
                    shared_office_fees.weekly_price,
                    (3959 * acos( cos( radians('" . $request->lat . "') ) * cos( radians( lat ) )  * cos( radians( lng ) - radians('" . $request->lang . "') ) + sin( radians('" . $request->lat . "') ) * sin( radians( lat ) ) ) ) AS 'total_distance'
                    "
                )
                ->where('active', '=', 1)
                ->whereRaw("( 3959 * acos( cos( radians( " . $request->lat . " ) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians( '" . $request->lang . "' ) ) + sin( radians( '" . $request->lat . "' ) ) * sin( radians( lat ) ) ) ) < 10000")
                ->orderBy('total_distance', 'asc')
                ->leftJoin('shared_office_languages', 'shared_offices.id', '=', 'shared_office_languages.shared_offices_id')
                ->leftjoin('shared_office_fees', 'shared_offices.id', '=', 'shared_office_fees.office_id')
                ->paginate(10);

            for ($i = 0; $i < count($sharedoffice); $i++) {
                $sharedOfficeProducts = SharedOfficeProducts::where('office_id', '=', $sharedoffice[$i]->id)->get();
                $city = WorldCities::where('id', '=', $sharedoffice[$i]->city_id)->select(['id', 'name', 'country_id'])->first();
                if (!is_null($city)) {
                    $country = WorldCountries::where('id', '=', $city->country_id)->select(['id', 'name'])->first();
                } else {
                    $country = null;
                }
                $sharedoffice[$i]->avg = (float) $sharedOfficeProducts->avg('rate');
                $sharedoffice[$i]->hot_desk = $sharedOfficeProducts->where('category_id', SharedOfficeProducts::CATEGORY_HOT_DESK)->count();
                $sharedoffice[$i]->dedicated_desk = $sharedOfficeProducts->where('category_id', SharedOfficeProducts::CATEGORY_DEDICATED_DESK)->count();
                $sharedoffice[$i]->meeting_room = $sharedOfficeProducts->where('category_id', SharedOfficeProducts::CATEGORY_MEETING_ROOM)->count();
                $sharedoffice[$i]->min_price = $sharedOfficeProducts->min('time_price');
                $sharedoffice[$i]->max_price = $sharedOfficeProducts->max('time_price');
                $sharedoffice[$i]->city = $city->name;
                $sharedoffice[$i]->country = is_null($country) ? $country : $country->name;
                $sharedoffice[$i]->image = 'https://www.xenren.co/'.$sharedoffice[$i]->image;
            }
            return response()->json($sharedoffice);
        } else {
            $x = geoip($request->ip);
            $sharedoffice = DB::table('shared_offices')
                ->selectRaw("
                shared_offices.id,
                shared_offices.lat,
                shared_offices.active,
                shared_offices.lng,
                office_name,
                shared_offices.city_id,
                image,
                description,
                location,
                version_english,
                version_chinese,
                office_name_cn,
                office_description_cn,
                shared_office_fees.hourly_price,
                shared_office_fees.daily_price,
                shared_office_fees.monthly_price,
                shared_office_fees.weekly_price,
                world_cities.name AS city_name,
                world_countries.name AS country_name,
                (3959 * acos( cos( radians('" . $x->lat . "') ) * cos( radians( lat ) )  * cos( radians( lng ) - radians('" . $x->lon . "') ) + sin( radians('" . $x->lat . "') ) * sin( radians( lat ) ) ) ) AS 'total_distance'")
                ->where('active', '=', 1)
                ->whereRaw("( 3959 * acos( cos( radians( " . $x->lat . " ) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians( '" . $x->lon . "' ) ) + sin( radians( '" . $x->lat . "' ) ) * sin( radians( lat ) ) ) ) < 10000")
                ->orderBy('total_distance', 'asc')
                ->leftJoin('shared_office_languages', 'shared_offices.id', '=', 'shared_office_languages.shared_offices_id')
                ->rightJoin('world_cities', 'shared_offices.city_id', '=', 'world_cities.id')
                ->rightJoin('world_countries', 'shared_offices.state_id', '=', 'world_countries.id')
                ->join('shared_office_fees', 'shared_offices.id', '=', 'shared_office_fees.office_id')
                ->paginate(10);
            for ($i = 0; $i < count($sharedoffice); $i++) {
                $sharedOfficeProducts = SharedOfficeProducts::where('office_id', '=', $sharedoffice[$i]->id)->get();
                $city = WorldCities::where('id', '=', $sharedoffice[$i]->city_id)->select(['id', 'name', 'country_id'])->first();
                if (!is_null($city)) {
                    $country = WorldCountries::where('id', '=', $city->country_id)->select(['id', 'name'])->first();
                } else {
                    $country = null;
                }
                $sharedoffice[$i]->avg = (float) $sharedOfficeProducts->avg('rate');
                $sharedoffice[$i]->hot_desk = $sharedOfficeProducts->where('category_id', SharedOfficeProducts::CATEGORY_HOT_DESK)->count();
                $sharedoffice[$i]->dedicated_desk = $sharedOfficeProducts->where('category_id', SharedOfficeProducts::CATEGORY_DEDICATED_DESK)->count();
                $sharedoffice[$i]->meeting_room = $sharedOfficeProducts->where('category_id', SharedOfficeProducts::CATEGORY_MEETING_ROOM)->count();
                $sharedoffice[$i]->min_price = $sharedOfficeProducts->min('time_price');
                $sharedoffice[$i]->max_price = $sharedOfficeProducts->max('time_price');
                $sharedoffice[$i]->city = $city->name;
                $sharedoffice[$i]->country = is_null($country) ? $country : $country->name;
                $sharedoffice[$i]->image = 'https://www.xenren.co/'.$sharedoffice[$i]->image;
            }
            return response()->json($sharedoffice);
        }
    }

    /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::                                                                         :*/
    /*::  This routine calculates the distance between two points (given the     :*/
    /*::  latitude/longitude of those points). It is being used to calculate     :*/
    /*::  the distance between two locations using GeoDataSource(TM) Products    :*/
    /*::                                                                         :*/
    /*::  Definitions:                                                           :*/
    /*::    South latitudes are negative, east longitudes are positive           :*/
    /*::                                                                         :*/
    /*::  Passed to function:                                                    :*/
    /*::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :*/
    /*::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :*/
    /*::    unit = the unit you desire for results                               :*/
    /*::           where: 'M' is statute miles (default)                         :*/
    /*::                  'K' is kilometers                                      :*/
    /*::                  'N' is nautical miles                                  :*/
    /*::  Worldwide cities and other features databases with latitude longitude  :*/
    /*::  are available at https://www.geodatasource.com                          :*/
    /*::                                                                         :*/
    /*::  For enquiries, please contact sales@geodatasource.com                  :*/
    /*::                                                                         :*/
    /*::  Official Web site: https://www.geodatasource.com                        :*/
    /*::                                                                         :*/
    /*::         GeoDataSource.com (C) All Rights Reserved 2018                  :*/
    /*::                                                                         :*/
    /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    function distance($lat1, $lon1, $lat2, $lon2, $unit) {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        }
        else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }

    public function index(Request $request){
        if ($request->mobileApp === 'true') {
            return self::mobileIndex($request);
        }

        $key = 'sharedoffice:'.$request->ip();
        $response = Cache::remember($key, 1800, function () use ($request) {
            $sharedOffices = (json_decode(file_get_contents(base_path('public/jsons/homepage.json')), true));

            foreach ($sharedOffices as $k => $v) {
                $lat = '';
                $lng = '';
                if (!is_null($request->ip)) {
                    $x = geoip($request->ip);
                    $lng = $x->lon;
                    $lat = $x->lat;
                } else {
                    $lat = $request->lat;
                    $lng = $request->lang;
                }
                $distance = self::distance($v['lat'], $v['lng'], $lat, $lng, 'M');
                $sharedOffices[$k]['distance'] = $distance;
            }

            $sharedOffices = collect($sharedOffices)->sortBy('distance')->values();

            if ($request->text) {
                $arr = collect();
                foreach ($sharedOffices as $k => $v) {
                    if (strpos($v['office_name'], $request->text) !== false || strpos($v['city']['name'], $request->text) !== false) {
                        $arr->push($v);
                    }
                }
                $sharedOffices = $arr;
            }

            if ($request->location) {
                $continents = array('Asia', 'Europe', 'Europe', 'Africa', 'Oceania', 'Antarctica', 'North America', 'South America');
                $check = (in_array(str_replace('-', ' ', $request->location), $continents));
                if ($check == true) {
                    $continentName = str_replace('-', ' ', $request->location);
                    $continent = WorldContinents::where('name', '=', $continentName)->first();
                    $arr = collect();
                    foreach ($sharedOffices as $k => $v) {
                        if ($v['country']['continent_id'] == $continent->id) {
                            $arr->push($v);
                        }
                    }
                    $sharedOffices = $arr;
                } else {
                    $location = str_replace(array(','), ' ', $request->location);
                    $getCityCountry = explode(' ', $location);
                    if (count($getCityCountry) > 1) {
                        $city = $getCityCountry[1];
                        $countryName = $getCityCountry[0];
                        $country = str_replace('-', ' ', $countryName);
                        $cityName = str_replace('-', ' ', $city);
                        if ($cityName != '') {
                            $city = WorldCities::where('name', 'like', '%' . strtolower($cityName) . '%')->with('country')->first();
                            // $sharedoffice->where('city_id', 'like', '%' . $city->id . '%');
                            if (!is_null($city)) {
                                $arr = collect();
                                foreach ($sharedOffices as $k => $v) {
                                    if ($v['city']['id'] == $city->id) {
                                        $arr->push($v);
                                    }
                                }
                                $sharedOffices = $arr;
                            }
                        }
                        if ($countryName != '') {
                            $country = WorldCountries::where('name', 'like', '%' . strtolower($country) . '%')->first();
                            if (!is_null($country)) {
                                $arr = collect();
                                foreach ($sharedOffices as $k => $v) {
                                    if ($v['country']['id'] == $country->id) {
                                        $arr->push($v);
                                    }
                                }
                                $sharedOffices = $arr;
                            }
                        }
                    } else {
                        $country = str_replace('-', ' ', $request->location);
                        $country = WorldCountries::where('name', 'like', '%' . $country . '%')->first();
                        if (!is_null($country)) {
                            $arr = collect();
                            foreach ($sharedOffices as $k => $v) {
                                if ($v['country']['id'] == $country->id) {
                                    $arr->push($v);
                                }
                            }
                            $sharedOffices = $arr;
                        }
                    }
                }
            }

            if ($request->filterType && $request->filterType != '' && $request->filterType != 'all-spaces') {
                $id = self::getFilterType($request->filterType);

                $arr = collect();
                foreach ($sharedOffices as $k => $v) {
                    $hasCategory = false;
                    foreach ($v['products'] as $k1 => $v1) {
                        if ((int)$v1['category_id'] == $id) {
                            $hasCategory = true;
                        }
                    }
                    if ($hasCategory) {
                        $arr->push($v);
                    }
                }
                $sharedOffices = $arr;
            }

            $sharedOffices = $sharedOffices->take(50);

            return $sharedOffices;
        });

        return response()->json($response);
    }

    public function mobileIndex(Request $request)
    {
        $language='';
        if(request('language')&&request('language')=='en'){
            $language = 'version_english';
        }elseif(request('language')&&request('language')=='cn'){
            $language = 'version_chinese';
        }
        if(isset($request->map) && $request->map == 'true') {
            $map = file_get_contents(public_path('jsons/sharedOffices.json'));
            return collect([
                'current_page' => 1,
                'data' => json_decode($map)
            ]);
        }
        if (is_null($request->ip)) {
            if($request->lat && $request->lat != '' && $request->lang && $request->lang != ''){
                $sharedoffice = DB::table('shared_offices')
                    ->selectRaw("
                        DISTINCT(shared_offices.id),
                        shared_offices.id,
                        shared_offices.active,
                        shared_offices.lat,
                        shared_offices.lng,
                        office_name,
                        image,
                        deleted_at,
                        description,
                        location,
                        version_english,
                        version_chinese,
                        office_name_cn,
                        office_description_cn,
                        world_cities.name AS city_name,
                        world_countries.name AS country_name,
                        (3959 * acos( cos( radians('" . $request->lat . "') ) * cos( radians( lat ) )  * cos( radians( lng ) - radians('" . $request->lang . "') ) + sin( radians('" . $request->lat . "') ) * sin( radians( lat ) ) ) ) AS 'total_distance'
                        ")
                    ->where('active', '=', 1)
                    ->where('image','!=','')
                    ->where('deleted_at','=',null)
                    ->whereRaw("( 3959 * acos( cos( radians( " . $request->lat . " ) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians( '" . $request->lang . "' ) ) + sin( radians( '" . $request->lat . "' ) ) * sin( radians( lat ) ) ) ) < 10000")
                    ->orderBy('total_distance', 'asc')
                    ->leftJoin('shared_office_languages', 'shared_offices.id', '=', 'shared_office_languages.shared_offices_id')
                    ->leftJoin('world_cities', 'shared_offices.city_id', '=', 'world_cities.id')
                    ->leftJoin('world_countries', 'shared_offices.state_id', '=', 'world_countries.id')
                    ->leftJoin('shared_office_fees', 'shared_offices.id', '=', 'shared_office_fees.office_id');
                // ->where('active', '=', 1)

                if($request->search) {
                    $sharedoffice->where('office_name', 'like', '%'.$request->search.'%');
                }

                $sharedoffice = $sharedoffice->paginate(10);

                for ($i = 0; $i < count($sharedoffice); $i++) {
                    $sharedoffice[$i]->avg = (float) SharedOfficeRating::where('office_id', '=', $sharedoffice[$i]->id)->avg('rate');
                    $sharedoffice[$i]->shared_office_rating = SharedOfficeRating::where('office_id', '=', $sharedoffice[$i]->id)->get();
                    $sharedoffice[$i]->shfacilities = DB::table('shared_office_shfacilitie')->where('shared_office_id','=',$sharedoffice[$i]->id)->get();
                }

            }
            if(!$request->lat || !$request->lang){
                DB::enableQueryLog();
                $sharedoffice = DB::table('shared_offices')
                    ->selectRaw("
                        DISTINCT(shared_offices.id),
                        shared_offices.id,
                        shared_offices.active,
                        shared_offices.lat,
                        shared_offices.lng,
                        office_name,
                        image,
                        deleted_at,
                        description,
                        location,
                        version_english,
                        version_chinese,
                        office_name_cn,
                        office_description_cn,
                        world_cities.name AS city_name,
                        world_countries.name AS country_name,
                        shared_office_fees.*
                        ")
                    ->where('active', '=', 1)
                    ->where('image','!=','')
                    ->where('deleted_at','=',null)
                    ->leftJoin('shared_office_languages', 'shared_offices.id', '=', 'shared_office_languages.shared_offices_id')
                    ->leftJoin('world_cities', 'shared_offices.city_id', '=', 'world_cities.id')
                    ->leftJoin('world_countries', 'shared_offices.state_id', '=', 'world_countries.id')
                    ->leftJoin('shared_office_fees', 'shared_offices.id', '=', 'shared_office_fees.office_id');

                if($request->search) {
                    $sharedoffice->where('office_name', 'like', '%'.$request->search.'%');
                }

                // ->where('active', '=', 1)
                $sharedoffice = $sharedoffice->paginate(10);


                for ($i = 0; $i < count($sharedoffice); $i++) {
                    $sharedoffice[$i]->avg = (float) SharedOfficeRating::where('office_id', '=', $sharedoffice[$i]->id)->avg('rate');
                    $sharedoffice[$i]->shared_office_rating = SharedOfficeRating::where('office_id', '=', $sharedoffice[$i]->id)->get();
                    $sharedoffice[$i]->shfacilities = DB::table('shared_office_shfacilitie')->where('shared_office_id','=',$sharedoffice[$i]->id)->get();
                }

            }

        } else {
            $x = geoip($request->ip);
            $sharedoffice = DB::table('shared_offices')
                ->selectRaw("
                        DISTINCT(shared_offices.id),
                        shared_offices.id,
                        shared_offices.active,
                        shared_offices.lat,
                        shared_offices.lng,
                        office_name,
                        image,
                        deleted_at,
                        description,
                        location,
                        version_english,
                        version_chinese,
                        office_name_cn,
                        office_description_cn,
                        world_cities.name AS city_name,
                        world_countries.name AS country_name,

                        shared_office_fees.type,
                        shared_office_fees.office_id,
                        shared_office_fees.no_of_peoples,
                        shared_office_fees.hourly_price,
                        shared_office_fees.daily_price,
                        shared_office_fees.weekly_price,
                        shared_office_fees.monthly_price,

                        (3959 * acos( cos( radians('" . $request->lat . "') ) * cos( radians( lat ) )  * cos( radians( lng ) - radians('" . $request->lang . "') ) + sin( radians('" . $request->lat . "') ) * sin( radians( lat ) ) ) ) AS 'total_distance'
                        ")
                ->where('active', '=', 1)
                ->where('image','!=','')
                ->where('deleted_at','=',null)
                ->whereRaw("( 3959 * acos( cos( radians( " . $x->lat . " ) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians( '" . $x->lon . "' ) ) + sin( radians( '" . $x->lat . "' ) ) * sin( radians( lat ) ) ) ) < 10000")
                ->orderBy('total_distance', 'asc')
                ->leftJoin('shared_office_languages', 'shared_offices.id', '=', 'shared_office_languages.shared_offices_id')
                ->leftJoin('world_cities', 'shared_offices.city_id', '=', 'world_cities.id')
                ->leftJoin('world_countries', 'shared_offices.state_id', '=', 'world_countries.id')
                ->leftJoin('shared_office_fees', 'shared_offices.id', '=', 'shared_office_fees.office_id')
//                ->where('type', SharedOfficeFee::HOT_DESK_TYPE)
//                ->orWhere('type', SharedOfficeFee::DEDICATED_DESK_TYPE)
//                ->orWhere('type', SharedOfficeFee::OFFICE_DESK_TYPE)
                ;

            if($request->search) {
                $sharedoffice->where('office_name', 'like', '%'.$request->search.'%');
            }

            // ->where('active', '=', 1)
            $sharedoffice = $sharedoffice->paginate(10);

            for ($i = 0; $i < count($sharedoffice); $i++) {
                $sharedoffice[$i]->avg = (float) SharedOfficeRating::where('office_id', '=', $sharedoffice[$i]->id)->avg('rate');
                $sharedoffice[$i]->shared_office_rating = SharedOfficeRating::where('office_id', '=', $sharedoffice[$i]->id)->get();
                $sharedoffice[$i]->shfacilities = DB::table('shared_office_shfacilitie')->where('shared_office_id','=',$sharedoffice[$i]->id)->get();
            }
        }
        if($language && $language!=''){
            $sharedoffice->whereIn($language,array('true','1'));
        }

        return response()->json($sharedoffice);
    }

    public function paginationIndex(Request $request){
        if ($request->mobileApp === 'true') {
            return self::mobileIndex($request);
        }

        $key = 'sharedoffice_1' . http_build_query($request->all());
        $response = Cache::remember($key, 1800, function () use ($request) {
            $listContinent = array('Asia', 'Europe', 'Europe', 'Africa', 'Oceania', 'Antarctica', 'North America', 'South America');
            $sharedOffices = collect(json_decode(file_get_contents(base_path('public/jsons/homepage.json')), true));

            $perPage = is_null($request->per_page) ? 10 : $request->per_page;
            $page = is_null($request->page) ? 0 : $request->page;

            $spaceType = $request->space_type;
            $continent = $request->continent;
            $rating = $request->rating;
            $maxPrice = $request->max_price;
            $minPrice = $request->min_price;

            if ($request->location) {
                $check = (in_array(str_replace('-', ' ', $request->location), $listContinent));
                if ($check == true) {
                    $continentName = str_replace('-', ' ', $request->location);
                    $continentData = WorldContinents::where('name', '=', $continentName)->first();
                    $sharedOffices = $sharedOffices->filter(function($value, $key) use ($continentData){
                        if ($value['country']['continent_id'] == $continentData->id) {
                            return $value;
                        }
                    });
                } else {
                    $location = str_replace(array(','), ' ', $request->location);
                    $getCityCountry = explode(' ', $location);
                    if (count($getCityCountry) > 1) {
                        $city = $getCityCountry[1];
                        $countryName = $getCityCountry[0];
                        $country = str_replace('-', ' ', $countryName);
                        $cityName = str_replace('-', ' ', $city);
                        if ($cityName != '') {
                            $city = WorldCities::where('name', 'like', '%' . mb_strtolower($cityName, 'UTF-8') . '%')->with('country')->first();
                            // $sharedoffice->where('city_id', 'like', '%' . $city->id . '%');
                            if (!is_null($city)) {
                                $sharedOffices = $sharedOffices->filter(function($value, $key) use ($city){
                                    if ($value['city']['id'] == $city->id) {
                                        return $value;
                                    }
                                });
                            }
                        }
                        if ($countryName != '') {
                            $country = WorldCountries::where('name', 'like', '%' . strtolower($country) . '%')->first();
                            if (!is_null($country)) {
                                $sharedOffices = $sharedOffices->filter(function($value, $key) use ($country){
                                    $d = $value['location'];
                                    if(strpos($d, $country->name) !== false) {
//                                    if ($value['country']['id'] == $country->id) {
                                        return $value;
//                                    }
                                    }
                                });
                            }
                        }
                    } else {
                        $country = str_replace('-', ' ', $request->location);
                        $country = WorldCountries::where('name', 'like', '%' . $country . '%')->first();
                        if (!is_null($country)) {
                            $sharedOffices = $sharedOffices->filter(function($value, $key) use ($country){
                                $d = $value['location'];
                                if(strpos($d, $country->name) !== false) {
//                                    if ($value['country']['id'] == $country->id) {
                                        return $value;
//                                    }
                                }
                            });
                        }
                    }
                }
            }

            // filter start
            $listId = [
                SharedOfficeProducts::CATEGORY_HOT_DESK,
                SharedOfficeProducts::CATEGORY_DEDICATED_DESK,
                SharedOfficeProducts::CATEGORY_MEETING_ROOM
            ];
            if (!is_null($spaceType) && !empty($spaceType)) {
                foreach ($spaceType as $type) {
                    if (in_array($type, $listId)) {
                        $sharedOffices = $sharedOffices->filter(function($value, $key) use ($type){
                            $hasCategory = false;
                            foreach ($value['products'] as $keyProduct => $valueProduct) {
                                if ((int)$valueProduct['category_id'] == $type) {
                                    $hasCategory = true;
                                }
                            }

                            if ($hasCategory) {
                                return $value;
                            }
                        });
                    }
                }
            }

            if (!is_null($continent) && !empty($continent)) {
                // in_array($continent, $listContinent)
                $check = true;
                foreach ($continent as $valueContinent) {
                    $continentData = WorldContinents::find($valueContinent);
                    if (is_null($continentData)) {
                        $check = false;
                        break;
                    }
                }

                if ($check) {
                    $sharedOffices = $sharedOffices->filter(function($value, $key) use ($continent){
                        if (in_array($value['country']['continent_id'], $continent)) {
                            return $value;
                        }
                    });
                }
            }

            if (!is_null($rating)) {
                $sharedOffices = $sharedOffices->filter(function($value, $key) use ($rating){
                    if ($value['rating'] >= $rating) {
                        return $value;
                    }
                });
            }

            if (!is_null($maxPrice) && $maxPrice > 0) {
                $sharedOffices = $sharedOffices->filter(function($value, $key) use ($maxPrice){
                    if (!is_null($value['max_seat_price'])) {
                        if ($value['max_seat_price'] <= $maxPrice) {
                            return $value;
                        }
                    } else {
                        if (!is_null($value['meeting_room_price']) && $value['meeting_room_price'] <= $maxPrice) {
                            return $value;
                        }
                    }
                });
            }

            if (!is_null($minPrice) && $minPrice > 0) {
                $sharedOffices = $sharedOffices->filter(function($value, $key) use ($minPrice){
                    if (!is_null($value['min_seat_price'])) {
                        if ($value['min_seat_price'] >= $minPrice) {
                            return $value;
                        }
                    } else {
                        if (!is_null($value['meeting_room_price']) && $value['meeting_room_price'] >= $minPrice) {
                            return $value;
                        }
                    }
                });
            }

            $countSharedOffices = $sharedOffices->count();
            $splicedSharedOffices = $sharedOffices->splice($page * $perPage, $perPage);

            return [
                'data' => $splicedSharedOffices,
                'total_data' => $countSharedOffices
            ];
        });

        return response()->json($response);
    }

    public function xindex(Request $request)
    {
        try {
            $language='';
            if(request('language')&&request('language')=='en'){
                $language = 'version_english';
            }elseif(request('language')&&request('language')=='cn'){
                $language = 'version_chinese';
            }
            if ($request->mobileApp === 'true') {
                if(isset($request->map) && $request->map == 'true') {
                    $map = file_get_contents(public_path('jsons/sharedOffices.json'));
                    return collect([
                        'current_page' => 1,
                        'data' => json_decode($map)
                    ]);
                }
                if (is_null($request->ip)) {
                    if($request->lat && $request->lat != '' && $request->lang && $request->lang != ''){
                        $sharedoffice = DB::table('shared_offices')
                        ->selectRaw("
                        DISTINCT(shared_offices.id),
                        shared_offices.id,
                        shared_offices.active,
                        shared_offices.lat,
                        shared_offices.lng,
                        office_name,
                        image,
                        description,
                        location,
                        version_english,
                        version_chinese,
                        office_name_cn,
                        office_description_cn,
                        world_cities.name AS city_name,
                        world_countries.name AS country_name,
                        (3959 * acos( cos( radians('" . $request->lat . "') ) * cos( radians( lat ) )  * cos( radians( lng ) - radians('" . $request->lang . "') ) + sin( radians('" . $request->lat . "') ) * sin( radians( lat ) ) ) ) AS 'total_distance'
                        ")
                        ->whereRaw("( 3959 * acos( cos( radians( " . $request->lat . " ) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians( '" . $request->lang . "' ) ) + sin( radians( '" . $request->lat . "' ) ) * sin( radians( lat ) ) ) ) < 10000")
                        ->orderBy('total_distance', 'asc')
                        ->where('active', '=', 1)
                        ->leftJoin('shared_office_languages', 'shared_offices.id', '=', 'shared_office_languages.shared_offices_id')
                        ->leftJoin('world_cities', 'shared_offices.city_id', '=', 'world_cities.id')
                        ->leftJoin('world_countries', 'shared_offices.state_id', '=', 'world_countries.id')
                        ->leftJoin('shared_office_fees', 'shared_offices.id', '=', 'shared_office_fees.office_id');
                        // ->where('active', '=', 1)

                        if($request->search) {
                            $sharedoffice->where('office_name', 'like', '%'.$request->search.'%');
                        }

                        $sharedoffice = $sharedoffice->paginate(10);

                    for ($i = 0; $i < count($sharedoffice); $i++) {
                        $sharedoffice[$i]->avg = (float) SharedOfficeRating::where('office_id', '=', $sharedoffice[$i]->id)->avg('rate');
                        $sharedoffice[$i]->shared_office_rating = SharedOfficeRating::where('office_id', '=', $sharedoffice[$i]->id)->get();
                        $sharedoffice[$i]->shfacilities = DB::table('shared_office_shfacilitie')->where('shared_office_id','=',$sharedoffice[$i]->id)->get();
                        }

                    }
                    if(!$request->lat || !$request->lang){
                        DB::enableQueryLog();
                        $sharedoffice = DB::table('shared_offices')
                        ->selectRaw("
                        DISTINCT(shared_offices.id),
                        shared_offices.id,
                        shared_offices.active,
                        shared_offices.lat,
                        shared_offices.lng,
                        office_name,
                        image,
                        description,
                        location,
                        version_english,
                        version_chinese,
                        office_name_cn,
                        office_description_cn,
                        world_cities.name AS city_name,
                        world_countries.name AS country_name,
                        shared_office_fees.*
                        ")
                        ->where('active', '=', 1)
                        ->leftJoin('shared_office_languages', 'shared_offices.id', '=', 'shared_office_languages.shared_offices_id')
                        ->leftJoin('world_cities', 'shared_offices.city_id', '=', 'world_cities.id')
                        ->leftJoin('world_countries', 'shared_offices.state_id', '=', 'world_countries.id')
                        ->leftJoin('shared_office_fees', 'shared_offices.id', '=', 'shared_office_fees.office_id');

                        if($request->search) {
                            $sharedoffice->where('office_name', 'like', '%'.$request->search.'%');
                        }

                        // ->where('active', '=', 1)
                        $sharedoffice = $sharedoffice->paginate(10);


                    for ($i = 0; $i < count($sharedoffice); $i++) {
                        $sharedoffice[$i]->avg = (float) SharedOfficeRating::where('office_id', '=', $sharedoffice[$i]->id)->avg('rate');
                        $sharedoffice[$i]->shared_office_rating = SharedOfficeRating::where('office_id', '=', $sharedoffice[$i]->id)->get();
                        $sharedoffice[$i]->shfacilities = DB::table('shared_office_shfacilitie')->where('shared_office_id','=',$sharedoffice[$i]->id)->get();
                        }

                    }

                } else {
                    $x = geoip($request->ip);
                    $sharedoffice = DB::table('shared_offices')
                        ->selectRaw("
                        DISTINCT(shared_offices.id),
                        shared_offices.id,
                        shared_offices.active,
                        shared_offices.lat,
                        shared_offices.lng,
                        office_name,
                        image,
                        description,
                        location,
                        version_english,
                        version_chinese,
                        office_name_cn,
                        office_description_cn,
                        world_cities.name AS city_name,
                        world_countries.name AS country_name,

                        shared_office_fees.type,
                        shared_office_fees.office_id,
                        shared_office_fees.no_of_peoples,
                        shared_office_fees.hourly_price,
                        shared_office_fees.daily_price,
                        shared_office_fees.weekly_price,
                        shared_office_fees.monthly_price,

                        (3959 * acos( cos( radians('" . $request->lat . "') ) * cos( radians( lat ) )  * cos( radians( lng ) - radians('" . $request->lang . "') ) + sin( radians('" . $request->lat . "') ) * sin( radians( lat ) ) ) ) AS 'total_distance'
                        ")
                        ->whereRaw("( 3959 * acos( cos( radians( " . $x->lat . " ) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians( '" . $x->lon . "' ) ) + sin( radians( '" . $x->lat . "' ) ) * sin( radians( lat ) ) ) ) < 10000")
                        ->orderBy('total_distance', 'asc')
                        ->where('active', '=', 1)
                        ->leftJoin('shared_office_languages', 'shared_offices.id', '=', 'shared_office_languages.shared_offices_id')
                        ->leftJoin('world_cities', 'shared_offices.city_id', '=', 'world_cities.id')
                        ->leftJoin('world_countries', 'shared_offices.state_id', '=', 'world_countries.id')
                        ->leftJoin('shared_office_fees', 'shared_offices.id', '=', 'shared_office_fees.office_id')->where('type', SharedOfficeFee::HOT_DESK_TYPE)
                        ->where('active', '=', 1);

                        if($request->search) {
                            $sharedoffice->where('office_name', 'like', '%'.$request->search.'%');
                        }

                        // ->where('active', '=', 1)
                        $sharedoffice = $sharedoffice->paginate(10);

                    for ($i = 0; $i < count($sharedoffice); $i++) {
                        $sharedoffice[$i]->avg = (float) SharedOfficeRating::where('office_id', '=', $sharedoffice[$i]->id)->avg('rate');
                        $sharedoffice[$i]->shared_office_rating = SharedOfficeRating::where('office_id', '=', $sharedoffice[$i]->id)->get();
                        $sharedoffice[$i]->shfacilities = DB::table('shared_office_shfacilitie')->where('shared_office_id','=',$sharedoffice[$i]->id)->get();
                    }
                }
                if($language && $language!=''){
                    $sharedoffice->whereIn($language,array('true','1'));
                }

//                $arr = collect();
//                foreach($sharedoffice as $k => $v){
//                    if(count($arr) < 1) {
//                        $arr->push($v);
//                    } else {
//                        $exist= $arr->where('office_id', '=', $v->office_id)->all();
//                        if(count($exist) <1) {
//                            $arr->push($v);
//                        }
//                    }
//                }
//                $arr = $arr->toArray();
//                return collect(($sharedoffice));
//                dd(count($arr));
//                $currentPage = LengthAwarePaginator::resolveCurrentPage();
//                $perPage = 11;
//                $currentItems = array_slice($arr, $perPage * ($currentPage - 1), $perPage);
//                $paginator = new LengthAwarePaginator($currentItems, count($arr), $perPage, $currentPage);
//                $paginator = collect($paginator);
//                $paginator['first_page_url'] = 'https://www.xenren.co/api/sharedoffice'.$paginator['first_page_url'];
//                $paginator['last_page_url'] = 'https://www.xenren.co/api/sharedoffice'.$paginator['last_page_url'];
//                if($paginator['next_page_url'] != null){
//                        $paginator['next_page_url'] = 'https://www.xenren.co/api/sharedoffice'.$paginator['next_page_url'];
//                    }
//                if($paginator['prev_page_url']!= null){
//                        $paginator['prev_page_url'] = 'https://www.xenren.co/api/sharedoffice'.$paginator['prev_page_url'];
//                    }
//                $paginator['path'] = 'https://www.xenren.co/api/sharedoffice';

                return response()->json($sharedoffice);
            } else {
                $sharedoffice = SharedOffice::with('totalOfficeMember', 'city','city.country', 'country', 'sharedOfficeRating', 'SharedOfficeLanguageData', 'sharedOfficeLikeCount', 'shfacilities','officeImages');
                if (is_null($request->ip)) {
                    $sharedoffice->selectRaw("shared_offices.id,  city_id , state_id ,office_space,office_size_x,office_size_y,office_space,   contact_phone,   monday_opening_time,monday_closing_time,saturday_opening_time,saturday_closing_time,sunday_opening_time,sunday_closing_time,               continent_id,shared_offices.lat,shared_offices.lng,office_name,image,description,location,version_english,version_chinese,created_at,updated_at, (3959 * acos( cos( radians('" . $request->lat . "') ) * cos( radians( lat ) )  * cos( radians( lng ) - radians('" . $request->lang . "') ) + sin( radians('" . $request->lat . "') ) * sin( radians( lat ) ) ) ) AS 'total_distance'");
                } else {
                    $x = geoip($request->ip);
                    $sharedoffice->selectRaw("shared_offices.id, office_space,office_space, office_size_x,office_size_y,contact_phone,monday_opening_time,monday_closing_time,saturday_opening_time,saturday_closing_time,sunday_opening_time,sunday_closing_time   city_id , state_id                   continent_id,shared_offices.lat,shared_offices.lng,office_name,image,description,location,version_english,version_chinese,office_name_cn,office_description_cn,created_at,updated_at, (3959 * acos( cos( radians('" . $x->lat . "') ) * cos( radians( lat ) )  * cos( radians( lng ) - radians('" . $x->lon . "') ) + sin( radians('" . $x->lat . "') ) * sin( radians( lat ) ) ) ) AS 'total_distance'");
                }

                $sharedoffice->orderByRaw("total_distance asc");

                if ($request->sort) $sharedoffice->orderBy('id', $request->sort);

                if ($request->text) {
                    $sharedoffice->where('office_name', 'like', '%' . $request->text . '%')->orwhere('location', 'like', '%' . $request->text . '%');
                }

                if($request->location) {
                    $continents = array('Asia','Europe','Europe','Africa','Oceania','Antarctica','North America','South America');
                    $check = (in_array(str_replace('-',' ',$request->location), $continents));
                    if ( $check == true ){
                        $continentName = str_replace('-',' ',$request->location);
                        $continent = WorldContinents::where('name', '=',$continentName)->first();
                        $sharedoffice->where('continent_id', '=', $continent->id);
                    }else{
                        $location = str_replace(array(','),' ',$request->location);
                        $getCityCountry = explode(' ', $location);
                        if(count($getCityCountry) > 1) {
                            $city = $getCityCountry[1];
                            $countryName = $getCityCountry[0];
                            $country = str_replace('-', ' ', $countryName);
                            $cityName = str_replace('-', ' ', $city);
                            if ($cityName != '') {
                                $city = WorldCities::where('name', 'like', '%' . $cityName . '%')->with('country')->first();
                                $sharedoffice->where('city_id', 'like', '%' . $city->id . '%');
                            }
                            if ($countryName != '') {
                                $country = WorldCountries::where('name', 'like', '%' . $country . '%')->first();
                                $sharedoffice->where('state_id', '=', $country->id);
                            }
                        }
                        else
                        {
                            $country = str_replace('-', ' ', $request->location);
                            $country = WorldCountries::where('name', 'like', '%'.$country.'%')->first();
                            $sharedoffice->where('state_id','=',$country->id);
                        }
                    }
                }

                if($request->filterType && $request->filterType != '' && $request->filterType != 'all-spaces' ){
                    $id = self::getFilterType($request->filterType) ;
                    $sharedoffice->whereHas('product', function ($query) use ($id) {
                        $query->where('category_id', '=', $id);
                    });
                }

                if($language && $language!=''){
                    $sharedoffice->whereIn($language,array('true','1'));
                }
                if ($request->limit) $sharedoffice->limit($request->limit);

                $sharedoffice->where('active', '=', 1);
                $sharedoffice->where('image','!=' , '');
                $sharedoffice =  $sharedoffice->get();
                return response()->json($sharedoffice);
            }
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage() . ' at ' . $e->getLine()
            ]);
        }
    }


    public function getFilterType($type){
        if ($type == 'hot-desk'){
            $id = SharedOfficeProducts::CATEGORY_HOT_DESK;
        }elseif ($type == 'dedicated-desk'){
            $id = SharedOfficeProducts::CATEGORY_DEDICATED_DESK;
        }elseif ($type == 'meeting-room'){
            $id = SharedOfficeProducts::CATEGORY_MEETING_ROOM;
        }
        return $id;
    }

    public function mapJson(Request $request) {
        $limit = isset($request->limit) ? $request->limit : 50;
        $limit = (int)$limit;
        if($request->mobileApp == 'true') {
            if ($request->lat && $request->lang) {
                $sharedoffice = DB::table('shared_offices')
                    ->selectRaw("
                        DISTINCT(shared_offices.id),
                        shared_offices.id,
                        shared_offices.active,
                        shared_offices.lat,
                        shared_offices.lng,
                        office_name,
                        image,
                        location,
                        version_english,
                        version_chinese,
                        office_name_cn,
                        world_cities.name AS city_name,
                        world_countries.name AS country_name,
                        (3959 * acos( cos( radians(' . $request->lat . ') ) * cos( radians( lat ) )  * cos( radians( lng ) - radians(' . $request->lang . ') ) + sin( radians(' . $request->lat . ') ) * sin( radians( lat ) ) ) ) AS 'total_distance'"
                    )
                    ->whereRaw("( 3959 * acos( cos( radians( " . $request->lat . " ) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians( '" . $request->lang . "' ) ) + sin( radians( '" . $request->lat . "' ) ) * sin( radians( lat ) ) ) ) < 10000")
                    ->orderBy('total_distance', 'asc')
                    ->where('active', '=', 1)
                    ->leftJoin('shared_office_languages', 'shared_offices.id', '=', 'shared_office_languages.shared_offices_id')
                    ->leftJoin('world_cities', 'shared_offices.city_id', '=', 'world_cities.id')
                    ->leftJoin('world_countries', 'shared_offices.state_id', '=', 'world_countries.id')
                    ->leftJoin('shared_office_products', 'shared_offices.id', '=', 'shared_office_products.office_id');
                if ($limit) {
                    $sharedoffice = $sharedoffice->paginate($limit);
                } else {
                    $sharedoffice = $sharedoffice->get();
                }
                for ($i = 0; $i < count($sharedoffice); $i++) {
                    $sharedoffice[$i]->location = addcslashes($sharedoffice[$i]->location, '"\\/');
                    $sharedoffice[$i]->avg = (float)SharedOfficeRating::where('office_id', '=', $sharedoffice[$i]->id)->avg('rate');
                    $sharedoffice[$i]->shared_office_rating = SharedOfficeRating::where('office_id', '=', $sharedoffice[$i]->id)->get();
                    $sharedoffice[$i]->shfacilities = DB::table('shared_office_shfacilitie')->where('shared_office_id', '=', $sharedoffice[$i]->id)->get();
                    $sharedoffice[$i]->min_seat_price = SharedOfficeProducts::where('office_id', '=', $sharedoffice[$i]->id)->orderBy('month_price', 'asc')->first();
                }
            } else {
                $sharedoffice = SharedOffice::with('totalOfficeMember', 'city', 'city.country', 'country', 'sharedOfficeRating', 'SharedOfficeLanguageData', 'sharedOfficeLikeCount', 'shfacilities', 'officeImages');
                if (is_null($request->ip)) {
                    $sharedoffice->selectRaw("shared_offices.id,  city_id , state_id                      continent_id,shared_offices.lat,shared_offices.lng,office_name,image,description,location,version_english,version_chinese,created_at,updated_at, (3959 * acos( cos( radians('" . $request->lat . "') ) * cos( radians( lat ) )  * cos( radians( lng ) - radians('" . $request->lang . "') ) + sin( radians('" . $request->lat . "') ) * sin( radians( lat ) ) ) ) AS 'total_distance'");
                } else {
                    $x = geoip($request->ip);
                    $sharedoffice->selectRaw("shared_offices.id,     city_id , state_id                   continent_id,shared_offices.lat,shared_offices.lng,office_name,image,description,location,version_english,version_chinese,created_at,updated_at, (3959 * acos( cos( radians('" . $x->lat . "') ) * cos( radians( lat ) )  * cos( radians( lng ) - radians('" . $x->lon . "') ) + sin( radians('" . $x->lat . "') ) * sin( radians( lat ) ) ) ) AS 'total_distance'");
                }
                if ($limit) {
                    $sharedoffice = $sharedoffice->paginate($limit);
                } else {
                    $sharedoffice = $sharedoffice->get();
                }
            }
        }
        if($request->ip) {
            $x = geoip($request->ip);
            return collect(['sharedoffice' => $sharedoffice,'lat' => $x->lat,'lng' => $x->lon]);
        } else {
            return collect(['sharedoffice' => $sharedoffice]);
        }
    }

    public function mapzoomIn(Request $request)
    {
        $language='';
        if(request('language')&&request('language')=='en'){
            $language = 'version_english';
        }elseif(request('language')&&request('language')=='cn'){
            $language = 'version_chinese';
        }

        $sharedoffice = SharedOffice::with('totalOfficeMember', 'city', 'country', 'sharedOfficeRating', 'SharedOfficeLanguageData', 'sharedOfficeLikeCount', 'shfacilities','officeImages');

        $sharedoffice->selectRaw("shared_offices.id,shared_offices.lat,shared_offices.lng,office_name,image,description,location,version_english,version_chinese,created_at,updated_at, (3959 * acos( cos( radians('" . $request->lat . "') ) * cos( radians( lat ) )  * cos( radians( lng ) - radians('" . $request->lang . "') ) + sin( radians('" . $request->lat . "') ) * sin( radians( lat ) ) ) ) AS 'total_distance'");

        if($language && $language!=''){
            $sharedoffice->whereIn($language,array('true','1'));
        }
        if ($request->limit) $sharedoffice->limit($request->limit);
        $sharedoffice->where('active', '=', 1);
        $sharedoffice =  $sharedoffice->get();

        return response()->json([$sharedoffice]);
    }

    /*****************************************************************************************************/
    /*****************        CheckUserIDVerification --> use in API2,API3,API4 ...   ********************/

    public function CheckUserIDVerification($user_id)
    {
        $userIdentity = UserIdentity::where('user_id', '=', $user_id)
            ->where('status', '=', 1)
            ->first();

        if (isset($userIdentity)) {
            if ($userIdentity->id_image != "" and $userIdentity->hold_id_image != "") {
                /* photo authentication -- user upload both image card_image and hold_card_image*/
                if ($userIdentity->real_name != "" and $userIdentity->id_card_no != "") {
                    $bool_var = true;
                    $msg = 'verify card_id + verify photo';
                    $authentication = "full";
                } else {
                    $bool_var = false;
                    $msg = 'verify photo';
                    $authentication = "photo";
                }
            } elseif ($userIdentity->real_name != "" and $userIdentity->id_card_no != "") {
                $bool_var = true;
                $msg = 'verify card_id';
                $authentication = "card";
            } else {
                $bool_var = false;
                $msg = 'user not complete authentication';
            }
            session(['msg' => $msg]);
            return $bool_var;
        } else {
            $bool_var = false; // false krna hai issy
            //            $msg='user not start authentication yet!!!';
            //            session(['msg' => $msg]);
            return $bool_var;
        }
    }
    /**************************          END CheckUserIDVerification              ***************************/


    /**********************************************************************************************************/
    /*****************                               API 0                               **********************/
    /*****************               Get details for sharing office                      **********************/
    /*****************         http://www.xenren.co/api/officeDetailsBtn                 **********************/
    /*****************          http://localhost:8000/api/officeDetailsBtn               **********************/
    /*****************               POST METHOD user_id, office_id                      **********************/

    public function officeDetailsBtn(Request $request)
    {
        $data = [];
        /* POST method */
        $user_id = $request->get('user_id');
        $office_id = $request->get('office_id');
        $userValidation = $this->CheckUserIDVerification($user_id);   // function return(true or false)

        $user = UserIdentity::where('user_id', '=', $user_id)
            ->first();

        $user_data_country = User::where('id', '=', $user_id)
            ->first();

        /*country*/
        if ($user_data_country) {
            $country = isset($user_data_country->country->name) ? $user_data_country->country->name : '';
        } else {
            $country = null;
        }
        /* city */
        $user_data_city = CountryCities::where('id', '=', $user_data_country->city_id)
            ->first();
        if (!$user_data_city) {
            $city = null;
            $lon = null;
            $lat = null;
        } else {
            $city = $user_data_city->name;
            $lon = $user_data_city->lon;
            $lat = $user_data_city->lat;
        }
        /**************/

        $products = SharedOfficeProducts::select('category_id', 'time_price', 'month_price')
            ->where('office_id', '=', $office_id)
            ->where(function ($q) {
                $q->where('category_id', 1)->orWhere('category_id', 2)->orWhere('category_id', 3);
            })
            ->get();

        $products = collect($products)->unique('category_id');

        $products_arr = [];

        foreach ($products as $pr) {
            $products_arr[] = [
                'name' => $pr->productcategory->name,
                'time_price' => $pr->time_price * 6,
                'month_price' => $pr->month_price
            ];
        }
        /**************/

        /*Count Active user in last month for selected office */
        $users_this_month = SharedOfficeFinance::where(DB::raw('MONTH(start_time)'), '=', date('n'))
            ->where('office_id', '=', $office_id)
            ->count();
        //->get();

        $sharedoffice = SharedOffice::with('shfacilities')->where('id', '=', $office_id)->first();
        if(is_null($sharedoffice)) {
            $sharedoffice = SharedOffice::with('shfacilities')->where('id', '=', $office_id)->onlyTrashed()->first();
        }
        if (isset($sharedoffice)) {

            $facilities_arr = [];
            $dataimages = [];
            $allimages = SharedOfficeImages::where('office_id', '=', $sharedoffice->id)->orderBy('updated_at', 'desc')->get();

            foreach ($allimages as $pic) {
                $dataimages[] = [
                    'name_image' => $pic->image
                ];
            }

            $facility = DB::table('shared_office_shfacilitie')
                ->leftJoin('shfacilities', 'shared_office_shfacilitie.shfacilitie_id', '=', 'shfacilities.id')
                ->where('shared_office_id', $sharedoffice->id)
                ->get();

            foreach ($facility as $fac) {
                $facilities_arr[] = [
                    'name' => $fac->title
                ];
            }

            $data[] = [
                'success' => true,
                'image' => asset($sharedoffice->image),
                'moreimages' => $dataimages,
                'facility' => $facilities_arr,
                'number' => $sharedoffice->number,
                'contact_Phone' => $sharedoffice->contact_phone,
                'qr' => $sharedoffice->qr,
                'price' => $products_arr,
                'user_id' => $user_id,
                'real_name' => isset($user->real_name) ? $user->real_name : '',
                'location_user' => [
                    'country' => $country,
                    'city' => $city,
                    'lon' => $lon,
                    'lat' => $lat,
                ],
                'office_details' => [
                    'id' => $sharedoffice->id,
                    'office_name' => $sharedoffice->office_name,
                    'size_x' => $sharedoffice->office_size_x,
                    'size_y' => $sharedoffice->office_size_y,
                    'location' => $sharedoffice->location,
                    'description' => $sharedoffice->description,
                    'english_version' => $sharedoffice->version_english,
                    'chinese_version' => $sharedoffice->version_chinese,
                    'shared_office_languages' => $sharedoffice->SharedOfficeLanguageData,
                    'contact_email' => $sharedoffice->contact_email,
                    'contact_phone' => $sharedoffice->contact_phone,
                    'monday_opening_time'=> $sharedoffice->monday_opening_time,
                    'monday_closing_time'=> $sharedoffice->monday_closing_time,
                    'sunday_opening_time'=> $sharedoffice->sunday_opening_time,
                    'sunday_closing_time'=> $sharedoffice->sunday_closing_time,
                ],
                'total_user' => SharedOfficeFinance::where('office_id', '=', $sharedoffice->id)->count(),
                'active_user_last_30_days' => $users_this_month,
                'created_at' => $sharedoffice->created_at,
                'updated_at' => $sharedoffice->updated_at,
            ];
            return response()->json($data);
        } else {
            $data[] = [
                'msg' => 'Wrong QR code for Office.',
                'success' => false,
            ];
            return response()->json($data);
        }
    }

    public function officeSpaceDetailsBtn(Request $request, $officeId)
    {
        $data = [];
        /* POST method */
//        $user_id = $request->get('user_id');
        $office_id = $officeId;
//        $userValidation = $this->CheckUserIDVerification($user_id);   // function return(true or false)
//
//        $user = UserIdentity::where('user_id', '=', $user_id)
//            ->first();
//
//        $user_data_country = User::where('id', '=', $user_id)
//            ->first();
//        /*country*/
//        if ($user_data_country) {
//            $country = isset($user_data_country->country->name) ? $user_data_country->country->name : '';
//        } else {
//            $country = null;
//        }
//        /* city */
//        if(isset($user_data_country->city_id)) {
//            $user_data_city = CountryCities::where('id', '=', $user_data_country->city_id)
//                ->first();
//        }
//        if (!isset($user_data_city)) {
//            $city = null;
//            $lon = null;
//            $lat = null;
//        } else {
//            $city = $user_data_city->name;
//            $lon = $user_data_city->lon;
//            $lat = $user_data_city->lat;
//        }
//        /**************/
//
//        $products = SharedOfficeProducts::select('category_id', 'time_price', 'month_price')
//            ->where('office_id', '=', $office_id)
//            ->where(function ($q) {
//                $q->where('category_id', 1)->orWhere('category_id', 2)->orWhere('category_id', 3);
//            })
//            ->get();
//
//        $products = collect($products)->unique('category_id');
//
//        $products_arr = [];
//
//        foreach ($products as $pr) {
//            $products_arr[] = [
//                'name' => $pr->productcategory->name,
//                'time_price' => $pr->time_price * 6,
//                'month_price' => $pr->month_price
//            ];
//        }
//        /**************/
//
//        /*Count Active user in last month for selected office */
//        $users_this_month = SharedOfficeFinance::where(DB::raw('MONTH(start_time)'), '=', date('n'))
//            ->where('office_id', '=', $office_id)
//            ->count();
//        //->get();

        \DB::enableQueryLog();
        $data = SharedOfficeFinance::with('office')
            ->where('id', '=', $office_id)
            ->whereHas('office')
            ->first();

        $sharedoffice = $data->office;

        if (isset($sharedoffice)) {

            $facilities_arr = [];
            $dataimages = [];
            $allimages = SharedOfficeImages::where('office_id', '=', $sharedoffice->id)->orderBy('updated_at', 'desc')->get();

            foreach ($allimages as $pic) {
                $dataimages[] = [
                    'name_image' => $pic->compressed_image
                ];
            }

            $facility = DB::table('shared_office_shfacilitie')
                ->leftJoin('shfacilities', 'shared_office_shfacilitie.shfacilitie_id', '=', 'shfacilities.id')
                ->where('shared_office_id', $sharedoffice->id)
                ->get();

            foreach ($facility as $fac) {
                $facilities_arr[] = [
                    'name' => $fac->title
                ];
            }

            return collect([
                'success' => true,
                'image' => $sharedoffice->compressed_image,
                'moreimages' => $dataimages,
                'facility' => $facilities_arr,
                'number' => $sharedoffice->number,
                'contact_Phone' => $sharedoffice->contact_phone,
                'qr' => $sharedoffice->qr,
                'user_id' => \Auth::user()->id,
                'real_name' => isset($user->real_name) ? \Auth::user()->real_name : '',
                'id' => $sharedoffice->id,
                'office_name' => $sharedoffice->office_name,
                'size_x' => $sharedoffice->office_size_x,
                'size_y' => $sharedoffice->office_size_y,
                'location' => $sharedoffice->location,
                'description' => $sharedoffice->description,
                'english_version' => $sharedoffice->version_english,
                'chinese_version' => $sharedoffice->version_chinese,
                'shared_office_languages' => $sharedoffice->SharedOfficeLanguageData,
                'contact_email' => $sharedoffice->contact_email,
                'contact_phone' => $sharedoffice->contact_phone,
                'monday_opening_time'=> $sharedoffice->monday_opening_time,
                'monday_closing_time'=> $sharedoffice->monday_closing_time,
                'sunday_opening_time'=> $sharedoffice->sunday_opening_time,
                'sunday_closing_time'=> $sharedoffice->sunday_closing_time,
                'created_at' => $sharedoffice->created_at,
                'updated_at' => $sharedoffice->updated_at,
            ]);
        } else {
            $data[] = [
                'msg' => 'Wrong QR code for Office.',
                'success' => false,
            ];
            return response()->json($data);
        }
    }

    public function officeDetailsBtnget($id)
    {
        $sharedoffice = SharedOffice::with([
            'officeImages' => function($q){
                $q->orderBy('updated_at', 'desc')->limit(5);
            },
            'currency' => function($q) {
                $q->select('id','currency_code','currency_name');
            },
            'shfacilities',
            'officeProducts.productcategory',
            'bulletins.bulletinDetail',
            'SharedOfficeLanguageData',
            'sharedOfficeRating.rater',
            'sharedOfficeFee',
        ])->where('id', '=', $id)->first();
        // $location = str_replace('-',' ',$sharedoffice->location);
        if(isset($sharedoffice->officeProducts)) {
        $sharedOfficeProducts = $sharedoffice->officeProducts->unique('category_id');
        $sharedoffice['categories'] = $sharedOfficeProducts;
        if(!is_null($sharedoffice->currency)) {
            $currencyName = $sharedoffice->currency->currency_code;
            $convertPrice = $this->getCurrency($currencyName);
            // $convertPrice = $this->cleanPrice($convertPrice, $currencyName);
            \Log::info($sharedoffice->office_name);
            \Log::info('converted price: ' . $convertPrice);
            foreach ($sharedoffice->officeProducts as $k => $products) {
                $sharedoffice->officeProducts[$k]['month_price'] = round((double)$products->month_price * $convertPrice, 2);
                $sharedoffice->officeProducts[$k]['hourly_price'] = round((double)$products->hourly_price * $convertPrice, 2);
                $sharedoffice->officeProducts[$k]['weekly_price'] = round((double)$products->weekly_price * $convertPrice, 2);
                $sharedoffice->officeProducts[$k]['daily_price'] = round((double)$products->daily_price * $convertPrice, 2);
            }

            \Log::info('-----------');
            \Log::info(json_encode($sharedoffice->officeProducts));
            \Log::info('-----------');
        }
            return collect($sharedoffice);
        } else {
            return collect($sharedoffice);
        }
    }

    public function cleanPrice($convertPrice, $currencyName){
        $data = json_decode($convertPrice);
        $price = 0;
        if (property_exists($data, 'rates')) {
            $price = round($data->rates->USD, 3);
        }

        return $price;
    }

    public function getCurrency($currencyName)
    {
        // scrap using transferwise
        $from = strtolower($currencyName);

        $client = new Client();
        $crawler = $client->request('GET', 'https://transferwise.com/gb/currency-converter/'.$from.'-to-usd-rate');

        $wrapper = $crawler->filter('.cc__source-to-target')->each(function ($innerNode) {
            return $innerNode->filter('.text-success')->text();
        });

        return (double) current($wrapper);

        // scrap from exchangeratesapi
        // $curl = curl_init();
        // curl_setopt_array($curl, array(
        //     CURLOPT_URL => "https://api.exchangeratesapi.io/latest?base=".$currencyName."&symbols=USD",
        //     CURLOPT_RETURNTRANSFER => true,
        //     CURLOPT_ENCODING => "",
        //     CURLOPT_MAXREDIRS => 10,
        //     CURLOPT_TIMEOUT => 30,
        //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //     CURLOPT_CUSTOMREQUEST => "GET",
        //     CURLOPT_POSTFIELDS => "",
        //     CURLOPT_HTTPHEADER => array(
        //         "Postman-Token: b40806c9-4c2b-4139-9b6e-d8690b520148",
        //         "cache-control: no-cache"
        //     ),
        // ));
        // $response = curl_exec($curl);
        // $err = curl_error($curl);

        // curl_close($curl);
        // if ($err) {
        //     return "cURL Error #:" . $err;
        // } else {
        //     return $response;
        // }
    }

    // // OLD Clean price
    // public function cleanPrice($convertPrice,$currencyName){
    //     $convertPrice = str_replace('{"'.$currencyName.'_USD":','',$convertPrice);
    //     return $convertPrice = round((double)str_replace('}','',$convertPrice),3);
    // }

    // OLD GET Currency
    // public function getCurrency($currencyName)
    // {
    //     $from = $currencyName;
    //     $to = 'USD';
    //     $curl = curl_init();
    //     // https://free.currconv.com/api/v7/convert?q=USD_PHP&compact=ultra&apiKey=5936149c17cbe2186434
    //     $q = strtoupper($from) ."_".strtoupper($to);
    //     curl_setopt_array($curl, array(
    //         CURLOPT_URL => "https://free.currconv.com/api/v7/convert?q=".$q."&compact=ultra&apiKey=5936149c17cbe2186434",
    //         CURLOPT_RETURNTRANSFER => true,
    //         CURLOPT_ENCODING => "",
    //         CURLOPT_MAXREDIRS => 10,
    //         CURLOPT_TIMEOUT => 30,
    //         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //         CURLOPT_CUSTOMREQUEST => "GET",
    //         CURLOPT_POSTFIELDS => "",
    //         CURLOPT_HTTPHEADER => array(
    //             "Postman-Token: b40806c9-4c2b-4139-9b6e-d8690b520148",
    //             "cache-control: no-cache"
    //         ),
    //     ));
    //     $response = curl_exec($curl);
    //     $err = curl_error($curl);

    //     curl_close($curl);
    //     if ($err) {
    //         return "cURL Error #:" . $err;
    //     } else {
    //         return $response;
    //     }
    // }

    public function helpfulReview(SharedOfficeRating $reviewId)
    {
        $exist = SharedOfficeRatingHelpfulCount::where('user_id', '=', Auth::user()->id)->where('shared_office_rating_id', '=', $reviewId->id)->first();
        if (is_null($exist)) {
            SharedOfficeRatingHelpfulCount::create([
                'user_id' => Auth::user()->id,
                'shared_office_rating_id' => $reviewId->id
            ]);
            $reviewId->increment('helpful_count');
            return collect([
                'status' => 'success',
                'message' => 'your vote casted',
                'user' => Auth::user(),
                'data' => $reviewId
            ]);
        } else {
            return collect([
                'status' => 'error',
                'message' => 'your vote already casted'
            ]);
        }
    }

    public function likeSharedOffice(SharedOffice $sharedOffice)
    {
        $exist = SharedOfficeLikeCount::where('user_id', '=', Auth::user()->id)->where('shared_office_id', '=', $sharedOffice->id)->first();
        if (is_null($exist)) {
            SharedOfficeLikeCount::create([
                'user_id' => Auth::user()->id,
                'shared_office_id' => $sharedOffice->id
            ]);
            $sharedOffice->increment('likes');
            return collect([
                'status' => 'success',
                'message' => 'Office Liked...!',
            ]);
        } else {
            return collect([
                'status' => 'error',
                'message' => 'your already liked..!'
            ]);
        }
    }

    /************************************************END API 0**************************************************/

    /**************************          END CheckUserIDVerification              ***************************/

    /********************************************************************************************************/
    /*****************                               API 1                             **********************/
    /*****************               Login API to check user verification              **********************/
    /*****************         http://www.xenren.co/api/sharedofficeUserValidation     **********************/
    /*****************          http://localhost:8000/api/sharedofficeUserValidation/  **********************/
    /*****************                      POST METHOD user_id                    **********************/

    public function RentProductsActive($office)
    {
        $data = SharedOffice::with('officeProducts')->where('id', '=', $office)->first();
        return collect([
            'status' => 'success',
            'data' => $data->officeProducts
        ]);
    }

    public function loginVerification(Request $request)
    {
        $data = [];
        $user_id = $request->get('user_id');
        $userIdentity = UserIdentity::where('user_id', '=', $user_id)
            ->first();
        if (isset($userIdentity)) {
            if ($userIdentity->country_id != "") {
                $countries = Countries::where('id', '=', $userIdentity->country_id)
                    ->first();
                $name_country = $countries->name;
            } else {
                $name_country = "";
            }

            if ($userIdentity->id_image != "" and $userIdentity->hold_id_image != "") {
                /* photo authentication -- user upload both image card_image and hold_card_image*/
                if ($userIdentity->real_name != "" and $userIdentity->id_card_no != "") {
                    $bool_var = true;
                    $msg = 'verify card_id + verify photo';
                    $authentication = "full";
                } else {
                    $bool_var = false;
                    $msg = 'verify photo';
                    $authentication = "photo";
                }
                $data[] = [
                    'user_id' => $userIdentity->user_id,
                    'real_name' => $userIdentity->real_name,
                    'id_card_no' => $userIdentity->id_card_no,
                    'gender' => $userIdentity->gender,
                    'date_of_birth' => $userIdentity->date_of_birth,
                    'address' => $userIdentity->address,
                    'country' => $name_country,
                    'handphone_no' => $userIdentity->handphone_no,
                    'card_image' => $userIdentity->id_image,
                    'holding_card_image' => $userIdentity->hold_id_image,
                    'status' => $userIdentity->status,
                    'authentication' => $authentication,
                    'is_submit' => $userIdentity->is_submit,
                    'created_at' => $userIdentity->created_at,
                    'updated_at' => $userIdentity->updated_at,
                    'success' => $bool_var,
                    'msg' => $msg
                ];
            } elseif ($userIdentity->real_name != "" and $userIdentity->id_card_no != "") {
                $bool_var = false;
                $msg = 'verify card_id';
                $data[] = [
                    'user_id' => $userIdentity->user_id,
                    'real_name' => $userIdentity->real_name,
                    'id_card_no' => $userIdentity->id_card_no,
                    'gender' => $userIdentity->gender,
                    'date_of_birth' => $userIdentity->date_of_birth,
                    'address' => $userIdentity->address,
                    'country' => $name_country,
                    'handphone_no' => $userIdentity->handphone_no,
                    'card_image' => $userIdentity->id_image,
                    'holding_card_image' => $userIdentity->hold_id_image,
                    'status' => $userIdentity->status,
                    'authentication' => 'card',
                    'is_submit' => $userIdentity->is_submit,
                    'created_at' => $userIdentity->created_at,
                    'updated_at' => $userIdentity->updated_at,
                    'success' => $bool_var,
                    'msg' => $msg
                ];
            } else {
                $bool_var = false;
                $msg = 'user not complete authentication';
                $data[] = [
                    'user_id' => $userIdentity->user_id,
                    'real_name' => $userIdentity->real_name,
                    'id_card_no' => $userIdentity->id_card_no,
                    'gender' => $userIdentity->gender,
                    'date_of_birth' => $userIdentity->date_of_birth,
                    'address' => $userIdentity->address,
                    'country' => $name_country,
                    'handphone_no' => $userIdentity->handphone_no,
                    'card_image' => $userIdentity->id_image,
                    'holding_card_image' => $userIdentity->hold_id_image,
                    'status' => $userIdentity->status,
                    'authentication' => 'none',
                    'is_submit' => $userIdentity->is_submit,
                    'created_at' => $userIdentity->created_at,
                    'updated_at' => $userIdentity->updated_at,
                    'success' => $bool_var,
                    'msg' => $msg
                ];
            }
            return response()->json($data);
        } else {
            $data[] = [
                'user_id' => $user_id,
                'authentication' => 'none',
                'success' => false,
                'msg' => 'user not start authentication yet!!!'
            ];
            return response()->json($data);
        }
    }
    /**************************************  END   API 1    *****************************************************/

    /*************************************************************************************************************/
    /*********                                        API 2  (REVIZIJA1)                            **************/
    /*********    Open SharingOffice with QR code office from Door, Return all seats in mobile app  **************/
    /*********                    http://www.xenren.co/api/sharedofficeQrOfficeDoor                 **************/
    /*********                    http://localhost:8000/api/sharedofficeQrOfficeDoor/               **************/
    /*********                          POST METHOD user_id, qrOfficeDoor,                          **************/
    /*
         public function sharedofficeQrOfficeDoor(Request $request)
         {
             $data = [];
                 // POST method
                 $user_id = $request->get('user_id');
                 $number = $request->get('qrOfficeDoor');
                 $userValidation = $this->CheckUserIDVerification($user_id);   // function return(true or false)

                 if ($userValidation) {
                     $user = UserIdentity::where('user_id', '=', $user_id)
                         ->first();

                 $sharedoffice = SharedOffice::where('number', '=', $number)->first();

                 if (isset($sharedoffice)) {
                     $sharedofficeproducts = SharedOfficeProducts::where('office_id', '=', $sharedoffice->id)
                         ->orderBy('x', 'asc')
                         ->orderBy('y', 'asc')
                         ->get();
                     if ($sharedofficeproducts->count() > 0) {
                         foreach ($sharedofficeproducts as $shar) {
                             if($shar->status_id==1){
                                 $status_bool=true;
                             }
                             else{
                                 $status_bool=false;
                             }
                             $seats[] = [
                                 'type' => $shar->productcategory->name,
                                 'position'=>
                                     $position[]=[
                                         'x' => $shar->x,
                                         'y' => $shar->y,
                                     ],

                                 $shar->productcategory->name=>[
                                 'id' => $shar->id,
                                 'busy'=>$status_bool,
                                 'status_id' => $shar->status_id,
                                 'status_name' => $shar->productstatus->status,
                                 'status_id_explanation' => '1-using,3-empty,2-month',
                                 'category_id' => $shar->category_id,
                                 'category_id_explanation' => '1-Seat, 2-Meeting room,3-Plants, 4-Table ...',
                                 'number' => $shar->number,
                                 'timeprice' => $shar->time_price,
                                 'description_timeprice' => 'Price on 10 minutes,ex. 10$=10min',
                                 'remark' => $shar->remark,
                                 'qr' => $shar->qr,
                                 ],
                                 'created_at' => $shar->created_at,
                                 'updated_at' => $shar->updated_at,
                             ];
                         }
                         $data[] = [
                             'office_id' => $shar->office_id,
                             'office_name' => $shar->office->office_name,
                             'office_size_x' => $shar->office->office_size_x,
                             'office_size_y' => $shar->office->office_size_y,
                             'user_id' => $user_id,
                             'real_name' => $user->real_name,
                             'success' => true,
                             'msg' => 'Success pulled seats for scaner QR sticker from Door',
                             'layouts' => $seats,
                         ];
                         return response()->json($data);
                     } else {
                         $data[] = [
                             'user_id' => $user_id,
                             'office_id' => $sharedoffice->id,
                             'office_name' => $sharedoffice->office_name,
                             'success' => false,
                             'msg' => 'No Seats!!. There is no Products(Seats) for selected office in database!!!'
                         ];
                         return response()->json($data);
                     }
                 }
                 elseif (!isset($sharedoffice)) {
                     $data[] = [
                         'user_id' => $user_id,
                         'real_name' => $user->real_name,
                         'success' => false,
                         'msg' => 'QR CODE from Door Not Active. There is no Office for selected QR code in database!!!'
                     ];
                     return response()->json($data);
                 }
             }
             else{
                 $data[] = [
                     'msg'=>'User_id FAIL. You MUST first verify your accont',
                     'success' => false,
                 ];
                 return response()->json($data);
             }
         }
         */
    /**************************************  END   API 2   **************************************************/

    /*************************************************************************************************************/
    /*********                                        API 2.2  (REVIZIJA2)                            ************/
    /*********    Open SharingOffice with QR code office from Door, Return all seats in mobile app  **************/
    /*********                    http://www.xenren.co/api/sharedofficeQrOfficeDoor                 **************/
    /*********                    http://localhost:8000/api/sharedofficeQrOfficeDoor/               **************/
    /*********                          POST METHOD user_id, qrOfficeDoor,                          **************/

    public function sharedofficeQrOfficeDoor(Request $request)
    {
        $data = [];
        /* POST method */
        //        $user_id = $request->get('user_id');
        $user_id = \Auth::user()->id;
        $number = $request->get('qrOfficeDoor');
        $userValidation = $this->CheckUserIDVerification($user_id);   // function return(true or false)

        if ($userValidation) {
            $user = UserIdentity::where('user_id', '=', $user_id)
                ->first();

            $sharedoffice = SharedOffice::where('id', '=', $number)->first();

            if (!is_null($sharedoffice)) {
                $sharedofficeproducts = SharedOfficeProducts::where('office_id', '=', $sharedoffice->id)
                    ->orderBy('x', 'asc')
                    ->orderBy('y', 'asc')
                    ->get();

                $office_details['size'] = [
                    'x' => $sharedoffice->office_size_x,
                    'y' => $sharedoffice->office_size_y
                ];

                if ($sharedofficeproducts->count() > 0) {
                    foreach ($sharedofficeproducts as $shar) {
                        if ($shar->status_id == 1) {
                            $status_bool = true;
                        } else {
                            $status_bool = false;
                        }
                        $image = SharedOfficeProductCategory::where('id', '=', $shar->category_id)->first();
                        $seats[] = [
                            'id' => $shar->id,
                            'type' => $shar->productcategory->name,
                            'position' =>
                            $position[] = [
                                'x' => $shar->x,
                                'y' => $shar->y,
                            ],
                            'extra' =>
                            $extra[] = [
                                'busy' => $status_bool,
                                'number' => $shar->number,
                                'qr' => $shar->qr,
                                'name' => $image->name
                                /*
                                     'status_id' => $shar->status_id,
                                    'status_name' => $shar->productstatus->status,
                                    'status_id_explanation' => '1-using,3-empty,2-month',
                                    'category_id' => $shar->category_id,
                                    'category_id_explanation' => '1-Seat, 2-Meeting room,3-Plants, 4-Table ...',
                                     'timeprice' => $shar->time_price,
                                    'description_timeprice' => 'Price on 10 minutes,ex. 10$=10min',
                                    'remark' => $shar->remark,

                                    */
                            ],
                        ];
                    }
                    $office_details['office_id'] = $shar->office_id;
                    $office_details['items'] = $seats;
                    $data[] = [
                        //'user_id' => $user_id,
                        //'real_name' => $user->real_name,
                        //                        'office_id' => 'office:'.$shar->office_id,
                        //'office_name' => $shar->office->office_name,
                        'status' => 'success',
                        'data' => $office_details,
                    ];
                    return response()->json($data[0]);
                } else {
                    $data[] = [
                        'user_id' => $user_id,
                        'office_id' => "office:" . $sharedoffice->id,
                        'office_name' => $sharedoffice->office_name,
                        'success' => false,
                        'msg' => 'No Seats!!. There is no Products(Seats) for selected office in database!!!'
                    ];
                    return response()->json($data);
                }
            } elseif (is_null($sharedoffice)) {
                $data[] = [
                    'user_id' => $user_id,
                    'real_name' => $user->real_name,
                    'success' => false,
                    'msg' => 'QR CODE from Door Not Active. There is no Office for selected QR code in database!!!'
                ];
                return response()->json($data);
            }
        } else {
            $data[] = [
                'msg' => 'User_id FAIL. You MUST first verify your accont',
                'tech_msg' => session('msg'),
                'success' => false,
            ];
            return response()->json($data);
        }
    }
    /**************************************  END   API 2.2 rev 2   **************************************************/



    /********************************************************************************************************/
    /******************                                API 3                                   **************/
    /******************   Pick Seat by QR code sticker from seat in office room , START TIMER  **************/
    /******************             http://www.xenren.co/api/sharedofficeQrSeat                **************/
    /******************             http://localhost:8000/api/sharedofficeQrSeat/              **************/
    /******************                  POST METHOD user_id, qr_seat,                         **************/

    public function sharedofficeQrSeat(Request $request)
    {
//        try {
            $data = [];
            /* POST method */
            $user_id = \Auth::user()->id;
            $number = $request->get('qr_seat');
            $userValidation = $this->CheckUserIDVerification($user_id);   // function return(true or false)
            $seatValidation = $this->CheckUserSeatVerification($user_id);   // function return(true or false)

            $products = SharedOfficeProducts::where('number', '=', $number)
                ->with('office.owner')
                ->where('office_id', '=', $request->get('office_id'))
//                ->where('status_id', '=', SharedOfficeProducts::STATUS_EMPTY) //seat must be free
                ->first();

            $productPrice = null;
            $office = SharedOffice::where('id', '=', $request->get('office_id'))->first();

            if (is_null($office)) {
                return collect([
                    'message' => 'This seat is not available.!! Please select other Seat',
                    'status' => 'error',
                ]);
            }
            if($request->other_scanner !== '1') {
                if ($office->verify_info_to_scan == 1) {
                    if ($userValidation == false) {
                        return collect([
                            'message' => 'Your Account is not verified please verify your account first!!',
                            'status' => 'error',
                        ]);
                    }
                }
            }
            //            if ($userValidation || $products->office->verify_info_to_scan == 0) {
            //                if ($seatValidation) {
            //                    return collect([
            //                        'status' => 'error',
            //                        'message' => 'User already using a seat!'
            //                    ]);
            //                }
            $usr = \Auth::user();

            if ($office->require_deposit == 1) {
                if(!isset($request->other_scanner) && $request->other_scanner != 1){
                    if ($usr->account_balance <= 0) {
                        return collect([
                            'message' => 'Insufficient balance or you need to recharge the balance!',
                            'status' => 'error',
                        ]);
                    }
                }
                if(isset($request->scan_type) && $request->scan_type == 1) { //HOURLY
                    //SKIP AS IT IS HOURLY
                }
                if(isset($request->scan_type) && $request->scan_type == 2) { //WEEKLY
                    if($usr->account_balance < $products->weekly_price) {
                        return collect([
                            'message' => 'Insufficient balance or you need to recharge the balance!',
                            'status' => 'error',
                            'type' => 'insufficient_balance'
                        ]);
                    }
                    $productPrice = is_null($products->weekly_price) ? 0 : $products->weekly_price;
                }
                if(isset($request->scan_type) && $request->scan_type == 3) { //DAILY
                    if($usr->account_balance < $products->daily_price) {
                        return collect([
                            'message' => 'Insufficient balance or you need to recharge the balance!',
                            'status' => 'error',
                            'type' => 'insufficient_balance'
                        ]);
                    }
                    $productPrice = is_null($products->daily_price) ? 0 : $products->daily_price;
                }
                if(isset($request->scan_type) && $request->scan_type == 4) { //MONTHLY
                    if($usr->account_balance < $products->month_price) {
                        return collect([
                            'message' => 'Insufficient balance or you need to recharge the balance!',
                            'status' => 'error',
                            'type' => 'insufficient_balance'
                        ]);
                    }
                    $productPrice = is_null($products->month_price) ? 0 : $products->month_price;
                }
                \Log::info($request->all());
                if(isset($request->is_custom_date) && $request->is_custom_date ==true) {
                    $startDate = Carbon::now();
                    $endDate = isset($request->end_date) && $request->end_date != 'undefined' ? (Carbon::parse($request->end_date)) : Carbon::now()->addDays(1);
                    $diffInDays = $endDate->diffInDays($startDate);
                    $dayRate = $products->daily_price;
                    $weekRate = $products->weekly_price;
                    $monthRate = $products->month_price;
                    if($diffInDays <= 7) {
                        $productPrice = $diffInDays * $dayRate;
                    }
                    if($diffInDays > 7 && $diffInDays < 30) {
                        $productPrice = ($weekRate/7) * $diffInDays;
                    }
                    if($diffInDays >=30) {
                        $productPrice = $monthRate / (30 * $diffInDays);
                    }
                }
            } else {
                if(!isset($request->other_scanner) && $request->other_scanner != 1) {
                    return collect([
                        'status' => 'error',
                        'message' => 'you need to have balance before the seat scan'
                    ]);
                }
            }

            $user = UserIdentity::where('user_id', '=', $user_id)
                ->first();

//            \DB::beginTransaction();
            if (!is_null($products)) {
                SharedOfficeFinance::where('user_id', '=', $user_id)->where('cost', '=', null)->update([
                    'end_time' => Carbon::now(),
                    'type' => SharedOfficeFinance::TYPE_OTHER_BAR_CODES
                ]);
                $of_id = $products->office_id;
                $pi_id = $products->id;
                $products->status_id = "1";  // put seat in USING-> 1
                $products->save();           // put status 1 in database, so seat from now is in USING

                if(isset($request->is_custom_date) && $request->is_custom_date ==true) {
                    $startTime = (Carbon::now());
                    $endTime = isset($request->end_date) && $request->end_date != 'undefined' ? (Carbon::parse($request->end_date)) : null;
                } else {
                    $startTime = $request->scan_type != SharedOfficeFinance::SCAN_TYPE_HOURLY ? $request->start_date : Carbon::now();
                    $endTime = $request->scan_type != SharedOfficeFinance::SCAN_TYPE_HOURLY ? $request->end_date : Carbon::now()->addDays(7);
                }

                $transaction = new SharedOfficeFinance();
                $transaction->start_time = $startTime; /*start TIMER */
                $transaction->user = isset($user->name) ? $user->name : '';
                $transaction->x = $products->x;
                $transaction->y = $products->y;
                $transaction->user_id = $user_id;
                if(isset($request->scan_type) && $request->scan_type != SharedOfficeFinance::SCAN_TYPE_HOURLY) {
                    $transaction->end_time = isset($endTime) && $endTime != '' && $endTime != 'undefined' ? $endTime : null;
                } else {
                    $transaction->end_time = null;
                }
                $transaction->scan_type = $request->scan_type;
                $transaction->office_id = $of_id;
                $transaction->product_id = $pi_id;
                if(isset($request->scan_type) && $request->scan_type != SharedOfficeFinance::SCAN_TYPE_HOURLY) {
                    $transaction->cost = $productPrice;
                }
                $transaction->save();

                if(isset($request->scan_type) && $request->scan_type != SharedOfficeFinance::SCAN_TYPE_HOURLY) {
                    $newBalance = Auth::user()->account_balance - $productPrice;
                    $tr = Transaction::create([
                        'name' => 'Seat Scan - Shared Office',
                        'amount' => $productPrice,
                        'milestone_id' => 0,
                        'due_date' => Carbon::now()->addYear(1),
                        'comment' => $productPrice . ' For Seat Scan',
                        'performed_by_balance_before' => Auth::user()->account_balance,
                        'performed_by_balance_after' => $newBalance,
                        'performed_to_balance_before' => Auth::user()->account_balance,
                        'performed_to_balance_after' => $newBalance,
                        'performed_by' => Auth::user()->id,
                        'performed_to' => Auth::user()->id,
                        'type' => Transaction::TYPE_TOP_UP,
                        'currency' => 'USD',
                        'status' => Transaction::STATUS_APPROVED_TRANSACTION,
                    ]);
                    TransactionDetail::create([
                        'transaction_id' => $tr->id,
                        'payment_method' => TransactionDetail::PAYMENT_METHOD_PAYPAL,
                        'raw' => '--'
                    ]);

                    $tax = $productPrice * 9.0;
                    $performed_by_balance_before = $products->office->owner->balance;
                    $performed_by_balance_after = $products->office->owner->balance > $tax ? $products->office->owner->balance - $tax : 0;
                    $admin = Staff::where('id', '=', 1)->first();
                    $tr = Transaction::create([
                        'name' => 'Seat Scan - Shared Office Admin Fee',
                        'amount' => $tax,
                        'milestone_id' => 0,
                        'due_date' => Carbon::now()->addYear(1),
                        'comment' => $tax . ' For Seat Scan',
                        'performed_by_balance_before' => $performed_by_balance_before,
                        'performed_by_balance_after' => $performed_by_balance_after,
                        'performed_to_balance_before' => $admin->account_balance,
                        'performed_to_balance_after' => $admin->account_balance + $tax,
                        'performed_by' => Auth::user()->id,
                        'performed_to' => $products->office->owner->id,
                        'type' => Transaction::TYPE_SERVICE_FEE,
                        'currency' => 'USD',
                        'status' => Transaction::STATUS_APPROVED_TRANSACTION,
                    ]);
                    TransactionDetail::create([
                        'transaction_id' => $tr->id,
                        'payment_method' => TransactionDetail::PAYMENT_METHOD_PAYPAL,
                        'raw' => '--'
                    ]);



                    User::where('id', '=', Auth::user()->id)->update([
                        'account_balance' => $newBalance
                    ]);
                } else if(isset($request->scan_type) && $request->scan_type == SharedOfficeFinance::SCAN_TYPE_HOURLY) {
                    $checkUserBalance = Auth::user()->account_balance;
                    if($checkUserBalance == 0) {
                        $tr = Transaction::create([
                            'name' => 'Seat Scan - Shared Office',
                            'amount' => $productPrice,
                            'milestone_id' => 0,
                            'due_date' => Carbon::now()->addYear(1),
                            'comment' => $productPrice . ' For Seat Scan',
                            'performed_by_balance_before' => Auth::user()->account_balance,
                            'performed_by_balance_after' => '',
                            'performed_to_balance_before' => Auth::user()->account_balance,
                            'performed_to_balance_after' => '',
                            'performed_by' => Auth::user()->id,
                            'performed_to' => Auth::user()->id,
                            'type' => Transaction::TYPE_INITIAL_SEAT_SCAN,
                            'currency' => 'USD',
                            'status' => Transaction::STATUS_INITIAL_SEAT_SCAN,
                        ]);
                    } else {
                        $newBalance = Auth::user()->account_balance - $productPrice;
                        if ($newBalance < 0 ) {
                            return collect([
                                'status' => 'error',
                                'message' => 'you need to have more balance before do this transaction'
                            ]);
                        }
                        $tr = Transaction::create([
                            'name' => 'Seat Scan - Shared Office',
                            'amount' => $productPrice,
                            'milestone_id' => 0,
                            'due_date' => Carbon::now()->addYear(1),
                            'comment' => $productPrice . ' For Seat Scan',
                            'performed_by_balance_before' => Auth::user()->account_balance,
                            'performed_by_balance_after' => $newBalance,
                            'performed_to_balance_before' => Auth::user()->account_balance,
                            'performed_to_balance_after' => $newBalance,
                            'performed_by' => Auth::user()->id,
                            'performed_to' => Auth::user()->id,
                            'type' => Transaction::TYPE_TOP_UP,
                            'currency' => 'USD',
                            'status' => Transaction::STATUS_APPROVED_TRANSACTION,
                        ]);
                        TransactionDetail::create([
                            'transaction_id' => $tr->id,
                            'payment_method' => TransactionDetail::PAYMENT_METHOD_PAYPAL,
                            'raw' => '--'
                        ]);

                        $tax = $productPrice * 9.0;
                        $performed_by_balance_before = $products->office->owner->balance;
                        $performed_by_balance_after = $products->office->owner->balance > $tax ? $products->office->owner->balance - $tax : 0;
                        $admin = Staff::where('id', '=', 1)->first();
                        $tr = Transaction::create([
                            'name' => 'Seat Scan - Shared Office Admin Fee',
                            'amount' => $tax,
                            'milestone_id' => 0,
                            'due_date' => Carbon::now()->addYear(1),
                            'comment' => $tax . ' For Seat Scan',
                            'performed_by_balance_before' => $performed_by_balance_before,
                            'performed_by_balance_after' => $performed_by_balance_after,
                            'performed_to_balance_before' => $admin->account_balance,
                            'performed_to_balance_after' => $admin->account_balance + $tax,
                            'performed_by' => Auth::user()->id,
                            'performed_to' => $products->office->owner->id,
                            'type' => Transaction::TYPE_SERVICE_FEE,
                            'currency' => 'USD',
                            'status' => Transaction::STATUS_APPROVED_TRANSACTION,
                        ]);
                        TransactionDetail::create([
                            'transaction_id' => $tr->id,
                            'payment_method' => TransactionDetail::PAYMENT_METHOD_PAYPAL,
                            'raw' => '--'
                        ]);

                        User::where('id', '=', Auth::user()->id)->update([
                            'account_balance' => $newBalance
                        ]);
                    }
                }

                /*also save in revision as zero revision orginal*/
                $financerevision = new SharedOfficeFinanceRevision();
                $financerevision->transaction_id = $transaction->id;
                $financerevision->x = $products->x;
                $financerevision->y = $products->y;
                $financerevision->start_time = \Carbon\Carbon::now();
                $financerevision->rev = 0;
                $financerevision->user_id = $user_id;
                $financerevision->office_id = $of_id;
                $financerevision->product_id = $pi_id;
                $financerevision->save();

                UserOtherTypeBarcodeScans::where('user_id', '=', \Auth::user()->id)->delete();
            }
//            \DB::commit();
            if (is_null($products)) {
                return collect([
                    'message' => 'This seat is not available.!! Please select other Seat',
                    'status' => 'error',
                ]);
            } elseif (!isset($transaction)) {
                return collect([
                    'message' => 'Error in saving data for seat in Database. TIMER IS NOT RUN! Please try again',
                    'status' => 'error',
                ]);
            } else {
                $icon = '';
                if ($products->productcategory->name === '1. Hot Desk') {
                    $icon = asset($products->productcategory->image);
                } else if ($products->productcategory->name === '2. Dedicated Desk') {
                    $icon = asset($products->productcategory->image);
                } else if ($products->productcategory->name === '3. Meeting Room') {
                    $icon = asset($products->productcategory->image);
                } else if ($products->productcategory->name === '4. Vending Machine') {
                    $icon = asset($products->productcategory->image);
                } else if ($products->productcategory->name === '5. Coffee Machine') {
                    $icon = asset($products->productcategory->image);
                } else if ($products->productcategory->name === '6. VR Room') {
                    $icon = asset($products->productcategory->image);
                } else if ($products->productcategory->name === '7. Door') {
                    $icon = asset($products->productcategory->image);
                } else if ($products->productcategory->name === '8. Phone Room') {
                    $icon = asset($products->productcategory->image);
                } else if ($products->productcategory->name === '9. Dining Room') {
                    $icon = asset($products->productcategory->image);
                } else if ($products->productcategory->name === '10. Table') {
                    $icon = asset($products->productcategory->image);
                } else if ($products->productcategory->name === '11. Wall 1') {
                    $icon = asset($products->productcategory->image);
                }
                return collect([
                    'status' => 'success',
                    'message' => 'Time ON. Successfully Start renting seat',
                    'seat_check' => $seatValidation,
                    'icon' => $icon,
                    'scan_type' => $request->scan_type,
                    'data' => [
                        'transaction_id' => $transaction->id,
                        'office_id' => $products->office_id,
                        'user_id' => $user_id,
                        'user' => isset($user->real_name) ? $user->real_name : '',
                        'type' => $products->productcategory->name,
                        'Position' =>
                        $position[] = [
                            'x' => $products->x,
                            'y' => $products->y,
                        ],
                        $products->productcategory->name =>
                        $seats[] = [
                            'id' => "seat:" . $products->id,
                        ],
                        'start_time' => \Carbon\Carbon::now(),
                    ]
                ]);
            }
            //            } else {
            //                return collect([
            //                    'message' => 'Your Account is not verified please verify your account first!!',
            //                    'status' => 'error',
            //                ]);
            //
            //            }
//        } catch (\Exception $e) {
//            \DB::rollBack();
//            return collect([
//                'message' => $e->getMessage() . ' at ' . $e->getLine() . ' at ' . $e->getFile(),
//                'status' => 'error',
//            ]);
//        }
    }

    public function checkUserAlreadyCheckIn($user_id)
    {
        $officeFinance = SharedOfficeFinance::where('user_id','=',$user_id)->where('end_time','=',null)->first();
        if(is_null($officeFinance)) {
            return collect([
                'status' => false,
                'timer' => 'Timer Closed'
            ]);
        } else {
            return collect([
                'status' => true,
                'timer' => 'Timer Already Started'
            ]);
        }
    }

    /**************************************  END   API 3   ***********************************************/

    /********************************************************************************************************/
    /******************                                API 4                                   **************/
    /******************           Pick Seat CLICKING ON BUTTON inside APP  , START TIMER       **************/
    /******************             http://www.xenren.co/api/sharedofficeClickBtSeat           **************/
    /******************             http://localhost:8000/api/sharedofficeClickBtSeat/         **************/
    /******************                  POST METHOD user_id, seat_id,                         **************/


    public function sharedofficeClickBtSeat(Request $request)
    {
        $data = [];
        // POST method

        $user_id = $request->get('user_id');
        $seat_id = $request->get('seat_id');
        $userValidation = $this->CheckUserIDVerification($user_id);   // function return(true or false)

        if ($userValidation) {
            $user = UserIdentity::where('user_id', '=', $user_id)
                ->first();
            $products = SharedOfficeProducts::where('id', '=', $seat_id)
                ->where('status_id', '=', '3') //seat must be free
                ->first();

            if ($products) {
                $of_id = $products->office_id;
                $pi_id = $products->id;
                $products->status_id = "1";  // put seat in USING-> 1
                $products->save();           // put status 1 in database, so seat from now is in USING

                $transaction = new SharedOfficeFinance();
                $transaction->start_time = \Carbon\Carbon::now(); //start TIMER
                $transaction->user = $user->real_name;
                $transaction->x = $products->x;
                $transaction->y = $products->y;
                $transaction->user_id = $user_id;
                $transaction->office_id = $of_id;
                $transaction->product_id = $pi_id;
                $transaction->save();

                //also save in revision as zero revision orginal
                $financerevision = new SharedOfficeFinanceRevision();
                $financerevision->transaction_id = $transaction->id;
                $financerevision->x = $products->x;
                $financerevision->y = $products->y;
                $financerevision->start_time = \Carbon\Carbon::now();
                $financerevision->rev = 0;
                $financerevision->user_id = $user_id;
                $financerevision->office_id = $of_id;
                $financerevision->product_id = $pi_id;
                $financerevision->save();
            }
            if (!isset($products)) {
                $data[] = [
                    'msg' => 'This seat is not available.!! Please select other Seat',
                    'success' => false,
                ];
                return response()->json($data);
            } elseif (!isset($transaction)) {
                $data[] = [
                    'msg' => 'Error in saving data for seat in Database. TIMER IS NOT RUN! Please try again',
                    'success' => false,
                ];
                return response()->json($data);
            } else {
                $data[] = [
                    'msg' => 'Time ON. Successfully Start renting product',
                    'transaction_id' => $transaction->id,
                    'office_id' => $products->office_id,
                    'user_id' => $user_id,
                    'user' => $user->real_name,
                    'type' => $products->productcategory->name,
                    'Position' =>
                    $position[] = [
                        'x' => $products->x,
                        'y' => $products->y,
                    ],
                    $products->productcategory->name =>
                    $seats[] = [
                        'id' => $products->id,
                    ],
                    'start_time' => \Carbon\Carbon::now(),
                    'success' => true,
                ];
                return response()->json($data);
            }
        } else {
            $data[] = [
                'msg' => 'User_id FAIL. You MUST first verify your accont',
                'success' => false,
            ];
            return response()->json($data);
        }
    }

    /**************************************  END   API 4  ***************************************************/



    /********************************************************************************************************/
    /******************                                API 4.2 revision 2                      **************/
    /******************   Pick Seat CLICKING ON BUTTON inside APP  , START TRACKER TIMER       **************/
    /******************             http://www.xenren.co/api/sharedofficeClickBtSeat           **************/
    /******************             http://localhost:8000/api/sharedofficeClickBtSeat/         **************/
    /******************                  POST METHOD user_id, seat_id,                         **************/
    /*                         http://www.xenren.co/api/sharedofficeStartTimer                              */
    /*                         http://localhost:8000/api/sharedofficeStartTimer/                            */
    public function sharedofficeStartTimer(Request $request)
    {
        $data = [];
        /* POST method */

        $user_id = $request->get('user_id');
        $seat_id = $request->get('seat_id');
        $userValidation = $this->CheckUserIDVerification($user_id);   // function return(true or false)

        if ($userValidation) {
            $user = UserIdentity::where('user_id', '=', $user_id)
                ->first();
            $products = SharedOfficeProducts::where('id', '=', $seat_id)
                ->where('status_id', '=', '3') //seat must be free
                ->first();

            if ($products) {
                $of_id = $products->office_id;
                $pi_id = $products->id;
                $products->status_id = "1";  // put seat in USING-> 1
                $products->save();           // put status 1 in database, so seat from now is in USING

                $transaction = new SharedOfficeFinance();
                $transaction->start_time = \Carbon\Carbon::now(); /*start TIMER */
                $transaction->user = $user->real_name;
                $transaction->x = $products->x;
                $transaction->y = $products->y;
                $transaction->user_id = $user_id;
                $transaction->office_id = $of_id;
                $transaction->product_id = $pi_id;
                $transaction->save();

                /*also save in revision as zero revision orginal*/
                $financerevision = new SharedOfficeFinanceRevision();
                $financerevision->transaction_id = $transaction->id;
                $financerevision->x = $products->x;
                $financerevision->y = $products->y;
                $financerevision->start_time = \Carbon\Carbon::now();
                $financerevision->rev = 0;
                $financerevision->user_id = $user_id;
                $financerevision->office_id = $of_id;
                $financerevision->product_id = $pi_id;
                $financerevision->save();
            }
            if (!isset($products)) {
                $data[] = [
                    'msg' => 'This seat is not available.!! Please select other Seat',
                    'success' => false,
                ];
                return response()->json($data);
            } elseif (!isset($transaction)) {
                $data[] = [
                    'msg' => 'Error in saving data for seat in Database. TIMER IS NOT RUN! Please try again',
                    'success' => false,
                ];
                return response()->json($data);
            } else {
                $data[] = [
                    'action' => 'Start-tracker',
                    'transaction_id' => $transaction->id,
                    'office_id' => $products->office_id,
                    'user_id' => $user_id,
                    'user' => $user->real_name,
                    'type' => $products->productcategory->name,
                    'Position' =>
                    $position[] = [
                        'x' => $products->x,
                        'y' => $products->y,
                    ],
                    $products->productcategory->name =>
                    $seats[] = [
                        'id' => $products->id,
                    ],
                    //'start_time'=>\Carbon\Carbon::now(),
                    'status' => 'success',
                    'data' =>
                    $time_tracker[] = [
                        'time' => \Carbon\Carbon::now(),
                    ]

                ];
                return response()->json($data);
            }
        } else {
            $data[] = [
                'msg' => 'User_id FAIL. You MUST first verify your accont',
                'status' => false,
            ];
            return response()->json($data);
        }
    }
    /**************************************  END   API 4.2 revision 2  ****************************************/





    /***********************************************************************************************************/
    /******************                                API 5                                      **************/
    /******************                    Pick Seat CLICKING ON BUTTON inside APP                **************/
    /******************                       endpoint to get the product info                    **************/
    /******************             http://www.xenren.co/api/sharedofficeClickBtSeatInfo          **************/
    /******************             http://localhost:8000/api/sharedofficeClickBtSeatInfo/        **************/
    /******************                  POST METHOD user_id, transaction_id                      **************/


    public function sharedofficeClickBtSeatInfo(Request $request)
    {
        $data = [];
        /* POST method */
        $user_id = $request->get('user_id');
        $transaction_id = $request->get('transaction_id');
        $userValidation = $this->CheckUserIDVerification($user_id);   // function return(true or false)

        if ($userValidation) {
            $user = UserIdentity::where('user_id', '=', $user_id)
                ->first();

            $transaction = SharedOfficeFinance::where('id', '=', $transaction_id)
                ->first();

            if ($transaction) {
                $products = SharedOfficeProducts::where('id', '=', $transaction->product_id)
                    //  ->where('status_id', '=', '1')//seat must be in USING
                    ->first();
                // if ($products) {
                $language = Session::get('lang');
                $sharedofficefinance = SharedOfficeFinance::where('product_id', '=', $products->id)->first();

                if ($sharedofficefinance->end_time == "0000-00-00 00:00:00") {
                    $minForPayment = \Carbon\Carbon::parse($sharedofficefinance->start_time)->diffInMinutes(\Carbon\Carbon::now());
                } else {
                    $minForPayment = \Carbon\Carbon::parse($sharedofficefinance->start_time)->diffInMinutes(\Carbon\Carbon::parse($sharedofficefinance->end_time));
                }

                /*******/
                if ($language == "en") {
                    $category_name = $sharedofficefinance->productfinance->productcategory->name;
                } elseif ($language == "cn") {
                    $category_name = $sharedofficefinance->productfinance->productcategory->name_ch;
                } else {
                    $category_name = $sharedofficefinance->productfinance->productcategory->name;
                }

                /*******/
                if ($sharedofficefinance->end_time == "0000-00-00 00:00:00") {
                    $cost = \Carbon\Carbon::parse($sharedofficefinance->start_time)->diffInMinutes(\Carbon\Carbon::now());
                    $cost = ($sharedofficefinance->productfinance->time_price * $cost / 10); //price is on 10 minute
                } else {
                    $cost = \Carbon\Carbon::parse($sharedofficefinance->start_time)->diffInMinutes(\Carbon\Carbon::parse($sharedofficefinance->end_time));
                    $cost = ($sharedofficefinance->productfinance->time_price * $cost / 10); //price is on 10 minute
                }

                /*******/
                if ($language == "en") {
                    $status = $sharedofficefinance->productfinance->productstatus->status;
                } elseif ($language == "cn") {
                    $status = $sharedofficefinance->productfinance->productstatus->status_ch;
                } else {
                    $status = $sharedofficefinance->productfinance->productstatus->status;
                }

                /*******/
                if ($sharedofficefinance->cost != "") {
                    $paid = "yes";
                } else {
                    $paid = "no";
                }
                //  }
            }

            if (!isset($transaction)) {
                $data[] = [
                    'msg' => 'There is no transaction id in database??',
                    'success' => false,
                ];
                return response()->json($data);
            }
            /*
            elseif (!isset($products)) {
                $data[] = [
                    'msg'=>'This seat is free. Can not get any info for seat which is not in USING!! Please select other Seat',
                    'success' => false,
                ];
                return response()->json($data);
            }
            */ else {
                $data[] = [
                    'transaction_id' => $sharedofficefinance->id,
                    'user_id' => $user_id,
                    'user' => $user->real_name,
                    'office_id' => $sharedofficefinance->office_id,
                    'office_name' => $sharedofficefinance->productfinance->office->office_name,
                    'category_id' => $sharedofficefinance->productfinance->productcategory->id,
                    'type' => $products->productcategory->name,
                    'product_id' => $sharedofficefinance->product_id,
                    'Position' =>
                    $position[] = [
                        'x' => $products->x,
                        'y' => $products->y,
                    ],
                    $products->productcategory->name =>
                    $seats[] = [
                        'id' => $products->id,
                    ],
                    'number' => $sharedofficefinance->productfinance->number,
                    'start_time' => $sharedofficefinance->start_time,
                    'end_time' => $sharedofficefinance->end_time,
                    'time_price' => $sharedofficefinance->productfinance->time_price,
                    'spendMinutes' => $minForPayment,
                    //'status_finance' =>$sharedofficefinance->status_finance,
                    'currentTime' => \Carbon\Carbon::now(),
                    'cost' => $cost,
                    'user' => $sharedofficefinance->user,
                    'status' => $status,
                    'paid' => $paid
                ];
                return response()->json($data);
            }
        } else {
            $data[] = [
                'msg' => 'User_id FAIL. You MUST first verify your accont',
                'success' => false,
            ];
            return response()->json($data);
        }
    }
    /**************************************  END   API 5  ******************************************************/



    /***********************************************************************************************************/
    /******************                                API 5.1                                    **************/
    /******************                         Change Seat by Qr code                            **************/
    /******************           http://www.xenren.co/api/sharedofficeChangeSeatByQR             **************/
    /******************          http://localhost:8000/api/sharedofficeChangeSeatByQR/            **************/
    /******************       POST METHOD user_id, QR CODE NEW CHAIR, transaction_id(MUST)        **************/

    public function sharedofficeChangeSeatByQR(Request $request)
    {
        $seatNumber = $request->qr_seat;
        $officeNumber = $request->office_id;

        $products = SharedOfficeProducts::where('number', '=', $seatNumber)
            ->where('status_id', '=', '3') //seat must be free
            ->where('office_id', '=', $officeNumber)
            ->first();

        \Log::info("stop timer");
        if (!is_null($products)) {
            $stop_time = self::sharedofficeStopTimer($request, false, false);
            if ($stop_time['status'] == 'success') {
                $seat_change = self::sharedofficeQrSeat($request);
                if ($seat_change['status'] == 'success') {
                    return collect([
                        'status' => 'success',
                        'data' => $seat_change,
                        'message' => 'Time ON. Successfully Start renting seat',
                    ]);
                } else {
                    return collect([
                        'status' => 'error',
                        'message' => $seat_change['message']
                    ]);
                }
            } else {
                return collect([
                    'status' => 'error',
                    'message' => $stop_time['message']
                ]);
            }
        } else {
            return collect([
                'status' => 'error',
                'message' => 'No seat found, or seat already taken!'
            ]);
        }
    }


    public function xsharedofficeChangeSeatByQR(Request $request)
    {
        try {
            $data = [];
            /* POST method */
            $user_id = \Auth::user()->id;
            $office_id = $request->office_id;
            $number = $request->new_seat_number;
            $userValidation = $this->CheckUserIDVerification($user_id);   // function return(true or false)

            if ($userValidation) {
                $user = UserIdentity::where('user_id', '=', $user_id)
                    ->first();
                $products = SharedOfficeProducts::where('number', '=', $number)
                    ->where('status_id', '=', '3') //seat must be free
                    ->first();
                if (is_null($products)) {
                    return collect([
                        'status' => 'error',
                        'message' => 'No seat found, or seat already taken!'
                    ]);
                }

                /**************************************************************************************/
                /*find old_seat_id and update status  */

                $oldtransaction = SharedOfficeFinance::where('user_id', '=', $user_id)->where('end_time', '=', null)->first();
                if ($products->id == $oldtransaction->product_id) {
                    return collect([
                        'status' => 'error',
                        'product' => $products,
                        'old_seat' => $oldtransaction,
                        'message' => 'you already using this seat!'
                    ]);
                }
                $oldproducts = SharedOfficeProducts::where('id', '=', $oldtransaction->product_id)
                    ->first();
                //                if($oldproducts) {
                //                    try {
                //                    $timeCost = $this->TimeAndCostCalculation($oldtransaction);
                //
                //                    $this->SavingCostAndTime($oldtransaction, $timeCost);
                //
                //                    $balance = $this->UpdateUserBalance($oldtransaction->id, $oldtransaction->user_id, $timeCost);
                //
                //                    $this->TransactionTableEntery($oldtransaction, $balance, $timeCost);
                //
                ////                    $model2 = $this->UpdatingSharedOfficeProductStatus($oldtransaction->product_id);
                //
                //                    $this->UpdatingSharedOfficeFinanceRevision($oldtransaction->id, $oldtransaction->product_id, $timeCost);
                //                    } catch (\Exception $e) {
                //                        return collect([
                //                            'status' => 'error',
                //                            'message' => $e->getMessage(). 'at' .$e->getLine()
                //                        ]);
                //                    }
                //                 } else {
                //                    return collect([
                //                        'status' => 'error',
                //                        'message' => 'Product not Found'
                //                    ]);
                //                }


                $new_pr = SharedOfficeProducts::where('number', '=', $number)
                    ->first();
                \DB::beginTransaction();
                if ($oldtransaction) {
                    $oldtransaction->product_id = $new_pr->id;  // put new seat in old transaction
                    $oldtransaction->save();
                }

                if ($oldproducts) {
                    $oldproducts->status_id = "3";  // put old seat now Free, after you take new seat
                    $oldproducts->save();
                }

                /*also save in tbl revision this changes to have history*/
                $max = SharedOfficeFinanceRevision::where('transaction_id', '=', $oldtransaction->id)->max('rev');

                $financerevision = new SharedOfficeFinanceRevision();
                $financerevision->transaction_id = $oldtransaction->id;
                $financerevision->x = $products->x;
                $financerevision->y = $products->y;
                $financerevision->start_time = \Carbon\Carbon::now();
                $financerevision->rev = $max + 1;
                $financerevision->user_id = $user_id;
                $financerevision->office_id = $products->office_id;
                $financerevision->product_id = $products->id;
                $financerevision->save();

                if ($products) {
                    $products->status_id = "1";  // put seat in USING-> 1
                    $products->save();           // put status 1 in database, so seat from now is in USING
                }
                \DB::commit();
                /****************************************************************************************/

                $data = collect([
                    'status' => 'success',
                    'message' => 'Timer Active. Successfully Change Seat',
                    'data' => collect([
                        'transaction_id' => $oldtransaction->id,
                        'office_id' => $products->office_id,
                        'seat_id' => $new_pr->id,
                        'user_id' => $user_id,
                        'user' => isset($user->real_name) ? $user->real_name : '',
                        'start_time' => $oldtransaction->start_time
                    ])
                ]);
                return response()->json($data);
            } else {
                $data[] = [
                    'messgage' => 'Your Account is not verified please verify your first!!',
                    'success' => false,
                ];
                return response()->json($data);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e->getMessage();
        }
    }
    /**************************************  END   API 5.1 ******************************************************/


    /********************************************************************************************************/
    /******************                                API 6                                   **************/
    /******************                              STOP TRACKER TIMER                        **************/
    /******************             http://www.xenren.co/api/sharedofficeStopTimer             **************/
    /******************             http://localhost:8000/api/sharedofficeStopTimer/           **************/
    /******************                  POST METHOD user_id, transaction_id                   **************/

    public function sharedofficeStopTimer(Request $request, $customProduct = false, $customUserId = false)
    {
        /* POST method */
        $user_id = $customUserId ? $customUserId : \Auth::user()->id;
        //        $userValidation = $this->CheckUserIDVerification($user_id);   // function return(true or false)
        //        if ($userValidation) {
        $sharedofficefinance = SharedOfficeFinance::with('office')->where('user_id', '=', $user_id)->where('end_time', '=', null)->first();
        if (!isset($sharedofficefinance) || is_null($sharedofficefinance)) {
            return collect([
                'status' => 'error',
                'message' => 'No active seat found for this user!'
            ]);
        }
        if ($customProduct) {
            $products = $customProduct;
        } else {
            $products = SharedOfficeProducts::where('id', '=', $sharedofficefinance->product_id)
                ->where('status_id', '=', '1') //seat should not be free, must be 1
                ->first();
        }
        $time_price = 0;
        if ($products) {
            //                Time and Cost Calculation

            $timeCost = $this->TimeAndCostCalculation($sharedofficefinance);
            $time_price = $timeCost;

            //                Saving cost and end time in Shared Office Finance Table

            $this->SavingCostAndTime($sharedofficefinance, $timeCost);


            //                Update User Balance
            $balance = $this->UpdateUserBalance($sharedofficefinance->id, $sharedofficefinance->user_id, $timeCost);

            //                Updating Shared Office Product status to 3 --> empty

            $model2 = $this->UpdatingSharedOfficeProductStatus($sharedofficefinance->product_id);

            //                Updating Shared Office Finance Revision table adding end time

            $this->UpdatingSharedOfficeFinanceRevision($sharedofficefinance->id, $sharedofficefinance->product_id, $timeCost);

            //               Create entery in the Transaction Table

            $this->TransactionTableEntery($sharedofficefinance, $balance, $timeCost);
        }
        if (!isset($products)) {
            return collect([
                'message' => 'This seat is not in using.!! You can not use stop time.',
                'status' => 'error',
            ]);
            //                return response()->json($data[0]);
        } elseif (!isset($sharedofficefinance)) {
            return collect([
                'message' => 'Fail saving data in SharedOfficeFinance',
                'status' => 'error',
            ]);
            //                return response()->json($data[0]);
        } elseif (!isset($model2)) {
            return collect([
                'message' => 'Fail saving data in SharedOfficeProduct',
                'status' => 'error',
            ]);
            //                return response()->json($data[0]);
        } else {
            return collect([
                'status' => 'success',
                'message' => 'Successfully payment for user',
                'data' =>
                $time_tracker[] = [
                    'time' => \Carbon\Carbon::now(),
                ],
                'timeCost' => $time_price
            ]);
            //                return response()->json($data[0]);
        }
        //        } else {
        //            return collect([
        //                'msg' => 'User_id FAIL. You MUST first verify your accont',
        //                'status' => false,
        //            ]);
        ////            return response()->json($data[0]);
        //        }
    }


    public function TimeAndCostCalculation($sharedofficefinance)
    {
        $minutes = \Carbon\Carbon::parse($sharedofficefinance->start_time)->diffInMinutes(\Carbon\Carbon::now());
        $value = $minutes / 10;
        $total_time = ceil($value);
        $totalCost = $sharedofficefinance->productfinance->time_price * $total_time; //price is on 10 minute
        $endTime = \Carbon\Carbon::now();
        return collect([
            'minutes' => $minutes,
            'totalCost' => $totalCost,
            'endTime' => $endTime,
        ]);
    }


    public function sharedofficeGetCost()
    {
        try {
            $user_id = \Auth::user()->id;
            $sharedofficefinance = SharedOfficeFinance::where('user_id', '=', $user_id)->where('end_time', '=', null)->with('productfinance')->first();
            if (!isset($sharedofficefinance) || is_null($sharedofficefinance)) {
                return collect([
                    'status' => 'error',
                    'message' => 'No active seat found for this user!',
                    'user' => \Auth::user(),
                ]);
            }
            $products = SharedOfficeProducts::where('id', '=', $sharedofficefinance->product_id)
                ->where('status_id', '=', '1') //seat should not be free, must be 1
                ->first();
            $time_price = isset($sharedofficefinance->productfinance) && isset($sharedofficefinance->productfinance->time_price) ? $sharedofficefinance->productfinance->time_price : 0;
            if (!is_null($products)) {
                $minutes = \Carbon\Carbon::parse($sharedofficefinance->start_time)->diffInMinutes(\Carbon\Carbon::now());
                $value = $minutes / 10;
                $total_time = ceil($value);
                $totalCost = $time_price * $total_time; //price is on 10 minute
                return collect([
                    'status' => 'success',
                    'minutes' => $minutes . ' MIN',
                    'totalCost' => $totalCost,
                    'seatNumber' => isset($products->number) ? $products->number : '',
                    'user' => \Auth::user(),
                    'value' => $value,
                ]);
            } else {
                return collect([
                    'status' => 'error',
                    'message' => 'Product no found..',
                    'user' => \Auth::user(),
                    'id' => $sharedofficefinance->product_id,
                ]);
            }
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => 'Network Error.. Please try later!!',
                'technical_message' => $e->getMessage() . ' at ' . $e->getFile() . ' at ' . $e->getLine()
            ]);
        }
    }

    public function SavingCostAndTime($sharedofficefinance, $timeCost)
    {
        try {
            \DB::beginTransaction();
            $sharedofficefinance->cost = $timeCost['totalCost'];
            $sharedofficefinance->end_time = $timeCost['endTime'];
            $sharedofficefinance->save();
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            return $e->getMessage();
        }
    }

    public function UpdateUserBalance($id, $user_id, $timeCost)
    {
        try {
            $sharedOfficeFinance = SharedOfficeFinance::find($id);
            $user = User::find($user_id);
            \DB::beginTransaction();
            $balanceBefore = $user->account_balance;
            if($user->account_balance > 0) {
                $user->account_balance = $user->account_balance - $timeCost['totalCost'];
            }
            $afterBalance = $user->account_balance;
            $user->save();
            \DB::commit();
            return collect([
                'balanceBefore' => $balanceBefore,
                'afterBalance' => $afterBalance,
            ]);
        } catch (\Exception $e) {
            \DB::rollback();
            return $e->getMessage();
        }
    }

    public function UpdatingSharedOfficeProductStatus($product_id)
    {
        try {
            $model2 = SharedOfficeProducts::find($product_id);
            \DB::beginTransaction();
            $model2->status_id = "3";  // 3-- empty
            $model2->save();
            \DB::commit();
            return $model2;
        } catch (\Exception $e) {
            \DB::rollback();
            return $e->getMessage();
        }
    }

    public function UpdatingSharedOfficeFinanceRevision($id, $product_id, $timeCost)
    {
        try {
            $model_revision = SharedOfficeFinanceRevision::where('transaction_id', '=', $id)
                ->where('product_id', '=', $product_id)
                ->orderBy('rev', 'desc')
                ->first();
            \DB::beginTransaction();
            $model_revision->end_time = $timeCost['endTime'];
            $model_revision->save();
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            return $e->getMessage();
        }
    }

    public function TransactionTableEntery($finance, $balance, $timeCost)
    {
        \Log::info($finance->office->staff_id);
        $staff = Staff::where('id','=',$finance->office->staff_id)->first();
        $staffAfterBalance = $staff->balance + $timeCost['totalCost'];
        \Log::info($staff->balance);
        \Log::info($staffAfterBalance);
        try {
            $transaction = new Transaction();

            \DB::beginTransaction();
            $transaction->name = 'SEAT TRANSACTION: ' . $finance->product_id . ' || FINANCE ID: ' . $finance->id;
            $transaction->amount = $timeCost['totalCost'];
            $transaction->due_date = carbon::now();
            $transaction->type = Transaction::TYPE_SHARED_OFFICE_ACCOUNT_BALANCE;
            $transaction->status = Transaction::STATUS_APPROVED;

            $transaction->comment = ' USER ID: ' . $finance->user_id . ' || FINANCE ID: ' . $finance->id . ' || START TIME: ' . $finance->start_time . ' || TOTAL MINUTES: ' . $timeCost['minutes'] . ' || BEFORE BALANCE: ' . $staff->balance . ' || TOTAL ACCOUNT BALANCE: ' . $staffAfterBalance;

            $transaction->balance_before = $staff->balance;
            $transaction->balance_after = $staffAfterBalance;
            $transaction->performed_by = $finance->user_id;
            $transaction->performed_to = $finance->office->staff_id;
            $transaction->currency = 0;
            $transaction->save();
            $this->AddBalanceToSharedOfficeOwner($finance, $staffAfterBalance);
            $this->TransactionTableEntryServiceFeeDeduction($finance, $timeCost ,$staffAfterBalance);

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            return $e->getMessage();
        }
    }

    public function AddBalanceToSharedOfficeOwner($finance, $staffAfterBalance)
    {
        try {
            Staff::where('id','=',$finance->office->staff_id)->update([
            'balance' => $staffAfterBalance
             ]);
        } catch (\Exception $e) {
            \DB::rollback();
            return $e->getMessage();
        }
    }

    public function TransactionTableEntryServiceFeeDeduction($finance, $timeCost , $staffAfterBalance)
    {
        $amountServiceFee = ($finance->cost*1.68)/100;
        \Log::info($amountServiceFee);
        $admin = Staff::where('id','=',1)->first();
        $afterAdminBalance = $admin->balance + $amountServiceFee;
        \Log::info($afterAdminBalance);
        try {
            $transaction = new Transaction();

            \DB::beginTransaction();
            $transaction->name = 'SEAT TRANSACTION: ' . $finance->product_id . ' || FINANCE ID: ' . $finance->id;
            $transaction->amount = $amountServiceFee;
            $transaction->due_date = carbon::now();
            $transaction->type = Transaction::TYPE_SERVICE_FEE;
            $transaction->status = Transaction::STATUS_APPROVED;

            $transaction->comment = ' USER ID: ' . Auth::user()->id . ' || FINANCE ID: ' . $finance->user_id . ' || START TIME: ' . $finance->start_time . ' || TOTAL MINUTES: ' . $timeCost['minutes'] . ' || BEFORE BALANCE: ' . $admin->balance . ' || TOTAL ACCOUNT BALANCE: ' . $afterAdminBalance;

            $transaction->balance_before = $admin->balance;
            $transaction->balance_after = $afterAdminBalance;
            $transaction->performed_by = $finance->office->staff_id;
            $transaction->performed_to = $admin->id;
            $transaction->currency = 0;
            $transaction->save();
            $this->deductPercentageAmountFromSharedOfficeOwner($finance , $amountServiceFee ,$staffAfterBalance);
            $this->getPercentageAmountFromSharedOfficeOwner($afterAdminBalance);
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            return $e->getMessage();
        }
    }

    public function deductPercentageAmountFromSharedOfficeOwner($finance , $amountServiceFee , $staffAfterBalance)
    {
        try {
            Staff::where('id','=',$finance->office->staff_id)->update([
                'balance' => $staffAfterBalance - $amountServiceFee
            ]);
        } catch (\Exception $e) {
            \DB::rollback();
            return $e->getMessage();
        }
    }

    public function getPercentageAmountFromSharedOfficeOwner($afterAdminBalance)
    {
        try {
            $updateAdminBalance = Staff::where('id','=',1)->update([
               'balance' => $afterAdminBalance
            ]);
            \Log::info($updateAdminBalance);
            return $updateAdminBalance;
        } catch (\Exception $e) {
            \DB::rollback();
            return $e->getMessage();
        }
    }

    /**************************************  END   API 6  ******************************************************/


    /********************************************************************************************************/
    /******************                                API 7                                   **************/
    /******************                      Finance detail for current transaction                **********/
    /******************             http://www.xenren.co/api/sharedofficeFinanceCurrentTransaction   ********/
    /******************             http://localhost:8000/api/sharedofficeFinanceCurrentTransaction/   ******/
    /******************                  POST METHOD user_id, transaction_id                   **************/


    public function sharedofficeFinanceCurrentTransaction(Request $request)
    {
        $data = [];
        /* POST method */
        $user_id = $request->get('user_id');
        $transaction_id = $request->get('transaction_id');

        $userValidation = $this->CheckUserIDVerification($user_id);   // function return(true or false)

        if ($userValidation) {
            $user = UserIdentity::where('user_id', '=', $user_id)
                ->first();
            $sharedofficefinance = SharedOfficeFinance::where('id', '=', $transaction_id)->first();

            if ($sharedofficefinance) {
                $data[] = [
                    'action' => 'Finance detail for current transaction',
                    'transaction_id' => $transaction_id,
                    'office_id' => $sharedofficefinance->office_id,
                    'user_id' => $user_id,
                    'user' => $user->real_name,
                    'product_id' => $sharedofficefinance->product_id,
                    'Position' =>
                    $position[] = [
                        'x' => $sharedofficefinance->x,
                        'y' => $sharedofficefinance->y,
                    ],
                    'start_time' => $sharedofficefinance->start_time,
                    'end_time' => $sharedofficefinance->end_time,
                    'cost' => $sharedofficefinance->cost,
                    'status_finance' => $sharedofficefinance->status_finance,
                    'status' => 'success',

                ];
                return response()->json($data);
            }
        } else {
            $data[] = [
                'msg' => 'User_id FAIL. You MUST first verify your accont',
                'status' => false,
            ];
            return response()->json($data);
        }
    }


    /********************************************** API 8 JWT TOKEN  TO SEE FOR TESTING****************************/
    public function loginJWTTokenOffice(Request $request)
    {
        /* POST method */
        $credentials = $request->only('email', 'password');
        $token = null;
        //dd(JWTAuth::attempt($credentials));
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        $data = [];
        $data[] = [
            'msg' => 'salji i user_id, transaction_id',
            'status' => compact('token'),

        ];
        // return response()->json($data);
        //$a=234;
        return response()->json(compact('token'));
        // return response()->json([compact('token'),"user_id:",$a]);

    }































    /************************************  PHOTO AUTHENTICATION  ******************************************/
    public function indexVerifyPhoto(Request $request)
    {
        /* return all office
           http://localhost:8000/api/sharedofficePhoto/
           POSTMAN USE METHOD POST,
           Post KEY:Token
           Content-Type:application/json
        */
        /*test
        token:136_nGCejBXy8S8YWKwTa4vp9dt2Z47HfhDT
        */

        $data = [];
        $token = $request->get('token');
        $userIdentityToken = UserToken::where('token', '=', $token)
            ->first();

        if (isset($userIdentityToken)) {

            /*Check Photo authentication */

            $userIdentityPhoto = UserIdentity::where('user_id', '=', $userIdentityToken->user_id)
                ->where('status', '=', 1)
                ->first();

            if (isset($userIdentityPhoto)) {
                $sharedoffice = SharedOffice::all();
                foreach ($sharedoffice as $shar) {
                    $data[] = [
                        'id' => $shar->id,
                        'office_name' => $shar->office_name,
                        'location' => $shar->location,
                        'description' => $shar->description,
                        'image' => $shar->image,
                        'pic_size' => $shar->pic_size,
                        'qr' => $shar->qr,
                        'staff_id' => $shar->staff_id,
                        'created_at' => $shar->created_at,
                        'updated_at' => $shar->updated_at,
                        'success' => false,
                        'error' => "OFFICE VIEW: PHOTO VALIDATE:PASS -- can see all offices, can see qr BUT can't use start timer in this step anyway"
                    ];
                }
                return response()->json($data);
            } else {

                /* photo fail in authentication , can not see QR, can not start timer*/
                $sharedoffice = SharedOffice::all();
                foreach ($sharedoffice as $shar) {
                    $data[] = [
                        'id' => $shar->id,
                        'office_name' => $shar->office_name,
                        'location' => $shar->location,
                        'description' => $shar->description,
                        'image' => $shar->image,
                        'pic_size' => $shar->pic_size,
                        'staff_id' => $shar->staff_id,
                        'created_at' => $shar->created_at,
                        'updated_at' => $shar->updated_at,
                        'success' => false,
                        'error' => "OFFICE VIEW: Photo validate:FAIL -- can see all offices, BUT can't see qr and can't use start timer in this step anyway"
                    ];
                }
                return response()->json($data);
            }
        }
    }


    public function singleOfficeProductsVerifyPhoto(Request $request)
    {
        /*
      return all products for selected office_id
      POSTMAN USE METHOD POST,
      KEY:office_id
      KEY:Token
      http://localhost:8000/api/singleOfficeVerifyPhoto/products/
      Content-Type:application/json
     */

        /*test
       token:136_nGCejBXy8S8YWKwTa4vp9dt2Z47HfhDT
       */

        $data = [];
        $office_id = $request->get('office_id');
        $token = $request->get('token');
        $userIdentityToken = UserToken::where('token', '=', $token)
            ->first();


        if ($userIdentityToken) {
            /*status user PHOTO check */
            $userIdentityPhoto = UserIdentity::where('user_id', '=', $userIdentityToken->user_id)
                ->where('status', '=', '1')  //1 approved
                ->first();
            if (isset($userIdentityPhoto)) {
                /*can see products and QR , can start timer*/
                $sharedofficeproducts = SharedOfficeProducts::where('office_id', '=', $office_id)->get();
                foreach ($sharedofficeproducts as $shar) {
                    $data[] = [
                        'id' => $shar->id,
                        'office_id' => $shar->office_id,
                        'category_id' => $shar->category_id,
                        'number' => $shar->number,
                        'status_id' => $shar->status_id,
                        'month_price' => $shar->month_price,
                        'time_price' => $shar->time_price,
                        'remark' => $shar->remark,
                        'qr' => $shar->qr,
                        'created_at' => $shar->created_at,
                        'updated_at' => $shar->updated_at,
                        'type_user' => 'photo_pass',
                        'success' => false,
                        'error' => "PRODUCTS VIEW: Photo Valdidation:PASS - Can see products,QR,Can start renting"
                    ];
                }
                return response()->json($data);
            } else {

                /*Not pass Photo user */

                /*can see products , can  see QR, start not start timer*/
                $sharedofficeproducts = SharedOfficeProducts::where('office_id', '=', $office_id)->get();
                foreach ($sharedofficeproducts as $shar) {
                    $data[] = [
                        'id' => $shar->id,
                        'office_id' => $shar->office_id,
                        'category_id' => $shar->category_id,
                        'number' => $shar->number,
                        'status_id' => $shar->status_id,
                        'month_price' => $shar->month_price,
                        'time_price' => $shar->time_price,
                        'remark' => $shar->remark,
                        'qr' => $shar->qr,
                        'created_at' => $shar->created_at,
                        'updated_at' => $shar->updated_at,
                        'type_user' => 'foto_fail',
                        'success' => false,
                        'error' => "PRODUCTS VIEW: Photo Validation: NOT PASS - Can see products, can see  QR, can not start timer."
                    ];
                }
                return response()->json($data);
            }
        } else {
            /* Token validation:NOT PASS */
            $sharedofficeproducts = SharedOfficeProducts::where('office_id', '=', $office_id)->get();
            foreach ($sharedofficeproducts as $shar) {
                $data[] = [
                    'id' => $shar->id,
                    'office_id' => $shar->office_id,
                    'category_id' => $shar->category_id,
                    'number' => $shar->number,
                    'status_id' => $shar->status_id,
                    'month_price' => $shar->month_price,
                    'time_price' => $shar->time_price,
                    'remark' => $shar->remark,
                    'created_at' => $shar->created_at,
                    'updated_at' => $shar->updated_at,
                    'type_user' => 'unregistred',
                    'success' => false,
                    'error' => "PRODUCTS VIEW: Token Validation: NOT PASS - Can see products, can not see QR, can not start timer."
                ];
            }
            return response()->json($data);
        }
    }


    public function RentProductStartByPidPhoto($product_id, $token)
    {
        //test : localhost:8000/api/singleOffice/RentProductStartByPidPhoto/1198/token */  /*first argument PID, second token */

        /*test

         GET  token:136_nGCejBXy8S8YWKwTa4vp9dt2Z47HfhDT
            */

        $userIdentityToken = UserToken::where('token', '=', $token)
            ->first();

        if ($userIdentityToken) {

            $user = User::where('id', '=', $userIdentityToken->user_id)
                ->first();

            /*rent product by PID */
            $model2 = SharedOfficeProducts::find($product_id);
            $model2->status_id = "1";  // 1 -- using
            $of_id = $model2->office_id;
            $model2->save();

            $model = new SharedOfficeFinance();
            $model->start_time = \Carbon\Carbon::now();
            $model->user = $user->name;
            $model->office_id = $of_id;
            $model->product_id = $product_id;
            $model->save();

            if (!isset($model)) {
                $res = [
                    'success' => false,
                    'error' => "Fail saving data in SharedOfficeFinance"
                ];
                return response()->json($res, 200);
            }

            if (!isset($model2)) {
                $res = [
                    'success' => false,
                    'error' => "Fail saving data in SharedOfficeProduct"
                ];
                return response()->json($res, 200);
            }

            $res = [
                'success' => true,
                'result' => "Successfully rented product"
            ];

            return response()->json($res, 200);

            //test : localhost:8000/api/singleOffice/RentProductStartByPID/1198/alisa */  /*first argument PID, second user */
        } else {
            /* token is not good */
            $res = [
                'success' => false,
                'result' => "Renting fail. Token is not good !!!!"
            ];

            return response()->json($res, 200);
        }
    }


    public function RentProductStartByQrPhoto($qr_data, $token)
    {
        /*rent product by QR */
        /*localhost:8000/api/singleOffice/RentProductStartByQrPhoto/1658642797/136_nGCejBXy8S8YWKwTa4vp9dt2Z47HfhDT */  /*first argument QR_DATA, second token */

        /*test

         GET  token:136_nGCejBXy8S8YWKwTa4vp9dt2Z47HfhDT
         */

        $userIdentityToken = UserToken::where('token', '=', $token)
            ->first();

        if ($userIdentityToken) {

            $user = User::where('id', '=', $userIdentityToken->user_id)
                ->first();

            $model2 = SharedOfficeProducts::where('number', '=', $qr_data)->first(); //when you saving using WHERE MUST BE first() not GET
            if ($model2) {
                $of_id = $model2->office_id;
                $pi_id = $model2->id;
                $model2->status_id = "1";  // 1 -- using
                $model2->save();

                $model = new SharedOfficeFinance();

                $model->start_time = \Carbon\Carbon::now();
                $model->user = $user->name;
                $model->office_id = $of_id;
                $model->product_id = $pi_id;
                $model->save();
            }

            if (!isset($model)) {
                $res = [
                    'success' => false,
                    'error' => "Fail saving data in SharedOfficeFinance"
                ];
                return response()->json($res, 200);
            }

            if (!isset($model2)) {
                $res = [
                    'success' => false,
                    'error' => "Fail saving data in SharedOfficeProduct"
                ];
                return response()->json($res, 200);
            }

            $res = [
                'success' => true,
                'result' => "Successfully rented product"
            ];

            return response()->json($res, 200);
        } else {
            /*token   not  pass */
            $res = [
                'success' => false,
                'result' => "Renting fail. Token is not good !!!!"
            ];

            return response()->json($res, 200);
        }
    }





    /*************************  END OF PHOTO AUTHENTICATION ****************************************/

    public function singleOfficeProducts($office_id)
    {
        /*return products for specified office_id */
        /*test http://localhost:8000/api/singleOffice/products/11 */ /*parametar is offce_id , on example 11*/
        $sharedofficeproducts = SharedOfficeProducts::with('productcategory')->where('office_id', '=', $office_id)->get();
        return response()->json($sharedofficeproducts, 200);
    }


    public function singleOfficeProductsVerifyInfo(Request $request)
    {
        $data = [];
        /*verify id_card_no and real_name and holding_id_image*/
        $office_id = $request->get('office_id');  // it will  be provided from  app phone as hidden data input
        /* POSTMAN USE METHOD POST,
        KEY:office_id, VALUE:hidden id from app
        KEY:id_card_no, VALUE:use FROM DATABASE for testing
        KEY:real_name, VALUE:use FROM DATABASE for testing
        KEY:key, VALUE:use FROM DATABASE for testing

       Content-Type:application/json
       */

        /*status user INFO check */
        $id_card_no = $request->get('id_card_no');
        $real_name = $request->get('real_name');
        $userIdentity = UserIdentity::where('id_card_no', '=', $id_card_no)
            ->where('real_name', '=', $real_name)
            ->first();

        if (!isset($userIdentity)) {

            /*not pass INFO user  */

            /*CHECK is it PHOTO user */
            $key = $request->get('key');
            $userIdentity3 = UserIdentity::where('key', '=', $key)
                ->where('status', '=', '2')  //2 approved
                ->first();
            if (isset($userIdentity3)) {
                /*PHOTO USER : TRUE*/
                $sharedofficeproducts = SharedOfficeProducts::where('office_id', '=', $office_id)->get();
                foreach ($sharedofficeproducts as $shar) {
                    $data[] = [
                        'id' => $shar->id,
                        'office_id' => $shar->office_id,
                        'category_id' => $shar->category_id,
                        'number' => $shar->number,
                        'status_id' => $shar->status_id,
                        'month_price' => $shar->month_price,
                        'time_price' => $shar->time_price,
                        'remark' => $shar->remark,
                        'qr' => $shar->qr,
                        'created_at' => $shar->created_at,
                        'updated_at' => $shar->updated_at,
                        'type_user' => 'photo',
                        'success' => false,
                        'error' => "PRODUCTS VIEW: PHOTO USER  -- see products,qr, BUT  can't use start timer.Need USER login"
                    ];
                }
                return response()->json($data);
            } else {
                /*Unregistred User */
                $sharedofficeproducts = SharedOfficeProducts::where('office_id', '=', $office_id)->get();
                foreach ($sharedofficeproducts as $shar) {
                    $data[] = [
                        'id' => $shar->id,
                        'office_id' => $shar->office_id,
                        'category_id' => $shar->category_id,
                        'number' => $shar->number,
                        'status_id' => $shar->status_id,
                        'month_price' => $shar->month_price,
                        'time_price' => $shar->time_price,
                        'remark' => $shar->remark,
                        'created_at' => $shar->created_at,
                        'updated_at' => $shar->updated_at,
                        'type_user' => 'unregistred',
                        'success' => false,
                        'error' => "PRODUCTS VIEW: Unregistred USER  -- see products, BUT can't  see QR and can't use start timer"
                    ];
                }
                return response()->json($data);
            }
        } else {
            /*Info user*/

            /*Check is it and Photo user*/
            $userIdentity2 = UserIdentity::where('id_card_no', '=', $id_card_no)
                ->where('real_name', '=', $real_name)
                ->where('status', '=', '1')->first();   // 1 ---approved
            if (!isset($userIdentity2)) {
                /*Only Info user, go to Photo login form */
                /*can see products and QR , can not start timer*/
                $sharedofficeproducts = SharedOfficeProducts::where('office_id', '=', $office_id)->get();
                foreach ($sharedofficeproducts as $shar) {
                    $data[] = [
                        'id' => $shar->id,
                        'office_id' => $shar->office_id,
                        'category_id' => $shar->category_id,
                        'number' => $shar->number,
                        'status_id' => $shar->status_id,
                        'month_price' => $shar->month_price,
                        'time_price' => $shar->time_price,
                        'remark' => $shar->remark,
                        'qr' => $shar->qr,
                        'created_at' => $shar->created_at,
                        'updated_at' => $shar->updated_at,
                        'type_user' => 'info',
                        'success' => false,
                        'error' => "PRODUCTS VIEW: INFO USER-Can see products,QR, BUT can't use start timer.Need to PHOTO Login Form"
                    ];
                }
                return response()->json($data);
            } else {
                /*full registration */
                /*can see ALL products with QR and start timer*/
                $sharedofficeproducts = SharedOfficeProducts::where('office_id', '=', $office_id)->get();
                foreach ($sharedofficeproducts as $shar) {
                    $data[] = [
                        'id' => $shar->id,
                        'office_id' => $shar->office_id,
                        'category_id' => $shar->category_id,
                        'number' => $shar->number,
                        'status_id' => $shar->status_id,
                        'month_price' => $shar->month_price,
                        'time_price' => $shar->time_price,
                        'remark' => $shar->remark,
                        'qr' => $shar->qr,
                        'created_at' => $shar->created_at,
                        'updated_at' => $shar->updated_at,
                        'type_user' => 'both',
                        'success' => true,
                        'error' => "PRODUCTS VIEW: INFO + CARD_ID + HOLD_CARID:  USER-Can see products,QR, use start timer"
                    ];
                }
                return response()->json($data);
            }
        }
    }









    public function RentProductStartByPID($product_id, $user)
    {
        //test : localhost:8000/api/singleOffice/RentProductStartByPID/1198/alisa */  /*first argument PID, second user */

        /*rent product by PID */
        $model2 = SharedOfficeProducts::find($product_id);
        $model2->status_id = "1";  // 1 -- using
        $of_id = $model2->office_id;
        $model2->save();

        $model = new SharedOfficeFinance();
        $model->start_time = \Carbon\Carbon::now();
        $model->user = $user;
        $model->office_id = $of_id;
        $model->product_id = $product_id;
        $model->save();

        if (!isset($model)) {
            $res = [
                'success' => false,
                'error' => "Fail saving data in SharedOfficeFinance"
            ];
            return response()->json($res, 200);
        }

        if (!isset($model2)) {
            $res = [
                'success' => false,
                'error' => "Fail saving data in SharedOfficeProduct"
            ];
            return response()->json($res, 200);
        }

        $res = [
            'success' => true,
            'result' => "Successfully rented product"
        ];

        return response()->json($res, 200);

        //test : localhost:8000/api/singleOffice/RentProductStartByPID/1198/alisa */  /*first argument PID, second user */

    }

    public function RentProductStartByQR($qr_data, $user)
    {
        /*rent product by QR */
        /*localhost:8000/api/singleOffice/RentProductStartByQR/4431983093/mike*/  /*first argument QR_DATA, second user */
        $model2 = SharedOfficeProducts::where('number', '=', $qr_data)->first(); //when you saving using WHERE MUST BE first() not GET

        $of_id = $model2->office_id;
        $pi_id = $model2->id;
        $model2->status_id = "1";  // 1 -- using
        $model2->save();

        $model = new SharedOfficeFinance();

        $model->start_time = \Carbon\Carbon::now();
        $model->user = $user;
        $model->office_id = $of_id;
        $model->product_id = $pi_id;
        $model->save();


        if (!isset($model)) {
            $res = [
                'success' => false,
                'error' => "Fail saving data in SharedOfficeFinance"
            ];
            return response()->json($res, 200);
        }

        if (!isset($model2)) {
            $res = [
                'success' => false,
                'error' => "Fail saving data in SharedOfficeProduct"
            ];
            return response()->json($res, 200);
        }

        $res = [
            'success' => true,
            'result' => "Successfully rented product"
        ];

        return response()->json($res, 200);
    }


    public function ShowFinanceDataView($office_id)
    {
        /*return finance details for all users for selected office_id*/
        /*test http://localhost:8000/api/singleOffice/finance/11 */ /*parametar is offce_id , on example 11*/
        $sharedofficefinance = SharedOfficeFinance::where('office_id', '=', $office_id)->get();
        $language = Session::get('lang');

        $data = [];
        foreach ($sharedofficefinance as $shar) {

            /*******/
            if ($shar->end_time == "0000-00-00 00:00:00") {
                $minForPayment = \Carbon\Carbon::parse($shar->start_time)->diffInMinutes(\Carbon\Carbon::now());
            } else {
                $minForPayment = \Carbon\Carbon::parse($shar->start_time)->diffInMinutes(\Carbon\Carbon::parse($shar->end_time));
            }

            /*******/
            if ($language == "en") {
                $category_name = $shar->productfinance->productcategory->name;
            } elseif ($language == "cn") {
                $category_name = $shar->productfinance->productcategory->name_ch;
            } else {
                $category_name = $shar->productfinance->productcategory->name;
            }

            /*******/
            if ($shar->end_time == "0000-00-00 00:00:00") {
                $cost = \Carbon\Carbon::parse($shar->start_time)->diffInMinutes(\Carbon\Carbon::now());
                $cost = ($shar->productfinance->time_price * $cost / 10); //price is on 10 minute
            } else {
                $cost = \Carbon\Carbon::parse($shar->start_time)->diffInMinutes(\Carbon\Carbon::parse($shar->end_time));
                $cost = ($shar->productfinance->time_price * $cost / 10); //price is on 10 minute
            }

            /*******/
            if ($language == "en") {
                $status = $shar->productfinance->productstatus->status;
            } elseif ($language == "cn") {
                $status = $shar->productfinance->productstatus->status_ch;
            } else {
                $status = $shar->productfinance->productstatus->status;
            }

            /*******/
            if ($shar->cost != "") {
                $paid = "yes";
            } else {
                $paid = "no";
            }

            $data[] = [
                'id' => $shar->id,
                'product_id' => $shar->product_id,
                'office_id' => $shar->office_id,
                'office_name' => $shar->productfinance->office->office_name,
                'category_id' => $shar->productfinance->productcategory->id,
                'category_name' => $category_name,
                'number' => $shar->productfinance->number,
                'start_time' => $shar->start_time,
                'end_time' => $shar->end_time,
                'time_price' => $shar->productfinance->time_price,
                'minForPayment' => $minForPayment,
                'paymentComment' => $shar->status_finance,
                'currentTime' => \Carbon\Carbon::now(),
                'cost' => $cost,
                'user' => $shar->user,
                'status' => $status,
                'paid' => $paid
            ];
        }
        return response()->json($data);
    }


    public function PaymentFinanceViewUser($finance_id)
    {
        /*return Payment Finance details for selected user, here get data to perform payment */
        /*test http://localhost:8000/api/singleOffice/paymentfinaceviewuser/17 */ /*parametar is finace_id , on example 17*/

        $language = Session::get('lang');
        $sharedofficefinance = SharedOfficeFinance::find($finance_id);


        if ($language == "en") {
            $category_name = $sharedofficefinance->productfinance->productcategory->name;
        } elseif ($language == "cn") {
            $category_name = $sharedofficefinance->productfinance->productcategory->name_ch;
        } else {
            $category_name = $sharedofficefinance->productfinance->productcategory->name;
        }


        if ($sharedofficefinance->cost != "") {
            $paid = "yes";
        } else {
            $paid = "no";
        }


        if ($sharedofficefinance->end_time == "0000-00-00 00:00:00") {
            $minForPayment = \Carbon\Carbon::parse($sharedofficefinance->start_time)->diffInMinutes(\Carbon\Carbon::now());
        } else {
            $minForPayment = \Carbon\Carbon::parse($sharedofficefinance->start_time)->diffInMinutes(\Carbon\Carbon::parse($sharedofficefinance->end_time));
        }

        $cost = \Carbon\Carbon::parse($sharedofficefinance->start_time)->diffInMinutes(\Carbon\Carbon::now());
        $cost = ($sharedofficefinance->productfinance->time_price * $cost / 10); //price is on 10 minute


        $data[] = [
            'id' => $sharedofficefinance->id,
            'product_id' => $sharedofficefinance->product_id,
            'office_id' => $sharedofficefinance->office_id,
            'office_name' => $sharedofficefinance->productfinance->office->office_name,
            'category_id' => $sharedofficefinance->productfinance->productcategory->id,
            'category_name' => $category_name,
            'number' => $sharedofficefinance->productfinance->number,
            'start_time' => $sharedofficefinance->start_time,
            'end_time' => $sharedofficefinance->end_time,
            'time_price' => $sharedofficefinance->productfinance->time_price,
            'minForPayment' => $minForPayment,
            'paymentComment' => $sharedofficefinance->status_finance,
            // 'currentTime' => \Carbon\Carbon::now(),
            'cost' => $cost,
            'user' => $sharedofficefinance->user,
            'status' => $sharedofficefinance->productfinance->productstatus->status,
            'paid' => $paid
        ];
        return response()->json($data);
    }



    public function StorePaymentForUser($finance_id, $status)
    {
        /* payment for user and storing data in database */
        /* http://localhost:8000/api/singleOffice/storepaymentforuser/17/pay/
        /*first parametar 17 -- finance_id, second is input status */

        $sharedofficefinance = SharedOfficeFinance::find($finance_id);

        $cost = \Carbon\Carbon::parse($sharedofficefinance->start_time)->diffInMinutes(\Carbon\Carbon::now());
        $cost = ($sharedofficefinance->productfinance->time_price * $cost / 10); //price is on 10 minute
        $endTime = \Carbon\Carbon::now();

        $sharedofficefinance->status_finance = $status;
        $sharedofficefinance->cost = $cost;
        $sharedofficefinance->end_time = $endTime;
        $product_id = $sharedofficefinance->product_id;
        $sharedofficefinance->save();

        $model2 = SharedOfficeProducts::find($product_id);
        $model2->status_id = "3";  // 3-- empty
        $model2->save();

        if (!isset($sharedofficefinance)) {
            $res = [
                'success' => false,
                'error' => "Fail saving data in SharedOfficeFinance"
            ];
            return response()->json($res, 200);
        }

        if (!isset($model2)) {
            $res = [
                'success' => false,
                'error' => "Fail saving data in SharedOfficeProduct"
            ];
            return response()->json($res, 200);
        }

        $res = [
            'success' => true,
            'result' => "Successfully payment for user"
        ];

        return response()->json($res, 200);
    }

    public function CheckUserSeatVerification($id)
    {
        $transaction = new SharedOfficeFinance();
        $data = $transaction->with('productfinance')
            ->whereHas('productfinance', function ($q) {
                $q->where('status_id', '=', 1);
            })
            ->where('user_id', '=', $id)
            ->whereNull('end_time')
            ->first();
        if (isset($data) || !is_null($data)) {
            return true;
        }
        return false;
    }

    public function userImage(Request $request)
    {
        try {
            $user_id = \Auth::user()->id;
            $record = UserIdentity::where('user_id', '=', $user_id)->first(); // if record is null then user first time login case and send status '0'
            if (!is_null($record)) {
                try {
                    $countryCode = WorldCountries::where('id', '=', $record->country_id)->first();
                    $record->country_id = $countryCode->code;
                } catch (\Exception $e) { }
                if ($record->status === 0) {
                    return ([
                        'status' => 'success',
                        'bit' => UserIdentity::STATUS_PENDING,
                        'data' => $record,
                    ]);
                } else if ($record->status === 1) {
                    return ([
                        'status' => 'success',
                        'bit' => UserIdentity::STATUS_APPROVED,
                        'data' => $record,
                    ]);
                } else if ($record->status === 3) {
                    return ([
                        'status' => 'success',
                        'bit' => UserIdentity::STATUS_REJECTED,
                        'data' => $record,
                    ]);
                } else {
                    return ([
                        'status' => 'success',
                        'bit' => UserIdentity::STATUS_REJECTED,
                        'data' => $record,
                    ]);
                }
            } else {
                return ([
                    'status' => 'success',
                    'bit' => UserIdentity::STATUS_PENDING,
                    'data' => $record,
                ]);
            }
        } catch (\Exception $e) {
            return collect([
                'status' => 'failure',
                'message' => $e->getMessage() . 'at' . $e->getLine()
            ]);
        }
    }

    public function reset()
    {
        try {
            $user_id = \Auth::user()->id;
            $record = UserIdentity::where('user_id', '=', $user_id)->first();
            if (!is_null($record)) {
                \DB::beginTransaction();
                $record->status = UserIdentity::STATUS_PENDING;
                $record->save();
                \DB::commit();
                return collect([
                    'status' => 'success',
                    'message' => $record->status
                ]);
            } else {
                return collect([
                    'status' => 'failure',
                    'message' => 'record not found',
                ]);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return collect([
                'status' => 'failure',
                'message' => $e
            ]);
        }
    }

    public function submitUserIdentity(Request $request)
    {
        try {
            $user_id = \Auth::user()->id;
            $record = UserIdentity::where('user_id', '=', $user_id)->first();
            $country_code = $request->country_id;
            $val = strval($country_code) !== strval(intval($country_code));
            if ($val === false) {
                $country_id = $country_code;
            } else {
                $id = WorldCountries::where('code', '=', strtolower($country_code))->first();
                if (is_null($id)) {
                    return collect([
                        'status' => 'failure',
                        'message' => 'Country does not exist',
                    ]);
                } else {
                    $country_id = $id->id;
                }
            }
            if (!is_null($record)) {
                \DB::beginTransaction();
                $record->real_name = $request->real_name;
                $record->id_card_no = $request->id_card;
                $record->date_of_birth = $request->dob;
                $record->address = $request->address;
                $record->gender = $request->gender;
                $record->handphone_no = $request->hand_phone;
                $record->country_id = $country_id;
                $record->save();
                \DB::commit();
                return collect([
                    'status' => 'success',
                    'message' => 'Successfully Insert User data'
                ]);
            } else {
                \DB::beginTransaction();
                $userIdentity = new UserIdentity();
                $userIdentity->real_name = $request->real_name;
                $userIdentity->user_id = $user_id;
                $userIdentity->id_card_no = $request->id_card;
                $userIdentity->date_of_birth = $request->dob;
                $userIdentity->address = $request->address;
                $userIdentity->gender = $request->gender;
                $userIdentity->handphone_no = $request->hand_phone;
                $userIdentity->country_id = $country_id;
                $userIdentity->save();
                \DB::commit();
                return collect([
                    'status' => 'success',
                    'message' => 'Successfully Insert User data'
                ]);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return collect([
                'status' => 'failure',
                'message' => $e->getMessage() . 'at ' . $e->getLine()
            ]);
        }
    }

    public function sharedOfficeRating(Request $request)
    {
        try {
            \DB::beginTransaction();
            $sharedOfficeRating = new SharedOfficeRating();
            $sharedOfficeRating->office_id = $request->office_id;
            $sharedOfficeRating->user_id = \Auth::user()->id;
            $sharedOfficeRating->rate = $request->rate;
            $sharedOfficeRating->comment = $request->comment;
            $sharedOfficeRating->save();
            \DB::commit();

            return collect([
                'status' => 'success',
                'message' => 'Thanks for your rating'
            ]);
        } catch (\Exception $e) {
            \DB::rollBack();
            return collect([
                'status' => 'failure',
                'message' => $e->getMessage() . 'at ' . $e->getLine()
            ]);
        }
    }

    public function saveStatus()
    {
        try {
            $userIdentity = UserIdentity::where('user_id', '=', \Auth::user()->id)->first();
            \DB::beginTransaction();
            $userIdentity->status = UserIdentity::STATUS_PENDING;
            $userIdentity->save();
            \DB::commit();

            return collect([
                'status' => 'success',
                'data' => $userIdentity,
            ]);
        } catch (\Exception $e) {
            \DB::rollBack();
            return collect([
                'status' => 'failure',
                'message' => $e->getMessage() . ' at ' . $e->getLine()
            ]);
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function officeImage(Request $request){
        $input = $request->all();
        if($request->hasFile('file')) {
            $filename = $request->file('file')->getClientOriginalName();
            $ext = $request->file('file')->getClientOriginalExtension();
            $path   = 'uploads/sharedoffice/';
            $filename = 'sahredoffice_' . generateRandomUniqueName();
            touchFolder($path);
            $request->file('file')->move($path, $filename. $ext);
            $input['file'] = ($path. $filename. $ext);
        }
        $sharedOfficeMainImage = SharedOfficeMainImage::create([
           'main_image' => $input['file']
        ]);
        return collect([
            'success' => 'true',
            'office_id' => $sharedOfficeMainImage->id,
        ]);
    }

    public function getOfficeImage($id){
        $sharedOfficeMainImage = SharedOfficeMainImage::where('id','=',$id)->first();
        return collect([
           'success' => true,
           'main_image' => $sharedOfficeMainImage->main_image
        ]);
    }

    private function checkAvailability($office_id, $category_id)
    {
        // check availability
        $ava = SharedOfficeProductAvailability::where('office_id', $office_id)
            ->where('category_id', $category_id)
            ->first();
        if (!$ava) return false;
        // check total empty spaces
        $products = SharedOfficeProducts::where('office_id', $office_id)
            ->where('category_id', $category_id)
            ->where('status_id', SharedOfficeProducts::STATUS_EMPTY)
            ->get();
        if(count($products) > 0) {
            return true;
        }
        return false;
//        $sum = 0;
//        foreach ($products as $val) {
//            $sum += $val;
//        }
//
//        if ($ava->availability - $sum <= 0) return false;
//
//        return true;

    }


    public function booking(Request $request)
    {
        $checkAvailability = SharedOfficeProducts::where('office_id','=',$request->office_id)->first();
        $checkBookingRequests = SharedOfficeBooking::where('office_id','=',$request->office_id)->get();
//        if(count($checkBookingRequests) > $checkAvailability->availability_id) {
//            return collect([
//                'status' => 'error',
//                'message' => 'There is no product availability'
//            ]);
//        }

        $seats = $checkAvailability->no_of_peoples;
        if($request->no_of_peoples > $checkAvailability->no_of_peoples) {
            return collect([
                'status' => 'info',
                'message' => 'You can book maximum '.$seats.' available seats this time'
            ]);
        }

        try {
            $email = isset($request->office_manager) ? $request->office_manager :'fatehhasan26@gmail.com';
            $senderEmail = "support@xenren.co";
            $receiverEmail = $email;

            $sharedOfficeBooking = new SharedOfficeBooking();

            \DB::beginTransaction();
            $sharedOfficeBooking->office_id = $request->office_id;
            $sharedOfficeBooking->category_id = $request->category_id;
            $sharedOfficeBooking->user_id = isset(\Auth::user()->id) ? \Auth::user()->id : 0;
            $sharedOfficeBooking->client_email = isset($request->office_manager) ? $request->office_manager :'fatehhasan26@gmail.com';
            $sharedOfficeBooking->full_name = $request->full_name;
            $sharedOfficeBooking->phone_no = $request->phone_no;
            $sharedOfficeBooking->check_in = $request->check_in_date;
            $sharedOfficeBooking->check_out = $request->check_out_date;
            $sharedOfficeBooking->no_of_rooms = $request->no_of_rooms;
            $sharedOfficeBooking->no_of_persons = $request->no_of_peoples;
            $sharedOfficeBooking->remarks = $request->remarks;
            $sharedOfficeBooking->save();
            \DB::commit();

            \Mail::send('mails.sharedofficebookingemail', array(
                'email' => $request->office_manager,
                'full_name' => $request->full_name,
                'phone_no' => $request->phone_no,
                'Check_in' => $request->check_in_date,
                'Check_out' => $request->check_out_date,
                'No_of_rooms' => $request->no_of_rooms,
                'No_of_Persons' => $request->no_of_peoples,
                'Remarks' => $request->remarks
            ), function ($message) use ($senderEmail, $receiverEmail) {
                $message->from($senderEmail);
                $message->to($receiverEmail);
                $message->cc('support@xenren.co')
                    ->subject(trans('common.shared_office_request'));
            });
            $checkStatus = $this->sendSmsToOwner($request->office_id,$request->category_id,\Auth::user()->id,$request->full_name,$request->phone_no);
//            if($checkStatus == false) {
//                return collect([
//                    'status' => 'false',
//                    'message' => 'office owner phone number missing or invalid please contact office owner via email'
//                ]);
//            }
            $this->sendPushyNotification($request->all());
            return collect([
                'status' => 'success',
                'message' => 'We will contact you soon...!'
            ]);
        } catch (\Exception $e) {
            \DB::rollBack();
            return collect([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function sendSmsToOwner($officeId,$cateGoryId,$userId,$name,$userPhoneNumber){
        $officeProduct = SharedOfficeProducts::where('office_id','=',$officeId)->where('category_id','=',$cateGoryId)->first();
        $officeCategoryProductName = SharedOfficeProductCategory::where('id','=',$cateGoryId)->first();
        $office = SharedOffice::where('id','=',$officeId)->first();
        $staff = Staff::where('id','=',$office->staff_id)->first();
//        if($staff->phone_number == '' || $staff->phone_number == 0) {
//            return false;
//        }
        $phone_number = $staff->phone_number;
        $username  = $name;
        $seatNo = $officeProduct->number;
        $officeName = $office->office_name;
        $seatName = $officeCategoryProductName->name;
        $seatName = str_replace(' ','',$seatName);
        $seatName = explode('.',$seatName);
        $seatName = $seatName[1];
        twilloSendSMSToOwner($phone_number,$username,$seatNo,$officeName,$seatName,$userPhoneNumber);
    }

    public function sendPushyNotification($request){
        $office = SharedOffice::where('id','=',$request['office_id'])->first();
        $staff = Staff::where('id','=',$office->staff_id)->first();
        if(isset($staff)) {
            $usersTokens = array();
            array_push($usersTokens,$staff->device_token);
            $data = array (
                "title" => "Xenren". $office->office_name, // Notification title
                'message' => 'New Request Received To Join Your Xenren Shared Office',
                'body' => 'Shared Office Booking Request ...!',
                "image" => asset('images/Favicon.jpg')
            );
            $options = array(
                'notification' => array(
                    'badge' => 0,
                    'sound' => 'ping.aiff',
                    'body' => "New Booking Request",
                )
            );
            $check = PushyAPI::sendBookingNotification($data, $usersTokens, $options);
        }
    }

    public function sharedOfficeReserve(Request $request)
    {
        $email = $request->office_manager;
        $senderEmail = "support@xenren.co";
        $receiverEmail = $email;

        $sharedOfficeReserve = new SharedOfficeReserveRequest();

        $sharedOfficeReserve->type = $request->office_type;
        $sharedOfficeReserve->office_id = $request->office_id;
        $sharedOfficeReserve->office_manager_email = $request->office_manager;
        $sharedOfficeReserve->name = $request->full_name;
        $sharedOfficeReserve->phone_number = $request->phone_no;
        $sharedOfficeReserve->check_in_date = $request->check_in_date;
        $sharedOfficeReserve->check_out_date = $request->check_out_date;
        $sharedOfficeReserve->no_of_rooms = $request->no_of_rooms;
        $sharedOfficeReserve->no_of_peoples = $request->no_of_peoples;
        $sharedOfficeReserve->remarks = $request->remarks;
        $sharedOfficeReserve->price = $request->price;
        $sharedOfficeReserve->duration_type = $request->duration_type;
        $sharedOfficeReserve->save();

        \Mail::send('mails.sharedofficeReserve', array(
            'type' => $request->office_type,
            'price' => $request->price,
            'duration_type' => $request->duration_type,
            'email' => $receiverEmail,
            'full_name' => $request->full_name,
            'phone_no' => $request->phone_no,
            'Check_in' => $request->check_in_date,
            'Check_out' => $request->check_out_date,
            'No_of_rooms' => $request->no_of_rooms,
            'No_of_Persons' => $request->no_of_peoples,
            'Remarks' => $request->remarks
        ), function ($message) use ($senderEmail, $receiverEmail) {
            $message->from($senderEmail);
            $message->to($receiverEmail);
            $message->cc('support@xenren.co')
                ->subject(trans('common.shared_office_request'));
        });

        return collect([
            'status' => 'success',
            'message' => 'We will contact you soon...!'
        ]);
    }

    public function getAllDesks()
    {
        $products = SharedOfficeProducts::whereHas('availability')
            ->select(['id', 'office_id', 'category_id', 'status_id', 'availability_id', 'no_of_peoples', 'time_price', 'hourly_price', 'month_price'])
            ->with([
                'availability' => function ($query) {
                    return $query->select(['id', 'availability']);
                },
                'productstatus' => function ($query) {
                    return $query->select(['id', 'status', 'status_ch']);
                },
                'office' => function ($query) {
                    return $query->select(['id', 'office_name']);
                },
                'productcategory' => function ($query) {
                    return $query->select(['id', 'name', 'name_ch', 'image']);
                }
            ])
            ->orderBy('no_of_peoples')
            ->orderBy('office_id')
            ->orderBy('category_id')
            ->where('status_id', 3)
            ->paginate(5);

        if ($products->count()) {
            return collect([
                'status' => 'success',
                'message' => 'All office desks data',
                'data' => $products
            ]);
        }

        return collect([
            'status' => 'failed',
            'message' => 'Get office desks failed',
            'data' => []
        ]);
    }

    public function selectDesk(Request $request)
    {
        try {
            if (!$this->checkAvailability($request->office_id, $request->category_id)) {
                return response()->json([
                    'status' => 'failed',
                    'message' => 'Facility not available',
                    'data' => []
                ], 404);
            }

            $email = $request->office_manager;
            $senderEmail = "support@xenren.co";
            $receiverEmail = $email;

            $sharedOfficeBooking = new SharedOfficeBooking();

            \DB::beginTransaction();
            $sharedOfficeBooking->office_id = $request->office_id;//
            $sharedOfficeBooking->category_id = $request->category_id;//
            $sharedOfficeBooking->user_id = isset(\Auth::user()->id) ? \Auth::user()->id : 0;//
            $sharedOfficeBooking->client_email = $request->office_manager;//
            $sharedOfficeBooking->full_name = $request->full_name;//
            $sharedOfficeBooking->phone_no = $request->phone_no;//
            $sharedOfficeBooking->check_in = $request->check_in_date;//
            $sharedOfficeBooking->check_out = $request->check_out_date;//
            $sharedOfficeBooking->no_of_rooms = $request->no_of_rooms;//
            $sharedOfficeBooking->no_of_persons = $request->no_of_peoples;//
            $sharedOfficeBooking->remarks = $request->remarks;
            $sharedOfficeBooking->save();
            \DB::commit();

            \Mail::send('mails.sharedofficebookingemail', array(
                'email' => $request->office_manager,
                'full_name' => $request->full_name,
                'phone_no' => $request->phone_no,
                'Check_in' => $request->check_in_date,
                'Check_out' => $request->check_out_date,
                'No_of_rooms' => $request->no_of_rooms,
                'No_of_Persons' => $request->no_of_peoples,
                'Remarks' => $request->remarks
            ), function ($message) use ($senderEmail, $receiverEmail) {
                $message->from($senderEmail);
                $message->to($receiverEmail);
                $message->cc('support@xenren.co')
                    ->subject(trans('common.shared_office_request'));
            });

            return response()->json([
                'status' => 'success',
                'message' => 'Booking has been recorded, we will contact you soon...!',
                'data' => $sharedOfficeBooking
            ], 200);
        } catch (\Exception $e) {
            \DB::rollBack();
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage(),
                'data' => []
            ], 400);
        }
    }
}
