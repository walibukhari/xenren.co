<?php

namespace App\Http\Controllers\Services;

use App\Http\Requests\Frontend\UserIdentityCreateFormRequest;
use App\Models\Countries;
use App\Models\CountryCities;
use App\Models\CountryLanguage;
use App\Models\GuestRegister;
use App\Models\Project;
use App\Models\ProjectChatMessage;
use App\Models\ProjectChatRoom;
use App\Models\States;
use App\Models\User;
use App\Models\UserCheckContactLog;
use App\Models\UserIdentity;
use App\Models\WorldCities;
use App\Models\WorldCountries;
use App\Models\WorldDivisions;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;
use Lang;
use mysql_xdevapi\CollectionFind;
use Pingpp\Collection;

class ServiceController extends Controller
{
    use \App\Traits\YahooWeatherTrait;
    //

    public function languages()
    {
//        dd(CountryLanguage::all());
        $data = [];
        foreach (CountryLanguage::all() as $lang) {
            $data[] = [
                'id' => $lang->id,
                'name' => $lang->language->name,
                'code' => isset($lang->country->code) ? $lang->country->code : '',
            ];
        }
        return response()->json($data);

    }

    public function CountriesAndCities(Request $request)
    {
	    $a = \App\Models\WorldCountries::with([
		    'locale' => function($q){
			    $q->where('locale', '=', 'zh-cn');
			    $q->select([
				    'id', 'name', 'country_id', 'full_name', 'currency_name'
			    ]);
		    },
		    'cities' => function($q){
			    $q->select([
				    'id', 'country_id', 'name', 'full_name', 'code'
			    ]);
		    },
		    'cities.locale' => function($q){
			    $q->where('locale', '=', 'zh-cn');
			    $q->select([
				    'id', 'city_id', 'name', 'full_name'
			    ]);
		    }])
//		->where('id', 87)
		    ->select([
			    'id', 'name', 'full_name', 'capital', 'code', 'currency_code', 'currency_name'
		    ]);

	    $a->when($request->country_id, function($q) use($request){
		    $q->where('id', $request->country_id);
	    });

	    $a->when($request->country_name, function($q) use($request){
		    $q->where('name', $request->country_name);
	    });

	    $a = $a->get();
	    return collect([
	    	'status' => 'success',
		    'data' => $a
	    ]);
    }

    public function CitiesAndCountries(Request $request)
    {
        try {
            $a = \App\Models\WorldCities::with([
                'country' => function($q){
                    $q->select([
                        'id', 'full_name', 'name'
                    ]);
                },
                'country.locale' => function($q){
                    $q->where('locale', '=', 'zh-cn');
                }
            ])
                ->select([
                    'id', 'name', 'full_name', 'code', 'country_id'
                ])
                ->where('id', '=', $request->city_id);

            $a = $a->get();
            return collect([
                'status' => 'success',
                'data' => $a
            ]);
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage(). 'at '.$e->getLine()
            ]);
        }
    }

    public function projectChatMessages(Request $request)
    {
        $project_id = $request->get('project_id');
        $chat_room_id = $request->get('chat_room_id');
        if (!isset($project_id)) {
            $res = [
                'success' => false,
                'error' => "project ID Missing"
            ];
            return response()->json($res, 200);
        }
        if (!isset($chat_room_id)) {
            $res = [
                'success' => false,
                'error' => "project ID Missing"
            ];
            return response()->json($res, 200);
        }

        $chatMessages = ProjectChatMessage::with('SendBy')->where('project_chat_room_id', $chat_room_id)
            ->get();
//        $chatMessages->count();
        return response()->json($chatMessages, 200);
    }

    public function projectChatMessagesCreate(Request $request)
    {
        $project_id = $request->get('project_id');
        $chat_room_id = $request->get('chat_room_id');
        $sender_user_id = $request->get('sender_user_id');
        $message = $request->get('message');


        if (!isset($chat_room_id)) {
            $res = [
                'success' => false,
                'error' => "Chat Room ID Missing"
            ];
            return response()->json($res, 200);
        }

        if (!isset($sender_user_id)) {
            $res = [
                'success' => false,
                'error' => "Sender User ID Missing"
            ];
            return response()->json($res, 200);
        }

        if (!isset($message)) {
            $res = [
                'success' => false,
                'error' => "Message Missing"
            ];
            return response()->json($res, 200);
        }

        $chatMessage = new ProjectChatMessage();
        $chatMessage->project_chat_room_id = $chat_room_id;

        $chatMessage->sender_user_id = $sender_user_id;

        $chatMessage->message = $message;

        if ($request->hasFile('image')) {
//            $file= $request->hasFile('image');
            $imageName = rand() . $request->file('image')->getClientOriginalName();
            $path = base_path() . '/public/uploads/chat/images/';
            $request->file('image')->move($path, $imageName);
            $chatMessage->file = 'uploads/chat/images/' . $imageName;
        }
        $chatMessage->save();


        Redis::publish('default_room', $chatMessage->load('sender'));

        $result = $chatMessage->load('SendBy');

        $res = [
            'success' => true,
            'result' => $result
        ];

        return response()->json($res, 200);
    }

    public function countries(Request $request)
    {
    	  $countries = WorldCountries::with(['locale' => function($q){
		      $q->select([
		      	'id', 'full_name', 'currency_name', 'country_id', 'name as text', 'name as name'
		      ]);
	      }])->select([
	      	'id', 'name as text', 'name as name', 'full_name', 'code'
	      ]);

    	  if(isset($request->q)) {
    	  	$countries->where('name', 'like', '%'.$request->q.'%');
	      }
	      $countries = $countries->get();
//        if (Lang::locale() == 'cn') {
//                $countries = Countries::selectRaw('Case WHEN name_cn = "" OR name_cn IS NULL
//                            THEN name
//                            ELSE name_cn END as text, id,code')
//                    ->where('name', 'like', '%' . $request->get('q') . '%')
//                    ->orWhere('name_cn', 'like', '%' . $request->get('q') . '%')->get();
//
//
//        } else {
//            $countries = Countries::selectRaw('Case WHEN name = "" OR name IS NULL
//                            THEN name_cn
//                            ELSE name END as text, id,code')
//                ->where('name', 'like', '%' . $request->get('q') . '%')
//                ->orWhere('name_cn', 'like', '%' . $request->get('q') . '%')->get();
//        }
        return response()->json($countries);
    }

    public function cities(Request $request)
    {
	    try {
		    $countries = WorldCountries::where('name', 'like', '%' . $request->q. '%')->select('id')->get();
		    $countryStates = WorldDivisions::whereIn('country_id', $countries)->select('id')->get();

		    $query = WorldCities::with('state', 'country')->where('name', 'like', '%' . $request->q. '%');
		    //            if(isset($countryStates) && count($countryStates) > 0) {
		    //            	$query->orWhereIn('state_id', $countryStates);
		    //            }
		    $data = $query->orderByRaw('CHAR_LENGTH(name)')->get();
		    $arr = array();
		    foreach ($data as $k => $v){
			    array_push($arr, array(
				    'id' => $v->id,
				    'text' => $v->name . ', '. $v->country->name
			    ));
		    }
		    return response()->json($arr);
	    } catch (\Exception $e) {
		    dd($e);
	    }
    }

    public function citiesOnly()
    {
	    try {
		    $query = WorldCities::with('locale')->select([
		       'id', 'country_id', 'name', 'code'
            ])->get();
		    return response()->json($query);
	    } catch (\Exception $e) {
		    return collect([
		        'status' => 'error',
                'message' => $e->getMessage() . 'at '. $e->getLine()
            ]);
	    }
    }

    public function resend(Request $request)
    {
        try {
            $key = $request->input('key');
            $email = $request->input('email');
            $record = GuestRegister::where('key', '=', $key)->first();
            if (isset($record)) {
                $sixDigit = $record->code;
                $senderEmail = "support@xenren.co";
                $senderName = "System Admin";
                $receiverEmail = $email;

                //for code display purpose
                $sixDigit = substr($sixDigit, 0, 3) . '-' . substr($sixDigit, 3, 6);
                Mail::send('mails.guestConfirmationCodeMobile', array('code' => $sixDigit, 'email' => $email), function ($message) use ($senderEmail, $senderName, $receiverEmail, $sixDigit) {
                    $message->from($senderEmail, $senderName);
                    $message->to($receiverEmail)
                        ->subject(trans('common.confirmation_code_for_xenren', ['code' => $sixDigit]));
                });
                return collect([
                    'status' => 'success',
                    'message' => 'Code has been Resend'
                ]);
            } else {
                return collect([
                    'status' => 'error',
                    'message' => 'Record not found!'
                ]);
            }

        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage() . 'at '. $e->getLine()
            ]);
        }
    }

    public function identityAuthenticationChange(Request $request)
    {
        try {
            $column = $request->get('column');
            $value = $request->get('value');
            $id = $request->get('id');
            $userIdentity = UserIdentity::find($id);

            switch ($column) {
                case "real_name":
                    if ($userIdentity->real_name == $value) {
                        return makeJSONResponse(true, 'success');
                    }

                    $userIdentity->real_name = $value;
                    break;
                case "id_card_no":
                    if ($userIdentity->id_card_no == $value) {
                        return makeJSONResponse(true, 'success');
                    }

                    $userIdentity->id_card_no = $value;
                    break;
                case "gender":
                    if ($userIdentity->gender == $value) {
                        return makeJSONResponse(true, 'success');
                    }

                    $userIdentity->gender = $value;
                    break;
                case "date_of_birth":
                    if ($userIdentity->date_of_birth == $value) {
                        return makeJSONResponse(true, 'success');
                    }

                    $userIdentity->date_of_birth = $value;
                    break;
                case "address":
                    if ($userIdentity->address == $value) {
                        return makeJSONResponse(true, 'success');
                    }

                    $userIdentity->address = $value;
                    break;
                case "country":
                    if ($userIdentity->country_id == $value) {
                        return makeJSONResponse(true, 'success');
                    }

                    $userIdentity->country_id = $value;
                    break;
                case "handphone_no":
                    if ($userIdentity->handphone_no == $value) {
                        return makeJSONResponse(true, 'success');
                    }

                    $userIdentity->handphone_no = $value;
                    break;
                case "id_image":
                    if ($request->hasFile('id_image')) {
                        $userIdentity->id_image = $request->file('id_image');
                    }
                    break;
                case "hold_id_image":
                    if ($request->hasFile('hold_id_image')) {
                        $userIdentity->hold_id_image = $request->file('hold_id_image');
                    }
                    break;
            }
            $userIdentity->is_submit = 0;
            $userIdentity->reject_reason = null;
            $userIdentity->status = null;
            if ($userIdentity->gender == null) {
                $userIdentity->gender = 0;
            }
            $userIdentity->save();
            return makeJSONResponse(true, 'success');
        } catch (\Exception $e){
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function identityAuthenticationSubmit(UserIdentityCreateFormRequest $request)
    {
        $id = $request->get('user_identity_id');
        $identity = UserIdentity::find($id);
        try {

            if ($identity->status == UserIdentity::STATUS_PENDING && $identity->is_submit == 1) {
                return makeResponse(trans('member.waiting_approval'), true);
            }

            if( $identity->id_image == '' || $identity->id_image == null )
            {
                return makeResponse(trans('validation.required', ['attribute' => trans('common.id_image')]), true);
            }

            if( $identity->hold_id_image == '' || is_null($identity->hold_id_image))
            {
                return makeResponse(trans('validation.required', ['attribute' => trans('common.hold_id_image')]), true);
            }

            $identity->status = UserIdentity::STATUS_PENDING;
            $identity->is_submit = 1;
            $identity->save();

            addSuccess(trans('member.successfully_submit_authentication'));
            return makeResponse(trans('member.successfully_submit_authentication'));
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function isAllowToCheckContact(Request $request){
        $ipAddress = $request->get('ipAddress');
        $userId = $request->get('userId');
        $targetUserId = $request->get('targetUserId');

        $addIntoDB = false;
        if( $userId > 0 )
        {
            $user = User::find($userId);
            if($user->getLatestIdentityStatus() == UserIdentity::STATUS_APPROVED )
            {
                //check authenticate user view count
                $count = UserCheckContactLog::where('user_id', $userId)
                    ->where('target_user_id', '!=', $targetUserId)
                    ->whereDate('created_at', '=', date('Y-m-d'))
                    ->count();
                if( $count >= 50 )
                {
                    return makeJSONResponse(false, trans('common.user_authentication_reach_max_view_contact_limit'));
                }
            }
            else
            {
                //check login user view count
                $count = UserCheckContactLog::where('user_id', $userId)
                    ->where('target_user_id', '!=', $targetUserId)
                    ->whereDate('created_at', '=', date('Y-m-d'))
                    ->count();
                if( $count >= 10 )
                {
                    return makeJSONResponse(false,  trans('common.user_login_reach_max_view_contact_limit'));
                }
            }

            $count = UserCheckContactLog::where('user_id', $userId)
                ->where('target_user_id', $targetUserId)
                ->whereDate('created_at', '=', date('Y-m-d'))
                ->count();
            if( $count == 0 )
            {
                $addIntoDB = true;
            }
        }
        else
        {
            //check guest contact view count
            $count = UserCheckContactLog::where('ip_address', $ipAddress)
                ->where('target_user_id', '!=', $targetUserId)
                ->whereDate('created_at', '=', date('Y-m-d'))
                ->count();
            if( $count >= 2 )
            {
                return makeJSONResponse(false, trans('common.guest_reach_max_view_contact_limit'));
            }

            $count = UserCheckContactLog::where('ip_address', $ipAddress)
                ->where('target_user_id', $targetUserId)
                ->whereDate('created_at', '=', date('Y-m-d'))
                ->count();
            if( $count == 0 )
            {
                $addIntoDB = true;
            }
        }


        if( $addIntoDB == true )
        {
            $userCheckContactLog = new UserCheckContactLog();
            if( $userId > 0 )
            {
                $userCheckContactLog->ip_address = null;
                $userCheckContactLog->user_id = $userId;
            }
            else
            {
                $userCheckContactLog->ip_address = $ipAddress;
                $userCheckContactLog->user_id = null;
            }

            $userCheckContactLog->target_user_id = $targetUserId;
            $userCheckContactLog->save();
        }


        return makeJSONResponse(true, 'success');
    }

    public function skipAddUserSkill(Request $request){
        $userId = $request->get('userId');
        if( $userId > 0 )
        {
            $user = User::find($userId);
            $user->is_first_time_skill_submit = 1; //assume already submit due to user press skip key
            $user->save();

            return makeJSONResponse(true, 'success');
        }
        else
        {
            return makeJSONResponse(false, 'fail');
        }
    }

    public function tempByLatLong(Request $request)
    {
        $lat = $request->lat;
        $long= $request->long;

        if(!(isset($lat) && isset($long))){
            return collect([
                'status' => 'error',
                'message' => 'latitude and longitude parameters are required'
            ]);
        }

        $return_data = $this->getWeatherByYahoo($lat,$long);

        if($return_data){
            $responseData = [
                'status' => 'success',
                'data' => [
                    'city' => $return_data->location->city?$return_data->location->city:'NA',
                    'region' => $return_data->location->region?$return_data->location->region:'NA',
                    'country' => $return_data->location->country?$return_data->location->country:'NA',
                    'lat' => $return_data->location->lat?$return_data->location->lat:'NA',
                    'long' => $return_data->location->long?$return_data->location->long:'NA',
                    'text' => $return_data->current_observation->condition->text?$return_data->current_observation->condition->text:'NA',
                    'unit' => 'Celcius',
                    'temperature' => $return_data->current_observation->condition->temperature?$return_data->current_observation->condition->temperature:'NA',
                    'low' => $return_data->forecasts[0]->low?$return_data->forecasts[0]->low:'NA',
                    'high' => $return_data->forecasts[0]->high?$return_data->forecasts[0]->high:'NA',
                ]
            ];
        }
        else{
            $responseData = [
                'status' => 'fail',
                'message' => 'Something went wrong'
            ];
        }
        return collect($responseData);
    }

    public function getLocation(Request $request) {
        $x = geoip($request->ip);
        return collect(['lat' => $x->lat , 'lng' => $x->lon]);
    }
}
