<?php

namespace App\Http\Controllers\Services;

use App\Models\Skill;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SkillController extends Controller
{
    //

    public function index()
    {
        $skill = Skill::select('id as value', 'name_en as text', 'name_cn as cn', 'description_en as description', 'description_cn as desc_cn')->get();

        return response()->json($skill);

    }
}
