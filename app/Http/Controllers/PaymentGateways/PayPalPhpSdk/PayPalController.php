<?php

namespace App\Http\Controllers\PaymentGateways\PayPalPhpSdk;

use App\Events\WithdrawAmount;
use App\Jobs\CheckPayPalWithdrawStatus;
use App\Libraries\PayPal_PHP_SDK\PayPalTrait;
use App\Models\SharedOffice;
use App\Models\Staff;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\User;
use App\Models\UserPaymentMethod;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class PayPalController extends Controller
{
		use PayPalTrait;

		public function index(Request $request)
		{
				session(['data' => $request]);
				$link = self::createPayment($request);
				return redirect($link);
		}

		public function getStaff($id){
		    $officeId = SharedOffice::where('id','=',$id)->first();
            $staff = Staff::where('id','=',$officeId->staff_id)->first();
            return $staff;
        }

		public function approvePayPal(Request $request)
		{
            $result = self::executePayment($request);
		    if(isset($request->type) && $request->type == Transaction::TYPE) {
		        $staff = $this->getStaff($request->office_id);
                if ($result['status'] == 'approved') {
//                    $job = (new \App\Jobs\PayPalSuccessPayment(Auth::user()->id, json_encode($result['result'])))->onQueue(config('jobs-queue-names.ApprovePayPal'));
//                    dispatch($job);
                    try {
                        \DB::beginTransaction();
                        $newBalance = $staff->balance + $result['result']['transactions']['amount'];
                        $tr = Transaction::create([
                            'name' => 'Topup Amount',
                            'amount' => $result['result']['transactions']['amount'],
                            'milestone_id' => 0,
                            'due_date' => Carbon::now()->addYear(1),
                            'comment' => $result['result']['transactions']['amount'] . ' TopUpped',
                            'performed_by_balance_before' => $staff->balance,
                            'performed_by_balance_after' => $newBalance,
                            'performed_to_balance_before' => $staff->balance,
                            'performed_to_balance_after' => $newBalance,
                            'performed_by' => $staff->id,
                            'performed_to' => $staff->id,
                            'type' => Transaction::TYPE_TOP_UP,
                            'release_at' => Carbon::now()->addDay(2),
                            'currency' => 'USD',
                            'status' => Transaction::STATUS_APPROVED_TRANSACTION,
                        ]);
                        TransactionDetail::create([
                            'transaction_id' => $tr->id,
                            'payment_method' => TransactionDetail::PAYMENT_METHOD_PAYPAL,
                            'raw' => json_encode($result['result'])
                        ]);
                        \DB::commit();
                        return \Redirect::to('/' . \Session::get('lang') . '/')->with('status', 'Amount added successfully...!');
                    } catch (\Exception $e) {
                        \DB::rollback();
                        return \Redirect::to('/' . \Session::get('lang') . '/')->with('status', $e->getMessage());
                    }
                } else {
                    dump($result);
                    dump('*-*-*-*-*-*-*-*-*-*-*-*-*-*-');
                    dump($this->getPaymentDetail($result['result']['id']));
                    dump('*-*-*-*-*-*-*-*-*-*-*-*-*-*-');
                    dump('errors');
                    dd(json_encode($result['result']));
                }
            } else {
                if ($result['status'] == 'approved') {
//                    $job = (new \App\Jobs\PayPalSuccessPayment(Auth::user()->id, json_encode($result['result'])))->onQueue(config('jobs-queue-names.ApprovePayPal'));
//                    dispatch($job);
                    try {
                        \DB::beginTransaction();
                        $newBalance = Auth::user()->account_balance + $result['result']['transactions']['amount'];
                        $tr = Transaction::create([
                            'name' => 'Topup Amount',
                            'amount' => $result['result']['transactions']['amount'],
                            'milestone_id' => 0,
                            'due_date' => Carbon::now()->addYear(1),
                            'comment' => $result['result']['transactions']['amount'] . ' TopUpped',
                            'performed_by_balance_before' => Auth::user()->account_balance,
                            'performed_by_balance_after' => $newBalance,
                            'performed_to_balance_before' => Auth::user()->account_balance,
                            'performed_to_balance_after' => $newBalance,
                            'performed_by' => Auth::user()->id,
                            'performed_to' => Auth::user()->id,
                            'type' => Transaction::TYPE_TOP_UP,
                            'currency' => 'USD',
                            'status' => Transaction::STATUS_APPROVED_TRANSACTION,
                        ]);
                        TransactionDetail::create([
                            'transaction_id' => $tr->id,
                            'payment_method' => TransactionDetail::PAYMENT_METHOD_PAYPAL,
                            'raw' => json_encode($result['result'])
                        ]);
                        User::where('id', '=', Auth::user()->id)->update([
                            'account_balance' => $newBalance
                        ]);
                        \DB::commit();
                        return redirect('/myAccount')->with('status', 'Amount added successfully...!');
                    } catch (\Exception $e) {
                        \DB::rollback();
                        return redirect('/myAccount')->with('status', $e->getMessage());
                    }
                } else {
                    dump($result);
                    dump('*-*-*-*-*-*-*-*-*-*-*-*-*-*-');
                    dump($this->getPaymentDetail($result['result']['id']));
                    dump('*-*-*-*-*-*-*-*-*-*-*-*-*-*-');
                    dump('errors');
                    dd(json_encode($result['result']));
                }
            }
		}

		public function cancelPayPal(Request$request)
		{
				return \redirect()->to('/');
		}

		public function withdrawPayPal(Request $request)
		{
			$data = UserPaymentMethod::where('user_id', '=', \Auth::user()->id)
							->where('payment_method', '=', TransactionDetail::PAYMENT_METHOD_PAYPAL)
							->first();
			if(is_null($data)) {
				return redirect()->back()->with('error', 'Please add account with PayPal to withdraw...!');
			}

			$request->request->add([
				'email' => $data->email
			]);
//			$tD = $this->withdraw($request);

            $job = (new \App\Jobs\WithdrawAmount(Auth::user()->id, $request->all(), $data->email))->onQueue(config('jobs-queue-names.WithdrawAmount'));
            dispatch($job);

//			\DB::beginTransaction();
//			$newBalance = Auth::user()->account_balance - $request->amount;
//			$transaction = Transaction::create([
//				'name' => 'Amount Withdraw',
//				'amount' => $request->amount,
//				'milestone_id' => 0,
//				'due_date' => Carbon::now()->addDay(10),
//				'comment' => 'Amount Withdrawal of ' . $request->amount,
//				'balance_before' => Auth::user()->account_balance,
//				'balance_after' => $newBalance,
//				'performed_by_balance_before' => Auth::user()->account_balance,
//				'performed_by_balance_after' => $newBalance,
//				'performed_to_balance_before' => Auth::user()->account_balance,
//				'performed_to_balance_after' => $newBalance,
//				'performed_by' => Auth::user()->id,
//				'performed_to' => Auth::user()->id,
//				'type' => Transaction::TYPE_WITHDRAW,
//				'currency' => 'USD',
//				'release_at' => Carbon::now(),
//				'status' => Transaction::STATUS_IN_PROGRESS_TRANSACTION
//			]);
//			$td = TransactionDetail::create([
//				'transaction_id' => $transaction->id,
//				'payment_method' => TransactionDetail::PAYMENT_METHOD_PAYPAL,
//				'raw' => json_encode($tD),
//			]);
//			User::where([
//				'id' => Auth::user()->id
//			])->update([
//				'account_balance' => $newBalance
//			]);
//			$job = (new CheckPayPalWithdrawStatus($request->all(), $tD, Auth::user()->id, $transaction->id))->delay(60 * 10)->onQueue('PayPalWithdrawStatus'); //delayed for 10 mins
//			$this->dispatch($job);
//			\DB::commit();
			return redirect()->back()->with('message', 'Your withdrawal is in process...!');

		}

		public function withdrawPayPalDetail(Request $request)
		{
			$data = $this->getWithdrawDetail($request->batch_id);

			dd($data->getItems()[0]->getPayoutItem()->getAmount()->getValue());
		}
}
