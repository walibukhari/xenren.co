<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Schema\Blueprint;

use App\Http\Requests;

class LocationsController extends Controller
{
    static function renameOldTable()
    {
        \Schema::rename('countries', 'x_countries');
        \Schema::rename('country_cities', 'x_country_cities');
    }

    static function createCountriesTable()
    {
        try {
            \Schema::create('countries', function (Blueprint $table) {
                $table->increments('id');
                $table->string('code');
                $table->string('name');
                $table->string('name_cn');
                $table->timestamps();
            });
        } catch (\Exception $e){
            dump($e->getLine());
            dump($e->getFile());
            dd($e->getMessage());
        }
    }

    static function createStatesTable()
    {
        try {
            \Schema::create('country_states', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->integer('country_id');
                $table->timestamps();
            });
        } catch (\Exception $e){
            dump($e->getLine());
            dump($e->getFile());
            dd($e->getMessage());
        }
    }

    static function createCitiesTable()
    {
        try {
            \Schema::create('country_cities', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('lon');
                $table->string('lat');
                $table->integer('state_id');
                $table->timestamps();
            });
        } catch (\Exception $e){
            dump($e->getLine());
            dump($e->getFile());
            dd($e->getMessage());
        }
    }
}
