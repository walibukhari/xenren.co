<?php

namespace App\Http\Controllers;

use App\Constants;
use App\Models\Category;
use App\Models\Countries;
use App\Models\ExchangeRate;
use App\Models\Language;
use App\Models\OfficialProject;
use App\Models\Settings;
use App\Models\Skill;
use App\Models\UserSkill;
use App\Models\WorldCountries;
use Carbon;
use Illuminate\Http\Request;

use Auth;
use App\Models\Project;
use App\Models\ProjectApplicant;
use App\Models\ProjectApplicantAnswer;
use App\Models\ProjectChatRoom;
use App\Models\ProjectChatFollower;

use App\Models\ThumbdownJob;

use App\Http\Requests\Frontend\JobApplyFormRequest;
use Illuminate\Support\Facades\Session;

class JobsController extends Controller
{
    public function getFilterValue(Request $request, $id)
    {
        $data = $this->getFilters($id);
        return makeJSONResponse(true, 'Success', ['c' => $data]);
    }

    protected function getFilters($id)
    {
        $data = array();
        switch ($id) {
            case 1:
                $c = Category::getCategories(1);
                foreach ($c->get() as $key => $var) {
                    $data[$var->id] = $var->getTitle();
                }
                break;
            case 2:
                $data = Constants::getMaterialLists();
                break;
            case 3:
                $data = Constants::getBodyShapeLists();
                break;
            case 4:
                $c = Category::getCategories(2);
                foreach ($c->get() as $key => $var) {
                    $data[$var->id] = $var->getTitle();
                }
                break;
        }

        return $data;
    }

    public function oldIndex(Request $request)
    {
        $models = Project::with(['creator', 'projectQuestions'])
            ->where('type', Project::PROJECT_TYPE_COMMON);

        $filter = array();
        $filter['type'] = null;
        $filter['order'] = null;
        $filter['advance_filter'] = null;

        //Check this only if user is tailor
        if (!$request->has('type') || $request->get('type') == 0) {
            //Only show order that match tailor skills
        } else {
            $filter['type'] = $request->get('type');
        }

        if ($request->has('order') && $request->get('order') != '') {
            switch ($request->get('order')) {
                case 1:
                    $filter['order'] = 1;
                    $models->orderBy('created_at', 'ASC');
                    break;
                case 2:
                    $filter['order'] = 2;
                    $models->orderBy('created_at', 'DESC');
                    break;
                case 3:
                    $filter['order'] = 3;
                    $models->orderBy('', 'ASC');
                    break;
                case 4:
                    $filter['order'] = 4;
                    $models->orderBy('', 'DESC');
                    break;
            }
        }

        if ($request->has('advance_filter') && $request->get('advance_filter') != '') {
            $filter['advance_filter'] = $request->get('advance_filter');
        }

        $type_url = route('frontend.tailororders') . '?';
        $order_url = route('frontend.tailororders') . '?';
        $advance_url = route('frontend.tailororders') . '?';

        if ($filter['order'] != null) {
            $type_url .= 'order=' . $filter['order'] . '&';
            $advance_url .= 'order=' . $filter['order'] . '&';
        }
        if ($filter['type'] != null) {
            $order_url .= 'type=' . $filter['type'] . '&';
            $advance_url .= 'type=' . $filter['type'] . '&';
        }

        if ($filter['advance_filter'] != null) {
            $type_url .= 'advance_filter=' . $filter['advance_filter'] . '&';
            $order_url .= 'advance_filter=' . $filter['advance_filter'] . '&';
        }

        $advance_filter_json = array();
        //Build the advance filter json and add the query
        if ($filter['advance_filter'] != null) {
            $filters = explode(',', $filter['advance_filter']);
            foreach ($filters as $f) {
                list($type, $value) = explode('|', $f);

                $datas = $this->getFilters($type);
                if (isset($datas[$value])) {
                    $text = '';
                    switch ($type) {
                        case 1:
                            $text = trans('order.style');
//                            $orders->where('style_id', '=', $value);
                            break;
                        case 2:
                            $text = trans('order.material');
//                            $orders->where('material', '=', $value);
                            break;
                        case 3:
                            $text = trans('order.body_shape');
//                            $orders->where('body_shape', '=', $value);
                            break;
                        case 4:
                            $text = trans('order.top_bottom');
//                            $orders->where('top_bottom_id', '=', $value);
                            break;
                    }
                    $advance_filter_json[] = array(
                        'value' => $type . '|' . $value,
                        'text' => $text . ':' . $datas[$value],
                    );
                }
            }
        }

        $user = \Auth::guard('users')->user();

        $models = $models->orderBy('created_at', 'desc');


        $officialProjects = OfficialProject::with(['projectPositions', 'projectImages'])
            ->GetRecords()
            ->paginate(10);


        return view('frontend.jobs.index', [
            'models' => $models->paginate(10),
            'user' => $user,
            'filter' => $filter,
            'type_url' => $type_url,
            'order_url' => $order_url,
            'advance_url' => $advance_url,
            'advance_filter_json' => $advance_filter_json,
            'officialProjects' => $officialProjects
        ]);
    }

    public function delete(Request $request)
    {
        $id = (int)$request->get('id');
        $model = Project::find($id);

        if ($model->user_id == \Auth::guard('users')->user()->id) {
            //delete
            $model->delete();
        }

        return response()->json([
            'status' => 'OK'
        ]);
    }

    public function makeOfferPost(JobApplyFormRequest $request)
    {
        try {
            $userId = \Auth::guard('users')->user()->id;
            $projectId = $request->get('project_id');

            if ($request->get('qq_id') == '' && $request->get('wechat_id') == '' &&
                $request->get('skype_id') == '' && $request->get('handphone_no') == '') {
                return makeJSONResponse(false, 'Need to fill at least one of the contact info');
            }

            $count = ProjectApplicant::GetProjectApplicant()
                ->where('project_id', $projectId)
                ->where('user_id', $userId)->count();

            if ($count >= 1) {
                return makeJSONResponse(false, trans('jobs.you_already_apply_this_job_before'));
            }

            if (Project::GetProject()
                ->where('user_id', $userId)
                ->where('id', $projectId)
                ->exists()
            ) {
                return makeJSONResponse(false, trans('jobs.you_are_not_allow_to_apply_because_you_are_project_creator'));
            }

            \DB::beginTransaction();


            $model = new ProjectApplicant;
            $model->user_id = $userId;
            $model->project_id = $request->get('project_id');
            $model->status = ProjectApplicant::STATUS_APPLY; //Apply
            $model->quote_price = $request->get('price');
            $model->pay_type = $request->get('pay_type');
            $model->about_me = $request->get('about_me');
            $model->save();
            $newProjectApplicantId = $model->id;

            //when make offer, freelancer need to answer employer question
            if ($newProjectApplicantId >= 1) {
                if ($request->has('answer')) {
                    $questionIds = $request->get('questionId');
                    $answers = $request->get('answer');
                    foreach ($answers as $key => $answer) {
                        if ($answer != '') {
                            $model = new ProjectApplicantAnswer;
                            $model->project_applicant_id = $newProjectApplicantId;
                            $model->project_question_id = $questionIds[$key];
                            $model->answer = $answer;
                            $model->save();
                        }
                    }
                }
            }

            $model = \Auth::guard('users')->user();
            $Update = false;
            if ($model->qq_id != $request->get('qq_id')) {
                $model->qq_id = $request->get('qq_id');
                $Update = true;
            }

            if ($model->wechat_id != $request->get('wechat_id')) {
                $model->wechat_id = $request->get('wechat_id');
                $Update = true;
            }

            if ($model->skype_id != $request->get('skype_id')) {
                $model->skype_id = $request->get('skype_id');
                $Update = true;
            }

            if ($model->handphone_no != $request->get('handphone_no')) {
                $model->handphone_no = $request->get('handphone_no');
                $Update = true;
            }

            if ($model->about_me != $request->get('about_me')) {
                $model->about_me = $request->get('about_me');
                $Update = true;
            }

            if ($model->hourly_pay != $request->get('price') && (int)$request->get('pay_type') == ProjectApplicant::PAY_TYPE_HOURLY_PAY) {
                $model->hourly_pay = $request->get('price');
                $Update = true;
            }

            if ($Update == true) {
                $model->save();
            }

            //Project Chat Room
            $projectChatRoom = ProjectChatRoom::GetProjectChatRoom()
                ->where('project_id', $request->get('project_id'))
                ->first();

            //chat room follower
            $projectChatFollower = ProjectChatFollower::GetProjectChatFollower()
                ->where('user_id', \Auth::guard('users')->user()->id)
                ->where('project_chat_room_id', $projectChatRoom->id)
                ->first();

            if ($projectChatFollower == null) {
                $projectChatFollower = new ProjectChatFollower();
                $projectChatFollower->project_chat_room_id = $projectChatRoom->id;
                $projectChatFollower->user_id = \Auth::guard('users')->user()->id;
                $projectChatFollower->is_kick = 0;
                $projectChatFollower->save();
            }

            //update badge count
            $project = Project::find($projectId);
            $project->is_read = Project::IS_READ_NO;
            $project->save();

            \DB::commit();
            return makeJSONResponse(true, trans('common.successfully_apply'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function index(Request $request)
    {
        //for wechat autologin and redirect back
        Session::put('redirect_url', route('frontend.jobs.index'));

        $projectType = $request->get('project_type');
        if ($projectType == "") {
            $projectType = Project::PROJECT_TYPE_COMMON;
        }

        $filter = array();
        $filter['project_type'] = $projectType;
        $filter['type'] = null;
        $filter['order'] = null;
        $filter['country'] = null;
        $filter['country_name'] = null;
        $filter['language'] = null;
        $filter['language_name'] = null;
        $filter['hourly_range'] = null;
        $filter['hourly_range_name'] = null;
        $filter['advance_filter'] = null;
        $filter['skill_id_list'] = null;

        //Check this only if user is tailor
        if (!$request->has('type') || $request->get('type') == 0) {
            //Only show order that match user skills
        } else {
            $filter['type'] = $request->get('type');
        }

        //recommend job only can work when user login, else will work like search all
        //user need to login to get the user skills
        if (!Auth::guard('users')->check()) {
            $skills = Skill::all();
            $skillArray = [];
            foreach ($skills as $skill) {
                array_push($skillArray, $skill->id);
            }
        } else {
            if ($request->get('type') == 1 || $request->get('type') == null) {
                $skills = Skill::all();
                $skillArray = [];
                foreach ($skills as $skill) {
                    array_push($skillArray, $skill->id);
                }
            } else {
                $userId = Auth::guard('users')->user()->id;
                $userSkills = UserSkill::where('user_id', $userId)->get();
                $skillArray = [];
                foreach ($userSkills as $userSkill) {
                    array_push($skillArray, $userSkill->skill_id);
                }
            }
        }

        $countries = WorldCountries::get()->sortBy('name');
        $languages = Language::getLanguageLists();
        $hourlyRanges = Constants::getHourlyRange();

        if ($projectType == Project::PROJECT_TYPE_COMMON) {
            $keywords = Project::getCommonProjectKeywords();

            //only allow white skill
            $skillList = Skill::getSearchSkillData(Skill::SEARCH_TYPE_WHITE);

            if (Auth::guard('users')->check()) {

                $thumbdownJobId = [];
                $thumbdownJobList = ThumbdownJob::select('project_id')
                    ->where('user_id', \Auth::guard('users')->user()->id)
                    ->where('project_type', $projectType)
                    ->get();

                foreach ($thumbdownJobList as $key) {
                    array_push($thumbdownJobId, $key->project_id);
                }

                $commonProjects = Project::with(['creator', 'projectQuestions', 'projectSkills', 'projectCountries', 'projectLanguages',  'projectLocation'])
                    ->where('type', Project::PROJECT_TYPE_COMMON)
                    ->where('freelance_type', '!=', Project::FREELANCER_TYPE_INVITED_USER)
                    ->whereNotIn('id', $thumbdownJobId)
                    ->whereIn('status', [
                        Project::STATUS_PUBLISHED,
                        Project::STATUS_HIRED,
                        Project::STATUS_SELECTED,
                    ]);

            } else {

                $commonProjects = Project::with(['creator', 'projectQuestions', 'projectSkills', 'projectCountries', 'projectLanguages', 'projectLocation'])
                    ->where('type', Project::PROJECT_TYPE_COMMON)
                    ->where('freelance_type', '!=', Project::FREELANCER_TYPE_INVITED_USER)
                    ->whereIn('status', [
                        Project::STATUS_PUBLISHED,
                        Project::STATUS_HIRED,
                        Project::STATUS_SELECTED,
                    ])// ->whereNotIn('id', $thumbdownJobId)
                ;

            }


            //not need show the project creator who account being freeze
            $commonProjects->whereHas('creator', function ($query) use ($skillArray) {
                $query->where('is_freeze', 0);
            });

            switch ($request->get('type')) {
                case 1:
                    $filter['type'] = $request->get('type');
                    break;
                case 2:
                    //Only show jobs that match user skills
                    $commonProjects->whereHas('projectSkills', function ($query) use ($skillArray) {
                        $query->whereIn('skill_id', $skillArray);
                    });
                    break;
                case 3:
                    //fixed price project
                    $commonProjects->where('pay_type', Project::PAY_TYPE_FIXED_PRICE);
                    break;
                case 4:
                    //hourly price project
                    $commonProjects->where('pay_type', Project::PAY_TYPE_HOURLY_PAY);
                    break;
                default:
//                    $commonProjects->whereHas('projectSkills', function ($query) use ($skillArray) {
//                        $query->whereIn('skill_id', $skillArray);
//                    });
                    $filter['type'] = 1;
                    break;
            }

            if ($request->has('order') && $request->get('order') != '') {
                switch ($request->get('order')) {
                    case 1:
                        $filter['order'] = 1;
                        $commonProjects->orderBy('created_at', 'DESC');
                        break;
                    case 2:
                        $filter['order'] = 2;
                        $commonProjects->orderBy('created_at', 'ASC');
                        break;
                    case 5:
                        $filter['order'] = 5;
                        $commonProjects->orderBy('reference_price', 'ASC');
                        break;
                    case 6:
                        $filter['order'] = 6;
                        $commonProjects->orderBy('reference_price', 'DESC');
                        break;
                }
            } else {
                $commonProjects->orderBy('created_at', 'DESC');
            }

            if ($request->has('advance_filter') && $request->get('advance_filter') != '') {
                $filter['advance_filter'] = $request->get('advance_filter');
            }

            if ($request->has('skill_id_list') && $request->get('skill_id_list') != '') {
                $advanceSearchSkill = explode(',', $request->get('skill_id_list'));

                $commonProjects->whereHas('projectSkills', function ($query) use ($advanceSearchSkill) {
                    $query->whereIn('skill_id', $advanceSearchSkill);
                });

                $filter['skill_id_list'] = $request->get('skill_id_list');
            }

            $type_url = route('frontend.jobs.index') . '?project_type=' . $projectType . '&';
            $order_url = route('frontend.jobs.index') . '?project_type=' . $projectType . '&';
            $advance_url = route('frontend.jobs.index') . '?project_type=' . $projectType . '&';

            if ($filter['order'] != null) {
                $type_url .= 'order=' . $filter['order'] . '&';
                $advance_url .= 'order=' . $filter['order'] . '&';
            } else {
                $advance_url .= 'order=1&';
            }

            if ($filter['type'] != null) {
                $order_url .= 'type=' . $filter['type'] . '&';
                $advance_url .= 'type=' . $filter['type'] . '&';
            } else {
                $advance_url .= 'type=1&';
            }

            if ($filter['advance_filter'] != null) {
                $type_url .= 'advance_filter=' . $filter['advance_filter'] . '&';
                $order_url .= 'advance_filter=' . $filter['advance_filter'] . '&';
            }

            if ($filter['skill_id_list'] != null) {
                $type_url .= 'skill_id_list=' . $filter['skill_id_list'] . '&';
                $order_url .= 'skill_id_list=' . $filter['skill_id_list'] . '&';
            }
            $advance_filter_json = array();
            //Build the advance filter json and add the query
            if ($filter['advance_filter'] != null) {
                $filters = explode(',', $filter['advance_filter']);
                foreach ($filters as $f) {
                    if ($f != "") {
                        list($type, $value) = explode('|', $f);
                        switch ($type) {
                            case 1:
                                $typeName = trans('common.country');
                                $valueText = WorldCountries::find($value)->getName();
//                                $commonProjects = $commonProjects->where('country_id', $value);
                                $commonProjects = $commonProjects->whereHas('projectCountries', function ($query) use ($value) {
                                    $query->where('country_id', $value);
                                });
                                $filter['country'] = $value;
                                $filter['country_name'] = $valueText;
                                break;
                            case 2:
                                $typeName = trans('common.language_2');
                                $valueText = Language::find($value)->getName();
//                                $commonProjects = $commonProjects->where('language_id', $value);
                                $commonProjects = $commonProjects->whereHas('projectLanguages', function ($query) use ($value) {
                                    $query->where('language_id', $value);
                                });
                                $filter['language'] = $value;
                                $filter['language_name'] = $valueText;
                                break;
                            case 3:
                                $typeName = trans('common.hourly_range');
                                $valueText = Project::translateHourlyRange($value);
                                $commonProjects = $commonProjects->where('pay_type', Project::PAY_TYPE_HOURLY_PAY);

                                $startValue = 0;
                                $endValue = 0;

                                switch ($value) {
                                    case 1:
                                        $startValue = 1;
                                        $endValue = 10;
                                        break;
                                    case 2:
                                        $startValue = 11;
                                        $endValue = 20;
                                        break;
                                    case 3:
                                        $startValue = 21;
                                        $endValue = 30;
                                        break;
                                    case 4:
                                        $startValue = 31;
                                        $endValue = 40;
                                        break;
                                    case 5:
                                        $startValue = 41;
                                        $endValue = 50;
                                        break;
                                    case 6:
                                        $startValue = 51;
                                        $endValue = 20000;
                                        break;
                                }

                                if (app()->getLocale() == "cn") {
                                    $exchangeRate = ExchangeRate::getExchangeRate(Constants::CNY, Constants::USD);
                                    $startValue = $startValue * $exchangeRate;
                                    $endValue = $endValue * $exchangeRate;
                                }

                                $commonProjects = $commonProjects->whereBetween('reference_price', array($startValue, $endValue));
                                $filter['hourly_range'] = $value;
                                $filter['hourly_range_name'] = $valueText;
                        }
                        $advance_filter_json[] = array(
                            'type' => $type,
                            'value' => $value,
                            'text' => "<b>" . $typeName . '</b> : ' . $valueText
                        );
                    }
                }
            }

            $totalCount = $commonProjects->count();
            $commonProjects = $commonProjects->paginate(10);
            $adminSetting = getAdminSetting(Settings::HOURLY_ON_OFF);
            return view('frontend.jobs.index', [
                'commonProjects' => $commonProjects,
                'totalCount' => $totalCount,
                'projectType' => $projectType,
                'filter' => $filter,
                'type_url' => $type_url,
                'order_url' => $order_url,
                'advance_url' => $advance_url,
                'keywords' => $keywords,
                'advance_filter_json' => $advance_filter_json,
                'countries' => $countries,
                'languages' => $languages,
                'hourlyRanges' => $hourlyRanges,
                'skillList' => $skillList,
                'adminSetting' => $adminSetting
            ]);
        }


    }


    public function officialProjects(Request $request)
    {
        //for wechat autologin and redirect back
        Session::put('redirect_url', route('frontend.jobs.index'));

        $projectType = $request->get('project_type');
        if ($projectType == "") {
            $projectType = Project::PROJECT_TYPE_COMMON;
        }

        $filter = array();
        $filter['project_type'] = $projectType;
        $filter['type'] = null;
        $filter['order'] = null;
        $filter['country'] = null;
        $filter['country_name'] = null;
        $filter['language'] = null;
        $filter['language_name'] = null;
        $filter['hourly_range'] = null;
        $filter['hourly_range_name'] = null;
        $filter['advance_filter'] = null;
        $filter['skill_id_list'] = null;

        //Check this only if user is tailor
        if (!$request->has('type') || $request->get('type') == 0) {
            //Only show order that match user skills
        } else {
            $filter['type'] = $request->get('type');
        }

        //recommend job only can work when user login, else will work like search all
        //user need to login to get the user skills
        if (!Auth::guard('users')->check()) {
            $skills = Skill::all();
            $skillArray = [];
            foreach ($skills as $skill) {
                array_push($skillArray, $skill->id);
            }
        } else {
            if ($request->get('type') == 1 || $request->get('type') == null) {
                $skills = Skill::all();
                $skillArray = [];
                foreach ($skills as $skill) {
                    array_push($skillArray, $skill->id);
                }
            } else {
                $userId = Auth::guard('users')->user()->id;
                $userSkills = UserSkill::where('user_id', $userId)->get();
                $skillArray = [];
                foreach ($userSkills as $userSkill) {
                    array_push($skillArray, $userSkill->skill_id);
                }
            }
        }

        $countries = WorldCountries::get();
        $languages = Language::getLanguageLists();
        $hourlyRanges = Constants::getHourlyRange();

        if ($projectType == Project::PROJECT_TYPE_OFFICIAL) {
            $keywords = Project::getOfficialProjectKeywords();

            //only allow white skill
            $skillList = Skill::getSearchSkillData(Skill::SEARCH_TYPE_GOLD);

            // $officialProjects = OfficialProject::with(['projectPositions', 'projectImages', 'project', 'project.projectSkills']);

            if (Auth::guard('users')->check()) {

                $thumbdownJobId = [];
                $thumbdownJobList = ThumbdownJob::select('project_id')
                    ->where('user_id', \Auth::guard('users')->user()->id)
                    ->where('project_type', $projectType)
                    ->get();

                foreach ($thumbdownJobList as $key) {
                    array_push($thumbdownJobId, $key->project_id);
                }

                $officialProjects = OfficialProject::with(['projectPositions', 'projectImages', 'project', 'project.projectSkills'])
                    ->whereNotIn('id', $thumbdownJobId)
                ;

            } else {
                $officialProjects = OfficialProject::with(['projectPositions', 'projectImages', 'project', 'project.projectSkills']);
            }

            if (empty($request->get('type')) || $request->get('type') == 1) {
                //Only show jobs that match user skills
                $officialProjects->whereHas('project.projectSkills', function ($query) use ($skillArray) {
                    $query->whereIn('skill_id', $skillArray);
                });
                $filter['type'] = 1;

            } else {
                $filter['type'] = $request->get('type');
            }

            if ($request->has('order') && $request->get('order') != '') {
                switch ($request->get('order')) {
                    case 1:
                        $filter['order'] = 1;
                        $officialProjects->orderBy('created_at', 'ASC');
                        break;
                    case 2:
                        $filter['order'] = 2;
                        $officialProjects->orderBy('created_at', 'DESC');
                        break;
                    case 3:
                        $filter['order'] = 3;
                        $officialProjects->orderBy('stock_total', 'ASC');
                        break;
                    case 4:
                        $filter['order'] = 4;
                        $officialProjects->orderBy('stock_total', 'DESC');
                        break;
                }
            } else {
                $officialProjects->orderBy('created_at', 'DESC');
            }

            if ($request->has('advance_filter') && $request->get('advance_filter') != '') {
                $filter['advance_filter'] = $request->get('advance_filter');
            }

            if ($request->has('skill_id_list') && $request->get('skill_id_list') != '') {
                $advanceSearchSkill = explode(',', $request->get('skill_id_list'));

                $officialProjects->whereHas('project.projectSkills', function ($query) use ($advanceSearchSkill) {
                    $query->whereIn('skill_id', $advanceSearchSkill);
                });

                $filter['skill_id_list'] = $request->get('skill_id_list');
            }

            $type_url = route('frontend.jobs.official', \Session::get('lang')) . '?project_type=' . $projectType . '&';
            $order_url = route('frontend.jobs.official', \Session::get('lang')) . '?project_type=' . $projectType . '&';
            $advance_url = route('frontend.jobs.official', \Session::get('lang')) . '?project_type=' . $projectType . '&';

            if ($filter['order'] != null) {
                $type_url .= 'order=' . $filter['order'] . '&';
                $advance_url .= 'order=' . $filter['order'] . '&';
            } else {
                $advance_url .= 'order=1&';
            }

            if ($filter['type'] != null) {
                $order_url .= 'type=' . $filter['type'] . '&';
                $advance_url .= 'type=' . $filter['type'] . '&';
            } else {
                $advance_url .= 'type=1&';
            }

            if ($filter['advance_filter'] != null) {
                $type_url .= 'advance_filter=' . $filter['advance_filter'] . '&';
                $order_url .= 'advance_filter=' . $filter['advance_filter'] . '&';
            }

            $advance_filter_json = array();
            //Build the advance filter json and add the query
            if ($filter['advance_filter'] != null) {
                $filters = explode(',', $filter['advance_filter']);
                foreach ($filters as $f) {
                    if ($f != "") {
                        list($type, $value) = explode('|', $f);
                        switch ($type) {
                            case 1:
                                $typeName = trans('common.country');
                                $valueText = WorldCountries::find($value)->getName();
                                $officialProjects = $officialProjects->whereHas('project', function ($query) use ($value) {
                                    $query->where('country_id', $value);
                                });
                                $filter['country'] = $value;
                                $filter['country_name'] = $valueText;
                                break;
                            case 2:
                                $typeName = trans('common.language_2');
                                $valueText = Language::find($value)->getName();
                                $officialProjects = $officialProjects->whereHas('project', function ($query) use ($value) {
                                    $query->where('language_id', $value);
                                });
                                $filter['language'] = $value;
                                $filter['language_name'] = $valueText;
                                break;
                        }
                        $advance_filter_json[] = array(
                            'type' => $type,
                            'value' => $value,
                            'text' => "<b>" . $typeName . '</b> : ' . $valueText
                        );
                    }
                }
            }

            $totalCount = $officialProjects->count();
            $officialProjects = $officialProjects->paginate(10);

            return view('frontend.jobs.index', [
                'officialProjects' => $officialProjects,
                'totalCount' => $totalCount,
                'projectType' => $projectType,
                'filter' => $filter,
                'type_url' => $type_url,
                'order_url' => $order_url,
                'advance_url' => $advance_url,
                'keywords' => $keywords,
                'advance_filter_json' => $advance_filter_json,
                'countries' => $countries,
                'languages' => $languages,
                'hourlyRanges' => $hourlyRanges,
                'skillList' => $skillList,
            ]);
        }
    }

    public function getCountries()
    {

        $countries = WorldCountries::getCountryLists();

        return response()->json([
            'status' => 'OK',
            'data' => $countries
        ]);
    }

    public function getLanguages()
    {

        $languages = Language::getLanguageLists();

        return response()->json([
            'status' => 'OK',
            'data' => $languages
        ]);
    }

    public function getHourlyRanges()
    {
        $arr = array();
        if (app()->getLocale() == "en") {
            $currency = trans("common.usd");
        } else {
            $currency = trans("common.rmb");
        }

        $arr[1] = "1 - 10 " . $currency;
        $arr[2] = "11 - 20 " . $currency;
        $arr[3] = "21 - 30 " . $currency;
        $arr[4] = "31 - 40 " . $currency;
        $arr[5] = "41 - 50 " . $currency;
        $arr[6] = "51+" . $currency;


        return response()->json([
            'status' => 'OK',
            'data' => $arr
        ]);
    }
}
