<?php
//https://tuts.codingo.me/laravel-5-1-and-dropzone-js-auto-image-uploads-with-removal-links/
//https://github.com/codingo-me/dropzone-laravel-image-upload
namespace App\Http\Controllers;

use App\Logic\UploadFile\UploadFileRepository;
use App\Models\UploadFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;

class FileUploadController extends Controller
{
    protected $uploadfiles;

    public function __construct(UploadFileRepository $uploadFileRepository)
    {
        $this->uploadFile = $uploadFileRepository;
    }

    public function getUpload()
    {
        $uploadFiles = UploadFile::where('user_id', \Auth::guard('users')->user()->id)
                        ->where('type', UploadFile::TYPE_PORTFOLIO)
                        ->get();

        foreach( $uploadFiles as $file )
        {
            $obj['name'] = $file->filename;
            $obj['size'] = filesize( Config::get('upload-files.location') . $file->filename);
            $obj['type'] = $file->getFileType();
            $result[] = $obj;
        }

        $jsonUploadFile = "{}";
        if( count($uploadFiles) != 0 )
        {
            $jsonUploadFile = json_encode($result);
        }

        return view('frontend.test.uploadFile', [
            'jsonUploadFile' => $jsonUploadFile
        ]);
    }

    public function postUpload()
    {
        $formInput = Input::all();
        $response = $this->uploadFile->upload($formInput);
        return $response;

    }

    public function deleteUpload()
    {

        $filename = Input::get('id');

        if(!$filename)
        {
            return 0;
        }

        $response = $this->uploadFile->delete( $filename );

        return $response;
    }

    public function submitForm(Request $request){
        $id = 29;
        $hidUploadedFiles = $request->get('hidUploadedFile');

        foreach ( $hidUploadedFiles as $file) {
            //copy from file uploaded location to portfolio pic location
            //add record to DB
            $this->uploadFile->copy( $file, UploadFile::TYPE_PORTFOLIO, $id );

//            //delete the record from uploaded location
//            //remove record from DB
//            $this->uploadFile->delete( $file );
        }

    }
}