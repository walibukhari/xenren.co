<?php

namespace App\Http\Controllers;

use App\Models\ProjectFile;
use App\Models\Skill;
use App\Models\UserSkillComment;
use Carbon;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use ZipArchive;

class DownloadController extends Controller
{
    public function download(Request $request)
    {
        $filename = $request->get('filename');
        // Fetch the file info.
        $filePath = public_path() . '/' . $filename;

        if(file_exists($filePath)) {
            $fileName = basename($filePath);
            $fileSize = filesize($filePath);

            // Output headers.
            header("Cache-Control: private");
            header("Content-Type: application/stream");
            header("Content-Length: ".$fileSize);
            header("Content-Disposition: attachment; filename=".$fileName);

            // Output file.
            readfile ($filePath);
            exit();
        }
        else {
            die('The provided file path is not valid.');
        }
    }

    public function downloadDocuments(Request $request)
    {
        $adminReviewID = $request->get('adminReviewID');
        $adminReview = UserSkillComment::with(['files'])
            ->where('id', $adminReviewID)
            ->first();

        $files = array();
        foreach( $adminReview->files as $file)
        {
            array_push($files, public_path() . '/' . $file->path);
        }

        $dt = Carbon::now();
        $current = $dt->format('YmdHis');

        $zipFileName = "documents" . $current . ".zip";
        $zipPathName = public_path() . "/uploads/skillcomment/" . $zipFileName;
        $zip = new ZipArchive;
        $zip->open($zipPathName, ZipArchive::CREATE);
        foreach ($files as $file) {
            $zip->addFile($file);
        }
        $zip->close();

        if(file_exists($zipPathName)) {
            $fileName = basename($zipPathName);
            $fileSize = filesize($zipPathName);

            // Output headers.
            header('Content-Type: application/zip');
            header('Content-disposition: attachment; filename='. $fileName);
            header('Content-Length: ' . $fileSize);

            // Output file.
            readfile($zipPathName);

            //remove zip file
            File::delete($zipPathName);
            exit();
        }
        else {
            die('The provided file path is not valid.');
        }
    }

    public function downloadProjectDocuments(Request $request)
    {
        $projectId = $request->get('projectId');
        $projectFiles = ProjectFile::where('project_id', $projectId)
            ->get();

        $files = array();
        foreach( $projectFiles as $projectFile)
        {
            array_push($files, public_path() . '/' . $projectFile->file);
        }

        $dt = Carbon::now();
        $current = $dt->format('YmdHis');

        $zipFileName = "documents" . $current . ".zip";
        $zipPathName = public_path() . "/uploads/project/" . $projectId . "/" . $zipFileName;
        $zip = new ZipArchive;
        if($zip->open($zipPathName, ZipArchive::CREATE) !== TRUE) {
	        echo("Zip failed");
        }
        foreach ($files as $file) {
            $zip->addFile($file);
        }
        $zip->close();

        if(file_exists($zipPathName)) {
            $fileName = basename($zipPathName);
            $fileSize = filesize($zipPathName);

            // Output headers.
	          header("Pragma: public");
            header('Content-Type: application/zip');
            header('Content-disposition: attachment; filename='. $fileName);
            header('Content-Length: ' . $fileSize);
		        header("Expires: 0");
	          header("Content-Transfer-Encoding: binary");

            // Output file.
            readfile($zipPathName);
	          header("Connection: close");

            //remove zip file
//            File::delete($zipPathName);
            exit();
        }
        else {
            die('The provided file path is not valid.');
        }
    }

    public function downloadCSV($table)
    {
        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-Encoding'    => 'UTF-8'
            ,   'Content-type'        => 'text/csv; charset=UTF-8'
            ,   'Content-Disposition' => 'attachment; filename=' . $table .  '.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];

        switch($table)
        {
            case 'skill':
                $list = Skill::all()->toArray();
                break;
            default:
                die('The database table is not valid.');
                break;
        }


        # add headers for each column in the CSV download
        array_unshift($list, array_keys($list[0]));

        $callback = function() use ($list)
        {
            $FH = fopen('php://output', 'w');
            foreach ($list as $row) {
                fputcsv($FH, $row);
            }
            fclose($FH);
        };

        return Response::stream($callback, 200, $headers);
    }
}