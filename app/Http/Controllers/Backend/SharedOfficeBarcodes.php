<?php

namespace App\Http\Controllers\Backend;

use App\Models\SharedOffice;
use App\Models\SharedOfficeBarcode;
use App\Models\Staff;
use App\Models\PermissionGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Datatables;
use Illuminate\Support\Facades\Auth;

class SharedOfficeBarcodes extends Controller
{
    public function barcodes(Request $request)
    {
        if(isset($request->office) && $request->office != ""){
            $id = $request->office;
            if(\Auth::guard('staff')->user()->staff_type == Staff::STAFF_TYPE_ADMIN) {
                $sharedoffice = SharedOfficeBarcode::getBarCodes($id);
            }
        }
        else{
            if(\Auth::guard('staff')->user()->staff_type == Staff::STAFF_TYPE_ADMIN) {
                $sharedoffice = SharedOfficeBarcode::getBarCodes();
            } else {
                $staff = \Auth::guard('staff')->user();
                if ($staff->permission_group_id == PermissionGroup::PERMISSION_GROUP_SHARED_OFFICE) {
                    $sharedOffice = SharedOffice::where('staff_id', '=', $staff->id)->first();
                } else {
                    $sharedOffice = SharedOffice::where('id', $staff->office_id)->first();
                }
                
                if(is_null($sharedOffice)) {
                    $sharedoffice  = [];
                } else {
                    $sharedoffice = SharedOfficeBarcode::getBarCodes($sharedOffice->id);
                }
            }
        }
        return view('backend.sharedofficebarcode.index',[
            'sharedofficebarcode' => $sharedoffice
        ]);
    }
    public function searchBarcode(Request $request) {
        $text = $request->text;
        if(isset($text) && $text != ""){
            $sharedoffice = SharedOfficeBarcode::with(['sharedOffice' => function($q) use ($text) {
                $q->where('office_name', 'like', '%' . $text . '%');
            }])
            ->whereHas('sharedOffice', function ($q) use ($text) {
                $q->where('office_name', 'like', '%' . $text . '%');
            })
            ->paginate(10);
        } else {
            if(\Auth::guard('staff')->user()->staff_type == Staff::STAFF_TYPE_ADMIN) {
                $sharedoffice = SharedOfficeBarcode::getBarCodes();
            } else {
                $staff = \Auth::guard('staff')->user();
                if ($staff->permission_group_id == PermissionGroup::PERMISSION_GROUP_SHARED_OFFICE) {
                    $sharedOffice = SharedOffice::where('staff_id', '=', $staff->id)->first();
                } else {
                    $sharedOffice = SharedOffice::where('id', '=', $staff->office_id)->first();
                }

                if(is_null($sharedOffice)) {
                    $sharedoffice  = [];
                } else {
                    $sharedoffice = SharedOfficeBarcode::getBarCodes($sharedOffice->id);
                }
            }
        }
        return view('backend.sharedofficebarcode.index',[
            'sharedofficebarcode' => $sharedoffice
        ]);
    }

    public function barcodesById(Request $request , $id)
    {
        $sharedoffice = SharedOfficeBarcode::where('shared_office_id','=',$id)
            ->get();
        return view('backend.sharedofficebarcode.index',[
            'sharedofficebarcode' => $sharedoffice,
            'id' => $id
        ]);
    }

    public function create(Request $request)
    {
        return view('backend.sharedofficebarcode.create');
    }

    public function createPost(Request $request)
    {
        if(\Auth::guard('staff')->user()->staff_type == Staff::STAFF_TYPE_ADMIN) {
            $sharedOffice = SharedOffice::where('id', '=', $request->office_id)->first();
        } else {
            $staff = \Auth::guard('staff')->user();
            if ($staff->permission_group_id == PermissionGroup::PERMISSION_GROUP_SHARED_OFFICE) {
                $sharedOffice = SharedOffice::where('staff_id', '=', $staff->id)->first();
            } else {
                $sharedOffice = SharedOffice::where('id', '=', $staff->office_id)->first();
            }

            if(is_null($sharedOffice)){
                return redirect()->back()->with('error', 'First generate shared Office Please..!');    
            }
        }
        $exist = SharedOfficeBarcode::where('type', '=', $request->type)
        ->where('category', '=', $request->category)
        ->where('shared_office_id', '=', $sharedOffice->id)
        ->first();

        if(!is_null($exist)) {
            return redirect()->back()->with('error', 'A type can have only one barcode..!');
        }
        try {
            \DB::beginTransaction();
            SharedOfficeBarcode::create([
                'shared_office_id' => $sharedOffice->id,
                'type' => $request->type,
                'barcode' => 'https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=office:'.$sharedOffice->id.'|type:'.$request->type.'|category:'.$request->category,
                'category' => $request->category,
                'code' => $request->code,
                'password' => $request->password
            ]);
            \DB::commit();
            return redirect(route('backend.sharedofficebarcodes'))->with('message', 'Barcode Generated..!');
        } catch (\Exception $e){
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function edit($barcode)
    {
        $barcode = SharedOfficeBarcode::where('id', '=', $barcode)->first();
        return view('backend.sharedofficebarcode.edit',[
            'sharedofficebarcode' => $barcode
        ]);
    }

    public function editPost(Request $request, $barcode)
    {
        if(\Auth::guard('staff')->user()->staff_type == Staff::STAFF_TYPE_ADMIN) {
            $sharedOffice = SharedOffice::where('id', '=', $request->office_id)->first();
        } else {
            $staff = \Auth::guard('staff')->user();
            if ($staff->permission_group_id == PermissionGroup::PERMISSION_GROUP_SHARED_OFFICE) {
                $sharedOffice = SharedOffice::where('staff_id', '=', $staff->id)->first();
            } else {
                $sharedOffice = SharedOffice::where('id', '=', $staff->office_id)->first();
            }

            if (is_null($sharedOffice)) {
                return redirect()->back()->with('error', 'First generate shared Office Please..!');
            }
            
            $sharedOffice = SharedOfficeBarcode::getBarCodes($sharedOffice->id);
        }
        SharedOfficeBarcode::where('id', '=', $barcode)->update([
            'shared_office_id' => $sharedOffice->id,
            'type' => $request->type,
            'barcode' => 'https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=office:'.$sharedOffice->id.'|type:'.$request->type.'|category:'.$request->category,
            'category' => $request->category,
            'code' => $request->code,
            'password' => $request->password
        ]);
        return redirect(route('backend.sharedofficebarcodes'))->with('message', 'Barcode Updated..!');
    }
}
