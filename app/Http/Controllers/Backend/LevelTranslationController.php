<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BackendController;
use App\Http\Requests\Backend\LevelTranslationCreateFormRequest;
use App\Models\Level;
use App\Models\LevelTranslation;
use Datatables;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LevelTranslationController extends BackendController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id = null)
    {
        return view('backend.leveltranslations.index');
    }

    public function indexDt()
    {
        //
        $model = LevelTranslation::getLevelTranslation();

        return Datatables::of($model)
            ->addColumn('names', function ($model) {
                return $model->getNames();
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = '';
                $actions .= buildRemoteLinkHtml([
                    'url' => route('backend.leveltranslations.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-pencil"></i>',
                    'modal' => '#remote-modal',
                ]);
                $actions .= buildConfirmationLinkHtml([
                    'url' => route('backend.leveltranslations.delete', ['id' => $model->id]),
                    'description' => '<i class="fa fa-trash"></i>',
                    'title' => '确认删除？',
                    'content' => '确认删除此银行？'
                ]);
                return $actions;
            })
	          ->rawColumns(['actions'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('backend.leveltranslations.create');

    }

    public function createPost(LevelTranslationCreateFormRequest $request, $id = null){
        try {
            $model = new LevelTranslation();
            $model->name_cn = $request->get('name_cn');
            $model->code_name = $request->get('code_name');
            $model->name_en = $request->get('name_en');
            $model->min_experience = $request->get('experience');

            $model->save();
            return makeResponse('成功新增技能');
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = LevelTranslation::find($id);

        return view('backend.leveltranslations.edit', ['model' => $model]);
    }

    public function editPost(LevelTranslationCreateFormRequest $request, $id)
    {
        //
        $model = LevelTranslation::find($id);

        if (!$model) {
            return makeResponse('技能不存在', true);
        }

        try {
            $model->name_cn = $request->get('name_cn');
            $model->code_name = $request->get('code_name');
            $model->name_en = $request->get('name_en');
            $model->min_experience = $request->get('experience');

            $model->save();
            return makeResponse('成功编辑技能');
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $model = LevelTranslation::find($id);

        try {
            if ($model->delete()) {
                return makeResponse('成功删除技能');
            } else {
                throw new \Exception('未能删除技能，请稍候再试');
            }
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }
}
