<?php

namespace App\Http\Controllers\Backend;

use App\Models\SharedOfficeCategory;
use App\Models\Staff;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SharedOfficeCategoryController extends Controller
{

    public function index(){
        $categories = SharedOfficeCategory::get();
        $types = SharedOfficeCategory::types();
        return view('backend.sharedoffice.category.index',[
            'categories' => $categories,
            'types' => $types
        ]);
    }

    public function addCategory(Request $request){
        if($request->hasFile('image') == false) {
            return back()->with('error_message','Please Upload Image');
        }
        if($request->hasFile('image')) {
            $path = 'uploads/sharedoffice/category/';
            $fileName = $request->file('image')->getClientOriginalName();
            $request->file('image')->move($path,$fileName);
            $fileName = $path.$fileName;
        }
        SharedOfficeCategory::create([
           'name' => $request->name,
           'image' => $fileName,
           'type' => $request->type
        ]);
        return back()->with('success_message','Category Added Successfully');
    }
    public function updateCategory(Request $request) {
        if($request->hasFile('image')) {
            $path = 'uploads/sharedoffice/category/';
            $fileName = $request->file('image')->getClientOriginalName();
            $request->file('image')->move($path,$fileName);
            $fileName = $path.$fileName;
        }
        $data = SharedOfficeCategory::where('id','=',$request->id)->first();
        $name = isset($fileName) ? $fileName : $data->image;
        SharedOfficeCategory::where('id','=',$request->id)->update([
            'name' => $request->name,
            'image' => $name,
            'type' => isset($request->type) ? $request->type : $data->type
        ]);
        return back()->with('success_message','Category Updated Successfully');
    }

    public function deleteCategory($id) {
        SharedOfficeCategory::where('id','=',$id)->delete();
        return back()->with('error_message','Category Deleted Successfully');
    }
}
