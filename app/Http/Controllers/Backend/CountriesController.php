<?php

namespace App\Http\Controllers\Backend;

use App\Models\Countries;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CountriesController extends Controller
{
    //

    public function index()
    {
        $data['countries'] = Countries::all();

        return view('backend.countries.index', $data);
    }


    public function post(Request $request)
    {

        if ($request->ajax()) {
            $lang = Countries::find($request->get('pk'));
            $lang->update([$request->get('name') => $request->get('value')]);

            return response()->json(['success' => true]);
        }
    }
}
