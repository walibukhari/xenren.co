<?php

namespace App\Http\Controllers\Backend;

use App\Libraries\Pushy\PushyAPI;
use App\Models\User;
use App\Models\UserLanguage;
use App\Models\UserSkill;
use App\Traits\sendPushNotification;
use Auth;
use Input;
use Datatables;
use Validator;
use DB;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;
use App\Models\PermissionGroup;
use App\Models\PermissionGroupPermission;
use App\Models\Staff;
use App\Http\Requests\Backend\StaffCreateFormRequest;
use App\Http\Requests\Backend\StaffEditFormRequest;
use App\Http\Requests\Backend\sharedOfficeOwnerRequest;
use App\Models\SharedOffice;

class StaffController extends BackendController
{
    use sendPushNotification;

    public function checkNotifications(){
        return view('backend.pushNotifications.index');
    }

    public function sendPushNotification(Request $request){
        //post Project push notifications
        $receiverId = '66';
        $users = User::where('id','=',$receiverId)->select([
            'id', 'web_token', 'post_project_notify','device_token'
        ])->where('post_project_notify', '=', 1)->get();
        $usersTokens = array();
        $usersDeviceToken = array();
        foreach ($users as $user) {
            array_push($usersTokens, $user->web_token);
            array_push($usersDeviceToken, $user->device_token);
        }
        $icon = asset('images/Favicon.jpg');


        $data = array(
            "title" => "Xenren", // Notification title
            'message' => 'Post Project ..!',
            'body' => 'New Project Posted ..!',
            "image" => $icon
        );
        $options = array(
            'notification' => array(
                'badge' => 1,
                'sound' => 'ping.aiff',
                'body' => 'New Post Project'
            )
        );
        PushyAPI::sendPushNotification($data, $usersTokens, $options);


        //send mobile notification
        $data1 = array(
            "title" => "Xenren", // Notification title
            'message' => 'Post Project ..!',
            'body' => 'New Project Posted ..!',
            "image" => $icon
        );
        $options1 = array(
            'notification' => array(
                'badge' => 1,
                'sound' => 'ping.aiff',
                'body' => 'New Post Project'
            )
        );
        PushyAPI::sendPushMobileNotification($data1, $usersDeviceToken, $options1);



        //send message push notifications
        $data2 = array(
            "title" => "Xenren", // Notification title
            'message' => 'New Message .. !',
            'body' => 'some one send you message ..!',
            "image" => $icon
        );
        $options2 = array(
            'notification' => array(
                'badge' => 1,
                'sound' => 'ping.aiff',
                'body' => 'New Message'
            )
        );
        PushyAPI::sendPushNotification($data2, $usersTokens, $options2);

        $data3 = array(
            "title" => "Xenren", // Notification title
            'message' => 'New Message .. !',
            'body' => 'some one send you message ..!',
            "image" => $icon
        );
        $options3 = array(
            'notification' => array(
                'badge' => 1,
                'sound' => 'ping.aiff',
                'body' => 'New Message'
            )
        );
        PushyAPI::sendPushMobileNotification($data3, $usersDeviceToken, $options3);




        //request contact push notification
        $data4 = array(
            "title" => "Xenren", // Notification title
            'message' => 'Request Contact Info .. !',
            'body' => 'some send you request for your contact info',
            "image" => $icon
        );
        $options4 = array(
            'notification' => array(
                'badge' => 1,
                'sound' => 'ping.aiff',
                'body' => 'Request Contact'
            )
        );
        PushyAPI::sendPushNotification($data4, $usersTokens, $options4);

        $data5 = array(
            "title" => "Xenren", // Notification title
            'message' => 'Request Contact Info .. !',
            'body' => 'some send you request for your contact info',
            "image" => $icon
        );
        $options5 = array(
            'notification' => array(
                'badge' => 1,
                'sound' => 'ping.aiff',
                'body' => 'Request Contact'
            )
        );
        PushyAPI::sendPushMobileNotification($data5, $usersDeviceToken, $options5);



        //send push notification to manager app
        $staff = Staff::where('id','=','39')->first();
        $usersTokens = array();
        array_push($usersTokens,$staff->device_token);
        $data6 = array (
            "title" => "Xenren", // Notification title
            'message' => 'New Request Received To Join Your Xenren Shared Office',
            'body' => 'Shared Office Booking Request ...!',
            "image" => $icon
        );
        $options6 = array(
            'notification' => array(
                'badge' => 1,
                'sound' => 'ping.aiff',
                'body' => "New Booking Request",
            )
        );
        PushyAPI::sendBookingNotification($data6, $usersTokens, $options6);


        return collect([
           'n1' => 'notification send successfully'
        ]);
    }

    public function resetUser(Request $request)
    {
        return view('backend.resetUser.index');
    }

    public function resetUserPost(Request $request)
    {
        $usr = User::where('email', $request->email)->first();
        if(is_null($usr)) {
            return redirect()->back()->with('error', 'User not found');
        }
        User::where('email', $request->email)->update([
            'id_card_no' => '',
            'is_first_time_skill_submit' => 0,
            'gender' => '',
            'date_of_birth' => '',
            'address' => '',
            'img_avatar' => '',
            'handphone_no' => '',
            'skype_id' => '',
            'wechat_id' => '',
            'qq_id' => '',
            'id_image_front' => '',
            'id_image_back' => '',
            'is_validated' => '',
            'device_token' => '',
            'device_os' => '',
            'user_link' => '',
            'facebook_id' => '',
            'face_image' => '',
            'hint' => '',
            'title' => '',
            'about_me' => '',
            'experience' => '',
            'is_contact_allow_view' => '',
            'line_id' => '',
            'free_notifications' => '',
            'web_token' => '',
            'post_project_notify' => '',
            'ip_country_code' => '',
            'account_balance' => 0
        ]);
        UserSkill::where('user_id', '=', $usr->id)->delete();
        UserLanguage::where('user_id', '=', $usr->id)->delete();

        return redirect()->back()->with('success', 'User data wiped...!');
    }

    public function setupPhone(){
        $staff = \Auth::guard('staff')->user();
        return view('backend.sharedoffice.phone.index',[
            'staff'=> $staff
        ]);
    }

    public function createPhoneNumber(Request $request){
        $staff = \Auth::guard('staff')->user()->id;
        Staff::where('id','=',$staff)->update([
           'phone_number' => $request->phone_number
        ]);
        SharedOffice::where('staff_id','=',$staff)->update([
           'contact_phone' =>  $request->phone_number
        ]);
        return collect([
           'status' => true,
           'message' => 'phone number added success'
        ]);
    }

    public function sharedOfficeSignUp(sharedOfficeOwnerRequest $request)
    {
        try {
            $model = new Staff();
            $model->username = $request->get('username');
            $model->email = $request->get('email');
            $model->name = $request->get('username');
            $model->password = $request->get('password');
            $model->staff_type = Staff::defaultUserType();
            $model->permission_group_id = PermissionGroup::PERMISSION_GROUP_SHARED_OFFICE;
            $model->title = Staff::Shared_OFFICE_TITLE;
            $model->save();
            $request->office_id;
            $a = SharedOffice::where('id', '=', $request->office_id)->update([
                'staff_id' => $model->id
            ]);
            return collect([
                'success' => $a
            ]);
        }
        catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function index(Request $request) {
        $users = Staff::onlyTrashed()->get();
        return view('backend.staff.index',[
            'users' => $users
        ]);
    }

    public function revertUsers($id){
        Staff::where('id','=',$id)->restore();
        $staff = Staff::where('id','=',$id)->first();
        $sharedOffice = SharedOffice::where('id','=',$staff->id)->first();
        if(!is_null($sharedOffice)) {
            SharedOffice::where('id','=',$staff->id)->restore();
        }
        return back()->with('success','User restored successfully');
    }

    public function indexDt(Request $request) {
        $user = \Auth::guard('staff')->user();
        $model = Staff::with(['permissionGroup']);

        if ($user->staff_type === Staff::STAFF_TYPE_STAFF) {
            if ($user->permission_group_id == PermissionGroup::PERMISSION_GROUP_SHARED_OFFICE) {
                $getOffice = SharedOffice::where('staff_id', '=', $user->id)->first();
                $model = $model->where('office_id', $getOffice->id);
            } else {
                $model = $model->where('office_id', $user->office_id);
            }
        }

        $model = $model->GetStaff();
        // $model = Staff::with(['permissionGroup'])->GetStaff();

        return Datatables::of($model)
            // ->editColumn('blabla', function ($model) {

            // })
            // ->addColumn('blabla2', function ($model) {

            // })
            ->addColumn('group', function ($model) {
                if ($model->permissionGroup) {
                    return $model->permissionGroup->group_name;
                } else {
                    return '-';
                }
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = buildRemoteLinkHtml([
                    'url' => route('backend.staff.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-pencil"></i>',
                    'modal' => '#remote-modal',
                ]);
                $actions .= buildConfirmationLinkHtml([
                    'url' => route('backend.staff.delete', ['id' => $model->id]),
                    'description' => '<i class="fa fa-trash"></i>',
                ]);
                return $actions;
            })
	        ->rawColumns(['actions'])
            ->make(true);
    }

    public function create(Request $request) {
        $user = \Auth::guard('staff')->user();

        if ($user->staff_type === Staff::STAFF_TYPE_STAFF) {
            // $getOffice = SharedOffice::where('staff_id', '=', $user->id)->first();
            // $permissions = PermissionGroup::where('office_id', $getOffice->id)->pluck('group_name', 'id');
            $permissions = PermissionGroupPermission::getPermissionsListsOwner();
        } else {
            $permissions = PermissionGroup::pluck('group_name', 'id');
        }

        return view('backend.staff.create', [
            'permissions' => $permissions,
            'is_owner' => $user->staff_type === Staff::STAFF_TYPE_STAFF
        ]);
    }


    public function createPost(StaffCreateFormRequest $request) {
        $user = \Auth::guard('staff')->user();
        DB::beginTransaction();
        try {
            $model = new Staff();
            $model->username = $request->get('username');
            $model->email = $request->get('email');
            $model->name = $request->get('name');
            $model->password = $request->get('password');
            $model->staff_type = Staff::defaultUserType();
            $model->permission_group_id = $request->get('permission_group_id');
            $model->title = $request->get('title');

            if ($user->staff_type === Staff::STAFF_TYPE_STAFF) {
                $model->office_id = $user->office_id;
                if ($user->permission_group_id == PermissionGroup::PERMISSION_GROUP_SHARED_OFFICE) {
                    $getOffice = SharedOffice::where('staff_id', '=', $user->id)->first();
                    $model->office_id = $getOffice->id;
                }

                if($request->is_operator != ''){
                    $model->is_operator = $request->get('is_operator');
                }

                // permission
                $listPermission = $request->get('permissions');
                if (empty($listPermission)) {
                    throw new \Exception('Permissions empty');
                }

                $permissions = array();
                foreach ($listPermission as $permission) {
                    $tmp = new PermissionGroupPermission();
                    $tmp->permission_tag = $permission;

                    $permissions[] = $tmp;
                }

                $group = new PermissionGroup();
                $group->group_name = $request->get('username');
                $group->office_id = $model->office_id;
                $group->save();

                if (!$group->permissions()->saveMany($permissions)) {
                    throw new \Exception('当前无法新增权限组，请稍后再试');
                }

                $model->permission_group_id = $group->id;
            }

            if ($request->hasFile('image')){

                $img    = \Image::make($request->file('image'));
                $mime   = $img->mime();
                $ext    = convertMimeToExt($mime);

                $path   = 'uploads/staff/';
                $filename = 'staff_' . generateRandomUniqueName();

                touchFolder($path);

                $request->file('image')->move($path, $filename.'.'.$ext);

                $input['image'] = ($path. $filename.'.'.$ext);

                $model->img_avatar = $input['image'];
            }

            $model->save();

            DB::commit();
            return makeResponse('成功新增操作员');
        } catch (\Exception $e) {
            DB::rollback();
            return makeResponse($e->getMessage(), true);
        }
    }

    public function edit($id) {
        $user = \Auth::guard('staff')->user();
        $model = Staff::getStaff()->where('id', '=', $id);

        if ($user->staff_type === Staff::STAFF_TYPE_STAFF) {
            // $getOffice = SharedOffice::where('staff_id', '=', $user->id)->first();
            // $permissions = PermissionGroup::where('office_id', $getOffice->id)->pluck('group_name', 'id');
            $permissions = PermissionGroupPermission::getPermissionsListsOwner();
            $model = $model->with(['permissionGroup.permissions'])->first();
            $selectedPermission = !empty($model->permissionGroup->permissions) ? $model->permissionGroup->permissions->pluck('permission_tag')->all() : null;
        } else {
            $permissions = PermissionGroup::pluck('group_name', 'id');
            $model = $model->with(['permissionGroup'])->first();
            $selectedPermission = null;
        }

        // if model not found
        if (is_null($model)) {
            addError('Staff not found');
            return redirect()->route("backend.staff");
        }

        return view('backend.staff.edit', [
            'model' => $model,
            'permissions' => $permissions,
            'selectedPermission' => $selectedPermission,
            'is_owner' => $user->staff_type === Staff::STAFF_TYPE_STAFF
        ]);
    }

    public function editPost(StaffEditFormRequest $request, $id) {
        $user = \Auth::guard('staff')->user();
        $model = Staff::with(['permissionGroup'])->getStaff()->where('id', '=', $id);

        if ($user->staff_type === Staff::STAFF_TYPE_STAFF) {
            if ($user->permission_group_id == PermissionGroup::PERMISSION_GROUP_SHARED_OFFICE) {
                $getOffice = SharedOffice::where('staff_id', '=', $user->id)->first();
                $model = $model->where('office_id', $getOffice->id);
            } else {
                $model = $model->where('office_id', $user->office_id);
            }
        }

        $model = $model->first();
        // if model not found
        if (is_null($model)) {
            return makeResponse("Staff not found", true);
        }

        //Check username availibility
        $username_exists = Staff::where('username', '=', $request->get('username'))->first();
        if ($username_exists && $username_exists->id != $model->id) {
            return makeResponse('登入账号已被注册', true);
        }

        //Check email availibility
        $email_exists = Staff::where('email', '=', $request->get('email'))->first();
        if ($email_exists && $email_exists->id != $model->id) {
            return makeResponse('电子邮件地址已被注册', true);
        }

        //Deny edit for admin account
        if ($model->staff_type != Staff::defaultUserType()) {
            return makeResponse('管理员帐号无法在本页修改', true);
        }

        $model->username = $request->get('username');
        $model->email = $request->get('email');
        $model->name = $request->get('name');
        if ($request->has('password') && $request->password != '') {
            $model->password = $request->get('password');
        }
        $model->title = $request->get('title');

        if ($request->hasFile('image')){

            $img    = \Image::make($request->file('image'));
            $mime   = $img->mime();
            $ext    = convertMimeToExt($mime);

            $path   = 'uploads/staff/';
            $filename = 'staff_' . generateRandomUniqueName();

            touchFolder($path);

            $request->file('image')->move($path, $filename.'.'.$ext);

            $input['image'] = ($path. $filename.'.'.$ext);
            $model->img_avatar = $input['image'];
        }

        DB::beginTransaction();
        try {
            // permission
            if ($user->staff_type === Staff::STAFF_TYPE_STAFF) {
                $listPermission = $request->get('permissions');
                if (empty($listPermission)) {
                    throw new \Exception('Permissions empty');
                }

                $permissions = array();
                foreach ($listPermission as $permission) {
                    $tmp = new PermissionGroupPermission();
                    $tmp->permission_tag = $permission;

                    $permissions[] = $tmp;
                }

                $group = PermissionGroup::find($model->permission_group_id);
                $group->group_name = $model->username;
                $group->save();

                if (!$group->permissions()->delete()) {
                    throw new \Exception('当前无法新增权限组，请稍后再试');
                }

                if (!$group->permissions()->saveMany($permissions)) {
                    throw new \Exception('当前无法新增权限组，请稍后再试');
                }

                $model->permission_group_id = $group->id;
            } else {
                $model->permission_group_id = $request->get('permission_group_id');
            }

            $model->save();

            DB::commit();
            return makeResponse('成功修改操作员资料');
        } catch (\Exception $e) {
            DB::rollback();
            return makeResponse($e->getMessage(), true);
        }
    }

    public function delete($id) {
        $user = \Auth::guard('staff')->user();
        $model = Staff::getStaff()->where('id', '=', $id);

        if ($user->staff_type === Staff::STAFF_TYPE_STAFF) {
            if ($user->permission_group_id != PermissionGroup::PERMISSION_GROUP_SHARED_OFFICE) {
                return makeResponse('No Permission', true);
            }

            $getOffice = SharedOffice::where('staff_id', '=', $user->id)->first();
            $model = $model->where('office_id', $getOffice->id);
        }

        $model = $model->first();
        $checkOffice = SharedOffice::where('staff_id','=',$model->id)->first();
        if(!is_null($checkOffice)) {
            $checkOffice->delete();
        }
        // if model not found
        if (is_null($model)) {
            return makeResponse('Staff not found', true);
        }

        try {
            if ($model->delete()) {
                return makeResponse('成功删除操作员');
            } else {
                throw new \Exception('当前无法删除操作员，请稍候再试');
            }
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }
}
