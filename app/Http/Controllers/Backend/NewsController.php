<?php

namespace App\Http\Controllers\Backend;

use Auth;
use Input;
use Datatables;
use Validator;
use DB;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;
use App\Http\Requests\Backend\SliderCreateFormRequest;
use App\Http\Requests\Backend\SliderUpdateFormRequest;
use App\Models\News;

class NewsController extends BackendController
{
    public function index(Request $request) {
        return view('backend.themes.news.index');
    }

    public function indexDt(Request $request) {
        $model = News::all();

        return Datatables::of($model)
            ->addColumn('image', function ($model) {
                $image_link = $model->image;
                $image =  "<img class='img-thumbnail news-thumb' src=".url($image_link)." >";
                return $image;
            })
            ->addColumn('title', function ($model) {
                $title = $model->title;
                return $title;
            })
            ->addColumn('sub_title', function ($model) {
                $sub_title = $model->sub_title;
                return $sub_title;
            })
            ->addColumn('link', function ($model) {
                $link = $model->link;
                return $link;
            })
            ->addColumn('actions', function ($model) {
                $actions = '<center>'.buildNormalLinkHtml([
                    'url' => route('backend.themes.news.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-pencil"></i>'
                ]);
                $actions .= buildConfirmationLinkHtml([
                    'url' => route('backend.themes.news.delete', ['id' => $model->id]),
                    'description' => '<i class="fa fa-trash"></i>',
                ]).'</center>';
                return $actions;
            })
	          ->rawColumns(['actions'])
            ->make(true);
    }

    public function create(Request $request) {
        return view('backend.themes.news.create');
    }


    public function createPost(SliderCreateFormRequest $request) {
        try {
            $input = $request->all();

            $model = new News();
            $model->title = $input['title'];
            $model->sub_title = $input['sub_title'];
            $model->link = $input['link'];
            $model->staff_id = \Auth::guard('staff')->user()->id;
            $model->pic_size = $input['pic_size'];
            $model->language = $input['language'];

            if ($request->hasFile('image')){

                $img    = \Image::make($request->file('image'));
                $mime   = $img->mime();
                $ext    = convertMimeToExt($mime);

                $path   = 'uploads/news/';
                $filename = 'news_' . generateRandomUniqueName();

                touchFolder($path);

                $request->file('image')->move($path, $filename.'.'.$ext);

                $input['image'] = ($path. $filename.'.'.$ext);
            }

            $model->image = $input['image'];
            $model->save();
            return makeResponse('成功新增新闻');
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function edit($id) {
        $model = News::find($id);

        return view('backend.themes.news.edit', ['model' => $model]);
    }

    public function editPost(SliderUpdateFormRequest $request, $id) {
        $input = $request->all();
        
        $model = News::find($id);

        if (!$model) {
            return makeResponse('新闻不存在', true);
        }

        try {
            $model->title = $input['title'];
            $model->sub_title = $input['sub_title'];
            $model->link = $input['link'];
            $model->staff_id = \Auth::guard('staff')->user()->id;
            $model->pic_size = $input['pic_size'];
            $model->language = $input['language'];

            if ($request->hasFile('image')){

                $img    = \Image::make($request->file('image'));
                $mime   = $img->mime();
                $ext    = convertMimeToExt($mime);

                $path   = 'uploads/news/';
                $filename = 'news_' . generateRandomUniqueName();

                touchFolder($path);

                $request->file('image')->move($path, $filename.'.'.$ext);

                $input['image'] = ($path. $filename.'.'.$ext);
            }
            
            if ($input['image'] == ''){
                
                $model->save();
            
            } else {

                $model->image = $input['image'];
                $model->save();
            }

            return makeResponse('成功编辑新闻');
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function delete($id) {
        $model = News::find($id);

        try {
            if ($model->delete()) {
                return makeResponse('成功删除新闻');
            } else {
                throw new \Exception('未能删除新闻，请稍候再试');
            }
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }
}
