<?php

namespace App\Http\Controllers\Backend;

use App\Models\BadWordsFilter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BadwordsController extends Controller
{
    public function index()
    {
        $data = BadWordsFilter::with('user', 'project')->orderBy('id', 'desc')->get();
        return view('backend.badWords.index',[
            'data' => $data
        ]);
    }
}
