<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UtilityController extends Controller
{
    public function downloadLog(Request $request)
    {
        $date = $request->date;
        if(!$date|| $date == '') {
            return 'Please enter date in "2020-01-28" this format';
        }
        $date = \Carbon\Carbon::parse($request->date)->toDateString();

        $filename = 'api-'.$date.'.log';
        shell_exec('cp '.storage_path().'/logs/'.$filename . ' ' . public_path());
        return redirect()->away(\URL::to('/').'/'.$filename);
    }
}
