<?php

namespace App\Http\Controllers\Backend;

use App\Models\Feedback;
use Auth;
use Input;
use Datatables;
use Validator;
use DB;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;

class FeedbackController extends BackendController
{
    public function index(Request $request, $id = null) {

        return view('backend.feedback.index');
    }

    public function indexDt(Request $request, $id = null) {
        $model = Feedback::where('id', '>', 0);

        return Datatables::of($model)
            ->editColumn('priority', function ($model) {
                return $model->translatePriority();
            })
            ->editColumn('status', function ($model) {
                return $model->translateStatus();
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = '';

                $actions .= buildRemoteLinkHtml([
                    'url' => route('backend.feedback.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-pencil"></i>',
                    'color' => 'green btn-modal-' . $model->id,
                    'modal' => '#remote-modal'
                ]);

//                $actions .= buildConfirmationLinkHtml([
//                    'url' => route('backend.officialproject.delete.post', ['id' => $model->id]),
//                    'description' => '<i class="fa fa-trash"></i>',
//                ]);
//
                $actions .= buildLinkHtml([
                    'url' => route('backend.feedback.chat', ['feedback_id' => $model->id]),
                    'color' => 'default',
                    'description' => '<i class="fa fa-users"></i>'
                ]);

                return $actions;
            })
	          ->rawColumns(['actions'])
            ->make(true);
    }

    public function edit($id) {
        $feedback = Feedback::with('files')->find($id);

        return view('backend.feedback.edit', [
            'feedback' => $feedback,
        ]);
    }

    public function editPost(Request $request, $id) {
        try{
            $feedback = Feedback::find($id);
            $feedback->status = $request->get('status');
            $feedback->save();

            return makeResponse(trans('common.submit_successful'));
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }
}
