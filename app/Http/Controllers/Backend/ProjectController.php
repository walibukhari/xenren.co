<?php

namespace App\Http\Controllers\Backend;

use App\Models\UserInbox;
use App\Models\UserInboxMessages;
use Auth;
use Input;
use Datatables;
use Validator;
use DB;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;
use App\Models\Project;
use App\Models\ProjectSkill;
use App\Models\Category;
use App\Models\Skill;
use App\Http\Requests\Backend\ProjectCreateFormRequest;
use App\Http\Requests\Backend\ProjectEditFormRequest;

class ProjectController extends BackendController
{
    public function index(Request $request, $id = null) {

        return view('backend.project.index');
    }

    public function indexDt(Request $request, $id = null) {
        $model = Project::GetProject();

        return Datatables::of($model)
            ->addColumn('names', function ($model) {
                return $model->name;
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = '';
                $actions .= buildRemoteLinkHtml([
                    'url' => route('backend.project.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-info-circle"></i>',
                    'modal' => '#remote-modal',
                    'tool-tip' => 'View Project Detail'
                ]);
                $actions .= buildConfirmationLinkHtml([
                    'url' => route('backend.project.delete', ['id' => $model->id]),
                    'description' => '<i class="fa fa-times"></i>',
                    'title' => trans('common.confirm_delete'),
                    'content' => trans('common.confirm_msg'),
                    'tool-tip' => 'Reject Project'
                ]);
                $actions .= buildRemoteLinkHtml([
                    'url' => route('backend.project.leave_message', ['id' => $model->id]),
                    'description' => '<i class="fa fa-comments"></i>',
                    'modal' => '#message-modal',
                    'tool-tip' => 'Leave Message',
                ]);
                $actions .= buildConfirmationLinkHtml([
                    'url' => route('backend.project.hide', ['id' => $model->id]),
                    'title' => trans('common.confirm_hide'),
                    'content' => trans('common.hide_msg'),
                    'description' => '<i class="fa fa-eye-slash"></i>',
                    'tool-tip' => 'Hide Project',
                    'color' => 'yellow'
                ]);
                return $actions;
            })
	          ->rawColumns(['actions'])
            ->make(true);
    }

    public function create(Request $request) {
        return view('backend.project.create');
    }

    public function hide($id) {
        Project::where('id', '=', $id)->update([
            'status' => Project::STATUS_CANCELLED
        ]);
        return makeResponse('Project Hidden...!');
    }

    public function createPost(ProjectCreateFormRequest $request, $id = null) {
        try {
            $model = new Project();
            $model->name_cn = $request->get('name_cn');
            $model->name_en = $request->get('name_en');
            $model->background_color = $request->get('background_color');
            $model->font_color = $request->get('font_color');
            $model->logo = $request->file('logo');

            $model->save();
            return makeResponse('成功新增项目');
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function edit($id) {
        $model = Project::GetProject()->find($id);

        $category = Category::where('id', $model->category_id)->get();

        $category_name = "";
        if( isset($category[0]->title_en) ){
            $category_name = $category[0]->title_en;
        }

        $projectSkillList = ProjectSkill::where('project_id', $model->id)->get();
        $skill_name_list = "";
        foreach($projectSkillList as $key=>$projectSkill){
            $skill = Skill::find($projectSkill->skill_id);

            if( $key == 0 ){
                $skill_name_list = $skill->name_en;
            }else{
                $skill_name_list .= ',' . $skill->name_en;
            }
        }

        return view('backend.project.edit', ['model' => $model, 'category_name'=>$category_name, 'skill_name_list'=>$skill_name_list]);
    }

    public function leaveMessageGet($id) {
        $model = Project::GetProject()->find($id);

        $category = Category::where('id', $model->category_id)->get();

        $category_name = "";
        if( isset($category[0]->title_en) ){
            $category_name = $category[0]->title_en;
        }

        $projectSkillList = ProjectSkill::where('project_id', $model->id)->get();
        $skill_name_list = "";
        foreach($projectSkillList as $key=>$projectSkill){
            $skill = Skill::find($projectSkill->skill_id);

            if( $key == 0 ){
                $skill_name_list = $skill->name_en;
            }else{
                $skill_name_list .= ',' . $skill->name_en;
            }
        }

        return view('backend.project.leaveMessage', ['model' => $model, 'category_name'=>$category_name, 'skill_name_list'=>$skill_name_list]);
    }

    public function leaveMessagePost(Request $request, $id)
    {
        try {
            $project = Project::where('id', '=', $id)->first();
            $inbox = UserInbox::create([
                'category' => UserInbox::CATEGORY_NORMAL,
                'project_id' => $project->id,
                'from_user_id' => \Auth::user()->id,
                'to_user_id' => $project->user_id,
                'type' => UserInbox::TYPE_PROJECT_NEW_MESSAGE,
                'is_trash' => '',
                'daily_update' => 0,
            ]);
            UserInboxMessages::create([
                'inbox_id' => $inbox->id,
                'from_user_id' => \Auth::user()->id,
                'to_user_id' => $project->user_id,
                'project_id' => $project->id,
                'is_read' => 0,
                'type' => UserInboxMessages::TYPE_PROJECT_NEW_MESSAGE,
                'custom_message' => $request->message,
            ]);
            return makeResponse('Message Sent...!');
        } catch (\Exception $e) {
            dd($e);
        }
    }

    public function editPost(ProjectEditFormRequest $request, $id) {
        $model = Project::GetProject()->find($id);

        if (!$model) {
            return makeResponse('项目不存在', true);
        }

        try {
            $model->name_cn = $request->get('name_cn');
            $model->name_en = $request->get('name_en');
            $model->background_color = $request->get('background_color');
            $model->font_color = $request->get('font_color');
            if ($request->hasFile('logo')) {
                $model->logo = $request->file('logo');
            }

            $model->save();
            return makeResponse('成功编辑项目');
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function delete($id) {
        $model = Project::GetProject()->find($id);

        try {
            if ($model->delete()) {
                return makeResponse('成功删除项目');
            } else {
                throw new \Exception('未能删除项目，请稍候再试');
            }
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }
}
