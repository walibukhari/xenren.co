<?php

namespace App\Http\Controllers\Backend;

use App\Models\Project;
use App\Models\User;
use App\Models\UserInbox;
use App\Models\UserInboxMessages;
use App\Models\UserXencoinDetail;
use Auth;
use Input;
use Datatables;
use Validator;
use DB;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;

class InboxController extends BackendController
{
    protected $inviteTOJoin;

    public function __construct()
    {
        $this->inviteTOJoin = new UserXencoinDetail();
    }


    public function index(Request $request) {
//        \Auth::guard('users')->user()->load(['myInbox', 'myInbox.fromUser', 'myInbox.messages']);
//        $myInbox = \Auth::guard('users')->user()->myInbox;
//
//        return view('frontend.inbox.index', ['myInbox' => $myInbox]);

        $category = $request->get('category');
        $isTrash = $request->get('isTrash');

        if( $isTrash == 0 && $category == 0 )
        {
            $category = UserInbox::CATEGORY_NORMAL;
        }

        $tab = 0;
        if( $category == UserInbox::CATEGORY_NORMAL )
        {
//            $myInbox = \Auth::guard('users')->user()->myInbox()
            $myInbox = UserInbox::where(function($query){
                $query->where('from_staff_id', \Auth::guard('staff')->user()->id);
                $query->orWhere('to_staff_id', \Auth::guard('staff')->user()->id);
            })
                ->where('category', UserInbox::CATEGORY_NORMAL)
                ->where('is_trash', 0)
                ->orderBy('created_at', 'desc')
                ->paginate(10);

            $actions = UserInbox::getInboxActionLists();
            $tab = 1;
        }
        else if( $category == UserInbox::CATEGORY_SYSTEM )
        {
//            $myInbox = \Auth::guard('users')->user()->myInbox()
            $myInbox = UserInbox::where(function($query){
                $query->where('from_staff_id', \Auth::guard('staff')->user()->id);
                $query->orWhere('to_staff_id', \Auth::guard('staff')->user()->id);
            })
                ->where('category', UserInbox::CATEGORY_SYSTEM)
                ->where('is_trash', 0)
                ->orderBy('created_at', 'desc')
                ->paginate(10);

            $actions = UserInbox::getInboxActionLists();
            $tab = 2;
        }
        else if( $isTrash == 1 )
        {
//            $myInbox = \Auth::guard('users')->user()->myInbox()
            $myInbox = UserInbox::where(function($query){
                $query->where('from_staff_id', \Auth::guard('staff')->user()->id);
                $query->orWhere('to_staff_id', \Auth::guard('staff')->user()->id);
            })
                ->where('is_trash', 1)
                ->orderBy('created_at', 'desc')
                ->paginate(10);

            $actions = UserInbox::getDeleteActionLists();
            $tab = 3;
        }


        return view('backend.inbox.index', [
            'myInbox' => $myInbox,
            'actions' => $actions,
            'tab' => $tab
        ]);
    }

    public function readInbox(Request $request) {
        if ($request->has('id') && $request->get('id') != '') {
            $inbox = UserInbox::find($request->get('id'));

            if ($inbox) {
                if ($inbox->to_user_id != \Auth::guard('users')->user()->id) {
                    return makeJSONResponse(false, trans('common.action_no_permission'));
                }

                $user = \Auth::guard('users')->user();

                $unread = UserInboxMessages::where('inbox_id', '=', $inbox->id)->where('to_user_id', '=', $user->id)->where('is_read', '=', 0);
                try {
                    \DB::beginTransaction();
                    $count = $unread->count();
                    if ($count > 0) {
                        $user = User::where('id', '=', \Auth::guard('users')->user()->id)->first();
                        $user->unread_message = $user->unread_message - $count;
                        if ($user->unread_message < 0) {
                            $user->unread_message = 0;
                        }
                    }

                    $user->save();
                    $unread->update(['is_read' => 1]);

                    \DB::commit();
                    return makeJSONResponse(true, 'Success', ['unread' => $user->unread_message]);
                } catch (\Exception $e) {
                    return makeJSONResponse(false, $e->getMessage());
                    \DB::rollback();
                }
            } else {
                return makeJSONResponse(false, trans('common.unknown_error'));
            }
        } else {
            return makeJSONResponse(false, trans('common.unknown_error'));
        }
    }

    public function acceptJob(Request $request)
    {
        $projectId = $request->get('projectId');
        $inboxId = $request->get('inboxId');

        try
        {
            \DB::beginTransaction();

            //check whether exist or not
            $count = ProjectApplicant::where('project_id', $projectId)
                ->where('user_id', \Auth::guard('users')->user()->id)
                ->where(function($query){
                    $query->where('status', ProjectApplicant::STATUS_APPLICANT_ACCEPTED);
                    $query->orWhere('status', ProjectApplicant::STATUS_APPLICANT_REJECTED);
                })
                ->count();

            if( $count > 0 )
            {
                return makeJSONResponse(false, trans('common.applicant_already_reply'));
            }

            $projectApplicant = ProjectApplicant::where('project_id', $projectId)
                ->where('user_id', \Auth::guard('users')->user()->id)
                ->first();
            $projectApplicant->status = ProjectApplicant::STATUS_APPLICANT_ACCEPTED;
            $projectApplicant->display_order = ProjectApplicant::DISPLAY_ORDER_PROGRESSING;
            $projectApplicant->is_read = ProjectApplicant::IS_READ_NO;
            $projectApplicant->save();

            $project = Project::find($projectId);
            $project->status = Project::STATUS_HIRED;
            $project->is_read = Project::IS_READ_NO;
            $project->save();

            $uim = new UserInboxMessages();
            $uim->inbox_id = $inboxId;
            $uim->from_user_id = \Auth::guard('users')->user()->id;
            $uim->to_user_id = $project->user_id;
            $uim->from_staff_id = null;
            $uim->to_staff_id = null;
            $uim->project_id = $project->id;
            $uim->is_read = 0;
            $uim->type = UserInboxMessages::TYPE_PROJECT_APPLICANT_ACCEPTED;
            $uim->custom_message = null;
            $uim->quote_price = null;
            $uim->pay_type = null;
            $uim->save();

            \DB::commit();
            return makeJSONResponse(true, trans('common.success_accept_job'));
        }
        catch (\Exception $e)
        {
            \DB::rollback();
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function rejectJob(Request $request)
    {
        $projectId = $request->get('projectId');
        $inboxId = $request->get('inboxId');

        try
        {
            \DB::beginTransaction();

            //check whether exist or not
            $count = ProjectApplicant::where('project_id', $projectId)
                ->where('user_id', \Auth::guard('users')->user()->id)
                ->where(function($query){
                    $query->where('status', ProjectApplicant::STATUS_APPLICANT_ACCEPTED);
                    $query->orWhere('status', ProjectApplicant::STATUS_APPLICANT_REJECTED);
                })
                ->count();

            if( $count > 0 )
            {
                return makeJSONResponse(false, trans('common.applicant_already_reply'));
            }

            $project = Project::find($projectId);
            $project->status = Project::STATUS_HIRED;
            $project->is_read = Project::IS_READ_NO;
            $project->save();

            $projectApplicant = ProjectApplicant::where('project_id', $projectId)
                ->where('user_id', \Auth::guard('users')->user()->id)
                ->first();
            $projectApplicant->status = ProjectApplicant::STATUS_APPLICANT_REJECTED;
            $projectApplicant->is_read = ProjectApplicant::IS_READ_NO;
            $projectApplicant->save();

            $uim = new UserInboxMessages();
            $uim->inbox_id = $inboxId;
            $uim->from_user_id = \Auth::guard('users')->user()->id;
            $uim->to_user_id = $project->user_id;
            $uim->from_staff_id = null;
            $uim->to_staff_id = null;
            $uim->project_id = $project->id;
            $uim->is_read = 0;
            $uim->type = UserInboxMessages::TYPE_PROJECT_APPLICANT_REJECTED;
            $uim->custom_message = null;
            $uim->quote_price = null;
            $uim->pay_type = null;
            $uim->save();

            \DB::commit();
            return makeJSONResponse(true, trans('common.success_accept_job'));
        }
        catch (\Exception $e)
        {
            \DB::rollback();
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function acceptRequestForContactInfo(Request $request)
    {
        try
        {
            $receiverId = $request->get('receiverId');
            $inboxId = $request->get('inboxId');

            $user = \Auth::guard('users')->user();
            $receiver = User::find($receiverId);
            $ui = UserInbox::find($inboxId);

            //check exist or not
            $count = UserInboxMessages::where('from_user_id', \Auth::guard('users')->user()->id)
                ->where('to_user_id', $receiver->id)
                ->where('inbox_id', $ui->id)
                ->where(function($query){
                    $query->where('type', UserInboxMessages::TYPE_PROJECT_AGREE_SEND_CONTACT);
                    $query->orWhere('type', UserInboxMessages::TYPE_PROJECT_REJECT_SEND_CONTACT);
                })
                ->count();

            if( $count > 0 )
            {
                return makeJSONResponse(false, trans('common.contact_request_already_reply'));
            }

            //send email
            $senderEmail = "support@xenren.co";
            $senderName = $user->getName();

            Mail::send('mails.acceptContactInfo', array('user' => $user), function ($message) use ($senderEmail, $senderName, $receiver) {
                $message->from($senderEmail, $senderName);
                $message->to($receiver->email, $receiver->first_name . ' ' . $receiver->last_name)
                    ->subject(trans('common.contact_info'));
            });

            \DB::beginTransaction();

            $uim = new UserInboxMessages();
            $uim->inbox_id = $ui->id;
            $uim->from_user_id = \Auth::guard('users')->user()->id;
            $uim->to_user_id = $receiver->id;
            $uim->from_staff_id = null;
            $uim->to_staff_id = null;
            $uim->project_id = $ui->project_id;
            $uim->is_read = 0;
            $uim->type = UserInboxMessages::TYPE_PROJECT_AGREE_SEND_CONTACT;
            $uim->save();

            \DB::commit();
            return makeJSONResponse(true, trans('common.success_accept_contact_request'));
        }
        catch (\Exception $e)
        {
            \DB::rollback();
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function rejectRequestForContactInfo(Request $request)
    {
        try
        {
            $receiverId = $request->get('receiverId');
            $inboxId = $request->get('inboxId');

            $user = \Auth::guard('users')->user();
            $receiver = User::find($receiverId);
            $ui = UserInbox::find($inboxId);

            //check exist or not
            $count = UserInboxMessages::where('from_user_id', \Auth::guard('users')->user()->id)
                ->where('to_user_id', $receiver->id)
                ->where('inbox_id', $ui->id)
                ->where(function($query){
                    $query->where('type', UserInboxMessages::TYPE_PROJECT_AGREE_SEND_CONTACT);
                    $query->orWhere('type', UserInboxMessages::TYPE_PROJECT_REJECT_SEND_CONTACT);
                })
                ->count();

            if( $count > 0 )
            {
                return makeJSONResponse(false, trans('common.contact_request_already_reply'));
            }

            //send email
            $senderEmail = "support@xenren.co";
            $senderName = $user->getName();

            Mail::send('mails.rejectContactInfo', array( 'user' => $user ), function($message) use ( $senderEmail, $senderName, $receiver ) {
                $message->from($senderEmail, $senderName);
                $message->to($receiver->email, $receiver->first_name.' '. $receiver->last_name )
                    ->subject(trans('common.contact_info'));
            });

            \DB::beginTransaction();

            $uim = new UserInboxMessages();
            $uim->inbox_id = $ui->id;
            $uim->from_user_id = \Auth::guard('users')->user()->id;
            $uim->to_user_id = $receiver->id;
            $uim->from_staff_id = null;
            $uim->to_staff_id = null;
            $uim->project_id = $ui->project_id;
            $uim->is_read = 0;
            $uim->type = UserInboxMessages::TYPE_PROJECT_REJECT_SEND_CONTACT;
            $uim->save();

            \DB::commit();
            return makeJSONResponse(true, trans('common.success_reject_contact_request'));
        }
        catch (\Exception $e)
        {
            \DB::rollback();
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function rejectInvite(Request $request)
    {
        try
        {
            $receiverId = $request->get('receiverId');
            $inboxId = $request->get('inboxId');

            $user = \Auth::guard('users')->user();
            $receiver = User::find($receiverId);
            $ui = UserInbox::find($inboxId);

            //check exist or not
            $count = UserInboxMessages::where('from_user_id', \Auth::guard('users')->user()->id)
                ->where('to_user_id', $receiver->id)
                ->where('inbox_id', $ui->id)
                ->where(function($query){
                    $query->where('type', UserInboxMessages::TYPE_PROJECT_INVITE_ACCEPT);
                    $query->orWhere('type', UserInboxMessages::TYPE_PROJECT_INVITE_REJECT);
                })
                ->count();

            if( $count > 0 )
            {
                return makeJSONResponse(false, trans('common.invite_request_already_reply'));
            }

            //send email
            $senderEmail = "support@xenren.co";
            $senderName = $user->getName();

            Mail::send('mails.rejectInvite', array('user' => $user), function ($message) use ($senderEmail, $senderName, $receiver) {
                $message->from($senderEmail, $senderName);
                $message->to($receiver->email, $receiver->first_name . ' ' . $receiver->last_name)
                    ->subject(trans('common.invite_for_work'));
            });

            \DB::beginTransaction();

            $uim = new UserInboxMessages();
            $uim->inbox_id = $ui->id;
            $uim->from_user_id = \Auth::guard('users')->user()->id;
            $uim->to_user_id = $receiver->id;
            $uim->from_staff_id = null;
            $uim->to_staff_id = null;
            $uim->project_id = $ui->project_id;
            $uim->is_read = 0;
            $uim->type = UserInboxMessages::TYPE_PROJECT_INVITE_REJECT;
            $uim->save();

            \DB::commit();
            return makeJSONResponse(true, trans('common.success_reject_invite_for_work'));
        }
        catch (\Exception $e)
        {
            \DB::rollback();
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function acceptSendOffer(Request $request)
    {
        try
        {
            $receiverId = $request->get('receiverId');
            $inboxId = $request->get('inboxId');

            $user = \Auth::guard('users')->user();
            $receiver = User::find($receiverId);
            $ui = UserInbox::find($inboxId);

            //check exist or not
            $count = UserInboxMessages::where('from_user_id', \Auth::guard('users')->user()->id)
                ->where('to_user_id', $receiver->id)
                ->where('inbox_id', $ui->id)
                ->where(function($query){
                    $query->where('type', UserInboxMessages::TYPE_PROJECT_ACCEPT_SEND_OFFER);
                    $query->orWhere('type', UserInboxMessages::TYPE_PROJECT_REJECT_SEND_OFFER);
                })
                ->count();

            if( $count > 0 )
            {
                return makeJSONResponse(false, trans('common.send_offer_already_reply'));
            }

            //send email
            $senderEmail = "support@xenren.co";
            $senderName = $user->getName();

            Mail::send('mails.acceptSendOffer', array('user' => $user), function ($message) use ($senderEmail, $senderName, $receiver) {
                $message->from($senderEmail, $senderName);
                $message->to($receiver->email, $receiver->first_name . ' ' . $receiver->last_name)
                    ->subject(trans('common.contact_info'));
            });

            \DB::beginTransaction();

            $uim = new UserInboxMessages();
            $uim->inbox_id = $ui->id;
            $uim->from_user_id = \Auth::guard('users')->user()->id;
            $uim->to_user_id = $receiver->id;
            $uim->from_staff_id = null;
            $uim->to_staff_id = null;
            $uim->project_id = $ui->project_id;
            $uim->is_read = 0;
            $uim->type = UserInboxMessages::TYPE_PROJECT_ACCEPT_SEND_OFFER;
            $uim->save();

            \DB::commit();
            return makeJSONResponse(true, trans('common.success_accept_send_offer'));
        }
        catch (\Exception $e)
        {
            \DB::rollback();
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function rejectSendOffer(Request $request)
    {
        try
        {
            $receiverId = $request->get('receiverId');
            $inboxId = $request->get('inboxId');

            $user = \Auth::guard('users')->user();
            $receiver = User::find($receiverId);
            $ui = UserInbox::find($inboxId);

            //check exist or not
            $count = UserInboxMessages::where('from_user_id', \Auth::guard('users')->user()->id)
                ->where('to_user_id', $receiver->id)
                ->where('inbox_id', $ui->id)
                ->where(function($query){
                    $query->where('type', UserInboxMessages::TYPE_PROJECT_ACCEPT_SEND_OFFER);
                    $query->orWhere('type', UserInboxMessages::TYPE_PROJECT_REJECT_SEND_OFFER);
                })
                ->count();

            if( $count > 0 )
            {
                return makeJSONResponse(false, trans('common.send_offer_already_reply'));
            }

            //send email
            $senderEmail = "support@xenren.co";
            $senderName = $user->getName();

            Mail::send('mails.rejectSendOffer', array( 'user' => $user ), function($message) use ( $senderEmail, $senderName, $receiver ) {
                $message->from($senderEmail, $senderName);
                $message->to($receiver->email, $receiver->first_name.' '. $receiver->last_name )
                    ->subject(trans('common.contact_info'));
            });

            \DB::beginTransaction();

            $uim = new UserInboxMessages();
            $uim->inbox_id = $ui->id;
            $uim->from_user_id = \Auth::guard('users')->user()->id;
            $uim->to_user_id = $receiver->id;
            $uim->from_staff_id = null;
            $uim->to_staff_id = null;
            $uim->project_id = $ui->project_id;
            $uim->is_read = 0;
            $uim->type = UserInboxMessages::TYPE_PROJECT_REJECT_SEND_OFFER;
            $uim->save();

            \DB::commit();
            return makeJSONResponse(true, trans('common.success_reject_send_offer'));
        }
        catch (\Exception $e)
        {
            \DB::rollback();
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function messages($inbox_id)
    {
        $inbox = UserInbox::with('messages')->find($inbox_id);

        $inboxMessages = UserInboxMessages::where('inbox_id', $inbox->id)
            ->get();
//
//        if( \Auth::guard('users')->check() )
//        {
//            $user = \Auth::guard('users')->user();
//            foreach( $inboxMessages as $inboxMessage )
//            {
//                if( $inboxMessage->to_user_id == $user->id )
//                {
//                    $inboxMessage->is_read = 1;
//                    $inboxMessage->save();
//                }
//            }
//        }

        if( \Auth::guard('staff')->check() )
        {
            $staff = \Auth::guard('staff')->user();
            foreach( $inboxMessages as $inboxMessage )
            {
                if( $inboxMessage->to_staff_id == $staff->id )
                {
                    $inboxMessage->is_read = 1;
                    $inboxMessage->save();
                }
            }
        }

        return view('backend.inbox.messages', [
            'inbox' => $inbox,
            'inboxMessages' => $inboxMessages,
//            'user' => $user
            'staff' => $staff
        ]);
    }

    public function trash(Request $request)
    {
        $inboxId = $request->get('inboxId');

        try {
            $inbox = UserInbox::find($inboxId);
            $inbox->is_trash = 1;
            $inbox->save();

            return makeJSONResponse(true, trans('common.success_move_to_trash'));

        } catch (\Exception $e) {
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function trashAll(Request $request)
    {
        $inboxIds = $request->get('inboxIds');

        try {

            foreach( $inboxIds as $inboxId)
            {
                $inbox = UserInbox::find($inboxId);
                $inbox->is_trash = 1;
                $inbox->save();
            }

            return makeJSONResponse(true, trans('common.success_move_to_trash'));

        } catch (\Exception $e) {
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function untrashAll(Request $request)
    {
        $inboxIds = $request->get('inboxIds');

        try {

            foreach( $inboxIds as $inboxId)
            {
                $inbox = UserInbox::find($inboxId);
                $inbox->is_trash = 0;
                $inbox->save();
            }

            return makeJSONResponse(true, trans('common.success_move_to_inbox'));

        } catch (\Exception $e) {
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function untrash(Request $request)
    {
        $inboxId = $request->get('inboxId');

        try {
            $inbox = UserInbox::find($inboxId);
            $inbox->is_trash = 0;
            $inbox->save();

            return makeJSONResponse(true, trans('common.success_move_to_inbox'));

        } catch (\Exception $e) {
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function markAsRead(Request $request)
    {
        $inboxIds = $request->get('inboxIds');

        try {

            foreach( $inboxIds as $inboxId)
            {
                $inbox = UserInbox::find($inboxId);

                $inboxMessages = UserInboxMessages::where('inbox_id', $inbox->id)
                    ->get();

                foreach( $inboxMessages as $inboxMessage )
                {
                    $inboxMessage->is_read = 1;
                    $inboxMessage->save();
                }
            }

            return makeJSONResponse(true, trans('common.success_mark_as_read'));

        } catch (\Exception $e) {
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function removeAll(Request $request)
    {
        $inboxIds = $request->get('inboxIds');

        try {
            \DB::beginTransaction();
            foreach( $inboxIds as $inboxId)
            {
                $inbox = UserInbox::find($inboxId);

                $inboxMessages = UserInboxMessages::where('inbox_id', $inbox->id)
                    ->get();

                foreach( $inboxMessages as $inboxMessage )
                {
                    $inboxMessage->delete();
                }

                $inbox->delete();
            }

            \DB::commit();
            return makeJSONResponse(true, trans('common.success_remove_all'));

        } catch (\Exception $e) {
            \DB::rollback();
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function sendMessage(Request $request)
    {
        $inboxId = $request->get('inboxId');
        $receiverId = $request->get('receiverId');
        $message = $request->get('message');

        if( $message == "" )
        {
            return makeJSONResponse(false, trans('common.message_can_not_empty'));
        }

        $user = \Auth::guard('staff')->user();
        $receiver = User::find($receiverId);
        $ui = UserInbox::find($inboxId);

        try
        {
            $uim = new UserInboxMessages();
            $uim->inbox_id = $inboxId;
            $uim->from_user_id = null;
            $uim->to_user_id = $receiver->id;
            $uim->from_staff_id = $user->id;
            $uim->to_staff_id = null;
            $uim->project_id = $ui->project_id;
            $uim->is_read = 0;
            $uim->type = UserInboxMessages::TYPE_PROJECT_NEW_MESSAGE;
            $uim->custom_message = $message;
            $uim->quote_price = null;
            $uim->pay_type = null;

            $uim->save();

            return makeJSONResponse(true, trans('common.success_send_message'));
        }
        catch (\Exception $e)
        {
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function chatImage($inbox_id, $receiver_id)
    {
        return view('backend.inbox.modalAddFileChat', [
            'inbox_id' => $inbox_id,
            'receiver_id' => $receiver_id
        ]);
    }

    public function chatImagePost(Request $request){
        $inboxId = $request->get('inbox-id');
        $receiverId = $request->get('receiver-id');

        $user = \Auth::guard('staff')->user();
        $ui = UserInbox::find($inboxId);

        try {
            if ($request->hasFile('file'))
            {
                $uim = new UserInboxMessages();
                $uim->inbox_id = $inboxId;
                $uim->from_user_id = null;
                $uim->to_user_id = $receiverId;
                $uim->from_staff_id = $user->id;
                $uim->to_staff_id = null;
                $uim->project_id = $ui->project_id;
                $uim->is_read = 0;
                $uim->type = UserInboxMessages::TYPE_PROJECT_FILE_UPLOAD;
                $uim->custom_message = null;
                $uim->quote_price = null;
                $uim->pay_type = null;
                $uim->file = $request->file('file');
                $uim->save();
            }
            else
            {
                return makeJSONResponse(false, trans('common.file_can_not_empty'));
            }

            return makeJSONResponse(true, trans('common.submit_successful'));
        } catch (Exception $e) {
            return makeJSONResponse(false, $e->getMessage());
        }
    }
    public function inviteToJoin(Request $request){
        $invite = $this->inviteTOJoin->getRecord();
        return view('backend.invite.inviteToJoin',[
            'data'=>$invite
        ]);
    }
}
