<?php

namespace App\Http\Controllers\Backend;

use App\Models\ProjectMilestones;
use App\Models\Settings;
use App\Models\Transaction;
use App\Models\UserIdentity;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MilestonesControllers extends Controller
{
    public function changeDateTime(Request $request)
    {
        Transaction::where('id','=',$request->id)->update([
            'release_at' => $request->date
        ]);
        return back()->with('success','release date change successfully');
    }

    public function changeAllDateTime(Request $request)
    {
        $transactions = Transaction::where('status','=',Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM)->with(['receiver.UserIdentity' => function ($q) {
            $q->where('status','=',UserIdentity::STATUS_APPROVED);
        }])
        ->get();
        if(count($transactions) == 0) {
            return back()->with('error','no found any verified users');
        }
        foreach($transactions as $trans) {
            if(isset($trans->receiver->UserIdentity)) {
                if($trans->receiver->UserIdentity->status == UserIdentity::STATUS_APPROVED ) {
                    $days = getDays('1', $request->date);
                    $date = Carbon::parse($trans->created_at);
                    $requestDate = $date->addDays($days);
                    $trans->update([
                        'release_at' => $requestDate
                    ]);
                }
            }
        }
            return back()->with('success','release date change successfully all verified users');
    }

    public function changeAllDateTimeUnVerified(Request $request)
    {
        $transactions = Transaction::where('status','=',Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM)->with(['receiver.UserIdentity' => function ($q) {
            $q->where('status','!=',UserIdentity::STATUS_APPROVED);
        }])->get();
        if(count($transactions) == 0) {
            return back()->with('error','no found any un verified users');
        }
        foreach($transactions as $trans) {
            if(!isset($trans->receiver->UserIdentity)) {
                $days = getDays('2', $request->date);
                $date = Carbon::parse($trans->created_at);
                $requestDate = $date->addDays($days);
                $trans->update([
                    'release_at' => $requestDate
                ]);
            } else {
                if($trans->receiver->UserIdentity->status != UserIdentity::STATUS_APPROVED ) {
                    $days = getDays('2', $request->date);
                    $date = Carbon::parse($trans->created_at);
                    $requestDate = $date->addDays($days);
                    $trans->update([
                        'release_at' => $requestDate
                    ]);
                }
            }
        }
        return back()->with('success','release date change successfully');
    }

    public function milestonesShow()
    {
        $transactions = Transaction::where('status','=',Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM)
            ->where('type','=',Transaction::TYPE_MILESTONE_PAYMENT)
            ->with('receiver','sender','milestone.milestoneStatus')
            ->get();
        $weeks1 = getReleaseTime('1');
        $weeks2 = getReleaseTime('2');
        return view('backend.milestonesConfirmation.show',[
            'transactions1' => $weeks1,
            'transactions2' => $weeks2
        ]);
    }
	public function milestonesConfirmation()
	{
		$transactions = Transaction::where('status', '=', Transaction::STATUS_WAITING_FOR_ADMIN_APPROVAL)->where('type','=',Transaction::TYPE_MILESTONE_PAYMENT)->with('sender', 'receiver', 'milestone.project')->where('type', '=', Transaction::TYPE_MILESTONE_PAYMENT)->get();
		return view('backend.milestonesConfirmation.index', [
			'data' => $transactions
		]);
	}

	public function approveMilestone(Request $request)
	{
		Transaction::where('id', '=', $request->id)->update([
			'status' => Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM,
			'release_at' => Carbon::parse(Carbon::now())->addDays(15)
		]);
		$paymentFeeId = $request->id + 1;
		$feeTransaction = Transaction::where('id', '=', $paymentFeeId)->where('type', '=', Transaction::TYPE_SERVICE_FEE)->first();
		if(isset($feeTransaction)) {
			Transaction::where('id', '=', $paymentFeeId)->
			update([
				'status' => Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM,
				'release_at' => Carbon::parse(Carbon::now())->addDays(15)
			]);
		}
		return redirect()->back();
	}
}
