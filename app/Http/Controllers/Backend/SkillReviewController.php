<?php

namespace App\Http\Controllers\Backend;

use App\Models\OfficialProject;
use App\Services\GeneralService;
use Auth;
use Input;
use Datatables;
use Validator;
use DB;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;
use App\Models\User;
use App\Models\UserSkill;
use App\Models\UserSkillComment;
use App\Models\UserSkillCommentSkills;
use App\Models\UserSkillCommentFiles;
use App\Models\Skill;
use App\Constants;

class SkillReviewController extends BackendController
{
    public function index(Request $request) {
        return view('backend.skillreview.index');
    }

    public function indexDt(Request $request) {
        $model = User::GetUsers();

        return Datatables::of($model)
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = buildLinkHtml([
                    'url' => route('backend.skillreview.view', ['id' => $model->id]),
                    'description' => '<i class="fa fa-wrench"></i>',
                ]);
                return $actions;
            })
	          ->rawColumns(['actions'])
            ->make(true);
    }

    public function view(Request $request, $id) {
        $model = User::with(['skills', 'skills.skill'])->find($id);

        return view('backend.skillreview.view', ['model' => $model]);
    }
    
    public function commentDt(Request $request, $id) {
        $model = UserSkillComment::with('officialProjects')
            ->where('user_id', '=', $id)->GetRecords();

        return Datatables::of($model)
            ->editColumn('project_name', function ($model) {
                if ($model->officialProjects != null)
                {
                    return $model->officialProjects->title;
                }
            })
//            ->addColumn('blabla2', function ($model) {
//
//            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) use ($id) {
                $actions = buildNormalLinkHtml([
                    'url' => route('backend.skillreview.comment.edit', ['id' => $id, 'cid' => $model->id]),
                    'description' => '<i class="fa fa-pencil"></i>',
                ]);
                $actions .= buildConfirmationLinkHtml([
                    'url' => route('backend.skillreview.comment.delete.post', ['id' => $id, 'cid' => $model->id]),
                    'description' => '<i class="fa fa-trash"></i>',
                ]);
                return $actions;
            })
	          ->rawColumns(['actions'])
            ->make(true);
    }

    public function commentCreate(Request $request, $id) {
        $model = User::find($id);

        //SEARCH_TYPE_WHITE with experience will become gold. Don't use gold type.
        //SEARCH_TYPE_GOLD is for project skill searching but not for user skill searching
        $skillList = Skill::getSearchSkillData(Skill::SEARCH_TYPE_WHITE);
        $officialProjects = OfficialProject::all();

        return view('backend.skillreview.comment_create', [
            'model' => $model,
            'skillList' => $skillList,
            'officialProjects' => $officialProjects
        ]);
    }

    public function commentCreatePost(Request $request, $id) {
	
        $model = User::find($id);

        $name = (\Session::get('lang') == 'cn') ? 'name_cn' : 'name_en';
        $skills = Skill::GetSkill()->pluck($name, 'id');

        try {
            \DB::beginTransaction();

            //Build the comment
            $c = new UserSkillComment;
            $c->user_id = $model->id;
            $c->staff_id = \Auth::guard('staff')->user()->id;
            if($request->get('official_project') == 0)
            {
                $c->official_project_id = null;
            }
            else
            {
                $c->official_project_id = $request->get('official_project');
            }
            $c->is_official = 1;
            $c->comment = $request->has('comment') && $request->get('comment') != '' ? $request->get('comment') : null;
            $c->attitude_rate = $request->has('attitude_rate') && $request->get('attitude_rate') == 1 ? 1 : 0;
            if ($c->attitude_rate == 1) {
                $c->attitude_comment = $request->has('attitude_comment') && $request->get('attitude_comment') != '' ? $request->get('attitude_comment') : null;
                $c->score_attitude = $request->has('score_attitude') && $request->get('score_attitude') != '' ? $request->get('score_attitude') : 0;
                if (!array_key_exists($c->score_attitude, Constants::getSkillScoreSelects())) {
                    throw new \Exception('错误的评分');
                }
            }

            //Build the skills
            $skill_models = array();
            $skill_count = 0;
            $score_total = 0;
            if ($request->has('skill')) {
                $skill_score = $request->get('skill_score');
                $skill_experience_action = $request->get('skill_experience_action');
                $skill_experience = $request->get('skill_experience');
                $exists = array();
                foreach ($request->get('skill') as $key => $var) {
                    if ($var != '' && isset($skill_score[$key]) && isset($skill_experience_action[$key]) && isset($skill_experience[$key])) {
                        if (array_key_exists($var, $skills->toArray())) {
                            $s = new UserSkillCommentSkills();
                            $s->user_id = $model->id;
                            $s->skill_id = $var;
                            $s->score = $skill_score[$key];
                            $s->action_type = $skill_experience_action[$key];
                            $s->experience = $skill_experience[$key];

                            if (array_key_exists($s->skill_id, $exists)) {
                                throw new \Exception('同样的技能不能评分两次');
                            }

                            $skill_models[] = $s;
                            $exists[$s->skill_id] = $s;
                            $score_total = $score_total + $s->score;
                            $skill_count++;
                        }
                    }
                }
            }

            if ($skill_count > 0) {
                $c->score_overall = $score_total / $skill_count;
            }


            //Images
            if ($request->hasFile('images')) {
                $files = $request->file('images');
                $images = array();
                foreach ($files as $key => $var) {
                    $image = new UserSkillCommentFiles();
                    $image->user_id = $model->id;
                    $image->path = $var;

                    $images[] = $image;
                }
            }

            $video = null;
            //video
		        if ($request->hasFile('video')) {
			        $files = $request->file('video');
			        $generalService = new GeneralService();
			        $video = ($generalService->uploadFile($request, '/uploads/videos', 'video'));
		        }

		        if(!is_null($video)) {
			        if ($request->hasFile('video_thumb')) {
				        $generalService = new GeneralService();
				        $video_thumb = ($generalService->uploadFile($request, '/uploads/videos', 'video_thumb'));
			        }
		          $c->video_name = $video['icon'];
		          $c->video_thumb = $video_thumb['icon'];
		        }
		        $c->github_link  = isset($request->github_link) ? $request->github_link : '';
	        
		        
            $c->save();
            foreach ($skill_models as $key => $var) {
                $var->comment_id = $c->id;
                $us = UserSkill::where('user_id', '=', $model->id)->where('skill_id', '=', $var->skill_id)->first();
                if (!$us) {
                    $us = new UserSkill();
                    $us->user_id = $model->id;
                    $us->skill_id = $var->skill_id;
                }

                if ($var->action_type == 0) {
                    $us->experience = $us->experience + $var->experience;
                } else if ($var->action_type == 1) {
                    $us->experience = $us->experience - $var->experience;
                } else {
                    throw new \Exception('未知的技能评分经验类型');
                }

                $var->save();
                $us->save();
            }

            if (isset($images) && count($images) > 0) {
                foreach ($images as $key => $var) {
                    $var->comment_id = $c->id;
                    $var->save();
                }
            }

            \DB::commit();
            return makeResponse('成功新增技能评论');
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse($e->getMessage(), true);
        }
    }

    public function commentEdit(Request $request, $id, $cid) {
        $model = User::find($id);
        $comment = UserSkillComment::find($cid);
        //SEARCH_TYPE_WHITE with experience will become gold. Don't use gold type.
        //SEARCH_TYPE_GOLD is for project skill searching but not for user skill searching
        $skillList = Skill::getSearchSkillData(Skill::SEARCH_TYPE_WHITE);
//        $skills = Skill::GetSkill()->lists('name_cn', 'id');
        $comment_skills = UserSkillCommentSkills::with(['skill'])->where('comment_id', '=', $comment->id)->get();
        $files = UserSkillCommentFiles::where('comment_id', '=', $comment->id)->get();
        $officialProjects = OfficialProject::all()->pluck('title', 'id');

        return view('backend.skillreview.comment_edit', [
            'model' => $model,
            'skillList' => $skillList,
//            'skills' => $skills,
            'comment' => $comment,
            'comment_skills' => $comment_skills,
            'files' => $files,
            'officialProjects' => $officialProjects
        ]);
    }

    public function commentEditPost(Request $request, $id, $cid) {



        $model = User::find($id);

        $skills = Skill::GetSkill()->pluck('name_cn', 'id');

        try {
            \DB::beginTransaction();

            //Build the comment
            $c = UserSkillComment::find($cid);
            $c->user_id = $model->id;
            $c->staff_id = \Auth::guard('staff')->user()->id;
//            $c->official_project_id = $request->get('official_project'); //not allow to edit after created
            $c->is_official = 1;
            $c->comment = $request->has('comment') && $request->get('comment') != '' ? $request->get('comment') : null;
            $c->attitude_rate = $request->has('attitude_rate') && $request->get('attitude_rate') == 1 ? 1 : 0;
            if ($c->attitude_rate == 1) {
                $c->attitude_comment = $request->has('attitude_comment') && $request->get('attitude_comment') != '' ? $request->get('attitude_comment') : null;
                $c->score_attitude = $request->has('score_attitude') && $request->get('score_attitude') != '' ? $request->get('score_attitude') : 0;
                if (!array_key_exists($c->score_attitude, Constants::getSkillScoreSelects())) {
                    throw new \Exception('错误的评分');
                }
            }

            $delete_imgs = array();

            //Build the skills
            $skill_models = array();
            $skill_count = 0;
            $score_total = 0;
            if ($request->has('skill')) {
                $skill_score = $request->get('skill_score');
                $skill_experience_action = $request->get('skill_experience_action');
                $skill_experience = $request->get('skill_experience');
                $exists = array();
                foreach ($request->get('skill') as $key => $var) {
                    if ($var != '' && isset($skill_score[$key]) && isset($skill_experience_action[$key]) && isset($skill_experience[$key])) {
                        if (array_key_exists($var, $skills->toArray())) {
                            $s = new UserSkillCommentSkills();
                            $s->user_id = $model->id;
                            $s->skill_id = $var;
                            $s->score = $skill_score[$key];
                            $s->action_type = $skill_experience_action[$key];
                            $s->experience = $skill_experience[$key];

                            if (array_key_exists($s->skill_id, $exists)) {
                                throw new \Exception('同样的技能不能评分两次');
                            }

                            $skill_models[] = $s;
                            $exists[$s->skill_id] = $s;
                            $score_total = $score_total + $s->score;
                            $skill_count++;
                        }
                    }
                }
            }

            if ($skill_count > 0) {
                $c->score_overall = $score_total / $skill_count;
            }


            if ($request->hasFile('images_old')) {
                $fs = $request->file('images_old');
                $image_old_path = $request->get('images_old_path');

                // dd($fs);
                foreach ($fs as $key => $var) {
                    $tru = !empty($image_old_path[$key]) ? $image_old_path[$key] : '';
                    $im = UserSkillCommentFiles::where('user_id', '=', $model->id)->where('comment_id', '=', $cid)->where('path', '=', $tru)->first();

                    if ($im) {
                        $old_imgs[] = $im->path;
                        $im->path = $var;
                        $im->thumbnail_file = '';
                        $im->save();
                    }
                }
            }


            if ($request->hasFile('images')) {
                $files = $request->file('images');
                $images = array();
                foreach ($files as $key => $var) {
                    $image = new UserSkillCommentFiles();
                    $image->user_id = $model->id;
                    $image->comment_id = $c->id;
                    $image->path = $var;
                    $images[] = $image;
                }
            }

            $c->save();
            foreach ($skill_models as $key => $var) {
                $var->comment_id = $c->id;
                $us = UserSkill::where('user_id', '=', $model->id)->where('skill_id', '=', $var->skill_id)->first();
                if (!$us) {
                    $us = new UserSkill();
                    $us->user_id = $model->id;
                    $us->skill_id = $var->skill_id;
                }

                if ($var->action_type == 0) {
                    $us->experience = $us->experience + $var->experience;
                } else if ($var->action_type == 1) {
                    $us->experience = $us->experience - $var->experience;
                } else {
                    throw new \Exception('未知的技能评分经验类型');
                }

                $var->save();
                $us->save();
            }

            if (isset($images) && count($images) > 0) {
                foreach ($images as $key => $var) {

                    $var->save();
                }
            }

            //Foreach delete old skill record need to reverse
            if ($request->has('delete_skill')) {
                foreach ($request->get('delete_skill') as $key => $var) {
                    $uscs = UserSkillCommentSkills::where('id', '=', $var)->where('user_id', '=', $model->id)->first();
                    //The user id is to make sure no changing of inspect element id is allowed
                    if ($uscs) {
                        $us = UserSkill::where('user_id', '=', $model->id)->where('skill_id', '=', $uscs->skill_id)->first();

                        if (!$us) {
                            $us = new UserSkill();
                            $us->user_id = $model->id;
                            $us->skill_id = $uscs->skill_id;
                        }

                        //Reverse the action
                        if ($uscs->action_type == 0) {
                            $us->experience = $us->experience - $uscs->experience;
                        } else if ($uscs->action_type == 1) {
                            $us->experience = $us->experience + $uscs->experience;
                        } else {
                            throw new \Exception('未知的技能评分经验类型');
                        }

                        $us->save();
                        $uscs->delete();
                    } else {
                        throw new \Exception('未知错误，请稍候刷新再试');
                    }
                }
            }

            if ($request->has('delete_image')) {
                foreach ($request->get('delete_image') as $key => $var) {
                    $img = UserSkillCommentFiles::where('comment_id', '=', $c->id)->where('user_id', '=', $model->id)->where('id', '=', $var)->first();
                    if ($img) {
                        $delete_imgs[] = $img->path;
                        $img->delete();
                    }
                }
            }

            \DB::commit();
            if (isset($delete_imgs) && count($delete_imgs) > 0) {
                foreach ($delete_imgs as $key => $var) {
                    if ($var != '' && $var != null) {
                        @unlink(public_path($var));
                    }
                }
            }

            if (isset($old_imgs) && count($old_imgs) > 0) {
                foreach ($old_imgs as $key => $var) {
                    if ($var != '' && $var != null) {
                        @unlink(public_path($var));
                    }
                }
            }
            return makeResponse('成功新增技能评论');
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse($e->getLine() . ' / ' . $e->getMessage(), true);
        }
    }

    public function commentDeletePost(Request $request, $id, $cid) {
        try {
            \DB::beginTransaction();
            $model = User::find($id);
            $comment = UserSkillComment::find($cid);

            if (!$model || !$comment) {
                throw new \Exception('未知错误，请刷新再试');
            }

            //Loop each comment skills and undo the user skill experience
            $comment_skills = UserSkillCommentSkills::where('comment_id', '=', $comment->id)->get();
            foreach ($comment_skills as $key => $var) {
                $us = UserSkill::where('user_id', '=', $model->id)->where('skill_id', '=', $var->skill_id)->first();

                if (!$us) {
                    $us = new UserSkill();
                    $us->user_id = $model->id;
                    $us->skill_id = $var->skill_id;
                }

                //Reverse the action
                if ($var->action_type == 0) {
                    $us->experience = $us->experience - $var->experience;
                } else if ($var->action_type == 1) {
                    $us->experience = $us->experience + $var->experience;
                } else {
                    throw new \Exception('未知的技能评分经验类型');
                }

                $us->save();
            }

            $comment->delete();
            \DB::commit();
            return makeResponse('成功删除评语');
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse($e->getMessage(), true);
        }
    }

    function testpage(Request $request, $id, $cid) {
        $jpeg_quality = 90;

        $x = $request->get('x');
        $y = $request->get('y');
        $w = $request->get('w');
        $h = $request->get('h');

        $src_x = intval($x);
        $src_y = intval($y);
        $src_w = intval($w);
        $src_h = intval($h);

        $src = $request->get('imgOriginalImage');
        $image_type = getFileType($src);
        if( $image_type == "jpeg" )
        {
            $src_image = imagecreatefromjpeg($src);
        }
        else if( $image_type == "png" )
        {
            $src_image = imagecreatefrompng($src);
        }
        else if( $image_type == "gif" )
        {
            $src_image = imagecreatefromgif($src);
        }
        else if( $image_type == "bmp" )
        {
            $src_image = imagecreatefrombmp($src);
        }

        $dst_w = 250;
        $dst_h = 180;
        $dst_image = ImageCreateTrueColor($dst_w, $dst_h);
        
        imagecopyresampled($dst_image, $src_image, 0, 0, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);

        $newFilename = generateRandomUniqueName();

        $path = 'uploads/skillcomment/thumbnail/';
        touchFolder($path);

        $filePath = $path . $newFilename . '.jpg';

        $filePath = $path . $newFilename . '.jpg';
        imagejpeg($dst_image, public_path() . '/' . $filePath, $jpeg_quality);

        $model = User::find($id);
        $c = UserSkillComment::find($cid);
        $c->user_id = $model->id;

        $tru = explode('uploads', $src);
        $tru = 'uploads' . $tru[1];

        // dd($tru);
        
        $im = UserSkillCommentFiles::where('user_id', '=', $model->id)->where('comment_id', '=', $cid)->where('path', '=', $tru)->first();

        //unlink old thumbnail file
        if($im->thumbnail_file){
            array_map('unlink', glob($im->thumbnail_file . '*'));
        }

        $im->thumbnail_file = $filePath;
        $f = $im->save();

        $thumbnailPath = asset($filePath);

        return response()->json([
            'status'        => 'OK',
            'thumbnailPath' => $thumbnailPath,
            'id'            => $request->get('img_id')
        ]);
       
    }
}
