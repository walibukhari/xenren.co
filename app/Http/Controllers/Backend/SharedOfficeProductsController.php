<?php

namespace App\Http\Controllers\Backend;
use App\Models\Staff;
use Carbon\Carbon;
use Session;
use Auth;
use Input;
use Datatables;
use Validator;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;

use App\Models\SharedOfficeProducts;
use App\Models\SharedOffice;
use App\Models\SharedOfficeProductStatus;
use App\Models\SharedOfficeProductCategory;
use App\Models\SharedOfficeProductAvailability;
use App\Models\SharedOfficeFinance;
use App\Models\SharedOfficeFinanceRevision;
use App\Models\PermissionGroup;

use App\Http\Requests\Backend\SharedOfficeProductCreateFormRequest;
use App\Http\Requests\Backend\SharedOfficeUpdateFormRequest;

class SharedOfficeProductsController extends BackendController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$id)
    {
        $staff = \Auth::guard('staff')->user();
        $checkOfficeExist = SharedOffice::where('id','=',$id)->first();
        if($staff->staff_type == Staff::STAFF_TYPE_STAFF) {
            $baseUrl = \URL::to('/');
            if ($staff->permission_group_id == PermissionGroup::PERMISSION_GROUP_SHARED_OFFICE) {
                $getStaffOffice = SharedOffice::where('staff_id','=',$staff->id)->first();
            } else {
                $getStaffOffice = SharedOffice::where('id','=',$staff->office_id)->first();
            }

            if(!is_null($checkOfficeExist)) {
                if (is_null($getStaffOffice)) {
                    $url = $baseUrl . '/admin/sharedoffice/products/' . $id;
                    return abort(403, 'Page Not Found.');
                }

                if ($checkOfficeExist->staff_id != $staff->id && $checkOfficeExist->id != $staff->office_id) {
                    $url = $baseUrl . '/admin/sharedoffice/products/' . $getStaffOffice->id;
                    return abort(403, 'Page Not Found.');
                }
            } else {
                if (is_null($checkOfficeExist))
                    return back();
                else
                    $url = $baseUrl . '/admin/sharedoffice/products/' . $id;
                    return abort(403, 'Page Not Found.');
            }
        }
       // dd( Session::get('lang'));
       // $checkfinance=SharedOfficeFinance::where([['product_id','=',$id],['cost','<>',NULL]])->get();
       // dd($checkfinance);
       // return view('backend.sharedoffice.products.index');//gadja index.blade.php
        return view('backend.sharedoffice.products.index',['id'=>$id]);//gadja index.blade.php
    }
    public function dt(Request $request,$id) {

        //$sharedofficeproducts = SharedOfficeProducts::all();

       // $sharedofficeproducts = SharedOfficeProducts::select('category_id')->where('office_id','=', $id);
       $sharedofficeproducts = SharedOfficeProducts::where('shared_office_products.office_id','=', $id);


        return Datatables::of($sharedofficeproducts )
            ->addColumn('office_id', function ($sharedofficeproducts) {
                $office_id = $sharedofficeproducts ->office_id;
                return $office_id;
            })

            ->addColumn('category_id', function ($sharedofficeproducts) {
                //$category_id= $sharedofficeproducts ->category_id;
                $language = Session::get('lang');
                if($language=="en") {
                    $category_id = $sharedofficeproducts->productcategory->name;
                }
                elseif($language=="cn"){
                    $category_id = $sharedofficeproducts->productcategory->name_ch;
                }
                else{
                    $category_id = $sharedofficeproducts->productcategory->name;
                }
                return $category_id;
            })

            ->addColumn('x', function ($sharedofficeproducts) {
                $x = $sharedofficeproducts ->x;
                return $x;
            })

            ->addColumn('y', function ($sharedofficeproducts) {
                $y = $sharedofficeproducts ->y;
                return $y;
            })

            ->addColumn('number', function ($sharedofficeproducts ) {
                $number = $sharedofficeproducts ->number;
                return $number;
            })
            ->addColumn('qr', function ($sharedofficeproducts) {
                $qr = $sharedofficeproducts->qr;
                $qr =  "<img class='img-thumbnail news-thumb' src=".url($qr)." >";
                $qr .= '<div>'.buildNormalLinkHtml([
                        'url' => route('backend.sharedoffice.products.qr', ['id' => $sharedofficeproducts ->id]),
                        'description' => '<div style="background-color: #25ce0f; padding: 5px; font-weight: 600; border-radius: 3px;font-size: 9pt;color: #fff;">
                                        Select QR type</div>'
                    ]).'</div>';
                return $qr;
            })

            ->addColumn('status_id', function ($sharedofficeproducts ) {
               // $status = $sharedofficeproducts ->status_id;
                $language = Session::get('lang');
                if($language=="en") {
                    $status = isset($sharedofficeproducts->productstatus->status) ? $sharedofficeproducts->productstatus->status : '';
                }
                elseif($language=="cn") {
                    $status = isset($sharedofficeproducts->productstatus->status_ch) ? $sharedofficeproducts->productstatus->status_ch : '';
                }
                else{
                    $status = isset($sharedofficeproducts->productstatus->status) ? $sharedofficeproducts->productstatus->status : '';
                }
                return $status;
            })

            ->addColumn('month_price', function ($sharedofficeproducts ) {
                $month_price = $sharedofficeproducts ->month_price;
                return $month_price;
            })

            ->addColumn('time_price', function ($sharedofficeproducts ) {
                $time_price = $sharedofficeproducts ->time_price;
                return $time_price;
            })

            ->addColumn('remark', function ($sharedofficeproducts ) {
                $remark = $sharedofficeproducts ->remark;
                return $remark;
            })

            ->filter(function ($sharedofficeproducts) {
                return $sharedofficeproducts->GetFilteredResults();
            })

            ->addColumn('actions', function ($sharedofficeproducts ) {

                $actions = '<center>'.buildNormalLinkHtml([
                    'url' => route('backend.sharedoffice.renting.index', ['id' => $sharedofficeproducts ->id]),
                    'description' => '<i class="fa fa-clock-o"></i>'
                ]);

                $actions .= buildText([
                    'description' => '|'
                ]);

                /*
                $actions .= buildNormalLinkHtml([
                    'url' => route('backend.sharedoffice.rentingmonth.index', ['id' => $sharedofficeproducts ->id]),
                    'description' => '<i class="fa fa-calendar"></i>'
                ]);
                */

                /*
                $actions .= buildNormalLinkHtml([
                    'url' => route('backend.sharedoffice.finance.index', ['id' => $sharedofficeproducts ->id]),
                    'description' => '<i class="fa fa-google-wallet"></i>'
                ]);
                */

                $actions .= buildNormalLinkHtml([
                        'url' => route('backend.sharedoffice.products.edit', ['id' => $sharedofficeproducts ->id]),
                        'description' => '<i class="fa fa-pencil"></i>'
                    ]);

                $actions .= buildText([
                    'description' => '|'
                ]);

                $actions .= buildConfirmationLinkHtml([
                        'url' => route('backend.sharedoffice.products.delete', ['id' => $sharedofficeproducts ->id]),
                        'description' => '<i class="fa fa-trash"></i>',
                    ]).'</center>';

                return $actions;
            })
	          ->rawColumns(['actions', 'qr'])
            ->make(true);
    }

    public function listfinanceRevisions(Request $request,$id){
        $sh=SharedOfficeFinance::where('id','=',$id)->first();
        return view('backend.sharedoffice.finance.listfinanceRevisions', ['id' => $id,'office_id'=>$sh->office_id]);
    }

    public function listfinanceRevisionsdt(Request $request,$id){
        $transactions=SharedOfficeFinanceRevision::where('transaction_id','=',$id)->get();

        return Datatables::of($transactions)

            ->addColumn('transaction_id', function ($transactions) {
                $transaction_id = $transactions->transaction_id;
                return $transaction_id;
            })

            ->addColumn('product_id', function ($transactions) {
                $product_id = $transactions->product_id;
                return $product_id;
            })

            ->addColumn('x', function ($transactions) {
                $x = $transactions->x;
                return $x;
            })

            ->addColumn('y', function ($transactions) {
                $y = $transactions->y;
                return $y;
            })

            ->addColumn('user_id', function ($transactions) {
                $user_id = $transactions->user_id;
                return $user_id;
            })

            ->addColumn('office_id', function ($transactions) {
                $office_id = $transactions->office_id;
                return $office_id;
            })

            ->addColumn('rev', function ($transactions) {
                $rev = $transactions->rev;
                return $rev;
            })

            ->addColumn('start_time', function ($transactions) {
                $start_time= $transactions->start_time;
                return $start_time;
            })

            ->addColumn('end_time', function ($transactions) {
                $end_time= $transactions->end_time;
                return $end_time;
            })

            ->addColumn('actions', function ($transactions ) {
                    $actions = "";
                    return $actions;
            })
	          ->rawColumns(['actions'])
            ->make(true);
    }

    public function showFinanceTable(Request $request,$id) {
        return view('backend.sharedoffice.finance.showfinancetable', ['id' => $id]);

   }

    public function showFinanceTabledt(Request $request,$id) {
        $sharedofficefinance=SharedOfficeFinance::where('office_id','=',$id)->get();

        return Datatables::of($sharedofficefinance )

            ->addColumn('office_id', function ($sharedofficefinance ) {
               $office_id = $sharedofficefinance ->productfinance->office->office_name;
               return $office_id;
            })

            ->addColumn('category_id', function ($sharedofficefinance) {
                $language = Session::get('lang');
                if($language=="en") {
                    $category_id = $sharedofficefinance->productfinance->productcategory->name;
                }
                elseif($language=="cn"){
                    $category_id = $sharedofficefinance->productfinance->productcategory->name_ch;
                }
                else{
                    $category_id = $sharedofficefinance->productfinance->productcategory->name;
                }
                return $category_id;
            })

            ->addColumn('product_id', function ($sharedofficefinance) {
                $product_id = $sharedofficefinance->productfinance->id;
                return $product_id;
            })

            ->addColumn('x', function ($sharedofficefinance) {
                $x = $sharedofficefinance->productfinance->x;
                return $x;
            })

            ->addColumn('y', function ($sharedofficefinance) {
                $y = $sharedofficefinance->productfinance->y;
                return $y;
            })

            ->addColumn('number', function ($sharedofficefinance) {
                $number = $sharedofficefinance->productfinance->number;
                return $number;
            })


            ->addColumn('start_time', function ($sharedofficefinance ) {
                $start_time= $sharedofficefinance->start_time;
                return $start_time;
            })

            ->addColumn('end_time', function ($sharedofficefinance ) {
                $end_time= $sharedofficefinance->end_time;
                return $end_time;
            })
            /*
            ->addColumn('month_price', function ($sharedofficefinance ) {
                $month_price = $sharedofficefinance ->productfinance->month_price;
                return $month_price;
            })
            */
            ->addColumn('time_price', function ($sharedofficefinance ) {
                $time_price = $sharedofficefinance ->productfinance->time_price;
                return $time_price;
            })

            ->addColumn('minForPayment', function ($sharedofficefinance ) {
                if($sharedofficefinance->end_time=="0000-00-00 00:00:00"){
                $minForPayment=\Carbon\Carbon::parse($sharedofficefinance->start_time)->diffInMinutes(\Carbon\Carbon::now());
                return $minForPayment;
                }
                else{
                    $minForPayment=\Carbon\Carbon::parse($sharedofficefinance->start_time)->diffInMinutes(\Carbon\Carbon::parse($sharedofficefinance->end_time));
                    return $minForPayment;
                }
            })

            ->addColumn('cost', function ($sharedofficefinance ) {
                if($sharedofficefinance->end_time=="0000-00-00 00:00:00") {
                    $cost = \Carbon\Carbon::parse($sharedofficefinance->start_time)->diffInMinutes(\Carbon\Carbon::now());
                    $cost = ($sharedofficefinance->productfinance->time_price * $cost / 10); //price is on 10 minute
                    return $cost;
                }
                else{
                    $cost = \Carbon\Carbon::parse($sharedofficefinance->start_time)->diffInMinutes(\Carbon\Carbon::parse($sharedofficefinance->end_time));
                    $cost = ($sharedofficefinance->productfinance->time_price * $cost / 10); //price is on 10 minute
                    return $cost;

                }
            })

            ->addColumn('paymentComment', function ($sharedofficefinance) {
                $paymentComment = $sharedofficefinance->status_finance;
                return $paymentComment;
            })


            ->addColumn('currentTime', function ($sharedofficefinance ) {
                $currentTime=\Carbon\Carbon::now();
                return $currentTime;
            })


            ->addColumn('user', function ($sharedofficefinance ) {
                $user= $sharedofficefinance->user;
                return $user;
            })

            ->addColumn('status', function ($sharedofficefinance) {
                $language = Session::get('lang');
                if($language=="en") {
                    $status = $sharedofficefinance->productfinance->productstatus->status;
                }
                elseif ($language=="cn")
                {
                    $status = $sharedofficefinance->productfinance->productstatus->status_ch;
                }
                else{
                    $status = $sharedofficefinance->productfinance->productstatus->status;
                }
                return $status;
            })

            ->addColumn('actions', function ($sharedofficefinance ) {

                if($sharedofficefinance->cost!="") {

                    $actions = '<center><br>'.buildNormalLinkHtml([
                            'url' => route('backend.sharedoffice.finance.revision', ['id' => $sharedofficefinance->id]),
                            'description' => 'Look Revision for Product',
                        ]).'</center>';
                    return $actions;
                }
                else {
                    $actions = '<center>' . buildNormalLinkHtml([
                            'url' => route('backend.sharedoffice.finance.editfinance', ['id' => $sharedofficefinance->id]),
                            'description' => '<i class="fa fa-usd"></i>',
                            'modal' => '#remote-modal',
                        ]);

                    $actions .= '<center><br>'.buildNormalLinkHtml([
                            'url' => route('backend.sharedoffice.finance.choosechair', ['id' => $sharedofficefinance->id]),
                            'description' => 'Change Seat',
                        ]).'</center>';

                    $actions .= '<center><br>'.buildNormalLinkHtml([
                            'url' => route('backend.sharedoffice.finance.revision', ['id' => $sharedofficefinance->id]),
                            'description' => 'Look Revision for Product',
                        ]).'</center>';


                    return $actions;

                }
            })
	          ->rawColumns(['actions'])
            ->make(true);
    }


    public function editFinance(Request $request,$id) {

        $sharedofficefinance = SharedOfficeFinance::find($id);
        $cost = \Carbon\Carbon::parse($sharedofficefinance->start_time)->diffInMinutes(\Carbon\Carbon::now());
        $cost = ($sharedofficefinance->productfinance->time_price * $cost / 10); //price is on 10 minute

        return view('backend.sharedoffice.finance.edit', ['sharedofficefinance' => $sharedofficefinance,'cost'=>$cost]);
    }

    public function qr(Request $request,$id) {

        return view('backend.sharedoffice.products.qr',['id'=>$id]);
    }


    public function chooseChair(Request $request,$id) {
        $sharedofficefinance = SharedOfficeFinance::find($id);

        $list_free_positions= SharedOfficeProducts::where('office_id', '=', $sharedofficefinance->office_id)
            ->where('y', '!=',null)
            ->where('x', '!=',null)
            ->where('status_id', '=',3)
            ->orderBy('x', 'asc')
            ->orderBy('y', 'asc')
            ->get();

        $sharedoffice=SharedOffice::where('id','=',$sharedofficefinance->office_id)->first(); // get first sharedoffice object
        return view('backend.sharedoffice.finance.choosenewchair',['id'=>$id])->withSharedoffice($sharedoffice)->withListproducts($list_free_positions)->withSharedofficefinance($sharedofficefinance);
    }

    public function savenewChair(Request $request,$id) {

        $input = $request->all();
        $model = SharedOfficeFinance::find($id);

        $data_for_selected_position= SharedOfficeProducts::where('x', '=', $input['x'])
            ->where('y', '=',$input['y'])
            ->where('office_id', '=',$input['off_id'])
            ->where('status_id', '=',3)
            ->first();
        $data_for_selected_position->status_id = 1;  // new product is now busy
        $data_for_selected_position->save();

        /*update finance table with new coordinate and product_id */
        $model->x = $input['x'];
        $model->y = $input['y'];
        $model->product_id = $data_for_selected_position->id;
        $model->save();

        $max=SharedOfficeFinanceRevision::where('transaction_id', '=', $model->id)->max('rev');

        /* add new revision for same transaction */
        $financerevision= new SharedOfficeFinanceRevision();
        $financerevision->transaction_id= $model->id;
        $financerevision->x = $input['x'];
        $financerevision->y = $input['y'];
        $financerevision->product_id = $data_for_selected_position->id;
        $financerevision->office_id = $input['off_id'];
        $financerevision->user_id =$data_for_selected_position->user_id;
        $financerevision->start_time = \Carbon\Carbon::now();
        $financerevision->rev = $max+1;
        $financerevision->save();

        /* free old chair so somebody else can use */
        $old_product= SharedOfficeProducts::where('id', '=', $input['product_id'])->first();
        $old_product->status_id = 3;  // old product now is free
        $old_product->save();

        Session::flash('msgRentingSave', 'User switch product successfully.');
        return redirect()->route('backend.sharedoffice.finance.showfinancetable', ['id' => $input['off_id']]);
    }

    public function storeFinancePayment(Request $request,$id) {

        $input = $request->all();

        $model2 = SharedOfficeFinance::find($id);
        $model2->status_finance = $input['status'];
        $model2->cost = $input['cost'];
        $model2->end_time = $input['endtime'];
        $model2->save();

        $model_revision= SharedOfficeFinanceRevision::where('transaction_id', '=', $id)
            ->where('product_id', '=',$model2->product_id)
            ->orderBy('rev', 'desc')
            ->first();
        $model_revision->end_time = $input['endtime'];
        $model_revision->save();


        $model1 = SharedOfficeProducts::find($input['product_id']);
        $model1->status_id = 3;
        $model1->save();

        Session::flash('msgFinanceClose', 'This product is free now. User stop to use product.');
        return redirect()->route('backend.sharedoffice.finance.showfinancetable', ['id' => $model1->office_id]);
    }

    public function deleteFinance(Request $request,$id) {

        dd("underconstruction delete");
    }

    public function renting(Request $request,$id) {

        $status=SharedOfficeProductStatus::all();
        $stats = array();
        foreach ($status as $st) {
            $stats[$st->id] = $st->status;
        }

        $sharedofficeproduct= SharedOfficeProducts::where('id','=', $id)->first();
        if($sharedofficeproduct->status_id == SharedOfficeProducts::STATUS_EMPTY) {
           return view('backend.sharedoffice.renting.adduser', ['id' => $id])->withSharedofficeproduct($sharedofficeproduct)->withStatus($stats);
        }  else{
            return redirect()->route('backend.sharedoffice.products.index', ['id'=>$sharedofficeproduct->office_id])->with('msgProductUsing', 'The product already is in using. You can not rent this product.!');
        }
    }


    public function saveRenting(Request $request, $office) {
        $input = $request->all();
        $model = new SharedOfficeFinance();
				$office = SharedOfficeProducts::find($office);

        //status is using
       //cetvrtak  $model->status_finance = $input['status'];
        $model->start_time = $input['startdate'];
        $model->user = $request->user;
        $model->x = $office['x'];
        $model->y = $office['y'];
        $model->office_id = $office['office_id'];
        $model->product_id = $office['id'];
        $model->status_finance = 0;
        $model->cost = 0;
        $model->end_time = null;
        $model->save();


        $financerevision= new SharedOfficeFinanceRevision();
        $financerevision->transaction_id= $model->id;
        $financerevision->x = $office['x'];
        $financerevision->y = $office['y'];
        $financerevision->start_time = Carbon::parse($request->startdate);
        $financerevision->rev =0;
        $financerevision->user_id = $request->user;
        $financerevision->office_id = $office['office_id'];
        $financerevision->product_id = $office['id'];
        $financerevision->save();


        $model2 = SharedOfficeProducts::find($office['id']);
        $model2->status_id = $input['status'];
        $model2->save();

        Session::flash('msgRentingSave', 'This product renting was successfully updated.');
        return redirect()->route('backend.sharedoffice.products.index', ['id' => $input['office_id']]);

    }


    public function rentingMonth(Request $request,$id) {

        $status=SharedOfficeProductStatus::all();
        $stats = array();
        foreach ($status as $st) {
            $stats[$st->id] = $st->status;
        }

        $sharedofficeproduct=SharedOfficeProducts::where('id','=', $id)->first();
        if($sharedofficeproduct->status_id!='1' and $sharedofficeproduct->status_id!='2' ) {
            return view('backend.sharedoffice.rentingmonth.adduser', ['id' => $id])->withSharedofficeproduct($sharedofficeproduct)->withStatus($stats);
        }
        else{
            return redirect()->route('backend.sharedoffice.products.index', ['id'=>$sharedofficeproduct->office_id])->with('msgProductUsing', 'The product already is in using. You can not rent this product.!');
        }
    }


    public function saveRentingMonth(Request $request,$id) {
        $input = $request->all();
        $model = new SharedOfficeFinance();
        // cetvrtak $model->status_finance = $input['statusfinance'];
        $model->start_time = $input['startdate'];
        $model->end_time = $input['enddate'];
        $model->user = $input['user'];
        $model->office_id = $input['office_id'];
        $model->product_id = $input['product_id'];
        $model->save();

        $model2 = SharedOfficeProducts::find($input['product_id']);
        $model2->status_id = $input['statusfinance'];
        $model2->save();

        Session::flash('msgRentingMonthSave', 'This Month renting for product was successfully updated.');
        return redirect()->route('backend.sharedoffice.products.index', ['id' => $input['office_id']]);
    }


    public function finance(Request $request,$id) {

        $sharedofficefinance=SharedOfficeFinance::where('product_id','=', $id)->first();
        $sharedofficeproduct=SharedOfficeProducts::where('id','=', $id)->first();
        $sharedoffice=SharedOffice::where('id','=', $sharedofficeproduct->office_id)->first();
        if($sharedofficeproduct->status_id=='1' or $sharedofficeproduct->status_id=='2' ) {
           return view('backend.sharedoffice.finance.index', ['id' => $id])->withSharedofficeproduct($sharedofficeproduct)->withSharedofficefinance($sharedofficefinance)->withSharedoffice($sharedoffice);
        }
        else{
           return redirect()->route('backend.sharedoffice.products.index', ['id'=>$sharedofficeproduct->office_id])->with('msgFinance', 'The product is empty. You can not perform any economic transaction for it.!');

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function checkposition(SharedOfficeProductCreateFormRequest $request,$id)
    {
        $checkPosition = SharedOfficeProducts::where('x', '=', $_GET['x'])
            ->where('y', '=', $_GET['y'])
            ->where('office_id', '=', $id)
            ->first();

        if($checkPosition){
           return "duplicate";
        }else{
           return "notduplicate";
        }
    }

    public function checkbusy(SharedOfficeProductCreateFormRequest $request,$id)
    {
        $checkFree = SharedOfficeProducts::where('x', '=', $_GET['x'])
            ->where('y', '=', $_GET['y'])
            ->where('office_id', '=', $_GET['offid'])
            ->where('status_id', '=', 3)
            ->first();

        if($checkFree){
            return "free";
        }else{
            return "busy";
        }
    }



    public function checkpositionedit(SharedOfficeProductCreateFormRequest $request,$id)
    {
        $checkPosition = SharedOfficeProducts::where('x', '=', $_GET['x'])
            ->where('y', '=', $_GET['y'])
            ->where('office_id', '=', $_GET['offid'])
            ->where('id', '!=', $id)  // don't include current product id
            ->first();

        if($checkPosition){
            return "duplicate";
        }else{
            return "notduplicate";
        }
    }

    public function create(SharedOfficeProductCreateFormRequest $request,$id)
    {

        $list_using_positions= SharedOfficeProducts::where('office_id', '=', $id)
            ->where('y', '!=',null)
            ->where('x', '!=',null)
            ->orderBy('x', 'asc')
            ->orderBy('y', 'asc')
            ->get();

        $categories=SharedOfficeProductCategory::orderBy('id')->limit(3)->get();
        $status=SharedOfficeProductStatus::all();
        $sharedoffice=SharedOffice::where('id','=',$id)->first(); // get first sharedoffice object
        //return view and pass in to sharedoffice object
        return view('backend.sharedoffice.products.create',[
            'id'=>$id
        ])->withSharedoffice($sharedoffice)
            ->withStatus($status)
            ->withCategories($categories)
            ->withListproducts($list_using_positions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SharedOfficeProductCreateFormRequest $request,$id)
    {
            $input = $request->all();
            if($input['no_of_peoples'] > $input['availability']) {
                return back()->with('danger','No Of People Greater Then Total Available Seats');
            }
            $checkProduct = SharedOfficeProducts::where('office_id','=',$input['office_id'])->where('category_id','=',$input['category_id'])->first();
            if(!is_null($checkProduct)) {
                $allproducts = SharedOfficeProducts::all();
                Session::flash('msgDanger', 'Category already exist');
                return view('backend.sharedoffice.products.index', ['id' => $input['office_id']])->withPost($allproducts);
            }
            // for($i=0;$i<$input['stock'];$i++) {
            // $productnamegeneratorIn=mt_rand(1,1000);
            $productnamegeneratorIn = $this->getUniqueRandomNumber($input['office_id']);

            //  $productnamegeneratorOut=mt_rand(100000000,10000000000);

            /*check is it x,y position used */
                $checkPosition = SharedOfficeProducts::where('x', '=', $input['x'])
                    ->where('y', '=', $input['y'])
                    ->where('office_id', '=', $id)
                    ->first();
            if(!$checkPosition) {
                $model = new SharedOfficeProducts();
                $model->qr = "https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=https://www.xenren.co/".getLocale()."/seat/" . $productnamegeneratorIn . "/office/" . $id;
                //  $model->out_qr = "https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=" . $productnamegeneratorOut;
                $model->category_id = $input['category_id'];
                $model->office_id = $input['office_id'];
                $model->x = $input['x'];
                $model->y = $input['y'];
                $model->number = $productnamegeneratorIn;
                $model->status_id = isset($input['status']) ? $input['status'] : SharedOfficeProducts::STATUS_EMPTY;
                $model->no_of_peoples = $input['no_of_peoples'];
                $model->month_price = $input['month_price'];
                $model->time_price = $input['time_price'];

                $availability = SharedOfficeProductAvailability::where('office_id', $input['office_id'])->where('category_id', $input['category_id'])->first();
                if (!$availability) {
                    $ava = new SharedOfficeProductAvailability;
                    $ava->availability = $input['availability'];
                    $ava->office_id = $input['office_id'];
                    $ava->category_id = $input['category_id'];
                    $ava->save();
                    $model->availability_id = $input['availability'];
                } else {
                    $model->availability_id = $input['availability'];
                }

                $model->weekly_price = $request->input('weekly_price');
                $model->hourly_price = $request->input('hourly_price');
                $model->remark = $input['remark'];
                $model->save();
                // }
                $allproducts = SharedOfficeProducts::all();
                Session::flash('msgProductCreate', 'Product is successfuly created.');
                return view('backend.sharedoffice.products.index', ['id' => $input['office_id']])->withPost($allproducts);
            }
            else {
                $allproducts = SharedOfficeProducts::all();
                Session::flash('msgDanger', 'NOT SAVED, POSITION ALREADY IN USE.');
                return view('backend.sharedoffice.products.index', ['id' => $input['office_id']])->withPost($allproducts);

            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {

        $status=SharedOfficeProductStatus::all();
        $stats = array();
        $language = Session::get('lang');
        if($language=="en") {
            foreach ($status as $st) {
                $stats[$st->id] = $st->status;
            }
        }
        elseif($language=="cn") {
            foreach ($status as $st) {
                $stats[$st->id] = $st->status_ch;
            }
        }
        else{
            foreach ($status as $st) {
                $stats[$st->id] = $st->status;
            }
        }

        $categories=SharedOfficeProductCategory::orderBy('id')->get();
        $categs= array();
        $language = Session::get('lang');
        if($language=="en") {
            foreach ($categories as $cat) {
                $categs[$cat->id] = $cat->name;
            }
        }
        elseif($language=="cn") {
            foreach ($categories as $cat) {
                $categs[$cat->id] = $cat->name_ch;
            }
        }
        else{
            foreach ($categories as $cat) {
                $categs[$cat->id] = $cat->name;
            }
        }

        $sharedofficeproduct=SharedOfficeProducts::where('id','=', $id)->first();

        $list_using_positions= SharedOfficeProducts::where('office_id', '=', $sharedofficeproduct->office_id)
            ->where('y', '!=',null)
            ->where('x', '!=',null)
            ->orderBy('x', 'asc')
            ->orderBy('y', 'asc')
            ->get();

        return view('backend.sharedoffice.products.edit',['id'=>$id])->withSharedofficeproduct($sharedofficeproduct)->withStatus($stats)->withCategories($categs)->withListproducts($list_using_positions);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Save the data to the database
        $products = SharedOfficeProducts::find($id);
        /*check is it x,y position used */
        // dd($products);
        // dd($products->status_id);
        // dd(SharedOfficeProducts::STATUS_USING);
        if ($products->status_id != SharedOfficeProducts::STATUS_USING) {
            $checkPosition = SharedOfficeProducts::where('x', '=', $request->input('x'))
                ->where('y', '=', $request->input('y'))
                ->where('id', '!=', $id)
                ->where('office_id', '=', $products->office_id)
                ->first();
            $checkSeatNumber = false;
            $checkNumber = SharedOfficeProducts::where('office_id', '=', $products->office_id)
                ->where('number', '=', $request->input('number'))
                ->first();

            if(!is_null($checkNumber)) {
                if ($checkNumber->id == $id) {
                    $checkSeatNumber = true;
                }
            } else {
                $checkSeatNumber = true;
            }
            if ($checkSeatNumber) {
                if (!$checkPosition) {
                    $products->category_id = $request->input('category_id');
                    $products->no_of_peoples = $request->input('no_of_peoples');
                    $products->month_price = $request->input('month_price');
                    $products->weekly_price = $request->input('weekly_price');
                    $products->hourly_price = $request->input('hourly_price');
                    $products->time_price = $request->input('time_price');

                    $ava = SharedOfficeProductAvailability::where('office_id', $request->office_id)->where('category_id', $request->category_id)->get();
                    if ($ava->count() == 1) {
                        $ava = new SharedOfficeProductAvailability();
                        $ava->availability = $request->availability;
                        $ava->office_id = $request->office_id;
                        $ava->category_id = $request->category_id;
                        $ava->save();
                        $products->availability_id = $ava->id;
                    }

                    $products->number = $request->input('number');
                    $products->remark = $request->input('remark');
                    $products->status_id = $request->input('status_id');
                    $products->x = $request->input('x');
                    $products->y = $request->input('y');
                    $products->qr = "https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=seat:" . $request->input('number') . "|office:" . $products->office_id;
                    $products->save();

                    // set flash data with success message
                    Session::flash('msgProductUpdate', 'Product is successfuly updated.');
                    // redirect with flash data to posts.show
                    return redirect()->route('backend.sharedoffice.products.index', ['id' => $products->office_id]);
                } else {
                    Session::flash('msgDanger', 'NOT SAVED, POSITION ALREADY IN USE.');
                    return redirect()->route('backend.sharedoffice.products.index', ['id' => $products->office_id]);
                }
            } else {
                Session::flash('msgDanger', 'NOT SAVED, SEAT NUMBER ALREADY EXISTS');
                return redirect()->route('backend.sharedoffice.products.index', ['id' => $products->office_id]);
            }
        } else {
            Session::flash('msgDanger', 'NOT SAVED, SEAT ALREADY IN USE');
            return redirect()->route('backend.sharedoffice.products.index', ['id' => $products->office_id]);
        }
    }

    public function getAvailability($officeId, $categoryId)
    {
        $ava = SharedOfficeProductAvailability::where('office_id', $officeId)->where('category_id', $categoryId)->first();
        if (isset($ava) && $ava->count()) {
            return response()->json([
                'message' => 'Product availability found!',
                'data' => $ava
            ],200);
        }
        return response()->json([
            'message' => 'Product availability not found!',
            'data' => []
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id) {
        $model = SharedOfficeProducts::find($id);
        if($model->status_id == SharedOfficeProducts::STATUS_USING) {
            return makeResponse('seat is in use you cannot delete this seat at this time');
        }
        /* check is it in using this product in other tables, if so, can not delete */
        $checkfinance = SharedOfficeFinance::where([['product_id','=',$id],['cost','=',NULL]])->get();
        $checkModel = SharedOfficeProducts::where('office_id', $model->office_id)->where('category_id', $model->category_id)->get();
        try {
            if($checkfinance->isEmpty()) {
                if ($model->delete()) {
                    if ($checkModel->count() == 1) {
                        if ($model->availability) {
                            $model->availability->delete();
                        }
                    }
                    return makeResponse('成功删除新闻');
                } else {
                    throw new \Exception('未能删除新闻，请稍候再试');
                }
            }
            else{
                return makeResponse('You can not erase product because is in using.');
            }
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }


    }

    public function generateNum()
    {
        return mt_rand(1,1000);
    }

    public function getUniqueRandomNumber($office_id=false)
    {
        $num = self::generateNum();

        if($office_id) {
            $items = SharedOfficeProducts::where('office_id', '=', $office_id)->where('number', '=', $num)->first();
            if(!isset($items)) {
                return $num;
            }
            self::getUniqueRandomNumber($office_id);
        }
    }


}
