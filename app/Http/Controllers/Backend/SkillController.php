<?php

namespace App\Http\Controllers\Backend;

use App\Models\SkillKeywordGroup;
use Auth;
use Illuminate\Support\Facades\DB;
use Input;
use Datatables;
use Validator;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;
use App\Models\Skill;
use App\Http\Requests\Backend\SkillCreateFormRequest;
use App\Http\Requests\Backend\SkillEditFormRequest;

class SkillController extends BackendController
{
    public function index(Request $request, $id = null) {
        return view('backend.skill.index');
    }

    public function indexDt(Request $request, $id = null) {
        $model = Skill::GetSkill()
            ->where('tag_color', Skill::TAG_COLOR_WHITE)
            ->withTrashed();

        return Datatables::of($model)
//            ->editColumn('blabla', function ($model) {
//
//            })
//            ->addColumn('blabla2', function ($model) {
//
//            })
            ->addColumn('names', function ($model) {
                return $model->getNames();
            })
            ->addColumn('status', function ($model) {
                return $model->explainStatus();
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = '';
                $actions .= buildRemoteLinkHtml([
                    'url' => route('backend.skill.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-pencil"></i>',
                    'modal' => '#remote-modal',
                ]);
                if ($model->deleted_at == null) {
                    $actions .= buildConfirmationLinkHtml([
                        'url' => route('backend.skill.delete', ['id' => $model->id]),
                        'description' => '<i class="fa fa-trash"></i>',
                        'title' => '确认删除？',
                        'content' => '确认删除此技能？'
                    ]);
                } else {
                    $actions .= buildConfirmationLinkHtml([
                        'url' => route('backend.skill.restore', ['id' => $model->id]),
                        'description' => '<i class="fa fa-check"></i>',
                        'title' => '确认激活？',
                        'content' => '确认激活此技能？',
                        'color' => 'green',
                    ]);
                }
                return $actions;
            })
	          ->rawColumns(['actions', 'names'])
            ->make(true);
    }

    public function create(Request $request) {
        return view('backend.skill.create');
    }

    public function createPost(SkillCreateFormRequest $request, $id = null) {
        $skillIdList = array();
        $groupId = DB::table('skill_keyword_groups')->max('group');
        if( $groupId == null )
        {
            $groupId = 1;
        }
        else
        {
            $groupId += 1;
        }

        try {
            $model = new Skill();
            $model->name_cn = $request->get('name_cn');
            $model->reference_link_cn = $request->get('reference_link_cn');
            $model->description_cn = $request->get('description_cn');
            $model->name_en = $request->get('name_en');
            $model->reference_link_en = $request->get('reference_link_en');
            $model->description_en = $request->get('description_en');
            $model->tag_color = Skill::TAG_COLOR_WHITE;
            $model->save();
            array_push($skillIdList, $model->id);

            $model = new Skill();
            $model->name_cn = $request->get('name_cn');
            $model->reference_link_cn = $request->get('reference_link_cn');
            $model->description_cn = $request->get('description_cn');
            $model->name_en = $request->get('name_en');
            $model->reference_link_en = $request->get('reference_link_en');
            $model->description_en = $request->get('description_en');
            $model->tag_color = Skill::TAG_COLOR_GOLD;
            $model->save();
            array_push($skillIdList, $model->id);

            foreach($skillIdList as $skillID)
            {
                $model = new SkillKeywordGroup();
                $model->skill_id = $skillID;
                $model->group = $groupId;
                $model->save();
            }

            return makeResponse('成功新增技能');
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function edit($id) {
        $model = Skill::GetSkill()->find($id);

        return view('backend.skill.edit', ['model' => $model]);
    }

    public function editPost(SkillEditFormRequest $request, $id) {
        $model = Skill::GetSkill()->find($id);

        if (!$model) {
            return makeResponse('技能不存在', true);
        }

        //auto update gold tag
        $group = SkillKeywordGroup::where('skill_id', $id)->first()->group;
        $skillKeywordGroups = SkillKeywordGroup::where('group', $group)->get();

        try {
            foreach( $skillKeywordGroups as $skillKeywordGroup )
            {
                $model = Skill::GetSkill()->find($skillKeywordGroup->skill_id);
                $model->name_cn = $request->get('name_cn');
                $model->reference_link_cn = $request->get('reference_link_cn');
                $model->description_cn = $request->get('description_cn');
                $model->name_en = $request->get('name_en');
                $model->reference_link_en = $request->get('reference_link_en');
                $model->description_en = $request->get('description_en');
                $model->save();
            }

            return makeResponse('成功编辑技能');
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function delete($id) {
        $model = Skill::find($id);

        //auto delete gold tag
        $record = SkillKeywordGroup::where('skill_id', $model->id)->first();
        $skillKeywordGroups = SkillKeywordGroup::where('group', $record->group)
            ->get();

        try {
            foreach( $skillKeywordGroups as $skillKeywordGroup )
            {
                $model = Skill::find($skillKeywordGroup->skill_id);
                $model->delete();
            }

            return makeResponse('成功删除技能');

//          throw new \Exception('未能删除技能，请稍候再试');

        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function restore($id) {
        $model = Skill::GetSkill()->withTrashed()->find($id);

        //auto restore gold tag
        $record = SkillKeywordGroup::where('skill_id', $model->id)->first();
        $skillKeywordGroups = SkillKeywordGroup::where('group', $record->group)
            ->get();

        try {

            foreach( $skillKeywordGroups as $skillKeywordGroup )
            {
                $model = Skill::withTrashed()->find($skillKeywordGroup->skill_id);
                $model->restore();
            }

//            if ($model->restore()) {
            return makeResponse('成功激活技能');
//            } else {
//                throw new \Exception('未能激活技能，请稍候再试');
//            }
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }
}
