<?php

namespace App\Http\Controllers\Backend;

use App\Models\ManageCoin;
use App\Models\UserCoins;

use App\UserFeedback;
use Auth;
use Input;
use Datatables;
use Validator;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;
use App\Http\Requests\Backend\CoinCreateFormRequest;
use App\Http\Requests\Backend\CoinUpdateFormRequest;

class ManageCoinController extends BackendController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        return view('backend.managecoin.index');
    }
    public function userindex(Request $request)
    {

        return view('backend.managecoin.userindex');
    }

    public function userfeedback(Request $request)
    {
        $data = UserFeedback::get();
        return view('backend.userfeedback.index',[
            'data' => $data
        ]);
    }

    public function indexDt(Request $request) {
        $sharedoffice = ManageCoin::all();

        return Datatables::of($sharedoffice)
            ->addColumn('actions', function ($sharedoffice) {

                $actions = '<center>';

                $actions .= buildNormalLinkHtml([
                        'url' => route('backend.manageCoins.edit', ['id' => $sharedoffice->id]),
                        'description' => '<i class="fa fa-pencil"></i>'
                    ]);

                $actions .= buildConfirmationLinkHtml([
                        'url' => route('backend.manageCoins.delete', ['id' => $sharedoffice->id]),
                        'description' => '<i class="fa fa-trash"></i>',
                    ]).'</center>';

                return $actions;
            })
	          ->rawColumns(['actions'])
            ->make(true);
    }

   public function userindexDt(Request $request) {
        $sharedoffice = UserCoins::all();

        return Datatables::of($sharedoffice)
            ->addColumn('actions', function ($sharedoffice) {

                $actions = '<center>';

                $actions .= buildNormalLinkHtml([
                        'url' => route('backend.manageCoins.edit', ['id' => $sharedoffice->id]),
                        'description' => '<i class="fa fa-pencil"></i>'
                    ]);

                $actions .= buildConfirmationLinkHtml([
                        'url' => route('backend.manageCoins.delete', ['id' => $sharedoffice->id]),
                        'description' => '<i class="fa fa-trash"></i>',
                    ]).'</center>';

                return $actions;
            })
	          ->rawColumns(['actions'])
            ->make(true);
    }


    public function create()
    {
        return view('backend.managecoin.create');
    }


    public function storeOffice(CoinCreateFormRequest $request) {
        try {
            $input = $request->all();

            $model = new ManageCoin();
            $model->coins = $input['coins'];
            $model->coins_type = $input['coin_type'];
            $model->per_coin_amount = $input['amount'];
            $model->save();
            return makeResponse('成功新增新闻');
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function editOffice(CoinUpdateFormRequest $request, $id) {
        $input = $request->all();

        $model = ManageCoin::find($id);

        if (!$model) {
            return makeResponse('新闻不存在', true);
        }

        try {
            $model->coins = $input['coins'];
            $model->coins_type= $input['coin_type'];
            $model->per_coin_amount = $input['amount'];
            $model->save();
            

            return makeResponse('成功编辑新闻');
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }


    public function edit($id) {
        $model = ManageCoin::find($id);
        return view('backend.managecoin.edit', ['model' => $model]);
    }



    public function delete($id) {
        $model = ManageCoin::find($id);

        try {
            if ($model->delete()) {
                return makeResponse('成功删除新闻');
            } else {
                throw new \Exception('未能删除新闻，请稍候再试');
            }
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }
}
