<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TimeTrack;
use App\Models\ProjectApplicant;
use App\Models\User;
use App\Models\CoinProject;
use App\Models\UserCoin;
use App\Models\Project;
use App\Models\Coins;
use App\Constants;
use Illuminate\Support\Facades\Validator;
use App\Support\UploadFileHelper;

class CoinController extends Controller
{
    use UploadFileHelper;

    public function index() {
    	$startTime = date('Y-m-d', strtotime('sunday last week'));
    	$endTime = date('Y-m-d H:i:s');

    	$projects = ProjectApplicant::with('project', 'user', 'user.userCoinAddress')
            ->select('project_applicants.*', 'user_coin.amount', 'coin_projects.coin_type')
            ->whereHas('project')
            ->join('coin_projects', 'coin_projects.project_id', '=', 'project_applicants.project_id')
            ->leftJoin('user_coin', function($join) {
                $join->on('user_coin.project_id', '=', 'project_applicants.project_id')
                    ->on('user_coin.user_id', '=', 'project_applicants.user_id');
            })
            ->where(function($query) {
                // only completed project or hourly project
                $query->where('status', ProjectApplicant::STATUS_COMPLETED)
                    ->orWhere('pay_type', ProjectApplicant::PAY_TYPE_HOURLY_PAY);
            })
            ->get();

        $data = [];
        foreach ($projects as $key => $project) {
            if (!is_null($project->user)) {
                $timeTrack = TimeTrack::selectRaw('SUM(TIMESTAMPDIFF(SECOND, created_at, updated_at)) as total_time')
                    ->where('project_id', $project->project_id)
                    ->where('user_id', $project->user->id);
            } else {
                $timeTrack = TimeTrack::selectRaw('SUM(TIMESTAMPDIFF(SECOND, created_at, updated_at)) as total_time')
                    ->where('project_id', $project->project_id);
            }

            $totalTimeTrack = $timeTrack->first();

            // when status not complete, limit weekly
            if ($project->status != ProjectApplicant::STATUS_COMPLETED) {
                $timeTrack = $timeTrack->whereBetween('user_time_track.updated_at', [$startTime, $endTime]);
            }

            $timeTrack = $timeTrack->first();

            // calculate total time, total dollar get by user, and check if moveer coin is already given
            $projects[$key]['total_time'] = !is_null($timeTrack->total_time) ? (int) $timeTrack->total_time : 0;
            $projects[$key]['total_dollar'] = $project->pay_type == ProjectApplicant::PAY_TYPE_HOURLY_PAY ? $project->quote_price * floor($projects[$key]['total_time'] / 3600) : $project->quote_price;

            $projects[$key]['total_time_all'] = !is_null($totalTimeTrack->total_time) ? (int) $totalTimeTrack->total_time : 0;
            $projects[$key]['total_dollar_all'] = $project->pay_type == ProjectApplicant::PAY_TYPE_HOURLY_PAY ? $project->quote_price * floor($projects[$key]['total_time_all'] / 3600) : $project->quote_price;
            $projects[$key]['is_coin_given'] = !is_null($project->amount) ? 
                $projects[$key]['total_dollar_all'] == $project->amount : false;

            // get moveer address
            $projects[$key]['coin_address'] = null;
            if (!is_null($project->user) && !is_null($project->user->userCoinAddress)) {
                $projects[$key]['coin_address'] = $project->user->userCoinAddress->address;
            }
        }

        $projects = $projects->sortByDesc('total_dollar');
        // var_dump($projects->toArray());die();

        // get pay type
        $payType = [
            null => trans('common.unknown'),
            ProjectApplicant::PAY_TYPE_UNKNOWN => trans('common.unknown'),
            ProjectApplicant::PAY_TYPE_HOURLY_PAY => trans('common.hourly_pay'),
            ProjectApplicant::PAY_TYPE_FIXED_PRICE => trans('common.fixed_price'),
        ];

        // get project type
        $projectType = [
            Project::PROJECT_TYPE_COMMON => trans('common.common_project'),
            Project::PROJECT_TYPE_OFFICIAL => trans('common.official_project'),
        ];

        // get coin list
        $coinList = Coins::getAllCoinFormated();

        return view('backend.coin.index', [
            'projects' => $projects,
            'payType' => $payType,
            'projectType' => $projectType,
            'coinList' => $coinList
        ]);
    }

    public function validateCoin(Request $request)
    {
        $startTime = date('Y-m-d', strtotime('sunday last week'));
        $endTime = date('Y-m-d');

        $userId = $request->user_id;
        $projectId = $request->project_id;
        $amount = $request->amount;

        $project = ProjectApplicant::with('project', 'user', 'user.userCoinAddress')
            ->select('project_applicants.*', 'user_coin.amount', 'coin_projects.coin_type')
            ->whereHas('project')
            ->join('coin_projects', 'coin_projects.project_id', '=', 'project_applicants.project_id')
            ->leftJoin('user_coin', function($join) {
                $join->on('user_coin.project_id', '=', 'project_applicants.project_id')
                    ->on('user_coin.user_id', '=', 'project_applicants.user_id');
            })
            ->where(function($query) {
                // only completed project or hourly project
                $query->where('status', ProjectApplicant::STATUS_COMPLETED)
                    ->orWhere('pay_type', ProjectApplicant::PAY_TYPE_HOURLY_PAY);
            })
            ->where('project_applicants.project_id', $projectId)
            ->where('project_applicants.user_id', $userId)
            ->first();

        if (!is_null($project)) {
            if (!is_null($project->user)) {
                $timeTrack = TimeTrack::selectRaw('SUM(TIMESTAMPDIFF(SECOND, created_at, updated_at)) as total_time')
                    ->where('project_id', $project->project_id)
                    ->where('user_id', $project->user->id);
            } else {
                $timeTrack = TimeTrack::selectRaw('SUM(TIMESTAMPDIFF(SECOND, created_at, updated_at)) as total_time')
                    ->where('project_id', $project->project_id);
            }

            $totalTimeTrack = $timeTrack->first();

            // when status not complete, limit weekly
            if ($project->status != ProjectApplicant::STATUS_COMPLETED) {
                $timeTrack = $timeTrack->whereBetween('user_time_track.updated_at', [$startTime, $endTime]);
            }

            $timeTrack = $timeTrack->first();

            // calculate total time, total dollar get by user, and check if moveer coin is already given
            $totalTime = !is_null($timeTrack->total_time) ? (int) $timeTrack->total_time : 0;
            $totalDollar = $project->pay_type == ProjectApplicant::PAY_TYPE_HOURLY_PAY ? $project->quote_price * floor($totalTime / 3600) : $project->quote_price;

            $totalTimeAll = !is_null($totalTimeTrack->total_time) ? (int) $totalTimeTrack->total_time : 0;
            $totalDollarAll = $project->pay_type == ProjectApplicant::PAY_TYPE_HOURLY_PAY ? $project->quote_price * floor($totalTimeAll / 3600) : $project->quote_price;
            $isCoinGiven = !is_null($project->amount) ? 
                $totalDollarAll == $project->amount : false;

            // coin already given
            if ($isCoinGiven) {
                return collect([
                    'status' => 'failed',
                    'message' => trans('common.coin_already_given'),
                    'data' => []
                ]);
            }

            $totalDollar = !$project->isCoinGiven ?
                $totalDollarAll - floatval($project->amount) : 0;
            if ($totalDollar != $amount) {
                return collect([
                    'status' => 'failed',
                    'message' => trans('common.amount_not_same'),
                    'data' => []
                ]);
            }

            return collect([
                'status' => 'success',
                'message' => trans('common.amount_same'),
                'data' => []
            ]);
        }

        return collect([
            'status' => 'failed',
            'message' => trans('common.project_not_found'),
            'data' => []
        ]);
    }

    public function addUserCoin(Request $request)
    {
        $userId = $request->user_id;
        $projectId = $request->project_id;
        $amount = $request->amount;

        $user = User::find($userId);
        if (is_null($userId) || $userId == '' || is_null($user)) {
            return collect([
                'status' => 'failed',
                'message' => trans('common.user_not_found'),
                'data' => []
            ]);
        }

        $project = Project::with('coinProject')->where('id', $projectId)->first();
        if (is_null($projectId) || $projectId == '' || is_null($project)) {
            return collect([
                'status' => 'failed',
                'message' => trans('common.project_not_found'),
                'data' => []
            ]);
        }

        if (is_null($amount) || $amount <= 0) {
            return collect([
                'status' => 'failed',
                'message' => trans('common.amount_empty'),
                'data' => []
            ]);
        }

        $userCoin = UserCoin::where('user_id', $userId)
            ->where('project_id', $projectId)
            ->where('coin_type', $project->coinProject->coin_type)
            ->first();
        if (is_null($userCoin)) {
            $userCoin = new UserCoin();
            $userCoin->project_id = $project->id;
            $userCoin->user_id = $user->id;
            $userCoin->coin_type = $project->coinProject->coin_type;
            $userCoin->amount = $amount;
            $userCoin->save();
        } else {
            // update only happen for hourly pay
            $userCoin->amount = $userCoin->amount + $amount;
            $userCoin->save();
        }

        return collect([
            'status' => 'success',
            'message' => trans('common.success_send_user_coin'),
            'data' => []
        ]);
    }

    public function coinList() {
        return view('backend.coin.coinList');
    }

    public function getListCoin(Request $request)
    {
        $coin = Coins::all();
        return collect([
            'status' => 'success',
            'message' => '',
            'data' => $coin
        ]);
    }

    public function submitCoin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'integer|exists:coins,id|nullable',
            'name_en' => 'string|required',
            'name_cn' => 'string|required',
            'contract' => 'string|required',
            'symbol' => 'string|required',
            'decimal' => 'string|required'
        ]);
        if ($validator->fails()) {
            return collect([
                'status' => 'failed',
                'message' => $validator->errors()->first(),
                'data' => []
            ]);
        }

        $id = $request->id;
        $name_en = $request->name_en;
        $name_cn = $request->name_cn;
        $contract = $request->contract;
        $symbol = $request->symbol;
        $decimal = $request->decimal;

        // create
        if (is_null($id)) {
            $coin = new Coins();
            $coin->name_en = $name_en;
            $coin->name_cn = $name_cn;
            $coin->contract_address = $contract;
            $coin->symbol = $symbol;
            $coin->decimal = $decimal;

            if ($request->file('img')) {
                if (!is_null($coin->img)) {
                    $this->deleteImg(Coins::UPLOAD_FOLDER, $coin->img);
                }
                $img = $this->uploadFile(Coins::UPLOAD_FOLDER, 'img', $request);
                $coin->img = $img['icon'];
            }

            $coin->save();
        // update
        } else {
            $coin = Coins::where('id', $id)->first();
            if (!is_null($coin)) {
                $coin->name_en = $name_en;
                $coin->name_cn = $name_cn;
                $coin->contract_address = $contract;
                $coin->symbol = $symbol;
                $coin->decimal = $decimal;

                if ($request->file('img')) {
                    if (!is_null($coin->img)) {
                        $this->deleteImg(Coins::UPLOAD_FOLDER, $coin->img);
                    }
                    $img = $this->uploadFile(Coins::UPLOAD_FOLDER, 'img', $request);
                    $coin->img = $img['icon'];
                }

                $coin->save();
            } else {
                return collect([
                    'status' => 'failed',
                    'message' => trans('common.coin_not_found'),
                    'data' => []
                ]);
            }
        }

        return collect([
            'status' => 'success',
            'message' => trans('common.coin_update_success'),
            'data' => []
        ]);
    }

    public function deleteCoin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'integer|exists:coins,id|required',
        ]);
        if ($validator->fails()) {
            return collect([
                'status' => 'failed',
                'message' => $validator->errors()->first(),
                'data' => []
            ]);
        }

        $coin = Coins::where('id', $request->id)->first();
        $coin->delete();

        return collect([
            'status' => 'success',
            'message' => trans('common.coin_delete_success'),
            'data' => []
        ]);
    }
}
