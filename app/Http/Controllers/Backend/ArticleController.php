<?php

namespace App\Http\Controllers\Backend;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ArticleController extends Controller
{
    public function articleCreate(){
        return view('backend.article.create');
    }
    public function saveArticle(Request $request){
        $rules = array(
            'file' => 'mimes:jpeg,jpg,bmp,png,gif|required' // max 10000kb
        );
        $fileArray = array('file' => $request->file('file'));
        $validator = Validator::make($fileArray, $rules);
        if($validator->fails()) {
            return redirect()->back()->with('error_message', 'Please Choose jpeg,jpg,png,gif extension');
        } else {
            if ($request->hasFile('file')) {
                $path = 'uploads/articles';
                $filename = $request->file('file')->getClientOriginalName();
                $save = $request->file('file')->move($path, $filename);
            }
            Article::create([
                'title' => $request->title,
                'article_description' => $request['article_description'],
                'files' => isset($filename) ? $filename : '',
            ]);

            return redirect()->route('backend.articleList')->with('success_message', 'Article Added Successfully.');
        }
    }

    public function articleList(){
        $article = Article::all();
        return view('backend.article.list',[
            'article' => $article
        ]);
    }

    public function edit($id){
        $article = Article::where('id','=',$id)->first();
        return view('backend.article.edit',[
           'article' => $article
        ]);
    }

    public function updateArticle(Request $request ,$id){
        Article::where('id','=',$id)->update([
            'title' => $request->title,
            'article_description' => $request->article_description,
        ]);
        return redirect()->route('backend.articleList')->with('success_message','Article Update Successfully .');
    }

    public function deleteArticle($id){
        Article::where('id','=',$id)->delete();
        return redirect()->route('backend.articleList')->with('success_message','Article Delete Successfully .');
    }
}
