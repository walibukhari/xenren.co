<?php

namespace App\Http\Controllers\Backend;

use Auth;
use Input;
use Datatables;
use Validator;
use DB;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;
use App\Models\User;
use App\Models\Skill;
use App\Models\SkillKeyword;
use App\Models\SkillKeywordGroup;
use Illuminate\Pagination\LengthAwarePaginator;


class SkillKeywordController extends BackendController
{
    public function index(Request $request) {
        $model = SkillKeyword::with('user')
            ->orderBy('id','desc')
            ->paginate(30);

        if($request->username) {
            $username = $request->username;
            $model = SkillKeyword::with(['user' =>  function($q) use ($username){
                $q->where('name','=',$username);
            }])
                ->whereHas('user', function ($q) use ($username){
                    $q->where('name','=',$username);
                })
                ->paginate(30);
        }
        if($request->key_word_name) {
            $model = SkillKeyword::with('user')->where('name_en','=',$request->key_word_name)->paginate(30);
        }
        return view('backend.skillkeyword.index',[
            'modal' => $model
        ]);
    }

    public function arrayPaginator($array, $request)
    {
        $setUrl = $request->url();
        $pageN = $request->page ? $request->page : 1;
        $page = \Input::get('page', $pageN);
        $perPage = 20;
        $offset = ($page * $perPage) - $perPage;
        $paginate =  new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,
            ['path' => $setUrl, 'query' => $request->query()]);
        return $paginate;
    }

    public function indexDt(Request $request, $id = null) {
        $model = SkillKeyword::with('user');

        return Datatables::of($model)
            ->addColumn('user', function ($model) {
                return $model->user->getName();
            })
            ->addColumn('name_en', function ($model) {
                return $model->name_en;
            })
            ->addColumn('name_cn', function ($model) {
                return $model->name_cn;
            })
            ->addColumn('reference_link', function ($model) {
                if($model->reference_link != "")
                {
                    return "<a href='". $model->reference_link . "' target='_blank'> 链接 </a>";
                }
                else
                {
                    return " - ";
                }
            })
            ->addColumn('submit_count', function ($model) {
                return $model->getSubmitCount();
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = '';

                $actions .= buildRemoteLinkHtml([
                    'url' => route('backend.skillkeyword.approve', ['id' => $model->id]),
                    'description' => '<abbr style="cursor: pointer" title="check circle keyword"><i class="fa fa-check-circle"></i></abbr>',
                    'modal' => '#remote-modal',
                    'toolTip' => 'check circle keyword'
                ]);

                $actions .= buildConfirmationLinkHtml([
                    'url' => route('backend.skillkeyword.delete', ['id' => $model->id]),
                    'description' => '<abbr style="cursor: pointer" title="delete keyword"><i class="fa fa-trash"></i></abbr>',
                    'title' => '确认删除？',
                    'content' => '确认删除此技能关键字',
                    'toolTip' => 'delete keyword'
                ]);

                $actions .= buildConfirmationLinkHtml([
                    'url' => route('backend.skillkeyword.ban', ['id' => $model->user_id]),
                    'description' => '<abbr style="cursor: pointer" title="ban keyword"><i class="fa fa-user-times"></i></abbr>',
                    'title' => '确认禁止？',
                    'content' => '确认禁止此人提交技能关键字',
                    'toolTip' => 'ban keyword'
                ]);

                $actions .= buildLinkHtmlForSubmit([
                    'description' => ' <input type="checkbox" value="'.$model->id.'" name="rows[]">',
                ]);

                return $actions;
            })
	          ->rawColumns(['actions'])
            ->make(true);
    }

    public function indexDtt(Request $request) {
        $model = SkillKeyword::with('user');

        return Datatables::of($model)
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('user', function ($model) {
                return $model->user->getName();
            })
            ->addColumn('name_en', function ($model) {
                return $model->name_en;
            })
            ->addColumn('name_cn', function ($model) {
                return $model->name_cn;
            })
            ->addColumn('reference_link', function ($model) {
                if($model->reference_link != "")
                {
                    return "<a href='". $model->reference_link . "' target='_blank'> 链接 </a>";
                }
                else
                {
                    return " - ";
                }
            })
            ->addColumn('submit_count', function ($model) {
                return $model->getSubmitCount();
            })
            ->addColumn('actions', function ($model) {
                $actions = '';

                $actions .= buildRemoteLinkHtml([
                    'url' => route('backend.skillkeyword.approve', ['id' => $model->id]),
                    'description' => '<abbr style="cursor: pointer" title="check circle keyword"><i class="fa fa-check-circle"></i></abbr>',
                    'modal' => '#remote-modal',
                    'toolTip' => 'check circle keyword'
                ]);

                $actions .= buildConfirmationLinkHtml([
                    'url' => route('backend.skillkeyword.delete', ['id' => $model->id]),
                    'description' => '<abbr style="cursor: pointer" title="delete keyword"><i class="fa fa-trash"></i></abbr>',
                    'title' => '确认删除？',
                    'content' => '确认删除此技能关键字',
                    'toolTip' => 'delete keyword'
                ]);

                $actions .= buildConfirmationLinkHtml([
                    'url' => route('backend.skillkeyword.ban', ['id' => $model->user_id]),
                    'description' => '<abbr style="cursor: pointer" title="ban keyword"><i class="fa fa-user-times"></i></abbr>',
                    'title' => '确认禁止？',
                    'content' => '确认禁止此人提交技能关键字',
                    'toolTip' => 'ban keyword'
                ]);

                $actions .= buildConfirmationLinkHtml([
                    'description' => ' <input type="checkbox" value="'.$model->id.'" name="rows[]">',
                ]);

                return $actions;
            })
	          ->rawColumns(['actions'])
            ->make(true);
    }

    public function delete($id) {
        $model = SkillKeyword::GetSkillKeyword()->find($id);

        try {
            if ($model->delete()) {
                return back();
            } else {
                throw new \Exception('未能删除技能关键字，请稍候再试');
            }
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function bulkDelete(Request $request)
    {
        $rows = explode(',',$request->rows[0]);
        if($rows[0] == '') {
            return redirect()->back()->with('error','Please Select Bulk Data..!');
        }
        try {
            SkillKeyword::whereIn('id', $rows)->delete();
            return redirect()->back()->with('message','Records Deleted..!');
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function ban($user_id){
        $model = User::find($user_id);

        try {
            $model->is_ban_submit_keyword = 1;
            $model->save();

            SkillKeyword::where('user_id', $user_id)->delete();

            return makeResponse('成功删除及禁止此人提交技能关键字');
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function approve($id){
        $model = SkillKeyword::GetSkillKeyword()->find($id);
        if( $model->name_en != ""){
            $reference_link_cn = "";
            $reference_link_en = $model->reference_link;
        }else{
            $reference_link_cn = $model->reference_link;
            $reference_link_en = "";
        }

        return view('backend.skillkeyword.approve', [
            'model' => $model,
            'reference_link_cn' => $reference_link_cn,
            'reference_link_en' => $reference_link_en

        ]);
    }

    public function approvePost(Request $request, $id){
        $model = Skill::GetSkill()
            ->where('name_cn', $request->get('name_cn'))
            ->first();

        if ($model) {
            return makeResponse(trans('common.this_keyword_already_in_skill_management'), true);
        }

        $model = Skill::GetSkill()
            ->where('name_en', $request->get('name_en'))
            ->first();

        if ($model) {
            return makeResponse(trans('common.skill_keyword_already_exist'), true);
        }

        if( $request->get('name_cn') == "" ){
            return makeResponse(trans('common.please_input_skill_keyword_chinese'), true);
        }

        if( $request->get('name_en') == "" ){
            return makeResponse(trans('common.please_input_skill_keyword_english'), true);
        }

        $groupId = DB::table('skill_keyword_groups')->max('group');
        if( $groupId == null )
        {
            $groupId = 1;
        }
        else
        {
            $groupId += 1;
        }

        \DB::beginTransaction();
        $skillIdList = array();
        try {
            $model1 = new Skill();
            $model1->name_cn = $request->get('name_cn');
            $model1->name_en = $request->get('name_en');
            $model1->description_en = $request->get('name_en');
            $model1->description_cn = $request->get('name_en');
            $model1->reference_link_en = $request->get('reference_link_en');
            $model1->reference_link_cn = $request->get('reference_link_cn');
            $model1->tag_color = 1;  //White Tag
            $model1->save();
            array_push($skillIdList, $model1->id);

            $model = new Skill();
            $model->name_cn = $request->get('name_cn');
            $model->description_cn = $request->get('name_cn');
            $model->name_en = $request->get('name_en');
            $model->description_en = $request->get('name_en');
            $model->reference_link_en = $request->get('reference_link_en');
            $model->reference_link_cn = $request->get('reference_link_cn');
            $model->tag_color = 2;  //Gold Tag
            $model->save();
            array_push($skillIdList, $model->id);

            foreach($skillIdList as $skillID)
            {
                $model = new SkillKeywordGroup();
                $model->skill_id = $skillID;
                $model->group = $groupId;
                $model->save();
            }


            $model = SkillKeyword::find($id);
            $model->delete();

            \DB::commit();
            return makeResponse(trans('common.successfully_approve_this_skill_keyword'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse($e->getMessage(), true);
        }
    }
}
