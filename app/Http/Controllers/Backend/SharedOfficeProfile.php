<?php

namespace App\Http\Controllers\Backend;

use App\Models\Staff;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\OwnerProfileUpdate;
use Illuminate\Support\Facades\Auth;

class SharedOfficeProfile extends Controller
{
    public function index(){
        return view('backend.sharedoffice.profile.index');
    }

    public function ProfileUpdate(OwnerProfileUpdate $request){
        $staff = Auth::guard('staff')->user();
        Staff::where('id','=',$staff->id)->update([
            'password' => bcrypt($request->password)
        ]);
        return collect([
           'status' => 'true',
           'message' => 'profile updated successfully'
        ]);
    }
}
