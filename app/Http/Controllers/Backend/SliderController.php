<?php

namespace App\Http\Controllers\Backend;

use Auth;
use Input;
use Datatables;
use Validator;
use DB;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;
use App\Http\Requests\Backend\SliderCreateFormRequest;
use App\Http\Requests\Backend\SliderUpdateFormRequest;
use App\Models\Sliders;

class SliderController extends BackendController
{
   public function index(Request $request) {
        return view('backend.themes.slider.index');
    }

    public function indexDt(Request $request) {
        $model = Sliders::all();

        return Datatables::of($model)
            ->addColumn('image', function ($model) {
                $image_link = $model->image;
                $image =  "<img class='img-thumbnail slider-thumb' src=".url($image_link)." >";
                return $image;
            })
            ->addColumn('title', function ($model) {
                $title = $model->title;
                return $title;
            })
            ->addColumn('sub_title', function ($model) {
                $sub_title = $model->sub_title;
                return $sub_title;
            })
            ->addColumn('link', function ($model) {
                $link = $model->link;
                return $link;
            })
            ->addColumn('actions', function ($model) {
                $actions = '<center>'.buildNormalLinkHtml([
                    'url' => route('backend.themes.slider.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-pencil"></i>'
                ]);
                $actions .= buildConfirmationLinkHtml([
                    'url' => route('backend.themes.slider.delete', ['id' => $model->id]),
                    'description' => '<i class="fa fa-trash"></i>',
                ]).'</center>';
                return $actions;
            })
	          ->rawColumns(['actions'])
            ->make(true);
    }

    public function create(Request $request) {
        return view('backend.themes.slider.create');
    }


    public function createPost(SliderCreateFormRequest $request) {
        try {
            $input = $request->all();

            $model = new Sliders();
            $model->title = $input['title'];
            $model->sub_title = $input['sub_title'];
            $model->link = $input['link'];
            $model->staff_id = \Auth::guard('staff')->user()->id;

            if ($request->hasFile('image')){

                $img    = \Image::make($request->file('image'));
                $mime   = $img->mime();
                $ext    = convertMimeToExt($mime);

                $path   = 'uploads/sliders/';
                $filename = 'slider_' . generateRandomUniqueName();

                touchFolder($path);

                $request->file('image')->move($path, $filename.'.'.$ext);

                $input['image'] = ($path. $filename.'.'.$ext);
            }

            $model->image = $input['image'];
            $model->save();
            return makeResponse('成功新增幻灯片');
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function edit($id) {
        $model = Sliders::find($id);

        return view('backend.themes.slider.edit', ['model' => $model]);
    }

    public function editPost(SliderUpdateFormRequest $request, $id) {
        $input = $request->all();
        
        $model = Sliders::find($id);

        if (!$model) {
            return makeResponse('幻灯片不存在', true);
        }

        try {
            $model->title = $input['title'];
            $model->sub_title = $input['sub_title'];
            $model->link = $input['link'];
            $model->staff_id = \Auth::guard('staff')->user()->id;

            if ($request->hasFile('image')){

                $img    = \Image::make($request->file('image'));
                $mime   = $img->mime();
                $ext    = convertMimeToExt($mime);

                $path   = 'uploads/sliders/';
                $filename = 'slider_' . generateRandomUniqueName();

                touchFolder($path);

                $request->file('image')->move($path, $filename.'.'.$ext);

                $input['image'] = ($path. $filename.'.'.$ext);
            }
            
            if ($input['image'] == ''){
                
                $model->save();
            
            } else {

                $model->image = $input['image'];
                $model->save();
            }

            return makeResponse('成功编辑幻灯片');
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function delete($id) {
        $model = Sliders::find($id);

        try {
            if ($model->delete()) {
                return makeResponse('成功删除幻灯片');
            } else {
                throw new \Exception('未能删除幻灯片，请稍候再试');
            }
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }
}
