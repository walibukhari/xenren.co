<?php

namespace App\Http\Controllers\Backend;

use App\Models\CommunityDiscussion;
use App\Models\CommunityReply;
use App\Models\ReportCoummunityTopic;
use App\Models\Topic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommunityController extends Controller
{
    public function communityReports()
    {
        $reports = ReportCoummunityTopic::all();
        return view('backend.community.report',[
            'reports' => $reports
        ]);
    }

    public function editReport($id)
    {
        $reports = ReportCoummunityTopic::where('id','=',$id)->first();
        $discussion = CommunityDiscussion::where('id','=',$reports->discussion_id)->first();
        return view('backend.community.editReport',[
            'reports' => $reports,
            'discussion' => $discussion
        ]);
    }

    public function updateReportedDiscussion(Request $request) {
        CommunityDiscussion::where('id','=',$request->d_id)->update([
           'name' => $request->name,
           'description' => $request->description
        ]);
        return back()->with('success','topic discussion updated');
    }

    public function deleteReport($id)
    {
        $report = ReportCoummunityTopic::where('id','=',$id)->first();
        if($report->replay_id != 0) {
            $c_reply = CommunityReply::where('id','=',$report->replay_id)->first();
            $c_reply->delete();
        } else {
            $discussion = CommunityDiscussion::where('id', '=', $report->discussion_id)->first();
            $discussion->delete();
        }
        $report->delete();
        return back();
    }

    public function communityIndex(Request $request)
    {
        $topics = Topic::all();
        return view('backend.community.index',[
            'topics' => $topics
        ]);
    }

    public function communityManagePostsIndex(){
        $posts = CommunityDiscussion::all();
        return view('backend.community.managePost',[
            'posts' => $posts
        ]);
    }

    public function deletePost($id){
        CommunityDiscussion::where('id','=',$id)->delete();
        return back()->with('message','post deleted success');
    }

    public function stopPost($id){
        CommunityDiscussion::where('id','=',$id)->update([
            'status' => CommunityDiscussion::STATUS_STOP
        ]);
        return back()->with('message','post stop success');
    }

    public function startPost($id){
        CommunityDiscussion::where('id','=',$id)->update([
            'status' => CommunityDiscussion::STATUS_START
        ]);
        return back()->with('message','post stop success');
    }

    public function createTopic(Request $request){
        Topic::createTopics($request->all());
        return back()->with('success','Topic Created successfully');
    }

    public function editTopic($id){
        $topic = Topic::where('id','=',$id)->first();
        return collect([
            'success' => true,
            'data' => $topic
        ]);
    }

    public function updateTopic(Request $request){
        Topic::where('id','=',$request->_id)->update([
           'topic_name' => $request->topic_name,
           'topic_description' => $request->topic_description,
           'topic_type' => $request->topic_type,
        ]);
        return back()->with('success','Topic Update Successfully');
    }

    public function deleteTopic($id){
        Topic::where('id','=',$id)->delete();
        return back()->with('success','Topic Delete Successfully');
    }
}
