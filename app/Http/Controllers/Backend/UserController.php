<?php

namespace App\Http\Controllers\Backend;

use App\Models\CoinLog;
use App\Models\Invitation;
use App\Models\UserActivity;
use App\Models\UserInbox;
use App\Models\UserInboxMessages;
use Auth;
use Input;
use Datatables;
use Validator;
use DB;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;
use App\Models\User;
use App\Models\UserIdentity;
use App\Models\ManageCoin;
use App\Models\UserCoins;
use App\Http\Requests\Backend\UserCreateFormRequest;
use App\Http\Requests\Backend\UserEditFormRequest;
use App\Http\Requests\Backend\RatingEditFormRequest;
use App\Models\Settings;
use App\Models\UserXencoinDetail;

class UserController extends BackendController
{
    public function userActivity() {
        $userActivity = UserActivity::orderBy('id','desc')->with('user')->get();
        return view('backend.user.userActivity',[
            'data' => $userActivity
        ]);
    }

    public function index(Request $request) {
        return view('backend.user.index');
    }

		public function userIdentityManagement(Request $request)
		{
			$data = UserIdentity::with('user')
				->orderBy('updated_at', 'desc')
                ->where('status', '!=', UserIdentity::STATUS_NEW)
				->paginate(20);
			return view('backend.user.user_identity_management',[
				'data' => $data
			]);
		}

		public function userIdentityManagementDetailApprove(Request $request, $id)
		{
            $invitorUserCommision = Settings::where('key','invitor_user_commission')->first();
            if(!$invitorUserCommision){
                Settings::create([
                    'key'=>'invitor_user_commission',
                    'value'=>false
                ]);

            $invitorUserCommision = Settings::where('key','invitor_user_commission')->first();
            }
            $invitedUserCommission = Settings::where('key','invited_user_commission')->first();
            if(!$invitedUserCommission){
                Settings::create([
                    'key'=>'invited_user_commission',
                    'value'=>false
                ]);
            $invitedUserCommission = Settings::where('key','invited_user_commission')->first();

            }

			(UserIdentity::where('id', '=', $id)->update([
				'status' => UserIdentity::STATUS_APPROVED
			]));
            $data = UserIdentity::where('id', '=', $id)->first();
            User::where('id', '=', $data->user_id)->update([
                'is_validated' => User::IS_VALIDATED_APPROVED
            ]);

            $user = User::where('id', '=', $data->user_id)->first();
            $isInvited = UserXencoinDetail::where('user_id',$user->id)->first();
            if($user->upline_id){
                if($invitorUserCommision->value==true){
                    $InviteSender = User::where('id', '=', $user->upline_id)->first();
                    $xen_coins = $InviteSender->xen_coin;
                    if(is_null($xen_coins)){
                        $xen_coins=10;
                    }else{
                        $xen_coins+=10;
                    }
                    $InviteSender->xen_coin = $xen_coins;
                    $InviteSender->save();
                }

                if($invitedUserCommission->value==true){
                    $xen_coins = $user->xen_coin;
                    if(is_null($xen_coins)){
                        $xen_coins=10;
                    }else{
                        $xen_coins+=10;
                    }
                    $user->xen_coin = $xen_coins;
                    $user->save();
                }
            }


			return redirect(route('backend.user_identity_management'));
		}

		public function userIdentityManagementDetailReject(Request $request, $id)
		{
			(UserIdentity::where('id', '=', $id)->update([
				'status' => UserIdentity::STATUS_REJECTED
			]));
            $data = UserIdentity::where('id', '=', $id)->first();
            User::where('id', '=', $data->user_id)->update([
                'is_validated' => User::IS_VALIDATED_REJECTED
            ]);
			return redirect(route('backend.user_identity_management'));
		}

		public function userIdentityManagementDetail(Request $request, $id)
		{
			$data = UserIdentity::with(['user', 'country', 'country.locale' => function($q){
				$q->where('locale', '=', 'zh-cn');
		}])->where('user_id', '=', $id)
				->first();
			return view('backend.user.user_identity_management_detail',[
				'data' => $data
			]);
		}

    public function indexDt(Request $request) {
        $model = User::GetUsers();

        return Datatables::of($model)
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = buildRemoteLinkHtml([
                    'url' => route('backend.user.edit', ['member_id' => $model->id]),
                    'description' => '<i class="fa fa-pencil" title="edit"></i>',
                    'modal' => '#remote-modal',
                ]);
                $actions .= buildConfirmationLinkHtml([
                    'url' => route('backend.user.delete', ['member_id' => $model->id]),
                    'description' => '<i class="fa fa-trash" title="delete"></i>',
                ]);
//				$actions .= buildRemoteLinkHtml([
//                    'url' => route('backend.user.info', ['member_id' => $model->id]),
//                    'description' => '<i class="fa fa-info"></i>',
//                    'modal' => '#remote-modal',
//                ]);

                $actions .= buildRemoteLinkHtml([
                    'url' => route('backend.user.sendmessage', ['id' => $model->id]),
                    'description' => '<i class="fa fa-inbox" title="send message"></i>',
                    'color' => 'yellow',
                    'modal' => '#remote-modal'
                ]);
				$actions .= buildRemoteLinkHtml([
                    'url' => route('backend.user.sendcoin', ['id' => $model->id]),
                    'description' => '<i class="fa fa-money" title="send coin"></i>',
                    'color' => 'yellow',
                    'modal' => '#remote-modal'
                ]);
				$actions .= buildLinkHtml([
                    'url' => route('frontend.resume.getdetails', ['id' => $model->id,'lang'=>\Session::get('lang')]),
                    'description' => '<i class="fa fa-link" title="get details"></i>',
                    'color' => 'green',
                ]);
                return $actions;
            })
	          ->rawColumns(['actions'])
            ->make(true);
    }

    public function create(Request $request) {
        return view('backend.user.create');
    }

    public function createPost(UserCreateFormRequest $request) {
        try {
            $model = new User();
            $model->email = $request->get('email');
            $model->password = $request->get('password');
			$model->name = $request->get('real_name');
			$model->date_of_birth = $request->get('birth_date');
			$model->experience = $request->get('work_experience');
			$model->handphone_no = $request->get('handphone');
			$model->address = $request->get('address');
			$model->gender = $request->get('gender');
//			$model->comment_rating = $request->get('comment_rating');
			$model->account_balance = $request->get('account_balance');
            $model->is_freeze = $request->get('is_freeze');

            $model->save();
            return makeResponse('成功新增会员');
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function edit($id) {
        $model = User::find($id);

        return view('backend.user.edit', ['model' => $model]);
    }

    public function editPost(UserEditFormRequest $request, $id) {
        $model = User::GetUsers()->find($id);

        if (!$model) {
            return makeResponse(trans('common.member_not_exists'), true);
        }

        try {
            $model->email = $request->get('email');
            if ($request->has('password') && $request->get('password') != '') {
                $model->password = $request->get('password');
            }

            //Check for email exists
            $email_exists = User::where('email', '=', $model->email)->where('id', '!=', $model->id)->exists();
            if ($email_exists) {
                throw new \Exception(trans('common.email_has_been_registered'));
            }

			$model->name = $request->get('real_name');
			$model->date_of_birth = $request->get('birth_date');
			$model->experience = $request->get('work_experience');
			$model->handphone_no = $request->get('handphone');
			$model->address = $request->get('address');
			$model->gender = $request->get('gender');
//			$model->comment_rating = $request->get('comment_rating');
			$model->account_balance = $request->get('account_balance');
            $model->is_freeze = $request->get('is_freeze');
            $model->is_agent = $request->get('is_agent');

            $model->save();
            return makeResponse(trans('common.success_edit_member'));
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function delete($id) {
        $model = User::find($id);

        try {
            if ($model->delete()) {
                return makeResponse('成功删除会员');
            } else {
                throw new \Exception('未能删除会员，请稍候再试');
            }
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function sendMessage($userId)
    {
        $user = User::find($userId);

        return view('backend.user.sendMessage', [
            'user' => $user
        ]);
    }

    public function sendMessagePost($userId, Request $request)
    {
        $user = User::find($userId);

        try {
            \DB::beginTransaction();

            //add user inbox notification
            $userInbox = new UserInbox();
            $userInbox->category = UserInbox::CATEGORY_NORMAL;
            $userInbox->project_id = null;
            $userInbox->from_user_id = null;
            $userInbox->to_user_id = $user->id;
            $userInbox->from_staff_id = Auth::guard('staff')->user()->id;
            $userInbox->to_staff_id = null;
            $userInbox->type = UserInbox::TYPE_PROJECT_NEW_MESSAGE;
            $userInbox->is_trash = 0;
            $userInbox->save();

            //add user inbox message
            $userInboxMessage = new UserInboxMessages();
            $userInboxMessage->inbox_id = $userInbox->id;
            $userInboxMessage->from_user_id = null;
            $userInboxMessage->to_user_id = $user->id;
            $userInboxMessage->from_staff_id = Auth::guard('staff')->user()->id;
            $userInboxMessage->to_staff_id = null;
            $userInboxMessage->project_id = null;
            $userInboxMessage->is_read = 0;
            $userInboxMessage->type = UserInbox::TYPE_PROJECT_NEW_MESSAGE;

            $message = $request->get('message');
            $userInboxMessage->custom_message = $message;
            $userInboxMessage->quote_price = null;
            $userInboxMessage->pay_type = null;
            $userInboxMessage->file = null;


            if ($userInboxMessage->save()) {

                \DB::commit();
                return makeResponse(trans('common.submit_successful'));

            } else {
                \DB::rollback();
                throw new \Exception(trans('common.err_happen'));
            }
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse($e->getMessage(), true);
        }
    }

   public function sendCoin($userId)
    {
        $user = User::find($userId);
        $coins = ManageCoin::all(['id', 'coins']);
        return view('backend.user.sendCoin', [
            'user' => $user,'coins' =>$coins
        ]);
    }

    public function sendCoinPost($userId, Request $request)
    {
        $user = User::find($userId);

        try {
            \DB::beginTransaction();
            $input = $request->all();
            //add user inbox notification
            $userCoins = new UserCoins();
            $userCoins->receiver_id = $user->id;
            $userCoins->admin_id = Auth::guard('staff')->user()->id;
            $userCoins->sender_id = null;
            $userCoins->coin_type_id = $input['coin_type'];;
            $userCoins->number_of_coin = $input['number_of_coins'];;
            $userCoins->coin_type = $input['coin_type_name'];;





            if ($userCoins->save()) {

                \DB::commit();
                return makeResponse(trans('common.submit_successful'));

            } else {
                \DB::rollback();
                throw new \Exception(trans('common.err_happen'));
            }
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse($e->getMessage(), true);
        }
    }

    public function submitID() {
		return view('backend.user.submitid');
	}

	public function submitIDDt() {
		$model = UserIdentity::where('status', 0)
            ->where('is_submit', 1);

        return Datatables::of($model)
			->addColumn('email', function ($model) {
                return $model->getOwnerEmail();
            })
			->addColumn('gender_text', function ($model) {
                return $model->getGenderText();
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = buildRemoteLinkHtml([
                    'url' => route('backend.user.identityInfo', ['request_id' => $model->id]),
                    'description' => '<i class="fa fa-info"></i>',
                    'color' => 'green',
                    'modal' => '#remote-modal'
                ]);
                $actions .= buildConfirmationLinkHtml([
                    'url' => route('backend.user.approve.id', ['request_id' => $model->id]),
                    'color' => 'blue',
                    'description' => '<i class="fa fa-check"></i>',
                ]);
                $actions .= buildRemoteLinkHtml([
                    'url' => route('backend.user.reject', ['request_id' => $model->id]),
                    'description' => '<i class="fa fa-remove"></i>',
                    'color' => 'red',
                    'modal' => '#remote-modal'
                ]);
                return $actions;
            })
	          ->rawColumns(['actions'])
            ->make(true);
	}

	/**
	 * approve a user's identity request
	 */
	public function approveId($id)
    {
        $model = UserIdentity::find($id);
		$model->status = 1;

        try {
            \DB::beginTransaction();

            if ($model->save()) {
				$user = User::find($model->user_id);
				$user->is_validated = 1;
				$user->is_tailor = 1;
                $user->name = $model->name;
                $user->id_card_no = $model->id_card_no;
                $user->gender = $model->gender;
                $user->date_of_birth = $model->date_of_birth;
                $user->address = $model->address;
                $user->country_id = $model->country_id;
                $user->handphone_no = $model->handphone_no;

                $remark = "Downline ( UserID = " . $user->id . " ) personal info approve by admin. ";
                $count = CoinLog::where('remark', $remark)
                            ->get()
                            ->count();
                if( $count == 0 )
                {
                    $invitation = Invitation::where('invitee_id', $user->id)
                        ->first();

                    if( !empty($invitation) )
                    {
                        $invitor = User::find($invitation->invitor_id);
                        $totalAmount = ( $invitor->xen_coin + 5.00 ); //each user success approve will get 5 point;
                        $invitor->xen_coin = $totalAmount;
                        $invitor->save();

                        $coinLog = new CoinLog();
                        $coinLog->user_id = $invitation->invitor_id;
                        $coinLog->amount = 5;
                        $coinLog->current_balance = $totalAmount;
                        $coinLog->remark = $remark;
                        $coinLog->save();
                    }
                }

				if ($user->save()) {
                    \DB::commit();
					return makeResponse(trans('common.approve_successful'));
				} else {
                    \DB::rollback();
	                throw new \Exception(trans('common.err_happen'));
	            }

            } else {
                \DB::rollback();
                throw new \Exception(trans('common.err_happen'));
            }
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse($e->getMessage(), true);
        }
    }

    public function reject($id)
    {
        return view('backend.user.identity_reject', [
            'id' => $id
        ]);
    }

	/**
	 * reject a user's identity request
	 */
	public function rejectPost($id, Request $request) {
        $model = UserIdentity::find($id);
		$model->status = 2;
        $model->reject_reason = $request->get('reject_reason');
        $user = User::find($model->user_id);
        $user->is_validated = User::IS_VALIDATED_REJECTED;

        try {
            if ($model->save()) {
                return makeResponse(trans('common.reject_successful'));
            } else {
                throw new \Exception(trans('common.err_happen'));
            }
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function identityInfo($id)
    {
        $identityInfo = UserIdentity::with('user')->find($id);

        return view('backend.user.identity_info', [
            'identityInfo' => $identityInfo
        ]);
    }



//    /**
//	 * view rating and level of user
//	 */
//	public function info($id) {
//        $model = User::find($id);
//
//        return view('backend.user.info', [
//            'rating' => round($model->getRating()),
//            'level' => $model->getLevel()
//        ]);
//    }

	/**
	 * view rating list of user
	 */
	public function rating($id) {
		$model = User::find($id);
		return view('backend.user.rating', ['model' => $model]);
	}

}
