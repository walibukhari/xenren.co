<?php

namespace App\Http\Controllers\Backend;

use App\Jobs\SharedOfficeSearchJson;
use App\Libraries\MobileDetect\Mobile_Detect;
use App\Models\Bulletin;
use App\Models\Invoice;
use App\Models\OwnerProgress;
use App\Models\PostFiles;
use App\Models\SharedOfficeCategory;
use App\Models\SharedOfficeSetting;
use Illuminate\Support\Facades\Validator;
use App\Models\SharedOffice;
use App\Models\SharedOfficeBarcode;
use App\Models\SharedOfficeBooking;
use App\Models\SharedOfficeChatAction;
use App\Models\SharedOfficeFinance;
use App\Models\SharedOfficeImages;
use App\Models\SharedOfficeLanguage;
use App\Models\SharedOfficeProductCategory;
use App\Models\SharedOfficeProducts;
use App\Models\SharedOfficesBulletin;
use App\Models\Shfacilitie;
use App\Models\Skill;
use App\Models\Staff;
use App\Models\Transaction;
use App\Models\User;
use App\Models\UserInboxMessages;
use App\Models\Withdraw;
use App\Models\WithdrawRequest;
use App\Models\WorldCountries;
use App\Models\PermissionGroup;
use App\Traits\CountryAndCityTrait;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Input;
use Datatables;
use PayPal\Api\Transactions;
use phpDocumentor\Reflection\DocBlock\Description;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;
use App\Http\Requests\Backend\SharedOfficeCreateFormRequest;
use App\Http\Requests\Backend\SharedOfficeUpdateFormRequest;
use App\Models\SharedOfficeRequest;
use App\Models\SharedOfficeFee;
use App\Models\Hiring;
use App\Models\SharedOfficeGroupChat;
use App\Models\SharedOfficeRating;
use App\Models\UserInbox;
use DateInterval;
use DateTime;
use DateTimeZone;
use App\Models\TimeZone;
use PDF;

class SharedOfficeController extends BackendController
{
    use CountryAndCityTrait;

    public function OwnerProgress(Request $request){
        $staff = Staff::where('staff_type','=',Staff::STAFF_TYPE_STAFF)->with('office')->Has('office')->get();
        return view('backend.ownerProgress.index',[
            'staff' => $staff
        ]);
    }

    public function Progress($id){
        $staff = Staff::where('id','=',$id)->with('office')->first();
        $createdAt = Carbon::parse($staff->created_at);
        $now = Carbon::now()->startOfMonth();
        $diff = $createdAt->diffInDays($now);
        if($diff > 30) {
            $diff = 30;
        }
        $staffProfile = $this->checkStaffProfile($staff);
        $progress = OwnerProgress::where('staff_id','=',$id)->first();
        if(isset($progress)) {
            $confirm_bookings = round($progress->confirm_bookings / $diff * 100, 0);
            $manage_bookings = round($progress->manage_bookings / $diff * 100, 0);
            $office_check_in = round($progress->office_check_in / $diff * 100, 0);
            if ($confirm_bookings > 100) {
                $confirm_bookings = 100;
            }
            if ($manage_bookings > 100) {
                $manage_bookings = 100;
            }
            if ($office_check_in > 100) {
                $office_check_in = 100;
            }
            $officeInfo = $progress->office_info;
        }
        return view('backend.ownerProgress.progress',[
            'progress' => $progress,
            'confirm_bookings' => isset($confirm_bookings) ? $confirm_bookings : 0,
            'office_info' => isset($officeInfo) ? $officeInfo : 0,
            'manage_bookings' => isset($manage_bookings) ? $manage_bookings : 0,
            'office_check_in' => isset($office_check_in) ? $office_check_in : 0,
            'owner_profile' => $staffProfile
        ]);
    }

    public function checkStaffProfile($staff){
        $total = 100;
        $overAll = '';
        if($staff->username == '') {
            $overAll = $total - 20;
        }
        if($staff->name == '') {
            $overAll = $total - 20;
        }
        if($staff->country_id == '') {
            $overAll = $total - 20;
        }
        if($staff->phone_number == ''){
            $overAll = $total - 20;
        }
        if($staff->img_avatar == ''){
            $overAll = $total - 20;
        }
        return $overAll;
    }


    public function saveInvoice(Request $request){
        Invoice::create([
           'invoice_number' => $request->invoice_no,
            'staff_id' => $request->staff_id,
            'date' => Carbon::parse($request->date),
            'amount' => $request->total_amount
        ]);
        return collect([
           'status' => true,
           'message' => 'invoice created successfuly'
        ]);
    }

    public function invoices() {
        $staff = \Auth::guard('staff')->user();
        $sharedOffice = Transaction::where('type','=',Transaction::TYPE_SHARED_OFFICE_ACCOUNT_BALANCE)->orwhere('performed_to','=',$staff->id)->get();
        $total_amount = [];
        $to = '';
        $date= [];
        foreach($sharedOffice as $office) {
            $to = Staff::where('id','=',$office->performed_to)->first();
            array_push($total_amount,$office->amount);
            array_push($date,$office->created_at);
        }
        $randomNo = 'X'.generateRandomString(10);
        if(count($date) > 0) {
            $date = $date[0];
        } else {
            $date = '';
        }
        $total_amount = array_sum($total_amount);
        return view('backend.sharedoffice.invoices.invoice',[
            'staff' => $staff,
            'invoices' => $sharedOffice,
            'total_amount' => $total_amount,
            'to' => $to,
            'date' => $date,
            'invoice_no' => $randomNo,
        ]);
    }

    public function downloadPDF()
    {
        $staff = \Auth::guard('staff')->user();
        $invoices = Transaction::where('type','=',Transaction::TYPE_SHARED_OFFICE_ACCOUNT_BALANCE)->orwhere('performed_to','=',$staff->id)->get();
        $total_amount = [];
        $to = '';
        $date= [];
        foreach($invoices as $office) {
            $to = Staff::where('id','=',$office->performed_to)->first();
            array_push($total_amount,$office->amount);
            array_push($date,$office->created_at);
        }
        $invoice_no = 'X'.generateRandomString(10);
        $date = $date[0];
        $total_amount = array_sum($total_amount);
        $image = public_path('images/Favicon.jpg');
        $pdF = PDF::loadView('pdf',compact(['invoices','staff','to','total_amount','date','invoice_no','image']))   ->setPaper('letter', 'portrait')
        ->setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true]);
        return $pdF->download('invoices.pdf');
    }

    public function showPDFPage()
    {
        $staff = \Auth::guard('staff')->user();
        $invoices = Transaction::where('type','=',Transaction::TYPE_SHARED_OFFICE_ACCOUNT_BALANCE)->orwhere('performed_to','=',$staff->id)->get();
        $total_amount = [];
        $to = '';
        $date= [];
        foreach($invoices as $office) {
            $to = Staff::where('id','=',$office->performed_to)->first();
            array_push($total_amount,$office->amount);
            array_push($date,$office->created_at);
        }
        $invoice_no = 'X'.generateRandomString(10);
        $date = $date[0];
        $total_amount = array_sum($total_amount);
        $image = public_path('images/Favicon.jpg');
        return view('pdf',[
            'staff' => $staff,
            'invoices' => $invoices,
            'total_amount' => $total_amount,
            'to' => $to,
            'date' => $date,
            'invoice_no' => $invoice_no,
            'image' => $image
        ]);
    }

    public function checkInSharedOffice(Request $request)
    {
        $token = $request->token;
        if (is_null($request->username)) {
            $username = '';
            return view('backend.sharedoffice.checkIn.checkIn', [
                'username' => $username,
                'token' => $token
            ]);
        } else {
            $user = User::where('name', 'like', '%' . $request->username . '%')->first();
            $sharedOfficeFinance = SharedOfficeFinance::where('user_id', '=', $user->id)
                ->with('office')
                ->get();
            $arr = collect($sharedOfficeFinance)->unique('office_id');
            $checkIn = [];
            $count = 0;
            foreach ($arr as $office) {
                $count += 1;
                array_push($checkIn, array(
                    'office_id' => $office->office_id,
                    'user_id' => $office->user_id,
                    'username' => $user->name,
                    'office_name' => isset($office->office->office_name) ? $office->office->office_name : 'office name missing',
                    'start_time' => $office->start_time,
                    'end_time' => $office->end_time,
                    'count' => $count
                ));
            }
            return view('backend.sharedoffice.checkIn.checkIn', [
                'checkIn' => $checkIn,
                'username' => $user,
                'token' => $token
            ]);
        }
    }

    public function sharedOfficeChat($id)
    {
        $staffType = \Auth::guard('staff')->user()->staff_type;
        $staffId = \Auth::guard('staff')->user()->id;
        $staff = \Auth::guard('staff')->user();
        $country = WorldCountries::all();
        $officeName = SharedOffice::where('id', '=', $id)->first();
        if ($staffType == Staff::STAFF_TYPE_STAFF) {
            if ($staff->permission_group_id == PermissionGroup::PERMISSION_GROUP_SHARED_OFFICE) {
                $getStaffOffice = SharedOffice::where('staff_id','=',$staff->id)->first();
            } else {
                $getStaffOffice = SharedOffice::where('id','=',$staff->office_id)->first();
            }

            $baseUrl = \URL::to('/');
            if (!is_null($officeName)) {
                if (is_null($getStaffOffice)) {
                    $url = $baseUrl . '/admin/sharedoffice/dashboard/' . $id;
                    return abort(403, 'Page Not Found.');
                }

                if ($officeName->staff_id != $staff->id && $officeName->id != $staff->office_id) {
                    $url = $baseUrl . '/admin/sharedoffice/dashboard/' . $getStaffOffice->id;
                    return abort(403, 'Page Not Found.');
                }
            } else {
                if(is_null($officeName)){
                    return back();
                } else {
                    $url = $baseUrl . '/admin/sharedoffice/dashboard/' . $getStaffOffice->id;
                    return abort(403, 'Page
                 Not Found.');
                }
            }
        }
        $staffCheckCurrencyExist = $this->checkStaffUserCurrency();
        if($staffCheckCurrencyExist != true) {
            $currencyData = WorldCountries::select(['id','name','currency_name','currency_code'])
                ->get()->sortBy('currency_code');
            $currencyData = collect($currencyData)->unique('currency_code');
        }
        return view("backend.sharedoffice.dashboard", [
            'id' => $id,
            'staff_type' => $staffType,
            'staff_id' => $staffId,
            'staffCheckCurrencyExist' => $staffCheckCurrencyExist,
            'currencies' => isset($currencyData) ? $currencyData : '',
            'countries' => $country,
            'office_name' => ucwords($officeName->office_name)
        ]);
    }

    public function staffCurrency(Request $request){
        $currency = WorldCountries::where('id','=',$request->currency_id)->first();
        if($currency){
            $staffId = \Auth::guard('staff')->user()->id;
            Staff::where('id','=',$staffId)->update([
               'country_id' => $currency->id
            ]);

            return collect([
               'status' => true,
               'message' => 'staff currency added successfully'
            ]);
        }
    }

    public function checkStaffUserCurrency(){
        $staffCurrency = \Auth::guard('staff')->user()->country_id;
        if(is_null($staffCurrency)) {
            return false;
        } else {
            return true;
        }
    }

    public function withdrawRequest(Request $request)
    {
        $staff = Staff::where('id','=',$request->staff_id)->first();
        $currency_name = Staff::getCurrencyName();
        if($staff->balance > $request->payment_request) {
            $deductBalance = $staff->balance - $request->payment_request;
            Staff::where('id', '=', $request->staff_id)->update([
                'balance' => $deductBalance
            ]);
            $transaction = Transaction::create([
                'amount' => $request->payment_request,
                'type' => Transaction::TYPE_SHARED_OFFICE_OWNER_STATUS_WITHDRAW_REQUEST,
                'status' => Transaction::STATUS_SHARED_OFFICE_OWNER_STATUS_WITHDRAW_REQUEST_SEND,
                'performed_by' => $request->staff_id,
                'performed_to' => 0,
                'currency' => $currency_name
            ]);

            WithdrawRequest::create([
                'bank_name' => $request->bank_name,
                'iban_number' => $request->bank_name,
                'card_number' => $request->card_number,
                'branch_name' => $request->branch_name,
                'branch_code' => $request->branch_code,
                'branch_address' => $request->branch_address,
                'payment_request' => $request->payment_request,
                'staff_id' => $request->staff_id,
                'office_id' => $request->office_id,
                'total_balance' => $deductBalance,
                'transaction_id' => $transaction->id,
                'country_id' => $request->country_id,
            ]);
            return collect([
                'success' => true,
                'message' => 'withdraw request created successfully, your remaining balance' . $deductBalance,
            ]);
        } else {
            return collect([
                'success' => false,
                'message' => 'you have not enough balance',
            ]);
        }
    }

    public function withdrawRequests()
    {
        $withdrawRequests = WithdrawRequest::with('transaction', 'staff', 'office')->get();
        return view('backend.winthdrawRequest.index', [
            'data' => $withdrawRequests
        ]);
    }

    public function rejectedPayments($id)
    {
        $previous = WithdrawRequest::where('staff_id', '=', $id)->where('status', '=', WithdrawRequest::STATUS_REJECTED)->get();
        return collect([
            'success' => true,
            'previous_data' => $previous
        ]);
    }

    public function approvedPayments($id)
    {
        $previous = WithdrawRequest::where('staff_id', '=', $id)->where('status', '=', WithdrawRequest::STATUS_APPROVED)->get();
        return collect([
            'success' => true,
            'previous_data' => $previous
        ]);
    }

    public function withdrawRequestApprove($id)
    {
        $withdrawRequests = WithdrawRequest::where('id', '=', $id)->with('transaction', 'staff', 'office')->first();
        return view('backend.winthdrawRequest.approve', [
            'dt' => $withdrawRequests
        ]);
    }

    public function requestApproved(Request $request)
    {
        $this->updateWithDrawRequest($request->all());
        $status = Transaction::where('id', '=', $request['transaction_id'])->update([
            'status' => Transaction::STATUS_SHARED_OFFICE_OWNER_STATUS_WITHDRAW_REQUEST_APPROVED
        ]);
        $getStaff = Transaction::where('id', '=', $request['transaction_id'])->first();
        $staffEmail = Staff::where('id', '=', $getStaff->performed_by)->first();
        $senderEmail = "support@xenren.co";
        $senderName = 'Xenren Confirmation Message';
        $receiverEmail = $staffEmail->email;
        Mail::send('mails.paymentApprovedEmailStaff', array('email' => $receiverEmail), function ($message) use ($senderEmail, $senderName, $receiverEmail) {
            $message->from($senderEmail, $senderName);
            $message->to($receiverEmail)
                ->subject('payment approved successfully');
        });
        return collect([
            'success' => true,
            'message' => 'payment approved',
        ]);
    }

    public function updateWithDrawRequest($data)
    {
        $file = $this->uploadFile($data['file']);
        $update = WithdrawRequest::where('id', '=', $data['withdrawRequest_id'])->update([
            'attachment' => $file,
            'remarks' => $data['remarks'],
            'status' => WithdrawRequest::STATUS_APPROVED,
            'amount' => $data['amount']
        ]);
        return $update;
    }

    public function uploadFile($file)
    {
        if ($file) {
            $fileName = $file->getClientOriginalName();
            $path = 'uploads/withdrawRequest';
            $save = $file->move($path, $fileName);
            return $fileName;
        }
        return false;
    }


    public function withdrawRequestReject(Request $request, $withdraw_id, $id)
    {
        $this->rejectPayment($withdraw_id, $request->remarks);
        $status = Transaction::where('id', '=', $id)->update([
            'status' => Transaction::STATUS_SHARED_OFFICE_OWNER_STATUS_WITHDRAW_REQUEST_REJECT
        ]);
        $getStaff = Transaction::where('id', '=', $id)->first();
        $staff = Staff::where('id', '=', $getStaff->performed_by)->first();
        Staff::where('id', '=', $getStaff->performed_by)->update([
            'balance' => $staff->balance + $getStaff->amount,
        ]);
        $staffEmail = Staff::where('id', '=', $getStaff->performed_by)->first();
        $senderEmail = "support@xenren.co";
        $senderName = 'Xenren Confirmation Message';
        $receiverEmail = $staffEmail->email;
        Mail::send('mails.paymentRejectedEmailStaff', array('email' => $receiverEmail), function ($message) use ($senderEmail, $senderName, $receiverEmail) {
            $message->from($senderEmail, $senderName);
            $message->to($receiverEmail)
                ->subject('payment approved successfully');
        });

        return collect([
            'success' => true,
            'message' => 'Withdraw Rejected Successfully',
        ]);
    }

    public function rejectPayment($withdraw_id, $remarks)
    {
        $withdraw = WithdrawRequest::where('id', '=', $withdraw_id)->update([
            'status' => WithdrawRequest::STATUS_REJECTED,
            'remarks' => $remarks,
        ]);
        return $withdraw;
    }

    public function getProducts($id)
    {
        $product = SharedOfficeProducts::where('id', '=', $id)->first();
        return collect([
            'office_id' => $product->office_id,
            'category_id' => $product->category_id,
            'number' => $product->number,
            'month_price' => $product->month_price,
            'hourly_price' => $product->hourly_price,
            'weekly_price' => $product->weekly_price,
            'daily_price' => $product->weekly_price,
        ]);
    }

    public function workingSheet()
    {
        $staffId = Auth::user()->id;
        $staff = Auth::user();
        // if owner
        if ($staff->permission_group_id == PermissionGroup::PERMISSION_GROUP_SHARED_OFFICE) {
            $staff = Staff::where('id', '=', $staffId)->with('office', 'office.officeProducts')->first();
            if (!is_null($staff->office)) {
                $product = $this->getOfficeProducts($staff->office->officeProducts);
                return view('backend.workingSheet.index', [
                    'products' => $product,
                    'office_id' => $staff->office->id
                ]);
            }
        // if staff
        } else {
            $staff = Staff::where('id', '=', $staffId)->with('officeStaff', 'officeStaff.officeProducts')->first();
            if (!is_null($staff->officeStaff)) {
                $product = $this->getOfficeProducts($staff->officeStaff->officeProducts);
                return view('backend.workingSheet.index', [
                    'products' => $product,
                    'office_id' => $staff->officeStaff->id
                ]);
            }
        }

        return view('backend.workingSheet.index');
    }

    public function getOfficeProducts($data)
    {
        $category = array();
        foreach ($data as $product) {
            array_push($category, array(
                'unique_id' => $product->id,
                'name' => $product['product_type_name'],
                'id' => $product->category_id,
                'month_price' => $product->month_price,
                'number' => $product->number
            ));
        }
        return $category;
    }

    public function updateProducts(Request $request)
    {
        $getData = SharedOfficeProducts::where('id', '=', $request->id)->where('office_id', '=', $request->office_id)->where('category_id', '=', $request->category_id)->first();
        SharedOfficeProducts::where('id', '=', $request->id)->where('office_id', '=', $request->office_id)->where('category_id', '=', $request->category_id)->update([
            'hourly_price' => isset($request->hourly_price) ? $request->hourly_price : $getData->hourly_price,
            'weekly_price' => isset($request->weekly_price) ? $request->weekly_price : $getData->weekly_price,
            'daily_price' => isset($request->daily_price) ? $request->daily_price : $getData->daily_price,
        ]);
        return collect([
            'success' => true,
            'message' => 'successfully updated'
        ]);
    }

    public function workingSheetDt(Request $request)
    {
        $max = $this->calculateMaximumEstimate($request->all());
        $min = $this->calculateMinimumEstimate($request->all());
        return collect([
            'maximum' => $max,
            'minimum' => $min
        ]);
    }

    /*maximum estimate*/
    public function calculateMaximumEstimate($data)
    {
        $seat = $data['seat'];
        $month_rate = $data['month_rate'];
        $weekly = $this->calculateMaxWeekEstimate($seat, $month_rate);
        $daily = $this->calculateMaxDailyEstimate($seat, $month_rate);
        $hourly = $this->calculateMaxHourlyEstimate($seat, $month_rate, $daily);
        return $data = [
            'weekly' => $weekly,
            'daily' => $daily,
            'hourly' => $hourly
        ];
    }

    public function calculateMaxWeekEstimate($seat, $month_rate)
    {
        /**
         * <=================== weekly ==================>
         * 1.5 is 50% Blank Ratio | Divide 4 weeks
         * then we calculate week rate_range monthPrice * 1.5 / 4
         * then we calculate maximum profit range monthPrice * 1.5
         */
        $weeklyRateRange = $month_rate * 1.5 / 4;
        $maxProfit = $month_rate * 1.5;
        return $data = [
            'weeklyRateRange' => number_format($weeklyRateRange, '2'),
            'maxProfit' => number_format($maxProfit, '2'),
        ];
    }

    public function calculateMaxDailyEstimate($seat, $month_rate)
    {
        /**
         * <=================== daily ==================>
         * 2 is 100% Blank Ratio | Divide into 24 working days
         * then we calculate daily rate_range monthPrice * 2 / 24
         * then we calculate maximum profit range monthPrice * 24
         */
        $dailyRateRange = $month_rate * 2 / 24;
        $maxProfit = $month_rate * 2;
        return $data = [
            'dailyRateRange' => number_format($dailyRateRange, '2'),
            'maxProfit' => number_format($maxProfit, '2'),
        ];
    }

    public function calculateMaxHourlyEstimate($seat, $month_rate, $daily)
    {
        /**
         * <=================== Hourly ==================>
         * Assume 24 days only rent for 5 hours per day
         * then we calculate Hourly dailyRateRange / 5
         * then we calculate maximum profit range is equal to dailyRateRange
         */
        $hourlyRateRange = $daily['dailyRateRange'] / 5;
        $maxProfit = ($hourlyRateRange * 8) * 24;
        return $data = [
            'hourlyRateRange' => number_format($hourlyRateRange, '2'),
            'maxProfit' => number_format($maxProfit, '2'),
        ];
    }

    /*minimum estimate*/
    public function calculateMinimumEstimate($data)
    {
        $seat = $data['seat'];
        $month_rate = $data['month_rate'];
        $weekly = $this->calculateMinWeekEstimate($seat, $month_rate);
        $daily = $this->calculateMinDailyEstimate($seat, $month_rate);
        $hourly = $this->calculateMinHourlyEstimate($seat, $month_rate, $daily);
        return $data = [
            'weekly' => $weekly,
            'daily' => $daily,
            'hourly' => $hourly
        ];
    }

    public function calculateMinWeekEstimate($seat, $month_rate)
    {
        /**
         * <=================== weekly ==================>
         * 1.25 is 25% Blank ratio | Divide 4 weeks
         * then we calculate week rate_range monthPrice * 1.25 / 4
         * then we calculate minimum profit range monthPrice * 1.25
         */
        $weeklyRateRange = $month_rate * 1.25 / 4;
        $maxProfit = $month_rate * 1.25;
        return $data = [
            'weeklyRateRange' => number_format($weeklyRateRange, '2'),
            'maxProfit' => number_format($maxProfit, '2'),
        ];
    }

    public function calculateMinDailyEstimate($seat, $month_rate)
    {
        /**
         * <=================== daily ==================>
         * 1.5 is 50% Blank Ratio | Divide into 24 working days
         * then we calculate daily rate_range monthPrice * 1.5 / 24
         * then we calculate minimum profit range monthPrice * 1.5
         */
        $dailyRateRange = $month_rate * 1.5 / 24;
        $maxProfit = $month_rate * 1.5;
        return $data = [
            'dailyRateRange' => number_format($dailyRateRange, '2'),
            'maxProfit' => number_format($maxProfit, '2'),
        ];
    }

    public function calculateMinHourlyEstimate($seat, $month_rate, $daily)
    {
        /**
         * <=================== Hourly ==================>
         * 8 is Assume 24 days only rent for 8 hours per day
         * then we calculate Hourly dailyRateRange / 8
         * then we calculate minimum profit range is equal to dailyRateRange
         */
        $hourlyRateRange = $daily['dailyRateRange'] / 8;
        $maxProfit = ($hourlyRateRange * 8) * 24;
        return $data = [
            'hourlyRateRange' => number_format($hourlyRateRange, '2'),
            'maxProfit' => number_format($maxProfit, '2'),
        ];
    }


    public function userDetails($user_id)
    {
        try {
            $user = User::with([
                'skills',
                'skills.skill',
                'sharedOfficeChatAction'
            ])
                ->where('id', '=', $user_id)
                ->first();
            return response()->json($user);
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }


    public function getMessagesByOfficeId($office_id)
    {
        try {
            $_sharedOfficeGroupChat = new SharedOfficeGroupChat();
            $result = $_sharedOfficeGroupChat->getMessagesByOfficeId($office_id);
            return collect([
                'status' => 'success',
                'data' => $result
            ]);
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function deletedMessage($id)
    {
        $deleted = SharedOfficeGroupChat::where('id', '=', $id)->delete();
        return collect([
            'success' => 'success',
            'date' => $deleted
        ]);
    }


    public function sendSharedofficeChatMessage(Request $request)
    {
        if ($request->hasFile('file')) {
            $fileName = $request->file('file')->getClientOriginalName();
            $path = 'uploads/sharedofficechat';
            $save = $request->file('file')->move($path, $fileName);
            SharedOfficeGroupChat::create([
                'office_id' => $request->office_id,
                'sender_staff_id' => $request->sender_staff_id,
                'sender_user_id' => null,
                'attachment' => SharedOfficeGroupChat::ATTACHMNET,
                'file' => $save
            ]);
            return collect([
                'success' => 'success',
            ]);

        } else {
            SharedOfficeGroupChat::create([
                'office_id' => $request->office_id,
                'sender_staff_id' => $request->sender_staff_id,
                'sender_user_id' => null,
                'message' => $request->message,
            ]);
            return collect([
                'success' => 'success',
            ]);
        }
    }

    public function adminAction($action, Request $request)
    {
        if ($action == SharedOfficeChatAction::BLOCKED_USER_IN_CHAT) {
            $userId = $request->user_id;
            $officeId = $request->office_id;
            $actions = $action;
            $check = SharedOfficeChatAction::where('user_id', '=', $userId)->where('action', '=', $action)->first();
            if (is_null($check)) {
                $data = SharedOfficeChatAction::create([
                    'user_id' => $userId,
                    'office_id' => $officeId,
                    'action' => $actions
                ]);
                return collect([
                    'success' => 'success',
                    'data' => $data
                ]);
            } else {
                $data = SharedOfficeChatAction::update([
                    'user_id' => $userId,
                    'office_id' => $officeId,
                    'action' => $actions
                ]);
                return collect([
                    'error' => 'error',
                    'data' => $data->action
                ]);
            }
        }
        if ($action == SharedOfficeChatAction::KICKED_USER_IN_CHAT) {
            $userId = $request->user_id;
            $officeId = $request->office_id;
            $actions = $action;
            $check = SharedOfficeChatAction::where('user_id', '=', $userId)->where('action', '=', $action)->first();
            if (is_null($check)) {
                $data = SharedOfficeChatAction::create([
                    'user_id' => $userId,
                    'office_id' => $officeId,
                    'action' => $actions
                ]);
                return collect([
                    'success' => 'success',
                    'data' => $data
                ]);
            } else {
                $data = SharedOfficeChatAction::update([
                    'user_id' => $userId,
                    'office_id' => $officeId,
                    'action' => $actions
                ]);
                return collect([
                    'error' => 'error',
                    'data' => $data->action
                ]);
            }
        }
    }

    public function chatAction()
    {
        $get = SharedOfficeChatAction::all();
        return collect([
            'data' => $get
        ]);
    }

    public function getStaffMessages($staff_id)
    {
        $getStaffMessages = SharedOfficeGroupChat::where('sender_staff_id', '=', $staff_id)->get();
        return collect([
            'messages' => $getStaffMessages
        ]);
    }


    public function getUsers($office_id)
    {
        try {
            $data = SharedOfficeFinance::select('shared_office_finances.*', 'soc.action', 'u.img_avatar', 'u.name', 'u.last_login_at')
                ->leftJoin('shared_office_chat_actions as soc', 'soc.user_id', '=', 'shared_office_finances.user_id')
                ->join('users as u', 'u.id', '=', 'shared_office_finances.user_id')
                ->where('shared_office_finances.office_id', '=', $office_id)
//                ->whereNull('end_time')
                // ->with([
                //     'user.sharedOfficeChatAction',
                //     'user.skills.skill',
                //     'userStatus'
                // ])
                ->whereNotNull('shared_office_finances.user_id')
                ->get();
            return collect([
                'status' => 'success',
                'data' => $data->unique('user_id')->values()
            ]);
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function sharedOfficeProductsCheckUsers(Request $request)
    {
        $userName = $request->username;
        $startTime = Carbon::parse($request->start_time);
        $endTime = Carbon::parse($request->end_time);
        $seatNo = $request->seat_no;
        $officeId = $request->office_id;

        /*
         * Validations
         * */
        if ($endTime->lessThan($startTime)) {
            return collect([
                'status' => 'failure',
                'message' => 'End time can\'t be greater then start time',
                'q_p' => $request->all()
            ]);
        }

        $user = User::where('name', '=', $userName)->first();
        if (is_null($user)) {
            return collect([
                'status' => 'failure',
                'message' => 'User not found.',
                'q_p' => $request->all()
            ]);
        }

        $seatFinance = SharedOfficeFinance::where('product_id', '=', $seatNo)->where('office_id', '=', $officeId)->where('end_time', '=', null)->first();

        if (!is_null($seatFinance)) {
            return collect([
                'status' => 'failure',
                'message' => 'Seat in use.',
                'q_p' => $request->all()
            ]);
        }
        $seat = SharedOfficeProducts::where('office_id', '=', $officeId)->where('number', '=', $seatNo)->first();
        if (is_null($seat)) {
            return collect([
                'status' => 'failure',
                'message' => 'Seat invalid.',
                'q_p' => $request->all()
            ]);
        }
        /*
        * Validations
        * */

        return collect([
            'status' => 'success'
        ]);
    }

    public function sharedOfficeProductsAddUsers(Request $request)
    {
        try {
            \DB::beginTransaction();
            foreach ($request->all() as $k => $v) {
                $userName = $v['name'];
                $startTime = Carbon::parse($v['start_time']);
                $endTime = Carbon::parse($v['end_time']);
                $seatNo = $v['number'];
                $officeId = $v['office_id'];

                /*
                 * Validations
                 * */
                if ($endTime->lessThan($startTime)) {
                    return collect([
                        'status' => 'failure',
                        'message' => 'End time can\'t be greater then start time'
                    ]);
                }

                $user = User::where('name', '=', $userName)->first();
                if (is_null($user)) {
                    return collect([
                        'status' => 'failure',
                        'message' => 'User not found.'
                    ]);
                }

                $seatFinance = SharedOfficeFinance::where('product_id', '=', $seatNo)->where('office_id', '=', $officeId)->where('end_time', '=', null)->first();

                if (!is_null($seatFinance)) {
                    return collect([
                        'status' => 'failure',
                        'message' => 'Seat in use.'
                    ]);
                }
                $seat = SharedOfficeProducts::where('office_id', '=', $officeId)->where('number', '=', $seatNo)->first();
                if (is_null($seat)) {
                    return collect([
                        'status' => 'failure',
                        'message' => 'Seat invalid.'
                    ]);
                }
                /*
                * Validations
                * */

                $finance = SharedOfficeFinance::create([
                    'product_id' => $seatNo,
                    'office_id' => $officeId,
                    'start_time' => $startTime,
                    'expected_end_time' => $endTime,
                    'user_id' => $user->id,
                    'x' => $seat->x,
                    'y' => $seat->y,
                    'type' => SharedOfficeFinance::TYPE_NORMAL,
                    'user' => $user->name
                ]);
            }
            \DB::commit();
            return collect([
                'status' => 'success',
                'message' => 'Seat started.',
            ]);
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage(),
            ]);
        }
    }

    public function sharedOfficeProductsUsersDT($id, Request $request)
    {
        $data = SharedOfficeFinance::select([
            'id', 'office_id', 'product_id', 'user_id', 'cost', 'start_time', 'end_time'
        ])->with([
            'user' => function ($q) {
                $q->select([
                    'id', 'name', 'email'
                ]);
            },
            'product'
        ])
            ->whereHas('user')->whereHas('product')->where('office_id', '=', $id)->get();
        return collect([
            'status' => 'success',
            'data' => $data
        ]);
    }

    public function sharedOfficeBookingsDT($id, Request $request)
    {
        $sharedOffice = SharedOfficeBooking::where('office_id', '=', $id)->where('status', '=', SharedOfficeBooking::STATUS_REQUEST)->get();
        return collect([
            'status' => 'success',
            'data' => $sharedOffice
        ]);
    }

    public function sharedOfficeBookingsCancel(Request $request)
    {
        SharedOfficeBooking::where('id', '=', $request->booking_id)->update([
           'remarks' => $request->remarks,
           'status' => SharedOfficeBooking::STATUS_CANCELED
        ]);
        SharedOfficeBooking::where('id', '=', $request->booking_id)->delete();
        return collect([
            'status' => 'success',
            'message' => 'Booking canceled...!'
        ]);
    }

    public function sharedOfficeAvailableSeatsDT($bookingId, Request $request)
    {
        $booking = SharedOfficeBooking::where('id', '=', $bookingId)->first();
        $availableSeats = SharedOfficeProducts::where('status_id', '=', SharedOfficeProducts::STATUS_EMPTY)
            ->where('category_id', '=', $booking->category_id)
            ->where('office_id', '=', $booking->office_id)
            ->get();

        return collect([
            'status' => 'success',
            'message' => $booking,
            'data' => $availableSeats
        ]);
    }

    public function sharedOfficeBookSeat(Request $request)
    {
        SharedOfficeProducts::where('id', '=', $request->seat_id)->update([
            'status_id' => SharedOfficeProducts::STATUS_BOOKED
        ]);
        SharedOfficeBooking::where('id', '=', $request->booking_id)->update([
            'status' => SharedOfficeBooking::STATUS_BOOKED
        ]);
        $staff = \Auth::guard('staff')->user();
        OwnerProgress::setProgress($staff,'manager_confirm_bookings');
        return collect([
            'status' => 'success',
            'success' => true,
            'data' => $request->all()
        ]);
    }

    public function sharedOfficeProductsDT($id, Request $request)
    {
        // \DB::enableQueryLog();
        // $arr = array();


        $requestDate = \Carbon\Carbon::parse($request->date)->startOfDay()->toDateTimeString();
        $start = new DateTime($requestDate, new DateTimeZone("UTC"));
        $month_before = clone $start;
        $month_before->sub(new DateInterval("P1M"));
        // $month_before = \Carbon\Carbon::parse($month_before)->startOfDay()->toDateTimeString();
// dd($month_before);
        $requestDate1 = \Carbon\Carbon::parse($request->date)->endOfDay()->toDateTimeString();

        $data = \App\Models\SharedOffice::select([
            'id',
            'office_name'
        ])
            ->with([
                'officeProducts',
                'officeProducts.finance' => function ($query) use ($id, $requestDate, $requestDate1, $month_before) {
                    $query->where('office_id', $id);
                    $query->where(function ($q) use ($requestDate, $requestDate1, $month_before) {
                        $q->where('start_time', '>=', $requestDate);
                        $q->Where('end_time', '<=', $requestDate1);
                    })
                        ->orWhere(function ($qu) use ($requestDate, $requestDate1, $month_before) {
                            $qu->where('start_time', '>=', $requestDate);
                            $qu->where('start_time', '<=', $requestDate1);
                            $qu->where('end_time', null);
                        })
                        ->orWhere(function ($qu) use ($requestDate, $requestDate1, $month_before) {
                            $qu->where('start_time', '<=', $requestDate);
                            // $qu->where('start_time', '>=', $month_before);
                            $qu->where('end_time', null);
                        });
                }
            ])
            ->where('id', '=', $id)
            ->first();
// dd(DB::getQueryLog());
// dd($data);
        foreach ($data->officeProducts as $k => $v) {
            // $d =  \App\Models\SharedOfficeFinance::where('product_id', '=', $v->number)
            //     ->where('office_id', '=', $v->office_id)
            //     ->where(function($q)use($requestDate, $requestDate1){
            //         $q->where('start_time', '>=', $requestDate);
            //         $q->Where('end_time', '>=', $requestDate);
            //     })
            //     ->orWhere(function($qu)use($requestDate, $requestDate1){
            //         $qu->where('start_time', '<=', $requestDate);
            //         $qu->where('end_time',null);
            //     })
            //     ->get();
            //     // dd($d);
            // $v->fin = $d;


            // foreach ($v->finance as $k1 => $v1) {
            //     $shd = \Carbon\Carbon::parse($v1->start_time)->startOfDay();
            //     $sh = \Carbon\Carbon::parse($v1->start_time)->format('H');
            //     $ehd = \Carbon\Carbon::parse($v1->end_time)->startOfDay();
            //     $eh = is_null($v1->end_time) ? \Carbon\Carbon::parse($v1->expected_end_time)->format('H') : \Carbon\Carbon::parse($v1->end_time)->format('H');
            //     $seat_num = $v1->product_id;
            //     if (\Carbon\Carbon::parse($v1->end_time)->format('i') > 0) {
            //         $eh += 1;
            //     }

            //     array_push($arr, array(
            //         'seat_number' => $seat_num,
            //         'start_hour' => $sh,
            //         'end_hour' => $eh,
            //         'date_jumped' => $shd->diffInSeconds($ehd) == 0 ? false : true
            //     ));
            // }

            // $v->usedhours = $arr;

            $arr = collect();
            $usedHoursArray = array();
            if ($v->status_id == SharedOfficeProducts::STATUS_BOOKED) {
                for ($i = 1; $i < 25; $i++) {
                    $exist = null;
                    $checkBookings = SharedOfficeBooking::where('office_id','=',$v['office_id'])->first();
                    if(isset($checkBookings) && $checkBookings->status == SharedOfficeBooking::STATUS_BOOKED) {
                        $datetime1 = new \DateTime($checkBookings->check_in);
                        $datetime2 = new \DateTime($checkBookings->check_out);
                        $interval = $datetime1->diff($datetime2);
                        $days = $interval->format('%a');
                        $dayz = $days * 24;
                        if($dayz) {
                            if ($start <= $datetime1) {
                                if($start != $datetime1) {
                                    $arr->put('slot_' . $i, 0);
                                } else {
                                    $arr->put('slot_' . $i, 2);
                                }
                            } elseif ($start <= $datetime2) {
                                $arr->put('slot_' . $i, 2);
                            } else {
                                $arr->put('slot_' . $i, 0);
                            }
                        }
                    } else {
                        $arr->put('slot_' . $i, 0);
                    }
                }
            } else {
                if (count($v->finance) > 0) {

                    for ($i = 1; $i < 25; $i++) {
                        $exist = null;

                        foreach ($v->finance as $k1 => $v1) {
                            $shd = \Carbon\Carbon::parse($v1->start_time)->startOfDay();
                            $sh = \Carbon\Carbon::parse($v1->start_time)->format('H');
                            $ehd = \Carbon\Carbon::parse($v1->end_time)->startOfDay();
                            $eh = is_null($v1->end_time) ? \Carbon\Carbon::parse($v1->expected_end_time)->format('H') : \Carbon\Carbon::parse($v1->end_time)->format('H');
                            $seat_num = $v1->product_id;
                            if (\Carbon\Carbon::parse($v1->end_time)->format('i') > 0) {
                                $eh += 1;
                            }

                            if ($seat_num == $v->id) {
                                if ($shd->diffInSeconds($ehd) == 0 ? false : true) {
                                    if ($i <= $eh) {
                                        $exist = true;
                                    }
                                } else if ($i >= $sh && $i <= $eh) {
                                    $exist = true;
                                }
                            }

                            array_push($usedHoursArray, array(
                                'seat_number' => $seat_num,
                                'start_hour' => $sh,
                                'end_hour' => $eh,
                                'date_jumped' => $shd->diffInSeconds($ehd) == 0 ? false : true
                            ));
                        }

                        $v->usedhours = $usedHoursArray;

                        if (!is_null($exist)) {
                            $arr->put('slot_' . $i, 1);
                        } else {
                            $arr->put('slot_' . $i, 0);
                        }
                    }
                } else {
                    for ($i = 1; $i < 25; $i++) {
                        $arr->put('slot_' . $i, 0);
                    }
                }
            }

            $v->timeSlots = $arr;
        }


        // dd($arr);
        // foreach ($data->officeProducts as $k => $v) {
        //     if ($v->status_id == SharedOfficeProducts::STATUS_BOOKED) {
        //         $arr = collect();
        //         for ($i = 1; $i < 25; $i++) {
        //             $exist = null;
        //             $checkBookings = SharedOfficeBooking::where('office_id','=',$v['office_id'])->first();
        //             if(isset($checkBookings) && $checkBookings->status == SharedOfficeBooking::STATUS_BOOKED) {
        //                 $datetime1 = new \DateTime($checkBookings->check_in);
        //                 $datetime2 = new \DateTime($checkBookings->check_out);
        //                 $interval = $datetime1->diff($datetime2);
        //                 $days = $interval->format('%a');
        //                 $dayz = $days * 24;
        //                 if($dayz) {
        //                     if ($start <= $datetime1) {
        //                         if($start != $datetime1) {
        //                             $arr->put('slot_' . $i, 0);
        //                         } else {
        //                             $arr->put('slot_' . $i, 2);
        //                         }
        //                     } elseif ($start <= $datetime2) {
        //                         $arr->put('slot_' . $i, 2);
        //                     } else {
        //                         $arr->put('slot_' . $i, 0);
        //                     }
        //                 }
        //             } else {
        //                 $arr->put('slot_' . $i, 0);
        //             }
        //         }
        //         $v->timeSlots = $arr;
        //     } else {
        //         $arr = collect();
        //         if (count($v->finance) > 0) {

        //             for ($i = 1; $i < 25; $i++) {
        //                 $exist = null;
        //                 foreach ($v->usedhours as $k1 => $v1) {
        //                     if ($v1['seat_number'] == $v->id) {
        //                         if ($v1['date_jumped']) {
        //                             if ($i <= $v1['end_hour']) {
        //                                 $exist = true;
        //                             }
        //                         } else if ($i >= $v1['start_hour'] && $i <= $v1['end_hour']) {
        //                             $exist = true;
        //                         }
        //                     }
        //                 }
        //                 if (!is_null($exist)) {
        //                     $arr->put('slot_' . $i, 1);
        //                 } else {
        //                     $arr->put('slot_' . $i, 0);
        //                 }
        //             }
        //         } else {
        //             for ($i = 1; $i < 25; $i++) {
        //                 $arr->put('slot_' . $i, 0);
        //             }
        //         }
        //         $v->timeSlots = $arr;

        //     }
        // }

        return collect([
            'date' => \Carbon\Carbon::parse($request->date),
            'data' => $data,
            'type' => collect([
                '0' => 'free',
                '1' => 'used'
            ]),
            // 'query' => \DB::getQueryLog()
        ]);
    }


    public function sharedOfficeFinancesDT($id, Request $request)
    {
        // \DB::enableQueryLog();
        $arr = array();

        $startDate = \Carbon\Carbon::parse($request->startdate)->startOfDay()->toDateTimeString();
        $endDate = \Carbon\Carbon::parse($request->enddate)->endOfDay()->toDateTimeString();

        $data = \App\Models\SharedOffice::select([
            'id',
            'office_name'
        ])
            ->with([
                'officeProducts',
                'officeProducts.finance' => function ($query) use ($id, $startDate, $endDate) {
                    $query->where('office_id', $id);
                    $query->where(function ($q) use ($startDate, $endDate) {
                        $q->where('start_time', '>=', $startDate);
                        $q->Where('end_time', '<=', $endDate);
                    })
                        ->orWhere(function ($qu) use ($startDate, $endDate) {
                            $qu->where('start_time', '<=', $endDate);
                            $qu->where('start_time', '>=', $startDate);
                            $qu->where('end_time', null);
                        })
                        ->orWhere(function ($qu) use ($startDate, $endDate) {
                            $qu->where('start_time', '<=', $startDate);
                            // $qu->where('start_time', '>=', $month_before);
                            $qu->where('end_time', null);
                        });
                },
                'officeProducts.finance.user'
            ])
            ->where('id', '=', $id)
            ->first();
// dd(DB::getQueryLog());
// return $data->officeProducts;
        $arr = array();
        foreach ($data->officeProducts as $k => $v) {
            if (count($v->finance) > 0) {
                foreach ($v->finance as $k1 => $v1) {

                    // if(!is_null($v1->cost) && ){
                    //     $financeData = $v1->toArray();
                    //     array_push($arr, array(
                    //         'name' => $financeData['user']['name'],
                    //         'cost' => $financeData['cost'],
                    //         'endDate' => Carbon::parse($financeData['end_time'])->toFormattedDateString()
                    //     ));
                    //     // return $arr;
                    // }else{
                    $financeData = $v1->toArray();
                    $financeData['end_time'] = now()->toDateTimeString();
                    $calculateStartDate = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $financeData['start_time']);
                    $calculateEndDate = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $financeData['end_time']);
                    if ($financeData['start_time'] < $startDate && $financeData['end_time'] >= $endDate) {
                        $financeData['start_time'] = \Carbon\Carbon::parse($startDate)->startOfDay();
                        $financeData['end_time'] = \Carbon\Carbon::parse($endDate)->endOfDay();
                        $calculateStartDate = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $financeData['start_time']);
                        $calculateEndDate = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $financeData['end_time']);
                    } elseif ($financeData['start_time'] < $startDate && $financeData['end_time'] < $endDate) {
                        $financeData['start_time'] = \Carbon\Carbon::parse($startDate)->startOfDay();
                        $financeData['end_time'] = \Carbon\Carbon::parse($financeData['end_time'])->endOfDay();
                        $calculateStartDate = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $financeData['start_time']);
                        $calculateEndDate = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $financeData['end_time']);
                    } elseif ($financeData['start_time'] >= $startDate && $financeData['end_time'] > $endDate) {
                        $financeData['start_time'] = \Carbon\Carbon::parse($financeData['start_time'])->startOfDay();
                        $financeData['end_time'] = \Carbon\Carbon::parse($endDate)->startOfDay();
                        $calculateStartDate = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $financeData['start_time']);
                        $calculateEndDate = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $endDate);
                    }

                    $difference = $calculateStartDate->diffInMinutes($calculateEndDate) / 10;
                    array_push($arr, array(
                        'name' => $financeData['user']['name'],
                        'cost' => $financeData['cost'],
                        'endDate' => Carbon::parse($calculateEndDate)->toFormattedDateString(),
                        'time' => $financeData['updated_at'],
                        'office_id' => $financeData['office_id'],
                        'id' => $financeData['id'],
                    ));
                    // }
                }
            }
        }
        if(isset($difference)) {
            $financeData['cost'] = $difference * $v->time_price;
        }
        $transaction = Transaction::with(['staff.office'])
            ->where('type', '=', Transaction::TYPE_SERVICE_FEE)
            ->get();
        $data = array();
        foreach ($transaction as $trans) {
            if (Carbon::parse($trans->created_at)->toFormattedDateString() == Carbon::now()->toFormattedDateString()) {
                array_push($data, array(
                    'name' => $trans->staff->office->office_name,
                    'cost' => $trans->amount,
                    'endDate' => Carbon::parse($trans->created_at)->toFormattedDateString(),
                    'time' => $trans->created_at,
                    'office_id' => $trans->staff->office->id,
                    'id' => $financeData['id'],
                ));
            }
        }
        $arrData = collect(array_merge($arr, $data))->sortBy('time');
        $totalAmount = 0;
        $totalDeduct = 0;

        for ($i = 0; $i < sizeof($data); $i++) {
            if (isset($data[$i])) {
                $totalDeduct += $data[$i]['cost'];
            }
        }

        for ($i = 0; $i < sizeof($arr); $i++) {
            if (isset($arr[$i])) {
                $totalAmount += $arr[$i]['cost'];
            }
        }
        return collect([
            'start_date' => \Carbon\Carbon::parse($request->startdate),
            'end_date' => \Carbon\Carbon::parse($request->enddate),
            'data' => $arrData,
            'totalAmount' => $totalAmount,
            'totalDeduct' => $totalDeduct,
            // 'query' => \DB::getQueryLog()
        ]);


        // \DB::enableQueryLog();
        // $cost=1;
        // $data = \App\Models\SharedOffice::select([
        //     'id',
        //     'office_name'
        // ])
        //     ->with([
        //         'officeProducts'
        //     ])
        //     ->where('id', '=', $id)
        //     ->first();
        // $startDate = \Carbon\Carbon::parse($request->startdate)->toDateTimeString();
        // $endDate = \Carbon\Carbon::parse($request->enddate)->endOfDay()->toDateTimeString();

        // $arr = array();
        // foreach ($data->officeProducts as $k => $v) {

        //         $d =  \App\Models\SharedOfficeFinance::select([
        //             'user_id',
        //             'office_id',
        //             'start_time',
        //             'end_time',
        //             'status_finance',
        //             'office_id',
        //             'product_id'
        //         ])
        //         ->with([
        //             'user',
        //             'product'
        //         ])
        //         ->whereHas('user')
        //         ->whereHas('product')
        //         ->where('product_id', $v->number)
        //             ->where('office_id', '=', $v->office_id)
        //             ->where(function($qu) use ($startDate,$endDate){
        //                 $qu->Where('start_time', '<=', $endDate)->Where('end_time', '>=' ,$startDate);
        //             })
        //             ->orWhere(function($que) use ($startDate,$endDate){
        //                 $que->Where('start_time','<=',  $endDate);
        //                 $que->Where('end_time',  null);
        //             })

        //             ->get();

        //     foreach ($d as $k1 => $v1) {

        //         if(is_null($v1->end_time)){
        //             $v1->end_time = now();
        //         }

        //         $db_sDateTime = \Carbon\Carbon::parse($v1->start_time)->toDatetimeString();
        //         $db_eDateTime = \Carbon\Carbon::parse($v1->end_time)->toDatetimeString();

        //         $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $db_sDateTime);
        //         $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $db_eDateTime);

        //         if($db_sDateTime < $startDate){
        //             if($db_eDateTime > $endDate){
        //                 $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $startDate);
        //                 $difference = $to->diffInMinutes($endDate);
        //                 $from = Carbon::parse($endDate)->toFormattedDateString();
        //             }
        //             if($db_eDateTime < $endDate){
        //                 $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $startDate);
        //                 $difference = $to->diffInMinutes($db_eDateTime);
        //                 $from = Carbon::parse($db_eDateTime)->toFormattedDateString();
        //             }
        //         }elseif($db_eDateTime >= $endDate && $db_sDateTime > $startDate){
        //             $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $db_sDateTime);
        //             $difference = $to->diffInMinutes($endDate);
        //             $from = Carbon::parse($endDate)->toFormattedDateString();
        //         }else{
        //             $difference = $to->diffInMinutes($from);
        //             $from = $from->toFormattedDateString();
        //         }

        //         $difference = $difference/10;
        //         $cost = $difference*$v1->product['time_price'];

        //         array_push($arr, array(
        //             'officeId' => $v1->office_id,
        //             'statusFinance' => $v1->status_finance,
        //             'cost' => $cost,
        //             'user'=>$v1->user['name'],
        //             'endDate' => $from
        //         ));
        //     }

        // }

        // $totalAmount = 0;

        // $arr = array_unique($arr,SORT_REGULAR);

        // for($i=0;$i<sizeof($arr);$i++){
        //     if(isset($arr[$i])){
        //         $totalAmount += $arr[$i]['cost'];
        //     }
        // }

        // return collect([
        //     'start_date' => \Carbon\Carbon::parse($request->startdate),
        //     'end_date' => \Carbon\Carbon::parse($request->enddate),
        //     'data' => $arr,
        //     'totalAmount' => $totalAmount,
        //     'query' => \DB::getQueryLog()
        // ]);


    }

    public function newdesign2()
    {
        return view("backend.sharedoffice.newDesign2");
    }

    public function income($id)
    {
        return view('backend.sharedoffice.newDesignIncome', compact('id'));
    }

    public function updateSharedOfficebit(Request $request)
    {
        if ($request->val === 'barcode') {
            $update = SharedOffice::where('id', '=', $request->office_id)->update([
                'qr' => $request->barcode,
            ]);
            $sharedOfficeBid = SharedOfficeBarcode::where('bid', '=', SharedOfficeBarcode::SHARED_OFFICE_BARCODE_BID)->update([
                'bid' => SharedOfficeBarcode::SHARED_OFFICE_BARCODE_BID_ZERO
            ]);
            $sharedofficeBarcode = SharedOfficeBarcode::where('shared_office_id', '=', $request->office_id)->where('type', '=', $request->type)->update([
                'bid' => SharedOfficeBarcode::SHARED_OFFICE_BARCODE_BID
            ]);
            return collect([
                'success' => 'success',
                'data' => $update,
                'bid' => $sharedofficeBarcode
            ]);
        }
        if ($request->val === 'active') {
            $check = SharedOfficeBarcode::where('shared_office_id', '=', $request->office_id)->where('type', '=', $request->type)->where('active', '=', $request->active)->first();
            if (is_null($check)) {
                $update = SharedOfficeBarcode::where('shared_office_id', '=', $request->office_id)->where('type', '=', $request->type)->update([
                    'active' => SharedOfficeBarcode::SHARED_OFFICE_BARCODE_ACTIVE
                ]);
            } else if ($check->active == SharedOfficeBarcode::SHARED_OFFICE_BARCODE_ACTIVE) {
                $update = SharedOfficeBarcode::where('shared_office_id', '=', $request->office_id)->where('type', '=', $request->type)->update([
                    'active' => SharedOfficeBarcode::SHARED_OFFICE_BARCODE_INACTIVE
                ]);
            }
            return collect([
                'success' => 'success',
                'data' => $update
            ]);
        }
    }

    public function getSharedOfficeActiveBarCodes()
    {
        return SharedOfficeBarcode::getBarCodes();
    }

    public function getSharedOffices(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get('start');
        $length = $request->get('length');
        $search = (isset($request->search['value'])) ? $request->search['value'] : '';
        $staff = Auth::guard('staff')->user();
        $staff_id = $staff->staff_type == Staff::STAFF_TYPE_ADMIN ? '' : $staff->id;
        if ($staff->staff_type == Staff::STAFF_TYPE_ADMIN) {
            $total_members = SharedOffice::count(); // get your total no of data;
            $record = DB::table('shared_offices')->select([
                'id',
                'office_name',
                'staff_id',
                'location',
                'description',
                'office_size_x as size_x',
                'office_size_y as size_y',
                'number',
                'description',
                'description',
            ])
                ->selectRaw("Concat('', '
                <center>
                <img class=\"img-thumbnail news-thumb\" src=\"', shared_offices.qr, '\">
                <h5>
                    <a href=\"/admin/sharedoffice/barcodes?office=',shared_offices.id,'\" style=\"width:78%;color:#fff;background:#25ce0f;\" class=\"btn btn-sm btn-success\" >Change</a>
                </h5>
                </center>
                ') as qr")
                ->selectRaw("Concat('', '<img class=\"img-thumbnail news-thumb set-news-thumb\" src=\"/', shared_offices.image, '\">') as image")
                ->selectRaw(
                    "Concat('', '<center style=\"height: 40px\">
                                <a href=\"/admin/sharedoffice/products/index/', shared_offices.id, '\" class=\"btn btn-xs primary uppercase blue\" style=\"background-color: white; border-color: white\" data-id=\"', shared_offices.id, '\"> <img src=\"/images/icons/chair.png\"> </a>
                                <div style=\"color: #cccccc;font-size:17pt;font-weight: 300;height: 30px;display:inline-block;vertical-align: middle\">|</div>
                                <a href=\"/admin/sharedoffice/edit/', shared_offices.id, '\" class=\"btn btn-xs primary uppercase blue\"  style=\"background-color: white; border-color: white; color: #7a7a7a\" data-id=\"', shared_offices.id, '\"> <i class=\"fa fa-pencil\"></i> </a>
                                <div style=\"color: #cccccc;font-size:17pt;font-weight: 300;height: 30px;display:inline-block;vertical-align: middle\">|</div>
                                <a href=\"/admin/sharedoffice/delete/', shared_offices.id, '\" class=\"btn btn-xs primary uppercase blue\" style=\"background-color: white; border-color: white; color: #7a7a7a\" data-id=\"', shared_offices.id, '\"> <i class=\"fa fa-trash\"></i> </a>
                                </center>
                                <center style=\"height: 40px\">
                                <a href=\"/admin/sharedoffice/addimages/', shared_offices.id, '\" class=\"btn btn-xs primary uppercase blue\"   style=\"background-color: white; border-color: white; color: #7a7a7a\">
                                    <div style=\"background-color: #e0e0df; padding: 5px; font-weight: 700; border-radius: 3px;font-size: 10pt;\">
                                        Add More Images</div>
                                </a>
                                </center>
                                <center>
                                <a href=\"/admin/sharedoffice/dashboard/',shared_offices.id,'\" class=\"uppercase\" style=\"color:green\">
                                    <center style=\"text-decoration:underline;color:#25ce0f;font-weight:700; font-size:10pt\">
                                        Dashboard
                                    </center >
                                </a >
                                <a href=\"/admin/sharedoffice/editmoreimagesIndex/',shared_offices.id,'\" class=\"uppercase\" style=\"color:green\">
                                    <center style=\"text-decoration: underline; color: #25ce0f; font-weight:700; font-size:10pt\">
                                        Total images</center>
                                </a>
                                <a href=\"/admin/sharedoffice/',shared_offices.id,'/chat\">
                                    <i class=\"fa fa-comments-o\" style=\"position: relative;float:right; left:0px;font-size:25px;top:-28px;color:#25ce0f\" aria-hidden=\"true\"></i>
                                </a>
                                </center>
                                ') AS action")
                ->where('id', '>=', $start)
                ->where('office_name', 'like', '%' . $search . '%')
                ->where('staff_id', 'like', '%' . $staff_id . '%')
                ->where('deleted_at', '=', null)
                ->limit($length)
                ->get();
        } else {
            $record = DB::table('shared_offices')->select([
                'id',
                'office_name',
                'staff_id',
                'location',
                'description',
                'office_size_x as size_x',
                'office_size_y as size_y',
                'number',
                'description',
                'description',
            ])
                ->selectRaw("Concat('', '
                <center>
                <img class=\"img-thumbnail news-thumb\" src=\"', shared_offices.qr, '\">
                <h5>
                    <a href=\"/admin/sharedoffice/barcodes?office=',shared_offices.id,'\" style=\"width:78%;color:#fff;background:#25ce0f;\" class=\"btn btn-sm btn-success\" >Change</a>
                </h5>
                </center>
                ') as qr")
                ->selectRaw("Concat('', '<img class=\"img-thumbnail news-thumb set-news-thumb\" src=\"/', shared_offices.image, '\">') as image")
                ->selectRaw(
                    "Concat('', '<center style=\"height: 40px\">
                                <a href=\"/admin/sharedoffice/delete/', shared_offices.id, '\" class=\"btn btn-xs primary uppercase blue\" style=\"background-color: white; border-color: white; color: #7a7a7a\" data-id=\"', shared_offices.id, '\"> <i class=\"fa fa-trash\"></i> </a>
                                </center>
                                <center>
                                <a href=\"/admin/sharedoffice/dashboard/',shared_offices.id,'\" class=\"uppercase\" style=\"color:green\">
                                    <center style=\"text-decoration:underline;color:#25ce0f;font-weight:700; font-size:10pt\">
                                        Dashboard
                                    </center >
                                </a >
                                <a href=\"/admin/sharedoffice/editmoreimagesIndex/',shared_offices.id,'\" class=\"uppercase\" style=\"color:green\">
                                    <center style=\"text-decoration: underline; color: #25ce0f; font-weight:700; font-size:10pt\">
                                        Total images</center>
                                </a>
                                <a href=\"/admin/sharedoffice/',shared_offices.id,'/chat\">
                                    <i class=\"fa fa-comments-o\" style=\"position: relative;float:unset; left:0px;font-size:25px;top:28px;color:#25ce0f\" aria-hidden=\"true\"></i>
                                </a>
                                </center>
                                ') AS action");

            $sharedoffice = SharedOffice::where('staff_id', $staff->id)->first();
            // if owner
            if ($staff->permission_group_id == PermissionGroup::PERMISSION_GROUP_SHARED_OFFICE) {
                $total_members = 0;
                $record = $record->where('staff_id', '=', $staff_id);
                if (!is_null($sharedoffice)) {
                    $total_members = 1 + Staff::where('office_id', $sharedoffice->id)->count();
                }

            // if staff
            } else {
                $record->where('id', '=', $sharedoffice->id);
                $total_members = 1 + Staff::where('office_id', $sharedoffice->id)->count();
            }

            $record = $record->where('deleted_at', '=', null)->get();
        }

        $record = collect($record);
        foreach ($record as $k => $v){
            $d = SharedOfficeSetting::where('staff_id', '=', $v->staff_id)->first();
            if(is_null($d)){
                $discount =  0;
            } else {
                $discount = $d->discount;
            }
            $record[$k]->discount = $discount.'%';
        }

        $id = $draw <= 1 ? 0 : $draw * $length;
        $data = array(
            'draw' => $draw,
            'recordsTotal' => $total_members,
            'recordsFiltered' => $total_members,
            'data' => $record,
        );

        return json_encode($data);

        if (Auth::guard('staff')->user()->staff_type == Staff::STAFF_TYPE_ADMIN) {
            $sharedoffice = SharedOffice::select([
                'id',
                'image',
                'office_name',
                'location',
                'description',
                'office_size_x as size_x',
                'office_size_y as size_y',
                'number',
                'qr',
                'description',
                'description',
            ])->paginate(30);
        } else {
            $sharedoffice = SharedOffice::where('staff_id', '=', Auth::guard('staff')->user()->id)->select([
                'id',
                'image',
                'office_name',
                'location',
                'description',
                'number',
                'qr',
                'description',
                'description',
            ])->paginate(30);
        }
        return collect($sharedoffice);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $detect = new Mobile_Detect();
        $userId = $request->u_id;
        $token = $request->token;
        $generateSharedOffice = true;
        $sharedOffice = null;
        if (Auth::guard('staff')->user()->staff_type == Staff::STAFF_TYPE_STAFF) {
            $staff = Auth::guard('staff')->user();
            // if staff
            if ($staff->permission_group_id != PermissionGroup::PERMISSION_GROUP_SHARED_OFFICE) {
                $sharedOffice = SharedOffice::where('staff_id', '=', $staff->id)->first();
                if ( !is_null($staff->id) ) {
                    $generateSharedOffice = false;
                }
            // if owner
            } else {
                $sharedOffice = SharedOffice::where('staff_id', '=', $staff->id)->first();
                $count = SharedOffice::where('staff_id', '=', $staff->id)->count();
                if ($count >= 1) {
                    $generateSharedOffice = false;
                }
            }
        }
        return view('backend.sharedoffice.index', [
            'generateSharedOffice' => $generateSharedOffice,
            'token' => $token,
            'is_mobile' => $detect->isMobile(),
            'sharedOffice' => $sharedOffice
        ]);
    }

    public function getSharedOfficeRequest(Request $request)
    {
        $show_marked = $request->show_markerd == 'true' ? $request->show_markerd : false;
        if ($show_marked == 'true') {
            $sharedOfficeRequests = SharedOfficeRequest::where('status', '=', SharedOfficeRequest::SHARED_OFFICE_REQUESTS_STATUS)->get();
        } else {
            $sharedOfficeRequests = SharedOfficeRequest::where('status', '!=', SharedOfficeRequest::SHARED_OFFICE_REQUESTS_STATUS)->get();
        }
        return view('backend.sharedoffice.requests.index', [
            'officeRequests' => $sharedOfficeRequests,
            'show_marked' => $show_marked
        ]);
    }

    public function getSharedOfficeBookingRequest(Request $request)
    {
        $sharedOfficeBookingRequests = SharedOfficeBooking::get();
        return view('backend.sharedoffice.requests.bookingRequest', [
            'bookingRequest' => $sharedOfficeBookingRequests
        ]);
    }

    public function editSharedOfficeRequest($id)
    {
        $edit = SharedOfficeRequest::where('id', '=', $id)->first();
        return response([
            'data' => $edit,
            'status' => 'success'
        ]);
    }

    public function updateSharedOfficeRequest(Request $request, $id)
    {
        if ($request->hasFile('file')) {
            $fileName = $request->file('file')->getClientOriginalName();
            $path = 'uploads/listSpaceImages';
            $save = $request->file('file')->move($path, $fileName);
        }
        $getFile = SharedOfficeRequest::where('id', '=', $id)->first();
        $update = SharedOfficeRequest::where('id', '=', $id)->update([
            'company_name' => $request->company_name,
            'full_name' => $request->full_name,
            'email' => $request->email,
            'location' => $request->location,
            'phone_number' => $request->phone_number,
            'image' => isset($fileName) ? $fileName : $getFile->image,
        ]);
        return response([
            'data' => $update,
            'status' => 'success'
        ]);
    }

    public function checkedValueSharedOfficeRequest()
    {
        $getCheckedValues = SharedOfficeRequest::where('status', '=', SharedOfficeRequest::SHARED_OFFICE_REQUESTS_STATUS)->get();
        return $getCheckedValues;
    }

    public function markedSharedOfficeRequest($id)
    {
        $status = SharedOfficeRequest::where('id', '=', $id)->update([
            'status' => SharedOfficeRequest::SHARED_OFFICE_REQUESTS_STATUS
        ]);
        return redirect()->back()->with('message', 'office request viewed by admin');
    }

    public function deleteSharedOfficeRequest($id)
    {
        $delete = SharedOfficeRequest::where('id', '=', $id)->delete();
        return response([
            'data' => $delete,
            'status' => 'success'
        ]);
    }

    public function truncateString($str, $chars, $to_space, $replacement = "...")
    {
        if ($chars > strlen($str)) return $str;

        $str = substr($str, 0, $chars);
        $space_pos = strrpos($str, " ");
        if ($to_space && $space_pos >= 0)
            $str = substr($str, 0, strrpos($str, " "));

        return ($str . $replacement);
    }

    public function indexDt(Request $request)
    {
        ini_set('max_execution_time', '9000000000000000000');
        if (Auth::guard('staff')->user()->staff_type == Staff::STAFF_TYPE_ADMIN) {
            $sharedoffice = SharedOffice::select([
                'id',
                'image',
                'office_name',
                'location',
                'description',
                'number',
                'qr',
                'description',
                'description',
            ])->limit(30)->get();
        } else {
            $staff = Auth::guard('staff')->user();
            if ($staff->permission_group_id == PermissionGroup::PERMISSION_GROUP_SHARED_OFFICE) {
                $sharedoffice = SharedOffice::where('staff_id', '=', $staff->id)->select([
                    'id',
                    'image',
                    'office_name',
                    'location',
                    'description',
                    'number',
                    'qr',
                    'description',
                    'description',
                ])->limit(30)->get();
            } else {
                $sharedoffice = SharedOffice::where('staff_id', '=', $staff->office_id)->select([
                    'id',
                    'image',
                    'office_name',
                    'location',
                    'description',
                    'number',
                    'qr',
                    'description',
                    'description',
                ])->limit(30)->get();
            }
        }

        return Datatables::of($sharedoffice)
            ->addColumn('image', function ($sharedoffice) {
                $image_link = $sharedoffice->image;
                $image = "<img class='img-thumbnail news-thumb' src=" . url($image_link) . " >";
                return $image;
            })
            ->addColumn('office_name', function ($sharedoffice) {
                $office_name = $sharedoffice->office_name;
                return $office_name;
            })
            ->addColumn('location', function ($sharedoffice) {
                $location = $sharedoffice->office_location;
                return $location;
            })
            ->addColumn('description', function ($sharedoffice) {
                $description = self::truncateString($sharedoffice->description, 100, true) . "\n";
                return $description;
            })
            ->addColumn('number', function ($sharedoffice) {
                $number = $sharedoffice->number;
                return $number;
            })
            ->addColumn('qr', function ($sharedoffice) {
                $qr = $sharedoffice->qr;
                $qr = "<img class='img-thumbnail news-thumb' src=" . url($qr) . " >";
                return $qr;
            })
            ->addColumn('actions', function ($sharedoffice) {
                $sharedofficeCount = SharedOfficeImages::where('office_id', '=', $sharedoffice->id)->get();
                $actions = '<center style="height: 40px">' . buildNormalLinkHtml([
                        'url' => route('backend.sharedoffice.products.index', ['id' => $sharedoffice->id]),
                        'description' => '<img src="/images/icons/chair.png"></img>',
                    ]);

                $actions .= buildText([
                    'description' => '|'
                ]);

                $actions .= buildNormalLinkHtml([
                    'url' => route('backend.sharedoffice.edit', ['id' => $sharedoffice->id]),
                    'description' => '<i class="fa fa-pencil"></i>'
                ]);

                $actions .= buildText([
                    'description' => '|'
                ]);

                $actions .= buildConfirmationLinkHtml([
                        'url' => route('backend.sharedoffice.delete', ['id' => $sharedoffice->id]),
                        'description' => '<i class="fa fa-trash"></i>',
                    ]) . '</center>';


                $actions .= '<center style="height: 40px">' . buildNormalLinkHtml([
                        'url' => route('backend.sharedoffice.addimages', ['id' => $sharedoffice->id]),
                        'description' => '<div style="background-color: #e0e0df; padding: 5px; font-weight: 700; border-radius: 3px;font-size: 10pt;">
                                            Add More Images</div>',
                    ]) . '</center>';

                $actions .= '<center>' . buildLinkSimpleHtml([
                        'url' => route('backend.sharedoffice.editmoreimagesIndex', ['id' => $sharedoffice->id]),
                        'description' => '<center style="text-decoration: underline; color: #25ce0f; font-weight:700; font-size:10pt">
                                               Total images - ' . $sharedofficeCount->count() . '</center>',
                    ]) . '</center>';

                return $actions;
            })
            ->rawColumns(['actions', 'image', 'qr'])
            ->make(true);
    }

    public function create()
    {
        $facility = Shfacilitie::all();
        $timezone = TimeZone::select('id', 'text')->get();
        $bulletins = Bulletin::where('status', '=', Bulletin::STATUS_ACTIVE)->get();
        $sharedOfficeProductCategories = SharedOfficeProductCategory::orderBy('id', 'asc')->get();
        $categories = SharedOfficeCategory::all();
        //return view('backend.sharedoffice.create');
        return view('backend.sharedoffice.create', [
            'bulletins' => $bulletins,
            'sharedOfficeProductCategories' => $sharedOfficeProductCategories,
            'timezone' => $timezone,
            'categories' => $categories
        ])->withShfacilities($facility);
    }

    public function storeOffice(SharedOfficeCreateFormRequest $request)
    {
        if (Auth::guard('staff')->user()->staff_type == Staff::STAFF_TYPE_STAFF) {
            $count = SharedOffice::where('staff_id', '=', Auth::guard('staff')->user()->id)->count();
            if ($count >= 1) {
                return collect([
                    'status' => 'error',
                    'message' => 'You\'re not allowed to create more then one shared office..!'
                ]);
            }
        }

        $checkEmail = User::where('email', '=', $request->office_manager)->first();

        if (is_null($checkEmail)) {
            return collect([
                'status' => 'error',
                'message' => 'Office manager Email not Exist'
            ]);
        }


//        try {
            $loc = json_decode($request->current_location);
            $country = self::validateCountry($loc->country);
            if (count((array)($loc->city)) < 1) {
                return collect([
                    'status' => 'error',
                    'message' => 'Select location on level of city'
                ]);
            }
            $city = self::validateCity($loc->city, $country);
            $input = $request->all();
            $input['location'] = $loc->city->long_name . ',' . ' ' . $loc->country->long_name;
            $sharedoffice = DB::transaction(function () use ($input, $request, $city, $country, $loc) {
                $shfacilities = explode(',', $request->shfacilities);
//	            $officenumbergenerator = mt_rand(100000000,1000000000);
                $sharedoffice = new SharedOffice();
                $sharedoffice->office_name = $input['office_name'];
                $sharedoffice->location = $input['location'];
                $sharedoffice->seatsData = $request->seatsData;
                $sharedoffice->office_size_x = $input['office_size_x'];
                $sharedoffice->office_size_y = $input['office_size_y'];
                $sharedoffice->staff_id = \Auth::guard('staff')->user()->id;
                $sharedoffice->description = $input['description'];
                $sharedoffice->pic_size = $input['pic_size'];
                $sharedoffice->qr = "https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=office:" . $sharedoffice->id;
                $sharedoffice->city_id = $city->id;
                $sharedoffice->state_id = $country->id;
                $sharedoffice->continent_id = $country->continent_id;
                $sharedoffice->lng = $loc->lng;
                $sharedoffice->lat = $loc->lat;

                $sharedoffice->category_id = $request->category_id;
                $sharedoffice->office_manager = $request->office_manager;
                $sharedoffice->contact_name = $request->contact_name;
                $sharedoffice->contact_phone = $request->contact_phone;
                $sharedoffice->contact_email = $request->contact_email;
                $sharedoffice->contact_address = $request->contact_address;
                $sharedoffice->monday_opening_time = $request->monday_opening_time;
                $sharedoffice->monday_closing_time = $request->monday_closing_time;
                $sharedoffice->saturday_opening_time = $request->saturday_opening_time;
                $sharedoffice->saturday_closing_time = $request->saturday_closing_time;
                $sharedoffice->sunday_opening_time = $request->sunday_opening_time;
                $sharedoffice->sunday_closing_time = $request->sunday_closing_time;
                $sharedoffice->office_space = $request->office_space;
                $sharedoffice->time_zone = $request->time_zone;

                $sharedoffice->version_english = $request->version_english;
                $sharedoffice->version_chinese = $request->version_chinese;


                if ($request->sharedoffice_status == 'true') {
                    $sharedoffice->active = 1;
                }
                if ($request->sharedoffice_status == 'false') {
                    $sharedoffice->active = 0;
                }


                if ($request->verify_info_to_scan == 'checked' || $request->verify_info_to_scan == 'true') {
                    $sharedoffice->verify_info_to_scan = 1;
                }
                if ($request->verify_info_to_scan == 'false') {
                    $sharedoffice->verify_info_to_scan = 0;
                }

                if ($request->require_deposit == 'checked' || $request->require_deposit == 'true') {
                    $sharedoffice->require_deposit = 1;
                }
                if ($request->require_deposit == 'false') {
                    $sharedoffice->require_deposit = 0;
                }

                $sharedOfficeLanguage = new SharedOfficeLanguage();
//                if ($request->hasFile('image')) {
//                    $img = \Image::make($request->file('image'));
//                    $mime = $img->mime();
//                    $ext = convertMimeToExt($mime);
//                    $path = 'uploads/sharedoffice/';
//                    $filename = 'sahredoffice_' . generateRandomUniqueName();
//                    touchFolder($path);
//                    $request->file('image')->move($path, $filename . $ext);
//                    $input['image'] = ($path . $filename . $ext);
//                }

                $sharedoffice->image = $request->image;
                $sharedoffice->currency_id = isset($input['currency_id']) ? $input['currency_id'] : '';
                $sharedoffice->time_zone = isset($input['time_zone']) ? $input['time_zone'] : '';
                $sharedoffice->save();

                $sharedoffice = SharedOffice::find($sharedoffice->id);
                $sharedoffice->qr = "https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=office:" . $sharedoffice->id;
                $sharedoffice->save();

                $sharedOfficeLanguage->shared_offices_id = $sharedoffice->id;
                $sharedOfficeLanguage->office_name_cn = isset($request->office_name_cn) ? $request->office_name_cn : '--';
                $sharedOfficeLanguage->office_description_cn = isset($request->description_cn) ? $request->description_cn : '--';
                $sharedOfficeLanguage->office_space_cn = isset($request->office_space_cn) ? $request->office_space_cn : '--';
                $sharedOfficeLanguage->contact_name_cn = isset($request->contact_name_cn) ? $request->contact_name_cn : '--';
                $sharedOfficeLanguage->contact_phone_cn = isset($request->contact_phone_cn) ? $request->contact_phone_cn : '--';
                $sharedOfficeLanguage->contact_email_cn = isset($request->contact_email_cn) ? $request->contact_email_cn : '--';
                $sharedOfficeLanguage->contact_address_cn = isset($request->contact_address_cn) ? $request->contact_address_cn : '--';
                $sharedOfficeLanguage->save();
//                dd(json_decode($request->feeHotDesks));
//                $feeHotDesks = json_decode($request->feeHotDesks);
//                $feeDedicatedDesks = json_decode($request->feeDedicatedDesks);
//                $feeOfficeDesks = json_decode($request->feeOfficeDesks);
//                $feeDesks = array_merge($feeHotDesks, $feeDedicatedDesks, $feeOfficeDesks);
//                foreach ($feeDesks as $key => $val) {
//                    $data = (array) $val;
//                    if (isset($val->id)) {
//                        $currentId = ['id' => $val->id];
//                    } else {
//                        $currentId = ['id' => null];
//                        $data = array_merge($data, ['office_id' => $sharedoffice->id]);
//                    }
//                }
                self::createProduct($request, $sharedoffice->id);
                if (isset($request->bulletins) && $request->bulletins != '') {
                    $b = explode(',', $request->bulletins);
                    foreach ($b as $k => $v) {
                        SharedOfficesBulletin::create([
                            'shared_offices_id' => $sharedoffice->id,
                            'bulletin_id' => $v
                        ]);
                    }
                }
                $sharedoffice->shfacilities()->sync($shfacilities, false);
                dispatch(new SharedOfficeSearchJson($sharedoffice, 'en'));
                dispatch(new SharedOfficeSearchJson($sharedoffice, 'cn'));
                return $sharedoffice;
            });

            return collect([
                'status' => 'success',
                'message' => '成功新增新闻'
            ]);
//        } catch (\Exception $e) {
//            return makeResponse($e->getMessage(), true);
//        }
    }

    public function createProduct($request, $officeId)
    {
        $input = $request->all();
        if ($input['feeHotDesks'] || $input['feeDedicatedDesks'] || $input['feeOfficeDesks']) {
            if (!is_null($input['feeHotDesks'])) {
                $feeHotDesks = json_decode($input['feeHotDesks']);
            }
            if (!is_null($input['feeDedicatedDesks'])) {
                $feeDedicatedDesks = json_decode($input['feeDedicatedDesks']);
            }
            if (!is_null($input['feeOfficeDesks'])) {
                $feeOfficeDesks = json_decode($input['feeOfficeDesks']);
            }
            $feeDesks = array_merge($feeHotDesks, $feeDedicatedDesks, $feeOfficeDesks);
            foreach ($feeDesks as $key => $val) {
                $data = (array)$val;
                $data = array_merge($data, ['office_id' => $officeId]);
                $data = array_merge($data, ['status_id' => $data['is_available']]);
                $data = array_merge($data, ['availability_id' => $data['is_available']]);
                $data = array_merge($data, ['qr' => '---']);
                $data = array_merge($data, ['time_price' => $val->no_of_peoples]);
                $data = array_merge($data, ['remark' => 'n/a']);
                $shareOffProd = SharedOfficeProducts::create($data);
                SharedOfficeProducts::where('id', '=', $shareOffProd->id)->update([
                    'number' => $shareOffProd->id,
                    'qr' => "https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=seat:" . $shareOffProd->id . "|office:" . $officeId
                ]);
            }
            return;
        }
        if ($input) {
            $seatsData = (json_decode($input['seatsData']));
            $sharedOfficeProducts = collect();
            foreach ($seatsData as $k => $v) {
                $checkPosition = SharedOfficeProducts::where('x', '=', $v->x)
                    ->where('y', '=', $v->y)
                    ->where('office_id', '=', $officeId)
                    ->first();
                if (!$checkPosition) {
                    //					$productnamegeneratorIn = $this->getUniqueRandomNumber($officeId);
                    $model = new SharedOfficeProducts();
                    $model->qr = "https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=seat:" . $v->seat_number . "|office:" . $officeId;
                    $model->category_id = $v->facility_number;
                    $model->office_id = $officeId;
                    $model->x = $v->x;
                    $model->y = $v->y;
                    $model->number = $v->seat_number;
                    $model->status_id = SharedOfficeProducts::STATUS_EMPTY;
                    $model->month_price = 0;
                    $model->time_price = 0;
                    $model->remark = '';
                    $model->save();
                    $sharedOfficeProducts->push($model);
                }
            }
            return $sharedOfficeProducts;
        }
        /*check is it x,y position used */
//			$checkPosition = SharedOfficeProducts::where('x', '=', $input['x'])
//				->where('y', '=', $input['y'])
//				->where('office_id', '=', $officeId)
//				->first();
//			if(!$checkPosition) {
//				$model = new SharedOfficeProducts();
//				$model->qr = "https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=seat:" . $productnamegeneratorIn . "|office:" . $officeId;
//				$model->category_id = $input['category_id'];
//				$model->office_id = $officeId;
//				$model->x = $input['x'];
//				$model->y = $input['y'];
//				$model->number = $productnamegeneratorIn;
//				$model->status_id = $input['status'];
//				$model->month_price = $input['month_price'];
//				$model->time_price = $input['time_price'];
//				$model->remark = $input['remark'];
//				$model->save();
//
//				$allproducts = SharedOfficeProducts::all();
//				Session::flash('msgProductCreate', 'Product is successfuly created.');
//				return view('backend.sharedoffice.products.index', ['id' =>$officeId])->withPost($allproducts);
//			}
//			else {
//				// SEAT ALREADY CONSUMED
//				// $allproducts = SharedOfficeProducts::all();
//				// Session::flash('msgDanger', 'NOT SAVED, POSITION ALREADY IN USE.');
//				// return view('backend.sharedoffice.products.index', ['id' => $input['office_id']])->withPost($allproducts);
//			}
    }

    public function getUniqueRandomNumber($office_id = false)
    {
        $num = self::generateNum();

        if ($office_id) {
            $items = SharedOfficeProducts::where('office_id', '=', $office_id)->where('number', '=', $num)->first();
            if (!isset($items)) {
                return $num;
            }
            self::getUniqueRandomNumber($office_id);
        }
    }

    public function generateNum()
    {
        return mt_rand(1, 1000);
    }

    public function removeImage($id)
    {
        $sharedOffice = SharedOfficeImages::where('id', '=', $id)->delete();
        return collect([
            'success' => 'true',
            'message' => 'image deleted successfully'
        ]);
    }

    public function storeadditionalimages(Request $request, $id)
    {
        $allowed_image_ext = ['.jpg', 'jpg', 'jpeg', '.jpeg', 'png', '.png'];
        $allowed_video_ext = ['avi', 'mov', 'mp4', 'wmv', 'ogg', 'webm', '3gp'];

        try {
            $input = $request->all();
            if (!is_null($request->image)) {
                $model = new SharedOfficeImages();
                $model->office_id = $input['office_id'];
                $model->pic_size = $input['pic_size'];
                if ($request->hasFile('image')) {
                    $img = \Image::make($request->file('image'));
                    $mime = $img->mime();
                    $ext = convertMimeToExt($mime);
                    $ext = $ext;
                    $array = in_array($ext, $allowed_image_ext);
                    if ($array) {
                        $path = 'uploads/sharedoffice/';
                        $filename = 'AdditionalImages_' . generateRandomUniqueName();
                        touchFolder($path);
                        $request->file('image')->move($path, $filename . '.' . $ext);
                        $input['image'] = ($path . $filename . '.' . $ext);
                        $office = SharedOffice::where('id', '=', $model->office_id)->first();
                        if ($office) {
                            $office->update([
                                'image' => $input['image']
                            ]);
                        }
                    } else {
                        return collect([
                            'success' => 'false',
                            'message' => 'please upload jpg,jpeg,png image'
                        ]);
                    }
                }
                $model->image = $input['image'];
                $model->save();
            } else {
                return collect([
                    'success' => 'false',
                    'message' => 'Ooops false'
                ]);
            }
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function removeDesk($id)
    {
        try {
            SharedOfficeProducts::find($id)->delete();
            return collect([
                'status' => 'success',
                'message' => 'Desk have been deleted!'
            ]);
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function editOffice(SharedOfficeUpdateFormRequest $request, $id)
    {
        $input = $request->all();
        $model = SharedOffice::find($id);
        if (!$model) {
            return makeResponse('新闻不存在', true);
        }
        try {
            if ($request->current_location != "undefined") {
                $loc = json_decode($request->current_location);
                $country = self::validateCountry($loc->country);
                $city = self::validateCity($loc->city, $country);
            }
            $input['location'] = $loc->city->long_name . ',' . ' ' . $loc->country->long_name;
            // $productnamegeneratorIn = $this->getUniqueRandomNumber($id);
            $model->office_name = $input['office_name'];
            $model->location = $request->location;
            $model->location = $request->location;
            $model->office_size_x = $input['office_size_x'];
            $model->office_size_y = $input['office_size_y'];
            $model->seatsData = $input['seatsData'];

            $model->staff_id = \Auth::guard('staff')->user()->id;
            $model->pic_size = $input['pic_size'];
            $model->description = $input['description'];

            if ($request->current_location != "undefined") {
                $model->city_id = $city->id;
                $model->state_id = $country->id;
                $model->continent_id = $country->continent_id;
                $model->lng = $loc->lng;
                $model->lat = $loc->lat;
            }
            $model->category_id = isset($request->category_id) ? $request->category_id : $model->category_id;
            $model->contact_name = $request->contact_name;
            $model->contact_phone = $request->contact_phone;
            $model->contact_email = $request->contact_email;
            $model->contact_address = $request->contact_address;
            $model->monday_opening_time = $request->monday_opening_time;
            $model->monday_closing_time = $request->monday_closing_time;
            $model->saturday_opening_time = $request->saturday_opening_time;
            $model->saturday_closing_time = $request->saturday_closing_time;
            $model->sunday_opening_time = $request->sunday_opening_time;
            $model->sunday_closing_time = $request->sunday_closing_time;
            $model->office_manager = $request->office_manager;
            $model->office_space = $request->office_space;

            if ($request->verify_info_to_scan == 'checked' || $request->verify_info_to_scan == 'true') {
                $model->verify_info_to_scan = 1;
            }
            if ($request->verify_info_to_scan == 'false') {
                $model->verify_info_to_scan = 0;
            }

            if ($request->require_deposit == 'checked' || $request->require_deposit == 'true') {
                $model->require_deposit = 1;
            }
            if ($request->require_deposit == 'false') {
                $model->require_deposit = 0;
            }

            $model->version_english = $request->version_english;
            $model->version_chinese = $request->version_chinese;

            if ($request->hasFile('image')) {
                $img = \Image::make($request->file('image'));
                $mime = $img->mime();
                $ext = convertMimeToExt($mime);
                $path = 'uploads/sharedoffice/';
                $filename = 'news_' . generateRandomUniqueName();
                touchFolder($path);
                $request->file('image')->move($path, $filename . $ext);
                $input['image'] = ($path . $filename . $ext);
            }

            if ($input['image'] == '' || $input['image'] == "undefined") {
                $model->save();
            } else {
                $model->image = $input['image'];
            }
            $model->currency_id = $input['currency_id'];
            $model->time_zone = $input['time_zone'];
            $model->save();
            if (isset($request->bulletins) && $request->bulletins != '') {
                SharedOfficesBulletin::where('shared_offices_id', '=', $id)->delete();
                $b = explode(',', $request->bulletins);
                foreach ($b as $k => $v) {
                    SharedOfficesBulletin::create([
                        'shared_offices_id' => $id,
                        'bulletin_id' => $v
                    ]);
                }
            }
            /* save data many to many in table shared_office_shfacilitie */
            if (isset($request->shfacilities) && !is_null($request->shfacilities)) {
                $request->shfacilities = explode(',', $request->shfacilities);
                $model->shfacilities()->sync($request->shfacilities);
            } else {
                $model->shfacilities()->sync(array());
            }

            $sharedOfficeLanguage = SharedOfficeLanguage::where('shared_offices_id', '=', $model->id)->first();
            if (is_null($sharedOfficeLanguage)) {
                $sharedOfficeLanguage = new SharedOfficeLanguage();
            }
            $sharedOfficeLanguage->shared_offices_id = $model->id;
            $sharedOfficeLanguage->office_name_cn = isset($request->office_name_cn) ? $request->office_name_cn : '--';
            $sharedOfficeLanguage->office_description_cn = isset($request->description_cn) ? $request->description_cn : '--';
            $sharedOfficeLanguage->office_space_cn = isset($request->office_space_cn) ? $request->office_space_cn : '--';
            $sharedOfficeLanguage->contact_name_cn = isset($request->contact_name_cn) ? $request->contact_name_cn : '--';
            $sharedOfficeLanguage->contact_phone_cn = isset($request->contact_phone_cn) ? $request->contact_phone_cn : '--';
            $sharedOfficeLanguage->contact_email_cn = isset($request->contact_email_cn) ? $request->contact_email_cn : '--';
            $sharedOfficeLanguage->contact_address_cn = isset($request->contact_address_cn) ? $request->contact_address_cn : '--';
            $sharedOfficeLanguage->save();

            self::updateProduct($request, $id);

            // }

            $seatsData = (json_decode($input['seatsData']));
            $sharedOfficeProducts = collect();
            if (count($seatsData) > 0) {
                // SharedOfficeProducts::where('office_id', '=', $id)->delete();
                foreach ($seatsData as $k => $v) {
                    $checkPosition = SharedOfficeProducts::where('x', '=', $v->x)
                        ->where('y', '=', $v->y)
                        ->where('office_id', '=', $id)
                        ->first();
                    if (!is_null($checkPosition)) {
                        $checkPosition->update([
                            'category_id' => $v->facility_number,
                            'month_price' => $checkPosition->month_price,
                            'time_price' => $checkPosition->time_price,
                            'number' => $checkPosition->number,
                            'remark' => $checkPosition->remark,
                            'status_id' => $checkPosition->status_id
                        ]);
                    } else {
                        $new = new SharedOfficeProducts();
                        $new->qr = "https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=seat:" . $v->seat_number . "|office:" . $id;
                        $new->category_id = $v->facility_number;
                        $new->office_id = $id;
                        $new->x = $v->x;
                        $new->y = $v->y;
                        $new->number = $v->seat_number;
                        $new->status_id = SharedOfficeProducts::STATUS_EMPTY;
                        $new->time_price = 0;
                        $new->month_price = 0;
                        $new->remark = '';
                        $new->save();
                    }
                }
            }
            dispatch(new SharedOfficeSearchJson($model, 'en'));
            dispatch(new SharedOfficeSearchJson($model, 'cn'));
            return collect([
                'status' => 'success',
                'message' => '成功编辑新闻'
            ]);
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
            // return makeResponse($e->getMessage() . ' at ' . $e->getLine(). ' at ' . $e->getFile(), true);
        }
    }

    public function updateProduct($request, $id)
    {
        $feeHotDesks = json_decode($request->feeHotDesks);
        $feeDedicatedDesks = json_decode($request->feeDedicatedDesks);
        $feeOfficeDesks = json_decode($request->feeOfficeDesks);
        $feeDesks = array_merge($feeHotDesks, $feeDedicatedDesks, $feeOfficeDesks);
        $check = SharedOfficeProducts::where('office_id', '=', $id)->get();
        if (count($check) > 1) {
            foreach ($feeDesks as $key => $val) {
                if ($val->number) {
                    SharedOfficeProducts::where('number', '=', $val->number)->where('office_id', '=', $id)->update([
                        'no_of_peoples' => $val->no_of_peoples,
                        'hourly_price' => $val->hourly_price,
                        'daily_price' => $val->daily_price,
                        'weekly_price' => $val->weekly_price,
                        'month_price' => $val->month_price,
                        'availability_id' => $val->availability_id,
                    ]);
                } elseif (!isset($val->number)) {
                    //add new product
                    $data = (array)$val;
                    $data = array_merge($data, ['office_id' => $id]);
                    $data = array_merge($data, ['status_id' => $data['availability_id']]);
                    $data = array_merge($data, ['qr' => '---']);
                    $data = array_merge($data, ['time_price' => $val->no_of_peoples]);
                    $data = array_merge($data, ['remark' => 'n/a']);
                    $shareOffProd = SharedOfficeProducts::create($data);
                    SharedOfficeProducts::where('id', '=', $shareOffProd->id)->update([
                        'number' => $shareOffProd->id,
                        'availability_id' => $data['availability_id'],
                        'qr' => "https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=seat:" . $shareOffProd->id . "|office:" . $id
                    ]);
                }
            }
        } else {
            foreach ($feeDesks as $key => $val) {
                $data = (array)$val;
                $data = array_merge($data, ['office_id' => $id]);
                $data = array_merge($data, ['status_id' => $data['availability_id']]);
                $data = array_merge($data, ['qr' => '---']);
                $data = array_merge($data, ['time_price' => $val->no_of_peoples]);
                $data = array_merge($data, ['remark' => 'n/a']);
                $shareOffProd = SharedOfficeProducts::create($data);
                SharedOfficeProducts::where('id', '=', $shareOffProd->id)->update([
                    'number' => $shareOffProd->id,
                    'availability_id' => $data['availability_id'],
                    'qr' => "https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=seat:" . $shareOffProd->id . "|office:" . $id
                ]);
            }
        }
    }

    public function edit($id)
    {
        $staff = \Auth::guard('staff')->user();
        if($staff->staff_type == Staff::STAFF_TYPE_STAFF) {
            $baseUrl = \URL::to('/');
            if ($staff->permission_group_id == PermissionGroup::PERMISSION_GROUP_SHARED_OFFICE) {
                $getStaffOffice = SharedOffice::where('staff_id','=',$staff->id)->first();
            } else {
                $getStaffOffice = SharedOffice::where('id','=',$staff->office_id)->first();
            }

            if(!is_null($getStaffOffice)) {
                if ($getStaffOffice->id != $id) {
                    $url = $baseUrl . '/admin/edit/sharedoffice/' . $getStaffOffice->id;
                    return abort(403, 'Page Not Found.');
                }
            } else {
                $url = $baseUrl . '/admin/edit/sharedoffice/' . $getStaffOffice->id;
                return abort(403, 'Page Not Found.');
            }
        }
        $hotDeskType = \App\Models\SharedOfficeFee::HOT_DESK_TYPE;
        $dedicatedDeskType = \App\Models\SharedOfficeFee::DEDICATED_DESK_TYPE;
        $officeDeskType = \App\Models\SharedOfficeFee::OFFICE_DESK_TYPE;
        $model = SharedOffice::with('officeProducts', 'bulletins', 'shfacilities', 'SharedOfficeLanguageData')->where('id', '=', $id)->first();
        $facility = Shfacilitie::all();
        $bulletins = Bulletin::where('status', '=', Bulletin::STATUS_ACTIVE)->get();
        $sharedOfficeProductCategories = SharedOfficeProductCategory::orderBy('id', 'asc')->get();
        $sharedOfficeFee = SharedOfficeProducts::where('office_id', '=', $id)->get();
        $hotDeskFee = SharedOfficeProducts::where('office_id', $id)->where('type', $hotDeskType)->get();
        $dedicatedDeskFee = SharedOfficeProducts::where('office_id', $id)->where('type', $dedicatedDeskType)->get();
        $officeDeskFee = SharedOfficeProducts::where('office_id', $id)->where('type', $officeDeskType)->get();
        $fee = array();
        foreach ($sharedOfficeFee as $officeFee) {
            array_push($fee, $officeFee);
        }
        $sbulletins = $model->bulletins;
        $selectedBulletins = array();
        foreach ($sbulletins as $k => $v) {
            array_push($selectedBulletins, $v->bulletin_id);
        }

        $sfacilities = $model->shfacilities;
        $selectedFacilities = array();
        foreach ($sfacilities as $k => $v) {
            array_push($selectedFacilities, $v->id);
        }
        $timezone = TimeZone::select('id', 'text')->get();
        $location = str_replace('-', ', ', $model->location);
        //check validate_json
        $monday_opening_time = json_decode($model->monday_opening_time);
        $monday_closing_time = json_decode($model->monday_closing_time);
        $date = json_decode(file_get_contents(public_path('date.json')), true);
        $date = json_encode($date);
        if ($monday_opening_time == '') {
            $model->update([
                'monday_opening_time' => $date,
            ]);
        }
        if ($monday_closing_time == '') {
            $model->update([
                'monday_closing_time' => $date
            ]);
        }
        $currency_name = User::getCurrencyFullName($model->currency_id);
        $zone = TimeZone::where('id', '=', $model->time_zone)->first();
        $categories = SharedOfficeCategory::all();
        return view('backend.sharedoffice.edit', [
            'model' => $model,
            'categories' => $categories,
            'currency_name' => $currency_name,
            'time_zone' => $zone,
            'location' => $location,
            'sharedOfficeFee' => json_encode($fee),
            'hotDeskFee' => json_encode($hotDeskFee),
            'countHotDesk' => count($hotDeskFee),
            'dedicatedDeskFee' => json_encode($dedicatedDeskFee),
            'countDedicatedDesk' => count($dedicatedDeskFee),
            'officeDeskFee' => json_encode($officeDeskFee),
            'countOfficeDesk' => count($officeDeskFee),
            'selectedBulletins' => json_encode($selectedBulletins),
            'selectedFacilities' => json_encode($selectedFacilities),
            'id' => $id,
            'bulletins' => $bulletins,
            'timezone' => $timezone,
            'sharedOfficeProductCategories' => $sharedOfficeProductCategories
        ])->withShfacilities($facility);
    }

    public function addimages(Request $request, $id)
    {
        $model = SharedOffice::find($id);
        if(is_null($model))
            return back();
        else
        $detect = new Mobile_Detect();
        $mainImage  = (str_replace('uploads',  'compresses/sharedOffice', $model->image));
        return view('backend.sharedoffice.addimages', [
            'model' => $model,
            'main_image' => $mainImage,
            'is_mobile' => $detect->isMobile()
        ]);
    }

    public function getImages($id)
    {
        $sharedOfficeImages = SharedOfficeImages::select('id', 'priority_images as src')
            ->where('office_id', '=', $id)
            ->where('priority_images', '!=', '')
            ->get();
        $getPriorityImage = [];
        foreach ($sharedOfficeImages as $images) {
            if ($images->priority_images) {
                $url = \URL::to('/') . '/compresses/sharedOffice/';
                $setImage = str_replace($url, '', $images->priority_images);
                $filename = $setImage;
                $src = $images->priority_images;
                array_push($getPriorityImage, array(
                    'name' => $filename,
                    'src' => $src,
                    'id' => $images->id,
                    'type' => SharedOfficeImages::TYPE_IMAGE
                ));
            }
        }
        return collect([
            'thumbnail_image' => $sharedOfficeImages,
            'type' => SharedOfficeImages::TYPE_IMAGE
        ]);
    }

    public function addFiles(Request $request)
    {
        $files = $request->files;
        $officeId = $request->office_id;
        $filesData = [];
        $allowed_image_ext = ['.jpg', 'jpg', 'jpeg', '.jpeg', 'png', '.png'];
        $allowed_video_ext = ['avi', 'mov', 'mp4', 'wmv', 'ogg', 'webm', '3gp'];
        foreach ($files as $filez) {
            foreach ($filez as $file) {
                array_push($filesData, $file);
            }
        }
        if ($filesData) {
            foreach ($filesData as $file) {
                $allowed_image_ext = ['gif', 'jpg', 'jpeg', 'png'];
                $allowed_video_ext = ['avi', 'mov', 'mp4', 'wmv', 'ogg', 'webm', '3gp'];
                $ext = $file->getClientOriginalExtension();
                $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                if (in_array($ext, $allowed_image_ext)) {
                    if ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
                        $type = SharedOfficeImages::TYPE_IMAGE;
                    }
                } else if (in_array($ext, $allowed_video_ext)) {
                    if ($file instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
                        $type = SharedOfficeImages::TYPE_VIDEO;
                    }
                } else {
                    $message = trans('common.not_allow_file_format', ['filetype' => implode(', ', $allowed_image_ext)]);

                    return response()->json([
                        'success' => false,
                        'error' => $message
                    ]);
                }
                if ($type === SharedOfficeImages::TYPE_IMAGE) {
                    $ext = $file->getClientOriginalExtension();
                    $array = in_array($ext, $allowed_image_ext);
                    if ($array) {
                        $filename = $file->getClientOriginalName();
                        $path = 'uploads/sharedOfficeImages';
                        $save = $file->move($path, $filename);
                        $imagePath = asset('uploads/sharedOfficeImages/' . $filename);
                        SharedOfficeImages::create([
                            'office_id' => $officeId,
                            'priority_images' => $imagePath
                        ]);
                    } else {
                        return collect([
                            'success' => 'false',
                            'message' => 'please upload jpg,jpeg,png image'
                        ]);
                    }
                } else {
                    $ext = $file->getClientOriginalExtension();
                    $array = in_array($ext, $allowed_video_ext);
                    if (!$array || $array) {
                        $path = 'uploads/sharedOfficeImages/';
                        $filename = 'Video_' . generateRandomUniqueName();
                        touchFolder($path);
                        $fileMp4 = $path . $filename . '.' . 'mp4';
                        $file->move($path, $filename . '.' . 'mp4');
                        $office = SharedOfficeImages::create([
                            'office_id' => $officeId,
                            'video' => $fileMp4
                        ]);
                    }
                }
            }
        }
        return collect([
            'success' => 'true',
            'message' => 'successfully uploaded files',
            'office_id' => $officeId
        ]);
    }

    public function getFiles($id)
    {
        $sharedOffice = SharedOfficeImages::select('id', 'video')
            ->where('office_id', '=', $id)
            ->whereNotNull('video')
            ->first();
        if (!is_null($sharedOffice)) {
            return collect([
                'id' => $sharedOffice->id,
                'src' => \URL::to('/') . '/' . $sharedOffice->video,
                'type' => SharedOfficeImages::TYPE_VIDEO
            ]);
        }
    }

    public function editmoreimagesIndex($id)
    {
        $model = SharedOffice::find($id);
        return view('backend.sharedoffice.editmoreimages', ['model' => $model]);
    }

    public  function  sharedofficeUpdateData(Request $request){
        $sharedOffice = SharedOffice::where('id','=',$request->office_id)->first();
        if($request->name) {
            $message = 'Office Name';
            $sharedOffice->update([
                'office_name' => $request->name
            ]);
        }
        if($request->location) {
            $message = 'Office Location';
            $sharedOffice->update([
                'location' => $request->location
            ]);
        }
        if($request->size_x || $request->size_y) {
            $message = 'Office Size';
            $sharedOffice->update([
                'office_size_x' => $request->size_x,
                'office_size_y' => $request->size_y
            ]);
        }

        if($request->number) {
            $message = 'Phone Number';
            $sharedOffice->update([
                'number' => $request->number,
            ]);
        }

        return collect([
           'status' => true,
           'message' => $message.' '.'update successfully'
        ]);
    }

    public function editmoreimagesDt($id)
    {
        $sharedoffice = SharedOfficeImages::where('office_id', '=', $id)->get();

        return Datatables::of($sharedoffice)
            ->addColumn('image', function ($sharedoffice) {
                $image_link = url($sharedoffice->image);
                return $image_link;
            })
            ->addColumn('office_id', function ($sharedoffice) {
                $office_id = $sharedoffice->office_id;
                return $office_id;
            })
            ->addColumn('actions', function ($sharedoffice) {
                $sharedofficeCount = SharedOfficeImages::where('office_id', '=', $sharedoffice->id)->get();


                $actions = '<center>' . buildConfirmationLinkHtml([
                        'url' => route('backend.sharedoffice.deleteimages', ['id' => $sharedoffice->id]),
                        'description' => '<i class="fa fa-trash"></i>',
                    ]) . '</center>';

                return $actions;
            })
            ->rawColumns(['actions'])
            ->make(true);

    }

    public function delete($id)
    {
        $model = SharedOffice::find($id);
        $checkproducts = SharedOfficeProducts::where('office_id', '=', $id)->get();
        $allProductsDelete = [];
        foreach($checkproducts as $products) {
            $delete = $products->delete();
            array_push($allProductsDelete,$delete);
        }
//        try {
            if (empty($allProductsDelete)) {
                $model->delete();
                if ($model->delete()) {
                    return makeResponse('成功删除新闻');
                } else {
                    throw new \Exception('未能删除新闻，请稍候再试');
                }
            } else {
                return makeResponse('You can not erase office because you need first to delete products.');
            }
//        } catch (\Exception $e) {
//            return makeResponse($e->getMessage(), true);
//        }
    }

    public function deleteImages($id)
    {
        /*underconstruction*/
        $model = SharedOfficeImages::find($id);

        $model->delete();
        return makeResponse('成功删除新闻');
    }

    public function gethiringRequest()
    {
        $hirings = Hiring::orderBy('id', 'DESC')->get();
        return view("backend.hiring.index", [
            'hiring' => $hirings
        ]);
    }

    public function downloadResume($id)
    {
        if ($id) {
            $download = Hiring::where('id', '=', $id)->first();
            $file_path = public_path('uploads/hiring/' . $download->resume);
            return response()->download($file_path);
        }
        return redirect()->back();
    }

    public function getTodaySharedOfficeRating(Request $request, $id)
    {
        $date = Carbon::parse(request('date'));

        DB::enableQueryLog();

        $sharedOfficeRatingData = SharedOfficeRating::with([
            'rater'
        ])->where('office_id', $id)->where(
            function ($q) use ($date) {
                $q->where('created_at', '>=', $date);
                $q->orWhere('updated_at', '>=', $date);
            })->get();
        if(isset($sharedOfficeRatingData) && count($sharedOfficeRatingData) > 0 ) {
            $average = $sharedOfficeRatingData[0] ? $sharedOfficeRatingData[0]->avg : 0;
            $ratingPercent = [];
            $totalRecords = count($sharedOfficeRatingData);
            $fiveStar = $sharedOfficeRatingData->where('rate', '>', 4);
            $fiveStarPercent = (count($fiveStar) / $totalRecords) * 100;
            $fourStar = $sharedOfficeRatingData->where('rate', '>', 3)->where('rate', '<=', 4);
            $fourStarPercent = (count($fourStar) / $totalRecords) * 100;
            $threeStar = $sharedOfficeRatingData->where('rate', '>', 2)->where('rate', '<=', 3);
            $threeStarPercent = (count($threeStar) / $totalRecords) * 100;
            $twoStar = $sharedOfficeRatingData->where('rate', '>', 1)->where('rate', '<=', 2);
            $twoStarPercent = (count($twoStar) / $totalRecords) * 100;
            $oneStar = $sharedOfficeRatingData->where('rate', '>', 0)->where('rate', '<=', 1);
            $oneStarPercent = (count($oneStar) / $totalRecords) * 100;
            $query = DB::getQueryLog();
            array_push($ratingPercent, array(
                'fiveStarPercent' => $fiveStarPercent,
                'fourStarPercent' => $fourStarPercent,
                'threeStarPercent' => $threeStarPercent,
                'twoStarPercent' => $twoStarPercent,
                'oneStarPercent' => $oneStarPercent,
            ));
        }
        return collect([
            'data' => $sharedOfficeRatingData,
            'query' => isset($query) ? $query : '',
            'ratingPercent' => isset($ratingPercent) ? $ratingPercent : '',
            'average' => isset($average) ? $average : '',
            'status' => 'success',
        ]);
    }

    public function logoutMobile()
    {
        Auth::guard('staff')->logout();
    }
}
