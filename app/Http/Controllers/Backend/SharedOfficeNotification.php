<?php

namespace App\Http\Controllers\Backend;

use App\Models\ChangeBookingRequest;
use App\Models\SharedOffice;
use App\Models\SharedOfficeBooking;
use App\Models\SharedOfficeProducts;
use App\Models\SharedOfficeReserveRequest;
use App\Models\Staff;
use App\Models\PermissionGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class SharedOfficeNotification extends Controller
{
    public  function index(){
        $staff = Auth::guard('staff')->user();
        $bookings = $this->getBookings($staff);
        return view('backend.sharedoffice.notifications.index',[
            'bookings' => $bookings
        ]);
    }

    public function unReadAble($id){
        SharedOfficeBooking::where('id','=',$id)->update([
            'is_read' => SharedOfficeBooking::IS_NOT_READ
        ]);
        return collect([
            'status' => true,
            'message' => 'unread'
        ]);
    }

    public function readAble($id){
        SharedOfficeBooking::where('id','=',$id)->update([
            'is_read' => SharedOfficeBooking::IS_READ
        ]);
        return collect([
            'status' => true,
            'message' => 'read'
        ]);
    }

    public function detailNotification($id){
        $details = SharedOfficeBooking::where('id','=',$id)->with('office','users')->first();
        return view('backend.sharedoffice.notifications.detail',[
            'detail' => $details
        ]);
    }

    public function getBookings($staff) {
        $staff = $staff->where('id','=',$staff->id)->with('office')->first();
        if(!is_null($staff->office)) {
            $officeId = $staff->office->id;
            $bookings = SharedOfficeBooking::where('office_id', '=', $officeId)->where('status','=',SharedOfficeBooking::STATUS_REQUEST)
                ->orderBy('id','desc')
                ->get();
            return $bookings;
        }
    }

    public function getBookedRequests(Request $request , $id){
        $staff = \Auth::guard('staff')->user();
        $checkOfficeExist = SharedOffice::where('id','=',$id)->first();
        if(!is_null($checkOfficeExist)) {
            $staff->update([
                'office_id' => $id
            ]);
        }
        if($staff->staff_type == Staff::STAFF_TYPE_STAFF) {
            $baseUrl = \URL::to('/');
            if ($staff->permission_group_id == PermissionGroup::PERMISSION_GROUP_SHARED_OFFICE) {
                $getStaffOffice = SharedOffice::where('staff_id','=',$staff->id)->first();
            } else {
                $getStaffOffice = SharedOffice::where('id','=',$staff->office_id)->first();
            }

            if(!is_null($checkOfficeExist)) {
                if (is_null($getStaffOffice)) {
                    $url = $baseUrl . '/admin/booked/requests/' . $id;
                    return abort(403, 'Page Not Found.');
                }

                if ($checkOfficeExist->staff_id != $staff->id && $checkOfficeExist->id != $staff->office_id) {
                    $url = $baseUrl . '/admin/booked/requests/' . $getStaffOffice->id;
                    return abort(403, 'Page Not Found.');
                }
            } else {
                if(is_null($checkOfficeExist)){
                    return  back();
                } else {
                    $url = $baseUrl . '/admin/booked/requests/' . $id;
                    return abort(403, 'Page Not Found.');
                }
            }
        }
        $bookedRequests = SharedOfficeBooking::where('status','=',SharedOfficeBooking::STATUS_BOOKED)->paginate(10);
        return view('backend.sharedoffice.bookedRequests.booked',[
            'bookedRequests' => $bookedRequests
        ]);
    }

    public function changeBookedRequests($id , Request $request){
        $bookedRequests = ChangeBookingRequest::where('office_id','=',$id)->with(['user',
        'booking' => function($q){$q->withTrashed();},'office','product'])
        ->Has('product')
        ->get();
        if($request->username) {
            $username = $request->username;
            $bookedRequests = ChangeBookingRequest::where('office_id','=',$id)->with(['user' => function($q) use ($username){
                $q->where('name', 'LIKE', "%{$username}%");
            }
            ,'booking','office','product'])
            ->whereHas('user', function($q) use ($username){
                $q->where('name', 'LIKE', "%{$username}%");
            })
                ->Has('product')
                ->get();
        } else if($request->email) {
            $email = $request->email;
            $bookedRequests = ChangeBookingRequest::where('office_id','=',$id)->with(['user' => function($q) use ($email){
                    $q->where('email', 'LIKE', "%{$email}%");
                }
                ,'booking','office','product'])
                ->whereHas('user', function($q) use ($email){
                    $q->where('email', 'LIKE', "%{$email}%");
                })
                ->Has('product')
                ->get();
        }
        return view('backend.sharedoffice.requests.change_booking',[
            'bookedRequests' => $bookedRequests,
            'id' => $id
        ]);
    }

    public function getProductDetail($officeId) {
        $getProductDetail = SharedOfficeProducts::where('office_id','=',$officeId)->get();
        return collect([
           'status' => true,
           'data' => $getProductDetail
        ]);
    }

    public function updateSeatNumber(Request $request){
        // change status previous seat
        $changePreSeatStatus = SharedOfficeProducts::where('office_id','=',$request->office_id)->where('number','=',$request->selected_seat)->update([
            'status_id' => SharedOfficeProducts::STATUS_EMPTY
        ]);
        // update status currant seat
        $changeCurrantSeatStatus = SharedOfficeProducts::where('office_id','=',$request->office_id)->where('number','=',$request->currant_seat)->update([
            'status_id' => SharedOfficeProducts::STATUS_BOOKED
        ]);
        // udpate currant seat
        $changeBooking = ChangeBookingRequest::where('id','=',$request->change_booking_request_id)->update([
            'currant_seat' => $request->currant_seat,
        ]);
        return collect([
            'status' => true,
            'message' => 'seat change successfully'
        ]);
    }

    public function updateDateTime(Request $request){
        ChangeBookingRequest::where('id','=',$request->id)->update([
           'check_in' => $request->check_in,
           'check_out' => $request->check_out,
        ]);
        return collect([
           'status' => true,
           'message' => 'date time update successfully'
        ]);
    }

    public function confirmChange(Request $request){
        $getProduct = SharedOfficeProducts::where('id','=',$request->change_seat_id)->first();
        $checkBooking = SharedOfficeBooking::where('id','=',$request->book_id)->first();
        //update previous seat status
        if($request->currant_seat_id != $getProduct->number){
            SharedOfficeProducts::where('number','=',$request->currant_seat_id)->update([
                'status_id' => SharedOfficeProducts::STATUS_EMPTY
            ]);
        }

        //update new seat status
        $getProduct->update([
           'status_id' => SharedOfficeProducts::STATUS_BOOKED
        ]);
        //update shared office booking table data
        $checkBooking->update([
            'check_in' => $request->check_in,
            'check_out' => $request->check_out,
            'category_id' => $getProduct->category_id
        ]);
        ChangeBookingRequest::where('id','=',$request->id)->update([
           'status' => ChangeBookingRequest::STATUS_CONFIRM
        ]);
        $office = $checkBooking->office;
        $user = $checkBooking->users;
        $reason = 'Your Booking Request Against '.$office->office_name.' Will be Changed Successfully';
        //sender email
        $senderEmail = "support@xenren.co";
        $senderName = "Support Team";
        Mail::send('mails.confirmChangeBookingRequest', array('office_name' => $office->office_name , 'reason' => $reason, 'username' => $user->first_name . ' ' . $user->last_name), function ($message) use ($senderEmail, $senderName, $user) {
            $message->from($senderEmail, $senderName);
            $message->to($user->email, $user->first_name . ' ' . $user->last_name)
                ->subject(trans('common.edit_email'));
        });
        return collect([
            'status' => true,
            'message' => 'Booking Time Change Success'
        ]);
    }

    public function cancelChange($bookId,$id){
        $checkBooking = SharedOfficeBooking::where('id','=',$bookId)->with('users','office')->first();
        $office = $checkBooking->office;
        $user = $checkBooking->users;
        $reason = 'Your Booking Request Against '.$office->office_name.' Will be Reject. We Cannot Proceed Your Request Right Now Thanks';
        ChangeBookingRequest::where('id','=',$id)->update([
            'status' => ChangeBookingRequest::STATUS_CANCEL
        ]);
        //sender email
        $senderEmail = "support@xenren.co";
        $senderName = "Support Team";
        Mail::send('mails.cancelChangeBookingRequest', array('office_name' => $office->office_name , 'reason' => $reason, 'username' => $user->first_name . ' ' . $user->last_name), function ($message) use ($senderEmail, $senderName, $user) {
            $message->from($senderEmail, $senderName);
            $message->to($user->email, $user->first_name . ' ' . $user->last_name)
                ->subject(trans('common.edit_email'));
        });
        return collect([
           'status' => true,
           'message' => 'Booking Time Not Change'
        ]);
    }

    public function cancelBookingRequest(Request $request){
        $getBooking = SharedOfficeBooking::where('id','=',$request->_id)->first();
        SharedOfficeProducts::where('office_id','=',$getBooking->office_id)->where('category_id','=',$getBooking->category_id)->update([
           'status_id' => SharedOfficeProducts::STATUS_EMPTY
        ]);
        SharedOfficeBooking::where('id','=',$request->_id)->update([
            'status' => SharedOfficeBooking::STATUS_CANCELED
        ]);
        SharedOfficeBooking::where('id','=',$request->_id)->delete();
        return back()->with('success_message','request deleted success');
    }
}
