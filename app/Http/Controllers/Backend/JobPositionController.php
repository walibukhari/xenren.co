<?php

namespace App\Http\Controllers\Backend;

use Auth;
use Datatables;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;
use App\Models\JobPosition;
use App\Http\Requests\Backend\JobPositionCreateFormRequest;
use App\Http\Requests\Backend\JobPositionEditFormRequest;

class JobPositionController extends BackendController
{
    public function index(Request $request, $id = null) {
        return view('backend.jobposition.index');
    }

    public function indexDt(Request $request, $id = null) {
        $model = JobPosition::GetJobPosition();

        return Datatables::of($model)
            ->addColumn('names', function ($model) {
                return $model->getNames();
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = '';
                $actions .= buildRemoteLinkHtml([
                    'url' => route('backend.jobposition.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-pencil"></i>',
                    'modal' => '#remote-modal',
                ]);
                $actions .= buildConfirmationLinkHtml([
                    'url' => route('backend.jobposition.delete', ['id' => $model->id]),
                    'description' => '<i class="fa fa-trash"></i>',
                    'title' => '确认删除？',
                    'content' => '确认删除此主职？'
                ]);
                return $actions;
            })
	          ->rawColumns(['actions'])
            ->make(true);
    }

    public function create(Request $request) {
        return view('backend.jobposition.create');
    }

    public function createPost(JobPositionCreateFormRequest $request, $id = null) {
        try {
            $model = new JobPosition();
            $model->name_cn = $request->get('name_cn');
            $model->name_en = $request->get('name_en');
            $model->remark_cn = $request->get('remark_cn');
            $model->remark_en = $request->get('remark_en');

            $model->save();
            return makeResponse('成功新增主职');
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function edit($id) {
        $model = JobPosition::GetJobPosition()->find($id);

        return view('backend.jobposition.edit', ['model' => $model]);
    }

    public function editPost(JobPositionEditFormRequest $request, $id) {
        $model = JobPosition::GetJobPosition()->find($id);

        if (!$model) {
            return makeResponse('主职不存在', true);
        }

        try {
            $model->name_cn = $request->get('name_cn');
            $model->name_en = $request->get('name_en');
            $model->remark_cn = $request->get('remark_cn');
            $model->remark_en = $request->get('remark_en');

            $model->save();
            return makeResponse('成功编辑主职');
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function delete($id) {
        $model = JobPosition::GetJobPosition()->find($id);

        try {
            if ($model->delete()) {
                return makeResponse('成功删除主职');
            } else {
                throw new \Exception('未能删除主职，请稍候再试');
            }
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }
}
