<?php

namespace App\Http\Controllers\Backend;

use App\Constants;
use App\Models\Countries;
use App\Models\Language;
use App\Models\OfficialProjectNews;
use App\Models\Project;
use App\Models\ProjectApplicant;
use App\Models\ProjectSkill;
use App\Models\Review;
use App\Models\ReviewSkill;
use App\Models\UserInbox;
use App\Models\UserInboxMessages;
use App\Models\WorldCountries;
use App\Models\CoinProject;
use App\Models\Coins;
use Auth;
use Input;
use Datatables;
use Validator;
use DB;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;
use App\Models\OfficialProject;
use App\Models\OfficialProjectImage;
use App\Models\OfficialProjectPosition;
use App\Models\Skill;
use App\Models\JobPosition;
use App\Http\Requests\Backend\OfficialProjectCreateFormRequest;
use App\Http\Requests\Backend\OfficialProjectEditFormRequest;

class OfficialProjectController extends BackendController
{
    public function index(Request $request, $id = null) {

        return view('backend.officialproject.index');
    }

    public function indexDt(Request $request, $id = null) {
        $model = OfficialProject::with('project')->orderBy('id', 'desc')->where('project_id', '>', 0);

        return Datatables::of($model)
            ->editColumn('language', function ($model) {
                if ($model->project->language != null)
                {
                    return $model->project->language->getName();
                }
            })
            ->editColumn('country', function ($model) {
                if ($model->project->country != null)
                {
                    return $model->project->country->getName();
                }
            })
            ->editColumn('status', function ($model) {
                return $model->explainStatus();
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = '';

                $actions .= buildLinkHtml([
                    'url' => route('backend.officialproject.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-pencil"></i>'
                ]);

                $actions .= buildConfirmationLinkHtml([
                    'url' => route('backend.officialproject.delete.post', ['id' => $model->id]),
                    'description' => '<i class="fa fa-trash"></i>',
                ]);

                $actions .= buildLinkHtml([
                    'url' => route('backend.officialproject.applicant', ['id' => $model->id]),
                    'color' => 'default',
                    'description' => '<i class="fa fa-users"></i>'
                ]);

                $actions .= buildLinkHtml([
                    'url' => route('backend.officialproject.news.list', ['project_id' => $model->project->id]),
                    'color' => 'green',
                    'description' => '<i class="fa fa-newspaper-o"></i>'
                ]);

                return $actions;
            })
		        ->rawColumns(['actions'])
            ->make(true);
    }

    public function create(Request $request) {
        $skills = Skill::where('tag_color', Skill::TAG_COLOR_GOLD)
            ->pluck('name_cn', 'id');
        if(app()->getLocale() == 'cn') {
	        $positions = JobPosition::pluck('name', 'id');
        } else {
	        $positions = JobPosition::pluck('name_cn', 'id');
        }
        $fundingStages = Constants::getFundingStageLists();
        $designTypes = OfficialProject::getDesignTypes();
        $countries = WorldCountries::get();
        $languages = Language::getLanguageLists();
        $coinList = Coins::getAllCoinFormated();

        return view('backend.officialproject.create', [
            'skills' => $skills,
            'positions' => $positions,
            'fundingStages' => $fundingStages,
            'designTypes' => $designTypes,
            'countries' => $countries,
            'languages' => $languages,
            'coinList' => $coinList
        ]);
    }

    public function createPost(OfficialProjectCreateFormRequest $request, $id = null) {
        try {
            \DB::beginTransaction();

            //add a record to project table
            $project = new Project();
            $project->type = Project::PROJECT_TYPE_OFFICIAL;
            $project->user_id = Auth::guard('staff')->user()->id;
            $project->staff_id = Auth::guard('staff')->user()->id;
            $project->name = $request->get('title');
            $project->country_id = $request->get('country_id');
            $project->language_id = $request->get('language_id');
            $project->description = $request->get('description');
            $project->pay_type = Constants::PAY_TYPE_HOURLY_PAY;
            $project->weekly_limit = isset($request->weekly_limit) ? $request->weekly_limit : 0;
            $project->weekly_limit = isset($request->weekly_limit) ? $request->weekly_limit : 0;
            $project->time_commitment = isset($request->time_commitment ) ? $request->time_commitment  : 0;
            $project->freelance_type = isset($request->freelance_type ) ? $request->freelance_type  : 1 ;
            $project->status = Project::STATUS_PUBLISHED;
            $project->is_read = Project::IS_READ_NO;
            $project->save();

            // add coin if exist
            $coinList = Coins::getAllCoinFormated();
            $keyList = array_keys($coinList);
            if (!is_null($request->coin_type) && in_array($request->coin_type, $keyList)) {
                $coinProject = CoinProject::where('project_id', $project->id)
                    ->where('coin_type', $request->coin_type)
                    ->first();
                if (is_null($coinProject)) {
                    $coinProject = new CoinProject();
                    $coinProject->project_id = $project->id;
                    $coinProject->coin_type = $request->coin_type;
                    $coinProject->save();
                }
            }

            $model = new OfficialProject();
            $model->project_id = $project->id;
            $model->lang= isset($request->lang) ? $request->lang : 'cn';
            $model->fill($request->except(['images', 'skills', 'positions', 'needed', 'position_description']));
            $images = array();
            $skills = array();
            $positions = array();
            $position_descriptions = $request->get('position_description');
            $position_needed = $request->get('needed');

            if ($request->hasFile('images')) {
                foreach ($request->file('images') as $key => $var) {
                    $i = new OfficialProjectImage();
                    $i->image = $var;
                    $images[] = $i;
                }
            }
            if ($request->has('skills')) {
                foreach ($request->get('skills') as $key => $var) {
                    $s = new ProjectSkill();
                    $s->skill_id = $var;
                    $skills[] = $s;
                }
            }
            if ($request->has('positions')) {
                foreach ($request->get('positions') as $key => $var) {
                    if (isset($position_needed[$key]) && $position_needed[$key]!= 0 ) {
                        $p = new OfficialProjectPosition();
                        $p->job_position_id = $var;
                        $p->needed = $position_needed[$key];
                        $p->description = isset($position_descriptions[$key]) ? $position_descriptions[$key] : null;
                        $positions[] = $p;
                    }
                }
            }

            $model->save();

            foreach ($images as $key => $var) {
                $var->official_project_id = $model->id;
                $var->save();
            }
            foreach ($skills as $key => $var) {
                $var->project_id = $project->id;
                $var->save();
            }
            foreach ($positions as $key => $var) {
                $var->official_project_id = $model->id;
                $var->save();
            }

            //add user inbox notification
            $userInbox =  new UserInbox();
            $userInbox->category = UserInbox::CATEGORY_SYSTEM;
            $userInbox->project_id =  $project->id;
            $userInbox->from_user_id = null;
            $userInbox->to_user_id = null;
            $userInbox->from_staff_id = 1;
            $userInbox->to_staff_id = Auth::guard('staff')->user()->id;
            $userInbox->type = UserInbox::TYPE_PROJECT_SUCCESSFUL_POST_JOB;
            $userInbox->is_trash = 0;
            $userInbox->save();

            //add user inbox message
            $userInboxMessage = new UserInboxMessages();
            $userInboxMessage->inbox_id = $userInbox->id;
            $userInboxMessage->from_user_id = null;
            $userInboxMessage->to_user_id = null;
            $userInboxMessage->from_staff_id = 1;
            $userInboxMessage->to_staff_id = Auth::guard('staff')->user()->id;
            $userInboxMessage->project_id = $project->id;
            $userInboxMessage->is_read = 0;
            $userInboxMessage->type = UserInboxMessages::TYPE_PROJECT_SUCCESSFUL_POST_JOB;

            $message = trans('common.you_success_post_project') . "<br/>" .
                trans('common.project_name') . " : <a href='" . route('backend.officialproject.edit', ['id' => $model->id ] ) . "'>" . $project->name . "</a>";
            $userInboxMessage->custom_message = $message;
            $userInboxMessage->save();

            \DB::commit();
            return makeResponse('成功新增官方项目');
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse($e->getMessage() . ' at ' . $e->getFile() . ' at ' . $e->getLine(), true);
        }
    }

    public function edit(Request $request, $id) {
        $skills = Skill::where('tag_color', Skill::TAG_COLOR_GOLD)
            ->pluck('name_cn', 'id');
		    if(app()->getLocale() == 'cn') {
			    $positions = JobPosition::pluck('name_cn', 'id');
		    } else {
			    $positions = JobPosition::pluck('name_en', 'id');
		    }
        $fundingStages = Constants::getFundingStageLists();
        $designTypes = OfficialProject::getDesignTypes();
        $countries = WorldCountries::get();
        $languages = Language::getLanguageLists();

        $model = OfficialProject::with(['projectPositions', 'projectImages'])->GetRecords()->find($id);

        $project = Project::with('projectSkills', 'coinProject')
            ->where('id', $model->project_id)->first();
        $coinList = Coins::getAllCoinFormated();

        return view('backend.officialproject.edit', [
            'skills' => $skills,
            'positions' => $positions,
            'fundingStages' => $fundingStages,
            'designTypes' => $designTypes,
            'model' => $model,
            'project' => $project,
            'countries' => $countries,
            'languages' => $languages,
            'coinList' => $coinList
        ]);
    }

    public function editPost(OfficialProjectEditFormRequest $request, $id = null) {
        try {
            \DB::beginTransaction();
            $model = OfficialProject::GetRecords()->find($id);

            //edit project table
            $project = Project::where('id', $model->project_id)->first();
            $project->name = $model->title;
            if($request->get('country_id')) {
	            $project->country_id = $request->get('country_id');
            }
            $project->language_id = $request->get('language_id');
            $project->description = $model->description;
		        $project->weekly_limit = $request->weekly_limit;
		        $project->save();

            // update coin if exist
            $coinList = Coins::getAllCoinFormated();
            $keyList = array_keys($coinList);
            // remove existing
            $coinProject = CoinProject::where('project_id', $project->id)->delete();
            if (!is_null($request->coin_type) && in_array($request->coin_type, $keyList)) {
                $coinProject = new CoinProject();
                $coinProject->project_id = $project->id;
                $coinProject->coin_type = $request->coin_type;
                $coinProject->save();
            }

            $model->fill($request->except(['images', 'skills', 'positions', 'needed', 'position_description']));
            $images = array();
            $skills = array();
            $positions = array();
            $position_descriptions = $request->get('position_description');
            $position_needed = $request->get('needed');
            $delete_image = $request->get('delete_image');
            $delete_images_paths = array();

            if ($delete_image && count($delete_image) > 0) {
                foreach ($delete_image as $key => $var) {
                    $image = OfficialProjectImage::find($var);
                    if ($image) {
                        $delete_images_paths[] = $image->image;
                        $image->delete();
                    }
                }
            }

            if ($request->hasFile('images')) {
                foreach ($request->file('images') as $key => $var) {
                    $i = new OfficialProjectImage();
                    $i->image = $var;
                    $images[] = $i;
                }
            }

            //Delete all skills
            $project->projectSkills()->delete();
            if ($request->has('skills')) {
                foreach ($request->get('skills') as $key => $var) {
                    $s = new ProjectSkill();
                    $s->skill_id = $var;
                    $skills[] = $s;
                }
            }

            //Delete all positions
            $model->projectPositions()->delete();
            if ($request->has('positions')) {
                foreach ($request->get('positions') as $key => $var) {
                    if (isset($position_needed[$key]) && $position_needed[$key]!= 0 ) {
                        $p = new OfficialProjectPosition();
                        $p->job_position_id = $var;
                        $p->needed = $position_needed[$key];
                        $p->description = isset($position_descriptions[$key]) ? $position_descriptions[$key] : null;
                        $positions[] = $p;
                    }
                }
            }

            $model->save();

            foreach ($images as $key => $var) {
                $var->official_project_id = $model->id;
                $var->save();
            }
            foreach ($skills as $key => $var) {
                $var->project_id = $project->id;
                $var->save();
            }
            foreach ($positions as $key => $var) {
                $var->official_project_id = $model->id;
                $var->save();
            }

            \DB::commit();
            //Delete image after commit, make sure no image get deleted but row still exists
            if ($delete_images_paths && count($delete_images_paths) > 0) {
                foreach ($delete_images_paths as $key => $var) {
                    @unlink(public_path($var));
                }
            }
            return makeResponse('成功新增官方项目');
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse($e->getMessage(), true);
        }
    }

    public function deletePost(Request $request, $id) {
        try {
            $model = OfficialProject::with(['projectPositions', 'projectImages'])->GetRecords()->find($id);

            $project = Project::where('id', $model->project_id)->first();
            $project->delete();

            $delete_images_paths = array();
            if ($model->projectImages) {
                foreach ($model->projectImages as $key => $var) {
                    $delete_images_paths[] = $var->image;
                }
            }

            $model->delete();
            \DB::commit();
            //Delete image after commit, make sure no image get deleted but row still exists
            if ($delete_images_paths && count($delete_images_paths) > 0) {
                foreach ($delete_images_paths as $key => $var) {
                    @unlink(public_path($var));
                }
            }
            return makeResponse('成功删除官方项目');
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse($e->getMessage(), true);
        }

    }

    public function applicant(Request $request, $id = null)
    {

        $officialProject = OfficialProject::find($id);

        return view('backend.officialproject.applicant', [
            'officialProject' => $officialProject
        ]);
    }



    public function applicantDt(Request $request, $id = null)
    {
        $officialProject = OfficialProject::find($id);

        $model = ProjectApplicant::with('user')->where('project_id', $officialProject->project_id);

        return Datatables::of($model)
            ->addColumn('username', function ($model) {
                return $model->user->getName();
            })
            ->addColumn('pay_type', function ($model) {
                return $model->getPayType();
            })
            ->addColumn('status', function ($model) {
                return $model->getStatus();
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = '';

                $actions = buildConfirmationLinkHtml([
                    'url' => route('backend.officialproject.applicant.awardjob', ['applicantid' => $model->id]),
                    'color' => 'blue',
                    'description' => '<i class="fa fa-check"></i>',
                    'title' => trans('common.award_job'),
	                  'modal' => '#award-job-modal-full',
	                  'tool-tip' => 'Award Job'
                ]);
                $actions .= buildConfirmationLinkHtml([
                    'url' => route('backend.officialproject.applicant.reject', ['applicantid' => $model->id]),
                    'description' => '<i class="fa fa-remove"></i>',
                    'color' => 'red',
                    'title' => trans('common.reject'),
                ]);
                $actions .= buildRemoteLinkHtml([
                    'url' => route('backend.officialproject.applicant.sendmessage', ['applicantid' => $model->id]),
                    'description' => '<i class="fa fa-inbox"></i>',
                    'color' => 'green',
                    'modal' => '#remote-modal'
                ]);
                $actions .= buildRemoteLinkHtml([
                    'url' => route('backend.officialproject.applicant.info', ['applicantid' => $model->id]),
                    'description' => '<i class="fa fa-user"></i>',
                    'color' => 'yellow',
                    'modal' => '#remote-modal'
                ]);
                $actions .= buildRemoteLinkHtml([
                    'url' => route('backend.officialproject.applicant.endcontract', ['applicantid' => $model->id]),
                    'description' => '<i class="fa fa-thumbs-o-up"></i>',
                    'color' => 'purple-seance',
                    'modal' => '#remote-modal-large'
                ]);
                return $actions;
            })
	          ->rawColumns(['actions'])
            ->make(true);
    }

    public function awardJob($applicantId, Request $request)
    {
        $model = ProjectApplicant::with(['project', 'user'])->find($applicantId);

        if ($model) {
            //Check if project have accepted applicant or not
            $exists = ProjectApplicant::where('project_id', '=', $model->project_id)
                ->where('status', '=', ProjectApplicant::STATUS_CREATOR_SELECTED)
                ->where('user_id', '=', $model->user_id)
                ->count();

            if ($exists) {
                return makeResponse(trans('common.project_already_have_applicant'));
            }

            $model->status = ProjectApplicant::STATUS_CREATOR_SELECTED;
            $model->is_read = ProjectApplicant::IS_READ_NO;
						$model->hourly_rate = isset($request->num_of_shares) ? $request->num_of_shares : 0;
            $project = $model->project;
            $project->status = Project::STATUS_SELECTED;

            //Here we send user inbox
            $ui = new UserInbox();
            $ui->category = UserInbox::CATEGORY_NORMAL;
            $ui->project_id = $project->id;
            $ui->from_user_id = null;
            $ui->to_user_id = $model->user_id;
            $ui->from_staff_id = $project->staff_id;
            $ui->to_staff_id = null;
            $ui->type = UserInbox::TYPE_PROJECT_APPLICANT_SELECTED;
            $ui->is_trash = 0;

            //Here we create user inbox message
            $uim = new UserInboxMessages();
            $uim->inbox_id = $ui->id;
            $uim->from_user_id = null;
            $uim->to_user_id = $model->user_id;
            $uim->from_staff_id = $project->staff_id;
            $uim->to_staff_id = null;
            $uim->project_id = $project->id;
            $uim->is_read = 0;
            $uim->type = UserInboxMessages::TYPE_PROJECT_APPLICANT_SELECTED;
            $uim->custom_message = null;
            $uim->quote_price = null;
            $uim->pay_type = null;

            try {
                \DB::beginTransaction();

                if (!$model->save()) {
                    return makeResponse(trans('common.unknown_error'));
                }
                if (!$project->save()) {
                    return makeResponse(trans('common.unknown_error'));
                }

                $ui->project_id = $project->id;
                if (!$ui->save()) {
                    return makeResponse(trans('common.unknown_error'));
                }

                $uim->project_id = $project->id;
                $uim->inbox_id = $ui->id;
                if (!$uim->save()) {
                    return makeResponse(trans('common.unknown_error'));
                }

                //Update the applicant unread message
                $applicant = $model->user;
                $applicant->unread_message = UserInboxMessages::where('to_user_id', '=', $applicant->id)
                    ->where('is_read', '=', 0)
                    ->count();
                if (!$applicant->save()) {
                    return makeResponse(trans('common.unknown_error'));
                }

                \DB::commit();
                return makeResponse(trans('common.submit_successful'));
            } catch (\Exception $e) {
                \DB::rollback();
                return makeResponse($e->getMessage(), true);
            }

        } else {
            return response(trans('common.project_applicant_not_found'), 422);
        }
    }

    public function reject($applicantId)
    {
        $model = ProjectApplicant::find($applicantId);
        $model->status = ProjectApplicant::STATUS_CREATOR_REJECTED;
        $model->is_read = ProjectApplicant::IS_READ_NO;

        try {
            if ($model->save()) {
                return makeResponse(trans('common.submit_successful'));

            } else {
                throw new \Exception(trans('common.err_happen'));
            }
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function sendMessage($applicantId)
    {
        $projectApplicant = ProjectApplicant::with('user')->find($applicantId);

        return view('backend.officialproject.sendMessage', [
            'projectApplicant' => $projectApplicant
        ]);
    }

    public function sendMessagePost($applicantId, Request $request)
    {
        $projectApplicant = ProjectApplicant::with('user')->where('id', $applicantId)->first();

        try {
            \DB::beginTransaction();

            //add user inbox notification
            $userInbox = new UserInbox();
            $userInbox->category = UserInbox::CATEGORY_NORMAL;
            $userInbox->project_id = $projectApplicant->project_id;
            $userInbox->from_user_id = null;
            $userInbox->to_user_id = $projectApplicant->user_id;
            $userInbox->from_staff_id = Auth::guard('staff')->user()->id;
            $userInbox->to_staff_id = null;
            $userInbox->type = UserInbox::TYPE_PROJECT_NEW_MESSAGE;
            $userInbox->is_trash = 0;
            $userInbox->save();

            //add user inbox message
            $userInboxMessage = new UserInboxMessages();
            $userInboxMessage->inbox_id = $userInbox->id;
            $userInboxMessage->from_user_id = null;
            $userInboxMessage->to_user_id = $projectApplicant->user_id;
            $userInboxMessage->from_staff_id = Auth::guard('staff')->user()->id;
            $userInboxMessage->to_staff_id = null;
            $userInboxMessage->project_id = $projectApplicant->project_id;
            $userInboxMessage->is_read = 0;
            $userInboxMessage->type = UserInbox::TYPE_PROJECT_NEW_MESSAGE;

            $message = $request->get('message');
            $userInboxMessage->custom_message = $message;
            $userInboxMessage->quote_price = null;
            $userInboxMessage->pay_type = null;
            $userInboxMessage->file = null;


            if ($userInboxMessage->save()) {

                \DB::commit();
                return makeResponse(trans('common.submit_successful'));

            } else {
                \DB::rollback();
                throw new \Exception(trans('common.err_happen'));
            }
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse($e->getMessage(), true);
        }
    }

    public function info($applicantId)
    {
        $projectApplicant = ProjectApplicant::with('user')->find($applicantId);

        return view('backend.officialproject.applicantInfo', [
            'projectApplicant' => $projectApplicant
        ]);
    }

    public function endContract($applicantId)
    {
        $projectApplicant = ProjectApplicant::with('user')->find($applicantId);

        $reasons = Review::getFreelancerReasonLists();
        $englishProficiencies = Review::getEnglishProficiencyLists();
        $rateCategories = Review::getFreelancerRateCategoryLists();

        $projectSkills = ProjectSkill::where('project_id', $projectApplicant->project->id)
            ->get();

        return view('backend.officialproject.endContract', [
            'projectApplicant' => $projectApplicant,
            'reasons' => $reasons,
            'englishProficiencies' => $englishProficiencies,
            'rateCategories' => $rateCategories,
            'projectSkills' => $projectSkills
        ]);
    }

    public function endContractPost(Request $request)
    {
        $this->validate($request, [
            'reason'=>'required|numeric',
            'recommendation'=>'required|numeric',
            'english_proficiency'=>'required|string',
            'rate_type_' . Review::RATE_SKILL =>'required|numeric',
            'rate_type_' . Review::RATE_QUALITY_OF_WORK =>'required|numeric',
            'rate_type_' . Review::RATE_AVAILABILITY =>'required|numeric',
            'rate_type_' . Review::RATE_ADHERENCE_TO_SCHEDULE =>'required|numeric',
            'rate_type_' . Review::RATE_COMMUNICATION =>'required|numeric',
            'rate_type_' . Review::RATE_COOPERATION =>'required|numeric',
            'total_score'=>'',
            'experience'=> 'required|string',
        ]);

        try {
            DB::beginTransaction();

            $projectApplicantId = $request->input('projectApplicantId');
            $projectId = $request->input('projectId');
            $reason = $request->input('reason');
            $recommendation = $request->input('recommendation');
            $proficiency = $request->input('english_proficiency');
            $skill = $request->input('rate_type_' . Review::RATE_SKILL);
            $quality_of_work = $request->input('rate_type_' . Review::RATE_QUALITY_OF_WORK);
            $availability = $request->input('rate_type_' . Review::RATE_AVAILABILITY);
            $adherence_to_schedule = $request->input('rate_type_' . Review::RATE_ADHERENCE_TO_SCHEDULE);
            $communication = $request->input('rate_type_' . Review::RATE_COMMUNICATION);
            $cooperation = $request->input('rate_type_' . Review::RATE_COOPERATION);
            $total_score = $request->input('total_score');
            $experience = $request->input('experience');
            $reviewSkills = $request->input('review_skills');

            $projectApplicant = ProjectApplicant::find($projectApplicantId);

            //check whether record already exist or not
            $count = Review::where('reviewer_staff_id', Auth::guard('staff')->user()->id)
                ->where('reviewee_user_id', $projectApplicant->user->id)
                ->where('project_id', $projectId)
                ->count();
            if( $count >= 1 )
            {
                return makeResponse(trans('common.only_can_take_one_time_review'), true);
            }

            $review = new Review();
            $review->type = Review::TYPE_FREELANCER_REVIEW;
            $review->from_who_to_who = Review::FWTW_ADMIN_TO_USER;
            $review->reviewer_user_id = null;
            $review->reviewer_staff_id = Auth::guard('staff')->user()->id;
            $review->reviewee_user_id = $projectApplicant->user->id;
            $review->reviewee_staff_id = null;
            $review->project_id = $projectId;
            $review->reason = $reason;
            $review->recommendation = $recommendation;
            $review->proficiency = $proficiency;
            $review->skills = $skill;
            $review->quality_work = $quality_of_work;
            $review->availability = $availability;
            $review->adherence_to_schedule = $adherence_to_schedule;
            $review->communication = $communication;
            $review->cooperation = $cooperation;
            $review->total_score = $total_score;
            $review->experience = $experience;
            $review->save();

            $reviewSkillList = explode(',', $reviewSkills);
            foreach( $reviewSkillList as $skill_id)
            {
                $reviewSkill = new ReviewSkill();
                $reviewSkill->review_id = $review->id;
                $reviewSkill->skill_id = $skill_id;
                $reviewSkill->save();
            }

            $projectApplicant->status = ProjectApplicant::STATUS_COMPLETED;
            $projectApplicant->display_order = ProjectApplicant::DISPLAY_ORDER_COMPLETED;
            $projectApplicant->is_read = ProjectApplicant::IS_READ_NO;
            $projectApplicant->save();

            $ui = new UserInbox();
            $ui->category = UserInbox::CATEGORY_SYSTEM;
            $ui->project_id = $projectId;
            $ui->from_user_id = null;
            $ui->to_user_id = $projectApplicant->user->id;
            $ui->from_staff_id = Auth::guard('staff')->user()->id;
            $ui->to_staff_id = null;
            $ui->type = UserInbox::TYPE_PROJECT_CONFIRMED_COMPLETED;
            $ui->is_trash = 0;
            $ui->save();

            $uim = new UserInboxMessages();
            $uim->inbox_id = $ui->id;
            $uim->from_user_id = null;
            $uim->to_user_id = $projectApplicant->user->id;
            $uim->from_staff_id = Auth::guard('staff')->user()->id;;
            $uim->to_staff_id = null;
            $uim->project_id = $projectId;
            $uim->is_read = 0;
            $uim->type = UserInboxMessages::TYPE_PROJECT_CONFIRMED_COMPLETED;
            $uim->custom_message = null;
            $uim->quote_price = null;
            $uim->pay_type = null;
            $uim->save();

            $project = Project::find($projectId);
            $project->is_read = Project::IS_READ_NO;
            $project->save();

            DB::commit();
            return makeResponse(trans('common.review_submit_success'), false);

        }catch(\Exception $e){

            DB::rollback();
            return makeResponse($e->getMessage(), true);
        }
    }

    public function news($project_id)
    {

        return view('backend.officialproject.news', [
            'project_id' => $project_id
        ]);
    }

    public function newsDt(Request $request, $project_id = null) {
        $model = OfficialProjectNews::where('project_id', $project_id);

        return Datatables::of($model)
            ->editColumn('type', function ($model) {
                return $model->translateType();
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = '';

                $actions .= buildRemoteLinkHtml([
                    'url' => route('backend.officialproject.news.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-pencil"></i>',
                    'modal' => '#remote-modal',
                ]);

                $actions .= buildConfirmationLinkHtml([
                    'url' => route('backend.officialproject.news.delete.post', ['id' => $model->id]),
                    'description' => '<i class="fa fa-trash"></i>',
                ]);

                return $actions;
            })
	          ->rawColumns(['actions'])
            ->make(true);
    }

    public function createNews($project_id)
    {
        $types = OfficialProjectNews::getTypeList();

        return view('backend.officialproject.createNews', [
            'types' => $types,
            'project_id' => $project_id
        ]);
    }

    public function createNewsPost(Request $request)
    {
        try {
            if( $request->get('content') == "" )
            {
                return makeResponse(trans('validation.required', ['attribute' => trans('common.content')]), true);
            }

            $officialProjectNews = new OfficialProjectNews();
            $officialProjectNews->project_id = $request->get('project_id');
            $officialProjectNews->type = $request->get('type');
            $officialProjectNews->content = $request->get('content');
            $officialProjectNews->save();

            return makeResponse(trans('common.success'));
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function editNews(Request $request, $id) {
        $types = OfficialProjectNews::getTypeList();

        $officialProjectNews = OfficialProjectNews::find($id);

        return view('backend.officialproject.editNews', [
            'types' => $types,
            'officialProjectNews' => $officialProjectNews
        ]);
    }

    public function editNewsPost(Request $request, $id) {
        try {
            if( $request->get('content') == "" )
            {
                return makeResponse(trans('validation.required', ['attribute' => trans('common.content')]), true);
            }

            $officialProjectNews = OfficialProjectNews::find($id);
            $officialProjectNews->type = $request->get('type');
            $officialProjectNews->content = $request->get('content');
            $officialProjectNews->save();

            return makeResponse(trans('common.success'));
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function deleteNewsPost(Request $request, $id) {
        try {
            $model = OfficialProjectNews::find($id);
            $model->delete();

            return makeResponse(trans('common.delete_success'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse($e->getMessage(), true);
        }
    }


}
