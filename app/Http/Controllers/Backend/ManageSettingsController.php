<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Settings;

class ManageSettingsController extends Controller
{
    public function index()
    {
        $invitorUserCommision = Settings::where('key','invitor_user_commission')->first();
            if(!$invitorUserCommision){
                Settings::create([
                    'key'=>'invitor_user_commission',
                    'value'=> false
                ]);

            $invitorUserCommision = Settings::where('key','invitor_user_commission')->first();
            }

            $invitedUserCommision = Settings::where('key','invited_user_commission')->first();
            if(!$invitedUserCommision){
                Settings::create([
                    'key'=>'invited_user_commission',
                    'value'=>false
                ]);

            $invitedUserCommision = Settings::where('key','invited_user_commission')->first();
            }

            $hourlyOnOff = Settings::where('key','=',Settings::HOURLY_ON_OFF)->first();
            if(!$hourlyOnOff){
                Settings::create([
                    'key'=>Settings::HOURLY_ON_OFF,
                    'value'=>false
                ]);

                $hourlyOnOff = Settings::where('key','=',Settings::HOURLY_ON_OFF)->first();
            }
            $hourlyOnOff = $hourlyOnOff->value;
            $invitorUserCommision = $invitorUserCommision->value;
            $invitedUserCommision = $invitedUserCommision->value;


        return view('backend.manageSettings.index',compact('invitorUserCommision','invitedUserCommision','hourlyOnOff'));
    }

    public function setInvitorUserCommission(Request $request)
    {
        try{
            $status = $request->status=='true'?true:false;
            $invitedUserCommision = Settings::where('key','invited_user_commission')->first();
            $invitedUserCommision->value = $status;
            $invitedUserCommision->save();
            return collect([
                'status'=> 'success',
                'message'=> 'settings saves successfully'
            ]);
        }catch(\Exception $e){
            $error = $e->getMessage();
            return collect([
                'status' => 'error',
                'message' => 'something went wrong'
            ]);
        }


    }

    public function setInvitedUserCommission(Request $request)
    {

        try{
            $status = $request->status=='true'?true:false;
            $invitorUserCommision = Settings::where('key','invitor_user_commission')->first();
            $invitorUserCommision->value = $status;
            $invitorUserCommision->save();
            return collect([
                'status'=> 'success',
                'message'=> 'settings saves successfully'
            ]);
        }catch(\Exception $e){
            $error = $e->getMessage();
            return collect([
                'status' => 'error',
                'message' => 'something went wrong'
            ]);
        }
    }

    public function setHourlyRate(Request $request){
        try{
            $status = $request->status=='true'?true:false;
            $hourly_on_off = Settings::where('key','=',Settings::HOURLY_ON_OFF)->first();
            $hourly_on_off->value = $status;
            $hourly_on_off->save();
            return collect([
                'status'=> 'success',
                'message'=> 'settings saves successfully'
            ]);
        }catch(\Exception $e){
            $error = $e->getMessage();
            return collect([
                'status' => 'error',
                'message' => 'something went wrong'
            ]);
        }
    }
}
