<?php

namespace App\Http\Controllers\Backend;

use Auth;
use Input;
use Datatables;
use Validator;
use DB;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;
use App\Models\PermissionGroup;
use App\Models\Staff;
use App\Models\PermissionGroupPermission;
use App\Models\SharedOffice;
use App\Http\Requests\Backend\PermissionGroupCreateFormRequest;
use App\Http\Requests\Backend\PermissionGroupEditFormRequest;

class PermissionController extends BackendController
{
    public function index(Request $request) {
        return view('backend.permission.index');
    }

    public function indexDt(Request $request) {
        $user = \Auth::guard('staff')->user();
        $model = PermissionGroup::with(['permissions']);

        if ($user->staff_type === Staff::STAFF_TYPE_STAFF) {
            if ($user->permission_group_id != PermissionGroup::PERMISSION_GROUP_SHARED_OFFICE) {
                return makeResponse('No Permission', true);
            }

            $getOffice = SharedOffice::where('staff_id', '=', $user->id)->first();
            $model = $model->where('office_id', $getOffice->id);
        }

        $model = $model->GetGroups();

        // $model = PermissionGroup::with(['permissions'])->GetGroups();

        return Datatables::of($model)
//            ->editColumn('blabla', function ($model) {
//
//            })
//            ->addColumn('blabla2', function ($model) {
//
//            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = '<a href="' . route('backend.permissions.edit', ['id' => $model->id]) . '" class="btn btn-xs blue"><i class="fa fa-pencil"></i></a>';
                $actions .= buildConfirmationLinkHtml([
                    'url' => route('backend.permissions.delete', ['id' => $model->id]),
                    'description' => '<i class="fa fa-trash"></i>',
                ]);
                return $actions;
            })
	          ->rawColumns(['actions'])
            ->make(true);
    }

    public function create(Request $request) {
        $user = \Auth::guard('staff')->user();
        $ownerPermission = PermissionGroupPermission::getPermissionsLists();

        // check if user is not admin
        if ($user->staff_type === Staff::STAFF_TYPE_STAFF) {
            $ownerPermission = PermissionGroupPermission::getPermissionsListsOwner();
        }

        return view('backend.permission.create', [
            'ownerPermission' => $ownerPermission
        ]);
    }

    public function createPost(PermissionGroupCreateFormRequest $request) {
        $user = \Auth::guard('staff')->user();
        $group = new PermissionGroup();
        $group->group_name = $request->get('group_name');

        if ($user->staff_type === Staff::STAFF_TYPE_STAFF) {
            if ($user->permission_group_id != PermissionGroup::PERMISSION_GROUP_SHARED_OFFICE) {
                return makeResponse('No Permission', true);
            }

            $getOffice = SharedOffice::where('staff_id', '=', $user->id)->first();
            $group->office_id = $getOffice->id;
        }

        $permissions = array();
        foreach ($request->get('permissions') as $key => $var) {
            $p = new PermissionGroupPermission();
            $p->permission_tag = $var;

            $permissions[] = $p;
        }

        try {
            DB::beginTransaction();

            if (count($permissions) < 1) {
                throw new \Exception('请至少选择一个权限');
            }

            $group->save();
            if (!$group->permissions()->saveMany($permissions)) {
                throw new \Exception('当前无法新增权限组，请稍后再试');
            }

            DB::commit();
            addSuccess('成功新增权限组');
            return makeResponse('成功新增权限组');
        } catch (\Exception $e) {
            DB::rollback();
            addError($e->getMessage());
            return makeResponse($e->getMessage(), true);
        }
    }

    public function edit($id) {
        $user = \Auth::guard('staff')->user();
        $model = PermissionGroup::with(['permissions']);
        $ownerPermission = PermissionGroupPermission::getPermissionsLists();

        // check if user is not admin
        if ($user->staff_type === Staff::STAFF_TYPE_STAFF) {
            $getOffice = SharedOffice::where('staff_id', '=', $user->id)->first();
            $model = $model->where('office_id', $getOffice->id);
            $ownerPermission = PermissionGroupPermission::getPermissionsListsOwner();
        }

        $model = $model->where('permission_groups.id', $id)->first();

        // if model not found not allow to edit
        if (is_null($model)) {
            addError('No Permission');
            return redirect()->route("backend.permissions");
        }
        
        // $model = PermissionGroup::with(['permissions'])->find($id);
        $permissions = array();
        foreach ($model->permissions as $key => $var) {
            $permissions[] = $var->permission_tag;
        }

        return view('backend.permission.edit', [
            'model' => $model,
            'permissions' => $permissions,
            'ownerPermission' => $ownerPermission
        ]);
    }

    public function editPost(PermissionGroupEditFormRequest $request, $id) {
        $user = Staff::find(\Auth::guard('staff')->user()->id);
        $group = PermissionGroup::with(['permissions'])->find($id);

        // check if user is not admin
        if ($user->staff_type === Staff::STAFF_TYPE_STAFF) {
            if ($user->permission_group_id != PermissionGroup::PERMISSION_GROUP_SHARED_OFFICE) {
                return makeResponse('No Permission', true);
            }

            $getOffice = SharedOffice::where('staff_id', '=', $user->id)->first();

            // not allowed to edit different office
            if ($group->office_id != $getOffice->id) {
                return makeResponse('No Permission', true);
            }

            $group->office_id = $getOffice->id;
        }

        try {
            DB::beginTransaction();
            $group->group_name = $request->get('group_name');
            $group->save();

            $permissions = array();
            foreach ($request->get('permissions') as $key => $var) {
                $p = new PermissionGroupPermission();
                $p->permission_tag = $var;

                $permissions[] = $p;
            }

            if (count($permissions) < 1) {
                throw new \Exception('请至少选择一个权限');
            }

            if (!$group->permissions()->delete()) {
                throw new \Exception('当前无法新增权限组，请稍后再试');
            }

            if (!$group->permissions()->saveMany($permissions)) {
                throw new \Exception('当前无法新增权限组，请稍后再试');
            }

            DB::commit();
            addSuccess('成功修改权限组');
            return makeResponse('成功修改权限组');
        } catch (\Exception $e) {
            DB::rollback();
            return makeResponse($e->getMessage(), true);
        }
    }

    public function delete($id) {
        $user = Staff::find(\Auth::guard('staff')->user()->id);
        $group = PermissionGroup::with(['permissions'])->find($id);

        // check if user is not admin
        if ($user->staff_type === Staff::STAFF_TYPE_STAFF) {
            if ($user->permission_group_id != PermissionGroup::PERMISSION_GROUP_SHARED_OFFICE) {
                return makeResponse('No Permission', true);
            }

            $getOffice = SharedOffice::where('staff_id', '=', $user->id)->first();

            // not allowed to edit different office
            if ($group->office_id != $getOffice->id) {
                addError('No Permission');
                return redirect()->route("backend.permissions");                
            }
        }

        if (is_null($group)) {
            return makeResponse('No Permission', true);
        }

        try {
            if ($group->delete()) {
                addSuccess('成功删除权限组');
                return makeResponse('成功删除权限组');
            } else {
                throw new \Exception('当前无法删除权限组，请稍后再试');
            }
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }
}
