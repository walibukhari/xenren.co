<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\BackendController;

class ThemesController extends BackendController
{
	public function index() {
        return view('backend.themes.index');
    }
}
