<?php

namespace App\Http\Controllers\Backend;

use App\Models\Bulletin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BulletinController extends Controller
{
	protected $bulletin;
	
	function __construct()
	{
		$this->bulletin = new Bulletin();
	}

	public function index()
	{
		$data = $this->bulletin->getBulletin();
		return view('backend.bulletins.index', [
			'data' => $data
		]);
	}
	
	public function create()
	{
		$data = $this->bulletin->getBulletin();
		return view('backend.bulletins.create', [
			'data' => $data
		]);
	}
	
	public function createBulletin(Request $request)
	{
		$data = $this->bulletin->create($request->all());
		return redirect(route('backend.bulletin'));
	}
	
	public function editBulletin(Request $request, Bulletin $id)
	{
		return view('backend.bulletins.edit', [
			'data' => $id
		]);
	}
	
	public function updateBulletin(Request $request, Bulletin $id)
	{
		$id->update([
			'text' => $request->text,
			'text_cn' => $request->text_cn,
			'status' => $request->status
		]);
		return redirect(route('backend.bulletin'));
	}
	
}
