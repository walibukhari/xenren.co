<?php

namespace App\Http\Controllers\Backend;

use App\Models\SharedOffice;
use App\Models\SharedOfficeSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SharedOfficeSettingsController extends Controller
{
    public function settings(){
        $staff_id = \Auth::guard('staff')->user()->id;
        $settings =  SharedOfficeSetting::where('staff_id','=',$staff_id)->first();
        return view('backend.sharedoffice.setting.index',[
            'settings' => $settings
        ]);
    }

    public function settingBookingPost(Request $request) {
        $staff_id = \Auth::guard('staff')->user()->id;
        $settings =  SharedOfficeSetting::where('staff_id','=',$staff_id)->first();
        if(isset($request->not_allow)) {
            $allow = SharedOffice::MANAGE_BOOKING_NOT_ALLOW;
        } else {
            $allow = SharedOffice::MANAGE_BOOKING_ALLOW;
        }
        if(!is_null($settings)) {
            SharedOfficeSetting::where('staff_id','=',$staff_id)->update([
                'allow' => $allow,
            ]);
            $message = 'settings update successfully';
        } else {
            SharedOfficeSetting::create([
                'staff_id' => $staff_id,
                'allow' => $allow
            ]);
            $message = 'settings saved successfully';
        }
        return back()->with('success_message',$message);
    }

    public function settingPost(Request $request){
        $staff_id = \Auth::guard('staff')->user()->id;
        if($request->book_type == SharedOffice::BOOK_TYPE_PAY_LATER) {
            $bookTypeStatus = SharedOfficeSetting::BOOK_STATUS_PAY_LATER;
        } else {
            $bookTypeStatus = SharedOfficeSetting::BOOK_STATUS_PAY_NOW;
        }
        if($request->cancel_type == SharedOfficeSetting::CANCEL_WITH_CHARGES) {
            $cancelStatusType = SharedOfficeSetting::CANCEL_WITH_CHARGES;
        } else {
            $cancelStatusType = SharedOfficeSetting::CANCEL_WITH_OUT_CHARGES;
        }
        $settings =  SharedOfficeSetting::where('staff_id','=',$staff_id)->first();
        if(!is_null($settings)) {
            SharedOfficeSetting::where('staff_id','=',$staff_id)->update([
                'staff_id' => $staff_id,
                'book_type' => isset($request->book_type) ? $request->book_type : $settings->book_type,
                'cancel_type' => isset($request->cancel_type) ? $request->cancel_type : $settings->cancel_type ,
                'status_book_type' => $bookTypeStatus,
                'status_cancel_type' => $cancelStatusType,
                'discount' => $request->discount
            ]);
            $message = 'settings update successfully';
        } else {
            SharedOfficeSetting::create([
                'staff_id' => $staff_id,
                'book_type' => $request->book_type,
                'cancel_type' => $request->cancel_type,
                'status_book_type' => $bookTypeStatus,
                'status_cancel_type' => $cancelStatusType,
                'discount' => $request->discount
            ]);
            $message = 'settings saved successfully';
        }
        return back()->with('success_message',$message);
    }
}
