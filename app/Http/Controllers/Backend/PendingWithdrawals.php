<?php

namespace App\Http\Controllers\Backend;

use App\Jobs\CheckPayPalWithdrawStatus;
use App\Libraries\PayPal_PHP_SDK\PayPalTrait;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\UserIdentity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PendingWithdrawals extends Controller
{
    use PayPalTrait;

    public function index()
    {
        $data = Transaction::with('sender')
            ->where('status', '=', Transaction::STATUS_IN_PROGRESS_TRANSACTION)
            ->where('type', '=', Transaction::TYPE_WITHDRAW)
            ->paginate(10);
        return view('backend.pendingWithdrals.index', [
            'data' => $data
        ]);
    }

    public function approvePayment(Request $request, Transaction $id)
    {
        $request->request->add([
            'email' => $id->sender->email,
            'amount' => $id->amount
        ]);
        $tD = $this->withdraw($request);
        if ($tD['status'] == 'error') {
            return redirect()->back()->with('error', $tD['message']);

        }
        Transaction::where('id', '=', $id->id)->update([
            'status' => Transaction::STATUS_IN_PROGRESS_APPROVED_BY_ADMIN_TRANSACTION
        ]);
        $td = TransactionDetail::create([
            'transaction_id' => $id->id,
            'payment_method' => TransactionDetail::PAYMENT_METHOD_PAYPAL,
            'raw' => json_encode($tD),
        ]);
        $job = (new CheckPayPalWithdrawStatus(
            $request, $tD, $id->sender->id, $id->id)
        )->delay(60 * 10)->onQueue(config('jobs-queue-names.PayPalWithdrawStatus')); //delayed for 10 mins
        dispatch($job);
        return redirect()->back()->with('success', 'Transaction proceeded');
    }

    public function declinePayment(Request $request, Transaction $id)
    {
        Transaction::where('id', '=', $id->id)->update([
            'status' => Transaction::STATUS_REJECTED
        ]);
        return redirect()->back()->with('success', 'Transaction rejected');
    }
}
