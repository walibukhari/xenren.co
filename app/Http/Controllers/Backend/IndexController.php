<?php

namespace App\Http\Controllers\Backend;

use Auth;
use Illuminate\Http\Request;
use Input;
use Datatables;
use Validator;
use DB;
use Carbon;
use App\Http\Controllers\BackendController;

class IndexController extends BackendController
{

    public function index(Request $request) {
        $token  = $request->token;
        return view('backend.index.index',[
            'token' => $token
        ]);
    }

}
