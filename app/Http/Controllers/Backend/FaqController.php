<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\FaqEditFormRequest;
use App\Models\Faq;
use Auth;
use Input;
use Datatables;
use Validator;
use DB;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;

use App\Http\Requests\Backend\FaqCreateFormRequest;

class FaqController extends BackendController
{
    public function index(Request $request, $id = null)
    {
        return view('backend.faq.index');
    }

    public function support() {
        $field = 'title_' . app()->getLocale();
        $faqs = Faq::where('parent_id', null)->get();
        $array = array();

        $field = 'title_' . app()->getLocale();
        $array[0] = 'select category';
        foreach ($faqs as $faq) {
            $array[$faq->id] = $faq->$field;
            foreach ($faq->children as $nodes) {

                $parent = '';
                foreach ($nodes->getAncestors() as $p) {
                    $parent .= $p->$field . ' - ';
                }
                $array[$nodes->id] = $parent . $nodes->$field;
                foreach ($nodes->children as $node) {

                    $parent = '';
                    foreach ($node->getAncestors() as $p) {
                        $parent .= $p->$field . ' - ';
                    }
                    $array[$node->id] = $parent . $node->$field;
                    foreach ($node->children as $node1) {

                        $parent = '';
                        foreach ($node1->getAncestors() as $p) {
                            $parent .= $p->$field . ' - ';
                        }
                        $array[$node1->id] = $parent . $node1->$field;

                    }
                }

            }

        }

        return view('backend.support.create',
            ['faqList' => $array]);
    }

    public function indexDt(Request $request, $id = null)
    {
        $model = Faq::GetFaq();

        return Datatables::of($model)
//            ->editColumn('blabla', function ($model) {
//
//            })
//            ->addColumn('blabla2', function ($model) {
//
//            })
            ->addColumn('title', function ($model) {
                return $model->getTitle();
            })
            ->addColumn('text', function ($model) {
                return $model->getText();
            })
            ->addColumn('parent', function ($model) {
                return $model->getParent();
            })

            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = '';
                $actions .= buildRemoteLinkHtml([
                    'url' => route('backend.faqs.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-pencil"></i>',
                    'modal' => '#remote-modal-full',
                ]);
                if ($model->deleted_at == null) {
                    $actions .= buildConfirmationLinkHtml([
                        'url' => route('backend.faqs.delete', ['id' => $model->id]),
                        'description' => '<i class="fa fa-trash"></i>',
                        'title' => '确认删除？',
                        'content' => '确认删除此技能？'
                    ]);
                } else {
                    $actions .= buildConfirmationLinkHtml([
                        'url' => route('backend.faqs.restore', ['id' => $model->id]),
                        'description' => '<i class="fa fa-check"></i>',
                        'title' => '确认激活？',
                        'content' => '确认激活此技能？',
                        'color' => 'green',
                    ]);
                }
                return $actions;
            })
	          ->rawColumns(['actions'])
            ->make(true);
    }

    public function create(Request $request)
    {

        $field = 'title_' . app()->getLocale();
        $faqs = Faq::where('parent_id', null)->get();
        $array = array();

        $field = 'title_' . app()->getLocale();
        $array[0] = 'select category';
        foreach ($faqs as $faq) {
            $array[$faq->id] = $faq->$field;
            foreach ($faq->children as $nodes) {

                $parent = '';
                foreach ($nodes->getAncestors() as $p) {
                    $parent .= $p->$field . ' - ';
                }
                $array[$nodes->id] = $parent . $nodes->$field;
                foreach ($nodes->children as $node) {

                    $parent = '';
                    foreach ($node->getAncestors() as $p) {
                        $parent .= $p->$field . ' - ';
                    }
                    $array[$node->id] = $parent . $node->$field;
                    foreach ($node->children as $node1) {

                        $parent = '';
                        foreach ($node1->getAncestors() as $p) {
                            $parent .= $p->$field . ' - ';
                        }
                        $array[$node1->id] = $parent . $node1->$field;

                    }
                }

            }

        }

        return view('backend.faq.create',
            ['faqList' => $array]);
    }

    public function createPost(FaqCreateFormRequest $request, $id = null)
    {
        try {
            $model = new Faq();

            $model->title_cn = $request->get('title_cn');
            $model->title_en = $request->get('title_en');
            $model->text_cn = $request->get('text_cn');
            $model->text_en = $request->get('text_en');
            $model->parent_id = $request->get('parent_id');
            $model->position = $request->get('position');
            $model->type = $request->get('type');

            $model->save();
            return makeResponse('成功新增技能');
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function edit($id)
    {
        $model = Faq::find($id);

        $field = 'title_' . app()->getLocale();
        $faqs = Faq::where('parent_id', null)->get();
        $array = array();

        $field = 'title_' . app()->getLocale();
        $array[0] = 'select category';
        foreach ($faqs as $faq) {
            $array[$faq->id] = $faq->$field;
            foreach ($faq->children as $nodes) {

                $parent = '';
                foreach ($nodes->getAncestors() as $p) {
                    $parent .= $p->$field . ' - ';
                }
                $array[$nodes->id] = $parent . $nodes->$field;
                foreach ($nodes->children as $node) {

                    $parent = '';
                    foreach ($node->getAncestors() as $p) {
                        $parent .= $p->$field . ' - ';
                    }
                    $array[$node->id] = $parent . $node->$field;
                    foreach ($node->children as $node1) {

                        $parent = '';
                        foreach ($node1->getAncestors() as $p) {
                            $parent .= $p->$field . ' - ';
                        }
                        $array[$node1->id] = $parent . $node1->$field;

                    }
                }

            }

        }

        return view('backend.faq.edit',
            ['model' => $model,
                'faqList' => $array
            ]);
    }

    public function editPost(FaqEditFormRequest $request, $id)
    {
        $model = Faq::find($id);

        if (!$model) {
            return makeResponse('技能不存在', true);
        }

        try {
            $model->title_cn = $request->get('title_cn');
            $model->title_en = $request->get('title_en');
            $model->text_cn = $request->get('text_cn');
            $model->text_en = $request->get('text_en');
            $model->parent_id = $request->get('parent_id');
            $model->position = $request->get('position');
            $model->type = $request->get('type');

            $model->save();
            return makeResponse('成功编辑技能');
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function delete($id)
    {
        $model = Faq::find($id);

        try {
            if ($model->delete()) {
                return makeResponse('成功删除技能');
            } else {
                throw new \Exception('未能删除技能，请稍候再试');
            }
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function restore($id)
    {
        $model = Faq::find($id);

        try {
            if ($model->restore()) {
                return makeResponse('成功激活技能');
            } else {
                throw new \Exception('未能激活技能，请稍候再试');
            }
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    function buildTree($array, $parent_id = 0, $level)
    {
        $opts = array();
        foreach ($array as $arr) {
            echo "<option value='$'>";
        }
    }
}
