<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\CountryLanguageCreateFormRequest;
use App\Models\Countries;
use App\Models\CountryLanguage;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class CountryLanguageController extends Controller
{
    public function index(Request $request, $id = null) {

        return view('backend.countrylanguage.index');
    }
    
    public function findExpertsNew(Request $request)
    {
	    return view('frontend.findExperts.indexNew');
    }
    
    
//    public function index(Request $request)
//    {
//    	//for resume user page and redirect back
//	    Session::put('redirect_url', route('frontend.findexperts'));
//	    Session::put('redirect_url_name', trans('common.back_to_find_expert'));
//
//	    $skillList = Skill::getSearchSkillData(Skill::SEARCH_TYPE_WHITE);
//
//	    $jobPositions = JobPosition::GetJobPosition()->get();
//	    $statusList = User::getStatusLists();
//	    $languages = json_encode(CountryLanguage::getCountryLanguages());
//	    $countries = Countries::getCountryLists();
//	    $hourlyRanges = $this::getHourlyRanges();
//
//	    $countriesCode = Countries::getCountryCodeLists();
//
//	    $filter = array();
//	    //        $filter['skill_id'] = null;
//	    $filter['name'] = null;
//	    $filter['skill_id_list'] = null;
//	    $filter['job_position_id'] = null;
//
//    }

    public function indexDt(Request $request, $id = null) {
        $model = CountryLanguage::where('id', '>', 0);

        return Datatables::of($model)
            ->editColumn('country', function ($model) {
                return $model->translateCountry();
            })
            ->editColumn('language', function ($model) {
                return $model->translateLanguage();
            })
            ->filter(function ($model) {
                return $model->GetFilteredResults();
            })
            ->addColumn('actions', function ($model) {
                $actions = '';

                $actions .= buildRemoteLinkHtml([
                    'url' => route('backend.countrylanguage.edit', ['id' => $model->id]),
                    'description' => '<i class="fa fa-pencil"></i>',
                    'color' => 'green btn-modal-' . $model->id,
                    'modal' => '#remote-modal'
                ]);

                $actions .= buildConfirmationLinkHtml([
                    'url' => route('backend.countrylanguage.delete', ['id' => $model->id]),
                    'description' => '<i class="fa fa-trash"></i>',
                    'title' => trans('common.confirm_delete'),
                    'content' => trans('common.confirm_delete_country_language')
                ]);

                return $actions;
            })
	          ->rawColumns(['actions'])
            ->make(true);
    }

    public function create(Request $request) {
        return view('backend.countrylanguage.create');
    }

    public function createPost(CountryLanguageCreateFormRequest $request, $id = null) {


        try {
            $model = new CountryLanguage();
            $model->country_id = $request->get('country_id');
            $model->language_id = $request->get('language_id');

            $model->save();
            return makeResponse(trans('common.add_success'));
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function edit($id) {
        $countryLanguage = CountryLanguage::find($id);

        return view('backend.countrylanguage.edit', [
            'countryLanguage' => $countryLanguage,
        ]);
    }

    public function editPost(Request $request, $id) {
        try{
            $countryLanguage = CountryLanguage::find($id);
            $countryLanguage->country_id = $request->get('country_id');
            $countryLanguage->language_id = $request->get('language_id');
            $countryLanguage->save();

            return makeResponse(trans('common.submit_successful'));
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function delete($id) {
        $model = CountryLanguage::find($id);

        try {
            if ($model->delete()) {
                return makeResponse(trans('common.delete_success'));
            } else {
                throw new \Exception(trans('common.cant_delete_try_later'));
            }
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }
}
