<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\ChatMessageCreateFormRequest;
use App\Http\Requests\Backend\ChatMessageCreateImageFormRequest;
use App\Models\ChatFollower;
use App\Models\ChatMessage;
use App\Models\ChatRoom;
use App\Models\Feedback;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Input;
use Datatables;
use Validator;
use DB;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;

class FeedbackChatController extends BackendController
{
    public function index($feedback_id) {
        $feedback = Feedback::find($feedback_id);

        $chatRoom = ChatRoom::where('feedback_id', $feedback->id)
            ->where('type', ChatRoom::TYPE_FEEDBACKS)
            ->first();

        if(!$chatRoom)
        {
            $chatRoom = new ChatRoom();
            $chatRoom->type = ChatRoom::TYPE_FEEDBACKS;
            $chatRoom->user_id = $feedback->user_id;
            $chatRoom->feedback_id =  $feedback->id;
            $chatRoom->save();
        }

        $chatFollower = ChatFollower::where('staff_id', \Auth::guard('staff')->user()->id)
            ->where('room_id', $chatRoom->id)
            ->first();

        if(!$chatFollower)
        {
            $chatFollower = new ChatFollower();
            $chatFollower->user_id = null;
            $chatFollower->staff_id = \Auth::guard('staff')->user()->id;
            $chatFollower->room_id = $chatRoom->id;
            $chatFollower->save();
        }

        $chatFollowers = ChatFollower::where('room_id', $chatRoom->id)
            ->get();

        return view('backend.feedback.chat', [
            'feedback' => $feedback,
            'chatRoom' => $chatRoom,
            'chatFollowers' => $chatFollowers
        ]);
    }

    public function postChat(ChatMessageCreateFormRequest $request){
        $ChatMessage                    = new ChatMessage();
        $ChatMessage->sender_staff_id   = \Auth::guard('staff')->user()->id;
        $ChatMessage->room_id           = $request->get('chat_room_id');
        $ChatMessage->message           = $request->get('message');

        try {
            $ChatMessage->save();
            $realName = \Auth::guard('staff')->user()->name;
            $avatar = \Auth::guard('staff')->user()->getAvatar();

            return response()->json([
                'status'        => 'OK',
                'message'       => $ChatMessage->message,
                'time'          => date('d M, Y H:i',strtotime($ChatMessage->created_at)),
                'realName'      => $realName,
                'avatar'        => $avatar
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'        => 'not-OK',
                'message'       => $e->getMessage()
            ]);
        }
    }

    public function chatImage($chat_room_id)
    {
        return view('backend.feedback.modalAddFileChat', [
            'chat_room_id' => $chat_room_id
        ]);
    }

    public function chatImagePost(ChatMessageCreateImageFormRequest $request){
        $ChatMessage                    = new ChatMessage();
        $ChatMessage->sender_staff_id    = \Auth::guard('staff')->user()->id;
        $ChatMessage->room_id          = $request->get('chat_room_id');
        $ChatMessage->message           = $request->get('message');
        $ChatMessage->file              = $request->file('file');

        try {
            $ChatMessage->save();

            //get html content for chat row
            $view = View::make('frontend.feedback.chatItem', [
                'message'   => $ChatMessage->message,
                'time'      => date('d M, Y H:i',strtotime($ChatMessage->created_at)),
                'realName'  => \Auth::guard('staff')->user()->name,
                'avatar'    => \Auth::guard('staff')->user()->getAvatar(),
                'file'      => $ChatMessage->getFile()
            ]);
            $contents = $view->render();

            return response()->json([
                'status'    => 'OK',
                'message'   => trans('common.submit_successful'),
                'contents'   => $contents
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status'        => 'not-OK',
                'message'       => $e->getMessage()
            ]);
        }
    }

    public function pullChat($chatRoomId = null){
        $chatLog    = ChatMessage::pullChatMessage($chatRoomId, 'staff');
        return response()->json([
            'status'        => 'OK',
            'chatLog'       => $chatLog['lists']
        ]);
    }
}
