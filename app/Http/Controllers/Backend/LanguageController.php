<?php

namespace App\Http\Controllers\Backend;

use App\Models\Language;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LanguageController extends Controller
{
    //

    public function index()
    {

        $data['languages'] = Language::all();


        return view('backend.language.index', $data);
    }

    public function post(Request $request)
    {

        if ($request->ajax()) {
            $lang = Language::find($request->get('pk'));
            $lang->update([$request->get('name') => $request->get('value')]);

            return response()->json(['success' => true]);
        }
    }
}
