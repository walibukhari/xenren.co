<?php

namespace App\Http\Controllers;

use App\Models\Portfolio;
use App\Models\Project;
use App\Models\Review;
use App\Models\UserIdentity;
use Carbon;
use Illuminate\Http\Request;

use Auth;
use App\Models\User;
use App\Models\UserSkill;
use App\Models\JobPosition;
use App\Models\UserSkillComment;
use App\Models\FavouriteFreelancer;
use Illuminate\Support\Facades\Session;

class ResumeController extends Controller
{
    public function index() {
    }

    public function getDetails($lang, $id) {
        $user = User::where('user_link', $id)->first();
        //if user account already freeze, not need show the info
        if(is_null($user))
        {
            return back();
        } else {
            if($user->is_freeze == 1 )
            {
                return view('errors.404');
            }

            $userSkills = UserSkill::with('skill')->where('user_id', $user->id)->get();
            $jobPosition = JobPosition::GetJobPosition()->where('id', $user->job_position_id)->first();

            $qrImage = $user->getResumeQRCode();
            $link = asset(\Session::get('lang').'/resume/getDetails/'.$id);
            $qr_code = 'https://api.qrserver.com/v1/create-qr-code/?size=100x100&data='.$link;
            $adminReviews = UserSkillComment::with(['files', 'staff', 'skills', 'skills.skill'])->where('user_id', '=', $user->id);

            $userIdentity = UserIdentity::where('user_id', $user->id)
                ->orderBy('id', 'desc')
                ->first();

            $reviews = Review::with('reviewer_user', 'reviewer_staff', 'reviewee_user', 'project')
                ->where('reviewee_user_id', $user->id)
                ->get();

            $job_positions = JobPosition::all();

            $portfolios = Portfolio::with('files')
                ->where('user_id', $user->id)
                ->where('job_position_id', $user->job_position_id)
                ->get();

            $skillArray = array();
            foreach( $userSkills as $userSkill)
            {
                array_push($skillArray, $userSkill->skill->id);
            }

            $similarUsers = User::with('skills')->whereHas('skills', function ($query) use ($skillArray) {
                $query->whereIn('skill_id', $skillArray)->orderBy('experience','desc');
            })
                ->where('id', '!=', $user->id)
                ->take(5)->get();

            $redirectBackUrl = Session::get('redirect_url');
            $redirectBackName = Session::get('redirect_url_name');

            if( $redirectBackUrl == "" )
            {
                $redirectBackUrl = route('frontend.findexperts');
                $redirectBackName = trans('common.back_to_find_expert');
            }

            if( Auth::guard('users')->check()){
                $projects = Project::GetProject()
                    ->where('user_id',  Auth::guard('users')->user()->id )->get();
                $favorite = FavouriteFreelancer::where('user_id', Auth::id())
                    ->where('freelancer_id', $user->id)
                    ->first();
            }else{
                $projects = null;
                $favorite = null;
            }

            $totalScore = 0;
            foreach ($reviews as $review) {
                $totalScore += $review->total_score;
            }

            if ($totalScore > 0 && count($reviews) > 0) {
                $totalScore = $totalScore / count($reviews);
            }
            $name = $jobPosition->name_en;
            return view('frontend.resume.index', [
                'user' => $user,
                'userSkills' => $userSkills,
                'jobPosition' => $jobPosition,
                'qrImage' => $qrImage,
                'qr_code' => $qr_code,
                'projects' => $projects,
                'adminReviews' => $adminReviews->get(),
                'userIdentity' => $userIdentity,
                'reviews' => $reviews,
                'total_score' => $totalScore,
                'favorite' => $favorite,
                'job_positions' => $job_positions,
                'portfolios' => $portfolios,
                'similarUsers' => $similarUsers,
                'redirectBackUrl' => $redirectBackUrl,
                'redirectBackName' => $redirectBackName,
                'name' => $name
            ]);
        }
    }
}
