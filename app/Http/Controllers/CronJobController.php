<?php

namespace App\Http\Controllers;

use App\Models\PortfolioFile;
use App\Models\Project;
use App\Models\ProjectApplicant;
use App\Models\ProjectFile;
use App\Models\UserCheckContactLog;
use Carbon;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class CronJobController extends Controller
{
    public static function deleteInactiveCommonProject()
    {
        //rule:
        //1. only delete inactive project
        //2. only apply to common project
        //3. 30 days after project created and no applicant hired
        //4. include the project attachment remove from server

        Log::info('=== Start Delete Inactive Common Project ===');

        //get date
        $today = Carbon::today();
        $thirtyDaysAgo = $today->subDays(30);

        Log::info('Thirty Days ago : ' . $thirtyDaysAgo->toDateString());

        $projects = Project::where('type', Project::PROJECT_TYPE_COMMON)
            ->where('created_at', '<=', $thirtyDaysAgo)
            ->get();

        foreach( $projects as $project )
        {
            $projectApplicantCount = ProjectApplicant::where('project_id', $project->id )
                ->where('status', '>=', ProjectApplicant::STATUS_CREATOR_SELECTED)
                ->count();

            if( $projectApplicantCount == 0 )
            {
                Log::info('Project ID : ' . $project->id);
                Log::info('Project Name : ' . $project->name);
                Log::info('Created At : ' . $project->created_at);
                Log::info('Total Project Applicant : ' . $projectApplicantCount);
                Log::info('');

                //delete project attachment
                $delete_project_images_paths = array();
                $projectAttachments = ProjectFile::where('project_id', $project->id)
                    ->get();
                foreach( $projectAttachments as $projectAttachment )
                {
                    $delete_project_images_paths[] = $projectAttachment->file;
                    $projectAttachment->delete();
                }

                //delete project attachment
                if ($delete_project_images_paths && count($delete_project_images_paths) > 0) {
                    foreach ($delete_project_images_paths as $var) {
                        @unlink(public_path($var));
                    }
                }

                //delete project attachment directory
                $directory = public_path().'/uploads/project/' . $project->id . '/';
                if( is_dir($directory) )
                {
                    File::deleteDirectory($directory);
                }

                //delete project
                $project->delete();
            }
        }

        Log::info('=== End Delete Inactive Common Project ===');
    }

    public static function deleteObsoleteViewContactLogs()
    {
        //rule:
        //1. only delete the record pass three days

        Log::info('=== Start Delete Obsolete View Contact Logs  ===');

        //get date
        $today = Carbon::today();
        $threeDaysAgo = $today->subDays(3);

        Log::info('Three Days ago : ' . $threeDaysAgo->toDateString());

        $userCheckContactLogs = UserCheckContactLog::where('created_at', '<=', $threeDaysAgo)
            ->get();

        foreach( $userCheckContactLogs as $userCheckContactLog )
        {
            $userCheckContactLog->delete();
        }
        Log::info('=== End Delete Obsolete View Contact Logs ===');
    }

    public static function houseKeeping()
    {
        //project attachment
        $uploadProjectPath = public_path().'/uploads/project';
        $directories = File::directories($uploadProjectPath);
        foreach( $directories as $directory)
        {
            $files = File::allFiles($directory);
            foreach ($files as $file)
            {
                if (strpos($file, 'chat') !== false) {
                    continue;
                }

                $filePathInDB = str_replace(public_path()."/", "", $file->getPathname());
                $filePathInDB = str_replace("\\", "/", $filePathInDB);
                $projectFile = ProjectFile::where('file', $filePathInDB)
                    ->first();
                if(!$projectFile)
                {
                    @unlink($file->getPathname());
                }

            }

            if( count($files) == 0 )
            {
                File::deleteDirectory($directory);
            }

        }

        //portfolio attachment
        $uploadPortfolioPath = public_path().'/uploads/portfolio';
        $directories = File::directories($uploadPortfolioPath);
        foreach( $directories as $directory)
        {
            $files = File::allFiles($directory);
            foreach ($files as $file)
            {
                $filePathInDB = str_replace(public_path()."/", "", $file->getPathname());
                $filePathInDB = str_replace("\\", "/", $filePathInDB);
                $projectFile = PortfolioFile::where('file', $filePathInDB)
                    ->first();
                if(!$projectFile)
                {
//                    @unlink($file->getPathname());
                }

            }

            if( count($files) == 0 )
            {
                File::deleteDirectory($directory);
            }

        }
    }
}