<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests;

class TestUtility extends Controller
{
    public function testMailSend(Request $request){        
        $senderEmail = "support@xenren.co";
        $senderName = "System Admin";
        $email = $receiverEmail = 'printexpert3@gmail.com';
        $sixDigit = '784124';
        //for code display purpose
        $sixDigit = substr($sixDigit, 0, 3) . '-' . substr($sixDigit, 3, 6);
        
        Mail::send('mails.guestConfirmationCodeMobile', array('code' => $sixDigit, 'email' => $email), function ($message) use ($senderEmail, $senderName, $receiverEmail, $sixDigit) {
            $message->from($senderEmail, $senderName);
            $message->to($receiverEmail)
                ->subject(trans('common.confirmation_code_for_xenren', ['code' => $sixDigit]));
        });
        var_dump( Mail:: failures());
        exit;
    }
}
