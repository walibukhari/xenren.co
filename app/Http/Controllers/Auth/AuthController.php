<?php

namespace App\Http\Controllers\Auth;

use App\Models\CountryCities;
use App\Models\GuestRegister;
use App\Models\RegisterCodeCount;
use App\Models\Invitation;
use App\Models\User;
use App\Models\UserContactPrivacy;
use App\Models\UserStatus;
use App\Models\WorldCities;
use App\Models\WorldCountries;
use App\Services\GeneralService;
use App\Traits\CountryAndCityTrait;
use Auth;
use Carbon\Carbon;
use Exception;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Stevebauman\Location\Facades\Location;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Auth\RegisterUserFormRequest;
use App\Http\Requests\Auth\LoginUserFormRequest;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Torann\GeoIP\Facades\GeoIP;


class AuthController extends Controller
{
    use AuthenticatesUsers, CountryAndCityTrait;

    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function register(Request $request)
    {
        $replace = str_replace('136-type-', '', $request->inviteCode);
        $request->session()->put('qr', $replace);
        $inviteCode = $request->get('inviteCode');
        $req = $request->instance();
        $myip = $req->getClientIp();
        $x = geoip($myip);
        $countryName = $x->country;

        //for wechat user invite code after login
        //        Log::useDailyFiles(storage_path().'/logs/weixin.log');
        //        Log::info('=== Start registerPost Log===');
        if ($inviteCode > 0) {
            Session::put('invite_code', $inviteCode);
            //            Log::info('Invite Code Save To Session: ' . $inviteCode );
        }

        return view('auth.register', [
            'inviteCode' => $inviteCode,
            'CountryLocation' => $countryName
        ]);
    }

    public function registerPost(RegisterUserFormRequest $request)
    {
        //	    if(simple_captcha_validate($request->input('CaptchaCode'), $request->input('BDC_VCID_LoginCaptcha')) === false) {
        //		    return makeResponse('Invalid Captcha', true);
        //	    }
        try {
            $country = self::validateCountry(json_decode($request->country_name));

            if ($country->name === false) {
                return makeResponse('Please select proper location from given options', TRUE);
            }

            $city = self::validateCity(json_decode($request->city_name), $country);

            if ($city == false) {
                return makeResponse(trans('validation.select_proper_location'), TRUE);
            }

            $phone_or_email = $request->get('phone_or_email');
            $password = $request->get('password');
            $sixDigit = generateStrongPassword(6, false, 'd');
            $key = generateStrongPassword(32, false, 'lud');
            $link = route('codeconfirmation', ['key' => $key]);
            $email = '';
            $phone_number = '';
            $cityId = $city->id;
            $result = FALSE;
            $passwordHint = $request->input('password');

            //check is register with phone or email
            if (strpos($phone_or_email, '@') !== false) { // is email
                $email = $phone_or_email;
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    return makeResponse(trans('member.not_email'), TRUE);
                }
                //check user exist
                $user_exist = User::where('email', $email)->first();
                if ($user_exist) {
                    return makeResponse(trans('member.unique_email'), TRUE);
                }
                $result = TRUE;
            } else {
                //253 SMS Api
                //check user exist
                $phone_number = $phone_or_email;
                $user_exist = User::where('phone_number', $phone_number)->first();
                if ($user_exist) {
                    return makeResponse(trans('member.unique_phone_number'), TRUE);
                }
                $msg = '[' . $sixDigit . '] 注册验证码，请在8分钟内输入';
                $params = [
                    'account' => env('253_SMS_ACCOUNT'),
                    'password' => env('253_SMS_PASSWORD'),
                    'msg' => urlencode($msg),
                    'phone' => $phone_number,
                    'report' => TRUE
                ];

                $result = $this->curlPost(env('253_SMS_URL') . '/send/json', $params);
                $result = json_decode($result);
                if (!empty($result->errorMsg)) {
                    return makeResponse($result->errorMsg, TRUE);
                } else {
                    $result = TRUE;
                }
            }

            //check register code max > 10 time within 24 hours
            $register_count = RegisterCodeCount::where('phone_or_email', $phone_or_email)->where('status', 1)->first();
            if ($register_count) {
                $code_count = $register_count->count;
                $code_time = $register_count->created_at;
                $max_expried_code_time = date("Y-m-d H:i:s", strtotime($register_count->created_at . ' +1 days'));
                $now = Carbon::now();
                // already pass one day
                if ($max_expried_code_time < $now) {
                    $register_count->status = 0;
                    $register_count->save();
                }
                if ($code_count > 9) {
                    $word = !empty($email) ? '电子邮件' : '电话';
                    return makeResponse('此' . $word . '己注册超过十次, 请在24小时后再注册', TRUE);
                }
            }

            if ($result == TRUE) {

                \DB::beginTransaction();

                //count how many this this email or phone register
                $register_count = RegisterCodeCount::where('phone_or_email', $phone_or_email)->where('status', 1)->first();
                if ($register_count) {
                    $register_count->count += 1;
                    $register_count->save();
                } else {
                    $register_count = new RegisterCodeCount();
                    $register_count->phone_or_email = $phone_or_email;
                    $register_count->count = 1;
                    $register_count->status = 1;
                    $register_count->save();
                }

                $code = session('invite_code');
                $inviteCode = isset($code) ? $code : 0;
                $user = GuestRegister::where('email', $email)->where('status', 1)->first();
                if ($user) {
                    $user_request_time = $user->created_at;
                    $max_expried_date = date("Y-m-d H:i:s", strtotime($user_request_time . ' +8 minutes'));
                    $now = Carbon::now();
                    if ($now > $max_expried_date) {

                        //update the old session
                        $user->status = 0;
                        $user->save();

                        //create new session
                        $guestRegister = new GuestRegister();
                        $guestRegister->email = $email;
                        $guestRegister->phone_number = $phone_number;
                        $guestRegister->password = $password;
                        $guestRegister->code = $sixDigit;
                        $guestRegister->key = $key;
                        $guestRegister->city_id = $cityId;
                        $guestRegister->status = 1; //active
                        $guestRegister->hint = self::getHint($passwordHint);
                        $guestRegister->real_name = $request->name . ' ' . $request->last_name;
                        $guestRegister->refer_by = $inviteCode;
                        $guestRegister->save();

                        //send email
                        if (!empty($email)) {
                            try {
                                $this->sendRegisterEmail($sixDigit, $email, $link);
                            } catch (\Exception $e){

                            }
                        }
                    }
                } else {
                    //create new session
                    $guestRegister = new GuestRegister();
                    $guestRegister->email = $email;
                    $guestRegister->phone_number = $phone_number;
                    $guestRegister->password = $password;
                    $guestRegister->code = $sixDigit;
                    $guestRegister->key = $key;
                    $guestRegister->city_id = $cityId;
                    $guestRegister->status = 1; //active
                    $guestRegister->hint = self::getHint($request->password);
                    $guestRegister->real_name = $request->name . ' ' . $request->last_name;
                    $guestRegister->refer_by = $inviteCode;
                    $guestRegister->save();
                    //send email
                    if (!empty($email)) {
                        try {
                            $this->sendRegisterEmail($sixDigit, $email, $link);
                        } catch (\Exception $e){
                            
                        }
                    }
                }
                \DB::commit();
                $request->request->add([
                    'refer_by' => $inviteCode
                ]);
                $request->session()->put('guest_user_info', [$request->all()]);
                $request->session()->put('hint', [self::getHint($request->password)]);
                return makeResponse(trans('member.successfully_register'));
            }
        } catch (\Exception $e) {
            return makeResponse(array(
                $e->getMessage(),
                $e->getFile(),
                $e->getMessage(),
            ), true);
        }
    }


    private function curlPost($url, $postFields)
    {
        $postFields = json_encode($postFields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json; charset=utf-8'
            )
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $ret = curl_exec($ch);
        if (false == $ret) {
            $result['errorMsg'] = curl_error($ch);
            $result = json_encode($result);
        } else {
            $rsp = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if (200 != $rsp) {
                $result = "请求状态 " . $rsp . " " . curl_error($ch);
            } else {
                $result = $ret;
            }
        }
        curl_close($ch);
        return $result;
    }

    public function sendRegisterEmail($sixDigit, $email, $link)
    {
        //send email
        $senderEmail = "support@xenren.co";
        $senderName = 'Xenren Confirmation Message';
        $receiverEmail = $email;

        //for code display purpose
        $sixDigit = substr($sixDigit, 0, 3) . '-' . substr($sixDigit, 3, 6);
        Mail::send('mails.guestConfirmationCode', array('code' => $sixDigit, 'email' => $email, 'link' => $link), function ($message) use ($senderEmail, $senderName, $receiverEmail, $sixDigit) {
            $message->from($senderEmail, $senderName);
            $message->to($receiverEmail)
                ->subject(trans('common.confirmation_code_for_xenren', ['code' => $sixDigit]));
        }
        );
    }

    public function codeVerification(Request $request)
    {
        $guest_user_info = $request->session()->get('guest_user_info');
        if (!$guest_user_info) {
            return redirect('/register');
        }
        $email_address = $guest_user_info[0]['phone_or_email'];
        return view('auth.codeVerification', [
            'email_address' => $email_address
        ]);
    }

    public function codeVerificationPost(Request $request)
    {
        try {
            $guest_user_info = $request->session()->get('guest_user_info');
            $guestuserHint = $request->session()->get('hint');
            $city = WorldCities::where('id', '=', $guest_user_info[0]['city'])->with('country')->first();
            $code = $request->get('code');
            $phone_or_email = $guest_user_info[0]['phone_or_email'];
            $email = '';
            $phone_number = '';
            if (strpos($phone_or_email, '@') !== false) { // is email
                $email = $phone_or_email;
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    return makeResponse(trans('member.not_email'), TRUE);
                }
            } else {
                $phone_number = $phone_or_email;
            }
            \DB::beginTransaction();
            $guestRegister = GuestRegister::where('email', $email)->where('status', 1)
                ->first();
            if (is_null($guestRegister)) {
                return makeResponse('user not found...!');
            }
            if ($guestRegister->code != (int)$code) {
                return makeResponse(trans('common.wrong_confirmation_code'), true);
            } else {
                $guestRegister->delete();
            }


            $exist = User::where('name', '=', $guest_user_info[0]['name'])->get();
            $newUserLink = GeneralService::cleanTheLink($guest_user_info[0]['name']);
            if (count($exist) > 0) {
                $n = GeneralService::cleanTheLink($guest_user_info[0]['name']);
                $exist = User::where('name', '=', $guest_user_info[0]['name'])->orderBy('id', 'desc')->first();
                $newUserLink = ($n) . '-' . ($exist->id + 2);
            }
            $ip = $request->ip();
            $location = userLocation($ip);
            if ($location['status'] == true) {
                $location = $location['response'];
            }
            $user = new User();
            $user->email = $email;
            $user->phone_number = $phone_number;
            $user->password = ($guest_user_info[0]['password']);
            $user->city_id = isset($city) && isset($city->id) ? $city->id : 0;
            $user->hint = $guestuserHint[0];
            $user->country_id = isset($city) && isset($city->country) && isset($city->country->id) ? $city->country->id : 0;
            $user->name = $guest_user_info[0]['name'] . ' ' . $guest_user_info[0]['last_name'];
            //            $user->name = $guest_user_info[0]['name'];
            $user->upline_id = $guest_user_info[0]['refer_by'];
            $user->user_link = $newUserLink;
            $user->is_freeze = 0;
            $user->uipreference = 0;
            $user->ip_country_code = isset($location) ? $location->country_code : '';
            $user->save();

            $user_status_data = UserStatus::where('user_id', $user->id)->delete();
            $userStatus = new UserStatus();
            $userStatus->user_id = $user->id;
            $userStatus->status_id = 1;
            $userStatus->save();

            $userStatus = new UserStatus();
            $userStatus->user_id = $user->id;
            $userStatus->status_id = 2;
            $userStatus->save();

            $userStatus = new UserStatus();
            $userStatus->user_id = $user->id;
            $userStatus->status_id = 3;
            $userStatus->save();

            UserContactPrivacy::create([
                'user_id' => $user->id,
                //    'qq' => 1,
                'weChat' => 1,
                'skype' => 1,
                'line' => 1,
            ]);

            //normal user
            $invitorId = isset($guest_user_info[0]['inviteCode']) ? $guest_user_info[0]['inviteCode'] : -1;
            if ($invitorId > 0) {
                $invitor = User::find($invitorId);
                $invitation = new Invitation();
                $invitation->invitor_id = $invitor->id;
                $invitation->invitee_id = $user->id;
                $invitation->save();
            }
            \DB::commit();

            $request->session()->forget('guest_user_info');
            addSuccess(trans('member.successfully_register'));
            \Auth::loginUsingId($user->id);
            return makeResponse(trans('member.successfully_register'));
        } catch (\Exception $e) {
            \DB::rollback();
            return makeResponse(array(
                $e->getLine(),
                $e->getFile(),
                $e->getMessage()
            ), true);
        }
    }

    public function login(Request $request)
    {
        $redirectUrl = $request->get('redirect_url');
        // dd($redirectUrl);
        if ($redirectUrl != "") {
            Session::put('redirect_url', $redirectUrl);
        }

        return view('auth.login');
    }

    public function loginPost(Request $request)
    {
        $validator = $request->validate([
            'phone_or_email' => 'required|phone_or_email',
            'pass' => 'required|string|min:6'
        ]);


        $lang = Session::get('lang');
        $password = $request->pass;
        $phone_or_email = $request->phone_or_email;
        $email = '';
        $phone_number = '';
        if (strpos($phone_or_email, '@') !== false) { // is email
            $email = $phone_or_email;
        } else {
            $phone_number = $phone_or_email;
        }

        try {
            if (!empty($email)) {
                $credentials = [
                    'email' => $email,
                    'password' => $password
                ];
            } else {
                $credentials = [
                    'phone_number' => $phone_number,
                    'password' => $password
                ];
            }
            //            $throttles = $this->isUsingThrottlesLoginsTrait();
            //
            //            if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            //                return $this->sendLockoutResponse($request);
            //            }
            if (Auth::guard('users')->attempt($credentials, $request->remember)) {
                $user = Auth::guard('users')->user();
                if ($user->is_freeze == true) {
                    Auth::guard('users')->logout();
                    return makeResponse(trans('common.user_account_freeze'));
                }
                $user->last_login_at = Carbon::now();
                $user->save();
                $ip = $request->ip();
                $location = userLocation($ip);
                if ($location['status'] == true) {
                    $location = $location['response'];
                }
                if ($user->ip_country_code != $location->country_code) {
                    $this->sendEmailIpAlert($user, $location);
                }
                Session::put('previous_page', 'login_page');
                Session::put('lang', $lang);
                return makeResponse(trans('member.successfully_login'));
                //                return $this->handleUserWasAuthenticated($request, $throttles);
            }

            // If the login attempt was unsuccessful we will increment the number of attempts
            // to login and redirect the user back to the login form. Of course, when this
            // user surpasses their maximum number of attempts they will get locked out.
            //            if ($throttles) {
            //                $this->incrementLoginAttempts($request);
            //            }
            return makeResponse(trans('auth.failed'), true);
        } catch (\Exception $e) {
            $e->getMessage();
            return makeResponse($e->getMessage(), true);
        }
    }

    public function sendEmailIpAlert($user, $data)
    {
        $senderEmail = "support@xenren.co";
        $senderName = 'Xenren IP Alert';
        $receiverEmail = $user->email;
        Mail::send('mails.alert_change_country', array('country_name' => $data->country, 'ip' => $data->ip, 'email' => $receiverEmail), function ($message) use ($senderEmail, $senderName, $receiverEmail) {
            $message->from($senderEmail, $senderName);
            $message->to($receiverEmail)
                ->subject('IP Alert Change Country Detect');
        }
        );
    }

    public function logout(Request $request)
    {
        $inviteCode = $request->get('inviteCode');

        Auth::guard('users')->logout();

        if ($inviteCode > 0) {
            return redirect()->route('register', ['inviteCode' => $inviteCode]);
        } else {
            return redirect()->route('home');
        }
    }

    public function authenticated(Request $request, $user)
    {
        addSuccess(trans('member.successfully_login'));
        return makeResponse(trans('member.successfully_login'));
    }

    protected function handleUserWasAuthenticated(Request $request, $throttles)
    {
        if ($throttles) {
            $this->clearLoginAttempts($request);
        }

        return $this->authenticated($request, Auth::guard('users'));
    }

    public function emailVerification($email, $token)
    {
        // $password = base64_decode($token);
        // $sixDigit = generateStrongPassword(6, false, 'd');
        // $key = generateStrongPassword(32, false, 'lud');
        // $link = route('codeconfirmation', ['key' => $key]);

        // $user_exist =  User::where('email', $email)->first();
        // if(!$user_exist){
        //     $user = GuestRegister::where('email', $email)->where('status', 1)->first();
        //     if($user){
        //         $user_request_time = $user->created_at;
        //         $max_expried_date = date("Y-m-d H:i:s",strtotime($user_request_time . ' +30 minutes'));
        //         $now = Carbon::now();
        //         if($now > $max_expried_date){
        //             $user->status = 0;
        //             $user->save();

        //             //send email
        //             $this->sendRegisterEmail($sixDigit, $email, $link);
        //         }
        //     }
        //     else{
        //         $guestRegister = new GuestRegister();
        //         $guestRegister->email = $email;
        //         $guestRegister->password = $password;
        //         $guestRegister->code = $sixDigit;
        //         $guestRegister->key = $key;
        //         $guestRegister->status = 1; //active
        //         $guestRegister->save();

        //         //send email
        //         $this->sendRegisterEmail($sixDigit, $email, $link);
        //     }
        // }
        // return view('auth.modalEmailVerification', [
        //     'email_address' => $email
        // ]);
    }


    public function codeConfirmation($key)
    {
        $questRegister = GuestRegister::where('key', $key)->first();
        if ($questRegister) {
            $email_address = $questRegister->email;

            return view('auth.codeConfirmation', [
                'email_address' => $email_address,
                'key' => $key
            ]);
        }
    }

    public function codeConfirmationPost(Request $request)
    {
        $key = $request->get('key');
        $code = $request->get('code');

        $questRegister = GuestRegister::where('key', $key)->first();
        if ($questRegister) {
            if ($questRegister->code == $code) {
                try {
                    \DB::beginTransaction();
                    $count = User::where('email', $questRegister->email)->count();
                    if ($count >= 1) {
                        return makeResponse(trans('validation.unique', ['attribute' => trans('common.email')]), true);
                    }

                    $user = new User();
                    $user->email = $questRegister->email;
                    $user->password = $questRegister->password;
                    $user->save();

                    $invitorId = $questRegister->invite_code;
                    if ($invitorId > 0) {
                        $invitor = User::find($invitorId);
                        $invitation = new Invitation();
                        $invitation->invitor_id = $invitor->id;
                        $invitation->invitee_id = $user->id;
                        $invitation->save();
                    }

                    $questRegister->delete();

                    \DB::commit();
                    return makeResponse(trans('member.successfully_register'));
                } catch (Exception $e) {
                    \DB::rollback();
                    return makeResponse($e->getMessage(), true);
                }
            } else {
                return makeResponse(trans('common.invalid_confirmation_code'), true);
            }
        }
    }

    public function emailExist(Request $request)
    {
        $user_exist = User::where('email', $request->get('email'))->count();
        if ($user_exist) {
            echo TRUE;
        } else {
            echo FALSE;
        }
    }


    public function getHint($string)
    {
        try {
            $length = strlen($string);
            $co = (int)($length / 2);
            $splittedString = str_split($string, $co);
            return $splittedString[0];
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }
}
