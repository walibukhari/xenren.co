<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\PasswordReset;
use App\Models\User;
use App\Notifications\ResetPasswordEmail;
use Illuminate\Auth\Passwords\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function getHint($string)
    {
        try {
            $length = strlen($string);
            $co = (int)($length / 2);
            $splittedString = str_split($string, $co);
            return $splittedString[0];
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }
	
		public function getReset(Request $request, $token = null)
		{
			return $this->showResetForm($request, $token);
		}
		
		public function postReset(Request $request, $token = null)
		{
			$user = User::where('email', '=', $request->email)->first();
			$validToken = (app(PasswordBroker::class)->tokenExists($user, $request->token));
			if($validToken == false) {
				return redirect()->back()->with('error', 'Token not found....!');
			} else {
				User::where('email', '=', $request->email)->update([
					'password' => bcrypt($request->password),
					'hint' => self::getHint($request->password),
				]);
				return redirect('/')->with('status','password reset successfully');
			}
		}
	
		public function postEmail(Request $request)
		{
			$user = User::where('email', '=', $request->email)->first();
			if(is_null($user)) {
				return redirect()->back()->with('message', 'user not found...!');
			} else {
				$user->notify(new ResetPasswordEmail($user));
				return redirect()->back()->with('message', 'Please check your email for more instructions...!');
			}
		}
  
		/**
		 * Display the form to request a password reset link.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function getEmail()
		{
			return $this->showLinkRequestForm();
		}
		
		/**
		 * Display the form to request a password reset link.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function showLinkRequestForm()
		{
			if (property_exists($this, 'linkRequestView')) {
				return view($this->linkRequestView);
			}
			
			if (view()->exists('auth.passwords.email')) {
				return view('auth.passwords.email');
			}
			
			return view('auth.password');
		}

    protected function resetPassword($user, $password)
    {
        $user->forceFill([
            'password' => $password,
            'hint' => self::getHint($password),
            'remember_token' => Str::random(60),
        ])->save();

        Auth::guard($this->getGuard())->login($user);
    }


    /**
     * Get the e-mail subject line to be used for the reset link email.
     *
     * @return string
     */
    protected function getEmailSubject()
    {
        return trans('common.Your_Password_Reset_Link');
    }

}
