<?php

namespace App\Http\Controllers\Api;

use App\Models\SharedOfficeFinance;
use App\Models\SharedOfficeImages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SpaceController extends Controller
{
    public function mySpaces(Request $request)
    {
        $data = SharedOfficeFinance::with('office')->where('user_id', '=', \Auth::user()->id)
            ->where('scan_type', '!=', SharedOfficeFinance::SCAN_TYPE_HOURLY)
            ->whereHas('office')
            ->get();

        foreach ($data as $k => $v) {
            $dataimages = [];
            $allimages = SharedOfficeImages::where('office_id', '=', $v->office->id)->orderBy('updated_at', 'desc')->limit(3)->get();

            foreach ($allimages as $pic) {
                $dataimages[] = [
                    'name_image' => $pic->compressed_image
                ];
            }
            $v['all_images'] = $allimages;
        }
        return collect([
           'status' => 'success',
           'data'=> $data,
        ]);
    }
}
