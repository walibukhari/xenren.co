<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\TopupRequest;
use App\Jobs\CheckPayPalWithdrawStatus;
use App\Libraries\PayPal_PHP_SDK\PayPalTrait;
use App\Models\TempTransaction;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\User;
use App\Models\UserPaymentMethod;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use RevenueMonster\SDK\Exceptions\ApiException;
use RevenueMonster\SDK\Exceptions\ValidationException;
use RevenueMonster\SDK\Request\QRPay;
use RevenueMonster\SDK\Request\WebPayment;
use RevenueMonster\SDK\RevenueMonster;

class TopupController extends Controller
{
	use PayPalTrait;
	public function topUp(Request $request)
	{
		$postChatObj = new TopupRequest();
		$validator = Validator::make($request->all(), $postChatObj->rules(), $postChatObj->messages());
		if ($validator->fails()) {
			return collect([
				'status' => 'error',
				'message' => $validator->errors()->first()
			]);
		}

		try {
			\DB::beginTransaction();
			$newBalance = Auth::user()->account_balance + $request->amount;
            $transaction = Transaction::create([
				'name' => 'Topup Amount',
				'amount' => $request->amount,
				'milestone_id' => 0,
				'due_date' => Carbon::now()->addYear(1),
				'comment' => $request->amount . ' TopUpped',
				'performed_by_balance_before' => Auth::user()->account_balance,
				'performed_by_balance_after' => $newBalance,
				'performed_to_balance_before' => Auth::user()->account_balance,
				'performed_to_balance_after' => $newBalance,
				'performed_by' => Auth::user()->id,
				'performed_to' => Auth::user()->id,
				'type' => Transaction::TYPE_TOP_UP,
				'currency' => 'USD',
				'status' => Transaction::STATUS_APPROVED_TRANSACTION,
			]);
			TransactionDetail::create([
			    'transaction_id' => $transaction->id,
				'payment_method' => TransactionDetail::PAYMENT_METHOD_PAYPAL,
				'raw' => json_encode($request->payment_method_response)
			]);
			User::where('id', '=', Auth::user()->id)->update([
				'account_balance' => $newBalance
			]);
			\DB::commit();
			return collect([
				'status' => 'success',
				'message' => 'Balance added..!',
                'n_b' => $request->amount
			]);
		}catch (\Exception $e) {
			\DB::rollback();
			return collect([
				'status' => 'failure',
				'message' => $e->getMessage()
			]);
		}
	}

	public function withdrawAmmount(Request $request)
	{
		$data = UserPaymentMethod::where('user_id', '=', \Auth::user()->id)
			->where('payment_method', '=', TransactionDetail::PAYMENT_METHOD_PAYPAL)
			->first();
		if(is_null($data)) {
			return collect([
				'status' => 'failure',
				'message' => 'Please link your paypal to withdraw'
			]);
		}
		$request->request->add([
			'email' => $data->email
		]);
		$tD = $this->withdraw($request);
		\DB::beginTransaction();
		$newBalance = Auth::user()->account_balance - $request->amount;
		$transaction = Transaction::create([
			'name' => 'Amount Withdraw',
			'amount' => $request->amount,
			'milestone_id' => 0,
			'due_date' => Carbon::now()->addDay(10),
			'comment' => 'Amount Withdrawal of ' . $request->amount,
			'balance_before' => Auth::user()->account_balance,
			'balance_after' => $newBalance,
			'performed_by_balance_before' => Auth::user()->account_balance,
			'performed_by_balance_after' => $newBalance,
			'performed_to_balance_before' => Auth::user()->account_balance,
			'performed_to_balance_after' => $newBalance,
			'performed_by' => Auth::user()->id,
			'performed_to' => Auth::user()->id,
			'type' => Transaction::TYPE_WITHDRAW,
			'currency' => 'USD',
			'release_at' => Carbon::now(),
			'status' => Transaction::STATUS_IN_PROGRESS_TRANSACTION
		]);
		$td = TransactionDetail::create([
			'transaction_id' => $transaction->id,
			'payment_method' => TransactionDetail::PAYMENT_METHOD_PAYPAL,
			'raw' => json_encode($tD),
		]);
		User::where([
			'id' => Auth::user()->id
		])->update([
			'account_balance' => $newBalance
		]);
		$job = (new CheckPayPalWithdrawStatus($request->all(), $tD, Auth::user()->id, $transaction->id))/*->delay(60 * 10)*/->onQueue('PayPalWithdrawStatus'); //delayed for 10 mins
		$this->dispatch($job);
		\DB::commit();
		return collect([
		'status' => 'success',
		'message' => 'Amount withdraw requested...!'
		]);
	}



	public function linkPayPal(Request $request)
	{
		$data = UserPaymentMethod::where('user_id' , '=' , Auth::user()->id)->where('payment_method' ,'=', TransactionDetail::PAYMENT_METHOD_PAYPAL)->first();
		if(is_null($data)){
			UserPaymentMethod::create([
				'user_id' => Auth::user()->id,
				'payment_method' => TransactionDetail::PAYMENT_METHOD_PAYPAL,
				'name' => $request->name,
				'email' => $request->email,
				'phone' => $request->phone,
			]);
		} else {
			UserPaymentMethod::where('user_id' , '=' , Auth::user()->id)->where('payment_method' ,'=', TransactionDetail::PAYMENT_METHOD_PAYPAL)
				->update([
					'user_id' => Auth::user()->id,
					'payment_method' => TransactionDetail::PAYMENT_METHOD_PAYPAL,
					'name' => $request->name,
					'email' => $request->email,
					'phone' => $request->phone,
				]);
		}
		return collect([
			'status' => 'success',
			'message' => 'PayPal attached...!'
		]);
	}

	public function generateRandomString($length = 7)
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function topUpRevenueMonster(Request $request)
    {
        $rm = new RevenueMonster([
            'clientId' => '1573107227524768921',
            'clientSecret' => 'olLXugBNyRTgDLOMaMFBalLGBjRYtdOW',
            'privateKey' => file_get_contents(public_path('rm-live.pem')),
            'isSandbox' => false,
        ]);
        $orderId = $this->generateRandomString();
        try {
            $t = Transaction::orderBy('id', 'desc')->first();
            $wp = new WebPayment;
            $wp->order->id = $orderId;
            $wp->order->title = 'Testing Web Payment';
            $wp->order->currencyType = 'MYR';
            $wp->order->amount = $request->amount * 100;
            $wp->order->detail = '';
            $wp->order->additionalData = '';
            $wp->storeId = "1571728609803307378";
            $wp->redirectUrl = 'https://www.xenren.co/revenue-monster/redirect';
            $wp->notifyUrl = 'https://www.xenren.co/revenue-monster/notify';
            $wp->layoutVersion = 'v1';
//            $wp->method = [];
            $response = $rm->payment->createWebPayment($wp);
//            echo $response->checkoutId; // Checkout ID
//            echo $response->url; // Payment gateway url
            $data  = TempTransaction::create([
               'order_id' => $orderId,
               'user_id' => Auth::user()->id,
               'amount' => $request->amount,
            ]);
            return collect([
                'status' => 'success',
                'data' => $response->url
            ]);
        } catch(ApiException $e) {
            echo "statusCode : {$e->getCode()}, errorCode : {$e->getErrorCode()}, errorMessage : {$e->getMessage()}";
        } catch(ValidationException $e) {
            var_dump($e->getMessage());
        }  catch(\Exception $e) {
            echo $e->getMessage();
        }

//        try {
//            $qrPay = new QRPay();
//            $qrPay->currencyType = 'MYR';
//            $qrPay->amount = (100) * ((double)$request->amount);
//            $qrPay->isPreFillAmount = true;
//            $qrPay->order->title = 'TopUp';
//            $qrPay->order->detail = 'Topup For user [' .Auth::user()->name.']';
//            $qrPay->method = [];
//            $qrPay->redirectUrl = 'http://e965bbd1.ngrok.io/revenue-monster/redirect';
//            $qrPay->storeId = '1571728609803307378';
//            $qrPay->type = 'DYNAMIC';
//            $response = $rm->payment->qrPay($qrPay);
//            return collect([
//                'status' => 'success',
//                'data' => $response
//            ]);
//        } catch(ApiException $e) {
//            echo "statusCode : {$e->getCode()}, errorCode : {$e->getErrorCode()}, errorMessage : {$e->getMessage()}";
//        } catch(\Exception $e) {
//            echo $e->getMessage();
//        }
	}
}
