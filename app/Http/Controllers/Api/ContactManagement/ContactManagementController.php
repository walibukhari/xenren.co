<?php

namespace App\Http\Controllers\Api\ContactManagement;

use App\Models\BlockedUser;
use App\Models\SharedOfficeFinance;
use App\Models\User;
use App\Models\UserContactPrivacy;
use App\Models\UserInbox;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use PhpParser\JsonDecoder;

class ContactManagementController extends Controller
{
	public function UsersList($office_id=false)
	{
		try {
		    if($office_id) {
                $data = SharedOfficeFinance::where('office_id', '=', $office_id)
                    ->with('user.skills.skill', 'userStatus')
                    ->has('user')
                    ->get();
            } else {
                $joinedSharedOffices  = SharedOfficeFinance::where('user_id', '=', Auth::user()->id)->select([
                    'id','office_id','user_id'
                ])->get();
                $array = array();
                foreach ($joinedSharedOffices as $k => $v) {
                    array_push($array, $v->office_id);
                }
                $data = SharedOfficeFinance::whereIn('office_id', $array)
                    ->with('user.skills.skill', 'userStatus')
                    ->has('user')
                    ->get();
            }
			$data2 = UserInbox::where('to_user_id', \Auth::user()->id)
				->with(['fromUser.skills.skill', 'fromUser.status'])
				->get();

			$master = collect();
			$data->map(function ($item, $key) use ($master) {
				$v = $item->toArray();
				if (!is_null($v['user'])) {
					$master->push($v['user']);
				}
			});
			$data2->map(function ($item, $key) use ($master) {
				$v = $item->toArray();
				if (!is_null($v['from_user'])) {
					$master->push($v['from_user']);
				}
			});
			$master = $master->unique('id')->values()->all();
			return collect([
				'status' => 'success',
				'data' => $master
			]);
		} catch (\Exception $e) {
			return collect([
				'status' => 'error',
				'message' => $e->getMessage()
			]);
		}
	}

    public function HideContactInfo(Request $request)
    {
        try {
            $this->blockedUser = new BlockedUser();
            if (!isset($request->users) || !isset($request->platform) || !isset($request->all_users)) {
                return collect([
                    'status' => 'error',
                    'message' => 'Invalid data!'
                ]);
            }
            $users = explode(',', $request->users);
            $del = $this->blockedUser->where('from', '=', \Auth::user()->id)->delete();
            foreach ($users as $id) {
                $result = $this->blockedUser->HideContactInfo($request, $id);
            }
            return collect([
                'status' => 'success',
                'message' => $result
            ]);
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage() . " in " . $e->getFile() . " in " . $e->getLine()
            ]);
        }
    }
//
//    public function BlockUserFromChat(Request $request)
//    {
//
//        try {
//            $this->blockedUser = new BlockedUser();
//            $result = $this->blockedUser->BlockUserFromChat($request);
//            return collect([
//                'status' => 'success',
//                'data' => []
//            ]);
//        } catch (\Exception $e) {
//            return collect([
//                'status' => 'error',
//                'message' => $e->getMessage()
//            ]);
//        }
//    }
//
//    public function GetMyBlockedUsers()
//    {
//        try {
//            $this->blockedUser = new BlockedUser();
//            $result = $this->blockedUser->GetMyBlockedUsers();
////            $blockedAll = $this->blockedUser->GetMyBlockedContactInfoForAllUsers();
//
//            $contact_info = collect([
//                'phone' => null,
//                'wechat' => null,
//                'skype' => null,
//                'qq' => null,
//            ]);
//
//            $platform_info = collect([
//                'phone' => null,
//                'wechat' => null,
//                'skype' => null,
//                'qq' => null,
//            ]);
//
//            $result->map(function ($item, $key) use ($contact_info, $platform_info) {
//                if (is_null($contact_info['phone'])) {
//                    if ($item['phone'] == BlockedUser::All_Active) {
//                        $contact_info['phone'] = true;
//                    }
//
//                    if ($item['phone'] == BlockedUser::Platform_Active) {
//                        $platform_info['phone'] = true;
//                    }
//                }
//
//                if (is_null($contact_info['wechat'])) {
//                    if ($item['wechat'] == BlockedUser::All_Active) {
//                        $contact_info['wechat'] = true;
//                    }
//
//                    if ($item['wechat'] == BlockedUser::Platform_Active) {
//                        $platform_info['wechat'] = true;
//                    }
//                }
//
//                if (is_null($contact_info['skype'])) {
//                    if ($item['skype'] == BlockedUser::All_Active) {
//                        $contact_info['skype'] = true;
//                    }
//
//                    if ($item['skype'] == BlockedUser::Platform_Active) {
//                        $platform_info['skype'] = true;
//                    }
//                }
//
//                if (is_null($contact_info['qq'])) {
//                    if ($item['qq'] == BlockedUser::All_Active) {
//                        $contact_info['qq'] = true;
//                    }
//
//                    if ($item['qq'] == BlockedUser::Platform_Active) {
//                        $platform_info['qq'] = true;
//                    }
//                }
//            });
//
//            return collect([
//                'status' => 'success',
//                'data' => collect([
//                    'users' => $result,
//                    'all_contact' => $contact_info,
//                    'platform_info' => $platform_info
//                ])
//            ]);
//        } catch (\Exception $e) {
//            return collect([
//                'status' => 'error',
//                'message' => $e->getMessage() . ' || ' . $e->getLine()
//            ]);
//        }
//    }
//
//    public function xblockUsers(Request $request)
//    {
//        $response = array();
//        try {
//            // TODO: Comment this
//            $user_id = $request->user_id;
//
//            $message = "";
//            BlockedUser::where("from", "=", $user_id)->delete();
//
//            $is_all = $request->get("all");
//            $platform = $request->get("platform");
//            $status = $request->get("status");
//
//            if ($is_all) {
//                $blocked_user = new BlockedUser();
//                $for_block = 2;
//                $for_others = 0;
//
//                switch ($platform) {
//                    case "qq":
//                        $blocked_user->qq = $for_block;
//                        $blocked_user->block_chat = $blocked_user->phone = $blocked_user->wechat = $blocked_user->skype = $for_others;
//                        break;
//
//                    case "skype":
//                        $blocked_user->skype = $for_block;
//                        $blocked_user->block_chat = $blocked_user->phone = $blocked_user->wechat = $blocked_user->qq = $for_others;
//                        break;
//
//                    case "wechat":
//                        $blocked_user->wechat = $for_block;
//                        $blocked_user->block_chat = $blocked_user->phone = $blocked_user->qq = $blocked_user->skype = $for_others;
//                        break;
//
//                    case "phone":
//                        $blocked_user->phone = $for_block;
//                        $blocked_user->block_chat = $blocked_user->qq = $blocked_user->wechat = $blocked_user->skype = $for_others;
//                        break;
//
//                    case "block_chat":
//                        $blocked_user->block_chat = $for_block;
//                        $blocked_user->phone = $blocked_user->qq = $blocked_user->wechat = $blocked_user->skype = $for_others;
//                        break;
//                }
//
//                $blocked_user->from = $user_id;
//                $blocked_user->to = $request->get("user_id");
//                $blocked_user->save();
//
//                $message = "All users for " . $platform . " has been " . ($status ? "blocked" : "unblocked");
//            } else {
//                $users = $request->get("users");
//
//                $users = explode(",", $users);
//
//                for ($a = 0; $a < count($users); $a++) {
//                    $blocked_user = new BlockedUser();
//                    $blocked_user->qq = BlockedUser::Block_Chat_True;
//                    $blocked_user->skype = BlockedUser::Block_Chat_True;
//                    $blocked_user->wechat = BlockedUser::Block_Chat_True;
//                    $blocked_user->phone = BlockedUser::Block_Chat_True;
//                    $blocked_user->block_chat = BlockedUser::Block_Chat_True;
//                    $blocked_user->from = $user_id;
//                    $blocked_user->to = $users[$a];
//                    $blocked_user->save();
//                }
//
//                $message = "Selected users has been blocked";
//            }
//
//            $response["status"] = "success";
//            $response["message"] = $message;
//        } catch (\Exception $e) {
//            $response["status"] = "error";
//            $response["message"] = $e->getMessage() . " in " . $e->getFile() . " in " . $e->getLine();
//        }
//
//        return $response;
//    }

//    public function unblockUsers(Request $request)
//    {
//        $response = array();
//        try {
//            // TODO: Comment this
//	          $user_id = $request->user_id;
//
//            $message = "";
//
//            $is_all = $request->get("all");
//            $platform = $request->get("platform");
//            $status = $request->get("status");
//
//            if ($is_all) {
//                BlockedUser::where("from", "=", $user_id)
//                    ->where($platform, "=", 2)
//                    ->update([$platform => 0]);
//                $message = "All users for " . $platform . " has been " . ($status ? "blocked" : "unblocked");
//            } else {
//                $users = $request->get("users");
//                $users = explode(",", $users);
//
//                for ($a = 0; $a < count($users); $a++) {
//                    BlockedUser::where("from", "=", $user_id)
//                        ->where("to", "=", $users[$a])
//                        ->delete();
//                }
//                $message = "Selected users has been unblocked";
//            }
//            $response["status"] = "success";
//            $response["message"] = $message;
//        } catch (\Exception $e) {
//            $response["status"] = "error";
//            $response["message"] = $e->getMessage() . " in " . $e->getFile() . " in " . $e->getLine();
//        }
//
//        return $response;
//    }
//
//    public function blockSingleUser(Request $request)
//    {
//        try {
//            $this->blockedUser = new BlockedUser();
//            $result = $this->blockedUser->BlockSingleUser($request,$request->user_id);
//
//            return collect([
//                'status' => 'success',
//                'data' => $result
//            ]);
//        } catch (\Exception $e) {
//            return collect([
//                'status' => 'error',
//                'message' => $e->getMessage()
//            ]);
//        }
//    }

//    public function unblockUser(Request $request)
//    {
//        $user = $request->user_id;
//        try {
//
//            BlockedUser::where('to',$user)->delete();
//
//            return collect([
//                'status' => 'User Blocked Success',
////                'data' => $result
//            ]);
//        } catch (\Exception $e) {
//            return collect([
//                'status' => 'error',
//                'message' => $e->getMessage()
//            ]);
//        }
//
//    }

	public function blockUsers(Request $request)
	{

		$users = $request->user_id;
		$result = '';
		try {
				$get = BlockedUser::where('to','=',$users)->first();
				if(is_null($get)){
					$this->blockedUser = new BlockedUser();
					$request->request->add([
					   'qq' => 0,
					   'skype' => 0,
					   'wechat' => 0,
					   'phone' => 0,
					   'block_chat' => 1,
                    ]);
					$result = $this->blockedUser->BlockSingleUser($request,$users);
				}
			return collect([
				'status' => 'success',
                'data' => $result
			]);
		} catch (\Exception $e) {
			return collect([
				'status' => 'error',
				'message' => $e->getMessage()
			]);
		}
	}

	public function unblockUser(Request $request)
	{
		$users = $request->user_id;
		$users = explode(",", $users);

		try {
			foreach ($users as $user) {
				BlockedUser::where('to','=',$user)->where('from','=',\Auth::user()->id)->delete();
			}
			return collect([
				'status' => 'success',
			]);
		} catch (\Exception $e) {
			return collect([
				'status' => 'error',
				'message' => $e->getMessage()
			]);
		}
	}

	public function getBlockedUsers(Request $request)
	{
		$blockedUsers = BlockedUser::where('from', '=', Auth::user()->id)
			->where('is_blocked', '=', 1)
			->with('toUser')
			->select(['id', 'to', 'from'])
			->get();
		return collect([
			'status' => 'success',
			'data' => $blockedUsers
		]);
	}

	public function getPartiallyBlockedUsers(Request $request)
	{
		$blockedUsers = BlockedUser::where('from', '=', Auth::user()->id)
			->where('is_blocked', '=', 0)
			->with('toUser')
			->select(['id', 'to', 'from'])
			->get();
		return collect([
			'status' => 'success',
			'data' => $blockedUsers
		]);
	}

	public function hideContactPlatform(Request $request)
	{

		$users = $request->user_id;
		$users = explode(',', $users);
		$plateForm = json_decode($request->plateForm);

		foreach ($users as $k => $v) {
			$blockedUsers = BlockedUser::where('from', '=' ,Auth::user()->id)->where('to', '=', $v)
				->with('toUser')
				->select(['id', 'to', 'from', 'is_blocked'])
				->first();

			if (!is_null($blockedUsers)) {
				if ((int)$blockedUsers->is_blocked == 1) {
					return collect([
						'status' => 'error',
						'message' => 'User '. $blockedUsers->toUser->name .' already blocked completely'
					]);
				}
			}
		}

		foreach ($users as $k => $v) {
//			BlockedUser::updateOrCreate([
//				'from' => Auth::user()->id,
//				'to' => $v,
//				'qq' => isset($request->qq) ? $request->qq : null,
//				'skype' => isset($request->skype) ? $request->skype : null,
//				'wechat' => isset($request->wechat) ? $request->wechat : null,
//				'phone' => isset($request->phone) ? $request->phone : null,
//				'block_chat' => isset($request->block_chat) ? $request->block_chat : null,
//			], [
//				'from' => Auth::user()->id,
//				'to' => $v,
//			]);
            $record = BlockedUser::where('to', '=', $v)->first();
            \DB::beginTransaction();
            if(!is_null($record)) {
                $record->qq = isset($plateForm->qq) ? $plateForm->qq : null;
                $record->skype = isset($plateForm->skype) ? $plateForm->skype : null;
                $record->wechat = isset($plateForm->wechat) ? $plateForm->wechat : null;
                $record->phone = isset($plateForm->phone) ? $plateForm->phone : null;
                $record->block_chat = isset($plateForm->block_chat) ? $plateForm->block_chat : null;
                $record->save();
            } else {
                $newRecord = new BlockedUser();
                $newRecord->from = Auth::user()->id;
                $newRecord->to = $v;
                $newRecord->qq = isset($plateForm->qq) ? $plateForm->qq : null;
                $newRecord->skype = isset($plateForm->skype) ? $plateForm->skype : null;
                $newRecord->wechat = isset($plateForm->wechat) ? $plateForm->wechat : null;
                $newRecord->phone = isset($plateForm->phone) ? $plateForm->phone : null;
                $newRecord->block_chat = isset($plateForm->block_chat) ? $plateForm->block_chat : null;
                $newRecord->save();
            }
            \DB::commit();
		}

		return collect([
			'status' => 'success',
			'data' => self::getPartiallyBlockedUsers($request),
		]);
	}

    public function hideContactPlatFromAll(Request $request)
    {
        try {
            $plateForm = json_decode($request->plateForm);
            $_privacy = UserContactPrivacy::where('user_id', '=', Auth::user()->id)->first();
            \DB::beginTransaction();
            if (!is_null($_privacy)) {
                $_privacy->qq = isset($plateForm->qq) ? $plateForm->qq : null;
                $_privacy->skype = isset($plateForm->skype) ? $plateForm->skype : null;
                $_privacy->weChat = isset($plateForm->wechat) ? $plateForm->wechat : null;
                $_privacy->phone = isset($plateForm->phone) ? $plateForm->phone : null;
                $_privacy->save();
            } else {
                $_New = new UserContactPrivacy();
                $_New->user_id = Auth::user()->id;
                $_New->qq = isset($plateForm->qq) ? $plateForm->qq : null;
                $_New->skype = isset($plateForm->skype) ? $plateForm->skype : null;
                $_New->weChat = isset($plateForm->wechat) ? $plateForm->wechat : null;
                $_New->phone = isset($plateForm->phone) ? $plateForm->phone : null;
                $_New->save();
            }
            \DB::commit();
            return collect([
                'status' => 'success',
            ]);
        } catch(\Exception $e) {
            \DB::rollback();
            return collect([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
	}

}
