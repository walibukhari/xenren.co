<?php

namespace App\Http\Controllers\Api;

use App\Models\CommunityDiscussion;
use App\Models\Topic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CommunityController extends Controller
{
    public function searchCommunity(Request $request){
        if($request->search) {
            $searchWorld = strtoupper($request->search);
            $searchResults = CommunityDiscussion::where('name', 'LIKE', '%' . $searchWorld . '%')->get();
            $arr = [];
            foreach($searchResults as $result) {
                $locale = app()->getLocale();
                $topicName = $this->getTopic($result['topic_id']);
                $discussionName = str_replace(' ','-',$result['name']);
                array_unshift($arr, collect([
                    'label' =>  $result->name,
                    'icon' => 'fa fa-check setfacolor',
                    'route' => '/'.$locale.'/community/'.$topicName.'/'.$discussionName.'/t-id/'.$result['topic_id'].'/d-id/'.$result['id']
                ]));
            }
            return collect([
                'status' => 'success',
                'query' => $request->search,
                'data' => $arr
            ]);
        }
    }

    public function getTopic($id) {
        $topic = Topic::where('id','=',$id)->first();
        return $topic->topic_name;
    }
}
