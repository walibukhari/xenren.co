<?php

namespace App\Http\Controllers\Api;

use App\Models\ImageModel;
use App\Models\Project;
use App\Models\Settings;
use App\Models\TimeTrack;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\ProjectApplicant;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TimeTrackController extends Controller
{
    public function startSession(Request $request)
    {
        $data = TimeTrack::create([
            'start_time' => Carbon::now()->toDateTimeString(),
            'user_id' => \Auth::user()->id,
            'memo' => $request->memo,
            'project_id' => $request->project_id,
        ]);
        return collect([
            'status' => 'success',
            'data' => $data
        ]);

    }

    public function endSession(Request $request)
    {

        $data = TimeTrack::where('user_id', '=', \Auth::user()->id)
            ->where('end_time', '=', null)->first();
        if (!is_null($data)) {
            $data1 = TimeTrack::where('id', '=', $data->id)
                ->update([
                    'end_Time' => Carbon::now()
                ]);
        }
        return collect([
            'status' => 'success',
            'data' => $data
        ]);

    }

    public function ImagePost(Request $request)
    {
        $currentSession = TimeTrack::with([
            'projectApplicant' => function ($q) {
                $q->where('user_id', '=', Auth::user()->id);
            }
        ])->where('user_id', '=', Auth::user()->id)->where('end_time', '=', null)->first();
        if (isset($request->image) || isset($request->token)) {
            if (is_null($currentSession->projectApplicant->hourly_rate)) {
                return collect([
                    'status' => 'failure',
                    'message' => 'no hourly rate'
                ]);
            }

            ImageModel::create([
                'text' => $request->image,
                'user_time_tracker_id' => is_null($currentSession) ? 0 : $currentSession->id,
                'activity_level' => isset($request->activity_level) ? $request->activity_level : 0,
                'hourly_rate' => $currentSession->projectApplicant->hourly_rate
            ]);
            return collect([
                'status' => 'success'
            ]);
        } else {
            return collect([
                'status' => 'failure',
                'message' => 'no image'
            ]);
        }
    }

    public function removeImage(Request $request)
    {
        //TODO: NEED TO TEST THIS...!
        $imageData = ImageModel::with('trackerDetail')->where('id', '=', $request->id)->first();

        $startTime = Carbon::parse($imageData->trackerDetail->start_time)->toTimeString();
        $startDate = Carbon::parse($imageData->trackerDetail->created_at)->toDateString();
        $endTime = Carbon::parse($imageData->created_at)->subMinute(5)->toTimeString();
        $endDate = Carbon::parse($imageData->trackerDetail->created_at)->toDateString();

        $startDateTime = $startDate . ' ' . $startTime;
        $endDateTime = $endTime . ' ' . $endDate;


        $startTime1 = Carbon::parse($imageData->created_at)->toTimeString();
        $startDate1 = Carbon::parse($imageData->trackerDetail->created_at)->toDateString();
        $endTime1 = isset($imageData->trackerDetail->end_time) ? Carbon::parse($imageData->trackerDetail->end_time)->toTimeString() : null;
        $endDate1 = Carbon::parse($imageData->trackerDetail->created_at)->toDateString();

        $startDateTime1 = $startDate1 . ' ' . $startTime1;
        $endDateTime1 = $endTime1 . ' ' . $endDate1;


        \DB::beginTransaction();
        $t1 = TimeTrack::create([
            'start_time' => $startTime,
            'end_time' => $endTime,
            'memo' => $imageData->trackerDetail->memo,
            'user_id' => $imageData->trackerDetail->user_id,
            'project_id' => $imageData->trackerDetail->project_id,
            'created_at' => $imageData->trackerDetail->created_at,
            'updated_at' => $imageData->trackerDetail->updated_at,
        ]);

        $t2 = TimeTrack::create([
            'start_time' => $startTime1,
            'end_time' => $endTime1,
            'memo' => $imageData->trackerDetail->memo,
            'user_id' => $imageData->trackerDetail->user_id,
            'project_id' => $imageData->trackerDetail->project_id,
            'created_at' => $imageData->trackerDetail->created_at,
            'updated_at' => $imageData->trackerDetail->updated_at,
        ]);

        TimeTrack::where('id', '=', $imageData->trackerDetail->id)->delete();

        ImageModel::where('user_time_tracker_id', '=', $imageData->trackerDetail->id)->update([
            'user_time_tracker_id' => $t2->id
        ]);

        ImageModel::where('id', '=', $request->id)->delete();
        \DB::commit();

        return collect([
            'status' => 'success'
        ]);
    }

    public function checkSession(Request $request)
    {
        $adminSettings = getAdminSetting(Settings::HOURLY_ON_OFF);
        if(isset($adminSettings) && $adminSettings->value == Project::PAY_TYPE_HOURLY_PAY) {
            $projects = ProjectApplicant::with(['project' => function($q){
                $q->where('pay_type' ,'!=',Project::PAY_TYPE_HOURLY_PAY);
            }])
                ->whereHas('project')
                ->where('user_id', '=', Auth::user()->id)->get();
        } else {
            $projects = ProjectApplicant::with('project')
                ->whereHas('project')
                ->where('user_id', '=', Auth::user()->id)->get();
        }
        $data = TimeTrack::where('user_id', '=', \Auth::user()->id)
            ->where('end_time', '=', null)->first();

        if (is_null($request->project_id)) {
            $data1 = TimeTrack::where('user_id', '=', \Auth::user()->id)
                ->whereBetween('created_at', [Carbon::today()->startOfDay(), Carbon::today()->endOfDay()])
                ->get();
            $record = [];
            $totalTimeToday = 0;
        } else {
            $data1 = TimeTrack::where('user_id', '=', \Auth::user()->id)
                ->where('project_id', $request->project_id)
                ->whereBetween('created_at', [Carbon::today()->startOfDay(), Carbon::today()->endOfDay()])
                ->get();
            $record = TimeTrack::select(\DB::raw("project_id, DATE_FORMAT(created_at, '%Y-%m-%d') as created_at, SUM(TIMESTAMPDIFF(SECOND, CONCAT(DATE_FORMAT(created_at, '%Y-%m-%d'), ' ', start_time), CONCAT(DATE_FORMAT(created_at, '%Y-%m-%d'), ' ', end_time))) as time_lapse, ( SELECT child.memo as memo FROM `user_time_track` child WHERE child.project_id = user_time_track.project_id and DATE_FORMAT(user_time_track.created_at, '%Y-%m-%d') = DATE_FORMAT(child.created_at, '%Y-%m-%d') and child.memo IS NOT NULL ORDER BY child.created_at DESC LIMIT 1 ) as memo"))
            ->where('user_id', '=', \Auth::user()->id)
            ->where('project_id', '=', $request->project_id)
            ->orderBy('id', 'desc')
            ->with('project')
            ->limit(6)
            ->groupBy(\DB::raw("project_id, DATE_FORMAT(created_at, '%Y-%m-%d')"))
            ->get();

            if (!is_null($data)) {
                $totalTimeToday = $data1->sum('total_time_elapsed') + Carbon::parse($data->start_time)->diffInSeconds(Carbon::now());
            } else {
                $totalTimeToday = $data1->sum('total_time_elapsed');
            }
        }

        $currentSession = !is_null($data) ? true : false;
//        foreach ($data1 as $k => $v) {
//            $startTime = isset($v->start_time) ? \Carbon\Carbon::parse($v->start_time) : \Carbon\Carbon::now();
//            $endTime = isset($v->end_times) ? \Carbon\Carbon::parse($v->end_time) : \Carbon\Carbon::now();
//            $totalTimeDiff = $endTime->diffInSeconds($startTime);
//            $totalTimeToday += $totalTimeDiff;
//        }
        $totalTime = 0;

        return collect([
            'status' => 'success',
            'projects' => $projects,
            'record' => $record,
            'user' => Auth::user(),
            'memos' => TimeTrack::where('user_id', '=', Auth::user()->id)->with('project')->orderBy('id', 'desc')->limit(2)->get(),
            'current_session' => collect([
                'is_active' => $currentSession,
                'current_session_time' => $totalTimeToday, //today
                'detail' => $data,
                'current_time' => Carbon::now()->toTimeString()
            ]),
            'current_date' => collect([
                'total_time' => $totalTime,
            ]),
            'data1'=> $data1
        ]);
    }

}
