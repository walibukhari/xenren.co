<?php

namespace App\Http\Controllers\Api\Home;

use App\Http\Controllers\Frontend\FavouriteJobController;
use App\Models\CountryLanguage;
use App\Models\FavouriteFreelancer;
use App\Models\FavouriteJob;
use App\Models\JobPosition;
use App\Models\User;
use App\Models\UserInbox;
use App\Models\WorldCountries;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Constants;
use App\Models\Category;
use App\Models\Countries;
use App\Models\ExchangeRate;
use App\Models\Language;
use App\Models\OfficialProject;
use App\Models\Skill;
use App\Models\UserSkill;
use Carbon;
use Auth;
use App\Models\Project;
use App\Models\ProjectApplicant;
use App\Models\ProjectApplicantAnswer;
use App\Models\ProjectChatRoom;
use App\Models\ProjectChatFollower;
use App\Models\ThumbdownJob;
use App\Models\UserInboxMessages;
use App\Http\Requests\Frontend\JobApplyFormRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class HomeApiController extends Controller
{

    function __construct()
    {
        App::setLocale("en");
    }

    public function search(Request $request)
    {
        try {
            $response = array();

            $projectType = $request->get('project_type');
            if ($projectType == "") {
                $projectType = Project::PROJECT_TYPE_COMMON;
            }

            $filter = array();
            $filter['project_type'] = $projectType;
            $filter['type'] = null;
            $filter['order'] = null;
            $filter['country'] = isset($request->country) && $request->country != '' ? $request->country : null;
            $filter['country_name'] = null;
            $filter['language'] = isset($request->language) && $request->language != '' ? $request->language : null;
            $filter['language_name'] = null;
            $filter['hourly_range'] = isset($request->hourly_range) && $request->hourly_range != '' ? $request->hourly_range : null;
            $filter['hourly_range_name'] = null;
            $filter['advance_filter'] = null;
            $filter['skill_id_list'] = null;

            //Check this only if user is tailor
            if (!$request->has('type') || $request->get('type') == 0) {
                //Only show order that match user skills
            } else {
                $filter['type'] = $request->get('type');
            }

            //recommend job only can work when user login, else will work like search all
            //user need to login to get the user skills
            if (!Auth::guard('users')->check()) {
                $skills = Skill::all();
                $skillArray = [];
                foreach ($skills as $skill) {
                    array_push($skillArray, $skill->id);
                }
            } else {
                if ($request->get('type') == 1 || $request->get('type') == null) {
                    $skills = Skill::all();
                    $skillArray = [];
                    foreach ($skills as $skill) {
                        array_push($skillArray, $skill->id);
                    }
                } else {
                    $userId = Auth::guard('users')->user()->id;
                    $userSkills = UserSkill::where('user_id', $userId)->get();
                    $skillArray = [];
                    foreach ($userSkills as $userSkill) {
                        array_push($skillArray, $userSkill->skill_id);
                    }
                }

            }

            $countries = Countries::getCountryLists();
            $languages = Language::getLanguageLists();
            $hourlyRanges = Constants::getHourlyRange();

            if ($projectType == Project::PROJECT_TYPE_OFFICIAL) {
                $keywords = Project::getOfficialProjectKeywords();

                //only allow white skill
                $skillList = Skill::getSearchSkillData(Skill::SEARCH_TYPE_GOLD);

                // $officialProjects = OfficialProject::with(['projectPositions', 'projectImages', 'project', 'project.projectSkills']);

                if (Auth::guard('users')->check()) {

                    $thumbdownJobId = [];
                    $thumbdownJobList = ThumbdownJob::select('project_id')
                        ->where('user_id', \Auth::guard('users')->user()->id)
                        ->where('project_type', $projectType)
                        ->get();

                    foreach ($thumbdownJobList as $key) {
                        array_push($thumbdownJobId, $key->project_id);
                    }

                    $officialProjects = OfficialProject::with(['projectPositions', 'projectImages', 'project', 'project.projectSkills.skill'])
                        ->whereNotIn('id', $thumbdownJobId);

                } else {
                    $officialProjects = OfficialProject::with(['projectPositions', 'projectImages', 'project', 'project.projectSkills.skill']);
                }

                if (empty($request->get('type')) || $request->get('type') == 1) {
                    //Only show jobs that match user skills
                    $officialProjects->whereHas('project.projectSkills', function ($query) use ($skillArray) {
                        $query->whereIn('skill_id', $skillArray);
                    });
                    $filter['type'] = 1;

                } else {
                    $filter['type'] = $request->get('type');
                }

                if ($request->has('order') && $request->get('order') != '') {
                    switch ($request->get('order')) {
                        case 1:
                            $filter['order'] = 1;
                            $officialProjects->orderBy('created_at', 'ASC');
                            break;
                        case 2:
                            $filter['order'] = 2;
                            $officialProjects->orderBy('created_at', 'DESC');
                            break;
                        case 3:
                            $filter['order'] = 3;
                            $officialProjects->orderBy('stock_total', 'ASC');
                            break;
                        case 4:
                            $filter['order'] = 4;
                            $officialProjects->orderBy('stock_total', 'DESC');
                            break;
                    }
                } else {
                    $officialProjects->orderBy('created_at', 'DESC');
                }

                if ($request->has('advance_filter') && $request->get('advance_filter') != '') {
                    $filter['advance_filter'] = $request->get('advance_filter');
                }

                if ($request->has('skill_id_list') && $request->get('skill_id_list') != '') {
                    $advanceSearchSkill = explode(',', $request->get('skill_id_list'));

                    $officialProjects->whereHas('project.projectSkills', function ($query) use ($advanceSearchSkill) {
                        $query->whereIn('skill_id', $advanceSearchSkill);
                    });

                    $filter['skill_id_list'] = $request->get('skill_id_list');
                }

                $type_url = route('frontend.jobs.index') . '?project_type=' . $projectType . '&';
                $order_url = route('frontend.jobs.index') . '?project_type=' . $projectType . '&';
                $advance_url = route('frontend.jobs.index') . '?project_type=' . $projectType . '&';

                if ($filter['order'] != null) {
                    $type_url .= 'order=' . $filter['order'] . '&';
                    $advance_url .= 'order=' . $filter['order'] . '&';
                } else {
                    $advance_url .= 'order=1&';
                }

                if ($filter['type'] != null) {
                    $order_url .= 'type=' . $filter['type'] . '&';
                    $advance_url .= 'type=' . $filter['type'] . '&';
                } else {
                    $advance_url .= 'type=1&';
                }

                if ($filter['advance_filter'] != null) {
                    $type_url .= 'advance_filter=' . $filter['advance_filter'] . '&';
                    $order_url .= 'advance_filter=' . $filter['advance_filter'] . '&';
                }

                $advance_filter_json = array();
                //Build the advance filter json and add the query
                if ($filter['advance_filter'] != null) {
                    $filters = explode(',', $filter['advance_filter']);
                    foreach ($filters as $f) {
                        if ($f != "") {
                            list($type, $value) = explode('|', $f);
                            switch ($type) {
                                case 1:
                                    $typeName = trans('common.country');
                                    $valueText = Countries::find($value)->getName();
                                    $officialProjects = $officialProjects->whereHas('project', function ($query) use ($value) {
                                        $query->where('country_id', $value);
                                    });
                                    $filter['country'] = $value;
                                    $filter['country_name'] = $valueText;
                                    break;
                                case 2:
                                    $typeName = trans('common.language_2');
                                    $valueText = Language::find($value)->getName();
                                    $officialProjects = $officialProjects->whereHas('project', function ($query) use ($value) {
                                        $query->where('language_id', $value);
                                    });
                                    $filter['language'] = $value;
                                    $filter['language_name'] = $valueText;
                                    break;
                            }
                            $advance_filter_json[] = array(
                                'type' => $type,
                                'value' => $value,
                                'text' => "<b>" . $typeName . '</b> : ' . $valueText
                            );
                        }
                    }
                }

                $totalCount = $officialProjects->count();
                $officialProjects = $officialProjects->get();

                $response['data'] = array('officialProjects' => $officialProjects,
                    'totalCount' => $totalCount,
                    'projectType' => $projectType,
                    'filter' => $filter,
                    'type_url' => $type_url,
                    'order_url' => $order_url,
                    'advance_url' => $advance_url,
                    'keywords' => $keywords,
                    'advance_filter_json' => $advance_filter_json,
                    'countries' => $countries,
                    'languages' => $languages,
                    'hourlyRanges' => $hourlyRanges,
                    'skillList' => json_decode($skillList));
            } else if ($projectType == Project::PROJECT_TYPE_COMMON) {
                $keywords = Project::getCommonProjectKeywords();

                //only allow white skill
                $skillList = Skill::getSearchSkillData(Skill::SEARCH_TYPE_WHITE);

                // $commonProjects = Project::with(['creator', 'projectQuestions', 'projectSkills', 'projectCountries', 'projectLanguages'])
                //     ->where('type', Project::PROJECT_TYPE_COMMON)
                //     ->where('freelance_type', '!=', Project::FREELANCER_TYPE_INVITED_USER);

                if (Auth::guard('users')->check()) {

                    $thumbdownJobId = [];
                    $thumbdownJobList = ThumbdownJob::select('project_id')
                        ->where('user_id', \Auth::guard('users')->user()->id)
                        ->where('project_type', $projectType)
                        ->get();

                    foreach ($thumbdownJobList as $key) {
                        array_push($thumbdownJobId, $key->project_id);
                    }

                    $commonProjects = Project::with([
	                    'creator',
	                    'projectQuestions',
	                    'projectSkills.skill',
	                    'projectCountries',
	                    'projectLanguages',
	                    'projectFiles',
	                    'thumbDown' => function($q){
		                    if(\Auth::user()) {
			                    $q->where('user_id', '=', \Auth::user()->id);
		                    }
	                    },
	                    'favoriteJob' => function($q){
		                    if(\Auth::user()) {
			                    $q->where('user_id', '=', \Auth::user()->id);
		                    }
	                    }])
                        ->where('type', Project::PROJECT_TYPE_COMMON)
                        ->where('freelance_type', '!=', Project::FREELANCER_TYPE_INVITED_USER)
                        ->whereNotIn('id', $thumbdownJobId)
                        ->whereIn('status', [
                            Project::STATUS_PUBLISHED,
                            Project::STATUS_HIRED,
                            Project::STATUS_SELECTED,
                        ]);

                } else {
					        
	                $commonProjects = Project::with([
		                'creator',
		                'projectQuestions',
		                'projectSkills.skill',
		                'projectCountries',
		                'projectLanguages',
		                'projectFiles',
		                'thumbDown' => function($q){
			                if(\Auth::user()) {
				                $q->where('user_id', '=', \Auth::user()->id);
			                }
		                },
		                'favoriteJob' => function($q){
	                	  if(\Auth::user()) {
			                  $q->where('user_id', '=', \Auth::user()->id);
		                  }
		                }])
		                ->where('type', Project::PROJECT_TYPE_COMMON)
		                ->where('freelance_type', '!=', Project::FREELANCER_TYPE_INVITED_USER)
		                ->whereIn('status', [
			                Project::STATUS_PUBLISHED,
			                Project::STATUS_HIRED,
			                Project::STATUS_SELECTED,
		                ]);
                    // ->whereNotIn('id', $thumbdownJobId)
                }

                //not need show the project creator who account being freeze
                $commonProjects->whereHas('creator', function ($query) use ($skillArray) {
                    $query->where('is_freeze', 0);
                });

                switch ($request->get('type')) {
                    case 1:
                        $filter['type'] = $request->get('type');
                        break;
                    case 2:
                        //Only show jobs that match user skills
                        $commonProjects->whereHas('projectSkills', function ($query) use ($skillArray) {
                            $query->whereIn('skill_id', $skillArray);
                        });
                        break;
                    case 3:
                        //fixed price project
                        $commonProjects->where('pay_type', Project::PAY_TYPE_FIXED_PRICE);
                        break;
                    case 4:
                        //hourly price project
                        $commonProjects->where('pay_type', Project::PAY_TYPE_HOURLY_PAY);
                        break;
                    default:
                        $commonProjects->whereHas('projectSkills', function ($query) use ($skillArray) {
                            $query->whereIn('skill_id', $skillArray);
                        });
                        $filter['type'] = 1;
                        break;
                }

                if ($request->has('order') && $request->get('order') != '') {
                    switch ($request->get('order')) {
                        case 1:
                            $filter['order'] = 1;
                            $commonProjects->orderBy('created_at', 'DESC');
                            break;
                        case 2:
                            $filter['order'] = 2;
                            $commonProjects->orderBy('created_at', 'ASC');
                            break;
                        case 5:
                            $filter['order'] = 5;
                            $commonProjects->orderBy('reference_price', 'ASC');
                            break;
                        case 6:
                            $filter['order'] = 6;
                            $commonProjects->orderBy('reference_price', 'DESC');
                            break;
                    }
                } else {
                    $commonProjects->orderBy('created_at', 'DESC');
                }

                if ($request->has('advance_filter') && $request->get('advance_filter') != '') {
                    $filter['advance_filter'] = $request->get('advance_filter');
                }

                if ($request->has('skill_id_list') && $request->get('skill_id_list') != '') {
                    $advanceSearchSkill = explode(',', $request->get('skill_id_list'));

                    $commonProjects->whereHas('projectSkills', function ($query) use ($advanceSearchSkill) {
                        $query->whereIn('skill_id', $advanceSearchSkill);
                    });

                    $filter['skill_id_list'] = $request->get('skill_id_list');
                }

                $type_url = route('frontend.jobs.index') . '?project_type=' . $projectType . '&';
                $order_url = route('frontend.jobs.index') . '?project_type=' . $projectType . '&';
                $advance_url = route('frontend.jobs.index') . '?project_type=' . $projectType . '&';

                if ($filter['order'] != null) {
                    $type_url .= 'order=' . $filter['order'] . '&';
                    $advance_url .= 'order=' . $filter['order'] . '&';
                } else {
                    $advance_url .= 'order=1&';
                }

                if ($filter['type'] != null) {
                    $order_url .= 'type=' . $filter['type'] . '&';
                    $advance_url .= 'type=' . $filter['type'] . '&';
                } else {
                    $advance_url .= 'type=1&';
                }

                if ($filter['advance_filter'] != null) {
                    $type_url .= 'advance_filter=' . $filter['advance_filter'] . '&';
                    $order_url .= 'advance_filter=' . $filter['advance_filter'] . '&';
                }

                if ($filter['skill_id_list'] != null) {
                    $type_url .= 'skill_id_list=' . $filter['skill_id_list'] . '&';
                    $order_url .= 'skill_id_list=' . $filter['skill_id_list'] . '&';
                }

                $advance_filter_json = array();
                //Build the advance filter json and add the query
                if ($filter['advance_filter'] != null) {
                    $filters = explode(',', $filter['advance_filter']);
                    foreach ($filters as $f) {
                        if ($f != "") {
                            list($type, $value) = explode('|', $f);
                            switch ($type) {
                                case 1:
                                    $typeName = trans('common.country');
                                    $valueText = Countries::find($value)->getName();
//                                $commonProjects = $commonProjects->where('country_id', $value);
                                    $commonProjects = $commonProjects->whereHas('projectCountries', function ($query) use ($value) {
                                        $query->where('country_id', $value);
                                    });
                                    $filter['country'] = $value;
                                    $filter['country_name'] = $valueText;
                                    break;
                                case 2:
                                    $typeName = trans('common.language_2');
                                    $valueText = Language::find($value)->getName();
//                                $commonProjects = $commonProjects->where('language_id', $value);
                                    $commonProjects = $commonProjects->whereHas('projectLanguages', function ($query) use ($value) {
                                        $query->where('language_id', $value);
                                    });
                                    $filter['language'] = $value;
                                    $filter['language_name'] = $valueText;
                                    break;
                                case 3:
                                    $typeName = trans('common.hourly_range');
                                    $valueText = Project::translateHourlyRange($value);
                                    $commonProjects = $commonProjects->where('pay_type', Project::PAY_TYPE_HOURLY_PAY);

                                    $startValue = 0;
                                    $endValue = 0;

                                    switch ($value) {
                                        case 1:
                                            $startValue = 1;
                                            $endValue = 10;
                                            break;
                                        case 2:
                                            $startValue = 11;
                                            $endValue = 20;
                                            break;
                                        case 3:
                                            $startValue = 21;
                                            $endValue = 30;
                                            break;
                                        case 4:
                                            $startValue = 31;
                                            $endValue = 40;
                                            break;
                                        case 5:
                                            $startValue = 41;
                                            $endValue = 50;
                                            break;
                                        case 6:
                                            $startValue = 51;
                                            $endValue = 20000;
                                            break;
                                    }

                                    if (app()->getLocale() == "cn") {
                                        $exchangeRate = ExchangeRate::getExchangeRate(Constants::CNY, Constants::USD);
                                        $startValue = $startValue * $exchangeRate;
                                        $endValue = $endValue * $exchangeRate;
                                    }

                                    $commonProjects = $commonProjects->whereBetween('reference_price', array($startValue, $endValue));
                                    $filter['hourly_range'] = $value;
                                    $filter['hourly_range_name'] = $valueText;
                            }
                            $advance_filter_json[] = array(
                                'type' => $type,
                                'value' => $value,
                                'text' => "<b>" . $typeName . '</b> : ' . $valueText
                            );
                        }
                    }
                }

                $totalCount = $commonProjects->count();
                $commonProjects = $commonProjects->get();

                for ($a = 0; $a < count($commonProjects); $a++) {
                    $commonProjects[$a]["description"] = strip_tags($commonProjects[$a]["description"]);
                    if (!Project::isMyFavouriteJob($commonProjects[$a]["id"]) && !Project::isMyThumbdownJob($commonProjects[$a]["id"])) {
                        $commonProjects[$a]["job_status"] = Project::JOB_NONE;
                    } else if (Project::isMyFavouriteJob($commonProjects[$a]["id"])) {
                        $commonProjects[$a]["job_status"] = Project::JOB_LIKED;
                    } else if (Project::isMyThumbdownJob($commonProjects[$a]["id"])) {
                        $commonProjects[$a]["job_status"] = Project::JOB_DISLIKED;
                    }
                }

                $response['data'] = array('commonProjects' => $commonProjects,
                    'totalCount' => $totalCount,
                    'projectType' => $projectType,
                    'filter' => $filter,
                    'type_url' => $type_url,
                    'order_url' => $order_url,
                    'advance_url' => $advance_url,
                    'keywords' => $keywords,
                    'advance_filter_json' => $advance_filter_json,
                    'countries' => $countries,
                    'languages' => $languages,
                    'hourlyRanges' => $hourlyRanges,
                    'skillList' => json_decode($skillList));
            }

            $response['status'] = 'success';
        } catch (\Exception $e) {
            $response["status"] = "error";
            $response["message"] = $e->getMessage() . " in " . $e->getFile() . " in " . $e->getLine();
        }
        return $response;
    }

    public function makeOfferPost(Request $request)
    {
        $response = array();
        $response['msg'] = '';
        $response['status'] = '';

        try {
            $userId = \Auth::guard('users')->user()->id;
            $projectId = $request->get('project_id');
            $inboxId = $request->get('inbox_id');

            $about_me = $request->get('about_me');

            $partOfString = truncateSentence($about_me, 100);
            $apiKey = '194c102703f8d300c5694b5fd7d88fd1';

//            $url = 'http://ws.detectlanguage.com/0.2/detect?q=' . $partOfString . '&key=' . $apiKey;
//            $client = new \GuzzleHttp\Client();
//            $res = $client->request('POST', $url);
//            $detectLanguage = json_decode($res->getBody());
//
//            $language = '';
//            if ($detectLanguage == null || $detectLanguage->data->detections[0]->language == '') {
//                $response['status'] = 'error';
//                $response['msg'] = trans('common.api_fail_detect_language');
//                return $response;
//            } else {
            $language = "en";
                //$detectLanguage->data->detections[0]->language;
//            }

            if ($language == "en") {
                if (str_word_count($about_me) > 500) {
                    $response['status'] = 'error';
                    $response['msg'] = trans('validation.max.string', ['attribute' => trans('validation.attributes.about_me'), 'max' => '500']);
                    return $response;
                }
            } else if (($language == "zh-Hant" || $language == "zh")) {
                if (strlen(utf8_decode($about_me)) > 500) {
                    $response['status'] = 'error';
                    $response['msg'] = trans('validation.max.string', ['attribute' => trans('validation.attributes.about_me'), 'max' => '500']);
                    return $response;
                }
            } else {
                if (str_word_count($about_me) > 500) {
                    $response['status'] = 'error';
                    $response['msg'] = trans('validation.max.string', ['attribute' => trans('validation.attributes.about_me'), 'max' => '500']);
                    return $response;
                }
            }

            $project = Project::find($projectId);
            if ($inboxId > 0) {
                //check exist or not
                $count = UserInboxMessages::where('from_user_id', \Auth::guard('users')->user()->id)
                    ->where('to_user_id', $project->user_id)
                    ->where('inbox_id', $inboxId)
                    ->where(function ($query) {
                        $query->where('type', UserInboxMessages::TYPE_PROJECT_INVITE_ACCEPT);
                        $query->orWhere('type', UserInboxMessages::TYPE_PROJECT_INVITE_REJECT);
                    })
                    ->count();

                if ($count > 0) {
                    $response['status'] = 'error';
                    $response['msg'] = trans('common.invite_request_already_reply');
                    return $response;
                }

                $uim = new UserInboxMessages();
                $uim->inbox_id = $inboxId;
                $uim->from_user_id = \Auth::guard('users')->user()->id;
                $uim->to_user_id = $project->user_id;
                $uim->from_staff_id = null;
                $uim->to_staff_id = null;
                $uim->project_id = $projectId;
                $uim->is_read = 0;
                $uim->type = UserInboxMessages::TYPE_PROJECT_INVITE_ACCEPT;
                $uim->save();
            }

//            if ($request->get('qq_id') == '' && $request->get('wechat_id') == '' &&
//                $request->get('skype_id') == '' && $request->get('handphone_no') == '') {
//                $response['status'] = 'error';
//                $response['msg'] = trans('common.at_least_one_contact_info');
//                return $response;
//            }

            $count = ProjectApplicant::GetProjectApplicant()
                ->where('project_id', $projectId)
                ->where('user_id', $userId)->count();

            if ($count >= 1) {
                $response['status'] = 'error';
                $response['msg'] = trans('jobs.you_already_apply_this_job_before');
                return $response;
            }

            if (Project::GetProject()
                ->where('user_id', $userId)
                ->where('id', $projectId)
                ->exists()
            ) {
                $response['status'] = 'error';
                $response['msg'] = trans('jobs.you_are_not_allow_to_apply_because_you_are_project_creator');
                return $response;
            }

            \DB::beginTransaction();

            //all the price save in usd
            //if language is en, no convert and straight save
            //if language is cn, covert to usd and save into db
            if (app()->getLocale() == "cn") {
                $exchangeRate = ExchangeRate::getExchangeRate(Constants::CNY, Constants::USD);
                $quote_price = $request->get('price') * $exchangeRate;
            } else {
                $quote_price = $request->get('price');
            }

            $model = new ProjectApplicant;
            $model->user_id = $userId;
            $model->project_id = $request->get('project_id');
            $model->daily_update = $request->get('daily_update');
            $model->status = ProjectApplicant::STATUS_APPLY; //Apply
            $model->quote_price = $quote_price; //$request->get('price');
            $model->pay_type = $request->get('pay_type');
            $model->about_me = $request->get('about_me');
            $model->save();
            $projectApplicant = $model;
            $newProjectApplicantId = $model->id;

            //when make offer, freelancer need to answer employer question
            if ($newProjectApplicantId >= 1) {
                if ($request->has('answer')) {
                    $questionIds = $request->get('questionId');
                    $answers = $request->get('answer');
//                    dd($answers);
                    foreach ($answers as $key => $answer) {
                        if ($answer != '') {
                            $model = new ProjectApplicantAnswer();
                            $model->project_applicant_id = $newProjectApplicantId;
                            $model->project_question_id = $questionIds[$key];
                            $model->answer = $answer;
                            $model->save();
                        }
                    }
                }
            }

            $model = \Auth::guard('users')->user();
            $Update = false;
            if ($model->qq_id != $request->get('qq_id')) {
                $model->qq_id = $request->get('qq_id');
                $Update = true;
            }

            if ($model->wechat_id != $request->get('wechat_id')) {
                $model->wechat_id = $request->get('wechat_id');
                $Update = true;
            }

            if ($model->skype_id != $request->get('skype_id')) {
                $model->skype_id = $request->get('skype_id');
                $Update = true;
            }

            if ($model->handphone_no != $request->get('handphone_no')) {
                $model->handphone_no = $request->get('handphone_no');
                $Update = true;
            }

            if ($model->about_me != $request->get('about_me')) {
                $model->about_me = $request->get('about_me');
                $Update = true;
            }

            if ($model->hourly_pay != $request->get('price') && (int)$request->get('pay_type') == ProjectApplicant::PAY_TYPE_HOURLY_PAY) {
                $model->hourly_pay = $request->get('price');
                $Update = true;
            }

            if ($Update == true) {
                $model->save();
            }

            //Project Chat Room
            $projectChatRoom = ProjectChatRoom::GetProjectChatRoom()
                ->where('project_id', $request->get('project_id'))
                ->first();

            //chat room follower
            $projectChatFollower = ProjectChatFollower::GetProjectChatFollower()
                ->where('user_id', \Auth::guard('users')->user()->id)
                ->where('project_chat_room_id', $projectChatRoom->id)
                ->first();

            if ($projectChatFollower == null) {
                $projectChatFollower = new ProjectChatFollower();
                $projectChatFollower->project_chat_room_id = $projectChatRoom->id;
                $projectChatFollower->user_id = \Auth::guard('users')->user()->id;
                $projectChatFollower->is_kick = 0;
                $projectChatFollower->save();
            }

            //update badge count
            $project = Project::find($projectId);
            $project->is_read = Project::IS_READ_NO;
            $project->save();

            \DB::commit();

//            FavouriteJobController::sendMakeOfferNotification($project, $projectApplicant);

            $response['status'] = 'success';
            $response['msg'] = trans('common.successfully_apply');
        } catch (\Exception $e) {
            \DB::rollback();

            $response['status'] = 'error';
            $response['msg'] = $e->getMessage() . " in " . $e->getFile() . " in " . $e->getLine();
        }

        return $response;
    }

    public function findExperts(Request $request)
    {
        try {
            $skillList = Skill::getSearchSkillData(Skill::SEARCH_TYPE_WHITE);

            $jobPositions = JobPosition::GetJobPosition()->get();
            $statusList = User::getStatusLists();
            $languages = json_encode(CountryLanguage::getCountryLanguages());
            $countries = WorldCountries::getCountryLists();
            $hourlyRanges = Constants::getHourlyRanges();

            $countriesCode = Countries::getCountryCodeLists();

            $filter = array();
//        $filter['skill_id'] = null;
            $filter['name'] = null;
            $filter['skill_id_list'] = null;
            $filter['job_position_id'] = null;
            $filter['job_position_name'] = null;
            $filter['hourly_range_id'] = null;
            $filter['hourly_range_name'] = null;
            $filter['status_id'] = null;
            $filter['review_type_id'] = null;
            $filter['review_type_name'] = null;
            $filter['job_title'] = null;
            $filter['last_activities_id'] = null;
            $filter['country_id'] = null;
            $filter['country_name'] = null;
            $filter['language_id'] = null;

            $users = User::with('job_position', 'skills.skill', 'comment', 'languages', 'country');
            //admin impersonate to user (use in message inbox)
            //not need to show if user account is freeze
            $users = $users->where('id', '!=', 1);

            if (\Auth::guard('users')->check()) {
                $users = $users->where('id', '!=', \Auth::guard('users')->user()->id);
            }

            if ($request->has('name') && $request->get('name') != "") {
                $users = $users->where('name', 'LIKE', '%' . $request->get('name') . '%')
                    ->orWhere('name', 'LIKE', '%' . $request->get('name') . '%');
                $filter['name'] = $request->get('name');
            }

            if ($request->has('job_position_id') && $request->get('job_position_id') != 0) {
                $users = $users->where('job_position_id', $request->get('job_position_id'));
                $filter['job_position_id'] = $request->get('job_position_id');

                $jobPosition = JobPosition::where('id', $request->get('job_position_id'))->first();
                $filter['job_position_name'] = $jobPosition->getName();
            }

            if ($request->has('hourly_range_id') && $request->get('hourly_range_id') != "") {
                switch ($request->get('hourly_range_id')) {
                    case "1":
                        $startValue = 1;
                        $endValue = 10;
                        break;
                    case "2":
                        $startValue = 11;
                        $endValue = 20;
                        break;
                    case "3":
                        $startValue = 21;
                        $endValue = 30;
                        break;
                    case "4":
                        $startValue = 31;
                        $endValue = 40;
                        break;
                    case "5":
                        $startValue = 41;
                        $endValue = 50;
                        break;
                    case "6":
                        $startValue = 51;
                        $endValue = 20000;
                        break;
                }

//            if (app()->getLocale() == "cn")
//            {
//                $exchangeRate = ExchangeRate::getExchangeRate(Constants::CNY, Constants::USD);
//                $startValue = $startValue * $exchangeRate;
//                $endValue = $endValue * $exchangeRate;
//            }

                $users = $users->whereBetween('hourly_pay', array($startValue, $endValue));
                $filter['hourly_range_id'] = $request->get('hourly_range_id');

                $hourlyRangeName = Project::translateHourlyRange($request->get('hourly_range_id'));
                $filter['hourly_range_name'] = $hourlyRangeName;
            }

            if ($request->has('review_type_id') && $request->get('review_type_id') != 0) {
                $review_type_id = $request->get('review_type_id');

                if ($review_type_id == Constants::REVIEW_TYPE_OFFICIAL) {
                    $users = $users->whereHas('comment', function ($query) {
                        $query->where('official_project_id', '>', 0);
                    });
                } else {

                }

//            $users = $users->where('status', $request->get('status_id'));
                $filter['review_type_id'] = $request->get('review_type_id');

                $filter['review_type_name'] = Constants::translateReviewType($request->get('review_type_id'));
            }

            if ($request->has('job_title') && $request->get('job_title') != "") {
                $users = $users->where('title', 'LIKE', '%' . $request->get('job_title') . '%');
                $filter['job_title'] = $request->get('job_title');
            }

            if ($request->has('last_activities_id') && $request->get('last_activities_id') != 0) {
                $last_activities_id = $request->get('last_activities_id');

                if ($last_activities_id == Constants::LAST_ACTIVITY_WITHIN_ONE_WEEK) {
                    $oneWeek = Carbon::now()->subWeek();
                    $users = $users->where('last_login_at', '>=', $oneWeek);
                } else if ($last_activities_id == Constants::LAST_ACTIVITY_WITHIN_ONE_MONTH) {
                    $oneMonth = Carbon::now()->subMonth(1);
                    $users = $users->where('last_login_at', '>=', $oneMonth);
                } else if ($last_activities_id == Constants::LAST_ACTIVITY_WITHIN_THREE_MONTHS) {
                    $threeMonths = Carbon::now()->subMonth(3);
                    $users = $users->where('last_login_at', '>=', $threeMonths);
                }

                $filter['last_activities_id'] = $request->get('last_activities_id');
            }

            if ($request->has('country_id') && $request->get('country_id') != "") {
                $users = $users->where('country_id', $request->get('country_id'));
                $filter['country_id'] = $request->get('country_id');

                $country = Countries::where('id', $request->get('country_id'))->first();
                $filter['country_name'] = $country->getName();
            }

            if ($request->has('language_id') && $request->get('language_id') != "") {
                $language_id = $request->get('language_id');
                $users = $users->whereHas('languages', function ($query) use ($language_id) {
                    $query->where('language_id', $language_id);
                });
                $filter['language_id'] = $request->get('language_id');
            }

            if ($request->has('status_id') && $request->get('status_id') != 0) {
                $status = $request->get('status_id');
                if ($status == User::STATUS_LOOK_FOR_COFOUNDER || $status == User::STATUS_LOOK_FOR_FULL_TIME_JOB) {
                    $users = $users->where('status', $request->get('status_id'));
                } else {
                    //any status allow
                }

                $filter['status_id'] = $request->get('status_id');
            }

//        if ($request->has('hourly_rate') && $request->get('hourly_rate') != "") {
//            $users = $users->where('hourly_pay', $request->get('hourly_rate'));
//            $filter['hourly_rate'] = $request->get('hourly_rate');
//        }

//        if ($request->has('city_id') && $request->get('city_id') != 0) {
//            $users = $users->where('city_id', $request->get('city_id'));
//            $filter['city_id'] = $request->get('city_id');
//            $filter['city_name'] = CountryCities::find($request->get('city_id'))->name;
//        }

            $arrSimilarSkillUserId = array();
            if ($request->has('skill_id_list') && $request->get('skill_id_list') != 0) {
                $skill_id_list = $request->get('skill_id_list');
                $skillArray = explode(',', $skill_id_list);

                DB::table('users')->update(array('search_order' => 0));

                //any of the skill option
                $users = $users->whereHas('skills', function ($query) use ($skillArray) {
                    $query
                        ->whereIn('skill_id', $skillArray);
                });

                $filter['skill_id_list'] = $skill_id_list;

                //calculate user skill
                //white skill - each skill give 1 mark
                //green skill - each skill give 10 mark
                //gold skill - each skill give 100 mark
                //update search_order

                foreach ($users->get() as $searchUser) {
                    $searchUser->search_order = getDisplayPriorityPoint($searchUser);
                    $searchUser->save();

                }
            }

            //remove the account which are freeze
            $users = $users->where('is_freeze', 0);

            $userCount = $users->count();

            $mode = "view";
            if ($filter['name'] == null && $filter['skill_id_list'] == null && $filter['job_position_id'] == null
                && $filter['job_position_name'] == null && $filter['hourly_range_id'] == null && $filter['hourly_range_name'] == null
                && $filter['status_id'] == null && $filter['review_type_id'] == null && $filter['review_type_name'] == null
                && $filter['job_title'] == null && $filter['last_activities_id'] == null && $filter['country_id'] == null
                && $filter['country_name'] == null && $filter['language_id'] == null
            ) {
                $mode = 'view';
            } else {
                $mode = 'search';
            }

            if ($mode == 'view') {
                $users = $users->orderBy('initial_display', 'desc')
                    ->get();
            } else {
                $users = $users->orderBy('search_order', 'desc')
                    ->orderBy('last_login_at', 'Desc')
                    ->get();
            }
//        }

//            dd(Auth::guard('users')->user()->id);
            if (Auth::guard('users')->check()) {
                $projects = Project::GetProject()
                    ->where('user_id', Auth::guard('users')->user()->id)->get();
            } else {
                $projects = null;
            }

            $invitationProject = null;
            if ($request->has('invitation_project_id') && $request->get('invitation_project_id') != 0) {
                $invitationProject = Project::find($request->get('invitation_project_id'));
            }

            if ($filter['review_type_id'] != null || $filter['job_title'] != null || $filter['last_activities_id'] != null ||
                $filter['country_id'] != null || $filter['language_id'] != null || $filter['status_id'] != null) {
                $showAdvanceFilterSection = "true";
            } else {
                $showAdvanceFilterSection = "false";
            }


            foreach ($users as $user) {
                $user->skills = $user->skills
                    ->sortByDesc('is_client_verified');
            }
            foreach ($users as $user) {
                $user->skills = $user->skills
                    ->sortByDesc('experience');
            }


            $aboutMe = array();
            foreach ($users as $user) {
                $aboutMe[$user->id] = '';
                $display = "";
                if (preg_match_all('/./u', $user->about_me, $matches) > 75) {
                    for ($i = 0; $i < 75; $i++) {
                        $display = $display . $matches[0][$i];
                    }

                    $aboutMe[$user->id] = $display;
                }
                $userStatus = $user['status'];
                $userStatusText = '';
                if($userStatus == 1) {
                    $userStatusText = 'Look for co-founder';
                }
                if($userStatus == 2) {
                    $userStatusText = 'Look for full time job';
                }
                if($userStatus == 3) {
                    $userStatusText = 'Look for part time job';
                }

                $user['status'] = $userStatusText;
            }

            $isSuccessPost = $request->get('success_post');

            return collect([
                'status' => 'success',
                'data' => collect([
                    'users' => $users,
                    'filter' => $filter,
                    'skillList' => json_decode($skillList),
                    'jobPositions' => $jobPositions,
                    'statusList' => $statusList,
                    'projects' => $projects,
//                'invitationProject' => $invitationProject,
//                'count' => $userCount,
                    'languages' => json_decode($languages),
                    'countries' => $countries,
                    'hourlyRanges' => $hourlyRanges,
                    'showAdvanceFilterSection' => $showAdvanceFilterSection,
                    'countriesCode' => $countriesCode,
                    'isSuccessPost' => $isSuccessPost,
//                'aboutMe' => $aboutMe
                ])
            ]);
        }catch (\Exception $e){
            dd($e);
        }
    }

    public function inviteToWork(Request $request)
    {
        try {
            $projectId = $request->get('projectId');
            $receiverId = $request->get('receiverId');
            $senderMessage = $request->get('senderMessage');

            $sender = \Auth::guard('users')->user();
            $receiver = User::where('id', $receiverId)->first();
            $project = Project::where('id', $projectId)->first();

            $url = route('frontend.joindiscussion.getproject', [$project->name, $project->id]);

            $exist = UserInbox::where('category', '=', UserInbox::CATEGORY_NORMAL)
                ->where('project_id', '=', $project->id)
                ->where('from_user_id', '=', $sender->id)
                ->where('to_user_id', '=', $receiverId)->first();

            if(!is_null($exist)) {
                return collect([
                    'status'     => 'Error',
                    'message'    => 'Invitation already sent...!'
                ]);
            }

            if( !( $receiver->email == null || $receiver->email == "" ) )
            {
                $senderEmail = 'support@xenren.co';
                $senderName = $sender->getName();

                Mail::send('mails.invite', array('project'=>$project, 'url' => $url, 'sender' => $sender, 'senderMessage' => $senderMessage ), function($message) use ( $senderEmail, $senderName, $receiver ) {
                    $message->from($senderEmail, $senderName);
                    $message->to($receiver->email, $receiver->first_name.' '. $receiver->last_name )->subject(trans('common.project_invite').'!');
                });
            }

            //add user inbox notification
            $userInbox =  new UserInbox();
            $userInbox->category = UserInbox::CATEGORY_NORMAL;
            $userInbox->project_id =  $project->id;
            $userInbox->from_user_id = $sender->id;
            $userInbox->to_user_id = $receiverId;
            $userInbox->from_staff_id = null;
            $userInbox->to_staff_id = null;
            $userInbox->type = UserInbox::TYPE_PROJECT_INVITE_YOU;
            $userInbox->is_trash = 0;
            $userInbox->save();

            //add user inbox message
            $userInboxMessage = new UserInboxMessages();
            $userInboxMessage->inbox_id = $userInbox->id;
            $userInboxMessage->from_user_id = $sender->id;
            $userInboxMessage->to_user_id = $receiverId;
            $userInboxMessage->from_staff_id = null;
            $userInboxMessage->to_staff_id = null;
            $userInboxMessage->project_id = $project->id;
            $userInboxMessage->is_read = 0;
            $userInboxMessage->type = UserInbox::TYPE_PROJECT_INVITE_YOU;

            $userInboxMessage->custom_message = $senderMessage;
            $userInboxMessage->save();

            return response()->json([
                'status'  => 'success',
                'data'    => $receiverId
            ]);
        } catch(\Exception $e){
            return collect([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function sendOffer(Request $request)
    {
        try {
            $projectId = $request->get('projectId');
            $receiverId = $request->get('receiverId');
            $payTypeId = $request->get('pay_type');
            $amount = $request->get('amount');
            $senderMessage = $request->get('senderMessage');

            $sender = \Auth::guard('users')->user();
            $receiver = User::where('id', $receiverId)->first();
            $project = Project::where('id', $projectId)->first();

            if(!$project) {
                return collect([
                    'status'   => 'error',
                    'message'  => trans('common.project_no_exist')
                ]);
            }

            if( $payTypeId == Constants::PAY_TYPE_FIXED_PRICE )
            {
                $pay_type = trans('common.fixed_price');
            }
            else if( $payTypeId == Constants::PAY_TYPE_HOURLY_PAY )
            {
                $pay_type = trans('common.pay_hourly');
            }

            $url = route('frontend.joindiscussion.getproject', [$project->name, $project->id]);

            $senderEmail = 'support@xenren.co';
            $senderName = $sender->getName();

            try {
                Mail::send('mails.sendOffer', array(
                    'project' => $project,
                    'url' => $url,
                    'sender' => $sender,
                    'senderMessage' => $senderMessage,
                    'pay_type' => $pay_type,
                    'amount' => $amount,

                ), function ($message) use ($senderEmail, $senderName, $receiver) {
                    $message->from($senderEmail, $senderName);
                    $message->to($receiver->email, $receiver->first_name . ' ' . $receiver->last_name)->subject(trans('common.send_offer') . '!');
                });
            } catch (\Exception $e){}

            //add user inbox notification
            $userInbox =  new UserInbox();
            $userInbox->category = UserInbox::CATEGORY_NORMAL;
            $userInbox->project_id =  $project->id;
            $userInbox->from_user_id = $sender->id;
            $userInbox->to_user_id = $receiverId;
            $userInbox->from_staff_id = null;
            $userInbox->to_staff_id = null;
            $userInbox->daily_update = $request->daily_update;
            $userInbox->type = UserInbox::TYPE_PROJECT_SEND_OFFER;
            $userInbox->is_trash = 0;
            $userInbox->save();

            //add user inbox message
            $userInboxMessage = new UserInboxMessages();
            $userInboxMessage->inbox_id = $userInbox->id;
            $userInboxMessage->from_user_id = $sender->id;
            $userInboxMessage->to_user_id = $receiverId;
            $userInboxMessage->from_staff_id = null;
            $userInboxMessage->to_staff_id = null;
            $userInboxMessage->project_id = $project->id;
            $userInboxMessage->is_read = 0;
            $userInboxMessage->type = UserInbox::TYPE_PROJECT_SEND_OFFER;
            $userInboxMessage->quote_price = $amount;
            $userInboxMessage->pay_type = $payTypeId;
            $userInboxMessage->custom_message = $senderMessage;
            $userInboxMessage->save();

            return collect([
                'status'  => 'success',
                'message' => 'Offer sent successfully!'
            ]);
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function addToFavorite($freelancer_id)
    {
        try {
            //check whether exist or not
            $count = FavouriteFreelancer::where('user_id', Auth::id())
                ->where('freelancer_id', $freelancer_id)
                ->count();
            if( $count >= 1 )
            {
                return collect([
                    'status'	=> 'error',
                    'message'   => trans('common.freelancer_already_exist_in_list')
                ]);
            }

            $favouriteFreelancer = new FavouriteFreelancer();
            $favouriteFreelancer->user_id = Auth::id();
            $favouriteFreelancer->freelancer_id = $freelancer_id;
            $favouriteFreelancer->save();

            return collect([
                'status'   => 'success',
                'message'  => trans('common.freelancer_added_into_list')
            ]);
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function removeFromFavorite($freelancer_id)
    {
        try {
            //check whether exist or not
            $count = FavouriteFreelancer::where('user_id', Auth::id())
                ->where('freelancer_id', $freelancer_id)
                ->count();
            if( $count >= 1 ) {
                FavouriteFreelancer::where('user_id', Auth::id())
                    ->where('freelancer_id', $freelancer_id)
                    ->first()->delete();

                return collect([
                    'status'   => 'success',
                    'message'  => trans('common.freelancer_remove_from_list_success')
                ]);
            }
            else {
                return collect([
                    'status'   => 'error',
                    'message'  => trans('common.freelancer_no_exist_in_list')
                ]);
            }
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    public static function getHourlyRanges(){
        $arr = array();
        if( app()->getLocale() == "en" )
        {
            $currency = trans("common.usd");
        }
        else
        {
            $currency = trans("common.rmb");
        }
        $arr[1] = "1 - 10 " . $currency;
        $arr[2] = "11 - 20 " . $currency;
        $arr[3] = "21 - 30 " . $currency;
        $arr[4] = "31 - 40 " . $currency;
        $arr[5] = "41 - 50 " . $currency;
        $arr[6] = "51+" . $currency;


        return $arr;
    }
}
