<?php
/**
 * Created by PhpStorm.
 * User: abdullah
 * Date: 9/4/18
 * Time: 9:58 PM
 */

namespace App\Http\Controllers\Api\Home;

use App\Http\Controllers\Controller;
use App\Models\ProjectChatRoom;
use Carbon;
use App\Http\Controllers\FrontendController;
use App\Http\Requests\Frontend\UserChatMessageCreateFormRequest;
use App\Http\Requests\Frontend\UserChatMessageCreateProjectFormRequest;
use App\Http\Requests\Frontend\UserChatMessageCreateProjectFileFormRequest;
use App\Http\Requests\Frontend\UserChatMessageCreateProjectImageFormRequest;
use Illuminate\Http\Request;
use App\Models\ProjectChatMessage;
use App\Models\UserInbox;
use App\Models\UserInboxMessages;
use App\Models\User;
use App\Models\Project;
use App\Models\ProjectApplicant;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\View;

class ProjectChatApiController extends Controller
{
    public function sendMessage(Request $request)
    {
        $response = array();
        $response['status'] = '';
        $response['message'] = '';

        try {
            $projectChatRoomId = $request->get('projectChatRoomId');
            $message = $request->get('message');

            ProjectChatMessage::create([
                'project_chat_room_id' => $projectChatRoomId,
                'sender_user_id' => \Auth::user()->id,
                'message' => $message
            ]);
            $response['trigger'] = $this->trigger($request);
            $response['status'] = 'Success';
            // TODO: Make it multi-lingual
            $response['message'] = 'Message has been sent';
        } catch (\Exception $exception) {
            $response['status'] = 'Error';
            $response['message'] = $exception->getMessage() . " in " . $exception->getFile() . " in " . $exception->getLine();
        }

        return $response;

//        return view('frontend.joinDiscussion.ChatItem',[
//            'data' => ProjectChatMessage::pullChatProjectMessage($request->projectChatRoomId)['lists']
//        ]);
    }

    public function trigger(Request $request)
    {
        $channel = 'project_chat';
        $event = $request['event'];
        $pusher = App::make('pusher');
        $pusher->trigger($channel,
            $event,
            array(
                'text' => $request['message'] . 'by: ' . \Auth::user()->name,
                'name' => \Auth::user()->name,
                'sender_id' => \Auth::user()->id
            ));

        return collect([
            'data' => 'success'
        ]);
    }

    public function postProjectChatFile(UserChatMessageCreateProjectFileFormRequest $request)
    {
        $response = array();
        $response['status'] = '';
        $response['message'] = '';

        try {
            $ProjectChatMessage = new ProjectChatMessage();
            $ProjectChatMessage->sender_user_id = \Auth::guard('users')->user()->id;
            $ProjectChatMessage->project_chat_room_id = $request->get('projectChatRoomId');
            $ProjectChatMessage->message = '--file upload: ' . $request->get('message');
            $ProjectChatMessage->file = $request->file('file');

            $projectChatRoom = ProjectChatRoom::find($ProjectChatMessage->project_chat_room_id);
            $projectID = $projectChatRoom->project_id;

            //for my publish order is read logic
            $project = Project::find($projectID);
            if ($project->user_id != \Auth::guard('users')->user()->id) {
                $project->is_read = Project::IS_READ_NO;
                $project->save();
            }

            //for my receive order is read logic
            $projectApplicants = ProjectApplicant::where('project_id', $projectID)
                ->get();
            foreach ($projectApplicants as $projectApplicant) {
                $projectApplicant->is_read = ProjectApplicant::IS_READ_NO;
                $projectApplicant->save();
            }

            $ProjectChatMessage->save();

            $this->redisPublish($ProjectChatMessage->id);

            //get html content for chat row
//            $view = View::make('frontend.joinDiscussion.ucChatItem', [
//                'message' => str_replace("--file upload: ", "", $ProjectChatMessage->message),
//                'time' => date('d M, Y H:i', strtotime($ProjectChatMessage->created_at)),
//                'realName' => \Auth::guard('users')->user()->getName(),
//                'avatar' => \Auth::guard('users')->user()->getAvatar(),
//                'file' => $ProjectChatMessage->getFile()
//            ]);
//            $contents = $view->render();

            $response['status'] = 'Success';
            // TODO: Add translation here
            $response['message'] = 'Image has been uploaded';

//            return response()->json([
//                'status' => 'OK',
//                'contents' => $contents
//            ]);

        } catch (\Exception $e) {
            $response['status'] = 'Error';
            $response['message'] = $e->getMessage() . " in " . $e->getFile() . " in " . $e->getLine();

//            return response()->json([
//                'status' => 'Error',
//                'message' => $e->getMessage()
//            ]);
        }

        return $response;
    }

    function redisPublish($id)
    {
        $message = ProjectChatMessage::find($id);
        $chat['project_chat_room_id'] = $message->project_chat_room_id;
        if ($message->SendBy()->first()->name != '') {
            $name = $message->SendBy()->first()->name;
        } else {
            $email = $message->SendBy()->first()->email;
            $name = explode('@', $email);
            if (count($name) >= 2) {
                $name = $name[0];
            } else {
                $name = $email;
            }
        }
        $chat['id'] = $message->id;
        $chat['sender_user_id'] = $message->sender_user_id;
        $chat['name'] = $name;
        $chat['avatar'] = $message->sendBy()->first()->getAvatar();
        $chat['email'] = $name;
        $chat['message'] = $message->message;
        $chat['content'] = $message->message;
        $chat['time'] = date('d M, Y H:i', strtotime($message->created_at));
        $chat['status'] = "out";
        $chat['sender_user_id'] = $message->sender_user_id;
        $chat['date'] = date('Y-m-d', strtotime($message->created_at));
        $chat['file'] = $message->file;
        $isImage = 0;

        if (file_exists($message->file)) {
            if (strpos($message->file, '.txt') !== false || strpos($message->file, '.doc') !== false) {
                $chat['content'] = str_replace('--file upload: ', '', '<a href="' . $message->getFile() . '">' . $message->message . '<a>');
                $isImage = 0;
            } else {
                $a = getimagesize($message->file);
                $image_type = $a[2];
                if (in_array($image_type, array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG, IMAGETYPE_BMP))) {
                    $chat['content'] = str_replace('--file upload: ', '', $message->message);
                    $isImage = 1;
                }
            }
        }
        $chat['isImage'] = $isImage;

        $isAudio = 0;
        if (file_exists($message->file)) {
            $allowed = array(
                'audio/mpeg', 'audio/x-mpeg', 'audio/mpeg3', 'audio/x-mpeg-3', 'audio/aiff',
                'audio/mid', 'audio/x-aiff', 'audio/x-mpequrl', 'audio/midi', 'audio/x-mid',
                'audio/x-midi', 'audio/wav', 'audio/x-wav', 'audio/xm', 'audio/x-aac', 'audio/basic',
                'audio/flac', 'audio/mp4', 'audio/x-matroska', 'audio/ogg', 'audio/s3m', 'audio/x-ms-wax',
                'audio/xm', 'application/ogg', 'application/mp3', 'audio/mp3'
            );

            // check REAL MIME type
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $type = finfo_file($finfo, $message->file);
            finfo_close($finfo);

            // check to see if REAL MIME type is inside $allowed array
            if (in_array($type, $allowed)) {
                $isAudio = 1;
            }
        }
        $chat['isAudio'] = $isAudio;
        Redis::publish('default_room', \GuzzleHttp\json_encode($chat));
    }
}