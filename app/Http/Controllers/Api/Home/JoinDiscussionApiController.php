<?php

namespace App\Http\Controllers\Api\Home;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\ProjectChatController;
use Illuminate\Http\Request;
use App\Models\ProjectChatMessage;

use App\Http\Requests;
use App\Models\Project;
use App\Models\ProjectChatRoom;
use App\Models\ProjectChatFollower;
use App\Models\ProjectApplicant;
use App\Models\Skill;
use Illuminate\Support\Facades\Redis;
use App\Models\JobPosition;
use App\Models\OfficialProjectNews;
use App\Models\Portfolio;
use App\Models\ProjectDispute;
use App\Models\Review;
use App\Models\UserInbox;
use App\Models\UserInboxMessages;
use App\Models\UserSkillComment;
use App\Constants;
use Carbon;

class JoinDiscussionApiController extends Controller
{
    public function getProject($id)
    {
        //for resume user page and redirect back
//        Session::put('redirect_url', route('frontend.joindiscussion.getproject', ['id' => $id ]));
//        Session::put('redirect_url_name', trans('common.back_to_join_discussion'));

        $response = array();
        $response['status'] = '';
        $response["message"] = "";

        try {
            $userInvited = preg_replace('/[0-9]/', '', $id);
            $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

            $project = Project::with(['projectChatRoom', 'projectSkills', 'projectFiles'])->find($id);
            if (!$project) {
                $response["status"] = "error";
                $response["message"] = "404 not found";
                return $response;
            } else {
                if ($project->freelance_type == Project::FREELANCER_TYPE_INVITED_USER) {
                    if ($userInvited != "pvt") {
                        $response["status"] = "error";
                        $response["message"] = "404 not found";
                        return $response;
                    }
                }
            }

            //redis init
            Redis::publish('rooms', json_encode(['room' => 'default_room']));
            Redis::publish('room', $id);
            //Project Chat Room

            $projectChatRoom = ProjectChatRoom::GetProjectChatRoom()
                ->where('project_id', $project->id)
                ->first();

            //create a chat room if the chat room not exist
            if (!isset($projectChatRoom)) {
                $projectChatRoom = new ProjectChatRoom();
                $projectChatRoom->project_id = $project->id;
                $projectChatRoom->save();
            }

            //chat room follower
            $projectChatFollower = ProjectChatFollower::GetProjectChatFollower()
                ->where('user_id', \Auth::guard('users')->user()->id)
                ->where('project_chat_room_id', $projectChatRoom->id)
                ->first();

            $isNewComer = false;
            if ($projectChatFollower == null) {
                $projectChatFollower = new ProjectChatFollower();
                $projectChatFollower->project_chat_room_id = $projectChatRoom->id;
                $projectChatFollower->user_id = \Auth::guard('users')->user()->id;
                $projectChatFollower->is_kick = 0;
                $projectChatFollower->save();

                $isNewComer = true;
            }

            $projectChatFollowers = ProjectChatFollower::with(['user'])
                ->where('project_chat_room_id', $projectChatRoom->id)
                ->where('user_id', '!=', null)
                ->orderBy('thumb_status', 'asc')
                ->get();

            $user = \Auth::guard('users')->user();

            $candidateAlreadySelected = $project->haveApprovedApplicant();
            $candidateUserId = $project->getApprovedApplicant();

            $projectApplicants = ProjectApplicant::with(['user'])
                ->where('user_id', '>', 0)//because have user = null when new job post for published status
                ->where('project_id', $project->id)
                ->paginate(10);

            $skill_id_list = "";
            foreach ($project->projectSkills as $key => $projectSkill) {
                if ($key == 0) {
                    $skill_id_list = $projectSkill->skill_id;
                } else {
                    $skill_id_list = $skill_id_list . "," . $projectSkill->skill_id;
                }
            }

            if ($isNewComer == true) {
                //check whether user inbox exist or not
                $userInbox = UserInbox::where('project_id', $project->id)
                    ->where('type', UserInbox::TYPE_PROJECT_PEOPLE_COME_IN)
                    ->first();

                if (!$userInbox) {
                    //system message notification
                    $userInbox = new UserInbox();
                    $userInbox->category = UserInbox::CATEGORY_SYSTEM;
                    $userInbox->project_id = $project->id;

                    if ($project->type == Project::PROJECT_TYPE_COMMON) {
                        $userInbox->from_user_id = null;
                        $userInbox->to_user_id = $project->user_id;
                        $userInbox->from_staff_id = 1;
                        $userInbox->to_staff_id = null;
                    } else if ($project->type == Project::PROJECT_TYPE_OFFICIAL) {
                        $userInbox->from_user_id = null;
                        $userInbox->to_user_id = null;
                        $userInbox->from_staff_id = 1;
                        $userInbox->to_staff_id = $project->staff_id;
                    }

                    $userInbox->type = UserInbox::TYPE_PROJECT_PEOPLE_COME_IN;
                    $userInbox->is_trash = 0;
                    $userInbox->save();
                }

                //add user inbox message
                $userInboxMessage = new UserInboxMessages();
                $userInboxMessage->inbox_id = $userInbox->id;
                if ($project->type == Project::PROJECT_TYPE_COMMON) {
                    $userInboxMessage->from_user_id = null;
                    $userInboxMessage->to_user_id = $project->user_id;
                    $userInboxMessage->from_staff_id = 1;
                    $userInboxMessage->to_staff_id = null;
                } else if ($project->type == Project::PROJECT_TYPE_OFFICIAL) {
                    $userInboxMessage->from_user_id = null;
                    $userInboxMessage->to_user_id = null;
                    $userInboxMessage->from_staff_id = 1;
                    $userInboxMessage->to_staff_id = $project->staff_id;
                }

                $userInboxMessage->project_id = $project->id;
                $userInboxMessage->is_read = 0;
                $userInboxMessage->type = UserInboxMessages::TYPE_PROJECT_PEOPLE_COME_IN;

                $message = trans('common.user_come_in', ['nickname' => \Auth::guard('users')->user()->getName()]);
                $userInboxMessage->custom_message = $message;
                $userInboxMessage->save();
            }

            $officialProjectNewsList = OfficialProjectNews::where('project_id', $project->id)
                ->orderBy('created_at', 'desc')
                ->limit(4)
                ->get();

            //make the project as read if you are project creator
            if ($project->is_read == Project::IS_READ_NO && $project->user_id == \Auth::guard('users')->user()->id) {
                $project->is_read = Project::IS_READ_YES;
                $project->save();
            }

            //make the project applicant read if you are project applicant
            $applicant = ProjectApplicant::where('user_id', \Auth::guard('users')->user()->id)
                ->where('project_id', $project->id)
                ->first();
            if (!empty($applicant)) {
                $applicant->is_read = ProjectApplicant::IS_READ_YES;
                $applicant->save();
            } else {
                $applicant = ProjectApplicant::where('user_id', null)
                    ->where('project_id', $project->id)
                    ->first();
            }

            $postSkill = [];
            if (!empty($applicant->skill_id_string)) {
                $postSkill = Skill::whereIn('id', explode(",", $applicant->skill_id_string))
                    ->get();
            }

            if (\Auth::guard('users')->user()->is_agent == 1) {
                $skillList = Skill::getSearchSkillData(Skill::TAG_COLOR_GOLD);
            } else {
                $skillList = Skill::getSearchSkillData(Skill::TAG_COLOR_WHITE);

            }

            $pay_types = Constants::getPayTypeLists();
            if (empty($applicant->reference_price_type))
                $type = $project->pay_type;
            else
                $type = $applicant->reference_price_type;

            $post_type = 'project';
        } catch (\Exception $exception) {
            $response['status'] = 'error';
            $response['error'] = $exception->getMessage() . " in " . $exception->getFile() . " in " . $exception->getLine();
        }

        $project->description = strip_tags($project->description);

        $response["status"] = "success";
        $response['project'] = $project;
        $response['projectChatRoomId'] = $projectChatRoom->id;
        $response['projectChatFollowers'] = $projectChatFollowers;
        $response['user'] = $user;
        $response['projectApplicants'] = $projectApplicants;
        $response['applicant'] = $applicant;
        $response['candidateAlreadySelected'] = $candidateAlreadySelected;
        $response['candidateUserId'] = $candidateUserId;
        $response['skill_id_list'] = $skill_id_list;
        $response['officialProjectNewsList'] = $officialProjectNewsList;
        $response['postSkill'] = $postSkill;
        $response['post_type'] = $post_type;
        $response['pay_types'] = $pay_types;
        $response['type'] = $type;
        $response['skillList'] = json_decode($skillList);
        $chatLog = ProjectChatMessage::pullChatProjectMessage($projectChatRoom->id);
        $response['chat_messages'] = $chatLog['lists'];

        return $response;

//        return view('frontend.joinDiscussion.index', [
//            'project' => $project,
//            'projectChatRoomId' => $projectChatRoom->id,
//            'projectChatFollowers' => $projectChatFollowers,
//            'user' => $user,
//            'projectApplicants' => $projectApplicants,
//            'applicant' => $applicant,
//            'candidateAlreadySelected' => $candidateAlreadySelected,
//            'candidateUserId' => $candidateUserId,
//            'skill_id_list' => $skill_id_list,
//            'officialProjectNewsList' => $officialProjectNewsList,
//            'postSkill' => $postSkill,
//
//            'post_type' => $post_type,
//            'pay_types' => $pay_types,
//            'type' => $type,
//            'skillList' => $skillList,
//        ]);
    }

    public function thumbUp($projectChatFollowerId)
    {
        $projectChatFollower = ProjectChatFollower::find($projectChatFollowerId);
        $projectChatFollower->thumb_status = ProjectChatFollower::THUMB_STATUS_THUMB_UP;

        try {
            if ($projectChatFollower->save()) {
                return makeJSONResponse(true, trans('common.submit_successful'));

            } else {
                return makeJSONResponse(false, trans('common.err_happen'));
            }
        } catch (\Exception $e) {
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function thumbDown($projectChatFollowerId)
    {
        $projectChatFollower = ProjectChatFollower::find($projectChatFollowerId);
        $projectChatFollower->thumb_status = ProjectChatFollower::THUMB_STATUS_THUMB_DOWN;

        try {
            if ($projectChatFollower->save()) {
                return makeJSONResponse(true, trans('common.submit_successful'));

            } else {
                return makeJSONResponse(false, trans('common.err_happen'));
            }
        } catch (\Exception $e) {
            return makeJSONResponse(false, $e->getMessage());
        }
    }
}
