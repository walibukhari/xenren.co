<?php
/**
 * Created by PhpStorm.
 * User: abdullah
 * Date: 9/4/18
 * Time: 8:13 PM
 */

namespace App\Http\Controllers\Api\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ThumbdownJob;
use Illuminate\Support\Facades\Auth;

class ThumbdownJobApiController extends Controller
{
    public function thumbsDownJob(Request $request)
    {
        $response = array();
        $response['status'] = "";

        try {
            $projectId = $request->get('projectId');
            $projectType = $request->get('projectType');

            //check whether exist or not
            $count = ThumbdownJob::where('user_id', Auth::id())
                ->where('project_id', $projectId)
                ->where('project_type', $projectType)
                ->first();

            if (!is_null($count)) {
                $count->delete();
                $response['status'] = "error";
                $response['message'] = 'Project Thumb Up...!';

//                return response()->json([
//                    'status' => 'Error',
//                    'message' => trans('common.error')
//                ]);
            } else {
                $thumbdownJob = new ThumbdownJob();
                $thumbdownJob->user_id = Auth::id();
                $thumbdownJob->project_id = $projectId;
                $thumbdownJob->project_type = $projectType;
                $thumbdownJob->save();

                $response['status'] = "success";
                $response['message'] = 'Project Thumb Downed...!';
            }

//        return response()->json([
//            'status'        => 'OK',
//            'message'       => trans('common.job_thumbed_down')
//        ]);
        } catch (\Exception $exception) {
            $response['status'] = "error";
            $response['error'] = $exception->getMessage() . " in " . $exception->getFile() . " in " . $exception->getLine();
        }

        return $response;
    }
}
