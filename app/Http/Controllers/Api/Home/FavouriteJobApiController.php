<?php
/**
 * Created by PhpStorm.
 * User: abdullah
 * Date: 9/4/18
 * Time: 9:18 PM
 */

namespace App\Http\Controllers\Api\Home;

use App\Http\Controllers\Controller;
use App\Constants;
use App\Http\Controllers\FrontendController;
use App\Http\Requests\Frontend\JobApplyFormRequest;
use App\Models\ExchangeRate;
use App\Models\FavouriteJob;
use App\Models\Project;
use App\Models\ProjectApplicant;
use App\Models\ProjectApplicantAnswer;
use App\Models\ProjectChatFollower;
use App\Models\ProjectChatRoom;
use App\Models\User;
use App\Models\UserInboxMessages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class FavouriteJobApiController extends Controller
{
    public function addFavouriteJob(Request $request)
    {
        $response = array();
        $response['status'] = '';
        $response['message'] = '';

        try {
            $projectId = $request->get('projectId');

            //check whether exist or not
            $count = FavouriteJob::where('user_id', Auth::id())
                ->where('project_id', $projectId)
                ->first();
            if (!is_null($count)) {
                $count->delete();
                $response['status'] = 'Error';
                $response['message'] = trans('common.job_already_exist_in_list');
            } else {
                $favouriteJob = new FavouriteJob();
                $favouriteJob->user_id = Auth::id();
                $favouriteJob->project_id = $projectId;
                $favouriteJob->save();

                $response['status'] = 'OK';
                $response['message'] = trans('common.job_added_into_list');
            }
        } catch (\Exception $exception) {
            $response['status'] = "Error";
            $response['message'] = $exception->getMessage() . " in " . $exception->getFile() . " in " . $exception->getLine();
        }

        return $response;
//        return response()->json([
//            'status' => 'OK',
//            'message' => trans('common.job_added_into_list')
//        ]);
    }
}
