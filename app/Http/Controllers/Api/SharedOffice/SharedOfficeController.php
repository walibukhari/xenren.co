<?php

namespace App\Http\Controllers\Api\SharedOffice;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\ChatRoom;
use App\Models\SharedOffice;
use App\Models\SharedOfficeFinance;
use App\Models\SharedOfficeGroupChat;
use App\Models\WorldCities;
use App\Models\WorldCountries;
use App\Models\WorldStates;
use App\Services\GeneralService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class SharedOfficeController extends Controller
{
    /****************************************************************************************************/
    /******************                      Shared Office Room Chat                       **************/
    /******************             http://www.xenren.co/api/sharedOffice/chat             **************/
    /******************             http://localhost:8000/api/sharedOffice/room/chat       **************/
    /******************                  POST METHOD room_id, message                      **************/

    public function indoorChat(Request $request)
    {
        try {
            $_sharedOfficeGroupChat = new SharedOfficeGroupChat();
            $result = $_sharedOfficeGroupChat->saveMessage($request);
            return collect([
                'status' => 'success',
                'data' => $result
            ]);
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function getMessagesByOfficeId($office_id)
    {
        try {
            $_sharedOfficeGroupChat = new SharedOfficeGroupChat();
            $result = $_sharedOfficeGroupChat->getMessagesByOfficeId($office_id);
            return collect([
                'status' => 'success',
                'data' => $result
            ]);
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }


    public function getChatRooms()
    {
        $data = SharedOffice::with(['totalMember', 'isMember' => function ($q) {
            $q->where('user_id', '=', \Auth::user()->id);
        }])->get();

        return collect([
            'status' => 'success',
            'data' => $data
        ]);
    }

    public function getChatRoomDetail($id)
    {
        $data = SharedOffice::with(['seatPrice', 'totalMember', 'isMember' => function ($q) {
            $q->where('user_id', '=', \Auth::user()->id);
        }])->where('id', '=', $id)->first();

        return collect([
            'status' => 'success',
            'data' => $data
        ]);
    }

    public function getUsers($office_id)
    {
        try {
            $data = SharedOfficeFinance::where('office_id', '=', $office_id)
//                ->whereNull('end_time')
                ->with('user.skills.skill', 'userStatus')
                ->has('user')
                ->get();
            return collect([
                'status' => 'success',
                'data' => $data->unique('user_id')->values()
            ]);
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    public static function get_check_in_users($office_id)
    {
        $checked_in_users = SharedOfficeFinance::where('office_id', '=', $office_id)
            ->whereNull('end_time')
            ->has('user')
            ->count();

        return $checked_in_users;
    }

		public function search(Request $request)
		{
            if (!file_exists(public_path() . "/SharedOfficesJson/sharedoffices_" . $request->lang . ".json")) {
                Artisan::call('generate:sharedofficesearch');
            }

            if(isset($request->lang) && $request->lang == 'cn') {
                $fileName = public_path() . "/SharedOfficesJson/sharedoffices_cn.json";
            } else {
                $fileName = public_path() . "/SharedOfficesJson/sharedoffices_en.json";
            }

            $data = collect(json_decode(file_get_contents($fileName), true));
			$data = collect($data);

			if(isset($request->search ) && $request->search  != '') {
				$arr = array();
				$explode = explode(',',$request->search);
				if(count($explode) >= 2 ) {
                    $cityName = $explode[0];
                    $countryName = $explode[1];
                    $city = WorldCities::where('name', 'like', '%'.$cityName.'%')->with(['country' => function($q) use ($countryName) {
                        $q->where('name', 'like', '%'.$countryName.'%');
                    }])
                        ->whereHas('country', function ($q) use ($countryName){
                            $q->where('name', 'like', '%'.$countryName.'%');
                        })
                        ->limit(5)->get();
                    $country = WorldCountries::where('name', 'like', '%' . $countryName . '%')->first();
                } else {
                    $city = WorldCities::select([
                        'id', 'name', 'country_id'
                    ])->where('name', 'like', '%' . $request->search . '%')->with(['country' => function ($q) {
                        $q->select([
                            'id', 'name', 'capital'
                        ]);
                    }])->limit(5)->get();
                    $country = WorldCountries::where('name', 'like', '%' . $request->search . '%')->first();
                }
				$isCountryOrCity = false;
				$countryOrCityText = '';
				$countryOrCityTextRoute = '';
                $isCountry = false;
                $isCity = false;
				if(!is_null($city) || !is_null($country)) {
					$isCountryOrCity = true;
				}
                if(count($city) > 0) {
                    $isCity = true;
                }
                if(!is_null($country)) {
                    $isCountry = true;
                }
                $cities = array();
				if(count($city) > 0) {
				    foreach ($city as $k => $v) {
                        $countryOrCityText = $v->name . ',' . $v->country->name;
                        $countryOrCityTextRoute = GeneralService::cleanTheLink($v->name) . ',' . GeneralService::cleanTheLink($v->country->name);
                        array_push($cities, array(
                            "countryOrCityText" => $countryOrCityText,
                            "countryOrCityTextRoute" => $countryOrCityTextRoute,
                            "capital" => $v->country->capital
                        ));
                    }
				} else if(!is_null($country)) {
					if($countryOrCityText == '') {
						$countryOrCityText = $country->name;
						$countryOrCityTextRoute = GeneralService::cleanTheLink($country->name);
					} else {
						$countryOrCityText .= ','.$country->name;
						$countryOrCityTextRoute .= ','.GeneralService::cleanTheLink($country->name);
					}
				}
				foreach ($data as $k => $v) {
				    $route = str_replace(' ','-',$v['route']);
					if (strpos(strtolower($v['actual_name']), strtolower($request->search)) !== false) {
						array_push($arr, collect([
							'label' => isset($v['office_location']) && $v['office_location'] != '' ? $v['name'].' - '. $v['office_location'] : $v['name'],
							'value' => $v['name'],
							'id' => $v['name'],
                            'searchByLocation' => false,
							'route' => $route,
							'location' => $v['actual_location'],
							'icon' => 'fa fa-building',
						]));
					} else if (strpos(strtolower($v['actual_location']), strtolower($request->search)) !== false) {
						array_push($arr, collect([
							'label' => isset($v['office_location']) && $v['office_location'] != '' ? $v['name'].' - '. $v['office_location'] : $v['name'],
							'value' => $v['name'],
							'id' => $v['name'],
                            'searchByLocation' => false,
							'route' => $route,
           				    'location' => $v['actual_location'],
							'icon' => 'fa fa-building'
						]));
					} else if (strpos(strtolower($v['office_location']), strtolower($request->search)) !== false) {
						array_push($arr, collect([
							'label' => isset($v['office_location']) && $v['office_location'] != '' ? $v['name'].' - '. $v['office_location'] : $v['name'],
							'value' => $v['name'],
							'id' => $v['name'],
                            'searchByLocation' => false,
							'route' => $route,
           				    'location' => $v['actual_location'],
							'icon' => 'glyphicon glyphicon-map-marker'
						]));
					}
				}
				if($isCity == true) {
				    foreach ($cities as $k => $v) {
                        if (strpos($v['countryOrCityText'], $v['capital']) !== false) {
                           //
                        } else {
                            $routeCountryCity = explode(',',$v['countryOrCityText']);
                            $countryCity = $routeCountryCity[1].'/'.$routeCountryCity[0];
                            $countryCity = str_replace(' ','-',$countryCity);
                            array_unshift($arr, collect([
                                'label' => $v['countryOrCityText'],
                                'value' => $v['countryOrCityText'],
                                'id' => $v['countryOrCityText'],
                                'searchByLocation' => true,
                                'location' => '',
                                'route' => '/' . $request->lang . '/coworking-spaces/' . $countryCity,
                                'icon' => 'glyphicon glyphicon-map-marker',
                            ]));
                        }
                    }
				    foreach ($cities as $k => $v) {
                        if (strpos($v['countryOrCityText'], $v['capital']) !== false) {
                            $routeCountryCity = explode(',',$v['countryOrCityText']);
                            $countryCity = $routeCountryCity[1].'/'.$routeCountryCity[0];
                            $countryCity = str_replace(' ','-',$countryCity);
                            array_unshift($arr, collect([
                                'label' => $v['countryOrCityText'],
                                'value' => $v['countryOrCityText'],
                                'id' => $v['countryOrCityText'],
                                'searchByLocation' => true,
                                'location' => '',
                                'route' => '/' . $request->lang . '/coworking-spaces/' . $countryCity,
                                'icon' => 'glyphicon glyphicon-map-marker',
                            ]));
                        }
                    }
                } else if($isCountry == true) {
                    array_unshift($arr, collect([
                        'label' => $countryOrCityText,
                        'value' => $countryOrCityText,
                        'id' => $countryOrCityText,
                        'searchByLocation' => true,
                        'location' => '',
                        'route' => '/'.$request->lang.'/coworking-spaces/'.$countryOrCityTextRoute,
                        'icon' => 'glyphicon glyphicon-globe',
                    ]));
                } else if($isCountryOrCity == true) {
					array_unshift($arr, collect([
						'label' => $countryOrCityText,
						'value' => $countryOrCityText,
						'id' => $countryOrCityText,
						'searchByLocation' => true,
            			'location' => '',
						'route' => '/'.$request->lang.'/coworking-spaces/'.$countryOrCityTextRoute,
						'icon' => 'glyphicon glyphicon-map-marker',
					]));
				}
			} else {
				$arr = $data;
			}
			$arr = array_slice($arr, 0, 10);
			return collect([
				'status' => 'success',
				'query' => $request->search,
				'data' => $arr
			]);
		}

	public function count(Request $request)
	{
        if($request->type == 'office') {
            $sharedOffice = SharedOffice::where('id','=',$request->office_id)->first();
            $countryId = $sharedOffice->state_id;
            $cityId = $sharedOffice->city_id;
            $city = WorldCities::with('country')->where('id','=',$cityId)->first();
            $countCountry = SharedOffice::where('state_id','=',$countryId)->count();
            $countCity = SharedOffice::where('city_id','=',$cityId)->count();
            return collect([
                'status' => 'success',
                'office_name' => $sharedOffice->office_name,
                'countryName' => isset($city->country->name) ? $city->country->name : '',
                'cityName' => isset($city->name) ? $city->name : '',
                'countCountry' => $countCountry,
                'countCity' => $countCity,
            ]);
        }

		if($request->type == 'state') {
			$state = WorldStates::where('name', 'like', '%'.$request->state_name.'%')->first();
			if(is_null($state)) {
				return collect([
					'status' => 'success',
					'count' => 0
				]);
			} else {
				$count = SharedOffice::where('state_id', '=', $state->id)->count();
				return collect([
					'status' => 'success',
					'count' => $count
				]);
			}
		}
		if($request->type == 'city') {
			$city = WorldCities::with('country')->where('name', 'like', '%'.$request->city_name.'%')->first();
			if(is_null($city)) {
				return collect([
					'status' => 'success',
					'count' => 0
				]);
			} else {
				$countCity = SharedOffice::where('city_id', '=', $city->id)->count();
                $countCountry = SharedOffice::where('state_id', '=', $city->country->id)->count(); //NOTE: state_id = continent_id
				return collect([
					'status' => 'success',
					'countCity' => $countCity,
					'countCountry' => $countCountry,
				]);
			}
		}
		if($request->type == 'country') {
            $country_name = str_replace("%20"," ",$request->country_name);
			$country = WorldCountries::with('continent')->where('name', 'like', '%'.$country_name.'%')->first();
			if(is_null($country)) {
				return collect([
					'status' => 'success',
					'count' => 0
				]);
			} else {
				$countContinent = SharedOffice::where('continent_id', '=', $country->continent_id)->count();
				$countCountry = SharedOffice::where('state_id', '=', $country->id)->count(); //NOTE: state_id = continent_id
				return collect([
					'status' => 'success',
					'countContinent' => $countContinent,
					'countCountry' => $countCountry,
					'country' => $country
				]);
			}
		}
		if($request->type == 'both') {

			$city = WorldCities::where('name', 'like', '%'.$request->city_name.'%')->first();
			$state = WorldStates::where('name', 'like', '%'.$request->state_name.'%')->first();
			$count = 0;
			if(is_null($city)) {
				return collect([
					'status' => 'success',
					'count' => 0
				]);
			} else {
				$count = SharedOffice::where('city_id', '=', $city->id)->count();
			}

			if(is_null($state)) {
				return collect([
					'status' => 'success',
					'count' => 0
				]);
			} else {
				$count += SharedOffice::where('state_id', '=', $state->id)->count();
			}

			return collect([
				'status' => 'success',
				'count' => $count
			]);

		}
	}
}
