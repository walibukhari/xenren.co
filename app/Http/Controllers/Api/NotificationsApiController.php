<?php
/**
 * Created by PhpStorm.
 * User: abdullah
 * Date: 9/6/18
 * Time: 6:23 PM
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\ProjectApplicant;
use App\Models\SharedOfficeBooking;
use App\Models\User;
use App\Models\UserInbox;
use App\Models\UserInboxMessages;
use App\Services\GeneralService;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use mysql_xdevapi\Collection;

class NotificationsApiController extends Controller
{
    public function get_notifications($notification_type)
    {
        $data = array();
        $response = array();
        $response['status'] = '';
//        $response['message'] = '';

        try {
            $notifications = UserInbox::where('to_user_id', '=', Auth::user()->id)
                ->where('type', '=', $notification_type)
                ->select('id AS notification_id', 'created_at AS time', 'users_inbox.*')
                ->get();

            // TODO: Comment this
//            App::setLocale('en');

            for ($a = 0; $a < count($notifications); $a++) {
                $category = $notifications[$a]->type;
                $message = "";

                switch ($category) {
                    case UserInbox::TYPE_PROJECT_APPLICANT_SELECTED:
                        $category = trans('common.applicant_accept');
                        $message = trans('common.congrat_to_selected_user');
                        break;

                    case UserInbox::TYPE_PROJECT_NEW_MESSAGE:
                        $category = trans('common.receive_new_message');
                        break;

                    case UserInbox::TYPE_PROJECT_CONFIRMED_COMPLETED:
                        $category = trans('common.project_completed');
                        $message = trans('common.project_completed');
                        break;

                    case UserInbox::TYPE_PROJECT_SUCCESSFUL_POST_JOB:
                        $category = trans('common.success_job_post_title');
                        break;

                    case UserInbox::TYPE_PROJECT_PEOPLE_COME_IN:
                        // Show nick name
                        $category = trans('common.user_come_in');
                        break;

                    case UserInbox::TYPE_PROJECT_INVITE_YOU:
                        $category = trans('common.invite_you_to_project');
                        $message = trans('common.example_message_to_freelancer');
                        break;

                    case UserInbox::TYPE_PROJECT_ASK_FOR_CONTACT:
                        $category = trans('common.user_ask_for_contact');
                        break;

                    case UserInbox::TYPE_PROJECT_APPLICANT_ACCEPTED:
                        $category = trans('common.applicant_accept');
                        $message = trans('common.congrat_to_selected_user');
                        break;

                    case UserInbox::TYPE_PROJECT_APPLICANT_REJECTED:
                        $category = trans('common.applicant_reject');
                        $message = trans('common.rejected_contact_visibility');
                        break;

                    case UserInbox::TYPE_PROJECT_SEND_OFFER:
                        $category = trans('common.send_offer');
                        $message = trans('common.example_message_to_freelancer');
                        break;

                    case UserInbox::TYPE_PROJECT_AGREE_SEND_CONTACT:
                        $category = trans('common.accept_send_contact');
                        break;

                    case UserInbox::TYPE_PROJECT_REJECT_SEND_CONTACT:
                        $category = trans('common.reject_send_contact');
                        break;

                    case UserInbox::TYPE_PROJECT_INVITE_ACCEPT:
                        $category = trans('common.invite_accept');
                        break;

                    case UserInbox::TYPE_PROJECT_INVITE_REJECT:
                        $category = trans('common.invite_reject');
                        break;

                    case UserInbox::TYPE_PROJECT_ACCEPT_SEND_OFFER:
                        $category = trans('common.accept_send_offer');
                        break;

                    case UserInbox::TYPE_PROJECT_REJECT_SEND_OFFER:
                        $category = trans('common.reject_send_offer');
                        break;

                    case UserInbox::TYPE_CHAT_FROM_APP:
                        $category = trans('common.wechat_notification');
                        break;
                }

                $data[$a]['notification_id'] = $notifications[$a]->notification_id;
                $data[$a]['category'] = $category;
                $data[$a]['message'] = $message;
                $data[$a]['time'] = GeneralService::time2str($notifications[$a]->created_at->format("Y-m-d h:i:s"));
            }

            $response['status'] = 'Success';
            $response['data'] = $data;
        } catch (\Exception $exception) {
            $response['status'] = 'Error';
            $response['message'] = $exception->getMessage() . " in " . $exception->getFile() . " in " . $exception->getLine();
        }

        return $response;
    }

    public function get_all_notification()
    {
        $data = array();
        $response = array();
        $response['status'] = '';
//        $response['message'] = '';

        try {
            $notifications = UserInbox::where('to_user_id', '=', Auth::user()->id)
                ->where('is_trash', '!=', 1)
                ->orWhere('from_user_id', '=', Auth::user()->id)
                ->select('id AS notification_id', 'from_user_id AS sender_id', 'type', 'created_at AS time', 'users_inbox.*')
                ->with('messages')
                ->get();
            $shredOfficeBooking = SharedOfficeBooking::where('client_email', '=', \Auth::user()->email)->with('office')->get();

            // TODO: Comment this
            App::setLocale('en');

            for ($a = 0; $a < count($notifications); $a++) {
                $category = $notifications[$a]->type;
                $message = "";

                switch ($category) {
                    case UserInbox::TYPE_PROJECT_APPLICANT_SELECTED:
                        $category = trans('common.applicant_accept');
                        $message = trans('common.congrat_to_selected_user');
                        break;

                    case UserInbox::TYPE_PROJECT_NEW_MESSAGE:
                        $category = trans('common.receive_new_message');
                        $message = trans('common.receive_new_message');
                        break;

                    case UserInbox::TYPE_PROJECT_CONFIRMED_COMPLETED:
                        $category = trans('common.project_completed');
                        $message = trans('common.project_completed');
                        break;

                    case UserInbox::TYPE_PROJECT_SUCCESSFUL_POST_JOB:
                        $category = trans('common.success_job_post_title');
                        $message = trans('common.success_job_post_title');
                        break;

                    case UserInbox::TYPE_PROJECT_PEOPLE_COME_IN:
                        // Show nick name
                        $category = trans('common.user_come_in');
                        $message = trans('common.new_user_join_discussion_room');
                        break;

                    case UserInbox::TYPE_PROJECT_INVITE_YOU:
                        $category = trans('common.invite_you_to_project');
                        $message = trans('common.example_message_to_freelancer');
                        break;

                    case UserInbox::TYPE_PROJECT_ASK_FOR_CONTACT:
                        $category = trans('common.user_ask_for_contact');
                        $message = trans('common.user_ask_for_contact');
                        break;

                    case UserInbox::TYPE_PROJECT_APPLICANT_ACCEPTED:
                        $category = trans('common.applicant_accept');
                        $message = trans('common.congrat_to_selected_user');
                        break;

                    case UserInbox::TYPE_PROJECT_APPLICANT_REJECTED:
                        $category = trans('common.applicant_reject');
                        $message = trans('common.rejected_contact_visibility');
                        break;

                    case UserInbox::TYPE_PROJECT_SEND_OFFER:
                        $category = trans('common.send_offer');
                        $message = trans('common.example_message_to_freelancer');
                        break;

                    case UserInbox::TYPE_PROJECT_AGREE_SEND_CONTACT:
                        $category = trans('common.accept_send_contact');
                        $message = trans('common.accept_send_contact');
                        break;

                    case UserInbox::TYPE_PROJECT_REJECT_SEND_CONTACT:
                        $category = trans('common.reject_send_contact');
                        $message = trans('common.reject_send_contact');
                        break;

                    case UserInbox::TYPE_PROJECT_INVITE_ACCEPT:
                        $category = trans('common.invite_accept');
                        $message = trans('common.invite_accept');
                        break;

                    case UserInbox::TYPE_PROJECT_INVITE_REJECT:
                        $category = trans('common.invite_reject');
                        $message = trans('common.invite_reject');
                        break;

                    case UserInbox::TYPE_PROJECT_ACCEPT_SEND_OFFER:
                        $category = trans('common.accept_send_offer');
                        $message = trans('common.accept_send_offer');
                        break;

                    case UserInbox::TYPE_PROJECT_REJECT_SEND_OFFER:
                        $category = trans('common.reject_send_offer');
                        $message = trans('common.reject_send_offer');
                        break;

                    case UserInbox::TYPE_CHAT_FROM_APP:
                        $category = trans('common.wechat_notification');
                        $message = trans('common.wechat_notification');
                        break;

                    case UserInbox::TYPE_PROJECT_RECEIVE_OFFER:
                        $category = trans('common.received_offer');
	                      $message = trans('common.received_offer');
                        break;
                }
                $messages = $notifications[$a]->messages;
                $is_read = $messages->where('is_read', '=', 1)->count() > 0 ? true : false;

                $data[$a]['notification_id'] = $notifications[$a]->notification_id;
                $data[$a]['sender_id'] = $notifications[$a]->sender_id;
                $data[$a]['type'] = $notifications[$a]->type;
                $data[$a]['category'] = $category;
                $data[$a]['message'] = $message;
                $data[$a]['is_read'] = $is_read;
                $data[$a]['time'] = GeneralService::time2str($notifications[$a]->created_at->format("Y-m-d h:i:s"));
            }

            $response['status'] = 'Success';
            $response['sharedOfficeBooking'] = $shredOfficeBooking;
            $response['data'] = $data;
        } catch (\Exception $exception) {
            $response['status'] = 'Error';
            $response['message'] = $exception->getMessage() . " in " . $exception->getFile() . " in " . $exception->getLine();
        }

        return $response;
    }

    public function get_received_orders()
    {
        $data = array();
        $response = array();
        $response['status'] = '';
//        $response['message'] = '';

        try {
            // TODO: Comment this
            App::setLocale("en");

            $progressingProjects = ProjectApplicant::where('user_id', '=', \Auth::user()->id)
                ->with(['project' => function ($q) {
                    $q->select(['id', 'name', 'created_at', 'type', 'pay_type', 'status', 'user_id']);
                }, 'project.creator'])
                ->orderBy('id', 'desc')
                ->get();

            for ($a = 0; $a < count($progressingProjects); $a++) {
                $project = $progressingProjects[$a]->project;

                $project_obj = array();
                $project_obj['project_id'] = $project->id;
                $project_obj['project_title'] = $project->name;
                $project_obj['project_date'] = $project->created_at->format("Y-m-d h:i:s");
                $project_obj['project_owner'] = $project->creator->name;

                switch ($project->type) {
                    case Project::PROJECT_TYPE_COMMON:
                        $project_obj["type"] = trans("common.common_project");
                        break;

                    case Project::PROJECT_TYPE_OFFICIAL:
                        $project_obj["type"] = trans("common.official_project");
                        break;
                }

                switch ($project->pay_type) {
                    case Project::PAY_TYPE_HOURLY_PAY:
                        $project_obj["pay_type"] = trans("common.hourly");
                        break;

                    case Project::PAY_TYPE_FIXED_PRICE:
                        $project_obj["pay_type"] = trans("common.fixed_price");
                        break;

                    case Project::PAY_TYPE_MAKE_OFFER:
                        $project_obj["pay_type"] = trans("common.make_offer");
                        break;
                }

                switch ($project->status) {
                    case Project::STATUS_PUBLISHED:
                        $project_obj["status"] = trans("common.published");
                        break;

                    case Project::STATUS_SELECTED:
                        $project_obj["status"] = trans("common.selected");
                        break;

                    case Project::STATUS_HIRED:
                        $project_obj["status"] = trans("common.hired");
                        break;

                    case Project::STATUS_COMPLETED:
                        $project_obj["status"] = trans("common.completed");
                        break;

                    case Project::STATUS_CANCELLED:
                        $project_obj["status"] = trans("common.cancelled");
                        break;
                }

                $data[$a] = $project_obj;
            }

            $response['status'] = 'Success';
            $response['data'] = $data;
        } catch (\Exception $exception) {
            $response['status'] = 'Error';
            $response['message'] = $exception->getMessage() . " in " . $exception->getFile() . " in " . $exception->getLine();
        }

        return $response;
    }

    public function inbox_notification()
    {
        $data = array();
        $response = array();
        $response['status'] = '';
//        $response['message'] = '';

        try {
            // TODO: Comment this
            App::setLocale("en");

            $category = UserInbox::CATEGORY_NORMAL;

            $myInbox = UserInbox::where(function ($query) {
                $query->where('from_user_id', \Auth::id());
                $query->orWhere('to_user_id', \Auth::id());
            })
                ->where('category', $category)
                ->where('is_trash', 0)
                ->with(['messages.fromUser', 'project'])
                ->orderBy('created_at', 'desc')
                ->get();
            $master = collect();
            foreach ($myInbox as $key => $val) {
                $messages = $val->messages;
                if (isset($val->project) && !is_null($val->project)) {
                    $project_name = $val->project->name;
                    foreach ($messages as $k => $msg) {
                        $slave = collect();
                        $slave->put('message_id', $msg->id);
                        $slave->put('message', $msg->custom_message);
                        $slave->put('sender_profile_pic', $msg->fromUser->img_avatar);
                        $slave->put('sender_name', $msg->fromUser->name);
                        $slave->put('project_name', $project_name);
                        $slave->put('status', $msg->is_read);
                        $slave->put('time', $msg->created_at->format("Y-m-d h:i:s"));
                        $master->push($slave);
                    }
                }
            }

//            for ($a = 0; $a < count($myInbox); $a++) {
//                if(isset()){}
//                $messages = $myInbox[$a]->messages;
//                $project_name = $myInbox[$a]->project->name;
//                for ($b = 0; $b < count($messages); $b++) {
//                    $data[$a][$b]['message_id'] = $messages[$b]->id;
//                    $data[$a][$b]['message'] = $messages[$b]->custom_message;
//                    $data[$a][$b]['sender_profile_pic'] = $messages[$b]->fromUser->img_avatar;
//                    $data[$a][$b]['sender_name'] = $messages[$b]->fromUser->real_name;
//                    $data[$a][$b]['project_name'] = $project_name;
//                    $data[$a][$b]['status'] = $messages[$b]->is_read ? true : false;
//                    $data[$a][$a]['time'] = $messages[$b]->created_at->format("Y-m-d h:i:s");
//                }
//            }

            $response['status'] = 'Success';
            $response['data'] = $master;
        } catch (\Exception $exception) {
            $response['status'] = 'Error';
            $response['message'] = $exception->getMessage() . " in " . $exception->getFile() . " in " . $exception->getLine();
        }

        return $response;
    }

    public function get_notification_detail($notification_id)
    {
        $response = array();
        $response['status'] = '';
//        $response['message'] = '';

        try {
            // TODO: Comment this
            App::setLocale("en");
            $id = UserInbox::where('id', '=', $notification_id)
                ->with(['project.creator' => function($q){
                $q->select([
                    'id', 'name', 'img_avatar', 'qq_id', 'skype_id', 'wechat_id', 'handphone_no', 'currency'
                ]);
            },
                    'project.projectFiles' => function($q){
                        $q->select([
                            'id', 'project_id', 'file'
                        ]);
                    },
                    'project.projectQuestions' => function($q){
                        $q->select([
                            'id', 'project_id', 'question'
                        ]);
                    },
                    'project.projectQuestions.applicantAnswer' => function($q){
                        $q->select([
                            'id', 'project_applicant_id', 'project_question_id', 'answer'
                        ]);
                    },
                    'project.projectSkills.skill' => function($q){
                    $q->select([
                        'id', 'name_cn', 'name_en'
                    ]);
                },
                    ])
                ->select(['id', 'project_id', 'type'])
                ->first();

            $myInbox = UserInboxMessages::where('inbox_id', '=', $id->id)
                ->with(['fromUser'])
                ->has('fromUser')
                ->orderBy('id', 'desc')
                ->get();
            \Log::info('******************************************************');
            \Log::info('******************************************************');
            \Log::info($myInbox);
            \Log::info('******************************************************');
            \Log::info('******************************************************');
            $master = collect();
            foreach ($myInbox as $inbox) {
                $slave = collect();
                $sender_name = isset($inbox->fromUser) ? $inbox->fromUser->name : '';
                $sender_id = isset($inbox->fromUser) ? $inbox->fromUser->id : '';
                $sender_profile_pic = isset($inbox->fromUser) ? asset($inbox->fromUser->img_avatar) : '';
                $quote_price = isset($inbox->quote_price) ? $inbox->quote_price : '';
                $message = isset($inbox->custom_message) ? $inbox->custom_message : '';
                $slave->put('sender_name', $sender_name);
                $slave->put('sender_id', $sender_id);
                $slave->put('sender_profile_pic', $sender_profile_pic);
                $slave->put('message', $message);
                $slave->put('quote_price', $quote_price);
                UserInboxMessages::where('id', '=', $inbox->id)->update([
                    'is_read' => 1
                ]);
                $master->push($slave);
            }

//            $data = array();
//            $data["sender_name"] = isset($myInbox->fromUser) ? $myInbox->fromUser->real_name : '';
//            $data["sender_profile_pic"] = isset($myInbox->fromUser) ? asset($myInbox->fromUser->img_avatar) : '';
//            $data["message"] = isset($myInbox->custom_message) ? $myInbox->custom_message : '';


//            $user_inbox_messages = UserInboxMessages::find($notification_id);
//            $myInbox->is_read = 1;
//            $myInbox->save();

            $response["status"] = "success";
            $response["data"] = $master;
            $response["projects"] = $id->project;
            $response["type"] = $id->type;
        } catch (\Exception $exception) {
            $response["status"] = "error";
            $response["message"] = $exception->getMessage() . " in " . $exception->getFile() . " in " . $exception->getLine();
        }

        return $response;
    }

    public function get_system_notifications()
    {
        $data = array();
        $response = array();
        $response['status'] = '';
//        $response['message'] = '';

        try {
            $notifications = UserInbox::where('to_user_id', '=', Auth::user()->id)
                ->where('category', '=', UserInbox::CATEGORY_SYSTEM)
                ->select('id AS notification_id', 'created_at AS time', 'users_inbox.*')
                ->get();

            for ($a = 0; $a < count($notifications); $a++) {
                $category = $notifications[$a]->type;
                $message = "";

                switch ($category) {
                    case UserInbox::TYPE_PROJECT_APPLICANT_SELECTED:
                        $category = trans('common.applicant_accept');
                        $message = trans('common.congrat_to_selected_user');
                        break;

                    case UserInbox::TYPE_PROJECT_NEW_MESSAGE:
                        $category = trans('common.receive_new_message');
                        break;

                    case UserInbox::TYPE_PROJECT_CONFIRMED_COMPLETED:
                        $category = trans('common.project_completed');
                        $message = trans('common.project_completed');
                        break;

                    case UserInbox::TYPE_PROJECT_SUCCESSFUL_POST_JOB:
                        $category = trans('common.success_job_post_title');
                        break;

                    case UserInbox::TYPE_PROJECT_PEOPLE_COME_IN:
                        // Show nick name
                        $category = trans('common.user_come_in');
                        break;

                    case UserInbox::TYPE_PROJECT_INVITE_YOU:
                        $category = trans('common.invite_you_to_project');
                        $message = trans('common.example_message_to_freelancer');
                        break;

                    case UserInbox::TYPE_PROJECT_ASK_FOR_CONTACT:
                        $category = trans('common.user_ask_for_contact');
                        break;

                    case UserInbox::TYPE_PROJECT_APPLICANT_ACCEPTED:
                        $category = trans('common.applicant_accept');
                        $message = trans('common.congrat_to_selected_user');
                        break;

                    case UserInbox::TYPE_PROJECT_APPLICANT_REJECTED:
                        $category = trans('common.applicant_reject');
                        $message = trans('common.rejected_contact_visibility');
                        break;

                    case UserInbox::TYPE_PROJECT_SEND_OFFER:
                        $category = trans('common.send_offer');
                        $message = trans('common.example_message_to_freelancer');
                        break;

                    case UserInbox::TYPE_PROJECT_AGREE_SEND_CONTACT:
                        $category = trans('common.accept_send_contact');
                        break;

                    case UserInbox::TYPE_PROJECT_REJECT_SEND_CONTACT:
                        $category = trans('common.reject_send_contact');
                        break;

                    case UserInbox::TYPE_PROJECT_INVITE_ACCEPT:
                        $category = trans('common.invite_accept');
                        break;

                    case UserInbox::TYPE_PROJECT_INVITE_REJECT:
                        $category = trans('common.invite_reject');
                        break;

                    case UserInbox::TYPE_PROJECT_ACCEPT_SEND_OFFER:
                        $category = trans('common.accept_send_offer');
                        break;

                    case UserInbox::TYPE_PROJECT_REJECT_SEND_OFFER:
                        $category = trans('common.reject_send_offer');
                        break;

                    case UserInbox::TYPE_CHAT_FROM_APP:
                        $category = trans('common.wechat_notification');
                        break;
                }

                $data[$a]['notification_id'] = $notifications[$a]->notification_id;
                $data[$a]['category'] = $category;
                $data[$a]['message'] = $message;
                $data[$a]['time'] = GeneralService::time2str($notifications[$a]->created_at->format("Y-m-d h:i:s"));
            }

            $response['status'] = 'Success';
            $response['data'] = $data;
        } catch (\Exception $exception) {
            $response['status'] = 'Error';
            $response['message'] = $exception->getMessage() . " in " . $exception->getFile() . " in " . $exception->getLine();
        }

        return $response;
    }

    public function trashAll(Request $request)
    {
        // TODO: Comment this
        App::setLocale('en');
        $inboxIds = $request->get('inboxIds');
        try {

            if ($inboxIds != null)
            {
                foreach ($inboxIds as $inboxId)
                {
                    $inbox = UserInbox::find($inboxId);
                    $inbox->is_trash = 1;
                    $inbox->save();
                }
            }

            return makeJSONResponse(true, trans('common.success_move_to_trash'));
        } catch (\Exception $e) {
            return makeJSONResponse(false, $e->getMessage());
        }
    }

    public function sendMessage(Request $request)
    {
        App::setLocale("en");
        try {
            $userInbox = UserInbox::where('id', '=', $request->inbox_id)->first();
            if (isset($userInbox)) {
                \DB::beginTransaction();
                UserInboxMessages::create([
                    'inbox_id' => $userInbox->id,
                    'from_user_id' => Auth::user()->id,
                    'to_user_id' => $request->to_user,
                    'project_id' => $userInbox->project_id,
                    'custom_message' => $request->message,
                    'type' => $request->category
                ]);
                \DB::commit();
                return collect([
                   'status' => 'success',
                   'message' => 'Message Send'
                ]);
            } else {
                return collect([
                    'status' => 'error',
                    'message' => 'User Inbox not found'
                ]);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return collect([
                'status' => 'error',
                'message' => $e->getMessage(). 'at '. $e->getLine()
            ]);
        }
    }

    public function getBookingUserDetail(Request $request)
    {
        try {
            $user_id = $request->user_id;
            $user = User::where('id', '=', $user_id)
                ->select('id', 'email', 'name', 'address', 'handphone_no', 'skype_id', 'wechat_id', 'qq_id', 'img_avatar', 'title')
                ->first();
            if (!is_null($user)) {
                return collect([
                    'status' => 'success',
                    'data' => $user
                ]);
            } else {
                return collect([
                    'status' => 'error',
                    'message' => 'record not available'
                ]);
            }

        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function delBookingUserDetail(Request $request)
    {
        try {
            $sharedOfficeBooking = SharedOfficeBooking::where('user_id', '=', $request->user_id)
                ->where('client_email', '=', $request->client_email)
                ->where('deleted_at', '=', null)
                ->first();
            if (!is_null($sharedOfficeBooking)) {
                \DB::beginTransaction();
                $sharedOfficeBooking->delete();
                \DB::commit();
                return collect([
                    'status' => 'success'
                ]);
            } else {
                return collect([
                    'status' => 'error',
                    'message' => 'record not available'
                ]);
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return collect([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function sendBookingMessage(Request $request)
    {
        try {
            $receiverRecord = User::where('id', '=', $request->send_to)->first();
            $senderEmail = "support@xenren.co";
            $receiverEmail = $receiverRecord->email;
            if (!is_null($receiverRecord)) {
                Mail::send('mails.sharedOfficeBookingReply', array(
                    'messages' => $request->message,
                ), function ($message) use ($senderEmail, $receiverEmail) {
                    $message->from($senderEmail);
                    $message->to($receiverEmail);
                    $message->cc($senderEmail)
                        ->subject(trans('common.shared_office_reply'));
                });
                return collect([
                    'status' => 'success',
                ]);
            } else {
                return collect([
                    'status' => 'error',
                    'message' => 'Client not found'
                ]);
            }
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function awardProject(Request $request)
    {
        \DB::beginTransaction();
        $inboxWithProject = UserInbox::with(['project', 'fromUser', 'toUser', 'firstMessage'])->where('id', $request->inboxId)->first();
        if($inboxWithProject->type == UserInbox::TYPE_PROJECT_APPLICANT_ACCEPTED) {
            return collect([
                'status' => 'failure',
                'message' => 'Already awarded...!'
            ]);
        }
        UserInbox::where('id', $request->inboxId)->update([
            'type' => UserInbox::TYPE_PROJECT_APPLICANT_ACCEPTED
        ]);
        $uim = UserInboxMessages::where('id', $inboxWithProject->firstMessage->id)->first();
        UserInboxMessages::where('id', $inboxWithProject->firstMessage->id)->update([
            'type' => UserInboxMessages::TYPE_PROJECT_APPLICANT_ACCEPTED
        ]);
        ProjectApplicant::create([
            'user_id' => $inboxWithProject->fromUser->id,
            'project_id' => $inboxWithProject->project_id,
            'status' => ProjectApplicant::STATUS_APPLICANT_ACCEPTED,
            'quote_price' => $inboxWithProject->firstMessage->quote_price,
            'about_me' => $inboxWithProject->firstMessage->custom_message,
            'display_order' => ProjectApplicant::DISPLAY_ORDER_PROGRESSING,
            'is_read' => 0,
            'pay_type' => $uim->pay_type,
            'daily_update' => $inboxWithProject->daily_update
        ]);
        //send email
        $senderEmail = "support@xenren.co";
        $senderName = $inboxWithProject->fromUser->getName();
        $receiver = $inboxWithProject->toUser;

        Mail::send('mails.acceptSendOffer', array('user' => $inboxWithProject->toUser), function ($message) use ($senderEmail, $senderName, $receiver) {
            $message->from($senderEmail, $senderName);
            $message->to($receiver->email, $receiver->first_name . ' ' . $receiver->last_name)
                ->subject(trans('common.offer_accepted'));
        });

        \DB::commit();
        return collect([
            'status' => 'success',
            'message' => 'Job awarded'
        ]);
    }

    public function rejectProjectOffer(Request $request)
    {
        try
        {
            App::setLocale("en");
            $receiverId = $request->get('receiverId');
            $inboxId = $request->get('inboxId');

            $user = \Auth::guard('users')->user();
            $receiver = User::find($receiverId);
            $project = Project::where('user_id','=',$user->id)->first();
            $ui = UserInbox::find($inboxId);
            $ui->type = UserInbox::TYPE_PROJECT_REJECT_SEND_OFFER;
            $ui->save();
            //check exist or not
            $count = UserInboxMessages::where('from_user_id', \Auth::guard('users')->user()->id)
                ->where('to_user_id', $receiver->id)
                ->where('inbox_id', $ui->id)
                ->where(function($query){
                    $query->where('type', UserInboxMessages::TYPE_PROJECT_ACCEPT_SEND_OFFER);
                    $query->orWhere('type', UserInboxMessages::TYPE_PROJECT_REJECT_SEND_OFFER);
                })
                ->count();

            if( $count > 0 )
            {
                return Collect([
                    'status' => 'error',
                    'message' => trans('common.send_offer_already_reply')
                ]);
            }

            //send email
            $senderEmail = "support@xenren.co";
            $senderName = $user->getName();

            Mail::send('mails.rejectSendOffer', array( 'user' => $user ,'receiver_name' => $receiver->name , 'project_name' => $project->name ), function($message) use ( $senderEmail, $senderName, $receiver ) {
                $message->from($senderEmail, $senderName);
                $message->to($receiver->email, $receiver->name)
                    ->subject(trans('common.offer_rejected'));
            });

            \DB::beginTransaction();

            $uim = new UserInboxMessages();
            $uim->inbox_id = $ui->id;
            $uim->from_user_id = \Auth::guard('users')->user()->id;
            $uim->to_user_id = $receiver->id;
            $uim->from_staff_id = null;
            $uim->to_staff_id = null;
            $uim->project_id = $ui->project_id;
            $uim->is_read = 0;
            $uim->custom_message = trans('common.reject_send_offer');
            $uim->type = UserInboxMessages::TYPE_PROJECT_REJECT_SEND_OFFER;
            $uim->save();

            \DB::commit();
            return collect([
                'status' => 'success',
                'message' => trans('common.success_reject_send_offer')
            ]);
        }
        catch (\Exception $e)
        {
            \DB::rollback();
            return collect([
                'status' => 'error',
                'message' => $e->getMessage() . 'at '. $e->getLine(),
            ]);
        }
    }
}
