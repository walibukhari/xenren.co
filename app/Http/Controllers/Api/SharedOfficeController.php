<?php

namespace App\Http\Controllers\Api;

use App\Models\ChangeBookingRequest;
use App\Models\SharedOfficeBooking;
use App\Models\SharedOfficeProducts;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserOtherTypeBarcodeScans;
use App\Models\SharedOfficeBarcode;
use App\Models\SharedOfficeFinance;
use Illuminate\Support\Facades\Auth;

class SharedOfficeController extends Controller
{

    public function changeBookingPost(Request $request) {
        $getChangeSeat = SharedOfficeProducts::where('id','=',$request->product_id)->first();
        $getBooking = SharedOfficeBooking::where('id','=',$request->book_id)->first();
        $getCurrantSeat = SharedOfficeProducts::where('category_id','=',$getBooking->category_id)->where('office_id','=',$getBooking->office_id)->first();
        ChangeBookingRequest::create([
            'office_id' => $request->office_id,
            'seat_id' => $request->product_id,
            'user_id' => $request->user_id,
            'check_in' => $request->check_in,
            'check_out' => $request->check_out,
            'book_id' => $request->book_id,
            'currant_seat' => $getCurrantSeat->number,
            'change_seat' => isset($getChangeSeat->number) ? $getChangeSeat->number: ''
        ]);
        return collect([
           'status' => true,
           'message' => 'request send successfully'
        ]);
    }

    public function scanOtherTypeBarCode(Request $request)
    {
        try {
            $barcode = SharedOfficeBarcode::where('shared_office_id', '=', $request->shared_office_id)->with('sharedOffice.officeProducts')
                ->where('type', '=', $request->type)
                ->where('category', '=', $request->category)
                ->first();

            if (is_null($barcode)) {
                return collect([
                    'status' => 'error',
                    'message' => 'invalid barcode..!'
                ]);
            }

            $servicesSharedOffice = new \App\Http\Controllers\Services\SharedOfficeController();
            $data = $servicesSharedOffice->sharedofficeGetCost();
            if ($data['status'] == 'success') {// already using some seat, stop the timer and deduct money
                $data1 = $servicesSharedOffice->sharedofficeStopTimer($request);
            } else {
                $data1 = [];
            }

            $exist = UserOtherTypeBarcodeScans::where('user_id', '=', \Auth::user()->id)->first();

            if(!is_null($exist)) {
                if ($exist->shared_office_barcode_id == $barcode->id) {
                    if($barcode->type == 2) {
                        UserOtherTypeBarcodeScans::where('user_id', '=', Auth::user()->id)->delete();
                    } else {
                        return collect([
                            'status' => 'error',
                            'message' => 'Already using this barcode..!',
                            'data' => $data
                        ]);
                    }
                } else {
                    UserOtherTypeBarcodeScans::where('user_id', '=', \Auth::user()->id)->delete();
                }
            }

//        if(is_null($exist)) {
            try {
                \DB::beginTransaction();
                UserOtherTypeBarcodeScans::create([
                    'user_id' => Auth::user()->id,
                    'shared_office_barcode_id' => $barcode->id
                ]);
                if ($barcode->type == 1) {
                    $finance = SharedOfficeFinance::create([
                        'product_id' => $barcode->id,
                        'office_id' => $request->shared_office_id,
                        'user' => Auth::user()->name,
                        'cost' => null,
                        'start_time' => Carbon::now(),
                        'user_id' => Auth::user()->id,
                        'x' => 0,
                        'y' => 0,
                        'type' => SharedOfficeFinance::TYPE_OTHER_BAR_CODES,
                    ]);
                    $data = SharedOfficeFinance::where('user_id', '=', \Auth::user()->id)
                        ->where('product_id', '=', $barcode->id)
                        ->with('productfinance', 'office.officeProducts')
                        ->first();
                } else if($barcode->type == 2) {
                    $barcode = SharedOfficeBarcode::where('shared_office_id', '=', $request->shared_office_id)
                        ->with([
                            'sharedOffice',
                            'sharedOffice.officeProducts' => function($q){
                                $q->where('category_id', '=', SharedOfficeProducts::CATEGORY_HOT_DESK)
                                ->orwhere('category_id', '=', SharedOfficeProducts::CATEGORY_DEDICATED_DESK)
                                ->orwhere('category_id', '=', SharedOfficeProducts::CATEGORY_MEETING_ROOM);
                            }
                        ])
                        ->where('type', '=', $request->type)
                        ->where('category', '=', $request->category)
                        ->first();
                    $sharedofficeBarcodeFee = array(
                        $barcode->sharedOffice->officeProducts
                            ->groupBy('product_type_name'),
                        $barcode->sharedOffice->sharedOfficeFee,
                    );
                    $data = collect([
                        'office' => $sharedofficeBarcodeFee
                    ]);
                } else {
                    $barcode->sharedOffice->sharedOfficeFee;
                    $data = collect([
                        'office' => $barcode->sharedOffice
                    ]);
                }
                \DB::commit();

                return collect([
                    'status' => 'success',
                    'data' => $data
                ]);
            } catch (\Exception $e) {
                \DB::rollback();
                return collect([
                    'status' => 'error',
                    'data' => $e->getMessage()
                ]);
            }
//        } else {
//            $data = SharedOfficeFinance::where('user_id', '=', \Auth::user()->id)
//                ->where('product_id', '=', $barcode->id)
//                ->with('productfinance', 'office.officeProducts')
//                ->first();
//            return collect([
//                'status' => 'error',
//                'message' => 'Already using this barcode..!',
//                'data' => $data
//            ]);
//        }
        } catch (\Exception $e) {
            return collect($e);
        }
    }

    public function getUserScannerBarCode(Request $request) {
        $exist = UserOtherTypeBarcodeScans::where('user_id', '=', \Auth::user()->id)->with('sharedOfficeBarCode.sharedOffice')->first();
        if(!is_null($exist)){
            $data = SharedOfficeFinance::where('user_id', '=', \Auth::user()->id)
                ->where('product_id', '=', $exist->shared_office_barcode_id)
                ->with('productfinance', 'office')
                ->first();
            return collect([
                'status' => 'success',
                'data' => $data,
            ]);
        } else {
            return collect([
                'status' => 'error',
                'message' => "no barcode scanned"
            ]);
        }
    }

    public function getBookingData(){
        $user_id = Auth::user()->id;
        $bookings = SharedOfficeBooking::with('office.shfacilities','products')->where('user_id','=',$user_id)
            ->withTrashed()
            ->get();
        return collect([
            'status' => true,
            'bookings' => $bookings
        ]);
    }
    public function bookingDetails($id){
        $getDetails = SharedOfficeBooking::with('office.shfacilities','products')->where('id','=',$id)
            ->withTrashed()
            ->first();
        return collect([
            'status' => true,
            'booking_detail' => $getDetails
        ]);
    }

    public function requestCancel($id){
        SharedOfficeBooking::where('id', '=', $id)->update([
            'status' => SharedOfficeBooking::STATUS_CANCELED_CLIENT_SIDE
        ]);
        SharedOfficeBooking::where('id', '=', $id)->delete();
        return collect([
            'status' => 'success',
            'message' => 'Booking canceled...!'
        ]);
    }
}
