<?php

namespace App\Http\Controllers\Api\User;

use App\Logic\UploadFile\UploadFileRepository;
use App\Models\JobPosition;
use App\Models\Language;
use App\Models\Portfolio;
use App\Models\PortfolioFile;
use App\Models\UploadFile;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class LanguagesController extends Controller
{
    public function allLanguages(Request $request)
    {
        $languages = Language::get();
        return collect([
           'status' => 'success',
            'data' => $languages
        ]);
    }
}
