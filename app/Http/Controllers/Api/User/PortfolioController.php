<?php

namespace App\Http\Controllers\Api\User;

use App\Logic\UploadFile\UploadFileRepository;
use App\Models\JobPosition;
use App\Models\Portfolio;
use App\Models\PortfolioFile;
use App\Models\Project;
use App\Models\UploadFile;
use App\Models\User;
use App\Services\GeneralService;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PortfolioController extends Controller
{
    public function __construct(UploadFileRepository $uploadFileRepository)
    {
        $this->uploadFile = $uploadFileRepository;
    }

    public function getMyOpenJobs()
    {
        $jobs = Project::where('user_id', '=', \Auth::user()->id)
            ->where('status', '!=', Project::STATUS_COMPLETED)
            ->orwhere('status', '!=', Project::STATUS_COMPLETED)
            ->get();
        return collect([
            'status' => 'success',
            'data' => $jobs
        ], 200);
    }

    /**
     * Return all portfolios(Portfolio) of loggedin user.
     *
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function index(Request $request) {
        $job_positions = JobPosition::all();

        $user = auth()->guard('users')->user();

        $portfolios = Portfolio::with('files')
            ->where('user_id', $user->id)
//            ->where('job_position_id', $user->job_position_id)
            ->get();

        return collect([
            'status' => 'success',
            'data' => [
                'portfolios' => $portfolios,
                'job_positions' => $job_positions
            ]
        ]);
    }

    /**
     * Update User Image, name & Profession.
     *
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function updateProfile(Request $request) {
    	\Log::info($request->all());
        $usr = \Auth::user();
        if($request->hasFile('image')) {
            $usr->img_avatar = $request->file('image');
        }
        $usr->name = $request->name;
        $usr->
        name = $request->name;
        $usr->job_position_id = $request->job_position_id;
        $usr->save();
        return collect([
            'status' => 'success',
            'user' => \Auth::user()
        ]);
    }

    /**
     * Return portfolios(Portfolio) of loggedin user in given category(JobPosition).
     *
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function byCategory(Request $request) {

        if(is_null($request->id) && $request->id <= 0)
        {
            return collect([
                'status' => 'error',
                'data' => [
                    'errors' => [
                        'id' => 'Provide a valid id'
                    ]
                ]
            ]);
        }

        $job_positions = JobPosition::all();
        $user = auth()->guard('users')->user();

        $portfolios = Portfolio::with('files')
            ->where('user_id', $user->id)
            ->where('job_position_id', $request->input('id'))
            ->get();

        return collect([
            'status' => 'success',
            'data' => [
                'portfolios' => $portfolios,
                'job_positions' => $job_positions
            ]
        ]);
    }


    /**
     * Create portfolio associated with loggedin user.
     *
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function create(Request $request) {
        $validator = \Validator::make($request->all(), [
            'job_position_id' => 'required|exists:job_position,id',
            'title' => 'required|min:1|max:500',
            'url' => 'required',
            'hidUploadedFile' => 'required'
        ]);
        if ($validator->fails()) {
            return collect([
                'status' => 'error',
                'data' => [
                    'errors' => $validator->errors()->first()
                ]
            ]);
        }

        try{

            DB::beginTransaction();

            $portfolio = new Portfolio();
            $portfolio->user_id = auth()->guard('users')->user()->id;
            $portfolio->job_position_id = $request->get('job_position_id');
            $portfolio->title = $request->get('title');
            $portfolio->url = $request->get('url');
            $portfolio->save();
//            dd($request->file('hidUploadedFile'));

            $hidUploadedFiles = $request->file('hidUploadedFile');
            foreach ( $hidUploadedFiles as $file) {
                //copy from file uploaded location to portfolio pic location
                //add record to DB
//                dump(count($hidUploadedFiles));
                $filename = $this->FileUpload($file, '', $portfolio->id );
//                dump($filename);
                $portfolioFile = new PortfolioFile();
                $portfolioFile->portfolio_id = $portfolio->id;
                $portfolioFile->file = $filename;
                $portfolioFile->thumbnail_file = $filename;
                $portfolioFile->save();
//                $res2 = $this->uploadFile->upload($request->toArray());
//                $res = $this->uploadFile->copy( $file, UploadFile::TYPE_PORTFOLIO, $portfolio->id );
//                dump($res2);
//                dump($res);
//
//                //delete the record from uploaded location
//                //remove record from DB
//                $res3 = $this->uploadFile->delete( $file );
//                dump($res3);
            }

            DB::commit();

            return collect([
                'status' => 'success',
                'data' => [
                    'portfolio' => Portfolio::where('id', $portfolio->id)->with('files')->first()
                ]
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return collect([
                'status' => 'error',
                'data' => [
                    'error' => app()->environment('production') ? "Something goes wrong!!!" : $e->getMessage() . ' || ' . $e->getFile(). ' || ' . $e->getLine()
                ]
            ]);
        }
    }

    /**
     * @param $file
     * @param $path
     * @param $folderName
     * @param string $base_folder
     * @return mixed
     */
    public function FileUpload($file, $path, $folderName, $base_folder = 'uploads/portfolio')
    {
        $path = $path . $folderName;
        $destination_folder = base_path('public/' . $base_folder .'/'. $path . '/');
        $oldmask = umask(0);
        self::make_dir($destination_folder, 0777, true);
        $image_name = self::RandomFileName($file, $folderName);
        $file->move($destination_folder, $image_name);
        umask($oldmask);
        return $image_name;
    }


    /** -- Make directory --
     * @param $path
     * @param $permission
     * @param bool $recursive
     * @return bool
     */
    public function make_dir($path, $permission, $recursive = false)
    {
        if (file_exists($path))
            return true;
        return mkdir($path, $permission, $recursive);
    }

    /**
     * @param $file
     * @return string
     */
    public function RandomFileName($file, $name)
    {
        $type = $file->getClientOriginalExtension();
        return $name . '-' . $this->TimeInMilliSec() . '.' . $type;
    }

    /**
     * @return float
     */
    public static function TimeInMilliSec()
    {
        return round(microtime(true) * 1000);
    }


    public function getPortfolio($id)
    {
        try {
            $job_positions = JobPosition::all();
            $portfolios = Portfolio::with('files')
                ->where('user_id', $id)
                ->get();

            return collect([
                'status' => 'success',
                'data' => [
                    'portfolios' => $portfolios,
                    'job_positions' => $job_positions
                ]
            ]);
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

}
