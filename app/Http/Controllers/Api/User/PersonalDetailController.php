<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Api\SharedOffice\SharedOfficeController;
use App\Models\CountryLanguage;
use App\Models\JobPosition;
use App\Models\Portfolio;
use App\Models\Review;
use App\Models\SharedOfficeFinance;
use App\Models\User;
use App\Models\UserLanguage;
use App\Models\UserSkill;
use App\Models\UserSkillComment;
use App\Models\WorldCities;
use App\Models\WorldCountries;
use App\Observers\RegisterCodeNotification;
use App\Traits\CountryAndCityTrait;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PersonalDetailController extends Controller
{
		use CountryAndCityTrait;
    /**
     * return all UserSkillComment associated with logged in User.
     *
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function comments(Request $request)
    {
        $user = auth()->guard('users')->user();
        $adminReviews = UserSkillComment::with(['files', 'staff', 'skills', 'skills.skill'])
            ->where('user_id', '=', $user->id)
            ->get();

        return collect([
            'status' => 'success',
            'data' => [
                'adminReviews' => $adminReviews
            ]
        ]);
    }

    /**
     * return all Review associated with logged in User.
     *
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function reviews(Request $request)
    {
        $user = auth()->guard('users')->user();
        $reviews = Review::with('reviewer_user', 'reviewer_staff', 'reviewee_user', 'project')
            ->where('reviewee_user_id', $user->id)
            ->get();

        return collect([
            'status' => 'success',
            'data' => [
                'reviews' => $reviews,
            ]
        ]);
    }

    /**
     * return rating info of logged in User.
     *
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function ratings(Request $request)
    {
        $user = auth()->guard('users')->user();
        $reviewInfo = $user->getReviewInfo();

        return collect([
            'status' => 'success',
            'data' => [
                'ratings' => $reviewInfo
            ]
        ]);
    }

    /**
     * return rating info of logged in User.
     *
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function userInfo(Request $request)
    {
        $user = auth()->guard('users')->user();
        $reviewInfo = $user->getReviewInfoForAPI();
        $adminReviews = UserSkillComment::with(['files', 'staff'=> function($q){
            $q->select([
                'id', 'username', 'name', 'staff_type', 'title', 'img_avatar'
            ]);
        },
            'skills' => function($q){
                $q->select([
                    'id', 'user_id', 'skill_id', 'experience'
                ]);
            },
            'skills.skill' => function($q){
                $q->select([
                    'id', 'name_cn', 'name_en'
                ]);
            },
            ])
            ->where('user_id', '=', $user->id)
            ->get();
        $reviews = Review::with('reviewer_user', 'reviewer_staff', 'reviewee_user', 'project')
            ->where('reviewee_user_id', $user->id)
            ->get();
        $job_positions = JobPosition::select([
            'id', 'name_cn', 'name_en'
        ])->get();
        $portfolios = Portfolio::with('files')
            ->where('user_id', $user->id)
            ->get();
//        $data = [];
//        foreach (CountryLanguage::all() as $lang) {
//            $data[] = [
//                'id' => $lang->id,
//                'name' => $lang->language->name,
//                'code' => isset($lang->country->code) ? $lang->country->code : '',
//            ];
//        }

        return collect([
            'status' => 'success',
            'data' => [
                'ratings' => $reviewInfo,
                'adminReviews' => $adminReviews,
                'reviews' => $reviews,
//                'languages' => response()->json($data),
                'data' => [
                    'portfolios' => $portfolios,
                    'job_positions' => $job_positions
                ],
            ]
        ]);
    }

    /**
     * Update contact info of logged in User.
     *
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function updateContactInfo(Request $request)
    {
        $model = auth()->guard('users')->user();
        $model->qq_id = $request->get('qq_id');
        $model->wechat_id = $request->get('wechat_id');
        $model->skype_id = $request->get('skype_id');
        $model->handphone_no = $request->get('handphone_no');
        $model->save();

        return collect([
            'status' => 'success',
            'data' => $model
        ]);
    }

    /**
     * Update profile picture(Avatar) of logged in User.
     *
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function updateProfilePicture(Request $request)
    {
        
        $user = auth()->guard('users')->user();
        if ($request->hasFile('avatar')) {
            $extension = $request->file('avatar')->getClientOriginalExtension();
            $size = $request->file('avatar')->getSize();

//            if (in_array(strtolower($extension), ['png', 'jpg', 'jpeg', 'gif']) && $size <= 800000) {
//                $user->img_avatar = $request->file('avatar');
//            } else {
//                return collect([
//                    'status' => 'error',
//                    'data' => [
//                        'errors' => [
//                            'avatar' => "Provide a valid image type!!!"
//                        ]
//                    ]
//                ]);
//            }
            $user->img_avatar = $request->file('avatar');
            $user->save();
            return collect([
                'status' => 'success',
                'data' => [
                    'avatar' => $user->img_avatar
                ]
            ]);
        }

        return collect([
            'status' => 'error',
            'data' => [
                'errors' => [
                    'avatar' => "Provide a valid image type!!!"
                ]
            ]
        ]);
    }

    public function updatePersonalAddress(Request $request)
    {
        $name = $request->nick_name;
        $about = $request->about_me;
        $password = $request->password;
        $qq = $request->qq;
        $weChat = $request->weChat;
        $skype = $request->skype;
        $headphone = $request->headphone;

        $input = [
            'name' => $name,
            'about_me' => $about,
            'qq_id' => $qq,
            'wechat_id' => $weChat,
            'skype_id' => $skype,
            'handphone_no' => $headphone,
            'password' => isset($password) && trim($password) != '' ? bcrypt($password) : NULL,
        ];
        $input = array_filter($input, 'strlen');

        User::where('id', '=', \Auth::user()->id)->update($input);

        return collect([
            'status' => 'success',
        ]);
    }

    public function updateAddress(Request $request)
    {
        if(isset($request->country) && $request->country != '') {
            $c = (object)$request->country['country'];
            $country = self::validateCountry($c);
            $c = (object)$request->country['city'];
            $city = self::validateCity($c, $country);
            $country = $country->id;
            $city = $city->id;
        } else {
            $country = 0;
            $city = 0;
        }
        $address = $request->address;
        $language = explode(',', $request->language);

        User::where('id', '=', \Auth::user()->id)->update([
            'country_id' => $country,
            'city_id' => $city,
            'address' => $address,
        ]);

        if (isset($language) && count($language) > 0) {
            UserLanguage::where('user_id', '=', \Auth::user()->id)->delete();
            foreach ($language as $k => $v) {
	            if (isset($v) && ($v) > 0 && $v != '') {
		            UserLanguage::create([
			            'user_id' => \Auth::user()->id,
			            'language_id' => $v,
		            ]);
	            }
            }
        }

        return collect([
            'status' => 'success',
        ]);
    }

    public function joinedGroups(Request $request)
    {
        $response = array();
        $response['status'] = '';

        try {

            $data = SharedOfficeFinance::where('user_id', '=', \Auth::user()->id)
                ->has('office')
                ->with('office.seatPrice')
                ->get();
            $data = $data->unique('office_id')->values();

            for ($a = 0; $a < count($data); $a++) {
                try {
                    if ($data[$a]['office']->image == null || $data[$a]['office']->image == '') {
                        $data[$a]['office']->image = 'images/logo_3';
                        $data[$a]['office']['checked_in_users'] = SharedOfficeController::get_check_in_users($data[$a]['office']['id']);
                    } else {
                        $data[$a]['office']['checked_in_users'] = SharedOfficeController::get_check_in_users($data[$a]['office']['id']);
                    }
                } catch(\Exception $e) {}
            }

            $response['status'] = 'success';
            $response['data'] = $data;
        } catch (\Exception $exception) {
            $response['status'] = 'Error';
            $response['message'] = $exception->getMessage() . " in " . $exception->getFile() . " in " . $exception->getLine();
        }

        return $response;
    }

    public function updateJobDetail(Request $request)
    {
        $experience_year = $request->experience_year;
        $job_position = $request->job_position;
        $job_title = $request->job_title;
        $skills = explode(',', $request->skills);
        $pay_hourly = $request->pay_hourly;
        $currency = $request->currency; // 1 or 2

	    try {
		    User::where('id', '=', \Auth::user()->id)->update([
			    'experience' => $experience_year,
			    'title' => $job_title,
			    'job_position_id' => $job_position,
			    'hourly_pay' => $pay_hourly,
			    'currency' => $currency
		    ]);

		    if (isset($skills) && count($skills) > 0) {
			    UserSkill::where('user_id', '=', \Auth::user()->id)->delete();
			    foreach ($skills as $k => $v) {
				    UserSkill::create([
					    'skill_id' => $v,
					    'user_id' => \Auth::user()->id,
				    ]);
			    }
		    }

		    return collect([
			    'status' => 'success',
		    ]);
	    } catch (\Exception $e) {
		    return collect([
			    'status' => 'failure',
			    'message' => $e->getMessage()
		    ]);
	    }
    }

    public function fetchCountryCode(Request $request)
    {
        try {
            $id = $request->city_id;
            $country = WorldCities::where('id', '=', $id)->first();
            if (!is_null($country)) {
                $country_code = WorldCountries::where('id', '=', $country->country_id)->first();
                return collect([
                    'status' => 'success',
                    'callingCode' => $country_code->callingcode
                ]);
            } else {
                return collect([
                    'status' => 'error',
                    'message' => 'Country Code not found'
                ]);
            }
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

}
