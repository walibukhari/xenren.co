<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libraries\PayPal_PHP_SDK\PayPalTrait;
use App\Models\TransactionDetail;
use App\Models\UserPaymentMethod;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Transaction;
use App\Models\User;
use App\Jobs\CheckPayPalWithdrawStatus;

class WithdrawController extends Controller {

    use PayPalTrait;

    public function withdrawApi(Request $request)
    {
    	  if(!isset($request->amount) || $request->amount < 1) {
    	  	return collect([
    	  		'status' => 'error',
			      'message'=> 'Please enter valid amount...!'
		      ]);
	      }
    	
        $data = UserPaymentMethod::where('user_id', '=', \Auth::user()->id)
            ->where('payment_method', '=', TransactionDetail::PAYMENT_METHOD_PAYPAL)
            ->first();
        if(is_null($data)) {
            return collect([
                'status' => 'error',
                'message' => 'Please add account with PayPal to withdraw...!'
            ]);
        }

        $request->request->add([
            'email' => $data->email
        ]);
        $tD = $this->withdraw($request);
        \DB::beginTransaction();
        $newBalance = Auth::user()->account_balance - $request->amount;
        $transaction = Transaction::create([
            'name' => 'Amount Withdraw',
            'amount' => $request->amount,
            'milestone_id' => 0,
            'due_date' => Carbon::now()->addDay(10),
            'comment' => 'Amount Withdrawal of ' . $request->amount,
            'balance_before' => Auth::user()->account_balance,
            'balance_after' => $newBalance,
            'performed_by_balance_before' => Auth::user()->account_balance,
            'performed_by_balance_after' => $newBalance,
            'performed_to_balance_before' => Auth::user()->account_balance,
            'performed_to_balance_after' => $newBalance,
            'performed_by' => Auth::user()->id,
            'performed_to' => Auth::user()->id,
            'type' => Transaction::TYPE_WITHDRAW,
            'currency' => 'USD',
            'release_at' => Carbon::now(),
            'status' => Transaction::STATUS_IN_PROGRESS_TRANSACTION
        ]);
        $td = TransactionDetail::create([
            'transaction_id' => $transaction->id,
            'payment_method' => TransactionDetail::PAYMENT_METHOD_PAYPAL,
            'raw' => json_encode($tD),
        ]);
        User::where([
            'id' => Auth::user()->id
        ])->update([
            'account_balance' => $newBalance
        ]);
        $job = (new CheckPayPalWithdrawStatus($request->all(), $tD, Auth::user()->id, $transaction->id))->delay(60 * 10)->onQueue('PayPalWithdrawStatus'); //delayed for 10 mins
        $this->dispatch($job);
        \DB::commit();
        return collect([
            'status' => 'success',
            'userBalance' => Auth::user()->account_balance,
            'message' => 'Your withdrawal is in process...!'
        ]);

    }
}
