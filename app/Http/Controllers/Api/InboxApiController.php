<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use App\Models\UserInboxMessages;
use App\Models\UserInbox;
use App\Models\ProjectApplicant;
use Illuminate\Support\Facades\App;

class InboxApiController extends Controller
{
    public function acceptOffer(Request $request)
    {
        try {
            App::setLocale('en');
            $receiverId = $request->get('receiverId');
            $inboxId = $request->get('inboxId');
            $user = \Auth::guard('users')->user();
            $receiver = User::find($receiverId);
            $ui = UserInbox::find($inboxId);
            $dt = UserInboxMessages::where('inbox_id', '=', $ui->id)->orderBy('id', 'desc')->select(['id', 'quote_price'])->first();
            $count = UserInboxMessages::where('from_user_id', \Auth::guard('users')->user()->id)
                ->where('to_user_id', $receiver->id)
                ->where('inbox_id', $ui->id)
                ->where(function($query){
                    $query->where('type', UserInboxMessages::TYPE_PROJECT_ACCEPT_SEND_OFFER);
                    $query->orWhere('type', UserInboxMessages::TYPE_PROJECT_REJECT_SEND_OFFER);
                })
                ->count();

            if( $count > 0 )
            {
                return collect([
                   'status' => 'error',
                   'message' => trans('common.send_offer_already_reply'),
                ]);
            }
            $senderEmail = "support@xenren.co";
            $senderName = $user->getName();

            Mail::send('mails.acceptSendOffer', array('user' => $user), function ($message) use ($senderEmail, $senderName, $receiver) {
                $message->from($senderEmail, $senderName);
                $message->to($receiver->email, $receiver->first_name . ' ' . $receiver->last_name)
                    ->subject(trans('common.offer_accepted'));
            });
            \DB::beginTransaction();

            $uim = new UserInboxMessages();
            $uim->inbox_id = $ui->id;
            $uim->from_user_id = \Auth::guard('users')->user()->id;
            $uim->to_user_id = $receiver->id;
            $uim->from_staff_id = null;
            $uim->to_staff_id = null;
            $uim->project_id = $ui->project_id;
            $uim->is_read = 0;
            $uim->type = UserInboxMessages::TYPE_PROJECT_ACCEPT_SEND_OFFER;
            $uim->save();

            $ui->type = UserInbox::TYPE_PROJECT_APPLICANT_ACCEPTED;
            $ui->save();

            ProjectApplicant::create([
                'user_id' => $ui->to_user_id,
                'project_id' => $ui->project_id,
                'status' => ProjectApplicant::STATUS_APPLICANT_ACCEPTED ,
                'quote_price' => isset($dt) ? 0 : $dt->quote_price,
                'about_me' => 'N/A',
                'display_order' => ProjectApplicant::DISPLAY_ORDER_PROGRESSING,
                'is_read' => 0,
                'daily_update' => $ui->daily_update
            ]);

            \DB::commit();

            return collect([
                'status' => 'success',
                'message' => trans('common.success_accept_send_offer')
            ]);

        } catch(\Exception $e) {
            \DB::rollback();
            return collect([
                'status' => 'error',
                'message' => $e->getMessage(), 'at ', $e->getLine()
            ]);
        }
    }

    public function rejectOffer(Request $request)
    {
        try {
            App::setLocale('en');
            $receiverId = $request->get('receiverId');
            $inboxId = $request->get('inboxId');

            $user = \Auth::guard('users')->user();
            $receiver = User::find($receiverId);
            $ui = UserInbox::find($inboxId);
            $count = UserInboxMessages::where('from_user_id', \Auth::guard('users')->user()->id)
                ->where('to_user_id', $receiver->id)
                ->where('inbox_id', $ui->id)
                ->where(function($query){
                    $query->where('type', UserInboxMessages::TYPE_PROJECT_ACCEPT_SEND_OFFER);
                    $query->orWhere('type', UserInboxMessages::TYPE_PROJECT_REJECT_SEND_OFFER);
                })
                ->count();

            if( $count > 0 )
            {
                return collect([
                    'status' => 'error',
                    'message' => trans('common.send_offer_already_reply')
                ]);
            }
            $senderEmail = "support@xenren.co";
            $senderName = $user->getName();

            Mail::send('mails.rejectSendOffer', array( 'user' => $user ), function($message) use ( $senderEmail, $senderName, $receiver ) {
                $message->from($senderEmail, $senderName);
                $message->to($receiver->email, $receiver->first_name.' '. $receiver->last_name )
                    ->subject(trans('common.contact_info'));
            });

            \DB::beginTransaction();

            $uim = new UserInboxMessages();
            $uim->inbox_id = $ui->id;
            $uim->from_user_id = \Auth::guard('users')->user()->id;
            $uim->to_user_id = $receiver->id;
            $uim->from_staff_id = null;
            $uim->to_staff_id = null;
            $uim->project_id = $ui->project_id;
            $uim->is_read = 0;
            $uim->type = UserInboxMessages::TYPE_PROJECT_REJECT_SEND_OFFER;
            $uim->save();

            \DB::commit();
            return collect([
                'status' => 'success',
                'message' => trans('common.success_reject_send_offer')
            ]);

        } catch (\Exception $e) {
            \DB::rollback();
            return collect([
                'status' => 'error',
                'message' => $e->getMessage(), 'at ', $e->getLine()
            ]);
        }
    }

    public function trash(Request $request)
    {
        App::setLocale('en');
        $inboxId = $request->get('inboxId');

        try {
            $inbox = UserInbox::find($inboxId);
            if($inbox) {
                \DB::beginTransaction();

                $inbox->is_trash = 1;
                $inbox->save();

                \DB::commit();
                return collect([
                    'status' => 'success',
                    'message' => trans('common.success_move_to_trash')
                ]);
            } else {
                return collect([
                    'status' => 'error',
                    'message' => 'inbox Chat not found'
                ]);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            return collect([
                'status' => 'error',
                'message' => $e->getMessage(). 'at '. $e->getLine()
            ]);
        }
    }
}
