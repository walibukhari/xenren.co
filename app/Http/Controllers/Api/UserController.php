<?php

namespace App\Http\Controllers\Api;

use App\Models\ReportedUser;
use App\Models\Review;
use App\Models\Skill;
use App\Models\SkillKeyword;
use App\Models\User;
use App\Models\UserLanguage;
use App\Models\UserSkill;
use App\Models\UserSkillComment;
use App\Models\UserSkillCommentSkills;
use App\Models\UserStatus;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function update(Request $request)
    {
        try {
            $model = \Auth::guard('users')->user();
            if ($request->hasFile('avatar')) {
                $extension = $request->file('avatar')->getClientOriginalExtension();
                $size = $request->file('avatar')->getSize();

                if (in_array(strtolower($extension), ['png', 'jpg', 'jpeg', 'gif']) && $size <= 800000) {
                    $model->img_avatar = $request->file('avatar');
                } else {
                    return makeResponse(trans('member.avatar_image_type'), true);
                }
            }

            //check email address is it already exist in DB
            $email = $request->get('email');
            $count = User::where('email', $email)->count();
            if( $count > 1 )
            {
                return makeResponse(trans('common.email_has_been_registered'), true);
            }

//            $model->real_name = $request->get('real_name');
            $model->name = $request->get('nick_name');
            $model->about_me = $request->get('about_me');
            $model->experience = $request->get('experience');
            $model->job_position_id = $request->get('job_position_id');
            $model->title = $request->get('title');
            $model->address = $request->get('address');
            $model->hourly_pay = $request->get('hourly_pay');
            $model->email = $request->get('email');
            $model->qq_id = $request->get('qq_id');
            $model->wechat_id = $request->get('wechat_id');
            $model->skype_id = $request->get('skype_id');
            $model->handphone_no = $request->get('handphone_no');
            $model->country_id = $request->get('country_id');
            $model->city_id = $request->get('city_id');
            $model->currency = $request->get('currency');

            //user didn't input, then put 2.8 /USD HOUR as default
            if( $model->hourly_pay == 0 || $model->hourly_pay == "")
            {
                $model->hourly_pay = 2.8;
                $model->currency = User::CURRENCY_USD;
            }

            $model->save();

            if( $model->id >= 1){
                UserSkill::where('user_id', $model->id)->delete();
                $skillIDList = explode(",", $request->get('skill_id_list'));
                foreach($skillIDList as $skillID){
                    $check = Skill::find($skillID);
                    if ($check) {
                        //due to when update the user skill, all the skill delete
                        //so need to reinsert the past experience
                        $totalExperience = UserSkillCommentSkills::where('user_id', Auth::guard('users')->user()->id)
                            ->where('skill_id', $skillID)
                            ->sum('experience');

                        if( $totalExperience == null )
                        {
                            $totalExperience = 0;
                        }

                        $userSkill = new UserSkill();
                        $userSkill->user_id = $model->id;
                        $userSkill->skill_id = trim($skillID);
                        $userSkill->experience = $totalExperience;
                        $userSkill->save();
                    } else {
                        $skill = new SkillKeyword();
                        $skill->name_en = $skillID;
                        $skill->user_id = $model->id;
                        $skill->save();
                    }
                }

                UserLanguage::where('user_id', $model->id)->delete();
                $lang_ids = explode(",", $request->get('lang_id_list'));
                foreach ($lang_ids as $lang) {
                    $userLanguage = new UserLanguage();
                    $userLanguage->user_id = $model->id;
                    $userLanguage->language_id = trim($lang);
                    $userLanguage->save();
                }
            }

            addSuccess(trans('member.successfully_update_personal_information'));
            return makeResponse(trans('member.successfully_update_personal_information'));
        } catch (\Exception $e) {
            return makeResponse($e->getMessage(), true);
        }
    }

    public function reportUser(Request $request)
    {
        try {
            $_report = new ReportedUser();
            $report = $_report
                ->where('reported_by', '=', \Auth::user()->id)
                ->where('reported_user_id', '=', $request->user_id)->first();
            if(isset($report)){
                return collect([
                    'status' => 'error',
                    'message' => 'Already reported this user!'
                ]);
            }
            $result = $_report->reportUser($request);
            return collect([
                'status' => 'success',
                'message' => 'User reported successfully!'
            ]);
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * return all UserSkillComment associated with logged in User.
     *
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function platformReviews($id)
    {
        $adminReviews = UserSkillComment::with(['files', 'staff', 'skills', 'skills.skill'])
            ->where('user_id', '=', $id)
            ->where('is_official', '=', 1)
            ->get();

        return collect([
            'status' => 'success',
            'data' => [
                'reviews' => $adminReviews
            ]
        ]);
    }

    /**
     * return all Review associated with logged in User.
     *
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function customerReviews($id)
    {
//        $reviews = Review::with('reviewer_user', 'reviewer_staff', 'reviewee_user', 'project')
//            ->where('reviewee_user_id', $id)
//            ->get();
//
//        return collect([
//            'status' => 'success',
//            'data' => [
//                'reviews' => $reviews,
//            ]
//        ]);
        $reviews = UserSkillComment::with(['files', 'staff', 'skills', 'skills.skill'])
            ->where('user_id', '=', $id)
            ->where('is_official', '=', 0)
            ->get();

        return collect([
            'status' => 'success',
            'data' => [
                'reviews' => $reviews
            ]
        ]);
    }
}
