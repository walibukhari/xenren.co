<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\API\PostChatRequest;
use App\Models\UserInbox;
use App\Models\UserInboxMessages;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ChatController extends Controller
{
	
	protected $userInbox;
	
	function __construct()
	{
		$this->userInbox = new UserInboxMessages();
	}
	
	public function getChat($opponentUserId)
	{
		$inboxId = UserInbox::where('to_user_id', '=', $opponentUserId)->where('from_user_id', '=', \Auth::user()->id)->select(['id'])->first();
		if(isset($inboxId)) {
			$chat = UserInboxMessages::where('inbox_id', '=', $inboxId->id)->with(['fromUser', 'toUser'])->whereNotNull('custom_message')->get();
			return collect([
				'status' => 'success',
				'data' => $chat
			]);
		} else {
			return collect([
				'status' => 'success',
				'data' => []
			]);
		}
	}
	
	public function postChat(Request $request, $opponentUserId)
	{
		$postChatObj = new PostChatRequest();
		$validator = Validator::make($request->all(), $postChatObj->rules(), $postChatObj->messages());
		if ($validator->fails()) {
			return collect([
				'status' => 'error',
				'message' => $validator->errors()->first()
			]);
		}
		
		$inboxId = UserInbox::where('to_user_id', '=', $opponentUserId)->where('from_user_id', '=', \Auth::user()->id)->select(['id'])->first();
		if(!isset($inboxId)) {
			$inboxId = UserInbox::create([
				'to_user_id' => $opponentUserId,
				'from_user_id' => \Auth::user()->id,
				'category' => UserInbox::CATEGORY_NORMAL,
				'type' => UserInbox::TYPE_CHAT_FROM_APP
			]);
		}
		UserInboxMessages::create([
			'inbox_id' => $inboxId->id,
			'from_user_id' => Auth::user()->id,
			'to_user_id' => $opponentUserId,
			'type' => UserInboxMessages::TYPE_CHAT_FROM_APP,
			'custom_message' => $request->message
		]);
		return self::getChat($opponentUserId);
	}
	
	
}
