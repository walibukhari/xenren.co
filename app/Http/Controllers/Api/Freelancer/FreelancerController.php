<?php

namespace App\Http\Controllers\Api\Freelancer;

use App\Models\FavouriteFreelancer;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FreelancerController extends Controller
{
	public function favoriteFreelancer($freelancerId)
	{
		FavouriteFreelancer::updateOrCreate([
			'user_id' => Auth::user()->id,
			'freelancer_id' => $freelancerId
		],[
			'user_id' => Auth::user()->id,
			'freelancer_id' => $freelancerId
		]);
		return collect([
			'status' => 'success',
			'message' => 'Freelancer added as favourite'
		]);
	}
}
