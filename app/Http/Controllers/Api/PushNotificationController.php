<?php

namespace App\Http\Controllers\Api;

use App\Models\PushNotificationCountrySettings;
use App\Models\PushNotificationKeywordSettings;
use App\Models\PushNotificationLanguageSettings;
use App\Models\PushNotificationSettings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PushNotificationController extends Controller
{
    /**
     * Protected members of the class
     * */
    protected $pushNotificationSettings;
    protected $pushNotificationKeywordSettings;
    protected $pushNotificationLanguageSettings;
    protected $pushNotificationCountrySettings;

    /**
     * PushNotificationController constructor.
     */
    public function __construct()
    {
        $this->pushNotificationSettings = new PushNotificationSettings();
        $this->pushNotificationKeywordSettings = new PushNotificationKeywordSettings();
        $this->pushNotificationLanguageSettings = new PushNotificationLanguageSettings();
        $this->pushNotificationCountrySettings = new PushNotificationCountrySettings();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Support\Collection
     */
    public function create(Request $request)
    {
        try {
            \DB::beginTransaction();
            $request->request->add([
               'user_id' => \Auth::user()->id
            ]);
            $exist = $this->pushNotificationSettings->getUserSettings($request->user_id);
            $id = isset($exist->id) ? $exist->id : $request->user_id;
            if(is_null($exist)) {
                $setting = $this->pushNotificationSettings->createSettings($request->all());
            } else {
                $setting = $this->pushNotificationSettings->updateSettings($request->all(), $exist->id);
                $this->pushNotificationKeywordSettings->deleteRecordBySettingId($exist->id);
                $this->pushNotificationLanguageSettings->deleteRecordBySettingId($exist->id);
                $this->pushNotificationCountrySettings->deleteRecordBySettingId($exist->id);
            }
            $keywords = isset($request->keyword) ? explode(',', $request->keyword) : null;
            if(!is_null($keywords)) {
                foreach ($keywords as $k => $v) {
                    $this->pushNotificationKeywordSettings->createRecord([
                        'push_notification_settings_id' => $id,
                        'keyword_id' => $v,
                    ]);
                }
            }
            $languages = isset($request->language) ? explode(',', $request->language) : null;
            if(!is_null($languages)) {
                foreach ($languages as $k => $v) {
                    $this->pushNotificationLanguageSettings->createRecord([
                        'push_notification_settings_id' => $id,
                        'language_id' => $v,
                    ]);
                }
            }
            $country = isset($request->country) ? explode(',', $request->country) : null;
            if(!is_null($country)) {
                foreach ($country as $k => $v) {
                    $this->pushNotificationCountrySettings->createRecord([
                        'push_notification_settings_id' => $id,
                        'country_id' => $v,
                    ]);
                }
            }
            \DB::commit();
            return self::get($request);
        } catch (\Exception $e) {
            \DB::rollback();
            return collect([
                'status' => 'failure',
                'message' => $e->getMessage() . 'at ' . $e->getFile().':'.$e->getLine()
            ]);
        }
    }

    public function get(Request $request)
    {
        $total_free_notifications = PushNotificationSettings::FREE_NOTIFICATIONS;
        $remaining_notifications = \Auth::user()->free_notifications;
        $setting = collect($this->pushNotificationSettings->getUserSettings(\Auth::user()->id));
        $setting->put('total_free_notifications',  $total_free_notifications);
        $setting->put('remaining_notifications',  $remaining_notifications);
        return collect([
           'status' => 'success',
            'data' => $setting,
        ]);
    }
}
