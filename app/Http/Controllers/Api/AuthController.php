<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Constants;
use App\Http\Controllers\UtilityController;
use App\Http\Requests;
use App\Models\Countries;
use App\Models\GuestRegister;
use App\Models\Invitation;
use App\Models\Project;
use App\Models\ProjectApplicant;
use App\Models\Review;
use App\Models\SharedOfficeFinance;
use App\Models\TimeTrack;
use App\Models\User;
use App\Models\UserInboxMessages;
use App\Models\UserSkill;
use App\Models\UserSkillComment;
use App\Models\UserStatus;
use App\Models\UserToken;
use App\Notifications\ResetPasswordEmail;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    public function registerWithFacebook(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required',
                'facebook_id' => 'required',
                'real_name' => 'required'
            ]);
            if ($validator->fails()) {
                return collect([
                    'status' => 'error',
                    'message' => $validator->errors()->first()
                ]);
            }

            $user = User::where('facebook_id', $request->facebook_id)
                    ->orWhere('email', '=', $request->email)
                    ->first();
            if (!isset($user) || is_null($user)) {
                 $user = User::create([
                    'name' => $request->real_name,
                    'email' => $request->email,
                    'password' => '',
                    'facebook_id' => $request->facebook_id,
                    'is_validated' => 2
                ]);
            }
            try {
                $token = JWTAuth::fromUser($user);
            } catch (\Exception $e) {
                dd($e);
            }
            return $this->respondWithToken($token, $user->id);
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage() . ' in ' . $e->getFile() . ' in ' . $e->getLine()
            ]);
        }
    }

		public function users(Request $request)
		{
			$users = User::where('name', 'like', '%'.$request->search.'%')->select([
				'name as text', 'id as id'
			])->get();
			return collect([
				'data' => $users
			]);
		}

    public function set_password(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'password' => 'sometimes|min:6',
            ]);
            $password = $request->get('password');
            if ($validator->fails()) {
                return collect([
                    'status' => 'error',
                    'message' => "Please provide at least six characters"
                ]);
            } else {
                \DB::beginTransaction();
                User::where("id", "=", \Auth::user()->id)
                    ->update(
                        [
                            'password' => bcrypt($request->get('password')),
                            'hint' => self::getHint($password)
                        ]
                    );
                \DB::commit();
            }

            return collect([
                'status' => 'success',
                'message' => 'Password has been updated'
            ]);
        } catch (\Exception $e) {
            \DB::rollback();
            return collect([
                'status' => 'error',
                'message' => $e->getMessage() . ' in ' . $e->getFile() . ' in ' . $e->getLine()
            ]);
        }
    }

    public function BindFacebook(Request $request)
    {
        /**
         * 1- User registered with email
         *      - after register mobile app with call a post api sending fb_id
         *      - find authenticated user and save fb_id
         **/

        try {
            User::where('id', "=", Auth::user()->id)
                ->update(["facebook_id" => $request->get('facebook_id')]);

            return collect([
                'status' => 'success',
                'message' => 'Facebook has been bind'
            ]);
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage() . ' in ' . $e->getFile() . ' in ' . $e->getLine()
            ]);
        }
    }

    public function createProviderFacebook($member)
    {
        try {
            $account = User::where('facebook_id', $member->fb_id)->first();
            if (!$account && Auth::user()) {
                User::where('id', '=', Auth::user()->id)->update([
                    'facebook_id' => $member->fb_id
                ]);

                return collect([
                    'account' => $account,
                    'status' => 0
                ]);
            }

            if ($account) {
                $account->facebook_id = $member->fb_id;
                $account->img_avatar = $member->avatar;
                $account->name = $member->name;
                $account->email = $member->email;
                $account->save();

                return collect([
                    'account' => $account,
                    'status' => 1
                ]);
            } else {
                $account = new User();
                $account->facebook_id = $member->fb_id;
                $account->img_avatar = $member->avatar;
                $account->name = $member->name;
                $account->email = $member->email;
                $account->save();

                return collect([
                    'account' => $account,
                    'status' => 2
                ]);
            }
        } catch (Exception $exception) {
            return collect([
                'status' => 'Error',
                'data' => $exception->getMessage() . ' in ' . $exception->getFile() . ' in ' . $exception->getLine(),
                'account' => null
            ]);
        }
    }

    public function login(Request $request)
    {
        $checkCred = User::where('email','=',$request->email)->first();
        try {
            if (!$token = JWTAuth::attempt($request->all())) {
                if(is_null($checkCred)){
                    return response()->json([
                        'success' => 'false',
                        'message' => 'wrong email'
                    ]);
                } else if(Hash::check($request->password,$checkCred['password']) === false) {
                    return response()->json([
                        'success' => 'false',
                        'message' => 'wrong password'
                    ], 401);
                } else {
                    return response()->json([
                        'success' => 'false',
                        'message' => 'We cant find an account with this credentials. Please make sure you entered the right information and you have verified your email address.'
                    ], 401);
                }
            }
        } catch (JWTException $e) {
            return response()->json(['success' => false, 'error' => 'Failed to login, please try again.', 'message' => $e->getMessage()], 500);
        }

        return $this->respondWithToken($token);

    }

		public function logout(Request $request)
		{
			User::where('id', '=', \Auth::user()->id)->update([
				'device_token' => null,
				'device_os' => null
			]);
			return collect([
				'status' => 'success'
			]);
		}

    public function me()
    {
        $_user = new User();
        $data = $_user
            ->where('id', '=', \Auth::user()->id)
            ->with([
            	  'city.locale' => function($q){
		              $q->where('locale', '=', 'zh-cn');
	              },
            	  'country.locale' => function($q){
		              $q->where('locale', '=', 'zh-cn');
	              },
                'job_position' => function($q){
                    $q->select([
                        'id', 'name_cn', 'name_en', 'remark_cn', 'remark_en'
                    ]);
                },
                'skills'  => function($q){
                    $q->select([
                        'id', 'user_id', 'skill_id', 'experience'
                    ]);
                },
                'skills.skill' => function($q){
                    $q->select([
                        'id', 'name_cn', 'name_en'
                    ]);
                },
                'status',
                'languages',
                'languages.languageDetail',
            ])->select([
                'id', 'initial_display', 'img_avatar', 'name', 'qq_id', 'wechat_id', 'skype_id', 'handphone_no', 'job_position_id', 'about_me', 'city_id', 'country_id', 'address', 'account_balance', 'xen_coin','email', 'is_validated', 'title', 'currency', 'experience', 'hourly_pay'])->first();
//        Check if User has any seat active in any shared office
        $transaction = new SharedOfficeFinance();
        $shared_office = $transaction->with('productfinance', 'office')
            ->whereHas('productfinance', function ($q) {
                $q->where('status_id', '=', 1);
            })
            ->where('user_id', '=', \Auth::user()->id)
            ->whereNull('end_time')
            ->first();
        try {
            $name = $shared_office->office->office_name;
            $number = (int)$shared_office->productfinance->number;
            $shared_office['office_name'] = $name;
        } catch (\Exception $exception) {
            $number = null;
        }

        $img = isset($data->img_avatar) && $data->img_avatar != '' ? \URL::to('/') . '/' . $data->img_avatar : '';
        $data->img_avatar = $img;
        $minutes = '';
        $totalCost= '';
        if ($shared_office != null) {
            $time_price = $shared_office->productfinance->time_price;
            $minutes = \Carbon\Carbon::parse($shared_office->start_time)->diffInMinutes(\Carbon\Carbon::now());
            $value = $minutes / 10;
            $total_time = ceil($value);
            $totalCost = $time_price * $total_time;
        }
        return collect([
            'status' => 'success',
            'data' => $data,
            'minutes' => $minutes,
            'time_price' => $totalCost,
            'shared_office' => $shared_office,
            'seat_number' => $number,
            'balance' => \Auth::user()->account_balance
        ]);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token, $id=false)
    {
        $_user = new User();
//        $user_id=Request::get('user_id');
        $user = Auth::guard('users')->user();
        if(isset($id) && $id != 0) {
            $user = User::find($id);
        }
        $user->last_login_at = Carbon::now();
        $user->save();
        $transaction = new SharedOfficeFinance();
        $data = $transaction->with('productfinance', 'office')
            ->whereHas('productfinance', function ($q) {
                $q->where('status_id', '=', 1);
            })
            ->where('user_id', '=', $user->id)
            ->whereNull('end_time')
            ->first();

        try {
            $name = $data->office->office_name;
            $number = (int)$data->productfinance->number;
            $data['office_name'] = $name;
        } catch (\Exception $exception) {
            $number = null;
        }
        $userData = $_user
            ->where('id', '=', $user->id)
            ->with([
                'job_position',
                'skills.skill',
                'status',
                'languages.languageDetail'
            ])->first();

        $img = isset($userData->img_avatar) && $userData->img_avatar != '' ? \URL::to('/') . '/' . $userData->img_avatar : '';
        $userData->img_avatar = $img;
        $is_user = GuestRegister::where('email', '=', $userData->email)->first();
        if(!is_null($is_user)) {
	        $sixDigit = generateStrongPassword(6, false, 'd');
	        $this->sendEmail($userData->email, $sixDigit);
        }
        return response()->json([
            'status' => 'success',
            'access_token' => $token,
            'token_type' => 'bearer',
            'user' => $userData,
            'shared_office' => $data,
            'seat_number' => $number
        ]);
    }

    public function sendEmail($email,$sixDigit)
    {
        $senderEmail = "support@xenren.co";
        $senderName = 'Xenren Confirmation Message';
        $receiverEmail = $email;
        $link = '';

        $sixDigit = substr($sixDigit, 0, 3) . '-' . substr($sixDigit, 3, 6);
        Mail::send('mails.guestConfirmationCode', array('code' => $sixDigit, 'email' => $email, 'link' => $link), function ($message) use ($senderEmail, $senderName, $receiverEmail, $sixDigit) {
            $message->from($senderEmail, $senderName);
            $message->to($receiverEmail)
                ->subject(trans('common.confirmation_code_for_xenren', ['code' => $sixDigit]));
        });
    }

    public function xlogin(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');
        $device_token = $request->input('device_token');

        $credentials = [
            'email' => $email,
            'password' => $password
        ];

        if (Auth::guard('users')->attempt($credentials)) {
            $user = Auth::guard('users')->user();
            $user->last_login_at = Carbon::now();
            $user->save();

            $userToken = UserToken::where([['user_id', '=', $user->id], ['token', '=', $device_token]])->first();
            if (empty($userToken)) {
                $userToken = new UserToken();
                $userToken->user_id = $user->id;
                $userToken->token = $device_token;
            }
            $userToken->is_login = '1';
            if ($userToken->save()) {
                return response()->json([
                    'status' => 'OK',
                    'message' => 'Success',
                    'token' => $device_token,
                    'user_id' => $user->id,
                    'success' => true
                ], 200);
            } else {
                return response()->json([
                    'status' => 'ERROR',
                    'message' => 'Cannot Save Token.',
                    'success' => false
                ], 400);
            }
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'Username or Password not Found.',
                'password' => bcrypt(bcrypt($password)),
                'success' => false
            ], 400);
        }
    }

    public function register(Request $request)
    {
        try {
            $email = $request->input('email');
            $name = $request->input('real_name') .' '. $request->input('last_name');
            $password = $request->input('password');
            $device_token = $request->input('device_token');

            $user = User::where('email', $email)->first();
            if ($user) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Email has already registered.'
                ], 400);
            } else {
                $sixDigit = generateStrongPassword(6, false, 'd');
                $key = generateStrongPassword(32, false, 'lud');
                $guest = GuestRegister::where('email', $email)->where('status', 1)->first();
                if ($guest) {
                    //send email
                    $this->sendRegisterEmail($sixDigit, $email);
                    return collect([
                        'status' => 'success',
                        'message' => 'Confirmation email send to your mail please check your Mail',
                        'code' => $sixDigit,
                        'key' => $key
                    ]);
                } else {
                    \DB::beginTransaction();
                    //create new session
                    $guestRegister = new GuestRegister();
                    $guestRegister->email = $email;
                    $guestRegister->real_name = $name;
                    $guestRegister->password = $password;
                    $guestRegister->code = $sixDigit;
                    $guestRegister->key = $key;
                    $guestRegister->device_token = $device_token;
                    $guestRegister->hint=self::getHint($password);
                    $guestRegister->status = 1; //active
                    $guestRegister->save();

                    \DB::commit();
                    //send email
                    $this->sendRegisterEmail($sixDigit, $email);

                    return collect([
                        'status' => 'success',
                        'message' => 'Confirmation email send to your mail please check your Mail',
                        'code' => $sixDigit,
                        'key' => $key
                    ]);
                }
            }
        } catch (\Exception $e) {
            \DB::rollBack();
            return collect([
                'status' => 'error',
                'message' => $e->getMessage(). 'at' . $e->getLine()
            ]);
        }
    }

    public function getHint($string)
    {
        try {
            $length = strlen($string);
            $co = (int)($length / 2);
            $splittedString = str_split($string, $co);
            return $splittedString[0];
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'message' => $e->getMessage(). 'at'. $e->getLine()
            ]);
        }
    }

    public function registered(Request $request)
    {
        $code = $request->sixDigitCode;
        $key = $request->key;

        $guestRegister = GuestRegister::where('key', '=', $key)->first();

        if(!is_null($guestRegister)) {
            if($guestRegister->code == $code) {
                try {
                    \DB::beginTransaction();
                    $user = new User();
                    $user->email = $guestRegister->email;
                    $user->password = $guestRegister->password;
                    $user->hint = $guestRegister->hint;
                    $user->last_login_at = Carbon::now();
                    $user->name = $guestRegister->real_name;
//                    $user->real_name = $guestRegister->real_name;
                    $user->del_flg = 0;
                    $user->is_ban_submit_keyword = 0;
                    $user->save();
//                $invitorId = $guestRegister->invite_code;
//                if ($invitorId > 0) {
//                    $invitor = User::find($invitorId);
//                    $invitation = new Invitation();
//                    $invitation->invitor_id = $invitor->id;
//                    $invitation->invitee_id = $user->id;
//                    $invitation->save();
//                }
                    $guestRegister->delete();
                    \DB::commit();

                    $usrData = User::where('id', '=', $user->id)->with('sharedOfficeSeat')->first();


                    $token = false;
                    try {
                        $token = JWTAuth::attempt([
                            'email' => $guestRegister->email,
                            'password' => $guestRegister->password,
                        ]);
                    } catch (\Exception $e) {
                        return collect([
                            'status' => 'success',
                            'err' => $e->getMessage() . 'at' . $e->getLine()
                        ]);
                    }

                    return collect([
                        'status' => 'success',
                        'user' => $usrData,
                        'access_token' => $token,
                        'token_type' => 'bearer',
//                        'shared_office' => $data,
//                        'seat_number' => $number,
                        'message' => 'Successfully Login'
                    ]);
                } catch (\Exception $e) {
                    \DB::rollback();
                    return collect([
                        'status' => 'error',
                        'message' => $e->getMessage() . 'at' . $e->getLine()
                    ]);
                }
            } else {
                return collect([
                    'status' => 'error',
                    'message' => 'You enter Invalid Code!!'
                ]);
            }
        } else {
            return collect([
                'status' => 'error',
                'message' => 'Guest Record not found'
            ]);
        }
    }

	public function forgotPassword(Request $request)
	{
		$user = User::where('email', '=', $request->email)->first();
		if(is_null($user)) {
			return response()->json([
				'status' => 'ERROR',
				'message' => 'Email not found'
			], 400);
		} else {
			$user->notify(new ResetPasswordEmail($user));
			return response()->json([
				'status' => 'OK',
				'message' => 'Please check your email for reset instruction'
			], 200);
		}
	}


	public function xforgotPassword(Request $request)
    {
        $email = $request->input('email');

        $user = User::where('email', $email)->first();
        if ($user) {
            $user->remember_token = Str::random(60);
            $user->save();

            $this->sendResetEmail($user->remember_token, $user->email);

            return response()->json([
                'status' => 'OK',
                'message' => 'Please check your email for reset instruction'
            ], 200);
        }

        return response()->json([
            'status' => 'ERROR',
            'message' => 'Email not found'
        ], 400);
    }

    public function verifyCode(Request $request)
    {
        try {
            $code = $request->input('code');
            $email = $request->input('email');
            $device_token = $request->input('device_token');

            \DB::beginTransaction();
            $guestRegister = GuestRegister::where('email', $email)->where('status', 1)->first();
            if (is_null($guestRegister)) {
                return response()->json([
                    'status' => 'ERROR',
                    'message' => 'Wrong email address..!.'
                ], 400);
            }
            if (intval($guestRegister->code) != intval($code)) {
                return response()->json([
                    'status' => 'ERROR',
                    'message' => 'Wrong confirmation code.'
                ], 400);
            }
            //accessing cache
            $storedData = Cache::get($guestRegister->key);

            $user = new User();
            $user->email = $email;
            $user->name = $guestRegister['name'];
            $user->password = $guestRegister['password'];

            $user->save();

            //normal user
            $invitorId = $guestRegister->invite_code;
            if ($invitorId > 0) {
                $invitor = User::find($invitorId);
                $invitation = new Invitation();
                $invitation->invitor_id = $invitor->id;
                $invitation->invitee_id = $user->id;
                $invitation->save();
            }
            $guestRegister->delete();
            \DB::commit();

            try {
	            $userToken = new UserToken();
	            $userToken->user_id = $user->id;
	            $userToken->token = $device_token;
	            $userToken->is_login = '0';
	            $success = true;
            } catch (\Exception $e) {
            	\Log::error($e->getMessage());
	            $success = true;
            }

	          if ($success) {
                return response()->json([
                    'status' => 'OK',
                    'message' => 'Success',
                    'token' => $device_token
                ], 200);
            } else {
                return response()->json([
                    'status' => 'ERROR',
                    'message' => 'Cannot Save Token.',
                ], 400);
            }

        } catch (\Exception $e) {
            return response()->json([
                'status' => 'ERROR',
                'message' => $e->getMessage()
            ], 400);
        }
    }

    private function generateToken($user)
    {
        return $user->id . "_" . generateStrongPassword(32, false, 'lud');
    }

    private function sendRegisterEmail($sixDigit, $email)
    {
        //send email
        $senderEmail = "support@xenren.co";
        $senderName = "System Admin";
        $receiverEmail = $email;

        //for code display purpose
        $sixDigit = substr($sixDigit, 0, 3) . '-' . substr($sixDigit, 3, 6);
        Mail::send('mails.guestConfirmationCodeMobile', array('code' => $sixDigit, 'email' => $email), function ($message) use ($senderEmail, $senderName, $receiverEmail, $sixDigit) {
            $message->from($senderEmail, $senderName);
            $message->to($receiverEmail)
                ->subject(trans('common.confirmation_code_for_xenren', ['code' => $sixDigit]));
        });
    }

    public function freelancerDetail($project_id)
    {
        $response = array();
        $response["status"] = "";

        try {
            /*$_user = new User();
            $data = $_user
                ->where('id', '=', $request->id)
                ->with([
                    'job_position',
                    'skills.skill',
                    'status',
                    'languages.languageDetail'
                ])->get();*/

            /*foreach ($user_inbox_messages as $key => $value) {
                if ($value->fromUser->id == Auth::user()->id) {
                    $user_id = $value->toUser->id;
                    $real_name = $value->toUser->real_name;
                    $img_avatar = $value->toUser->img_avatar;
                    $age = UtilityController::get_age_from_dob($value->toUser->date_of_birth);
                } else {
                    $user_id = $value->fromUser->id;
                    $real_name = $value->fromUser->real_name;
                    $img_avatar = $value->fromUser->img_avatar;
                    $age = UtilityController::get_age_from_dob($value->fromUser->date_of_birth);
                }

                array_push($detail, array(
                    "user_id" => $user_id,
                    "real_name" => $real_name,
                    "img_avatar" => $img_avatar,
                    "age" => $age
                ));

                $response["temp"][] = $value;
            }*/

            /*foreach ($data as $key => $value) {
                $transaction = new SharedOfficeFinance();
                $shared_office = $transaction->with('productfinance')
                    ->whereHas('productfinance', function ($q) {
                        $q->where('status_id', '=', 1);
                    })
                    ->whereNull('end_time')
                    ->get();

                $data[$key]['shared_office'] = isset($shared_office) ? true : false;
                $img = \URL::to('/') . '/' . $value->img_avatar;
                $data[$key]['img_avatar'] = $img;
                $data[$key]['user_ratings'] = $_user->getReviewInfo($value->id);

                $detail[$count]["id"] = $value->id;
                $detail[$count]["user_name"] = $value->real_name;
                $detail[$count]["avatar"] = $value->img_avatar;

                $detail[$count]["age"] = $age;
                $detail[$count]["position"] = $value->job_position["name_" . App::getLocale()];
                $detail[$count]["last_login_at"] = $value->last_login_at;
                $detail[$count]["experience"] = $value->experience;

                $project_completed = UserSkillComment::with(['files', 'staff', 'skills', 'skills.skill'])
                    ->count();

                $detail[$count]["projects_completed"] = $project_completed;
                $detail[$count]["about_user"] = $value->about_me;
                $detail[$count]["skills"] = $value->skills;
                $count++;
            }*/

            $data = ProjectApplicant::where("project_id", "=", $project_id)
                ->with(["user" => function ($q) {
                    $q->withCount('CompletedProjects');
                }, "user.skills", "user.skills.skill"])
                ->get();

            $response["status"] = "success";
            $response["data"] = $data;
        } catch (\Exception $exception) {
            $response["status"] = "error";
            $response["message"] = $exception->getMessage() . " in " . $exception->getFile() . " in " . $exception->getLine();
        }

        return $response;
    }

    public function publicProfile(Request $request)
    {
        try {
            $_user = new User();
            $data = $_user
                ->where('id', '=', $request->id)
                ->with([
                    'job_position',
                    'skills.skill',
                    'status',
                    'languages.languageDetail'
                ])->first();
//        Check if User has any seat active in any shared office
            $transaction = new SharedOfficeFinance();
            $shared_office = $transaction->with('productfinance')
                ->whereHas('productfinance', function ($q) {
                    $q->where('status_id', '=', 1);
                })
                ->where('user_id', '=', $request->id)
                ->whereNull('end_time')
                ->first();

            $data['shared_office'] = isset($shared_office) ? true : false;
            $img = isset($data->img_avatar) && !is_null($data->img_avatar) && $data->img_avatar != '' ? \URL::to('/') . '/' . $data->img_avatar : '';
            $data->img_avatar = $img;
            $data['user_ratings'] = $_user->getReviewInfo($request->id);
            return collect([
                'status' => 'success',
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return collect([
                'status' => 'error',
                'data' => $e->getMessage()
            ]);
        }

    }

    public function saveToken(Request $request)
    {
        $token = $request->device_token;
        $os = $request->device_os;
        if (isset($token)) {
            User::where('id', '=', \Auth::user()->id)->update([
                'device_token' => $token,
                'device_os' => $os
            ]);
            return collect([
                'status' => 'success',
            ]);
        } else {
            return collect([
                'status' => 'error',
                'data' => 'Device token required...!'
            ]);
        }
    }

}
