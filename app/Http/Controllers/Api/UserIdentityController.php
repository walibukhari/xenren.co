<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Constants;
use App\Http\Requests;
use App\Models\User;
use App\Models\UserIdentity;
use App\Models\UserToken;
use App\Services\PushNotificationService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Image;

class UserIdentityController extends Controller {

		public function test()
		{
			$push = PushNotificationService::sendPush('dfff1f7a1122f124120d03',	collect([
				'message' => 'You received a new message',
				'body' => 'You received a new message',
				'title' => 'New Message - XENREN',
				'data' => ' ok ok'
			]));
			return collect([
				'status' => 'success',
				'data' => $push
			]);
		}

    public function uploadIdCard(Request $request) {
        $res = ['result' => '', 'status' => 'ERROR', 'message' => ''];
        $file = $request->file('image');
        $is_file = false;
        $hasFile = $request->hasFile('image');
        $user_id = $request->get('user_id');
        $img_type = $request->get('img_type');

        if (empty($user_id)) {
            $res['message'] = 'User ID Missing';
            return response()->json($res);
        }
        $user = User::where('id', '=', $user_id)
            ->first();
        \Log::channel('api')->info($user);
        if($user->del_flg == 1 || $user->is_ban_submit_keyword == 1) {
            $res['message'] = 'User not exists or ban by admin';
            $res['user'] = $user;
            return response()->json($res);
        }
        if (empty($user)) {
            $res['message'] = 'User not exists or ban by admin';
            return response()->json($res);
        }
        $imgFullFilePath = "";

        if (!($hasFile == '' || $hasFile == false)) {

            $allowed_ext = ['gif', 'jpg', 'jpeg', 'png'];
            $ext = $request->file('image')->getClientOriginalExtension();
            if (in_array($ext, $allowed_ext)) {
                if ($file instanceof \Illuminate\Http\UploadedFile) {
                    $is_file = true;
                }
            } else {
                $message = trans('common.not_allow_file_format', ['filetype' => implode(', ', $allowed_ext)]);
                $res['message'] = $message;
            }
        }

        $userIdentity = UserIdentity::where('user_id', $user_id)->first();
        if(empty($userIdentity)){
            $userIdentity = new UserIdentity();
            $userIdentity->user_id = $user_id;
            $userIdentity->status = UserIdentity::STATUS_NEW;
            $userIdentity->key = generateStrongPassword(32, false, 'lud');
        }

        if ($is_file && in_array($img_type,['ID','HOLD_ID'])) {
            try {
                if($img_type == 'ID'){
                    $userIdentity->status = UserIdentity::STATUS_NEW;
                    $userIdentity->setIdImageAttribute($file);
                }
                elseif($img_type == 'HOLD_ID'){
                    $userIdentity->status = UserIdentity::STATUS_NEW;
                    $userIdentity->setHoldIdImageAttribute($file);
                }
                $userIdentity->save();
                $user->is_validated = 0;
                $user->save();

                $res['status'] = 'OK';
                $res['message'] = 'Image Upload Successfully';

            } catch (Exception $e) {
                Log::info('Exception : ' . $e->getMessage());
                return collect([
                   'status' => 'ERROR',
                   'message' => $e->getMessage(). 'at ', $e->getLine()
                ]);
            }
        }

        if($img_type == 'id_image') {
            $userIdentity->setIdImageAttribute($file);
            $userIdentity->save();
            $user->is_validated = 0;
            $user->save();
            $res['status'] = 'OK';
        } else if($img_type == 'hold_id_image') {
            $userIdentity->setIdImageAttribute($file);
            $userIdentity->save();
            $user->is_validated = 0;
            $user->save();
            $res['status'] = 'OK';
        }

        return response()->json($res);
    }

}
