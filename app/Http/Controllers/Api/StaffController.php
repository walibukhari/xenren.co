<?php

namespace App\Http\Controllers\Api;

use App\Models\PermissionGroup;
use App\Models\SharedOffice;
use App\Models\Staff;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Auth\StaffController as AuthStaffController;


class StaffController extends Controller
{

    protected $loginPost;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        Config::set('auth.providers.users.model', Staff::class);
        $this->loginPost = new \App\Http\Controllers\Auth\StaffController();
    }
    public function loginPost(Request $request){

        $login = $request->get('login');
        $password = $request->get('password');
        $access_token = $request->get('token');
        $field = null;


        if (filter_var($request->get('login'), FILTER_VALIDATE_EMAIL)) {
            $field = 'email';
        } else {
            $field = 'username';
        }

        $credentials = [
            $field => $login,
            'password' => $password
        ];
        $checkCred = Staff::where('email','=',$login)->orwhere('username','=',$login)->first();
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                if(is_null($checkCred)){
                    return response()->json([
                        'status' => 'false',
                        'message' => 'wrong email'
                    ]);
                } else if(Hash::check($password,$checkCred['password']) === false) {
                    return response()->json([
                        'status' => 'false',
                        'message' => 'wrong password'
                    ], 401);
                } else {
                }
            }
        } catch (JWTException $e) {
            return response()->json(['success' => false, 'error' => 'Failed to login, please try again.', 'message' => $e->getMessage()], 500);
        }
        $user = Auth::guard('users')->user();
        $data = $this->authenticated($credentials,$user,$token);
        if ($data['status'] == true) {
            return collect([
                'status' => 'true',
                'token' => $token,
                'data' => Auth::guard('users')->user(),
                'email' => $user->email,
                'username' => $user->username,
                'password' => $password,
                'name' => $user->name,
            ]);
        }
    }

    public function authenticated($request, $user,$token) {
        if(Auth::guard('users')->user()->staff_type === Staff::STAFF_TYPE_ADMIN) {
            return collect(['0']);
        } else {
            $id = Auth::guard('users')->user()->id;
            $check = $this->checkStaffSharedOffice($id);
            if($check == true) {
                return collect([
                    'status' => true,
                    'message' => true
                ]);
            } else {
                return collect([
                    'status' => true,
                    'message' => false
                ]);
            }
        }
    }

    public function checkStaffSharedOffice($id) {
        $sharedOffice = SharedOffice::where('staff_id','=',$id)->first();
        if(!is_null($sharedOffice)) {
            return true;
        } else {
            return false;
        }
    }

    public function registerPost(Request $request){
        $check = $this->checkUserExist($request->email);
        if($check == true) {
            try {
                $model = new Staff();
                $model->username = $request->get('username');
                $model->email = $request->get('email');
                $model->name = $request->get('username');
                $model->password = $request->get('password');
                $model->staff_type = Staff::defaultUserType();
                $model->permission_group_id = PermissionGroup::PERMISSION_GROUP_SHARED_OFFICE;
                $model->title = Staff::Shared_OFFICE_TITLE;
                $model->save();
                $request->office_id;
                $a = SharedOffice::where('id', '=', $request->office_id)->update([
                    'staff_id' => $model->id
                ]);
                $token = JWTAuth::fromUser($model);
                return collect([
                    'success' => 'true',
                    'message' => 'Successfully Registered',
                    'token' => $token,
                    'data' => $model
                ]);
            } catch (\Exception $e) {
                return makeResponse($e->getMessage(), true);
            }
        } else {
            return collect([
                'status' => 'false',
                'message' => 'email already exist'
            ]);
        }
    }

    public function checkUserExist($email){
        $check = Staff::where('email','=',$email)->first();
        if(is_null($check)) {
            return true;
        } else {
            return false;
        }
    }

    public function forgotPassword(Request $request){
        $getEmail = Staff::where('email', '=', $request->email)->first();
        if (is_null($getEmail)) {
            return collect([
                'status' => 'false',
                'message' => 'email does not exit please enter correct email'
            ]);
        } else {
            $lang = \App::getLocale();
            $link = route('sharedoffice.setPassword',[$lang,$getEmail->id]);
            $senderEmail = "support@xenren.co";
            $senderName = 'Xenren Confirmation Message';
            $receiverEmail = $getEmail->email;
            Mail::send('mails.setStaffPasswordConfirmationMail', array('email' => $getEmail->email, 'link' => $link), function ($message) use ($senderEmail, $senderName, $receiverEmail) {
                $message->from($senderEmail, $senderName);
                $message->to($receiverEmail)
                    ->subject('reset password confirmation email');
            });
            return collect([
                'status' => 'true',
                'message' => 'we will send you email check you email',
            ]);
        }
    }

    public function logout(){
        Auth::guard('users')->logout();
        return collect([
           'status' =>  'true',
           'message' => 'Successfully Logout'
        ]);
    }

    public function staffExist(){
        $staff = Auth::guard('users')->user();
        $s_staff = new Staff();
        if($staff) {
            $data = $s_staff->where('id','=',$staff->id)->first();
            $sharedoffice = SharedOffice::where('staff_id','=',$data->id)->first();
            return collect([
                'data' => $data,
                'has_office' => isset($sharedoffice) ? $sharedoffice : ''
            ]);
        } else {
            return collect([
                'status' => 'false',
            ]);
        }
    }

    public function saveToken(Request $request)
    {
        $token = $request->device_token;
        $os = $request->device_os;
        if (isset($token)) {
            Staff::where('id', '=', \Auth::user()->id)->update([
                'device_token' => $token,
                'device_os' => $os
            ]);
            return collect([
                'status' => 'success',
            ]);
        } else {
            return collect([
                'status' => 'error',
                'data' => 'Device token required...!'
            ]);
        }
    }
}
