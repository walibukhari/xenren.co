<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class UtilityController extends Controller
{
    public static function get_age_from_dob($dob)
    {
        $originalDate = $dob;
        $newDate = date("m/d/Y", strtotime($originalDate));

        $birthDate = explode("/", $newDate);
        $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
            ? ((date("Y") - $birthDate[2]) - 1)
            : (date("Y") - $birthDate[2]));
        return $age;
    }
}
