<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\HiringJobPosition;
use Illuminate\Support\Facades\Auth;
use App\Models\Hiring;
use Illuminate\Support\Facades\Validator;

class HiringController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'captcha' => 'captcha'
        ]);

        if ($validator->fails()) {
            return collect([
                'error' => 'captcha not same',
            ]);
        }

        if($request->hasFile('resume'))
        {
            $fileName = $request->file('resume')->getClientOriginalName();
            $path = 'uploads/hiring';
            $request->file('resume')->move($path,$fileName);
        }

        $check = Hiring::where('resume','=',$fileName)->where('email','=',$request->email)->get()->last();
        if(is_null($check))
        {
            $status = Hiring::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'job_position' => $request->job_position,
                'bio' => $request->bio,
                'resume' => $fileName
            ]);
            return collect([
                'success' => $status,
            ]);

        } else {
            if($check->job_position == $request->job_position && $check->email == $request->email)
            {
                return collect([
                    'message' => "you already apply for this position",
                ]);
            } else {
                $status = Hiring::create([
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'email' => $request->email,
                    'job_position' => $request->job_position,
                    'bio' => $request->bio,
                    'resume' => $fileName
                ]);

                return collect([
                    'success' => $status,
                ]);
            }
        }
    }
}
