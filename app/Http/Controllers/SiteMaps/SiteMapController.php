<?php

namespace App\Http\Controllers\SiteMaps;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\SharedOffice;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;

class SiteMapController extends Controller
{

	/**
	 * NOTE:
	 * step1 - /media/ahsanhussain/6EC201C0C2018E11/Workspace/xenrencc_5_7/vendor/laravelium/sitemap/src/views/sitemapindex.php
	 * goto above path and replace line # 1 with below
	 * <?= "\n"; ?>
	 * step2- goto /media/ahsanhussain/6EC201C0C2018E11/Workspace/xenrencc_5_7/vendor/laravelium/sitemap/src/views/sitemapindex.php
	 * and replace line # 5 with below
	 * <sitemapindex xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd">
	 * step3- goto /media/ahsanhussain/6EC201C0C2018E11/Workspace/xenrencc_5_7/vendor/laravelium/sitemap/src/views/xml.php
	 * and replace line # 1 with below
	 * <?= "\n" ?>
	 * */
	public function index()
	{
		$sitemap = App::make("sitemap"); // create sitemap
		$sitemap->setCache('laravel.sitemap-index', 3600); // set cache

		$totalProjects = Project::count();
		$totalProjects  = (int)ceil($totalProjects/500);
		for($i=0; $i < $totalProjects; $i++) {

			$index = $i == 0 ? '' : $i;
			$sitemap->addSitemap(URL::to('sitemap-jobs'.$index.'.xml'));
//			$sitemap->set(URL::to('sitemap-jobs'.$index));
		}

		/** TODO:
		 * Uncomment below section after shared office complete...!
		*/
		$sharedOffices = SharedOffice::count();
		$sharedOffices  = (int)ceil($sharedOffices/500);
		for($i=0; $i < $sharedOffices; $i++) {

			$index = $i == 0 ? '' : $i;
			$sitemap->addSitemap(URL::to('sitemap-shared-office'.$index.'.xml'));
		}

		/**
		 * TODO:
		 * Update for freelancers only...!
		 */
		$users = User::count();
		$users  = (int)ceil($users/500);
		for($i=0; $i < $users; $i++) {
			$index = $i == 0 ? '' : $i;
			$sitemap->addSitemap(URL::to('sitemap-experts'.$index.'.xml'));
		}

		$sitemap->addSitemap(URL::to('sitemap-website.xml'));
		// show sitemap
		return $sitemap->render('sitemapindex', 'utf');
	}

	public function jobs()
	{
        // create new sitemap object
        $sitemap = App::make('sitemap');

        // set cache key (string), duration in minutes (Carbon|Datetime|int), turn on/off (boolean)
        // by default cache is disabled
        $sitemap->setCache('laravel.sitemap', 60);

        // check if there is cached sitemap and build new only if is not
        if (!$sitemap->isCached()) {
            // add item to the sitemap (url, date, priority, freq)

            // add item with translations (url, date, priority, freq, images, title, translations)
            $translations = [
                ['language' => 'cn', 'url' => URL::to('/setlang/cn')],
            ];
            $sitemap->add( URL::to('/setlang/cn'), '2015-06-24T14:30:00+02:00', '0.9', 'daily', [], null, $translations);

//			// add item with images
//			$images = [
//				['url' => URL::to('images/pic1.jpg'), 'title' => 'Image title', 'caption' => 'Image caption', 'geo_location' => 'Plovdiv, Bulgaria'],
//				['url' => URL::to('images/pic2.jpg'), 'title' => 'Image title2', 'caption' => 'Image caption2'],
//				['url' => URL::to('images/pic3.jpg'), 'title' => 'Image title3'],
//			];
//			$sitemap->add(URL::to('post-with-images'), '2015-06-24T14:30:00+02:00', '0.9', 'daily', $images);
//
            // get all posts from db
            $posts = Project::get();

            // add every post to the sitemap1234
            foreach ($posts as $post) {
				$translations = [
					['language' => 'cn', 'url' => URL::to('/').'/cn/joinDiscussion/getProject/'.$post->getSlug().'/'.$post->id],
					['language' => 'en', 'url' => URL::to('/').'/en/joinDiscussion/getProject/'.$post->getSlug().'/'.$post->id],
				];
                $sitemap->add(URL::to('/').'/en/joinDiscussion/getProject/'.$post->getSlug().'/'.$post->id, Carbon::parse($post->created_at)->toDateTimeString(), 0, 'daily', [], null, $translations);
            }
        }

        // show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
        return $sitemap->render('xml');
	}

//	public function commonJobs()
//	{
//		// create new sitemap object
//		$sitemap = App::make('sitemap');
//
//		// set cache key (string), duration in minutes (Carbon|Datetime|int), turn on/off (boolean)
//		// by default cache is disabled
//		$sitemap->setCache('laravel.sitemap', 60);
//
//		// check if there is cached sitemap and build new only if is not
//		if (!$sitemap->isCached()) {
//			// add item to the sitemap (url, date, priority, freq)
//
//			// add item with translations (url, date, priority, freq, images, title, translations)
//			$translations = [
//				['language' => 'cn', 'url' => URL::to('/setlang/cn')],
//			];
//			$sitemap->add( URL::to('/setlang/cn'), '2015-06-24T14:30:00+02:00', '0.9', 'daily', [], null, $translations);
//
////			// add item with images
////			$images = [
////				['url' => URL::to('images/pic1.jpg'), 'title' => 'Image title', 'caption' => 'Image caption', 'geo_location' => 'Plovdiv, Bulgaria'],
////				['url' => URL::to('images/pic2.jpg'), 'title' => 'Image title2', 'caption' => 'Image caption2'],
////				['url' => URL::to('images/pic3.jpg'), 'title' => 'Image title3'],
////			];
////			$sitemap->add(URL::to('post-with-images'), '2015-06-24T14:30:00+02:00', '0.9', 'daily', $images);
////
//			// get all posts from db
//			$posts = Project::where('type', '=', Project::PROJECT_TYPE_COMMON)->get();
//
//			// add every post to the sitemap
//			foreach ($posts as $post) {
//				$sitemap->add(URL::to('/').'/joinDiscussion/getProject/'.$post->getSlug().'/'.$post->id, Carbon::parse($post->created_at)->toDateTimeString(), 0, 'daily');
//			}
//		}
//
//		// show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
//		return $sitemap->render('xml');
//	}
//
//	public function officialJobs()
//	{
//		// create new sitemap object
//		$sitemap = App::make('sitemap');
//
//		// set cache key (string), duration in minutes (Carbon|Datetime|int), turn on/off (boolean)
//		// by default cache is disabled
//		$sitemap->setCache('laravel.sitemap', 60);
//
//		// check if there is cached sitemap and build new only if is not
//		if (!$sitemap->isCached()) {
//			// add item to the sitemap (url, date, priority, freq)
//
//			// add item with translations (url, date, priority, freq, images, title, translations)
//			$translations = [
//				['language' => 'cn', 'url' => URL::to('/setlang/cn')],
//			];
//			$sitemap->add( URL::to('/setlang/cn'), '2015-06-24T14:30:00+02:00', '0.9', 'daily', [], null, $translations);
//
////			// add item with images
////			$images = [
////				['url' => URL::to('images/pic1.jpg'), 'title' => 'Image title', 'caption' => 'Image caption', 'geo_location' => 'Plovdiv, Bulgaria'],
////				['url' => URL::to('images/pic2.jpg'), 'title' => 'Image title2', 'caption' => 'Image caption2'],
////				['url' => URL::to('images/pic3.jpg'), 'title' => 'Image title3'],
////			];
////			$sitemap->add(URL::to('post-with-images'), '2015-06-24T14:30:00+02:00', '0.9', 'daily', $images);
////
//			// get all posts from db
//			$posts = Project::where('type', '=', Project::PROJECT_TYPE_OFFICIAL)->get();
//
//			// add every post to the sitemap
//			foreach ($posts as $post) {
//				$sitemap->add(URL::to('/').'/joinDiscussion/getProject/'.$post->getSlug().'/'.$post->id, Carbon::parse($post->created_at)->toDateTimeString(), 0, 'daily');
//			}
//		}
//
//		// show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
//		return $sitemap->render('xml');
//
//	}

	public function sharedOffice($id=0)
	{
	    $id = (int)$id;
	    if($id != 0) {
            $id = $id + 1;
        }

	    $devident = 500;
	    $max = $id == 0 ? $devident : $devident*$id;
	    $min = $id == 0 ? 0 : $max - $devident;

		// create new sitemap object
		$sitemap = App::make('sitemap');

		// set cache key (string), duration in minutes (Carbon|Datetime|int), turn on/off (boolean)
		// by default cache is disabled
//		$sitemap->setCache('laravel.sitemap', 60);

		// check if there is cached sitemap and build new only if is not
//		if (!$sitemap->isCached()) {
			// add item to the sitemap (url, date, priority, freq)

			// add item with translations (url, date, priority, freq, images, title, translations)
			$translations = [
				['language' => 'cn', 'url' => URL::to('/setlang/cn')],
			];
			$sitemap->add( URL::to('/setlang/cn'), '2015-06-24T14:30:00+02:00', '0.9', 'daily', [], null, $translations);


//			// get all posts from db
			$posts = SharedOffice::skip($min)->take($max)->get();
			// add every post to the sitemap
			foreach ($posts as $post) {
				$translations = [
				['language' => 'cn', 'url' => URL::to('/').'/cn/coworking-spaces/'.$post->office_name.'/'.$post->id],
				['language' => 'en', 'url' => URL::to('/').'/en/coworking-spaces/'.$post->office_name.'/'.$post->id],
			];
				$sitemap->add(URL::to('/').'/en/coworking-spaces/'.$post->office_name.'/'.$post->id, Carbon::parse($post->created_at)->toDateTimeString(), 0, 'daily',[],null,$translations);
			}
//		}

		// show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
		return $sitemap->render('xml');

	}

	public function experts(Request $request)
	{
		// create new sitemap object
		$sitemap = App::make('sitemap');

		// set cache key (string), duration in minutes (Carbon|Datetime|int), turn on/off (boolean)
		// by default cache is disabled
		$sitemap->setCache('laravel.sitemap', 60);

		// check if there is cached sitemap and build new only if is not
		if (!$sitemap->isCached()) {
			// add item to the sitemap (url, date, priority, freq)

			// add item with translations (url, date, priority, freq, images, title, translations)
			$translations = [
				['language' => 'cn', 'url' => URL::to('/setlang/cn')],
			];
			$sitemap->add( URL::to('/setlang/cn'), '2015-06-24T14:30:00+02:00', '0.9', 'daily', [], null, $translations);


			/** TODO:
			 * Add UI PREFERENCE
			*/
			// get all posts from db
			$experts = User::get();

			// add every post to the sitemap
			foreach ($experts as $expert) {
				$translations = [
					['language' => 'cn', 'url' => URL::to('/').'/cn/resume/getDetails/'.$expert->user_link],
					['language' => 'en', 'url' => URL::to('/').'/en/resume/getDetails/'.$expert->user_link],
				];
				$sitemap->add(URL::to('/').'/en/resume/getDetails/'.$expert->user_link, Carbon::parse($expert->created_at)->toDateTimeString(), 0, 'daily',[],null,$translations);
			}
		}

		// show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
		return $sitemap->render('xml');
	}

	public function general()
	{
		// create new sitemap object
		$sitemap = App::make('sitemap');

		// set cache key (string), duration in minutes (Carbon|Datetime|int), turn on/off (boolean)
		// by default cache is disabled
		$sitemap->setCache('laravel.sitemap', 60);

		// check if there is cached sitemap and build new only if is not
		if (!$sitemap->isCached()) {
			// add item to the sitemap (url, date, priority, freq)

			// add item with translations (url, date, priority, freq, images, title, translations)
			$translations = [
				['language' => 'cn', 'url' => URL::to('/setlang/cn')],
			];
			$sitemap->add( URL::to('/setlang/cn'), '2015-06-24T14:30:00+02:00', '0.9', 'daily', [], null, $translations);

			$translations = [
				['language' => 'cn', 'url' => URL::to('/').'/cn/expert-virtual-assistant'],
				['language' => 'en', 'url' => URL::to('/').'/en/expert-virtual-assistant'],
			];

			$translations1 = [
				['language' => 'cn', 'url' => URL::to('/').'/cn/instructionHiring'],
				['language' => 'en', 'url' => URL::to('/').'/en/instructionHiring'],
			];

			$translations2 = [
				['language' => 'cn', 'url' => URL::to('/').'/cn/companyCulture'],
				['language' => 'en', 'url' => URL::to('/').'/en/companyCulture'],
			];

			$translations3 = [
				['language' => 'cn', 'url' => URL::to('/').'/cn//aboutUs'],
				['language' => 'en', 'url' => URL::to('/').'/en//aboutUs'],
			];

			$translations4 = [
				['language' => 'cn', 'url' => URL::to('/').'/cn/hiringPage'],
				['language' => 'en', 'url' => URL::to('/').'/en/hiringPage'],
			];

			$translations5 = [
				['language' => 'cn', 'url' => URL::to('/').'/cn/terms-service'],
				['language' => 'en', 'url' => URL::to('/').'/en/terms-service'],
			];

			$translations6 = [
				['language' => 'cn', 'url' => URL::to('/').'/cn/privacy-policy'],
				['language' => 'en', 'url' => URL::to('/').'/en/privacy-policy'],
			];

			$translations7 = [
				['language' => 'cn', 'url' => URL::to('/').'/cn/meetTheTeam'],
				['language' => 'en', 'url' => URL::to('/').'/en/meetTheTeam'],
			];

			$translations8 = [
				['language' => 'cn', 'url' => URL::to('/').'/cn/contactUs'],
				['language' => 'en', 'url' => URL::to('/').'/en/contactUs'],
			];

			$translations9 = [
				['language' => 'cn', 'url' => URL::to('/').'/cn/meetSalesTeam'],
				['language' => 'en', 'url' => URL::to('/').'/en/meetSalesTeam'],
			];

			$translations10 = [
				['language' => 'cn', 'url' => URL::to('/').'/cn/cooperatePage'],
				['language' => 'en', 'url' => URL::to('/').'/en/cooperatePage'],
			];

			$translations11 = [
				['language' => 'cn', 'url' => URL::to('/').'/cn/faq-buyer'],
				['language' => 'en', 'url' => URL::to('/').'/en/faq-buyer'],
			];

			$sitemap->add(URL::to('/').'/en/expert-virtual-assistant', null, 0, 'monthly', [], null, $translations);
			$sitemap->add(URL::to('/').'/en/instructionHiring', null, 0, 'monthly', [], null, $translations1);
			$sitemap->add(URL::to('/').'/en/companyCulture', null, 0, 'monthly',[], null, $translations2);
			$sitemap->add(URL::to('/').'/en/aboutUs', null, 0, 'monthly',[], null, $translations3);
			$sitemap->add(URL::to('/').'/en/hiringPage', null, 0, 'monthly',[], null, $translations4);
			$sitemap->add(URL::to('/').'/en/terms-service', null, 0, 'monthly',[], null, $translations5);
			$sitemap->add(URL::to('/').'/en/privacy-policy', null, 0, 'monthly',[], null, $translations6);
			$sitemap->add(URL::to('/').'/en/meetTheTeam', null, 0, 'monthly',[], null, $translations7);
			$sitemap->add(URL::to('/').'/en/contactUs', null, 0, 'monthly',[], null, $translations8);
			$sitemap->add(URL::to('/').'/en/meetSalesTeam', null, 0, 'monthly',[], null, $translations9);
			$sitemap->add(URL::to('/').'/en/cooperatePage', null, 0, 'monthly',[], null, $translations10);
			$sitemap->add(URL::to('/').'/en/faq-buyer', null, 0, 'monthly',[], null, $translations11);
		}

		// show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
		return $sitemap->render('xml');
	}

	public function sharedOfficesitemap()
	{
		$sitemap = App::make("sitemap"); // create sitemap
		$sitemap->setCache('laravel.sitemap-index', 3600); // set cache


		/** TODO:
		 * Uncomment below section after shared office complete...!
		*/
		$sharedOffices = SharedOffice::count();
		$sharedOffices  = (int)ceil($sharedOffices/500);
		for($i=0; $i < $sharedOffices; $i++) {

			$index = $i == 0 ? '' : $i;
			$sitemap->addSitemap(URL::to('sitemap-shared-office'.$index.'.xml'));
		}

		// show sitemap
		return $sitemap->render('sitemapindex', 'utf');
	}
}
