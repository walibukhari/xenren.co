<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\Pushy\PushyAPI;
use App\Models\User;
use App\Models\PushNotificationSettings;
use Illuminate\Support\Facades\Auth;

class SendPushNotification extends Controller
{
    public function saveDeviceToken($user_id, $deviceToken)
    {
        $user_id = Auth::user()->id;
        User::where('id','=',$user_id)->update([
            'web_token' => $deviceToken,
            'post_project_notify' => 1
        ]);
        return collect([
            'success' => 'true',
        ]);
    }
}
