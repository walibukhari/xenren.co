<?php

namespace App\Http\Controllers;

use App\Http\Controllers\PaymentGateways\PayPalPhpSdk\PayPalController;
use App\Models\SharedOffice;
use App\Models\Staff;
use App\Models\TempTransaction;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\User;
use App\Traits\RevenueMonsterTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TopupController extends Controller
{
    use RevenueMonsterTrait;

    public function getStaff($id){
        $officeId = SharedOffice::where('id','=',$id)->first();
        $staff = Staff::where('id','=',$officeId->staff_id)->first();
        return $staff;
    }

    public function index(Request $request)
    {
        $amount = str_replace(',','.',$request->amount);
        if(is_numeric($amount) == true) {
            if ($request->payment_method == '') {
                return back()->with('error', 'Please Select Payment Method');
            } else if ($request->amount == '') {
                return back()->with('error', 'Please Enter Amount');
            }
            if ($request->payment_method == "rm") {
                if(isset($request->type) && $request->type != '' && $request->type == Transaction::TYPE) {
                    $staff = $this->getStaff($request->office_id);
                    $url = (self::generatePaymentLink($request));
                    session(['checkout_id' => $url->checkoutId]);
                    session(['user_id' => $staff->id]);
                    session(['amount' => $amount]);
                    return redirect($url->url);
                } else {
                    $url = (self::generatePaymentLink($request));
                    session(['checkout_id' => $url->checkoutId]);
                    session(['user_id' => Auth::user()->id]);
                    session(['amount' => $amount]);
                    return redirect($url->url);
                }
            } else if ($request->payment_method == 'pp') {
                $ppc = new PayPalController();
                return $ppc->index($request);
            } else {
                return \Redirect::to('/' . \Session::get('lang') . '/')->with('error', 'selected payment gateway not developed yet');
            }
        } else {
            return \Redirect::to('/' . \Session::get('lang') . '/')->with('error', 'Please Enter a valid amount');
        }
    }

    public function redirect(Request $request)
    {
        $data = TempTransaction::with('user','staff')->where('order_id','=',$request->orderId)->first();
        if($data->user_id == 0) {
            $staff = Staff::where('email', '=', $data->staff->email)->first();
            $account_balance = $staff->balance;
            $currency_name = Staff::getCurrencyName($staff->country_id);
            \DB::beginTransaction();
            $amount = $data->amount / 100;
            $newBalance = (int)$account_balance + $amount;
            $tr = Transaction::create([
                'name' => 'Topup Amount',
                'amount' => $amount,
                'milestone_id' => 0,
                'due_date' => Carbon::now()->addYear(1),
                'comment' => session('amount') . ' TopUpped',
                'performed_by_balance_before' => $account_balance,
                'performed_by_balance_after' => $newBalance,
                'balance_before' => $account_balance,
                'balance_after' => $newBalance,
                'performed_by' => $staff->id,
                'performed_to' => $staff->id,
                'release_at' => Carbon::now()->addDay(2),
                'type' => Transaction::TYPE_TOP_UP,
                'currency' => $currency_name,
                'status' => Transaction::STATUS_APPROVED_TRANSACTION,
            ]);
            TransactionDetail::create([
                'transaction_id' => $tr->id,
                'payment_method' => TransactionDetail::PAYMENT_METHOD_REVENUE_MONSTER,
                'raw' => json_encode($request->all())
            ]);
            \DB::commit();
            return \Redirect::to('/' . \Session::get('lang') . '/')->with('message', 'Account Topup');

        } else {
            $user = User::where('email', '=', $data->user->email)->first();
            $account_balance = $user->account_balance;
            $currency_name = User::getCurrencyName($user->country_id);
            \DB::beginTransaction();
            $amount = $data->amount / 100;
            $newBalance = (int)$account_balance + $amount;
            $tr = Transaction::create([
                'name' => 'Topup Amount',
                'amount' => $amount,
                'milestone_id' => 0,
                'due_date' => Carbon::now()->addYear(1),
                'comment' => session('amount') . ' TopUpped',
                'performed_by_balance_before' => $account_balance,
                'performed_by_balance_after' => $newBalance,
                'balance_before' => $account_balance,
                'balance_after' => $newBalance,
                'performed_by' => $user->id,
                'performed_to' => $user->id,
                'type' => Transaction::TYPE_TOP_UP,
                'currency' => $currency_name,
                'status' => Transaction::STATUS_APPROVED_TRANSACTION,
            ]);
            TransactionDetail::create([
                'transaction_id' => $tr->id,
                'payment_method' => TransactionDetail::PAYMENT_METHOD_REVENUE_MONSTER,
                'raw' => json_encode($request->all())
            ]);
            User::where('id', '=', $user->id)->increment('account_balance', $amount);
            \DB::commit();
            return \Redirect::to('/' . \Session::get('lang') . '/myAccount')->with('message', 'Account Topup');
        }
    }
}
