<?php

namespace App\Http\Controllers;

use App\Constants;
use App\Http\Requests;
use App\Models\Countries;
use App\Models\GuestRegister;
use App\Models\Invitation;
use App\Models\UserLanguage;
use App\Models\UserSkill;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;
//use Socialite;


class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

//    public function weiboLogin(Request $request) {
//        return \Socialize::driver('weibo')->redirect();
//    }
//
//    public function weiboCallback(Request $request) {
//        $oauthUser = \Socialize::driver('weibo')->user();
//
////        $client = new \GuzzleHttp\Client();
////        dd($oauthUser);
////        $res = $client->request('GET', 'https://api.weibo.com/2/account/profile/email.json', [
////            'query' => ['access_token' => $oauthUser->token]
////        ]);
////        dd($res);
//
////        var_dump($oauthUser->getId());
////        var_dump($oauthUser->getNickname());
////        var_dump($oauthUser->getName());
////        var_dump($oauthUser->getEmail());
////        var_dump($oauthUser->getAvatar());
////        die;
//
//        //Search if oauthUser->id exists in users table? if yes do login, no do binding(register)
//        $user = User::where('weibo_open_id', '=', $oauthUser->id);
//
//        if ($user->exists()) {
//            \Auth::guard('users')->login($user->first());
//            return redirect()->route('home');
//        } else {
//            return view('auth.register', ['oauthUser' => $oauthUser, 'weiboLogin' => true]);
//        }
//    }

    public function qqLogin(Request $request) {
        return \Socialite::with('qq')->redirect();
    }

    public function qqCallback(Request $request) {
        $oauthUser = Socialite::driver('qq')->user();

        //Search if oauthUser->id exists in users table? if yes do login, no do binding(register)
        $user = User::where('qq_open_id', '=', $oauthUser->id);

        if ($user->exists()) {
            \Auth::guard('users')->login($user->first());
            return redirect()->route('home');
        } else {
            return view('auth.register', ['oauthUser' => $oauthUser, 'qqLogin' => true]);
        }
    }

    public function weixinLogin(Request $request) {
        return \Socialize::driver('weixin')->redirect();
    }

    public function weixinCallback(Request $request) {
//        Log::useDailyFiles(storage_path().'/logs/weixin.log');
//        Log::info('=== Start weixinCallback Log===');

        $oauthUser = \Socialize::driver('weixin')->user();
        //Search if oauthUser->id exists in users table? if yes do login, no create new user
        $user = User::where('weixin_open_id', '=', $oauthUser->id);

        if ($user->exists())
        {
            //login previously using wechat account
            \Auth::guard('users')->login($user->first());
            $user = User::where('weixin_open_id', '=', $oauthUser->id)->first();
            $user->last_login_at = Carbon::now();
            $user->save();
        }
        else
        {
            //create new user
            $user = new User();
            $user->email = null; //dummy email is given and a must due to unique users_email_unique index
            $user->wechat_id = $oauthUser->nickname;
            $user->name = $oauthUser->nickname;
            $user->weixin_open_id = $oauthUser->id;
            if( $oauthUser->user['sex'] == 0  || $oauthUser->user['sex'] == 1 )
            {
                //unknown gender
                //male
                $user->gender = User::GENDER_MALE;
            }
            else if( $oauthUser->user['sex'] == 2 )
            {
                //female
                $user->gender = User::GENDER_FEMALE;
            }

            if( isset($oauthUser->user['country']))
            {
                $country = Countries::where('name', $oauthUser->user['country'])
                    ->orWhere('name_cn', $oauthUser->user['country'])
                    ->first();

                if( $country && $country->id > 0 )
                {
                    $user->country_id = $country->id;
                }
            }

            $user->img_avatar = $oauthUser->avatar;
            $user->last_login_at = Carbon::now();
            $user->save();

            $invitorId = Session::get('invite_code');
//            Log::info('Invite Code Get From Session: ' . $invitorId );
            if( $invitorId > 0 )
            {
                $invitor = User::find($invitorId);
                $invitation = new Invitation();
                $invitation->invitor_id = $invitor->id;
                $invitation->invitee_id = $user->id;
                $invitation->save();
            }

            \Auth::guard('users')->login($user);
        }

        $value = Session::get('redirect_url');
//        Log::info('value : ' . $value);
//        Log::info('=== End Wechat Log===');

        //redirect the wechat home page if login with wechat browser
        Session::put('is_wechat_browser', 'true');

        if( $value == null || $value == "")
        {
            return redirect()->route('home');
        }
        else
        {
            return Redirect::to($value);
        }
    }

    public function weixinwebLogin(Request $request) {
        return \Socialize::driver('weixinweb')->redirect();
    }

    public function weixinwebCallback(Request $request) {
        $oauthUser = \Socialize::driver('weixinweb')->user();

        //Search if oauthUser->id exists in users table? if yes do login, no create new user
        $user = User::where('weixin_open_id', '=', $oauthUser->id);

        if ($user->exists())
        {
            //login previously using wechat account
            \Auth::guard('users')->login($user->first());
            $user = User::where('weixin_open_id', '=', $oauthUser->id)->first();
            $user->last_login_at = Carbon::now();
            $user->save();
        }
        else
        {
            //create new user
            $user = new User();
            $user->email = null; //dummy email is given and a must due to unique users_email_unique index
            $user->wechat_id = $oauthUser->nickname;
            $user->name = $oauthUser->nickname;
            $user->weixin_open_id = $oauthUser->id;
            if( $oauthUser->user['sex'] == 0  || $oauthUser->user['sex'] == 1 )
            {
                //unknown gender
                //male
                $user->gender = User::GENDER_MALE;
            }
            else if( $oauthUser->user['sex'] == 2 )
            {
                //female
                $user->gender = User::GENDER_FEMALE;
            }

            if( isset($oauthUser->user['country']))
            {
                $country = Countries::where('name', $oauthUser->user['country'])
                    ->orWhere('name_cn', $oauthUser->user['country'])
                    ->first();

                if( $country && $country->id > 0 )
                {
                    $user->country_id = $country->id;
                }
            }

            $user->img_avatar = $oauthUser->avatar;
            $user->last_login_at = Carbon::now();
            $user->save();

            \Auth::guard('users')->login($user);
        }

        return redirect()->route('home');
    }

    public function facebookLogin()
    {
        return Socialite::driver('facebook')->redirect();
    }

    //facebook callback handler
    public function handleProviderCallback()
    {
        $member = Socialite::driver('facebook')->user();
		    $account = User::where('facebook_id', $member->getId())->first();
		    if(!is_null($account) && \Auth::user()) {
			    return redirect()->route('home')->with('fb_err', 'This facebook account already associated with another account...!');
		    }
		    $newMember = $this->createProviderFacebook($member);
        if(!\Auth::user()) {
            \Auth::guard('users')->login($newMember);
        }
        return redirect()->route('home');
    }

     public function createProviderFacebook($member)
    {
         $account = User::where('facebook_id', $member->getId())->first();
         if(!$account && \Auth::user()) {
            User::where('id', '=', \Auth::user()->id)->update([
                'facebook_id' => $member->getId()
            ]);
            return $account;
         }
        $account = User::where('email', $member->getEmail())->first();
        if(!is_null($account)) {
            return $account;
        }
         if (!is_null($account)) {
	         $skills = null;
	         $languages = null;
	         $account->facebook_id = $member->getId();
	         $account->img_avatar = $member->getAvatar();
//	         $account->real_name = $member->getName();
	         $account->name = $member->getName();
	         $account->email = $member->getEmail();
	         $account->password = bcrypt('123456789');
	         $referral = session('referral_invite');
	         if(isset($referral) && $referral != '') {
		         $userData = GuestRegister::where('code', $referral)->first();
		         $account->about_me = $userData['about'];
		         $account->upline_id = $userData['refer_by'];
		         if(isset($userData['skills']) && $userData['skills'] != '') {
		         	  $skills = explode(',', $userData['skills']);
		         }
		         if(isset($userData['language']) && $userData['language'] != '') {
			          $languages = explode(',', $userData['language']);
		         }
	         }
	         $account->save();
	         
	         if(!is_null($skills)) {
		         UserSkill::where('user_id', '=', $account->id)->delete();
		         foreach ($skills as $k => $v) {
		            UserSkill::create([
		            	'user_id' => $account->id,
			            'skill_id' => $v
		            ]);
	            }
	         }
	         
	         if(!is_null($languages)) {
		         UserLanguage::where('user_id', '=', $account->id)->delete();
		         foreach ($languages as $k => $v) {
			         UserLanguage::create([
				         'user_id' => $account->id,
				         'skill_id' => $v
			         ]);
		         }
	         }
	         
           return $account;
        } else {
            $account = new User();
            $account->facebook_id = $member->getId();
            $account->img_avatar = $member->getAvatar();
//            $account->real_name = $member->getName();
	          $account->email = $member->getEmail();
            $account->save();
            return $account;
        }

    }
		
		public function updatePassword(Request $request)
		{
			$user = User::where('id', '=', \Auth::user()->id)->first();
			$user->password = $request->password;
			$user->save();
			
			return collect([
				'status' => 'success',
				'user' => $user,
				'user_id' =>  \Auth::user()->id,
				'data' =>  User::where('id', '=', \Auth::user()->id)->first()
			]);
			
		}
}
