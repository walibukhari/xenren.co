<?php

namespace App\Http\Middleware;

use Closure;

class LanguageRedirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        if(\Session::get('lang') == 'en') {
//            return redirect('/en');
//        } else {
//            return redirect('/cn');
//        }
        return $next($request);
    }

    public function index()
    {
        $disputes = ProjectFileDisputes::with([
            'transaction.milestone.project',
            'transaction.receiver',
            'transaction.sender',
            'milestones',
            'milestones.project.creator',
            'milestones.applicant'
        ])->has('transaction')->orHas('milestones')
            ->get();

        return view('backend.disputeRequests.index', [
            'disputes' => $disputes
        ]);
    }

    public function view($disputeId)
    {
        $dispute = ProjectFileDisputes::with([
            'transaction.receiver',
            'transaction.sender',
            'milestones.project.creator',
            'milestones.applicant',
            'comments' => function ($q) {
                //              $q->withTrashed();
            },
            'userAttachments' => function ($q) {
                //              $q->withTrashed();
            }
        ])->where('id', '=', $disputeId)
            ->first();

        if (isset($dispute->transaction)) {
            $sender_id = isset($dispute->transaction->sender->id) ? $dispute->transaction->sender->id : '';
            $receiver_id = isset($dispute->transaction->receiver->id) ? $dispute->transaction->receiver->id : '';
            $sender = isset($dispute->transaction->sender) ? $dispute->transaction->sender : '';
            $receiver = isset($dispute->transaction->receiver) ? $dispute->transaction->receiver : '';
        } else {
            $sender_id = isset($dispute->milestones->project->creator->id) ? $dispute->milestones->project->creator->id : '';
            $receiver_id = isset($dispute->milestones->applicant->id) ? $dispute->milestones->applicant->id : '';
            $sender = isset($dispute->milestones->project->creator) ? $dispute->milestones->project->creator : '';
            $receiver = isset($dispute->milestones->applicant) ? $dispute->milestones->applicant : '';
        }

        $senderComment = $dispute->comments->where('deleted_at', null)->where('user_id', $sender_id)->values();
        $senderComment = isset($senderComment[0]) ? $senderComment[0] : null;

        $receivedComment = $dispute->comments->where('deleted_at', null)->where('user_id', $receiver_id)->values();
        $receivedComment = isset($receivedComment[0]) ? $receivedComment[0] : null;

        $senderAttachments = $dispute->userAttachments->where('deleted_at', null)->where('user_id', $sender_id)->values();
        $receivedAttachments = $dispute->userAttachments->where('deleted_at', null)->where('user_id', $receiver_id)->values();

        return view('backend.disputeRequests.view', [
            'disputes' => $dispute,
            'senderComment' => $senderComment,
            'receiverComment' => $receivedComment,
            'senderAttachments' => $senderAttachments,
            'receiverAttachments' => $receivedAttachments,
            'sender' => $sender,
            'receiver' => $receiver,
            'dispute' => $dispute]);
    }

    public function history($disputeId, $userId)
    {
        $attachments = ProjectDisputesAttachments::where('project_dispute_id', '=', $disputeId)->where('user_id', '=', $userId)->onlyTrashed()->get();
        $comments = ProjectDisputesComment::where('project_dispute_id', '=', $disputeId)->where('user_id', '=', $userId)->onlyTrashed()->get();
        $user = User::where('id', '=', $userId)->first();

        return view('backend.disputeRequests.history', [
            'attachments' => $attachments,
            'comments' => $comments,
            'user' => $user,
        ]);
    }

    public function approveDispute(Request $request)
    {
        ProjectFileDisputes::where('id', '=', $request->dispute_id)->update([
            'status' => ProjectFileDisputes::STATUS_FILE_DISPUTE_IN_APPROVED,
            'admin_comment' => $request->admin_comment
        ]);
        return collect([
            'status' => 'success',
            'message' => 'Dispute Approved..1'
        ]);
    }

    public function index()
    {
        $disputes = ProjectFileDisputes::with([
            'transaction.milestone.project',
            'transaction.receiver',
            'transaction.sender',
            'milestones',
            'milestones.project.creator',
            'milestones.applicant'
        ])->has('transaction')->orHas('milestones')
            ->get();

        return view('backend.disputeRequests.index', [
            'disputes' => $disputes
        ]);
    }

    public function view($disputeId)
    {
        $dispute = ProjectFileDisputes::with([
            'transaction.receiver',
            'transaction.sender',
            'milestones.project.creator',
            'milestones.applicant',
            'comments' => function ($q) {
                //              $q->withTrashed();
            },
            'userAttachments' => function ($q) {
                //              $q->withTrashed();
            }
        ])->where('id', '=', $disputeId)
            ->first();

        if (isset($dispute->transaction)) {
            $sender_id = isset($dispute->transaction->sender->id) ? $dispute->transaction->sender->id : '';
            $receiver_id = isset($dispute->transaction->receiver->id) ? $dispute->transaction->receiver->id : '';
            $sender = isset($dispute->transaction->sender) ? $dispute->transaction->sender : '';
            $receiver = isset($dispute->transaction->receiver) ? $dispute->transaction->receiver : '';
        } else {
            $sender_id = isset($dispute->milestones->project->creator->id) ? $dispute->milestones->project->creator->id : '';
            $receiver_id = isset($dispute->milestones->applicant->id) ? $dispute->milestones->applicant->id : '';
            $sender = isset($dispute->milestones->project->creator) ? $dispute->milestones->project->creator : '';
            $receiver = isset($dispute->milestones->applicant) ? $dispute->milestones->applicant : '';
        }

        $senderComment = $dispute->comments->where('deleted_at', null)->where('user_id', $sender_id)->values();
        $senderComment = isset($senderComment[0]) ? $senderComment[0] : null;

        $receivedComment = $dispute->comments->where('deleted_at', null)->where('user_id', $receiver_id)->values();
        $receivedComment = isset($receivedComment[0]) ? $receivedComment[0] : null;

        $senderAttachments = $dispute->userAttachments->where('deleted_at', null)->where('user_id', $sender_id)->values();
        $receivedAttachments = $dispute->userAttachments->where('deleted_at', null)->where('user_id', $receiver_id)->values();

        return view('backend.disputeRequests.view', [
            'disputes' => $dispute,
            'senderComment' => $senderComment,
            'receiverComment' => $receivedComment,
            'senderAttachments' => $senderAttachments,
            'receiverAttachments' => $receivedAttachments,
            'sender' => $sender,
            'receiver' => $receiver,
            'dispute' => $dispute]);
    }

    public function history($disputeId, $userId)
    {
        $attachments = ProjectDisputesAttachments::where('project_dispute_id', '=', $disputeId)->where('user_id', '=', $userId)->onlyTrashed()->get();
        $comments = ProjectDisputesComment::where('project_dispute_id', '=', $disputeId)->where('user_id', '=', $userId)->onlyTrashed()->get();
        $user = User::where('id', '=', $userId)->first();

        return view('backend.disputeRequests.history', [
            'attachments' => $attachments,
            'comments' => $comments,
            'user' => $user,
        ]);
    }

    public function approveDispute(Request $request)
    {
        ProjectFileDisputes::where('id', '=', $request->dispute_id)->update([
            'status' => ProjectFileDisputes::STATUS_FILE_DISPUTE_IN_APPROVED,
            'admin_comment' => $request->admin_comment
        ]);
        return collect([
            'status' => 'success',
            'message' => 'Dispute Approved..1'
        ]);
    }

    public function rejectDispute(Request $request)
    {
        ProjectFileDisputes::where('id', '=', $request->dispute_id)->update([
            'status' => ProjectFileDisputes::STATUS_FILE_DISPUTE_IN_REJECTED,
            'admin_comment' => $request->admin_comment
        ]);
        return collect([
            'status' => 'success',
            'message' => 'Dispute Rejected..1'
        ]);
    }

    public function index()
    {
        $disputes = ProjectFileDisputes::with([
            'transaction.milestone.project',
            'transaction.receiver',
            'transaction.sender',
            'milestones',
            'milestones.project.creator',
            'milestones.applicant'
        ])->has('transaction')->orHas('milestones')
            ->get();

        return view('backend.disputeRequests.index', [
            'disputes' => $disputes
        ]);
    }

    public function view($disputeId)
    {
        $dispute = ProjectFileDisputes::with([
            'transaction.receiver',
            'transaction.sender',
            'milestones.project.creator',
            'milestones.applicant',
            'comments' => function ($q) {
                //              $q->withTrashed();
            },
            'userAttachments' => function ($q) {
                //              $q->withTrashed();
            }
        ])->where('id', '=', $disputeId)
            ->first();

        if (isset($dispute->transaction)) {
            $sender_id = isset($dispute->transaction->sender->id) ? $dispute->transaction->sender->id : '';
            $receiver_id = isset($dispute->transaction->receiver->id) ? $dispute->transaction->receiver->id : '';
            $sender = isset($dispute->transaction->sender) ? $dispute->transaction->sender : '';
            $receiver = isset($dispute->transaction->receiver) ? $dispute->transaction->receiver : '';
        } else {
            $sender_id = isset($dispute->milestones->project->creator->id) ? $dispute->milestones->project->creator->id : '';
            $receiver_id = isset($dispute->milestones->applicant->id) ? $dispute->milestones->applicant->id : '';
            $sender = isset($dispute->milestones->project->creator) ? $dispute->milestones->project->creator : '';
            $receiver = isset($dispute->milestones->applicant) ? $dispute->milestones->applicant : '';
        }

        $senderComment = $dispute->comments->where('deleted_at', null)->where('user_id', $sender_id)->values();
        $senderComment = isset($senderComment[0]) ? $senderComment[0] : null;

        $receivedComment = $dispute->comments->where('deleted_at', null)->where('user_id', $receiver_id)->values();
        $receivedComment = isset($receivedComment[0]) ? $receivedComment[0] : null;

        $senderAttachments = $dispute->userAttachments->where('deleted_at', null)->where('user_id', $sender_id)->values();
        $receivedAttachments = $dispute->userAttachments->where('deleted_at', null)->where('user_id', $receiver_id)->values();

        return view('backend.disputeRequests.view', [
            'disputes' => $dispute,
            'senderComment' => $senderComment,
            'receiverComment' => $receivedComment,
            'senderAttachments' => $senderAttachments,
            'receiverAttachments' => $receivedAttachments,
            'sender' => $sender,
            'receiver' => $receiver,
            'dispute' => $dispute]);
    }

    public function history($disputeId, $userId)
    {
        $attachments = ProjectDisputesAttachments::where('project_dispute_id', '=', $disputeId)->where('user_id', '=', $userId)->onlyTrashed()->get();
        $comments = ProjectDisputesComment::where('project_dispute_id', '=', $disputeId)->where('user_id', '=', $userId)->onlyTrashed()->get();
        $user = User::where('id', '=', $userId)->first();

        return view('backend.disputeRequests.history', [
            'attachments' => $attachments,
            'comments' => $comments,
            'user' => $user,
        ]);
    }

    public function approveDispute(Request $request)
    {
        ProjectFileDisputes::where('id', '=', $request->dispute_id)->update([
            'status' => ProjectFileDisputes::STATUS_FILE_DISPUTE_IN_APPROVED,
            'admin_comment' => $request->admin_comment
        ]);
        return collect([
            'status' => 'success',
            'message' => 'Dispute Approved..1'
        ]);
    }

    public function rejectDispute(Request $request)
    {
        ProjectFileDisputes::where('id', '=', $request->dispute_id)->update([
            'status' => ProjectFileDisputes::STATUS_FILE_DISPUTE_IN_REJECTED,
            'admin_comment' => $request->admin_comment
        ]);
        return collect([
            'status' => 'success',
            'message' => 'Dispute Rejected..1'
        ]);
    }

    public function index()
    {
        $disputes = ProjectFileDisputes::with([
            'transaction.milestone.project',
            'transaction.receiver',
            'transaction.sender',
            'milestones',
            'milestones.project.creator',
            'milestones.applicant'
        ])->has('transaction')->orHas('milestones')
            ->get();

        return view('backend.disputeRequests.index', [
            'disputes' => $disputes
        ]);
    }

    public function view($disputeId)
    {
        $dispute = ProjectFileDisputes::with([
            'transaction.receiver',
            'transaction.sender',
            'milestones.project.creator',
            'milestones.applicant',
            'comments' => function($q) {
                //              $q->withTrashed();
            },
            'userAttachments' => function($q) {
                //              $q->withTrashed();
            }
            ])->where('id', '=', $disputeId)
            ->first();

        if(isset($dispute->transaction)) {
            $sender_id = isset($dispute->transaction->sender->id) ? $dispute->transaction->sender->id : '';
            $receiver_id = isset($dispute->transaction->receiver->id) ? $dispute->transaction->receiver->id : '';
            $sender = isset($dispute->transaction->sender) ? $dispute->transaction->sender : '';
            $receiver = isset($dispute->transaction->receiver) ? $dispute->transaction->receiver : '';
        }else {
            $sender_id = isset($dispute->milestones->project->creator->id) ? $dispute->milestones->project->creator->id : '';
            $receiver_id = isset($dispute->milestones->applicant->id) ? $dispute->milestones->applicant->id : '';
            $sender = isset($dispute->milestones->project->creator) ? $dispute->milestones->project->creator : '';
            $receiver = isset($dispute->milestones->applicant) ? $di
        }
        }
            }
        ])
    }
        ])
        ])
    }
        ])
        ])
    }
        ])
        ])
    }
        ])
    }
        ])
        }
        }
            }
        ])
    }
        ])
        ])
    }
        ])
        ])
    }
        ])
        ])
    }
        ])
    }
        ])
        }
        }
            }
        ])
    }
        ])
        ])
    }

    public function rejectDispute(Request $request)
    {
        ProjectFileDisputes::where('id', '=', $request->dispute_id)->update([
            'status' => ProjectFileDisputes::STATUS_FILE_DISPUTE_IN_REJECTED,
            'admin_comment' => $request->admin_comment
        ]);
        return collect([
            'status' => 'success',
            'message' => 'Dispute Rejected..1'
        ]);
    }

    public function index()
    {
        $disputes = ProjectFileDisputes::with([
            'transaction.milestone.project',
            'transaction.receiver',
            'transaction.sender',
            'milestones',
            'milestones.project.creator',
            'milestones.applicant'
        ])->has('transaction')->orHas('milestones')
            ->get();

        return view('backend.disputeRequests.index', [
            'disputes' => $disputes
        ]);
    }

    public function view($disputeId)
    {
        $dispute = ProjectFileDisputes::with([
            'transaction.receiver',
            'transaction.sender',
            'milestones.project.creator',
            'milestones.applicant',
            'comments' => function($q) {
                //              $q->withTrashed();
            },
            'userAttachments' => function($q) {
                //              $q->withTrashed();
            }
            ])->where('id', '=', $disputeId)
            ->first();

        if(isset($dispute->transaction))
            }
        ])
    }
        ])
        ])
    }
        ])
        ])
    }
        ])
        ])
    }
        ])
    }
        ])
        }
        }
            }
        ])
    }
        ])
        ])
    }
}
