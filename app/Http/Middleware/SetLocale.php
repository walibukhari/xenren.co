<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Request;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \App::setLocale(session('lang'));
        $locale = app()->getLocale();
        if(!isset($locale) || $locale == '' || is_null($locale)) {
            session([
                'lang' => 'en'
            ]);
            \App::setLocale('en');
        }
        if(Request::isMethod('get') && $request->segment(1)!='logout')
        {
            if($request->segment(1)=='cn')
            {
                session([
                    'lang' => 'cn'
                ]);
                \App::setLocale('cn');
            }

            elseif($request->segment(1)=='en')
            {
                session([
                    'lang' => 'en'
                ]);
                \App::setLocale('en');
            }
//          dd($request->url());
//            $segments = $request->segments();
//            if(! (in_array('en',$segments) || in_array('cn',$segments)))
//            {
//                $lang_addition = Session::get('lang');
//                $url = implode('/',array_prepend($segments,$lang_addition));
//                return redirect($url);
//
//            }
        }



        return $next($request);
    }
}
