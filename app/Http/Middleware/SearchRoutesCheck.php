<?php
namespace App\Http\Middleware;
use Closure;
use Session;
use Request;

class SearchRoutesCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        if(Request::isMethod('get')) {
        if(($request->segment(1)=='remote-jobs-online' || $request->segment(1)=='Coworkers'|| $request->segment(1)=='postProject' || $request->segment(1)=='login' || $request->segment(1)=='register'  || $request->segment(1)=='resume')|| $request->segment(1)=='personalInformation' || $request->segment(1)=='userCenter') {
            $furl = $request->fullUrl();
            $segments = $request->segments();
            if(! (in_array('en',$segments) || in_array('cn',$segments)))  {
                $lang_addition = Session::get('lang');
                $getparams = explode('?',$furl);
                $url = implode('/',array_prepend($segments,$lang_addition));
                if(isset($getparams[1])) {
                    $url = $url.'?'.$getparams[1];
                }
                return redirect($url);
            }
        }
    }
    return $next($request);
        
    }
}
