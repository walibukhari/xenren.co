<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
//use Tymon\JWTAuth\Facades\JWTFactory;
use Exception;
use App\Models\User;
//use Tymon\JWTAuth\Contracts\JWTSubject;


class ValidateWhenTokenExist
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    try {
	    	$isToken = $request->header('Authorization');
	    	if(!is_null($isToken)) {
			    $user = JWTAuth::parseToken()->authenticate();
		    }
	    } catch (Exception $e) {
		    if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
			    return response()->json([
				    'status'=> 'failure',
				    'error'=> 'Token is Invalid'
			    ]);
		    }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
			    return response()->json([
				    'status'=> 'failure',
				    'error'=>'Token is Expired'
			    ]);
		    }else{
			    return response()->json([
				    'status'=> 'failure',
				    'error'=> $e->getMessage()
			    ]);
		    }
	    }
	    return $next($request);
    }
}
