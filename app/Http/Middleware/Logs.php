<?php

namespace App\Http\Middleware;

use Closure;

class Logs
{
	
		protected $end;
		protected $start;
	
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    $this->start = microtime(true);
	    return $next($request);
    }
	
		public function terminate($request, $response)
		{
			$this->end = microtime(true);
			$this->log($request, $response);
		}
		
		protected function log($request, $response)
		{
			$duration = $this->end - $this->start;
			$url = $request->fullUrl();
			$method = $request->getMethod();
			$ip = $request->getClientIp();
			$headers = $request->headers;
			$requestParameters = $request->all();

			$log = "{$ip}: {$method}@{$url} - {$duration}ms";

			\Log::channel('api')->info($log);
			\Log::channel('api')->info($headers);
			\Log::channel('api')->info($requestParameters);
			\Log::channel('api')->info($response);
		}
}
