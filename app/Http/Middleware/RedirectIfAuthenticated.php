<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return redirect('/home');
        }
//        $ip = file_get_contents("http://ipecho.net/plain");
//        $url = 'http://ip-api.com/json/'.$ip;
//        $tz = file_get_contents($url);
//        $tz = json_decode($tz,true)['timezone'];
//        $timezone = \Carbon::now($tz);
//        config(['app.timezone' => $tz]);

        return $next($request);
    }
}
