<?php

namespace App\Http\Middleware;

use Closure;

class RedirectRoute
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $sharedOfficePath = 'https://coworkspace.xenren.co/'.$request->path();
        $xenRenPath = 'https://www.xenren.co/'.$request->path();
        if (strpos($request->root(), 'http') !== false) {
            return $next($request);
        }

       // $xenRenPath = '';
        if (strpos($request->path(), 'coworking-spaces') !== false) {
            if (strpos($request->root(), 'coworkspace.xenren.co') !== false) {
                return redirect()->away($sharedOfficePath);
            } else {
                return $next($request);
            }
        } else {
            if (strpos($request->root(), 'www.xenren.co') !== false) {
                return redirect()->away($xenRenPath);
            } else {
                return $next($request);
            }
        }
        return $next($request);
    }
}
