<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class SharedOffice
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if(Auth::user()) {
			$user_id = \Auth::user()->id;
			$user = \App\Models\User::find($user_id);
			$token = JWTAuth::fromUser($user, []);
			session(['api_token' => $token]);
			return $next($request);
		} else {
			$user = \App\Models\User::first();
			$token = JWTAuth::fromUser($user, []);
			session(['api_token' => $token]);
			return $next($request);
		}
	}
}
