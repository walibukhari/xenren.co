<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App;

class StaffHasPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {

//        App::setLocale('cn');

        self::addAndUpdatePermission($permission);
        if (Auth::guard('staff')->check()) {
            if (!Auth::guard('staff')->user()->hasPermission($permission)) {
                if ($request->ajax()) {
                    return response('Unauthorized.', 401);
                } else {
//                    addError('No Permission');
                    return redirect()->route("backend.index");
                }
            }
        } else {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest(route("staff.login"));
            }
        }
        if(Auth::guard('staff')->user()->staff_type == App\Models\Staff::STAFF_TYPE_STAFF && Auth::guard('staff')->user()->is_operator == 0) {
            $staff = Auth::guard('staff')->user();
            App\Models\OwnerProgress::setProgress($staff,$permission);
        }
        return $next($request);
    }

    private function addAndUpdatePermission($permission) {
        // check path if not exist
        $path = storage_path('app/json');
        if (!file_exists($path)) {
            mkdir($path);
        }

        // create file if not exist
        $file = $path . '/permissions.json';
        if (!file_exists($file)) {
            file_put_contents($file, json_encode(array()));
        }

        // get list permission
        $listPermission = Cache::rememberForever('permission_list', function() use ($file) {
            return (array) json_decode(file_get_contents($file));
        });

        // var_dump($listPermission);die();
        if (!in_array($permission, $listPermission)) {
            $listPermission []= $permission;

            // put back
            file_put_contents($file, json_encode($listPermission));
            Cache::pull('permission_list');
        }
    }
}
