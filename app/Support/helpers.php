<?php

use App\Models\Portfolio;
use App\Models\ProjectApplicant;
use App\Models\Settings;
use App\Models\UserIdentity;
use App\Models\UserSkill;
use App\Services\Mixpanel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\MessageBag;
use Illuminate\Http\Exception\HttpResponseException;
use Twilio\Rest\Client;


function pushFlash($key, $value) {
    $values = Session::get($key, []);
    $values[] = $value;
    Session::flash($key, $values);
}

function makeJSON($status, $msg, $extra = array()) {
    $data = array(
        'status' => $status === true ? 'success' : 'error',
        'msg' => $msg
    );

    if (isset($extra) && count($extra) > 0) {
        foreach ($extra as $key => $var) {
            $data[$key] = $var;
        }
    }

    return json_encode($data);
}

function makeJSONResponse($status = false, $msg = '', $data = array()) {
    if (isEmpty($msg)) {
        $msg = 'Please fix the following error(s)';
    }

    return response()->make(array(
                'status' => $status === true ? 'success' : 'error',
                'msg' => $msg,
                'data' => $data
    ));
}

function addError($msg) {
    if (!is_array($msg)) {
        pushFlash('flash_error', $msg);
    } else {
        foreach ($msg as $key => $var) {
            pushFlash('flash_error', $var);
        }
    }
}

function addInfo($msg) {
    if (!is_array($msg)) {
        pushFlash('flash_info', $msg);
    } else {
        foreach ($msg as $key => $var) {
            pushFlash('flash_info', $var);
        }
    }
}

function addSuccess($msg) {
    if (!is_array($msg)) {
        pushFlash('flash_success', $msg);
    } else {
        foreach ($msg as $key => $var) {
            pushFlash('flash_success', $var);
        }
    }
}

function addValidatorMsg($arr) {
    foreach ($arr->getMessages() as $key => $var) {
        foreach ($var as $k => $v) {
            addError($v);
        }
    }
}

function touchFolder($path) {
    $path = public_path($path);

    if (!file_exists($path)) {
        mkdir($path, 0777, true);
    }
}

function generateRandomUniqueName($length = 40) {
    return Str::random($length);
}

function generateRandomHtmlId($length = 20) {
    $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function getValidImageMimeType() {
    return array(
        'image/gif', 'image/jpg', 'image/jpeg', 'image/png'
    );
}

function isImage($name) {
    if (\Input::hasFile($name)) {
        $images_mimes = getValidImageMimeType();

        if (in_array(\Input::file($name)->getMimeType(), $images_mimes)) {
            return true;
        }
    }

    return false;
}

function convertMimeToExt($mime) {
    if ($mime == 'image/jpeg')
        $extension = '.jpg';
    elseif ($mime == 'image/png')
        $extension = '.png';
    elseif ($mime == 'image/gif')
        $extension = '.gif';
    else
        $extension = '';

    return $extension;
}

function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds') {
    $sets = array();
    if (strpos($available_sets, 'l') !== false)
        $sets[] = 'abcdefghjkmnpqrstuvwxyz';
    if (strpos($available_sets, 'u') !== false)
        $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
    if (strpos($available_sets, 'd') !== false)
        $sets[] = '23456789';
    if (strpos($available_sets, 's') !== false)
        $sets[] = '!@#$%&*?';

    $all = '';
    $password = '';
    foreach ($sets as $set) {
        $password .= $set[array_rand(str_split($set))];
        $all .= $set;
    }

    $all = str_split($all);
    for ($i = 0; $i < $length - count($sets); $i++)
        $password .= $all[array_rand($all)];

    $password = str_shuffle($password);

    if (!$add_dashes)
        return $password;

    $dash_len = floor(sqrt($length));
    $dash_str = '';
    while (strlen($password) > $dash_len) {
        $dash_str .= substr($password, 0, $dash_len) . '-';
        $password = substr($password, $dash_len);
    }
    $dash_str .= $password;
    return $dash_str;
}

function notEmpty($value) {
    if ($value != '' && $value != null && $value != false) {
        return true;
    } else {
        return false;
    }
}

function isEmpty($value) {
	return notEmpty($value) === true ? false : true;
}

function notDigit($value) {
	if ($value != '' && $value != null && $value != false && $value != true && $value >= 0 && ctype_digit((string)$value)) {
        return false;
    } else {
        return true;
    }
}

function isDigit($value) {
    if ($value != '' && $value != null && $value != false && $value != true && $value >= 0 && ctype_digit((string)$value)) {
        return true;
    } else {
        return false;
    }
}

function isFund($value) {
    return preg_match_all('/[\d](.[\d]{1,2})?/im' ,$value) ? true : false;
}

function notFund($value) {
    return preg_match_all('/[\d](.[\d]{1,2})?/im' ,$value) ? false : true;
}

function isPercentage($value) {
    if (is_numeric($value) && $value <= 100) {
        return true;
    } else {
        return false;
    }
}

function notPercentage($value) {
    if (is_numeric($value) && $value <= 100) {
        return false;
    } else {
        return true;
    }
}

function isNumber($value) {
    return ctype_digit((string)$value) ? true : false;
}

function notNumber($value) {
    return ctype_digit((string)$value) ? false : true;
}

function isPost()
{
    return strtolower(request()->method()) == 'post' ? true : false;
}

function isAjax()
{
    return request()->ajax() ? true : false;
}

function string_contains($str, array $arr)
{
    foreach($arr as $a) {
        if (mb_stripos($str,$a) !== false) return true;
    }
    return false;
}

function getSensitiveName() {
    $sensitive_name = array(
        'admin', 'moderator', 'operator',
    );

    return $sensitive_name;
}

function checkSensitiveName($name) {
    $sensitive_name = getSensitiveName();
    return string_contains(strtolower($name), $sensitive_name);
}

function writeErrorLog($log) {
    if ($log instanceof \Exception) {
        Log::error(
            "ERROR CODE = " . $log->getCode() . ": \n" . "Unexpected ERROR on " . $log->getFile() . " LINE: " . $log->getLine() . ", " . $log->getMessage() . "\n"
        );
    } else {
        Log::error($log);
    }
}

function toFund($value) {
    return number_format($value, 2);
}

function pc_permute($items, $perms = array()) {
    if (empty($items)) {
        return join(',', $perms);
    } else {
        $lists = array();
        for ($i = count($items) - 1; $i >= 0; --$i) {
            $newitems = $items;
            $newperms = $perms;
            list($foo) = array_splice($newitems, $i, 1);
            array_unshift($newperms, $foo);
            $tmp = pc_permute($newitems, $newperms);
            $lists[] = str_replace(',', '', $tmp);
        }

        return $lists;
    }
}

function noticeDeveloper($string, $file, $line, $extra = null) {

    Mail::send('emails.developer', ['string' => $string, 'file' => $file, 'line' => $line, 'extra' => $extra], function($m)
    {
        $now = new Carbon\Carbon();
        $m->from(env('MAIL_USERNAME'), 'System Alert - ' . $now->format('d-m-Y'));

        $m->to(env('DEVELOPER_EMAIL'), 'Developer')
            ->subject('System Error!');
    });
}

function buildRemoteLinkHtml($arr) {
    $url = isset($arr['url']) ? $arr['url'] : '#';
    $description = isset($arr['description']) ? $arr['description'] : 'button';
    $color = isset($arr['color']) ? $arr['color'] : 'blue';
    $modal = isset($arr['modal']) ? $arr['modal'] : '#remote-modal-large';
    $toolTip = isset($arr['tool-tip']) ? $arr['tool-tip'] : '';

    return sprintf('<a title="'.$toolTip.'" href="%s" data-toggle="modal" data-target="%s" class="btn btn-xs primary uppercase %s">%s</a>', $url, $modal, $color, $description);
}

function buildConfirmationLinkHtml($arr) {
    $url = isset($arr['url']) ? $arr['url'] : '#';
    $description = isset($arr['description']) ? $arr['description'] : 'button';
    $color = isset($arr['color']) ? $arr['color'] : 'red';
    $title = isset($arr['title']) ? $arr['title'] : trans('common.confirm') . '?';
    $content = isset($arr['content']) ? $arr['content'] : trans('common.are_you_sure');
    $redirect = isset($arr['redirect']) && in_array($arr['redirect'], [true, 1, '1', 'yes']) ? 'yes' : 'no';
    $modal = isset($arr['modal']) ? $arr['modal'] : '#confirm-modal';
    $toolTip = isset($arr['tool-tip']) ? $arr['tool-tip'] : '';

    return sprintf('<a title="'.$toolTip.'" data-href="%s" class="uppercase btn btn-xs primary %s" data-redirect="%s" data-toggle="modal" data-target="%s" data-header="%s" data-body="%s">%s</a>', $url, $color, $redirect, $modal, $title, $content, $description);
}

function getMetronicColours() {
    $arr = array(
        'white', 'default', 'dark', 'blue', 'blue-madison', 'blue-chambray', 'blue-ebonyclay', 'blue-hoki',
        'blue-steel', 'blue-soft', 'blue-dark', 'blue-sharp', 'green', 'green-meadow', 'green-seagreen', 'green-turquoise',
        'green-haze', 'green-jungle', 'green-soft', 'green-dark', 'green-sharp', 'grey', 'grey-steel',
        'grey-cararra', 'grey-gallery', 'grey-cascade', 'grey-silver', 'grey-salsa', 'grey-salt', 'grey-mint',
        'red', 'red-pink', 'red-sunglo', 'red-intense', 'red-thunderbird', 'red-flamingo', 'red-soft', 'red-haze',
        'red-mint', 'yellow', 'yellow-gold', 'yellow-casablanca', 'yellow-crusta', 'yellow-lemon', 'yellow-saffron',
        'yellow-soft', 'yellow-haze', 'yellow-mint', 'purple', 'purple-plum', 'purple-medium', 'purple-studio',
        'purple-wisteria', 'purple-seance', 'purple-intense', 'purple-sharp', 'purple-soft',
    );

    return $arr;
}

function getRandomMetronicColour() {
    $arr = getMetronicColours();
    return $arr[array_rand($arr)];
}

function makeResponse($msg, $error = false) {
    $status = $error === false ? 200 : 422;

    if (is_array($msg)) {
        $bag = new MessageBag();
        foreach ($msg as $key => $var) {
            $bag->add($key, $var);
        }
    } else if (is_string($msg)) {
        $bag = new MessageBag();
        $bag->add(generateRandomUniqueName(), $msg);
    } else if ($msg == null || $msg == '' || $msg == false || empty($msg)) {
        $bag = new MessageBag();
        $bag->add('unknown_error', trans('common.unknown_error'));
    } else {
        $bag = $msg;
    }

    if (isAjax()) {
        return new JsonResponse($bag->toArray(), $status);
    } else {
        return redirect()->back()
            ->withInput()
            ->withErrors($bag->toArray());
    }
}

function convertCNDateTimePicker($value){
    //01 三月 2016 - 14:25 ($value)
    //01 March 2016 - 14:25 ($result)

    if (strpos($value, '一月') !== false) {
        $result = str_replace('一月', 'January', $value);
        return $result;
    }else if(strpos($value, '二月') !== false){
        $result = str_replace('二月', 'February', $value);
        return $result;
    }else if(strpos($value, '三月') !== false){
        $result = str_replace("三月", "March", $value);
        return $result;
    }else if(strpos($value, '四月') !== false){
        $result = str_replace('四月', 'April', $value);
        return $result;
    }else if(strpos($value, '五月') !== false){
        $result = str_replace('五月', 'May', $value);
        return $result;
    }else if(strpos($value, '六月') !== false){
        $result = str_replace('六月', 'June', $value);
        return $result;
    }else if(strpos($value, '七月') !== false){
        $result = str_replace('七月', 'July', $value);
        return $result;
    }else if(strpos($value, '八月') !== false){
        $result = str_replace('八月', 'August', $value);
        return $result;
    }else if(strpos($value, '九月') !== false){
        $result = str_replace('九月', 'September', $value);
        return $result;
    }else if(strpos($value, '十月') !== false){
        $result = str_replace('十月', 'October', $value);
        return $result;
    }else if(strpos($value, '十一月') !== false){
        $result = str_replace('十一月', 'November', $value);
        return $result;
    }else if(strpos($value, '十二月') !== false){
        $result = str_replace('十二月', 'December', $value);
        return $result;
    }
}

function convertENDateTimePicker($value){
    //01 三月 2016 - 14:25 ($value)
    //01 March 2016 - 14:25 ($result)

    if (strpos($value, 'January') !== false) {
        $result = str_replace('January', '一月', $value);
        return $result;
    }else if(strpos($value, 'February') !== false){
        $result = str_replace('February', '二月', $value);
        return $result;
    }else if(strpos($value, 'March') !== false){
        $result = str_replace("March", "三月", $value);
        return $result;
    }else if(strpos($value, 'April') !== false){
        $result = str_replace('April', '四月', $value);
        return $result;
    }else if(strpos($value, 'May') !== false){
        $result = str_replace('May', '五月', $value);
        return $result;
    }else if(strpos($value, 'June') !== false){
        $result = str_replace('June', '六月', $value);
        return $result;
    }else if(strpos($value, 'July') !== false){
        $result = str_replace('July', '七月', $value);
        return $result;
    }else if(strpos($value, 'August') !== false){
        $result = str_replace('August', '八月', $value);
        return $result;
    }else if(strpos($value, 'September') !== false){
        $result = str_replace('September', '九月', $value);
        return $result;
    }else if(strpos($value, 'October') !== false){
        $result = str_replace('October', '十月', $value);
        return $result;
    }else if(strpos($value, 'November') !== false){
        $result = str_replace('November', '十一月', $value);
        return $result;
    }else if(strpos($value, 'December') !== false){
        $result = str_replace('December', '十二月', $value);
        return $result;
    }
}

function convertCNDatePicker($value){
    //1900年12月10日 ($value)
    //1900-12-10($result)

    $result = str_replace('年', '-', $value);
    $result = str_replace('月', '-', $result);
    $result = str_replace('日', '', $result);
    return $result;
}

function buildLinkHtml($arr) {
    $url = isset($arr['url']) ? $arr['url'] : '#';
    $description = isset($arr['description']) ? $arr['description'] : 'button';
    $color = isset($arr['color']) ? $arr['color'] : 'blue';

    return sprintf('<a href="%s" class="btn btn-xs primary uppercase %s">%s</a>', $url, $color, $description);
}


function buildLinkHtmlForSubmit($arr) {
    $description = isset($arr['description']) ? $arr['description'] : 'button';
    $color = isset($arr['color']) ? $arr['color'] : 'blue';
    return sprintf('<div class="btn btn-xs primary uppercase %s">%s</div>',$color, $description);
}

function buildLinkSimpleHtml($arr) {
    $url = isset($arr['url']) ? $arr['url'] : '#';
    $description = isset($arr['description']) ? $arr['description'] : 'button';
    $color = isset($arr['color']) ? $arr['color'] : 'green';

    return sprintf('<a href="%s" class="uppercase" style="color:%s">%s</a>', $url, $color, $description);
}

function buildNormalLinkHtml($arr) {
    $url = isset($arr['url']) ? $arr['url'] : '#';
    $description = isset($arr['description']) ? $arr['description'] : 'button';
    $color = isset($arr['color']) ? $arr['color'] : 'blue';

    return sprintf('<a href="%s" class="btn btn-xs primary uppercase %s">%s</a>', $url, $color, $description);
}

function buildText($arr) {
    $description = isset($arr['description']) ? $arr['description'] : 'button';
    return sprintf('<div style="color: #cccccc;font-size:17pt;font-weight: 300;height: 30px;display:inline-block;vertical-align: middle">%s</div>', $description);
}

function truncateSentence($value, $maxChar, $symbol = "...")
{
    if (strlen($value) > $maxChar) {
        $result = substr( $value , 0, $maxChar ) . $symbol;
    } else {
        $result = $value;
    }

    return $result;
}

function getFileType($filePath)
{
    $result = "";

    if (in_array( substr(strrchr($filePath,'.'),1), ['jpg', 'jpeg'])) {
        $result = "jpeg";
    } else if (in_array( substr(strrchr($filePath,'.'),1), ['gif'])) {
        $result = "gif";
    } else if (in_array( substr(strrchr($filePath,'.'),1), ['png'])) {
        $result = "png";
    } else if (in_array( substr(strrchr($filePath,'.'),1), ['bmp'])) {
        $result = "bmp";
    }

    return $result;
}

function getDisplayPriorityPoint($user)
{
    try {
        $totalPoint = 0;
        $pointWhiteTagSkill = 10;
        $pointGreenTagSkill = 50;
        $pointGoldTagSkill = 100;
        $pointUserVerified = 5;
        $pointWechat = 1;
        $pointSkype = 1;
        $pointPhoneNumber = 1;
        $pointQQ = 1;
        $pointAge = 1;
        $pointSalary = 1;
        $pointStatus = 1;
        $pointLanguage = 1;
        $pointProjectDone = 1;
        $pointPortfolioUpload = 1;
        $pointLastLogin30Days = 5;
        $pointLastLogin31To60Days = 2;
        $pointLastLogin61To90Days = 1;
        $pointNoImage = -5;
        $pointLessInfo = -5;
        $pointNoHourlyRate = -5;
        $pointNoCountry = -5;
        $pointNoTransaction = -5;

        //Each Gold Skill Tag - 100
        //Each Green Skill Tag - 50
        //Each White Skill Tag - 10

        //white skill tag
        $count = UserSkill::where('user_id', $user->id)
            ->where('experience', 0)
            ->where('is_client_verified', 0)
            ->count();
        if ($count >= 1)
            $totalPoint = $totalPoint + ($count * $pointWhiteTagSkill);

        //green skill tag
        $count = UserSkill::where('user_id', $user->id)
            ->where('is_client_verified', 1)
            ->count();
        if ($count >= 1)
            $totalPoint = $totalPoint + ($count * $pointGreenTagSkill);

        //gold skill tag
        $count = UserSkill::where('user_id', $user->id)
            ->where('experience', '>=', 1)
            ->count();
        if ($count >= 1)
            $totalPoint = $totalPoint + ($count * $pointGoldTagSkill);

        //User Verified point 5
        if ($user->getLatestIdentityStatus() == UserIdentity::STATUS_APPROVED)
            $totalPoint += $pointUserVerified;

        //Wechat point 1
        if (!($user->wechat_id == null || $user->wechat_id == ""))
            $totalPoint += $pointWechat;

        //Skype point 1
        if (!($user->skype_id == null || $user->skype_id == ""))
            $totalPoint += $pointSkype;

        //Handphone no point 1
        if (!($user->handphone_no == null || $user->handphone_no == ""))
            $totalPoint += $pointPhoneNumber;

        //QQ point 1
        if (!($user->qq_id == null || $user->qq_id == ""))
            $totalPoint += $pointQQ;

        //Age point 1
        if (!($user->age > 0))
            $totalPoint += $pointAge;

        //Salary point 1
        if (!($user->hourly_pay == null || $user->hourly_pay == 0.00))
            $totalPoint += $pointSalary;

        //Status point 1
        try {
            if (!is_null($user) && !($user->status == null || $user->status == 0))
                $totalPoint += $pointStatus;
        } catch (\Exception $e) {
        }

        //language point 1
        if (!($user->language_preference == null || $user->language_preference == 0))
            $totalPoint += $pointLanguage;

        //Each Project done point 1
        $count = ProjectApplicant::where('user_id', $user->id)
            ->where('status', ProjectApplicant::DISPLAY_ORDER_COMPLETED)
            ->count();
        if ($count >= 1)
            $totalPoint = $totalPoint + ($count * $pointProjectDone);

        //Each portfolio uploaded point 1
        $count = Portfolio::where('user_id', $user->id)->count();
        if ($count >= 1)
            $totalPoint = $totalPoint + ($count * $pointPortfolioUpload);

        //Last login date - (0-30)5, (31-60)2, (61-90)1
        $now = Carbon::now();
        $lastLogin = new Carbon($user->last_login_at);
        $differentDays = $now->diffInDays($lastLogin);

        if ($differentDays > 0 || $differentDays <= 30)
            $totalPoint += $pointLastLogin30Days;
        $totalPoint += $pointLastLogin31To60Days;
        $totalPoint += $pointLastLogin61To90Days;

        // pointNoImage
        if (substr($user->user_image, -17) == "/images/image.png")
            $totalPoint += $pointNoImage;

        // pointLessInfo
        if (strlen($user->about_me) < 75)
            $totalPoint += $pointLessInfo;

        // pointNoHourlyRate
        if (!$user->hourly_pay)
            $totalPoint += $pointNoHourlyRate;

        // pointNoCountry
        if (!$user->country()->count())
            $totalPoint += $pointNoCountry;

        // pointNoTransaction
        if (!$user->transactions->count())
            $totalPoint += $pointNoTransaction;

        return $totalPoint;
    } catch (\Exception $e){

    }
}

function getCurrencyByCountryCode($countryCode){
    return \App\Models\WorldCountries::where('code', '=', strtolower($countryCode))->first();
}

function getCurrency($from, $to)
{
    // scrap using transferwise
//    $from = strtolower($currencyName);

    $client = new \Goutte\Client();
    $crawler = $client->request('GET', 'https://transferwise.com/gb/currency-converter/' . $from . '-to-'.$to.'-rate');

    $wrapper = $crawler->filter('.cc__source-to-target')->each(function ($innerNode) {
        return $innerNode->filter('.text-success')->text();
    });

    return (double)current($wrapper);
}

function userLocation($ip){
//    https://ipapi.co/jsonp/?callback=?
//    https://www.iplocate.io/api/lookup/
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://www.iplocate.io/api/lookup/".$ip,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => "",
        CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded",
            "x-rapidapi-host: faceplusplus-faceplusplus.p.rapidapi.com",
            "x-rapidapi-key: 138cb4582cmsh22edf357340ddcbp18dbbbjsne7d149c34fa5"
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        return collect([
            'status' => false,
            'response' => $err
        ]);
    } else {
        $location = str_replace('(','',$response);
        $location = str_replace(')','',$location);
        $location = str_replace(';','',$location);
        $location = json_decode($location);
        return collect([
            'status' => true,
            'response' => $location
        ]);
    }
}


function cleanLink($link){
    return str_replace(' ','-',$link);
}

function cleanName($link){
    return str_replace('-',' ',$link);
}


function getAdminSetting($setting){
    $adminSetting = Settings::where('key','=',$setting)->first();
    return $adminSetting;
}

function generateRandomString($length = 7)
{
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function twiloSendSMS($phone_number,$randomSixDigitCode) {
    $account_sid = env('TWILIO_SID');
    $auth_token =  env('TWILIO_TOKEN');
    // A Twilio number you own with SMS capabilities

    $client = new Client($account_sid, $auth_token);
    $message = 'Here is a six digit code '. $randomSixDigitCode .' please enter code';
    $response = $client->messages->create(
    // Where to send a text message (your cell phone?)
        $phone_number,
        array(
            'from' => env('TWILIO_FROM'),
            'body' => $message
        )
    );

    return $response;
}

function twilloSendSMSToOwner($phone_number,$username,$seatNo,$officeName,$seatName,$userPhoneNumber) {
    $account_sid = env('TWILIO_SID');
    $auth_token =  env('TWILIO_TOKEN');
    // A Twilio number you own with SMS capabilities

    $client = new Client($account_sid, $auth_token);
    $message = $username.' '.'send you a booking request with seat name'.' '.$seatName.' '.'against office name'.' '.$officeName.' '.'Phone number : '.$userPhoneNumber;
    $phone_number = '+'.$phone_number;
    $response = $client->messages->create(
    // Where to send a text message (your cell phone?)
        $phone_number,
        array(
            'from' => env('TWILIO_FROM'),
            'body' => $message
        )
    );

    return $response;
}


function getLocale() {
    return \Session::get('lang');
}


function trackUser(){
//    $token = env('MIXPANEL_PROJECT_TOKEN');
//    $mp = Mixpanel::getInstance($token, array("debug" => true));
//    $mp->register('a','Xenren');
//    $mp->register('button','Xenren');
//    // track an event with a property "Ad Source" = "Google"
//    $mp->track("button clicked");
//    // register the Ad Source super property
//    $mp->track("test_event", array("color" => "#5Ec329"));
//    $mp->track("anchor", array("a" => "blue"));
//    $mp->track("Login", array("button" => "Login"));
//    // create or update a profile with First Name, Last Name,
//    // E-Mail Address, Phone Number, and Favorite Color
//    // without updating geolocation data or $last_seen
//    $user = \Auth::guard('users')->user();
//    setUser($user,$mp);
}

function setUser($user,$mp){
    if(!$user) {
        $mp->people->set(12345, array(
            '$first_name' => "John",
            '$last_name' => "Doe",
            '$email' => "dummyUser@example.com",
            '$phone' => "5555555555",
            "Favorite Color" => "red"
        ), $ip = 0, $ignore_time = true);
    } else {
        $mp->people->set($user->id, array(
            '$first_name' => $user->name,
            '$last_name' => $user->name,
            '$email' => $user->email,
            '$phone' => isset($user->phone_number) ? $user->phone_number : $user->handphone_no,
            "Favorite Color" => "red"
        ), $ip = 0, $ignore_time = true);
        $mp->people->increment($user->id, "login count", 1);
    }
}

function userAverageResponseTime(){
    $user_id = \Auth::user()->id;
    $userResponseTime = \App\Models\UserInboxMessages::where('from_user_id','=',\Auth::user()->id)
        ->avg('response_time');
    $time = covertMinutesToHours(round($userResponseTime));
    return $time;
}

function covertMinutesToHours($time , $format = '%02d:%02d'){
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}

function getDays($val, $weeks){
    if($val == 1) {
        $days = 7 * $weeks;
        Settings::where('key', '=', Settings::SET_RELEASE_TIME)->update([
            'value' => $days
        ]);
        return $days;
    } else if($val == 2) {
        $days = 7 * $weeks;
        Settings::where('key', '=', Settings::SET_RELEASE_TIME_UNVERIFIED)->update([
            'value' => $days
        ]);
        return $days;
    }
}

function getReleaseTime($val){
    if($val == 1) {
        $week1 = Settings::where('key', '=', Settings::SET_RELEASE_TIME)->first();
        return $week1->value / 7;
    } else if($val == 2) {
        $week2 = Settings::where('key', '=', Settings::SET_RELEASE_TIME_UNVERIFIED)->first();
        return $week2->value / 7;
    }
}

function getReleaseDays($val){
    if($val == 1) {
        $week1 = Settings::where('key', '=', Settings::SET_RELEASE_TIME)->first();
        return $week1->value;
    } else if($val == 2) {
        $week2 = Settings::where('key', '=', Settings::SET_RELEASE_TIME_UNVERIFIED)->first();
        return $week2->value;
    }
}
