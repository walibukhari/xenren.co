<?php

namespace App\Support;

use Carbon\Carbon;
use Exception;
use Image;

trait UploadFileHelper {
	
    /**
    * upload file
    * @param mixed $path
    * @param mixed $fileName
    * @param Request $request
    */
	public function uploadFile($path, $fileName, $request)
	{
		if (!$path)
			return false;

		// dynamic make path if not exist
		if (!file_exists($path)) {
			$paths = strpos($path, '/') !== false ? explode('/', $path) : [];
			$fullPath = public_path();
			foreach ($paths as $splitPath) {
				$currentPath = $fullPath.'/'.$splitPath;
				if (!file_exists($currentPath)) {
					mkdir($currentPath);
				}
				$fullPath .= '/'.$splitPath;
			}
		}
		
		try {
			$dt = Carbon::parse(Carbon::Now());
			$timeStamp = $dt->timestamp;
			$destinationPath = public_path() . '/' .$path;
			$file = $request->file($fileName);
			$extension = $request->file($fileName)->getClientOriginalExtension();
			$originalName = $request->file($fileName)->getClientOriginalName();
			$fileOriginalName = $request->file($fileName)->getClientOriginalName();
			$fileOriginalName = str_replace('.' . $extension, "", $fileOriginalName);
			$fileOriginalName = str_replace(' ', "_", $fileOriginalName);
			$fileOriginalName = preg_replace('/[^A-Za-z0-9\-]/', '', $fileOriginalName);
			$fileName = $timeStamp . '__' . rand() . '.' . $extension;
			$img = Image::make($file->getRealPath());
	        $img->resize(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$fileName);
			return collect([
				'status' => 'success',
				'icon' => $fileName,
				'fileName' => $originalName
			]);
		} catch (Exception $e) {
			\Log::info('------ Upload Error ------');
			\Log::info('error: '.$e->getMessage());
			\Log::info('------ Upload Error ------');
			return $e;
		}
	}

    /**
    * delete img if exist
    * @param Branch $branch
    */
    public function deleteImg($path, $fileName)
    {
        $directoryImg = public_path().'/'.$path;

        // remove img first if exist
        if (!is_null($fileName) && $fileName != '') {
            $image = explode('/', $fileName);
            $image = end($image);
            $image = $directoryImg.'/'.$image;
            if (file_exists($image)) {
                unlink($image);
            }
        }
    }
	
}