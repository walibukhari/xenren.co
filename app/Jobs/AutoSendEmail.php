<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class AutoSendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $email;
    protected $name;

    /**
     * Create a new job instance.
     *
     * @param $user_id
     * @param $email
     * @param $name
     */

    public function __construct($user_id,$email,$name)
    {
        $this->userId = $user_id;
        $this->email = $email;
        $this->name = $name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        \Log::info('send auto email to user');
        $id = $this->userId;
        $email = $this->email;
        $name = $this->name;
        \Log::info($id);
        //send email
        $senderEmail = "support@xenren.co";
        $senderName = 'xenren.co';
        $receiverEmail = $email;

        \Log::info('receiver email');
        \Log::info($receiverEmail);
        Mail::send('mails.auto-replay', array('email' => $email,'name' => $name), function ($message) use ($senderEmail, $senderName, $receiverEmail) {
                $message->from($senderEmail, $senderName);
                $message->to($receiverEmail)
                    ->subject('Auto-reply from XenRen');
            }
        );

        \Log::info('auto replay email send successfully');
    }
}
