<?php

namespace App\Jobs;

use App\Models\SharedOffice;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use File;

class SharedOfficeSearchJson implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	protected $office;
	protected $fileName;
	protected $lang;

    /**
     * Create a new job instance.
     *
     * @param $office
     * @param string $lang
     */
	public function __construct($office, $lang='en')
	{
		$this->office = $office;
		$this->fileName = public_path() . "/SharedOfficesJson/sharedoffices_".$lang.".json";
		$this->lang = $lang;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$exist = $this->checkFileExist($this->lang);
		if($exist) {
			$json = collect(json_decode(file_get_contents($this->fileName), true));
			$officeName = self::clean($this->office->office_name);
			$json->push(collect([
				'name' => $this->office->office_name,
				'actual_name' => $this->office->office_name,
				'actual_location' => $this->office->city_name.' '.$this->office->country_name,
				'office_location' => $this->office->location,
				'contact_address' => $this->office->contact_address,
				'id' => $this->office->id,
                'route' => 'https://coworkspace.xenren.co/'.$this->lang.'/coworking-spaces/'.$this->office->country_name.'/'.$this->office->city_name.'/'.$officeName.'/'.$this->office->id,
			]));

			$json = $json->values()->unique('name');

			self::writeFile($json);
		} else {
			$data = collect();
			$officeName = self::clean($this->office->office_name);
			$data->push(collect([
				'name' => $this->office->office_name,
				'id' => $this->office->id,
				'actual_name' => $this->office->office_name,
				'actual_location' => $this->office->city_name.' '.$this->office->country_name,
        		'office_location' => $this->office->location,
				'contact_address' => $this->office->contact_address,
				'route' => 'https://coworkspace.xenren.co/'.$this->lang.'/coworking-spaces/'.$this->office->country_name.'/'.$this->office->city_name.'/'.$officeName.'/'.$this->office->id,
			]));
			self::writeFile($data);
		}

	}

	function clean($string) {
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

		return strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $string)); // Removes special chars.
	}

	public function writeFile($data)
	{
		$path = $this->fileName;
        $dir = public_path("SharedOfficesJson");
        if (!File::isDirectory($dir)){
            File::makeDirectory($dir, 0777, true, true);
        }

        File::put($path,$data);
	}

	public function checkFileExist($lang)
	{
	    if(file_exists( public_path() . "/SharedOfficesJson/sharedoffices_".$lang.".json")) {
	        return file_exists( public_path() . "/SharedOfficesJson/sharedoffices_".$lang.".json");
        } else {
            // touch(public_path() . "/SharedOfficesJson/sharedoffices_" . $lang . ".json");
            return file_exists(public_path() . "/SharedOfficesJson/sharedoffices_" . $lang . ".json");
        }
	}
}
