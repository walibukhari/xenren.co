<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SharedOfficeContact implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $to = '';
    protected $cc = 'support@xenren.co';
    protected $request = '';

	/**
	 * Create a new job instance.
	 *
	 * @param $request
	 * @param bool $cc
	 * @internal param $to
	 */
    public function __construct($request, $cc=false)
    {
    	$to = $request['to'];
        $this->to = $to;
        if($cc) {
        	$this->cc = $cc;
        }
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
	    Mail::to($this->to)
		    ->cc($this->cc)
		    ->send(new \App\Mail\SharedOfficeContact($this->request));
    }
}
