<?php

namespace App\Jobs;

use App\Libraries\PayPal_PHP_SDK\PayPalTrait;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Auth;

class WithdrawAmount implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, PayPalTrait;

    protected $userId;
    protected $user;
    protected $tD;
    protected $requestObject;
    protected $email;

    /**
     * Create a new job instance.
     *
     * @param $userId
     * @param $requestObject
     * @param $email
     * @internal param $amount
     */
    public function __construct($userId, $requestObject, $email)
    {
        $this->userId = $userId;
        $this->user = User::where('id', '=', $userId)->first();
        $this->email = $email;
        $this->requestObject =  $requestObject;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $a = (object)$this->requestObject;
//            $tD = $this->withdraw($a);
//            \Log::info($tD);
//            if($tD['status'] == 'error') {
//                dd($tD['message']);
//            }
            //1- paypal withdraw
            //2- update transaction with paypal response
            //3- transaction detail entry
            $amount = $a->amount;
            $account_balance = $this->user->account_balance;
            $userId = $this->user->id;
            $currency_name = User::getCurrencyName($this->user->country_id);
            \DB::beginTransaction();
            $newBalance = $account_balance - $amount;
            $transaction = Transaction::create([
                'name' => 'Amount Withdraw',
                'amount' => $amount,
                'milestone_id' => 0,
                'due_date' => Carbon::now()->addDay(10),
                'comment' => 'Amount Withdrawal of ' . $amount,
                'balance_before' => $account_balance,
                'balance_after' => $newBalance,
                'performed_by_balance_before' => $account_balance,
                'performed_by_balance_after' => $newBalance,
                'performed_to_balance_before' => $account_balance,
                'performed_to_balance_after' => $newBalance,
                'performed_by' => $userId,
                'performed_to' => $userId,
                'type' => Transaction::TYPE_WITHDRAW,
                'currency' => $currency_name,
                'release_at' => Carbon::now(),
                'status' => Transaction::STATUS_IN_PROGRESS_TRANSACTION
            ]);
//            $td = TransactionDetail::create([
//                'transaction_id' => $transaction->id,
//                'payment_method' => TransactionDetail::PAYMENT_METHOD_PAYPAL,
//                'raw' => json_encode($tD),
//            ]);
            User::where([
                'id' => $userId
            ])->update([
                'account_balance' => $newBalance
            ]);
//            $job = (new CheckPayPalWithdrawStatus(
//                $this->requestObject, $tD, $userId, $transaction->id)
//            )->delay(60 * 10)->onQueue(config('jobs-queue-names.PayPalWithdrawStatus')); //delayed for 10 mins
//            dispatch($job);
            \DB::commit();
        } catch (\Exception $e) {
            \Log::info($a);
            \Log::info($e->getMessage());
            \Log::info($a->email);
        }
    }
}
