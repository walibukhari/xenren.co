<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\User;
use App\Services\GeneralService;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Intervention\Image\Facades\Image;
use Softon\LaravelFaceDetect\Facades\FaceDetect;
use File;

class ValidateFaceDetect extends Job implements ShouldQueue
{
	use InteractsWithQueue, SerializesModels;
	
	protected $imagePath;
	protected $userId;
	
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($imagePath, $userId)
	{
		\Log::info('dispatcher for face logging called');
		$this->imagePath = $imagePath;
		$this->userId = $userId;
	}
	
	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		try {
			$imagePath = base_path().'/public/'.$this->imagePath;
			\Log::info($imagePath);
			$p_path = public_path().'/temp';
			if (!file_exists($p_path)) {
				mkdir($p_path, 666, true);
			}
			$image_path = public_path().'/temp/'.date('Y-m-d-H:i:s')."-".'as.jpg';
			Image::make($imagePath)->encode('jpg')->save(($image_path));
			$crop_params = FaceDetect::extract(($image_path))->face;
			\Log::info('validating face...!');
			\Log::info($crop_params);
			if (count($crop_params) < 1) {
				\Log::info('not validated...!');
				User::where('id', '=', $this->userId)->update([
					'face_image' => 0
				]);
				if(File::exists($image_path)) {
					File::delete($image_path);
				}
			} else {
				\Log::info('validating face...!');
				User::where('id', '=', $this->userId)->update([
					'face_image' => 1
				]);
				if(File::exists($image_path)) {
					File::delete($image_path);
				}
			}
		} catch (\Exception $e) {
			User::where('id', '=', $this->userId)->update([
				'face_image' => 0
			]);
			if(File::exists($image_path)) {
				File::delete($image_path);
			}
			\Log::info('no no ');
			\Log::info($e);
		}
	}
}
