<?php

namespace App\Jobs;

use App\Constants;
use App\Logic\UploadFile\UploadFileRepository;
use App\Models\ExchangeRate;
use App\Models\PortfolioFile;
use App\Models\Project;
use App\Models\ProjectChatFollower;
use App\Models\ProjectChatRoom;
use App\Models\ProjectCountry;
use App\Models\ProjectFile;
use App\Models\ProjectLanguage;
use App\Models\ProjectQuestion;
use App\Models\ProjectSkill;
use App\Models\UploadFile;
use App\Models\User;
use App\Models\UserInbox;
use App\Models\UserInboxMessages;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\File;

class CreateDraftProject implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $request;
    protected $userId;
    protected $mode;
    protected $uploadFile;
    protected $status;

    /**
     * Create a new job instance.
     *
     * @param $request
     * @param $userId
     * @param $mode
     */
    public function __construct($request, $userId, $mode)
    {
        $this->request = $request;
        $this->userId = $userId;
        $this->mode = $mode;
        $this->uploadFile = new UploadFileRepository();
    }

    public function copy( $filename, $type, $id )
    {
        try {
            if( $type == UploadFile::TYPE_PORTFOLIO ) {
                $portfolioFile = new PortfolioFile();
                $portfolioFile->portfolio_id = $id;
                $portfolioFile->file = $filename;
                $portfolioFile->save();
            } else if( $type == UploadFile::TYPE_POST_PROJECT ) {
                $projectFile = new ProjectFile();
                $projectFile->project_id = $id;
                $projectFile->file = $filename;
                $projectFile->save();
            }
            return true;
        } catch(\Exception $e) {
            \Log::info('********ExceptionInPostProjectQueue*********');
            \Log::info($e->getMessage());
            \Log::info('********ExceptionInPostProjectQueue*********');
        }

    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::info('GOING TO POST PROJECT');
        $request = (object)$this->request;
        $user = User::where('id', '=', $this->userId)->first();
        $mode = $this->mode;
        $project_id = $request->pre_project_id;
        $project = Project::where('user_id', '=', $user->id)->where('status', '=', Project::STATUS_DRAFT)->select(['id'])->first();
        if(is_null($project)) {
            $project = new Project();
        }

        try {
            $project->type = Project::PROJECT_TYPE_COMMON;
            $project->user_id = $user->id;
            $project->name = $request->name;
            $project->category_id = $request->category_id;
            $project->description = $request->description;
            $project->pay_type = $request->pay_type;
            $project->time_commitment = $request->time_commitment;
            $project->freelance_type = $request->freelance_type;
            $project->nearby_place = isset($request->nearby_place) ? $request->nearby_place  : 0;
            $project->status = Project::STATUS_DRAFT;

            //always convert the price to usd
            if (app()->getLocale() == "en") {
                $project->reference_price = $request->reference_price;
            } elseif (app()->getLocale() == "cn") {
                $price = $request->reference_price;
                $exchangeRate = ExchangeRate::getExchangeRate(Constants::CNY, Constants::USD);
                $value = $price * $exchangeRate;
                $project->reference_price = $value;
            }

            $project->is_read = Project::IS_READ_NO;
            $project->save();

            $delete_files_paths = array();
            if ($mode == "update") {
                $delete_file = $request->delete_file;
                if ($delete_file && count($delete_file) > 0) {
                    foreach ($delete_file as $key => $var) {
                        $projectFile = ProjectFile::find($var);
                        if ($projectFile) {
                            $delete_images_paths[] = $projectFile->file;
                            $projectFile->delete();
                        }
                    }
                }
            }

            $hidUploadedFiles = isset($request->hidUploadedFile) ? $request->hidUploadedFile : null;
//            \Log::info('$hidUploadedFiles in queue');
//            \Log::info($hidUploadedFiles);
            if (!is_null($hidUploadedFiles)) {
                foreach ($hidUploadedFiles as $file) {
                    //copy from file uploaded location to portfolio pic location
                    //add record to DB
//                    \Log::info('FILE: ');
//                    \Log::info($file);
                    $this->copy($file, UploadFile::TYPE_POST_PROJECT, $project->id);

                }
            }

            if ($mode == "update") {
                ProjectSkill::where('project_id', $project->id)->delete();
                ProjectCountry::where('project_id', $project->id)->delete();
                ProjectLanguage::where('project_id', $project->id)->delete();
            }

            $skillIDList = explode(",", $request->skill_id_list);
            $i_skills = array();
            foreach ($skillIDList as $skillID) {
                $i_skills[] = [
                    'project_id' => $project->id,
                    'skill_id' => trim($skillID),
                    'created_at' => date('Y-m-d H:i:s')
                ];
            }
//            \Log::info('$i_skills');
//            \Log::info('$i_skills');
//            \Log::info($i_skills);
            if (!empty($i_skills)) {
                ProjectSkill::insert($i_skills);
            }
            $countryIDList = explode(",", $request->country_id_list);
            $i_countries = array();
            foreach ($countryIDList as $countryID) {
                if ($countryID == "") {
                    continue;
                }
                $i_countries[] = [
                    'project_id' => $project->id,
                    'country_id' => trim($countryID),
                    'created_at' => date('Y-m-d H:i:s')
                ];
            }
            if (!empty($i_countries)) {
                ProjectCountry::insert($i_countries);
            }
            $languageIDList = explode(",", $request->language_id_list);
            $i_languages = array();
            foreach ($languageIDList as $languageID) {
                if ($languageID == "") {
                    continue;
                }
                $i_languages[] = [
                    'project_id' => $project->id,
                    'language_id' => trim($languageID),
                    'created_at' => date('Y-m-d H:i:s')
                ];
            }
            if (!empty($i_languages)) {
                ProjectLanguage::insert($i_languages);
            }
            if ($project->id >= 1) {
                //clear all question before save a new set

                $questions = isset($request->question) ? $request->question : null;
                if (!is_null($questions)) {
                    ProjectQuestion::GetProjectQuestion()->where('project_id', $project->id)->delete();
                    $i_questions = array();
                    foreach ($questions as $key => $question) {
                        if ($question != '') {
                            $i_questions[] = [
                                'project_id' => $project->id,
                                'question' => trim($question),
                                'created_at' => date('Y-m-d H:i:s')
                            ];
                        }
                    }
                    if (!empty($i_questions)) {
                        ProjectQuestion::insert($i_questions);
                    }
                }

                //Create Project Chat Room
                $projectChatRoom = ProjectChatRoom::GetProjectChatRoom()
                    ->where('project_id', $project->id)
                    ->first();
                if ($projectChatRoom == null) {
                    $projectChatRoom = new ProjectChatRoom();
                    $projectChatRoom->project_id = $project->id;
                    $projectChatRoom->save();
                }

                //Project Owner become first chat room follower
                $projectChatFollower = ProjectChatFollower::GetProjectChatFollower()
                    ->where('user_id', $user->id)
                    ->first();

                if ($projectChatFollower == null) {
                    $projectChatFollower = new ProjectChatFollower();
                    $projectChatFollower->project_chat_room_id = $projectChatRoom->id;
                    $projectChatFollower->user_id = $user->id;
                    $projectChatFollower->is_kick = 0;
                    $projectChatFollower->save();
                }

                if ($mode == 'create') {
                    //add user inbox notification
                    $this->userInbox($project, $user->id);
                    $this->sendNewProjectNotification($project, $user->id);
                } else {
                    if ($delete_files_paths && count($delete_files_paths) > 0) {
                        foreach ($delete_files_paths as $key => $var) {
                            @unlink(public_path($var));
                        }
                    }
                }
            }
            $job = (new \App\Jobs\PostProjectSkillNotification($i_skills))->onQueue(config('jobs-queue-names.PostProjectSkillNotification'));
            dispatch($job);
        } catch (\Exception $e) {
            \Log::info('********ExceptionInPostProjectQueue*********');
            \Log::info($e->getMessage(). 'at '.$e->getLine());
            \Log::info('********ExceptionInPostProjectQueue*********');
        }
    }

    public function userInbox($project, $userId)
    {
        $userInbox =  new UserInbox();
        $userInbox->category = UserInbox::CATEGORY_SYSTEM;
        $userInbox->project_id =  $project->id;
        $userInbox->from_user_id = null;
        $userInbox->to_user_id = $userId;
        $userInbox->from_staff_id = 1;
        $userInbox->to_staff_id = null;
        $userInbox->type = UserInbox::TYPE_PROJECT_SUCCESSFUL_POST_JOB;
        $userInbox->is_trash = 0;
        $userInbox->save();

        //add user inbox message
        $userInboxMessage = new UserInboxMessages();
        $userInboxMessage->inbox_id = $userInbox->id;
        $userInboxMessage->from_user_id = null;
        $userInboxMessage->to_user_id = $userId;
        $userInboxMessage->from_staff_id = 1;
        $userInboxMessage->to_staff_id = null;
        $userInboxMessage->project_id = $project->id;
        $userInboxMessage->is_read = 0;
        $userInboxMessage->type = UserInboxMessages::TYPE_PROJECT_SUCCESSFUL_POST_JOB;

        $message = trans('common.you_success_post_project') . "<br/>" .
            trans('common.project_name') . " : <a href='" . route('frontend.joindiscussion.getproject', ['slug' => $project->name,'id' => $project->id ] ) . "'>" . $project->name . "</a>";
        $userInboxMessage->custom_message = $message;
        $userInboxMessage->save();
    }

    public function sendNewProjectNotification(Project $project, $userId)
    {
        //get the user who have following skill
        $projectId = $project->id;
        $projectName = $project->name;

        $projectSkills = ProjectSkill::where('project_id', $projectId)
            ->get();
        $skillIds = array();
        foreach( $projectSkills as $projectSkill)
        {
            array_push($skillIds, $projectSkill->skill_id);
        }

        //select user who match the project skill
        $users = User::with('skills')->where('id', '!=', $userId);
        $users = $users->whereHas('skills', function ($query) use ($skillIds) {
            $query
                ->whereIn('skill_id', $skillIds)
            ;
        })
            ->where('email_notification', 1) //allow send email notification
            ->get();

        $this->sendEmail($users, $projectId, $projectName);
    }

    public function sendEmail($users, $projectId, $projectName)
    {
        $senderEmail = "support@xenren.co";
        $senderName = "Support Team";

        $emails = array();
        foreach($users as $user)
        {
            if( $user->email != "" || $user->email != null )
            {
                array_push($emails, $user->email);
            }

        }

        $projectLink = route('frontend.joindiscussion.getproject', ['slug' => $projectName , 'id' => $projectId ]);

        Mail::send('mails.skillMatchEmailNotification', array(
            'projectLink' => $projectLink,
            'projectName' => $projectName
        ), function ($message) use ($senderEmail, $senderName, $emails) {
            $message->from($senderEmail, $senderName);
            $message->to($emails)
                ->subject(trans('common.new_project_notification'));
        });
    }
}
