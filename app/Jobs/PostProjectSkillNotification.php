<?php

namespace App\Jobs;

use App\Models\User;
use App\Models\SkillNotification;
use App\Models\UserInbox;
use App\Models\UserInboxMessages;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\PushNotificationService;
use Illuminate\Support\Facades\DB;

class PostProjectSkillNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $skillList;
    protected $tenDays = 10;
    protected $thirtyDays = 30;
    protected $ninetyDays = 90;
    protected $message = 'New project has been posted match to your skills';

    /**
     * Create a new job instance.
     *
     * @param $skillList
     */
    public function __construct($skillList)
    {
        $this->skillList = $skillList;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $arr = collect([
            'body' => 'You received a new message',
            'title' => 'New Message - XENREN',
            'sharedOffice' => false,
            'data' => $this->message
        ]);

        try {
            $skills_id = $this->skillsFilter();

            // Ten Days off Users
            $user_tenDays = $this->getUsers($skills_id, $this->tenDays);
            $this->trackUserRecord($user_tenDays, $this->skillList);
            $count_tenDays = $user_tenDays->count();
            $this->excucateJob($count_tenDays, $user_tenDays, $arr);

            // Thirty Days off Users
            $user_thirtyDays = $this->getUsers($skills_id, $this->thirtyDays);
            $this->trackUserRecord($user_thirtyDays, $this->skillList);
            $count_thirtyDays = $user_thirtyDays->count();
            $this->excucateJob($count_thirtyDays, $user_thirtyDays, $arr);

            // Ninety Days off Users
            $user_ninetyDays = $this->getUsers($skills_id, $this->ninetyDays);
            $this->trackUserRecord($user_ninetyDays, $this->skillList);
            $count_ninetyDays = $user_ninetyDays->count();
            $this->excucateJob($count_ninetyDays, $user_ninetyDays, $arr);

        } catch (\Exception $e) {
            \Log::info('********ExceptionInProjectPost*********');
            \Log::info($e->getMessage(). 'at '. $e->getLine());
            \Log::info('********ExceptionInProjectPost*********');
        }
    }

    public function excucateJob($count, $user, $message)
    {
        if ($count > 1) {
            foreach ($user as $k => $v) {
                if ($v->device_token != null) {
                    if ($v->free_notifications == 0) {
                        return false;
                    } else {
                        $this->insertIntoUserInbox($v);
                        $this->notificationDecrement($v);
                        $this->sendAppNotification($v, $message);
                    }
                }
            }
        }
    }

    public function notificationDecrement($user)
    {
        return User::where('id','=',$user->id)->decrement('free_notifications',1);
    }

    public function skillsFilter()
    {
        $skills_id = array();
        foreach ($this->skillList as $key => $val) {
            array_push($skills_id, $val['skill_id']);
        }
        return $skills_id;
    }

    public function getUsers($skills_id, $days)
    {
        $user = User::where('last_login_at', '>=', Carbon::now()->subDays($days)->toDateTimeString())
            ->whereHas('skills', function($q) use($skills_id) {
                $q->whereIn('skill_id', $skills_id);
            })
            ->inRandomOrder()
            ->limit(5)
            ->select('id', 'device_token', 'last_login_at','free_notifications','email')
            ->get();
        return $user;
    }

    public function insertIntoUserInbox($user)
    {
        $project_id = $this->skillList[0]['project_id'];
        DB::beginTransaction();
        $userInbox =  new UserInbox();
        $userInbox->category = UserInbox::CATEGORY_SYSTEM;
        $userInbox->project_id =  $project_id;
        $userInbox->from_user_id = null;
        $userInbox->to_user_id = $user->id;
        $userInbox->from_staff_id = 1;
        $userInbox->to_staff_id = null;
        $userInbox->type = UserInbox::TYPE_PROJECT_SKILL_NOTIFICATION;
        $userInbox->is_trash = 0;
        $userInbox->save();
        DB::commit();

        //add user inbox message
        DB::beginTransaction();
        $userInboxMessage = new UserInboxMessages();
        $userInboxMessage->inbox_id = $userInbox->id;
        $userInboxMessage->from_user_id = null;
        $userInboxMessage->to_user_id = $user->id;
        $userInboxMessage->from_staff_id = 1;
        $userInboxMessage->to_staff_id = null;
        $userInboxMessage->project_id = $project_id;
        $userInboxMessage->is_read = 0;
        $userInboxMessage->type = UserInboxMessages::TYPE_PROJECT_SKILL_NOTIFICATION;
        $userInboxMessage->custom_message = $this->message;
        $userInboxMessage->save();
        DB::commit();
    }

    public function trackUserRecord($user, $skillList)
    {
        $skills_id = $this->filterRecord($skillList);
        $project_id = $skillList[0]['project_id'];
        DB::beginTransaction();
        foreach ($user as $k => $v) {
            $skill_notification = new SkillNotification();
            $skill_notification->project_id = $project_id;
            $skill_notification->user_id = $v->id;
            $skill_notification->skills_id = $skills_id;
            $skill_notification->save();
        }
        DB::commit();
    }

    public function filterRecord($skillList)
    {
        $array = array();
        foreach ($skillList as $k => $v) {
            array_push($array, $v['skill_id']);
        }
        $skills = implode(',', $array);
        return $skills;
    }

    public function sendAppNotification($user, $message)
    {
        return PushNotificationService::sendPush($user->device_token, $message);
    }
}
