<?php

namespace App\Jobs;

use App\Models\CommunityDiscussion;
use App\Models\CommunitySubscribe;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Traits\sendPushNotification;

class SendPushyNotifications implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, sendPushNotification;

    protected $userId;
    protected $topicId;

    /**
     * Create a new job instance.
     *
     * @param $user_id
     * @param $topicId
     */

    public function __construct($user_id,$topicId)
    {
        $this->userId = $user_id;
        $this->topicId = $topicId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        \Log::info('send auto email to user');
        $topicId = $this->topicId;
        $checkUserSubscribe = CommunitySubscribe::where('subscribe', '=' , CommunityDiscussion::SUBSCRIBE_RSS_FEED)->get();
        \Log::info($topicId);
        //send push notification
        \Log::info('send push notifications to those how subscribe community topics');
        $message = 'Community Add New Topics';
        $body = 'see new community topic';
        foreach($checkUserSubscribe as $users) {
            $userId = $users->user_id;
            $this->sendNotifications($userId, $message, $body);

            //send email
            $userData = User::where('id', '=', $userId)->first();
            $senderEmail = "support@xenren.co";
            $senderName = 'xenren.co';
            $receiverEmail = $userData->email;

            \Log::info('send email');
            \Log::info($receiverEmail);
            Mail::send('mails.auto-replay', array('message' => 'Community Add New Topics'), function ($message) use ($senderEmail, $senderName, $receiverEmail) {
                $message->from($senderEmail, $senderName);
                $message->to($receiverEmail)
                    ->subject('XenRen Community');
            }
            );
        }

        \Log::info('successfully send push notifications to those how subscribe community topics');
    }
}
