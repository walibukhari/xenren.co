<?php

namespace App\Jobs;

use App\Models\BadWordsFilter;
use App\Models\Project;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Snipe\BanBuilder\CensorWords;

class CheckBadWords implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $userId;
    protected $text;
    protected $type;
    protected $referenceId;

    /**
     * Create a new job instance.
     *
     * @param $userId
     * @param $text
     * @param $type
     * @param $referenceId
     */
    public function __construct($userId, $text, $type, $referenceId)
    {
        $this->userId = $userId;
        $this->text = $text;
        $this->type = $type;
        $this->referenceId = $referenceId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $containsBadWords = self::containsBadWords($this->text);
        \Log::info('bad words checking');
        \Log::info($containsBadWords);
        \Log::info(count($containsBadWords['matched']));
        if(count($containsBadWords['matched']) >= 0) {
            \Log::info('matched');
            BadWordsFilter::create([
                'user_id' => $this->userId,
                'data' => collect([
                    'type' => $this->type,
                    'data' => $containsBadWords,
                    'reference_id' => $this->referenceId
                ])
            ]);
            \Log::info('checking tpe');
            \Log::info($this->type .' == ' . BadWordsFilter::TYPE_PROJECT);
            \Log::info($this->type .' == ' . BadWordsFilter::TYPE_PROFILE);
            if($this->type == BadWordsFilter::TYPE_PROJECT) {
                \Log::info('checking project');
                self::updateProject($this->referenceId);
            }
            if($this->type == BadWordsFilter::TYPE_PROFILE) {
                \Log::info('checking user');
                self::updateUser($this->referenceId);
            }
        }
    }

    public function updateProject($id)
    {
        $project= Project::where('id', '=', $id)->first();
        return Project::where('id', '=', $id)->update([
            "name" => self::containsBadWords($project->name)['clean'],
            "description"  => self::containsBadWords($project->description)['clean']
        ]);
    }

    public function updateUser($id)
    {
        $user= User::where('id', '=', $id)->first();
        return User::where('id', '=', $id)->update([
            "name" => self::containsBadWords($user->name)['clean'],
            "about_me" => self::containsBadWords($user->about_me)['clean'],
            "title" => self::containsBadWords($user->title)['clean']
        ]);
    }

    public function containsBadWords($text)
    {
        $censor = new CensorWords;
        //$badwords = $censor->setDictionary (public_path('/CustomBadWords/customDictionary.php'));
        $string = $censor->censorString($text);
        return $string;
    }
}
