<?php

namespace App\Jobs;

use App\Http\Controllers\PaymentGateways\PayPalPhpSdk\PayPalController;
use App\Jobs\Job;
use App\Libraries\PayPal_PHP_SDK\PayPalTrait;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\User;
use App\Models\UserPaymentMethod;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CheckPayPalWithdrawStatus extends Job implements ShouldQueue
{
	use InteractsWithQueue, SerializesModels, PayPalTrait;
	
	protected $request, $paypalController, $transactionData, $userId, $transactionId;

    /**
     * Create a new job instance.
     *
     * @param $request
     * @param $transactionData
     * @param $userId
     * @param $transactionId
     */
	public function __construct($request, $transactionData, $userId, $transactionId)
	{
//		$this->paypalController = new PayPalController();
		$this->request = $request;
		$this->transactionData = $transactionData;
		$this->userId = $userId;
		$this->transactionId = $transactionId;
	}
	
	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		try {
			$data = $this->getWithdrawDetail($this->transactionData['payout_batch_id']);
			\Log::info('*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-');
			\Log::info('Batch Id: ' . $this->transactionData['payout_batch_id']);
			\Log::info($data);
			$status = $data->getItems()[0]->getTransactionStatus();
			\Log::info($status);
			if ($status != 'SUCCESS') {
				\Log::info('in if');
				$user = User::where('id', '=', $this->userId)->first();
				User::where('id', '=', $this->userId)->update([
					'account_balance' => $user->account_balance + $data->getItems()[0]->getPayoutItem()->getAmount()->getValue()
				]);
				Transaction::where('id', '=', $this->transactionId)->update([
					'status' => Transaction::STATUS_IN_REJECTED_TRANSACTION
				]);
				UserPaymentMethod::where('user_id', $this->userId)->where('payment_method', '=', TransactionDetail::PAYMENT_METHOD_PAYPAL)->update([
					'status' => UserPaymentMethod::STATUS_INACTIVE
				]);
			} else {
				Transaction::where('id', '=', $this->transactionId)->update([
					'status' => Transaction::STATUS_APPROVED_TRANSACTION
				]);
			}
			\Log::info('*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-');
		} catch (\Exception $e) {
			\Log::info('*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* Exception in Withdrawl  *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-');
			\Log::info($e->getMessage());
			\Log::info($e->getCode());
			\Log::info($e->getFile());
			\Log::info('*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* Exception in Withdrawl  *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-');
		}
		
	}
}
