<?php

namespace App\Jobs;

use App\Libraries\PayPal_PHP_SDK\PayPalTrait;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Carbon;

class PayPalSuccessPayment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, PayPalTrait;
    protected $userId;
    protected $payPalResponse;

    /**
     * Create a new job instance.
     *
     * @param $userId
     * @param $payPalResponse
     * @internal param $paypalResponse
     * @internal param $request
     */
    public function __construct($userId, $payPalResponse)
    {
        $this->userId = $userId;
        $this->payPalResponse = json_decode($payPalResponse);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::where('id', '=', $this->userId)->first();
        $result = $this->payPalResponse;
        \Log::info($result);
        \DB::beginTransaction();
        $newBalance = $user->account_balance + $result['result']['transactions']['amount'];
        $tr = Transaction::create([
            'name' => 'Topup Amount',
            'amount' => $result->transactions->amount,
            'milestone_id' => 0,
            'due_date' => Carbon::now()->addYear(1),
            'comment' => $result->transactions->amount . ' TopUpped',
            'performed_by_balance_before' => $user->account_balance,
            'performed_by_balance_after' => $newBalance,
            'performed_to_balance_before' => $user->account_balance,
            'performed_to_balance_after' => $newBalance,
            'performed_by' => $user->id,
            'performed_to' => $user->id,
            'type' => Transaction::TYPE_TOP_UP,
            'currency' => 'USD',
            'status' => Transaction::STATUS_APPROVED_TRANSACTION,
        ]);
        TransactionDetail::create([
            'transaction_id' => $tr->id,
            'payment_method' => TransactionDetail::PAYMENT_METHOD_PAYPAL,
            'raw' => json_encode($result)
        ]);
        User::where('id', '=', $user->id)->update([
            'account_balance' => $newBalance
        ]);
        \DB::commit();
    }
}
