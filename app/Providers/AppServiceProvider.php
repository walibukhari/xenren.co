<?php

namespace App\Providers;

use App\Models\GuestRegister;
use App\Models\Project;
use App\Models\SharedOffice;
use App\Models\SharedOfficeBooking;
use App\Models\Staff;
use App\Models\User;
use App\Models\UserContactPrivacy;
use App\Models\UserInbox;
use App\Models\UserInboxMessages;
use App\Observers\ProjectObserver;
use App\Observers\RegisterCodeNotification;
use App\Observers\SharedOfficeBookingObserver;
use App\Observers\SharedOfficesObserver;
use App\Observers\UserContactPrivaciesObserver;
use App\Observers\UserInboxMessagesObserver;
use App\Observers\UserObserver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        header("Access-Control-Allow-Origin: *");
	    header("Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE");
	    header("Access-Control-Allow-Headers: Content-Type, X-Auth-Token, Origin");
	    Schema::defaultStringLength(191);

	    Validator::extend('isPermissionTag', function($attribute, $value, $parameters) {
		    return \App\Models\PermissionGroupPermission::isValidPermission($value);
	    });

	    Validator::extend('notEmail', function($attribute, $value, $parameters) {
		    return !filter_var($value, FILTER_VALIDATE_EMAIL);
	    });

	    Validator::extend('isPassword', function($attribute, $value, $parameters) {
		    return strlen($value) >= 6 ? true : false;
	    });

	    Validator::extend('isNumber', function($attribute, $value, $parameters) {
		    return $value != '' && $value >= 0 && ctype_digit((string)$value) ? true : false;
	    });

	    Validator::extend('isDwgFile', function($attribute, $value, $parameters) {
		    $ext = $value->guessClientExtension();
		    return $ext == 'dwg' ? true : false;
	    },'The :attribute must be DWG file');

	    Validator::extend('isFund', function($attribute, $value, $parameters) {
		    return $value == 0 || preg_match_all('/[\d](.[\d]{1,2})?/im' ,$value) ? true : false;
	    });

	    Validator::extend('isProjectLevel', function($attribute, $value, $parameters) {
		    return array_key_exists($value, \App\Constants::getProjectLevels());
	    });

	    Validator::extend('isProjectType', function($attribute, $value, $parameters) {
		    return array_key_exists($value, \App\Constants::getProjectTypes());
	    });

	    Validator::extend('isLang',function($attribute, $value, $parameters) {
		    return array_key_exists($value, \App\Constants::getLangs());
	    });

	    Validator::extend('isSkillLevel',function($attribute, $value, $parameters) {
		    return array_key_exists($value, \App\Constants::getSkillLevel());
	    });

	    Validator::extend('alpha_space', function($attribute, $value, $parameters){
		    return preg_match('/^[\w\-\s]+$/', $value);
	    });

	    Validator::extend('phone_number_match', function($attribute, $value, $parameters){
		    $china_phone_number = '86';
		    $first_and_second_phone_number = $value[0] . $value[1];
		    if($china_phone_number != $first_and_second_phone_number){
			    return false;
		    }
		    else{
			    return true;
		    }
	    });

        Validator::extend('phone_or_email', function($attribute, $value, $parameters){
            $isEmail = filter_var($value, FILTER_VALIDATE_EMAIL) ? true : false;
            $isPhone = preg_match("/^[0-9]{7,15}$/", $value); // numeric between 7 to 15 characters

            if ($isEmail || $isPhone) return true;
            else return false;
        });

	    view()->composer('frontend.layouts.default', function($view)
	    {
            trackUser();
//            app('session')->put('screeWidth', 1140);
		    $screenWidth = app('session')->get('screenWidth');
		    $bothSideMargin = '';
		    if(isset($screenWidth)){
			    if( $screenWidth > 1300 ){
				    $margin = ( $screenWidth - 1300) / 3;
				    $bothSideMargin = "margin-left: " . $margin . "px !important; margin-right: " . $margin . "px!important";
			    }
		    }

		    $view->with('bothSideMargin', $bothSideMargin);
		    $userStatus = User::getStatusLists();
		    $view->with('userStatus', $userStatus);

		    if (\Auth::guard('users')->check()) {
			    $user = \Auth::guard('users')->user();
			    $userInboxMessages = UserInboxMessages::with(['fromUser', 'toUser'])
				    ->where('to_user_id', $user->id)
				    // ->limit(4)
				    ->orderBy('id', 'desc');

			    $userInboxMessages = $userInboxMessages->get();
			    $pendingNotification = $userInboxMessages->where('is_read', 0)->count();
//dd($userInboxMessages);
			    $view->with('pendingNotification', $pendingNotification);
			    $view->with('userInboxMessages', $userInboxMessages);
		    }

		    /*Fetch the private chat events for the logged in user*/
		    if ((\Auth::user())) {
			    $ui = new UserInbox();
			    $inbox = $ui->getMyMessages();
			    $view->with('inboxEvents', $inbox);
		    }
	    });

	    view()->composer('frontend-xenren.layouts.default-new', function($view)
	    {
            trackUser();
		    $userStatus = User::getStatusLists();
		    $view->with('userStatus', $userStatus);

		    if (\Auth::guard('users')->check()) {
			    $user = \Auth::guard('users')->user();
			    $userInboxMessages = UserInboxMessages::with(['fromUser', 'toUser'])
				    ->where('to_user_id', $user->id)
				    ->orderBy('id', 'desc');
			    $userInboxMessages = $userInboxMessages->get();
			    $pendingNotification = $userInboxMessages->where('is_read', 0)->count();
			    $view->with('pendingNotification', $pendingNotification);
			    $view->with('userInboxMessages', $userInboxMessages);
		    }

		    /*Fetch the private chat events for the logged in user*/
		    if ((\Auth::user())) {
			    $ui = new UserInbox();
			    $inbox = $ui->getMyMessages();
			    $view->with('inboxEvents', $inbox);
		    }
	    });
        view()->composer('frontend.coWorks.layouts.default', function($view)
        {
            trackUser();
            $userStatus = User::getStatusLists();
            $view->with('userStatus', $userStatus);

            if (\Auth::guard('users')->check()) {
                $user = \Auth::guard('users')->user();
                $userInboxMessages = UserInboxMessages::with(['fromUser', 'toUser'])
                    ->where('to_user_id', $user->id)
                    ->orderBy('id', 'desc');
                $userInboxMessages = $userInboxMessages->get();
                $pendingNotification = $userInboxMessages->where('is_read', 0)->count();
                $view->with('pendingNotification', $pendingNotification);
                $view->with('userInboxMessages', $userInboxMessages);
            }

            /*Fetch the private chat events for the logged in user*/
            if ((\Auth::user())) {
                $ui = new UserInbox();
                $inbox = $ui->getMyMessages();
                $view->with('inboxEvents', $inbox);
            }
        });

        view()->composer('frontend.includes.100-coworkers', function($view)
        {
            $screenWidth = app('session')->get('screenWidth');
            $bothSideMargin = '';
            if(isset($screenWidth)){
                if( $screenWidth > 1300 ){
                    $margin = ( $screenWidth - 1300) / 3;
                    $bothSideMargin = "margin-left: " . $margin . "px !important; margin-right: " . $margin . "px!important";
                }
            }

            $view->with('bothSideMargin', $bothSideMargin);
        });

        view()->composer('backend.layout', function($view)
        {
            if(\Auth::guard('staff')->check()) {
                $staff = \Auth::guard('staff')->user();
                $check = \Auth::guard('staff')->user()->staff_type === Staff::STAFF_TYPE_STAFF;
                if ($check) {
                	// if owner staff
                    if (!is_null($staff->office_id)) {
                    	$view->with('ownerOfficeId', $staff->office_id);
                    }

                    // if owner
                    $getOffice = SharedOffice::where('staff_id', '=', $staff->id)->first();
                    if(!is_null($getOffice)) {
                        $view->with('ownerOfficeId', $getOffice->id);
                    }
                }
            }
        });

//        view()->share('bothSideMargin',$bothSideMargin);

	  UserInboxMessages::observe(UserInboxMessagesObserver::class);
      SharedOfficeBooking::observe(SharedOfficeBookingObserver::class);
      GuestRegister::observe(RegisterCodeNotification::class);
      UserContactPrivacy::observe(UserContactPrivaciesObserver::class);
      Project::observe(ProjectObserver::class);
      User::observe(UserObserver::class);
      SharedOffice::observe(SharedOfficesObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//	    if ($this->app->environment() !== 'production') {
//		    $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
//		    $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
//	    }
    }
}
