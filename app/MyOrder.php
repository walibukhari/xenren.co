<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyOrder extends Model
{
    //
    protected $table = "my_orders";

    protected $fillable = ['project_id','status'];




    public function project()
    {
        return $this->belongsTo('App\Models\Project');
    }



}
