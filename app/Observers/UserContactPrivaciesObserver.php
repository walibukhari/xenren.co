<?php

namespace App\Observers;

use App\Models\SharedOfficeBooking;
use App\Models\User;
use App\Models\UserContactPrivacy;
use App\Services\PushNotificationService;
use App\Traits\PredisTrait;

class UserContactPrivaciesObserver
{
    use PredisTrait;
	
	/**
	 * Handle the shared office booking "created" event.
	 *
	 * @param UserContactPrivacy $userContactPrivacy
	 * @return void
	 * @internal param SharedOfficeBooking $sharedOfficeBooking
	 */
    public function created(UserContactPrivacy $userContactPrivacy)
    {
        $userId = $userContactPrivacy->user_id;
	      $qq = $userContactPrivacy->qq;
			  $weChat = $userContactPrivacy->weChat;
			  $skype = $userContactPrivacy->skype;
			  $phone = $userContactPrivacy->phone;
        if($qq == 1 || $weChat == 1 || $skype == 1 || $phone == 1) {
        	User::where('id', '=', $userId)->update([
        		'is_contact_allow_view' => 1
	        ]);
        } else {
	        User::where('id', '=', $userContactPrivacy)->update([
		        'is_contact_allow_view' => 0
	        ]);
        }
    }
	
	/**
	 * Handle the shared office booking "updated" event.
	 *
	 * @param UserContactPrivacy $userContactPrivacy
	 * @return void
	 * @internal param SharedOfficeBooking $sharedOfficeBooking
	 */
    public function updated(UserContactPrivacy $userContactPrivacy)
    {
    	\Log::info('observer called');
	    $userId = $userContactPrivacy->user_id;
	    $qq = $userContactPrivacy->qq;
	    $weChat = $userContactPrivacy->weChat;
	    $skype = $userContactPrivacy->skype;
	    $phone = $userContactPrivacy->phone;
	    if($qq == 1 || $weChat == 1 || $skype == 1 || $phone == 1) {
		    User::where('id', '=', $userId)->update([
			    'is_contact_allow_view' => 1
		    ]);
	    } else {
		    User::where('id', '=', $userId)->update([
			    'is_contact_allow_view' => 0
		    ]);
	    }
    }
	
	/**
	 * Handle the shared office booking "deleted" event.
	 *
	 * @param UserContactPrivacy $userContactPrivacy
	 * @return void
	 * @internal param SharedOfficeBooking $sharedOfficeBooking
	 */
    public function deleted(UserContactPrivacy $userContactPrivacy)
    {
        //
    }
	
	/**
	 * Handle the shared office booking "restored" event.
	 *
	 * @param UserContactPrivacy $userContactPrivacy
	 * @return void
	 * @internal param SharedOfficeBooking $sharedOfficeBooking
	 */
    public function restored(UserContactPrivacy $userContactPrivacy)
    {
        //
    }
	
	/**
	 * Handle the shared office booking "force deleted" event.
	 *
	 * @param UserContactPrivacy $userContactPrivacy
	 * @return void
	 * @internal param SharedOfficeBooking $sharedOfficeBooking
	 */
    public function forceDeleted(UserContactPrivacy $userContactPrivacy)
    {
        //
    }
}
