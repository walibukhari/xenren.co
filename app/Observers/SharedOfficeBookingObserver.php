<?php

namespace App\Observers;

use App\Models\SharedOfficeBooking;
use App\Models\User;
use App\Services\PushNotificationService;
use App\Traits\PredisTrait;

class SharedOfficeBookingObserver
{
    use PredisTrait;
    /**
     * Handle the shared office booking "created" event.
     *
     * @param  \App\Models\SharedOfficeBooking  $sharedOfficeBooking
     * @return void
     */
    public function created(SharedOfficeBooking $sharedOfficeBooking)
    {
        $data = User::where('email', '=', $sharedOfficeBooking->client_email)->first();
        if(isset($data)) {
            \Log::info('user');
            \Log::info($data);
            \Log::info('user');
            \Log::info('device_token');
            \Log::info($data->device_token);
            \Log::info('device_token');
            $arr = collect([
                'body' => 'You received a new message',
                'title' => 'New Message - XENREN',
                'sharedOffice' => true,
                'data' => 'Shared office booking request received'
            ]);
            if (!is_null($data)) {
                \Log::info('shared office booking notification');
                $a = PushNotificationService::sendPush($data->device_token, $arr);
                \Log::info($a);
                \Log::info('shared office booking notification');
            }
        }
    }

    /**
     * Handle the shared office booking "updated" event.
     *
     * @param  \App\Models\SharedOfficeBooking  $sharedOfficeBooking
     * @return void
     */
    public function updated(SharedOfficeBooking $sharedOfficeBooking)
    {
        //
    }

    /**
     * Handle the shared office booking "deleted" event.
     *
     * @param  \App\Models\SharedOfficeBooking  $sharedOfficeBooking
     * @return void
     */
    public function deleted(SharedOfficeBooking $sharedOfficeBooking)
    {
        //
    }

    /**
     * Handle the shared office booking "restored" event.
     *
     * @param  \App\Models\SharedOfficeBooking  $sharedOfficeBooking
     * @return void
     */
    public function restored(SharedOfficeBooking $sharedOfficeBooking)
    {
        //
    }

    /**
     * Handle the shared office booking "force deleted" event.
     *
     * @param  \App\Models\SharedOfficeBooking  $sharedOfficeBooking
     * @return void
     */
    public function forceDeleted(SharedOfficeBooking $sharedOfficeBooking)
    {
        //
    }
}
