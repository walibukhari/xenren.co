<?php

namespace App\Observers;

use App\Models\GuestRegister;
use App\Services\PushNotificationService;
use App\Traits\PredisTrait;

class RegisterCodeNotification
{
    use PredisTrait;
    /**
     * Handle the guest register "created" event.
     *
     * @param  \App\\Models\GuestRegister  $guestRegister
     * @return void
     */
    public function created(GuestRegister $guestRegister)
    {
        $data = GuestRegister::where('device_token', '=', $guestRegister->device_token)->first();
        \Log::info('device_token');
        \Log::info($data->device_token);
        \Log::info('device_token');
        $arr = collect([
            'body' => 'You received a new message',
            'title' => 'New Message - XENREN',
            'sharedOffice' => true,
            'data' => $data->code
        ]);
        if (!is_null($data)) {
            \Log::info('shared office booking notification');
            $a = PushNotificationService::sendPush($data->device_token, $arr);
            \Log::info($a);
            \Log::info('shared office booking notification');
        }
    }

    /**
     * Handle the guest register "updated" event.
     *
     * @param  \App\\Models\GuestRegister  $guestRegister
     * @return void
     */
    public function updated(GuestRegister $guestRegister)
    {
        //
    }

    /**
     * Handle the guest register "deleted" event.
     *
     * @param  \App\\Models\GuestRegister  $guestRegister
     * @return void
     */
    public function deleted(GuestRegister $guestRegister)
    {
        //
    }

    /**
     * Handle the guest register "restored" event.
     *
     * @param  \App\\Models\GuestRegister  $guestRegister
     * @return void
     */
    public function restored(GuestRegister $guestRegister)
    {
        //
    }

    /**
     * Handle the guest register "force deleted" event.
     *
     * @param  \App\\Models\GuestRegister  $guestRegister
     * @return void
     */
    public function forceDeleted(GuestRegister $guestRegister)
    {
        //
    }
}
