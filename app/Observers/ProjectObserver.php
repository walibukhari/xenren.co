<?php

namespace App\Observers;

use App\Jobs\CheckBadWords;
use App\Libraries\Pushy\PushyAPI;
use App\Models\BadWordsFilter;
use App\Models\GuestRegister;
use App\Models\Project;
use App\Models\User;
use App\Services\PushNotificationService;
use App\Traits\PredisTrait;

class ProjectObserver
{
    /**
     * @param Project $project
     */
    public function created(Project $project)
    {
        \Log::info('Project crate Observer Called');
        $job = new CheckBadWords($project->user_id, $project->name, BadWordsFilter::TYPE_PROJECT, $project->id);
        dispatch($job);

        $job = new CheckBadWords($project->user_id, $project->description, BadWordsFilter::TYPE_PROJECT, $project->id);
        dispatch($job);
    }

    /**
     * @param Project $project
     */
    public function updated(Project $project)
    {
        \Log::info('Project Update Observer Called');
        $job = new CheckBadWords($project->user_id, $project->name, BadWordsFilter::TYPE_PROJECT, $project->id);
        dispatch($job);

        $job = new CheckBadWords($project->user_id, $project->description, BadWordsFilter::TYPE_PROJECT, $project->id);
        dispatch($job);
    }
}