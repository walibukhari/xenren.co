<?php

namespace App\Observers;

use App\Models\User;
use App\Models\UserInboxMessages;
use App\Services\PushNotificationService;
use App\Traits\PredisTrait;

class UserInboxMessagesObserver {
	use PredisTrait;

	/**
	 * This function listen to user_inbox_message created fuction and send push notification
	 * to android/ios devices.
	 *
	 * @param UserInboxMessages|UserInboxMessagesObserver $userInboxMessages
	 */
	public function created(UserInboxMessages $userInboxMessages)
	{
//		\Log::info('user inbox observer called');
		$data = User::where('id', '=', $userInboxMessages->to_user_id)->first();
		$arr = collect([
			'body' => 'You received a new message',
			'title' => 'New Message - XENREN',
			'sharedOffice' => false,
			'message' => 'New project has been posted match to your skills'
		]);
		if (!is_null($data)) {
//			\Log::info('pushing to redis');
			$a = $this->pushToRedis('private-message' , $arr, 'global-channel');
//			\Log::info($a);
//			\Log::info('pushed to redis');
//			PushNotificationService::sendPush('dfff1f7a1122f124120d03', $arr);
			PushNotificationService::sendPush($data->device_token, $arr, 'New Message Received');
		}
	}

}
