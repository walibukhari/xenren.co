<?php

namespace App\Observers;

use App\Models\SharedOffice;
use App\Models\SharedOfficeBarcode;
use App\Services\SharedOfficeService;
use Log;

class SharedOfficesObserver
{
    /**
     * Handle the shared office "created" event.
     */
    public function created(SharedOffice $sharedOffice)
    {
        $sharedOfficeBarcodeType1 = SharedOfficeBarcode::create([
            'shared_office_id' => $sharedOffice->id,
            'type' => SharedOfficeBarcode::BARCODE_TYPE,
            'barcode' => 'https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=office:'.$sharedOffice->id.'|type:'.SharedOfficeBarcode::BARCODE_TYPE.'|category:'.SharedOfficeBarcode::BARCODE_CATEGORY_TYPE,
            'category' => SharedOfficeBarcode::BARCODE_CATEGORY_TYPE,
            'code' => '',
            'password' => '',
        ]);

        $sharedOfficeBarcodeType2 = SharedOfficeBarcode::create([
            'shared_office_id' => $sharedOffice->id,
            'type' => SharedOfficeBarcode::BARCODE_TYPE_TWO,
            'barcode' => 'https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=office:'.$sharedOffice->id.'|type:'.SharedOfficeBarcode::BARCODE_TYPE_TWO.'|category:'.SharedOfficeBarcode::BARCODE_CATEGORY_TYPE,
            'category' => SharedOfficeBarcode::BARCODE_CATEGORY_TYPE,
            'code' => '',
            'password' => '',
        ]);

        $sharedOfficeBarcodeType3 = SharedOfficeBarcode::create([
            'shared_office_id' => $sharedOffice->id,
            'type' => SharedOfficeBarcode::BARCODE_TYPE_THREE,
            'barcode' => 'https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=office:'.$sharedOffice->id.'|type:'.SharedOfficeBarcode::BARCODE_TYPE_THREE.'|category:'.SharedOfficeBarcode::BARCODE_CATEGORY_TYPE,
            'category' => SharedOfficeBarcode::BARCODE_CATEGORY_TYPE,
            'code' => '',
            'password' => '',
        ]);
        \Log::info('shared_office_barcode_type_1');
        \Log::info($sharedOfficeBarcodeType1);
        \Log::info('shared_office_barcode_type_2');
        \Log::info($sharedOfficeBarcodeType2);
        \Log::info('shared_office_barcode_type_3');
        \Log::info($sharedOfficeBarcodeType3);
        $this->updateSharedOfficeJsonList($sharedOffice, 'created event');
    }

    public function updated(SharedOffice $sharedOffice)
    {
        $this->updateSharedOfficeJsonList($sharedOffice, 'Updated event');
    }

    public function deleted(SharedOffice $sharedOffice)
    {
        $this->deleteSharedOfficeJsonList($sharedOffice, 'Delete event');
    }

    private function updateSharedOfficeJsonList(SharedOffice $sharedOffice, $source = 'unknown source')
    {
        Log::INFO("updateSharedOfficeJsonList called from {$source}");

        // return (new SharedOfficeService())->updateJsonList($sharedOffice->toArray());
    }

    private function deleteSharedOfficeJsonList(SharedOffice $sharedOffice, $source = 'unknown source')
    {
        Log::INFO("updateSharedOfficeJsonList called from {$source}");

        // return (new SharedOfficeService())->deleteJsonList($sharedOffice->toArray());
    }
}
