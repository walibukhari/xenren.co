<?php
/**
 * Created by PhpStorm.
 * User: ahsanhussain
 * Date: 6/3/19
 * Time: 1:42 PM.
 */

namespace App\Observers;

use App\Jobs\CheckBadWords;
use App\Models\BadWordsFilter;
use App\Models\User;
use App\Models\UserActivity;

class UserObserver
{
    public function updated(User $user)
    {
        \Log::info('create entry user activity');
        $userArray = $user->toArray();
        $filteredUser = $this->filterDirty($user, [
            'id', 'name', 'email', 'skype_id', 'wechat_id', 'line_id', 'handphone_no',
        ]);
        UserActivity::create([
            'user_id' => $filteredUser['id'],
            'username' => $filteredUser['name'],
            'email' => $filteredUser['email'],
            'skype' => $filteredUser['skype_id'],
            'we_chat' => $filteredUser['wechat_id'],
            'line' => $filteredUser['line_id'],
            'phone_number' => $filteredUser['handphone_no'],
        ]);
        \Log::info('User Updated Observer Called');
        $job = new CheckBadWords($user->id, $user->about_me, BadWordsFilter::TYPE_PROFILE, $user->id);
        dispatch($job);

        $job = new CheckBadWords($user->id, $user->name, BadWordsFilter::TYPE_PROFILE, $user->id);
        dispatch($job);

        $job = new CheckBadWords($user->id, $user->title, BadWordsFilter::TYPE_PROFILE, $user->id);
        dispatch($job);
    }

    public function created(User $user)
    {
        \Log::info('User Created Observer Called');
        \Log::info($user->name);
        $job = new CheckBadWords($user->id, $user->about_me, BadWordsFilter::TYPE_PROFILE, $user->id);
        dispatch($job);

        $job = new CheckBadWords($user->id, $user->name, BadWordsFilter::TYPE_PROFILE, $user->id);
        dispatch($job);

        $job = new CheckBadWords($user->id, $user->title, BadWordsFilter::TYPE_PROFILE, $user->id);
        dispatch($job);
    }

    private function filterDirty(User $model, $filters)
    {
        $data = User::query()->select($filters)->where('id', $model->id)->first();
        if (!$data) {
            return $model->toArray();
        }

        return $data;
    }
}
