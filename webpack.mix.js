const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//general files
mix.styles([
    'public/custom/css/frontend/headerOrFooter.css', //main file
    'public/custom/css/frontend/home.css', //main file
    'public/custom/css/frontend/modalSetUsername.css',
    'public/custom/css/frontend/modalSetContactInfo.css',
    'public/custom/css/frontend/modalAddNewSkillKeywordv2.css',
    'public/css/sales-version.css',
    'public/css/style.css', //main file
    'public/css/frontend_custom.css', //main file
    'public/css/frontend.css', //main file
    'public/custom/css/frontend/searchButtons.css',
    'public/assets/layouts/layout4/css/custom.min.css',
    'public/assets/layouts/layout4/css/themes/light.css',
    'public/assets/layouts/layout4/css/layout.css', //main file
    'public/assets/global/plugins/magicsuggest/magicsuggest-min.css',
    'public/assets/global/plugins/bootstrap-toastr/toastr.min.css',
    'public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
    'public/assets/global/plugins/uniform/css/uniform.default.css', //main file
], 'public/css/app.css');

//xenren home page file
mix.styles([
        'public/xenren-assets/css/bootstrap.min.css',
        'public/xenren-assets/css/animate.min.css',
        'public/xenren-assets/css/font-awesome.min.css',
        'public/xenren-assets/css/themify-icons.css',
        'public/xenren-assets/css/YTPlayer.css',
        'public/xenren-assets/css/owl.carousel.css',
        'public/xenren-assets/css/magnific-popup.css',
        'public/xenren-assets/css/style.css',
        'public/xenren-assets/components/revolutionslider/css/settings.css',
        'public/xenren-assets/components/revolutionslider/css/navigation.css',
        'public/xenren-assets/css/responsive.css',
        'public/assets/global/plugins/bootstrap-toastr/toastr.min.css',
        'public/assets/global/plugins/magicsuggest/magicsuggest-min.css',
        'public/css/frontend-new-color.css',
        'public/css/homeHeader/homeHeader.css',
], 'public/css/minifiedHome.css');

//xenren inner home page css
mix.styles([
   'public/assets/global/plugins/bootstrap/css/bootstrap.min.css',
   'public/assets/global/css/components-md.min.css',
   'public/assets/global/css/plugins-md.min.css',
   'public/css/themify-icons.css',
   'public/custom/css/frontend/homePagePopup.css',
], 'public/css/innerMinifiedHome.css');

//jobs page css
mix.styles([
    'public/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css',
    'public/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css',
    'public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
    'public/custom/css/frontend/jobs.css',
    'public/custom/css/frontend/modalMakeOffer.css',
    'public/css/100-coworkers.css',
],'public/css/minifiedJobs.css');

//findExperts page css
mix.styles([
    'public/assets/global/plugins/bootstrap-star-rating/css/star-rating.css',
    'public/assets/pages/css/profile.min.css',
    'public/custom/css/frontend/findExperts.css',
    'public/custom/css/frontend/findExpertsNew.css',
    'public/assets/global/plugins/select2/css/select2.css',
    'public/assets/global/plugins/select2/css/select2-bootstrap.min.css',
    'public/custom/css/frontend/modalInviteToWork.css',
    'public/custom/css/frontend/modalSendOffer.css',
    'public/custom/css/frontend/findExpertsSearch.css',
],'public/css/minifiedFindExpert.css');

//favourite job page css
mix.styles([
    'public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
    'public/custom/css/frontend/jobs.css',
    'public/custom/css/frontend/favouriteJobs.css',
    'public/custom/css/frontend/modalMakeOffer.css',
],'public/css/minifiedFavouriteJob.css');

//favourite freelancer page css
mix.styles([
    'public/custom/css/frontend/modalInviteToWork.css',
    'public/custom/css/frontend/favouriteFreelancers.css',
    'public/custom/css/frontend/modalSendOffer.css',
],'public/css/minifiedFavouriteFreelancer.css');

//inbox page general css
mix.styles([
    'public/custom/css/frontend/inbox.css',
],'public/css/minifiedInbox.css');
//inbox Messages page css
mix.styles([
    'public/custom/css/frontend/InboxMessages.css',
    'public/custom/css/frontend/modalMakeOffer.css',
    'public/assets/global/plugins/lightbox2/dist/css/lightbox.min.css',
],'public/css/minifiedInboxMessage.css');

//join discussion page css
mix.styles([
    'public/assets/global/plugins/bootstrap-star-rating/css/star-rating.css',
    'public/assets/pages/css/profile.min.css',
    'public/custom/css/frontend/joinDiscussion.css',
    'public/custom/css/frontend/findExperts.css',
    'public/assets/global/plugins/lightbox2/dist/css/lightbox.min.css',
    'public/custom/css/frontend/modalMakeOffer.css',
    'public/custom/css/frontend/modalMakeOfferDetail.css',
    'public/custom/css/frontend/personalPortfolio.css',
    'public/custom/css/frontend/skill_review_review_portfolio.css',
    'public/custom/css/frontend/modalSendPrivateMessage.css',
    'public/assets/global/plugins/bootstrap-summernote/summernote.css',
    'public/css/100-coworkers.css',
],'public/css/JoinDiscussion.css');

//post project page css
mix.styles([
    'public/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',
    'public/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css',
    'public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
    'public/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.css',
    'public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
    'public/custom/css/frontend/postProject.css',
    'public/custom/css/frontend/modalAddNewSkillKeywordv2.css',
    'public/assets/global/plugins/dropzone-4.0.1/min/dropzone.min.css',
    'public/assets/global/plugins/dropzone-4.0.1/min/basic.min.css',
    'public/assets/global/plugins/bootstrap-summernote/summernote.css',
],'public/css/PostProject.css');

//ReceiveOrder offical JobDetail Page
mix.styles([
    'public/assets/pages/css/profile.min.css',
    'public/custom/css/frontend/userCenter.css',
    'public/custom/css/frontend/fixedPriceEscrowModule.css',
    'public/custom/css/frontend/jobDetails.css',
    'public/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',
    'public/calender/public/styles/css/bundle.min.css',
],'public/css/OfficalJobDetail.css');

//ReceiveOrder JobDetail Page
mix.styles([
    'public/assets/pages/css/profile.min.css',
    'public/custom/css/frontend/userCenter.css',
    'public/custom/css/frontend/fixedPriceEscrowModule.css',
    'public/custom/css/frontend/jobDetails.css',
    'public/custom/css/frontend/jobMilestone.css',
    'public/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',
],'public/css/JobDetail.css');



mix.styles([
    'public/css/dateTimePicker.css',
    'public/css/income_admin.css',
    'public/css/newDesign.css',
    'public/css/backendproducts.css'
],'public/css/OwnerDashboard.css');



//Owner End
mix.styles([

    'public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
    'public/assets/layouts/layout/css/custom.min.css',
    'public/assets/layouts/layout/css/themes/darkblue.min.css',
    'public/assets/layouts/layout/css/layout.min.css',
    'public/custom/plugins/anytimepicker/flick.css',
    'public/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',
    'public/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
    'public/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
    'public/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css',
    'public/custom/plugins/anytimepicker/anytimepicker.min.css',
    'public/assets/global/css/plugins.min.css',
    'public/assets/global/css/components-rounded.min.css',
    'public/assets/global/plugins/jcrop/css/jquery.Jcrop.min.css',
    'public/assets/global/plugins/bootstrap-toastr/toastr.min.css',
    'public/assets/global/plugins/select2/css/select2-bootstrap.min.css',
    'public/assets/global/plugins/select2/css/select2.min.css',
    'public/assets/global/plugins/jquery-multi-select/css/multi-select.css',
    'public/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css',
    'public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
    'public/css/global.css',
    'public/css/backend.css',

],'public/css/MainBackend.css');




//general
mix.scripts([
    'public/js/global.js', //main file
    'public/js/app1.js', //main file
    'public/js/ajaxform.min.js',
    'public/assets/layouts/layout4/scripts/demo.min.js',
    'public/assets/layouts/global/scripts/quick-sidebar.min.js',
    'public/assets/layouts/layout4/scripts/layout.min.js',
    'public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
], 'public/js/app.js');

//xenren inner home page js
mix.scripts([
    'public/assets/global/plugins/jquery.min.js',
    'public/assets/global/plugins/bootstrap/js/bootstrap.min.js',
    'public/assets/global/plugins/js.cookie.min.js',
    'public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
    'public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
    'public/assets/global/plugins/jquery.blockui.min.js',
    'public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
    'public/assets/global/plugins/bootstrap-toastr/toastr.min.js',
    'public/assets/global/plugins/magicsuggest/magicsuggest-min.js',
    'public/assets/global/plugins/morris/morris.min.js',
],'public/js/innerMinifiedHome.js');


//home page js
mix.scripts([
    'public/xenren-assets/js/bootstrap.min.js',
    'public/xenren-assets/js/jquery.fitvids.js',
    'public/xenren-assets/js/jquery.mb.YTPlayer.js',
    'public/xenren-assets/js/owl.carousel.min.js',
    'public/xenren-assets/js/wow.min.js',
    'public/xenren-assets/js/jquery.parallax-1.1.3.js',
    'public/xenren-assets/js/jquery.countTo.js',
    'public/xenren-assets/js/jquery.countdown.min.js',
    'public/xenren-assets/js/jquery.appear.js',
    'public/xenren-assets/js/smoothscroll.js',
    'public/xenren-assets/js/jquery.magnific-popup.min.js',
    'public/xenren-assets/components/revolutionslider/js/jquery.themepunch.revolution.min.js',
    'public/xenren-assets/components/revolutionslider/js/jquery.themepunch.tools.min.js',
    'public/xenren-assets/components/revolutionslider/js/extensions/revolution.extension.video.min.js',
    'public/xenren-assets/components/revolutionslider/js/extensions/revolution.extension.slideanims.min.js',
    'public/xenren-assets/components/revolutionslider/js/extensions/revolution.extension.actions.min.js',
    'public/xenren-assets/components/revolutionslider/js/extensions/revolution.extension.layeranimation.min.js',
    'public/xenren-assets/components/revolutionslider/js/extensions/revolution.extension.kenburn.min.js',
    'public/xenren-assets/components/revolutionslider/js/extensions/revolution.extension.navigation.min.js',
    'public/xenren-assets/components/revolutionslider/js/extensions/revolution.extension.migration.min.js',
    'public/xenren-assets/components/revolutionslider/js/setup.js',
    'public/xenren-assets/js/jquery.particleground.js',
    'public/xenren-assets/js/imagesloaded.pkgd.min.js',
    'public/xenren-assets/js/isotope.pkgd.min.js',
    'public/xenren-assets/js/main.js',
    'public/assets/global/plugins/jquery.blockui.min.js',
    'public/assets/global/plugins/bootstrap-toastr/toastr.min.js',
    'public/assets/global/plugins/magicsuggest/magicsuggest-min.js',
], 'public/js/minifiedHome.js');

//jobs page js
mix.scripts([
    'public/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js',
    'public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
    'public/custom/js/frontend/ucProjectItem.js',
    'public/custom/js/frontend/jobs.js'
],'public/js/minifiedJobs.js');

//find expert page js
mix.scripts([
    'public/assets/global/plugins/bootstrap-star-rating/js/star-rating.js',
    'public/assets/global/plugins/select2/js/select2.full.min.js',
    'public/assets/global/plugins/select2/js/i18n/zh-CN.js',
    'public/assets/global/plugins/readmore/readmore.min.js',
    'public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
    'public/custom/js/frontend/findExperts.js'
],'public/js/minifiedFinExpert.js');

//favourite job page js
mix.scripts([
    'public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
    'public/custom/js/frontend/favouriteJobs.js',
],'public/js/minifiedFavouriteJob.js');



//favourite freelancer page js
mix.scripts([
    'public/custom/js/frontend/favouriteFreelancers.js',
],'public/js/minifiedFavouriteFreelancer.js');

//Inbox Messages page js
mix.scripts([
    'public/custom/js/frontend/inboxMessages.js',
    'public/assets/global/plugins/lightbox2/dist/js/lightbox.min.js',
    'public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
],'public/js/minifiedInboxMessage.js');


//Join Discussion page js
mix.scripts([
    'public/custom/js/frontend/joinDiscussion.js',
    'public/custom/js/frontend/favouriteJobs.js',
    'public/custom/js/frontend/OnOffswitch.js',
    'public/custom/js/frontend/modalAddNewSkillKeywordv2.js',
],'public/js/minifiedJoinDiscussion.js');


//Post project js
mix.scripts([
    'public/custom/js/frontend/modalAddNewSkillKeywordv2.js',
    'public/custom/js/frontend/postProject.js',
    'public/assets/global/plugins/bootstrap-summernote/lang/summernote-zh-CN.js',
    'public/assets/global/plugins/bootstrap-summernote/summernote.min.js',
    'public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
    'public/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js',
    'public/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js',
    'public/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js',
    'public/assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
],'public/js/minifiedPostProject.js');


//ReceiveOrder offical JobDetail Page
mix.scripts([
    'public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
    'public/custom/js/frontend/orderDetail.js',
    'public/js/moment.min.js',
    'public/js/bootstrap-datepicker.min.js',
    'public/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js',
],'public/js/minifiedOfficalJobDetail.js');
