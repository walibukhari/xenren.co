<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

//use App\Libraries\Pushy\PushyAPI;
//use App\Models\User;
//use Snipe\BanBuilder\CensorWords;
//
//Route::get('test', function(){
//    $users = User::select([
//        'id', 'web_token', 'post_project_notify'
//    ])->where('post_project_notify', '=', 1)->get();
//
//    $usersTokens = array();
//    foreach($users as $user)
//    {
//        array_push($usersTokens, $user->web_token);
//    }
//
//    $data = array(
//        "title" => "Xenren", // Notification title
//        'message' => 'asasd',
//        'body' => 'asdsad'
//    );
//    $options = array(
//        'notification' => array(
//            'badge' => 0,
//            'sound' => 'ping.aiff',
//            'body'  => "New project posted on Xenren"
//        )
//    );
//    PushyAPI::sendPushNotification($data, $usersTokens, $options);
//});

Route::get('mmap', function(){
    return view('map.index');
});

Route::get('rm', [
    'uses' => 'HomeController@rm'
]);
Route::get('revenue-monster/redirect', [
    'uses' => 'TopupController@redirect'
]);
Route::get('revenue-monster/notify', [
    'uses' => 'HomeController@notify'
]);

Route::get('get-map-data', [
    'uses' => 'Frontend\SharedOfficeController@mapJson'
]);

Route::get('get-app-map-data', [
    'uses' => 'Frontend\SharedOfficeController@mapJsonApp'
]);

Route::get('{lang?}/booking/requests', [
    'uses' => 'Frontend\SharedOfficeController@getBookingData'
]);

Route::get('/change/booking/request/{office_id}', [
    'uses' => 'Frontend\SharedOfficeController@changeBookingRequest'
]);

Route::get('{lang?}/booking/detail/{id}', [
    'uses' => 'Frontend\SharedOfficeController@bookingDetails'
]);

Route::get('booking/request/cancel/{id}', [
    'as' => 'client.request.cancel',
    'uses' => 'Frontend\SharedOfficeController@requestCancel'
]);

Route::get('/cancel/booking/{office_id}/{id}/{category_id}', [
    'as' => 'client.request.cancel',
    'uses' => 'Frontend\SharedOfficeController@cancelBooking'
]);

Route::group(['domain' => 'space.xenren.co', 'middleware' => 'sharedoffice'], function() {
    Route::get('/', function () {
        if(is_null(\Auth::guard('staff')->user())) {
            return redirect('/en/coworking-spaces/sign-in');
        } else {
            return redirect('/admin');
        }
    });
});

Route::get('/save/device/token/{userId}/{deviceToken}', 'SendPushyNotifications@saveDeviceToken');

Route::group(array('prefix' => 'shared-office', 'namespace' => 'Backend',), function (){
    Route::post('/create-owner','StaffController@sharedOfficeSignUp');
});

Route::get('adminlang/{lang}', ['as' => 'adminlang', 'uses' => 'EtcController@adminLang']);

Route::group(['domain' => 'coworkspace.xenren.co', 'middleware' => 'sharedoffice'], function() {
    Route::get('/', function () {
        return redirect('/en/coworking-spaces');
    });
});
// Route::group(['domain' => 'www.xenren.co'], function() {
Route::group(['middleware' => 'redirectRoute'], function () {
    Route::get('/home ', function () {
        return redirect('/');
    });
    Route::get('/shared ', 'Services\SharedOfficeController@freeSeats');
    Route::get('/test-paypal', 'PaymentGateways\PayPalPhpSdk\PayPalController@index');
    Route::get('/test-paypal-withdraw', 'PaymentGateways\PayPalPhpSdk\PayPalController@withdrawPayPal');
    Route::get('/test-paypal-withdraw-status', 'PaymentGateways\PayPalPhpSdk\PayPalController@withdrawPayPalDetail');
    Route::get('/cancel-paypal-payment', 'PaymentGateways\PayPalPhpSdk\PayPalController@cancelPayPal');

    Route::get('/receive-Image', [
        'uses' => 'Api\TimeTrackController@getImage'
    ]);

    Route::get('/video/share ', [
        'as' => 'frontend.shareVideo',
        'uses' => 'HomeController@shareVideo',
    ]);

    Route::get('/verify/{token}', [
        'as' => 'frontend.newDesign.acceptInvitation',
        'uses' => 'Frontend\InviteController@verifyToken',
    ]);

//    Route::group(['middleware' => ['web', 'auth:users']], function () {
        Route::post('topup-payment', [
            'as' => 'paypal.topup-payment',
            'uses' => 'TopupController@index',
        ]);
        Route::post('paypal-topup', [
            'as' => 'paypal.topup',
            'uses' => 'PaymentGateways\PayPalPhpSdk\PayPalController@index',
        ]);
        Route::post('add-paypal', [
            'as' => 'add.paypal',
            'uses' => 'Frontend\UserCenterController@addPayPal',
        ]);
        Route::post('withdraw-paypal', [
            'as' => 'add.withdrawpaypal',
            'uses' => 'PaymentGateways\PayPalPhpSdk\PayPalController@withdrawPayPal',
        ]);
        Route::get('/approve-paypal-payment', 'PaymentGateways\PayPalPhpSdk\PayPalController@approvePayPal');

        Route::post('/remove-image', [
            'uses' => 'Api\TimeTrackController@removeImage'
        ]);
//    });

    Route::group(['middleware' => 'web'], function () {

        Route::group(array('namespace' => 'Frontend', 'middleware' => ['auth:users']), function () {
            // New Design Routes
            Route::get('userRefer', [
                'as' => 'frontend.newDesign.userRefer',
                'uses' => 'InviteController@userRefer',
                'disable_sidemenu' => true,
                'xenren_path' => true,
            ]);
//        Route::post('userRefer', ['as' => 'refer.post', 'uses' => 'InviteController@userReferPost']);

            Route::get("/testpage", [
                'as' => 'frontend.newDesign.friend_invite',
                "uses" => "InviteController@friend_invite"
            ]);

            Route::get('/success', [
                'as' => 'frontend.newDesign.success',
                'uses' => 'InviteController@success',
            ]);

            Route::get('register-complete', [
                'as' => 'register-complete',
                'uses' => 'UserCenterController@afterregister',
                'disable_sidemenu' => true
            ]);

        });
        //Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index', 'disable_sidemenu' => true]);

        // harshadNewDesign
//  Route::get('/harshadNewDesign/changeStatusPopup', function(){
//      $language = 'en';
//      $userStatus = [1,2];
//      $skillList = 1;
//      return view('harshadNewDesign.changeStatusPopup', compact('language','userStatus','skillList'));
//  });
//  Route::get('/harshadNewDesign/email0Popup', function(){
//      $language = 'en';
//      $userStatus = [1,2];
//      $skillList = 1;
//      return view('harshadNewDesign.email0Popup', compact('language','userStatus','skillList'));
//  });
//  Route::get('/harshadNewDesign/email1Popup', function(){
//      $language = 'en';
//      $userStatus = [1,2];
//      $skillList = 1;
//      return view('harshadNewDesign.email1Popup', compact('language','userStatus','skillList'));
//  });
//  Route::get('/harshadNewDesign/facebook2Popup', function(){
//      $language = 'en';
//      $userStatus = [1,2];
//      $skillList = 1;
//      return view('harshadNewDesign.facebook2Popup', compact('language','userStatus','skillList'));
//  });
//  Route::get('/harshadNewDesign/facebook1Popup', function(){
//      $language = 'en';
//      $userStatus = [1,2];
//      $skillList = 1;
//      return view('harshadNewDesign.facebook1Popup', compact('language','userStatus','skillList'));
//  });
//  Route::get('/harshadNewDesign/Xpopup_5', function(){
//      $language = 'en';
//      $userStatus = [1,2];
//      $skillList = 1;
//      return view('harshadNewDesign.Xpopup_5', compact('language','userStatus','skillList'));
//  });
//  Route::get('/harshadNewDesign/Xpopup_6', function(){
//      $language = 'en';
//      $userStatus = [1,2];
//      $skillList = 1;
//      return view('harshadNewDesign.Xpopup_6', compact('language','userStatus','skillList'));
//  });
//


        Route::get('/stripe/invoices', [
            'uses' => 'Frontend\StripeController@invoices'
        ]);

        Route::post('/stripe/charge', [
            'uses' => 'Frontend\StripeController@index'
        ]);

        Route::post('/stripe/webhook', [
            'uses' => 'Frontend\StripeWebhookController@handleWebhook'
        ]);

        // fixedPriceEscrowModule
        Route::get('/publishedJobMilestone', ['as' => 'fixedPriceEscrowModule.publishedJobMilestone', 'uses' => 'HomeController@publishedJobMilestone', 'xenren_path' => true]);
        Route::get('/refundNew', ['as' => 'fixedPriceEscrowModule.refundNew', 'uses' => 'HomeController@refundNew', 'xenren_path' => true]);
        Route::get('/endContractEdited', ['as' => 'fixedPriceEscrowModule.endContractEdited', 'uses' => 'HomeController@endContractEdited', 'xenren_path' => true]);
        Route::get('/refundEdit', ['as' => 'fixedPriceEscrowModule.refundEdit', 'uses' => 'HomeController@refundEdit', 'xenren_path' => true]);

        Route::get('fetchCity', [
            'uses' => 'Frontend\LocationsController@index'
        ]);
        Route::get('evidence', [
            'uses' => 'HomeController@evidence',
            'disable_sidemenu' => true,
            'xenren_path' => true,
        ]);

        // New Color landing page
        Route::get('/new', ['as' => 'home', 'uses' => 'HomeController@xenrenLandingNew', 'xenren_path' => true]);
        Route::get('/new-CN', ['as' => 'home', 'uses' => 'HomeController@xenrenLandingNewCN', 'xenren_path' => true]);

        // fixedPriceEscrowModule
        //Check this publishedJobMilestone route
        Route::get('/publishedJobMilestone', ['as' => 'fixedPriceEscrowModule.publishedJobMilestone', 'uses' => 'HomeController@publishedJobMilestone', 'xenren_path' => true]);
        Route::get('/refundNew', ['as' => 'fixedPriceEscrowModule.refundNew', 'uses' => 'HomeController@refundNew', 'xenren_path' => true]);
        Route::get('/endContractEdited', ['as' => 'fixedPriceEscrowModule.endContractEdited', 'uses' => 'HomeController@endContractEdited', 'xenren_path' => true]);
        Route::get('/refundEdit', ['as' => 'fixedPriceEscrowModule.refundEdit', 'uses' => 'HomeController@refundEdit', 'xenren_path' => true]);

        // Mobile web landing page
        Route::get('/mobileWebLanding', ['as' => 'mobileWebLanding.default', 'uses' => 'HomeController@mobileWebLanding', 'xenren_path' => true]);


        // instruction Hiring
        Route::get('{lang?}/instructionHiring', ['as' => 'frontend.instruction.hiring', 'uses' => 'HomeController@instructionHiring', 'xenren_path' => true]);

        // test route
//    Route::get('/xenrenLanding', ['as' => 'home.xenrenLanding', 'uses' => 'HomeController@xenrenLandingTest', 'xenren_path' => true]);

        Route::get('{lang?}/faq-buyer', ['as' => 'faq.faq-buyer', 'uses' => 'HomeController@faqBuyer', 'xenren_path' => true]);
        Route::get('{lang?}/faq-buyer/freelancer', ['as' => 'faq.faq-buyer.freelancer', 'uses' => 'HomeController@faqBuyerFreelancer', 'xenren_path' => true]);
        Route::get('/faq-question-main/freelancer/{id}', ['as' => 'faq.faq-question-main.freelancer', 'uses' => 'HomeController@faqQuestionMainFreelancer', 'xenren_path' => true]);
        Route::get('{lang?}/faq-buyer/employer', ['as' => 'faq.faq-buyer.employer', 'uses' => 'HomeController@faqBuyerEmployer', 'xenren_path' => true]);
        Route::get('/faq-question-main/employer/{id}', ['as' => 'faq.faq-question-main.employer', 'uses' => 'HomeController@faqQuestionMainEmployer', 'xenren_path' => true]);
        Route::get('{lang?}/co-Work-faq/cowork-space', ['as' => 'faq.coWorkfaq.cowork.space', 'uses' => 'HomeController@coWorkfaqSpaceOwner', 'xenren_path' => true]);
        Route::get('{lang?}/co-Work-faq/cowork-user', ['as' => 'faq.coWorkfaq.cowork.user', 'uses' => 'HomeController@coWorkfaqUser', 'xenren_path' => true]);
        Route::post('/send-request', ['as' => 'faq.send-request.post', 'uses' => 'HomeController@sendRequest', 'xenren_path' => true]);
        Route::get('/faq-question-main/{id}', ['as' => 'faq.faq-question-main', 'uses' => 'HomeController@faqQuestionMain', 'xenren_path' => true]);
        Route::get('/co-Work-faq-question-main/{id}', ['as' => 'coWork.faq.faq-question-main', 'uses' => 'HomeController@coWorkfaqQuestionMain', 'xenren_path' => true]);
        Route::get('/co-Work-faq-question-main/user/{id}', ['as' => 'coWork.faq.faq-question-main.user', 'uses' => 'HomeController@coWorkfaqQuestionMainUser', 'xenren_path' => true]);
        Route::get('/faq-question-sub', ['as' => 'faq.faq-question-sub', 'uses' => 'HomeController@faqQuestionSub', 'xenren_path' => true]);
        Route::get('/send-request', ['as' => 'faq.send-request', 'uses' => 'HomeController@sendRequest', 'xenren_path' => true]);
        Route::get('{lang?}/privacy-policy', ['as' => 'frontend.privacy-policy', 'uses' => 'HomeController@privacyPolicy', 'xenren_path' => true]);
        Route::get('{lang?}/meetTheTeam', ['as' => 'frontend.meet-the-team2', 'uses' => 'HomeController@salesVersion52', 'xenren_path' => true]);
        Route::get('{lang?}/meetSalesTeam', ['as' => 'frontend.meet-sales-team', 'uses' => 'HomeController@salesVersion2', 'xenren_path' => true]);
        Route::get('/meetTheTeam-5-3', ['as' => 'frontend.meet-the-team3', 'uses' => 'HomeController@salesVersion53', 'xenren_path' => true]);
        Route::get('{lang?}/terms-service', ['as' => 'frontend.terms-service', 'uses' => 'HomeController@termsService', 'xenren_path' => true]);
        Route::get('{lang?}/aboutUs', ['as' => 'frontend.about-us', 'uses' => 'HomeController@aboutUs', 'xenren_path' => true]);
        Route::get('{lang?}/contactUs', ['as' => 'frontend.contact-us', 'uses' => 'HomeController@contactUs', 'xenren_path' => true]);
        Route::get('{lang?}/cooperatePage', ['as' => 'frontend.cooperate-page', 'uses' => 'HomeController@cooperatePage', 'xenren_path' => true]);
        Route::post('/cooperatePage', ['as' => 'frontend.feedbacksent', 'uses' => 'HomeController@sentFeedback', 'xenren_path' => true]);
        Route::get('{lang?}/hiringPage', ['as' => 'frontend.hiring-page', 'uses' => 'HomeController@hiringPage', 'xenren_path' => true]);
        Route::get('/hiringPage2', ['as' => 'frontend.hiring-page2', 'uses' => 'HomeController@hiringPage2', 'xenren_path' => true]);
        Route::get('{lang?}/companyCulture', ['as' => 'frontend.company-culture', 'uses' => 'HomeController@companyCulture', 'xenren_path' => true]);
        Route::get('{lang?}/chineseCompanyCulture', ['as' => 'frontend.chinese-company-culture', 'uses' => 'HomeController@chineseCompanyCulture', 'xenren_path' => true]);


        Route::get('/testSendEmail', ['as' => 'home.test.send.email', 'uses' => 'HomeController@testSendEmail', 'xenren_path' => true]);
        Route::post('/getNewNotification', ['as' => 'frontend.getnewnotification.post', 'uses' => 'HomeController@getNewNotificationPost']);
        Route::post('/updateNotificationToIsRead', ['as' => 'frontend.updatenotificationtoisread.post', 'uses' => 'HomeController@updateNotificationToIsRead']);

        //wechat payment
        Route::get('wechat/qr/{amount}', ['as' => 'wechat/qr', 'uses' => 'Frontend\WechatPaymentController@qr_scan']);
        Route::any('wechat/notify', ['as' => 'wechat/notify', 'uses' => 'Frontend\WechatPaymentController@notify']);


//    Route::get('payment/callback', ['as' => 'payment.callback', 'uses' => 'Frontend\PaymentController@paymentCallback', 'disable_sidemenu' => true]);

        Route::get('setlang/{lang}', ['as' => 'setlang', 'uses' => 'EtcController@setLang']);

        Route::get('{lang?}/login', ['as' => 'login', 'uses' => 'Auth\AuthController@login', 'disable_sidemenu' => true]);
        Route::post('login', ['as' => 'login.post', 'uses' => 'Auth\AuthController@loginPost']);

        Route::get('/setPassword', [
            'as' => 'setPass',
            'uses' => 'Frontend\InviteController@setPassword',
            'disable_sidemenu' => true,
            'xenren_path' => true,
        ]);

        Route::post('/setPassword', [
            'as' => 'setPassword.post',
            'uses' => 'Frontend\InviteController@setPasswordPost',
            'disable_sidemenu' => true,
            'xenren_path' => true,
        ]);
        Route::get('/inviteFriend/{id}', [
            'as' => 'frontend.newDesign.inviteFriend',
            'uses' => 'Frontend\InviteController@inviteFriend',
            'disable_sidemenu' => true,
            'xenren_path' => true,
        ]);

        Route::post('/inviteFriend', [
            'as' => 'inviteFriend.post',
            'uses' => 'Frontend\InviteController@inviteFriendPost',
            'disable_sidemenu' => true,
            'xenren_path' => true,
        ]);
        Route::get('/acceptInvitation', [
            'as' => 'frontend.newDesign.acceptInvitation',
            'uses' => 'Frontend\InviteController@acceptInvitation',
            'disable_sidemenu' => true,
        ]);

        Route::post('generateLink', [
            'uses' => 'Frontend\InviteController@generateLink',
            'disable_sidemenu' => true,
            'xenren_path' => true,
        ]);


        Route::get('{lang?}/register', ['as' => 'register', 'uses' => 'Auth\AuthController@register', 'disable_sidemenu' => true]);

        Route::post('register', ['as' => 'register.post', 'uses' => 'Auth\AuthController@registerPost']);

        Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@logout']);
        Route::get('password/email', ['as' => 'password.reset', 'uses' => 'Auth\PasswordController@getEmail', 'disable_sidemenu' => true]);
        Route::post('password/email', ['as' => 'password.reset.post', 'uses' => 'Auth\PasswordController@postEmail', 'disable_sidemenu' => true]);

        Route::get('password/reset/{token}', ['as' => 'password.reset.set', 'uses' => 'Auth\PasswordController@getReset', 'disable_sidemenu' => true]);
        Route::post('password/reset', ['as' => 'password.reset.set.post', 'uses' => 'Auth\PasswordController@postReset', 'disable_sidemenu' => true]);

        //email verification
        Route::get('/modal/emailVerification/{email}/{token}', ['as' => 'frontend.modal.emailVerification', 'uses' => 'Auth\AuthController@emailVerification', 'disable_sidemenu' => true]);
        Route::get('/codeConfirmationEmail', ['as' => 'frontend.codeconfirmationemail', 'uses' => 'HomeController@codeConfirmationEmail']);

        Route::get('/codeConfirmation', function () {
            return view("mails.guestConfirmationCode");
        });

        Route::get('codeConfirmation/{key}', ['as' => 'codeconfirmation', 'uses' => 'Auth\AuthController@codeConfirmation', 'disable_sidemenu' => true]);
        Route::post('codeConfirmation', ['as' => 'codeconfirmation.post', 'uses' => 'Auth\AuthController@codeConfirmationPost']);
        Route::get('codeVerification', ['as' => 'codeverification', 'uses' => 'Auth\AuthController@codeVerification', 'disable_sidemenu' => true]);
        Route::post('codeVerification', ['as' => 'codeverification.post', 'uses' => 'Auth\AuthController@codeVerificationPost']);

        //check user email exist
        Route::post('emailExist', ['as' => 'emailexist.post', 'uses' => 'Auth\AuthController@emailExist']);

//    Route::get('password/reset', ['as' => 'password.reset', 'uses' => 'Auth\PasswordController@showResetForm', 'disable_sidemenu' => true]);
//    Route::post('password/email', ['as' => 'password.reset.post', 'uses' => 'Auth\PasswordController@postReset', 'disable_sidemenu' => true]);

//  Route::get('home', function (){
//      redirect('/');
//  });

        //Check this storeScreenWidth route

        Route::get('home/storeScreenWidth', ['as' => 'home.storescreenwidth', 'uses' => 'HomeController@storeScreenWidth', 'disable_sidemenu' => true]);

        Route::get('auth/weibo', ['as' => 'weibo.login', 'uses' => 'AuthController@weiboLogin']);
        Route::get('auth/weibo/callback', ['as' => 'weibo.login.callback', 'uses' => 'AuthController@weiboCallback', 'disable_sidemenu' => true]);

//    Route::get('auth/weixin', ['as' => 'weixin.login', 'uses' => 'AuthController@weixinLogin']);
//    Route::get('auth/weixinweb', ['as' => 'weixinweb.login', 'uses' => 'AuthController@weixinwebLogin']);
//    Route::get('auth/weixin/callback', ['as' => 'weixin.login.callback', 'uses' => 'AuthController@weixinCallback', 'disable_sidemenu' => true]);
        Route::get('auth/weixinweb/callback', ['as' => 'weixinweb.login.callback', 'uses' => 'AuthController@weixinwebCallback', 'disable_sidemenu' => true]);

        //facebook login
        Route::get('auth/facebook', ['as' => 'facebook.login', 'uses' => 'AuthController@facebookLogin']);
        // check this callback route
        Route::get('callback', ['as' => 'facebook.login.callback', 'uses' => 'AuthController@handleProviderCallback']);
        Route::post('updatePassword', ['as' => 'facebook.login.callback', 'uses' => 'AuthController@updatePassword']);

        Route::get('auth/qq', ['as' => 'qq.login', 'uses' => 'AuthController@qqLogin']);
        Route::get('auth/qq/callback', ['as' => 'qq.login.callback', 'uses' => 'AuthController@qqCallback', 'disable_sidemenu' => true]);

        //check this route
        Route::get('{lang?}/resume/getDetails/{id}', ['as' => 'frontend.resume.getdetails', 'uses' => 'ResumeController@getDetails', 'disable_sidemenu' => true]);

        //Find expert
        Route::get('/findExpertsNew', ['as' => 'frontend.findexpertsNew', 'uses' => 'FindExpertsController@findExpertsNew', 'disable_sidemenu' => true]);
//      Route::get('/findExperts', ['as' => 'frontend.findexperts', 'uses' => 'FindExpertsController@index', 'disable_sidemenu' => true]);
        Route::get('/{lang?}/Coworkers', ['as' => 'frontend.findexperts', 'uses' => 'FindExpertsController@index', 'disable_sidemenu' => true]);
        Route::post('/findExperts', ['as' => 'frontend.findexperts.post', 'uses' => 'FindExpertsController@indexPost', 'disable_sidemenu' => true]);
        Route::post('/findExperts/sendRequestForContactInfo', ['as' => 'frontend.findexperts.sendrequestforcontactinfo', 'uses' => 'FindExpertsController@sendRequestForContactInfo', 'disable_sidemenu' => true]);
        Route::get('/findExperts/inviteToWork/{user_id}/{invitation_project_id}', ['as' => 'frontend.findexperts.invitetowork', 'uses' => 'FindExpertsController@inviteToWork', 'disable_sidemenu' => true]);
        Route::post('/findExperts/inviteToWork', ['as' => 'frontend.findexperts.invitetowork.post', 'uses' => 'FindExpertsController@inviteToWorkPost', 'disable_sidemenu' => true]);
        Route::get('/findExperts/sendOffer/{user_id}', ['as' => 'frontend.findexperts.sendoffer', 'uses' => 'FindExpertsController@sendOffer', 'disable_sidemenu' => true]);
        Route::post('/findExperts/sendOffer', ['as' => 'frontend.findexperts.sendoffer.post', 'uses' => 'FindExpertsController@sendOfferPost', 'disable_sidemenu' => true]);

        //Job
        Route::get('{lang?}/remote-jobs-online', ['as' => 'frontend.jobs.index', 'uses' => 'JobsController@index', 'disable_sidemenu' => true]);
        // check this oldJobs route
        Route::get('/oldJobs', ['as' => 'frontend.oldJobs.index', 'uses' => 'JobsController@oldIndex', 'disable_sidemenu' => true]);
        //TODO CHECK jobs/delete ROUTE
        Route::get('/remote-jobs-online/delete', ['as' => 'frontend.jobs.delete', 'uses' => 'JobsController@delete']);
        Route::post('/remote-jobs-online/makeOffer', ['as' => 'frontend.jobs.makeoffer.post', 'uses' => 'JobsController@makeOfferPost']);
        Route::post('/remote-jobs-online/getCountries', ['as' => 'frontend.jobs.getcountries', 'uses' => 'JobsController@getCountries']);
        Route::post('/remote-jobs-online/getLanguages', ['as' => 'frontend.jobs.getlanguage', 'uses' => 'JobsController@getLanguages']);
        Route::post('/remote-jobs-online/getHourlyRanges', ['as' => 'frontend.jobs.gethourlyranges', 'uses' => 'JobsController@getHourlyRanges']);

        //offical projects urls
        Route::get('{lang?}/company-official-projects', ['as' => 'frontend.jobs.official', 'uses' => 'JobsController@officialProjects', 'disable_sidemenu' => true]);


        //Download
        Route::get('/download', ['as' => 'frontend.download', 'uses' => 'DownloadController@download']);
        //TODO CHECK ALL DOWNLOAD ROUTE
        Route::get('/downloadDocuments', ['as' => 'frontend.downloaddocuments', 'uses' => 'DownloadController@downloadDocuments']);
        Route::get('/downloadProjectDocuments', ['as' => 'frontend.downloadprojectdocuments', 'uses' => 'DownloadController@downloadProjectDocuments']);
        Route::get('/downloadCSV/{table}', ['as' => 'frontend.downloadcsv', 'uses' => 'DownloadController@downloadCSV']);

        //One Time Script (remove after running or replace)
        Route::get('/oneTimeScript', ['as' => 'frontend.onetimescript', 'uses' => 'HomeController@oneTimeScript']);

        // for design
        Route::get('/skillVerification3', ['as' => 'frontend.skillVerification3', 'uses' => 'HomeController@skillVerification3']);
//    //post image
//    Route::get('/upload', ['as' => 'frontend.upload', 'uses' => 'ImageController@getUpload', 'disable_sidemenu' => true]);
//    Route::get('/upload4', ['as' => 'frontend.upload', 'uses' => 'ImageController@getUpload4', 'disable_sidemenu' => true]);
//    Route::post('/upload', ['as' => 'frontend.upload.post', 'uses' =>'ImageController@postUpload']);
//    Route::post('/upload/delete', ['as' => 'frontend.upload.remove', 'uses' =>'ImageController@deleteUpload']);

        //post file
        Route::get('/uploadFile', ['as' => 'frontend.uploadfile', 'uses' => 'FileUploadController@getUpload', 'disable_sidemenu' => true]);
        Route::post('/uploadFile', ['as' => 'frontend.uploadfile.post', 'uses' => 'FileUploadController@postUpload']);
        Route::post('/uploadFile/delete', ['as' => 'frontend.uploadfile.remove', 'uses' => 'FileUploadController@deleteUpload']);
        Route::post('/uploadFile/submit', ['as' => 'frontend.uploadfile.submit', 'uses' => 'FileUploadController@submitForm']);

        //cron job
        Route::get('/deleteInactiveCommonProject', array('as' => 'deleteinactivecommonproject', 'uses' => 'CronJobController@deleteInactiveCommonProject'));
        Route::get('/houseKeeping', array('as' => 'housekeeping', 'uses' => 'CronJobController@houseKeeping'));
        Route::get('/deleteObsoleteViewContactLogs', array('as' => 'deleteobsoleteviewcontactlogs', 'uses' => 'CronJobController@deleteObsoleteViewContactLogs'));

        //crop image
//  Route::get('/testPage',  ['as' => 'frontend.testpage', 'uses' => 'ImageController@testPage', 'disable_sidemenu' => true]);
        Route::get('/modal/cropImage/{type}/{id}', ['as' => 'frontend.modal.cropImage', 'uses' => 'ImageController@modalCropImage']);
        Route::post('/modal/cropImage', ['as' => 'frontend.modal.cropImage.post', 'uses' => 'ImageController@modalCropImagePost']);

        //identity authentication
        Route::get('/uploadIDCard/{key}', ['as' => 'frontend.uploadidcard', 'uses' => 'ImageController@uploadIdCard', 'disable_sidemenu' => true]);

        //Frontend logged in route
        Route::group(array('namespace' => 'Frontend', 'middleware' => ['auth:users']), function () {
            //Resume
            Route::get('{lang?}/resume/', ['as' => 'frontend.resume', 'uses' => 'UserCenterController@index']);

            Route::get('/currency', 'UserCenterController@getCurrency');
            Route::get('{lang?}/myAccount', ['as' => 'frontend.myaccount', 'uses' => 'MyAccountController@index']);
            Route::post('/myAccount/create', ['as' => 'frontend.myaccount.create', 'uses' => 'MyAccountController@create']);
            Route::post('/myAccount/delete/{id}', ['as' => 'frontend.myaccount.delete', 'uses' => 'MyAccountController@delete']);

            Route::post('userRefer', ['as' => 'refer.post', 'uses' => 'InviteController@userReferPost']);
            Route::get('mail-invitation', ['as' => 'refer.maininvitation', 'uses' => 'InviteController@mailInvitations']);

            //Identity Authentication
            Route::get('{lang?}/identityAuthentication', ['as' => 'frontend.identityauthentication', 'uses' => 'IdentityAuthenticationController@index']);
            Route::post('/identityAuthentication/create', ['as' => 'frontend.identityauthentication.create', 'uses' => 'IdentityAuthenticationController@create']);
            Route::get('/identityAuthentication/delete', ['as' => 'frontend.identityauthentication.delete', 'uses' => 'IdentityAuthenticationController@delete']);
            Route::post('/identityAuthentication/change', ['as' => 'frontend.identityauthentication.change', 'uses' => 'IdentityAuthenticationController@change']);

            //Invite
            Route::get('{lang?}/invite', ['as' => 'frontend.invite', 'uses' => 'InviteController@index']);

            //Skill Verification
            Route::get('{lang?}/skillVerification', ['as' => 'frontend.skillverification', 'uses' => 'SkillVerificationController@index']);
            Route::post('/skillVerification/update', ['as' => 'frontend.skillverification.update', 'uses' => 'SkillVerificationController@update']);

            //My Receive Order
            Route::get('{lang?}/myReceiveOrder', ['as' => 'frontend.myreceiveorder', 'uses' => 'MyReceiveOrderController@index']);
            //TODO CHECK THIS ROUTE
            Route::get('/myReceiveOrder/removeOffer/{project_id}/{user_id}', ['as' => 'frontend.myreceiveorder.removeoffer', 'uses' => 'MyReceiveOrderController@removeOffer']);
            Route::get('/myReceiveOrder/jobdetail/{id}/user/{userId}', ['as' => 'frontend.myreceiveorder.jobdetail', 'uses' => 'JoinDiscussionController@jobDetail']);

            //CommonProjects

            Route::get('/commonHourlyProjects/myReceiveOrder/jobdetail/{id}/user/{userId}', ['as' => 'frontend.commonHourlyProject.jobdetail', 'uses' => 'JoinDiscussionController@jobDetail']);


            //My Publish Order
            //      Route::get('/myPublishOrdernew', ['as' => 'frontend.mypublishorder', 'uses' => 'MyPublishOrderController@NEWindex']);
            Route::get('{lang?}/myPublishOrder', ['as' => 'frontend.mypublishorder', 'uses' => 'MyPublishOrderController@index']);
            Route::get('/myPublishOrder/deleteProject/{id}', ['as' => 'frontend.mypublishorder.deleteproject', 'uses' => 'MyPublishOrderController@deleteProject']);
            //Route::get('/myPublishOrderDetails/{id}', ['as' => 'frontend.mypublishorderdetails', 'uses' => 'MyPublishOrderDetailsController@index']);
            Route::post('/myPublishOrderDetails/createExtraSize', ['as' => 'frontend.mypublishorderdetails.createextrasize', 'uses' => 'MyPublishOrderDetailsController@create']);
            Route::get('/myPublishOrder/editProject/{id}', ['as' => 'frontend.mypublishorder.editproject', 'uses' => 'MyPublishOrderController@editProject', 'disable_sidemenu' => true]);
            Route::get('/myPublishOrder/orderDetail/{id}/user/{userId}', ['as' => 'frontend.mypublishorder.orderDetail', 'uses' => 'MyPublishOrderController@orderDetail', 'disable_sidemenu' => true]);
            Route::post('/myPublishOrder/postEditProject', ['as' => 'frontend.mypublishorder.posteditproject', 'uses' => 'MyPublishOrderController@postEditProject']);

            // Progressive Projects
            //      Route::get('/myProgressiveProjects', ['as' => 'frontend.myprogressiveprojects', 'uses' => 'MyProgressiveProjectController@index']);


            Route::post('/giveBonus/{projectId}', [
                'as' => 'frontend.mypublishorder.createMilestones',
                'uses' => 'ProjectOrderController@giveBonus'
            ]);
            Route::post('/giveBonusP/{projectId}', [
                'as' => 'frontend.mypublishorder.createMilestones',
                'uses' => 'ProjectOrderController@giveBonusP'
            ]);
            Route::post('/createMilestone/{projectId}', [
                'as' => 'frontend.mypublishorder.createMilestones',
                'uses' => 'ProjectOrderController@createMilestone'
            ]);
            Route::post('/confirmMilestone/{projectId}', [
                'as' => 'frontend.mypublishorder.confirmMilestone',
                'uses' => 'ProjectOrderController@confirmMilestone'
            ]);
            Route::post('/updateTransaction/{transactionId}', [
                'as' => 'frontend.mypublishorder.updateTransactions',
                'uses' => 'ProjectOrderController@updateTransaction'
            ]);
            Route::post('/acceptMilestone/{projectId}', [
                'as' => 'frontend.mypublishorder.acceptMilestones',
                'uses' => 'ProjectOrderController@acceptMilestone'
            ]);
            Route::post('/rejectMilestone/{projectId}', [
                'as' => 'frontend.mypublishorder.rejectMilestones',
                'uses' => 'ProjectOrderController@rejectMilestone'
            ]);
            Route::post('/changeStatus/{projectId}', [
                'as' => 'frontend.mypublishorder.changeStatus',
                'uses' => 'ProjectOrderController@changeStatus'
            ]);
            Route::post('/createDispute/{projectId}', [
                'as' => 'frontend.mypublishorder.createDispute',
                'uses' => 'ProjectDisputeController@createDispute'
            ]);
            Route::post('/refundDispute/{projectId}', [
                'as' => 'frontend.myreceiveorder.refundDispute',
                'uses' => 'ProjectDisputeController@refundDispute'
            ]);
            Route::post('/rejectRefundRequest/{milestoneId}', [
                'as' => 'frontend.myreceiveorder.refundDispute',
                'uses' => 'ProjectDisputeController@rejectRefundDispute'
            ]);
            Route::post('/createRefund/{projectId}', [
                'as' => 'frontend.myreceiveorder.createRefund',
                'uses' => 'ProjectOrderController@createRefund'
            ]);
            Route::post('/endContract/postMessage/{projectId}', [
                'as' => 'frontend.mypublishorder.endContract',
                'uses' => 'ProjectDisputeController@endContract'
            ]);
            Route::post('/dailyUpdate/{projectId}', [
                'as' => 'frontend.mypublishorder.dailyUpdate',
                'uses' => 'ProjectDisputeController@dailyUpdate'
            ]);
            Route::post('updateDisputeStatus', [
                'as' => 'frontend.mypublishorder.dailyUpdate',
                'uses' => 'ProjectDisputeController@updateDisputeStatus'
            ]);
            Route::post('switchFixed/{projectId}', [
                'as' => 'frontend.mypublishorder.dailyUpdate',
                'uses' => 'ProjectController@switchFixed'
            ]);

            /**
             * File Dispute Detail routes
             */

            Route::post('fileDisputeToAdmin', [
                'as' => 'frontend.myreceiveorder.fileDispute',
                'uses' => 'ProjectDisputeController@fileDispute'
            ]);

            Route::post('cancelDisputeFreelancer', [
                'as' => 'frontend.myreceiveorder.cancelDisputeFreelancer',
                'uses' => 'ProjectDisputeController@cancelDisputeFreelancer'
            ]);

            Route::post('fileDisputeToAdminEmployer', [
                'as' => 'frontend.myreceiveorder.fileDisputeToAdminEmployer',
                'uses' => 'ProjectDisputeController@fileDisputeEmployer'
            ]);
            //TODO CHECK THIS ROUTE
            Route::get('/project/disputeDetail/{disputeId}', [
                'as' => 'frontend.fileDisputeDetail',
                'uses' => 'ProjectDisputeController@detail',
                'disable_sidemenu' => true,
                'xenren_path' => true,
            ]);

            Route::post('/update-dispute-comment', [
                'as' => 'frontend.fileDisputeDetail',
                'uses' => 'ProjectDisputeController@updateDisputeComment',
                'disable_sidemenu' => true,
                'xenren_path' => true,
            ]);

            Route::post('/update-dispute-attachment', [
                'as' => 'frontend.fileDisputeDetail',
                'uses' => 'ProjectDisputeController@updateDisputeAttachment',
                'disable_sidemenu' => true,
                'xenren_path' => true,
            ]);

            /************** END OF THE File Dispute Detail routes **************/

            // Route::get('/tailorOrders', ['as' => 'frontend.tailororders', 'uses' => 'TailorOrdersController@index', 'disable_sidemenu' => true]);
            // Route::any('/createOrder', ['as' => 'frontend.createorder', 'uses' => 'CreateOrderController@index', 'disable_sidemenu' => true]);
            // Route::get('/deleteOrder/{id}', ['as' => 'frontend.deleteorder', 'uses' => 'CreateOrderController@delete', 'disable_sidemenu' => true]);
            // Route::any('/editOrder/{id}', ['as' => 'frontend.editorder', 'uses' => 'CreateOrderController@editOrder', 'disable_sidemenu' => true]);
            // Route::post('/saveOrder/{orderstep}', ['as' => 'frontend.saveorder', 'uses' => 'CreateOrderController@saveOrder']);

            //personal information
            Route::get('{lang?}/personalInformation', ['as' => 'frontend.personalinformation', 'uses' => 'PersonalInformationController@index']);
            Route::post('/personalInformation/delete/{language_id}', ['as' => 'frontend.personalinformation.deleteLanguage', 'uses' => 'PersonalInformationController@deleteLanguage']);
            Route::post('/personalInformation/addStatus', ['as' => 'frontend.personalinformation.addstatus', 'uses' => 'PersonalInformationController@addStatus']);
            Route::post('/personalInformation/addskill', ['as' => 'frontend.personalinformation.addskill', 'uses' => 'UserCenterController@changeSkill']);
            Route::post('/personalInformation/change', ['as' => 'frontend.personalinformation.change', 'uses' => 'PersonalInformationController@change']);
            Route::post('/personalInformation/update', ['as' => 'frontend.personalinformation.update', 'uses' => 'PersonalInformationController@update']);
            Route::post('/personalInformation/updateAvatar', ['as' => 'frontend.personalinformation.updateavatar', 'uses' => 'PersonalInformationController@updateAvatar']);
            Route::post('/personalInformation/updateContactView', ['as' => 'frontend.personalinformation.updatecontactview', 'uses' => 'PersonalInformationController@updateContactView']);
            Route::post('/personalInformation/updateAvatar', ['as' => 'frontend.personalinformation.updateavatar', 'uses' => 'PersonalInformationController@updateAvatar']);
            Route::post('/personalInformation/sendEditEmailVerification', ['as' => 'frontend.personalinformation.sendeditemailverification', 'uses' => 'PersonalInformationController@sendEditEmailVerification']);
            Route::post('/personalInformation/sendEditPasswordVerification', ['as' => 'frontend.personalinformation.sendeditpasswordverification', 'uses' => 'PersonalInformationController@sendEditPasswordVerification']);
            Route::get('/personalInformations/shared-office/get', ['as' => 'frontend.personalinformation.sharedoffice.get', 'uses' => 'PersonalInformationController@getSharedOffice']);

            //change email
            Route::get('/changeEmail', ['as' => 'frontend.changeemail', 'uses' => 'PersonalInformationController@changeEmail']);
            Route::post('/changeEmail', ['as' => 'frontend.changeemail.post', 'uses' => 'PersonalInformationController@changeEmailPost']);

            //change password
            Route::get('/changePassword', ['as' => 'frontend.changepassword', 'uses' => 'PersonalInformationController@changePassword']);
            Route::post('/changePassword', ['as' => 'frontend.changepassword.post', 'uses' => 'PersonalInformationController@changePasswordPost']);

            Route::get('{lang?}/userCenter', ['as' => 'frontend.usercenter', 'uses' => 'UserCenterController@index']);
            Route::post('/userCenter/closeNotice', ['as' => 'frontend.usercenter.closenotice', 'uses' => 'UserCenterController@closeNotice']);
            Route::post('job-position/userCenter/{id}', 'UserCenterController@updatePositions');

            // For New Design
//      Route::get('/newUserCenter', ['as' => 'frontend.newUsercenter', 'uses' => 'UserCenterController@newIndex']);
//      Route::get('/userCenterHover', ['as' => 'frontend.userCenterHover', 'uses' => 'UserCenterController@userCenterHover']);

            //Inbox
            Route::get('{lang?}/inbox', ['as' => 'frontend.inbox', 'uses' => 'InboxController@index']);
            Route::post('/inbox/acceptJob', ['as' => 'frontend.inbox.acceptjob', 'uses' => 'InboxController@acceptJob']);
            Route::post('/inbox/rejectJob', ['as' => 'frontend.inbox.rejectjob', 'uses' => 'InboxController@rejectJob']);
            Route::post('/inbox/acceptRequestForContactInfo', ['as' => 'frontend.inbox.acceptrequestforcontactinfo', 'uses' => 'InboxController@acceptRequestForContactInfo']);
            Route::post('/inbox/rejectRequestForContactInfo', ['as' => 'frontend.inbox.rejectrequestforcontactinfo', 'uses' => 'InboxController@rejectRequestForContactInfo']);
            Route::post('/inbox/rejectInvite', ['as' => 'frontend.inbox.rejectinvite', 'uses' => 'InboxController@rejectInvite']);
            Route::post('/inbox/acceptReceiveOffer', ['as' => 'frontend.inbox.acceptreceiveoffer', 'uses' => 'InboxController@acceptReceiveOffer']);
            Route::post('/inbox/rejectReceiveOffer', ['as' => 'frontend.inbox.rejectrecieveoffer', 'uses' => 'InboxController@rejectSendOffer']);
            Route::post('/inbox/acceptSendOffer', ['as' => 'frontend.inbox.acceptsendoffer', 'uses' => 'InboxController@acceptSendOffer']);
            Route::post('/inbox/rejectSendOffer', ['as' => 'frontend.inbox.rejectsendoffer', 'uses' => 'InboxController@rejectSendOffer']);
            Route::post('/readinbox', ['as' => 'frontend.readinbox', 'uses' => 'InboxController@readInbox']);
//        Route::get('/systemMessage', ['as' => 'frontend.systemmessage', 'uses' => 'SystemMessageController@index']);
//        Route::get('/trash', ['as' => 'frontend.trash', 'uses' => 'TrashController@index']);
            Route::get('/inbox/messages/{inbox_id}', ['as' => 'frontend.inbox.messages', 'uses' => 'InboxController@messages']);
            Route::post('/inbox/trash', ['as' => 'frontend.inbox.trash', 'uses' => 'InboxController@trash']);
            Route::post('/inbox/untrash', ['as' => 'frontend.inbox.untrash', 'uses' => 'InboxController@untrash']);
            Route::post('/inbox/trashAll', ['as' => 'frontend.inbox.trashall', 'uses' => 'InboxController@trashAll']);
            Route::post('/inbox/untrashAll', ['as' => 'frontend.inbox.untrashall', 'uses' => 'InboxController@untrashAll']);
            Route::post('/inbox/markasread', ['as' => 'frontend.inbox.markasread', 'uses' => 'InboxController@markAsRead']);
            Route::post('/inbox/removeall', ['as' => 'frontend.inbox.removeall', 'uses' => 'InboxController@removeAll']);
            Route::post('/inbox/sendmessage', ['as' => 'frontend.inbox.sendmessage', 'uses' => 'InboxController@sendMessage']);
            Route::get('/inbox/chatImage/{inbox_id}/{receiver_id}', ['as' => 'frontend.inbox.chatimage', 'uses' => 'InboxController@chatImage']);
            Route::post('/inbox/chatImage', ['as' => 'frontend.inbox.chatimage.post', 'uses' => 'InboxController@chatImagePost']);
            Route::post('/inbox/getNewInboxMessage', ['as' => 'frontend.inbox.getnewinboxmessage.post', 'uses' => 'InboxController@getNewInboxMessagePost']);

            //Inbox Only For Design
//        Route::get('/chatDetails', ['as' => 'frontend.chatDetails', 'uses' => 'InboxController@chatDetails']);
//        Route::get('/chatDetails/sendInvitation', ['as' => 'frontend.chatDetails.sendInvitation', 'uses' => 'InboxController@chatDetailsSendInvitation']);
//        Route::get('/chatDetails/sendOffer', ['as' => 'frontend.chatDetails.sendOffer', 'uses' => 'InboxController@chatDetailsSendOffer']);
//        Route::get('/chatDetails/requestForContactA', ['as' => 'frontend.chatDetails.requestForContactA', 'uses' => 'InboxController@requestForContactA']);
//        Route::get('/chatDetails/requestForContact', ['as' => 'frontend.chatDetails.requestForContact', 'uses' => 'InboxController@requestForContact']);

            //Project
            Route::get('{lang?}/postProject', ['as' => 'frontend.postproject', 'uses' => 'PostProjectController@index', 'disable_sidemenu' => true]);
            //TODO CHECK THIS ROUTE
            Route::get('/checkPersonalInfo', ['as' => 'frontend.checkPersonalInfo', 'uses' => 'PostProjectController@checkPersonalInfo']);
            Route::post('/postProject/create', ['as' => 'frontend.postproject.create', 'uses' => 'PostProjectController@create']);
            Route::get('/postProject/getSkill', ['as' => 'frontend.postproject.getskill', 'uses' => 'PostProjectController@getSkill', 'disable_sidemenu' => true]);
            Route::post('/getprojectinfo', ['as' => 'frontend.getprojectinfo', 'uses' => 'PostProjectController@getProjectInfo']);
            Route::post('/postProject/addNewSkillKeyword', ['as' => 'frontend.postproject.addNewSkillKeyword', 'uses' => 'PostProjectController@addNewSkillKeyword']);
            Route::post('/postProject/getInputFile', ['as' => 'frontend.postproject.getinputfile', 'uses' => 'PostProjectController@getinputfile']);
            Route::post('/postProject/uploadDescriptionImage', ['as' => 'frontend.postproject.uploaddescriptionimage', 'uses' => 'PostProjectController@uploadDescriptionImage']);
            // Route::get('/postProject/{project_id}', ['as' => 'frontend.postproject.edit', 'uses' => 'PostProjectController@editPostProject', 'disable_sidemenu' => true]);

            //Join Discussion Common Project
//          Route::get('/joinDiscussion/getProject/{slug}/{id}', ['as' => 'frontend.joindiscussion.getproject', 'uses' => 'JoinDiscussionController@getProject', 'disable_sidemenu' => true]);
            Route::get('/joinDiscussion/chooseCandidate', ['as' => 'frontend.joindiscussion.choosecandidate', 'uses' => 'JoinDiscussionController@chooseCandidate']);
            Route::post('/joinDiscussion/reject/{applicant_id}', ['as' => 'frontend.joindiscussion.reject', 'uses' => 'JoinDiscussionController@reject', 'disable_sidemenu' => true]);
            Route::post('/joinDiscussion/awardJob/{applicant_id}', ['as' => 'frontend.joindiscussion.awardjob', 'uses' => 'JoinDiscussionController@awardJob', 'disable_sidemenu' => true]);
            Route::get('/joinDiscussion/sendmessage/{applicant_id}', ['as' => 'frontend.joindiscussion.sendmessage', 'uses' => 'JoinDiscussionController@sendMessage', 'disable_sidemenu' => true]);
            Route::post('/joinDiscussion/sendmessage/{applicant_id}', ['as' => 'frontend.joindiscussion.sendmessage.post', 'uses' => 'JoinDiscussionController@sendMessagePost', 'disable_sidemenu' => true]);
            Route::get('/joinDiscussion/applicantDetail/{applicant_id}', ['as' => 'frontend.joindiscussion.applicantdetail', 'uses' => 'JoinDiscussionController@applicantDetail', 'disable_sidemenu' => true]);
            Route::post('/joinDiscussion/applicantDetail/{applicant_id}', ['as' => 'frontend.joindiscussion.applicantdetail.post', 'uses' => 'JoinDiscussionController@applicantDetailPost', 'disable_sidemenu' => true]);
            Route::post('/joinDiscussion/thumbup/{chat_follower_id}', ['as' => 'frontend.joindiscussion.thumbup.post', 'uses' => 'JoinDiscussionController@thumbUp', 'disable_sidemenu' => true]);
            Route::post('/joinDiscussion/thumbdown/{chat_follower_id}', ['as' => 'frontend.joindiscussion.thumbdown.post', 'uses' => 'JoinDiscussionController@thumbDown', 'disable_sidemenu' => true]);
            Route::post('/joinDiscussion/quitProjectDiscussion', ['as' => 'frontend.joindiscussion.quitprojectdiscussion.post', 'uses' => 'JoinDiscussionController@quitProjectDiscussion', 'disable_sidemenu' => true]);
            Route::get('/joinDiscussion/getProjectApplicant/{id}/{project_name}', ['as' => 'frontend.joindiscussion.getprojectapplicant', 'uses' => 'JoinDiscussionController@getProjectApplicant', 'disable_sidemenu' => true]);
            Route::post('/joinDiscussion/change', ['as' => 'frontend.joindiscussion.change', 'uses' => 'joinDiscussionController@change']);


            //send private message
            Route::get('/sendPrivateMessage/{user_id}', ['as' => 'frontend.sendprivatemessage', 'uses' => 'ModalController@sendPrivateMessage']);
            Route::post('/sendPrivateMessage/{user_id}', ['as' => 'frontend.sendprivatemessage.post', 'uses' => 'ModalController@sendPrivateMessagePost']);

            //Official Project News
            Route::get('/joinDiscussion/news/{project_id}', ['as' => 'frontend.joindiscussion.news', 'uses' => 'JoinDiscussionController@news', 'disable_sidemenu' => true]);

            //Common Project Chat
            Route::get('/privateChat', ['as' => 'backend.inbox.privateChat', 'uses' => 'InboxController@getPrivateChat']);
            Route::get('/projectChat', ['as' => 'frontend.projectchat', 'uses' => 'ProjectChatController@pullProjectChats']);
            Route::get('/projectChat/pullChat/{id}', ['as' => 'frontend.projectchat.pullchat', 'uses' => 'ProjectChatController@pullChat']);
            Route::get('/projectChat/pullProjectChat/{id}', ['as' => 'frontend.projectchat.pullprojectchat', 'uses' => 'ProjectChatController@pullProjectChat']);
            Route::get('/projectChat/pullProjectChatFile/{id}', ['as' => 'frontend.projectchat.pullprojectchatfile', 'uses' => 'ProjectChatController@pullProjectChatFile']);
            Route::post('/projectChat/postChat', ['as' => 'frontend.projectchat.postchat', 'uses' => 'ProjectChatController@postChat']);
            Route::post('/projectChat/postProjectChat', ['as' => 'frontend.projectchat.postprojectchat', 'uses' => 'ProjectChatController@postProjectChat']);
            Route::post('/projectChat/postProjectChatFile', ['as' => 'frontend.projectchat.postprojectchatfile', 'uses' => 'ProjectChatController@postProjectChatFile']);
            Route::post('/projectChat/postProjectChatImage', ['as' => 'frontend.projectchat.postprojectchatimage', 'uses' => 'ProjectChatController@postProjectChatImage']);
            Route::post('sendMessage', ['as' => 'frontend.projectchat.sendMessage', 'uses' => 'ProjectChatController@sendMessage']);
            Route::post('getLastMessage', ['as' => 'frontend.projectchat.sendMessage', 'uses' => 'ProjectChatController@getLastMessage']);
            Route::post('updateLastMessage', ['as' => 'frontend.projectchat.sendMessage', 'uses' => 'ProjectChatController@updateLastMessage']);
            Route::get('/projectChat/postInviteHelpdeskChat/{id}', ['as' => 'frontend.projectchat.postinvitehelpdeskchat', 'uses' => 'ProjectChatController@postInviteHelpdeskChat']);
            Route::get('/projectChat/checkNew', ['as' => 'frontend.projectchat.check', 'uses' => 'ProjectChatController@checkNewChat']);

            //Map
            Route::get('/map/userLocation/{user_id}', ['as' => 'frontend.map.user', 'uses' => 'MapController@index', 'disable_sidemenu' => true]);
            Route::get('/map/searchByKeyword', ['as' => 'frontend.map.searchbykeyword', 'uses' => 'MapController@searchByKeyword', 'disable_sidemenu' => true]);
            Route::get('/map/searchByAddress', ['as' => 'frontend.map.searchbyaddress', 'uses' => 'MapController@searchByAddress', 'disable_sidemenu' => true]);
            Route::get('/map/officialProjectLocation/{official_project_id}', ['as' => 'frontend.map.officialprojectlocation', 'uses' => 'MapController@officialProjectLocation', 'disable_sidemenu' => true]);

//      Route::get('/getcategory/level2', ['as' => 'frontend.getcategory.leveltwo', 'uses' => 'CreateOrderController@levelTwo', 'disable_sidemenu' => true]);
//        Route::get('/getcategory/level3', ['as' => 'frontend.getcategory.levelthree', 'uses' => 'CreateOrderController@levelThree', 'disable_sidemenu' => true]);
//        Route::get('/orderDetails/{id}', ['as' => 'frontend.order.details', 'uses' => 'OrderController@details']);
//        Route::post('/approveTailor/{id?}', ['as' => 'frontend.applicant.approve', 'uses' => 'OrderController@approveApplicant']);
//        Route::get('/getfiltervalue/{id?}', ['as' => 'frontend.getfiltervalue', 'uses' => 'TailorOrdersController@getFilterValue']);

            Route::get('/checkcategory/level2', ['as' => 'frontend.checkcategory.leveltwo', 'uses' => 'CheckCategoryController@levelTwo', 'disable_sidemenu' => true]);
            Route::get('/checkcategory/level3', ['as' => 'frontend.checkcategory.levelthree', 'uses' => 'CheckCategoryController@levelThree', 'disable_sidemenu' => true]);
            //TODO CHECK This route
            Route::get('/checktailor/{id}', ['as' => 'frontend.checktailor', 'uses' => 'CheckTailorController@index']);

            //Tailor only route
//        Route::post('/order/apply/{id?}', ['as' => 'frontend.order.apply', 'uses' => 'OrderController@applyOrder', 'middleware' => 'tailorOnly']);
//        Route::post('/order/applydelete/{id?}', ['as' => 'frontend.order.applydelete', 'uses' => 'OrderController@deleteApplyOrder', 'middleware' => 'tailorOnly']);

            //End Contract
            //TODO CHECK resume/getDetails/{id} route if that works then this routes works
            Route::get('/endContract/freelancer/{projectApplicant_id}', ['as' => 'frontend.endcontract.freelancer', 'uses' => 'EndContractController@getFreelancerForm', 'disable_sidemenu' => true]);
            Route::post('/endContract/freelancer', ['as' => 'frontend.endcontract.freelancer.post', 'uses' => 'EndContractController@postFreelancerForm', 'disable_sidemenu' => true]);
            Route::get('/endContract/employer/{projectApplicant_id}', ['as' => 'frontend.endcontract.employer', 'uses' => 'EndContractController@getEmployerForm', 'disable_sidemenu' => true]);
            Route::post('/endContract/employer', ['as' => 'frontend.endcontract.employer.post', 'uses' => 'EndContractController@postEmployerForm', 'disable_sidemenu' => true]);

            // Favourite Freelancers
            Route::get('{lang?}/favouriteFreelancers', ['as' => 'frontend.favouriteFreelancers', 'uses' => 'FavouriteFreelancerController@index', 'xenren_path' => true]);
            Route::post('/addFavouriteFreelancer', ['as' => 'frontend.addfavouritefreelancer.post', 'uses' => 'FavouriteFreelancerController@addFavouriteFreelancer', 'disable_sidemenu' => true]);
            Route::post('/removeFavouriteFreelancer', ['as' => 'frontend.removefavouritefreelancer.post', 'uses' => 'FavouriteFreelancerController@removeFavouriteFreelancer', 'disable_sidemenu' => true]);

            // Favourite Jobs
            Route::get('{lang?}/favouriteJobs', ['as' => 'frontend.favouriteJobs', 'uses' => 'FavouriteJobController@index', 'xenren_path' => true]);
            Route::post('/addFavouriteJob', ['as' => 'frontend.addfavouritejob.post', 'uses' => 'FavouriteJobController@addFavouriteJob', 'disable_sidemenu' => true]);
            Route::post('/removeFavouriteJob', ['as' => 'frontend.removefavouritejob.post', 'uses' => 'FavouriteJobController@removeFavouriteJob', 'disable_sidemenu' => true]);

            // Transaction Details
            Route::get('/transactionDetails', ['as' => 'frontend.transactionDetails', 'uses' => 'TransactionDetailsController@index', 'xenren_path' => true]);
            Route::get('{lang?}/transactionHistory', [
                'as' => 'frontend.transactionHistory',
                'uses' => 'TransactionHistoryController@index',
                'xenren_path' => true,
                'disable_sidemenu' => true
            ]);


            Route::get('/download-transaction-csv', [
                'as' => 'frontend.newDesign.success',
                'uses' => 'TransactionHistoryController@downloadTransactionCSV',
            ]);

            // Thumb Down Jobs
            Route::post('/addThumbdownJob', ['as' => 'frontend.addthumbdownjob.post', 'uses' => 'ThumbdownJobController@thumbsDownJob', 'disable_sidemenu' => true]);
            //modal
            Route::get('/modal/makeOffer/{project_id}/{user_id}/{inbox_id}', ['as' => 'frontend.modal.makeoffer', 'uses' => 'FavouriteJobController@makeOffer']);
            Route::post('/modal/makeOffer', ['as' => 'frontend.modal.makeoffer.post', 'uses' => 'FavouriteJobController@makeOfferPost']);
            Route::get('/modal/setUsername/{user_id}', ['as' => 'frontend.modal.setusername', 'uses' => 'ModalController@setUsername']);
            Route::post('/modal/setUsername', ['as' => 'frontend.modal.setusername.post', 'uses' => 'ModalController@setUsernamePost']);
            Route::get('/modal/setContactInfo/{user_id}', ['as' => 'frontend.modal.setcontactinfo', 'uses' => 'ModalController@setContactInfo']);
            Route::post('/modal/setContactInfo', ['as' => 'frontend.modal.setcontactinfo.post', 'uses' => 'ModalController@setContactInfoPost']);
            //chat
//        Route::get('/myChat', ['as' => 'frontend.mychat', 'uses' => 'MyChatController@index']);
//        Route::get('/myChat/pullChat/{id}', ['as' => 'frontend.mychat.pullchat', 'uses' => 'MyChatController@pullChat']);
//        Route::get('/myChat/pullOrderChat/{id}', ['as' => 'frontend.mychat.pullorderchat', 'uses' => 'MyChatController@pullOrderChat']);
//        Route::get('/myChat/pullOrderChatFile/{id}', ['as' => 'frontend.mychat.pullorderchatfile', 'uses' => 'MyChatController@pullOrderChatFile']);
//        Route::post('/myChat/postChat', ['as' => 'frontend.mychat.postchat', 'uses' => 'MyChatController@postChat']);
//        Route::post('/myChat/postOrderChat', ['as' => 'frontend.mychat.postorderchat', 'uses' => 'MyChatController@postOrderChat']);
//        Route::post('/myChat/postOrderChatFile', ['as' => 'frontend.mychat.postorderchatfile', 'uses' => 'MyChatController@postOrderChatFile']);
//        Route::post('/myChat/postOrderChatImage', ['as' => 'frontend.mychat.postorderchatimage', 'uses' => 'MyChatController@postOrderChatImage']);
//        Route::get('/myChat/postInviteHelpdeskChat/{id}', ['as' => 'frontend.mychat.postinvitehelpdeskchat', 'uses' => 'MyChatController@postInviteHelpdeskChat']);
//        Route::get('/myChat/checkNew', ['as' => 'frontend.mychat.check', 'uses' => 'MyChatController@checkNewChat']);

//        //order detail
//

            // Feedback
            Route::get('{lang?}/feedback', ['as' => 'frontend.feedback', 'uses' => 'FeedbackController@index']);
            Route::get('/feedback/create', ['as' => 'frontend.feedback.create', 'uses' => 'FeedbackController@create']);
            Route::post('/feedback/create', ['as' => 'frontend.feedback.create.post', 'uses' => 'FeedbackController@createPost']);
            Route::post('/feedback/delete', ['as' => 'frontend.feedback.delete', 'uses' => 'FeedbackController@delete']);
            Route::post('/feedback/waitAdminReply', ['as' => 'frontend.feedback.waitadminreply', 'uses' => 'FeedbackController@waitAdminReply']);

            //Chat (Feedback)
            Route::post('/chat/postChat', ['as' => 'frontend.chat.postchat', 'uses' => 'ChatController@postChat']);
            Route::get('/chat/chatImage/{chat_room_id}', ['as' => 'frontend.chat.chatimage', 'uses' => 'ChatController@chatImage']);
            Route::post('/chat/chatImage', ['as' => 'frontend.chat.chatimage.post', 'uses' => 'ChatController@chatImagePost']);
            Route::get('/chat/pullChat/{id}', ['as' => 'frontend.chat.pullchat', 'uses' => 'ChatController@pullChat']);

            //Portfolio
            Route::get('/portfolio', ['as' => 'frontend.portfolio', 'uses' => 'PortfolioController@index']);
            Route::post('/portfolio/getThumbnail', ['as' => 'frontend.portfolio.getthumbnail', 'uses' => 'PortfolioController@getThumbnail']);
            Route::post('/portfolio/create', ['as' => 'frontend.portfolio.create.post', 'uses' => 'PortfolioController@createPost']);
            Route::post('/portfolio/delete', ['as' => 'frontend.portfolio.delete', 'uses' => 'PortfolioController@delete']);

            //Setting
            Route::get('{lang?}/setting', ['as' => 'frontend.setting', 'uses' => 'SettingController@index']);
            Route::post('/data/{id}', ['as' => 'frontend.setting.post', 'uses' => 'SettingController@settingPost']);

            Route::get('/mob-dev', function () {
                return view("frontend.mobDev.index");
            });
            Route::get('/get-users', 'UserCenterController@getUsers');

        });

        Route::get('{lang?}/joinDiscussion/getProject/{slug}/{id}', ['as' => 'frontend.joindiscussion.getproject', 'uses' => 'Frontend\JoinDiscussionController@getProject', 'disable_sidemenu' => true]);
        Route::post('/submit-hiring-form', 'HiringController@store');

        Route::get('/{lang?}', ['as' => 'home', 'uses' => 'HomeController@xenrenLanding', 'xenren_path' => true]);
    });

});
//  Route::post('/user/contact/privacy/{data}', 'Frontend\ModalController@userContactQQ');
Route::post('/user/contact/privacy/{data}', 'Frontend\ModalController@userContactline');
Route::post('/user/contact/privacy/wechat/{data}', 'Frontend\ModalController@userContactWeChat');
Route::post('/user/contact/privacy/skype/{data}', 'Frontend\ModalController@userContactSkype');
Route::post('/user/contact/privacy/phone/{data}', 'Frontend\ModalController@userContactPhoneii');

Route::post('save/user/currency', 'Frontend\UserCurrencyController@saveUserCurrency');

Route::get('user/get/contact/privacy', 'Frontend\ModalController@getContacts');
Route::post('/shared-office-list-space-request','Frontend\SharedOfficeController@sharedOfficeRequest');
Route::get('/office/creator/email-verification/{requestId}','Frontend\SharedOfficeController@verifyCreatorEmail');
Route::get('/get-shared-office-rating/{office_id}','Frontend\SharedOfficeController@getSharedOfficeRating');
Route::post('/book/shared/office/send/email', 'Services\SharedOfficeController@booking');
Route::post('shared-office-reserve','Services\SharedOfficeController@sharedOfficeReserve');
Route::get("test_spinning", [
    "uses" => "AuthController@test_spinning"
]);
Route::get('/get-users','Frontend\UserCenterController@getUsers');
Route::get('/xenren-search-freelancer',[
    'uses'  =>  'HomeController@xenrenSearchFreelancers',
    'as' => 'xenrenSearchFreelancers.index',
    'xenren_path' => true
]);

Route::get('/get-experts',[
    'uses' => 'HomeController@getExperts',
    'xenren_path' => true,
]);
Route::get('/get-experts?&name=',[
    'uses' => 'HomeController@getExperts',
    'xenren_path' => true,
]);

Route::get('/{lang}/mark-all-read',[
    'uses' => 'HomeController@markAllRead',
    'as' => 'markAllRead',
    'xenren_path' => true,
]);
// });


Route::get('get-map',[
    'as' => 'getMap',
    'uses' => 'MapController@index',
]);

Route::get('/iframe/map',[
    'as' => 'iframeMap',
    'uses' => 'MapController@iframe',
    'xenren_path' => true,
]);


Route::get('{lang?}/blog',[
    'as' => 'frontend.Blog',
    'uses' => 'Frontend\BlogController@index',
    'disable_sidemenu' => true,
]);

Route::get('{lang?}/community/home',[
    'as' => 'frontend.Community',
    'uses' => 'Frontend\CommunityController@index',
    'disable_sidemenu' => true,
]);

Route::get('{lang?}/community/Xenren-Offical/{topic_name}/{topic_id}',[
    'as' => 'frontend.XenrenOffical',
    'uses' => 'Frontend\CommunityController@xenrenOffical',
    'disable_sidemenu' => true,
]);

Route::get('{lang?}/community/Community-Discussion/{topic_name}/{topic_id}',[
    'as' => 'frontend.CommunityDiscussion',
    'uses' => 'Frontend\CommunityController@communityDiscussion',
    'disable_sidemenu' => true,
]);


Route::get('{lang?}/forums/{topic_name}',[
    'as' => 'frontend.CommunityForums',
    'uses' => 'Frontend\CommunityController@communityForums',
    'disable_sidemenu' => true,
]);

Route::get('{lang?}/forums/reply/{topic_name}/t-id{t_id}/d-id/{d_id}/type/{type}/reply_id/{r_id}',[
    'as' => 'frontend.CommunityReplyForums',
    'uses' => 'Frontend\CommunityController@communityReplyForums',
    'disable_sidemenu' => true,
]);

Route::post('reply/to/message',[
    'as' => 'frontend.submitAnswer',
    'uses' => 'Frontend\CommunityController@submitAnswer',
    'disable_sidemenu' => true,
]);

Route::get('{lang?}/community/{topic_type}/{topic_name}/t-id/{t_id}/d-id/{d_id}',[
    'as' => 'frontend.CommunityReply',
    'uses' => 'Frontend\CommunityController@communityReply',
    'disable_sidemenu' => true,
]);

Route::post('/submit/topics',[
    'as' => 'frontend.submitQuestions',
    'uses' => 'Frontend\CommunityController@submitQuestions',
    'disable_sidemenu' => true,
]);

Route::get('/submit/like/{d_id}',[
    'as' => 'frontend.submitLikes',
    'uses' => 'Frontend\CommunityController@submitLikes',
    'disable_sidemenu' => true,
]);

Route::get('/submit/topic/like/{topic_id}',[
    'as' => 'frontend.topicLikes',
    'uses' => 'Frontend\CommunityController@topicLikes',
    'disable_sidemenu' => true,
]);


Route::get('/feature/{topic_id}/{type}',[
    'as' => 'frontend.feature',
    'uses' => 'Frontend\CommunityController@feature',
    'disable_sidemenu' => true,
]);

Route::get('/reply-feature-book-mark/{topic_id}/{type}',[
    'as' => 'frontend.replyFeatureS',
    'uses' => 'Frontend\CommunityController@replyFeatureS',
    'disable_sidemenu' => true,
]);

Route::get('{lang?}/email-to-friend/{id}',[
    'as' => 'frontend.emailToFriend',
    'uses' => 'Frontend\CommunityController@emailToFriend',
    'disable_sidemenu' => true,
]);

Route::get('{lang?}/report-topic/{id}',[
    'as' => 'frontend.reportCommunityMessage',
    'uses' => 'Frontend\CommunityController@reportCommunityMessage',
    'disable_sidemenu' => true,
]);

Route::get('{lang?}/report-topic-replay/{id}',[
    'as' => 'frontend.reportCommunityReplyMessage',
    'uses' => 'Frontend\CommunityController@reportCommunityReplyMessage',
    'disable_sidemenu' => true,
]);

Route::post('/report-admin',[
    'as' => 'frontend.reportToAdmin',
    'uses' => 'Frontend\CommunityController@reportToAdmin',
    'disable_sidemenu' => true,
]);

Route::post('/send-email',[
    'as' => 'frontend.sendEmailToFriend',
    'uses' => 'Frontend\CommunityController@sendEmailToFriend',
    'disable_sidemenu' => true,
]);

Route::post('/send-invite-email',[
    'as' => 'frontend.inviteEmailToFriendPost',
    'uses' => 'Frontend\CommunityController@inviteEmailToFriendPost',
    'disable_sidemenu' => true,
]);

Route::get('{lang?}/invite-email/{id}',[
    'as' => 'frontend.inviteEmailToFriend',
    'uses' => 'Frontend\CommunityController@inviteEmailToFriend',
    'disable_sidemenu' => true,
]);

Route::get('/reply-feature/{reply_id}/{type}',[
    'as' => 'frontend.replayFeature',
    'uses' => 'Frontend\CommunityController@replayFeature',
    'disable_sidemenu' => true,
]);


Route::get('{lang?}/Blog/{title}/{id}',[
    'as' => 'frontend.detail',
    'uses' => 'Frontend\BlogController@detail',
    'disable_sidemenu' => true,
]);

Route::get('{lang?}/search-blog',[
    'as' => 'searchBlogData',
    'uses' => 'Frontend\BlogController@searchData',
    'disable_sidemenu' => true,
]);

Route::post('/UploadFiles','Frontend\BlogController@uploadImages');

Route::get('{lang?}/seat/{seat_id}/office/{office_id}',[
    'as' => 'choseOption',
    'uses' => 'HomeController@choseOption',
    'disable_sidemenu' => true
]);

Route::get('{lang?}/enter-phone-number/{seat_id}/{office_id}',[
    'as' => 'gotToPhone',
    'uses' => 'HomeController@gotToPhone',
    'disable_sidemenu' => true
]);

Route::post('/save-phone-number',[
    'as' => 'savePhoneData',
    'uses' => 'HomeController@savePhoneData',
    'disable_sidemenu' => true
]);

Route::get('{lang?}/enter-code',[
    'as' => 'enterCode',
    'uses' => 'HomeController@enterCode',
    'disable_sidemenu' => true
]);

Route::post('/match-code',[
    'as' => 'matchCode',
    'uses' => 'HomeController@matchCode',
    'disable_sidemenu' => true
]);


Route::get('{lang?}/select-seat/{code}',[
    'as' => 'selectSeat',
    'uses' => 'HomeController@selectSeat',
    'disable_sidemenu' => true
]);

Route::post('/select-seat-data',[
    'as' => 'selectSeatPost',
    'uses' => 'HomeController@selectSeatPost',
    'disable_sidemenu' => true
]);

Route::get('/user-history/{user_id}',[
    'as' => 'userHistory',
    'uses' => 'HomeController@userHistory',
    'disable_sidemenu' => true
]);
