<?php
/**
 * Created by PhpStorm.
 * User: ahsanhussain
 * Date: 1/30/19
 * Time: 4:47 PM
 * Skype: ahsan.mughal2
 */

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
	
	Route::group(['middleware' => 'jwt2.auth'], function () {
		
		Route::post('startSession', [
			'uses' => 'TimeTrackController@startSession'
		]);
		
		Route::post('endSession', [
			'uses' => 'TimeTrackController@endSession'
		]);
		
		Route::post('checkSession', [
			'uses' => 'TimeTrackController@checkSession'
		]);
		
		Route::post('/receive-Image', [
			'uses' => 'TimeTrackController@ImagePost'
		]);
		
	});
	
});