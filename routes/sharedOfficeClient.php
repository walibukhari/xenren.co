<?php

/**
 * Shared Office Client
 * Author: Ahsan Hussain
 * Skype: ahsan.mughal2
 *
 * PREFIX: shared-office
 * */

Route::group(['middleware' => 'redirectRoute'], function () {
    Route::group(['middleware' => 'sharedoffice'], function () {
        //shared office register login routes //
        Route::group(['prefix' => '{lang?}/coworking-spaces'], function (){
            Route::get('/{country_name}/{city_name}/{office_name}/{office_id}','Frontend\SharedOfficeController@officeDetail');
            Route::get('/sign-up','Frontend\SharedOfficeController@signUp');
            Route::get('/sign-in','Frontend\SharedOfficeController@signIn');
            Route::get('/reset-password','Frontend\SharedOfficeController@resetPassword');
            Route::post('/forgot-password','Frontend\SharedOfficeController@forgotPassword');
            Route::get('/set-password/staff-id={id}',[
                'uses' =>  'Frontend\SharedOfficeController@setPassword',
                'as' => 'sharedoffice.setPassword',
            ]);
            Route::get('/top-co-work-spaces', function(){
                return view('frontend.coWorks.topcoworkSpaces');
            });
            Route::get('/newest-co-work-spaces', function(){
                return view('frontend.coWorks.newestCoworkSpaces');
            });

            Route::get('/sitemap.xml', [
                'uses' => 'SiteMaps\SiteMapController@sharedOfficesitemap'
            ]);
            Route::post('/set-password-post',[
                'uses' =>  'Frontend\SharedOfficeController@setPasswordPost',
                'as' => 'sharedoffice.setPasswordPost',
            ]);
            Route::get('/', 'Frontend\SharedOfficeController@index');

            Route::get('/{country_name?}/{city_name?}', 'Frontend\SharedOfficeController@searchPage');

        });
        Route::group(['prefix' => '/coworking-spaces'], function (){

    //shared office register login routes end//
    	Route::post('contact-shared-office/{officeId}', [
    		'uses' => 'Frontend\SharedOfficeController@message',
    		'as' => 'sharedoffice.contact'
    	]);

        Route::post('/shared-office-rating','Frontend\SharedOfficeController@sharedOfficeRating');
        Route::get('/shared-office-coWork/{userId}/{officeId}','Frontend\SharedOfficeController@sharedOfficeCoworkCheck');
    });
    });
});
