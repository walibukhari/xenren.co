<?php

/**
 * Modified & Refactored to have separate API and WEB Routes
 * Author: Ahsan Hussain
 * Skype: ahsan.mughal2
 * */


Route::get('/search-community', [
    'uses' => 'Api\CommunityController@searchCommunity'
]);


Route::get('/test', [
    'uses' => 'Api\UserIdentityController@test'
]);

Route::get('/search-shared-office/', [
    'uses' => 'Api\SharedOffice\SharedOfficeController@search'
]);

Route::get('/count-shared-office/', [
    'uses' => 'Api\SharedOffice\SharedOfficeController@count'
]);

Route::post('/uploadIdCard', [
    'uses' => 'Api\UserIdentityController@uploadIdCard'
]);

/**** jwt token ****/
Route::post('/loginJWTTokenOffice', [
    'as' => 'api.loginJWTTokenOffice',
    'uses' => 'Services\SharedOfficeController@loginJWTTokenOffice'
]);

Route::post('/sharedoffice/image', [
    'as' => 'api.sharedofficeImage',
    'uses' => 'Services\SharedOfficeController@officeImage'
]);

Route::get('/get/sharedoffice/image/{id}', [
    'as' => 'api.getsharedofficeImage',
    'uses' => 'Services\SharedOfficeController@getOfficeImage'
]);

Route::get('/checkUserAlreadyCheckIn/{user_id}', [
    'as' => 'api.checkUserAlreadyCheckIn',
    'uses' => 'Services\SharedOfficeController@checkUserAlreadyCheckIn'
]);

Route::post('/sharedoffice/change/booking/request', [
    'as' => 'api.sharedoffice.changeBookingPost',
    'uses' => 'Api\SharedOfficeController@changeBookingPost'
]);

Route::group(['middleware' => 'jwt2.auth', 'namespace' => 'Services'], function () {

    Route::get('/sharedoffice', [
        'as' => 'api.sharedoffice',
        'uses' => 'SharedOfficeController@index'
    ]);

    Route::get('/sharedoffice/pagination', [
        'as' => 'api.sharedoffice',
        'uses' => 'SharedOfficeController@paginationIndex'
    ]);

    Route::get('/sharedoffice/zoom', [
        'as' => 'api.sharedoffice.zoom',
        'uses' => 'SharedOfficeController@mapzoomIn'
    ]);

    Route::get('/sharedofficeAll', [
        'as' => 'api.sharedoffice',
        'uses' => 'SharedOfficeController@getAllSharedOffices'
    ]);

    Route::get('/sharedofficeFilterData', [
        'as' => 'api.sharedoffice',
        'uses' => 'SharedOfficeController@sharedofficeFilterData'
    ]);

    Route::get('/sharedofficeFilterResults', [
        'as' => 'api.sharedofficeFilterResults',
        'uses' => 'SharedOfficeController@sharedofficeFilterResults'
    ]);

    Route::post('/sharedoffice/helpful-review/{reviewId}', [
        'as' => 'api.sharedoffice',
        'uses' => 'SharedOfficeController@helpfulReview'
    ]);
    Route::post('/sharedoffice/like-shared-office/{sharedOffice}', [
        'as' => 'api.sharedoffice',
        'uses' => 'SharedOfficeController@likeSharedOffice'
    ]);

    Route::get('/userImage', [
        'as' => 'api.userImage',
        'uses' => 'SharedOfficeController@userImage'
    ]);

    Route::post('/saveStatus', [
        'as' => 'api.saveStatus',
        'uses' => 'SharedOfficeController@saveStatus'
    ]);

    Route::post('/sharedOffice/rating', [
        'as' => 'api.sharedOffice.rating',
        'uses' => 'SharedOfficeController@sharedOfficeRating'
    ]);

    Route::get('/reset', [
        'as' => 'api.reset',
        'uses' => 'SharedOfficeController@reset'
    ]);

    Route::post('/submitUserIdentity', [
        'as' => 'api.submitUserIdentity',
        'uses' => 'SharedOfficeController@submitUserIdentity'
    ]);

    /* SHARED OFFICE (API1,API2,API3,API4,API5, API5.1 ... */
    Route::post('/sharedofficeUserValidation', [
        'as' => 'api.sharedofficeUserValidation',
        'uses' => 'SharedOfficeController@loginVerification'
    ]);

    Route::post('/officeDetailsBtn', [
        'as' => 'api.officeDetailsBtn',
        'uses' => 'SharedOfficeController@officeDetailsBtn'
    ]);

    Route::any('/officeSpaceDetailsBtn/{officeId}', [
        'as' => 'api.officeSpaceDetailsBtn',
        'uses' => 'SharedOfficeController@officeSpaceDetailsBtn'
    ]);

    Route::get('/officeDetailsBtn/{id}', [
        'as' => 'api.officeDetailsBtn',
        'uses' => 'SharedOfficeController@officeDetailsBtnget'
    ]);

    Route::post('/sharedofficeQrOfficeDoor', [
        'as' => 'api.sharedofficeQrOfficeDoor',
        'uses' => 'SharedOfficeController@sharedofficeQrOfficeDoor'
    ]);
    Route::post('/sharedofficeQrSeat', [
        'as' => 'api.sharedofficeQrSeat',
        'uses' => 'SharedOfficeController@sharedofficeQrSeat'
    ]);
    Route::post('/sharedofficeClickBtSeat', [
        'as' => 'api.sharedofficeClickBtSeat',
        'uses' => 'SharedOfficeController@sharedofficeClickBtSeat'
    ]);
    Route::post('/sharedofficeClickBtSeatInfo', [
        'as' => 'api.sharedofficeClickBtSeatInfo',
        'uses' => 'SharedOfficeController@sharedofficeClickBtSeatInfo'
    ]);
    Route::post('/sharedofficeChangeSeatByQR', [
        'as' => 'api.sharedofficeChangeSeatByQR',
        'uses' => 'SharedOfficeController@sharedofficeChangeSeatByQR'
    ]);
    Route::get('/sharedofficeGetCost', [
        'as' => 'api.sharedofficeGetCost',
        'uses' => 'SharedOfficeController@sharedofficeGetCost'
    ]);

    /*19 mart 2018 */
    Route::post('/sharedofficeStartTimer', [
        'as' => 'api.sharedofficeStartTimer',
        'uses' => 'SharedOfficeController@sharedofficeStartTimer'
    ]);
    Route::get('/sharedofficeStopTimer', [
        'as' => 'api.sharedofficeStopTimer',
        'uses' => 'SharedOfficeController@sharedofficeStopTimer'
    ]);
    Route::post('/sharedofficeFinanceCurrentTransaction', [
        'as' => 'api.sharedofficeFinanceCurrentTransaction',
        'uses' => 'SharedOfficeController@sharedofficeFinanceCurrentTransaction'
    ]);

    Route::get('/sharedofficeDesks', [
        'as' => 'api.sharedofficeDesks',
        'uses' => 'SharedOfficeController@getAllDesks'
    ]);

    Route::post('/sharedofficeDesks/booking/', [
        'as' => 'api.sharedofficeDesks',
        'uses' => 'SharedOfficeController@selectDesk'
    ]);
    /* END SHARED OFFICE (API1,API2,API3,API4,... */

    Route::get('/singleOffice/products/{id}', [
        'as' => 'api.singleOfficeProducts',
        'uses' => 'SharedOfficeController@singleOfficeProducts'
    ]);
    Route::post('/singleOfficeVerifyInfo/products/', [
        'as' => 'api.singleOfficeProductsInfo',
        'uses' => 'SharedOfficeController@singleOfficeProductsVerifyInfo'
    ]);
    Route::post('/singleOfficeVerifyPhoto/products/', [
        'as' => 'api.singleOfficeProductsPhoto',
        'uses' => 'SharedOfficeController@singleOfficeProductsVerifyPhoto'
    ]);
    Route::post('/sharedofficePhoto', [
        'as' => 'api.sharedofficePhoto',
        'uses' => 'SharedOfficeController@indexVerifyPhoto'
    ]);
    Route::get('/singleOffice/RentProductStartByPidPhoto/{product_id}/{token}', [
        'as' => 'api.RentProductStartByPidPhoto',
        'uses' => 'SharedOfficeController@RentProductStartByPidPhoto'
    ]);
    Route::get('/singleOffice/RentProductStartByQrPhoto/{qr}/{token}', [
        'as' => 'api.RentProductStartByQrPhoto',
        'uses' => 'SharedOfficeController@RentProductStartByQrPhoto'
    ]);


    Route::get('/singleOffice/RentProductStartByPID/{product_id}/{user}', [
        'as' => 'api.RentProductStartByPID',
        'uses' => 'SharedOfficeController@RentProductStartByPID'
    ]);
    Route::get('/singleOffice/RentProductStartByQR/{qr}/{user}', [
        'as' => 'api.RentProductStartByQR',
        'uses' => 'SharedOfficeController@RentProductStartByQR'
    ]);
    Route::get('/singleOffice/RentProductsActive/{office_id}', [
        'as' => 'api.RentProductsActive',
        'uses' => 'SharedOfficeController@RentProductsActive'
    ]);
    Route::get('/singleOffice/finance/{office_id}', [
        'as' => 'api.ShowFinanceData',
        'uses' => 'SharedOfficeController@ShowFinanceDataView'
    ]);
    Route::get('/singleOffice/paymentfinaceviewuser/{finance_id}', [
        'as' => 'api.PaymentFinanceViewUser',
        'uses' => 'SharedOfficeController@PaymentFinanceViewUser'
    ]);
    Route::get('/singleOffice/storepaymentforuser/{finance_id}/{status}', [
        'as' => 'api.StorePaymentForUser',
        'uses' => 'SharedOfficeController@StorePaymentForUser'
    ]);
});

Route::get('/skills', [
    'as' => 'api.skills',
    'uses' => 'Services\SkillController@index'
]);
Route::get('/languages', [
    'as' => 'api.languages',
    'uses' => 'Services\ServiceController@languages'
]);
Route::get('/getUser', [
    'as' => 'api.user',
    'uses' => 'Services\ServiceController@getUser'
]);
Route::post('/project-chat-messages', [
    'as' => 'api.project-chat-message',
    'uses' => 'Services\ServiceController@projectChatMessages'
]);
Route::post('/project-chat-messages/post', [
    'as' => 'api.project-chat-message.post',
    'uses' => 'Services\ServiceController@projectChatMessagesCreate'
]);

Route::get('/location', [
    'uses' => 'Services\ServiceController@tempByLatLong'
]);

Route::get('/get/location', [
    'uses' => 'Services\ServiceController@getLocation'
]);

Route::get('/countries', [
    'as' => 'api.countries',
    'uses' => 'Services\ServiceController@countries'
]);

Route::get('/cities', [
    'as' => 'api.cities',
    'uses' => 'Services\ServiceController@cities'
]);

Route::get('/cities-only', [
    'as' => 'api.cities',
    'uses' => 'Services\ServiceController@citiesOnly'
]);

Route::get('/resend-code', [
    'as' => 'api.resend',
    'uses' => 'Services\ServiceController@resend'
]);

Route::get('/countries-cities', [
    'as' => 'api.cities',
    'uses' => 'Services\ServiceController@CountriesAndCities'
]);

Route::get('/cities-country', [
    'as' => 'api.cities',
    'uses' => 'Services\ServiceController@CitiesAndCountries'
]);

Route::get('/etc', [
    'as' => 'api.etc',
    'uses' => 'EtcController@etc'
]);
Route::post('/identityAuthentication/change', [
    'as' => 'api.identityauthentication.change',
    'uses' => 'Services\ServiceController@identityAuthenticationChange'
]);
Route::post('/identityAuthentication/submit', [
    'as' => 'api.identityauthentication.submit',
    'uses' => 'Services\ServiceController@identityAuthenticationSubmit'
]);
Route::post('/isAllowToCheckContact', [
    'as' => 'api.isallowtocheckcontact',
    'uses' => 'Services\ServiceController@isAllowToCheckContact'
]);
Route::post('/skipAddUserSkill', [
    'as' => 'api.skipadduserskill',
    'uses' => 'Services\ServiceController@skipAddUserSkill'
]);

/**
 * v1 routes starts here
 * */

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
    Route::post('/auth/facebook', [
        'uses' => 'AuthController@registerWithFacebook'
    ]);

    Route::post('/create', [
        'uses' => 'AuthController@addTimeRecord'
    ]);

    Route::get('/fetchCountryCode', [
        'uses' => 'User\PersonalDetailController@fetchCountryCode'
    ]);

    Route::post('/auth/login', [
        'as' => 'api.auth.login',
        'uses' => 'AuthController@login'
    ]);

    Route::post('/auth/register', [
        'as' => 'api.auth.register',
        'uses' => 'AuthController@register'
    ]);
    Route::post('/auth/registered', [
        'as' => 'api.auth.register',
        'uses' => 'AuthController@registered'
    ]);
    Route::post('/auth/forgot-password', [
        'as' => 'api.auth.forgotPassword',
        'uses' => 'AuthController@forgotPassword'
    ]);

    Route::post('/auth/verify-code', [
        'as' => 'api.auth.verifyCode',
        'uses' => 'AuthController@verifyCode'
    ]);

    Route::post('/staff/login', [
        'as' => 'staff.loginPost',
        'uses' => 'StaffController@loginPost'
    ]);

    Route::post('/staff/register', [
        'as' => 'staff.registerPost',
        'uses' => 'StaffController@registerPost'
    ]);

    Route::post('/staff/forgot/password', [
        'as' => 'staff.forgotPassword',
        'uses' => 'StaffController@forgotPassword'
    ]);

    Route::get('/staff/logout', [
        'as' => 'staff.logout',
        'uses' => 'StaffController@logout'
    ]);

    Route::get('/sharedoffice/booking',[
       'as' => 'sharedOfficeBooking',
       'uses' => 'StaffController@officeBooking'
    ]);

    Route::get('/languages', [
        'as' => 'api.language',
        'uses' => 'User\LanguagesController@allLanguages'
    ]);

    Route::get('/users', [
			'as' => 'api.sharedoffice',
			'uses' => 'AuthController@users'
		]);

	Route::group(['middleware' => 'jwt2.auth'], function () {

        Route::get('/booking/requests', [
            'as' => 'api.bookingRequests',
            'uses' => 'SharedOfficeController@getBookingData'
        ]);

        Route::get('/booking/details/{id}', [
            'as' => 'api.bookingDetails',
            'uses' => 'SharedOfficeController@bookingDetails'
        ]);

        Route::get('booking/request/cancel/{id}', [
            'as' => 'api.cancelBookingRequest',
            'uses' => 'SharedOfficeController@requestCancel'
        ]);

        Route::get('/staff/check', [
            'as' => 'staff.check',
            'uses' => 'StaffController@staffExist'
        ]);

        Route::post('/staff/save-token', [
            'as' => 'staff.saveToken',
            'uses' => 'StaffController@saveToken'
        ]);

        Route::post('/create/push-notification-settings', [
            'uses' => 'PushNotificationController@create'
        ]);

        Route::get('/push-notification-settings', [
            'uses' => 'PushNotificationController@get'
        ]);

        Route::post('/scan-other-barcode', [
            'uses' => 'SharedOfficeController@scanOtherTypeBarCode'
        ]);

        Route::get('/scan-other-barcode', [
            'uses' => 'SharedOfficeController@getUserScannerBarCode'
        ]);

        Route::get('/auth/logout', [
            'as' => 'api.sharedoffice',
            'uses' => 'AuthController@logout'
        ]);

        // Route::post('/scan-other-barcode', [
        //     'uses' => 'SharedOfficeController@scanOtherTypeBarCode'
        // ]);

        Route::post('getData', [
            'uses' => 'AuthController@getTimeRecord'
        ]);

        Route::post('startSession', [
            'uses' => 'TimeTrackController@startSession'
        ]);

        Route::post('endSession', [
            'uses' => 'TimeTrackController@endSession'
        ]);

        Route::post('checkSession', [
            'uses' => 'TimeTrackController@checkSession'
        ]);

        Route::post('/receive-Image', [
            'uses' => 'TimeTrackController@ImagePost'
        ]);

        Route::post('/topup-amount', [
            'uses' => 'TopupController@topUp'
        ]);

        Route::post('/topup-revenue-monster', [
            'uses' => 'TopupController@topUpRevenueMonster'
        ]);

        Route::post('/withdraw-amount', [
            'uses' => 'TopupController@withdraw'
        ]);

        Route::post('/withdraw-api', [
            'uses' => 'WithdrawController@withdrawApi'
        ]);

        Route::get('/my-spaces', [
            'uses' => 'SpaceController@mySpaces'
        ]);

        Route::post('/my-spaces', [
            'uses' => 'TopupController@linkPayPal'
        ]);

        Route::post('/updateUserProfile', [
            'uses' => 'User\PortfolioController@updateProfile'
        ]);

        Route::post('/save-token', [
            'as' => 'api.auth.verifyCode',
            'uses' => 'AuthController@saveToken'
        ]);

        Route::post('auth/BindFacebook', [
            'uses' => 'AuthController@BindFacebook'
        ]);

        Route::post("auth/set_password", [
            "uses" => "AuthController@set_password"
        ]);

        Route::get('me', [
            'as' => 'api.auth.me',
            'uses' => 'AuthController@me'
        ]);

        Route::get('public-profile/{id}', [
            'as' => 'api.auth.publicProfile',
            'uses' => 'AuthController@publicProfile'
        ]);

        Route::get("freelancer_detail/{project_id}", [
            "uses" => "AuthController@freelancerDetail"
        ]);

        Route::post("freelancer/addToFvorite/{freelancer_id}", [
            "uses" => "Freelancer\FreelancerController@favoriteFreelancer"
        ]);

        Route::get('user/portfolio/{id}', [
            'as' => 'api.auth.user.portfolio',
            'uses' => 'User\PortfolioController@getPortfolio'
        ]);
        Route::get('user/platformReviews/{id}', [
            'as' => 'api.auth.user.platformReviews',
            'uses' => 'UserController@platformReviews'
        ]);
        Route::get('user/customerReviews/{id}', [
            'as' => 'api.auth.user.customerReviews',
            'uses' => 'UserController@customerReviews'
        ]);
        Route::get('me/userInfo', [
            'as' => 'api.auth.user.info',
            'uses' => 'User\PersonalDetailController@userInfo'
        ]);
        Route::post('/me/comments', [
            'as' => 'api.auth.me.personalDetail.comments',
            'uses' => 'User\PersonalDetailController@comments'
        ]);
        Route::post('/me/ratings', [
            'as' => 'api.auth.me.personalDetail.ratings',
            'uses' => 'User\PersonalDetailController@ratings'
        ]);
        Route::post('/me/reviews', [
            'as' => 'api.auth.me.personalDetail.reviews',
            'uses' => 'User\PersonalDetailController@reviews'
        ]);

        Route::get('/me/portfolio', [
            'as' => 'api.auth.me.portfolio',
            'uses' => 'User\PortfolioController@index'
        ]);
        Route::get('/me/portfolio/{id}', [
            'as' => 'api.auth.me.portfolio.byCategory',
            'uses' => 'User\PortfolioController@byCategory'
        ]);
        Route::post('/me/portfolio/create', [
            'as' => 'api.auth.me.portfolio.create',
            'uses' => 'User\PortfolioController@create'
        ]);

        Route::post('/me/update/personal-address', [
            'as' => 'api.auth.me.portfolio.create',
            'uses' => 'User\PersonalDetailController@updatePersonalAddress'
        ]);

        Route::post('/me/update/job-detail', [
            'as' => 'api.auth.me.portfolio.create',
            'uses' => 'User\PersonalDetailController@updateJobDetail'
        ]);

        Route::post('/me/update/address', [
            'as' => 'api.auth.me.portfolio.addressupdate',
            'uses' => 'User\PersonalDetailController@updateAddress'
        ]);

        Route::get('/me/joined-groups', [
            'as' => 'api.auth.me.portfolio.addressupdate',
            'uses' => 'User\PersonalDetailController@joinedGroups'
        ]);

        Route::post('/me/updateContactInfo', [
            'as' => 'api.auth.me.personalDetail.updateContactInfo',
            'uses' => 'User\PersonalDetailController@updateContactInfo'
        ]);
        Route::post('/me/updateProfilePicture', [
            'as' => 'api.auth.me.personalDetail.updateProfilePicture',
            'uses' => 'User\PersonalDetailController@updateProfilePicture'
        ]);

        Route::get('/me/get_notifications/{notification_type}', [
            'uses' => 'NotificationsApiController@get_notifications'
        ]);

        Route::get('/me/sharedOfficeBookingUserDetail', [
            'uses' => 'NotificationsApiController@getBookingUserDetail'
        ]);

        Route::post('/me/deleteSharedOfficeBooking', [
            'uses' => 'NotificationsApiController@delBookingUserDetail'
        ]);

        Route::post('/me/sendBookingMessage', [
            'uses' => 'NotificationsApiController@sendBookingMessage'
        ]);

        Route::get("/me/get_all_notification", [
            "uses" => "NotificationsApiController@get_all_notification"
        ]);

        Route::get('/me/get_received_orders', [
            'uses' => 'NotificationsApiController@get_received_orders'
        ]);

        Route::get('/me/inbox_notification', [
            'uses' => 'NotificationsApiController@inbox_notification'
        ]);

        Route::get('/me/system_notification', [
            'uses' => 'NotificationsApiController@get_system_notifications'
        ]);

        Route::put("/me/notification_detail/{notification_id}", [
            "uses" => "NotificationsApiController@get_notification_detail"
        ]);

        Route::post("/me/awardProject", [
            "uses" => "NotificationsApiController@awardProject"
        ]);

        Route::post("/me/rejectProjectOffer", [
            "uses" => "NotificationsApiController@rejectProjectOffer"
        ]);

        Route::get("/me/get-my-open-projects", [
            "uses" => "User\PortfolioController@getMyOpenJobs"
        ]);

        Route::post("/send-inbox-message", [
            "uses" => "NotificationsApiController@sendMessage"
        ]);

        Route::post("/inbox/trashAll", [
            "uses" => "NotificationsApiController@trashAll"
        ]);

        Route::post("/inbox/trash", [
            "uses" => "InboxApiController@trash"
        ]);

        Route::post("/acceptOffer", [
            "uses" => "InboxApiController@acceptOffer"
        ]);

        Route::post("/rejectOffer", [
            "uses" => "InboxApiController@rejectOffer"
        ]);

        /* 16 July 2018 - Shared Office Room Chat */
        Route::post('/sharedOffice/room/chat', [
            'as' => 'sharedOffice.room.chat',
            'uses' => 'SharedOffice\SharedOfficeController@indoorChat'
        ]);
        Route::get('/sharedOffice/room/chat/{office_id}', [
            'as' => 'sharedOffice.room.chat',
            'uses' => 'SharedOffice\SharedOfficeController@getMessagesByOfficeId'
        ]);
        Route::get('/chatrooms', [
            'as' => 'chatrooms',
            'uses' => 'SharedOffice\SharedOfficeController@getChatRooms'
        ]);
        Route::get('/chatroom/{id}', [
            'as' => 'chatrooms',
            'uses' => 'SharedOffice\SharedOfficeController@getChatRoomDetail'
        ]);

        Route::get('/sharedOffice/users/{office_id}', [
            'as' => 'sharedOffice.users',
            'uses' => 'SharedOffice\SharedOfficeController@getUsers'
        ]);

//        Route::put('/report-user', [
//            'as' => 'report.user',
//            'uses' => 'UserController@reportUser'
//        ]);

        /**
         * CHAT APIs
         * */
        Route::get('/chat/{opponentUserId}', [
            'as' => 'report.user',
            'uses' => 'ChatController@getChat'
        ]);
        Route::post('/chat/{opponentUserId}', [
            'as' => 'report.user',
            'uses' => 'ChatController@postChat'
        ]);

        // Contact management
        Route::get('/contact-management/usersList/{office_id}', [
            'as' => 'contactManagement.usersList',
            'uses' => 'ContactManagement\ContactManagementController@UsersList'
        ]);
        Route::get('/contact-management/usersList', [
            'as' => 'contactManagement.usersList',
            'uses' => 'ContactManagement\ContactManagementController@UsersList'
        ]);

        Route::post('/report-user',[
            'as' => 'report-user',
            'uses' => 'UserController@reportUser'
        ]);
        Route::post("/contact-management/blockSingleUser","ContactManagement\ContactManagementController@blockUsers");
        Route::post("/contact-management/BlockMultiUser",'ContactManagement\ContactManagementController@blockUsers');
        Route::post("/contact-management/unblockUser","ContactManagement\ContactManagementController@unblockUser");
        Route::post("/contact-management/unBlockMultiUser",'ContactManagement\ContactManagementController@unblockUser');
        Route::get("/contact-management/getBlockedUsers",'ContactManagement\ContactManagementController@getBlockedUsers');
        Route::post("/contact-management/hideContactPlatformSingleUser",'ContactManagement\ContactManagementController@hideContactPlatform');
        Route::post("/contact-management/hideContactPlatformMultipleUsers",'ContactManagement\ContactManagementController@hideContactPlatform');
        Route::post("/contact-management/hideContactPlatformAllUsers",'ContactManagement\ContactManagementController@hideContactPlatFromAll');


//        Route::post('/contact-management/hideContactInfo', [
//            'as' => 'contactManagement.hideContactInfo',
//            'uses' => 'ContactManagement\ContactManagementController@HideContactInfo'
//        ]);
//        Route::post('/contact-management/blockUserFromChat', [
//            'as' => 'contactManagement.blockUserFromChat',
//            'uses' => 'ContactManagement\ContactManagementController@BlockUserFromChat'
//        ]);
//        Route::get('/contact-management/getMyBlockedUsers', [
//            'as' => 'contactManagement.getMyBlockedUsers',
//            'uses' => 'ContactManagement\ContactManagementController@GetMyBlockedUsers'
//        ]);
//        Route::post('/contact-management/blockUsers', [
//            'as' => 'contactManagement.blockUsers',
//            'uses' => 'ContactManagement\ContactManagementController@blockUsers'
//        ]);

//        Route::put('/contact-management/unblockUsers', [
//            'as' => 'contactManagement.unblockUsers',
//            'uses' => 'ContactManagement\ContactManagementController@unblockUsers'
//        ]);

        // Make Offer
        Route::post('/jobs/make_offer', [
            'uses' => 'Home\HomeApiController@makeOfferPost'
        ]);

        //Join Discussion Common Project
        Route::get('/joinDiscussion/getProject/{id}', [
            'uses' => 'Home\JoinDiscussionApiController@getProject'
        ]);

        // Thumb Down Jobs
        Route::post('/addThumbdownJob', [
            'uses' => 'Home\ThumbdownJobApiController@thumbsDownJob'
        ]);
        Route::post('/addFavouriteJob', [
            'uses' => 'Home\FavouriteJobApiController@addFavouriteJob'
        ]);

        Route::post('/sendMessage', [
            'uses' => 'Home\ProjectChatApiController@sendMessage'
        ]);

        Route::post('/joinDiscussion/thumbdown/{chat_follower_id}', [
            'uses' => 'Home\JoinDiscussionApiController@thumbDown'
        ]);

        Route::post('/joinDiscussion/thumbup/{chat_follower_id}', [
            'uses' => 'Home\JoinDiscussionApiController@thumbUp'
        ]);

        Route::post('/projectChat/postProjectChatFile', [
            'uses' => 'Home\ProjectChatApiController@postProjectChatFile'
        ]);

        // Invite Freelancer to Work
        Route::post('/freelancer/inviteToWork', [
            'uses' => 'Home\HomeApiController@inviteToWork'
        ]);
        // Send Offer to Freelancer
        Route::post('/freelancer/sendOffer', [
            'uses' => 'Home\HomeApiController@sendOffer'
        ]);
        // Add Freelancer to Favorite List
        Route::get('/freelancer/addToFavorite/{freelancer_id}', [
            'uses' => 'Home\HomeApiController@addToFavorite'
        ]);
        // Remove Freelancer from Favorite List
        Route::get('/freelancer/removeFromFavorite/{freelancer_id}', [
            'uses' => 'Home\HomeApiController@removeFromFavorite'
        ]);

    });

	Route::group(['middleware' => 'jwt2.authIfToken'], function () {
		Route::get('/jobs/search', [
			'uses' => 'Home\HomeApiController@search'
		]);

		// Frelancer Search API
        Route::get('/freelancer/search', [
            'uses' => 'Home\HomeApiController@findExperts'
        ]);
	});
});

Route::get('get-map-data', [
    'uses' => 'Services\SharedOfficeController@mapJson'
]);
