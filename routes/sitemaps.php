<?php
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Sitemap Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::get('/sitemap.xml', [
	'uses' => 'SiteMapController@index'
]);
Route::get('/sitemap-jobs.xml', [
	'uses' => 'SiteMapController@jobs'
]);
//Route::get('/sitemap-common-jobs.xml', [
//	'uses' => 'SiteMapController@commonJobs'
//]);
//Route::get('/sitemap-official-jobs.xml', [
//	'uses' => 'SiteMapController@officialJobs'
//]);
Route::get('/sitemap-shared-office.xml', [
	'uses' => 'SiteMapController@sharedOffice'
]);

Route::get('/sitemap-shared-office{id}.xml', [
    'uses' => 'SiteMapController@sharedOffice'
]);

Route::get('/sitemap-experts.xml', [
	'uses' => 'SiteMapController@experts'
]);
Route::get('/sitemap-website.xml', [
	'uses' => 'SiteMapController@general'
]);