<?php
//Backend route
Route::group(array('namespace' => 'Auth', 'middleware' => ['guest:staff']), function() {
    //Backend not logged in route
    Route::group(array('middleware' => ['guest:staff']), function () {
//        Route::group(['domain' => 'www.xenren.co', 'middleware' => 'sharedoffice'], function() {
            Route::get('/staff/login', ['as' => 'staff.login', 'uses' => 'StaffController@login']);
//        });
        Route::post('/staff/login/post', ['as' => 'staff.login.post', 'uses' => 'StaffController@loginPost']);
    });
});

//Backend route
Route::group(array('prefix' => 'admin', 'namespace' => 'Backend', 'middleware' => ['auth:staff']), function () {
    Route::get('/', ['as' => 'backend.index', 'uses' => 'IndexController@index']);
    Route::get('/sharedoffice/operator', ['as' => 'backend.index.operator', 'uses' => 'IndexController@index']);
    Route::get('/notifications', [
        'as' => 'backend.sharedofficeNotification',
        'uses' => 'SharedOfficeNotification@index',
        'middleware' => 'staffHasPermission:manage_sharedoffice_notifications'
    ]);

    Route::get('/office-category', [
        'as' => 'backend.sharedofficeCategories',
        'uses' => 'SharedOfficeCategoryController@index'
    ]);


    Route::get('/office-owner-progress', [
        'as' => 'backend.sharedofficeOwnerProgress',
        'uses' => 'SharedOfficeController@OwnerProgress'
    ]);

    Route::get('/owner-progress/{id}', [
        'as' => 'backend.sharedofficeProgress',
        'uses' => 'SharedOfficeController@Progress'
    ]);

    Route::post('/add-category', [
        'as' => 'backend.AddCategory',
        'uses' => 'SharedOfficeCategoryController@addCategory'
    ]);

    Route::get('/delete-category/{id}', [
        'as' => 'backend.deleteCategory',
        'uses' => 'SharedOfficeCategoryController@deleteCategory'
    ]);

    Route::post('/update-category', [
        'as' => 'backend.updateCategory',
        'uses' => 'SharedOfficeCategoryController@updateCategory'
    ]);

    Route::get('/settings', [
        'as' => 'backend.sharedOfficeSettings',
        'uses' => 'SharedOfficeSettingsController@settings',
        'middleware' => 'staffHasPermission:manage_sharedoffice_setting'
    ]);

    Route::get('/invoices', [
        'as' => 'backend.sharedOfficeInvoices',
        'uses' => 'SharedOfficeController@invoices',
        'middleware' => 'staffHasPermission:manage_sharedoffice_invoices'
    ]);

    Route::get('/download-invoice-pdf', [
        'as' => 'backend.downloadPdf',
        'uses' => 'SharedOfficeController@downloadPdf'
    ]);
    Route::get('/showPDFPage', [
        'as' => 'backend.showPDFPage',
        'uses' => 'SharedOfficeController@showPDFPage'
    ]);

    Route::post('/settings', [
        'as' => 'backend.post.sharedoffice.settings',
        'uses' => 'SharedOfficeSettingsController@settingPost'
    ]);

    Route::post('/invoice', [
        'as' => 'backend.saveInvoiceRecord',
        'uses' => 'SharedOfficeController@saveInvoice'
    ]);

    Route::post('/manage-booking', [
        'as' => 'backend.post.sharedofficeBooking.settings',
        'uses' => 'SharedOfficeSettingsController@settingBookingPost'
    ]);

    Route::get('/booked/requests/{id}', [
        'as' => 'backend.sharedofficeOwnerBookedRequests',
        'uses' => 'SharedOfficeNotification@getBookedRequests',
        'middleware' => 'staffHasPermission:manage_sharedoffice_owner_request'
    ]);

    Route::get('/change/booking/requests/{id}', [
        'as' => 'backend.sharedofficeChangeBookedRequests',
        'uses' => 'SharedOfficeNotification@changeBookedRequests',
        'middleware' => 'staffHasPermission:manage_sharedoffice_owner_request'
    ]);

    Route::get('/get/office/products/{office_id}', [
        'as' => 'backend.sharedofficeGetProductDetail',
        'uses' => 'SharedOfficeNotification@getProductDetail',
        'middleware' => 'staffHasPermission:manage_sharedoffice_owner_request'
    ]);

     Route::post('/confirm/change/booking/requests', [
        'as' => 'backend.confirmChangeBookingRequest',
        'uses' => 'SharedOfficeNotification@confirmChange',
        'middleware' => 'staffHasPermission:manage_sharedoffice_owner_request'
    ]);

     Route::post('/update/seat/number', [
        'as' => 'backend.updateSeatNumber',
        'uses' => 'SharedOfficeNotification@updateSeatNumber',
        'middleware' => 'staffHasPermission:manage_sharedoffice_owner_request'
    ]);

     Route::post('/update/date-time/requests', [
        'as' => 'backend.updateDateTime',
        'uses' => 'SharedOfficeNotification@updateDateTime',
        'middleware' => 'staffHasPermission:manage_sharedoffice_owner_request'
    ]);

     Route::get('/cancel/change/booking/requests/{id}/{book_id}', [
        'as' => 'backend.cancelChangeBookingRequest',
        'uses' => 'SharedOfficeNotification@cancelChange',
        'middleware' => 'staffHasPermission:manage_sharedoffice_owner_request'
    ]);

    Route::get('cancel/booked/requests', [
        'as' => 'backend.sharedoffice.request.cancel',
        'uses' => 'SharedOfficeNotification@cancelBookingRequest',
        'middleware' => 'staffHasPermission:manage_sharedoffice_owner_request'
    ]);

    Route::get('/notification/detail/{id}', [
        'as' => 'backend.notificationDetails',
        'uses' => 'SharedOfficeNotification@detailNotification'
    ]);

    Route::get('/un-readable/{id}', [
        'as' => 'backend.unReadAble',
        'uses' => 'SharedOfficeNotification@unReadAble'
    ]);

    Route::get('/readable/{id}', [
        'as' => 'backend.readAble',
        'uses' => 'SharedOfficeNotification@readAble'
    ]);

    Route::get('/edit/sharedoffice/{id}', [
        'as' => 'backend.sharedofficeOwnerEdit',
        'uses' => 'SharedOfficeController@edit',
        'middleware' => 'staffHasPermission:manage_sharedoffice'
    ]);

    Route::get('/sharedoffice/products/{id}', [
        'as' => 'backend.sharedofficeOwnerProducts',
        'uses' => 'SharedOfficeProductsController@index',
        'middleware' => 'staffHasPermission:manage_sharedofficeproducts'
    ]);

    Route::get('/sharedoffice/moreImages/{id}', [
        'as' => 'backend.sharedofficeOwnerAddImages',
        'uses' => 'SharedOfficeController@addimages',
        'middleware' => 'staffHasPermission:manage_sharedoffice_images'
    ]);

    Route::post('/sharedoffice/update-data/', [
        'as' => 'backend.sharedofficeUpdateData',
        'uses' => 'SharedOfficeController@sharedofficeUpdateData',
        'middleware' => 'staffHasPermission:manage_sharedoffice_images'
    ]);

    Route::post('/upload/files', [
        'as' => 'backend.sharedofficeAddfiles',
        'uses' => 'SharedOfficeController@addFiles',
    ]);

    Route::get('/sharedoffice/files/{office_id}', [
        'as' => 'backend.sharedofficegetfiles',
        'uses' => 'SharedOfficeController@getFiles',
    ]);

    Route::get('/profile-update', [
        'as' => 'backend.sharedofficeProfile',
        'uses' => 'SharedOfficeProfile@index',
        'middleware' => 'staffHasPermission:manage_sharedoffice'
    ]);
    Route::post('/update-profile', [
        'as' => 'backend.sharedofficeProfileUpdate',
        'uses' => 'SharedOfficeProfile@ProfileUpdate',
    ]);

    //bulletin routes
    Route::get('/bulletin', [
        'as' => 'backend.bulletin',
        'uses' => 'BulletinController@index',
        'middleware' => 'staffHasPermission:manage_bullet_ins'
    ]);

    Route::get('/article', [
        'as' => 'backend.articleCreate',
        'uses' => 'ArticleController@articleCreate',
        'middleware' => 'staffHasPermission:manage_article'
    ]);

    Route::get('/topics', [
        'as' => 'backend.topic',
        'uses' => 'CommunityController@communityIndex',
        'middleware' => 'staffHasPermission:manage_community'
    ]);

    Route::get('/manage/posts', [
        'as' => 'backend.managePost',
        'uses' => 'CommunityController@communityManagePostsIndex',
        'middleware' => 'staffHasPermission:manage_community'
    ]);

    Route::get('/community/reports', [
        'as' => 'backend.communityReports',
        'uses' => 'CommunityController@communityReports',
        'middleware' => 'staffHasPermission:manage_community'
    ]);

    Route::get('/community/reports/delete/{id}', [
        'as' => 'backend.deleteReport',
        'uses' => 'CommunityController@deleteReport',
        'middleware' => 'staffHasPermission:manage_community'
    ]);

    Route::get('/community/reports/edit/{id}', [
        'as' => 'backend.editReport',
        'uses' => 'CommunityController@editReport',
        'middleware' => 'staffHasPermission:manage_community'
    ]);

    Route::post('/community/reports/update', [
        'as' => 'backend.updateReportedDiscussion',
        'uses' => 'CommunityController@updateReportedDiscussion',
        'middleware' => 'staffHasPermission:manage_community'
    ]);

    Route::get('/stop/posts/{id}', [
        'as' => 'backend.stopPost',
        'uses' => 'CommunityController@stopPost',
        'middleware' => 'staffHasPermission:manage_community'
    ]);;

    Route::get('/start/posts/{id}', [
        'as' => 'backend.startPost',
        'uses' => 'CommunityController@startPost',
        'middleware' => 'staffHasPermission:manage_community'
    ]);

    Route::get('/delete/posts/{id}', [
        'as' => 'backend.deletePost',
        'uses' => 'CommunityController@deletePost',
        'middleware' => 'staffHasPermission:manage_community'
    ]);

    Route::post('/create/topics', [
        'as' => 'backend.createTopic',
        'uses' => 'CommunityController@createTopic',
        'middleware' => 'staffHasPermission:manage_community'
    ]);

    Route::get('/edit/topic/{id}', [
        'as' => 'backend.editTopic',
        'uses' => 'CommunityController@editTopic',
        'middleware' => 'staffHasPermission:manage_community'
    ]);

    Route::post('/update/topic', [
        'as' => 'backend.updateTopic',
        'uses' => 'CommunityController@updateTopic',
        'middleware' => 'staffHasPermission:manage_community'
    ]);

    Route::any('/delete/topic/{id}', [
        'as' => 'backend.deleteTopic',
        'uses' => 'CommunityController@deleteTopic',
        'middleware' => 'staffHasPermission:manage_community'
    ]);

    Route::get('/article/list', [
        'as' => 'backend.articleList',
        'uses' => 'ArticleController@articleList',
        'middleware' => 'staffHasPermission:manage_article'
    ]);

    Route::get('/article/edit/{id}', [
        'as' => 'backend.editArticle',
        'uses' => 'ArticleController@edit',
        'middleware' => 'staffHasPermission:manage_article'
    ]);

    Route::post('/article/update/{id}', [
        'as' => 'backend.updateArticle',
        'uses' => 'ArticleController@updateArticle',
        'middleware' => 'staffHasPermission:manage_article'
    ]);

    Route::get('/article/delete/{id}', [
        'as' => 'backend.deleteArticle',
        'uses' => 'ArticleController@deleteArticle',
        'middleware' => 'staffHasPermission:manage_article'
    ]);

    Route::post('/create/article', [
        'as' => 'backend.createArticle',
        'uses' => 'ArticleController@saveArticle',
        'middleware' => 'staffHasPermission:manage_article'
    ]);

    Route::get('/bulletin/create', [
        'as' => 'backend.bulletin.create',
        'uses' => 'BulletinController@create',
        'middleware' => 'staffHasPermission:manage_bullet_ins'
    ]);

    Route::post('/bulletin/create', [
        'as' => 'backend.bulletin.createBulletin',
        'uses' => 'BulletinController@createBulletin',
        'middleware' => 'staffHasPermission:manage_bullet_ins'
    ]);

    Route::get('/bulletin/edit/{id}', [
        'as' => 'backend.bulletin.editBulletin',
        'uses' => 'BulletinController@editBulletin',
        'middleware' => 'staffHasPermission:manage_bullet_ins'
    ]);

    Route::post('/bulletin/edit/{id}', [
        'as' => 'backend.bulletin.updateBulletin',
        'uses' => 'BulletinController@updateBulletin',
        'middleware' => 'staffHasPermission:manage_bullet_ins'
    ]);

    //dispute routes
    Route::get('/disputes', [
        'as' => 'backend.disputes',
        'uses' => 'DisputeController@index',
        'middleware' => 'staffHasPermission:dispute_requests'
    ]);

    Route::get('/dispute/{disputeId}', [
        'as' => 'backend.disputes.view',
        'uses' => 'DisputeController@view',
        'middleware' => 'staffHasPermission:dispute_requests'
    ]);

    Route::get('/milestones.php-confirmation', [
        'as' => 'backend.milestone_confirmation',
        'uses' => 'MilestonesControllers@milestonesConfirmation',
        'middleware' => 'staffHasPermission:milestone_confirmation'
    ]);

    Route::get('/change/date-time', [
        'as' => 'backend.changeDateTime',
        'uses' => 'MilestonesControllers@changeDateTime',
        'middleware' => 'staffHasPermission:milestone_confirmation'
    ]);

    Route::get('/change/all/date-time', [
        'as' => 'backend.changeAllDateTime',
        'uses' => 'MilestonesControllers@changeAllDateTime',
        'middleware' => 'staffHasPermission:milestone_confirmation'
    ]);

    Route::get('/change/all/date-time/un-verified', [
        'as' => 'backend.changeAllDateTimeUnVerified',
        'uses' => 'MilestonesControllers@changeAllDateTimeUnVerified',
        'middleware' => 'staffHasPermission:milestone_confirmation'
    ]);

    Route::get('/in-progress/milestones', [
        'as' => 'backend.show_milestone',
        'uses' => 'MilestonesControllers@milestonesShow',
        'middleware' => 'staffHasPermission:milestone_confirmation'
    ]);

    Route::get('/milestone/approve/', [
        'as' => 'backend.milestone_confirmation_approve',
        'uses' => 'MilestonesControllers@approveMilestone',
        'middleware' => 'staffHasPermission:milestone_confirmation'
    ]);

    Route::get('/dispute/{disputeId}/history/{userId}/view', [
        'as' => 'backend.disputes.history',
        'uses' => 'DisputeController@history',
        'middleware' => 'staffHasPermission:dispute_requests'
    ]);

    Route::post('approve-dispute-admin', [
        'as' => 'backend.disputes.history',
        'uses' => 'DisputeController@approveDispute',
        'middleware' => 'staffHasPermission:dispute_requests'
    ]);


    Route::post('reject-dispute-admin', [
        'as' => 'backend.disputes.history',
        'uses' => 'DisputeController@rejectDispute',
        'middleware' => 'staffHasPermission:dispute_requests'
    ]);

    Route::get('/badwords', [
        'as' => 'backend.badwords',
        'uses' => 'BadwordsController@index',
        'middleware' => 'staffHasPermission:manage_badwords'
    ]);

    //manage_permission_groups related
    Route::get('/permissions', ['as' => 'backend.permissions', 'uses' => 'PermissionController@index', 'middleware' => 'staffHasPermission:manage_permission_groups']);
    Route::get('/permissions/dt', ['as' => 'backend.permissions.dt', 'uses' => 'PermissionController@indexDt', 'middleware' => 'staffHasPermission:manage_permission_groups']);
    Route::get('/permissions/create', ['as' => 'backend.permissions.create', 'uses' => 'PermissionController@create', 'middleware' => 'staffHasPermission:manage_permission_groups']);
    Route::post('/permissions/create', ['as' => 'backend.permissions.create.post', 'uses' => 'PermissionController@createPost', 'middleware' => 'staffHasPermission:manage_permission_groups']);
    Route::get('/permissions/edit/{id}', ['as' => 'backend.permissions.edit', 'uses' => 'PermissionController@edit', 'middleware' => 'staffHasPermission:manage_permission_groups']);
    Route::post('/permissions/edit/{id}', ['as' => 'backend.permissions.edit.post', 'uses' => 'PermissionController@editPost', 'middleware' => 'staffHasPermission:manage_permission_groups']);
    Route::post('/permissions/delete/{id}', ['as' => 'backend.permissions.delete', 'uses' => 'PermissionController@delete', 'middleware' => 'staffHasPermission:manage_permission_groups']);

    //manage_staff related
    Route::get('/staff', ['as' => 'backend.staff', 'uses' => 'StaffController@index', 'middleware' => 'staffHasPermission:manage_staff']);
    Route::get('/staff/revert/trashed/user/{id}', ['as' => 'backend.revert.trashed.users', 'uses' => 'StaffController@revertUsers', 'middleware' => 'staffHasPermission:manage_staff']);
    Route::get('/staff/add-phone-number', ['as' => 'backend.addPhoneNumber', 'uses' => 'StaffController@setupPhone', 'middleware' => 'staffHasPermission:manage_sharedoffice']);
    Route::post('/staff/create-phone-number', ['as' => 'backend.createPhoneNumber', 'uses' => 'StaffController@createPhoneNumber', 'middleware' => 'staffHasPermission:manage_staff']);
    Route::get('/staff/dt', ['as' => 'backend.staff.dt', 'uses' => 'StaffController@indexDt', 'middleware' => 'staffHasPermission:manage_staff']);
    Route::get('/staff/create', ['as' => 'backend.staff.create', 'uses' => 'StaffController@create', 'middleware' => 'staffHasPermission:manage_staff']);
    Route::post('/staff/create', ['as' => 'backend.staff.create.post', 'uses' => 'StaffController@createPost', 'middleware' => 'staffHasPermission:manage_staff']);
    Route::get('/staff/edit/{id}', ['as' => 'backend.staff.edit', 'uses' => 'StaffController@edit', 'middleware' => 'staffHasPermission:manage_staff']);
    Route::post('/staff/edit/{id}', ['as' => 'backend.staff.edit.post', 'uses' => 'StaffController@editPost', 'middleware' => 'staffHasPermission:manage_staff']);
    Route::post('/staff/delete/{id}', ['as' => 'backend.staff.delete', 'uses' => 'StaffController@delete', 'middleware' => 'staffHasPermission:manage_staff']);

    Route::get('/use/identity/management', ['as' => 'backend.user_identity_management', 'uses' => 'UserController@userIdentityManagement', 'middleware' => 'staffHasPermission:manage_user']);
    Route::get('/use/identity/management/{user_id}', ['as' => 'backend.user_identity_management_detail', 'uses' => 'UserController@userIdentityManagementDetail', 'middleware' => 'staffHasPermission:manage_user']);
    Route::get('/use/identity/management/approve/{id}', ['as' => 'backend.user_identity_management_approve', 'uses' => 'UserController@userIdentityManagementDetailApprove', 'middleware' => 'staffHasPermission:manage_user']);
    Route::get('/use/identity/management/reject/{id}', ['as' => 'backend.user_identity_management_reject', 'uses' => 'UserController@userIdentityManagementDetailReject', 'middleware' => 'staffHasPermission:manage_user']);

    //manage_member related
    Route::get('/user/activity',[
        'as' => 'backend.userActivity',
        'uses' => 'UserController@userActivity',
        'middleware' => 'staffHasPermission:manage_user'
    ]);
    Route::get('/user', ['as' => 'backend.user', 'uses' => 'UserController@index', 'middleware' => 'staffHasPermission:manage_user']);
    Route::get('/user/dt', ['as' => 'backend.user.dt', 'uses' => 'UserController@indexDt', 'middleware' => 'staffHasPermission:manage_user']);
    Route::get('/user/create', ['as' => 'backend.user.create', 'uses' => 'UserController@create', 'middleware' => 'staffHasPermission:manage_user']);
    Route::post('/user/create', ['as' => 'backend.user.create.post', 'uses' => 'UserController@createPost', 'middleware' => 'staffHasPermission:manage_user']);
    Route::get('/user/edit/{member_id}', ['as' => 'backend.user.edit', 'uses' => 'UserController@edit', 'middleware' => 'staffHasPermission:manage_user']);
    Route::post('/user/edit/{member_id}', ['as' => 'backend.user.edit.post', 'uses' => 'UserController@editPost', 'middleware' => 'staffHasPermission:manage_user']);
    Route::post('/user/delete/{member_id}', ['as' => 'backend.user.delete', 'uses' => 'UserController@delete', 'middleware' => 'staffHasPermission:manage_user']);
     // Route::get('/user/info/{member_id}', ['as' => 'backend.user.info', 'uses' => 'UserController@info', 'middleware' => 'staffHasPermission:manage_user']);
     // Route::get('/user/rating/{member_id}', ['as' => 'backend.user.rating', 'uses' => 'UserController@rating', 'middleware' => 'staffHasPermission:manage_user']);
    //TODO CHECK THIS RATING ROUTES
    Route::get('/user/rating_dt', ['as' => 'backend.user.rating.dt', 'uses' => 'UserController@ratingDt', 'middleware' => 'staffHasPermission:manage_user']);
    Route::get('/user/rating/edit/{rating_id}', ['as' => 'backend.user.rating.edit', 'uses' => 'UserController@ratingEdit', 'middleware' => 'staffHasPermission:manage_user']);
    Route::post('/user/rating/edit/{rating_id}', ['as' => 'backend.user.rating.edit.post', 'uses' => 'UserController@ratingEditPost', 'middleware' => 'staffHasPermission:manage_user']);
    Route::post('/user/rating/delete/{rating_id}', ['as' => 'backend.user.rating.delete', 'uses' => 'UserController@ratingDelete', 'middleware' => 'staffHasPermission:manage_user']);
    Route::get('/user/sendmessage/{id}', ['as' => 'backend.user.sendmessage', 'uses' => 'UserController@sendMessage', 'middleware' => 'staffHasPermission:manage_user']);
    Route::post('/user/sendmessage/{id}', ['as' => 'backend.user.sendmessage.post', 'uses' => 'UserController@sendMessagePost', 'middleware' => 'staffHasPermission:manage_user']);
    Route::get('/user/sendcoin/{id}', ['as' => 'backend.user.sendcoin', 'uses' => 'UserController@sendCoin', 'middleware' => 'staffHasPermission:manage_user']);
    Route::post('/user/sendcoin/{id}', ['as' => 'backend.user.sendcoin.post', 'uses' => 'UserController@sendCoinPost', 'middleware' => 'staffHasPermission:manage_user']);

    // submit ID
    Route::get('/user/submit_id', ['as' => 'backend.user.submitid', 'uses' => 'UserController@submitID', 'middleware' => 'staffHasPermission:submit_id']);
    Route::get('/user/submit_id_dt', ['as' => 'backend.user.submitid.dt', 'uses' => 'UserController@submitIDDt', 'middleware' => 'staffHasPermission:submit_id']);
    Route::post('/user/approve/{request_id}', ['as' => 'backend.user.approve.id', 'uses' => 'UserController@approveId', 'middleware' => 'staffHasPermission:submit_id']);
    Route::get('/user/reject/{request_id}', ['as' => 'backend.user.reject', 'uses' => 'UserController@reject', 'middleware' => 'staffHasPermission:submit_id']);
    Route::post('/user/reject/{request_id}', ['as' => 'backend.user.reject.post', 'uses' => 'UserController@rejectPost', 'middleware' => 'staffHasPermission:submit_id']);
    Route::get('/user/identityInfo/{request_id}', ['as' => 'backend.user.identityInfo', 'uses' => 'UserController@identityInfo', 'middleware' => 'staffHasPermission:submit_id']);

    // Tailor Request
    // Route::get('/user/tailor_request', ['as' => 'backend.user.tailor.request', 'uses' => 'UserController@tailorRequest', 'middleware' => 'staffHasPermission:tailor_request']);
    // Route::get('/user/tailor_request_dt', ['as' => 'backend.user.tailor.request.dt', 'uses' => 'UserController@tailorRequestDt', 'middleware' => 'staffHasPermission:tailor_request']);
    // Route::post('/user/approve_tailor/{request_id}', ['as' => 'backend.user.approve.tailor.id', 'uses' => 'UserController@approveTailor', 'middleware' => 'staffHasPermission:tailor_request']);
    // Route::post('/user/reject_tailor/{request_id}', ['as' => 'backend.user.reject.tailor.id', 'uses' => 'UserController@rejectTailor', 'middleware' => 'staffHasPermission:tailor_request']);


    //manage_category related
    Route::get('/category/index/{id?}', ['as' => 'backend.category', 'uses' => 'CategoryController@index', 'middleware' => 'staffHasPermission:manage_category']);
    Route::get('/category/dt/{id?}', ['as' => 'backend.category.dt', 'uses' => 'CategoryController@indexDt', 'middleware' => 'staffHasPermission:manage_category']);
    Route::get('/category/create/{id?}', ['as' => 'backend.category.create', 'uses' => 'CategoryController@create', 'middleware' => 'staffHasPermission:manage_category']);
    Route::post('/category/create/{id?}', ['as' => 'backend.category.create.post', 'uses' => 'CategoryController@createPost', 'middleware' => 'staffHasPermission:manage_category']);
    Route::get('/category/edit/{id}', ['as' => 'backend.category.edit', 'uses' => 'CategoryController@edit', 'middleware' => 'staffHasPermission:manage_category']);
    Route::post('/category/edit/{id}', ['as' => 'backend.category.edit.post', 'uses' => 'CategoryController@editPost', 'middleware' => 'staffHasPermission:manage_category']);
    Route::post('/category/delete/{id}', ['as' => 'backend.category.delete', 'uses' => 'CategoryController@delete', 'middleware' => 'staffHasPermission:manage_category']);

    //Pending Withdrawls
    Route::get('/pending-withdrawals', [
        'as' => 'backend.pendingwithdrawals',
        'uses' => 'PendingWithdrawals@index',
        'middleware' => 'staffHasPermission:manage_payments'
    ]);
    Route::get('/pending-withdrawals/approve-payment/{id}', [
        'as' => 'backend.approvepayment',
        'uses' => 'PendingWithdrawals@approvePayment',
        'middleware' => 'staffHasPermission:manage_payments'
    ]);
    Route::get('/pending-withdrawals/decline-payment/{id}', [
        'as' => 'backend.declinepayment',
        'uses' => 'PendingWithdrawals@declinePayment',
        'middleware' => 'staffHasPermission:manage_payments'
    ]);

    //manage_skill related
    Route::get('/skill', ['as' => 'backend.skill', 'uses' => 'SkillController@index', 'middleware' => 'staffHasPermission:manage_skill']);
    Route::get('/skill/dt', ['as' => 'backend.skill.dt', 'uses' => 'SkillController@indexDt', 'middleware' => 'staffHasPermission:manage_skill']);
    Route::get('/skill/create', ['as' => 'backend.skill.create', 'uses' => 'SkillController@create', 'middleware' => 'staffHasPermission:manage_skill']);
    Route::post('/skill/create', ['as' => 'backend.skill.create.post', 'uses' => 'SkillController@createPost', 'middleware' => 'staffHasPermission:manage_skill']);
    Route::get('/skill/edit/{id}', ['as' => 'backend.skill.edit', 'uses' => 'SkillController@edit', 'middleware' => 'staffHasPermission:manage_skill']);
    Route::post('/skill/edit/{id}', ['as' => 'backend.skill.edit.post', 'uses' => 'SkillController@editPost', 'middleware' => 'staffHasPermission:manage_skill']);
    Route::post('/skill/delete/{id}', ['as' => 'backend.skill.delete', 'uses' => 'SkillController@delete', 'middleware' => 'staffHasPermission:manage_skill']);
    Route::post('/skill/restore/{id}', ['as' => 'backend.skill.restore', 'uses' => 'SkillController@restore', 'middleware' => 'staffHasPermission:manage_skill']);

    //manage_skill keyword related
    Route::get('/skillkeyword', ['as' => 'backend.skillkeyword', 'uses' => 'SkillKeywordController@index', 'middleware' => 'staffHasPermission:manage_skill_keyword']);
    Route::get('/skillkeyword/dt', ['as' => 'backend.skillkeyword.dt', 'uses' => 'SkillKeywordController@indexDtt', 'middleware' => 'staffHasPermission:manage_skill_keyword']);
    Route::get('/skillkeyword/delete/{id}', ['as' => 'backend.skillkeyword.delete', 'uses' => 'SkillKeywordController@delete', 'middleware' => 'staffHasPermission:manage_skill_keyword']);

    Route::get('/skillkeyword/bulkDelete', ['as' => 'backend.skillkeyword.bulkDelete', 'uses' => 'SkillKeywordController@bulkDelete', 'middleware' => 'staffHasPermission:manage_skill_keyword']);

    Route::post('/skillkeyword/ban/{id}', ['as' => 'backend.skillkeyword.ban', 'uses' => 'SkillKeywordController@ban', 'middleware' => 'staffHasPermission:manage_skill_keyword']);
    Route::get('/skillkeyword/approve/{id}', ['as' => 'backend.skillkeyword.approve', 'uses' => 'SkillKeywordController@approve', 'middleware' => 'staffHasPermission:manage_skill_keyword']);
    Route::post('/skillkeyword/approve/{id}', ['as' => 'backend.skillkeyword.approve.post', 'uses' => 'SkillKeywordController@approvePost', 'middleware' => 'staffHasPermission:manage_skill_keyword']);

    //manage_project related
    Route::get('/project', ['as' => 'backend.project', 'uses' => 'ProjectController@index', 'middleware' => 'staffHasPermission:manage_project']);
    Route::get('/project/dt', ['as' => 'backend.project.dt', 'uses' => 'ProjectController@indexDt', 'middleware' => 'staffHasPermission:manage_project']);
    Route::get('/project/create', ['as' => 'backend.project.create', 'uses' => 'ProjectController@create', 'middleware' => 'staffHasPermission:manage_project']);
    Route::post('/project/create', ['as' => 'backend.project.create.post', 'uses' => 'ProjectController@createPost', 'middleware' => 'staffHasPermission:manage_project']);
    Route::get('/project/edit/{id}', ['as' => 'backend.project.edit', 'uses' => 'ProjectController@edit', 'middleware' => 'staffHasPermission:manage_project']);
    Route::get('/project/leave-message/{id}', ['as' => 'backend.project.leave_message', 'uses' => 'ProjectController@leaveMessageGet', 'middleware' => 'staffHasPermission:manage_project']);
    Route::post('/project/leave-message/{id}', ['as' => 'backend.project.leave_message_post', 'uses' => 'ProjectController@leaveMessagePost']);
    Route::post('/project/edit/{id}', ['as' => 'backend.project.edit.post', 'uses' => 'ProjectController@editPost', 'middleware' => 'staffHasPermission:manage_project']);
    Route::post('/project/delete/{id}', ['as' => 'backend.project.delete', 'uses' => 'ProjectController@delete', 'middleware' => 'staffHasPermission:manage_project']);
    Route::post('/project/hide/{id}', ['as' => 'backend.project.hide', 'uses' => 'ProjectController@hide', 'middleware' => 'staffHasPermission:manage_project']);

    //manage_job position related
    Route::get('/jobposition', ['as' => 'backend.jobposition', 'uses' => 'JobPositionController@index', 'middleware' => 'staffHasPermission:manage_job_position']);
    Route::get('/jobposition/dt', ['as' => 'backend.jobposition.dt', 'uses' => 'JobPositionController@indexDt', 'middleware' => 'staffHasPermission:manage_job_position']);
    Route::get('/jobposition/create', ['as' => 'backend.jobposition.create', 'uses' => 'JobPositionController@create', 'middleware' => 'staffHasPermission:manage_job_position']);
    Route::post('/jobposition/create', ['as' => 'backend.jobposition.create.post', 'uses' => 'JobPositionController@createPost', 'middleware' => 'staffHasPermission:manage_job_position']);
    Route::get('/jobposition/edit/{id}', ['as' => 'backend.jobposition.edit', 'uses' => 'JobPositionController@edit', 'middleware' => 'staffHasPermission:manage_job_position']);
    Route::post('/jobposition/edit/{id}', ['as' => 'backend.jobposition.edit.post', 'uses' => 'JobPositionController@editPost', 'middleware' => 'staffHasPermission:manage_job_position']);
    Route::post('/jobposition/delete/{id}', ['as' => 'backend.jobposition.delete', 'uses' => 'JobPositionController@delete', 'middleware' => 'staffHasPermission:manage_job_position']);

    //manage_bank related
    Route::get('/bank', ['as' => 'backend.bank', 'uses' => 'BankController@index', 'middleware' => 'staffHasPermission:manage_bank']);
    Route::get('/bank/dt', ['as' => 'backend.bank.dt', 'uses' => 'BankController@indexDt', 'middleware' => 'staffHasPermission:manage_bank']);
    Route::get('/bank/create', ['as' => 'backend.bank.create', 'uses' => 'BankController@create', 'middleware' => 'staffHasPermission:manage_bank']);
    Route::post('/bank/create', ['as' => 'backend.bank.create.post', 'uses' => 'BankController@createPost', 'middleware' => 'staffHasPermission:manage_bank']);
    Route::get('/bank/edit/{id}', ['as' => 'backend.bank.edit', 'uses' => 'BankController@edit', 'middleware' => 'staffHasPermission:manage_bank']);
    Route::post('/bank/edit/{id}', ['as' => 'backend.bank.edit.post', 'uses' => 'BankController@editPost', 'middleware' => 'staffHasPermission:manage_bank']);
    Route::post('/bank/delete/{id}', ['as' => 'backend.bank.delete', 'uses' => 'BankController@delete', 'middleware' => 'staffHasPermission:manage_bank']);

    //manage_raw_material related
    // Route::get('/rawmaterial', ['as' => 'backend.rawmaterial', 'uses' => 'RawMaterialController@index', 'middleware' => 'staffHasPermission:manage_raw_material']);
    // Route::get('/rawmaterial/dt', ['as' => 'backend.rawmaterial.dt', 'uses' => 'RawMaterialController@indexDt', 'middleware' => 'staffHasPermission:manage_raw_material']);
    // Route::get('/rawmaterial/create', ['as' => 'backend.rawmaterial.create', 'uses' => 'RawMaterialController@create', 'middleware' => 'staffHasPermission:manage_raw_material']);
    // Route::post('/rawmaterial/create', ['as' => 'backend.rawmaterial.create.post', 'uses' => 'RawMaterialController@createPost', 'middleware' => 'staffHasPermission:manage_raw_material']);
    // Route::get('/rawmaterial/edit/{id}', ['as' => 'backend.rawmaterial.edit', 'uses' => 'RawMaterialController@edit', 'middleware' => 'staffHasPermission:manage_raw_material']);
    // Route::post('/rawmaterial/edit/{id}', ['as' => 'backend.rawmaterial.edit.post', 'uses' => 'RawMaterialController@editPost', 'middleware' => 'staffHasPermission:manage_raw_material']);
    // Route::post('/rawmaterial/delete/{id}', ['as' => 'backend.rawmaterial.delete', 'uses' => 'RawMaterialController@delete', 'middleware' => 'staffHasPermission:manage_raw_material']);

    //manage_order related
    // Route::get('/order', ['as' => 'backend.order', 'uses' => 'OrderController@index', 'middleware' => 'staffHasPermission:manage_order']);
    //   Route::get('/order/dt', ['as' => 'backend.order.dt', 'uses' => 'OrderController@indexDt', 'middleware' => 'staffHasPermission:manage_order']);

    // admin conflict support related
    // Route::get('/order/conflict', ['as' => 'backend.order.conflict', 'uses' => 'OrderController@conflict', 'middleware' => 'staffHasPermission:manage_order']);
    // Route::get('/order/conflict/dt', ['as' => 'backend.order.conflict.dt', 'uses' => 'OrderController@conflictDt', 'middleware' => 'staffHasPermission:manage_order']);
    // Route::get('/order/conflict/conversation/{order_id}', ['as' => 'backend.order.conflict.conversation', 'uses' => 'OrderController@conflictConversation', 'middleware' => 'staffHasPermission:manage_order']);
    // Route::get('/order/conflict/pullOrderChat/{id}', ['as' => 'backend.order.conflict.pullorderchat', 'uses' => 'OrderController@pullOrderChat']);
    // Route::post('/order/conflict/post-chat', ['as' => 'backend.order.conflict.postorderchat', 'uses' => 'OrderController@postOrderChat']);
    // Route::post('/order/conflict/post-chat-image', ['as' => 'backend.order.conflict.postorderchatimage', 'uses' => 'OrderController@postOrderChatImage']);
    // Route::get('/order/conflict/refund/{order_id}', ['as' => 'backend.order.conflict.refund', 'uses' => 'OrderController@conflictRefund', 'middleware' => 'staffHasPermission:manage_order']);
    // Route::get('/order/conflict/release/{order_id}', ['as' => 'backend.order.conflict.release', 'uses' => 'OrderController@conflictRelease', 'middleware' => 'staffHasPermission:manage_order']);
    // Route::post('/order/conflict/forward', ['as' => 'backend.order.conflict.forward', 'uses' => 'OrderController@conflictForward', 'middleware' => 'staffHasPermission:manage_order']);

    // Manage withdraw
    Route::get('/withdraw', ['as' => 'backend.withdraw', 'uses' => 'WithdrawController@index', 'middleware' => 'staffHasPermission:manage_withdraw']);
    Route::get('/withdraw/dt', ['as' => 'backend.withdraw.dt', 'uses' => 'WithdrawController@indexDt', 'middleware' => 'staffHasPermission:manage_withdraw']);
    Route::post('/withdraw/approve/{request_id}', ['as' => 'backend.withdraw.approve', 'uses' => 'WithdrawController@approve', 'middleware' => 'staffHasPermission:manage_withdraw']);
    Route::post('/withdraw/reject/{request_id}', ['as' => 'backend.withdraw.reject', 'uses' => 'WithdrawController@reject', 'middleware' => 'staffHasPermission:manage_withdraw']);

    //manage_bank related
    Route::get('/level', ['as' => 'backend.level', 'uses' => 'LevelController@index', 'middleware' => 'staffHasPermission:manage_level']);
    Route::get('/level/dt', ['as' => 'backend.level.dt', 'uses' => 'LevelController@indexDt', 'middleware' => 'staffHasPermission:manage_level']);
    Route::get('/level/create', ['as' => 'backend.level.create', 'uses' => 'LevelController@create', 'middleware' => 'staffHasPermission:manage_level']);
    Route::post('/level/create', ['as' => 'backend.level.create.post', 'uses' => 'LevelController@createPost', 'middleware' => 'staffHasPermission:manage_level']);
    Route::get('/level/edit/{id}', ['as' => 'backend.level.edit', 'uses' => 'LevelController@edit', 'middleware' => 'staffHasPermission:manage_level']);
    Route::post('/level/edit/{id}', ['as' => 'backend.level.edit.post', 'uses' => 'LevelController@editPost', 'middleware' => 'staffHasPermission:manage_level']);
    Route::post('/level/delete/{id}', ['as' => 'backend.level.delete', 'uses' => 'LevelController@delete', 'middleware' => 'staffHasPermission:manage_level']);

    //themes panel related
    Route::get('/themes', ['as' => 'backend.themes', 'uses' => 'ThemesController@index', 'middleware' => 'staffHasPermission:manage_themes']);
    //TODO LAST here
    //manage_slider related
    Route::get('/themes/slider', ['as' => 'backend.themes.slider', 'uses' => 'SliderController@index', 'middleware' => 'staffHasPermission:manage_sliders']);
    Route::get('/themes/slider/dt', ['as' => 'backend.themes.slider.dt', 'uses' => 'SliderController@indexDt', 'middleware' => 'staffHasPermission:manage_sliders']);
    Route::get('/themes/slider/create', ['as' => 'backend.themes.slider.create', 'uses' => 'SliderController@create', 'middleware' => 'staffHasPermission:manage_sliders']);
    Route::post('/themes/slider/create', ['as' => 'backend.themes.slider.create.post', 'uses' => 'SliderController@createPost', 'middleware' => 'staffHasPermission:manage_sliders']);
    Route::get('/themes/slider/edit/{id}', ['as' => 'backend.themes.slider.edit', 'uses' => 'SliderController@edit', 'middleware' => 'staffHasPermission:manage_sliders']);
    Route::post('/themes/slider/edit/{id}', ['as' => 'backend.themes.slider.edit.post', 'uses' => 'SliderController@editPost', 'middleware' => 'staffHasPermission:manage_sliders']);
    Route::post('/themes/slider/delete/{id}', ['as' => 'backend.themes.slider.delete', 'uses' => 'SliderController@delete', 'middleware' => 'staffHasPermission:manage_sliders']);

    //manage_news related
    Route::get('/themes/news', ['as' => 'backend.themes.news', 'uses' => 'NewsController@index', 'middleware' => 'staffHasPermission:manage_news']);
    Route::get('/themes/news/dt', ['as' => 'backend.themes.news.dt', 'uses' => 'NewsController@indexDt', 'middleware' => 'staffHasPermission:manage_news']);
    Route::get('/themes/news/create', ['as' => 'backend.themes.news.create', 'uses' => 'NewsController@create', 'middleware' => 'staffHasPermission:manage_news']);
    Route::post('/themes/news/create', ['as' => 'backend.themes.news.create.post', 'uses' => 'NewsController@createPost', 'middleware' => 'staffHasPermission:manage_news']);
    Route::get('/themes/news/edit/{id}', ['as' => 'backend.themes.news.edit', 'uses' => 'NewsController@edit', 'middleware' => 'staffHasPermission:manage_news']);
    Route::post('/themes/news/edit/{id}', ['as' => 'backend.themes.news.edit.post', 'uses' => 'NewsController@editPost', 'middleware' => 'staffHasPermission:manage_news']);
    Route::post('/themes/news/delete/{id}', ['as' => 'backend.themes.news.delete', 'uses' => 'NewsController@delete', 'middleware' => 'staffHasPermission:manage_news']);

    //manage_official_project related
    Route::get('/officialproject', ['as' => 'backend.officialproject', 'uses' => 'OfficialProjectController@index', 'middleware' => 'staffHasPermission:manage_official_project']);
    Route::get('/officialproject/dt', ['as' => 'backend.officialproject.dt', 'uses' => 'OfficialProjectController@indexDt', 'middleware' => 'staffHasPermission:manage_official_project']);
    Route::get('/officialproject/create', ['as' => 'backend.officialproject.create', 'uses' => 'OfficialProjectController@create', 'middleware' => 'staffHasPermission:manage_official_project']);
    Route::post('/officialproject/create', ['as' => 'backend.officialproject.create.post', 'uses' => 'OfficialProjectController@createPost', 'middleware' => 'staffHasPermission:manage_official_project']);
    Route::get('/officialproject/edit/{id}', ['as' => 'backend.officialproject.edit', 'uses' => 'OfficialProjectController@edit', 'middleware' => 'staffHasPermission:manage_official_project']);
    Route::post('/officialproject/edit/{id}', ['as' => 'backend.officialproject.edit.post', 'uses' => 'OfficialProjectController@editPost', 'middleware' => 'staffHasPermission:manage_official_project']);
    Route::post('/officialproject/delete/{id}', ['as' => 'backend.officialproject.delete.post', 'uses' => 'OfficialProjectController@deletePost', 'middleware' => 'staffHasPermission:manage_official_project']);
    Route::get('/officialproject/applicant/{id}', ['as' => 'backend.officialproject.applicant', 'uses' => 'OfficialProjectController@applicant', 'middleware' => 'staffHasPermission:manage_official_project']);
    Route::get('/officialproject/applicant/dt/{id}', ['as' => 'backend.officialproject.applicant.dt', 'uses' => 'OfficialProjectController@applicantDt', 'middleware' => 'staffHasPermission:manage_official_project']);
    Route::post('/officialproject/applicant/awardjob/{applicantid}', ['as' => 'backend.officialproject.applicant.awardjob', 'uses' => 'OfficialProjectController@awardJob', 'middleware' => 'staffHasPermission:manage_official_project']);
    Route::post('/officialproject/applicant/reject/{applicantid}', ['as' => 'backend.officialproject.applicant.reject', 'uses' => 'OfficialProjectController@reject', 'middleware' => 'staffHasPermission:manage_official_project']);
    Route::get('/officialproject/applicant/info/{applicantid}', ['as' => 'backend.officialproject.applicant.info', 'uses' => 'OfficialProjectController@info', 'middleware' => 'staffHasPermission:manage_official_project']);
    Route::get('/officialproject/applicant/sendmessage/{applicantid}', ['as' => 'backend.officialproject.applicant.sendmessage', 'uses' => 'OfficialProjectController@sendMessage', 'middleware' => 'staffHasPermission:manage_official_project']);
    Route::post('/officialproject/applicant/sendmessage/{applicantid}', ['as' => 'backend.officialproject.applicant.sendmessage.post', 'uses' => 'OfficialProjectController@sendMessagePost', 'middleware' => 'staffHasPermission:manage_official_project']);
    Route::get('/officialproject/applicant/endcontract/{applicantid}', ['as' => 'backend.officialproject.applicant.endcontract', 'uses' => 'OfficialProjectController@endContract', 'middleware' => 'staffHasPermission:manage_official_project']);
    Route::post('/officialproject/applicant/endcontract', ['as' => 'backend.officialproject.applicant.endcontract.post', 'uses' => 'OfficialProjectController@endContractPost', 'middleware' => 'staffHasPermission:manage_official_project']);
    Route::get('/officialproject/news/list/{project_id}', ['as' => 'backend.officialproject.news.list', 'uses' => 'OfficialProjectController@news', 'middleware' => 'staffHasPermission:manage_official_project']);
    Route::get('/officialproject/news/dt/{project_id}', ['as' => 'backend.officialproject.news.dt', 'uses' => 'OfficialProjectController@newsDt', 'middleware' => 'staffHasPermission:manage_official_project']);
    Route::get('/officialproject/news/create/{project_id}', ['as' => 'backend.officialproject.news.create', 'uses' => 'OfficialProjectController@createNews', 'middleware' => 'staffHasPermission:manage_official_project']);
    Route::post('/officialproject/news/create', ['as' => 'backend.officialproject.news.create.post', 'uses' => 'OfficialProjectController@createNewsPost', 'middleware' => 'staffHasPermission:manage_official_project']);
    Route::get('/officialproject/news/edit/{id}', ['as' => 'backend.officialproject.news.edit', 'uses' => 'OfficialProjectController@editNews', 'middleware' => 'staffHasPermission:manage_official_project']);
    Route::post('/officialproject/news/edit/{id}', ['as' => 'backend.officialproject.news.edit.post', 'uses' => 'OfficialProjectController@editNewsPost', 'middleware' => 'staffHasPermission:manage_official_project']);
    Route::post('/officialproject/news/delete/{id}', ['as' => 'backend.officialproject.news.delete.post', 'uses' => 'OfficialProjectController@deleteNewsPost', 'middleware' => 'staffHasPermission:manage_official_project']);

    //manage skill level related
    Route::get('/leveltranslations', ['as' => 'backend.leveltranslations', 'uses' => 'LevelTranslationController@index']);
    Route::get('/leveltranslations/dt', ['as' => 'backend.leveltranslations.dt', 'uses' => 'LevelTranslationController@indexDt']);
    Route::get('/leveltranslations/create', ['as' => 'backend.leveltranslations.create', 'uses' => 'LevelTranslationController@create', 'middleware' => 'staffHasPermission:manage_leveltranslations']);
    Route::post('/leveltranslations/create', ['as' => 'backend.leveltranslations.create.post', 'uses' => 'LevelTranslationController@createPost', 'middleware' => 'staffHasPermission:manage_leveltranslations']);
    Route::get('/leveltranslations/edit/{id}', ['as' => 'backend.leveltranslations.edit', 'uses' => 'LevelTranslationController@edit', 'middleware' => 'staffHasPermission:manage_leveltranslations']);
    Route::post('/leveltranslations/edit/{id}', ['as' => 'backend.leveltranslations.edit.post', 'uses' => 'LevelTranslationController@editPost', 'middleware' => 'staffHasPermission:manage_leveltranslations']);
    Route::post('/leveltranslations/delete/{id}', ['as' => 'backend.leveltranslations.delete', 'uses' => 'LevelTranslationController@delete', 'middleware' => 'staffHasPermission:manage_leveltranslations']);
    //manage_skill_review related
    Route::get('/skillreview', ['as' => 'backend.skillreview', 'uses' => 'SkillReviewController@index', 'middleware' => 'staffHasPermission:manage_skill_review']);
    Route::get('/skillreview/dt', ['as' => 'backend.skillreview.dt', 'uses' => 'SkillReviewController@indexDt', 'middleware' => 'staffHasPermission:manage_skill_review']);
    Route::get('/skillreview/view/{id}', ['as' => 'backend.skillreview.view', 'uses' => 'SkillReviewController@view', 'middleware' => 'staffHasPermission:manage_skill_review']);
    // Route::post('/skillreview/view/{id}', ['as' => 'backend.skillreview.view.post', 'uses' => 'SkillReviewController@viewPost', 'middleware' => 'staffHasPermission:manage_skill_review']);
    Route::get('/skillreview/comment/{id}/create', ['as' => 'backend.skillreview.comment.create', 'uses' => 'SkillReviewController@commentCreate']);
    Route::post('/skillreview/comment/{id}/create', ['as' => 'backend.skillreview.comment.create.post', 'uses' => 'SkillReviewController@commentCreatePost']);
    Route::get('/skillreview/comment/{id}/edit/{cid}', ['as' => 'backend.skillreview.comment.edit', 'uses' => 'SkillReviewController@commentEdit']);
    Route::post('/skillreview/comment/{id}/edit/{cid}', ['as' => 'backend.skillreview.comment.edit.post', 'uses' => 'SkillReviewController@commentEditPost']);
    Route::get('/skillreview/comment/{id}/dt', ['as' => 'backend.skillreview.comment.dt', 'uses' => 'SkillReviewController@commentDt', 'middleware' => 'staffHasPermission:manage_skill_review']);
    // Route::get('/skillreview/comment/{id}/create', ['as' => 'backend.skillreview.comment.create', 'uses' => 'SkillReviewController@commentCreate', 'middleware' => 'staffHasPermission:manage_skill_review']);
    // Route::post('/skillreview/comment/{id}/create', ['as' => 'backend.skillreview.comment.create.post', 'uses' => 'SkillReviewController@commentCreatePost', 'middleware' => 'staffHasPermission:manage_skill_review']);
    // Route::get('/skillreview/comment/{id}/edit/{cid}', ['as' => 'backend.skillreview.comment.edit', 'uses' => 'SkillReviewController@commentEdit', 'middleware' => 'staffHasPermission:manage_skill_review']);
    // Route::post('/skillreview/comment/{id}/edit/{cid}', ['as' => 'backend.skillreview.comment.edit.post', 'uses' => 'SkillReviewController@commentEditPost', 'middleware' => 'staffHasPermission:manage_skill_review']);
    Route::post('/skillreview/comment/{id}/delete/{cid}', ['as' => 'backend.skillreview.comment.delete.post', 'uses' => 'SkillReviewController@commentDeletePost', 'middleware' => 'staffHasPermission:manage_skill_review']);
    //crop image
    Route::post('/cropImage/{id}/{cid}',  ['as' => 'backend.cropImage', 'uses' => 'SkillReviewController@testpage']);

    Route::get('/faqs', ['as' => 'backend.faqs', 'uses' => 'FaqController@index']);
    Route::get('/faqs/dt', ['as' => 'backend.faqs.dt', 'uses' => 'FaqController@indexDt']);
    Route::get('/faqs/create', ['as' => 'backend.faqs.create', 'uses' => 'FaqController@create', 'middleware' => 'staffHasPermission:manage_faqs']);
    Route::post('/faqs/create', ['as' => 'backend.faqs.create.post', 'uses' => 'FaqController@createPost', 'middleware' => 'staffHasPermission:manage_faqs']);
    Route::get('/faqs/edit/{id}', ['as' => 'backend.faqs.edit', 'uses' => 'FaqController@edit', 'middleware' => 'staffHasPermission:manage_faqs']);
    Route::post('/faqs/edit/{id}', ['as' => 'backend.faqs.edit.post', 'uses' => 'FaqController@editPost', 'middleware' => 'staffHasPermission:manage_faqs']);
    Route::post('/faqs/delete/{id}', ['as' => 'backend.faqs.delete', 'uses' => 'FaqController@delete', 'middleware' => 'staffHasPermission:manage_faqs']);

    //countries & language crud
    Route::get('/languages', [
        'as' => 'backend.languages',
        'uses' => 'LanguageController@index',
        'middleware' => 'staffHasPermission:language_management'
    ]);
    Route::post('/languages-post', [
        'as' => 'backend.languages.post',
        'uses' => 'LanguageController@post',
        'middleware' => 'staffHasPermission:language_management'
    ]);

    Route::get('/countries', [
        'as' => 'backend.countries',
        'uses' => 'CountriesController@index',
        'middleware' => 'staffHasPermission:country_management'
    ]);
    Route::post('/countries-post', [
        'as' => 'backend.countries.post',
        'uses' => 'CountriesController@post',
        'middleware' => 'staffHasPermission:country_management'
    ]);

    //Manage Country Language
    Route::get('/countryLanguage', ['as' => 'backend.countrylanguage', 'uses' => 'CountryLanguageController@index', 'middleware' => 'staffHasPermission:manage_country_language']);
    Route::get('/countryLanguage/dt', ['as' => 'backend.countrylanguage.dt', 'uses' => 'CountryLanguageController@indexDt', 'middleware' => 'staffHasPermission:manage_country_language']);
    Route::get('/countryLanguage/create', ['as' => 'backend.countrylanguage.create', 'uses' => 'CountryLanguageController@create', 'middleware' => 'staffHasPermission:manage_country_language']);
    Route::post('/countryLanguage/create', ['as' => 'backend.countrylanguage.create.post', 'uses' => 'CountryLanguageController@createPost', 'middleware' => 'staffHasPermission:manage_country_language']);
    Route::get('/countryLanguage/edit/{id}', ['as' => 'backend.countrylanguage.edit', 'uses' => 'CountryLanguageController@edit', 'middleware' => 'staffHasPermission:manage_country_language']);
    Route::post('/countryLanguage/edit/{id}', ['as' => 'backend.countrylanguage.edit.post', 'uses' => 'CountryLanguageController@editPost', 'middleware' => 'staffHasPermission:manage_country_language']);
    Route::post('/countryLanguage/delete/{id}', ['as' => 'backend.countrylanguage.delete', 'uses' => 'CountryLanguageController@delete', 'middleware' => 'staffHasPermission:manage_country_language']);

    //Managed SharedOffice
    Route::get('/sharedoffice', [
        'as' => 'backend.sharedoffice',
        'uses' => 'SharedOfficeController@index',
        'middleware' => 'staffHasPermission:manage_sharedoffice'
    ]);

    Route::get('/sharedoffice/checkIn', [
        'as' => 'sharedofficeCheckIn',
        'uses' => 'SharedOfficeController@checkInSharedOffice',
        'middleware' => 'staffHasPermission:manage_sharedoffice_checkin'
    ]);

    Route::get('/sharedoffice/dt', ['as' => 'backend.sharedoffice.dt', 'uses' => 'SharedOfficeController@indexDt', 'middleware' => 'staffHasPermission:manage_sharedoffice']);
    Route::get('/sharedoffice/get-data', ['as' => 'backend.sharedoffice.data', 'uses' => 'SharedOfficeController@getSharedOffices', 'middleware' => 'staffHasPermission:manage_sharedoffice']);


    //working time sheet
    Route:: get('/sharedoffice/working/sheet',[
        'as' => 'backend.working_sheet',
        'uses' => 'SharedOfficeController@workingSheet',
        'middleware' => 'staffHasPermission:manage_sharedoffice_working_sheet',
    ]);
    Route:: post('/sharedoffice/working-sheet',[
        'as' => 'backend.working_sheet_Dt',
        'uses' => 'SharedOfficeController@workingSheetDt',
    ]);

    Route:: post('/sharedoffice/update/products',[
        'as' => 'backend.update_products',
        'uses' => 'SharedOfficeController@updateProducts',
    ]);

    Route::get('/sharedoffice/create', ['as' => 'backend.sharedoffice.create', 'uses' => 'SharedOfficeController@create', 'middleware' => 'staffHasPermission:manage_sharedoffice']);

    Route::get('/sharedoffice/create/{id}', ['as' => 'backend.sharedoffice.managercreate', 'uses' => 'SharedOfficeController@create', 'middleware' => 'staffHasPermission:manage_sharedoffice']);

    Route::post('/sharedoffice/create', ['as' => 'backend.sharedoffice.create', 'uses' => 'SharedOfficeController@storeOffice', 'middleware' => 'staffHasPermission:manage_sharedoffice']);

    Route::get('/sharedoffice/edit/{id}', ['as' => 'backend.sharedoffice.edit', 'uses' => 'SharedOfficeController@edit', 'middleware' => 'staffHasPermission:manage_sharedoffice']);
    Route::post('/sharedoffice/edit/{id}', ['as' => 'backend.sharedoffice.edit', 'uses' => 'SharedOfficeController@editOffice', 'middleware' => 'staffHasPermission:manage_sharedoffice']);
    Route::get('/sharedoffice/delete/removeDesk/{id}', 'SharedOfficeController@removeDesk')->name('remove-desk');

    Route::get('/sharedoffice/addimages/{id}', ['as' => 'backend.sharedoffice.addimages', 'uses' => 'SharedOfficeController@addimages', 'middleware' => 'staffHasPermission:manage_sharedoffice_images']);

    Route::get('/sharedoffice/removeImage/{id}', ['as' => 'backend.removeImage', 'uses' => 'SharedOfficeController@removeImage', 'middleware' => 'staffHasPermission:manage_sharedoffice_images']);
    Route::get('/sharedoffice/getImages/{id}', ['as' => 'backend.sharedoffice.getImages', 'uses' => 'SharedOfficeController@getImages', 'middleware' => 'staffHasPermission:manage_sharedoffice_images']);
    Route::post('/sharedoffice/addimages/{id}', ['as' => 'backend.sharedoffice.addimages.post', 'uses' => 'SharedOfficeController@storeadditionalimages', 'middleware' => 'staffHasPermission:manage_sharedoffice_images']);


    Route::get('/sharedoffice/editmoreimagesIndex/{id}', ['as' => 'backend.sharedoffice.editmoreimagesIndex', 'uses' => 'SharedOfficeController@editmoreimagesIndex', 'middleware' => 'staffHasPermission:manage_sharedoffice_images']);
    Route::get('/sharedoffice/editmoreimagesDt/{id}', ['as' => 'backend.sharedoffice.editmoreimagesDt', 'uses' => 'SharedOfficeController@editmoreimagesDt', 'middleware' => 'staffHasPermission:manage_sharedoffice_images']);


    Route::any('/sharedoffice/delete/{id}', ['as' => 'backend.sharedoffice.delete', 'uses' => 'SharedOfficeController@delete', 'middleware' => 'staffHasPermission:manage_sharedoffice']);
    Route::post('/sharedoffice/deleteimages/{id}', ['as' => 'backend.sharedoffice.deleteimages', 'uses' => 'SharedOfficeController@deleteImages', 'middleware' => 'staffHasPermission:manage_sharedoffice_images']);

    Route::get('/sharedoffice/barcodes/{id}', [
        'as' => 'backend.sharedofficebarcodes',
        'uses' => 'SharedOfficeBarcodes@barcodes',
        'middleware' => 'staffHasPermission:manage_sharedofficebarcodes'
    ]);

    Route::post('/sharedoffice/search/barcodes', [
        'as' => 'backend.sharedofficeSearchbarcodes',
        'uses' => 'SharedOfficeBarcodes@searchBarcode',
        'middleware' => 'staffHasPermission:manage_sharedofficebarcodes'
    ]);

    Route::get('/sharedoffice/products/index/qr/{id}', [
        'as' => 'backend.sharedoffice.products.qr',
        'uses' => 'SharedOfficeBarcodes@barcodesById',
        'middleware' => 'staffHasPermission:manage_sharedofficebarcodes'
    ]);

    Route::get('/sharedoffice/create/barcodes', [
        'as' => 'backend.sharedofficebarcodes.create',
        'uses' => 'SharedOfficeBarcodes@create',
        'middleware' => 'staffHasPermission:manage_sharedofficebarcodes'
    ]);

    Route::post('/sharedoffice/create/barcodes', [
        'as' => 'backend.sharedofficebarcodes.createPost',
        'uses' => 'SharedOfficeBarcodes@createPost',
        'middleware' => 'staffHasPermission:manage_sharedofficebarcodes'
    ]);

    Route::get('/sharedoffice/edit/barcodes/{id}', [
        'as' => 'backend.sharedofficebarcodes.edit',
        'uses' => 'SharedOfficeBarcodes@edit',
        'middleware' => 'staffHasPermission:manage_sharedofficebarcodes'
    ]);

    Route::post('/sharedoffice/edit/barcodes/{id}', [
        'as' => 'backend.sharedofficebarcodes.editPost',
        'uses' => 'SharedOfficeBarcodes@editPost',
        'middleware' => 'staffHasPermission:manage_sharedofficebarcodes'
    ]);

    Route::get('/sharedoffice/delete/barcodes/{id}', [
        'as' => 'backend.sharedofficebarcodes.delete',
        'uses' => 'SharedOfficeBarcodes@edit',
        'middleware' => 'staffHasPermission:manage_sharedofficebarcodes'
    ]);

    Route::get('/sharedoffice/{id}/chat', [
        'as' => 'backend.sharedofficeChat',
        'uses' => 'SharedOfficeController@sharedOfficeChat',
        'middleware' => 'staffHasPermission:manage_sharedoffice'
    ]);

    //get user list
    Route::get('/sharedOffice/users/{office_id}', [
        'as' => 'sharedOffice.users',
        'uses' => 'SharedOfficeController@getUsers',
        'middleware' => 'staffHasPermission:manage_sharedoffice'
    ]);

    //get shared office user record by id
    Route::get('/sharedOffice/user/details/{user_id}', [
        'as' => 'sharedOffice.users',
        'uses' => 'SharedOfficeController@userDetails',
        'middleware' => 'staffHasPermission:manage_sharedoffice'
    ]);

    //get messages
    Route::get('/sharedOffice/room/chat/{office_id}', [
        'as' => 'sharedOffice.room.chat',
        'uses' => 'SharedOfficeController@getMessagesByOfficeId',
        'middleware' => 'staffHasPermission:manage_sharedoffice'
    ]);

    Route::get('/sharedoffice/getStaffMessage/{staff_id}', [
        'as' => 'sharedOffice.staff.chatMessage',
        'uses' => 'SharedOfficeController@getStaffMessages',
        'middleware' => 'staffHasPermission:manage_sharedoffice'
    ]);

    //delete Messages
    Route::get('/sharedoffice/delete/message/{id}', [
        'as' => 'sharedOffice.staff.deletedMessage',
        'uses' => 'SharedOfficeController@deletedMessage',
        'middleware' => 'staffHasPermission:manage_sharedoffice'
    ]);

    //Admin actions
    Route::post('/sharedoffice/chat/action/{action}', [
        'as' => 'sharedOffice.staff.adminAction',
        'uses' => 'SharedOfficeController@adminAction',
        'middleware' => 'staffHasPermission:manage_sharedoffice'
    ]);

    Route::get('/sharedoffice/chat/actions', [
        'as' => 'sharedOffice.staff.chatAction',
        'uses' => 'SharedOfficeController@chatAction',
        'middleware' => 'staffHasPermission:manage_sharedoffice'
    ]);



    //send message
    Route::post('/sharedoffice/sendMessage', [
        'as' => 'sharedOffice.sendSharedofficeChatMessage',
        'uses' => 'SharedOfficeController@sendSharedofficeChatMessage',
        'middleware' => 'staffHasPermission:manage_sharedoffice'
    ]);


    Route::get('/sharedoffice/dashboard/{id}', [
        'as' => 'backend.sharedofficedashboard',
        'uses' => 'SharedOfficeController@sharedOfficeChat',
        'middleware' => 'staffHasPermission:manage_sharedoffice_dashboard'
    ]);

    Route::post('/save/staff/currency', [
        'as' => 'backend.saveStaffCurrency',
        'uses' => 'SharedOfficeController@staffCurrency',
        'middleware' => 'staffHasPermission:manage_sharedoffice'
    ]);

    //Manage Shared office new design
    // Route::get('/sharedoffice/dashboard/{id}', [
    //    'as' => 'backend.sharedofficeNewDesign',
    //    'uses' => 'SharedOfficeController@newdesign',
    //    'middleware' => 'staffHasPermission:manage_sharedoffice_request'
    // ]);

    Route::get('/sharedOfficeProducts/{id}', [
        'uses' => 'SharedOfficeController@sharedOfficeProductsDT'
	]);

    Route::get('/sharedOfficeBookings/{id}', [
        'uses' => 'SharedOfficeController@sharedOfficeBookingsDT'
	]);

    Route::get('/sharedOfficeAvailaleSeats/{bookingId}', [
        'uses' => 'SharedOfficeController@sharedOfficeAvailableSeatsDT'
	]);

    Route::post('/bookSeat', [
        'uses' => 'SharedOfficeController@sharedOfficeBookSeat'
	]);

    Route::post('/sharedOfficeBookingsCancel', [
        'uses' => 'SharedOfficeController@sharedOfficeBookingsCancel'
	]);

	Route::get('/getTodaySharedOfficeRating/{id}', [
        'as' => 'api.getSharedOffice.rating',
        'uses' => 'SharedOfficeController@getTodaySharedOfficeRating'
    ]);

    Route::get('/sharedOfficeProductsUsers/{id}', [
        'uses' => 'SharedOfficeController@sharedOfficeProductsUsersDT'
    ]);

    Route::post('/sharedOfficeProductsAddUsers/', [
        'uses' => 'SharedOfficeController@sharedOfficeProductsAddUsers'
    ]);

    Route::post('/sharedOfficeProductsCheckUsers/', [
        'uses' => 'SharedOfficeController@sharedOfficeProductsCheckUsers'
    ]);

    Route::get('/sharedoffice/newdesign-2', [
        'as' => 'backend.sharedofficeNewDesign',
        'uses' => 'SharedOfficeController@newdesign2',
        'middleware' => 'staffHasPermission:manage_sharedoffice_request'
    ]);

    Route::get('/sharedoffice/income/{id}', [
        'as' => 'backend.sharedofficeNewDesign',
        'uses' => 'SharedOfficeController@income',
        'middleware' => 'staffHasPermission:manage_sharedoffice_request'
    ]);

    Route::get('/sharedOfficeFinances/{id}', [
        'uses' => 'SharedOfficeController@sharedOfficeFinancesDT'
    ]);

    //withdraw_request
    Route::post('/withdraw/request', [
        'as' => 'backend.withdraw.request',
        'uses' => 'SharedOfficeController@withdrawRequest'
    ]);

    Route::get('/withdraw/requests', [
        'as' => 'backend.withdraw.requests',
        'uses' => 'SharedOfficeController@withdrawRequests'
    ]);

    Route::get('/withdraw/requests/approve/{id}', [
        'as' => 'backend.withdraw.requests.approve',
        'uses' => 'SharedOfficeController@withdrawRequestApprove'
    ]);

    Route::post('/approved/request', [
        'as' => 'backend.requests.approved',
        'uses' => 'SharedOfficeController@requestApproved'
    ]);

    Route::get('/withdraw/requests/reject/{transaction_id}/{withdraw_id}', [
        'as' => 'backend.withdraw.requests.reject',
        'uses' => 'SharedOfficeController@withdrawRequestReject'
    ]);

    Route::get('/rejected/payments/{withdraw_id}', [
        'as' => 'backend.rejectedPayments',
        'uses' => 'SharedOfficeController@rejectedPayments'
    ]);

    Route::get('/approved/payments/{withdraw_id}', [
        'as' => 'backend.approvedPayments',
        'uses' => 'SharedOfficeController@approvedPayments'
    ]);

    Route::get('/get/product/{id}', [
        'as' => 'backend.getProduct',
        'uses' => 'SharedOfficeController@getProducts'
    ]);

    //Manage shared office requests
    Route::get('/sharedofficerequest', [
        'as' => 'backend.sharedofficerequest',
        'uses' => 'SharedOfficeController@getSharedOfficeRequest',
        'middleware' => 'staffHasPermission:manage_sharedoffice_request'
    ]);
    // Mange shared office booking request
    Route::get('/sharedofficebookingrequest', [
        'as' => 'backend.sharedofficebookingrequest',
        'uses' => 'SharedOfficeController@getSharedOfficeBookingRequest',
        'middleware' => 'staffHasPermission:manage_sharedoffice_request'
    ]);

    // Mange Xenren hiring requests
    Route::get('/hiringrequest', [
        'as' => 'backend.hiringsrequest',
        'uses' => 'SharedOfficeController@gethiringRequest',
        'middleware' => 'staffHasPermission:manage_sharedoffice_request'
    ]);

    Route::get('download/resume/{id}',[
        'uses' => 'SharedOfficeController@downloadResume',
        'middleware' => 'staffHasPermission:manage_sharedoffice_request'
    ]);

    Route::get('delete/sharedofficerequest/{id}',[
        'as' => 'backend.sharedofficerequest.delete',
        'uses' => 'SharedOfficeController@deleteSharedOfficeRequest',
        'middleware' => 'staffHasPermission:manage_sharedoffice_request'
    ]);

    Route::get('edit/sharedofficerequest/{id}',[
        'as' => 'backend.sharedofficerequest.edit',
        'uses' => 'SharedOfficeController@editSharedOfficeRequest',
        'middleware' => 'staffHasPermission:manage_sharedoffice_request'
    ]);

    Route::post('update/shardofficerequest/{id}',[
        'as' => 'backend.sharedofficerequest.update',
        'uses' => 'SharedOfficeController@updateSharedOfficeRequest',
        'middleware' => 'staffHasPermission:manage_sharedoffice_request'
    ]);

    Route::get('markasviewed/sharedofficerequest/{id}',[
        'as' => 'backend.sharedofficerequest.markasviewed',
        'uses' => 'SharedOfficeController@markedSharedOfficeRequest',
        'middleware' => 'staffHasPermission:manage_sharedoffice_request'
    ]);

    //SharedOffice -- chair and other settings
    Route::get('/sharedoffice/products/index/{id}', [
        'as' => 'backend.sharedoffice.products.index',
        'uses' => 'SharedOfficeProductsController@index',
        'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);
    Route::get('/sharedoffice/products/index/dt/{id}', ['as' => 'backend.sharedoffice.products.dt', 'uses' => 'SharedOfficeProductsController@dt', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);

    Route::get('/sharedoffice/products/get-availability/{officeId}/{categoryId}', ['as' => 'backend.sharedoffice.get-availability', 'uses' => 'SharedOfficeProductsController@getAvailability', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);

    Route::get('/sharedoffice/products/index/create/{id}', ['as' => 'backend.sharedoffice.products.create', 'uses' => 'SharedOfficeProductsController@create', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);
    Route::post('/sharedoffice/products/index/create/{id}', ['as' =>'backend.sharedoffice.products.create', 'uses' => 'SharedOfficeProductsController@store', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);

    Route::get('/sharedoffice/products/index/checkposition/{id}', ['as' => 'backend.sharedoffice.products.checkposition', 'uses' => 'SharedOfficeProductsController@checkposition', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);
    Route::get('/sharedoffice/products/index/checkpositionedit/{id}', ['as' => 'backend.sharedoffice.products.checkpositionedit', 'uses' => 'SharedOfficeProductsController@checkpositionedit', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);
    Route::get('/sharedoffice/products/index/checkbusy/{id}', ['as' => 'backend.sharedoffice.products.checkbusy', 'uses' => 'SharedOfficeProductsController@checkbusy', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);

    Route::post('/sharedoffice/products/index/delete/{id}', ['as' => 'backend.sharedoffice.products.delete', 'uses' => 'SharedOfficeProductsController@delete', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);

    Route::get('/sharedoffice/products/index/edit/{id}', ['as' => 'backend.sharedoffice.products.edit', 'uses' => 'SharedOfficeProductsController@edit', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);
    Route::post('/sharedoffice/products/index/edit/{id}', ['as' => 'backend.sharedoffice.products.edit', 'uses' => 'SharedOfficeProductsController@update', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);

    Route::get('/sharedoffice/renting/index/{id}', ['as' => 'backend.sharedoffice.renting.index', 'uses' => 'SharedOfficeProductsController@renting', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);
    Route::post('/sharedoffice/renting/index/{id}', ['as' => 'backend.sharedoffice.renting.index', 'uses' => 'SharedOfficeProductsController@saveRenting', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);

    Route::get('/sharedoffice/rentingmonth/index/{id}', ['as' => 'backend.sharedoffice.rentingmonth.index', 'uses' => 'SharedOfficeProductsController@rentingMonth', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);
    Route::post('/sharedoffice/rentingmonth/index/{id}', ['as' => 'backend.sharedoffice.rentingmonth.index', 'uses' => 'SharedOfficeProductsController@saveRentingMonth', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);

    Route::get('/sharedoffice/finance/index/{id}', ['as' => 'backend.sharedoffice.finance.index', 'uses' => 'SharedOfficeProductsController@finance', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);

    Route::get('/sharedoffice/showfinancetable/index/{id}', ['as' => 'backend.sharedoffice.finance.showfinancetable', 'uses' => 'SharedOfficeProductsController@showFinanceTable', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);
    Route::get('/sharedoffice/showfinancetable/dt/index/{id}', ['as' => 'backend.sharedoffice.finance.showfinancetable.dt', 'uses' => 'SharedOfficeProductsController@showFinanceTabledt', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);

    Route::get('/sharedoffice/showfinancetable/editfinance/{id}', ['as' => 'backend.sharedoffice.finance.editfinance', 'uses' => 'SharedOfficeProductsController@editFinance', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);
    Route::post('/sharedoffice/showfinancetable/editfinance/{id}', ['as' => 'backend.sharedoffice.finance.editfinance', 'uses' => 'SharedOfficeProductsController@storeFinancePayment', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);

    Route::get('/sharedoffice/showfinancetable/choosechair/{id}', ['as' => 'backend.sharedoffice.finance.choosechair', 'uses' => 'SharedOfficeProductsController@chooseChair', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);
    Route::post('/sharedoffice/showfinancetable/choosechair/{id}', ['as' => 'backend.sharedoffice.finance.choosechair', 'uses' => 'SharedOfficeProductsController@savenewChair', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);

    Route::get('/sharedoffice/showfinancetable/revision/{id}', ['as' => 'backend.sharedoffice.finance.revision', 'uses' => 'SharedOfficeProductsController@listfinanceRevisions', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);
    Route::get('/sharedoffice/listrevision/dt/index/{id}', ['as' => 'backend.sharedoffice.finance.listrevision.dt', 'uses' => 'SharedOfficeProductsController@listfinanceRevisionsdt', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);

    Route::get('/sharedoffice/showfinancetable/deletefinance/{id}', ['as' => 'backend.sharedoffice.finance.deletefinance', 'uses' => 'SharedOfficeProductsController@deleteFinance', 'middleware' => 'staffHasPermission:manage_sharedofficeproducts']);


    //Route::post('/sharedoffice/chairsettings/{id}', ['as' => 'backend.sharedoffice.edit', 'uses' => 'SharedOfficeProductsController@editOfficef', 'middleware' => 'staffHasPermission:manage_sharedoffice']);
    // Managed Coins
    Route::get('/manageUserCoins', ['as' => 'backend.manageUserCoins', 'uses' => 'ManageCoinController@userindex', 'middleware' => 'staffHasPermission:manage_coins']);
    Route::get('/userfeedback', ['as' => 'backend.userfeedback', 'uses' => 'ManageCoinController@userfeedback', 'middleware' => 'staffHasPermission:manage_coins']);
    Route::get('/manageUserCoins/dt', ['as' => 'backend.manageUserCoins.dt', 'uses' => 'ManageCoinController@userindexDt', 'middleware' => 'staffHasPermission:manage_coins']);

    Route::get('/manageCoins', ['as' => 'backend.manageCoins', 'uses' => 'ManageCoinController@index', 'middleware' => 'staffHasPermission:manage_coins']);
    Route::get('/manageCoins/dt', ['as' => 'backend.manageCoins.dt', 'uses' => 'ManageCoinController@indexDt', 'middleware' => 'staffHasPermission:manage_coins']);

    Route::get('/manageCoins/create', ['as' => 'backend.manageCoins.create', 'uses' => 'ManageCoinController@create', 'middleware' => 'staffHasPermission:manage_coins']);
    Route::post('/manageCoins/create', ['as' => 'backend.manageCoins.create', 'uses' => 'ManageCoinController@storeOffice', 'middleware' => 'staffHasPermission:manage_coins']);

    Route::get('/manageCoins/edit/{id}', ['as' => 'backend.manageCoins.edit', 'uses' => 'ManageCoinController@edit', 'middleware' => 'staffHasPermission:manage_coins']);
    Route::post('/manageCoins/edit/{id}', ['as' => 'backend.manageCoins.edit', 'uses' => 'ManageCoinController@editOffice', 'middleware' => 'staffHasPermission:manage_coins']);

    Route::post('/manageCoins/delete/{id}', ['as' => 'backend.manageCoins.delete', 'uses' => 'ManageCoinController@delete', 'middleware' => 'staffHasPermission:manage_coins']);

    //Manage Settings

    Route::get('/manageSetting', ['as' => 'backend.manageSettings', 'uses' => 'ManageSettingsController@index', 'middleware' => 'staffHasPermission:manage_coins']);
    Route::post('/setInvitorUserCommission', ['uses' => 'ManageSettingsController@setInvitorUserCommission', 'middleware' => 'staffHasPermission:manage_coins']);
    Route::post('/setInvitedUserCommission', ['uses' => 'ManageSettingsController@setInvitedUserCommission', 'middleware' => 'staffHasPermission:manage_coins']);

    Route::post('/setHourlyRate', ['uses' => 'ManageSettingsController@setHourlyRate', 'middleware' => 'staffHasPermission:manage_coins']);


    //Manage Feedback
    Route::get('/feedback', ['as' => 'backend.feedback', 'uses' => 'FeedbackController@index', 'middleware' => 'staffHasPermission:manage_feedback']);
    Route::get('/feedback/dt', ['as' => 'backend.feedback.dt', 'uses' => 'FeedbackController@indexDt', 'middleware' => 'staffHasPermission:manage_feedback']);
    Route::get('/feedback/edit/{id}', ['as' => 'backend.feedback.edit', 'uses' => 'FeedbackController@edit', 'middleware' => 'staffHasPermission:manage_feedback']);
    Route::post('/feedback/edit/{id}', ['as' => 'backend.feedback.edit.post', 'uses' => 'FeedbackController@editPost', 'middleware' => 'staffHasPermission:manage_feedback']);

    //Manage Chat
    Route::get('/feedback/chat/{feedback_id}', ['as' => 'backend.feedback.chat', 'uses' => 'FeedbackChatController@index', 'middleware' => 'staffHasPermission:manage_chat']);
    Route::post('/feedback/postChat', ['as' => 'backend.feedback.postchat', 'uses' => 'FeedbackChatController@postChat', 'middleware' => 'staffHasPermission:manage_chat']);
    Route::get('/feedback/chatImage/{chat_room_id}', ['as' => 'backend.feedback.chatimage', 'uses' => 'FeedbackChatController@chatImage', 'middleware' => 'staffHasPermission:manage_chat']);
    Route::post('/feedback/chatImage', ['as' => 'backend.feedback.chatimage.post', 'uses' => 'FeedbackChatController@chatImagePost', 'middleware' => 'staffHasPermission:manage_chat']);
    Route::get('/feedback/pullChat/{id}', ['as' => 'backend.feedback.pullchat', 'uses' => 'FeedbackChatController@pullChat', 'middleware' => 'staffHasPermission:manage_chat']);

    //Inbox
    Route::get('/inbox', ['as' => 'backend.inbox', 'uses' => 'InboxController@index']);
    Route::get('/inbox/messages/{inbox_id}', ['as' => 'backend.inbox.messages', 'uses' => 'InboxController@messages']);
    Route::post('/inbox/trash', ['as' => 'backend.inbox.trash', 'uses' => 'InboxController@trash']);
    Route::post('/inbox/untrash', ['as' => 'backend.inbox.untrash', 'uses' => 'InboxController@untrash']);
    Route::post('/inbox/trashAll', ['as' => 'backend.inbox.trashall', 'uses' => 'InboxController@trashAll']);
    Route::post('/inbox/untrashAll', ['as' => 'backend.inbox.untrashall', 'uses' => 'InboxController@untrashAll']);
    Route::post('/inbox/markasread', ['as' => 'backend.inbox.markasread', 'uses' => 'InboxController@markAsRead']);
    Route::post('/inbox/removeall', ['as' => 'backend.inbox.removeall', 'uses' => 'InboxController@removeAll']);
    Route::post('/inbox/sendmessage', ['as' => 'backend.inbox.sendmessage', 'uses' => 'InboxController@sendMessage']);
    Route::get('/inbox/chatImage/{inbox_id}/{receiver_id}', ['as' => 'backend.inbox.chatimage', 'uses' => 'InboxController@chatImage']);
    Route::post('/inbox/chatImage', ['as' => 'backend.inbox.chatimage.post', 'uses' => 'InboxController@chatImagePost']);
    Route::post('/shared-office-active','SharedOfficeController@updateSharedOfficebit');


    Route::get('/invite-to-join', ['as' => 'backend.inviteJoin', 'uses' => 'InboxController@inviteToJoin']);
    Route::get('/download-log', [
        'uses' => 'UtilityController@downloadLog'
    ]);

    // Coin
    Route::get('/give-coin', ['as' => 'backend.giveCoin', 'uses' => 'CoinController@index', 'middleware' => 'staffHasPermission:manage_coins']);
    Route::post('/give-coin/validate', ['as' => 'backend.giveCoin.validate', 'uses' => 'CoinController@validateCoin', 'middleware' => 'staffHasPermission:manage_coins']);
    Route::post('/give-coin/add-user-coin', ['as' => 'backend.giveCoin.addUserCoin', 'uses' => 'CoinController@addUserCoin', 'middleware' => 'staffHasPermission:manage_coins']);

    Route::get('/coin', ['as' => 'backend.coin', 'uses' => 'CoinController@coinList', 'middleware' => 'staffHasPermission:manage_coins']);
    Route::get('/coin/get-list', ['as' => 'backend.coin.getListCoin', 'uses' => 'CoinController@getListCoin', 'middleware' => 'staffHasPermission:manage_coins']);
    Route::post('/coin', ['as' => 'backend.coin.submit', 'uses' => 'CoinController@submitCoin', 'middleware' => 'staffHasPermission:manage_coins']);
    Route::post('/coin/delete', ['as' => 'backend.coin.delete', 'uses' => 'CoinController@deleteCoin', 'middleware' => 'staffHasPermission:manage_coins']);

    Route::get('/check-reset-usr', [
        'as' => 'backend.reset-user',
        'uses' => 'StaffController@resetUser',
    ]);

    Route::post('/reset-usr', [
        'as' => 'backend.reset-user-post',
        'uses' => 'StaffController@resetUserPost',
    ]);

    Route::get('/check-push-notification', [
        'as' => 'backend.pushNotification',
        'uses' => 'StaffController@checkNotifications',
    ]);

    Route::any('/send-push-notification', [
        'as' => 'backend.sendPushNotification',
        'uses' => 'StaffController@sendPushNotification',
    ]);
});

//Backend auth route
Route::group(array('middleware' => ['auth:staff']), function() {
    Route::get('/profile', ['as' => 'staff.profile', 'uses' => 'Auth\StaffController@profile']);
    Route::post('/profile', ['as' => 'staff.profile.post', 'uses' => 'Auth\StaffController@profilePost']);
});

//Backend logout
Route::get('/staff/logout', ['as' => 'staff.logout', 'uses' => 'Auth\StaffController@logout']);
Route::get('/staff/logout/mobile', ['as' => 'staff.mobile.logout', 'uses' => 'Backend\SharedOfficeController@logoutMobile']);
Route::group(array('prefix' => 'test'), function () {
    Route::get('/sendmail', ['uses' => 'TestUtility@testMailSend']);
});

