/**
 * Created by khegiw on 28/12/2016.
 */
var https = require('https');
var app = require('express')();
var fs = require('fs');
var server = https.createServer({
	key: fs.readFileSync('/home/xenren/conf/web/ssl.xenren.co.key'),
	cert: fs.readFileSync('/home/xenren/conf/web/ssl.xenren.co.crt'),
	requestCert: false,
	rejectUnauthorized: false
}, app);
var io = require('socket.io')(server);
var Redis = require('ioredis');
var redis = new Redis('redis://:null@192.199.231.203:6379/2');
var redisMessenger = new Redis;
var redisRoom = new Redis;
var redisNotifications = new Redis;

redisRoom.subscribe('rooms');
redisNotifications.subscribe('projectNotifications');
var rooms = [];
redisRoom.on('message', function (channel, message) {
    var room = JSON.parse(message).room;
    redisMessenger.subscribe(room);
    // console.log(room);
});

var users = [];
io.sockets.on('connection', function (socket) {
    socket.on('register', function (user) {
        // users[user.id] = socket.id;
        console.log(' registered');
        console.log(socket);
    });

    socket.on('delete', function (msg) {
        io.emit('messageHasBeenDeleted', msg.id)
    });

    socket.on('disconnect', function () {
        console.log( ' disconnected');
        console.log(socket);
        //remove user from db
    });
});

redisMessenger.on('message', function (channel, message) {

    message = JSON.parse(message);
    // console.log(message);
    if (message.to) {
        io.to(users[message.user_id]).emit(channel, message);
        io.to(users[message.to]).emit(channel, message);
    } else {
        io.emit(channel, message);
    }
});

redisNotifications.on('message', function (channel, message) {
    console.log('redisNotifications');
    console.log(channel);
    console.log(message);
    // message = JSON.parse(message);
    // // console.log(message);
    // if (message.to) {
    //     io.to(users[message.user_id]).emit(channel, message);
    //     io.to(users[message.to]).emit(channel, message);
    // } else {
    //     io.emit(channel, message);
    // }
});

/**
 * Code by Ahsan Hussain
* */
redis.subscribe('global-channel');

redis.on('message', function (channel, message) {
	console.log(message);
	message = JSON.parse(message);
	io.emit(channel + ':' + message.event, message.data);
});

server.listen(3000, '128.199.231.203' , function () {
	console.log('listing on port 3000');
});


//
// /**
//  * Created by khegiw on 28/12/2016.
//  */
// var http = require('http');
// var app = require('express')();
// var fs = require('fs');
// // var server = http.createServer({
// // 	// key: fs.readFileSync('/home/xenren/conf/web/ssl.xenren.co.key'),
// // 	// cert: fs.readFileSync('/home/xenren/conf/web/ssl.xenren.co.crt'),
// // 	requestCert: false,
// // 	rejectUnauthorized: false
// // }, app);
// var server = http.createServer(function(req, res) {
// });
// var io = require('socket.io')(server);
// var Redis = require('ioredis');
// var redis = new Redis('redis://:null@localhost:6379/2');
// var redisMessenger = new Redis;
// var redisRoom = new Redis;
// var redisNotifications = new Redis;
//
// redisRoom.subscribe('rooms');
// redisNotifications.subscribe('projectNotifications');
// var rooms = [];
// redisRoom.on('message', function (channel, message) {
//     console.log('message recieved');
//     var room = JSON.parse(message).room;
//     redisMessenger.subscribe(room);
//     console.log(room);
// });
//
// var users = [];
// io.sockets.on('connection', function (socket) {
//     socket.on('register', function (user) {
//         // users[user.id] = socket.id;
//         console.log(' registered');
//         console.log(socket);
//     });
//
//     socket.on('delete', function (msg) {
//         io.emit('messageHasBeenDeleted', msg.id)
//     });
//
//     socket.on('disconnect', function () {
//         console.log( ' disconnected');
//         console.log(socket);
//         //remove user from db
//     });
// });
//
// redisMessenger.on('message', function (channel, message) {
//     console.log('message recieved redis');
//     message = JSON.parse(message);
//     console.log(message);
//     if (message.to) {
//         io.to(users[message.user_id]).emit(channel, message);
//         io.to(users[message.to]).emit(channel, message);
//     } else {
//         io.emit(channel, message);
//     }
// });
//
// redisNotifications.on('message', function (channel, message) {
//     console.log('redisNotifications');
//     console.log(channel);
//     console.log(message);
//     // message = JSON.parse(message);
//     // // console.log(message);
//     // if (message.to) {
//     //     io.to(users[message.user_id]).emit(channel, message);
//     //     io.to(users[message.to]).emit(channel, message);
//     // } else {
//     //     io.emit(channel, message);
//     // }
// });
//
// /**
//  * Code by Ahsan Hussain
//  * */
// redis.subscribe('global-channel');
//
// redis.on('message', function (channel, message) {
//     console.log('global channel');
//     console.log(message);
//     message = JSON.parse(message);
//     io.emit(channel + ':' + message.event, message.data);
// });
//
// server.listen(2000, 'localhost' , function () {
//     console.log('listing on port 2000');
// });


