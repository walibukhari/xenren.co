/**
 * Created by khegiw on 28/12/2016.
 */

var _ = require('underscore');
Vue.http.headers.common['X-CSRF-TOKEN'] = $('input[name="_token"]').val();

new Vue({
    el: "#chat",
    data: {
        loading: true,
        messages: null,
        newMessage: "",
        newFile: null,
        files: '',
        user: user,
        rooms: [],
        room: room,
        project_id: project_id,
        notifications: [],

    },
    methods: {
        onScroll: function (e, position) {
            if (position.scrollTop === 0) {
                //get previous message
                // this.$http.get('/api/chatmessage/getallweb?chat_room_id=' + this.room + '&length=' + this.messages.length).then((response) => {
                //
                //     for (key in response.data) {
                //         this.messages.unshift(response.data[key]);
                //
                //     }
                // }, (response) => {
                //     // error callback
                //
                // })
            }
        },
        sendMessage: function (e) {
            console.log('message sent');
            e.preventDefault();
            if (this.newMessage == "") return;
            var that = this;

            data = new FormData();
            data.append('project_id', that.project_id);
            data.append('chat_room_id', that.project_id);
            data.append('sender_user_id', that.user.id);
            data.append('message', this.newMessage);
            data.append('image', this.newFile);

            //post message
            this.$http.post('/api//project-chat-messages/post', data).then((response)=> {
                that.newMessage = "";
                that.newFile = null;
                that.files = null;
                that.scrollBottom();
            }, (response)=> {

            })


        },
        initListener: function () {

            console.log('init Listener');
            var that = this;
            // register to room and get messages
            socket.on(that.room, function (msg) {
                if (msg.chat_room_id == that.room) {
                    that.messages.push(msg);
                }

                that.scrollBottom();
            })
        },
        // loadRooms:function(){
        //
        // },
        setUser: function () {
            var that = this;
            // this.$http.get('/api/getuser').then((response) => {
            //     that.user = response.data;
            socket.emit('register', that.user);
            // }, (response) => {
            //     error callback
            // })
        },

        cleanMessage: function (text) {
            var urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
            return text.replace(urlRegex, function (url) {
                return '<a href="' + url + '">' + url + '</a>';
            });
        },
        loadMessages: function () {

            console.log('loadmessages');
            this.loading = false;
            var input = {
                project_id: this.project_id,
                chat_room_id: this.room,
            };


            this.$http.post('/api/project-chat-messages', input).then((response) => {

                this.messages = response.data;
                // this.messages.push(response.data);

                // this.scrollBottom();
                // this.scrollBottom();

            }, (response) => {
                // error callback

            })
        },
        scrollBottom: function () {
            var messageBox = this.$els.msgs;
            // var messageBox = $('#msgs');
            // alert(messageBox.scrollHeight);
            $(messageBox).animate({'scrollTop': messageBox.scrollHeight}, 'slow');
            // $(messageBox).scrollTop(messageBox.scrollHeight)

        },

    },

    mounted: function () {

        this.setUser();
        this.initListener();
        this.loadMessages();
    }

});