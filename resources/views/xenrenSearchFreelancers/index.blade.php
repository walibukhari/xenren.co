@extends('frontend-xenren.layouts.default-new')

@section('title')
    {{ trans('home.title') }}
@endsection

@section('keywords')
    {{ trans('home.keywords') }}
@endsection

@section('description')
    {{ trans('home.description') }}
@endsection

@section('author')
    Xenren
@endsection

@section('header')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="/custom/css/frontend/homePagePopup.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('xenren-assets/css/custom_style.css')}}" type="text/css">

    <style>
        .bgSkillColor {
            background-color: gray;
            margin: 2px;
            color: #fff;
            padding: 5px;
            display: inline-block;
            border-radius: 10px
        }

        @media only screen and (max-width: 540px) {
            .right-nav ul li {
                /*display: inline-table;*/
            }
        }
        @media only screen and (min-width:500px){
            .floating-chat {
                margin-right: 5px !important;
            }
        }
        @media only screen and (min-width:300px){
            .floating-chat {
                margin-right: 5px !important;
            }
        }
        @media (max-width:1199px) and (min-width:992px)
        {

            .section .setMPHSC {
                width: 100% !important;
                text-align: -webkit-center;
                margin-left:0px !important;
            }
        }

        @media (max-width:991px)
        {
            .setSectionServices{
                padding-left:3%;
            }
            footer img {
                width: 40px;
                height: 36px;
            }
            .setPeopleShortStory{
                width:100%;
                text-align: center;
            }
            .section .setMPHSC {
                width: 100% !important;
                text-align: -webkit-center;
                margin-left:0px !important;
            }
        }

        .navbar{
            margin-top:0p !important;
        }

        .setHPROW{
            margin-right: 0px !important;
            margin-left: 0px !important;
        }
        .setHPIMAGENL{
            width:100px;
            margin-bottom:15px;
        }
        .setHPMSHELPER{
            display:none;
        }
        .setMSCTNFOCUSC,.ms-res-ctn,.dropdown-menu{
            margin-top:2px !important;
        }
        .setHPMSERACHBOX{
            width:492px;
        }
        .setHPUBTN4{
            position: absolute;
            top: 150px;
        }
        .setHPSTYPE{
            display:none;
        }
        .setcolMDOFS1Col9ColMD10 {
            padding-left:13%;
            padding-right:13%;
        }
        .setHPDIVTOC{
            margin-top:10px !important;
        }
        .setHPFCHART{
            margin-right: 100px;
            margin-bottom: 20px;
            z-index: 99999999999;
        }
        .setFCHARTIMAGE{
            height:42px;
            width: 42px;
        }
        .setFCHARTBTN{
            margin: 0 0px -50px 250px;
            z-index: 99;
        }
        .setFCHARADIV{
            position:relative;
            bottom: 7px;
        }
        .setHPAUTHCIALINK{
            display:none;
        }
        .setHPAUTHCIALINK2{
            display:block;
        }
        .setHPDIVROWMSEARCH{
            display:none;
        }

        #msSearchBar > .ms-res-ctn.dropdown-menu{
            border-top: 0px !important;
            margin-top: 0px !important;
            border-radius: 0px !important;
            border-bottom-left-radius: 25px !important;
            border-bottom-right-radius: 25px !important;
        }

        .ms-ctn {
            background-color: #fff;
            border-radius: 25px;
            border: 0px solid #fff;
            box-shadow: 0px 0px 15px 0px #B6B3B1;
        }

        /* width */
        #msSearchBar > .ms-res-ctn.dropdown-menu::-webkit-scrollbar {
            width: 4px;
            height: 10px;
            max-height: 10px;
            min-height: 15px;
            border-radius: 20%;
        }

        /* Track */
        #msSearchBar > .ms-res-ctn.dropdown-menu::-webkit-scrollbar-track {
            height: 10px;
            max-height: 10px;
            min-height: 15px;
            border-radius: 20%;
        }

        .ms-res-ctn{
            display: none !important;
        }

        .displayBlock{
            display: block !important;
        }

        /* Handle */
        #msSearchBar > .ms-res-ctn.dropdown-menu::-webkit-scrollbar-thumb {
            background: #888;
            height: 10px;
            max-height: 10px;
            min-height: 15px;
            border-radius: 20%;
        }

        /* Handle on hover */
        #msSearchBar > .ms-res-ctn.dropdown-menu::-webkit-scrollbar-thumb:hover {
            background: #555;
        }

        .search-freelancer-fa-fa-search{
            font: normal normal normal 14px/1 FontAwesome !important;
        }

    </style>
@endsection

@section('content')
<div class="container-fluid" id="searchFreelancers">
    <!----------------------SUB-MENU STARTS----------------------->
    <nav class="set-navbar set-nav-search-results-freelancers">
        <div class="navbar-toggler" data-toggle="collapse" data-target="#subNavBar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            Our Services <span class="fa fa-angle-double-down" style="font: normal normal normal 14px/1 FontAwesome !important;"></span>
        </div>
        <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2" id="subNavBar">
            <ul class="navbar-nav mr-auto mx-auto order-0 set-ul-search-freelancers">
                <li class="nav-item">
                    <a class="nav-link" href="#">Web Dev</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Mobile Dev</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Design</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Writing</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Admin Support</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Customer Service</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Marketing</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">See all Categories</a>
                </li>
            </ul>
        </div>
    </nav>

    <!--------SUB MENU ENDS HERE----------->

    <!--------------BANNER STARTS----------->
    <div class="banner">
        <img src="{{asset('xenren-assets/images/slider-image.png')}}">
        <h3 class="banner-text">Hire the best Blockchain Developers</h3>
        <br>
        <h5 class="banner-text">Find top Blockchain Developers on Xenren— the leading freelancing website for short-
            term, recurring, and full-time Blockchain contract work.
        </h5>
<br>
        <form>
            <input type="text" placeholder="Get Started" class="form-control"><span class="fa fa-search search-freelancer-fa-fa-search"></span>
        </form>
    </div>
    <!---------------BANNER ENDS-------------->
    <!----------------MAIN BODY START--------->
    <div class="container-fluid main-body">
        <br>
        <h2 class="set-search-freelancers-explore-some">Explore some of the top freelancers</h2>
        <div class="row set-search-xenren-freelancer">
            <div class="filter col-lg-2 col-xl-2 col-sm-12 col-xs-12 col-md-12">
                <h4 class="set-filters-search-freelance">Filters</h4>
                <hr>
                <h5 class="set-skills-search-freelance">Skills</h5>
                <form class="form-group">
                    <input type="text" name="skills" @change="searchbox('skill')" v-model="skills" class="set-input-search-freelance form-control" placeholder="Select Skills">
                </form>
                <br>
                <h5 class="set-hourly-rate-search">Hourly Rate</h5>
                <input type="range"  min="1" max="100" @change="searchbox('price')" v-model="price_range" class="set-input-search-freelance slider" id="myRange">
                <hr>
                <h5 class="set-countries-search">Country</h5>
                <form class="form-group">
                    <input type="text" name="country" v-on:blur="searchbox('country')" v-model="country" class="form-control set-input-search-freelance" style="" placeholder="Select Country">
                </form>
                <br>
                <h6 @click="resetFilters()" class="set-search-freelance-reset">Reset Filters</h6>
            </div>
            <div class="freelancers col-lg-9 col-md-12 col-xl-9 col-sm-12 col-xs-12">
                <div class="row set-search-xenren-freelancer">
                    <div class="col-lg-9  col-md-12 col-xl-9 col-xs-12 col-sm-12">
                        <form class="form-group">
                            <a href="javascript:;" id="btn-conform-2" @click="searchButton()" class="fa fa-search search-freelancer-fa-fa-search" style="font: normal normal normal 14px/1 FontAwesome !important" aria-hidden="true"></a>
                            <input name="name" id="tbxName" v-on:blur="searchbox('name')" v-model="tbxName" type="text" placeholder="{{ trans('common.search_freelancer') }}" class="form-control set-input-search-freelance">
                        </form>
                    </div>
                    <div class="col-lg-3 col-md-12 col-xl-3 col-xs-12 col-sm-12">
                        <p>Sort by: <a href="">Hourly Rate</a></p>
                    </div>
                </div>
                <div class="preload">
                    <img src="http://i.imgur.com/KUJoe.gif">
                </div>

                <div class="row set-search-xenren-freelancer" id="set-xenren-search-freelancers">

                    <div class="col-lg-4  col-md-12 col-xl-4 col-xs-12 col-sm-12" v-for="users in freelancers">
                    <div class="card">
                        <img class="card-img-top" :src="users.img_avatar" onerror="this.src='{{asset('/images/default-m.jpg')}}'" alt="Card image cap">

                        <div class="card-body">
                            <div>
                                <div v-if="users.hourly_pay > 0">
                                    <p class="amount">
                                        $@{{ users.hourly_pay }}
                                    </p>
                                    <p class="amount-details">USD/{{trans('common.hr')}}</p>
                                </div>
                                <div v-else>
                                </div>
                            </div>
                            <h5 class="card-title">@{{ users.name }}</h5>
                            <p class="details">@{{ getJobPosition(users) }}</p>
                            <br>
                            <div class="set-marker-countryname">
                                <i class="fa fa-map-marker"></i>
                                    @{{ users.country_name }}
                            </div>
                        </div>
                        <br>
                        <ul>
                            <li>
                                {{--@{{ getSkills(users) }}--}}
                                <div v-html="getSkills(users)"></div>

                            </li>
                        </ul>
                    </div>
                </div>
                    <br>
                </div>


                <div class="preload">
                    <img src="http://i.imgur.com/KUJoe.gif">
                </div>

                <div class="container set-container-pagination-vue-js" id="set-xenren-search-freelancers-pagination">
                    <div class="pagination">
                        <button class="btn btn-default" @click="previewBack(pagination.prev_page_url)"
                                :disabled="!pagination.prev_page_url">
                            Previous
                        </button>
                        <span>Page @{{pagination.current_page}} of @{{pagination.last_page}}</span>
                        <button class="btn btn-default" @click="getExperts(pagination.next_page_url)"
                                :disabled="!pagination.next_page_url">Next
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--END OF MAIN BODY-->

    <!--HOW DOES IT WORK STARTS-->
    <div class="row set-search-xenren-freelancer how-it-works">
        <div class="  col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <h3 class="set-how-its-work">How it works</h3>
        </div>
    </div>
    <div class="row set-search-xenren-freelancer how-it-works">
        <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
            <img src="{{asset('xenren-assets/images/post-a-job.png')}}">
            <p class="title">Post a Job (It's free)</p>
            <p class="describe">Tell us about your project.
                Xenren connects you with the top talent
                around the world, or near you.
            </p>
        </div>
        <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
            <img src="{{asset('xenren-assets/images/freelancer-come-to-you.png')}}">
            <p class="title">Freelancer come to you</p>
            <p class="describe">Get qualified proposals within 24 hours.
                Compare bids, reviews, and prior
                work. Interview favorites and
                hire the best fit.
            </p>
        </div>
        <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
            <img src="{{asset('xenren-assets/images/collaborate-easily.png')}}">
            <p class="title">Collaborate Easily</p>
            <p class="describe">Use Xenren to chat or video call,
                share files, and track project
                milestones from your desktop or
                mobile.
            </p>
        </div>
        <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
            <img src="{{asset('xenren-assets/images/payment.png')}}">
            <p class="title">Payment Simplified</p>
            <p class="describe">Pay hourly or fixed-price and receive
                invoices through Xenren. Pay for
                work you authorize.
            </p>
        </div>
    </div>
    <!--END OF HOW DOES IT WORK-->
</div>
@endsection

@section('footer')
    <script>
        $(function() {
            // if (window.matchMedia("(max-width: 556px)").matches) {
                $(".preload").fadeOut(2000, function() {
                    $("#set-xenren-search-freelancers").fadeIn(1000);
                    $("#set-xenren-search-freelancers-pagination").fadeIn(1000);
                });
            // }
        });
    </script>
    <script src="{{asset('js/vue.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vue-resource.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script>
        var token = '{!! csrf_token() !!}';
        var vm = new Vue({
            el: "#searchFreelancers",
            data: {
                tbxName:'',
                freelancers:[],
                skills:'',
                country:'',
                pagination:'',
                page:1,
                Orginalfreelancers:[],
                price_range:'',
            },
            methods: {

                resetFilters()
                {
                    this.tbxName = '';
                    this.skills = '';
                    this.country = '';
                    this.price_range = '';
                    this.getExperts();
                },

                getParameterByName(name, url) {
                    if (!url) url = window.location.href;
                    name = name.replace(/[\[\]]/g, '\\$&');
                    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                        results = regex.exec(url);
                    if (!results) return null;
                    if (!results[2]) return '';
                    return decodeURIComponent(results[2].replace(/\+/g, ' '));
                },

                getExperts()
                {
                    var link = '/get-experts?page='+this.page;
                    this.$http.get(link).then(function(response){
                        this.freelancers = response.data.data;
                        this.Orginalfreelancers = response.data.data;
                        this.pagination = response.data;
                        var nextPageUrl  = response.data.next_page_url;
                        var page = this.getParameterByName('page', nextPageUrl)
                        this.page = page;
                    }, function(error){
                    });
                },

                previewBack()
                {
                    var pagination = this.pagination;
                    var prevbackUrl = pagination.prev_page_url;
                    console.log(prevbackUrl);
                    var page = this.getParameterByName('page', prevbackUrl)
                    this.page = page-1;
                },

                getJobPosition(users)
                {
                    var apps = users.job_position;
                    const locale = '{{\Config::get('app.locale')}}';

                    if (locale === 'cn') {
                        return apps ? apps['name_cn'] : '' ;
                    } else  {
                        return apps ? apps['name_en'] : '';
                    }

                },

                getSkills(skills)
                {
                    var s = skills.skills;
                    var skill = [];
                    var arr = "";
                    s.map(function (items) {
                        if(items.experience >= 1)
                        {
                            var sk = items.skill ? items.skill.name_en : '';
                            arr += ('<div class=\'bgSkillColor\'>'+sk+'</div>');
                        } else if(items.is_client_verified === 1)
                        {
                            var sk = items.skill ? items.skill.name_en : '';
                            arr += ('<div class=\'bgSkillColor\'>'+sk+'</div>');
                        } else {
                            var sk = items.skill ? items.skill.name_en : '';
                            arr += ('<div class=\'bgSkillColor\'>'+sk+'</div>');
                        }
                    });
                    return arr;

                },

                searchbox(val)
                {
                    let arr = [];
                    if(val === 'country') {
                        var th = this;
                        this.Orginalfreelancers.map(function (items) {
                            var strRegExPattern = '\\b'+th.country+'\\b';
                            var matching = items.country_name.match(new RegExp(strRegExPattern,'g'));
                            if (matching) {
                                arr.push(items);
                            }
                        });
                        this.freelancers =  arr;
                    } else if(val === 'name'){
                            var t = this;
                            this.Orginalfreelancers.map(function (items) {
                                var strName = '\\b'+t.tbxName+'\\b';
                                var matching = items.name.match(new RegExp(strName,'g'));
                                if (matching) {
                                    arr.push(items);
                                }
                            });
                        this.freelancers =  arr;
                    } else if(val === 'skill'){
                        var ts = this;
                        this.Orginalfreelancers.filter(function (items) {
                            items.skills.filter(function (item) {
                                console.log("match");
                                var match = item.skill.name_en.toLowerCase() === ts.skills.toLowerCase();
                                if(match === true)
                                {
                                    arr.push(items);
                                }
                            });
                        });
                        this.freelancers =  arr;
                    } else if(val === 'price'){
                        var tp = this;
                        this.Orginalfreelancers.map(function (items) {
                            var price = parseInt(tp.price_range);
                            var value = parseInt(items.hourly_pay);
                            var priceMatch = price===value;
                            if(priceMatch === true)
                            {
                                arr.push(items);
                            }
                        });
                        this.freelancers =  arr;
                    } else{
                        this.Orginalfreelancers = this.freelancers;
                    }

                }
            },
            mounted() {
                this.getExperts()
            }
        });
    </script>
@endsection
