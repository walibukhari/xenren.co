<!-- Emai lModel Modal -->
<div class="modal fade" id="emailModel" role="dialog">
    <div class="modal-dialog facebook-success-dialog email-Model-dialog">
        <div class="modal-content facebook-success-content email-pass-conpassword-content" style="border-radius: 2px !important;">
            <div class="modal-header facebook-register-img facebook-success-header text-right">
                <a href="" data-dismiss="modal" class="close-btn"><img src="{{ asset('images/Delete-100.jpg') }}"></a>
                <h4 class="modal-title facebook-success-title email-model-title">
                    <div class="row">
                        <div class="col-md-12 text-center logo-icon-img">
                            <img src="{{ asset('images/email-popup-logo.jpg') }}">
                        </div>
                    </div>
                </h4>
            </div>
            <div class="modal-body email-pass-confirn-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Email" name="email" id="email"
                                   value="{{\Auth::user()->email}}" disabled="">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="Password" id="pwd" name="password">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="pwd1"
                                   placeholder="Re Enter password" name="password">
                        </div>

                        <div class="form-group">
                            <select name="city" id="js-example-data-array"
                                    class="js-example-data-array form-control">
                            </select>


                                <button
                                        onclick="getLocation(showCityInFacebookBinding);"
                                        class="btn find-experts-offer pull-right"
                                        type="button"
                                        style="height: 48px;
                                        width: 130px;
                                        border:2px solid #57af33;
                                        color:#57af33;
                                        background-color: white;
                                        border-radius: 5px;
                                        position: relative;
                                        top: 0px;
                                        right: -7px;">
                                    <img style=" position: relative; bottom: 7px" src="{{asset('images/location1.png')}}">
                                    <small style="position: relative; bottom: 7px"><b>   {{trans('common.location')}} </b></small>
                                </button>


                                {{--<img class="btn find-experts-offer pull-right" src="{{ asset('images/location.jpg') }}" onclick="getLocation(showCityInFacebookBinding);" style="height: 80px;--}}
    {{--margin-right: 0px;--}}
    {{--padding-right: 0px;--}}
    {{--position: relative;--}}
    {{--bottom: 15px;--}}
    {{--right: -7px;">--}}
                        </div>

                        <button type="button" class="btn btn-success confirmPassword" id="confirmPassword">Submit
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>