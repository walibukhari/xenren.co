@extends('frontend-xenren.layouts.default-new')

@section('title')
    {{ trans('home.title') }}
@endsection

@section('keywords')
    {{ trans('home.keywords') }}
@endsection

@section('description')
    {{ trans('home.description') }}
@endsection

@section('author')
    Xenren
@endsection

@section('header')
    <link href="/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="/custom/css/frontend/home.css" rel="stylesheet" type="text/css" /> -->
    <!-- <link href="/custom/css/frontend/home-new-custom.css" rel="stylesheet" type="text/css" /> -->
    <link href="/custom/css/frontend/modalSetUsername.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/modalSetContactInfo.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/modalAddNewSkillKeywordv2.css" rel="stylesheet" type="text/css" />
    <link href="/css/sales-version.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/searchButtons.css" rel="stylesheet" type="text/css" />

    {{-- For popup design --}}
    <link rel="stylesheet" type="text/css" href="https://dbrekalo.github.io/fastselect/dist/fastselect.min.css">
    <link href="/custom/css/frontend/homePagePopup.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <!-- Content -->
    <!-- Revolution slider -->
    <section id="slider">
        <div class="slider-logo">
            <img src="/images/N-logo_dark01.png" alt="Logo" width="134px" />
            <h2 class="{{ $language == 1 ? 'big-text' : '' }}">{{ trans('home.legends_lair') }}</h2>
            <h3 class="{{ $language == 1 ? 'big-text' : '' }}">{{ trans('home.hall_of_talent') }}</h3>
        </div>

        <div class="row">
            <div class="col-md-5 search-main">
                <div id="msSearchBar">
                    {{--load to slow, temporary design--}}
                    <div class="ms-ctn form-control  ms-no-trigger ms-ctn-focus" style="" id="msSearchBar">
                        <span class="ms-helper" style="display: none;"></span>

                        <div class="ms-sel-ctn">
                            <input class="search-box-home" placeholder="{{ trans('common.search') }}" style="width: 492px;" type="text">
                            <div style="display: none;"></div>
                        </div>
                    </div>
                </div>
                <i class="fa fa-search btn-search"></i>
                <div class="row">
                    <div class="col-xs-12 col-md-12 search-type-div {{Session::get('lang')=='cn'? 'search-type-cn' : 'search-type-en' }}">
                        <div class="col-xs-4 col-md-4">
                            <button id="findProject" class="search-type-btn-default find-project-btn search-type-text pull-left" data-search-type="find-project">
                                {{ trans('common.find_project') }}
                            </button>
                        </div>
                        <div class="col-xs-4 col-md-4">
                            <button id="findFreelancer" class="search-type-btn-default find-freelancer-btn search-type-text" data-search-type="find-freelancer">
                                {{ trans('common.find_freelancer') }}
                            </button><!-- as -->
                        </div>
                        <div class="col-xs-4 col-md-4">
                            <button id="findCoFounder" class="search-type-btn-default find-co-founder-btn search-type-text pull-right" data-search-type="find-co-founder">
                                {{ trans('common.find_co_founder') }}
                            </button>
                        </div>
                    </div>
                </div>

                <span id="search-type" style="display: none;"></span>
            </div>
        </div>
        <input type="hidden" id="skill_id_list" name="skill_id_list" value="{{ isset($filter['skill_id_list'])? $filter['skill_id_list']: '' }}">
    </section>
    <!-- /#Revolution slider -->

    <!-- Testimonial -->
    <section id="testimonial" class="section testimonial">
        <div class="container">
            <div class="row">
                <div style="padding-left:13%;padding-right:13%;" class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
                    <h2 class="heading">{{ trans('home.people_say') }}</h2>
                    <h2 class="heading-line"></h2>

                    <div style="margin-top: 10%!important;"  class="testimonial-1 owl-carousel owl-single-all owl-pagi-1 owl-nav-1 pagi-center owl testimonial-1 mrg-top-50">
                        @if (Session::get('lang')=="en")
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6 col-xs-12 people-shortstory-img text-center">
                                        <img src="/images/N-team1.png">
                                    </div>
                                    <div class="col-md-8 col-sm-6 col-xs-12 people-shortstory">
                                        <h3>{{ trans('home.people_helen') }}</h3>
                                        <h5>{{ trans('home.entrepreneur') }}</h5>
                                        <p>“ {{ trans('home.people_helen_say') }} ”</p>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6 col-xs-12 people-shortstory-img text-center">
                                        <img src="/images/N-team2.png">
                                    </div>
                                    <div class="col-md-8 col-sm-6 col-xs-12 people-shortstory">
                                        <h3>{{ trans('home.people_xubing') }}</h3>
                                        <h5>{{ trans('home.hr') }}</h5>
                                        <p>“ {{ trans('home.people_xubing_say') }} ”</p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6 col-xs-12 people-shortstory-img text-center">
                                        <img src="/images/N-team3.png">
                                    </div>
                                    <div class="col-md-8 col-sm-6 col-xs-12 people-shortstory">
                                        <h3>{{ trans('home.people_khairul') }}</h3>
                                        <h5>{{ trans('home.freelancer') }}</h5>
                                        <p>“ {{ trans('home.people_khairul_say') }} ”</p>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6 col-xs-12 people-shortstory-img text-center">
                                        <img src="/images/N-team17.png">
                                    </div>
                                    <div class="col-md-8 col-sm-6 col-xs-12 people-shortstory">
                                        <h3>{{ trans('home.people_MsWang') }}</h3>
                                        <h5>{{ trans('home.entrepreneur') }}</h5>
                                        <p>“ {{ trans('home.people_MsWang_say') }} ”</p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6 col-xs-12 people-shortstory-img text-center">
                                        <img src="/images/N-team15.png">
                                    </div>
                                    <div class="col-md-8 col-sm-6 col-xs-12 people-shortstory">
                                        <h3>{{ trans('home.people_MsLi') }}</h3>
                                        <h5>{{ trans('home.Technicalstaff') }}</h5>
                                        <p>“ {{ trans('home.people_MsLi_say') }} ”</p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6 col-xs-12 people-shortstory-img text-center">
                                        <img src="/images/N-team16.png">
                                    </div>
                                    <div class="col-md-8 col-sm-6 col-xs-12 people-shortstory">
                                        <h3>{{ trans('home.people_MrZhang') }}</h3>
                                        <h5>{{ trans('home.entrepreneur') }}</h5>
                                        <p>“ {{ trans('home.people_MrZhang_say') }} ”</p>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <!--  /testimonial  -->
        </div>
        <!-- /container -->
    </section>
    <!-- /testimonial -->

    <!-- Team 1 -->
    <section id="team" class="section">
        <div class="container">
            <h2 class="heading">{{ trans('home.meet_our_team') }}</h2>
            <h2 class="heading-line"></h2>
            <div class="row team-main">
                <div class="col-md-3 team-sub text-center">
                    <div class="team-block">
                        <img src="/images/N-team2.png">
                        <a href="#" class="overlay-team"><i class="fa fa-expand" aria-hidden="true"></i></a>
                    </div>
                    <h3>john doe</h3>
                    <small>{{ trans('home.ui_designer') }}</small>
                </div>
                <div class="col-md-3 team-sub text-center">
                    <div class="team-block">
                        <img src="/images/N-team1.png">
                        <a href="#" class="overlay-team"><i class="fa fa-expand" aria-hidden="true"></i></a>
                    </div>
                    <h3>Samntha smith</h3>
                    <small>{{ trans('home.co_owner') }}</small>
                </div>
                <div class="col-md-3 team-sub text-center">
                    <div class="team-block">
                        <img src="/images/N-team3.png">
                        <a href="#" class="overlay-team"><i class="fa fa-expand" aria-hidden="true"></i></a>
                    </div>
                    <h3>Anna Mcdoogle</h3>
                    <small>{{ trans('home.content_writer') }}</small>
                </div>
                <div class="col-md-3 team-sub text-center">
                    <div class="team-block">
                        <img src="/images/N-team4.png">
                        <a href="#" class="overlay-team"><i class="fa fa-expand" aria-hidden="true"></i></a>
                    </div>
                    <h3>dan wood</h3>
                    <small>{{ trans('home.ui_designer') }}</small>
                </div>
            </div>
        </div>
        <!-- /container -->
    </section>
    <!-- /Team 1-->

    <!-- About -->
    <section id="about" class="section">
        <div class="container">
            <h2 class="heading">{{ trans('home.about_us') }}</h2>
            <h2 class="heading-line"></h2>
            <div class="row about-main">
                <div class="col-md-12 about-text">
                    <p> {{ strip_tags(trans('home.team_desc'), '<br/>') }} </p>
                </div>
                <div class="col-md-3 about-sub text-center">

                    <a href="{{ URL::route('frontend.meet-the-team2') }}">
                        <div class="row our-teams">
                            <div class="col-md-4 col-xs-12 col-sm-4 team-img">
                                <div class="img">
                                    <img src="/images/fong.jpg" style="margin: 10px 0px 0px 6px;">
                                </div>
                            </div>
                        </div>
                        <h3>{{ trans('home.team_fong') }}</h3>
                        <small>{{ trans('home.team_CEO') }}</small>
                    </a>

                    <br>
                    <a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    <a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                    <a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-3 about-sub text-center">
                    <a href="{{ URL::route('frontend.meet-the-team2') }}">
                        <div class="row our-teams">
                            <div class="col-md-4 col-xs-12 col-sm-4 team-img">
                                <div class="img">
                                    <img src="/images/marsel.jpg" style="margin:25px 0px 0px -8px;">
                                </div>
                            </div>
                        </div>
                        <h3>{{ trans('home.team_Xenren') }}</h3>
                        <small>{{ trans('home.team_CTO') }}</small>
                    </a>a
                    <br>
                    <a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    <a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                    <a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-3 about-sub text-center">
                    <a href="{{ URL::route('frontend.meet-the-team2') }}">
                        <div class="row our-teams">
                            <div class="col-md-4 col-xs-12 col-sm-4 team-img">
                                <div class="img">
                                    <img src="/images/hasper.jpg" style="margin:20px 0px 0px 0px;">
                                </div>
                            </div>
                        </div>
                        <h3>{{ trans('home.team_chenlou') }}</h3>
                        <small>{{ trans('home.team_CPO') }}</small>
                    </a>
                    <br>
                    <a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    <a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                    <a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-3 about-sub text-center">
                    <a href="{{ URL::route('frontend.meet-the-team2') }}">
                        <div class="row our-teams">
                            <div class="col-md-4 col-xs-12 col-sm-4 team-img">
                                <div class="img">
                                    <img src="/images/niew.jpg" style="margin:10px 0px 0px -8px;">
                                </div>
                            </div>
                        </div>
                        <h3>{{ trans('home.team_chenkun') }}</h3>
                        <small>{{ trans('home.team_CCO') }}</small>
                    </a>
                    <br>
                    <a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    <a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                    <a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
        <!-- /container -->
    </section>
    <!-- /About-->

    <!-- About -->
    <section id="services" class="section">
        <div class="container">
            <h2 class="heading">{{ trans('home.services_security') }}</h2>
            <h2 class="heading-line"></h2>
            <div class="row services-main">
                <div class="col-md-6">
                    <div class="row home-services-main">
                        <div class="col-md-3 home-services-main-l">
                            <a href="{{ URL::route('frontend.about-us') }}">
                                <div class="home-services-main-l-icon">
                                    <img src="/images/N-icon1.png" data-src="money" alt="money guarantee" />
                                </div>
                            </a>
                        </div>
                        <div class="col-md-9 home-services-main-r">
                            <a href="{{ URL::route('frontend.about-us') }}">
                                {{ trans('home.money_guarantee') }}
                            </a>
                            <p>
                                {{ trans('home.money_guarantee_text') }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row home-services-main">
                        <div class="col-md-3 home-services-main-l">
                            <a href="{{ URL::route('frontend.about-us') }}">
                                <div class="home-services-main-l-icon">
                                    <img src="/images/N-icon4.png" data-src="money" alt="money guarantee" />
                                </div>
                            </a>
                        </div>
                        <div class="col-md-9 home-services-main-r">
                            <a href="{{ URL::route('frontend.about-us') }}">
                                {{ trans('home.skill_verify') }}
                            </a>
                            <p>
                                {{ trans('home.skill_verify_text') }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row services-main">
                <div class="col-md-6">
                    <div class="row home-services-main">
                        <div class="col-md-3 home-services-main-l">
                            <a href="{{ URL::route('frontend.about-us') }}">
                                <div class="home-services-main-l-icon">
                                    <img src="/images/N-icon3.png" data-src="money" alt="money guarantee" />
                                </div>
                            </a>
                        </div>
                        <div class="col-md-9 home-services-main-r">
                            <a href="{{ URL::route('frontend.about-us') }}">
                                {{ trans('home.tech_support') }}
                            </a>
                            <p>
                                {{ trans('home.tech_support_text') }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row home-services-main">
                        <div class="col-md-3 home-services-main-l">
                            <a href="{{ URL::route('frontend.about-us') }}">
                                <div class="home-services-main-l-icon">
                                    <img src="/images/N-icon2.png" data-src="money" alt="money guarantee" />
                                </div>
                            </a>
                        </div>
                        <div class="col-md-9 home-services-main-r">
                            <a href="{{ URL::route('frontend.about-us') }}">
                                {{ trans('home.customer_service') }}
                            </a>
                            <p>
                                {{ trans('home.customer_service_text') }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row services-main">
                <div class="col-md-6">
                    <div class="row home-services-main">
                        <div class="col-md-3 home-services-main-l">
                            <a href="{{ URL::route('frontend.about-us') }}">
                                <div class="home-services-main-l-icon">
                                    <img src="/images/N-icon5.png" data-src="money" alt="money guarantee" />
                                </div>
                            </a>
                        </div>
                        <div class="col-md-9 home-services-main-r">
                            <a href="{{ URL::route('frontend.about-us') }}">
                                {{ trans('home.skillfull_person') }}
                            </a>
                            <p>
                                {{ trans('home.skillfull_person_text') }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row home-services-main">
                        <div class="col-md-3 home-services-main-l">
                            <div class="home-services-main-l-icon order">
                                <a href="{{ URL::route('login') }}" class="centering pagination-centered">{!! trans('home.order_now') !!} <br> <i class="fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                        <div class="col-md-9 home-services-main-r">
                            <p>
                                {!! trans('home.team_desc') !!}
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- /container -->
    </section>
    <!-- /About-->

    <input id="user-id" type="hidden" value="{{ isset($_G['user']->id)? $_G['user']->id : '' }}">
    <input id="user-status" type="hidden" value="{{ isset($_G['user']) && $_G['user']->getStatusIds() != "" ? $_G['user']->getStatusIds() : '' }}">

    @include('frontend.userCenter.modalChangeUserStatus')
    @include('frontend.includes.modalAddNewSkillKeywordv2', [
        'tagColor' => \App\Models\Skill::TAG_COLOR_WHITE
    ])

    @if( \Auth::check())
        <input id="username" type="hidden" value="{{ isset($_G['user']->name)? $_G['user']->name : '' }}">
        <a id="editUsername"
           href="{{ route('frontend.modal.setusername', ['user_id' => $_G['user']->id ] ) }}"
           data-toggle="modal"
           data-target="#modalSetUsername"
           style="display:none">
        </a>

        <input id="isContactInfoExist" type="hidden" value="{{ $_G['user']->isContactInfoExist() == true? 'true' : 'false' }}">
        <a id="editContactInfo"
           href="{{ route('frontend.modal.setcontactinfo', ['user_id' => $_G['user']->id ] ) }}"
           data-toggle="modal"
           data-target="#modalSetContactInfo"
           style="display:none">
        </a>

        <input id="isFirstTimeSkillSubmit" type="hidden" value="{{ isset($_G['user']->is_first_time_skill_submit)? $_G['user']->is_first_time_skill_submit : '0' }}">
        @include('frontend.userCenter.modalChangeUserSkill', [ 'isSkipButtonAllow' => true ])
    @endif

@endsection

@section('modal')
    <div id="modalSetUsername" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>

    <div id="modalSetContactInfo" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>

    <!-- X popup_5 Modal --> 
    <div class="modal fade" id="email-registration-facebook" role="dialog">
        <div class="modal-dialog facebook-success-dialog email-Model-dialog">
            <div class="modal-content facebook-success-content email-Model-content">
                <div class="modal-header facebook-success-header text-right">
                        <a href="" data-dismiss="modal" class="close-btn"><img src="{{ asset('images/Delete-100.jpg') }}"></a>
                        <h4 class="modal-title facebook-success-title email-model-title">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <img src="{{ asset('images/email-popup-logo.jpg') }}">      
                            </div>
                        </div>
                    </h4>
                </div>
                <div class="modal-body Xpopup5-body facebook-success-body email-Model-body">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <p class="p-0 m-0">Thanks</p>
                            <h2 class="p-0 m-0">Email Register</h2>
                        </div>
                    </div>
                </div>
                <div class="modal-footer facebook-success-footer email-registration-facebook">
                    <button type="button" class="btn btn-defualt btn-block"><i class="fa fa-facebook" aria-hidden="true"></i> Bind Facebook Account</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#email-registration-facebook').modal('show');
        });
    </script>
    <script type="text/javascript">
        var lang = {
            error: '{{trans('common.unknown_error')}}',
            please_choose_search_type : "{{ trans('common.please_choose_search_type') }}",
            please_select_skills  : "{{ trans('common.please_select_skills') }}",
            help_us_improve_skill_keyword : "{{ trans('common.help_us_improve_skill_keyword') }}",
            unable_to_request : "{{ trans('common.unable_to_request') }}",
            please_select_freelancer_project_cofounder : "{{ trans('common.please_select_freelancer_project_cofounder') }}",
            freelancer_require_what_skill : "{{ trans('common.freelancer_require_what_skill') }}",
            what_skill_you_have : "{{ trans('common.what_skill_you_have') }}",
            co_founder_require_what_skill : "{{ trans('common.co_founder_require_what_skill') }}",
            new_skill_keywords : "{{ trans('common.new_skill_keywords') }}",
            please_use_wiki_or_baidu_support_your_keyword : "{{ trans('common.please_use_wiki_or_baidu_support_your_keyword') }}",
            one_of_skill_input_empty : "{{ trans('common.one_of_skill_input_empty') }}",
            maximum_ten_skill_keywords_allow_to_submit : "{{ trans('common.maximum_ten_skill_keywords_allow_to_submit') }}",
        }

        var url = {
            jobs : '{{ route('frontend.jobs.index') }}',
            findExperts : '{{ route('frontend.findexperts') }}',
            login : '{{ route('login') }}',
            api_skipAddUserSkill : '{{ route('api.skipadduserskill') }}',
            personalInformationChange : '{{ route('frontend.personalinformation.change') }}',
            weixinweb: '{{ route('weixin.login') }}'
        }

        var skillList = {!! $skillList !!};
        var skill_id_list = '{{ isset($_G['user'])? $_G['user']->getUserSkillList(): '' }}';

        var is_guest = {{ Auth::guard('users')->guest() == 1? 'true': 'false' }};

        function checkFirstTimeFilling(userId)
        {
            var userStatus = $('#user-status').val();
            var username = $('#username').val();
            var isFirstTimeSkillSubmit = $('#isFirstTimeSkillSubmit').val();
            var isContactInfoExist = $('#isContactInfoExist').val();

            if( userId > 0 && userStatus == '0' || userId > 0 && userStatus == '' )
            {
                $('#modalChangeUserStatus').modal('show');
            }
            else if( userId > 0 && username == '' )
            {
                $('#editUsername').trigger('click');
            }
            else if( userId > 0 && isFirstTimeSkillSubmit == 0 )
            {
                $('#modalChangeUserSkill').modal('show');
            }
            else if( userId > 0 && isContactInfoExist == 'false' )
            {
                $('#editContactInfo').trigger('click');
            }
        }

        window.onload = function()
        {
            if(isWeiXin() && is_guest == true)
            {
//                alert("weixin browser");
                window.location.href = url.weixinweb;
            }
//            else
//            {
//                alert("normal browser");
//            }
        }

        function isWeiXin()
        {
            var ua = window.navigator.userAgent.toLowerCase();
            if(ua.match(/MicroMessenger/i) == 'micromessenger')
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    </script>

    <script src="/custom/js/frontend/home.js" type="text/javascript"></script>
    <script src="/custom/js/frontend/modalChangeUserStatus.js" type="text/javascript"></script>
    {{--<script src="/custom/js/frontend/modalAddNewSkillKeyword.js" type="text/javascript"></script>--}}
    <script src="/custom/js/frontend/modalAddUserSkill.js" type="text/javascript" ></script>
    <script src="/custom/js/frontend/modalAddNewSkillKeywordv2.js" type="text/javascript" ></script>
    <script src="/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>

    {{-- for popup design --}}
    <script src="https://dbrekalo.github.io/fastselect/dist/fastselect.standalone.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.multipleSelect').fastselect();
        });
    </script>
@endsection