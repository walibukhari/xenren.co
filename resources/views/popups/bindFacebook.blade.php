<div class="modal fade" id="facebookModal" role="dialog">
    <div class="modal-dialog facebook-success-dialog">
        <div class="modal-content facebook-success-content" style="border-radius: 2px !important;">
            <div class="modal-header facebook-success-header text-right">
                <a href="" data-dismiss="modal" class="close-btn"><img src="{{ asset('/images/Delete-100.jpg') }}"></a>
                <h4 class="modal-title facebook-success-title">
                    @if(\Session::has('fb_err'))
                    <div class="alert alert-danger" style="text-align: left">
                        {{\Session::get('fb_err')}}
                    </div>
                    @endif
                    <div class="row">
                    <div class="col-md-12 text-center">
                        <img src="{{ asset('/images/icons/bind-facebook-success.png') }}">
                    </div>
                </div>
                </h4>
            </div>
            <div class="modal-body facebook-success-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2 class="p-0">{{ trans('home.bind_facebook') }}</h2>
                        <p>{{ trans('home.now_enjoy_access_in_one_click') }}</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer facebook-success-footer">
                <button type="button" style="display: flex;justify-content: center;align-items: center;" class="btn btn-success btn-block" id="bindFaceBook" onclick="window.location='{{ route('facebook.login') }}'">
                    <span class="idSpanBindFaceBook">{{ trans('home.bind_your_account') }}</span>
                    <div style="display: none;" class="lds-ring">
                        <div></div><div></div><div></div><div></div>
                    </div>
                </button>
            </div>
        </div>
    </div>
</div>
