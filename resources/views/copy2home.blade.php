@extends('frontend.layouts.default')

@section('title')
Home
@endsection

@section('description')
Home
@endsection

@section('author')
Xenren
@endsection

@section('header')
    <link href="/custom/css/auth/home.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="container" >
        <div class="row col-md-12 logo-banner">
            <img alt="" src="/images/banner.png">
        </div>
        <div class="row col-md-12 slogan">
            {{ trans('member.slogan') }}
        </div>
        <div class="row col-md-12 text-center">
            <div class="col-md-4">
                <img alt="" src="/images/team.jpg">
            </div>
            <div class="col-md-4">
                <img alt="" src="/images/about.jpg">
            </div>
            <div class="col-md-4">
                <img alt="" src="/images/success-sample.jpg">
            </div>
        </div>
        <div class="row col-md-12" style="height: 80px;">
        </div>
    </div>
@endsection

@section('footer')
@endsection
