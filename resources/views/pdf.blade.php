<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
          name='viewport'>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">
    <style type="text/css">
        html,p,body,h1,span,a,button,tr,td,th{
            font-family: "Montserrat", sans-serif;
        }
        @media print {
            .pTag {
                color:rgb(99, 184, 13) !important;
                -webkit-print-color-adjust: exact;
            }
        }
        .trtable{
            height: 200px;
        }
    </style>
</head>
<body>
    <div style="max-width: 720px;margin:0 auto;;padding-left: 0px;padding-right: 0px;">
        <div style="height: 90px;">
            <div style="width:500px;float: left;">
                <div style="margin-bottom: 12px;padding-left: 0px;">
                    <img style="position: absolute;
    top: 12px;width: 50px;float: left;" alt="Logo" src="{{$image}}" />
                    <div style="padding-left: 2px;position: relative;
                        top: 2px;margin-left:48px;
                    ">
                        <p class="pTag" style="height:40px;margin: 0px; font-size: 28px;font-weight: bold; letter-spacing: 0px;color:#63B80D;">XenRen</p>
                        <p class="pTag" style="color:#63B80D;font-size:8px;margin: 0px;position: relative;
    bottom: 6px;">Coworker & Coworking Space</p>
                    </div>
                </div>
            </div>
            <div style="float:right;text-align: right;width:210px;">
                <p class="pTag2" style="
                margin: 0px;
                color: #000;
                font-size: 30px;
                font-weight: 600;
                ">INVOICE</p>
                <p style="margin-top:10px;margin-bottom:10px;color: #000;font-weight: 500;font-size: 15px;">Invoice Details</p>
            </div>
        </div>
        <br>
        <div style="height: 90px;">
            <div style="float:left;width:360px;">
                <div style="width:475px;">
                    <p style="margin: 0px;
                padding-left: 0px;
                padding-right: 5px;
                color: #000;
                font-size: 15px;
                font-weight: 500;
                float: left;
                    ">
                        To&nbsp;:
                    </p>
                    <p style="width: 442px;
    float: right;text-transform: capitalize;margin:0px;font-weight:500;font-size: 15px;color: #5C5C5C;padding-left: 0px;">
                        {{$to->username}}
                    </p>
                    <br>
                    <div style="
                    width:500px;padding-left: 0px;margin-top: 3px;"
                    >
                        <p style="margin: 0px; font-weight: 400;font-size: 12px;">{{$to->email}}</p>
                    </div>
                </div>
            </div>
            <div style="width:210px;float:right;text-align: right">
                <div style="margin: 0px; padding-right: 0px;">
                    <p style="
                    text-align: right;
                    padding-bottom: 2px;
                    margin: 0px;
                    font-size: 13px;
                    ">Date: &nbsp; <span style="">{{\Carbon\Carbon::parse($date)->format('y:m:d')}}</span></p>
                    <p style="
                    text-align: right;
                    padding-bottom: 2px;
                    margin: 0px;
                    font-size: 13px;
                    font-weight: bold;"><b style="font-weight: 500;
    color: #000;">Due Date:&nbsp;<span style="">{{\Carbon\Carbon::now()->format('y:m:d')}}</span></b></p>
                    <p style="
                    text-align: right;
                    padding-bottom: 2px;
                    margin: 0px;
                    font-size: 13px;
                    ">Total Amount: <span style="">&nbsp; ${{number_format($total_amount,2)}}</span></p>
                    <p style="
                    margin: 0px;
                    padding-bottom: 2px;
                    text-align: right;
                    font-size: 13px;
                    font-weight: bold;"><b style="font-weight: 500;
    color: #000;">Total Due: <span style="">&nbsp; ${{number_format($total_amount,2)}}</span></b></p>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div style="max-width: 720px;padding-left: 0px;margin:0 auto;padding-right: 0px;">
                <table class="table table-bordered" style="">
                        <thead>
                        <tr>
                            <th style="color: #000;font-weight:500;font-size:14px;" >Description</th>
                            <th style="text-align: center;color: #000;font-size:14px;font-weight:500;">Time min.</th>
                            <th style="text-align: center;color: #000;font-size:14px;font-weight:500;">Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($invoices as $invoice)
                            @if($invoice->comment != '')
                                <tr>
                                    <td>{{$invoice->comment}}</td>
                                    <td style="width: 13%;text-align: center;">20 Mins.</td>
                                    <td style="width: 13%;text-align: center;">$ {{number_format($invoice->amount,2)}}</td>
                                </tr>
                            @endif
                        @endforeach
                        @if(isset($invoices) && count($invoices) <= 2)
                            <tr>
                                <td class="trtable"></td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endif
                        <tr>
                            <td><b style="color:#000;font-weight: 600;font-size:18px;padding-left: 12px;">Total</b></td>
                            <td style="width: 13%;text-align: center;color: #000;font-weight:600;">40 Mins.</td>
                            <td style="width: 13%;text-align: center;color: #000;font-weight:600;">$ {{number_format($total_amount,2)}}</td>
                        </tr>
                        </tbody>
                    </table>
                <div class="row">
                    <div style="">
                        <div>
                            <p style="margin: 0px;
                padding-left: 15px;
                    width: 90px;
                float: left;
                padding-right: 8px;
                color: #000;
                font-size: 18px;
                font-weight: 500;">
                                From&nbsp;&nbsp;:
                            </p>
                            <p style="float: right;
                                width: 660px;text-transform: capitalize;margin:0px;
                                font-weight:500;font-size: 18px;color: #000;padding-left:0px;
                                ">
                                Legends Lair
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div style="display: flex; align-items: center;">
                        <div style="padding-top:0px;display: flex;align-items: center;margin-bottom: 25px;padding-left: 15px;">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div style="display: flex; align-items: center;">
                        <div style="padding-top:0px;display: flex;align-items: center;margin-bottom: 12px;padding-left: 15px;">
                            <p style="text-transform: capitalize;margin:0px;font-size: 17px;color: #000;padding-left: 0px;font-weight: 500;">
                                Invoice Created Via :
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div style="display: flex; align-items: center;">
                        <div style="display: flex;align-items: center;margin-bottom: 12px;padding-left: 10px;">
                            <img style="position: absolute;
    top: 12px;width: 50px;float: left;" alt="Logo" src="{{$image}}" />
                            <div style="padding-left: 2px;height: 48px;position: relative;
    top: 2px;">
                                <p class="pTag" style="height:40px;margin: 0px;font-size: 25px;font-weight: bold; letter-spacing: 0px;color:#63B80D;display: flex;
    justify-content: flex-end;">XenRen</p>
                                <p class="pTag" style="color:#63B80D;font-size:7px;margin: 0px;position: relative;
    bottom: 6px;">Coworker & Coworking Space</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</body>
</html>
