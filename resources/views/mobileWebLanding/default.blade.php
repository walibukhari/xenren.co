<!DOCTYPE html>
<html lang="{{ trans('common.lang') }}">

<head>
    <meta charset="utf-8">
    <title>{{ trans('common.mobileWebLanding_title') }}</title>
    <meta name="description" content="{{ trans('common.mobileWebLanding_desc') }}">
    <meta name="keywords" content="{{ trans('common.mobileWebLanding_keyword') }}">
    <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <meta content="@yield('description')" name="description"/>
    <meta content="@yield('author')" name="author"/>
    <meta property="wb:webmaster" content="4bfa78a5221bb48a" />
    <meta property="qc:admins" content="2552431241756375" />
    <!-- FAVICON -->
    <link href="{{$_G['xenren_path']}}images/logo/fav_new.png" rel="icon" type="image/x-icon" />
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    
    <link rel="stylesheet" href="{{$_G['xenren_path']}}css/bootstrap.min.css" />
    <link rel="stylesheet" href="{{$_G['xenren_path']}}css/font-awesome.min.css" />
    <link rel="stylesheet" href="{{$_G['xenren_path']}}css/style.css" />
    <link rel="stylesheet" href="/custom/css/frontend/mobile-home.css" />
    <link rel="stylesheet" href="/assets/global/plugins/magicsuggest/magicsuggest-min.css" type="text/css" />
    <link href="/custom/css/frontend/home.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/custom/css/mobileWebLanding/custom.css" />
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link href="/custom/css/frontend/modalSetContactInfo.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css">

    <style>
        input::-webkit-input-placeholder {
            font-size: 25px;
        }
    </style>
</head>

<body style="margin-bottom: 25px;">
    <div class="row">
        <div class="col-md-offset-4 col-md-4 col-xs-12">
            @include('mobileWebLanding.header')
            <section id="slider">
                <div class="tp-banner-container">            
                    <div class="tp-banner rev-interactive">
                        <div class="hero-caption caption-center caption-height-center">
                            <img src="{{$_G['xenren_path']}}images/logo/{{ trans('common.logo_name') }}.png" alt="Logo" class="banner-logo"/>
                                <div>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-6 banner-searchbox">
                                        <div id="msSearchBar">
                                            <div class="ms-ctn form-control  ms-no-trigger ms-ctn-focus" style="" id="msSearchBar">
                                                <span class="ms-helper" style="display: none;"></span>
                                                
                                                <div class="ms-sel-ctn">
                                                    <input class="search-box-home" placeholder="{{ trans('common.search') }}" style="width: 492px;" type="text">
                                                    <div style="display: none;"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="display: block;">
                                            <i class="fa fa-search btn-search"></i>
                                        </div>
                                        <div class="row search-type-div-main">
                                            <div class="col-md-12 search-type-div">
                                                <button class="uppercase mobile-search-type-btn-default find-project-btn mobile-search-type-text" data-search-type="find-project">
                                                    {{ trans('common.find_project') }}
                                                </button>
                                                <button class="uppercase mobile-search-type-btn-default find-freelancer-btn mobile-search-type-text" data-search-type="find-freelancer">
                                                    {{ trans('common.find_freelancer') }}
                                                </button>
                                                <button class="uppercase mobile-search-type-btn-default find-co-founder-btn mobile-search-type-text" data-search-type="find-co-founder">
                                                    {{ trans('common.find_co_founder') }}
                                                </button>
                                            </div>
                                        </div>

                                        <span id="search-type" style="display: none;"></span>
                                    </div>
                                    <div class="col-md-3">
                                    </div>
                                </div>
                                <input type="hidden" id="skill_id_list" name="skill_id_list" value="{{ isset($filter['skill_id_list'])? $filter['skill_id_list']: '' }}">
                        </div>

                        <div id="particles-js"></div>

                    </div>
                    <!-- /tp-banner -->
                </div>
                <!-- /tp-banner-container -->
            </section>

            <section class="slider-custom">
                <div class="row">
                    <div class="col-md-12 text-center section-header">
                        <img src="/images/icons/thamb.png" height="70px" width="70px">
                        <h1>不断的客户好评</h1>
                        <p>我们取得的成绩, 源自客户的配合与选择</p>
                    </div>
                </div>
                <div class="row" style="margin: 0px; overflow: hidden;">
                    <div id="myCarousel" class="carousel fdi-Carousel slide">
                     <!-- Carousel items -->
                        <div class="carousel fdi-Carousel slide" id="eventCarousel" data-interval="0">
                            <div class="carousel-inner onebyone-carosel">
                                <div class="item active">
                                    <div class="col-md-4 col-xs-4 item-img">
                                        <a href="javascript:void(0)">
                                            <img src="/images/mob-slid03.jpg">
                                            <div class="overlay"></div>
                                        </a>
                                    </div>
                                    <div class="col-md-12 text-center-custom">
                                        <h1>Jack</h1>
                                        <p>过客科技联合创始人</p>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12 desc">
                                                <img src="/images/icons/coma-01.png" width="45px" class="pull-left quot-left">
                                                <p> 神人帮我找到了合适的合伙人，这是一次满意的合作!</p>
                                                <img src="/images/icons/coma-02.png" width="45px" class="pull-right quot-right">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="col-md-4 col-xs-4 item-img">
                                        <a href="javascript:void(0)">
                                            <img src="/images/mob-slid02.jpg">
                                            <div class="overlay"></div>
                                        </a>
                                    </div>
                                    <div class="col-md-12 text-center-custom">
                                        <h1>Judy</h1>
                                        <p>自由职业者</p>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12 desc">
                                                <img src="/images/icons/coma-01.png" width="45px" class="pull-left quot-left">
                                                <p> 通过神人， 我接到了靠谱的项目</p>
                                                <img src="/images/icons/coma-02.png" width="45px" class="pull-right quot-right">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="col-md-4 col-xs-4 item-img">
                                        <a href="javascript:void(0)">
                                            <img src="/images/mob-slid01.jpg">
                                            <div class="overlay"></div>
                                        </a>
                                    </div>
                                    <div class="col-md-12 text-center-custom">
                                        <h1>Ben</h1>
                                        <p>伯乐软件公司人力资源经理</p>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12 desc">
                                                <img src="/images/icons/coma-01.png" width="45px" class="pull-left quot-left">
                                                <p>借助平台， 我们找到了靠谱的团队成员</p>
                                                <img src="/images/icons/coma-02.png" width="45px" class="pull-right quot-right">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <!-- <a class="left carousel-control" href="#eventCarousel" data-slide="prev"></a> -->
                            <!-- <a class="right carousel-control" href="#eventCarousel" data-slide="next"></a> -->
                        </div>
                        <!--/carousel-inner-->
                    </div>
                </div>
            </section>    

            <section class="section-part3">
                <div class="row">
                    <div class="col-md-12 text-center section-header">
                        <img src="/images/icons/icon-users01.png" height="70px" width="70px">
                        <h1>更加强力的用户</h1>
                        <p>越来越多的神人加入我们</p>
                    </div>
                </div><br>
                <div class="row main-div">
                    <div class="col-md-3 col-xs-4 section-left">
                        <img src="/images/tem-member01.png">
                    </div>
                    <div class="col-md-8 col-xs-7 section-mid">
                        <h2>Adam Johnson</h2>   
                        <p>自由设计师</p>
                    </div>
                    <div class="col-md-1 col-xs-1 section-right">
                        <a href="">  
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>

                <div class="row main-div">
                    <div class="col-md-3 col-xs-4 section-left">
                        <img src="/images/tem-member02.png">
                    </div>
                    <div class="col-md-8 col-xs-7 section-mid">
                        <h2>Uoau</h2>   
                        <p>前端工程师</p>
                    </div>
                    <div class="col-md-1 col-xs-1 section-right">
                        <a href="">  
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>

                <div class="row main-div">
                    <div class="col-md-3 col-xs-4 section-left">
                        <img src="/images/tem-member03.png">
                    </div>
                    <div class="col-md-8 col-xs-7 section-mid">
                        <h2>John do</h2>   
                        <p>前端工程师</p>
                    </div>
                    <div class="col-md-1 col-xs-1 section-right">
                        <a href="">  
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>

                <div class="row main-div">
                    <div class="col-md-3 col-xs-4 section-left">
                        <img src="/images/tem-member04.png">
                    </div>
                    <div class="col-md-8 col-xs-7 section-mid">
                        <h2>Mike sui</h2>   
                        <p>JAVA工程师</p>
                    </div>
                    <div class="col-md-1 col-xs-1 section-right">
                        <a href="">  
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>

                <div class="row last-div">
                    <div class="col-md-12 col-xs-12 text-center">
                        <p>查看更多神人</p>
                    </div>
                </div>
            </section>

            <section class="section-part4">
                <div class="row">
                    <div class="col-md-12 text-center section-header">
                        <img src="/images/icons/icon-star.png" height="70px" width="70px">
                        <h1>我们提供的服务</h1>
                        <p>让你更快适应我们的工作模式</p>
                    </div>
                </div>
                <br><br>
                <div class="row main-div">
                    <div class="col-md-3 col-xs-3 section-left text-center">
                        <img src="/images/icons/icon-aword.png" height="70px" width="70px">
                    </div>
                    <div class="col-md-8 col-xs-8 section-right">
                        <h2>资金托管</h2>   
                        <p>我们会非常慎重的保存用户的资金。付款都有第三方股托管用户的资金得到保障</p>
                    </div>
                </div>

                <div class="row main-div">
                    <div class="col-md-3 col-xs-3 section-left text-center">
                        <img src="/images/icons/icon-eie.png" height="70px" width="70px">
                    </div>
                    <div class="col-md-8 col-xs-8 section-right">
                        <h2>官方技能鉴定</h2>   
                        <p>平台上验证的神人都是官方亲自审核，同时案例也只有在完成项目后才给予。</p>
                    </div>
                </div>

                <div class="row main-div">
                    <div class="col-md-3 col-xs-3 section-left text-center">
                        <img src="/images/icons/icon-four.png" height="70px" width="70px">
                    </div>
                    <div class="col-md-8 col-xs-8 section-right">
                        <h2>客户咨询</h2>   
                        <p>遇到任何平台问题相关，我们的客服都为您提供耐心服务。</p>
                    </div>
                </div>

                <div class="row main-div">
                    <div class="col-md-3 col-xs-3 section-left text-center">
                        <img src="/images/icons/icon-setting.png" height="70px" width="70px">
                    </div>
                    <div class="col-md-8 col-xs-8 section-right">
                        <h2>技术支持</h2>   
                        <p>平台上验证的神人都是官方亲自审核，同时案例也只有在完成项目后才给予。</p>
                    </div>
                </div>

                <div class="row main-div">
                    <div class="col-md-3 col-xs-3 section-left text-center">
                        <img src="/images/icons/icon-headphone.png" height="70px" width="70px">
                    </div>
                    <div class="col-md-8 col-xs-8 section-right">
                        <h2>10万专家在线</h2>   
                        <p>平台的活跃用户超过10万。</p>
                    </div>
                </div>


            </section>
        @include('mobileWebLanding.footer')       
        </div>

        <div id="modalSetContactInfo" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                </div>
            </div>
        </div>

        <input id="user-id" type="hidden" value="{{ isset($_G['user']->id)? $_G['user']->id : '' }}">
        @if( \Auth::check())
            <input id="isContactInfoExist" type="hidden" value="{{ $_G['user']->isContactInfoExist() == true? 'true' : 'false' }}">
            <a id="editContactInfo"
                href="{{ route('frontend.modal.setcontactinfo', ['user_id' => $_G['user']->id ] ) }}"
                data-toggle="modal"
                data-target="#modalSetContactInfo"
                style="display:none">
            </a>
        @endif
    </div>


    <script type="text/javascript" src="{{$_G['xenren_path']}}js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="{{$_G['xenren_path']}}js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/ajaxform.min.js" ></script>
    <script type="text/javascript" src="/js/app.js" ></script>
    <script type="text/javascript" src="/js/global.js" ></script>
    <script type="text/javascript" src="/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" ></script>
    <script type="text/javascript" src="/assets/global/plugins/magicsuggest/magicsuggest-min.js"></script>
    <script type="text/javascript" src="/assets/global/plugins/jquery.blockui.min.js" ></script>
    <!-- <script src="/custom/js/frontend/home.js" type="text/javascript"></script> -->

    <script type="text/javascript">
        var userId = $('#user-id').val();
        function checkFirstTimeFilling(userId)
        {
//            var userStatus = $('#user-status').val();
//            var username = $('#username').val();
//            var isFirstTimeSkillSubmit = $('#isFirstTimeSkillSubmit').val();
            var isContactInfoExist = $('#isContactInfoExist').val();

//            if( userId > 0 && userStatus == '0' || userId > 0 && userStatus == '' )
//            {
//                $('#modalChangeUserStatus').modal('show');
//            }
//            else if( userId > 0 && username == '' )
//            {
//                $('#editUsername').trigger('click');
//            }
//            else if( userId > 0 && isFirstTimeSkillSubmit == 0 )
//            {
//                $('#modalChangeUserSkill').modal('show');
//            }
//            else
            if( userId > 0 && isContactInfoExist == 'false' )
            {
                $('#editContactInfo').trigger('click');
            }
        }

        $(document).ready(function () {
            
            var is_guest = {{ Auth::guard('users')->guest() == 1? 'true': 'false' }};

            $('#myCarousel').carousel({
                interval: 10000
            })
            $('.fdi-Carousel .item').each(function () {
                var next = $(this).next();
                if (!next.length) {
                    next = $(this).siblings(':first');
                }
                next.children(':first-child').clone().appendTo($(this));

                if (next.next().length > 0) {
                    next.next().children(':first-child').clone().appendTo($(this));
                }
                else {
                    $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
                }
            });

            $('.mobile-search-type-text').click(function(){
            $('#search-type').hide();
            $('.mobile-search-type-text').each(function(index){
                $( this ).removeClass('mobile-search-type-btn-color').addClass('mobile-search-type-btn-default');
            });

            $(this).addClass('mobile-search-type-btn-color');
            $searchType = $(this).data('search-type');
            $('.btn-search').attr('data-search-type', $searchType);

            if( $(this).hasClass('find-freelancer-btn') ){
                $(".search-box-home").attr("placeholder", "{{trans('common.freelancer_require_what_skill')}}");
            }
            else if( $(this).hasClass('find-project-btn') ){
                $(".search-box-home").attr("placeholder", "{{trans('common.what_skill_you_have')}}");
            }
            else if( $(this).hasClass('find-co-founder-btn') ){
                $(".search-box-home").attr("placeholder", "{{trans('common.co_founder_require_what_skill')}}");
            }
            else{
                $(".search-box-home").attr("placeholder", "{{trans('common.search')}}");
            }

        });

            var lang = {
            error: '{{trans('common.unknown_error')}}',
            please_choose_search_type : "{{ trans('common.please_choose_search_type') }}",
            please_select_skills  : "{{ trans('common.please_select_skills') }}",
            help_us_improve_skill_keyword : "{{ trans('common.help_us_improve_skill_keyword') }}",
            unable_to_request : "{{ trans('common.unable_to_request') }}",
            please_select_freelancer_project_cofounder : "{{ trans('common.please_select_freelancer_project_cofounder') }}",
            freelancer_require_what_skill : "{{ trans('common.freelancer_require_what_skill') }}",
            what_skill_you_have : "{{ trans('common.what_skill_you_have') }}",
            co_founder_require_what_skill : "{{ trans('common.co_founder_require_what_skill') }}"
        }
            // $('.btn-search').click(function(){
            //     var skillIdList = $(this).attr('data-skill-id-list');
            //     if( skillIdList == null )
            //     {
            //         alertError(lang.please_select_skills);
            //         return true;
            //     }

            //     var searchType = $(this).data('search-type');
            //     if( searchType == null )
            //     {
            //         $('#search-type').show();
            //         $('#search-type').text(lang.please_select_freelancer_project_cofounder);
                    
            //         // alertError(lang.please_choose_search_type);
            //         return true;
            //     }
            //     else
            //     {
            //         $('#search-type').text("");
            //         $('#search-type').hide();
            //     }

            //     if( searchType == 'find-project' )
            //     {
            //         window.location.href = url.jobs + '?project_type=1&order=1&type=1&skill_id_list=' + skillIdList;
            //     }
            //     else if( searchType == 'find-freelancer' )
            //     {
            //         window.location.href = url.findExperts + '?skill_id_list=' + skillIdList;
            //     }
            //     else if( searchType == 'find-co-founder' )
            //     {
            //         window.location.href = url.findExperts + '?skill_id_list=' + skillIdList + '&status_id=1';
            //     }
            // });

            if( is_guest == false )
            {
                var noSuggestionText = "<a href='javascript:;' data-toggle='modal' data-target='#modelAddNewSkillKeyword'>" + lang.help_us_improve_skill_keyword + "</a>";
            }
            else
            {
                var noSuggestionText = "<a href='" + url.login + "' >" + lang.help_us_improve_skill_keyword + "</a>";
            }
            var g_lang = {
                search: '{{trans('common.search')}}'
            };
            var skillList = {!! $skillList !!};

            var ms = $('#msSearchBar').magicSuggest({
                allowFreeEntries: false,
                allowDuplicates: false,
                hideTrigger: true,
                placeholder: g_lang.search,
                data: skillList,
                method: 'get',
                highlight: true,
                noSuggestionText: noSuggestionText,
                renderer: function(data){
                    if( data.description == null )
                    {
                        return '<b>' + data.name + '</b>';
                        // return '<b>' + data.name + '</b>' + '<i class="fa fa-plus pull-right add-more"></i>';
                    }
                    else
                    {
                        var name = data.name.split("|");

                        return '<b>' + name[0] + '</b> | <span class="search-desc">' + data.description + '<i class="fa fa-plus search-plus "></i></span>';
                        // return '<b>' + name[0] + '</b> | <span class="search-desc">' + data.description + '</span>' + '<i class="fa fa-plus pull-right add-more"></i>';
                    }

                },
                selectionRenderer: function(data){
                    result = data.name;
                    name = result.split("|")[0];
                    return name;
                }
            });

            $(ms).on(
            'selectionchange', function(e, cb, s){
                $('.btn-search').attr('data-skill-id-list', cb.getValue());
                }
            );

        $(ms).on('keydown', function(e,m,v){
            if(v.keyCode === 13 || v.which  === 13 ){
                var skillIdList = $('.btn-search').data('skill-id-list');
                if( skillIdList == null )
                {
                    return true;
                }

                var searchType = $('.btn-search').data('search-type');
                if( searchType == null )
                {
                    return true;
                }

                $('.btn-search').trigger('click');
            }
        });


            checkFirstTimeFilling(userId);
        });    
    </script>   
</body>
</html>