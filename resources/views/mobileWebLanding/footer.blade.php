<footer class="footer-main">
    <div class="col-md-12" style="background-color: #333;">
        <div class="row" style="padding:5px;">
            <div class="section-top">
                <div class="row">
                    <div class="col-md-8 col-sm-8">
                        <div class="row">
                            <div class="col-md-12 link">
                                <h3>{{ trans('home.network') }}</h3>
                                <ul>
                                    <li><a href="{{ route('home') }}">{{ trans('home.home') }}</a></li>
                                    <li><a href="{{ route('frontend.jobs.index') }}">{{ trans('home.jobs') }}</a></li>
                                    <li><a href="{{ route('frontend.postproject') }}">{{ trans('home.project_post') }}</a></li>
                                    <li><a href="{{ route('register') }}">{{ trans('home.register') }}</a></li>
                                    <li><a href="{{ route('login') }}">{{ trans('home.login') }}</a></li>
                                    <li><a href="javascript:void(0);">{{ trans('home.sitemap') }}</a></li>
                                    <li><a href="{{ route('frontend.findexperts') }}">{{ trans('common.find_experts') }}</a></li>
                                    <li><a href="javascript:void(0);">{{ trans('home.escrow') }}</a></li>
                                    <li><a href="http://f.amap.com/f7qK_0DE2dKb">{{ trans('home.location') }}</a></li>
                                </ul>
                            </div>
                            <div class="col-md-12 link">
                                <h3>{{ trans('home.about_xenren') }}</h3>
                                <ul>
                                    <li>
                                        @if(App::isLocale('cn'))
                                            <a href="{{ route('frontend.chinese-company-culture') }}">{{ trans('home.company_culture') }}</a>
                                        @elseif(App::isLocale('en'))
                                            <a href="{{ route('frontend.company-culture') }}">{{ trans('home.company_culture') }}</a>
                                        @endif
                                    </li>
                                    <li><a href="{{ route('frontend.about-us') }}">{{ trans('home.about_us') }}</a></li>
                                    <li><a href="{{ route('frontend.hiring-page') }}">{{ trans('home.careers') }}</a></li>
                                    <li><a href="{{ route('frontend.terms-service') }}">{{ trans('home.terms_of_service') }}</a></li>
                                    <li><a href="{{ route('frontend.privacy-policy') }}">{{ trans('home.privacy_policy') }}</a></li>
                                    <li><a href="{{ route('frontend.meet-the-team2') }}">{{ trans('home.meet_the_team') }}</a></li>
                                    <li><a href="{{ route('frontend.contact-us') }}">{{ trans('home.contact_us') }}</a></li>
                                </ul>
                            </div>
                            <div class="col-md-12 link">
                                <h3>{{ trans('home.additional_service') }}</h3>
                                <ul>
                                    <li>
                                        <a href="{{ route('frontend.meet-sales-team') }}">{{ trans('home.enterprise_solutions') }}</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('frontend.cooperate-page') }}">{{ trans('home.offline_business_cooperation') }}</a>
                                    </li>
                                </ul>

                                <h3>{{ trans('home.connect_with_us') }}</h3>
                                <ul>
                                    <li><a href="{{ route('faq.faq-buyer') }}">{{ trans('home.contact_support') }}</a>
                                    </li>
                                    <li><a href="javascript:void(0);">{{ trans('home.api_center') }}</a></li>
                                    <li><a href="javascript:void(0);">{{ trans('home.mobile_app') }}</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 social-section">
                        <h3>{{ trans('home.social_media') }}</h3>
                        <div class="social-icon">
                            <a href="javascript:void(0);" class="social-icons"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="javascript:void(0);" class="social-icons"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                            <a href="javascript:void(0);" class="social-icons"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="javascript:void(0);" class="social-icons"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        </div>

                        <div class="barcode-section">
                            <img src="/images/icons/barcode-icon.png">
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-middal">
                <div class="row">
                    <div class="col-md-12 left">
                        <div class="row">
                            <div class="col-md-2 col-sm-2">
                                <img src="/xenren-assets/images/logo/new-main-L.png">
                            </div>
                            <div class="col-md-10 col-sm-10">
                                <a href="javascript:void(0);">
                                    <h1>{{ trans('home.company_name') }}</h1>
                                    <span>{{ trans('home.company_register_id') }}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 right">
                        <h3>{{ trans('home.mobile_apph') }}</h3>
                        <div class="col-md-6 col-sm-12 mobile-app">
                            <a href="javascript:void(0);">
                                <img src="/images/icons/googal-play.png">
                                <div>
                                    <small>{{ trans('home.get_it') }}</small>
                                    <h3>{{ trans('home.google_play') }}</h3>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-12 mobile-app">
                            <a href="javascript:void(0);">
                                <img src="/images/icons/apple-icon.png">
                                <div>
                                    <small>{{ trans('home.download_on_the') }}</small>
                                    <h3>{{ trans('home.app_store') }}</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-last">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footer-list-custom">
                            <div class="icon">
                                <img src="/images/icons/help-center.png" width="40px">
                            </div>
                            <div class="icon-text">
                                <span>{{ trans('home.customer_support') }}:</span>
                                <p>{{ trans('home.customer_support_e') }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="footer-list-custom">
                            <div class="icon">
                                <img src="/images/icons/phone.png" width="40px">
                            </div>
                            <div class="icon-text">
                                <span>{{ trans('home.service_line') }}:</span>
                                <p>{{ trans('home.service_line_m') }}</p>
                            </div>
                        </div>    
                        <div class="footer-list-custom">
                            <div class="icon">
                                <img src="/images/icons/hours.png" width="40px">
                            </div>
                            <div class="icon-text">
                                <span>{{ trans('home.working_hour') }}:</span>
                                <p>{{ trans('home.working_hour_t') }}</p>
                            </div>
                        </div>    
                    </div>
                    <div class="col-md-12">
                        <div class="footer-list-custom">
                            <div class="icon">
                                <img src="/images/icons/location.png" width="40px">
                            </div>
                            <div class="icon-text">
                                <span>{{ trans('home.company_address') }}:</span>
                                <address>
                                    {!! trans('home.company_address_s') !!}
                                </address>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /container -->

</footer>
<div class="footer-bottom">
    <div class="col-md-12" style="background-color: #272727;">
        <!-- <div class="col-md-3 col-sm-2 footer-line"><p class="pull-right"></p></div> -->
        <p class="copyright col-md-12 text-cenetr">{{ trans('home.copyright', ['currentYear' => date('Y')]) }}</p>
        <!-- <div class="col-md-3 col-sm-2 footer-line"><p class="pull-left"></p></div> -->
    </div>
    <!-- /container -->
</div>
<!-- footer-bottom -->

<!-- Back to top -->
<a href="javascript:void(0);" id="back-to-top" title="Back to top"><i class="ti-angle-up"></i></a>
<!-- /Back to top -->

<!-- path to asset -->
<input type="hidden" name="xenren_path" class="xenren_path" value="{{$_G['xenren_path']}}">
<!-- /path to asset -->