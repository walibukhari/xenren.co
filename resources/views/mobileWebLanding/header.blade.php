<div class="navbar" role="navigation">
    <button class="btn navbar-btn" data-toggle="collapse" data-target="#menu">
        <span class="fa fa-bars"></span>
    </button>
</div>

<div id="menu" class="collapse">
  <!-- <div class="container"> -->
    <ul class="nav nav-pills nav-stacked">
        <li class="dropdown active">
            <a href="{{ route('home') }}">{{ trans('common.home')}}</a>
        </li>
        <!-- /dropdown -->

        <li class="dropdown classic-dropdown">
            <a href="{{ route('frontend.jobs.index') }}">{{ trans('common.jobs')}}</a>
            <ul class="dropdown-menu">
                <li class="submenu dropdown">
                    <a href="{{ route('frontend.jobs.index') }}?project_type=2">
                        {{ trans('common.official_project') }}
                    </a>
                </li>
                <li class="submenu dropdown">
                    <a href="{{ route('frontend.skillverification') }}">
                        {{ trans('common.skill_verification') }}
                    </a>
                </li>
                <li class="submenu dropdown">
                    <a href="{{ route('frontend.favouriteJobs') }}">
                        {{ trans('common.favourite_jobs') }}
                    </a>
                </li>
                <li class="submenu dropdown">
                    <a href="#">
                        {{ trans('common.isued_job') }}
                    </a>
                </li>
                <li class="submenu dropdown">
                    <a href="#">
                        {{ trans('common.recieved_job') }}
                    </a>
                </li>
            </ul>
        </li>

        <li class="dropdown classic-dropdown">
            <a href="{{ route('frontend.findexperts') }}">{{ trans('common.find_experts')}}</a>
            <ul class="dropdown-menu">
                <li class="submenu dropdown">
                    <a href="{{ route('frontend.favouriteFreelancers') }}">
                        {{ trans('common.favourite_freelancers') }}
                    </a>
                </li>
            </ul>
        </li>

        <li class="dropdown mega-menu">
            <a href="{{ route('frontend.postproject') }}">{{ trans('common.post_project')}}</a>
        </li>
        <!-- /dropdown -->

        @if (Auth::guard('users')->guest())
            <li class="dropdown mega-menu">
                <a href="{{ route('register') }}">{{ trans('member.register') }}</a>
            </li>
            <!-- /dropdown -->

            <li class="dropdown classic-dropdown">
                <a href="{{ route('login') }}">{{ trans('member.login') }}</a>
            </li>
            <!-- /dropdown -->
        @else
            <li class="dropdown classic-dropdown">
                <a href="{{ route('frontend.usercenter') }}">{{ trans('common.user_center') }}</a>
                <ul class="dropdown-menu">
                    <li class="submenu dropdown">
                        <a id="changeStatusMenu" href="javascript:;" data-toggle="modal"
                           data-target="#modalChangeUserStatus">
                            {{ trans('common.change_status') }}
                        </a>
                    </li>
                    <li class="submenu dropdown">
                        <a href="{{ route('frontend.usercenter') }}">
                            {{ trans('common.personal_information') }}
                        </a>
                    </li>
                    <li class="submenu dropdown">
                        <a href="{{ route('frontend.skillverification') }}">
                            {{ trans('common.skill_verification') }}
                        </a>
                    </li>
                    <li class="submenu dropdown">
                        <a href="{{ route('frontend.myaccount') }}">
                            {{ trans('common.receive_payment_details') }}
                        </a>
                    </li>
                    <li class="submenu dropdown">
                        <a href="{{ route('logout') }}">
                            {{ trans('member.logout') }}
                        </a>
                    </li>
                </ul>
            </li>
        @endif
        
        <li class="hide-tablet hidden">
            <a href="#/" class="ti-search search-trigger"></a>
        </li>
    </ul>
    <!-- /nav -->
</div>