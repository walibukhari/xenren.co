@extends('frontend-xenren.layouts.default-new')

@section('title')
    {{ trans('home.title') }}
@endsection

@section('keywords')
    {{ trans('home.keywords') }}
@endsection

@section('description')
    {{ trans('home.description') }}
@endsection

@section('author')
    Xenren
@endsection

@section('header')
    <link href="/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css">
    <!-- <link href="/custom/css/frontend/home.css" rel="stylesheet" type="text/css" /> -->
    <!-- <link href="/custom/css/frontend/home-new-custom.css" rel="stylesheet" type="text/css" /> -->
    <link href="/custom/css/frontend/modalSetUsername.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/modalSetContactInfo.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/modalAddNewSkillKeywordv2.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/jquery.easy_slides.css" rel="stylesheet" type="text/css" />

    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <style type="text/css">
        #about .heading{
            font-family: 'Montserrat', sans-serif !important;
        }
        #about .about-sub h3{
            font-family: 'Montserrat', sans-serif !important;
            margin-bottom: 5px;
        }
        .about-text{
            font-size: 20px;
            text-align: center;
        }
    </style>
@endsection

@section('content')

	<!-- Content -->
    <!-- Revolution slider -->
    <section id="slider">
    	<div class="slider-logo">
            <img src="/images/N-logo_dark01.png" alt="Logo" width="134px" />
            <h2 class="{{ $language == 1 ? 'big-text' : '' }}">{{ trans('home.legends_lair') }}</h2>
            <h3 class="{{ $language == 1 ? 'big-text' : '' }}">{{ trans('home.hall_of_talent') }}</h3>
        </div>
                    
        <div class="row">
            <div class="col-md-5 search-main">
                <div id="msSearchBar">
                    {{--load to slow, temporary design--}}
                    <div class="ms-ctn form-control  ms-no-trigger ms-ctn-focus" style="" id="msSearchBar">
                        <span class="ms-helper" style="display: none;"></span>
                        
                        <div class="ms-sel-ctn">
                            <input class="search-box-home" placeholder="{{ trans('common.search') }}" style="width: 492px;" type="text">
                            <div style="display: none;"></div>
                        </div>
                    </div>
                </div>
                <i class="fa fa-search btn-search"></i>
                <div class="row">
                    <div class="col-md-12 search-type-div {{Session::get('lang')=='cn'? 'search-type-cn' : 'search-type-en' }}">
                        <button class="search-type-btn-default find-project-btn search-type-text pull-left" data-search-type="find-project">
                            {{ trans('common.find_project') }}
                        </button>
                        <button class="search-type-btn-default find-freelancer-btn search-type-text" data-search-type="find-freelancer">
                            {{ trans('common.find_freelancer') }}
                        </button>
                        <button class="search-type-btn-default find-co-founder-btn search-type-text pull-right" data-search-type="find-co-founder">
                            {{ trans('common.find_co_founder') }}
                        </button>
                    </div>
                </div>

                <span id="search-type" style="display: none;"></span>
            </div>
        </div>
        <input type="hidden" id="skill_id_list" name="skill_id_list" value="{{ isset($filter['skill_id_list'])? $filter['skill_id_list']: '' }}">
    </section>
    <!-- /#Revolution slider -->
    
     <!-- Testimonial -->
    <section id="testimonial" class="section testimonial">
        <div class="container">
        	<div class="row">
        		<div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
        			<h2 class="heading">我们的平台得到了更多用户的支持</h2>
        			<h2 class="heading-line"></h2>
        
		            <div class="testimonial-1 owl-carousel owl-single-all owl-pagi-1 owl-nav-1 pagi-center owl testimonial-1 mrg-top-50">
		                <div class="item">
		                    <div class="row">
				                <div class="col-md-3 col-sm-5 col-xs-12 people-shortstory-img text-center">
				                    <img src="/images/N-team5.png">
				                </div>
				                <div class="col-md-9 col-sm-7 col-xs-12 people-shortstory">
				                    <h3>王女士 <small>（创业者）</small> </h3>
                                    <p>很感谢神人能够给予我这次机会，让我很顺利的找到了我现在的合伙人，并且公司也在一步一步的成长起来，壮大起来。这个平台也不断的在给年轻人机会，也是给创业者迈出第一步的勇气，相信神人的未来是属于这个世界的一笔财富。</p>
				                </div>
		                    </div>
		                </div>
		                <div class="item">
                            <div class="row">
                                <div class="col-md-3 col-sm-5 col-xs-12 people-shortstory-img text-center">
                                    <img src="/images/N-team5.png">
                                </div>
                                <div class="col-md-9 col-sm-7 col-xs-12 people-shortstory">
                                    <h3>王女士 <small>（创业者）</small> </h3>
                                    <p>很感谢神人能够给予我这次机会，让我很顺利的找到了我现在的合伙人，并且公司也在一步一步的成长起来，壮大起来。这个平台也不断的在给年轻人机会，也是给创业者迈出第一步的勇气，相信神人的未来是属于这个世界的一笔财富。</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-md-3 col-sm-5 col-xs-12 people-shortstory-img text-center">
                                    <img src="/images/N-team5.png">
                                </div>
                                <div class="col-md-9 col-sm-7 col-xs-12 people-shortstory">
                                    <h3>王女士 <small>（创业者）</small> </h3>
                                    <p>很感谢神人能够给予我这次机会，让我很顺利的找到了我现在的合伙人，并且公司也在一步一步的成长起来，壮大起来。这个平台也不断的在给年轻人机会，也是给创业者迈出第一步的勇气，相信神人的未来是属于这个世界的一笔财富。</p>
                                </div>
                            </div>
                        </div>
		            </div>	
        		</div>
        	</div>
            <!-- /testimonial -->
        </div>
        <!-- /container -->
    </section>
    <!-- /testimonial -->

    <!-- Team 1 -->
    <section id="team" class="section">
        <div class="container">
            <h2 class="heading">我们的用户量也在不断地扩大</h2>
            <h2 class="heading-line"></h2>
            <div class="row team-main">
            	<div class="col-md-3 team-sub text-center">
            		<div class="team-block">
            			<img src="/images/N-team6.png">
            			<a href="#" class="overlay-team"><i class="fa fa-expand" aria-hidden="true"></i></a>
            		</div>
            		<h3>约翰桥</h3>
            		<small>UI设计师</small>
            	</div>
            	<div class="col-md-3 team-sub text-center">
            		<div class="team-block">
            			<img src="/images/N-team7.png">
            			<a href="#" class="overlay-team"><i class="fa fa-expand" aria-hidden="true"></i></a>
            		</div>
            		<h3>史密斯</h3>
            		<small>CEO/联合创始人</small>
            	</div>
            	<div class="col-md-3 team-sub text-center">
            		<div class="team-block">
            			<img src="/images/N-team8.png">
            			<a href="#" class="overlay-team"><i class="fa fa-expand" aria-hidden="true"></i></a>
            		</div>
            		<h3>安娜</h3>
            		<small>产品经理</small>
            	</div>
            	<div class="col-md-3 team-sub text-center">
            		<div class="team-block">
            			<img src="/images/N-team9.png">
            			<a href="#" class="overlay-team"><i class="fa fa-expand" aria-hidden="true"></i></a>
            		</div>
            		<h3>丹木</h3>
            		<small>UI设计师</small>
            	</div>
            </div>
        </div>
        <!-- /container -->
    </section>
    <!-- /Team 1-->

    <!-- About -->
    <section id="about" class="section">
        <div class="container">
            <h2 class="heading">About us</h2>
            <h2 class="heading-line"></h2>
            <div class="row">
            	<div class="col-md-offset-1 col-md-10">
                    <div class="row about-main">
                        <div class="col-md-12 about-text">
                            <p>神人致力打造高科技人才就业、创业平台。让你的实力成为你的履历。这里专注提供流动性的人力以 ，协助各种企业的长短期人力需求。</p>
                    	</div>

                        <div class="slider slider_circle_10">
                            <div>
                                <div class="img"><img src="/images/N-team14.png"></div>
                                <div class="slider-desc">
                                    <div class="about-sub text-center">
                                        <h3>{{ trans('home.team_fong') }}</h3>
                                        <small>{{ trans('home.team_CEO') }}</small>
                                        <br>
                                        <a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                        <a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                        <a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="img"><img src="/images/N-team12.png"></div>
                                <div class="slider-desc">
                                    <div class="about-sub text-center">
                                        <h3>{{ trans('home.team_Xenren') }}</h3>
                                        <small>{{ trans('home.team_CTO') }}</small>
                                        <br>
                                        <a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                        <a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                        <a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="img"><img src="/images/N-team10.png"></div>
                                <div class="slider-desc">
                                    <div class="about-sub text-center">
                                        <h3>{{ trans('home.team_chenlou') }}</h3>
                                        <small>{{ trans('home.team_CPO') }}</small>
                                        <br>
                                        <a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                        <a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                        <a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="img"><img src="/images/N-team11.png"></div>
                                <div class="slider-desc">
                                    <div class="about-sub text-center">
                                        <h3>{{ trans('home.team_chenkun') }}</h3>
                                        <small>{{ trans('home.team_CCO') }}</small>
                                        <br>
                                        <a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                        <a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                        <a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="img"><img src="/images/N-team13.png"></div>
                                <div class="slider-desc">
                                    <div class="about-sub text-center">
                                        <h3>{{ trans('home.team_chenkun') }}</h3>
                                        <small>{{ trans('home.team_CCO') }}</small>
                                        <br>
                                        <a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                        <a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                        <a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="next_button"><i class="fa fa-angle-right"></i></div>  
                            <div class="prev_button"><i class="fa fa-angle-left"></i></div>  
                            <div class="nav_indicators"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /container -->
    </section>
    <!-- /About-->

    <!-- About -->
    <section id="services" class="section">
        <div class="container">
            <h2 class="heading">{{ trans('home.services_security') }}</h2>
            <h2 class="heading-line"></h2>
            <div class="row services-main">
            	<div class="col-md-6">
            		<div class="row home-services-main">
	                    <div class="col-md-3 home-services-main-l">
	                        <a href="{{ URL::route('frontend.about-us') }}">
	                        	<div class="home-services-main-l-icon">
	                            	<img src="/images/N-icon1.png" data-src="money" alt="money guarantee" />
	                        	</div>
	                        </a>    
	                    </div>
	                    <div class="col-md-9 home-services-main-r">
	                        <a href="{{ URL::route('frontend.about-us') }}">
	                            {{ trans('home.money_guarantee') }}
	                        </a>    
	                        <p>
	                            {{ trans('home.money_guarantee_text') }}    
	                        </p>
	                    </div>
	                </div>
            	</div>
            	<div class="col-md-6">
            		<div class="row home-services-main">
	                    <div class="col-md-3 home-services-main-l">
	                        <a href="{{ URL::route('frontend.about-us') }}">
	                        	<div class="home-services-main-l-icon">
	                            	<img src="/images/N-icon4.png" data-src="money" alt="money guarantee" />
	                        	</div>
	                        </a>    
	                    </div>
	                    <div class="col-md-9 home-services-main-r">
	                        <a href="{{ URL::route('frontend.about-us') }}">
	                            {{ trans('home.skill_verify') }}
	                        </a>    
	                        <p>
	                            {{ trans('home.skill_verify_text') }}
	                        </p>
	                    </div>
	                </div>
            	</div>
            </div>
            <div class="row services-main">	
            	<div class="col-md-6">
            		<div class="row home-services-main">
	                    <div class="col-md-3 home-services-main-l">
	                        <a href="{{ URL::route('frontend.about-us') }}">
	                        	<div class="home-services-main-l-icon">
	                            	<img src="/images/N-icon3.png" data-src="money" alt="money guarantee" />
	                        	</div>
	                        </a>    
	                    </div>
	                    <div class="col-md-9 home-services-main-r">
	                        <a href="{{ URL::route('frontend.about-us') }}">
	                            {{ trans('home.tech_support') }}
	                        </a>    
	                        <p>
	                            {{ trans('home.tech_support_text') }}
	                        </p>
	                    </div>
	                </div>
            	</div>
            	<div class="col-md-6">
            		<div class="row home-services-main">
	                    <div class="col-md-3 home-services-main-l">
	                        <a href="{{ URL::route('frontend.about-us') }}">
	                        	<div class="home-services-main-l-icon">
	                            	<img src="/images/N-icon2.png" data-src="money" alt="money guarantee" />
	                        	</div>
	                        </a>    
	                    </div>
	                    <div class="col-md-9 home-services-main-r">
	                        <a href="{{ URL::route('frontend.about-us') }}">
	                            {{ trans('home.customer_service') }}
	                        </a>    
	                        <p>
	                            {{ trans('home.customer_service_text') }}
	                        </p>
	                    </div>
	                </div>
            	</div>
            </div>
            <div class="row services-main">	
            	<div class="col-md-6">
            		<div class="row home-services-main">
	                    <div class="col-md-3 home-services-main-l">
	                        <a href="{{ URL::route('frontend.about-us') }}">
	                        	<div class="home-services-main-l-icon">
	                            	<img src="/images/N-icon5.png" data-src="money" alt="money guarantee" />
	                        	</div>
	                        </a>    
	                    </div>
	                    <div class="col-md-9 home-services-main-r">
	                        <a href="{{ URL::route('frontend.about-us') }}">
	                            {{ trans('home.skillfull_person') }}
	                        </a>    
	                        <p>
	                            {{ trans('home.skillfull_person_text') }}
	                        </p>
	                    </div>
	                </div>
            	</div>
            	<div class="col-md-6">
            		<div class="row home-services-main">
	                    <div class="col-md-3 home-services-main-l">
                        	<div class="home-services-main-l-icon order">
                                <a href="{{ URL::route('login') }}" class="centering pagination-centered">{!! trans('home.order_now') !!} <br> <i class="fa fa-arrow-right"></i></a>
                        	</div>
	                    </div>
	                    <div class="col-md-9 home-services-main-r">
	                        <p>
	                            {!! trans('home.team_desc') !!}
	                        </p>
	                    </div>
	                </div>
            	</div>
            	
            </div>
        </div>
        <!-- /container -->
    </section>
    <!-- /About-->

	<input id="user-id" type="hidden" value="{{ isset($_G['user']->id)? $_G['user']->id : '' }}">
	<input id="user-status" type="hidden" value="{{ isset($_G['user']) && $_G['user']->getStatusIds() != "" ? $_G['user']->getStatusIds() : '' }}">

	@include('frontend.userCenter.modalChangeUserStatus')
	@include('frontend.includes.modalAddNewSkillKeywordv2', [
	    'tagColor' => \App\Models\Skill::TAG_COLOR_WHITE
	])

	@if( \Auth::check())
	<input id="username" type="hidden" value="{{ isset($_G['user']->name)? $_G['user']->name : '' }}">
	<a id="editUsername"
	    href="{{ route('frontend.modal.setusername', ['user_id' => $_G['user']->id ] ) }}"
	    data-toggle="modal"
	    data-target="#modalSetUsername"
	    style="display:none">
	</a>

	<input id="isContactInfoExist" type="hidden" value="{{ $_G['user']->isContactInfoExist() == true? 'true' : 'false' }}">
	<a id="editContactInfo"
	    href="{{ route('frontend.modal.setcontactinfo', ['user_id' => $_G['user']->id ] ) }}"
	    data-toggle="modal"
	    data-target="#modalSetContactInfo"
	    style="display:none">
	</a>

	<input id="isFirstTimeSkillSubmit" type="hidden" value="{{ isset($_G['user']->is_first_time_skill_submit)? $_G['user']->is_first_time_skill_submit : '0' }}">
	@include('frontend.userCenter.modalChangeUserSkill', [ 'isSkipButtonAllow' => true ])
	@endif

@endsection

@section('modal')
    <div id="modalSetUsername" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>

    <div id="modalSetContactInfo" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript">
        var lang = {
            error: '{{trans('common.unknown_error')}}',
            please_choose_search_type : "{{ trans('common.please_choose_search_type') }}",
            please_select_skills  : "{{ trans('common.please_select_skills') }}",
            help_us_improve_skill_keyword : "{{ trans('common.help_us_improve_skill_keyword') }}",
            unable_to_request : "{{ trans('common.unable_to_request') }}",
            please_select_freelancer_project_cofounder : "{{ trans('common.please_select_freelancer_project_cofounder') }}",
            freelancer_require_what_skill : "{{ trans('common.freelancer_require_what_skill') }}",
            what_skill_you_have : "{{ trans('common.what_skill_you_have') }}",
            co_founder_require_what_skill : "{{ trans('common.co_founder_require_what_skill') }}",
            new_skill_keywords : "{{ trans('common.new_skill_keywords') }}",
            please_use_wiki_or_baidu_support_your_keyword : "{{ trans('common.please_use_wiki_or_baidu_support_your_keyword') }}",
            one_of_skill_input_empty : "{{ trans('common.one_of_skill_input_empty') }}",
            maximum_ten_skill_keywords_allow_to_submit : "{{ trans('common.maximum_ten_skill_keywords_allow_to_submit') }}",
        }

        var url = {
            jobs : '{{ route('frontend.jobs.index') }}',
            findExperts : '{{ route('frontend.findexperts') }}',
            login : '{{ route('login') }}',
            api_skipAddUserSkill : '{{ route('api.skipadduserskill') }}',
            personalInformationChange : '{{ route('frontend.personalinformation.change') }}',
            weixinweb: '{{ route('weixin.login') }}'
        }

        var skillList = {!! $skillList !!};
        var skill_id_list = '{{ isset($_G['user'])? $_G['user']->getUserSkillList(): '' }}';

        var is_guest = {{ Auth::guard('users')->guest() == 1? 'true': 'false' }};

        function checkFirstTimeFilling(userId)
        {
            var userStatus = $('#user-status').val();
            var username = $('#username').val();
            var isFirstTimeSkillSubmit = $('#isFirstTimeSkillSubmit').val();
            var isContactInfoExist = $('#isContactInfoExist').val();

            if( userId > 0 && userStatus == '0' || userId > 0 && userStatus == '' )
            {
                $('#modalChangeUserStatus').modal('show');
            }
            else if( userId > 0 && username == '' )
            {
                $('#editUsername').trigger('click');
            }
            else if( userId > 0 && isFirstTimeSkillSubmit == 0 )
            {
                $('#modalChangeUserSkill').modal('show');
            }
            else if( userId > 0 && isContactInfoExist == 'false' )
            {
                $('#editContactInfo').trigger('click');
            }
        }

        window.onload = function()
        {
            if(isWeiXin() && is_guest == true)
            {
//                alert("weixin browser");
                window.location.href = url.weixinweb;
            }
//            else
//            {
//                alert("normal browser");
//            }
        }

        function isWeiXin()
        {
            var ua = window.navigator.userAgent.toLowerCase();
            if(ua.match(/MicroMessenger/i) == 'micromessenger')
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    </script>

    <script src="/custom/js/frontend/home.js" type="text/javascript"></script>
    <script src="/custom/js/frontend/modalChangeUserStatus.js" type="text/javascript"></script>
    {{--<script src="/custom/js/frontend/modalAddNewSkillKeyword.js" type="text/javascript"></script>--}}
    <script src="/custom/js/frontend/modalAddUserSkill.js" type="text/javascript" ></script>
    <script src="/custom/js/frontend/modalAddNewSkillKeywordv2.js" type="text/javascript" ></script>
    <script src="/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    
    <script src="/custom/js/jquery.easy_slides.js" type="text/javascript" ></script>
    <script>
    $(document).ready(function() {
        $('.slider_circle_10').EasySlides({'autoplay': true, 'show': 5, 'vertical': true})

    });
    </script>
@endsection