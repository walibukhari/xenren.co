<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/codeConfirmation.css" type="text/css" rel="stylesheet">
</head>
<body style="background-color: #fff;">
    @if( trans('common.lang') == 'en' )
        <div class="frame {{ trans('common.lang') }}" style="background-color: #f7f7f7;  margin: 5px;  font-family: 'Montserrat', sans-serif !important;">
    @else
        <div class="frame {{ trans('common.lang') }}" style="background-color: #f7f7f7;  margin: 5px;  font-family: 'Microsoft YaHei' !important;">
    @endif

            <div class="img-sec" style="padding: 20px 0px;  text-align: center;  margin-bottom: 50px;">
                {{--<img src="{{ asset('images/fav.png') }}" />--}}
                <img src="https://www.xenren.co/images/HomePageLogo.png" />
            </div>
            <div class="content" style="background-color: #fff;  border-radius: 15px;  padding: 60px;">
                <div class="title" style="text-align: center;  font-size: 26px;  color: #57b029;  font-weight: bold;  padding-bottom: 100px;">
                    {{ trans('common.confirm_your_email') }}
                </div>
                {{ trans('common.hello') }}

                <p>{!! trans('common.thank_message') !!}</p>
                @if ($link != null && $link != '')
                <p>
                    {{ trans('common.accidentally_close_the_confirmation_code_feature') }}<br/>
                    {{--{{ $link }}--}}
                    <br>
                    <a href="{{ $link }}" target="_blank" style=" color:#fff;
            background-color: #5ca427 !important;
            width: 150px !important;
            border-radius: 5px;
            position: absolute;
            padding:10px;
            margin-top:10px;
            text-align: center;" class="setLink">
                            Activate Account
                    </a>
                </p>
                @endif

                <br/>

                <p>{!! trans('common.when_register_input_code') !!}</p>
                <br/><br/>

                <div class="code" style="text-align: center;  font-size: 70px;">
                    {{ $code }}
                </div>

                <p>
                    {{ trans('common.email') }} :
                    <span class="email" style="color: #57b029;">
                        {{ $email }}
                    </span>
                </p>

                {{ trans('common.regards') }},
                <br/>
                {{ trans('common.xenren_team') }}
            </div>
            <div class="footer" style="text-align: center;  padding: 10px;  color: #a4a4a4;  font-size: 14px;  font-weight: bold;">
                {{ trans('common.unsubscribe_from_xenren') }}<br/>

                <hr style="width: 50%;  color: #dcdfe0;  margin-bottom: 10px;  border-left: 0px;  border-top: 0px;  border-bottom: solid 1px;">

                <div class="company-name" style="color: #57b029;">
                    {{ trans('home.company_name')}} &copy; 2015 - <?php echo date('Y'); ?>
                </div>
                <br/>
                    <span class="company-register" style="padding: 0px 5px;">
                    {{ trans('common.company_register_no') }} : {{ trans('home.register_id') }}
                </span> |
                <span class="customer-hotlink" style="padding: 0px 5px;">
                    {{ trans('home.customer_hotline')}} : 0755-23320228
                </span>
            </div>
        </div>
        </div>
</body>
</html>


