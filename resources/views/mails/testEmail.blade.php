{{ trans('common.user_accept_contact_info_request', ['nickname' => $user->getName() ]) }}
<br/><br/>

{{ trans('common.contact_info') }} :
<p>{{ trans('common.handphone') }} : {{ $user->handphone_no }}</p>
<p>{{ trans('common.skype_id') }} : {{ $user->skype_id }}</p>
<p>{{ trans('common.wechat_id') }} : {{ $user->wechat_id }}</p>
<p>{{ trans('common.qq_id') }} : {{ $user->qq_id }}</p>
