<p>{{ trans('common.you_have_been_invite') }}</p>
<br/>

<p>{{ trans('common.project_name') }} : {{ $project->name }}</p>
<p>{{ trans('common.sender') }}: {{ $sender->getName() }}</p>
<br/>

<p>{{ trans('common.message') }}: </p>
<p>{{ $senderMessage }}</p>
<br/>

<a href="{{ $url }}">{{ trans('common.view_info') }}</a>