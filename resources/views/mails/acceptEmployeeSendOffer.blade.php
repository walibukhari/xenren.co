{{--{{ trans('common.user_accept_send_offer', ['nickname' => $user->getName() ]) }}--}}

<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/acceptSendOffer.css" type="text/css">
</head>
<body style="background-color: #f7f7f7;font-family: 'Raleway',Sans-Serif;">
    <div  class="row" align="center" style="margin-top:10%;">
        <img align="center" class="setImage" style="width:100px;" src="https://xenren.co/images/MainlogoSccuess.png">
    </div>
    <div class="container" style="margin:auto;padding:50px;margin-top:6%;width:600px;height:504px;background-color: #fff;border-radius:5px;">

        <h3 class="setH3" align="center" style="color:#57b029;font-size: 26px; font-family: 'Raleway',Sans-Serif;">Your Hire New Freelancer</h3>
        <br>
        <br>
        <div class="setInnerContainer">
            <label style="font-size: 14px;font-family: 'Raleway',Sans-Serif;color:#44403e;">Hello</label>
            <p style="font-family: 'Raleway',Sans-Serif;font-size: 14px;color:#44403e;">You Accepted {{$user->getName()}} As A Freelancer</p>

            <p style="font-family: 'Raleway',Sans-Serif;font-size: 14px;color:#44403e;">To continue checking your contract, you must click on the confirmation link :</p>
                    <a href="" style="text-decoration:none;color:#57b029;">
                        http://xenren.co/userprofile/
                        verification/token~2V3sferuj4A989
                        7cef445436BfgdgdX00geJr0U</a>
            <p style="font-family: 'Raleway',Sans-Serif;font-size: 14px;color:#44403e;">or click on below button </p>
            <br>
            <br>
            <div align="center">
                <a href="" class="setbtncheck" style="text-decoration:none; border-radius:5px;position: relative;
    top: 20px;font-weight:bold;padding: 15px 100px; width:240px;height:45px;color:#fff;background-color: #57b029;">Check Contract</a>
            </div>

            <p style="margin-top:15%; font-size: 14px; color:#44403e;">If after clicking the link seems brocken than please copy it and paste manually to your browser.</p>
            <p style="font-size: 14px; color:#44403e;">
                Regards,
            </p>
            <p style="font-size: 14px; color:#44403e;">
                Xenren Team
            </p>
        </div>
        <br>
        <br>
        <br><br><br><br>
        <div style="font-weight: bolder;">
            <div align="center">
                <small style=" color:#a4a4a4;
            margin-top: 10px;
            font-weight: bolder;
            font-size:14px;
            font-family: Montserrat, sans-serif;opacity: .5;">Unsubscribe from Xenren</small>
                <hr style="opacity: .8;
            width:50%;
            border: 0px;
            border-top:1px solid #C0C0C0;">
                <small style="color: #5ec329;
            font-size:14px;
            font-family: Montserrat, sans-serif;">Legends Lair Limited &copy; 2004-2017</small>
                <br>
                <small style="color:#a4a4a4; font-size:14px;
            font-family: Montserrat, sans-serif; opacity: .5">Company Register N0:2099572</small> <i style="color:#a4a4a4;">|</i> <small style="font-size:14px;
            font-family: Montserrat, sans-serif; color:#a4a4a4;opacity: .5" > Customer Hotline:075523320228</small>
            </div>
        </div>
    </div>
</body>
</html>
