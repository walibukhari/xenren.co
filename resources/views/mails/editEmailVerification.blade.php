
<html>
    <head>
        <style>
            body {
                background-color: #fff;
            }

            .frame.en{
                padding: 50px 200px;
                background-color: #f7f7f7;
                margin: 5px;
                font-family: 'Montserrat', sans-serif !important;
            }

            .frame.cn{
                padding: 50px 200px;
                background-color: #f7f7f7;
                margin: 5px;
                font-family: "Microsoft YaHei" !important;
            }

            .content{
                background-color: #fff;
                border-radius: 15px;
                padding: 60px;
            }

            .title{
                 text-align: center;
                 font-size: 26px;
                 color: #57b029;
                 font-weight: bold;
                 padding-bottom: 100px;
            }

            .img-sec{
                padding: 20px 0px;
                text-align: center;
                margin-bottom: 50px;
            }

            .footer{
                text-align: center;
                padding: 10px;
                color: #a4a4a4;
                font-size: 14px;
                font-weight: bold;
            }

            .footer hr{
                width: 50%;
                color: #dcdfe0;
                margin-bottom: 10px;
                border-left: 0px;
                border-top: 0px;
                border-bottom: solid 1px;
            }

            .company-name, .email, .link{
                color: #57b029;
            }

            a{
                text-decoration: none;
            }

            .code{
                text-align: center;
                font-size: 70px;
            }

            .company-register,
            .customer-hotlink{
                padding: 0px 5px;
            }
        </style>
    </head>
    <body >
        <div class="frame {{ trans('common.lang') }}">
            <div class="img-sec">
                <img src="http://www.xenren.co/images/fav.png" />
            </div>
            <div class="content">
                <div class="title">
                    {{ trans('common.edit_email') }}
                </div>
                {{ trans('common.hello') }}
                <br/><br/>
                <p>{!!  trans('common.link_to_edit_email')  !!}</p>

                <p>
                    <a href="{{ $edit_email_link }}" target="_blank">
                        <span class="link">
                        {{ $edit_email_link }}
                        </span>
                    </a>
                </p>
                <br/>

                <br/><br/>

                {{ trans('common.regards') }},<br/>
                {{ trans('common.xenren_team') }}
            </div>
            <div class="footer">
                {{ trans('common.unsubscribe_from_xenren') }}<br/>
                <hr>
                <div class="company-name">{{ trans('home.company_name')}} &copy; 2015 - <?php echo date('Y'); ?></div>
                <br/>
                <span class="company-register"> {{ trans('common.company_register_no') }} : {{ trans('home.register_id') }}</span> | <span class="customer-hotlink">{{ trans('home.customer_hotline')}} : 0755-23320228</span>
            </div>
        </div>
    </body>
</html>
