<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Coworker Owner Login</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700&display=swap" rel="stylesheet">
        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="{{asset('css/loginpage.css')}}">
    </head>

    <body>
    <div id="wrapper" class="full-height">
        <div class="left-content full-height">
            <div class="title">
                Share your space to global
            </div>
            <p style="font-size: 12pt;width: 68%">
                Mandate is to use technology to make the world a better place. We utilize technology to simplify online recruitment processes.
            </p>
        </div>
        <div class="right-content full-height">
            <img style="width: 35%; height: auto; margin-top: 13vh" src="{{asset('images/xenren-cowoker-logo.png')}}" alt="imgLogo">
            <p style="color: #6bc30f; font-size: 9pt; margin-top:0; margin-bottom: 5vh">
                Coworker & Coworking space
            </p>
            @if (Route::has('login'))
                <div class="login links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group row">
                                <label class="login-label">{{ ('EMAIL') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <br>

                            <div class="form-group row">
                                <label class="login-label">{{ ('PASSWORD') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row" style="height: 25px; margin-top: 15px; margin-bottom: 10px">

                                        <div style="float: left; display: inline-block">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember" style="font-size: 8pt; display: inline-block">
                                            {{ __('REMEMBER ME') }}
                                        </label>
                                        </div>


                                        @if (Route::has('password.request'))
                                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                                {{ __('FORGET PASSWORD?') }}
                                            </a>
                                        @endif


                            </div>

                            <div class="form-group row mb-0" style="width: 68%; margin-left: auto; margin-right: auto">
                                <div class="col-md-8 offset-md-4" style="padding-left: 3.4%">
                                    <button type="submit" class="btn btn-submit">
                                        {{ __('Sign in') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <p style="font-size: 9pt; margin-top: 10px; margin-bottom: 1px">
                        Don’t have an account?
                        </p>
                        @if (Route::has('register'))
                        <a href="{{ route('register') }}" style="color: #539b0d">Sign up</a>
                        @endif
                    @endauth
                </div>
            @endif
        </div>
    </div>
    </body>
</html>
