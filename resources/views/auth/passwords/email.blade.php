@extends('frontend.layouts.default')

@section('title')
    {!! trans('common.email_title') !!}
@endsection

@section('keywords')
    {!! trans('common.email_keyword') !!}
@endsection

@section('description')
    {!! trans('common.email_desc') !!}
@endsection

@section('author')
vika
@endsection

@section('header')

    <style type="text/css">
        .pass-text p{
            color:#2f2d2d;
            font-weight:500;
            font-size:17px;
            margin-bottom:30px;
        }
    </style>
    <style>
        .lds-ring {
            display: inline-block;
            position: relative;
            width: 20px;
            height: 20px;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 20px;
            height: 20px;
            margin: 1px;
            border: 2px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
@endsection

<!-- Main Content -->
@section('content')
    <div class="container" style="height: 400px;">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @if(\Session::has('message'))
                            <div class="row">
                                <div class="col-md-12 alert alert-success">
                                    {{\Session::get('message')}}
                                </div>
                            </div>
                        @endif
                        @if(\Session::has('error'))
                            <div class="row">
                                <div class="col-md-12 alert alert-danger">
                                    {{\Session::get('message')}}
                                </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12"><h4 class="caption-subject bold ">{{ trans('member.forget_password') }}</h4></div>
                        </div>
                        <hr>
                        <div class="row text-center">
                            <div class="col-md-12">
                                <img src="{{ asset('images/icons/reset_password_icon.png') }}">
                            </div>
                            <div class="col-md-12 pass-text">
                                <p>
                                    {!! trans('member.reset_password_contant') !!}
                                </p>
                            </div>
                        </div>
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                        </div>
                    @endif

                    {!! Form::open(['route' => 'password.reset.post', 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'forgotPasswordSubmitForm' , 'role' => 'form']) !!}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="email" id="emailForGot" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ trans('member.email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!--
                        Apply at last state of project
                        <div class="form-group">
                            <div class="col-md-3 col-md-offset-3">
                                <input type="text" class="form-control" name="verification_code" placeholder="{{ trans('member.verification_code') }}">
                            </div>
                            <div class="col-md-3">
                                <label style="background-color: #c2cad8; padding:8px 32px;">{{ trans('member.get_verification_code') }} ( 0 / 3 )</label>
                            </div>
                        </div>
                        -->
                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-6 col-md-offset-3">--}}
                                {{--<input type="password" class="form-control" name="password" placeholder="{{ trans('member.new_password') }}">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-6 col-md-offset-3">--}}
                                {{--<input type="password" class="form-control" name="confirm_password" placeholder="{{ trans('member.confirm_new_password') }}">--}}
                            {{--</div>--}}
                        {{--</div>--}}
						<div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <button type="button" id="submitForGot" style="width:180px;display: flex;justify-content: center;align-items: center;" class="col-md-12 btn btn-primary">
                                    <span class="idSpanFogotPassword">
                                        {{ trans('member.reset_password') }}
                                    </span>
                                    <div style="display: none;" class="lds-ring">
                                        <div></div><div></div><div></div><div></div>
                                    </div>
                                </button>
							</div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    <script>
        $('#submitForGot').click(function () {
            $('.lds-ring').show();
            $('.idSpanFogotPassword').hide();
            var email = $('#emailForGot').val();
            if(email == '') {
                $('.idSpanFogotPassword').show();
                $('.lds-ring').hide();
            } else {
                $('.idSpanFogotPassword').hide();
                $('.lds-ring').show();
                $('#forgotPasswordSubmitForm').submit();
            }
        });
    </script>
@endsection
