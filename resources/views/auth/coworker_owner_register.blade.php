@extends('layouts.app')

@section('content')
<div id="wrapper" class="full-height">
    <div class="left-content full-height">
            <div class="title">
                Share your space to global
            </div>
            <p style="font-size: 12pt;width: 68%">
                Mandate is to use technology to make the world a better place. We utilize technology to simplify online recruitment processes.
            </p>
        </div>
    
        <div class="right-content full-height">
            
            <form method="POST" action="{{ route('register') }}" style="margin-top: 18vh">
                        <h class="signupHeading">
                            Welcome to XenRen
                        </h>
                        <p style="font-size: 9pt; padding-right: 17%; margin-top: 0; margin-bottom: 25px">
                            SignUp by enter information below
                        </p>
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="login-label">{{ __('OFFICE NAME') }}</label>

                            <div class="col-md-6">
                                <input list="officeName" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="officeName" autofocus>
                                <datalist id="officeName">
                                    <option value="GOPGOS">
                                    <option value="Emperial Palace">
                                    <option value="Netsole">
                                    <option value="Capital Tower">
                                    <option value="Silicon Valley">
                                </datalist>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="login-label">{{ __('EMAIL') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="login-label">{{ __('PASSWORD') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="login-label">{{ __('RETYPE PASSWORD') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0"style="width: 68%">
                            <div class="col-md-6 offset-md-4" style="margin-top: 15px;padding-left: 3.4%">
                                <button type="submit" class="btn btn-submit">
                                    {{ __('Create Account   ') }}
                                </button>
                            </div>
                        </div>
                    </form>
                        <p style="font-size: 9pt; margin-top: 15px; margin-bottom: 1px">
                        Already have an account?
                        </p>
                        @if (Route::has('login'))
                        <a href="{{ route('login') }}" style="color: #539b0d">Sign in</a>
                        @endif
                </div>
            
</div>
@endsection
