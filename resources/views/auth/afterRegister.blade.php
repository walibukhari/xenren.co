@extends('frontend.layouts.default')

@section('title')
    Register
@endsection

@section('description')
    Register
@endsection

@section('author')
    vika
@endsection

@section('header')
    <link href="../custom/css/auth/register.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <br><br><br><br>
            <div class="col-md-12 text-center">
                <img src="{{asset('images/afterRegister.png')}}">
            </div>
            <br>
            <div class="col-md-12 text-center m-top-12">
                <label class="setColorRegister">Register Successfull</label>
            </div>
            <div class="col-md-12 text-center">
                {{--<label class="setlabelRegister"></label>--}}
            </div>
        </div>
    </div>
@endsection

@section("footer")
    <script>
        var url = {
            'home' : '{{ route('home') }}'
        }
    </script>
    <script>
        +function () {
            $(document).ready(function () {
                setTimeout(function () {
                    window.location.href = url.home;
                }, 3000);
            });
        }(jQuery);
    </script>
@endsection