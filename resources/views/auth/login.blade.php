@extends('frontend.layouts.default')

@section('title')
    {!! trans('common.login_title') !!}
@endsection

@section('keywords')
    {!! trans('common.login_keyword') !!}
@endsection

@section('description')
    {!! trans('common.login_desc') !!}
@endsection

@section('author')
    vika
@endsection

@section('header')
    <link href="/custom/css/auth/login.css" rel="stylesheet" type="text/css" />
    <style>
        @media (max-width: 460px) {
            .fa-search {
            top: 35px !important;
        }
        }
        .lds-ring {
            display: inline-block;
            position: relative;
            width: 40px;
            height: 40px;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 30px;
            height: 30px;
            margin: 5px;
            border: 3px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
@endsection

@section('content')
    <div id="errorMsg" class="alert alert-danger" style="display: none;"></div>
    <div class="container set-login-page-main-container">
        <div class="row setROWColl12">
            <div class="col-md-6 set-col-md-6-login-page">
                <div class="setBTNBTNLOGPPADPE">
                    <a href="{{ url('/login') }}" class="btn btn-success setLPAGEH5">{{ trans('member.login') }}</a>
                    <a class="btn btn-primary setBTNBTNBBLP" href="{{ url('/register') }}">{{ trans('member.register') }}</a>
                </div>
                <div class="panel panel-default setPanelD">
                    <div class="panel-body setLOGINPBODY">
                        <form class="form setFORMBODY" role="form" method="POST" action="{{ route('login.post') }}" id="login-form">
                            {!! csrf_field() !!}

                            <div class="form-group">
                                <div class="col-md-12" id="login-alert-container"></div>
                            </div>
                            <div class="col-md-12 set-col-md-12-login-page">
                                <div class="form-group {{ $errors->has('phone_or_email') ? ' has-error' : '' }} e-form">
                                    <label for="phone_or_email" class="setLABELLPAGE">{{ trans('member.email') }}</label>
                                    <input tabindex="1" type="text" class="form-control setFIELD" id="phone_or_email" placeholder="{{trans('common.adminna')}}" name="phone_or_email" value="{{ old('phone_or_email') }}" required>
                                    <img src="/images/user123.png" class="setNameICON">
                                    <a href="javascript:void(0);" class="reset-input">×</a>
                                    {{-- <span id="emailError" class="help-block"><strong></strong></span> --}}
                                </div>
                            </div>

                            <div class="col-md-12 set-col-md-12-login-page">
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} e-form">
                                    <label class="setLABELLPAGE">{{ trans('common.password') }}</label>
                                    <input tabindex="2" type="password" placeholder="{{trans('common.pussword')}}" class="form-control setFIELD" name="password" id="pass" required>
                                    <img src="/images/lock123.png" class="setGLYPHICON">
                                    <a href="javascript:void(0);" class="reset-input">×</a>
                                    {{-- <span id="passError" class="help-block"><strong></strong></span> --}}
                                </div>
                            </div>

                            <div class="setAREALP">
                                <div class="col-md-6 setColRow6CB">
                                    <label class="checkBoxContainer">
                                        <input tabindex="3" type="checkbox" name="remember" id="remember">
                                        <span class="checkmark"></span>
                                    </label>
                                    <span class="setRememberMeLPAGE">{{ trans('member.remember_me') }}</span></div>
                                <div class="col-md-6 text-right set-padding-forgot"><a tabindex="4" href="{{ route('password.reset') }}"><span class="setFORGPTLP">{{ trans('member.forget_password') }}?</span></a></div>
                            </div>

                            <div class="col-md-12 setCOL12LPBTN set-col-md-12-login-page">
                                <div class="form-group">
                                    <button type="button" tabindex="5" class="btn blue btn-block setBTNLOGIN" id="submit-login">
                                        <span class="idSpanLogin">{{ trans('member.login_now') }}</span>
                                        <div style="display: none;" class="lds-ring">
                                            <div></div><div></div><div></div><div></div>
                                        </div>
                                    </button>
                                </div>
                            </div>

                            <div class="col-md-12 text-center setSUUU">
                                <p>{{trans('common.or_signup_using')}}</p>
                            </div>

                            <div class="row setUORDEREDLIST">
                                <div class="col-md-12">
                                    <a tabindex="6" href="{{ route('facebook.login') }}"><img class="avatar setQQIMAGELPA" src="/images/loginImage/fb0.png"></a>

                                    {{--@if(isset($_G['is_weixin_browser']) && $_G['is_weixin_browser'] === true)--}}
                                    {{--<a href="{{ route('weixin.login') }}"><img class="avatar setQQIMAGELPA" alt="" src="/images/loginImage/we.png" /></a>--}}
                                    {{--<a href="{{ route('weixin.login') }}"><img class="avatar setQQIMAGELPA" alt="" src="/images/loginImage/we.png" /></a>--}}
                                    {{--@else--}}
                                    {{--<a href="{{ route('qq.login') }}"><img src="/images/loginImage/qq0.png" class="avatar setQQIMAGELPA"></a>--}}
                                    {{--@endif--}}
                                    {{--<a href="{{ route('weixinweb.login') }}"><img class="avatar setQQIMAGELPA" alt="" src="/images/loginImage/we.png" /></a>--}}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6 setROWCOL6LP">
                <img src="/images/loginImage/setLoginBackgroundChangeImage.png">
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script>
        $(document).ready(function () {
            $("#submit-login").click(function(e){
                e.preventDefault();
                ajaxProceed()
            });

            function ajaxProceed() {
                var _token = $("input[name='_token']").val()
                var phone_or_email = $('#phone_or_email').val()
                var pass = $('#pass').val()
                var errField = $('.err-msg')
                $('.lds-ring').show();
                $('.idSpanLogin').hide();
                $.ajax({
                    url: "{{ route('login.post') }}",
                    type:'POST',
                    data: {_token:_token, phone_or_email:phone_or_email, pass:pass},
                    success: function(data) {
                        errField.remove()
                        window.location.href='{{ route('home') }}';
                    },
                    error: function(err) {
                        $('.lds-ring').hide();
                        $('.idSpanLogin').show();
                        errField.remove()
                        printErrorMsg(err)
                    }
                });
            }

            function printErrorMsg (err) {
                var fields = $('.setFIELD')
                $.each(err.responseJSON, function (i, error) {
                    if ($.type(error) == 'object') {
                        var phone_or_email = $('#phone_or_email')
                        var pass = $('#pass')

                        $.each(error, function (key, val) {
                            var el = $('#'+key)
                            el.fadeIn().after($('<div><span class="err-msg" style="color: red;">' + val + '</span></div>'))
                            el.removeClass('hasContent')
                            el.addClass('hasContent-error')
                        })

                        if (error.phone_or_email == null) setBtn(true, phone_or_email)
                        else setBtn(false, phone_or_email)

                        if (error.pass == null) setBtn(true, pass)
                        else setBtn(false, pass)

                    } else {
                        $('#errorMsg').fadeIn().text(error)
                        $('.err-msg').remove()
                        $.each(fields, function (key, val) {
                            $(this).removeClass('hasContent')
                            $(this).addClass('hasContent-error')
                        })
                    }
                })
            }

            function setBtn(stat, that) {
                if (stat) {
                    that.addClass('hasContent')
                    that.removeClass('hasContent-error')
                } else {
                    that.addClass('hasContent-error')
                    that.removeClass('hasContent')
                }
            }
        });
    </script>
    <script type="text/javascript">
        $('#pass').bind('keypress',function(e){
            if(e.keyCode === 13) {
                $('#submit-login').click();
            }
        });
    </script>
@endsection
