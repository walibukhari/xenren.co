@extends('frontend.layouts.default')

@section('title')
    Register
@endsection

@section('description')
    Register
@endsection

@section('author')
    vika
@endsection
@section('modal')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="{{asset('css/slideToUnlock.css')}}" rel="stylesheet" />
    <style>
        @media (max-width: 460px)
        {   .fa-search {
            top: 35px !important;
        }
        }
    </style>
    <link href="{{asset('css/green.theme.css')}}" rel="stylesheet" />
    <script src="{{asset('js/select2.min.js')}}"></script>
    <script src="{{asset('js/jquery.slideToUnlock.js')}}"></script>
    <script>
        $(function () {
            window.isLocked = true;
        });
        $("#slideUnlock").slideToUnlock({
            userForm: $('#submit-register'),
            tranText: "{{trans('login.slide_to_register')}}",
            unlockfn: function(){
                window.isLocked = false;
                $('.text').css('color','#fff')
                $('#register-form').submit();
                console.log(window.isLocked)
            },
            lockfn  : function(){
                window.isLocked = true;
                $('.text').css('color','#fff')
                console.log(window.isLocked)
            }
        });
    </script>
    <script>

        var x = document.getElementById("pac-input");

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition,showError);
            } else {
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }

        function showPosition(position) {
            console.log("show position");
            console.log(position);
            console.log("show position");
            lat = position.coords.latitude;
            long = position.coords.longitude;
            displayLocation(lat,long);
        }

        function showError(error) {
            console.log(error);
            switch(error.code) {
                case error.PERMISSION_DENIED:
//                    x.innerHTML = "User denied the request for Geolocation."
                    break;
                case error.POSITION_UNAVAILABLE:
//                    x.innerHTML = "Location information is unavailable."
                    break;
                case error.TIMEOUT:
//                    x.innerHTML = "The request to get user location timed out."
                    break;
                case error.UNKNOWN_ERROR:
//                    x.innerHTML = "An unknown error occurred."
                    break;
            }
        }

        function displayLocation(latitude,longitude){
            var geocoder;
            geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(latitude, longitude);

            geocoder.geocode(
                {'latLng': latlng},
                function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            var add= results[0].formatted_address ;
                            var  value=add.split(",");
                            var names = value[2]+','+value[4];
                            console.log("getnames");
                            var cleanName = names.replace(/^\s+|\s+$/g, '');
                            console.log(cleanName);
                            console.log("getnames");
                            $('.getplacename').val(cleanName);
                        }
                        else  {
                            x.innerHTML = "address not found";
                        }
                    }
                    else {
                        x.innerHTML = "Geocoder failed due to: " + status;
                    }
                }
            );
        }

        setTimeout(function(){
            getLocation();
        });

        $(".js-example-data-array").select2({
            minimumInputLength: 3,
            placeholder: '{{trans('sharedoffice.enterlocation')}}',
            language: {
                inputTooShort: function () {
                    return '{{trans('member.please_enter_3_or_more_characters')}}';
                }
            },
            ajax: {
                url: '/fetchCity',
                dataType: 'json',
                processResults: function (data) {
                    // Tranforms the top-level key of the response object from 'items' to 'results'
                    return data;
                }
            }
        })
    </script>
@endsection
@section('header')
    <?php session_start(); ?>
    <?php require("lib/simple-botdetect.php"); ?>
    <link href="../custom/css/auth/register.css" rel="stylesheet" type="text/css" />
    <link href="{{ captcha_layout_stylesheet_url() }}" type="text/css" rel="stylesheet">
@endsection

@section('content')
    <style>
        #CaptchaCode {
            color: #848484;
            font-weight: 600;
            font-family: Montserrat, sans-serif;
            font-size: 13px;
            border: 1px solid #c2cad8;
            height: 40px;
            width: 245px;
        }
    </style>
    @php
        $options = \App\Models\CountryCities::with('state')->get();
        $query = @unserialize (file_get_contents('http://ip-api.com/php/'));
        if ($query && $query['status'] == 'success') {
            $countrtyName= $query['country'];

        }
    @endphp

    <input type="hidden" value="{{$CountryLocation}}" id="stateName">

    <div class="container">
        <div class="row">
            <div class="col-md-6 set-col-md-6-register-page">
                <div id="err"></div>
                <div class="panel panel-default box-register">
                    <div class="panel-body setPANELBODY">
                        <div class="form-group setFormGroupXRA">
                            <h5> {{ trans('member.register_xenrencc_member') }}</h5>

                            {{--<ul class="chats">--}}
                            {{--<li class="out">--}}
                            {{--<div class="col-md-4"><h5--}}
                            {{--style="padding-top:5px">{{ trans('member.social_login') }}</h5></div>--}}
                            {{--<div class="col-md-2" id="wechatt">--}}
                            {{--<a href="{{ route('qq.login') }}">--}}
                            {{--<img class="avatar" alt="" src="//images/1.jpg"/></a>--}}
                            {{--</div>--}}
                            {{--@if(isset($_G['is_weixin_browser']) && $_G['is_weixin_browser'] === true)--}}
                            {{--<div class="col-md-2"><a href="{{ route('weixin.login') }}">--}}
                            {{--<img class="avatar" alt="" src="//images/2.jpg" />--}}
                            {{--</a>--}}
                            {{--</div>--}}
                            {{--@else--}}
                            {{--<div class="col-md-2">--}}
                            {{--<a href="{{ route('weixinweb.login') }}">--}}
                            {{--<img class="avatar" alt="" src="//images/2.jpg" />--}}
                            {{--</a>--}}
                            {{--</div>--}}
                            {{--@endif--}}
                            {{--<div class="col-md-2"><a href="{{ route('weibo.login') }}"><img class="avatar" alt="" src="//images/3.jpg" /></a></div>--}}
                            {{--<div class="col-md-2"><a href="{{ route('facebook.login') }}">FB login</a></div>--}}

                            {{--</li>--}}
                            {{--</ul>--}}
                            {{--<div class="col-md-2"><a href="{{ route('facebook.login') }}"  id="fb">--}}
                            {{--<i class="fa fa-facebook fa-3x fa-align-left" aria-hidden="true"></i></a>--}}
                            {{--</div>--}}

                        </div>


                        <form class="form" role="form" method="POST" action="{{ url('/register') }}" id="register-form">
                            {!! csrf_field() !!}

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12" id="register-alert-container"></div>
                                </div>
                            </div>
                            <div class="row setRowCol12PBPR">
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} e-form">
                                        <label class="setLabel">{{trans('member.first_name')}}</label>
                                        <input  tabindex="1" type="text" placeholder="{{trans('member.first_name')}}" class="form-control setField" name="name" value="{{ old('name') }}" id="name">
                                        <img src="/images/username.png" class="setNameICON">
                                        <a href="javascript:void(0);" class="reset-input">×</a>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }} e-form">
                                        <label class="setLabel">{{ trans('member.last_name') }}</label>
                                        <input  tabindex="2" required type="email" placeholder="{{ trans('member.last_name') }}" class="form-control setField" name="last_name" value="{{ old('last_name') }}" id="last_name">
                                        <img src="/images/username.png" class="setNameICON">
                                        <a href="javascript:void(0);" class="reset-input">×</a>
                                        @if ($errors->has('last_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('last_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row setRowCol12PBPR">
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} e-form setSelect2" >
                                        <label class="setLabel">{{trans('member.choose_location')}}</label>
                                        {{--<select tabindex="3" name="city" id="js-example-data-array" class="js-example-data-array form-control"></select>--}}
                                        <input type="text"
                                               tabindex="3"
                                               name="city"
                                               id="pac-input"
                                               class="form-control getplacename"
                                               placeholder="{{trans('sharedoffice.enterlocation')}}"
                                               autocomplete="on"
                                        />

                                        <input type="hidden" name="city_name" id="city_name">
                                        <input type="hidden" name="country_name" id="country_name">
                                        <div id="map" style="display: none"></div>


                                        <span class="glyphicon glyphicon-map-marker setGLYPHIL"></span>
                                        <a href="javascript:void(0);" class="reset-input">×</a>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('phone_or_email') ? ' has-error' : '' }} e-form">
                                        <label class="setLabel">{{ trans('member.phone_or_email') }}</label>
                                        <input  tabindex="4" required type="email" placeholder="{{ trans('member.phone_or_email') }}" class="form-control setField" name="phone_or_email" value="{{ old('phone_or_email') }}" id="phone_or_email">
                                        <img src="/images/emailimg.png" class="setEmailICON">
                                        <a href="javascript:void(0);" class="reset-input">×</a>
                                        @if ($errors->has('phone_or_email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone_or_email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} e-form" >
                                        <label for="password setLabel" class="setLabel">{{ trans('member.password') }}</label>
                                        <input  tabindex="5" type="password" class="form-control setField" name="password" id="password" />
                                        <img src="/images/lock1.png" class="setGLYPHICON">
                                        <a href="javascript:void(0);" class="reset-input">×</a>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }} e-form" >
                                        <label for="password_confirmation" class="setLabel">{{ trans('member.confirm_password') }}</label>
                                        <input  tabindex="6" type="password" class="form-control setField" name="password_confirmation" id="password_confirmation" />
                                        <img src="/images/lock1.png" class="setGLYPHICON">
                                        <a href="javascript:void(0);" class="reset-input">×</a>

                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row setROWIARP">
                                <div class="col-md-12 setCHECKERRCol12">
                                    <div class="form-group" {{ $errors->has('remember') ? ' has-error' : '' }}>
                                        <h5>
                                            <div class="form-group set-fonts-rememberMe">
                                                <label class="set-container-checkbox-login-cowork">
                                                    <input tabindex="7" type="checkbox" name="remember" required>
                                                    <span class="setSPN1">{{ trans('member.i_accept') }}</span><a href="{{ route('frontend.terms-service') }}" class="setSPN2">{{ trans('member.terms_and_conditions') }}</a><span class="setSPN3" >{{ trans('member.and') }}</span><a href="{{ route('frontend.privacy-policy') }}" class="setSPN4">{{ trans('member.privacy_policy') }}</a>
                                                    <span class="checkmark-login-coworkspace"></span>
                                                </label>
                                            </div>
                                        </h5></div>
                                    @if ($errors->has('remember'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('remember') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                @if (isset($qqLogin) && $qqLogin === true)
                                    <input type="hidden" name="qq_open_id" value="{{ $oauthUser->id }}" />
                                    <input type="hidden" name="qqlogin" value="1" />
                                @endif

                                @if (isset($weiboLogin) && $weiboLogin === true)
                                    <input type="hidden" name="weibo_open_id" value="{{ $oauthUser->id }}" />
                                    <input type="hidden" name="weibologin" value="1" />
                                @endif

                                @if (isset($weixinLogin) && $weixinLogin === true)
                                    <input type="hidden" name="weixin_open_id" value="{{ $oauthUser->id }}" />
                                    <input type="hidden" name="weixinlogin" value="1" />
                                @endif
                            </div>
                            <div class="row set-register-slide-main-row">
                                <div class="col-md-6 captcha-custom setBDC">
                                    <div tabindex="8" id="slideUnlock"></div>
{{--                                    <div class="form-group">--}}

{{--                                        @php--}}
{{--                                            $error = '';--}}

{{--                                           if (isset($_REQUEST['error'])) {--}}
{{--                                               $error = $_REQUEST['error'];--}}
{{--                                           }--}}
{{--                                           if ('Format' == $error) { ?>--}}
{{--                                               <p class="incorrect">Invalid authentication info</p><?php--}}
{{--                                           } else if ('Auth' == $error) { ?>--}}
{{--                                               <p class="incorrect">Authentication failed</p><?php--}}
{{--                                           }--}}
{{--                                           $LoginCaptcha = new SimpleCaptcha('LoginCaptcha');--}}
{{--                                           $LoginCaptcha->CodeLength = 3;--}}
{{--                                           $LoginCaptcha->CodeStyle = CodeStyle::Alpha;--}}

{{--                                           // only show the Captcha if it hasn't been already solved for the current message--}}
{{--                                           if(!isset($_SESSION['IsCaptchaSolved'])) { ?>--}}
{{--                                              <label for="CaptchaCode">Retype the characters from the picture:</label>--}}
{{--                                           <?php echo $LoginCaptcha->Html(); ?>--}}
{{--                                             <input type="text" tabindex="6" name="CaptchaCode" id="CaptchaCode" class="textbox" /><?php--}}

{{--                                           // CAPTCHA validation failed, show error message--}}
{{--                                           if ('Captcha' == $error) { ?>--}}
{{--                                             <span class="incorrect">Incorrect code</span><?php--}}
{{--                                           }--}}
{{--                                           }--}}
{{--                                        @endphp--}}


{{--                                        --}}{{--{!! captcha_image_html('ContactCaptcha') !!}--}}

{{--                                        --}}{{--<br>--}}
{{--                                        --}}{{--<div class="e-form setEFORM">--}}
{{--                                        --}}{{--<input type="text" id="setREGPAGEEFORMINF1" placeholder="{{ trans('member.three_alphabet') }}" id="CaptchaCode" name="CaptchaCode" class="form-control setField">--}}
{{--                                        --}}{{--<label for="CaptchaCode">{{ trans('member.three_alphabet') }}</label>--}}
{{--                                        --}}{{--<a href="javascript:void(0);" class="reset-input">×</a>--}}
{{--                                        --}}{{--</div>--}}
{{--                                    </div>--}}
                                </div>
                                <span class="set-span-divider-or">OR</span>
                                <div class="col-md-6 set-facebook-login-col-md-6">
                                    <a href="{{ route('facebook.login') }}" tabindex="9">
                                        <button type="button" class="btn btn-block setFBBTNC">Facebook</button>
                                        <i class="fa fa-facebook fa-3x setFBICON" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="hidden" name="inviteCode" value="{{ isset( $inviteCode )? $inviteCode: 0 }}" />
                                        {{--<button type="button" tabindex="8" class="btn btn-block success setBTNRP" id="submit-register" ><strong>{{ trans('member.register_now') }}!</strong></button>--}}
                                        {{--<input type="hidden" id="hidConfirmationCode" name="hidConfirmationCode" value="">--}}
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">--}}
                                {{--<div class="col-md-12 text-center setSLORPCol12">--}}
                                    {{--<i>{{trans('member.or_social_login')}}</i>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-12 setFBCol12">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<a href="{{ route('facebook.login') }}" tabindex="10">--}}
                                            {{--<button type="button" class="btn btn-block setFBBTNC">Facebook</button>--}}
                                            {{--<i class="fa fa-facebook fa-3x setFBICON" aria-hidden="true"></i>--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            <div class="row">
                                <div class="col-md-12 text-left setAHAL">
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        {{ trans('member.have_account') }}
                                        <a href="{{url('/login')}}" tabindex="10" class="setREGPGELOGINM">
                                            {{ trans('member.login_now') }}
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <img src="/images/registerBackground.png" class="setBGIMAGE">
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <div id="modalEmailVerification" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <style>
        #map{
            width: 100%;
            height: 500px;
        }
        #description {
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
        }
        #infowindow-content .title {
            font-weight: bold;
        }
        #infowindow-content {
            display: none;
        }
        #map #infowindow-content {
            display: inline;
        }
        .pac-card {
            margin: 10px 10px 0 0;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            background-color: #fff;
            font-family: Roboto;
        }
        #pac-container {
            padding-bottom: 12px;
            margin-right: 12px;
        }
        .pac-controls {
            display: inline-block;
            padding: 5px 11px;
        }
        .pac-controls label {
            font-family: Roboto;
            font-size: 13px;
            font-weight: 300;
        }
        #pac-input {
            background-color: #fff;
            font-size: 15px;
            font-weight: 300;
            height:40px;
            border-radius: 0px !important;
            /*margin-left: 12px;*/
            text-overflow: ellipsis;
            padding: 6px 31px 4px !important;
        }
        #pac-input:focus {
            border-color: #4d90fe;
        }
        .pac-item:hover {
            background-color: #57b029 !important;
            color:#fff !important;
        }
        #title {
            color: #fff;
            background-color: #4d90fe;
            font-size: 25px;
            font-weight: 500;
            padding: 6px 12px;
        }
        #target {
            width: 345px;
        }

        .pac-item-selected, .pac-item-selected:hover {
            background-color: #57b029 !important;
            color:#fff !important;
        }

        .pac-item-selected .pac-icon-marker {
            background-position: -18px -161px;
        }

        .pac-item .pac-icon-marker:hover{
            background-position: -18px -161px !important;
        }

    </style>
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCIasatwSA8nyOeN8Cobzil6yO1jsT13rE&callback=initMap&libraries=places,controls"></script>
    <script>
        function initMap() {
            //The center location of our map.
            var centerOfMap = new google.maps.LatLng(52.357971, -6.516758);
            //Map options.
            var options = {
                center: centerOfMap, //Set center.
                zoom: 7, //The zoom value.
                mapTypeId: 'roadmap'
            };
            //Create the map object.
            map = new google.maps.Map(document.getElementById('map'), options);
            //Listen for any clicks on the map.
            google.maps.event.addListener(map, 'click', function(event) {
                console.log('click markers');
                console.log(event.latLng.lng());

                removeAllMarkersFromMap(window.markers, marker);

                var geocoder = new google.maps.Geocoder;
                var latlng = {lat: parseFloat(event.latLng.lat()), lng: parseFloat(event.latLng.lng())};
                geocoder.geocode({'location': latlng}, function(place, status) {
                    if (status === 'OK') {
                        pickRequiredInformation(place[0]);
                    } else {
                        window.alert('Geocoder failed due to: ' + status);
                    }
                    console.log(window.selectedPlace);
                });
                //Get the location that the user clicked.
                var clickedLocation = event.latLng;
                //If the marker hasn't been added.
                if(marker === false){
                    //Create the marker.
                    marker = new google.maps.Marker({
                        position: clickedLocation,
                        title: 'place.name',
                        map: map,
                        draggable: true //make it draggable
                    });
                    //Listen for drag events!
                    google.maps.event.addListener(marker, 'dragend', function(event){
                        // markerLocation();
                    });
                } else{
                    //Marker has already been added, so just change its location.
                    marker.setPosition(clickedLocation);
                }
                //Get the marker's location.
                // markerLocation();
            });
            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');
                var searchBox = new google.maps.places.SearchBox(input);
                // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                // Bias the SearchBox results towards current map's viewport.
                map.addListener('bounds_changed', function() {
                    searchBox.setBounds(map.getBounds());
                });
                // Listen for the event fired when the user selects a prediction and retrieve
                // more details for that place.
                searchBox.addListener('places_changed', function() {
                console.log('place changed');
                var places = searchBox.getPlaces();
                console.log(places);
                console.log(places[0].address_components);
                var country = '';
                var state = '';
                var city = '';
                if (places.length == 0) {
                    return;
                }

                places[0].address_components.forEach(function(v){
                    console.log(v);
                    if(v.types[0] == 'country') {
                        country = {
                            'long_name': v.long_name,
                            'short_name': v.short_name
                        }
                    }

                    if(v.types[0] == 'administrative_area_level_1') {
                        state = {
                            'long_name': v.long_name,
                            'short_name': v.short_name
                        }
                    }

                    if(v.types[0] == 'locality') {
                        city = {
                            'long_name': v.long_name,
                            'short_name': v.short_name
                        }
                    }
                });
                console.log(('city'));
                console.log((city));
                if(!city) {
                    toastr.error('Please select city') ;
                } else {
                    var vales = $('#pac-input').val(city.long_name + ', ' + country.long_name);
                    $('#city_name').val(JSON.stringify(city));
                    $('#country_name').val(JSON.stringify(country));
                }
            });
        }
    </script>
    <script>

        var countryName = $('#stateName').val();
        if(countryName.toLowerCase() == "china"){
            $("#fb").hide();
            $("#wechatt").show();
        }
        function findSuccess() {
            var success = $(".alert-success").text();
            if(success != ""){
                alert('no');
            }
        }
        $(document).ready(function () {
            $('#register-form').makeAjaxForm({
                submitBtn: '#submit-register',
                alertContainer: '#register-alert-container',
                redirectTo: '{{ route('codeverification') }}',
                beforeFunction: function(){
                    if(window.isLocked) {
                      alertError('Please slide to prove you\'re human');
                      return false;
                  }
                    return true;
                },
                successFunction: function(response, $el, $this){
                    window.location.href = '{{ route('codeverification') }}';
                },
                errorFunction: function(response, $el, $this) {
                    $('.blockUI').hide();
                    if(response.responseJSON && response.responseJSON.errors) {
                        $("#slideUnlock").slideToUnlock({
                            status: false,
                            userForm: $('#submit-register'),
                            tranText: "{{trans('login.slide_to_register')}}",
                            unlockfn: function(){
                                window.isLocked = false;
                                $('.text').css('color','#fff')
                                $('#register-form').submit();
                                console.log(window.isLocked)
                            },
                            lockfn  : function(){
                                window.isLocked = true;
                                $('.text').css('color','#fff')
                                console.log(window.isLocked)
                            }
                        });
                        let err = response.responseJSON.errors;
                        var innerHtml = `<div class='alert alert-danger'> <ul>`;
                        Object.keys(err).forEach(function (key) {
                            console.log(key, err[key][0]);
                            innerHtml += "<li>" + err[key][0] + "</li>";
                        });
                        innerHtml += `</ul></div>`;
                        $('#err').html(innerHtml);
                        $("html, body").animate({scrollTop: 0}, "slow");
                    } else {
                        console.log('else');
                        console.log(response.responseJSON);
                        $("#slideUnlock").slideToUnlock({
                            status: false,
                            userForm: $('#submit-register'),
                            tranText: "{{trans('login.slide_to_register')}}",
                            unlockfn: function(){
                                window.isLocked = false;
                                $('.text').css('color','#fff')
                                $('#register-form').submit();
                                console.log(window.isLocked)
                            },
                            lockfn  : function(){
                                window.isLocked = true;
                                $('.text').css('color','#fff')
                                console.log(window.isLocked)
                            }
                        });
                        let err = response.responseJSON;
                        var innerHtml = `<div class='alert alert-danger'> <ul>`;
                        Object.keys(err).forEach(function (key) {
                            console.log(key, err[key][0]);
                            innerHtml += "<li>" + err[key][0] + "</li>";
                        });
                        innerHtml += `</ul></div>`;
                        $('#err').html(innerHtml);
                        $("html, body").animate({scrollTop: 0}, "slow");
                    }
                }
            });
        });

        $('input[type="email"]').on('focusout', function(){
            var value = $(this).val();
            if(value == ''){
                $(this).addClass('hasContent-error');
                $(this).removeClass('hasContent');
            }else{
                $(this).addClass('hasContent');
                $(this).removeClass('hasContent-error');
            }
        });

        $('input[type="password"]').on('focusout', function(){
            var value = $(this).val();
            if(value == '' || value.length < 8){
                $(this).addClass('hasContent-error');
                $(this).removeClass('hasContent');
            }else{
                $(this).addClass('hasContent');
                $(this).removeClass('hasContent-error');
            }
        });

        $('input[type="text"]').on('focusout', function(){
            var value = $(this).val();
            if(value == ''){
                $(this).addClass('hasContent-error');
                $(this).removeClass('hasContent');
            }else{
                $(this).addClass('hasContent');
                $(this).removeClass('hasContent-error');
            }
        });
        $('input[type="checkbox"]').on('focusout', function(){
            var value = $(this).val();
            if(value != checked){
                $(this).addClass('hasContent-error');
                $(this).removeClass('hasContent');
            }else{
                $(this).addClass('hasContent');
                $(this).removeClass('hasContent-error');
            }
        });
        $(document).ready(function(){
            var valueEmail = $('input[type="email"]').val();
            var valuePassword = $('input[type="password"]').val();
            var valueText = $('input[type="text"]').val();
            var valueCheckBox = $('input[type="checkbox"]').val();
            if(valueEmail != ''){
                $('input[type="email"]').addClass('hasContent');
            }
            if(valuePassword != ''){
                $('input[type="password"]').addClass('hasContent');
            }
            if(valueText != ''){
                $('input[type="text"]').addClass('hasContent');
            }
//            if(checked && valueCheckBox == checked){
//                $('input[type="checkbox"]').addClass('hasContent');
//            }
        });




    </script>
@endsection
