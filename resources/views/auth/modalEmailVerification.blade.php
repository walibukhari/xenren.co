@extends('frontend.includes.ajaxmodal')

@section('title')
    {{trans('common.check_your_email')}}
@endsection

@section('script')
    <script>
        +function () {
            $(document).ready(function () {

            });

        }(jQuery);
    </script>
@endsection

@section('footer')
    <div class="text-center">
    </div>
@endsection

@section('content')

    {!! Form::open(['method' => 'post', 'role' => 'form']) !!}

        <div class="form-body">
            <div class="form-group">
                <div id="model-alert-container">
                </div>
            </div>

            <div class="form-group f-c-gray-2">
                <div>
                    {{ trans('common.already_send_six_digit_confirmation_code', ['emailAddress' => $email_address ]) }}
                </div>
            </div>

            <div class="form-group">
                <div>
                    {!! trans('common.your_confirmation_code') !!}
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <input id="txtCharOne" type="text" maxlength="1" class="form-control number-input" value="" >
                    <input id="txtCharTwo" type="text" maxlength="1" class="form-control number-input center" value="">
                    <input id="txtCharThree" type="text" maxlength="1" class="form-control number-input" value="">
                    <span class="input-group-addon hyphen">-</span>
                    <input id="txtCharFour" type="text" maxlength="1" class="form-control number-input" value="">
                    <input id="txtCharFive" type="text" maxlength="1" class="form-control number-input center" value="">
                    <input id="txtCharSix" type="text" maxlength="1" class="form-control number-input" value="">
                </div>
            </div>

            <div class="form-group">
                <div class="text-center f-c-gray-2">
                    {{ trans('common.keep_this_window_open_while_checking') }}<br/>
                    {{ trans('common.havent_received_our_email') }}
                </div>
            </div>
        </div>

    {!! Form::close() !!}
@endsection


