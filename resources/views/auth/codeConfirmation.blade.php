@extends('frontend.layouts.default')

@section('title')
Register
@endsection

@section('description')
Register
@endsection

@section('author')
vika
@endsection

@section('header')
    <link href="../custom/css/auth/codeConfirmation.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="container">
    <div class="col-md-2">
    </div>
    <div class="col-md-8 setCol8CCP">
        <div class="text-center">
            <h4>{{trans('common.check_your_email')}}</h4>
        </div>
        {!! Form::open(['route' => 'codeconfirmation.post', 'method' => 'post', 'role' => 'form', 'id' => 'code-confirmation-form']) !!}

            <div class="form-body setCVFormBody">
                <div class="form-group">
                    <div id="alert-container">
                    </div>
                </div>

                <div class="form-group f-c-gray-2">
                    <div>
                        {{ trans('common.already_send_six_digit_confirmation_code', ['emailAddress' => $email_address ]) }}
                        {{--{{ trans('common.already_send_six_digit_confirmation_code') }}--}}
                    </div>
                </div>

                <div class="form-group">
                    <div>
                        {!! trans('common.your_confirmation_code') !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group setInputBox">
                        <input id="txtCharOne" type="text" maxlength="1" class="form-control number-input" value="" >
                        <input id="txtCharTwo" type="text" maxlength="1" class="form-control number-input center" value="">
                        <input id="txtCharThree" type="text" maxlength="1" class="form-control number-input" value="">
                        <span class="input-group-addon hyphen sethyphen">-</span>
                        <input id="txtCharFour" type="text" maxlength="1" class="form-control number-input" value="">
                        <input id="txtCharFive" type="text" maxlength="1" class="form-control number-input center" value="">
                        <input id="txtCharSix" type="text" maxlength="1" class="form-control number-input" value="">
                    </div>
                </div>

                <div class="form-group">
                    <div class="text-center f-c-gray-2">
                        {{ trans('common.keep_this_window_open_while_checking') }}<br/>
                        {{ trans('common.havent_received_our_email') }}
                    </div>
                </div>
            </div>

            <button id="btn-submit" type="button" style="display: none"></button>
            <input type="hidden" name="key" value="{{ $key }}"/>
            <input type="hidden" name="code" value=""/>
        {!! Form::close() !!}
    </div>
    <div class="col-md-2">
    </div>
</div>
@endsection

@section('footer')
    <script>
        var lang = {
            'unknown_error' : '{{ trans('validation.unknown_error') }}'
        };

        var url = {
            'home' : '{{ route('home') }}'
        }
    </script>
    <script>
        +function () {
            $(document).ready(function () {
                $(".number-input").each(function() {
                    $(this).val("");
                });
                $('#txtCharOne').focus();

                function showErrorMessage(msg)
                {
                    var element =   '<div class="custom-alerts alert alert-danger fade in">'+
                                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'+
                                        '<i class="fa-lg fa fa-warning"></i>' +
                                        msg +
                                    '</div>';

                    $('#alert-container').append(element);
                }

                $(document.body)
                    .on("keyup", "input#txtCharOne", function(){
                       $('#txtCharTwo').focus();
                    })
                    .on("keyup", "input#txtCharTwo", function(){
                       $('#txtCharThree').focus();
                    })
                    .on("keyup", "input#txtCharThree", function(){
                       $('#txtCharFour').focus();
                    })
                    .on("keyup", "input#txtCharFour", function(){
                       $('#txtCharFive').focus();
                    })
                    .on("keyup", "input#txtCharFive", function(){
                       $('#txtCharSix').focus();
                    })
                    .on("keyup", "input#txtCharSix", function(){
                        var num1 = $('#txtCharOne').val();
                        var num2 = $('#txtCharTwo').val();
                        var num3 = $('#txtCharThree').val();
                        var num4 = $('#txtCharFour').val();
                        var num5 = $('#txtCharFive').val();
                        var num6 = $('#txtCharSix').val();

                        var confirmationCode = num1 + num2 + num3 + num4 + num5 + num6;
                        $('input[name="code"]').val(confirmationCode);

                        $('#btn-submit').trigger('click');

                });

                $('#code-confirmation-form').makeAjaxForm({
                    closeModal: true,
                    inModal: true,
                    submitBtn: '#btn-submit',
                    alertContainer : '#alert-container',
                    refreshForm: true,
                    errorFunction: function(response, statusText, xhr, formElm){
                        var first = true;
                        var scroll = true;
                        if (response.status == 422) {
                            $.each(response.responseJSON, function(i) {
                                $.each(response.responseJSON[i], function(key, value) {
                                    App.alert({
                                        type: 'danger',
                                        icon: 'warning',
                                        message: value,
                                        container: '#alert-container',
                                        reset: first,
                                        focus: scroll
                                    });
                                    if (first === true) {
                                        first = false;
                                        scroll = false;
                                    }
                                });
                            });

                            $('#txtCharOne').val('');
                            $('#txtCharTwo').val('');
                            $('#txtCharThree').val('');
                            $('#txtCharFour').val('');
                            $('#txtCharFive').val('');
                            $('#txtCharSix').val('');

                            $('#txtCharOne').focus();
                        } else {
                            alert(lang.unknown_error);
                        }
                    },
                    afterSuccessFunction: function (response, $el, $this) {
                        $.each(response, function(i) {
                            $.each(response[i], function (key, value) {
                                App.alert({
                                    type: 'success',
                                    icon: 'check',
                                    message: value,
                                    place: 'append',
                                    container: '#alert-container',
                                });

                            });
                        });

                        setTimeout(function(){
                            window.location.href = url.home;
                        }, 3000);

                    }
                });

            });
        }(jQuery);
    </script>
@endsection
