@extends('backend.layout')

@section('title')
    {{trans('order.银行管理')}}
@endsection

@section('description')
    {{trans('order.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('order.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.order.conflict') }}">{{trans('order.admin_conflict_support')}}</a>
        </li>
    </ul>
@endsection

@section('footer')
    <script>
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#conflict-dt'),
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('backend.order.conflict.dt') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'order_id', name: 'order_id'},
                            {data: 'creator_name', name: 'creator_name'},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false}
                        ],
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase">{{trans('order.admin_conflict_support')}}</span>
            </div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="conflict-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th>order ID</th>
                        <th>{{trans('order.创造者')}}</th>
                        <th width="150">{{trans('order.新增时间')}}</th>
                        <th>{{trans('order.操作')}}</th>
                    </tr>
                    <tr role="row" class="filter">
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_order_id"
                                   placeholder="{{trans('order.id')}}"/>
                        </td>
                        <td>
                        </td>
                        <td>
                            <div class="input-group margin-bottom-5">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_created_after" placeholder="{{trans('order.开始')}}">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                            <div class="input-group">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_created_before" placeholder="{{trans('order.开始')}}">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </td>
                        <td>
                            <div class="margin-bottom-5">
                                <button class="btn btn-info-alt filter-submit">
                                    <i class="fa fa-search"></i> {{trans('order.过滤')}}
                                </button>
                            </div>
                            <button class="btn btn-danger-alt filter-cancel">
                                <i class="fa fa-times"></i> {{trans('order.重置')}}
                            </button>
                        </td>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection