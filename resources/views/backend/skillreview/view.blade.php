@extends('backend.layout')

@section('title')
    {{trans('common.skill_score')}}
@endsection

@section('description')

@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('common.backend')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.skillreview') }}">{{trans('common.skill_score')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="javascript:;">{{ $model->real_name or $model->email }}</a>
        </li>
    </ul>
@endsection

@section('footer')
    <script>
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#comment-dt'),
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('backend.skillreview.comment.dt', ['id' => $model->id]) }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'project_name', name: 'project_name'},
                            {data: 'comment', name: 'comment'},
                            {data: 'score_overall', name: 'score_overall'},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false}
                        ],
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase">{{trans('common.skill_list')}}
                    ( {{ $model->real_name or $model->email }} )</span>
            </div>
            <div class="actions">

            </div>
        </div>
        <div class="portlet-body">
            <div class="table-scrollable">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th> {{trans('common.skill')}} </th>
                        <th> {{trans('common.experience')}}</th>
                        <th> {{trans('common.grade')}} </th>
                    </tr>
                    </thead>
                    <tbody id="skills-container">
                        @foreach ($model->skills as $key => $var)
                            <tr>
                                <td>{{ $var->skill->name_cn }}</td>
                                <td>
                                    @if( isset($var->experience) )
                                    {{ $var->experience }}
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>{{ $var->explainSkillLevel() }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('skillreview.技能评语')}}
                    ( {{ $model->real_name or $model->email }} )</span>
            </div>
            <div class="actions">
                <a href="{{ route('backend.skillreview.comment.create', ['id' => $model->id]) }}"
                   class="btn btn-primary red"><i class="fa fa-plus"></i> {{trans('skillreview.新增评语')}}</a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="comment-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th width="80">{{trans('common.id')}}</th>
                        <th width="300">{{trans('common.project_title')}}</th>
                        <th>{{trans('common.comments')}}</th>
                        <th width="100">{{trans('common.average_score')}}</th>
                        <th width="200">{{trans('common.comment_time')}}</th>
                        <th width="50">{{trans('common.action')}}</th>
                    </tr>
                    <tr role="row" class="filter">
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_id"
                                   placeholder="{{trans('common.id')}}"/>
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_id"
                                   placeholder="{{trans('common.project_title')}}"/>
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_comment"
                                   placeholder="{{trans('common.content')}}">
                        </td>
                        <td>
                            <div class="input-group margin-bottom-5">
                                <input type="text" class="form-control form-filter input-sm fund-input"
                                       name="filter_score_overall_from" placeholder="{{trans('common.start')}}">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm default" type="button">
                                        <i class="fa fa-asterisk"></i>
                                    </button>
                                </span>
                            </div>
                            <div class="input-group">
                                <input type="text" class="form-control form-filter input-sm fund-input"
                                       name="filter_score_overall_to" placeholder="{{trans('common.end')}}">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm default" type="button">
                                        <i class="fa fa-asterisk"></i>
                                    </button>
                                </span>
                            </div>
                        </td>
                        <td>
                            <div class="input-group margin-bottom-5">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_created_after" placeholder="{{trans('common.start')}}">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                            <div class="input-group">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_created_before" placeholder="{{trans('common.end')}}">
                                <span class="input-group-btn">
                                    <button class="btn btn-sm default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                        </td>
                        <td>
                            <div class="margin-bottom-5">
                                <button class="btn btn-info-alt filter-submit">
                                    <i class="fa fa-search"></i> {{trans('common.filter')}}
                                </button>
                            </div>
                            <button class="btn btn-danger-alt filter-cancel">
                                <i class="fa fa-times"></i> {{trans('common.reset')}}
                            </button>
                        </td>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection