@extends('backend.layout')

@section('title')
    {{trans('common.skill_score')}}
@endsection

@section('description')

@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('common.backend')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.skillreview.view', ['id' => $model->id]) }}">{{trans('common.skill_score')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.skillreview.comment.create', ['id' => $model->id]) }}">{{trans('common.add_skill_score')}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <link rel="stylesheet" href="/assets/global/plugins/magicsuggest/magicsuggest-min.css" type="text/css" />
    <style>
        #map-container { width: 100%; min-height: 360px; }
        #images-container > div {
            margin-bottom: 20px;
        }

        .ms-helper {
            display: none !important;
        }

        i.fa.fa-plus.search-plus {
            display: none !important;
        }

        .ms-res-ctn em {
            background-color: #fcc33a !important;
        }

        .ms-res-item.ms-res-item-active {
            background-color: #5ec329;
            color: #fff;
        }

    </style>
@endsection

@section('footer')
    <script type="text/javascript" src="/assets/global/plugins/magicsuggest/magicsuggest-min.js" ></script>
    <script>
        var lang = {
            search: '{{ trans('common.search') }}',
        }

        var skillList = {!! $skillList !!};
    </script>
    <script>
        +function() {
            $(document).ready(function() {
                $('#skill-form').makeAjaxForm({
                    submitBtn: '#submit-btn',
                    redirectTo: '{{ route('backend.skillreview.view', ['id' => $model->id]) }}',
                });

                $(document).on('click', '.skill-delete', function () {
                    $(this).parent('td').parent('tr').remove();
                });

                var rowItem = 1;
                $(document).on('click', '#add-skill', function () {

                    var skill_template =
                        '<tr>' +
                            '<td width="60%"> <div id="msSkill' + rowItem + '"></div> <input type="hidden" id="hid-skill' + rowItem + '" name="skill[]" value=""></td>' +
                            '<td>{!! Form::select('skill_score[]', \App\Constants::getSkillScoreSelects(), 0, ['class' => 'form-control']) !!}</td>' +
                            '<td width="20%"> <div class="col-md-6">{!! Form::select('skill_experience_action[]', [0 => '+', 1 => '-'], 0, ['class' => 'form-control']) !!}</div> <div class="col-md-6">{!! Form::text('skill_experience[]', '', ['class' => 'form-control number-input']) !!}</div> </td>' +
                            '<td> <a href="javascript:;" class="btn btn-xs red skill-delete"><i class="fa fa-trash"></i></a> </td>' +
                        '</tr>';

                    $('#skills-container').append(skill_template);
                    $('.number-input').numberInput();

                    var msSkill = $('#msSkill' + rowItem).magicSuggest({
                        allowFreeEntries: false,
                        allowDuplicates: false,
                        hideTrigger: true,
                        placeholder: lang.search,
                        data: skillList,
                        method: 'get',
                        highlight: true,
                        noSuggestionText: '',
                        maxSelection: 1,
                        renderer: function(data){
                            if( data.description == null )
                            {
                                return '<b>' + data.name + '</b>';
                                // return '<b>' + data.name + '</b>' + '<i class="fa fa-plus pull-right add-more"></i>';
                            }
                            else
                            {
                                var name = data.name.split("|");

                                return '<b>' + name[0] + '</b> | <span class="search-desc">' + data.description + '<i class="fa fa-plus search-plus "></i></span>';
                                // return '<b>' + name[0] + '</b> | <span class="search-desc">' + data.description + '</span>' + '<i class="fa fa-plus pull-right add-more"></i>';
                            }

                        },
                        selectionRenderer: function(data){
                            result = data.name;
                            name = result.split("|")[0];
                            return name;
                        }
                    });

                    $(msSkill).on(
                        'selectionchange', function(e, cb, s){
                            var id = cb.input.parent().parent()[0].nextElementSibling.id;
                            $('#' + id ).val(cb.getValue());
                        }
                    );

                    rowItem = rowItem + 1;
                });

                $(document).on('change', '#attitude-switch', function () {
                    var related = $('.attitude-related');
                    if ($(this).val() == 1) {
                        related.show();
                    } else {
                        related.hide();
                    }
                });

                var image_template =
                    '<div class="col-xs-12 col-sm-3 image-box-container">' +
                        '<div>' +
                            '<div class="fileinput fileinput-new" data-provides="fileinput">' +
                                '<div class="fileinput-new thumbnail" style="max-width: 100%;">' +
                                    '<img src="http://www.placehold.it/1024x768/EFEFEF/AAAAAA&amp;text=No+Image" alt="" />' +
                                '</div>' +
                                '<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100%;"> </div>' +
                                '<div>' +
                                    '<span class="btn default btn-file">' +
                                        '<span class="fileinput-new"> {{trans('common.select_image')}} </span>' +
                                        '<span class="fileinput-exists"> {{trans('common.change_image')}} </span>' +
                                        '{!! Form::file('images[]', ['class' => 'form-control']) !!}' +
                                    '</span>' +
                                    '<a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{trans('common.remove')}} </a>' +
                                    '<a class="btn btn-primary red remove-img"> <i class="fa fa-trash"></i></a>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>';

                $(document).on('click', '#add-image', function () {
                    $('#images-container').append(image_template);
                });

                $(document).on('click', '.remove-img', function () {
                    $(this).closest('.image-box-container').remove();
                });


                var msSkill = $('#msSkill').magicSuggest({
                    allowFreeEntries: false,
                    allowDuplicates: false,
                    hideTrigger: true,
                    placeholder: lang.search,
                    data: skillList,
                    method: 'get',
                    highlight: true,
                    noSuggestionText: '',
                    maxSelection: 1,
                    renderer: function(data){
                        if( data.description == null )
                        {
                            return '<b>' + data.name + '</b>';
                            // return '<b>' + data.name + '</b>' + '<i class="fa fa-plus pull-right add-more"></i>';
                        }
                        else
                        {
                            var name = data.name.split("|");

                            return '<b>' + name[0] + '</b> | <span class="search-desc">' + data.description + '<i class="fa fa-plus search-plus "></i></span>';
                            // return '<b>' + name[0] + '</b> | <span class="search-desc">' + data.description + '</span>' + '<i class="fa fa-plus pull-right add-more"></i>';
                        }

                    },
                    selectionRenderer: function(data){
                        result = data.name;
                        name = result.split("|")[0];
                        return name;
                    }
                });

                $(msSkill).on(
                    'selectionchange', function(e, cb, s){
                        $('#hid-skill0').val(cb.getValue());
                    }
                );
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    {!! Form::open(['route' => ['backend.skillreview.comment.create.post', 'id' => $model->id], 'id' => 'skill-form', 'method' => 'post', 'role' => 'form']) !!}
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <span class="caption-subject bold uppercase"> {{trans('common.skill_score')}}</span>
                </div>
                <div class="actions">

                </div>
            </div>
            <div class="portlet-body">
                <div class="form-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{ trans('common.official_project') }}</label>
                            <div class="col-sm-9">
                                <select name="official_project" class="form-control">
                                    <option value="0" >{{ trans('common.none') }}</option>
                                    @foreach($officialProjects as $officialProject)
                                    <option value="{{ $officialProject->id }}" >{{ $officialProject->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="table-scrollable">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th> {{trans('common.skill')}} </th>
                                    <th> {{trans('common.score')}} </th>
                                    <th class="text-center"> {{trans('common.experience')}} </th>
                                    <th> {{trans('common.action')}} </th>
                                </tr>
                            </thead>
                            <tbody id="skills-container">
                                <tr>
                                    <td width="60%">
                                        <div id="msSkill"></div>
                                        <input type="hidden" id="hid-skill0" name="skill[]" value="">
                                    </td>
                                    <td>
                                        {!! Form::select('skill_score[]', \App\Constants::getSkillScoreSelects(), 0, ['class' => 'form-control']) !!}
                                    </td>
                                    <td width="20%">
                                        <div class="col-md-6">
                                            {!! Form::select('skill_experience_action[]', [0 => '+', 1 => '-'], 0, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="col-md-6">
                                            {!! Form::text('skill_experience[]', '', ['class' => 'form-control number-input']) !!}
                                        </div>
                                    </td>
                                    <td> <a href="javascript:;" class="btn btn-xs red skill-delete"><i class="fa fa-trash"></i></a> </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4">
                                        <a href="javascript:;" class="btn purple btn-block" id="add-skill"> <i
                                                    class="fa fa-plus"></i> {{trans('common.add_skill')}}</a>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <span class="caption-subject bold uppercase"> {{trans('common.other')}}</span>
                </div>
                <div class="actions">

                </div>
            </div>
            <div class="portlet-body form">
                <div class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans('common.attitude')}}</label>
                            <div class="col-sm-9">
                                {!! Form::select('attitude_rate', [0 => trans('common.no_comment'), 1 => trans('common.have_something_to_say')], 0, ['class' => 'form-control', 'id' => 'attitude-switch']) !!}
                            </div>
                        </div>
                        <div class="form-group attitude-related" style="display: none;">
                            <label class="control-label col-sm-3">{{trans('common.attitude_score')}}</label>
                            <div class="col-sm-9">
                                {!! Form::select('score_attitude', \App\Constants::getSkillScoreSelects(), 0, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group attitude-related" style="display: none;">
                            <label class="control-label col-sm-3">{{trans('common.attitude_review')}}</label>
                            <div class="col-sm-9">
                                {!! Form::textarea('attitude_comment', '', ['class' => 'form-control', 'rows' => 4]) !!}
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <label class="control-label col-sm-3">Github Link:</label>
                            <div  class="col-sm-9">
                                <input class="form-control" placeholder="https://www.github.com/username/reponame/commit/abA12DAs8" name="github_link">
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <label class="control-label col-sm-3">Video Fle: <small>(Max 30MB)</small>  </label>
                            <div  class="col-sm-3">
                                <input class="form-control" type="file" name="video">
                            </div>
                            <label class="control-label col-sm-3">Video Fle Thumb: <small>(Max 30MB)</small>  </label>
                            <div  class="col-sm-3">
                                <input class="form-control" type="file" name="video_thumb">
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans('common.concluding_comments')}}</label>
                            <div class="col-sm-9">
                                {!! Form::textarea('comment', '', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <span class="caption-helper">{{trans('common.file')}}</span>
                </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-primary purple" id="add-image"><i
                                class="fa fa-plus"></i> {{trans('common.add_file')}}</a>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="row" id="images-container">
                    <div class="col-xs-12 col-sm-3 image-box-container">
                        <div>
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="max-width: 100%;">
                                    <img src="http://www.placehold.it/1024x768/EFEFEF/AAAAAA&amp;text=No+Image" alt="" />
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100%;"> </div>
                                <div>
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new"> {{trans('common.select_file')}}</span>
                                        <span class="fileinput-exists"> {{trans('common.change_file ')}} </span>
                                        {!! Form::file('images[]', ['class' => 'form-control']) !!}
                                    </span>
                                    <a href="javascript:;" class="btn red fileinput-exists"
                                       data-dismiss="fileinput"> {{trans('common.remove')}} </a>
                                    <a class="btn btn-primary red remove-img"> <i class="fa fa-trash"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="portlet light bordered">
            <div class="portlet-body">
                <button type="button" class="btn btn-block blue" id="submit-btn"><i
                            class="fa fa-check"></i> {{trans('common.submit')}}</button>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection