@extends('backend.layout')

@section('title')
    {{trans('common.skill_score')}}
@endsection

@section('description')

@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('common.backend')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.skillreview.view', ['id' => $model->id]) }}">{{trans('common.skill_score')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.skillreview.comment.edit', ['id' => $model->id, 'cid' => $comment->id]) }}">{{trans('common.edit_skill_score')}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <link rel="stylesheet" href="/assets/global/plugins/magicsuggest/magicsuggest-min.css" type="text/css" />
    <style>
        #map-container { width: 100%; min-height: 360px; }

        .ms-helper {
            display: none !important;
        }

        i.fa.fa-plus.search-plus {
            display: none !important;
        }

        .ms-res-ctn em {
            background-color: #fcc33a !important;
        }

        .ms-res-item.ms-res-item-active {
            background-color: #5ec329;
            color: #fff;
        }
    </style>
@endsection

@section('footer')
    <script type="text/javascript" src="/assets/global/plugins/magicsuggest/magicsuggest-min.js" ></script>
    <script>
        var lang = {
            search: '{{ trans('common.search') }}',
        }

        var skillList = {!! $skillList !!};
    </script>
    <script>
        +function() {
            $(document).ready(function() {
                var jcrop_api;
                $('#cropping-image-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-crop-image-submit',
                    alertContainer : '#model-alert-container',
                    afterSuccessFunction: function (response, $el, $this) {
                        if( response.status == 'OK'){
                            $('#modalCropImage').modal('toggle');
                            $('#' + response.id ).attr('src', response.thumbnailPath);
                            resetJcrop();
                        }else{
                            App.alert({
                                type: 'danger',
                                icon: 'warning',
                                message: response.message,
                                container: '#model-alert-container',
                                reset: true,
                                focus: true
                            });
                        }

                        App.unblockUI('#cropping-image-form');
                    }
                });

                function updateCoords(c)
                {
                    $('#crop_x').val(c.x);
                    $('#crop_y').val(c.y);
                    $('#crop_x2').val(c.x2);
                    $('#crop_y2').val(c.y2);
                    $('#crop_w').val(c.w);
                    $('#crop_h').val(c.h);
                };

                $(".tool").click(function(){
                    var img = $(this).parent().find(".link-img .old_image").attr("src");
                    var img_id = $(this).parent().find(".link-img img").attr("id");
                    $('#modalCropImage').modal('toggle');
                    $("#imgOriginalImage").attr("src", img);
                    $("input[name=imgOriginalImage]").val(img);
                    $("input[name=img_id]").val(img_id);
                    Jcrop();
                });

                //reset crop img
                $(".modal-custom").click(function(e){
                    if (e.target !== this)
                    return;
                    resetJcrop();
                });

                function Jcrop(){
                    $('#imgOriginalImage').Jcrop({
                        onSelect:    updateCoords,
                        bgColor:     'black',
                        bgOpacity:   .4,
                        setSelect:   [ 100, 100, 50, 50 ],
                        aspectRatio: 250 / 180,
                        boxWidth: 450,   //Maximum width you want for your bigger images
                        boxHeight: 400,  //Maximum Height for your bigger images
                        addClass: 'center-block'
                    },function(){
                        jcrop_api = this;
                    });
                }

                function resetJcrop(){
                    jcrop_api.destroy();
                }

                $(".thumbnail img").click(function(){
                    var img = $(this).attr("src");
                    $("#popup-img img").attr("src", img);
                    $('#popup-img').modal('toggle');
                });

                $('#skill-form').makeAjaxForm({
                    submitBtn: '#submit-btn',
                    redirectTo: '{{ route('backend.skillreview.view', ['id' => $model->id]) }}',
                });

                $(".old").click(function(){
                    if($(this).val() == ""){
                        $(this).attr("name", "images_old[]");
                        $(this).next(".images_old_path").attr("name", "images_old_path[]");
                    }
                });

                $(document).on('click', '.skill-delete', function () {
                    if ($(this).hasClass('old-record')) {
                        var id = $(this).data('recordid');
                        var str = '<input type="hidden" name="delete_skill[]" value="' + id + '" />';
                        $('#skill-form').append(str);
                    }
                    $(this).parent('td').parent('tr').remove();
                });

                var rowItem = 1;
                $(document).on('click', '#add-skill', function () {

                    var skill_template =
                        '<tr>' +
                            '<td width="60%"> <div id="msSkill' + rowItem + '"></div> <input type="hidden" id="hid-skill' + rowItem + '" name="skill[]" value=""></td>' +
                            '<td>{!! Form::select('skill_score[]', \App\Constants::getSkillScoreSelects(), 0, ['class' => 'form-control']) !!}</td>' +
                            '<td width="20%"> <div class="col-md-6">{!! Form::select('skill_experience_action[]', [0 => '+', 1 => '-'], 0, ['class' => 'form-control']) !!}</div> <div class="col-md-6">{!! Form::text('skill_experience[]', '', ['class' => 'form-control number-input']) !!}</div> </td>' +
                            '<td> <a href="javascript:;" class="btn btn-xs red skill-delete"><i class="fa fa-trash"></i></a> </td>' +
                        '</tr>';

                    $('#skills-container').append(skill_template);
                    $('.number-input').numberInput();

                    var msSkill = $('#msSkill' + rowItem).magicSuggest({
                        allowFreeEntries: false,
                        allowDuplicates: false,
                        hideTrigger: true,
                        placeholder: lang.search,
                        data: skillList,
                        method: 'get',
                        highlight: true,
                        noSuggestionText: '',
                        maxSelection: 1,
                        renderer: function(data){
                            if( data.description == null )
                            {
                                return '<b>' + data.name + '</b>';
                                // return '<b>' + data.name + '</b>' + '<i class="fa fa-plus pull-right add-more"></i>';
                            }
                            else
                            {
                                var name = data.name.split("|");

                                return '<b>' + name[0] + '</b> | <span class="search-desc">' + data.description + '<i class="fa fa-plus search-plus "></i></span>';
                                // return '<b>' + name[0] + '</b> | <span class="search-desc">' + data.description + '</span>' + '<i class="fa fa-plus pull-right add-more"></i>';
                            }

                        },
                        selectionRenderer: function(data){
                            result = data.name;
                            name = result.split("|")[0];
                            return name;
                        }
                    });

                    $(msSkill).on(
                        'selectionchange', function(e, cb, s){
                            var id = cb.input.parent().parent()[0].nextElementSibling.id;
                            $('#' + id ).val(cb.getValue());
                        }
                    );

                    rowItem = rowItem + 1;
                });

                $(document).on('change', '#attitude-switch', function () {
                    var related = $('.attitude-related');
                    if ($(this).val() == 1) {
                        related.show();
                    } else {
                        related.hide();
                    }
                });

                var image_template =
                    '<div class="col-xs-12 col-sm-3 image-box-container">' +
                        '<div>' +
                            '<div class="fileinput fileinput-new" data-provides="fileinput">' +
                                '<div class="fileinput-new thumbnail" style="max-width: 100%;">' +
                                    '<img src="http://www.placehold.it/1024x768/EFEFEF/AAAAAA&amp;text=No+Image" alt="" />' +
                                '</div>' +
                                '<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100%;"> </div>' +
                                '<div>' +
                                    '<span class="btn default btn-file">' +
                                        '<span class="fileinput-new"> {{trans('common.select_image')}} </span>' +
                                        '<span class="fileinput-exists"> {{trans('common.change_image')}} </span>' +
                                        '{!! Form::file('images[]', ['class' => 'form-control']) !!}' +
                                    '</span>' +
                    '<a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{trans('common.remove')}} </a>' +
                                    '<a class="btn btn-primary red remove-img"> <i class="fa fa-trash"></i></a>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>';

                $(document).on('click', '#add-image', function () {
                    $('#images-container').append(image_template);
                });

                $(document).on('click', '.remove-img', function () {
                    if ($(this).attr('data-imageid')) {
                        var str = '<input type="hidden" name="delete_image[]" value="' + $(this).data('imageid') + '" />';
                        $('#skill-form').append(str);
                    }
                    $(this).closest('.image-box-container').remove();
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    {!! Form::open(['route' => ['backend.skillreview.comment.edit.post', 'id' => $model->id, 'cid' => $comment->id], 'id' => 'skill-form', 'method' => 'post', 'role' => 'form', 'enctype' => 'multipart/form-data']) !!}
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <span class="caption-subject bold uppercase"> {{trans('common.skill_score')}}</span>
                </div>
                <div class="actions">

                </div>
            </div>
            <div class="portlet-body">
                <div class="form-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{ trans('common.official_project') }}</label>
                            <div class="col-sm-9">
                                {{--<select name="official_project" class="form-control" disabled>--}}
                                    {{--@foreach($officialProjects as $officialProject)--}}
                                    {{--<option value="{{ $officialProject->id }}" {{ $comment->official_project_id == $officialProject->id? 'selected=selected': '' }}>--}}
                                        {{--{{ $officialProject->title }}--}}
                                    {{--</option>--}}
                                    {{--@endforeach--}}
                                {{--</select>--}}
                                {{ Form::select('official_project', $officialProjects, $comment->official_project_id, ['class' => 'form-control', 'disabled' => 'disabled']) }}
                            </div>
                        </div>
                    </div>
                    <div class="table-scrollable">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th> {{trans('common.skill')}}  </th>
                                    <th> {{trans('common.score')}} </th>
                                    <th class="text-center"> {{trans('common.experience')}}  </th>
                                    <th>  {{trans('common.action')}} </th>
                                </tr>
                            </thead>
                            <tbody id="skills-container">
                                @foreach ($comment_skills as $key => $var)
                                    <tr>
                                        <td width="60%"> {{ $var->skill->name_cn }} </td>
                                        <td>
                                            {{ $var->score }}
                                        </td>
                                        <td width="20%">
                                            @if ($var->action_type == 0)
                                                +
                                            @elseif ($var->action_type == 1)
                                                -
                                            @else

                                            @endif
                                            {{ $var->experience }}
                                        </td>
                                        <td> <a href="javascript:;" class="btn btn-xs red skill-delete old-record" data-recordid="{{ $var->id }}"><i class="fa fa-trash"></i></a> </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4">
                                        <a href="javascript:;" class="btn purple btn-block" id="add-skill"> <i
                                                    class="fa fa-plus"></i> {{trans('common.add_skill')}}</a>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <span class="caption-subject bold uppercase"> {{trans('common.other')}}</span>
                </div>
                <div class="actions">

                </div>
            </div>
            <div class="portlet-body form">
                <div class="form-horizontal">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans('common.attitude')}}</label>
                            <div class="col-sm-9">
                                {!! Form::select('attitude_rate', [0 => trans('common.no_comment'), 1 => trans('common.have_something_to_say')], $comment->attitude_rate, ['class' => 'form-control', 'id' => 'attitude-switch']) !!}
                            </div>
                        </div>
                        <div class="form-group attitude-related" style="{{ $comment->attitude_rate == 0 ? 'display: none;' : '' }}">
                            <label class="control-label col-sm-3">{{trans('common.attitude_score')}}</label>
                            <div class="col-sm-9">
                                {!! Form::select('score_attitude', \App\Constants::getSkillScoreSelects(), $comment->score_attitude, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group attitude-related" style="{{ $comment->attitude_rate == 0 ? 'display: none;' : '' }}">
                            <label class="control-label col-sm-3">{{trans('common.attitude_review')}}</label>
                            <div class="col-sm-9">
                                {!! Form::textarea('attitude_comment', $comment->attitude_comment, ['class' => 'form-control', 'rows' => 4]) !!}
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <label class="control-label col-sm-3">Github Link:</label>
                            <div  class="col-sm-9">
                                <input class="form-control" placeholder="https://www.github.com/username/reponame/commit/abA12DAs8" name="github_link" value="{{$comment->github_link}}">
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <label class="control-label col-sm-3">Video Fle: <small>(Max 30MB)</small>  </label>
                            <div  class="col-sm-3">
                                <input class="form-control" type="file" name="video">
                            </div>
                            <label class="control-label col-sm-3">Video Fle Thumb: <small>(Max 30MB)</small>  </label>
                            <div  class="col-sm-3">
                                <input class="form-control" type="file" name="video_thumb">
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans('common.concluding_comments')}}</label>
                            <div class="col-sm-9">
                                {!! Form::textarea('comment', $comment->comment, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <span class="caption-helper">{{trans('common.file')}}</span>
                </div>
                <div class="actions">
                    <a href="javascript:;" class="btn btn-primary purple" id="add-image"><i
                                class="fa fa-plus"></i> {{trans('common.add_file')}}</a>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="row" id="images-container">
                    @if ($files && count($files) > 0)
                        @foreach ($files as $key => $var)
                            <div class="col-xs-12 col-sm-3 image-box-container">
                                <div>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="max-width: 100%;">
                                            @if( in_array( substr(strrchr($var->path,'.'),1), ['jpg', 'jpeg', 'gif', 'bmp', 'png']) )
                                            <div>
                                                <a href="#/" data-lightbox="portfolio">
                                                    <div class="link-img">
                                                        @if($var->thumbnail_file)
                                                            <img class="img-responsive" id="portfolio-img-{{ $var->id }}" src="{{ asset($var->thumbnail_file) }}">
                                                        @else($var->path)
                                                            <img class="img-responsive" id="portfolio-img-{{ $var->id }}" src="{{ asset($var->path) }}">
                                                        @endif
                                                            <img class="old_image hide" src="{{ asset($var->path) }}">
                                                    </div>
                                                </a>
                                                <div class="tool">
                                                    <i class="fa fa-scissors"></i>
                                                </div>
                                            </div>
                                            @elseif (in_array( substr(strrchr($var->path,'.'),1), ['doc', 'docx']))
                                            <img src="\images\doc.jpg" alt="" />
                                            @elseif (in_array( substr(strrchr($var->path,'.'),1), ['pdf']))
                                            <img src="\images\pdf.jpg" alt="" />
                                            @elseif (in_array( substr(strrchr($var->path,'.'),1), ['xls', 'xlsx']))
                                            <img src="\images\xls.jpg" alt="" />
                                            @elseif (in_array( substr(strrchr($var->path,'.'),1), ['mp4']))
                                            <img src="\images\mp4.png" alt="" />
                                            @elseif (in_array( substr(strrchr($var->path,'.'),1), ['mp3']))
                                            <img src="\images\mp3.png" alt="" />
                                            @endif
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100%;"> </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> {{trans('common.select_file')}} </span>
                                                <span class="fileinput-exists"> {{trans('common.change_file')}} </span>
                                                {!! Form::file('', ['class' => 'form-control old']) !!}
                                                {!! Form::hidden('', $var->path, ['class' => 'images_old_path']) !!}
                                            </span>
                                            <a href="javascript:;" class="btn red fileinput-exists"
                                               data-dismiss="fileinput"> {{trans('common.remove')}}</a>
                                            <a class="btn btn-primary red remove-img" data-imageid="{{ $var->id }}"> <i class="fa fa-trash"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>

        <div class="portlet light bordered">
            <div class="portlet-body">
                <button type="button" class="btn btn-block blue" id="submit-btn"><i
                            class="fa fa-check"></i> {{trans('common.submit')}}</button>
            </div>
        </div>
    {!! Form::close() !!}

    {{-- crop img --}}
    <div class="modal modal-custom fade" id="modalCropImage" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content"> 
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">crop image</h4>
                </div>
                <div class="modal-body">
                     {!! Form::open(['route' => ['backend.cropImage', 'id' => $model->id, 'cid' => $comment->id], 'id' => 'cropping-image-form', 'method' => 'post', 'role' => 'form', 'enctype' => 'multipart/form-data']) !!}
                        <div class="form-body">
                            <div class="form-group">
                                <div id="model-alert-container">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="crop-image-wrapper">
                                    <img src="" id="imgOriginalImage" />
                                </div>
                            </div>
                        </div>
                        <input class="imgOriginalImage" name="imgOriginalImage" type="hidden">
                        <input id="crop_x" name="x" type="hidden">
                        <input id="crop_y" name="y" type="hidden">
                        <input id="crop_x2" name="x2" type="hidden">
                        <input id="crop_y2" name="y2" type="hidden">
                        <input id="crop_w" name="w" type="hidden">
                        <input id="crop_h" name="h" type="hidden">
                        <input id="img_id" name="img_id" type="hidden">
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" id="btn-crop-image-submit" class="btn btn-primary blue">编辑</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
            </div>
            
        </div>
    </div>

    {{-- popup img --}}
    <div id="popup-img" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">image</h4>
                </div>
                <div class="modal-body">
                    <img src="" class="img-responsive center-block">
                </div>
            </div>
        </div>
    </div>

</div>@endsection

@section('modal')

@endsection