@extends('ajaxmodal')

@section('title')
    {{trans('skillkeywords.批准通过技能关键字')}}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                $('#approve-skill-keyword-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-approve'
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-approve" class="btn btn-primary blue">approve</button>
@endsection

@section('content')
    {!! Form::open(['route' => ['backend.skillkeyword.approve.post', 'id' => $model->id], 'method' => 'post', 'role' => 'form', 'id' => 'approve-skill-keyword-form', 'files' => true]) !!}
        <div class="form-body">
            <div class="form-group">
                <label>Approved by skill keywords
                </label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-building"></i>
                    </span>
                    {!! Form::text('name_cn', $model->name_cn, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>Skill keyword name-Chinese
                </label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-link"></i>
                    </span>
                    {!! Form::text('reference_link_cn', $reference_link_cn, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>Skill Keyword Name-English
                </label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-building"></i>
                    </span>
                    {!! Form::text('name_en', $model->name_en, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>Skill Keyword Link-English
                </label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-link"></i>
                    </span>
                    {!! Form::text('reference_link_en', $reference_link_en, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection
