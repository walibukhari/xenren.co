@extends('backend.layout')

@section('title')
    {{trans('skillkeywords.技能关键字管理')}}
@endsection

@section('description')
    {{trans('skillkeywords.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sideMenu.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.skillkeyword') }}">{{trans('sideMenu.技能关键字管理')}}</a>
        </li>
    </ul>
@endsection

@section('header')

@endsection

@section('footer')
    <script>
        {{--+function() {--}}
        {{--    $(document).ready(function() {--}}
        {{--        var dTable = new Datatable();--}}
        {{--        dTable.init({--}}
        {{--            src: $('#skill-keywords-dt'),--}}
        {{--            dataTable: {--}}
        {{--                @include('custom.datatable.common')--}}
        {{--                "ajax": {--}}
        {{--                    "url": "{{ route('backend.skillkeyword.dt') }}",--}}
        {{--                    "type": "GET"--}}
        {{--                },--}}
        {{--                columns: [--}}
        {{--                    {data: 'id', name: 'id'},--}}
        {{--                    {data: 'user', name: 'user', orderable: false, searchable: false},--}}
        {{--                    {data: 'name_en', name: 'name_en', orderable: false, searchable: false},--}}
        {{--                    {data: 'name_cn', name: 'name_cn', orderable: false, searchable: false},--}}
        {{--                    {data: 'reference_link', name: 'reference_link', orderable: false, searchable: false},--}}
        {{--                    {data: 'submit_count', name: 'submit_count', orderable: false, searchable: false},--}}
        {{--                    {data: 'created_at', name: 'created_at'},--}}
        {{--                    {data: 'actions', name: 'actions', orderable: false, searchable: false}--}}
        {{--                ],--}}
        {{--            }--}}
        {{--        });--}}
        {{--    });--}}
        {{--}(jQuery);--}}
        var data = [];
        function bulkDelete(id) {
            var value = document.getElementById("checkValue-"+id).checked;
            if(value === true) {
                console.log(value);
                console.log('push');
                data.push(id);
            } else {
                console.log(value);
                data.map(function (check) {
                    if(check === id) {
                        var index = data.indexOf(check);
                        data.splice(index,1);
                    }
                })
            }
            console.log('data data data');
            console.log(data);
        }

        function bulk_delete() {
            console.log('check data is come or not');
            console.log(data);
            $('#rows').val(data);
        }
    </script>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('error'))
                <div class="alert alert-danger">
                     {{session()->get('error')}}
                </div>
            @endif
        </div>
    </div>
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('skillkeywords.技能关键字列表')}}</span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
{{--                <form method="post" action="{{route('backend.skillkeyword.bulkDelete')}}">--}}
{{--                <table class="table table-striped table-bordered table-hover table-checkable" id="skill-keywords-dt">--}}
{{--                    <thead>--}}
{{--                    <tr role="row" class="heading">--}}
{{--                        <th width="80">{{trans('common.id')}}</th>--}}
{{--                        <th>{{trans('skillkeywords.用户')}}</th>--}}
{{--                        <th>{{trans('skillkeywords.关键字-英')}}</th>--}}
{{--                        <th>{{trans('skillkeywords.关键字-华')}}</th>--}}
{{--                        <th>{{trans('skillkeywords.参考链接')}}</th>--}}
{{--                        <th>{{trans('skillkeywords.提交次数')}}</th>--}}
{{--                        <th width="200">{{trans('skillkeywords.新增时间')}}</th>--}}
{{--                        <th width="200">{{trans('skillkeywords.操作')}}</th>--}}
{{--                    </tr>--}}
{{--                    <tr role="row" class="filter">--}}
{{--                        <td>--}}
{{--                            <input type="text" class="form-control form-filter input-sm" name="filter_id"--}}
{{--                                   placeholder="{{trans('common.id')}}"/>--}}
{{--                        </td>--}}
{{--                        <td>--}}
{{--                            <input type="text" class="form-control form-filter input-sm" name="filter_user"--}}
{{--                                   placeholder="{{trans('skillkeywords.用户')}}">--}}
{{--                        </td>--}}
{{--                        <td>--}}
{{--                            <input type="text" class="form-control form-filter input-sm" name="filter_name_en"--}}
{{--                                   placeholder="{{trans('skillkeywords.技能关键字-英')}}">--}}
{{--                        </td>--}}
{{--                        <td>--}}
{{--                            <input type="text" class="form-control form-filter input-sm" name="filter_name_cn"--}}
{{--                                   placeholder="{{trans('skillkeywords.技能关键字-华')}}">--}}
{{--                        </td>--}}
{{--                        <td>--}}
{{--                        </td>--}}
{{--                        <td>--}}
{{--                        </td>--}}
{{--                        <td>--}}
{{--                            <div class="input-group margin-bottom-5">--}}
{{--                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly--}}
{{--                                       name="filter_created_after" placeholder="{{trans('common.start')}}">--}}
{{--                                    <span class="input-group-btn">--}}
{{--                                        <button class="btn btn-sm default" type="button">--}}
{{--                                            <i class="fa fa-calendar"></i>--}}
{{--                                        </button>--}}
{{--                                    </span>--}}
{{--                            </div>--}}
{{--                            <div class="input-group">--}}
{{--                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly--}}
{{--                                       name="filter_created_before" placeholder="{{trans('common.end')}}">--}}
{{--                                    <span class="input-group-btn">--}}
{{--                                        <button class="btn btn-sm default" type="button">--}}
{{--                                            <i class="fa fa-calendar"></i>--}}
{{--                                        </button>--}}
{{--                                    </span>--}}
{{--                            </div>--}}
{{--                        </td>--}}
{{--                        <td>--}}
{{--                            <div class="margin-bottom-5">--}}
{{--                                <button class="btn btn-info-alt filter-submit">--}}
{{--                                    <i class="fa fa-search"></i> {{trans('common.filter')}}--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                            <button class="btn btn-danger-alt filter-cancel">--}}
{{--                                <i class="fa fa-times"></i> {{trans('common.reset')}}--}}
{{--                            </button>--}}
{{--                            <button type="submit" class="btn btn-primary">Bulk Delete</button>--}}
{{--                        </td>--}}
{{--                    </tr>--}}
{{--                    </thead>--}}
{{--                </table>--}}
{{--                </form>--}}
                <form action="{{route('backend.skillkeyword')}}">
                <div class="row">
                    <div class="col-md-3">
                        <input type="text" name="key_word_name" placeholder="search using keyword name" class="form-control" />
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="username" placeholder="search using username" class="form-control" />
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-success">submit</button>
                        <button class="btn btn-danger">reset</button>
                    </div>
                </div>
                <br>
                </form>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Username</th>
                            <th>Name Cn</th>
                            <th>Name En</th>
                            <th>Reference Link</th>
                            <th>Created At</th>
                            <th>Actions</th>
                            <th>
                                <form action="{{route('backend.skillkeyword.bulkDelete')}}">
                                    <input type="hidden" name="rows[]" id="rows" />
                                    <button onclick="bulk_delete()" class="btn btn-primary" type="submit">Bulk Delete</button>
                                </form>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($modal as $data)
                            <tr>
                                <td>{{$data->id}}</td>
                                <td>{{isset($data->user) ? $data->user->name : '--'}}</td>
                                <td>{{isset($data->name_cn) ? $data->name_cn : '--'}}</td>
                                <td>{{isset($data->name_en) ? $data->name_en : '--'}}</td>
                                <td><a href='{{$data->reference_link}}' target='_blank'> 链接 </a></td>
                                <td>{{$data->created_at}}</td>
                                <td>
                                    <a tooltip="check circle keyword" data-toggle="modal"  data-target="#remote-modal" href="{{route('backend.skillkeyword.approve', ['id' => $data->id])}}"><i class="fa fa-check-circle"></i></a>

                                    <a tooltip="delete keyword" href="{{route('backend.skillkeyword.delete', ['id' => $data->id])}}"><i class="fa fa-trash"></i></a>

                                    <a tooltip="ban keyword" href="{{route('backend.skillkeyword.ban', ['id' => $data->user_id])}}"><i class="fa fa-user-times"></i></a>
                                </td>
                                <td>
                                    <input type="checkbox" id="checkValue-{{$data->id}}" onclick="bulkDelete('{{$data->id}}')" name="rows[]" value="{{$data->id}}" />
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="row">
                    @include('backend.includes.paginate', ['paginator' => $modal])
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection
