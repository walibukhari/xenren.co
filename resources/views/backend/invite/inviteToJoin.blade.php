@extends('backend.layout')

@section('title')
    {{ trans('sideMenu.inbox') }}
@endsection

@section('description')
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('officialprojects.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.inbox') }}">{{trans('sideMenu.inbox')}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <link href="/assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/backend/inbox.css" rel="stylesheet" type="text/css" />
@endsection

@section('footer')
    <script src="/assets/global/plugins/fancybox/source/jquery.fancybox.js" type="text/javascript"></script>
    <script>
        var url = {
            'trashAll': "{{ route('backend.inbox.trashall') }}",
            'untrashAll': "{{ route('backend.inbox.untrashall') }}",
            'markAsRead': "{{ route('backend.inbox.markasread') }}",
            'removeAll': "{{ route('backend.inbox.removeall') }}"
        }
        $(document).ready(function () {
            $('.chkAllItemTick').on('click', function () {
                if ($(this)[0].checked) {
                    $('.chkItemTick').each(function () {
                        if( $(this).prop('checked') == false ) {
                            $(this).trigger('click');
                        }
                    });

                    $('.spnAllItemTick').addClass('checked');
                } else {
                    $('.chkItemTick').each(function () {

                        if( $(this).prop('checked') == true ) {
                            $(this).trigger('click');
                        }
                    });

                    $('.spnAllItemTick').removeClass('checked');
                }
            });

            $('.chkItemTick').on('click', function () {
                var id = $(this).val();
                if( $(this).prop('checked') == true ) {
                    $('.spnItemTick-' + id ).addClass('checked');
                } else {
                    $('.spnItemTick-' + id ).removeClass('checked');
                }
            });

            $('#slcActions').on('change', function(e){

                var data = { 'inboxIds[]' : []};
                $(".chkItemTick:checked").each(function() {
                    data['inboxIds[]'].push($(this).val());
                });

                if( $(this).val() == '1' )
                {
                    //mark as read
                    $.ajax({
                        url: url.markAsRead,
                        dataType: 'json',
                        data: data,
                        method: 'POST',
                        error: function () {
                            alert(lang.unable_to_request);
                        },
                        success: function (result) {
                            if(result.status == 'success') {
                                alertSuccess(result.msg);

                                $(".chkItemTick:checked").each(function() {
                                    $('#inbox-' + $(this).val()).removeClass('striped');
                                });
                            }else{
                                alertError(result.msg);
                            }
                        }
                    });
                }
                else if( $(this).val() == '2' )
                {
                    //move to trash
                    $.ajax({
                        url: url.trashAll,
                        dataType: 'json',
                        data: data,
                        method: 'POST',
                        error: function () {
                            alert(lang.unable_to_request);
                        },
                        success: function (result) {
                            if(result.status == 'success') {
                                alertSuccess(result.msg);

                                $(".chkItemTick:checked").each(function() {
                                    $('#inbox-' + $(this).val()).hide();
                                });
                            }else{
                                alertError(result.msg);
                            }
                        }
                    });
                }
                else if( $(this).val() == '3' )
                {
                    //cancel trash and move back to inbox
                    $.ajax({
                        url: url.untrashAll,
                        dataType: 'json',
                        data: data,
                        method: 'POST',
                        error: function () {
                            alert(lang.unable_to_request);
                        },
                        success: function (result) {
                            if(result.status == 'success') {
                                alertSuccess(result.msg);

                                $(".chkItemTick:checked").each(function() {
                                    $('#inbox-' + $(this).val()).hide();
                                });
                            }else{
                                alertError(result.msg);
                            }
                        }
                    });
                }
                else if( $(this).val() == '4' )
                {
                    //remove inbox and inbox message
                    $.ajax({
                        url: url.removeAll,
                        dataType: 'json',
                        data: data,
                        method: 'POST',
                        error: function () {
                            alert(lang.unable_to_request);
                        },
                        success: function (result) {
                            if(result.status == 'success') {
                                alertSuccess(result.msg);

                                $(".chkItemTick:checked").each(function() {
                                    $('#inbox-' + $(this).val()).hide();
                                });
                            }else{
                                alertError(result.msg);
                            }
                        }
                    });
                }
            });
        });
    </script>

@endsection

@section('content')


    <div class="invite-join">
        <div class="invite">
            <div class="invite-btn">
                <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#inviteModal">
                    Invite
                </button>
            </div>

            <div class="modal fade" id="inviteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Invite User</h4>
                        </div>
                        <div class="modal-body invite-modal">
                            <div class="form-group">
                                <label for="sel1">Select Option:</label>
                                <select class="form-control" id="inviteOption">
                                    <option disabled selected="selected">Please Select the Option</option>
                                    <option value="generate_link">Generate Link</option>
                                    <option value="bar_code">BarCode</option>
                                </select>
                            </div>
                            <div class="output">
                                <div id="generate_link" class="output-inner generateLink" style="display: none;">

                                    <div class="form-group">
                                        <input type="text" class="form-control" readonly="readonly" value="{{URL::to('/')}}/inviteFriend/{{$userId = Auth::id()}}" id="copyLInk">
                                    </div>

                                    <button class="btn btn-primary" onclick="linkCopy()">Copy Link</button>


                                </div>
                                <div id="bar_code" class="output-inner barCode" style="display: none;">
                                    <a href="https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=http://localhost:8000/logout?inviteCode=136?type=qr">
                                        <img src="https://api.qrserver.com/v1/create-qr-code/?size=160x170&data=http://localhost:8000/logout?inviteCode=136?type=qr"/>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-condensed">
            <thead>
            <tr>
                <th scope="col">User #</th>
                <th scope="col">Upline</th>
                <th scope="col">Type</th>
                <th scope="col">Status</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $key=>$val)
            <tr>
                <td>{{$val->user_id}}</td>
                <td>{{$val->upline_id}}</td>
                <td>{{$val->type}}</td>
                <td>{{$val->status}}</td>
            </tr>
                @endforeach
            </tbody>
        </table>
    </div>





@endsection

@section('modal')

    <script>
        $(function() {
            $('#inviteOption').change(function(){

                $('.output-inner').hide();
                $('#' + $(this).val()).show();
            });
        });
        function linkCopy() {
            var copyText = document.getElementById("copyLInk");
            copyText.select();
            document.execCommand("copy");
            toastr.success('Successfully Copied the link');
            $('#inviteModal').modal('toggle');

        }

    </script>

@endsection

