@extends('backend.layout')

@section('title')
    {{trans('sideMenu.会员')}}
@endsection

@section('description')
    {{trans('user.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('order.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.withdraw') }}"><?= Lang::get('member.withdraw'); ?></a>
        </li>
    </ul>
@endsection

@section('header')

@endsection

@section('footer')
    <script>
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#withdraw-dt'),
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('backend.withdraw.dt') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'email', name: 'email'},
                            {data: 'bank_account_no', name: 'bank_account_no'},
                            {data: 'bank_name', name: 'bank_name'},
                            {data: 'bank_address', name: 'bank_address'},
                            {data: 'amount', name: 'amount'},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false}
                        ],
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"><?= Lang::get('member.withdraw'); ?></span>
            </div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="withdraw-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th>ID</th>
                        <th width="200">{{trans('user.电子邮件地址')}}</th>
                        <th><?= Lang::get('member.bank_card_no'); ?></th>
                        <th><?= Lang::get('member.bank_name'); ?></th>
                        <th><?= Lang::get('member.bank_address'); ?></th>
                        <th><?= Lang::get('member.amount'); ?></th>
                        <th>{{trans('common.created')}}</th>
                        <th>{{trans('common.actions')}}</th>
                    </tr>
                    <tr role="row" class="filter">
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_id"
                                   placeholder="{{trans('common.actions')}}"/>
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_email"
                                   placeholder="{{trans('user.电子邮件地址')}}">
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <div class="input-group margin-bottom-5">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_created_after" placeholder="{{trans('skill.开始')}}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                            <div class="input-group">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_created_before" placeholder="{{trans('skill.结束')}}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                        </td>
                        <td>
                            <div class="margin-bottom-5">
                                <button class="btn btn-info-alt filter-submit">
                                    <i class="fa fa-search"></i> {{trans('skill.过滤')}}
                                </button>
                            </div>
                            <button class="btn btn-danger-alt filter-cancel">
                                <i class="fa fa-times"></i> {{trans('skill.重置')}}
                            </button>
                        </td>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection