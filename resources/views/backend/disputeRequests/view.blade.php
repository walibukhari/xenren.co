@extends('backend.layout')

@section('title')
    {{trans('sideMenu.dispute_requests')}}
@endsection

@section('description')

@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('bank.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.disputes') }}">{{trans('sideMenu.dispute_requests')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="javascript:;">{{trans('sideMenu.dispute_requests')}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <link href="/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/jquery-multi-select/css/multi-select.css" rel="stylesheet" type="text/css" />
@endsection

@section('footer')
    <script src="/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
    <script>
			+function() {
				$(document).ready(function() {
					$('#permissions-selector').multiSelect({
						selectableHeader: '<span class="caption font-red-sunglo">{{trans('permissions.未选择')}}</span>',
						selectionHeader: '<span class="caption font-green-sharp">{{trans('permissions.已选择')}}</span>',
					});
					$('#permission-form').makeAjaxForm({
						redirectTo: '{{ route('backend.permissions') }}',
					});
				});
			}(jQuery);
    </script>
    <style>
        .upload-box {

            width: 120px;
            background-color: #F0F0F0;
            font-size: 20px;
            color: #58AF2A;
            border-radius: 5px;
            text-align: center;
            padding: 23px 0px;
            float: left;
            margin-right: 12px;

        }
        .uploadImage {
            background-color: lightgrey;
            height: 80px;
            border-radius: 4px;
        }
        .plusIcon {
            display: block !important;
            margin: 0 auto;
            border: 1px solid;
            width: 20px;
            padding: 3px 3px 3px 3px;
            border-radius: 50%;
            color: #61AF2C;
            margin-top: 25px;
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">

            <div class="">
                <label for="">Comment</label>
                <textarea name="admin_comment" id="admin_comment" class="form-control admin_comment"></textarea><br/>
                @if($dispute->status != \App\Models\ProjectFileDisputes::STATUS_FILE_DISPUTE_IN_APPROVED)
                    <input type="button" class="btn btn-success approveDispute" data-disputeid="{{$dispute->id}}" value="Approve">
                @endif
                @if($dispute->status != \App\Models\ProjectFileDisputes::STATUS_FILE_DISPUTE_IN_APPROVED)
                    <input type="button" class="btn btn-danger rejectDispute" data-disputeid="{{$dispute->id}}" value="Reject">
                @endif
            </div>
        </div>
        <div class="portlet-body form">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-2">
                        <img src="{{$sender->img_avatar}}" onerror="this.src='/images/avatar_xenren.png'" style="width: 100px"/>
                    </div>
                    <div class="col-md-8">
                        <p><b>{{$sender->real_name}}</b></p>
                        <p>{{isset($senderComment->comment) ? $senderComment->comment : 'N/A'}}</p>
                        <p><b>Attachments: </b></p>
                        <div class="col-md-12">
                            @foreach($senderAttachments as $k => $v)
                                <div class="upload-box">
                                    <i class="fa fa-search" style="cursor: pointer" aria-hidden="true" onclick="window.open('{{asset('uploads/disputes/'.$v->attachment)}}')"></i>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="row">
                        <input type="button" class="btn btn-primary" onclick="window.location='/admin/dispute/{{$dispute->id}}/history/{{$sender->id}}/view'" value="History">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <img src="{{$receiver->img_avatar}}" onerror="this.src='/images/avatar_xenren.png'" style="width: 100px"/>
                    </div>
                    <div class="col-md-8">
                        <br/>
                        <p><b>{{$receiver->real_name}}</b></p>
                        <p>{{isset($receiverComment->comment) ? $receiverComment->comment : 'N/A'}}</p>
                        <p><b>Attachments: </b></p>
                        <div class="col-md-12">
                            @foreach($receiverAttachments as $k => $v)
                                <div class="upload-box">
                                    <i class="fa fa-search" style="cursor: pointer" aria-hidden="true" onclick="window.open('{{asset('uploads/disputes/'.$v->attachment)}}')"></i>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="row">
                        <input type="button" class="btn btn-primary" value="History" onclick="window.location='/admin/dispute/{{$dispute->id}}/history/{{$receiver->id}}/view'" >
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <div class="modal fade addComment" id="" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body confirm-box-model-contant" style="background-color: white">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <img src="/images/icons/questionMarkIcon.png">
                        </div>
                        <div class="col-md-12 text-center">
                            <h1>File Dispute Update</h1>
                        </div>
                        <div class="col-md-12 released-milestone">
                            <form>
                                <div class="form-group">
                                    <textarea class="form-control usercomment" rows="5" id="" placeholder="Write down all the reasons"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6 confirm-box-model-btn-section">
                            <div class="row text-center">
                                <div class="col-md-6">
                                    <button class="btn btn-success btn-green-bg-white-text btn-block updateComment" type="button">Approve</button>
                                </div>
                                <div class="col-md-6">
                                    <button class="btn btn-success btn-white-bg-green-text btn-block" type="button"  data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
			$(function () {
				$('.approveDispute').on('click', function(){
					var form = new FormData();
					form.append("dispute_id", $(this).data('disputeid'));
					form.append("admin_comment", $('.admin_comment').val());
					form.append("_token", '{{csrf_token()}}');
					$.ajax({
						url: '/admin/approve-dispute-admin',
						"data": form,
						"headers": {
							"Cache-Control": "no-cache"
						},
						"processData": false,
						"contentType": false,
						"mimeType": "multipart/form-data",
						method: 'POST',
						error: function () {
//							alert(lang.unable_to_request);
						},
						success: function (result) {
							result = JSON.parse(result);
							console.log(result);
							if(result.status == 'success') {
								alertSuccess(result.message);
								setTimeout(function(){
									window.location = '/admin/disputes'
								}, 3000)
							}else{
								alertError(result.message);
							}
						}
					});
				});
				$('.rejectDispute').on('click', function(){
					var form = new FormData();
					form.append("dispute_id", $(this).data('disputeid'));
					form.append("admin_comment", $('.admin_comment').val());
					form.append("_token", '{{csrf_token()}}');
					$.ajax({
						url: '/admin/reject-dispute-admin',
						"data": form,
						"headers": {
							"Cache-Control": "no-cache"
						},
						"processData": false,
						"contentType": false,
						"mimeType": "multipart/form-data",
						method: 'POST',
						error: function () {
//							alert(lang.unable_to_request);
						},
						success: function (result) {
							result = JSON.parse(result);
							console.log(result);
							if(result.status == 'success') {
								alertSuccess(result.message);
								setTimeout(function(){
									window.location = '/admin/disputes'
								}, 3000)
							}else{
								alertError(result.message);
							}
						}
					});
				});
			})
    </script>
@endsection
@section('backend.footscript')

@endsection