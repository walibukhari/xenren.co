@extends('backend.layout')

@section('title')
    {{trans('sideMenu.dispute_requests')}}
@endsection

@section('description')
    {{trans('permissions.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('bank.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.disputes') }}">{{trans('sideMenu.dispute_requests')}}</a>
        </li>
    </ul>
@endsection

@section('header')

@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('sideMenu.dispute_requests')}}</span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="permissions-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th>#</th>
                        <th>Sender</th>
                        <th>Receiver</th>
                        <th>Project</th>
                        <th>Milestone</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    @foreach($disputes as $k => $v)
                    @php
                    if(isset($v->transaction)) {
                        $sender_real_name = isset($v->transaction->sender->name) ? $v->transaction->sender->real_name : '';
                        $receiver_real_name = isset($v->transaction->receiver->name) ? $v->transaction->receiver->real_name : '';
                        $milestone_project = isset($v->transaction->milestone->project->name) ? $v->transaction->milestone->project->name : '';
                        $milestone_name = isset($v->transaction->milestone->name) ? $v->transaction->milestone->name . '<i> (Partial Milestone Request)</i> ': '';
                    }else {
                        $sender_real_name = isset($v->milestones->project->creator->name) ? $v->milestones->project->creator->real_name : '';
                        $receiver_real_name = isset($v->milestones->applicant->name) ? $v->milestones->applicant->real_name : '';
                        $milestone_project = isset($v->milestones->project->name) ? $v->milestones->project->name : '';
                        $milestone_name = isset($v->milestones->name) ? $v->milestones->name : '';
                        }
                    @endphp
                        <tr role="row" class="filter">
                            <td> {{ $v->id }} </td>
                            <td> {!! $sender_real_name !!} </td>
                            <td> {!! $receiver_real_name !!} </td>
                            <td> {!! $milestone_project !!} </td>
                            <td> {!! $milestone_name !!} </td>
                            <td> {!! $v->status_name !!} </td>
                            <td><a href="{{route('backend.disputes.view', $v->id)}}" class="fa fa-eye"></a> </td>
                        </tr>
                    @endforeach
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection