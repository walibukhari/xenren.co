@extends('backend.layout')

@section('title')
    {{trans('sideMenu.dispute_requests')}}
@endsection

@section('description')

@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('bank.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.disputes') }}">{{trans('sideMenu.dispute_requests')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="javascript:;">{{trans('sideMenu.dispute_requests')}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <link href="/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/jquery-multi-select/css/multi-select.css" rel="stylesheet" type="text/css" />
@endsection

@section('footer')
    <script src="/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
    <script>
			+function() {
				$(document).ready(function() {
					$('#permissions-selector').multiSelect({
						selectableHeader: '<span class="caption font-red-sunglo">{{trans('permissions.未选择')}}</span>',
						selectionHeader: '<span class="caption font-green-sharp">{{trans('permissions.已选择')}}</span>',
					});
					$('#permission-form').makeAjaxForm({
						redirectTo: '{{ route('backend.permissions') }}',
					});
				});
			}(jQuery);
    </script>
    <style>
        .upload-box {

            width: 120px;
            background-color: #F0F0F0;
            font-size: 20px;
            color: #58AF2A;
            border-radius: 5px;
            text-align: center;
            padding: 23px 0px;
            float: left;
            margin-right: 12px;

        }
        .uploadImage {
            background-color: lightgrey;
            height: 80px;
            border-radius: 4px;
        }
        .plusIcon {
            display: block !important;
            margin: 0 auto;
            border: 1px solid;
            width: 20px;
            padding: 3px 3px 3px 3px;
            border-radius: 50%;
            color: #61AF2C;
            margin-top: 25px;
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">

            <div class="actions">

            </div>
        </div>
        <div class="portlet-body form">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-2">
                        <img src="{{$user->img_avatar}}" onerror="this.src='/images/avatar_xenren.png'" style="width: 100px"/>
                    </div>
                    <div class="col-md-8">
                        <br/>
                        <p><b>{{$user->name}}</b></p>
                        <p>
                            <b>Deleted Comments</b>
                        </p>
                        @php
                            $count = 1;
                        @endphp
                        @if(count($comments) < 1)
                            <p>N/A</p>
                        @endif
                        @foreach($comments as $k => $v)

                            <p><b>{{$count}}</b>: {{$v->comment}}</p>
                            @php
                                $count++;
                            @endphp
                        @endforeach
                        <p>
                            <b>Deleted Attachments</b>
                        </p>
                        @php
                            $count = 1;
                        @endphp

                        @if(count($attachments) < 1)
                            <p>N/A</p>
                        @endif
                        @foreach($attachments as $k => $v)

                            <p>
                                <div class="upload-box">
                                    <i class="fa fa-search" style="cursor: pointer" aria-hidden="true" onclick="window.open('{{asset('uploads/disputes/'.$v->attachment)}}')"></i>
                                </div>
                            </p>
                            @php
                                $count++;
                            @endphp
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection