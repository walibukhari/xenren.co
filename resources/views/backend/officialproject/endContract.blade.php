@extends('ajaxmodal')

@section('title')
    {{trans("common.end_contract")}}
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rating/1.4.0/bootstrap-rating.min.js"></script>
    <script>
        +function () {
            $(document).ready(function () {
                var lang = {
                    'choose_max_three_skills': "{{ trans('common.choose_max_three_skills') }}"
                }

                $('#end-contract-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit'
                });

                function add(a, b) {
                    return parseInt(a) + parseInt(b);
                }

                $(".rating.feedback").on('change', function(){
                    var values = [];
                    $(".rating.feedback").each(function() {
                        values.push($(this).val());
                    });
                    var sum = (values.reduce(add,0));
                    var avgRating = (sum/6).toFixed(2);
                    $("#ratings-total").html(avgRating);
                    $(".total_score").val(avgRating);
                });

                $('label[id^="lbl-skill-tag-"]').click(function(){
                    var skillId = parseInt(this.id.replace("lbl-skill-tag-", ""));
                    var existingReviewSkills = $('.review_skills').val();
                    if( existingReviewSkills == "")
                    {
                        var newReviewSkills = [];
                    }
                    else
                    {
                        var newReviewSkills = existingReviewSkills.split(',');
                    }

                    if( $(this).hasClass('label-default'))
                    {
                        if( newReviewSkills.length >= 3)
                        {
                            alertError(lang.choose_max_three_skills);
                            return false;
                        }

                        $(this).removeClass('label-default').addClass('label-color');
                        newReviewSkills.push(skillId);
                    }
                    else
                    {
                        $(this).removeClass('label-color').addClass('label-default');
                        var index = newReviewSkills.indexOf('' + skillId + '');
                        if (index > -1) {
                            newReviewSkills.splice(index, 1);
                        }
                    }
                    $('.review_skills').val(newReviewSkills);

                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit" class="btn green">{{ trans('common.end_contract') }}</button>
@endsection

@section('content')
    {!! Form::open(['route' => 'backend.officialproject.applicant.endcontract.post', 'method' => 'post', 'role' => 'form', 'id' => 'end-contract-form']) !!}
        <input type="hidden" name="projectId" value="{{ $projectApplicant->project->id }}">
        <input type="hidden" name="projectApplicantId" value="{{ $projectApplicant->id }}">
        <div class="form-body">
            <div class="row text-center">
                <img src="{{ $projectApplicant->user->getAvatar() }}"
                     alt="" class="profile-picture img-circle" width="80px" height="80px">
            </div>

            <div class="row">
                <div class="col-md-3">
                    <label>{{trans("common.username")}} : </label>
                </div>
                <div class="col-md-9">
                    <label>{{ $projectApplicant->user->getName() }}</label>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <label>{{trans("common.job_position")}} : </label>
                </div>
                <div class="col-md-9">
                    <label>{{  $projectApplicant->user->getJobPosition() }}</label>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <label>{{trans("common.quote_price")}} : </label>
                </div>
                <div class="col-md-9">
                    <label>USD {{ $projectApplicant->quote_price }} [ {{ $projectApplicant->getPayType() }} ]</label>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <label>{{trans("common.about_me")}} : </label>
                </div>
                <div class="col-md-9">
                    <label>{{ $projectApplicant->user->getAboutMe() }}</label>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <label>{{trans("common.project_name")}} : </label>
                </div>
                <div class="col-md-9">
                    <label>{{ $projectApplicant->project->name }}</label>
                </div>
            </div>
            <br/>

            <div class="form-group row">
                <label class="control-label col-sm-3">
                    {{ trans('common.reason_for_ending_contract') }}
                </label>
                <div class="col-sm-9">
                    <select class="form-control" name="reason">
                       <option value="">{{ trans('common.please_select') }}</option>
                       @foreach( $reasons as $key => $reason )
                       <option value="{{ $key }}">{{ $reason }}</option>
                       @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label class="control-label col-sm-3">
                    {{ trans('common.how_likely_to_recommend', ['who' => trans('common.freelancer')]) }}
                </label>
                <div class="col-sm-2 text-center">{{ trans('common.not_at_all_likely') }}</div>
                <div class="col-sm-5 text-center">
                    <label>1<input type="radio" class="recommendation" name="recommendation" value="1"></label>
                    <label>2<input type="radio" class="recommendation" name="recommendation" value="2"></label>
                    <label>3<input type="radio" class="recommendation" name="recommendation" value="3"></label>
                    <label>4<input type="radio" class="recommendation" name="recommendation" value="4"></label>
                    <label>5<input type="radio" class="recommendation" name="recommendation" value="5"></label>
                    <label>6<input type="radio" class="recommendation" name="recommendation" value="6" checked="checked"></label>
                    <label>7<input type="radio" class="recommendation" name="recommendation" value="7"></label>
                    <label>8<input type="radio" class="recommendation" name="recommendation" value="8"></label>
                    <label>9<input type="radio" class="recommendation" name="recommendation" value="9"></label>
                    <label>10<input type="radio" class="recommendation" name="recommendation" value="10"></label>
                </div>
                <div class="col-sm-2 text-center">{{ trans('common.extremely_likely') }}</div>
            </div>

            <div class="form-group row">
                <label class="control-label col-sm-3">
                    {{ trans('common.rate_their_english') }}
                </label>
                <div class="col-sm-9">
                    @foreach( $englishProficiencies as $key => $englishProficiency )
                    <div class="radios">
                        <input type="radio" class="proficiency" name="english_proficiency" value="{{ $key }}">
                        <label for="radio1">{{ $englishProficiency }}</label>
                    </div>
                    @endforeach
                </div>
            </div>

            <div class="form-group row form-field">
                <label class="control-label col-sm-3">
                    {{ trans('common.feedback_to', [ 'who' => trans('common.freelancer')]) }}
                </label>
                <div class="col-sm-5">
                    <div class=" ratting-custom">
                        @foreach( $rateCategories as $key => $rateCategory )
                        <div class="ratting">
                            <input type="hidden" class="rating feedback" value="3" name="rate_type_{{ $key }}" value="1" data-filled="fa fa-star" data-empty="fa fa-star star-o"/>
                            <span>{{ $rateCategory }} </span>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="total uppercase">
                        {{ trans('common.total_score') }}:
                        <h1 id="ratings-total">3.00</h1>
                        <input type="hidden" class="total_score" name="total_score" value="3.00">
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label class="control-label col-sm-3">
                    {{ trans('common.skill_freelancer_most_experience') }}:
                </label>
                <div class="col-sm-9">
                    <div class="label-custom">
                        @foreach( $projectSkills as $projectSkill)
                        <label id="lbl-skill-tag-{{ $projectSkill->skill->id }}" class="label-default">
                            <span>
                                {{ $projectSkill->skill->getName() }}
                            </span>
                        </label>
                        @endforeach
                        <input class="review_skills" type="hidden" name="review_skills" value="">
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label class="control-label col-sm-3">
                    {{ trans('common.share_your_experience', [ 'who' => trans('common.freelancer') ]) }}
                </label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="experience"></textarea>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection