@extends('ajaxmodal')

@section('title')
    {{trans("common.send_message")}}
@endsection

@section('script')
    <script>
        +function () {
            $(document).ready(function () {
                $('#send-message-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit'
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit" class="btn green">{{ trans('common.send_message') }}</button>
@endsection

@section('content')
    {!! Form::open(['route' => ['backend.officialproject.applicant.sendmessage.post', 'applicantid' => $projectApplicant->id], 'method' => 'post', 'role' => 'form', 'id' => 'send-message-form']) !!}
    <div class="form-body">

        <div class="row">
            <div class="col-md-12">
                <label>{{ trans('common.message') }}</label>
                <div class="input-group">
                    {{ Form::textarea('message', null, ['class' => 'form-control', 'cols' => '100']) }}
                </div>
            </div>
        </div>

    </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection