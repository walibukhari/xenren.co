@extends('backend.layout')

@section('title')
    {{trans('officialprojects.官方项目')}}
@endsection

@section('description')
    {{trans('officialprojects.修改-官方项目')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;"> {{trans('officialprojects.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.officialproject') }}">{{trans('officialprojects.官方项目')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="javascript:;">{{trans('officialprojects.修改官方项目')}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <style>
        #map-container { width: 100%; min-height: 360px; }
    </style>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title tabbable-line">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase">{{trans('officialprojects.官方项目')}} </span>
            </div>
            <div class="actions">

            </div>
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab-content" data-toggle="tab"> {{trans('officialprojects.内容')}}</a>
                </li>
                <li>
                    <a href="#tab-image" data-toggle="tab"> {{trans('officialprojects.图集')}}</a>
                </li>
            </ul>
        </div>
        <div class="portlet-body form">
            {!! Form::open(['route' => ['backend.officialproject.edit.post', 'id' => $model->id], 'method' => 'post', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'officialproject-form', 'files' => true]) !!}
            <div class="tab-content">
                <div class="tab-pane active" id="tab-content">
                    <div class="form-body">
                        {{--<div class="form-group">--}}
                        {{--<label class="control-label col-sm-3">{{trans('officialprojects.项目语言')}}</label>--}}
                        {{--<div class="col-sm-9">--}}
                        {{--{!! Form::select('lang', \App\Constants::getLangs(), $model->lang, ['class' => 'form-control']) !!}--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans('officialprojects.项目标题')}}</label>
                            <div class="col-sm-9">
                                {!! Form::text('title', $model->title, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans('common.country')}}</label>
                            <div class="col-sm-9">
                                <select name="country_id" id="country_id" class="form-control">
                                    @foreach($countries as $k => $v)
                                        <option value="{{$v->d}}">{{$v->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans('common.language_2')}}</label>
                            <div class="col-sm-9">
                                {!! Form::select('language_id', $languages, $project->language_id, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans('officialprojects.项目简介')}}</label>
                            <div class="col-sm-9">
                                {!! Form::textarea('description', $model->description, ['class' => 'form-control', 'rows' => 4]) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{ trans('common.coin_type') }}</label>
                            <div class="col-sm-9">
                                <select name="coin_type" class="form-control">
                                    <option {{ is_null($project->coinProject) ? 'selected' : '' }} value="">{{ trans('common.select_coin') }}</option>
                                    @foreach ($coinList as $key => $value )
                                        <option 
                                        {{ !is_null($project->coinProject) && $project->coinProject->coin_type == $key ? 'selected' : '' }} 
                                        value="{{ $key }}">
                                        {{ $value['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans('officialprojects.招聘开始时间')}}</label>
                            <div class="col-sm-9">
                                {!! Form::text('recruit_start', $model->recruit_start, ['class' => 'form-control datetime-picker']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans('officialprojects.招聘结束时间')}}</label>
                            <div class="col-sm-9">
                                {!! Form::text('recruit_end', $model->recruit_end, ['class' => 'form-control datetime-picker']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans('officialprojects.完成时间')}}</label>
                            <div class="col-sm-9">
                                {!! Form::text('completion', $model->completion, ['class' => 'form-control datetime-picker']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans('officialprojects.办公地点地址')}}</label>
                            <div class="col-sm-9">
                                <body onload="geocoder()"></body>
                                <div id="map-container"></div>
                                <div id="tip">
                                    <span id="result"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">&nbsp;</label>
                            <div class="col-sm-9">
                                <div class="col-xs-11 col-sm-9" style="padding-left: 0;">
                                    {!! Form::text('location_address', $model->location_address, ['class' => 'form-control', 'id' => 'addr']) !!}
                                </div>
                                <div class="col-xs-1 col-sm-3" style="padding-right: 0;">
                                    {!! Form::text('city', $model->city, ['class' => 'form-control', 'id' => 'city', 'placeholder' => trans('common.city'), 'readonly' => 'readonly']) !!}
                                </div>
                                <div>&nbsp;</div>

                                <div class="col-xs-12 col-sm-6" style="padding-left: 0;">
                                    {!! Form::text('location_lat', $model->location_lat, ['class' => 'form-control', 'id' => 'lat', 'readonly' => 'readonly']) !!}
                                </div>
                                <div class="col-xs-12 col-sm-6" style="padding-right: 0;">
                                    {!! Form::text('location_lng', $model->location_lng, ['class' => 'form-control', 'id' => 'lng', 'readonly' => 'readonly']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans('officialprojects.预算范围')}}</label>
                            <div class="col-sm-3">
                                {!! Form::select('budget_switch', [0 => trans("officialprojects.不显示"), 1 => trans("officialprojects.显示")], $model->budget_switch, ['class' => 'form-control', 'id' => 'budget-switch']) !!}
                            </div>
                            <div class="col-sm-3">
                                {!! Form::text('budget_from', $model->budget_from, ['class' => 'form-control fund-input display-none budget-input', 'placeholder' => trans("officialprojects.开始")]) !!}
                            </div>
                            <div class="col-sm-3">
                                {!! Form::text('budget_to', $model->budget_to, ['class' => 'form-control fund-input display-none budget-input', 'placeholder' => trans("officialprojects.结束")]) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans("officialprojects.股份")}}</label>
                            <div class="col-sm-4">
                                {!! Form::text('stock_total', $model->stock_total, ['class' => 'form-control', 'placeholder' => trans("common.total_share_value")]) !!}
                            </div>
                            <div class="col-sm-5">
                                {!! Form::text('stock_share', $model->stock_share, ['class' => 'form-control', 'placeholder' => trans("common.number_share_issued")]) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans("officialprojects.股份追加说明")}}</label>
                            <div class="col-sm-9">
                                {!! Form::textarea('stock_description', $model->stock_description, ['class' => 'form-control', 'placeholder' => trans("officialprojects.股份追加说明")]) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans('officialprojects.资金阶段')}}</label>
                            <div class="col-sm-9">
                                <select name="funding_stage" class="form-control">
                                    @foreach($fundingStages as $key => $fundingStage)
                                        <option value="{{ $key }}" {{ $key == $model->funding_stage?"selected=selected": '' }}>{{ $fundingStage }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans("officialprojects.合同")}}</label>
                            <div class="col-sm-9">
                                {!! Form::file('contract_file', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans("common.icon")}}</label>
                            <div class="col-sm-9">
                                {!! Form::file('icon', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans("common.banner")}}</label>
                            <div class="col-sm-9">
                                {!! Form::file('banner', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans("common.design_type")}}</label>
                            <div class="col-sm-9">
                                {!! Form::select('design_type', $designTypes, $model->design_type,['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans("officialprojects.外主图")}}</label>
                            <div class="col-sm-9">
                                {!! Form::file('out_cover', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans("officialprojects.内主图")}}</label>
                            <div class="col-sm-9">
                                {!! Form::file('in_cover', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans("officialprojects.项目等级")}}</label>
                            <div class="col-sm-9">
                                {!! Form::select('project_level', \App\Constants::getProjectLevels(), $model->project_level, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans("officialprojects.项目类型")}}</label>
                            <div class="col-sm-9">
                                {!! Form::select('project_type', \App\Constants::getProjectTypes(), $model->project_type, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans("officialprojects.分享得分")}}</label>
                            <div class="col-sm-4">
                                {!! Form::select('share_has_reward', [0 => trans("officialprojects.无"), 1 => trans("officialprojects.有")], $model->share_has_reward, ['class' => 'form-control', 'id' => 'share-reward-switch']) !!}
                            </div>
                            <div class="col-sm-5">
                                {!! Form::text('share_reward', $model->share_reward, ['class' => 'form-control fund-input display-none share-reward-input']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans("officialprojects.功勋")}}</label>
                            <div class="col-sm-9">
                                {!! Form::text('reward', $model->reward, ['class' => 'form-control fund-input']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans("common.status")}}</label>
                            <div class="col-sm-9">
                                {!! Form::select('status', [0 => trans('common.draft'), 1 => trans('common.published')], $model->status, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans("officialprojects.技能")}}</label>
                            <div class="col-sm-9">
                                <div class="input-group select2-bootstrap-append">
                                    {!! Form::select('skills[]', $skills, '', ['class' => 'form-control select2-allow-clear', 'multiple' => 'multiple', 'id' => 'skill-selector']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans('officialprojects.weekly_limit')}}</label>
                            <div class="col-sm-9">
                                <input type="text" name="weekly_limit" class="form-control fund-input" value="{{$project->weekly_limit}}">
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{trans("officialprojects.职位")}}</label>
                            <div class="col-sm-9">
                                <div class="table-scrollable">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th> {{trans("officialprojects.职位")}}</th>
                                            <th> {{trans("officialprojects.数量")}} </th>
                                            <th> {{trans("officialprojects.简介")}} </th>
                                            <th> {{trans("officialprojects.操作")}} </th>
                                        </tr>
                                        </thead>
                                        <tbody id="positions-container">
                                        @foreach ($model->projectPositions as $key => $var)
                                            <tr>
                                                <td> {!! Form::select('positions[]', $positions, $var->job_position_id, ['class' => 'form-control']) !!} </td>
                                                <td> {!! Form::text('needed[]', $var->needed, ['class' => 'form-control']) !!} </td>
                                                <td> {!! Form::textarea('position_description[]', $var->description, ['class' => 'form-control']) !!} </td>
                                                <td> <a href="javascript:;" class="btn btn-xs red position-delete"><i class="fa fa-trash"></i></a> </td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td> {!! Form::select('positions[]', $positions, '', ['class' => 'form-control']) !!} </td>
                                            <td> {!! Form::text('needed[]', '', ['class' => 'form-control']) !!} </td>
                                            <td> {!! Form::textarea('position_description[]', '', ['class' => 'form-control']) !!} </td>
                                            <td> <a href="javascript:;" class="btn btn-xs red position-delete"><i class="fa fa-trash"></i></a> </td>
                                        </tr>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colspan="4">
                                                <a href="javascript:;" class="btn blue btn-block"
                                                   id="add-position"> <i
                                                            class="fa fa-plus"></i> {{trans("officialprojects.新增职位")}}
                                                </a>
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab-image">
                    <div class="form-group" id="images-container">
                        @foreach ($model->projectImages as $key => $var)
                            <div class="col-xs-6 col-sm-4" id="image-c-{{ $var->id }}">
                                <div class="fileinput fileinput-exists" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 100%;">
                                        <img src="https://www.placehold.it/502x314/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100%;">
                                        <img src="{{ asset($var->getImage()) }}" />
                                    </div>
                                    <div>
                                        <a href="javascript:;" class="btn red delete-image"
                                           data-imageid="{{ $var->id }}"> {{trans("officialprojects.删除")}} </a>
                                    </div>
                                </div>
                                <div class="clearfix margin-top-10"></div>
                            </div>
                        @endforeach
                        <div class="col-xs-6 col-sm-4">
                            <a href="javascript:;" class="btn blue text-center" id="add-image"><i
                                        class="fa fa-plus"></i> {{trans("officialprojects.新增图")}}</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <button type="button" class="btn btn-primary blue" id="submit-btn"><i
                            class="fa fa-check"></i>{{trans("officialprojects.提交")}}</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript" src="https://webapi.amap.com/maps?v=1.3&key={{ config('app.GAODE_API_KEY') }}&plugin=AMap.Geocoder"></script>
    <script>
			function geocoder() {
				try {
					var geocoder = new AMap.Geocoder({
//                city: "010", //城市，默认：“全国”
						radius: 1000 //范围，默认：500
					});

					//地理编码,返回地理编码结果
					var targetAddress = "{{$model->location_address }}";
					geocoder.getLocation(targetAddress, function (status, result) {
						if (status === 'complete' && result.info === 'OK') {
							geocoder_CallBack(result);
						}
					});
				} catch (e){
					console.log(e);
				}
			}

			function addMarker(i, d) {
				try {
					var marker = new AMap.Marker({
						map: map,
						position: [d.location.getLng(), d.location.getLat()]
					});
					var infoWindow = new AMap.InfoWindow({
						content: d.formattedAddress,
						offset: {x: 0, y: -30}
					});
					marker.on("mouseover", function (e) {
						infoWindow.open(map, marker.getPosition());
					});

					prevMarker = marker;
				} catch (e) {
					console.log(e);
				}
			}

			//地理编码返回结果展示
			function geocoder_CallBack(data) {
				try {
					var resultStr = "";
					//地理编码结果数组
					var geocode = data.geocodes;
					for (var i = 0; i < geocode.length; i++) {
						//拼接输出html
						resultStr += "<span style=\"font-size: 12px;padding:0px 0 4px 2px; border-bottom:1px solid #C1FFC1;\">" +
							"<b>地址</b>：" + geocode[i].formattedAddress + "" +
							"&nbsp;&nbsp;<b>的地理编码结果是:</b><b>&nbsp;&nbsp;&nbsp;&nbsp;坐标</b>：" +
							geocode[i].location.getLng() + ", " + geocode[i].location.getLat() + "" +
							"<b>&nbsp;&nbsp;&nbsp;&nbsp;匹配级别</b>：" + geocode[i].level + "</span>";

						addMarker(i, geocode[i]);

						$('#lng').val(geocode[i].location.getLng());
						$('#lat').val(geocode[i].location.getLat());
						$('#city').val(geocode[i].addressComponent.city);
					}
					map.setFitView();
					document.getElementById("result").innerHTML = resultStr;
				}catch (e) {
					console.log(e);
				}
			}

			+function ($) {
				$(document).ready(function () {
					try {
						var map = new AMap.Map("map-container", {
							resizeEnable: true
						});
						var prevMarker = null;
					}catch (e) {
						console.log(e);
					}
					$('#budget-switch').on('change', function () {
						if ($(this).val() == 1) {
							$('.budget-input').show();
						} else {
							$('.budget-input').hide();
						}
					});
					$('#share-reward-switch').on('change', function () {
						if ($(this).val() == 1) {
							$('.share-reward-input').show();
						} else {
							$('.share-reward-input').hide();
						}
					});
					$('#officialproject-form').makeAjaxForm({
						submitBtn: '#submit-btn',
						redirectTo: '{{ route('backend.officialproject') }}',
					});

					var image_template =
						'<div class="col-xs-6 col-sm-4">' +
						'<div class="fileinput fileinput-new" data-provides="fileinput">' +
						'<div class="fileinput-new thumbnail" style="width: 100%;">' +
						'<img src="https://www.placehold.it/502x314/EFEFEF/AAAAAA&amp;text=no+image" alt="" />' +
						'</div>' +
						'<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100%;"> </div>' +
						'<div>' +
						'<span class="btn default btn-file">' +
						'<span class="fileinput-new"> {{trans("officialprojects.选图")}} </span>' +
						'<span class="fileinput-exists"> {{trans("officialprojects.更改")}} </span>' +
						'<input type="file" name="images[]" />' +
						'</span>' +
						'<a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{trans("officialprojects.删除")}} </a>' +
						'</div>' +
						'</div>' +
						'<div class="clearfix margin-top-10"></div>' +
						'</div>';

					var position_template =
						'<tr>' +
						'<td> {!! Form::select('positions[]', $positions, '', ['class' => 'form-control']) !!} </td>' +
						'<td> {!! Form::text('needed[]', '', ['class' => 'form-control']) !!} </td>' +
						'<td> {!! Form::textarea('position_description[]', '', ['class' => 'form-control']) !!} </td>' +
						'<td> <a href="javascript:;" class="btn btn-xs red position-delete"><i class="fa fa-trash"></i></a> </td>' +
						'</tr>';

					$(document).on('click', '#add-image', function () {
						$('#images-container').prepend(image_template);
					});

					$("#skill-selector").select2({
						allowClear: true,
						placeholder: '{{trans("officialprojects.请选择技能")}}',
						width: null
					});

					var defaultData = [];
					@foreach ($project->projectSkills as $key => $var)
defaultData.push({{ $var->skill_id }});
					@endforeach
$('#skill-selector').select2('val', defaultData);
					$('#skill-selector').trigger('change');

					$(document).on('click', '#add-position', function () {
						$('#positions-container').append(position_template);
					});

					$(document).on('click', '.position-delete', function () {
						$(this).parent('td').parent('tr').remove();
					});

					$(document).on('click', '.delete-image', function () {
						var image_id = $(this).data('imageid');
						var input = '<input type="hidden" name="delete_image[]" value="' + image_id + '" />';

						$('#officialproject-form').append(input);
						$('#image-c-' + image_id).remove();
					});

					$('body').on('blur', 'input[id=addr]', function () {
						if ( prevMarker!= null)
						{
							prevMarker.setMap(null);
						}

						var address = $('#addr').val();

						var geocoder = new AMap.Geocoder({
//                        city: "010", //城市，默认：“全国”
							radius: 1000 //范围，默认：500
						});

						//地理编码,返回地理编码结果
						geocoder.getLocation(address, function(status, result) {
							if (status === 'complete' && result.info === 'OK') {
								geocoder_CallBack(result);
							}
						});
					});
				});
			}(jQuery);
    </script>
@endsection

@section('modal') @endsection