@extends('backend.layout')

@section('title')
    {{ trans('common.official_project_news_management') }}
@endsection

@section('description')
    {{trans('common.official_project_news_crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('officialprojects.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.officialproject') }}">{{trans('officialprojects.官方项目')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.officialproject.news.list', ['project_id' => $project_id]) }}">{{trans('common.official_project_news_management')}}</a>
        </li>
    </ul>
@endsection

@section('header')
@endsection

@section('footer')
    <script>
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#project-dt'),
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('backend.officialproject.news.dt', ['project_id' => $project_id ]) }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'type', name: 'type', orderable: false, searchable: false},
                            {data: 'content', name: 'content'},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false}
                        ],
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('common.official_project_news_list')}}</span>
            </div>
            <div class="actions">
                <a href="{{ route('backend.officialproject.news.create', [ 'project_id' => $project_id ]) }}"
                    class="btn btn-circle red-sunglo btn-sm"
                    data-target="#remote-modal"
                    data-toggle="modal">
                    <i class="fa fa-plus"></i> {{trans('common.add_official_project_news')}}
                </a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="project-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th width="80">{{ trans('common.id') }}</th>
                        <th width="100">{{trans('common.type')}}</th>
                        <th>{{trans('common.content')}}</th>
                        <th width="200">{{trans('common.date')}}</th>
                        <th width="150">{{trans('common.actions')}}</th>
                    </tr>
                    <tr role="row" class="filter">
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_id" placeholder="ID" />
                        </td>
                        <td>
                            {!! Form::select('filter_type', \App\Models\OfficialProjectNews::getTypeList(true), '', ['class' => 'form-control form-filter input-sm']) !!}
                        </td>
                        <td>
                            {{--<input type="text" class="form-control form-filter input-sm" name="filter_content"--}}
                                   {{--placeholder="{{trans('common.content')}}">--}}
                        </td>
                        <td>
                            <div class="input-group margin-bottom-5">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_created_after" placeholder="{{trans('common.start')}}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                            <div class="input-group">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_created_before" placeholder="{{trans('common.end')}}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                        </td>
                        <td>
                            <div class="margin-bottom-5">
                                <button class="btn btn-info-alt filter-submit">
                                    <i class="fa fa-search"></i> {{trans('common.filter')}}
                                </button>
                            </div>
                            <button class="btn btn-danger-alt filter-cancel">
                                <i class="fa fa-times"></i> {{trans('common.reset')}}
                            </button>
                        </td>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection