@extends('ajaxmodal')

@section('title')
    {{trans('common.add_official_project_news')}}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                $('#official-project-news-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit'
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit" class="btn btn-primary blue">{{trans("common.add")}}</button>
@endsection

@section('content')
    {!! Form::open(['route' => ['backend.officialproject.news.create.post'], 'method' => 'post', 'role' => 'form', 'id' => 'official-project-news-form']) !!}
        {{ Form::hidden('project_id', $project_id) }}
        <div class="form-body">
            <div class="form-group">
                <label>{{trans('common.type')}}</label>
                <div>
                    {!! Form::select('type', $types, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>{{trans('common.content')}}</label>
                <div>
                    {!! Form::textarea('content', '', ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection