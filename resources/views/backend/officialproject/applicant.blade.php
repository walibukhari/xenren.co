@extends('backend.layout')

@section('title')
    {{ trans('common.official_project_applicants_management') }}
@endsection

@section('description')
    {{ trans('common.project_name') }}: {{ $officialProject->title }}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('officialprojects.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.officialproject') }}">
                {{ trans('common.official_project_applicants_management') }}
            </a>
        </li>
    </ul>
@endsection

@section('header')
    <link href="/custom/css/backend/endContract.css" rel="stylesheet" type="text/css"/>
@endsection

@section('footer')
    <script>
        +function () {
            $(document).ready(function () {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#project-dt'),
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('backend.officialproject.applicant.dt', $officialProject->id) }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'username', name: 'username'},
                            {data: 'quote_price', name: 'quote_price'},
                            {data: 'pay_type', name: 'pay_type'},
                            {data: 'status', name: 'status'},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false}
                        ],
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{ trans('common.official_project_applicants_list') }}</span>
            </div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="project-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th width="80">{{ trans('common.id') }}</th>
                        <th width="150">{{ trans('common.username') }}</th>
                        <th width="150">{{ trans('common.price') }}</th>
                        <th width="150">{{ trans('common.pay_type') }}</th>
                        <th width="150">{{ trans('common.status') }}</th>
                        <th width="180">{{ trans('common.created_at') }}</th>
                        <th width="150">{{trans('common.action')}}</th>
                    </tr>
                    <tr role="row" class="filter">
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_id"
                                   placeholder="{{ trans('common.id') }}"/>
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_username"
                                   placeholder="{{ trans('common.username') }}">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_quote_price"
                                   placeholder="{{ trans('common.quote_price') }}">
                        </td>
                        <td>
                            {!! Form::select('filter_pay_type',
                                    [0 => trans('common.all'), 1 => trans('common.pay_hourly'), 2 => trans('common.fixed_price')],
                                    0, ['class' => 'form-control form-filter input-sm',
                                    'placeholder' => trans('common.pay_type')]) !!}

                        </td>
                        <td>
                            {!! Form::select('filter_status',
                                    [
                                        -1 => trans('common.all'),
                                        2 => trans('common.apply'),
                                        3 => trans('common.creator_selected'),
                                        4 => trans('common.project_creator_reject'),
                                        5 => trans('common.applicant_accept'),
                                        6 => trans('common.applicant_reject'),
                                        7 => trans('common.completed'),
                                    ],
                                    -1, ['class' => 'form-control form-filter input-sm']) !!}

                        </td>
                        <td>
                            <div class="input-group margin-bottom-5">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_created_after" placeholder="{{trans('officialprojects.开始')}}">
                                <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                            <div class="input-group">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_created_before" placeholder="{{trans('officialprojects.结束')}}">
                                <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                        </td>
                        <td>
                            <div class="margin-bottom-5">
                                <button class="btn btn-info-alt filter-submit">
                                    <i class="fa fa-search"></i> {{trans('officialprojects.过滤')}}
                                </button>
                            </div>
                            <button class="btn btn-danger-alt filter-cancel">
                                <i class="fa fa-times"></i> {{trans('officialprojects.重置')}}
                            </button>
                        </td>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection