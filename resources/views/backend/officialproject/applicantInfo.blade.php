@extends('ajaxmodal')

@section('title')
    {{trans("common.more_details")}}
@endsection

@section('script')
    <script>
        +function () {
            $(document).ready(function () {
            });
        }(jQuery);
    </script>
@endsection

@section('footer')

    <button type="button" class="btn blue"
            data-href="{{ route('backend.officialproject.applicant.awardjob', ['applicantid' => $projectApplicant->id]) }}"
            data-redirect="no"
            data-toggle="modal"
            data-target="#confirm-modal"
            data-header="{{ trans('common.award_job') }}"
            data-body="{{ trans('common.are_you_sure') }}">
        {{trans("common.award_job")}}
    </button>

    <button type="button" class="btn red"
            data-href="{{ route('backend.officialproject.applicant.reject', ['applicantid' => $projectApplicant->id]) }}"
            data-redirect="no"
            data-toggle="modal"
            data-target="#confirm-modal"
            data-header="{{ trans('common.reject') }}"
            data-body="{{ trans('common.are_you_sure') }}">
        {{trans("common.reject")}}
    </button>

    <button type="button" class="btn yellow" id="btn-send-message"
            href="{{ route('backend.officialproject.applicant.sendmessage', ['applicantid' => $projectApplicant->id]) }}"
            data-toggle="modal"
            data-target="#remote-modal-large">
        {{ trans('common.send_message') }}
    </button>
@endsection

@section('content')
    {!! Form::open(['method' => 'post', 'role' => 'form', 'id' => 'applicant-info-form']) !!}
    <div class="form-body">
        <div class="row text-center">
            <img src="{{ $projectApplicant->user->getAvatar() }}"
                 alt="" class="profile-picture img-circle" width="80px" height="80px">
        </div>

        <div class="row">
            <div class="col-md-3">
                <label>{{trans("common.username")}} : </label>
            </div>
            <div class="col-md-9">
                <label>{{ $projectApplicant->user->getName() }}</label>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <label>{{trans("common.job_position")}} : </label>
            </div>
            <div class="col-md-9">
                <label>{{  $projectApplicant->user->getJobPosition() }}</label>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <label>{{trans("common.quote_price")}} : </label>
            </div>
            <div class="col-md-9">
                <label>USD {{ $projectApplicant->quote_price }} [ {{ $projectApplicant->getPayType() }} ]</label>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <label>{{trans("common.about_me")}} : </label>
            </div>
            <div class="col-md-9">
                <label>{{ $projectApplicant->user->getAboutMe() }}</label>
            </div>
        </div>

        <br/>
        <div class="row">
            <div class="col-md-3">
                <label>{{trans("common.contact")}} : </label>
            </div>
            <br/>
            <div class="clearfix"></div>
            <div>
                <div class="col-md-6">
                    <div class="col-md-2">
                        <img src="/images/Tencent_QQ.png" width="20px" height="20px">
                    </div>
                    <div class="col-md-10">
                        {{  $projectApplicant->user->qq_id }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-2">
                        <img src="/images/skype-icon-5.png" width="20px" height="20px">
                    </div>
                    <div class="col-md-10">
                        {{  $projectApplicant->user->skype_id }}
                    </div>
                </div>
            </div>
            <div>
                <div class="col-md-6">
                    <div class="col-md-2">
                        <img src="/images/wechat-logo.png" width="20px" height="20px">
                    </div>
                    <div class="col-md-10">
                        {{  $projectApplicant->user->wechat_id }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-2">
                        <img src="/images/MetroUI_Phone.png" width="20px" height="20px">
                    </div>
                    <div class="col-md-10">
                        {{  $projectApplicant->user->handphone_no }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection