@extends('ajaxmodal')

@section('title')
    {{trans('common.edit_official_project_news')}}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                $('#official-project-news-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit'
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit" class="btn btn-primary blue">{{trans("common.edit")}}</button>
@endsection

@section('content')
    {!! Form::open(['route' => ['backend.officialproject.news.edit.post', 'id' => $officialProjectNews->id ], 'method' => 'post', 'role' => 'form', 'id' => 'official-project-news-form']) !!}
        <div class="form-body">
            <div class="form-group">
                <label>{{trans('common.type')}}</label>
                <div>
                    {!! Form::select('type', $types, $officialProjectNews->type, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>{{trans('common.content')}}</label>
                <div>
                    {!! Form::textarea('content', $officialProjectNews->content, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection