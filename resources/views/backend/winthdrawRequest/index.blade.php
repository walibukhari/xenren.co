@extends('backend.layout')

@section('title')
{{trans('sideMenu.会员')}}
@endsection

@section('description')
{{trans('user.crud')}}
@endsection

@section('breadcrumb')
<ul class="page-breadcrumb">
    <li>
        <a href="javascript:;">{{trans('order.后台')}}</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="{{ route('backend.withdraw.requests') }}"><?= Lang::get('member.withdraw'); ?></a>
    </li>
</ul>
@endsection

@section('header')
<style>
    .custom-alerts{
        display: none !important;
    }
    #withdraw-dt_filter{
        float: right;
    }
    #withdraw-dt_paginate{
        float: right;
    }
    #withdraw-dt_info{
        position: relative;
        top: 15px;
    }
</style>
@endsection

@section('footer')
<script>
    +function() {
        $(document).ready(function() {
            $('#withdraw-dt').dataTable();
        });
    }(jQuery);

    function rejectModal(staff_id,id,transactionId,e) {
        $('.withdraw_id').val(id);
        $('.transaction_id').val(transactionId);
        $('#tBody').append('');
        var link = '/admin/rejected/payments/'+staff_id;
        $.ajax({
           url:link,
            success: function (response) {
                response.previous_data.map(function (data) {
                    console.log(data);
                    var html = '<tr><td>' + data.id + '</td><td>' + data.remarks + '</td><td>' + data.created_at + '</td></tr>'
                    $('#tBody').append(html);
                });
            }, error: function(error) {
               console.log(error);
            }
        });
        e.preventDefault();
        $('#reject').modal('show');
    }
    function rejectPayment() {
        var withdraw_id = $('.withdraw_id').val();
        var transaction_id = $('.transaction_id').val();
        var remarks = $('#remarks').val();
        var link = '/admin/withdraw/requests/reject/'+withdraw_id+'/'+transaction_id;
        $.ajax({
            url:link,
            method:'GET',
            data:{'remarks':remarks},
            success: function (response) {
                console.log(response);
                if(response.success === true){
                    toastr.success(response.message);
                    setTimeout(function () {
                        window.location.reload();
                    },600);
                }
            }, error:function (error) {
                console.log(error);
            }
        });
    }
</script>
@endsection

@section('content')
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-sharp">
            <span class="caption-subject bold uppercase"><?= Lang::get('member.withdraw'); ?></span>
        </div>
        <div class="actions">
        </div>
    </div>
    <div class="portlet-body">
        @if(session()->has('message'))
            <div class="alert alert-danger">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="table-container">
            <table class="table table-striped table-bordered table-hover table-checkable" id="withdraw-dt">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Staff Email</th>
                        <th>Payment Request</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($data as $dt)
                    <tr>
                        <td>{{$dt->id}}</td>
                        <td>{{$dt->staff->email}}</td>
                        <td>{{$dt->payment_request}}</td>
                        <td>{{$dt->status}}</td>
                        <td>
                            <a href="{{route('backend.withdraw.requests.approve',[$dt->id])}}" class="btn btn-sm btn-success">Review</a>
                            @if(isset($dt->status) && $dt->status != \App\Models\WithdrawRequest::STATUS_APPROVED)
                                @if($dt->status != \App\Models\WithdrawRequest::STATUS_REJECTED)
                                <span>/</span><a data-toggle="modal" data-target="#reject" class="btn btn-sm btn-danger" onclick="rejectModal('{{$dt->staff_id}}','{{$dt->id}}','{{$dt->transaction->id}}')">reject</a>
                                @endif
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('modal')
    <!-- Modal -->
    <div id="reject" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Reject</h4>
                </div>
                <form>
                <div class="modal-body">
                    <h3>Previous Record</h3>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>remarks</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody id="tBody">
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <hr>
                    <br>
                        @csrf
                        <input type="hidden" name="id" value="" class="withdraw_id">
                        <input type="hidden" name="id" value="" class="transaction_id">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Add Remarks</label>
                                    <textarea class="form-control" id="remarks" name="remarks"></textarea>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="rejectPayment()">Submit</button>
                </div>
                </form>
            </div>

        </div>
    </div>
@endsection