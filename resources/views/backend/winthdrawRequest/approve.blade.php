@extends('backend.layout')

@section('title')
{{trans('sideMenu.会员')}}
@endsection

@section('description')
{{trans('user.crud')}}
@endsection

@section('breadcrumb')
<ul class="page-breadcrumb">
    <li>
        <a href="javascript:;">{{trans('order.后台')}}</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="{{ route('backend.withdraw') }}"><?= Lang::get('member.withdraw'); ?></a>
    </li>
</ul>
@endsection

@section('header')
<style>
    .custom-alerts{
        display: none !important;
    }
</style>
@endsection

@section('footer')
<script>
    function approved(id,e) {
        $('#tBody').append('');
        var link = '/admin/approved/payments/'+id;
        $.ajax({
            url:link,
            success: function (response) {
                response.previous_data.map(function (data) {
                    console.log(data);
                    var html = '<tr><td>' + data.id + '</td><td>' + data.remarks + '</td><td>' + data.attachment + '</td><td>' + data.amount + '</td><td>' + data.created_at + '</td></tr>'
                    $('#tBody').append(html);
                });
            }, error: function(error) {
                console.log(error);
            }
        });
        e.preventDefault();
        $('.aprovedModal').modal('show');
    }
    function imageFileInputChange(element) {
        var reader = new FileReader();
        reader.onload = function (e) {
            var target = element.getAttribute('target');
            $('#' + target).attr('src', e.target.result);
        };
        reader.readAsDataURL(element.files[0]);
    }
    function approvedPayment(id) {
        var files = $('#image_file')[0].files[0];
        var remarks = $('#remarks').val();
        var form = new FormData();
        form.append('transaction_id',id);
        form.append('withdrawRequest_id','{{$dt->id}}');
        form.append('file',files);
        form.append('remarks',remarks);
        form.append('amount','{{$dt->payment_request}}');
        var link = '/admin/approved/request';
        $.ajax({
            url:link,
            method:'POST',
            data:form,
            processData: false,
            contentType: false,
            success: function (response) {
                console.log(response);
                if(response.success === true){
                    toastr.success(response.message);
                    setTimeout(function () {
                        window.location.reload();
                    },600);
                }
            }, error:function (error) {
                console.log(error);
            }
        });
    }
</script>
@endsection

@section('content')
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-sharp">
            <span class="caption-subject bold uppercase"><?= Lang::get('member.approve'); ?></span>
        </div>
        <div class="actions">
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-container">
            <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover table-checkable" id="withdraw-dt">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Staff Email</th>
                    <th>bank name</th>
                    <th>iban number</th>
                    <th>card number</th>
                    <th>branch name</th>
                    <th>branch code</th>
                    <th>branch address</th>
                    <th>Payment Request</th>
                    <th>total balance</th>
                    <th>office Name</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{$dt->id}}</td>
                    <td>{{$dt->staff->email}}</td>
                    <td>{{$dt->bank_name}}</td>
                    <td>{{$dt->iban_number}}</td>
                    <td>{{$dt->card_number}}</td>
                    <td>{{$dt->branch_name}}</td>
                    <td>{{$dt->branch_code}}</td>
                    <td>{{$dt->branch_address}}</td>
                    <td>{{$dt->payment_request}}</td>
                    <td>{{$dt->total_balance}}</td>
                    <td>{{$dt->office->office_name}}</td>
                    <td>
@if(isset($dt->transaction->status) && $dt->transaction->status == \App\Models\Transaction::STATUS_SHARED_OFFICE_OWNER_STATUS_WITHDRAW_REQUEST_APPROVED)
                            <a href="#" class="btn btn-sm btn-success" >Approved</a>
                        @else
                            @if($dt->status == \App\Models\WithDrawRequest::STATUS_REJECTED)
                                <button class="btn btn-sm btn-danger">Rejected</button>
                            @else
                                <button class="btn btn-sm btn-success" data-toggle="modal" data-target=".aprovedModal" onclick="approved('{{$dt->staff_id}}')">Approve</button>

                            @endif
                    @endif
                    </td>

                </tr>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="aprovedModal" class="modal fade aprovedModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Approved</h4>
            </div>
            <div class="modal-body">
                <h3>Previous Record</h3>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>remarks</th>
                            <th>attachment</th>
                            <th>amount</th>
                            <th>date</th>
                        </tr>
                        </thead>
                        <tbody id="tBody">
                        </tbody>
                    </table>
                </div>
                <br>
                <hr>
                <br>
                <form enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Upload Slip</label>
                                <input type="file"
                                       name="bank_slip"
                                       id="image_file"
                                       class="image-upload"
                                       target="image_preview"
                                       onChange="imageFileInputChange(this);"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <img id="image_preview" class="img-thumbnail img-preview" src=""/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Remarks</label>
                                <textarea class="form-control" id="remarks" name="remarks"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline btn-success" onclick="approvedPayment('{{$dt->transaction_id}}')">Submit</button>
            </div>
        </div>

    </div>
</div>
@endsection
