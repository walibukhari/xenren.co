@extends('backend.layout')

@section('title')
    {{trans('managecoin.managecoin')}}
@endsection

@section('description')
    {{trans('managecoin.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>

        <li>
            <a href="{{ route('backend.manageCoins') }}">{{trans('managecoin.managecoin')}}</a>
        </li>
    </ul>
@endsection

@section('header')

@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('managecoin.dashboard')}}</span>
            </div>
          <!--  <div class="actions">
                <a href="{{ route('backend.manageCoins.create') }}" class="btn btn-circle red-sunglo btn-sm"><i
                            class="fa fa-plus"></i> {{trans('managecoin.add_coin')}}</a>
            </div> -->
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="manageCoin-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th>id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Feedback</th>
                    </tr>
                    <tbody>
                    @if(count($data) < 1)
                        <tr>
                            <td colspan="4"> No Data</td>
                        </tr>
                    @endif
                    @foreach($data as $k => $v)
                        <tr>
                            <th>{{$v->id}}</th>
                            <th>{{isset($v->username) ? $v->username  : 'N/A'}}</th>
                            <th>{{isset($v->email) ? $v->email  : 'N/A'}}</th>
                            <th>{{isset($v->feedback_text) ? $v->feedback_text  : 'N/A'}}</th>
                        </tr>
                    @endforeach
                    </tbody>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection