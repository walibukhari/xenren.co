@extends('backend.layout')

@section('title')
{{trans('sideMenu.working_sheet')}}
@endsection
@section('description')

@endsection
<head xmlns:v-on="http://www.w3.org/1999/xhtml">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="{{asset('js/vue.min.js')}}"></script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">
    <link href="{{asset('custom/css/backend/workingsheet.css')}}" type="text/css" rel="stylesheet">
    <style>
        [v-cloak] {
            display: none;
        }
        .filter-option > .pull-left{
            font-size: 12px !important;
        }
        @media (max-width: 480px) {

            .page-content-wrapper .page-content {
                display: flex;
                justify-content: center;
                height: fit-content;
            }
            .customSetStyle{
                text-align: center;
                margin-top: 0px;
                font-size: 21px;
                font-weight: 400;
                color: #25ce0f;
            }
        }

        .loader {
            font-size: 10px;
            margin: 50px auto;
            text-indent: -9999em;
            width: 11em;
            height: 11em;
            border-radius: 50%;
            background: #ffffff;
            background: -moz-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            background: -webkit-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            background: -o-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            background: -ms-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            background: linear-gradient(to right, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            position: relative;
            -webkit-animation: load3 1.4s infinite linear;
            animation: load3 1.4s infinite linear;
            -webkit-transform: translateZ(0);
            -ms-transform: translateZ(0);
            transform: translateZ(0);
        }
        .loader:before {
            width: 50%;
            height: 50%;
            background: #555;
            border-radius: 100% 0 0 0;
            position: absolute;
            top: 0;
            left: 0;
            content: '';
        }
        .loader:after {
            background: #fff;
            width: 75%;
            height: 75%;
            border-radius: 50%;
            content: '';
            margin: auto;
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }
        @-webkit-keyframes load3 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @keyframes load3 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
    </style>
</head>
@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Working Sheet</a>
            <i class="fa fa-circle"></i>
        </li>
    </ul>
@endsection
@section('footer')
    <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <script>
        $('.scroll-div').slimScroll({});
    </script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/vue.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vue-resource.min.js')}}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.15/lodash.core.min.js"></script>
    <script>
        let vu = new Vue({
            el: '#workingSheet',
           data: {
               selectSeat:'',
               monthPrice:'',
               max_weekly: '0',
               max_weekly_profit: '0',
               max_daily: '0',
               max_daily_profit: '0',
               max_hourly: '0',
               max_hourly_profit: '0',
               min_weekly: '0',
               min_weekly_profit: '0',
               min_daily: '0',
               min_daily_profit: '0',
               min_hourly: '0',
               min_hourly_profit: '0',
               seat_id:'',
               checkData:[],
               isConfirm:'',
               week:'',
               daily:'',
               hourly:'',
               category_id:'',
               seat_name:'',
               seat_number:'',
               seat_type:'',
               price:'',
               oldPrice:'',
               number:'',
               shared_office_product_id:'',
               weekly_price:'',
               hourly_price:'',
               daily_price:'',
               month_price:'',
               loaderLoad:true
           },
            methods: {
                estimatePrice: function(){
                    let selectedSeat = this.selectSeat;
                    let monthly_rate = this.monthPrice;
                    var link = '/admin/sharedoffice/working-sheet';
                    var formData = new FormData();
                    formData.append('csrf_token','{{csrf_token()}}');
                    formData.append('seat',selectedSeat);
                    formData.append('month_rate',monthly_rate);
                     if(selectedSeat === '') {
                         toastr.error('please select seat');
                     } else if(monthly_rate === '') {
                         toastr.error('please enter month rate');
                     } else {
                         var self = this;
                         self.$http.post(link, formData).then(function (response) {
                             console.log(response.body);
                             this.checkData = response.body;
                             /* maximum */
                             console.log('maximum');
                             console.log(this.max_weekly = response.body.maximum.weekly.weeklyRateRange);
                             console.log(this.max_weekly_profit = response.body.maximum.weekly.maxProfit);
                             console.log(this.max_daily = response.body.maximum.daily.dailyRateRange);
                             console.log(this.max_daily_profit = response.body.maximum.daily.maxProfit);
                             console.log(this.max_hourly = response.body.maximum.hourly.hourlyRateRange);
                             console.log(this.max_hourly_profit = response.body.maximum.hourly.maxProfit);

                             /* minimum */
                             console.log('minimum');
                             console.log(this.min_weekly = response.body.minimum.weekly.weeklyRateRange);
                             console.log(this.min_weekly_profit = response.body.minimum.weekly.maxProfit);
                             console.log(this.min_daily = response.body.minimum.daily.dailyRateRange);
                             console.log(this.min_daily_profit = response.body.minimum.daily.maxProfit);
                             console.log(this.min_hourly = response.body.minimum.hourly.hourlyRateRange);
                             console.log(this.min_hourly_profit = response.body.minimum.hourly.maxProfit);

                         }, function (error) {
                             console.log(error);
                         })
                     }
                },
                getMonthPrice: function (e) {
                    this.monthPrice = this.selectSeat;
                    if(e.target.options.selectedIndex > -1) {
                        this.seat_id = e.target.options[e.target.options.selectedIndex].dataset.id;
                        this.seat_name = e.target.options[e.target.options.selectedIndex].dataset.name;
                        this.seat_number = e.target.options[e.target.options.selectedIndex].dataset.number;
                        this.shared_office_product_id = e.target.options[e.target.options.selectedIndex].dataset.unique;
                        console.log(e.target.options[e.target.options.selectedIndex].dataset);
                    }
                    var link = '/admin/get/product/'+this.shared_office_product_id;
                    this.$http.get(link).then(function (response) {
                        console.log(response.body);
                        this.weekly_price = response.body.weekly_price;
                        this.hourly_price = response.body.hourly_price;
                        this.daily_price = response.body.daily_price;
                        this.month_price = response.body.month_price;
                    }, function (error) {
                        console.log(error);
                    });
                },
                apply: function (key,val) {
                    if (this.checkData.length === 0) {
                        toastr.error('no data available please select seat');
                    } else {
                        $('#confirmmodal').modal('show');
                        let seatId = this.seat_id;
                        let number = this.seat_number;
                        var week = '';
                        var daily = '';
                        var hourly = '';
                        var seat_type = '';
                        var price = '';
                        var oldPrice = '';
                        if (key === 'max_week') {
                            console.log(val);
                            week = val;
                            price = val;
                            seat_type = 'Weekly';
                            oldPrice = this.weekly_price;
                        } else if (key === 'max_daily') {
                            console.log(val);
                            daily = val;
                            price = val;
                            seat_type = 'Daily';
                            oldPrice = this.daily_price;
                        } else if (key === 'max_hourly') {
                            console.log(val);
                            hourly = val;
                            price = val;
                            oldPrice = this.hourly_price;
                            seat_type = 'Hourly';
                        } else if (key === 'min_week') {
                            console.log(val);
                            week = val;
                            price = val;
                            oldPrice = this.weekly_price;
                            seat_type = 'Weekly';
                        } else if (key === 'min_daily') {
                            console.log(val);
                            daily = val;
                            price = val;
                            oldPrice = this.daily_price;
                            seat_type = 'Daily';
                        } else if (key === 'min_hourly') {
                            console.log(val);
                            hourly = val;
                            price = val;
                            oldPrice = this.hourly_price;
                            seat_type = 'Hourly';
                        }
                        console.log("now values get");
                        console.log(seatId);
                        this.category_id = seatId;
                        this.seat_type = seat_type;
                        this.number = number;
                        this.price = price;
                        this.oldPrice = oldPrice;
                        console.log('week');
                        console.log(week);
                        this.week = week;
                        console.log('daily');
                        console.log(daily);
                        this.daily = daily;
                        console.log('hourly');
                        console.log(hourly);
                        this.hourly = hourly;
                        console.log("now values get");
                    }
                },
                onConfirm: function () {
                    let link = '/admin/sharedoffice/update/products';
                    let week = this.week;
                    let seatId = this.category_id;
                    let number = this.number;
                    let daily = this.daily;
                    let hourly = this.hourly;
                    var seatData = new FormData();
                    seatData.append('csrf_token', '{{csrf_token()}}');
                    seatData.append('category_id', seatId);
                    seatData.append('office_id', '{{ isset($office_id) ? $office_id :'' }}');
                    seatData.append('weekly_price', week);
                    seatData.append('daily_price', daily);
                    seatData.append('hourly_price', hourly);
                    seatData.append('id', this.shared_office_product_id);
                    seatData.append('number', number);
                    this.$http.post(link, seatData).then(function (response) {
                        console.log(response);
                        if(response.body.success === true) {
                            toastr.success(response.body.message);
                            setTimeout(function () {
                                window.location.reload();
                            },1000);
                        }
                    }, function (error) {
                        console.log(error);
                        toastr.error('some thing went wrong');
                    })
                },
            },
            mounted() {
                console.log("vue initialized");
                let self = this;
                setTimeout(() => {
                    self.loaderLoad = false
                },800)
            }
        });
    </script>
@endsection
@section('content')
    <div class="worksheet" id="workingSheet" v-cloak>
        <h1 class="customSetStyle">Working Sheet</h1>
        <br>
        <form class="form-group" id="worksheet">
            <div class="row">
                <div class="customsetClass">
                <div class="col-sm-2 box-dropdown">
                    <select class="selectpicker show-tick" @change="getMonthPrice" data-placement="Select Seat" data-live-search="true" v-model="selectSeat">
                        <option value="Select Desk">Select Desk</option>
                        @if(isset($products))
                            @foreach($products as $ptd)
                                <option value="{{$ptd['month_price']}}" data-unique="{{$ptd['unique_id']}}" data-id="{{$ptd['id']}}" data-number="{{$ptd['number']}}" data-name="{{$ptd['name']}}">{{$ptd['name']}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-sm-3 setCustomClassTimeSheet">
                    <div class="monthly-price-box">
                        <span><i class="fa fa-usd" aria-hidden="true"></i> Monthly Rate</span>
                        <input type="number" class="form-control" v-model="monthPrice" value="300">
                    </div>
                </div>
                </div>
                <div class="col-sm-3">
                    <input type="button" class="btn-green" @click="estimatePrice" value="$ Estimate Income">
                </div>
            </div>
        </form>


        <div v-if="loaderLoad">
            <div class="loader">Loading...</div>
        </div>
        <div v-if="!loaderLoad" class="bg-heading">
            <h2>Working reports</h2>
        </div>
        <p v-if="!loaderLoad" class="text-center">Divide monthly into different lease period</p>

        <div v-if="!loaderLoad" class="table-report">
            <div class="custom-table">
            <div class="minimum">Minimum</div>
                <div class="table-responsive ">
            <table id="tabele" class="table custom-table-height text-center table-bordered table-hover ">
                <thead>
                <tr role="row" class="heading">
                    <th>Weekly
                        <span class="small">(Rate Range)</span>
                        <span class="short-area">50% Blank Ratio<br>Divide 4 weeks</span>
                    </th>
                    <th>Profit <span class="small">(Rate Range)</span></th>
                    <th>Daily
                        <span class="small">(Rate Range)</span>
                        <span class="short-area">100% Blank Ratio<br>Divide into 24 working days</span></th>
                    <th>Profit <span class="small">(Rate Range)</span></th>
                    <th>Hourly
                        <span class="small">(Rate Range)</span>
                        <span class="short-area">Assume 24 days only rent for<br>8 hours per day</th>
                    <th>Profit <span class="small">(Rate Range)</span></th>
                </tr>
                <tr role="row">
                    <td>
                        @{{ min_weekly }}
                        <a class="apply-now" @click="apply('min_week',min_weekly)" >Apply</a>
                    </td>
                    <td>
                        @{{ this.min_weekly_profit }}
                    </td>
                    <td>
                        @{{ this.min_daily }}
                        <a class="apply-now" @click="apply('min_daily',min_daily)" >Apply</a>
                    </td>
                    <td>
                        @{{ this.min_daily_profit }}
                    </td>
                    <td>
                        @{{ this.min_hourly }}
                        <a class="apply-now" @click="apply('min_hourly',min_hourly)" >Apply</a>
                    </td>
                    <td>
                        @{{ this.min_hourly_profit }}
                    </td>
                </tr>
                </thead>
            </table>
                </div>
        </div>

            <div class="custom-table">
                <div class="maximum">Maximum</div>
                <div class="table-responsive">
                <table id="tabele" class="table custom-table-height text-center table-bordered table-hover  ">
                    <thead>
                    <tr role="row" class="heading">
                        <th>Weekly
                            <span class="small">(Rate Range)</span>
                            <span class="short-area">50% Blank Ratio<br>Divide 4 weeks</span>
                        </th>
                        <th>Profit <span class="small">(Rate Range)</span></th>
                        <th>Daily
                            <span class="small">(Rate Range)</span>
                            <span class="short-area">50% Blank Ratio<br>Divide into 24 working days</span></th>
                        <th>Profit <span class="small">(Rate Range)</span></th>
                        <th>Hourly
                            <span class="small">(Rate Range)</span>
                            <span class="short-area">Assume 24 days only rent for<br>8 hours per day</span></th>
                        <th>Profit <span class="small">(Rate Range)</span></th>
                    </tr>
                    <tr role="row">
                        <td>
                            @{{ this.max_weekly  }}
                            <a class="apply-now" @click="apply('max_week',max_weekly)">Apply</a>
                        </td>
                        <td>
                            @{{ this.max_weekly_profit }}
                        </td>
                        <td>
                            @{{ this.max_daily }}
                            <a class="apply-now" @click="apply('max_daily',max_daily)" >Apply</a>
                        </td>
                        <td>
                            @{{ this.max_daily_profit }}
                        </td>
                        <td>
                            @{{ this.max_hourly }}
                            <a class="apply-now" @click="apply('max_hourly',max_hourly)" >Apply</a>
                        </td>
                        <td>
                            @{{ this.max_hourly_profit }}
                        </td>
                    </tr>
                    </thead>
                </table>
            </div>
            </div>

        </div>
        <!-- Modal -->
        <div class="modal fade bd-example-modal-sm" id="confirmmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog vertical-align-center modal-sm" role="document">
                <div class="modal-content confirm-popup">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                    </div>
                    <div class="modal-body">
                        <form method="post">
                            @csrf
                            <input type="hidden" v-model="week" >
                            <input type="hidden" v-model="daily" >
                            <input type="hidden" v-model="hourly" >
                            <input type="hidden" v-model="category_id">
                            <input type="hidden" v-model="shared_office_product_id">
                            <input type="hidden" v-model="number">
                        </form>
                        Please notice that, This Will be Charge<br>
                        Your Current @{{ this.seat_name }} @{{ this.seat_type }}<br>
                        @{{ oldPrice }} USD to @{{ this.price }} USD.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-success confirm" @click="onConfirm">Confirm</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


