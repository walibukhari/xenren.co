@extends('backend.layout')

@section('title')
    {{ trans('common.feedback_chat_room') }}
@endsection

@section('description')
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('officialprojects.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.feedback') }}">{{trans('common.feedback_chat_room')}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <link href="/custom/css/frontend/chat-room.css" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/lightbox2/dist/css/lightbox.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{ $feedback->title }}</span>
            </div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-8 chat-room-div">
                    <h5 class="title">{{ trans('common.chat_room') }}</h5>

                    <div class="row conversation" data-url="{{ route('backend.feedback.pullchat',$chatRoom->id)}}">
                        <div class="col-md-12 clearfix">
                            <div class="portlet box gray-color">
                                <div class="portlet-body form">
                                    <div class="scroller text-center" id="content-chat" style="height: 525px;">
                                        <h3>{{ trans('common.chat_content') }} ...</h3>
                                        {{--<ul class="chats">--}}

                                            {{--<li class="in">--}}
                                                {{--<img class="avatar" alt=""--}}
                                                     {{--src="/images/people-avatar.png"/>--}}
                                                {{--<div class="message">--}}
                                                    {{--<a href="javascript:void(0);" class="name">--}}
                                                        {{--Adam Johnson </a>--}}
                                                    {{--<div class="div-datetime">--}}
                                                        {{--<span class="datetime">07 Nov, 2016 09:33</span>--}}
                                                    {{--</div>--}}
                                                    {{--<span class="body">--}}
                                                        {{--Sed elelfend nonummy dlam. Praesent maurls ante, elementum et. blbendum at, posuere sit amet, nlbh. DuIs tIncldunt lectus quls dul vlverra vestlbulum. Suspendlsse vulputate allquam dul. NuIla elementum dul ut augue. Allquam vehlcula ml at maurls. Maecenas placerat, nisi at consequa--}}
                                                    {{--</span>--}}
                                                {{--</div>--}}
                                            {{--</li>--}}

                                            {{--<li class="in">--}}
                                                {{--<img class="avatar" alt=""--}}
                                                     {{--src="/images/people-avatar.png"/>--}}
                                                {{--<div class="message">--}}
                                                    {{--<a href="javascript:void(0);" class="name">--}}
                                                        {{--John Henry </a>--}}
                                                    {{--<div class="div-datetime">--}}
                                                        {{--<span class="datetime">3 min ago</span>--}}
                                                    {{--</div>--}}
                                                    {{--<span class="body">--}}
                                                    {{--DuIs tIncldunt lectus quls dui vlverra vestlbulum. Suspendlsse vulputate allquam dul. NuIla elementum dul ut augue. --}}
                                                {{--</span>--}}
                                                {{--</div>--}}
                                            {{--</li>--}}

                                            {{--<li class="in">--}}
                                                {{--<img class="avatar" alt=""--}}
                                                     {{--src="/images/avatar_xenren.png"/>--}}
                                                {{--<div class="message">--}}
                                                    {{--<a href="javascript:void(0);" class="name">--}}
                                                        {{--You </a>--}}
                                                    {{--<div class="div-datetime">--}}
                                                        {{--<span class="datetime">3 min ago</span>--}}
                                                    {{--</div>--}}
                                                    {{--<span class="body">--}}
                                                    {{--DuIs tIncldunt lectus quls dui vlverra vestlbulum. Suspendlsse vulputate allquam dul. NuIla elementum dul ut augue. --}}
                                                {{--</span>--}}
                                                {{--</div>--}}
                                            {{--</li>--}}

                                        {{--</ul>--}}
                                    </div>
                                    <div class="chat-form">
                                        <form action="{{ route('backend.feedback.postchat') }}" method="post" class="">
                                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                            <input type="hidden" name="chat_room_id" id="chat_room_id" value="{{ $chatRoom->id}}">
                                            <div class="row chat-form-main">
                                                <div class="col-md-1 chat-form-l">
                                                    <div class="upload-picture upload-btn-custom">
                                                        <a href="{{ route('backend.feedback.chatimage', ['chat_room_id' => $chatRoom->id] ) }}"
                                                            data-toggle="modal"
                                                            data-target="#modalAddFileChat">
                                                            <i class="fa fa-paperclip"
                                                               aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                    <div class="upload-btn-custom">
                                                        <a href="javascript:;">
                                                            <i class="fa fa-cog"
                                                               aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-11 chat-form-r">
                                                    <div class="input-cont">
                                                        <input class="form-control message"
                                                               name="message" type="text"
                                                               placeholder="{{ trans('member.leave_message') }}..."/>
                                                        <button type="submit"
                                                                class="btn btn-submit">
                                                            <i class="fa fa-send"
                                                               aria-hidden="true"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <h5 class="title">{{ trans('common.chat_room_members') }} ({{count($chatFollowers)}})</h5>
                    <div class="portlet light bordered chat-room-memb-div">
                        <div class="portlet-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_actions_pending">
                                    @foreach( $chatFollowers as $key => $chatFollower )
                                    <div class="mt-actions">
                                        <div class="mt-action">
                                            @if( $chatFollower->staff_id != null )
                                            <div class="mt-action-img">
                                                <img alt="" class="img-circle"
                                                     src="{{ $chatFollower->staff->getAvatar() }}"
                                                     style="width: 55px; height: 55px;">
                                            </div>
                                            <div class="mt-action-body">
                                                <div class="mt-action-row">
                                                    <div class="mt-action-info ">
                                                        <div class="mt-action-details"
                                                             style="display: unset;">

                                                        <span class="mt-action-author">
                                                            {{ $chatFollower->staff->name }}
                                                            <span class="fa fa-check-circle"></span>
                                                        </span>
                                                            <p class="mt-action-desc">{{ trans('common.support_executive') }}</p>
                                                        </div>

                                                    <div>
                                                    <br/></div>
                                                    </div>
                                                </div>
                                            </div>
                                            @elseif( $chatFollower->user_id != null )
                                            <div class="mt-action-img">
                                                <img alt="" class="img-circle"
                                                     src="{{ $chatFollower->user->getAvatar() }}"
                                                     style="width: 55px; height: 55px;">
                                            </div>

                                            <div class="mt-action-body">
                                                <div class="mt-action-row">
                                                    <div class="mt-action-info ">
                                                        <div class="mt-action-details"
                                                             style="display: unset;">
                                                        <span class="mt-action-author">
                                                            {{ $chatFollower->user->getName() }}
                                                            @if( $chatFollower->user->getLatestIdentityStatus() == \App\Models\UserIdentity::STATUS_APPROVED )
                                                            <span class="fa fa-check-circle"></span>
                                                            @endif
                                                        </span>
                                                            <p class="mt-action-desc">{{ trans('common.creator') }}</p>
                                                        </div>
                                                        <div><br/></div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    @endforeach

                                    {{--<div class="mt-actions">--}}
                                        {{--<div class="mt-action">--}}
                                            {{--<div class="mt-action-img">--}}
                                                {{--<img alt="" class="img-circle"--}}
                                                     {{--src="/images/people-avatar.png"--}}
                                                     {{--style="width: 55px; height: 55px;">--}}
                                            {{--</div>--}}
                                            {{--<div class="mt-action-body">--}}
                                                {{--<div class="mt-action-row">--}}
                                                    {{--<div class="mt-action-info ">--}}
                                                        {{--<div class="mt-action-details"--}}
                                                             {{--style="display: unset;">--}}
                                                        {{--<span class="mt-action-author">--}}
                                                            {{--Adam Johnson--}}
                                                            {{--<span class="fa fa-check-circle"></span>--}}
                                                        {{--</span>--}}
                                                            {{--<p class="mt-action-desc">Support--}}
                                                                {{--Excecutive</p>--}}
                                                        {{--</div>--}}
                                                        {{--<div><br/></div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="mt-actions">--}}
                                        {{--<div class="mt-action">--}}
                                            {{--<div class="mt-action-img">--}}
                                                {{--<img alt="" class="img-circle"--}}
                                                     {{--src="/images/avatar_xenren.png"--}}
                                                     {{--style="width: 55px; height: 55px;">--}}
                                            {{--</div>--}}

                                            {{--<div class="mt-action-body">--}}
                                                {{--<div class="mt-action-row">--}}
                                                    {{--<div class="mt-action-info ">--}}
                                                        {{--<div class="mt-action-details"--}}
                                                             {{--style="display: unset;">--}}
                                                        {{--<span class="mt-action-author">--}}
                                                            {{--You--}}
                                                            {{--<span class="fa fa-check-circle"></span>--}}
                                                        {{--</span>--}}
                                                            {{--<p class="mt-action-desc">Guest</p>--}}
                                                        {{--</div>--}}
                                                        {{--<div><br/></div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <div id="modalAddFileChat" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{asset('assets/global/plugins/lightbox2/dist/js/lightbox.min.js')}}" type="text/javascript"></script>
    <script>
        var lang = {
            unable_to_request: "{{ trans('common.unable_to_request') }}",
            unknown_error: "{{ trans('common.unknown_error') }}"
        };
    </script>
    <script src="/custom/js/backend/feedbackChat.js" type="text/javascript"></script>
    <script>
        +function () {
            $(document).ready(function () {
                $(".btn-delete").on('click', function(){
                    var url = $(this).data('url');
                    var id = $(this).data('id');
                    $.ajax({
                        url: url,
                        dataType: 'json',
                        data: {id: id},
                        method: 'POST',
                        error: function () {
                            alert(lang.unable_to_request);
                        },
                        success: function (result) {
                            if (result.status == 'OK') {
                                alertSuccess(result.message);
                                $('#feedback-' + id).hide();
                            } else if (result.status == 'Error') {
                                alertError(result.message);
                            }
                        }
                    });
                });

                var _self = this;

                //handlers
                document.addEventListener('paste', function (e) {
                    _self.paste_auto(e);
                }, false);

                //on paste
                this.paste_auto = function (e) {
                    if (e.clipboardData) {
                        var items = e.clipboardData.items;
                        if (!items) return;

                        //access data directly
                        for (var i = 0; i < items.length; i++) {
                            if (items[i].type.indexOf("image") !== -1) {
                                //image
                                var blob = items[i].getAsFile();

                                var URLObj = window.URL || window.webkitURL;
                                var source = URLObj.createObjectURL(blob);

                                var f = document.createElement("form");
                                f.setAttribute('id',"upload-image-form");
                                f.setAttribute('method',"post");
                                f.setAttribute('enctype',"multipart/form-data");

                                var fd = new FormData();
                                var attrs = $("#upload-image-form").serialize().split("&");
                                for (i = 0; i < attrs.length - 1; i++) {
                                    var temp = attrs[i].split('=');
                                    fd.append(temp[0], temp[1]);
                                }
                                fd.append("_token", "{!! csrf_token() !!}");
                                fd.append("chat_room_id", "{{ $chatRoom->id}}");
                                fd.append("message", "PrintScreen");
                                fd.append('file', blob);
                                $.ajax({
                                    type: 'POST',
                                    url: '{{ route('backend.feedback.chatimage.post')}}',
                                    data: fd,
                                    processData: false,
                                    contentType: false,
                                    error: function () {
                                        alert(lang.unable_to_request);
                                    },
                                    success: function (result) {
                                        if (result.status == 'OK') {
//                                            alertSuccess(result.message);
//                                            setTimeout(function () {
//                                                location.reload();
//                                            }, 3000);
                                            $('#content-chat').append(result.contents);

                                            //always scroll to bottom
                                            var scrollHeight =  $('#content-chat').prop('scrollHeight');
                                            $("#content-chat").slimScroll({ scrollTo: scrollHeight });

                                        } else if (result.status == 'Error') {
                                            alertError(result.message);
                                        }
                                    }
                                });
                            }
                        }
                        e.preventDefault();
                    }
                };
            });
        }(jQuery);
    </script>
@endsection