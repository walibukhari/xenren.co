@extends('ajaxmodal')

@section('title')
    {{trans("common.feedback")}}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                $('#feedback-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit'
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit" class="btn btn-primary blue">{{trans("common.edit")}}</button>
@endsection

@section('content')
    {!! Form::open(['route' => ['backend.feedback.edit.post', 'id' => $feedback->id], 'method' => 'post', 'role' => 'form', 'id' => 'feedback-form', 'files' => true]) !!}
        <div class="form-body">
            <div class="form-group">

                <div class="row">
                    <div class="col-md-5">
                        <label>{{trans('common.id')}}</label> :
                    </div>
                    <div class="col-md-7">
                        {{ $feedback->id }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <label>{{trans('common.priority')}}</label> :
                    </div>
                    <div class="col-md-7">
                        {{ $feedback->translatePriority() }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <label>{{trans('common.username')}}</label> :
                    </div>
                    <div class="col-md-7">
                        {{ $feedback->user->getName() }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <label>{{trans('common.title')}}</label> :
                    </div>
                    <div class="col-md-7">
                        {{ $feedback->title }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <label>{{trans('common.description')}}</label> :
                    </div>
                    <div class="col-md-7">
                        {{ $feedback->description }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <label>{{trans('common.date')}}</label> :
                    </div>
                    <div class="col-md-7">
                        {{ $feedback->created_at }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <label>{{trans('common.status')}}</label> :
                    </div>
                    <div class="col-md-7">
                        {!! Form::select('status', \App\Models\Feedback::getStatusLists(false), $feedback->status, ['class' => 'form-control form-filter input-sm']) !!}
                    </div>
                </div>

                @foreach( $feedback->files as $file )
                {{--<div class="row">--}}
                    <div class="col-md-4 text-center" style="padding:10px 0px;">
                        <a href="{{ $file->getFile() }}" rel="gallery" class="fancybox" title="{{ $feedback->id }}">
                            <img src="{{ $file->getFile() }}" width="100px">
                        </a>
                    </div>
                {{--<div>--}}
                @endforeach

                <div class="clearfix">
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection