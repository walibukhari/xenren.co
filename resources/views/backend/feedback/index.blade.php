@extends('backend.layout')

@section('title')
    {{ trans('common.feedback_management') }}
@endsection

@section('description')
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('officialprojects.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.feedback') }}">{{trans('common.feedback_management')}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <link href="/assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/backend/feedback.css" rel="stylesheet" type="text/css" />
@endsection

@section('footer')
    <script src="/assets/global/plugins/fancybox/source/jquery.fancybox.js" type="text/javascript"></script>
    <script>
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#feedback-dt'),
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('backend.feedback.dt') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'priority', name: 'priority'},
                            {data: 'title', name: 'title', orderable: false, searchable: false},
                            {data: 'status', name: 'status'},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false}
                        ],
                    }
                });

                $('.fancybox').fancybox({
                    padding : 0,
                    openEffect  : 'elastic',
                    beforeLoad : function() {
                        var isVisible = $('#remote-modal').is(':visible');
                        if( isVisible == true )
                        {
                            $('#remote-modal').modal('hide')
                        }
                    },
                    afterClose: function() {
                        var id = this.title;
                        $('.btn-modal-' + id ).trigger('click');
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('common.feedback_list')}}</span>
            </div>
            <div class="actions">
                {{--<a href="{{ route('backend.officialproject.create') }}" class="btn btn-circle red-sunglo btn-sm">\--}}
                    {{--<i class="fa fa-plus"></i> {{trans('officialprojects.新增官方项目')}}--}}
                </a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="feedback-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th>ID</th>
                        <th>{{trans('common.priority')}}</th>
                        <th>{{trans('common.title')}}</th>
                        <th>{{trans('common.status')}}</th>
                        <th>{{trans('common.date')}}</th>
                        <th>{{ trans('common.action') }}</th>
                    </tr>
                    <tr role="row" class="filter">
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_id"
                                placeholder="ID" />
                        </td>
                        <td>
                            {!! Form::select('filter_priority', \App\Models\Feedback::getPriorityLists(true), '', ['class' => 'form-control form-filter input-sm']) !!}
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_title"
                                placeholder="{{trans('common.title')}}">
                        </td>
                        <td>
                            {!! Form::select('filter_status', \App\Models\Feedback::getStatusLists(true), '', ['class' => 'form-control form-filter input-sm']) !!}
                        </td>
                        <td>
                            <div class="input-group margin-bottom-5">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_created_after" placeholder="{{trans('common.start')}}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                            <div class="input-group">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_created_before" placeholder="{{trans('common.end')}}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                        </td>
                        <td>
                            <div class="margin-bottom-5">
                                <button class="btn btn-info-alt filter-submit">
                                    <i class="fa fa-search"></i> {{trans('officialprojects.过滤')}}
                                </button>
                            </div>
                            <button class="btn btn-danger-alt filter-cancel">
                                <i class="fa fa-times"></i> {{trans('officialprojects.重置')}}
                            </button>
                        </td>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection