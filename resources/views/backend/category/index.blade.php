@extends('backend.layout')

@section('title')
    {{trans("category.类别管理")}}
@endsection

@section('description')
    {{trans("category.desc")}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('category.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.category') }}">{{trans('category.类别')}}</a>
            @if ($model)
                <i class="fa fa-circle"></i>
            @endif
        </li>
        @if ($model && $model->parent)
            <li>
                <a href="{{ route('backend.category', ['id' => $model->parent->id]) }}">
                    {{ $model->parent->getTitles() }}
                </a>
                <i class="fa fa-circle"></i>
            </li>
        @endif
        @if ($model)
            <li>
                <a href="{{ route('backend.category', ['id' => $model->id]) }}">
                    {{ $model->getTitles() }}
                </a>
            </li>
        @endif
    </ul>
@endsection

@section('header')

@endsection

@section('footer')
    <script>
        var url = "{{($id === null) ? route('backend.category.dt')  : route('backend.category.dt', ['id' => $id]) }}";

        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#category-dt'),
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url" : url,
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'titles', name: 'titles', orderable: false, searchable: false},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false}
                        ],
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('category.类别列表')}}</span>
            </div>
            <div class="actions">
                @if ($id === null)
                <a href="{{ route('backend.category.create') }}" class="btn btn-circle red-sunglo btn-sm" data-target="#remote-modal" data-toggle="modal"><i class="fa fa-plus"></i> 新增类别</a>
                @else
                <a href="{{ route('backend.category.create', ['id' => $id]) }}" class="btn btn-circle red-sunglo btn-sm" data-target="#remote-modal" data-toggle="modal"><i class="fa fa-plus"></i> 新增类别</a>
                @endif
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="category-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th>ID</th>
                        <th width="700">{{trans("category.类别名称")}}</th>
                        <th>{{trans("category.新增时间")}}</th>
                        <th>{{trans("category.操作")}}</th>
                    </tr>
                    <tr role="row" class="filter">
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_id" placeholder="ID" />
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_title"
                                   placeholder="{{trans("category.类别名称")}}">
                        </td>
                        <td>
                            <div class="input-group margin-bottom-5">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_created_after" placeholder="{{trans("category.开始")}}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                            <div class="input-group">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_created_before" placeholder="{{trans("category.结束")}}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                        </td>
                        <td>
                            <div class="margin-bottom-5">
                                <button class="btn btn-info-alt filter-submit">
                                    <i class="fa fa-search"></i> {{trans("category.过滤")}}
                                </button>
                            </div>
                            <button class="btn btn-danger-alt filter-cancel">
                                <i class="fa fa-times"></i> {{trans("category.重置")}}
                            </button>
                        </td>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection