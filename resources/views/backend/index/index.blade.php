@extends('backend.layout')

@section('title')
    {{trans("sideMenu.后台首页")}}
@endsection

@section('description')

@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans("sideMenu.后台")}}</a>
        </li>
    </ul>
@endsection

@section('header')
    @if(Auth::guard('staff')->user()->staff_type == \App\Models\Staff::STAFF_TYPE_STAFF)
        <link href="{{asset('custom/css/backend/staffDashboard.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('custom/css/backend/staffHeader.css')}}" rel="stylesheet" type="text/css">
    @endif
@endsection

@section('footer')
    <script>
        +function() {
            $(document).ready(function() {
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans("sideMenu.看板")}}</span>
            </div>
            <div class="actions">

            </div>
        </div>
        <div class="portlet-body">
            {{trans("sideMenu.即将推出")}}
        </div>
    </div>
@endsection

@section('modal')

@endsection
