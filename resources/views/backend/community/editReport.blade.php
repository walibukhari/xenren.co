@extends('backend.layout')

@section('title')
    Edit Report
@endsection

@section('description')
    {{trans('permissions.crudL')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="{{route('backend.managePost')}}">Manage Edit Report</a>
        </li>
    </ul>
@endsection

@section('header')
    <!-- include libraries(jQuery, bootstrap) -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">
    <style>
        .page-container-bg-solid .page-bar, .page-content-white .page-bar {
            background-color: #fff;
            position: relative;
            padding: 0 20px;
            margin: -25px -9px 0;
        }
        .modalDialogM{
            position: relative;
            top:50px;
        }
    </style>
@endsection

@section('footer')
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.js"></script>
    <script>
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase">Edit Report</span>
            </div>
        </div>
        <div class="col-md-12">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{session()->get('success')}}
                </div>
            @endif
        </div>
        <div class="portlet-body">
            <form method="POST" action="{{route('backend.updateReportedDiscussion')}}">
                @csrf
                <input type="hidden" name="d_id" value="{{$discussion->id}}" />
                <div class="row">
                    <div class="col-md-12">
                        <label>Name : </label>
                        <input type="text" name="name" class="form-control" value="{{$discussion->name}}" />
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <labe>Body:</labe>
                        <textarea class="form-control" name="description" cols="12" rows="12">
                        @php
                            $article = $discussion->description;
                            $html =  explode( "<img>", $article );
                            $html = preg_replace('/<[^>]*>/', '', $html[0]);
                            echo $html;
                        @endphp
                    </textarea>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('modal')
@endsection
