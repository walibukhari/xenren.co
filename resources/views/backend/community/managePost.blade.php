@extends('backend.layout')

@section('title')
    Manage Posts
@endsection

@section('description')
    {{trans('permissions.crudL')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="{{route('backend.managePost')}}">Manage Posts</a>
        </li>
    </ul>
@endsection

@section('header')
    <!-- include libraries(jQuery, bootstrap) -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">
    <style>
        .page-container-bg-solid .page-bar, .page-content-white .page-bar {
            background-color: #fff;
            position: relative;
            padding: 0 20px;
            margin: -25px -9px 0;
        }
        .modalDialogM{
            position: relative;
            top:50px;
        }
    </style>
@endsection

@section('footer')
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.js"></script>
    <script>
        function getEditData(url) {
            $.ajax({
                url:url,
                type:'GET',
                success: function (response) {
                    console.log('response');
                    console.log(response.data);
                    $('#topic_name').val(response.data.topic_name);
                    $('#topic_description').val(response.data.topic_description);
                    $('#topic_id').val(response.data.id);
                },
                error: function (error) {
                    console.log('error');
                    console.log(error);
                }
            });
        }

        function deleteModal(url) {
            $('#formDelete').attr('action',url);
        }
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase">Posts</span>
            </div>
        </div>
        <div class="col-md-12">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{session()->get('success')}}
                </div>
            @endif
        </div>
        <div class="portlet-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Post Type</th>
                        <th>Post Name</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                        <tr>
                            <td>{{$post->id}}</td>
                            <td>{{$post->getPostTopicType($post->topic_type)}}</td>
                            <td>{{$post->name}}</td>
                            <td>{!! $post->description !!}</td>
                            <td>
                                @if($post->status == \App\Models\CommunityDiscussion::STATUS_START)
                                    <a href="{{ route('backend.stopPost',[$post->id]) }}" class="btn btn-success">Hide</a>
                                @else
                                    <a href="{{ route('backend.startPost',[$post->id]) }}" class="btn btn-success">Show</a>
                                @endif
                                <a href="{{ route('backend.deletePost',[$post->id]) }}" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')
@endsection
