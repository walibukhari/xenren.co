@extends('backend.layout')

@section('title')
    Topic
@endsection

@section('description')
    {{trans('permissions.crudL')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="{{route('backend.topic')}}">Topic</a>
        </li>
        <li>
            /
        </li>
        <li>
            <a href="{{ route('backend.articleCreate') }}">List</a>
        </li>
    </ul>
@endsection

@section('header')
    <!-- include libraries(jQuery, bootstrap) -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">
    <style>
        .page-container-bg-solid .page-bar, .page-content-white .page-bar {
            background-color: #fff;
            position: relative;
            padding: 0 20px;
            margin: -25px -9px 0;
        }
        .modalDialogM{
            position: relative;
            top:50px;
        }
    </style>
@endsection

@section('footer')
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.js"></script>
    <script>
        function getEditData(url) {
            $.ajax({
               url:url,
               type:'GET',
               success: function (response) {
                    console.log('response');
                    console.log(response.data);
                    $('#topic_name').val(response.data.topic_name);
                    $('#topic_description').val(response.data.topic_description);
                    $('#topic_id').val(response.data.id);
               },
               error: function (error) {
                    console.log('error');
                    console.log(error);
               }
            });
        }

        function deleteModal(url) {
            $('#formDelete').attr('action',url);
        }
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase">Topic</span>
            </div>
            <div class="actions">
                <a href="" style="text-transform:lowercase;" class="btn btn-circle red-sunglo btn-sm" data-toggle="modal" data-target="#topicModal"><i
                        class="fa fa-plus"></i>Add Topics</a>
            </div>
        </div>
        <div class="col-md-12">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{session()->get('success')}}
                </div>
            @endif
        </div>
        <div class="portlet-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($topics as $topic)
                        <tr>
                            <td>{{$topic->id}}</td>
                            <td>{{$topic->topic_name}}</td>
                            <td>{{$topic->topic_description}}</td>
                            <td>{{$topic->topic_type}}</td>
                            <td>
                                <a data-toggle="modal" onclick="getEditData('{{ route('backend.editTopic',[$topic->id]) }}')" data-target="#editModal" class="btn btn-success">Edit</a>
                                <a data-toggle="modal" onclick="deleteModal('{{ route('backend.deleteTopic',[$topic->id]) }}')" data-target="#deleteModal" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

    <!-- delete Modal -->
    <div id="deleteModal" class="modal fade" role="dialog">
        <div class="modalDialogM modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Topic</h4>
                </div>
                <form id="formDelete" action="" method="get">
                    <div class="modal-body">
                        <p>Are you sure you want to delete topic ... !</p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">Delete</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <!-- edit Modal -->
    <div id="editModal" class="modal fade" role="dialog">
        <div class="modalDialogM modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Topic</h4>
                </div>
                <form action="{{route('backend.updateTopic')}}" method="POST">
                    <div class="modal-body">
                        <input type="hidden" name="_token" value="{{csrf_token()}}" />
                        <input type="hidden" id="topic_id" name="_id" value="" />
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label>Topic Name:</label>
                                    <input type="text" name="topic_name" id="topic_name" required class="form-control" />
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label>Topic Name:</label>
                                    <select class="form-control" name="topic_type">
                                        <option selected disabled>select topic type</option>
                                        <option value="1">offical</option>
                                        <option value="2">discussion</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Topic Description:</label>
                                    <textarea name="topic_description" id="topic_description" required class="form-control" cols="7" rows="7"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Update</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div id="topicModal" class="modal fade" role="dialog">
        <div class="modalDialogM modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Topic</h4>
                </div>
                <form action="{{route('backend.createTopic')}}" method="POST">
                <div class="modal-body">
                        <input type="hidden" name="_token" value="{{csrf_token()}}" />
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label>Topic Name:</label>
                                    <input type="text" name="topic_name" required class="form-control" />
                                </div>
                            </div>
                        </div>
                        <br>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-6">
                                <label>Topic Name:</label>
                                <select class="form-control" name="topic_type">
                                    <option selected disabled>select topic type</option>
                                    <option value="1">offical</option>
                                    <option value="2">discussion</option>
                                </select>
                            </div>
                        </div>
                    </div>
                        <br>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Topic Description:</label>
                                    <textarea name="topic_description" required class="form-control" cols="7" rows="7"></textarea>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>

        </div>
    </div>
@endsection
