@extends('backend.layout')

@section('title')
    Community Report
@endsection

@section('description')
    {{trans('permissions.crudL')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="{{route('backend.managePost')}}">Manage Community Report</a>
        </li>
    </ul>
@endsection

@section('header')
    <!-- include libraries(jQuery, bootstrap) -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">
    <style>
        .page-container-bg-solid .page-bar, .page-content-white .page-bar {
            background-color: #fff;
            position: relative;
            padding: 0 20px;
            margin: -25px -9px 0;
        }
        .modalDialogM{
            position: relative;
            top:50px;
        }
    </style>
@endsection

@section('footer')
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.js"></script>
    <script>
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase">Community Report</span>
            </div>
        </div>
        <div class="col-md-12">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{session()->get('success')}}
                </div>
            @endif
        </div>
        <div class="portlet-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Report To User Id</th>
                        <th>Report By User Id</th>
                        <th>Report To</th>
                        <th>Report By</th>
                        <th>Topic Name</th>
                        <th>Discussion Id</th>
                        <th>Reply Id</th>
                        <th>Discussion Name</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($reports as $report)
                        <tr>
                            <td>{{$report->id}}</td>
                            <td>{{$report->report_to}}</td>
                            <td>{{$report->report_by}}</td>
                            <td>{{$report->reportToUsername($report->report_to)}}</td>
                            <td>{{$report->reportByUsername($report->report_by)}}</td>
                            <td>{{$report->topic_name}}</td>
                            <td>{{$report->discussion_id}}</td>
                            <td>{{isset($report->replay_id) ? $report->replay_id : '--'}}</td>
                            <td>{{$report->discussion_name}}</td>
                            <td>{{$report->comment}}</td>
                            <td>
                                <a href="{{route('backend.deleteReport',[$report->id])}}" class="btn btn-sm btn-danger">Delete</a>
                                <a href="{{route('backend.editReport',[$report->id])}}" class="btn btn-sm btn-primary">Edit</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')
@endsection
