@extends('backend.layout')

@section('title')
    {{trans('sideMenu.member_management')}}
@endsection

@section('description')
    {{trans('user.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sideMenu.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.user') }}">{{trans('sideMenu.member_management')}}</a>
        </li>
    </ul>
@endsection

@section('header')

@endsection

@section('footer')

@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('common.member_list')}}</span>
            </div>
            <div class="actions">
                <a href="{{ route('backend.user.create') }}" class="btn btn-circle red-sunglo btn-sm"
                   data-target="#remote-modal" data-toggle="modal"><i class="fa fa-plus"></i> {{ trans('common.add_member') }}</a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="user-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th width="20%">{{trans('common.email')}}</th>
                        {{--<th>{{trans('member.nickname')}}</th>--}}
                        <th>{{trans('member.real_name')}}</th>
                        <th>{{trans('common.created_at')}}</th>
                        <th>{{trans('common.status')}}</th>
                        <th>{{trans('common.actions')}}</th>
                    </tr>
                    @foreach($data as $k => $v)
                        @if(isset($v->user->email))
                        <tr role="row">
                            <td>
                                {{$v->user->email}}
                            </td>
                            {{--<td>--}}
                                {{--{{isset($v->user->name) && $v->user->name != '' ? $v->user->name  : 'N/A'}}--}}
                            {{--</td>--}}
                            <td>
                                {{isset($v->user->name) && $v->user->name != '' ? $v->user->name : 'N/A'}}
                            </td>
                            <td>
                                {{$v->status_text}}
                            </td>
                            <td>
                                {{$v->user->created_at}}
                            </td>
                            <td>
                                <a title="" href="{{route('backend.user_identity_management_detail', $v->user->id)}}" class="btn btn-xs primary uppercase blue"><i class="fa fa-eye"></i></a>
                            </td>
                        </tr>
                        @endif
                    @endforeach
                    </thead>
                </table>
                {{$data->links()}}
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection