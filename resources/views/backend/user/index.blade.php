@extends('backend.layout')

@section('title')
    {{trans('sideMenu.member_management')}}
@endsection

@section('description')
    {{trans('user.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sideMenu.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.user') }}">{{trans('sideMenu.member_management')}}</a>
        </li>
    </ul>
@endsection

@section('header')

@endsection

@section('footer')
    <script>
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#user-dt'),
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('backend.user.dt') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'email', name: 'email'},
                            {data: 'name', name: 'name'},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false}
                        ],
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('common.member_list')}}</span>
            </div>
            <div class="actions">
                <a href="{{ route('backend.user.create') }}" class="btn btn-circle red-sunglo btn-sm"
                   data-target="#remote-modal" data-toggle="modal"><i class="fa fa-plus"></i> {{ trans('common.add_member') }}</a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="user-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th>{{trans('common.id')}}</th>
                        <th width="200">{{trans('common.email')}}</th>
                        <th>{{trans('member.nickname')}}</th>
                        <th>{{trans('member.real_name')}}</th>
                        <th>{{trans('common.created_at')}}</th>
                        <th>{{trans('common.actions')}}</th>
                    </tr>
                    <tr role="row" class="filter">
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_id"
                                   placeholder="{{trans("common.id")}}"/>
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_email"
                                   placeholder="{{trans('common.email')}}">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_nick_name"
                                   placeholder="{{trans('member.nickname')}}">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_real_name"
                                   placeholder="{{trans('member.real_name')}}">
                        </td>
                        <td>
                            <div class="input-group margin-bottom-5">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_created_after" placeholder="{{trans('common.start')}}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                            <div class="input-group">  
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_created_before" placeholder="{{trans('common.end')}}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                        </td>
                        <td>
                            <div class="margin-bottom-5">
                                <button class="btn btn-info-alt filter-submit">
                                    <i class="fa fa-search"></i> {{trans('common.filter')}}
                                </button>
                            </div>
                            <button class="btn btn-danger-alt filter-cancel">
                                <i class="fa fa-times"></i> {{trans('common.reset')}}
                            </button>
                        </td>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection