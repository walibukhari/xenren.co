@extends('backend.layout')

@section('title')
    {{trans('sideMenu.member_management')}}
@endsection

@section('description')
    {{trans('user.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sideMenu.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.user') }}">{{trans('sideMenu.member_management')}}</a>
        </li>
    </ul>
@endsection

@section('header')

@endsection

@section('footer')

@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('common.member_list')}}</span>
            </div>
            <div class="actions">
                @if($data->status == \App\Models\UserIdentity::STATUS_APPROVED)
                    <a href="{{ route('backend.user_identity_management_reject', $data->id) }}" class="btn btn-circle red-sunglo btn-sm"><i class="fa fa-check"></i> Reject </a>
                @endif
                @if($data->status == \App\Models\UserIdentity::STATUS_REJECTED || $data->status == \App\Models\UserIdentity::STATUS_PENDING )
                    <a href="{{ route('backend.user_identity_management_approve', $data->id) }}" class="btn btn-circle btn-sm"><i class="fa fa-check"></i> Approve </a>
                @endif
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <br/>
                <div class="row">
                    <div class="col-md-2">
                        <label for=""><b>Real Name: </b></label>
                    </div>
                    <div class="col-md-10">
                        {{$data->name}}
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-2">
                        <label for=""><b>Identity Card No: </b></label>
                    </div>
                    <div class="col-md-10">
                        {{$data->id_card_no}}
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-2">
                        <label for=""><b>Gender: </b></label>
                    </div>
                    <div class="col-md-10">
                        {{$data->gender_text}}
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-2">
                        <label for=""><b>Birthday: </b></label>
                    </div>
                    <div class="col-md-10">
                        {{$data->date_of_birth}}
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-2">
                        <label for=""><b>Address: </b></label>
                    </div>
                    <div class="col-md-10">
                        {{$data->address}}
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-2">
                        <label for=""><b>Hand Phone: </b></label>
                    </div>
                    <div class="col-md-10">
                        {{$data->handphone_no}}
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-2">
                        <label for=""><b>Country: </b></label>
                    </div>
                    <div class="col-md-10">
                        @if(app()->getLocale() == 'cn')
                            {{$data->country->locale->full_name}}
                        @else
                            {{$data->country->name}}
                        @endif
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-2">
                        <label for=""><b>Id Card: </b></label>
                    </div>
                    <div class="col-md-10">
                        <img src="{{asset($data->id_image)}}" alt="" style="width: 250px">
                        @if(isset($data->id_image) && $data->id_image != '')
                            <i><a href="{{($data->id_image)}}" target="_blank">Enlarge</a></i>
                        @endif
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-2">
                        <label for=""><b>Holding Id Card: </b></label>
                    </div>
                    <div class="col-md-10">
                        <img src="{{asset($data->hold_id_image)}}" alt="" style="width: 250px">
                        @if(isset($data->hold_id_image) && $data->hold_id_image != '')
                            <i><a href="{{($data->hold_id_image)}}" target="_blank">Enlarge</a></i>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection