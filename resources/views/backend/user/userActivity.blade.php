@extends('backend.layout')

@section('title')
    User Activity
@endsection

@section('description')
    {{trans('managecoin.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>

        <li>
            <a href="{{ route('backend.userActivity') }}">User Activity</a>
        </li>
    </ul>
@endsection

@section('header')

@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('managecoin.dashboard')}}</span>
            </div>
        <!--  <div class="actions">
                <a href="{{ route('backend.userActivity') }}" class="btn btn-circle red-sunglo btn-sm"><i
                            class="fa fa-plus"></i> {{trans('managecoin.add_coin')}}</a>
            </div> -->
        </div>
        <div class="portlet-body">
            <div class="table-container table-responsive">
                <table class="table table-striped table-bordered table-hover table-checkable" id="manageCoin-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th>id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Skype</th>
                        <th>We Chat</th>
                        <th>Phone Number</th>
                        <th>Line</th>
                        <th>Created at</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($data) < 1)
                        <tr>
                            <td colspan="20">No Data Found</td>
                        </tr>
                    @endif
                    @foreach($data as $k => $v)
                        <tr>
                            <th>{{$v->id}}</th>
                            <th>{{isset($v->username) ? $v->username  : 'N/A'}}</th>
                            <th>{{isset($v->email) ? $v->email  : 'N/A'}}</th>
                            <th>{{isset($v->skype) ? $v->skype  : 'N/A'}}</th>
                            <th>{{isset($v->we_chat) ? $v->we_chat  : 'N/A'}}</th>
                            <th>{{isset($v->phone_number) ? $v->phone_number  : 'N/A'}}</th>
                            <th>{{isset($v->line) ? $v->line  : 'N/A'}}</th>
                            <th>{{isset($v->created_at) ? $v->created_at  : 'N/A'}}</th>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection
