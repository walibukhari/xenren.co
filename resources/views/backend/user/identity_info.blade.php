@extends('ajaxmodal')

@section('title')
    {{trans("common.identity_authentication")}}
@endsection

@section('script')
    <script>
//        +function() {
//            $(document).ready(function() {
//                $('#identity-info-form').makeAjaxForm({
//                    inModal: true,
//                    closeModal: true,
//                    submitBtn: '#btn-submit'
//                });
//            });
//        }(jQuery);
    </script>
@endsection

@section('footer')
    {{--<button type="button" id="btn-submit" class="btn btn-primary blue">{{trans("common.approve")}}</button>--}}
    {{--<button type="button" id="btn-submit" class="btn btn-primary red">{{trans("common.reject")}}</button>--}}
@endsection

@section('content')
    {!! Form::open(['method' => 'post', 'role' => 'form', 'id' => 'identity-info-form', 'files' => true]) !!}
        <div class="form-body">
            <div class="form-group">

                <div class="row">
                    <div class="col-md-5">
                        <label>{{trans('common.email')}}</label> :
                    </div>
                    <div class="col-md-7">
                        {{ $identityInfo->user->email }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <label>{{trans('member.real_name')}}</label> :
                    </div>
                    <div class="col-md-7">
                        {{ $identityInfo->name }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <label>{{trans('member.id_card_no')}}</label> :
                    </div>
                    <div class="col-md-7">
                        {{ $identityInfo->id_card_no }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <label>{{trans('member.gender')}}</label> :
                    </div>
                    <div class="col-md-7">
                        {{ $identityInfo->getGenderText() }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <label>{{trans('member.birth_date')}}</label> :
                    </div>
                    <div class="col-md-7">
                        {{ $identityInfo->date_of_birth }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <label>{{trans('member.address')}}</label> :
                    </div>
                    <div class="col-md-7">
                        {{ $identityInfo->address }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <label>{{trans('common.country')}}</label> :
                    </div>
                    <div class="col-md-7">
                        {{ $identityInfo->country != null ? $identityInfo->country->getName(): '' }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <label>{{trans('member.handphone')}}</label> :
                    </div>
                    <div class="col-md-7">
                        {{ $identityInfo->handphone_no }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 text-center">
                        <img src="{{ $identityInfo->getIdImage() }}" width="400px">
                        <br/><br/>
                    </div>
                <div>

                <div class="row">
                    <div class="col-md-12 text-center">
                        <img src="{{ $identityInfo->getHoldIdImage() }}" width="400px">
                        <br/><br/>
                    </div>
                <div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection