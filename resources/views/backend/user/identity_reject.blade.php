@extends('ajaxmodal')

@section('title')
    {{trans("common.reject")}}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                $('#identity-reject-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit'
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit" class="btn btn-primary blue">{{trans("common.confirm")}}</button>
@endsection

@section('content')
    {!! Form::open(['route' => ['backend.user.reject.post', 'request_id' => $id ], 'method' => 'post', 'role' => 'form', 'id' => 'identity-reject-form', 'files' => true]) !!}
        <div class="form-body">
            <div class="form-group">
                <label>{{trans("common.reason")}}</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-building"></i>
                    </span>
                    {!! Form::text('reject_reason', '', ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection