@extends('backend.layout')

@section('title')
    {{trans('sideMenu.提交个人身份管理')}}
@endsection

@section('description')
    {{trans('common.approve_reject_member_id')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('order.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.user.submitid') }}"><?= Lang::get('sideMenu.提交个人身份管理'); ?></a>
        </li>
    </ul>
@endsection

@section('header')

@endsection

@section('footer')
    <script>
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#submitid-dt'),
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('backend.user.submitid.dt') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'email', name: 'email'},
                            {data: 'name', name: 'name'},
                            {data: 'id_card_no', name: 'id_card_no'},
                            {data: 'gender_text', name: 'gender_text'},
                            {data: 'date_of_birth', name: 'date_of_birth'},
                            {data: 'address', name: 'address'},
                            {data: 'handphone_no', name: 'handphone_no'},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false}
                        ],
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"><?= Lang::get('common.member_id_list'); ?></span>
            </div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="submitid-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th>ID</th>
                        <th>{{trans('common.email')}}</th>
                        <th><?= Lang::get('member.real_name'); ?></th>
                        <th><?= Lang::get('member.id_card_no'); ?></th>
                        <th><?= Lang::get('member.gender'); ?></th>
                        <th><?= Lang::get('member.birth_date'); ?></th>
                        <th><?= Lang::get('member.address'); ?></th>
                        <th><?= Lang::get('member.handphone'); ?></th>
                        <th>{{trans('common.created_at')}}</th>
                        <th>{{trans('common.actions')}}</th>
                    </tr>
                    <tr role="row" class="filter">
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_id"
                                   placeholder="{{trans('common.actions')}}"/>
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_email"
                                   placeholder="{{trans('common.email')}}">
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <div class="input-group margin-bottom-5">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly name="filter_created_after" placeholder="开始">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                            <div class="input-group">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly name="filter_created_before" placeholder="结束">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                        </td>
                        <td>
                            <div class="margin-bottom-5">
                                <button class="btn btn-info-alt filter-submit">
                                    <i class="fa fa-search"></i> {{trans('skill.过滤')}}
                                </button>
                            </div>
                            <button class="btn btn-danger-alt filter-cancel">
                                <i class="fa fa-times"></i> {{trans('skillreview.重置')}}
                            </button>
                        </td>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection