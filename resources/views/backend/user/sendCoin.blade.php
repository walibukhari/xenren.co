@extends('ajaxmodal')

@section('title')
    {{trans("common.send_coin")}}
@endsection

@section('script')
    <script>
        +function () {
            $(document).ready(function () {
                $('#send-coin-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit'
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit" class="btn green">{{ trans('common.send_coin') }}</button>
@endsection

@section('content')
    {!! Form::open(['route' => ['backend.user.sendcoin.post', 'id' => $user->id], 'method' => 'post', 'role' => 'form', 'id' => 'send-coin-form']) !!}
    <div class="form-body">

        <div class="row">
            <div class="col-md-12">
			<div class="form-group">
			  {!! Form::label('coin_type','Coin Type',['class'=>'control-label']) !!}
				 <select class="form-control" name="coin_type">
    @foreach($coins as $item)
      <option value="{{$item->id}}">{{$item->coins}}</option>
    @endforeach
  </select>
   @foreach($coins as $item)
       {{ Form::hidden('coin_type_name',  $item->coins, array('id' => 'invisible_id')) }}
    @endforeach
 
  </div>
                <div class="form-group">
                    {!! Form::label('coins','Number of Coin',['class'=>'control-label']) !!}
                    {!! Form::number('number_of_coins',Input::old('coins'),['class'=>'form-control', 'placeholder' => 'Enter Number of Coins']) !!}
                </div>
				
            </div>
        </div>

    </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection