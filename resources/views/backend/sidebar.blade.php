@php
$isOwner = Auth::guard('staff') && Auth::guard('staff')->user()->staff_type === \App\Models\Staff::STAFF_TYPE_STAFF && isset($ownerOfficeId);

$isOwnerNoId = Auth::guard('staff') && Auth::guard('staff')->user()->staff_type === \App\Models\Staff::STAFF_TYPE_STAFF;

$is_operator = Auth::guard('staff') && Auth::guard('staff')->user()->staff_type === \App\Models\Staff::STAFF_TYPE_STAFF && isset($ownerOfficeId) && Auth::guard('staff')->user()->is_operator === \App\Models\Staff::STAFF_TYPE_IS_OPERATOR;
if(Auth::guard('staff')->user()->is_operator) {
    $operator_assign_office_id = Auth::guard('staff')->user()->office_id;
}

@endphp
<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-light " data-keep-expanded="false"
            data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper hide">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler"></div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            @if ($isOwner  && Auth::guard('staff')->user()->hasPermission('manage_sharedoffice_dashboard') && Auth::guard('staff')->user()->is_operator == 0)
                <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.sharedofficedashboard']) ? ' active' : ''  }}">
                    <a style="border-bottom: transparent !important;" href="{{ route('backend.sharedofficedashboard',[$ownerOfficeId]) }}" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">{{trans('sideMenu.dashboard')}}</span>
                    </a>
                </li>
            @endif

            @if($is_operator && Auth::guard('staff')->user()->hasPermission('manage_sharedoffice_dashboard') && Auth::guard('staff')->user()->is_operator == 1)
                <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.sharedofficedashboard']) ? ' active' : ''  }}">
                    <a style="border-bottom: transparent !important;" href="{{ route('backend.sharedofficedashboard',[$operator_assign_office_id]) }}" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">{{trans('sideMenu.dashboard')}}</span>
                    </a>
                </li>
            @endif

            @if(Auth::guard('staff')->user()->staff_type == \App\Models\Staff::STAFF_TYPE_ADMIN)
                <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.sharedofficeOwnerProgress']) ? ' active' : ''  }}">
                    <a style="border-bottom: transparent !important;" href="{{ route('backend.sharedofficeOwnerProgress') }}" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">Check Owner Progress</span>
                    </a>
                </li>
            @endif

            @if(Auth::guard('staff')->user()->is_operator == 1)
                <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.index.operator']) ? ' active' : ''  }}">
                    <a style="border-bottom: transparent !important;" href="{{ route('backend.index.operator') }}" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">{{trans('sideMenu.dashboard')}}</span>
                    </a>
                </li>
            @endif

            @if(Auth::guard('staff')->user()->hasPermission('manage_sharedoffice_dashboard') && Auth::guard('staff')->user()->staff_type == \App\Models\Staff::STAFF_TYPE_ADMIN)
            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.index']) ? ' active' : ''  }}">
                <a style="border-bottom: transparent !important;" href="{{ route('backend.index') }}" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">{{trans('sideMenu.dashboard')}}</span>
                </a>
            </li>
            @endif

            @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_staff'))
            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.staff']) ? ' active' : ''  }}">
                <a href="{{ route('backend.staff') }}" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">{{trans('sideMenu.操作员')}}</span>
                </a>
            </li>
            @endif

            @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('dispute_requests'))
            <li class="nav-item start{{ Route::currentRouteName() == 'backend.disputes' ? ' active' : '' }}">
                <a href="{{ route('backend.disputes') }}" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">{{trans('sideMenu.dispute_requests')}}</span>
                </a>
            </li>
            @endif

            @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('withdraw_request'))
                <li class="nav-item start{{ Route::currentRouteName() == 'backend.withdraw.requests' ? ' active' : '' }}">
                    <a href="{{ route('backend.withdraw.requests') }}" class="nav-link nav-toggle">
                        <i class="fa fa-hand-lizard-o"></i>
                        <span class="title">{{trans('sideMenu.withdraw_requests')}}</span>
                    </a>
                </li>
            @endif

            @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_sharedoffice_working_sheet'))
                <li class="nav-item start{{ Route::currentRouteName() == 'backend.working_sheet' ? ' active' : '' }}">
                    <a id="finance" class                                                                                                                                                                                                                                                                       ="nav-link nav-toggle">
                        <i class="fa fa-file-text-o" aria-hidden="true"></i>
                        <span class="title">{{trans('sideMenu.finance')}}</span>
                    </a>

                    <ul style="display: none;" class="drop-down-menu-finance">
                        <li class="nav-item start{{ Route::currentRouteName() == 'backend.working_sheet' ? ' active' : '' }}">
                            <a href="{{ route('backend.working_sheet') }}" id="finance" class="nav-link nav-toggle">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                <span class="title">{{trans('sideMenu.working_sheet')}}</span>
                            </a>
                        </li>
                    </ul>
                </li>
            @endif

            @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('milestone_confirmation'))
            <li class="nav-item start{{ Route::currentRouteName() == 'backend.milestone_confirmation' ? ' active' : '' }}">
                <a  id="milestone" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">{{trans('sideMenu.mile_stone')}}</span>
                </a>

                <ul class="drop-down-menu-milestone" style="display: none;">
                    <li class="nav-item start{{ Route::currentRouteName() == 'backend.milestone_confirmation' ? ' active' : '' }}">
                        <a href="{{ route('backend.milestone_confirmation') }}" class="nav-link nav-toggle">
                            <i class="icon-home"></i>
                            <span class="title">{{trans('sideMenu.milestone_confirmation')}}</span>
                        </a>
                    </li>
                    <li class="nav-item start{{ Route::currentRouteName() == 'backend.show_milestone' ? ' active' : '' }}">
                        <a href="{{ route('backend.show_milestone') }}" class="nav-link nav-toggle">
                            <i class="icon-home"></i>
                            <span class="title">{{trans('sideMenu.change_release_date')}}</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endif

            @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_article'))
                <li class="nav-item start{{ Route::currentRouteName() == 'backend.articleList' ? ' active' : '' }}">
                    <a href="{{ route('backend.articleList') }}" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title">{{trans('sideMenu.article')}}</span>
                    </a>
                </li>
            @endif

            @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_community'))
                <li class="nav-item start{{ Route::currentRouteName() == 'backend.topic' ? ' active' : '' }}">
                    <a id="community" class="nav-link nav-toggle">
                        <i class="icon-home"></i>
                        <span class="title">{{trans('sideMenu.community')}}</span>
                    </a>

                    <ul class="drop-down-menu-community" style="display:none;">
                        <li class="nav-item start{{ Route::currentRouteName() == 'backend.topic' ? ' active' : '' }}">
                            <a href="{{ route('backend.topic') }}" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">{{trans('common.add_topics')}}</span>
                            </a>
                        </li>
                        <li class="nav-item start{{ Route::currentRouteName() == 'backend.managePost' ? ' active' : '' }}">
                            <a href="{{ route('backend.managePost') }}" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">{{trans('sideMenu.manage_posts')}}</span>
                            </a>
                        </li>
                        <li class="nav-item start{{ Route::currentRouteName() == 'backend.communityReports' ? ' active' : '' }}">
                            <a href="{{ route('backend.communityReports') }}" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">{{trans('sideMenu.reports')}}</span>
                            </a>
                        </li>
                    </ul>
                </li>
            @endif

            @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_permission_groups'))
            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.permissions', 'backend.permissions.create', 'backend.permissions.edit']) ? ' active' : ''  }}">
                <a href="{{ route('backend.permissions') }}" class="nav-link nav-toggle">
                    <i class="fa fa-key"></i>
                    <span class="title">{{trans('sideMenu.权限组')}}</span>
                </a>
            </li>
            @endif

            @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_badwords'))
                <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.badwords']) ? ' active' : ''  }}">
                    <a href="{{ route('backend.badwords') }}" class="nav-link nav-toggle">
                        <i class="fa fa-ban"></i>
                        <span class="title">{{trans('sideMenu.bad_filter')}}</span>
                    </a>
                </li>
            @endif


            @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_user'))
                <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.user']) ? ' active' : ''  }}">
                    <a id="management" class="nav-link nav-toggle">
                        <i class="fa fa-user"></i>
                        <span class="title">{{trans('sideMenu.management')}}</span>
                    </a>

                    <ul class="drop-down-menu-management" style="display: none;">
                        <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.user']) ? ' active' : ''  }}">
                            <a href="{{ route('backend.user') }}" id="managment" class="nav-link nav-toggle">
                                <i class="fa fa-user"></i>
                                <span class="title">{{trans('sideMenu.member_management')}}</span>
                            </a>
                        </li>
                        <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.user_identity_management']) ? ' active' : ''  }}">
                            <a href="{{ route('backend.user_identity_management') }}" class="nav-link nav-toggle">
                                <i class="fa fa-user"></i>
                                <span class="title">{{trans('sideMenu.iden_manage')}}</span>
                            </a>
                        </li>
                        @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_feedback'))
                            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.feedback']) ? ' active' : '' }}">
                                <a href="{{ route('backend.feedback') }}" class="nav-link nav-toggle">
                                    <i class="fa fa-pencil-square-o"></i>
                                    <span class="title">{{trans('sideMenu.feedback_management')}}</span>
                                </a>
                            </li>
                        @endif

                        @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('language_management'))
                            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.languages']) ? ' active' : '' }}">
                                <a href="{{ route('backend.languages') }}" class="nav-link nav-toggle">
                                    <i class="fa fa-language"></i>
                                    <span class="title">{{trans('sideMenu.language_management')}}</span>
                                </a>
                            </li>
                        @endif

                        @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('country_management'))
                            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.countries']) ? ' active' : '' }}">
                                <a href="{{ route('backend.countries') }}" class="nav-link nav-toggle">
                                    <i class="fa fa-globe"></i>
                                    <span class="title">{{trans('sideMenu.country_management')}}</span>
                                </a>
                            </li>
                        @endif

                        @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_country_language') )
                            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.countrylanguage']) ? ' active' : '' }}">
                                <a href="{{ route('backend.countrylanguage') }}" class="nav-link nav-toggle">
                                    <i class="fa fa-comments"></i>
                                    <span class="title">{{trans('sideMenu.country_language_management')}}</span>
                                </a>
                            </li>
                        @endif

                        @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_category'))
                            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.category', 'backend.category.create', 'backend.category.edit']) ? ' active' : '' }}">
                                <a href="{{ route('backend.category') }}" class="nav-link nav-toggle">
                                    <i class="fa fa-bookmark"></i>
                                    <span class="title">{{trans('sideMenu.类别管理')}}</span>
                                </a>
                            </li>
                        @endif

                        @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_skill_keyword'))
                            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.skillkeyword']) ? ' active' : '' }}">
                                <a href="{{ route('backend.skillkeyword') }}" class="nav-link nav-toggle">
                                    <i class="fa fa-file-text-o"></i>
                                    <span class="title">{{trans('sideMenu.技能关键字管理')}}</span>
                                </a>
                            </li>
                        @endif

                        @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_skill'))
                            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.skill', 'backend.skill.create', 'backend.skill.edit']) ? ' active' : '' }}">
                                <a href="{{ route('backend.skill') }}" class="nav-link nav-toggle">
                                    <i class="fa fa-graduation-cap"></i>
                                    <span class="title">{{trans('sideMenu.技能管理')}}</span>
                                </a>
                            </li>
                        @endif

                        @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_job_position'))
                            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.jobposition', 'backend.jobposition.create', 'backend.jobposition.edit']) ? ' active' : '' }}">
                                <a href="{{ route('backend.jobposition') }}" class="nav-link nav-toggle">
                                    <i class="fa fa-user-secret"></i>
                                    <span class="title">{{trans('sideMenu.主职管理')}}</span>
                                </a>
                            </li>
                        @endif

                        @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_project'))
                            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.project', 'backend.project.create', 'backend.project.edit']) ? ' active' : '' }}">
                                <a href="{{ route('backend.project') }}" class="nav-link nav-toggle">
                                    <i class="fa fa-briefcase"></i>
                                    <span class="title">{{trans('sideMenu.项目管理')}}</span>
                                </a>
                            </li>
                        @endif

                        @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_official_project'))
                            <li class="nav-item{{ in_array($_G['route_name'], ['backend.officialproject', 'backend.officialproject.create']) ? ' active' : '' }}">
                                <a href="{{ route('backend.officialproject') }}" class="nav-link nav-toggle">
                                    <i class="fa fa-briefcase"></i>
                                    <span class="title">{{trans('sideMenu.官方项目管理')}}</span>
                                </a>
                            </li>
                        @endif

                        @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_bank'))
                            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.bank']) ? ' active' : '' }}">
                                <a href="{{ route('backend.bank') }}" class="nav-link nav-toggle">
                                    <i class="fa fa-building"></i>
                                    <span class="title">{{trans('sideMenu.银行管理')}}</span>
                                </a>
                            </li>
                        @endif

                        @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_level'))
                            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.level']) ? ' active' : '' }}">
                                <a href="{{ route('backend.level') }}" class="nav-link nav-toggle">
                                    <i class="fa fa-building"></i>
                                    <span class="title">{{trans('sideMenu.水平管理')}}</span>
                                </a>
                            </li>
                        @endif


                    </ul>
                </li>
            @endif


            @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_payments'))
                <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.category', 'backend.category.create', 'backend.category.edit']) ? ' active' : '' }}">
                    <a href="{{ route('backend.pendingwithdrawals') }}" class="nav-link nav-toggle">
                        <i class="fa fa-money"></i>
                        <span class="title">{{trans('sideMenu.pend_manage')}}</span>
                    </a>
                </li>
            @endif


            @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_skill_review'))
                <li class="nav-item{{ in_array($_G['route_name'], ['backend.skillreview', 'backend.skillreview.view', 'backend.skillreview.comment.create', 'backend.skillreview.comment.edit']) ? ' active' : '' }}">
                    <a href="{{ route('backend.skillreview') }}" class="nav-link nav-toggle">
                        <i class="fa fa-wrench"></i>
                        <span class="title">{{trans('sideMenu.技能认证')}}</span>
                    </a>
                </li>
            @endif

            @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_withdraw'))
                <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.withdraw']) ? ' active' : '' }}">
                    <a href="{{ route('backend.withdraw') }}" class="nav-link nav-toggle">
                        <i class="fa fa-hand-lizard-o"></i>
                        <span class="title"><?= Lang::get('member.withdraw'); ?></span>
                    </a>
                </li>
            @endif

            @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_sliders'))
                <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.themes', 'backend.themes.slider', 'backend.themes.slider.create', 'backend.themes.slider.edit']) ? ' active' : ''  }}">
                    <a href="{{ route('backend.themes') }}" class="nav-link nav-toggle">
                        <i class="fa fa-gear"></i>
                        <span class="title">{{trans('sideMenu.themes_panel')}}</span>
                    </a>
                </li>
            @endif


            @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('submit_id'))
                <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.user.submitid']) ? ' active' : '' }}">
                    <a href="{{ route('backend.user.submitid') }}" class="nav-link nav-toggle">
                        <i class="fa fa-thumbs-o-up"></i>
                        <span class="title"><?= Lang::get('member.submit_id'); ?></span>
                    </a>
                </li>
            @endif

            @if ($is_operator || Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_sharedoffice') )
                <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.sharedoffice']) ? ' active' : '' }}">
                    <a id="sharedOffice" class="nav-link nav-toggle">
                        <i class="fa fa-share-alt" aria-hidden="true"></i>
                        <span class="title">{{trans('sideMenu.shared_office')}}</span>
                    </a>

                    <ul class="drop-down-menu-shared-office" style="display: none;">
                        @if(Auth::guard('staff')->user()->hasPermission('manage_sharedoffice'))
                        <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.sharedoffice']) ? ' active' : '' }}">
                            <a href="{{ route('backend.sharedoffice') }}" id="sharedOffice" class="nav-link nav-toggle">
                                <i class="fa fa-share-alt" aria-hidden="true"></i>
                                <span class="title">{{trans('sideMenu.shared_office')}}</span>
                            </a>
                        </li>
                        @endif
                        @if (Auth::guard('staff')->user()->hasPermission('manage_sharedoffice_checkin') )
                            <li class="nav-item{{ in_array(Route::currentRouteName(), ['sharedofficeCheckIn']) ? ' active' : '' }}">
                                <a href="{{ route('sharedofficeCheckIn') }}" class="nav-link nav-toggle">
                                    <i class="fa fa-check-circle" aria-hidden="true"></i>
                                    <span class="title">{{trans('sideMenu.check_in_user_sharedOffice')}}</span>
                                </a>
                            </li>
                        @endif
                        @if (Auth::guard('staff')->user()->hasPermission('manage_sharedofficebarcodes') )
                            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.sharedofficebarcodes']) ? ' active' : '' }}">
                                @if($isOwnerNoId)
                                    <a href="{{ route('backend.sharedofficebarcodes',[$isOwnerNoId]) }}" class="nav-link nav-toggle">
                                        <i class="fa fa-barcode" aria-hidden="true"></i>
                                        <span class="title">{{trans('sideMenu.sharedofficebarcodes')}}</span>
                                    </a>
                                @else
                                    <a href="{{ route('backend.sharedofficebarcodes',[0]) }}" class="nav-link nav-toggle">
                                        <i class="fa fa-barcode" aria-hidden="true"></i>
                                        <span class="title">{{trans('sideMenu.sharedofficebarcodes')}}</span>
                                    </a>
                                @endif
                            </li>
                        @endif
                        @if ($isOwnerNoId && Auth::guard('staff')->user()->hasPermission('manage_sharedoffice'))
                            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.sharedofficeProfile']) ? ' active' : '' }}">
                                <a href="{{ route('backend.sharedofficeProfile') }}" class="nav-link nav-toggle">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                    <span class="title">{{trans('sideMenu.sharedofficeprofileupdate')}}</span>
                                </a>
                            </li>
                        @endif
                        @if (Auth::guard('staff')->user()->hasPermission('manage_sharedoffice_notifications') )
                        <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.sharedofficeNotification']) ? ' active' : '' }}">
                            <a href="{{ route('backend.sharedofficeNotification') }}" class="nav-link nav-toggle">
                                <i class="fa fa-bell" aria-hidden="true"></i>
                                <span class="title">{{trans('sideMenu.sharedofficeNotifications')}}</span>
                            </a>
                        </li>
                        @endif
                        @if ($isOwner && Auth::guard('staff')->user()->hasPermission('manage_sharedoffice'))
                            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.sharedofficeOwnerEdit']) ? ' active' : '' }}">
                                <a href="{{ route('backend.sharedofficeOwnerEdit',[$ownerOfficeId]) }}" class="nav-link nav-toggle">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                    <span class="title">{{trans('sideMenu.sharedofficeOwnerEdit')}}</span>
                                </a>
                            </li>
                        @endif
                        @if ($isOwner && Auth::guard('staff')->user()->hasPermission('manage_sharedoffice_owner_request'))
                            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.sharedofficeOwnerBookedRequests']) ? ' active' : '' }}">
                                <a href="{{ route('backend.sharedofficeOwnerBookedRequests',[$ownerOfficeId]) }}" class="nav-link nav-toggle">
                                    <img src="/images/icons/chair.png" style="padding-right:10px;">
                                    <span class="title">{{trans('sideMenu.office_request_owner')}}</span>
                                </a>
                            </li>
                        @endif
                        @if ($isOwner && Auth::guard('staff')->user()->hasPermission('manage_sharedoffice_owner_request'))
                            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.sharedofficeOwnerBookedRequests']) ? ' active' : '' }}">
                                <a href="{{ route('backend.sharedofficeChangeBookedRequests',[$ownerOfficeId]) }}" class="nav-link nav-toggle">
                                    <img src="/images/icons/chair.png" style="padding-right:10px;">
                                    <span class="title">{{trans('sideMenu.change_booking_request')}}</span>
                                </a>
                            </li>
                        @endif
                        @if ($isOwner && Auth::guard('staff')->user()->hasPermission('manage_sharedofficeproducts'))
                            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.sharedofficeOwnerProducts']) ? ' active' : '' }}">
                                <a href="{{ route('backend.sharedofficeOwnerProducts',[$ownerOfficeId]) }}" class="nav-link nav-toggle">
                                    <img src="/images/icons/chair.png" style="padding-right:10px;">
                                    <span class="title">{{trans('sideMenu.sharedofficeOwnerProducts')}}</span>
                                </a>
                            </li>
                        @endif

                        @if ($isOwner && Auth::guard('staff')->user()->hasPermission('manage_sharedoffice_images'))
                            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.sharedofficeOwnerAddImages']) ? ' active' : '' }}">
                                <a href="{{ route('backend.sharedofficeOwnerAddImages',[$ownerOfficeId]) }}" class="nav-link nav-toggle">
                                    <i class="fa fa-file-image-o" aria-hidden="true"></i>
                                    <span class="title">{{trans('sideMenu.sharedofficeOwnerAddImages')}}</span>
                                </a>
                            </li>
                        @endif
                        @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_sharedoffice_request') )
                            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.sharedofficerequest']) ? ' active' : '' }}">
                                <a href="{{ route('backend.sharedofficerequest') }}" class="nav-link nav-toggle">
                                    <i class="fa fa-building"></i>
                                    <span class="title">{{trans('sideMenu.shared_office_request')}}</span>
                                </a>
                            </li>
                        @endif
                        @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_sharedoffice_request') )
                            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.sharedofficebookingrequest']) ? ' active' : '' }}">
                                <a href="{{ route('backend.sharedofficebookingrequest') }}" class="nav-link nav-toggle">
                                    <i class="fa fa-building"></i>
                                    <span class="title">{{trans('sideMenu.booking_request')}}</span>
                                </a>
                            </li>
                        @endif
                        @if (\Auth::guard('staff')->user()->staff_type == \App\Models\Staff::STAFF_TYPE_ADMIN)
                            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.sharedofficeCategories']) ? ' active' : '' }}">
                                <a href="{{ route('backend.sharedofficeCategories') }}" class="nav-link nav-toggle">
                                    <i class="fa fa-building"></i>
                                    <span class="title">{{trans('sideMenu.cate')}}</span>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif

            @if ($isOwner && Auth::guard('staff')->user()->hasPermission('manage_sharedoffice_setting'))
                <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.sharedOfficeSettings']) ? ' active' : '' }}">
                    <a href="{{ route('backend.sharedOfficeSettings') }}" class="nav-link nav-toggle">
                        <i class="fa fa-cog" aria-hidden="true"></i>
                        <span class="title">{{trans('common.setting')}}</span>
                    </a>
                </li>
            @endif

            @if ($isOwner && Auth::guard('staff')->user()->hasPermission('manage_sharedoffice_invoices'))
                <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.sharedOfficeInvoices']) ? ' active' : '' }}">
                    <a href="{{ route('backend.sharedOfficeInvoices') }}" class="nav-link nav-toggle">
                        <i class="fa fa-files-o" aria-hidden="true"></i>
                        <span class="title">{{trans('sideMenu.invoices')}}</span>
                    </a>
                </li>
            @endif
            @if ($isOwner && Auth::guard('staff')->user()->hasPermission('manage_sharedoffice'))
                <li class="nav-item start{{ Route::currentRouteName() == 'backend.addPhoneNumber' ? ' active' : '' }}">
                    <a href="{{ route('backend.addPhoneNumber') }}" id="operator" class="nav-link nav-toggle">
                        <i class="fa fa-phone"></i>
                        <span class="title">{{trans('sideMenu.set_phone')}}</span>
                    </a>
                </li>
            @endif
            @if ($isOwner)
                <li class="nav-item{{ in_array(Route::currentRouteName(), ['staff.mobile.logout']) ? ' active' : '' }}">
                    <a href="{{ route('staff.mobile.logout') }}" class="nav-link nav-toggle">
                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                            <span class="title">{{trans('sideMenu.logout')}}</span>
                    </a>
                </li>
            @endif

            @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_sharedoffice_request') )
                <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.hiringsrequest']) ? ' active' : '' }}">
                    <a href="{{ route('backend.hiringsrequest') }}" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title">{{trans('sideMenu.hiring_request')}}</span>
                    </a>
                </li>
            @endif
            @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_bullet_ins') )
                <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.bulletin']) ? ' active' : '' }}">
                    <a href="{{ route('backend.bulletin') }}" class="nav-link nav-toggle">
                        <i class="fa fa-check-circle"></i>
                        <span class="title">{{trans('common.bulletin')}}</span>
                    </a>
                </li>
            @endif

            @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_bullet_ins') )
                <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.faqs']) ? ' active' : '' }}">
                    <a href="{{ route('backend.faqs') }}" class="nav-link nav-toggle">
                        <i class="fa fa-check-circle"></i>
                        <span class="title">{{trans('common.faq')}}</span>
                    </a>
                </li>
            @endif

            @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_coins') )
                <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.manageCoins']) ? ' active' : '' }}">
                    <a id="coins" class="nav-link nav-toggle">
                        <i class="fa fa-money"></i>
                        <span class="title">{{trans('sideMenu.coins')}}</span>
                    </a>

                    <ul class="drop-down-menu-coins" style="display: none;">
                        <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.manageCoins']) ? ' active' : '' }}">
                            <a href="{{ route('backend.manageCoins') }}" id="coins" class="nav-link nav-toggle">
                                <i class="fa fa-money"></i>
                                <span class="title">{{trans('sideMenu.coins_manage')}}</span>
                            </a>
                        </li>

                        <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.manageCoins']) ? ' active' : '' }}">
                            <a href="{{ route('backend.manageUserCoins') }}" class="nav-link nav-toggle">
                                <i class="fa fa-money"></i>
                                <span class="title">{{trans('sideMenu.user_coins_manage')}}</span>
                            </a>
                        </li>

                        <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.giveCoin']) ? ' active' : '' }}">
                            <a href="{{ route('backend.giveCoin') }}" class="nav-link nav-toggle">
                                <i class="fa fa-money"></i>
                                <span class="title">{{trans('sideMenu.give_coin')}}</span>
                            </a>
                        </li>
                        <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.coin']) ? ' active' : '' }}">
                            <a href="{{ route('backend.coin') }}" class="nav-link nav-toggle">
                                <i class="fa fa-money"></i>
                                <span class="title">{{trans('sideMenu.coin')}}</span>
                            </a>
                        </li>

                    </ul>
                </li>
            @endif
            @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_user') )
				 <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.userfeedback']) ? ' active' : '' }}">
                    <a id="usersU" class="nav-link nav-toggle">
                        <i class="fa fa-money"></i>
                        <span class="title">{{trans('sideMenu.users')}}</span>
                    </a>
                     <ul class="drop-down-menu-users" style="display: none;">
                         <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.userfeedback']) ? ' active' : '' }}">
                             <a href="{{ route('backend.userfeedback') }}" class="nav-link nav-toggle">
                                 <i class="fa fa-money"></i>
                                 <span class="title">{{trans('sideMenu.user_feedback')}}</span>
                             </a>
                         </li>

                         <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.userActivity']) ? ' active' : '' }}">
                             <a href="{{ route('backend.userActivity') }}" class="nav-link nav-toggle">
                                 <i class="fa fa-money"></i>
                                 <span class="title">{{trans('sideMenu.user_activity')}}</span>
                             </a>
                         </li>
                     </ul>
                </li>
            @endif

            @if (Auth::guard('staff') && Auth::guard('staff')->user()->staff_type == \App\Models\Staff::STAFF_TYPE_ADMIN )
                <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.pushNotification']) ? ' active' : '' }}">
                    <a href="{{ route('backend.pushNotification') }}" class="nav-link nav-toggle">
                        <i class="fa fa-bell-o"></i>
                        <span class="title">{{trans('sideMenu.push_notification')}}</span>
                    </a>
                </li>
            @endif

            @if (Auth::guard('staff') && Auth::guard('staff')->user()->staff_type == \App\Models\Staff::STAFF_TYPE_ADMIN )
                <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.pushNotification']) ? ' active' : '' }}">
                    <a href="{{ route('backend.reset-user') }}" class="nav-link nav-toggle">
                        <i class="fa fa-bell-o"></i>
                        <span class="title">{{trans('sideMenu.reset_user')}}</span>
                    </a>
                </li>
            @endif

            @if (Auth::guard('staff') && Auth::guard('staff')->user()->hasPermission('manage_coins') )
                <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.manageCoins']) ? ' active' : '' }}">
                    <a href="{{ route('backend.manageSettings') }}" class="nav-link nav-toggle">
                        <i class="fa fa-gear"></i>
                        <span class="title">{{trans('common.setting')}}</span>
                    </a>
                </li>
            @endif

        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->
