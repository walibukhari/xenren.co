@extends('backend.layout')

@section('title')
    {{trans('common.skill_management')}}
@endsection

@section('description')
    {{trans('common.skill_crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans("common.backend")}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.skill') }}">{{trans('common.skill_management')}}</a>
        </li>
    </ul>
@endsection

@section('header')

@endsection

@section('footer')
    <script>
        var url = "{{ route('backend.skill.dt') }}";
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#skill-dt'),
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": url,
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'names', name: 'names', orderable: false, searchable: false},
                            {data: 'status', name: 'status', orderable: false, searchable: false},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false}
                        ],
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('common.skill_list')}}</span>
            </div>
            <div class="actions">
                <a href="{{ route('frontend.downloadcsv', ['table' => 'skill' ]) }}" class="btn btn-circle blue btn-sm">
                    <i class="fa fa-file-o"></i>
                    {{trans('common.download_csv')}}
                </a>
                <a href="{{ route('backend.skill.create') }}" class="btn btn-circle red-sunglo btn-sm"
                   data-target="#remote-modal" data-toggle="modal"><i class="fa fa-plus"></i> {{trans('common.add_skill')}}
                </a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="skill-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th width="80">{{trans('common.id')}}</th>
                        <th>{{trans('common.skill_name')}}</th>
                        <th width="200">{{trans("common.status")}}</th>
                        <th width="200">{{trans('common.created_at')}}</th>
                        <th width="200">{{trans('common.actions')}}</th>
                    </tr>
                    <tr role="row" class="filter">
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_id"
                                   placeholder="{{trans('common.id')}}"/>
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_name"
                                   placeholder="{{trans('common.skill_name')}}">
                        </td>
                        <td>
                            {!! Form::select('filter_status', [0 => trans('common.all'), 1 => trans('common.active'), 2 => trans('common.inactive')], 0, ['class' => 'form-control form-filter input-sm', 'placeholder' => trans('common.status')]) !!}
                        </td>
                        <td>
                            <div class="input-group margin-bottom-5">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_created_after" placeholder="{{trans('common.start')}}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                            <div class="input-group">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_created_before" placeholder="{{trans('common.end')}}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                        </td>
                        <td>
                            <div class="margin-bottom-5">
                                <button class="btn btn-info-alt filter-submit">
                                    <i class="fa fa-search"></i> {{trans('common.filter')}}
                                </button>
                            </div>
                            <button class="btn btn-danger-alt filter-cancel">
                                <i class="fa fa-times"></i> {{trans('common.reset')}}
                            </button>
                        </td>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection