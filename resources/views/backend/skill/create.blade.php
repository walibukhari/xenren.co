@extends('ajaxmodal')

@section('title')
    {{trans('common.add_skill')}}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                $('#skill-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit-skill'
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit-skill" class="btn btn-primary blue">{{trans('common.add')}}</button>
@endsection

@section('content')
    {!! Form::open(['route' => ['backend.skill.create.post'], 'method' => 'post', 'role' => 'form', 'id' => 'skill-form', 'files' => true]) !!}
        <div class="form-body">
            <div class="form-group">
                <label>{{trans('common.skill_name_cn')}}</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-building"></i>
                    </span>
                    {!! Form::text('name_cn', '', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>{{trans('common.skill_link_cn')}}</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-link"></i>
                    </span>
                    {!! Form::text('reference_link_cn', '', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>{{trans('common.description_cn')}}</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-building"></i>
                    </span>
                    {!! Form::text('description_cn', '', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>{{trans('common.skill_name_en')}}</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-building"></i>
                    </span>
                    {!! Form::text('name_en', '', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>{{trans('common.skill_link_en')}}</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-link"></i>
                    </span>
                    {!! Form::text('reference_link_en', '', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>{{trans('common.description_en')}}</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-building"></i>
                    </span>
                    {!! Form::text('description_en', '', ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection