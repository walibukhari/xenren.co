@extends('backend.layout')

@section('title')
    {{ trans('sideMenu.language_management') }}
@endsection

@section('description')
    {{trans('common.edit_language')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('bank.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.languages') }}">{{trans('sideMenu.language_management')}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <link href="/custom/plugins/colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css"
          rel="stylesheet" type="text/css"/>
@endsection

@section('footer')
    <script src="/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js"></script>
    <script src="/custom/plugins/colorpicker/js/colorpicker.js"></script>
    <script>
        $(function () {
            $('.apply-editable').editable({
                params: function (params) {
                    // add additional params from data-attributes of trigger element
                    params._token = $("#_token").data("token");
                    return params;
                },
                mode: 'inline'
            });
        })
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('sideMenu.language_management')}}</span>
            </div>
        </div>

        <div id="_token" class="hidden" data-token="{{ csrf_token() }}"></div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="bank-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th>{{trans('common.id')}}</th>
                        <th width="50%">{{trans('common.english')}}</th>
                        <th width="50%">{{trans('common.chinese')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($languages as $language)
                        <tr>
                            <td>{{$language->id}}</td>
                            <td><a href="#" class="apply-editable" id="name" data-type="text"
                                   data-pk="{{$language->id}}" data-name="name"
                                   data-url="{{route('backend.languages.post')}}"
                                   data-original-title="Enter Name In English">{{$language->name}}</a></td>
                            <td><a href="#" class="apply-editable" id="name_cn" data-type="text"
                                   data-pk="{{$language->id}}" data-name="name_cn"
                                   data-url="{{route('backend.languages.post')}}"
                                   data-original-title="Enter Name In Chinese">{{$language->name_cn}}</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection