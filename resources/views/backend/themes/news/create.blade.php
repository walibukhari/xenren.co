@extends('backend.layout')

@section('title')
    {{trans('sideMenu.themes_panel')}}
@endsection


@section('description')

@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('new.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.themes.news') }}">{{trans('sideMenu.themes_panel')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.themes.news') }}">{{trans('news.news')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.themes.news.create') }}">{{trans('home.create_post')}}</a>
        </li>
    </ul>
@endsection

@section('description')

@endsection

@section('footer')
    <script>
        +function() {
            $(document).ready(function() {
                $('#news-form').makeAjaxForm({
                    redirectTo: '{{ route('backend.themes.news') }}',
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-helper">Fill in the form to add a news</span>
            </div>
            <div class="actions">

            </div>
        </div>
        <div class="portlet-body form">
             {!! Form::open(['method' => 'post', 'role' => 'form', 'id' => 'news-form']) !!}
             <div class="form-body">
                <div class="form-group">
                    {!! Form::label('image','Image',['class'=>'control-label']) !!}
                    <div>
                    <img id="image_preview" class="img-thumbnail img-preview" alt="1366x768 or 683x768" src="{{ (isset($model) && $model->image) ? url($model->image) : '' }}"/>
                    </div>
                    <input accept="image/*" class="image-upload" target="image_preview" onChange="imageFileInputChange(this)" name="image" type="file">
                </div>
                <div class="form-group">
                    {!! Form::label('title','Title',['class'=>'control-label']) !!}
                    {!! Form::text('title',Input::old('title'),['class'=>'form-control', 'placeholder' => 'Enter title']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('language','Language',['class'=>'control-label']) !!}
                    {!! Form::select('language', array('1' => 'Chinsese', '2' => 'English'), '1', array('class' => 'form-control')); !!}
                </div>
                <div class="form-group">
                    {!! Form::label('pic_size','Picture Size',['class'=>'control-label']) !!}
                    {!! Form::select('pic_size', array('1' => '1366 X 768', '2' => '683 X 768'), '1', array('class' => 'form-control')); !!}
                </div>
                <div class="form-group">
                    {!! Form::label('sub_title','Sub Title',['class'=>'control-label']) !!}
                    {!! Form::text('sub_title',Input::old('sub_title'),['class'=>'form-control', 'placeholder' => 'Enter sub-title']) !!}                    </div>
                <div class="form-group">
                    {!! Form::label('link','Link',['class'=>'control-label']) !!}
                    {!! Form::text('link',Input::old('link'),['class'=>'form-control', 'placeholder' => 'Enter link']) !!}
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" class="btn blue">{{trans('news.save')}}</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('modal')

@endsection