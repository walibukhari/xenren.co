@extends('backend.layout')

@section('title')
    {{trans('news.slider')}}
@endsection

@section('description')
    {{trans('theme.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('news.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.themes') }}">{{trans('sideMenu.themes_panel')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.themes.slider') }}">{{trans('news.slider')}}</a>
        </li>
    </ul>
@endsection

@section('header')

@endsection

@section('footer')
    <script>
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#sliders-dt'),
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('backend.themes.slider.dt') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id', orderable: true, searchable: false},
                            {data: 'image', name: 'image', orderable: false, searchable: false},
                            {data: 'title', name: 'title'},
                            {data: 'sub_title', name: 'sub_title'},
                            {data: 'link', name: 'link'},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false}
                        ],
                        createdRow: function(row, data, index) {
                            //check its read 
                            if(data.is_read == 0) {
                                $(row).css({'font-weight':'900'});
                            }
                            /*
                            if(data.status == 'waiting') {
                                $(row).addClass('warning');
                            }else if(data.status == 'decline'){
                                $(row).addClass('danger');
                            }
                            */
                        },
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('news.slider')}}</span>
            </div>
            <div class="actions">
                <a href="{{ route('backend.themes.slider.create') }}" class="btn btn-circle red-sunglo btn-sm"><i
                            class="fa fa-plus"></i> {{trans('theme.add_slider')}}</a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="sliders-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th>{{trans('common.id')}}</th>
                        <th>{{trans('news.image')}}</th>
                        <th>{{trans('news.title')}}</th>
                        <th>{{trans('news.sub-title')}}</th>
                        <th>{{trans('common.link)}}</th>
                        <th>{{trans('news.action')}}</th>
                    </tr>
                    <!--
                    <tr role="row" class="filter">
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_email" placeholder="Username">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_fullname" placeholder="Fullname">
                        </td>
                        <td>
                            <div class="input-group margin-bottom-5">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly name="filter_created_after" placeholder="From">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                            <div class="input-group">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly name="filter_created_before" placeholder="To">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                        </td>
                        <td>
                            <div class="input-group margin-bottom-5">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly name="filter_updated_after" placeholder="From">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                            <div class="input-group">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly name="filter_updated_before" placeholder="To">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                        </td>
                        <td>
                            <div class="margin-bottom-5">
                                <button class="btn btn-info-alt filter-submit">
                                    <i class="fa fa-search"></i> 过滤
                                </button>
                            </div>
                            <button class="btn btn-danger-alt filter-cancel">
                                <i class="fa fa-times"></i> 重置
                            </button>
                        </td>
                    </tr>
                    -->
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection