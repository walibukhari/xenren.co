@extends('backend.layout')

@section('title')
    {{trans('sideMenu.themes_panel')}}
@endsection


@section('description')

@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('news.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.themes.slider') }}">{{trans('sideMenu.themes_panel')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.themes.slider') }}">{{trans('news.slider')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.themes.slider.create') }}">{{trans('news.create')}}</a>
        </li>
    </ul>
@endsection

@section('description')

@endsection

@section('footer')
    <script>
        +function() {
            $(document).ready(function() {
                $('#slider-form').makeAjaxForm({
                    redirectTo: '{{ route('backend.themes.slider') }}',
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-helper">{{trans('theme.fill_in_the_form_to_add_a_slider')}}</span>
            </div>
            <div class="actions">

            </div>
        </div>
        <div class="portlet-body form">
             {!! Form::open(['method' => 'post', 'role' => 'form', 'id' => 'slider-form']) !!}
             <div class="form-body">
                <div class="form-group">
                    {!! Form::label('image','Image',['class'=>'control-label']) !!}
                    <div>
                    <img id="image_preview" class="img-thumbnail img-preview" alt="1920x700" src="{{ (isset($model) && $model->image) ? url($model->image) : '' }}"/>      
                    </div>
                    <input accept="image/*" class="image-upload" target="image_preview" onChange="imageFileInputChange(this)" name="image" type="file">
                </div>
                <div class="form-group">
                    {!! Form::label('title','Title',['class'=>'control-label']) !!}
                    {!! Form::text('title',Input::old('title'),['class'=>'form-control', 'placeholder' => trans('news.title')]) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('sub_title','Sub Title',['class'=>'control-label']) !!}
                    {!! Form::text('sub_title',Input::old('sub_title'),['class'=>'form-control', 'placeholder' => trans('news.sub-title')]) !!}                    </div>
                <div class="form-group">
                    {!! Form::label('link','Link',['class'=>'control-label']) !!}
                    {!! Form::text('link',Input::old('link'),['class'=>'form-control', 'placeholder' => trans('common.link')]) !!}
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" class="btn blue">{{trans('news.save')}}</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('modal')

@endsection