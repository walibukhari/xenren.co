@extends('backend.layout')

@section('title')
    {{trans('theme.projects_panel')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('news.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.themes') }}">{{trans('sideMenu.themes_panel')}}</a>
        </li>
    </ul>
@endsection

@section('header')

@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-body">
            <div class="caption font-green-sharp">
                <a href="{{ route('backend.themes.slider') }}"
                   class="caption-subject bold uppercase">{{trans('theme.main_page_slider')}}</a><br/>
                <a href="{{ route('backend.themes.news') }}"
                   class="caption-subject bold uppercase">{{trans('theme.main_page_newsr')}}</a><br/>
            </div>
        </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection