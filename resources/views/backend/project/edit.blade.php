@extends('ajaxmodal')

@section('title')
    <!--修改-->{{trans('project.项目')}}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                $('#project-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit-project'
                });

                $('#color-picker-1').ColorPicker({
                    color: '#{{ $model->background_color }}',
                    onShow: function (colpkr) {
                        $(colpkr).fadeIn(500);
                        return false;
                    },
                    onHide: function (colpkr) {
                        $(colpkr).fadeOut(500);
                        return false;
                    },
                    onChange: function (hsb, hex, rgb) {
                        $('#color-picker-1-preview').css('backgroundColor', '#' + hex);
                        $('#color-picker-1').val(hex);
                    }
                });

                $('#color-picker-2').ColorPicker({
                    color: '#{{ $model->font_color }}',
                    onShow: function (colpkr) {
                        $(colpkr).fadeIn(500);
                        return false;
                    },
                    onHide: function (colpkr) {
                        $(colpkr).fadeOut(500);
                        return false;
                    },
                    onChange: function (hsb, hex, rgb) {
                        $('#color-picker-2-preview').css('backgroundColor', '#' + hex);
                        $('#color-picker-2').val(hex);
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <!--<button type="button" id="btn-submit-project" class="btn btn-primary blue">修改</button>-->
@endsection

@section('content')
    {!! Form::open(['route' => ['backend.project.edit.post', 'id' => $model->id], 'method' => 'post', 'role' => 'form', 'id' => 'project-form', 'files' => true]) !!}
        <div class="form-body">
            <div class="form-group">
                <label>{{trans('name')}}</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-building"></i>
                    </span>
                    {!! Form::text('name', $model->name, ['class' => 'form-control', 'disabled']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>{{trans('project.category')}}</label>
                <div class="input-group">
                    <span class="input-group-addon" id="color-picker-1-preview" style="background-color: #{{ $model->background_color }};">
                        <i class="fa fa-code"></i>
                    </span>
                    {!! Form::text('category_name', $category_name, ['class' => 'form-control', 'disabled']) !!}
                    {!! Form::hidden('category_id', $model->category_id, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>{{trans('project.description')}}</label>
                <div class="input-group">
                    <span class="input-group-addon" id="color-picker-2-preview" style="background-color: #{{ $model->font_color }};">
                        <i class="fa fa-info"></i>
                    </span>
                    {!! Form::text('description', $model->description, ['class' => 'form-control', 'disabled']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>{{trans('project.attachment')}}</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-image"></i>
                    </span>
                    {!! Form::text('attachment', 'http://' . Request::server ('HTTP_HOST'). '/'  . $model->attachment, ['class' => 'form-control', 'disabled']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>{{trans('usercenter.skill')}}</label>
                <div class="input-group">
                    <span class="input-group-addon" id="color-picker-1-preview" style="background-color: #{{ $model->background_color }};">
                        <i class="fa fa-thumbs-o-up"></i>
                    </span>
                    {!! Form::text('skill_name_list', $skill_name_list, ['class' => 'form-control', 'disabled']) !!}
                    {!! Form::hidden('skill_id', $model->skill_id, ['class' => 'form-control', 'disabled']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>{{trans('project.pay-type')}}</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-money"></i>
                    </span>
                    <select class="pay-type form-control" disabled>
                        <option value="1" @if($model->pay_type == 1){{ 'selected="selected"' }} @endif >{{trans('project.pay_by_the_hour')}}</option>
                        <option value="2" @if($model->pay_type == 2){{ 'selected="selected"' }} @endif >{{trans('project.pay_a_fixed_price')}} </option>
                    </select>
                    {!! Form::hidden('pay_type', $model->pay_type, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>Time Commitment</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-clock-o"></i>
                    </span>
                    <select class="time-commitment form-control" disabled>
                        <option value="1" @if($model->time_commitment == 1){{ 'selected="selected"' }} @endif > {{trans('project.more_than_30_hrs_week')}}</option>
                        <option value="2" @if($model->time_commitment == 2){{ 'selected="selected"' }} @endif >{{trans('project.less_than_30_hrs_week')}} </option>
                        <option value="3" @if($model->time_commitment == 3){{ 'selected="selected"' }} @endif >{{trans('project.dont_know_yet')}} </option>
                    </select>
                    {!! Form::hidden('time_commitment', $model->time_commitment, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>Freelance Type</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-users"></i>
                    </span>
                    <select class="freelance-type form-control" disabled>
                        <option value="1" @if($model->freelance_type == 1){{ 'selected="selected"' }} @endif >{{trans('project.anyone_can_find_and_apply_to_this_job')}}</option>
                        <option value="2" @if($model->freelance_type == 2){{ 'selected="selected"' }} @endif >{{trans('project.only_upwork_users_can_find_this_job')}}</option>
                        <option value="3" @if($model->freelance_type == 3){{ 'selected="selected"' }} @endif >{{trans('project.only_freelancers_i_have_invited_can_find_this_job')}}</option>
                    </select>
                    {!! Form::hidden('freelance_type', $model->freelance_type, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>{{trans('project.question')}}</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-question"></i>
                    </span>
                    {!! Form::textarea('questions', $model->questions, ['class' => 'form-control', 'disabled']) !!}
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection