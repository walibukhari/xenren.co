@extends('ajaxmodal')

@section('title')
    {{trans('project.项目')}}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                $('#leave-message-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-leave-message'
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-leave-message" class="btn btn-success blue">Submit</button>
@endsection

@section('content')
    {!! Form::open(['route' => ['backend.project.leave_message_post', 'id' => $model->id], 'method' => 'post', 'role' => 'form', 'id' => 'leave-message-form', 'files' => true]) !!}
        <div class="form-body">
            <div class="form-group">
                <label>Message</label>
                <div class="">
                    <textarea name="message" id="message" class="form-control" style="width: 100%"></textarea>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection