@extends('backend.layout')

@section('title')
    {{trans('managecoin.managecoin')}}
@endsection

@section('description')
    {{trans('managecoin.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>

        <li>
            <a href="{{ route('backend.manageCoins') }}">{{trans('managecoin.managecoin')}}</a>
        </li>
    </ul>
@endsection

@section('header')
<style>
    .settingPage{
        float: left !important;
    }

    .settingPageSwitch{
        margin-left: 100px !important;
    }
</style>

@endsection

@section('footer')
    <script>


        $('#check1').click(function(){
            var c = $(this).prop('checked')
            console.log("check")
            console.log(c)
        })

        $(function(){

                $('#setInvitorUserCommission').on('change', function(e){
                    var userStatus = $(this).prop('checked')
                    $.ajax({
                        url:'/admin/setInvitorUserCommission',
                        type:'POST',
                        data:{
                            "status": userStatus,
                        },
                        success: function(response) {
                            if(response.status == 'failure') {
                                toastr.error(response.message);
                            } else {
                                toastr.success(response.message);
                                // setTimeout(function(){
                                    // $("#createMile").trigger("reset");
                                    // window.location.reload();
                                // }, 3000)
                            }
                        },
                    });
                    e.preventDefault();
                    return false;

                })
            })


            $(function(){

                $('#setInvitedUserCommission').on('change', function(e){
                    var senderStatus = $(this).prop('checked')
                    $.ajax({
                        url:'/admin/setInvitedUserCommission',
                        type:'POST',
                        data:{
                            "status": senderStatus,
                        },
                        success: function(response) {
                            if(response.status == 'failure') {
                                toastr.error(response.message);
                            } else {
                                toastr.success(response.message);
                                // setTimeout(function(){
                                    // $("#createMile").trigger("reset");
                                    // window.location.reload();
                                // }, 3000)
                            }
                        },
                    });
                    e.preventDefault();
                    return false;

                })
                })


            $(function(){

                $('#setHourlyRate').on('change', function(e){
                    var hourlyStatus = $(this).prop('checked');
                    console.log('hourlyStatus');
                    console.log(hourlyStatus);
                    $.ajax({
                        url:'/admin/setHourlyRate',
                        type:'POST',
                        data:{
                            "status": hourlyStatus,
                        },
                        success: function(response) {
                            console.log('response');
                            console.log(response);
                            if(response.status == 'failure') {
                                toastr.error(response.message);
                            } else {
                                toastr.success(response.message);
                                // setTimeout(function(){
                                    // $("#createMile").trigger("reset");
                                    // window.location.reload();
                                // }, 3000)
                            }
                        },
                    });
                    e.preventDefault();
                    return false;

                })
                })
   </script>
@endsection

@section('content')
<div class="row">
    <div class="btnswtich settingPage">
        <p style="display:inline-block; font-weight:600; font-size:12pt; color:#a0a0a0">Sender on/off</p>
        <label class="switch settingPageSwitch">
            <input type="checkbox" name="sender" id="setInvitorUserCommission" {{$invitorUserCommision?'checked':''}}>
            <span class="slider round"></span>
        </label>
        <div>
        </div>
    </div>
    <!-- <br> -->
    <div class="btnswtich settingPage">
        <p style="display:inline-block; font-weight:600; font-size:12pt; color:#a0a0a0">New User on/off</p>
        <label class="switch settingPageSwitch">
            <input type="checkbox" name="user" id="setInvitedUserCommission" {{$invitedUserCommision?'checked':''}}>
            <span class="slider round"></span>
        </label>
        <div>
        </div>
    </div>
</div>


<div class="row">
    <div class="btnswtich settingPage">
        <p style="display:inline-block; font-weight:600; font-size:12pt; color:#a0a0a0">Hourly On/Off</p>
        <label class="switch settingPageSwitch">
            <input type="checkbox" name="user" id="setHourlyRate" {{$hourlyOnOff?'checked':''}}>
            <span class="slider round"></span>
        </label>
        <div>
        </div>
    </div>
</div>

@endsection

@section('modal')

@endsection
