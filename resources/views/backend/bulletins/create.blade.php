@extends('backend.layout')

@section('title')
    {{trans('permissions.新增权限组')}}
@endsection

@section('description')

@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans("bank.后台")}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.permissions') }}">{{trans('sideMenu.权限组')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.permissions.create') }}">{{"permissions.新增权限组"}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <link href="/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/jquery-multi-select/css/multi-select.css" rel="stylesheet" type="text/css" />
@endsection

@section('footer')
    <script src="/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
    <script>
        +function() {
            $(document).ready(function() {
                $('#permissions-selector').multiSelect({
                    selectableHeader: '<span class="caption font-red-sunglo">{{trans('permissions.未选择')}}</span>',
                    selectionHeader: '<span class="caption font-green-sharp">{{trans('permissions.已选择')}}</span>',
                });
                $('#permission-form').makeAjaxForm({
                    redirectTo: '{{ route('backend.permissions') }}',
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-helper">{{trans('permissions.填上表格以新增权限组')}}</span>
            </div>
            <div class="actions">

            </div>
        </div>
        <div class="portlet-body form">
            <form action="{{route('backend.bulletin.createBulletin')}}" method="POST">
                <div class="form-group">
                    <label for="email">Text</label>
                    <textarea name="text" id="text" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label for="email">Text Chinese</label>
                    <textarea name="text_cn" id="text" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label for="pwd">Status:</label>
                    <select name="status" id="status" class="form-control">
                        <option value="1">Active</option>
                        <option value="0">In Active</option>
                    </select>
                </div>

                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
@endsection

@section('modal')

@endsection