@extends('backend.layout')

@section('title')
    {{trans('sideMenu.dispute_requests')}}
@endsection

@section('description')
    {{trans('permissions.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('bank.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="/admin/bulletin">{{trans('common.bulletin')}}</a>
        </li>
    </ul>
@endsection

@section('header')

@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase">{{trans('common.bulletin')}}</span><br/>
            </div>
            <input type="button" onclick="window.location.href='{{route('backend.bulletin.create')}}'" value="{{trans('common.add')}} {{trans('common.bulletin')}}" class="btn btn-primary" style="float: right">
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="permissions-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th>#</th>
                        <th>Text</th>
                        <th>Text CN</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    @foreach($data as $k => $v)
                        <tr role="row" class="filter">
                            <td> {{ $v->id }} </td>
                            <td> {!! $v->text !!} </td>
                            <td> {!! $v->text_cn !!} </td>
                            <td> {!! $v->status_name !!} </td>
                            <td>
                                <a href="{{route('backend.bulletin.editBulletin', $v->id)}}" class="fa fa-pencil">Edit</a>
                            </td>
                        </tr>
                    @endforeach
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection