@extends('backend.layout')

@section('title')
    {{trans('sharedoffice.sharedoffice')}}
@endsection


@section('description')

@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('managecoin.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.manageCoins') }}">{{trans('managecoin.managecoin')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.manageCoins.create') }}">{{trans('managecoin.create_coin')}}</a>
        </li>
    </ul>
@endsection

@section('description')

@endsection

@section('footer')
    <script>
        +function() {
            $(document).ready(function() {
                $('#sharedoffice-form').makeAjaxForm({
                    redirectTo: '{{ route('backend.manageCoins') }}',
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-helper">{{trans('managecoin.msg1')}}</span>
            </div>
            <div class="actions">

            </div>
        </div>
        <div class="portlet-body form">
            {!! Form::open(['method' => 'post', 'role' => 'form', 'id' => 'sharedoffice-form']) !!}
            <div class="form-body">
                <div class="form-group">
                    {!! Form::label('coins','Coins',['class'=>'control-label']) !!}
                    {!! Form::text('coins',Input::old('coins'),['class'=>'form-control', 'placeholder' => 'Enter Coins']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('coin_type','Coin Type',['class'=>'control-label']) !!}
                    {!! Form::text('coin_type',Input::old('coin_type'),['class'=>'form-control', 'placeholder' => 'Enter Coin Type']) !!}
					</div>
                    <div class="form-group">

  
                        <div class="form-group">
                            {!! Form::label('amount','Amount',['class'=>'control-label']) !!}
                            {!! Form::text('amount',Input::old('amount'),['class'=>'form-control', 'placeholder' => 'Enter Per Coin Amount']) !!}
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn blue">{{trans('managecoin.save')}}</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
@endsection

@section('modal')

@endsection