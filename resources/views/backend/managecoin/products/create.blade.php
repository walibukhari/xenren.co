@extends('backend.layout')

@section('title')
    {{trans('sharedoffice.create_product')}}
@endsection

@section('description')
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedoffice.products.index', ['id' => $id]) }}">{{trans('sharedoffice.sharedofficeproduct')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedoffice.products.create', ['id' => $id]) }}">{{trans('sharedoffice.create_product')}}</a>
        </li>
    </ul>
@endsection

@section('description')

@endsection

@section('footer')
    <script>
        +function() {
            $(document).ready(function() {
                $('#sharedoffice-form').makeAjaxForm({
                    redirectTo: '{{ route('backend.sharedoffice.products.create', ['id' => $id]) }}',
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-helper">{{trans('sharedoffice.msg3')}}</span>
            </div>
            <div class="actions">

            </div>
        </div>
        <div>
        <h3 class=""><span >Office Name:</span> <span class="badge badge-info">{{ $sharedoffice->office_name }}</span> </h3>
            <h3 class=""><span >Location:</span>  <span class="badge badge-info">{{ $sharedoffice->location }}</span></h3>
        </div>
        <div class="portlet-body form">
            {!! Form::open(['method' => 'post', 'role' => 'form']) !!}
            <div class="form-body">

                <div class="form-group">
                    {{ Form::hidden('office_id', $sharedoffice->id) }}
                    {!! Form::label('categories','Categories',['class'=>'control-label']) !!}
                    {!! Form::text('categories',Input::old('categories'),['class'=>'form-control','required' => '', 'placeholder' => 'Enter categories']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('stock','Stock',['class'=>'control-label']) !!}
                    {!! Form::text('stock',Input::old('stock'),['class'=>'form-control','required' => '', 'placeholder' => 'Enter Sock']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('month_price','Month Price',['class'=>'control-label']) !!}
                    {!! Form::text('month_price',Input::old('month_price'),['class'=>'form-control','required' => '', 'placeholder' => 'Enter Month Price']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('time_price','Time Price',['class'=>'control-label']) !!}
                    {!! Form::text('time_price',Input::old('time_price'),['class'=>'form-control','required' => '', 'placeholder' => 'Enter Time Price']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('remark','Remark',['class'=>'control-label']) !!}
                    {!! Form::text('remark',Input::old('remark'),['class'=>'form-control','required' => '', 'placeholder' => 'Enter Remark']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('status','Status',['class'=>'control-label']) !!}
                    {!! Form::text('status',Input::old('status'),['class'=>'form-control','required' => '', 'placeholder' => 'Enter Status']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('number_product','Number Product',['class'=>'control-label']) !!}
                    {!! Form::text('number_product',Input::old('status'),['class'=>'form-control','required' => '', 'placeholder' => 'Enter Number Product']) !!}
                </div>

                <div class="form-actions">
                    <button type="submit" class="btn blue">{{trans('sharedoffice.saveproduct')}}</button>
                </div>

                {!! Form::close() !!}

                </div>
            </div>
@endsection

@section('modal')

@endsection