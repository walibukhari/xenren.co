@extends('backend.layout')

@section('title')
    {{trans('sharedoffice.sharedofficechange')}}
@endsection

@section('description')

@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.sharedoffice')}}</a>
            <i class="fa fa-circle"></i>
        </li>

        <li>
            <a href="{{ Request::url() }}">{{trans('sharedoffice.sharedofficechange')}}</a>
        </li>
    </ul>
@endsection

@section('footer')
    <script>
        +function() {
            $(document).ready(function() {
                $('#managecoin-form').makeAjaxForm({
                    redirectTo: '{{ route('backend.manageCoins') }}',
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-helper">{{trans('managecoin.msg2')}}</span>
            </div>
            <div class="actions">

            </div>
        </div>
        <div class="portlet-body form">
            {!! Form::open(['method' => 'post', 'files' => true, 'role' => 'form', 'id' => 'managecoin-form']) !!}

            <div class="form-body">
               
                    
                   
                <div class="form-group">
                    {!! Form::label('coins','Coins',['class'=>'control-label']) !!}
                    {!! Form::text('coins',$model->coins,['class'=>'form-control', 'placeholder' => 'Enter Coins']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('coin_type','Coin Type',['class'=>'control-label']) !!}
                    {!! Form::text('coin_type',$model->coins_type,['class'=>'form-control', 'placeholder' => 'Enter Coin Type']) !!}
                </div>


                <div class="form-group">
                    {!! Form::label('amount','Amount',['class'=>'control-label']) !!}
                    {!! Form::text('amount',$model->per_coin_amount,['class'=>'form-control', 'placeholder' => 'Enter Per Coin Amount']) !!}
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" class="btn blue">{{trans('managecoin.edit')}}</button>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('modal')

@endsection