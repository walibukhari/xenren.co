@extends('backend.layout')

@section('title')
    {{trans('managecoin.managecoin')}}
@endsection

@section('description')
    {{trans('managecoin.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>

        <li>
            <a href="{{ route('backend.manageCoins') }}">{{trans('managecoin.managecoin')}}</a>
        </li>
    </ul>
@endsection

@section('header')

@endsection

@section('footer')
    <script>
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#manageCoin-dt'),
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('backend.manageUserCoins.dt') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id', orderable: true, searchable: false},
                            {data: 'coins', name: 'coins', orderable: false, searchable: false},
                            {data: 'coins_type', name: 'coins_type'},
                            {data: 'per_coin_amount', name: 'per_coin_amount'},
                            {data: 'actions',width:'20%', name: 'actions', orderable: false, searchable: false}
                        ],
                        createdRow: function(row, data, index) {
                            //check its read
                            if(data.is_read == 0) {
                                $(row).css({'font-weight':'900'});
                            }
                            /*
                             if(data.status == 'waiting') {
                             $(row).addClass('warning');
                             }else if(data.status == 'decline'){
                             $(row).addClass('danger');
                             }
                             */
                        },
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('managecoin.dashboard')}}</span>
            </div>
          <!--  <div class="actions">
                <a href="{{ route('backend.manageCoins.create') }}" class="btn btn-circle red-sunglo btn-sm"><i
                            class="fa fa-plus"></i> {{trans('managecoin.add_coin')}}</a>
            </div> -->
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="manageCoin-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th>{{trans('common.id')}}</th>
                        <th>{{trans('managecoin.coins')}}</th>
                        <th>{{trans('managecoin.coin_type')}}</th>
                        <th>{{trans('managecoin.amount')}}</th>
                        <th>{{trans('managecoin.action')}}</th>
                    </tr>
                   
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection