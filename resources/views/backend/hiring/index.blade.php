@extends('backend.layout')

@section('title')
    {{trans('sharedoffice.hiring_request')}}&nbsp
@endsection


@section('description')

@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.sharedoffice')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.hiringsrequest') }}">{{trans('sharedoffice.hiring_request')}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <link href="/css/hiringRequest.css" rel="stylesheet" type="text/css">
@endsection

@section('footer')

@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase">{{trans('sharedoffice.hiring_request')}}</span>
            </div>
            <div class="caption font-green-sharp" style="float:right">

            </div>
        </div>
        <div class="portlet-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Job Position</th>
                            <th>About Us</th>
                            <th>Resume/CV</th>
                            <th>Download</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($hiring as $hirings)
                        <tr>
                            <td>{{$hirings->first_name}}</td>
                            <td>{{$hirings->last_name}}</td>
                            <td>{{$hirings->email}}</td>
                            <td>{{$hirings->job_position}}</td>
                            <td>{{$hirings->bio}}</td>
                            <td>{{$hirings->resume}}</td>
                            <td><a href="{{url('admin/download/resume',$hirings->id)}}" class="btn btn-sm btn-success">Download Resume/CV</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection