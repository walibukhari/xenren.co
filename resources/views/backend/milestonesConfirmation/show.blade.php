@extends('backend.layout')

@section('title')
    Milestone
@endsection

@section('description')
    {{trans('permissions.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('bank.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">MileStone</a>
        </li>
    </ul>
@endsection

@section('header')

@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase">Milestone</span>
            </div>
        </div>
        <div class="col-md-12">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{session()->get('success')}}
                </div>
            @endif
            @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{session()->get('error')}}
                </div>
            @endif
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <div class="row">
                    <div class="col-md-6">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        Currant Release Time Verified Users: <b>{{$transactions1}} weeks</b>
                    </div>
                    <div class="col-md-3 text-right">
                        <a onclick="openEditAllPopUp('a')" class="btn btn-sm btn-success">Edit All</a>
                    </div>
                    <div class="col-md-3">
                        Currant Release Time Un Verified Users: <b>{{$transactions2}} weeks</b>
                    </div>
                    <div class="col-md-3 text-right">
                        <a onclick="openEditAllPopUp('b')" class="btn btn-sm btn-success">Edit All</a>
                    </div>
                </div>
{{--                <table class="table table-striped table-bordered table-hover table-checkable" id="permissions-dt">--}}
{{--                    <thead>--}}
{{--                    <tr role="row" class="heading">--}}
{{--                        <th>#</th>--}}
{{--                        <th>Sender</th>--}}
{{--                        <th>Receiver</th>--}}
{{--                        <th>Project</th>--}}
{{--                        <th>Status</th>--}}
{{--                        <th>Amount</th>--}}
{{--                        <th>Created At</th>--}}
{{--                        <th>Release At</th>--}}
{{--                        <th>Action</th>--}}
{{--                    </tr>--}}
{{--                    </thead>--}}
{{--                    <tbody>--}}
{{--                    @foreach($transactions as $mile)--}}
{{--                        <tr>--}}
{{--                            <td>{{$mile->id}}</td>--}}
{{--                            <td>{{$mile->sender->name}}</td>--}}
{{--                            <td>{{$mile->receiver->name}}</td>--}}
{{--                            <td>{{$mile->name}}</td>--}}
{{--                            <td>--}}
{{--                            @if($mile->status == 5)--}}
{{--                               Approved By Client  Hold By System--}}
{{--                            @endif--}}
{{--                            </td>--}}
{{--                            <td>{{$mile->amount}}</td>--}}
{{--                            <td>{{$mile->getCreatedAT($mile->created_at)}}</td>--}}
{{--                            <td>{{$mile->release_at}}</td>--}}
{{--                            <td><a href="#" onclick="openPopUp({{$mile->id}})">Edit</a> / <a href="#">Delete</a></td>--}}
{{--                        </tr>--}}
{{--                    @endforeach--}}
{{--                    </tbody>--}}
{{--                </table>--}}
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script>
        $(function () {
            $('.datepickerChangeDate').datetimepicker({
                autoClose: true,
            }).on('changeDate', function(e){
                $('.datepickerChangeDate').datetimepicker('hide');
            });
        });
    </script>
    <script>
        function openEditAllPopUp(val) {
            if(val == 'a') {
                $('#changeAll').modal('show');
            } else {
                $('#changeAllUnVerified').modal('show');
            }
        }
        function openPopUp(id) {
            $('#value').val(id);
            $('#myModal').modal('show');
        }
    </script>
@endsection
@section('modal')
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Change Release Date</h4>
                </div>
                <form action="{{route('backend.changeDateTime')}}">
                    @csrf
                    <input type="hidden" value="" name="id" id="value" />
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Date Time:</label>
                                <input type="text" name="date" class="datepickerChangeDate form-control" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default" >Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>

        </div>
    </div>
    <div id="changeAll" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Change Release Week</h4>
                </div>
                <form action="{{route('backend.changeAllDateTime')}}">
                    @csrf
                    <input type="hidden" value="" name="id" id="value" />
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Select Week:</label>
{{--                                <input type="text" name="date" class="datepickerChangeDate form-control" />--}}
                                <select class="form-control" name="date">
                                    <option value="1">1 week</option>
                                    <option value="2">2 week</option>
                                    <option value="3">3 week</option>
                                    <option value="4">4 week</option>
                                    <option value="5">5 week</option>
                                    <option value="6">6 week</option>
                                    <option value="7">7 week</option>
                                    <option value="8">8 week</option>
                                    <option value="9">9 week</option>
                                    <option value="10">10 week</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default" >Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>

        </div>
    </div>
    <div id="changeAllUnVerified" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Change Release Week</h4>
                </div>
                <form action="{{route('backend.changeAllDateTimeUnVerified')}}">
                    @csrf
                    <input type="hidden" value="" name="id" id="value" />
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Select Week:</label>
{{--                                <input type="text" name="date" class="datepickerChangeDate form-control" />--}}
                                <select class="form-control" name="date">
                                    <option value="1">1 week</option>
                                    <option value="2">2 week</option>
                                    <option value="3">3 week</option>
                                    <option value="4">4 week</option>
                                    <option value="5">5 week</option>
                                    <option value="6">6 week</option>
                                    <option value="7">7 week</option>
                                    <option value="8">8 week</option>
                                    <option value="9">9 week</option>
                                    <option value="10">10 week</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default" >Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>

        </div>
    </div>
@endsection
