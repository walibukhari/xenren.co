@extends('backend.layout')

@section('title')
    {{trans('sideMenu.milestone_confirmation')}}
@endsection

@section('description')
    {{trans('permissions.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('bank.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.disputes') }}">{{trans('sideMenu.dispute_requests')}}</a>
        </li>
    </ul>
@endsection

@section('header')

@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('sideMenu.dispute_requests')}}</span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="permissions-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th>#</th>
                        <th>Sender</th>
                        <th>Receiver</th>
                        <th>Project</th>
                        <th>Milestone</th>
                        <th>Amount</th>
                        <th>Action</th>
                    </tr>
                    @foreach($data as $k => $v)
                        @php
                            $sender_real_name = isset($v->sender->real_name) ? $v->sender->real_name : '';
                            $receiver_real_name = isset($v->receiver->real_name) ? $v->receiver->real_name : '';
                            $milestone_project = isset($v->milestone->project->name) ? $v->milestone->project->name : '';
                            $milestone_name = isset($v->milestone->name) ? $v->milestone->name: '';
                            $milestone_amount = isset($v->amount) ? $v->amount: '';
                        @endphp
                        <tr role="row" class="filter">
                            <td> {{ $v->id }} </td>
                            <td> {!! $sender_real_name !!} </td>
                            <td> {!! $receiver_real_name !!} </td>
                            <td> {!! $milestone_project !!} </td>
                            <td> {!! $milestone_name !!} </td>
                            <td> {!! $milestone_amount !!} </td>
                            <td>
                                <input type="button" class="btn btn-primary" value="Approve" onclick="window.location='/admin/milestone/approve?id={{$v->id}}'">
                            </td>
                        </tr>
                    @endforeach
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection