@extends('backend.layout')

@section('title')
    Owner Progress
@endsection

@section('description')
    Section
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">Owner Section</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.index') }}">Owner Progress</a>
        </li>
    </ul>
@endsection

@section('header')
    <link href="/custom/plugins/colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css"/>
@endsection

@section('footer')
    <script src="/custom/plugins/colorpicker/js/colorpicker.js"></script>
    <script>
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> Progress </span>
            </div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">

                <div class="row">
                    <label style="font-weight: bold;">Owner Profile</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label style="font-weight: bold;"></label>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: {{$owner_profile}}%;" aria-valuenow="{{$owner_profile}}" aria-valuemin="0" aria-valuemax="100">
                            {{$owner_profile}}%</div>
                    </div>
                </div>

                <div class="row">
                    <label style="font-weight: bold;">Manage Bookings</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label style="font-weight: bold;">Clicked:{{isset($progress) ? $progress->manage_bookings : 0}}</label>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="@if($manage_bookings == 0) color:#555; @endif width: {{$manage_bookings}}%;" aria-valuenow="{{$manage_bookings}}" aria-valuemin="0" aria-valuemax="100">
                            {{$manage_bookings}}%</div>
                    </div>
                </div>

                <div class="row">
                    <label style="font-weight: bold;">Confirm Bookings</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label style="font-weight: bold;">Clicked:{{isset($progress) ? $progress->confirm_bookings : 0}}</label>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="@if($confirm_bookings == 0) color:#555; @endif width: {{$confirm_bookings}}%;" aria-valuenow="{{$confirm_bookings}}" aria-valuemin="0" aria-valuemax="100">
                            {{$confirm_bookings}}%</div>
                    </div>
                </div>

                <div class="row">
                    <label style="font-weight: bold;">Office Checked In</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label style="font-weight: bold;">Clicked:{{isset($progress) ? $progress->office_check_in : 0}}</label>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="@if($office_check_in == 0) color:#555; @endif width: {{$office_check_in}}%;" aria-valuenow="{{$office_check_in}}" aria-valuemin="0" aria-valuemax="100">
                            {{$office_check_in}}%</div>
                    </div>
                </div>

                <div class="row">
                    <label style="font-weight: bold;">Office</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label style="font-weight: bold;">Clicked:{{isset($progress) ? $progress->office_info : 0}}</label>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="@if($office_info == 0) color:#555; @endif width: {{$office_info}}%;" aria-valuenow="{{$office_info}}" aria-valuemin="0" aria-valuemax="100">
                            {{$office_info}}%</div>
                    </div>
                </div>
            </div>
    </div>
@endsection

@section('modal')

@endsection
