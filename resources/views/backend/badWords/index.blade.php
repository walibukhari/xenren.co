@extends('backend.layout')

@section('title')
    {{trans('sideMenu.member_management')}}
@endsection

@section('description')
    {{trans('user.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sideMenu.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.user') }}">{{trans('sideMenu.member_management')}}</a>
        </li>
    </ul>
@endsection

@section('header')

@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('common.member_list')}}</span>
            </div>
            <div class="actions">
                <a href="{{ route('backend.user.create') }}" class="btn btn-circle red-sunglo btn-sm"
                   data-target="#remote-modal" data-toggle="modal"><i class="fa fa-plus"></i> {{ trans('common.add_member') }}</a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="user-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th>{{trans('common.id')}}</th>
                        <th>{{trans('common.user')}}</th>
                        <th>{{trans('common.type')}}</th>
                        <th>{{trans('common.original')}}</th>
                        <th>{{trans('common.matches')}}</th>
                    </tr>
                    <tr role="row" class="filter">
                        @foreach($data as $k => $v)
                        <tr>
                            @php
                            try {
                                if($v->data['type'] == \App\Models\BadWordsFilter::TYPE_PROJECT) {
                                    $projectId = isset($v->data['reference_id']) ? $v->data['reference_id'] : '';
                                    $link = '/joinDiscussion/getProject/'.$v->project->name.'/'.$projectId;
                                }
                                else {
                                    $userId = isset($v->user_id) ? $v->user_id : '';
                                    $link = \Session::get('lang').'/resume/getDetails/'.$v->user->user_link;
                                }
                            } catch (\Exception $e) {
                                $link = '#';
                            }

                            @endphp
                            <td>{{$loop->index+1}}</td>
                            <td>{{$v->user['name']}}</td>
                            <td><a href="{{$link}}">{{isset($v->data['type']) ? $v->getTypeName($v->data['type']) : ''}}</a></td>
                            <td>{{isset($v->data['data']) && isset($v->data['data']['orig']) ? $v->data['data']['orig'] : ''}}</td>
                            <td>{{implode(',', $v->data['data']['matched'])}}</td>
                        </tr>
                        @endforeach
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection