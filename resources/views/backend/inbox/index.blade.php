@extends('backend.layout')

@section('title')
    {{ trans('sideMenu.inbox') }}
@endsection

@section('description')
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('officialprojects.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.inbox') }}">{{trans('sideMenu.inbox')}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <link href="/assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/backend/inbox.css" rel="stylesheet" type="text/css" />
@endsection

@section('footer')
    <script src="/assets/global/plugins/fancybox/source/jquery.fancybox.js" type="text/javascript"></script>
    <script>
        var url = {
            'trashAll': "{{ route('backend.inbox.trashall') }}",
            'untrashAll': "{{ route('backend.inbox.untrashall') }}",
            'markAsRead': "{{ route('backend.inbox.markasread') }}",
            'removeAll': "{{ route('backend.inbox.removeall') }}"
        }
        $(document).ready(function () {
            $('.chkAllItemTick').on('click', function () {
                if ($(this)[0].checked) {
                    $('.chkItemTick').each(function () {
                        if( $(this).prop('checked') == false ) {
                            $(this).trigger('click');
                        }
                    });

                    $('.spnAllItemTick').addClass('checked');
                } else {
                    $('.chkItemTick').each(function () {

                        if( $(this).prop('checked') == true ) {
                            $(this).trigger('click');
                        }
                    });

                    $('.spnAllItemTick').removeClass('checked');
                }
            });

            $('.chkItemTick').on('click', function () {
                var id = $(this).val();
                if( $(this).prop('checked') == true ) {
                    $('.spnItemTick-' + id ).addClass('checked');
                } else {
                    $('.spnItemTick-' + id ).removeClass('checked');
                }
            });

            $('#slcActions').on('change', function(e){

                var data = { 'inboxIds[]' : []};
                $(".chkItemTick:checked").each(function() {
                    data['inboxIds[]'].push($(this).val());
                });

                if( $(this).val() == '1' )
                {
                    //mark as read
                    $.ajax({
                        url: url.markAsRead,
                        dataType: 'json',
                        data: data,
                        method: 'POST',
                        error: function () {
                            alert(lang.unable_to_request);
                        },
                        success: function (result) {
                            if(result.status == 'success') {
                                alertSuccess(result.msg);

                                $(".chkItemTick:checked").each(function() {
                                    $('#inbox-' + $(this).val()).removeClass('striped');
                                });
                            }else{
                                alertError(result.msg);
                            }
                        }
                    });
                }
                else if( $(this).val() == '2' )
                {
                    //move to trash
                    $.ajax({
                        url: url.trashAll,
                        dataType: 'json',
                        data: data,
                        method: 'POST',
                        error: function () {
                            alert(lang.unable_to_request);
                        },
                        success: function (result) {
                            if(result.status == 'success') {
                                alertSuccess(result.msg);

                                $(".chkItemTick:checked").each(function() {
                                    $('#inbox-' + $(this).val()).hide();
                                });
                            }else{
                                alertError(result.msg);
                            }
                        }
                    });
                }
                else if( $(this).val() == '3' )
                {
                    //cancel trash and move back to inbox
                    $.ajax({
                        url: url.untrashAll,
                        dataType: 'json',
                        data: data,
                        method: 'POST',
                        error: function () {
                            alert(lang.unable_to_request);
                        },
                        success: function (result) {
                            if(result.status == 'success') {
                                alertSuccess(result.msg);

                                $(".chkItemTick:checked").each(function() {
                                    $('#inbox-' + $(this).val()).hide();
                                });
                            }else{
                                alertError(result.msg);
                            }
                        }
                    });
                }
                else if( $(this).val() == '4' )
                {
                    //remove inbox and inbox message
                    $.ajax({
                        url: url.removeAll,
                        dataType: 'json',
                        data: data,
                        method: 'POST',
                        error: function () {
                            alert(lang.unable_to_request);
                        },
                        success: function (result) {
                            if(result.status == 'success') {
                                alertSuccess(result.msg);

                                $(".chkItemTick:checked").each(function() {
                                    $('#inbox-' + $(this).val()).hide();
                                });
                            }else{
                                alertError(result.msg);
                            }
                        }
                    });
                }
            });
        });
    </script>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 right-top-main">
            <div class="row">
                <div class="col-md-12 top-tabs-menu">
                    <div class="order-status-menu pull-right">
                        <ul>
                            <li class="{{ $tab == 1? 'active': '' }}">
                                <a href="{{ route('backend.inbox') }}?category=1">{{ trans('common.inbox') }}</a>
                            </li>
                            <li class="{{ $tab == 2? 'active': '' }}">
                                <a href="{{ route('backend.inbox') }}?category=2">{{ trans('common.system_message') }}</a>
                            </li>
                            <li class="{{ $tab == 3? 'active': '' }}">
                                <a href="{{ route('backend.inbox') }}?isTrash=1">{{ trans('common.trash')}}</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tab-content">
        <div class="tab-pane active inbox-main-section" id="inbox">
            <div class="row header-action">
                <div class="col-md-6 ">
                    <div class="col-md-1 checkbox-sub checkbox-all">
                        <span class="spnAllItemTick">
                            <input type="checkbox" value="" name="chkAllItemTick" class="chkAllItemTick">
                        </span>
                    </div>

                    <div class="search-selectbox">
                        <select class="form-control" name="slcActions" id="slcActions">
                            <option value="0">{{ trans('common.actions') }}</option>
                            @foreach( $actions as $key => $action )
                            <option value="{{ $key }}">{{ $action }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    @if ($myInbox->lastPage() > 1)
                    <div class="pull-right">
                        {{ $myInbox->links() }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="portlet box">
                <div class="portlet-body">
                    @foreach ($myInbox as $key => $var)
                    <div class="row inbox-mail {{ $var->getLatestInboxMessageIsRead(false, Auth::guard('staff')->user()->id) == 0? 'striped': '' }}" id="inbox-{{ $var->id }}">
                        <div class="col-md-1 checkbox-sub">
                            <span class="spnItemTick-{{ $var->id }}">
                                <input type="checkbox" value="{{ $var->id }}" name="chkItemTick[]" class="chkItemTick">
                            </span>
                        </div>
                        <div class="col-md-11 inbox-mail-detail">
                            <a href="{{ route('backend.inbox.messages', ['inbox_id' => $var->id ]) }}">
                                <div class="row">
                                    @php
                                        $sender = $var->getSenderInfo()
                                    @endphp
                                    <div class="col-md-1 user-img">
                                        <div class="user-imgs">
                                            <img src="{{ $sender['avatar'] }}">
                                            <i class="fa fa-circle offline"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-4 user-name">
                                        <h5>
                                            {{ $sender['name'] }}
                                        </h5>
                                        @if( isset($var->project) )
                                        <h6>{{ truncateSentence($var->project->name, 30) }}</h6>
                                        @endif
                                    </div>
                                    <div class="col-md-5 user-message">
                                        <p>{{ $sender['name'] }}:
                                            <span class="green-color">
                                                {!! truncateSentence($var->getTitle(), 50) !!}
                                            </span>
                                        </p>
                                    </div>
                                    <div class="col-md-2 time">
                                        <span>
                                            {{ $var->getLatestInboxMessageDate() }}
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection