@extends('backend.layout')

@section('title')
    {{ trans('common.inbox_messages') }}
@endsection

@section('description')
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('officialprojects.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.inbox') }}">{{trans('common.inbox_messages')}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <link href="{{asset('assets/global/plugins/lightbox2/dist/css/lightbox.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="/custom/css/backend/inbox.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/modalMakeOffer.css" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 right-top-main">
            <div class="row">
                <div class="col-md-12 top-tabs-menu">
                    <div class="order-status-menu pull-right">
                        <ul>
                            <li class="{{ $inbox->isNormalMessage()? 'active': '' }}">
                                <a href="{{ route('backend.inbox') }}?category=1">
                                    {{ trans('common.inbox') }}
                                </a>
                            </li>
                            <li class="{{ $inbox->isSystemMessage()? 'active': '' }}">
                                <a href="{{ route('backend.inbox') }}?category=2">
                                    {{ trans('common.system_message') }}
                                </a>
                            </li>
                            <li class="{{ $inbox->isTrash()? 'active': '' }}">
                                <a href="{{ route('backend.inbox') }}?isTrash=1">
                                    {{ trans('common.trash') }}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="caption">
                        {{--<span class="find-experts-search-title text-uppercase">{{ trans('common.inbox_messages') }}</span>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tab-content">
        <div class="tab-pane active chat-details-section" id="inbox">
            <div class="row header-action">
                <div class="col-md-12">
                    @if( $inbox->isNormalMessage() )
                    <a href="{{ route('backend.inbox') }}?category=1" class="back">
                    @elseif( $inbox->isSystemMessage() )
                    <a href="{{ route('backend.inbox') }}?category=2" class="back">
                    @elseif( $inbox->isTrash() )
                    <a href="{{ route('backend.inbox') }}?isTrash=1" class="back">
                    @endif
                    {{--<a href="{{ URL::previous() }}" class="back" onclick="history.go(-1);">--}}
                        <i class="fa fa-chevron-left" aria-hidden="true"></i>
                        <span>
                            {{ trans('common.back_to_inbox') }}
                        </span>
                    </a>
                </div>
            </div>
            <div class="portlet box" id="inbox-{{ $inbox->id }}">
                <div class="portlet-body">
                    <div class="row header-section">
                        <div class="col-md-7">
                            <h5>{{ $inbox->getSenderInfo()['name'] }}</h5>
                            @if( isset($inbox->project) )
                            <h6>{{ $inbox->project->name }}</h6>
                            @endif
                        </div>
                        <div class="col-md-5 text-right">
                            @if( isset($inbox->project) )
                            <a href="{{ route('frontend.joindiscussion.getproject', [ 'slug' => $inbox->project->id, 'id' => $inbox->project->id ]) }}">
                                <button class="btn-customs-all btn-green-custom margin-r-10">
                                    {{ trans('common.job_details') }}
                                </button>
                            </a>
                            @endif

                            @if( $inbox->is_trash == 0 )
                            <a class="btn-delete" data-url="{{ route('frontend.inbox.trash') }}" data-inbox-id="{{ $inbox->id }}">
                                <button class="btn-customs-all btn-default-custom">
                                    {{ trans('common.delete') }}
                                </button>
                            </a>
                            @else
                            <a class="btn-undelete" data-url="{{ route('frontend.inbox.untrash') }}" data-inbox-id="{{ $inbox->id }}">
                                <button class="btn-customs-all btn-default-custom">
                                    {{ trans('common.undelete') }}
                                </button>
                            </a>
                            @endif
                        </div>
                    </div>

                    <div class="row body-section">
                        @foreach( $inboxMessages as $key => $inboxMessage )
                        <div class="col-md-12 message-section">
                            <div class="row">
                                @php
                                    $senderInfo = $inboxMessage->getSenderInfo()
                                @endphp
                                <div class="col-md-1 user-img">
                                    <img src="{{ $senderInfo['avatar'] }}">
                                </div>
                                <div class="col-md-10 user-detail">
                                    <h5>{{ $senderInfo['name'] }}</h5>
                                    <span>{{ $inboxMessage->getCreatedDate() }}</span>

                                    <p>
                                        {!! $inboxMessage->getCustomMessage() !!}
                                    </p>

                                    @if( $inboxMessage->file != null )
                                    <p>
                                        <a href="{{ $inboxMessage->getFile() }}" data-lightbox="image" >
                                            <img src="{{ $inboxMessage->getFile() }}" width="100px">
                                        </a>
                                    </p>
                                    @endif

                                    @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_APPLICANT_SELECTED )
                                    <div class="row message-section-sub">
                                        <div class="col-md-12 message-section-subs">
                                            <h5>{{ trans('common.congrat_to_selected_user') }}</h5>
                                            {{--<p>Pinterest standard Graphic designer for long term.</p>--}}
                                            {{--<p>Budget: <span>$ 4500</span> </p>--}}
                                            <br>
                                            @if( $inboxMessage->from_staff_id != $staff->id )
                                            <a class="acceptJob"
                                                data-url="{{ route('frontend.inbox.acceptjob') }}"
                                                data-project-id="{{ $inbox->project->id }}"
                                                data-inbox-id="{{ $inbox->id }}">
                                                <button class="btn-customs-all btn-green-custom margin-r-10">
                                                    {{ trans('common.accept') }}
                                                </button>
                                            </a>

                                            <a class="rejectJob"
                                                data-url="{{ route('frontend.inbox.rejectjob') }}"
                                                data-project-id="{{ $inbox->project->id }}"
                                                data-inbox-id="{{ $inbox->id }}">
                                                <button class="btn-customs-all btn-default-custom">
                                                    {{ trans('common.reject') }}
                                                </button>
                                            </a>
                                            @endif
                                        </div>
                                    </div>
                                    @endif

                                    @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_NEW_MESSAGE  )
                                    @endif

                                    @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_CONFIRMED_COMPLETED )
                                    <div class="row message-section-sub">
                                        <div class="col-md-12 message-section-subs">
                                            <h5>{{ trans('common.project_completed') }}</h5>
                                            <br>
                                        </div>
                                    </div>
                                    @endif

                                    @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_INVITE_YOU )
                                    <div class="row message-section-sub">
                                        <div class="col-md-12 message-section-subs">
                                            <h5>{{ trans('common.send_invite_by_the_project') }}</h5>
                                            <p>{{ $inboxMessage->project->name }}</p>
                                            {{--<p>Budget: <span>$ 4500</span> </p>--}}
                                            <br>

                                            @if( $inboxMessage->from_user_id != $user->id )
                                            <a href="{{ route('frontend.modal.makeoffer', [
                                                    'project_id' => $inboxMessage->project->id,
                                                    'user_id' => \Auth::id(),
                                                    'inbox_id' => $inboxMessage->inbox_id
                                                ] ) }}"
                                                data-toggle="modal"
                                                data-target="#modalMakeOffer">
                                                <button class="btn-customs-all btn-green-custom margin-r-10">
                                                    {{ trans('common.accept') }}
                                                </button>
                                            </a>
                                            <a class="rejectInvite"
                                                data-url="{{ route('frontend.inbox.rejectinvite') }}"
                                                data-receiver-id="{{ $inboxMessage->from_user_id }}"
                                                data-inbox-id="{{ $inbox->id }}">
                                                <button class="btn-customs-all btn-default-custom">
                                                    {{ trans('common.reject') }}
                                                </button>
                                            </a>
                                            @endif
                                        </div>
                                    </div>
                                    @endif

                                    @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_SEND_OFFER )
                                    <div class="row message-section-sub">
                                        <div class="col-md-12 message-section-subs">
                                            <h5>{{ trans('common.send_offer_by_the_project') }}</h5>
                                            <p>{{ $inboxMessage->project->name }}</p>
                                            @if ( $inboxMessage->quote_price != null )
                                            <p> {{ trans('common.offer') }}: <span>$ {{ $inboxMessage->quote_price }} ( {{ \App\Constants::translatePayType( $inboxMessage->pay_type ) }} )</span> </p>
                                            @endif
                                            <br>

                                            @if( $inboxMessage->from_user_id != $user->id )
                                            <a class="acceptSendOffer"
                                                data-url="{{ route('frontend.inbox.acceptsendoffer') }}"
                                                data-receiver-id="{{ $inboxMessage->from_user_id }}"
                                                data-inbox-id="{{ $inbox->id }}">
                                                <button class="btn-customs-all btn-green-custom margin-r-10">
                                                    {{ trans('common.accept') }}
                                                </button>
                                            </a>
                                            <a class="rejectSendOffer"
                                                data-url="{{ route('frontend.inbox.rejectsendoffer') }}"
                                                data-receiver-id="{{ $inboxMessage->from_user_id }}"
                                                data-inbox-id="{{ $inbox->id }}">
                                                <button class="btn-customs-all btn-default-custom">
                                                    {{ trans('common.reject') }}
                                                </button>
                                            </a>
                                            @endif
                                        </div>
                                    </div>
                                    @endif

                                    @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_ASK_FOR_CONTACT &&
                                        $inboxMessage->from_user_id != $user->id )
                                    <div class="row message-section-sub">
                                        <div class="col-md-12 message-section-subs">
                                            <h5>{{ trans('common.request_for_contact_visible') }}</h5>
                                            <p> {{ trans('common.email') }} : <span>{{ $user->email }}</span> </p>
                                            <p> Line : <span>{{ $user->line_id }}</span> </p>
                                            <p> {{ trans('common.wechat') }} : <span>{{ $user->wechat_id }}</span> </p>
                                            <p> Skype : <span>{{ $user->skype_id }}</span> </p>
                                            <br>
                                            <a class="acceptContactInfoRequest"
                                                data-url="{{ route('frontend.inbox.acceptrequestforcontactinfo') }}"
                                                data-receiver-id="{{ $inboxMessage->from_user_id }}"
                                                data-inbox-id="{{ $inbox->id }}">
                                                <button class="btn-customs-all btn-green-custom margin-r-10">
                                                    {{ trans('common.accept') }}
                                                </button>
                                            </a>
                                            <a class="rejectContactInfoRequest"
                                                data-url="{{ route('frontend.inbox.rejectrequestforcontactinfo') }}"
                                                data-receiver-id="{{ $inboxMessage->from_user_id }}"
                                                data-inbox-id="{{ $inbox->id }}">
                                                <button class="btn-customs-all btn-default-custom">
                                                     {{ trans('common.reject') }}
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                    @endif

                                    @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_AGREE_SEND_CONTACT)
                                    <div class="row message-section-sub">
                                        <div class="col-md-12 message-section-subs">
                                            <h5>{{ trans('common.accepted_contact_visibility') }}</h5>
                                            <p> {{ trans('common.email') }} : <span>{{ $inboxMessage->fromUser->email }}</span> </p>
                                            <p> Line : <span>{{ $inboxMessage->fromUser->line_id }}</span> </p>
                                            <p> {{ trans('common.wechat') }} : <span>{{ $inboxMessage->fromUser->wechat_id }}</span> </p>
                                            <p> Skype : <span>{{ $inboxMessage->fromUser->skype_id }}</span> </p>
                                            <br>
                                            {{--<button class="btn-customs-all btn-green-custom margin-r-10">Confirm</button>--}}
                                            {{--<button class="btn-customs-all btn-default-custom">Cancel</button>--}}
                                        </div>
                                    </div>
                                    @endif

                                    @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_REJECT_SEND_CONTACT)
                                    <div class="row message-section-sub">
                                        <div class="col-md-12 message-section-subs">
                                            <h5>{{ trans('common.rejected_contact_visibility') }}</h5>
                                            <br>
                                        </div>
                                    </div>
                                    @endif

                                    @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_INVITE_ACCEPT)
                                    <div class="row message-section-sub">
                                        <div class="col-md-12 message-section-subs">
                                            <h5>{{ trans('common.invite_accept') }}</h5>
                                            <br>
                                        </div>
                                    </div>
                                    @endif

                                    @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_INVITE_REJECT)
                                    <div class="row message-section-sub">
                                        <div class="col-md-12 message-section-subs">
                                            <h5>{{ trans('common.invite_reject') }}</h5>
                                            <br>
                                        </div>
                                    </div>
                                    @endif

                                    @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_ACCEPT_SEND_OFFER)
                                    <div class="row message-section-sub">
                                        <div class="col-md-12 message-section-subs">
                                            <h5>{{ trans('common.accept_send_offer') }}</h5>
                                            <br>
                                        </div>
                                    </div>
                                    @endif

                                    @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_REJECT_SEND_OFFER)
                                    <div class="row message-section-sub">
                                        <div class="col-md-12 message-section-subs">
                                            <h5>{{ trans('common.reject_send_offer') }}</h5>
                                            <br>
                                        </div>
                                    </div>
                                    @endif

                                    @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_APPLICANT_ACCEPTED)
                                    <div class="row message-section-sub">
                                        <div class="col-md-12 message-section-subs">
                                            <h5>{{ trans('common.you_already_accept_job') }}</h5>
                                            <br>
                                        </div>
                                    </div>
                                    @endif

                                    @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_APPLICANT_REJECTED)
                                    <div class="row message-section-sub">
                                        <div class="col-md-12 message-section-subs">
                                            <h5>{{ trans('common.you_already_reject_job') }}</h5>
                                            <br>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endforeach
                        {{--<div class="col-md-12 message-section">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-1 user-img">--}}
                                    {{--<img src="/images/f-user-icon1.png">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-10 user-detail">--}}
                                    {{--<h5>Adam Johnson</h5>--}}
                                    {{--<span>3 min ago</span>--}}

                                    {{--<p>Duis tincidunt lectus quis dul viverra vestlbulum. Suspendisse vulputate aliquam dui. Nulla elementum dui ut augue.</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="col-md-12 message-section">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-1 user-img">--}}
                                    {{--<img src="/images/avatar_xenren.png">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-10 user-detail">--}}
                                    {{--<h5>You</h5>--}}
                                    {{--<span>Just Now</span>--}}

                                    {{--<p>Duis tincidunt lectus quis dul viverra vestlbulum. Suspendisse vulputate aliquam dui. Nulla elementum dui ut augue.</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    </div>

                    <div class="chat-form">
                        @if( $inbox->isNormalMessage() || $inbox->type == \App\Models\UserInbox::TYPE_PROJECT_ASK_FOR_CONTACT )
                        {{--<form action="" method="post" class="">--}}
                            <div class="row chat-form-main">
                                <div class="col-md-1 chat-form-l">
                                    <div class="upload-picture upload-btn-custom">
                                        <a href="{{ route('backend.inbox.chatimage', ['inbox_id' => $inbox->id, 'receiver_id' =>$inbox->getReceiver(false, Auth::guard('staff')->user()->id )['id']] ) }}"
                                            data-toggle="modal"
                                            data-target="#modalAddFileChat">
                                            <i class="fa fa-paperclip" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <div class="upload-btn-custom">
                                        <a href="javascript:;">
                                            <i class="fa fa-cog" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-10 chat-form-r">
                                    <div class="input-cont">
                                        <input class="form-control tbxMessage" name="message" id="message" placeholder="" type="text">
                                    </div>
                                </div>
                                <div class="col-md-1 submit-btn-custom">
                                    <button type="button" class="btn btn-submit btnSendMessage"
                                        data-inbox-id="{{ $inbox->id }}"
                                        data-receiver-id = "{{ $inbox->getReceiver(false, Auth::guard('staff')->user()->id )['id'] }}"
                                        data-url="{{ route('backend.inbox.sendmessage') }}">
                                        <i class="fa fa-send" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>

                        {{--</form>--}}
                        @endif
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="tab-pane" id="system-message">--}}
            {{--<div class="row header-action">--}}
                {{--<div class="col-md-12">--}}
                    {{--<a href="" class="back">--}}
                        {{--<i class="fa fa-chevron-left" aria-hidden="true"></i>--}}
                        {{--<span>back to inbox</span>--}}
                    {{--</a>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="portlet box">--}}
                {{--<div class="portlet-body">--}}
                    {{--System Message--}}
                {{--</div>--}}
            {{--</div>	--}}
        {{--</div>--}}
        {{--<div class="tab-pane" id="trash">--}}
            {{--<div class="row header-action">--}}
                {{--<div class="col-md-12">--}}
                    {{--<a href="" class="back">--}}
                        {{--<i class="fa fa-chevron-left" aria-hidden="true"></i>--}}
                        {{--<span>back to inbox</span>--}}
                    {{--</a>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="portlet box">--}}
                {{--<div class="portlet-body">--}}
                    {{--Trash--}}
                {{--</div>--}}
            {{--</div>	--}}
        {{--</div>--}}
    </div>

@endsection

@section('modal')
    <div id="modalMakeOffer" class="modal fade apply-job-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>

    <div id="modalAddFileChat" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{asset('assets/global/plugins/lightbox2/dist/js/lightbox.min.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        var lang = {
            'unable_to_request' : "{{ trans('common.unable_to_request') }}",
        };
    </script>
    <script src="/custom/js/backend/inboxMessages.js" type="text/javascript"></script>
    <script type="text/javascript">
        +function () {
            $(document).ready(function () {
                var _self = this;

                //handlers
                document.addEventListener('paste', function (e) {
                    _self.paste_auto(e);
                }, false);

                //on paste
                this.paste_auto = function (e) {
                    if (e.clipboardData) {
                        var items = e.clipboardData.items;
                        if (!items) return;

                        //access data directly
                        for (var i = 0; i < items.length; i++) {
                            if (items[i].type.indexOf("image") !== -1) {
                                //image
                                var blob = items[i].getAsFile();

                                var URLObj = window.URL || window.webkitURL;
                                var source = URLObj.createObjectURL(blob);

                                var f = document.createElement("form");
                                f.setAttribute('id',"upload-image-form");
                                f.setAttribute('method',"post");
                                f.setAttribute('enctype',"multipart/form-data");

                                var fd = new FormData();
                                var attrs = $("#upload-image-form").serialize().split("&");
                                for (i = 0; i < attrs.length - 1; i++) {
                                    var temp = attrs[i].split('=');
                                    fd.append(temp[0], temp[1]);
                                }
                                fd.append("inbox-id", "{{ $inbox != null? $inbox->id: 0 }}");
                                fd.append("receiver-id", "{{ $inbox->getReceiver(false, Auth::guard('staff')->user()->id )['id'] }}");
                                fd.append('file', blob);
                                $.ajax({
                                    type: 'POST',
                                    url: '{{ route('backend.inbox.chatimage.post')}}',
                                    data: fd,
                                    processData: false,
                                    contentType: false,
                                    error: function () {
                                        alert(lang.unable_to_request);
                                    },
                                    success: function (result) {
                                        if (result.status == 'success') {
                                            alertSuccess(result.msg);
                                            setTimeout(function () {
                                                location.reload();
                                            }, 1000);
                                        } else{
                                            alertError(result.msg);
                                        }
                                    }
                                });
                            }
                        }
                        e.preventDefault();
                    }
                };
            });
        }(jQuery);
    </script>
@endsection