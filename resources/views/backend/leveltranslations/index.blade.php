@extends('backend.layout')

@section('title')
    {{ trans('leveltranslations.title') }}
@endsection

@section('description')
    {{trans('leveltranslations.desc')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('bank.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.leveltranslations') }}">{{trans('leveltranslations.title')}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <link href="/custom/plugins/colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css"/>
@endsection

@section('footer')
    <script src="/custom/plugins/colorpicker/js/colorpicker.js"></script>
    <script>
        var url = "{{ route('backend.leveltranslations.dt') }}";
        +function () {
            $(document).ready(function () {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#leveltranslations-dt'),
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": url,
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'code_name', name: 'code_name'},
                            {data: 'names', name: 'names'},
                            {data: 'min_experience', name: 'min_experience'},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false}
                        ],
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('leveltranslations.title')}}</span>
            </div>
            <div class="actions">
                <a href="{{ route('backend.leveltranslations.create') }}" class="btn btn-circle red-sunglo btn-sm"
                   data-target="#remote-modal" data-toggle="modal"><i class="fa fa-plus"></i> 新增银行</a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="leveltranslations-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th>ID</th>
                        <th width="700">{{trans('leveltranslations.code_name')}}</th>
                        <th width="700">{{trans('leveltranslations.name')}}</th>
                        <th>{{trans('leveltranslations.experience')}}</th>
                        <th></th>

                    </tr>
                    <tr role="row" class="filter">
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_id"
                                   placeholder="ID"/>
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_name"
                                   placeholder="{{trans('leveltranslations.name')}}">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter__code_name"
                                   placeholder="{{trans('leveltranslations.code_name')}}">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_experience"
                                   placeholder="{{trans('leveltranslations.experience')}}">
                        </td>
                        <td>
                            <div class="input-group margin-bottom-5">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_created_after" placeholder="{{trans('bank.开始')}}">
                                <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                            <div class="input-group">
                                <input type="text" class="form-control form-filter input-sm datetime-picker"
                                       readonly name="filter_created_before" placeholder="{{trans('bank.结束')}}">
                                <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                        </td>
                        <td>
                            <div class="margin-bottom-5">
                                <button class="btn btn-info-alt filter-submit">
                                    <i class="fa fa-search"></i> {{trans('bank.过滤')}}
                                </button>
                            </div>
                            <button class="btn btn-danger-alt filter-cancel">
                                <i class="fa fa-times"></i> {{trans('bank.重置')}}
                            </button>
                        </td>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection