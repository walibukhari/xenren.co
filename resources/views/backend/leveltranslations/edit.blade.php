@extends('ajaxmodal')

@section('title')
    {{trans('leveltranslations.title')}}
@endsection

@section('script')
    <script>
        +function () {
            $(document).ready(function () {
                $('#leveltranslations-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit-leveltranslations'
                });

                $('#color-picker-1').ColorPicker({
                    color: '#{{ $model->background_color }}',
                    onShow: function (colpkr) {
                        $(colpkr).fadeIn(500);
                        return false;
                    },
                    onHide: function (colpkr) {
                        $(colpkr).fadeOut(500);
                        return false;
                    },
                    onChange: function (hsb, hex, rgb) {
                        $('#color-picker-1-preview').css('backgroundColor', '#' + hex);
                        $('#color-picker-1').val(hex);
                    }
                });

                $('#color-picker-2').ColorPicker({
                    color: '#{{ $model->font_color }}',
                    onShow: function (colpkr) {
                        $(colpkr).fadeIn(500);
                        return false;
                    },
                    onHide: function (colpkr) {
                        $(colpkr).fadeOut(500);
                        return false;
                    },
                    onChange: function (hsb, hex, rgb) {
                        $('#color-picker-2-preview').css('backgroundColor', '#' + hex);
                        $('#color-picker-2').val(hex);
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit-leveltranslations" class="btn btn-primary blue">{{trans('common.edit')}}(
    </button>
@endsection

@section('content')
    {!! Form::open(['route' => ['backend.leveltranslations.edit.post', 'id' => $model->id], 'method' => 'post', 'role' => 'form', 'id' => 'leveltranslations-form', 'files' => true]) !!}
    <div class="form-body">
        <div class="form-group">
            <label>{{trans('leveltranslations.name_cn')}}</label>
            <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-building"></i>
                    </span>
                {!! Form::text('name_cn', $model->name_cn, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label>{{trans('leveltranslations.name_en')}}</label>
            <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-building"></i>
                    </span>
                {!! Form::text('name_en', $model->name_en, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label>{{trans('leveltranslations.code_name')}}</label>
            <div class="input-group">

                    <span class="input-group-addon">
                        <i class="fa fa-code"></i>
                    </span>
                {!! Form::text('code_name', $model->code_name, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label>{{trans('common.experience')}}</label>
            <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-code"></i>
                    </span>
                {!! Form::text('experience', $model->min_experience, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection