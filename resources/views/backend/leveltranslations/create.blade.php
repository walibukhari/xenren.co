@extends('ajaxmodal')

@section('title')
    {{trans('leveltranslations.title')}}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                $('#leveltranslation-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit-leveltranslation'
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit-leveltranslation" class="btn btn-primary blue">{{trans('skill.新增')}}</button>
@endsection

@section('content')
    {!! Form::open(['route' => ['backend.leveltranslations.create.post'], 'method' => 'post', 'role' => 'form', 'id' => 'leveltranslation-form', 'files' => true]) !!}
    <div class="form-body">
        <div class="form-group">
            <label>{{trans('leveltranslations.name_cn')}}</label>
            <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-building"></i>
                    </span>
                {!! Form::text('name_cn', '', ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label>{{trans('leveltranslations.name_en')}}</label>
            <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-building"></i>
                    </span>
                {!! Form::text('name_en', '', ['class' => 'form-control']) !!}
            </div>
        </div><div class="form-group">
            <label>{{trans('leveltranslations.code_name')}}</label>
            <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-building"></i>
                    </span>
                {!! Form::text('code_name', '', ['class' => 'form-control']) !!}
            </div>
        </div><div class="form-group">
            <label>{{trans('leveltranslations.experience')}}</label>
            <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-building"></i>
                    </span>
                {!! Form::text('experience', '', ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection