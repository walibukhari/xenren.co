@extends('backend.layout')

@section('title')
    {{trans('faqs.title')}}
@endsection

@section('description')
    {{trans('faqs.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans("sideMenu.后台")}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.faqs') }}">{{trans('faqs.title')}}</a>
        </li>
    </ul>
@endsection

@section('header')

@endsection

@section('footer')
    <script>
        var url = "{{ route('backend.faqs.dt') }}";
        +function () {
            $(document).ready(function () {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#skill-dt'),
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": url,
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'title', name: 'title', orderable: false, searchable: false},
                            {data: 'text', name: 'text', orderable: false, searchable: false},
                            {data: 'parent', name: 'parent', orderable: false, searchable: false},
                            {data: 'position', name: 'position', orderable: false, searchable: false},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false}
                        ],
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('faqs.list')}}</span>
            </div>
            <div class="actions">
                <a href="{{ route('backend.faqs.create') }}" class="btn btn-circle red-sunglo btn-sm"
                   data-target="#remote-modal-full" data-toggle="modal"><i class="fa fa-plus"></i> {{trans('faqs.add')}}
                </a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="skill-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th width="80">{{trans('common.id')}}</th>
                        <th width="200">{{trans("faqs.column.title")}}</th>
                        <th width="200">{{trans('faqs.column.text')}}</th>
                        <th width="200">{{trans('faqs.column.parent')}}</th>
                        <th width="200">{{trans('faqs.column.position')}}</th>
                        <th width="200">{{trans('faqs.column.created_at')}}</th>
                        <th width="200">{{trans('common.actions')}}</th>
                    </tr>
                    <tr role="row" class="filter">
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            <div class="input-group margin-bottom-5">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_created_after" placeholder="{{trans('skill.开始')}}">
                                <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                            <div class="input-group">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_created_before" placeholder="{{trans('skill.结束')}}">
                                <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                        </td>
                        <td>
                            <div class="margin-bottom-5">
                                <button class="btn btn-info-alt filter-submit">
                                    <i class="fa fa-search"></i> {{trans('skill.过滤')}}
                                </button>
                            </div>
                            <button class="btn btn-danger-alt filter-cancel">
                                <i class="fa fa-times"></i> {{trans('skill.重置')}}
                            </button>
                        </td>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection

@push('css')

<link href="{{asset('assets/global/plugins/bootstrap-summernote/summernote.css')}}" rel="stylesheet" type="text/css"/>
{{--<link href="{{asset('assets/global/plugins/bootstrap-summernote/summernote-bs3.css')}}" rel="stylesheet" type="text/css"/>--}}
@endpush
@push('js')

<script src="{{asset('assets/global/plugins/bootstrap-summernote/summernote.min.js')}}" type="text/javascript"></script>

@endpush