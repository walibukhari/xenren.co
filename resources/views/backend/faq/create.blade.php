@extends('ajaxmodal')

@section('title')
    {{trans('faqs.title')}}
@endsection

@section('script')
    <script>
        +function () {
            $(document).ready(function () {
                $('#faq-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit-faq'
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit-faq" class="btn btn-primary blue">{{trans('skill.新增')}}</button>
@endsection

@section('content')
    {!! Form::open(['route' => ['backend.faqs.create.post'], 'method' => 'post', 'role' => 'form', 'id' => 'faq-form', 'files' => true]) !!}
    {{csrf_field()}}
    <div class="form-body">
        <div class="form-group">
            <label>{{trans('faqs.column.title_cn')}}</label>
            <div class="input-group">
                    <span class="input-group-addon">
                    </span>
                {!! Form::text('title_cn', '', ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label>{{trans('faqs.column.title_en')}}</label>
            <div class="input-group">
                    <span class="input-group-addon">
                    </span>
                {!! Form::text('title_en', '', ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label>{{trans('project.category')}}</label>
            <div class="input-group">
                    <span class="input-group-addon">
                    </span>
                {!! Form::select('parent_id',$faqList, null,['class'=>'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label>{{trans('common.type')}}</label>
            <div class="input-group">
                    <span class="input-group-addon">
                    </span>
                <select class="form-control" name="type">
                    <option value="1">Co-Work User</option>
                    <option value="2">Freelancer</option>
                    <option value="3">Employer</option>
                    <option value="4">Co-Work Space Owner</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label>{{trans('faqs.column.position')}}</label>
            <div class="input-group">
                    <span class="input-group-addon">
                    </span>
                {!! Form::text('position', '', ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            <label>{{trans('faqs.column.content_cn')}}</label>

            {!! Form::textarea('text_cn', '', ['class' => 'form-control summernote']) !!}

        </div>
        <div class="form-group">
            <label>{{trans('faqs.column.content_en')}}</label>


            {!! Form::textarea('text_en', '', ['class' => 'form-control summernote ']) !!}

        </div>


    </div>
    {!! Form::close() !!}
    <script>
        $(function () {
            $('.summernote').summernote({
                dialogsInBody: true,
                height: 200
            });
        });

    </script>
@endsection

@section('modal')

@endsection
