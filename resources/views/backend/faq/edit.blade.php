@extends('ajaxmodal')

@section('title')
    {{trans('skill.修改技能')}}
@endsection

@section('script')
    <script>
        +function () {
            $(document).ready(function () {
                $('#faq-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit-faq'
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit-faq" class="btn btn-primary blue">{{trans('skill.修改')}}</button>
@endsection

@section('content')
    {!! Form::open(['route' => ['backend.faqs.edit.post', 'id' => $model->id], 'method' => 'post', 'role' => 'form', 'id' => 'faq-form', 'files' => true]) !!}
    <div class="form-body">
        {{csrf_field()}}
        <div class="form-group">
            <label>{{trans('faqs.column.title_cn')}}</label>
            <div class="input-group">
                    <span class="input-group-addon">
                    </span>
                {!! Form::text('title_cn', $model->title_cn, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label>{{trans('faqs.column.title_en')}}</label>
            <div class="input-group">
                    <span class="input-group-addon">
                    </span>
                {!! Form::text('title_en', $model->title_en, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            <label>{{trans('faqs.column.parent')}}</label>
            <div class="input-group">
                    <span class="input-group-addon">
                    </span>
                {!! Form::select('parent_id',$faqList,$model->parent_id,['class'=>'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label>{{trans('faqs.column.position')}}</label>
            <div class="input-group">
                    <span class="input-group-addon">
                    </span>
                {!! Form::text('position', $model->position, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label>{{trans('faqs.column.text_cn')}}</label>

            {!! Form::textarea('text_cn', $model->text_cn, ['class' => 'form-control summernote']) !!}

        </div>
        <div class="form-group">
            <label>{{trans('faqs.column.text_en')}}</label>


            {!! Form::textarea('text_en', $model->text_en, ['class' => 'form-control summernote ']) !!}

        </div>

    </div>
    {!! Form::close() !!}
    <script>
        $(function () {
            $('.summernote').summernote({
                dialogsInBody: true,
                height: 200
            });
        });

    </script>
@endsection

@section('modal')

@endsection