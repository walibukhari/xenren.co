@extends('backend.layout')

@section('title')
    {{trans('common.coin_management')}}
@endsection

@section('description')
    {{trans('common.give_coin')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sideMenu.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.giveCoin') }}">{{trans('sideMenu.coin')}}</a>
        </li>
    </ul>
@endsection

@section('header')

@endsection

@section('content')
    <div class="portlet light bordered" id="main-content">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('common.coin')}}</span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="user-dt">
                    <thead>
                        <tr role="row" class="heading">
                            <th>{{trans('common.id')}}</th>
                            <th>{{trans('common.user')}}</th>
                            <th>{{trans('common.project_type')}}</th>
                            <th>{{trans('common.project')}}</th>
                            <th>{{trans('common.coin_type')}}</th>
                            <th>{{trans('common.pay_type')}}</th>
                            <th>{{trans('common.total_time')}}</th>
                            <th>{{trans('common.total_dollar')}}</th>
                            <th>{{trans('common.total_coin_given')}}</th>
                            <th>{{trans('common.total_coin')}}</th>
                            <th>{{trans('common.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($projects as $k => $project)
                        <tr role="row" class="filter">
                            <td>{{ $loop->index+1 }}</td>
                            <td>{{ !is_null($project->user) ? $project->user->name : '-' }}</td>
                            <td>{{ $projectType[$project->project->type] }}</td>
                            <td>{{ $project->project->name }}</td>
                            <td>{{ is_null($project->coin_type) ? '-' : $coinList[$project->coin_type]['name'] }}</td>
                            <td>{{ $payType[$project->pay_type] }}</td>
                            @if($project->total_time > 0)
                                <td>{{ $project->total_time }} {{ trans('common.seconds') }}</td>
                            @else
                                <td>-</td>
                            @endif
                            <td style="text-align: right;">${{ $project->total_dollar }}</td>
                            <td style="text-align: right;">{{ is_null($project->amount) ? '-' : '$'.$project->amount }}</td>
                            <td style="text-align: right;" class="total_coin">{{ !$project->is_coin_given ? $project->total_dollar_all - floatval($project->amount) : 0 }}</td>
                            <td>
                                @if($project->total_dollar_all - floatval($project->amount) > 0 && !$project->is_coin_given && !is_null($project->coin_type))
                                    <button type="button" class="btn btn-sm red-sunglo give-moveer-coin" 
                                        data-address="{{ !is_null($project->coin_address) ? $project->coin_address : '' }}" 
                                        data-user="{{ !is_null($project->user) ? $project->user->id : ''  }}"
                                        data-project="{{ !is_null($project->project) ? $project->project->id : '' }}"
                                        data-coin-address="{{ !is_null($project->coin_type) ? $coinList[$project->coin_type]['contract'] : ''  }}"
                                        >{{ trans('common.give_coin') }}</button>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('footer')
<script src="https://unpkg.com/web3@latest/dist/web3.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    let minABI = [{
        "constant": false,
        "inputs": [
            {
                "name": "_to",
                "type": "address"
            },
            {
                "name": "_value",
                "type": "uint256"
            }
        ],
        "name": "transfer",
        "outputs": [
            {
                "name": "success",
                "type": "bool"
            }
        ],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    }]

    function validateCoin(project, user, amount) {
        var params = {
            project_id: project,
            user_id: user,
            amount: amount
        }

        return $.ajax({
            url: '{{ route('backend.giveCoin.validate') }}',
            type: 'POST',
            data: params,
        })
    }

    function addCoinUser(project, user, amount) {
        var params = {
            project_id: project,
            user_id: user,
            amount: amount
        }

        return $.ajax({
            url: '{{ route('backend.giveCoin.addUserCoin') }}',
            type: 'POST',
            data: params,
        })
    }

    $('.give-moveer-coin').click(async function(event) {
        var contractAddress = $(this).attr('data-coin-address');
        if (window.ethereum) {
            window.web3 = new Web3(ethereum);
            ethereum.autoRefreshOnNetworkChange = false
            try {
                const accounts = await ethereum.enable()
                const account = accounts[0]
                const moveerAddress = $(this).attr('data-address')
                var coinGiven = parseFloat($(this).parent().parent().find('.total_coin').text())
                if (moveerAddress.length > 0 && coinGiven > 0) {
                    // validate coin
                    const user = $(this).attr('data-user')
                    const project = $(this).attr('data-project')
                    var check = validateCoin(project, user, coinGiven)
                    var oriCoinGiven = coinGiven

                    check.then(response => {
                        if (response.status == 'success') {
                            // convert to hexadecimal
                            coinGiven = coinGiven * 1000000000000000000
                            coinGiven = '0x' + coinGiven.toString(16)

                            let contract = new web3.eth.Contract(minABI, contractAddress)
                            contract.methods.transfer(moveerAddress, coinGiven).send({
                                from: account,
                                gasPrice: web3.utils.toWei('2', 'gwei')
                            }).then(response => {
                                var addCoin = addCoinUser(project, user, oriCoinGiven)

                                addCoin.then(responseCoin => {
                                    if (responseCoin.status == 'success') {
                                        swal("{{ trans('common.success') }}", responseCoin.message, "success")
                                        window.location.reload();
                                    } else {
                                        swal("{{ trans('common.warning') }}", responseCoin.message, "warning")
                                    }
                                })
                            }).catch(e => {
                                swal("{{ trans('common.warning') }}", e.message, "warning")
                            });
                        } else {
                            swal("{{ trans('common.warning') }}", response.message, "warning")
                        }
                    })
                } else {
                    swal("{{ trans('common.warning') }}", '{{ trans('common.user_address_not_set') }}', "warning")
                }
            } catch(e) {
                swal("{{ trans('common.warning') }}", e.message, "warning")
            }

        } else if (window.web3) {
            window.web3 = new Web3(web3.currentProvider);
            ethereum.autoRefreshOnNetworkChange = false
            try {
                const accounts = await web3.eth.getAccounts()
                const account = accounts[0]
                const moveerAddress = $(this).attr('data-address')
                var coinGiven = parseInt($(this).parent().parent().find('.total_coin').text())
                if (moveerAddress.length > 0 && coinGiven > 0) {
                    // validate coin
                    const user = $(this).attr('data-user')
                    const project = $(this).attr('data-project')
                    var check = validateCoin(project, user, coinGiven)
                    var oriCoinGiven = coinGiven

                    check.then(response => {
                        if (response.status == 'success') {
                            // convert to hexadecimal
                            coinGiven = coinGiven * 1000000000000000000
                            coinGiven = '0x' + coinGiven.toString(16)

                            let contract = new web3.eth.Contract(minABI, contractAddress)
                            contract.methods.transfer(moveerAddress, coinGiven).send({
                                from: account,
                                gasPrice: web3.utils.toWei('2', 'gwei')
                            }).then(response => {
                                var addCoin = addCoinUser(project, user, oriCoinGiven)

                                addCoin.then(responseCoin => {
                                    if (responseCoin.status == 'success') {
                                        swal("{{ trans('common.success') }}", responseCoin.message, "success")
                                        window.location.reload();
                                    } else {
                                        swal("{{ trans('common.warning') }}", responseCoin.message, "warning")
                                    }
                                })
                            }).catch(e => {
                                swal("{{ trans('common.warning') }}", e.message, "warning")
                            });
                        } else {
                            swal("{{ trans('common.warning') }}", response.message, "warning")
                        }
                    })
                } else {
                    swal("{{ trans('common.warning') }}", '{{ trans('common.user_address_not_set') }}', "warning")
                }
            } catch(e) {
                swal("{{ trans('common.warning') }}", e.message, "warning")
            }

        } else {
            swal("{{ trans('common.warning') }}", "{{ trans("common.meta_mask_not_installed") }}", "warning")
        }
    });
</script>
@endsection