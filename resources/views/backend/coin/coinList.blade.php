@extends('backend.layout')

@section('title')
    {{trans('common.coin_management')}}
@endsection

@section('description')
    {{trans('common.coin_list')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sideMenu.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.coin') }}">{{trans('sideMenu.coin_list')}}</a>
        </li>
    </ul>
@endsection

@section('header')
<style type="text/css">
    .icon-large {
        font-size: 15px;
    }
    .swal-overlay {
        z-index: 10050;
    }
    .swal-modal {
        z-index: 10052;
    }
    .preview {
        margin-top: 10px;
    }
    [v-cloak] {
        display: none !important;
    }
</style>
@endsection

@section('content')
    <div class="portlet light bordered" id="main-content" v-cloak>
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('common.coin_list')}}</span>
            </div>
            <div class="actions">
                <button @click="addCoin" class="btn btn-circle red-sunglo btn-sm" data-target="#add-modal" data-toggle="modal"
                   ><i class="fa fa-plus"></i> {{ trans('common.add_coin') }}
                </button>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="user-dt">
                    <thead>
                        <tr role="row" class="heading">
                            <th>{{trans('common.id')}}</th>
                            <th>{{trans('common.coin')}}</th>
                            <th>{{trans('common.coin_cn')}}</th>
                            <th>{{trans('common.contract_address')}}</th>
                            <th>{{trans('common.symbol')}}</th>
                            <th>{{trans('common.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr role="row" class="filter" v-if="listCoin.length > 0" v-for="(coin, key) in listCoin">
                            <td>@{{ key + 1 }}</td>
                            <td>@{{ coin.name_en }}</td>
                            <td>@{{ coin.name_cn }}</td>
                            <td>@{{ coin.contract_address }}</td>
                            <td>@{{ coin.symbol }}</td>
                            <td>
                                <button @click="editCoin(coin)" data-target="#add-modal" data-toggle="modal" type="button" class="btn btn-sm btn-primary"><i class="icon-large fa fa-check-square-o"></i></button>
                                <button @click="deleteCoin(coin)" type="button" class="btn btn-sm btn-warning"><i class="icon-large fa fa-trash"></i></button>
                            </td>
                        </tr>
                        <tr v-else>
                            <td colspan="4">{{ trans('common.record_not_found') }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <!--begin::Modal-->
        <div class="modal fade" id="add-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="row" style="width: 100%;">
                            <div class="col-lg-6">
                                <h4 class="modal-title text-white" v-if="isAdd">{{ trans('common.add_coin') }}</h4>
                                <h4 class="modal-title text-white" v-else>{{ trans('common.edit_coin') }}</h4>
                            </div>
                            <div class="col-lg-6 text-right">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body text-center body-mdl form-horizontal" style="padding:40px;">
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{ trans('common.coin') }}:</label>
                            <div class="col-sm-9">
                                <input type="text" v-model="input.name_en" class="form-control" placeholder="{{ trans('common.coin_placeholder') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{ trans('common.coin_cn') }}:</label>
                            <div class="col-sm-9">
                                <input type="text" v-model="input.name_cn" class="form-control" placeholder="{{ trans('common.coin_cn_placeholder') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{ trans('common.contract') }}:</label>
                            <div class="col-sm-9">
                                <input type="text" v-model="input.contract" class="form-control" placeholder="{{ trans('common.contract_placeholder') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{ trans('common.symbol') }}:</label>
                            <div class="col-sm-9">
                                <input type="text" v-model="input.symbol" class="form-control" placeholder="{{ trans('common.symbol_placeholder') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{ trans('common.decimal') }}:</label>
                            <div class="col-sm-9">
                                <input type="text" v-model="input.decimal" class="form-control" placeholder="{{ trans('common.decimal_placeholder') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">{{ trans('common.coin_img') }}</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="file" accept="image/*" @change="onImgChange($event)" />
                                <div class="preview">
                                    <img v-if="input.img" :src="img_src" class="img-responsive" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button @click="submitCoin" type="button" class="btn btn-primary">{{ trans('common.submit') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Modal-->
    </div>
@endsection

@section('footer')
<script src="{{asset('js/vue.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/vue-resource.min.js')}}" type="text/javascript"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    // fix bootstap modal and sweet alert
    $.fn.modal.Constructor.prototype._enforceFocus = function() {};

    Vue.config.debug = true
    const app = new Vue({
        el:'#main-content',
        data: {
            listCoin: {},
            isAdd: true,
            img_src: '',
            input: {
                _token: '{{ csrf_token() }}',
                id: null,
                name_en: '',
                name_cn: '',
                contract: '',
                symbol: '',
                decimal: 18,
                img: ''
            }
        },
        methods: {
            getListCoin: function () {
                this.$http.get('{{ route('backend.coin.getListCoin') }}').then(response => {
                    var data = response.data;
                    if (data.status == 'success') {
                        this.listCoin = data.data
                    } else {
                        swal("{{ trans('common.warning') }}", data.message, "warning")
                    }
                }).catch(error => {
                    swal("{{ trans('common.warning') }}", error.message, "warning")
                })
            },
            editCoin: function(coin) {
                this.isAdd = false
                this.input.id = coin.id
                this.input.name_en = coin.name_en
                this.input.name_cn = coin.name_cn
                this.input.contract = coin.contract_address
                this.input.symbol = coin.symbol
                this.input.decimal = coin.decimal
                this.input.img = coin.img
                this.img_src = coin.img
            },
            addCoin: function() {
                this.isAdd = true
                this.input.id = null
                this.input.name_en = ''
                this.input.name_cn = ''
                this.input.contract = ''
                this.input.symbol = ''
                this.input.decimal = 18
                this.input.img = ''
                this.img_src = ''
            },
            onImgChange: function(e) {
                const file = e.target.files[0];
                this.input.img = file;
                this.img_src = URL.createObjectURL(file);
            },
            submitCoin: function() {
                var formData = new FormData();
                formData.append('_token', this.input._token);
                if (this.input.id !== null) {
                    formData.append('id', this.input.id);
                }
                formData.append('name_en', this.input.name_en);
                formData.append('name_cn', this.input.name_cn);
                formData.append('contract', this.input.contract);
                formData.append('symbol', this.input.symbol);
                formData.append('decimal', this.input.decimal);
                formData.append('img', this.input.img);

                this.$http.post('{{ route('backend.coin.submit') }}', formData).then(response => {
                    var data = response.data;
                    if (data.status == 'success') {
                        swal("{{ trans('common.success') }}", data.message, "success")
                        $('#add-modal').modal('hide')
                        this.getListCoin()
                    } else {
                        swal("{{ trans('common.warning') }}", data.message, "warning")
                    }
                }).catch(error => {
                    swal("{{ trans('common.warning') }}", error.message, "warning")
                })
            },
            deleteCoin: function(coin) {
                swal({
                    title: '{{ trans('common.warning') }}',
                    text: '{{ trans('common.are_you_sure') }}',
                    icon: 'warning',
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        var params = {
                            id: coin.id
                        }

                        this.$http.post('{{ route('backend.coin.delete') }}', params).then(response => {
                            var data = response.data;
                            if (data.status == 'success') {
                                swal("{{ trans('common.success') }}", data.message, "success")
                                this.getListCoin()
                            } else {
                                swal("{{ trans('common.warning') }}", data.message, "warning")
                            }
                        }).catch(error => {
                            swal("{{ trans('common.warning') }}", error.message, "warning")
                        })
                    }
                })
            }
        },
        mounted() {
            this.getListCoin()
        }
    })
</script>
@endsection