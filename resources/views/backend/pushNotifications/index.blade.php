@extends('backend.layout')

@section('title')
    Admin&nbsp;<small>Push Notification</small>
@endsection

@section('description')

@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">Dashboard</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Push Notifications</a>
            <i class="fa fa-circle"></i>
        </li>
    </ul>
@endsection

@section('header')
    <link href="/css/sharedOfficeRequest.css" rel="stylesheet" type="text/css">
    <style>
        .dangerBtn{
            background-color: red;
            border: 0px;
        }
        .dangerBtn:hover{
            background-color: red;
            border: 0px;
        }
        .dangerBtn:focus{
            background-color: red;
            border: 0px;
        }
    </style>
@endsection

@section('footer')
    <script>
        function sendRequestToCheck(){
            $('#fa-spin-check').show();
            $.ajax({
                url:'{{route('backend.sendPushNotification')}}',
                method:'GET',

                success: function (response) {
                    console.log('response');
                    console.log(response);
                    $('#fa-spin-check').hide();
                },
                error: function (error) {
                    console.log('error');
                    console.log(error);
                }
            })
        }
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase">Push Notifications</span>
            </div>
        </div>
        <div class="col-md-12">
            @if(session()->has('success_message'))
                <div class="alert alert-success">
                    {{session()->get('success_message')}}
                </div>
            @endif
        </div>
        <div class="portlet-body" id="checkPushNotification">
            <div class="row">
                <div class="col-md-12">
                    <label>{{trans('common.dd_d')}}</label>
                    <ul>
                        <li>{{trans('common.post_pro')}}.</li>
                        <li>{{trans('common.send_me')}}.</li>
                        <li>{{trans('common.request_c_in')}}.</li>
                        <li>{{trans('common.j_j')}}.</li>
                        <li>{{trans('common.s_r')}}.</li>
                        <li>{{trans('common.m_r')}}.</li>
                    </ul>
                    <br>
                    <br>
                    <button class="btn btn-success" onclick="sendRequestToCheck()">
                        <i id="fa-spin-check" style="display: none;" class="fa fa-refresh fa-spin"></i>
                        {{trans('common.c_p_n')}}
                    </button>
                </div>
            </div>
        </div>
    </div>

@endsection
