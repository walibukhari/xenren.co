@extends('backend.layout')

@section('title')
    {{trans('sharedoffice.sharedoffice')}}
@endsection

@section('description')
    {{trans('sharedoffice.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>

        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.sharedoffice')}}</a>
        </li>
    </ul>
@endsection

@section('header')
        <link rel="stylesheet" href="{{asset('assets/css/sharedofficeBarcode.css')}}">
        <style>
            .customCss {
                display: flex;
                align-items: center;
            }
            .customRowCss {
                width: 100%;
            }
            .customCaptionCss {
                float:right;
                display: flex;
            }
            .customLabelCss{
                align-items: center;
                display: flex;
                width:70px;
            }
            .customCssFaSearch {
                display: flex;
                align-items: center;
                padding: 10px;
                background: #57a957;
                color: #fff;
                cursor:pointer;
            }
            .customInputSearch {
                border-top-right-radius: 0px;
                border-bottom-right-radius: 0px;
            }
            .customFormCss {
                display:flex;
            }
            .customCssBtn {
                background: transparent;
                border: 0px;
                padding: 0px;
            }

            @media (max-width:480px) {
                .portlet.light.bordered {
                    border-radius: 17px;
                    width: 100% !important;
                    height: fit-content;
                }
                .page-content-wrapper .page-content {
                    display: flex;
                    justify-content: center;
                    height: fit-content;
                }
                .portlet.light.bordered {
                     border: 0px solid #e7ecf1!important;
                }
                .portlet.bordered {
                     border-left: 0px solid #e6e9ec!important;
                }
                .portlet.light.bordered>.portlet-title{
                    border-bottom: 0px solid #eef1f5;
                }
                .uppercase {
                    text-transform: capitalize !important;
                    font-weight: 500 !important;
                }
            }
        </style>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title customCss pcWeb">
            <div class="row customRowCss">
                <div class="col-md-6">
                    <div class="caption font-green-sharp">
                        <span class="caption-subject bold uppercase"> {{trans('sharedoffice.sharedoffice')}}</span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="caption font-green-sharp customCaptionCss">
                        <label class="customLabelCss">{{trans('common.search')}} : </label>
                        <form class="customFormCss" action="{{ route('backend.sharedofficeSearchbarcodes') }}" method="post">
                            @csrf
                        <input type="text" name="text" class="form-control customInputSearch">
                        <button class="customCssBtn" type="submit"><i class="fa fa-search customCssFaSearch" aria-hidden="true"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="portlet-body pcWeb">
            @if(count($sharedofficebarcode) > 1)
                @foreach($sharedofficebarcode as $k => $v)
                <div class="QRcontainer">
                    <div class="innerQRleft">
                            <img class="QRimg qRImage" onclick="openPopup('{{$v->google_barcode}}')" src="{{$v->google_barcode}}"/>
                    </div>
                    <input type="hidden" value="{{$v->shared_office_id}}" class="office_id">
                    <div class="innerQRright">
                        <div class="btnswtich">
                            <p style="display:inline-block; font-weight:600; font-size:12pt; color:#a0a0a0">Public</p>
                            <label class="switch">
                                <input type="checkbox" @if($v->active==1) checked="checked" @endif name="{{$v->shared_office_id}}" id="check" onclick="check('active',{{$v}})">
                                <span class="slider round"></span>
                            </label>
                            <div>
                                @if($v->bid == 1)
                                    <button class="setdefaultbtn" style="background-color:#25ce0f; color:#fff;" id="setdefaultbtn" onclick="check('barcode',{{$v}})">Set As Default</button>
                                @else
                                    <button class="setdefaultbtn" id="setdefaultbtn" onclick="check('barcode',{{$v}})">Set As Default</button>
                                @endif
                            </div>
                        </div>
                        <p class="p1">Chat Room Name:{{$v->sharedOffice->office_name}}</p>
                        <p class="p1">Chat Room Password:{{$v->password}}</p>
                        <p class="p2">
                        @if($v->type == 1)
                                Enterprise only wish to use chatting module, with using XENREN hourly office Manage service.
                         @endif
                        @if($v->type == 2)
                                <span style="color:#800080;">Owner wish to user the hourly Manage Service, the office OR and seat OR Setup</span>
                        @endif
                        @if($v->type == 3)
                                <span style="color:#0000FF;">Enterprise using the hourly office Manage Service, and only need chat module. without using the seat Manage OR</span>
                        @endif
                        </p>
                        <div class="video-area hvr_{{$v->id}}" onmouseleave="hideHover({{$v->id}})" onmouseover="showHover('{{$v->id}}')" data-toggle="modal" data-target="#myVideoPopup"  data-link="{{ asset('video/xenren_home.mp4') }}">
                            <img src="{{asset('images/film-action.png')}}">
                        </div>
                        <div class="hover-box-show-video show-video_{{$v->id}}" style="display: none;">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12" style="padding:9px;">
                                        <video width="100%" height="100%" controls autoplay class="stylishHover addSrc" id="homePageVideo">
                                            Your browser does not support the video tag.
                                        </video>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @if(!isset($id))
                    {{$sharedofficebarcode->links()}}
                @endif
            @endif
        </div>

        <div class="mobilewebDesign" style="display: none;">
            <div class="portlet-title customCss mobilewebDesign">
                <div class="row customRowCss">
                    <div class="col-md-6 customColMd6">
                        <div class="caption font-green-sharp">
                            <span class="caption-subject bold uppercase"> {{trans('sharedoffice.sharedoffice')}}</span>
                        </div>
                    </div>
                    <div class="col-md-6 customRowColMd62">
                        <i onclick="openSearchBar()" class="fa fa-search faSearch" aria-hidden="true"></i>
                        <form class="customFormCss" id="customFormCss" style="display: none" action="{{ route('backend.sharedofficeSearchbarcodes') }}" method="post">
                            @csrf
                            <input type="text" name="text" class="form-control customInputSearch">
                            <button class="customCssBtn" type="submit"><i class="fa fa-search customCssFaSearch" aria-hidden="true"></i></button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                @if(count($sharedofficebarcode) > 1)
                    @foreach($sharedofficebarcode as $k => $v)
                        <div class="container customContainer">
                            <img class="qRImage setQrImage" onclick="openPopup('{{$v->google_barcode}}')" src="{{$v->google_barcode}}"/>
                            <input type="hidden" value="{{$v->shared_office_id}}" class="office_id">
                            <div class="innerContainer">
                                <p class="setChatRoom">Chat Room</p>
                                <p class="setChatRoom">Name: {{$v->sharedOffice->office_name}}</p>
                                <p class="setChatRoom">Password: {{$v->password}}</p>
                            </div>
                            <p class="btnbtnPublic">Public: </p>
                            <label class="switch">
                                <input type="checkbox" @if($v->active==1) checked="checked" @endif name="{{$v->shared_office_id}}" id="check" onclick="check('active',{{$v}})">
                                <span class="slider round"></span>
                            </label>
                        </div>
                        <div class="row customRowBtn">
                            @if($v->bid == 1)
                                <button class="setdefaultbtn" style="background-color:#25ce0f; color:#fff;" id="setdefaultbtn" onclick="check('barcode',{{$v}})">Set As Default</button>
                            @else
                                <button class="setdefaultbtn" id="setdefaultbtn" onclick="check('barcode',{{$v}})">Set As Default</button>
                            @endif
                        </div>
                        <div class="row customRow2">
                            <div class="col-md-10">
                                <p class="customp">
                                    @if($v->type == 1)
                                        Enterprise only wish to use chatting module, with using XENREN hourly office Manage service.
                                    @endif
                                    @if($v->type == 2)
                                        <span style="color:#800080;">Owner wish to user the hourly Manage Service, the office OR and seat OR Setup</span>
                                    @endif
                                    @if($v->type == 3)
                                        <span style="color:#0000FF;">Enterprise using the hourly office Manage Service, and only need chat module. without using the seat Manage OR</span>
                                    @endif
                                </p>
                            </div>
                            <div class="col-md-2">
                                <div class="video-area hvr_{{$v->id}}" onmouseleave="hideHover({{$v->id}})" onmouseover="showHover('{{$v->id}}')" data-toggle="modal" data-target="#myVideoPopup"  data-link="{{ asset('video/xenren_home.mp4') }}">
                                    <img src="{{asset('images/film-action.png')}}">
                                </div>
                            </div>
                        </div>
                    @endforeach
                    @if(!isset($id))
                        {{$sharedofficebarcode->links()}}
                    @endif



                        {{-- Video Modal --}}
                        <div id="myVideoPopup" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <img class="fa-time-close" src="{{asset('images/cross-times.png')}}">
                                    <div class="modal-body">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <video width="100%" height="100%" controls autoplay class="addHSrc" id="homePageVideo">
                                                        Your browser does not support the video tag.
                                                    </video>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                <!-- Modal -->
                @endif
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div id="barcodePopup" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <img class="bgImage" src="{{asset('/images/backgroundCover.png')}}">
                    <div class="xenre-logo">
                        <img src="{{asset('images/xenrenLogo.png')}}">
                        <p style="color: #6bc30f;-webkit-print-color-adjust: exact;">XenRen</p>
                        <span style="color: #6bc30f;-webkit-print-color-adjust: exact;">Coworker & Coworking space</span>
                    </div>
                    <div class="three-rounded-circle">
                        <div class="circle-1">
                            <img class="circle" src="{{asset('images/roundedCircle.png')}}">
                            <img class="iconnn" src="{{asset('images/dedicatedDesk.png')}}">
                            <p style="color: #6bc30f;-webkit-print-color-adjust: exact;">Dedicated Desk</p>
                        </div>
                        <div class="circle-1">
                            <img class="circle" src="{{asset('images/roundedCircle.png')}}">
                            <img class="iconnn" src="{{asset('images/HotDesk.png')}}">
                            <p style="color: #6bc30f;-webkit-print-color-adjust: exact;">Hot Desk</p>
                        </div>
                        <div class="circle-1">
                            <img class="circle" src="{{asset('images/roundedCircle.png')}}">
                            <img class="iconnn" style="width:74px;" src="{{asset('images/meetingRoom.png')}}">
                            <p style="color: #6bc30f;-webkit-print-color-adjust: exact;">Meeting Desk</p>
                        </div>
                    </div>
                    <div class="qr-frame">
                        <p style="color: #fff;-webkit-print-color-adjust: exact;">Get Scanning</p>
                        <div class="fit-frame">
                            <img class="iframe" src="{{asset('/images/qrFrame.png')}}">
                            <img class="fit-to-qr-frame" />
                        </div>
                        <button onclick="printAssessment()" class="printQr">Print Qr</button>
                        <br>
                        <span style="color: #fff;-webkit-print-color-adjust: exact;">it's easy! Scan the code to get full details and book co-work space.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script>
        function openSearchBar() {
            $('#customFormCss').slideToggle();
        }
        function printAssessment() {
            $('.portlet-title').hide();
            $('.portlet-title').css('border-bottom','0px');
            $('.page-bar').css('border-bottom','0px');
            $('.page-title').hide();
            $('.page-breadcrumb').hide();
            $('.QRcontainer').hide();
            $('.printQr').hide();
            window.print();
            closeWindow();
        }

        function closeWindow()
        {
            $('.portlet-title').show();
            $('.portlet-title').css('border-bottom','1px solid #e7ecf1');
            $('.page-bar').css('border-bottom','1px solid #e7ecf1');
            $('.page-title').show();
            $('.page-breadcrumb').show();
            $('.QRcontainer').show();
            $('.printQr').show();
            window.close();
        }


        function openPopup(barcode)
        {
            $('.fit-to-qr-frame').attr('src',barcode);
            $('#barcodePopup').modal('show');
        }


        $('#myVideoPopup').on('shown.bs.modal', function () {
            var video = $('.video-area').attr('data-link');
            console.log("click video");
            var add = $('.addHSrc').attr('src',video)
        });

        $('.fa-time-close').click(function(){
            $('#myVideoPopup').modal('hide');
        });



        function showHover(id) {
            var video = $('.video-area').attr("data-link");
            var add = $('.addSrc').attr('src',video)
            $('.show-video_'+id).addClass('showVideo');
        }

        function hideHover(id)
        {
            $('.show-video_'+id).removeClass('showVideo');
        }

        function check(val,data)
        {
            var url = '/admin/shared-office-active';
            var office_id = data.shared_office_id;
            var type = data.type;
            if(val === 'active')
            {
                var check = document.getElementById('check');
                var status = check.classList.toggle('active_class');
                if(status === true)
                {
                    var a = 1;
                    $.ajax({
                        url:url,
                        type:'post',
                        data:{
                            'active':a,
                            'office_id':office_id,
                            'val':val,
                            'type':type
                        },

                        success: function(response)
                        {
                            console.log(response);
                            if(response.success)
                            {
                                toastr.success('Shared office updated successfully');
                                window.location.reload();
                            }
                        },
                        error: function(error)
                        {
                            console.log(error)
                        }

                    });
                }
                else
                {
                    var a = 0;
                    $.ajax({
                        url:url,
                        type:'post',
                        data:{
                            'active':a,
                            'office_id':office_id,
                            'val':val,
                            'type':type
                        },

                        success: function(response)
                        {
                            console.log("response");
                            console.log(response);
                            console.log("response");
                            if(response.success)
                            {
                                toastr.success('Shared office updated successfully');
                                window.location.reload();
                            }
                        }

                    });
                }
            } else if(val === 'barcode')
            {
                var barcode = data.barcode;
                var type = data.type;
                var office_id = data.shared_office_id;
                    var a = 1;
                    $.ajax({
                        url:url,
                        type:'post',
                        data:{
                            'barcode':barcode,
                            'office_id':office_id,
                            'type':type,
                            'val':val,
                        },

                        success: function(response)
                        {
                            console.log("response");
                            console.log(response);
                            console.log("response");
                            if(response.success)
                            {
                                toastr.success('Shared office updated successfully');
                                window.location.reload();
                            }
                        }

                    });
            }
        }

    </script>
@endsection

@section('modal')

@endsection
