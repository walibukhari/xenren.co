@extends('backend.layout')

@section('title')
    {{trans('sharedoffice.sharedoffice')}}
@endsection


@section('description')

@endsection

<head xmlns:v-on="http://www.w3.org/1999/xhtml">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
</head>

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.sharedoffice')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedoffice.create') }}">{{trans('sharedoffice.create_office')}}</a>
        </li>
    </ul>
@endsection

@section('description')

@endsection

@section('footer')

@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-body form">
            @if(\Session::has('error'))
                <div class="alert alert-danger">
                    <span>{{\Session::get('error')}}</span>
                </div>
            @endif
            <form method="POST" action="{{route('backend.sharedofficebarcodes.editPost', $sharedofficebarcode->id)}}" id="sharedofficebarcode-form" enctype="multipart/form-data"  role="form" novalidate>
                @if(\Auth::guard('staff')->user()->staff_type == \App\Models\Staff::STAFF_TYPE_ADMIN)
                @php
                    $sharedOffices = \App\Models\SharedOffice::get();
                @endphp
                <div class="form-group">
                    <label for="office_id">Select Office</label>
                    <select name="office_id" id="office_id" class="form-control">
                        @foreach($sharedOffices as $k => $v)
                            <option value="{{$v->id}}">{{$v->office_name}}</option>
                        @endforeach
                    </select>
                </div>
                @endif
                <div class="form-group">
                    <label for="type">Select Type</label>
                    <select name="type" id="type" class="form-control">
                        <option value="1" @if($sharedofficebarcode->type == 1) selected @endif>Type 1</option>
                        <option value="2" @if($sharedofficebarcode->type == 2) selected @endif>Type 2</option>
                        <option value="3" @if($sharedofficebarcode->type == 3) selected @endif>Type 3</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="category">Select Category</label>
                    <select name="category" id="category" class="form-control">
                        <option value="1" @if($sharedofficebarcode->category == 1) selected @endif>Open</option>
                        <option value="2" @if($sharedofficebarcode->category == 2) selected @endif>Close</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="code">Code</label>
                    <input type="text" class="form-control" id="code" name="code" placeholder="Enter code" value="{{$sharedofficebarcode->code}}">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="text" class="form-control" id="password" name="password" placeholder="Enter password" value="{{$sharedofficebarcode->password}}">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
@endsection

@section('modal')

@endsection


