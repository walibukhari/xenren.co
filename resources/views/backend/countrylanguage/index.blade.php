@extends('backend.layout')

@section('title')
    {{ trans('sideMenu.country_language_management') }}
@endsection

@section('description')
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('officialprojects.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.feedback') }}">{{trans('sideMenu.country_language_management')}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <link href="/assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/backend/feedback.css" rel="stylesheet" type="text/css" />
@endsection

@section('footer')
    <script src="/assets/global/plugins/fancybox/source/jquery.fancybox.js" type="text/javascript"></script>
    <script>
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#country-language-dt'),
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": "{{ route('backend.countrylanguage.dt') }}",
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'country', name: 'country', orderable: false, searchable: false},
                            {data: 'language', name: 'language', orderable: false, searchable: false},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false}
                        ]
                    }
                });

            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('common.country_language_list')}}</span>
            </div>
            <div class="actions">
                <a href="{{ route('backend.countrylanguage.create') }}" class="btn btn-circle red-sunglo btn-sm"
                    data-target="#remote-modal" data-toggle="modal">
                    <i class="fa fa-plus"></i> {{trans('common.add_country_language')}}
                </a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="country-language-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th>{{ trans('common.id') }}</th>
                        <th>{{ trans('common.country') }}</th>
                        <th>{{ trans('common.language_2') }}</th>
                        <th>{{ trans('common.action') }}</th>
                    </tr>
                    <tr role="row" class="filter">
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_id"
                                placeholder="ID" />
                        </td>
                        <td>
                            {!! Form::select('filter_country', \App\Models\Countries::getCountryLists(true), '', ['class' => 'form-control form-filter input-sm']) !!}
                        </td>
                        <td>
                            {!! Form::select('filter_language', \App\Models\Language::getLanguageLists(true), '', ['class' => 'form-control form-filter input-sm']) !!}
                        </td>
                        <td>
                            <div class="margin-bottom-5">
                                <button class="btn btn-info-alt filter-submit">
                                    <i class="fa fa-search"></i> {{trans('common.filter')}}
                                </button>
                            </div>
                            <button class="btn btn-danger-alt filter-cancel">
                                <i class="fa fa-times"></i> {{trans('common.reset')}}
                            </button>
                        </td>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection