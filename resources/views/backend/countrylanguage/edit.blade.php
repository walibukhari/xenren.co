@extends('ajaxmodal')

@section('title')
    {{trans("common.edit_country_language")}}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                $('#country-language-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit'
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit" class="btn btn-primary blue">{{trans("common.edit")}}</button>
@endsection

@section('content')
    {!! Form::open(['route' => ['backend.countrylanguage.edit.post', 'id' => $countryLanguage->id], 'method' => 'post', 'role' => 'form', 'id' => 'country-language-form', 'files' => true]) !!}
        <div class="form-body">
            <div class="form-group">
                <label>{{trans('common.country')}}</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-globe"></i>
                    </span>
                    {!! Form::select('country_id', \App\Models\Countries::getCountryLists(false), $countryLanguage->country_id, ['class' => 'form-control form-filter input-sm']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>{{trans('common.language_2')}}</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-language"></i>
                    </span>
                    {!! Form::select('language_id', \App\Models\Language::getLanguageLists(false), $countryLanguage->language_id, ['class' => 'form-control form-filter input-sm']) !!}
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection