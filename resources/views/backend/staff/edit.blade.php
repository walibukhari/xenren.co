@extends('ajaxmodal')

@section('title')
    {{trans('staff.修改操作员')}}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                $('#staff-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit-staff'
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit-staff" class="btn btn-primary blue">{{trans('staff.修改')}}</button>
@endsection

@section('content')
    {!! Form::open(['route' => ['backend.staff.edit.post', 'id' => $model->id], 'method' => 'post', 'role' => 'form', 'id' => 'staff-form']) !!}
    <div class="form-body">
        <div class="form-group">
            <label>{{trans('staff.用户名')}}</label>
            <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-user"></i>
                    </span>
                {!! Form::text('username', $model->username, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label>{{trans('staff.电子邮件地址')}}</label>
            <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-envelope"></i>
                    </span>
                {!! Form::text('email', $model->email, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label>{{trans('staff.姓名')}}</label>
            <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-tag"></i>
                    </span>
                {!! Form::text('name', $model->name, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label>{{trans('staff.权限组')}}</label>
            <div class="input-group">
                @if ($is_owner)
                    @foreach ($permissions as $key => $permission)
                        <div class="form-check">
                            <input type="checkbox" name="permissions[]" class="form-check-input" id="{{ $key }}_edit" value="{{ $key }}" {{ !is_null($selectedPermission) ? (in_array($key, $selectedPermission) ? "checked" : "") : "" }}>
                            <label class="form-check-label" for="{{ $key }}_edit">{{ $permission }}</label>
                        </div>
                    @endforeach
                @else
                    <span class="input-group-addon">
                        <i class="fa fa-users"></i>
                    </span>
                    {!! Form::select('permission_group_id', $permissions, $model->permissionGroup ? $model->permissionGroup->id : null, ['class' => 'form-control']) !!}
                @endif
            </div>
        </div>
        <div class="form-group">
            <label>{{trans('staff.职称')}}</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-graduation-cap"></i>
                </span>
                {!! Form::text('title', $model->title, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label>{{trans('staff.密码')}}</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-key"></i>
                </span>
                {!! Form::password('password', ['class' => 'form-control']) !!}
            </div>
            <span class="help-block">{{trans('staff.如不修改密码请留空')}}</span>
        </div>
        <div class="form-group">
            <label>{{trans('skill.确认密码')}}</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-key"></i>
                </span>
                {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
            </div>
            <span class="help-block">{{trans('staff.如不修改密码请留空')}}</span>
        </div>
        <div class="form-group">
            <label>{{trans('staff.头像')}}</label>
            <div>
                <img id="image_preview" class="img-thumbnail avatar-preview" alt="" src="{{ (isset($model) && $model->img_avatar) ? url($model->img_avatar) : '/images/no-image-box2.png' }}"/>
            </div>
            <input accept="image/*" class="image-upload" target="image_preview" onChange="imageFileInputChange(this)" name="image" type="file">
        </div>
    </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection