@extends('backend.layout')

@section('title')
    {{trans('skill.操作员')}}
@endsection

@section('description')
    {{trans('skill.crud_skill')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sideMenu.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.staff') }}">{{trans('sideMenu.操作员')}}</a>
        </li>
    </ul>
@endsection

@section('header')

    <style>
        .portlet.light>.portlet-title>.caption>.caption-subject{
            font-size: 20px !important;
        }
        .table-checkable tr>td:first-child, .table-checkable tr>th:first-child{
            padding-right:10px;
            padding-left:10px;
        }
        .confirm-modal-detele{
            display: flex;
        }
        .descTT{
            font-size: 14px !important;
            color: #555 !important;
            font-weight: bold !important;
        }
        .titleTT{
            font-size: 14px !important;
            color: #25ce0f !important;
            font-weight: bold !important;
        }
        #staff-dt_paginate{
            display: none !important;
        }
        #staff-dt_length{
            display: none !important;
        }
        #staff-dt_info{
            display: none !important;
        }
        table.dataTable tr.heading>th {
            background-color: #fff !important;
            color: #555 !important;
            font-size: 12px;
            text-align: center;
        }
        .setBtnDesign{
            color: #fff;
            background-color: #25ce0f;
            border-color: #25ce0f;
            border-radius: 0px !important;
            height: 35px;
            display: flex;
            align-items: center;
        }
        .setFafa{
            position: relative;
            padding-right: 10px;
        }
        .setModalTitle{
            color:#25ce0f !important;
            font-weight: bold;
        }



        /* check box css */
        /* The container */
        .containerCheckBox {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 12px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default checkbox */
        .containerCheckBox input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom checkbox */
        .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 20px;
            width: 20px;
            background-color: #eee;
        }

        /* On mouse-over, add a grey background color */
        .containerCheckBox:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a blue background */
        .containerCheckBox input:checked ~ .checkmark {
            background-color: #25ce0f;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .containerCheckBox input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .containerCheckBox .checkmark:after {
            left: 8px;
            top: 3px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
        .span123{
            position: relative;
            top:2px;
        }

        .avatar-preview {
            line-height: 300px;
            text-align: center;
            color: #ddd;
            font-weight: bold;
            font-size: 2.0em;
            padding: 28px;
            border-radius: 8px;
        }

        #btn-submit-staff{
            background: #25ce0f;
            color: #fff;
            border-radius: 4px;
            border: 0px;
            width: 100px;
        }
        .btn-default{
        }
        .addLabelCss{
            font-weight: bold !important;
            color: #555 !important;
            font-size: 13px;
        }
        .addClassFormCss{
            font-size: 13px !important;
        }
    </style>

@endsection

@section('footer')
    <script>
        var url = "{{ route('backend.staff.dt') }}";
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#staff-dt'),
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": url,
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id'},
                            {data: 'username', name: 'username'},
                            {data: 'email', name: 'email'},
                            {data: 'name', name: 'name'},
                            {data: 'group', name: 'group', orderable: false, searchable: false},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'updated_at', name: 'updated_at'},
                            {data: 'actions', name: 'actions', orderable: false, searchable: false}
                        ]
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{session()->get('success')}}
            </div>
        @endif
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('skill.操作员列表')}} </span>
            </div>
            <div class="actions" style="margin-left: 5px;">
                <a href="{{ route('backend.staff.create') }}" class="setBtnDesign btn btn-circle red-sunglo btn-sm"
                   data-target="#remote-modal" data-toggle="modal"><i class="setFafa fa fa-plus"></i> {{trans('common.add_operator')}}
                </a>
            </div>
            @if(\Auth::user()->staff_type == \App\Models\Staff::STAFF_TYPE_ADMIN)
                <div class="actions">
                    <a href="javascript:;" class="setBtnDesign btn btn-circle red-sunglo btn-sm"
                       data-target="#show-trashed-users" data-toggle="modal"> {{trans('common.trashed_user')}}
                    </a>
                </div>
            @endif
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="staff-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th width="80">#</th>
                        <th>{{trans('skill.登入名')}}</th>
                        <th>{{trans("common.email")}}</th>
                        <th>{{trans('skill.名称')}}</th>
                        <th>{{trans('skill.权限组')}}</th>
                        <th>{{trans('skill.新增日期')}}</th>
                        <th>{{trans('skill.最后修改日期')}}</th>
                        <th>{{trans('common.actions')}}</th>
                    </tr>
                    <tr role="row" class="filter">
                        <td>
                            <input type="text" class="form-control form-filter input-sm number-input"
                                   name="filter_row_id" placeholder="{{trans('skill.id')}}">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_username"
                                   placeholder="{{trans('member.username')}}">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_email"
                                   placeholder="{{trans('member.email')}}">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_name"
                                   placeholder="{{trans('member.name')}}">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_group_name"
                                   placeholder="{{trans("member.group_name")}}">
                        </td>
                        <td>
                            <div class="input-group margin-bottom-5">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_created_after" placeholder="{{trans('member.from')}}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                            <div class="input-group">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_created_before" placeholder="{{trans('member.to')}}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                        </td>
                        <td>
                            <div class="input-group margin-bottom-5">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_updated_after" placeholder="{{trans('member.from')}}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                            <div class="input-group">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly
                                       name="filter_updated_before" placeholder="{{trans('member.to')}}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                        </td>
                        <td>
                            <div class="margin-bottom-5">
                                <button class="btn btn-info-alt filter-submit">
                                    <i class="fa fa-search"></i> {{trans('bank.过滤')}}
                                </button>
                            </div>
                            <button class="btn btn-danger-alt filter-cancel">
                                <i class="fa fa-times"></i> {{trans('bank.重置')}}
                            </button>
                        </td>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

        <!-- Modal -->
        <div id="show-trashed-users" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Trashed Users</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>
                                        <a href="{{route('backend.revert.trashed.users',[$user->id])}}" class="btn btn-sm btn-success">
                                            revert
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

    </div>
@endsection

@section('modal')

@endsection
