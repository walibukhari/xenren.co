@extends('ajaxmodal')

@section('title')
    {{trans('staff.新增操作员')}}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                $('#staff-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit-staff'
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit-staff" class="btn btn-primary blue">{{trans('staff.新增')}}</button>
@endsection

@section('content')
    {!! Form::open(['route' => 'backend.staff.create.post', 'method' => 'post', 'role' => 'form', 'id' => 'staff-form']) !!}
        <div class="form-body">
            @if(\Auth::user()->staff_type == \App\Models\Staff::STAFF_TYPE_STAFF)
                <input type="hidden" name="is_operator" value="1" />
            @endif
           <div class="row">
               <div class="col-md-6">
                   <div class="form-group">
                       <label class="addLabelCss">{{trans('staff.用户名')}}</label>
                       <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-user"></i>
                    </span>
                           {!! Form::text('username', '', ['class' => 'addClassFormCss  form-control']) !!}
                       </div>
                   </div>
               </div>
               <div class="col-md-6">
                   <div class="form-group">
                       <label class="addLabelCss">{{trans('staff.电子邮件地址')}}</label>
                       <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-envelope"></i>
                    </span>
                           {!! Form::text('email', '', ['class' => 'addClassFormCss  form-control']) !!}
                       </div>
                   </div>
               </div>
           </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="addLabelCss">{{trans('staff.姓名')}}</label>
                        <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-tag"></i>
                    </span>
                            {!! Form::text('name', '', ['class' => 'addClassFormCss  form-control']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <label class="addLabelCss">{{trans('staff.权限组')}}</label>
                    </div>
                </div>
            @if ($is_owner)
                <div class="row" style="margin-top: 12px;">
                @foreach ($permissions as $key => $permission)
                    <div class="col-md-6">
                        <label class="containerCheckBox" for="{{ $key }}_create">
                            <span class="span123">{{ $permission }}</span>
                            <input type="checkbox" name="permissions[]" id="{{ $key }}_create" value="{{ $key }}">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                @endforeach
                </div>
            @else
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-users"></i>
                        </span>
                        {!! Form::select('permission_group_id', $permissions, null, ['class' => 'addClassFormCss  form-control']) !!}
                </div>
            @endif
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="addLabelCss">{{trans('staff.职称')}}</label>
                        <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-graduation-cap"></i>
                    </span>
                            {!! Form::text('title', '', ['class' => 'addClassFormCss  form-control']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="addLabelCss">{{trans('staff.密码')}}</label>
                        <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-key"></i>
                    </span>
                            {!! Form::password('password', ['class' => 'addClassFormCss  form-control']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="addLabelCss">{{trans('staff.确认密码')}}</label>
                        <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-key"></i>
                    </span>
                            {!! Form::password('password_confirmation', ['class' => 'addClassFormCss  form-control']) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="addLabelCss">{{trans('staff.头像')}}</label>
                <div>
                    <img id="image_preview" class="img-thumbnail avatar-preview" alt="" src="{{ (isset($model) && $model->image) ? url($model->image) : '/images/avatarImages.png' }}"/>
                </div>
                <input accept="image/*" class="image-upload" target="image_preview" onChange="imageFileInputChange(this)" name="image" type="file">
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection
