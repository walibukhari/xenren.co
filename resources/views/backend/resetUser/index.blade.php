@extends('backend.layout')

@section('title')
    Admin&nbsp;<small>Reset User</small>
@endsection

@section('description')

@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">Dashboard</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Reset User</a>
            <i class="fa fa-circle"></i>
        </li>
    </ul>
@endsection

@section('header')
    <link href="/css/sharedOfficeRequest.css" rel="stylesheet" type="text/css">
    <style>
        .dangerBtn {
            background-color: red;
            border: 0px;
        }

        .dangerBtn:hover {
            background-color: red;
            border: 0px;
        }

        .dangerBtn:focus {
            background-color: red;
            border: 0px;
        }
    </style>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase">Reset User</span>
            </div>
        </div>
        <div class="col-md-12">
            <div class="alert alert-success">
                Make account as brand new for testing Facebook log in , bind account so on
            </div>
        </div>
        <div class="col-md-12">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{session()->get('success')}}
                </div>
            @endif
            @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{session()->get('error')}}
                </div>
            @endif
        </div>
        <div class="portlet-body" id="checkPushNotification">
            <div class="row">
                <div class="col-md-12">
                    <form action="{{route('backend.reset-user-post')}}" method="POST">
                        <label>Enter Email</label>
                        <br>
                        <input type="text" class="form-control" name="email">
                        <br>
                        <button class="btn btn-success">
                            <i id="fa-spin-check" style="display: none;" class="fa fa-refresh fa-spin"></i>
                            Click
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
