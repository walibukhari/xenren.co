@extends('backend.layout')

@section('title')
    {{trans('permissions.article')}}
@endsection

@section('description')
    {{trans('permissions.crudL')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="{{route('backend.articleList')}}">{{trans('sideMenu.article')}}</a>
        </li>
        <li>
            /
        </li>
        <li>
            <a href="{{ route('backend.articleCreate') }}">{{trans('sideMenu.create_article')}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <!-- include libraries(jQuery, bootstrap) -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">
    <style>
        .page-container-bg-solid .page-bar, .page-content-white .page-bar {
            background-color: #fff;
            position: relative;
            padding: 0 20px;
            margin: -25px -9px 0;
        }
    </style>
@endsection

@section('footer')
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote();
        });
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('permissions.create_article')}}</span>
            </div>
            <div class="actions">
                <a href="{{ route('backend.articleCreate') }}" style="text-transform:lowercase;" class="btn btn-circle red-sunglo btn-sm"><i
                        class="fa fa-plus"></i> {{trans('permissions.create_article')}}</a>
            </div>
        </div>
        <div class="portlet-body">
            @if(session()->has('success_message'))
                <div class="alert alert-success">
                    {{ session()->get('success_message') }}
                </div>
            @endif
            <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th style="width: 10%">#</th>
                            <th style="width: 70%">title</th>
                            <th style="width: 20%">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($article as $data)
                            <tr>
                                <td>{{$data->id}}</td>
                                <td>{{$data->title}}</td>
                                <td>
                                    <a href="{{route('backend.editArticle',[$data->id])}}" class="btn btn-success">Edit</a> /
                                    <a href="{{route('backend.deleteArticle',[$data->id])}}" class="btn btn-danger">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection
