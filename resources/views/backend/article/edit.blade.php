@extends('backend.layout')

@section('title')
    {{trans('permissions.article')}}
@endsection

@section('description')

@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="{{route('backend.articleList')}}">{{trans('sideMenu.article')}}</a>
        </li>
        <li>
            /
        </li>
        <li>
            <a href="{{ route('backend.articleCreate') }}">{{trans('sideMenu.create_article')}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <link href='https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/css/froala_editor.pkgd.min.css' rel='stylesheet' type='text/css' />

    <style>
        .page-container-bg-solid .page-bar, .page-content-white .page-bar {
            background-color: #fff;
            position: relative;
            padding: 0 20px;
            margin: -25px -9px 0;
        }
    </style>
@endsection

@section('footer')
    <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/js/froala_editor.pkgd.min.js'></script>
    <script>
        $(document).ready(function() {
            new FroalaEditor('#flroa',{
                events: {
                    "image.beforeUpload": function(files) {
                        var editor = this;
                        if (files.length) {
                            // Create a File Reader.
                            var reader = new FileReader();
                            // Set the reader to insert images when they are loaded.
                            reader.onload = function(e) {
                                var result = e.target.result;
                                editor.image.insert(result, null, null, editor.image.get());
                            };
                            // Read image as base64.
                            reader.readAsDataURL(files[0]);
                        }
                        editor.popups.hideAll();
                        // Stop default upload chain.
                        return false;
                    }
                }
            })
        });
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('permissions.create_article')}}</span>
            </div>
        </div>
        <div class="portlet-body">
            @if(session()->has('success_message'))
                <div class="alert alert-success">
                    {{ session()->get('success_message') }}
                </div>
            @endif
            <form method="post" action="{{route('backend.updateArticle',[$article->id])}}">
                @csrf
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Title :</label>
                            <input type="text" class="form-control" id="title" onkeyup="this.value = this.value.replace(/[^a-z0-9_-]/g, '')" required value="{{$article->title}}" name="title"  />
                        </div>
                    </div>
                </div>
                <br>
                <label>Description :</label>
                <textarea id="flroa" required name="article_description">{!! $article->article_description !!}</textarea>
                <br>
                <div class="row">
                    <div class="col-md-2">
                        <input type="submit" name="submit" class="btn btn-success form-control" />
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('modal')

@endsection
