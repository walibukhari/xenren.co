@extends('backend.layout')

@section('title')
    {{trans('sideMenu.sharedofficeprofileupdate')}}
@endsection
@section('description')

@endsection
<head xmlns:v-on="http://www.w3.org/1999/xhtml">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="{{asset('js/vue.min.js')}}"></script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">
    <link href="{{asset('custom/css/backend/workingsheet.css')}}" type="text/css" rel="stylesheet">
    <style>
        [v-cloak] {
            display: none;
        }
        .filter-option > .pull-left{
            font-size: 12px !important;
        }
        @media (max-width: 480px) {

            .page-content-wrapper .page-content {
                display: flex;
                justify-content: center;
                height: fit-content;
            }
            .customSetStyle{
                text-align: center;
                margin-top: 0px;
                font-size: 21px;
                font-weight: 400;
                color: #25ce0f;
            }
        }

        .loader {
            font-size: 10px;
            margin: 50px auto;
            text-indent: -9999em;
            width: 11em;
            height: 11em;
            border-radius: 50%;
            background: #ffffff;
            background: -moz-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            background: -webkit-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            background: -o-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            background: -ms-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            background: linear-gradient(to right, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            position: relative;
            -webkit-animation: load3 1.4s infinite linear;
            animation: load3 1.4s infinite linear;
            -webkit-transform: translateZ(0);
            -ms-transform: translateZ(0);
            transform: translateZ(0);
        }
        .loader:before {
            width: 50%;
            height: 50%;
            background: #555;
            border-radius: 100% 0 0 0;
            position: absolute;
            top: 0;
            left: 0;
            content: '';
        }
        .loader:after {
            background: #fff;
            width: 75%;
            height: 75%;
            border-radius: 50%;
            content: '';
            margin: auto;
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }
        @-webkit-keyframes load3 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @keyframes load3 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
    </style>
    <style>
        .lds-ring {
            display: inline-block;
            position: relative;
            width: 20px;
            height: 20px;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 20px;
            height: 20px;
            margin: 1px;
            border: 2px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
</head>
@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">{{trans('common.profile')}}</a>
            <i class="fa fa-circle"></i>
        </li>
    </ul>
@endsection
@section('footer')
    <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <script>
        $('.scroll-div').slimScroll({});
    </script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/vue.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vue-resource.min.js')}}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.15/lodash.core.min.js"></script>
    <script>
        let vu = new Vue({
            el: '#profile',
            data: {
                confirm_password:'',
                password :'',
                loaderLoad:true,
                LoadSpinner:false
            },
            methods: {
                updateProfile(){
                    this.LoadSpinner = true;
                    let url = '{{route('backend.sharedofficeProfileUpdate')}}';
                    console.log(this.confirm_password);
                    console.log(this.password);
                    if(!this.password){
                        this.LoadSpinner = false;
                        toastr.error('Please Enter Password');
                    } else if(!this.confirm_password) {
                        this.LoadSpinner = false;
                        toastr.error('Please Enter Confirm Password');
                    } else if(this.confirm_password !== this.password) {
                        this.LoadSpinner = false;
                        toastr.error('Confirm Password and Password not match');
                    } else {
                        let form = new FormData();
                        form.append('_token','{{csrf_token()}}');
                        form.append('password',this.password);
                        form.append('confirm_password',this.confirm_password);
                        this.$http.post(url,form).then((response) => {
                            console.log('response');
                            console.log(response.data.status);
                            if(response.data.status === 'true') {
                                this.LoadSpinner = false;
                                toastr.success(response.data.message);
                            }
                            this.confirm_password = '';
                            this.password = '';
                        }, function (error) {
                            this.LoadSpinner = false;
                            console.log('error');
                            console.log(error);
                        })
                    }
                }
            },
            mounted() {
                console.log("vue initialized");
                console.log("Profile page");
                let self = this;
                setTimeout(() => {
                    self.loaderLoad = false
                },800)
            }
        });
    </script>
@endsection
@section('content')
    <div class="worksheet" id="profile" v-cloak>
        <h1 class="customSetStyle">{{trans('common.change_pass')}}</h1>
        <br>
        <div v-if="loaderLoad">
            <div class="loader">{{trans('common.load')}}...</div>
        </div>
        <form v-if="!loaderLoad">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>{{trans('common.pass')}} : </label>
                        <input type="password" name="password" v-model="password" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>{{trans('common.confirm_pass')}} : </label>
                        <input type="password" name="confirm_password" v-model="confirm_password" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-success" style="display:flex;background:#25ce0f;justify-content: center;align-items: center;width: 100px" @click.prevent="updateProfile()">
                        <span class="idSpanSaveProfileManager" v-if="!LoadSpinner">{{trans('common.save')}}</span>
                        <div v-if="LoadSpinner" class="lds-ring">
                            <div></div><div></div><div></div><div></div>
                        </div>
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection


