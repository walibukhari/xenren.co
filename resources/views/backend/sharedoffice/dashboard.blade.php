@extends('backend.layout')

@section('title')
    {{trans('sharedoffice.sharedoffice')}}
@endsection

@section('description')
    {{trans('sharedoffice.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li class="color-green font-set">
            <a href="javascript:;">{{trans('common.home')}}</a> <span class="slash">/</span>
        </li>
        <li class="color-green font-set">
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
        </li>

        <li class="color-gray font-set">
            <span class="slash-gray">/</span> <a href="{{ route('backend.sharedoffice') }}">{{$office_name}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <style>

        .customBtnAddUserSet {
            display: flex;
            height: 60px;
            justify-content: flex-start;
            padding-bottom: 25px;
            padding-right:25px;
        }

        [v-cloak] {
            display: none !important;
        }
        .daterangepicker .ranges {
            float: right !important;
        }
        .greycolor{
            color:grey !important ;
        }
        .progress-bar{
            background-color: #25ce0f !important;
        }
        .box-fourth-modal-withdraw{
            position: relative;
            left: 18px;
            background-color: #25ce0f;
            padding: 13px;
            border-radius: 4px;
            text-align: center;
            font-weight: 600;
            font-family: Montserrat, Helvetica, sans-serif;
            font-size: 16px;
            color: #fff;
            width: 120px;
            box-shadow: 0px 2px 9px 1px #ebecec;
            cursor:pointer;
        }
        #modalHeader{
            background-color: #2b3643 !important;
            border-top-right-radius: 15px;
            border-top-left-radius: 15px;
        }
        #modalContent{
            border-top-right-radius: 21px;
            border-top-left-radius: 21px;
            border-bottom-left-radius: 21px;
            border-bottom-right-radius: 21px;
        }
        #modalTitle{
            color:#fff;
            font-weight: bold;
            text-align: center;
        }
        #buttonClose{
            background: #fff;
            border: 0px;
            border-radius: 30px;
            position: absolute;
            right: 15px;
            padding-top: 0px;
        }
        #modalBody{
            position: relative;
            padding-bottom: 15px;
            padding-left: 35px;
            padding-right: 35px;
            padding-top: 35px;
        }
        #modalDailog{
            width: 60%;
        }
        #modalFooter{
            border-top:0px !important;
            text-align: center;
            padding-bottom: 50px;
        }
        .btnWithdrawal{
            background-color: #25ce0f;
            border-radius: 10px;
            width: 60%;
            padding: 8px;
            font-weight: bold;
            font-size: 19px;
            border-color: #25ce0f;
            border: 0px;
        }
        @media(max-width:767px) {
            #modalDailog{
                width: 95%;
            }
        }
        @media (max-width:480px) {
            .portlet.light.bordered {
                border-radius: 0px !important;
                width: 100% !important;
                height: fit-content !important;
            }
            .customBtnAddUserSet {
                display: flex;
                justify-content: flex-start;
                padding-bottom: 25px;
                padding-right:25px;
            }
            .customBtnAddUserDisplayD{
                display: flex;
                align-items: center;
            }
        }

        .loader {
            font-size: 10px;
            margin: 50px auto;
            text-indent: -9999em;
            width: 11em;
            height: 11em;
            border-radius: 50%;
            background: #ffffff;
            background: -moz-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            background: -webkit-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            background: -o-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            background: -ms-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            background: linear-gradient(to right, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            position: relative;
            -webkit-animation: load3 1.4s infinite linear;
            animation: load3 1.4s infinite linear;
            -webkit-transform: translateZ(0);
            -ms-transform: translateZ(0);
            transform: translateZ(0);
        }
        .loader:before {
            width: 50%;
            height: 50%;
            background: #555;
            border-radius: 100% 0 0 0;
            position: absolute;
            top: 0;
            left: 0;
            content: '';
        }
        .loader:after {
            background: #fff;
            width: 75%;
            height: 75%;
            border-radius: 50%;
            content: '';
            margin: auto;
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }
        @-webkit-keyframes load3 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @keyframes load3 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        .booking{
            background-color: #fcb817;
        }
    </style>
    <link href="{{asset('css/OwnerDashboard.css')}}" rel="stylesheet"  type="text/css">
{{--    <link href="{{asset('css/dateTimePicker.css')}}" rel="stylesheet" type="text/css">--}}
{{--    <link href="{{asset('css/income_admin.css')}}" rel="stylesheet" type="text/css">--}}
{{--    <link href="{{asset('css/newDesign.css')}}" rel="stylesheet" type="text/css">--}}
{{--    <link href="{{asset('css/backendproducts.css')}}" rel="stylesheet" type="text/css">--}}
    @if(Auth::guard('staff')->user()->staff_type == \App\Models\Staff::STAFF_TYPE_STAFF)
        <link href="{{asset('custom/css/backend/staffHeader.css')}}" rel="stylesheet" type="text/css">
    @endif
    <style>
        .setSelectBoxStyle{
            border-top: 0px;
            border-right: 0px;
            border-left: 0px;
            border-radius: 0px;
            border-bottom: 2px solid #a8a8a8;
        }
    </style>
@endsection

@section('footer')
    {{--    <script src="{{asset('custom/js/bs-modal-bootstrap.js')}}"></script>--}}
    <script src="{{asset('js/vue.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vue-resource.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('custom/js/bs-modal-bootstrap.js')}}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <script>
        $('.scroll-div').slimScroll({});
    </script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/vue.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vue-resource.min.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $(".form_datetime2").datetimepicker({
                format: "MM dd",
                // altFormat: 'dd-mm-yy',
                format:'dd-mm-yyyy',
                altField: "#hiddenField",
                autoClose: true,
                pickTime: false,
                minView: 2
            }).on('changeDate', function (e) {
                $('.form_datetime2').datetimepicker('hide');
                var today = formatDateForVue(new Date(e.date));
                $('#datetime').val(today);
                vu.getDetail();
            });
            $('.start_time_dtp').datetimepicker({
                format: "yyyy-M-dd hh:mm",
                autoClose: true,
            }).on('changeDate', function (e) {
                $('.start_time_dtp').datetimepicker('hide');
                vu.changeStartTime(formatDateForVueWithSec(e.date));
            });

            $('.end_time_dtp').datetimepicker({
                format: "yyyy-M-dd hh:mm",
                autoClose: true,
                daysOfWeekDisabled: [0, 6]
            }).on('changeDate', function (e) {
                $('.end_time_dtp').datetimepicker('hide');
                vu.changeEndTime(formatDateForVueWithSec(e.date));

            });

            function curruntDate(date) {
                $('.form_datetime2').attr('placeholder', date);
            }

            function timeNow() {
                var d = new Date(),
                    h = (d.getHours() < 10 ? '0' : '') + d.getHours(),
                    m = (d.getMinutes() < 10 ? '0' : '') + d.getMinutes();
                return h + ':' + m;
            }

            function formatDateForVueWithSec(date) {
                var dd = date.getDate();
                var mm = date.getMonth() + 1; //January is 0!
                var yyyy = date.getFullYear();
                var hour = date.getHours();
                var minute = date.getMinutes();
                var sec = date.getSeconds();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (mm < 10) {
                    mm = '0' + mm;
                }
                return today = dd + '-' + mm + '-' + yyyy + ' ' + hour + ":" + minute + ":" + sec;
            }

            function formatDateForVue(date) {
                var dd = date.getDate();
                var mm = date.getMonth() + 1; //January is 0!
                var yyyy = date.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (mm < 10) {
                    mm = '0' + mm;
                }
                return today = dd + '-' + mm + '-' + yyyy;
            }

            function formatDate(date) {
                var monthNames = [
                    "January", "February", "March",
                    "April", "May", "June", "July",
                    "August", "September", "October",
                    "November", "December"
                ];

                var day = date.getDate();
                var month = monthNames[date.getMonth()];
                curruntDate(month + ' ' + day);
                var monthIndex = date.getMonth();
                var year = date.getFullYear();
                var time = timeNow();
                return day + '-' + monthNames[monthIndex] + '-' + year + ' ' + time;
            }

            var dateTime = formatDate(new Date());
            $('#simple').val(dateTime);
            var today = formatDateForVue(new Date());
            $('#datetime').val(today);

        });
    </script>
    <script>
        $(function () {
            200
            $('.aLink').click(function () {
                var text = $(this).text();
                $('#dropbtn').html(text + '<i class="fa fa-caret-down"></i>');
            });
        })
    </script>
    <!-- office finance -->
    <script type="text/javascript">
            $(function() {
                $('.reportrange').daterangepicker({
                    ranges: {
                        'Daily Sales': [moment(), moment()],
                        'This Week': [moment().startOf('week'), moment().endOf('week')],
                        'Last 90 Days': [moment().subtract(90, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                        'This Year': [moment().startOf('year'), moment().endOf('year')],
                    },
                    "alwaysShowCalendars": true,
                    "maxDate":new Date(),
                }, function(start, end, label) {
                    console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');

                    let startDate = start.format('MM/DD/YYYY');
                    let endDate = end.format('MM/DD/YYYY');
                    console.log(startDate,endDate);
                    $('#startedDate').val(startDate);
                    $('#endedDate').val(endDate);
                    vu.getFinanceDetail(startDate,endDate);

                });


                function formatDateForVue(date) {
                    var dd = date.getDate();
                    var mm = date.getMonth() + 1; //January is 0!
                    var yyyy = date.getFullYear();
                    if (dd < 10) {
                        dd = '0' + dd;
                    }
                    if (mm < 10) {
                        mm = '0' + mm;
                    }
                    return  dd + '-' + mm + '-' + yyyy;
                }

            });
        </script>
    <!-- office finance ends -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.15/lodash.core.min.js"></script>
    <script>
        let vu = new Vue({
            el: '#vueJs',
            data: {
                activeColor:'#EBEBEB',
                productsOrg: [],
                products: [],
                financeProducts: [],
                sharedOfficeRating:[],
                sharedOfficeRatingAverage:0,
                sharedOfficeRatingPercentage:50,
                showLoader: true,
                addUserPanel: false,
                // totalPersons: 10,
                totalAmount: 0,
                financeTotalAmount: 0,
                // personsUsage: [],
                startTimeFilter: 0,
                endTimeFilter: 0,
                personsAdded: [],
                personsAddedObj: [],
                hideShowCheck:false,
                sharedOfficeUsers:[],
                userDetails:'',
                userSkills:[],
                officelS:'',
                clientVerifiedS:'',
                normalS:'',
                messagesSend:'',
                hideMessage:false,
                getOfficeMessages:[],
                getStaffM:[],
                staffMessages:[],
                filename:'',
                hideshowImage:false,
                searchBox:'',
                OriginalData:[],
                checkedSharedOffice:'',
                payment_request:'',
                bank_name:'',
                iban_number:'',
                card_number:'',
                branch_name:'',
                branch_code:'',
                branch_address:'',
                currency:'',
                buttonActive:false,
                bookings: [],
                availableSeats: [],
                selectedAvailableSeat: 0,
                selectedBookingId: 0
            },
            methods: {
                getDetail() {
                    this.showLoader = true;
                    let self = this;
                    setTimeout(function () {
                        let date = $('#datetime').val();
                        self.$http.get('/admin/sharedOfficeProducts/{{$id}}?date=' + date).then(function (response) {
                            self.showLoader = false;
                            self.products = response.body.data;
                            self.productsOrg = response.body.data;
                        }, function (error) {
                            self.showLoader = false;
                        });
                    }, 100)
                },
                getTodaySharedOfficeRating() {
                    let self = this;
                    setTimeout(function () {
                        let date = new Date().toDateString();
                        self.$http.get('/admin/getTodaySharedOfficeRating/{{$id}}?date=' + date).then(function (response) {
                            if(response.body.data.length > 0) {
                                self.sharedOfficeRating = response.body.data;
                                self.sharedOfficeRatingAverage = response.body.average;
                                self.sharedOfficeRatingPercentage = response.body.ratingPercent[0];
                            }
                        }, function (error) {
                            //
                        });
                    }, 100)
                },
                saveUser(cb) {
                    let data = new FormData();
                    data.append('username', $('#username').val());
                    data.append('start_time', $('#start_time').val());
                    data.append('end_time', $('#end_time').val());
                    data.append('seat_no', $('#seat_no').val());
                    data.append('office_id', '{{$id}}');
                    let self = this;
                    self.$http.post('/admin/sharedOfficeProductsCheckUsers', data).then((response) => {
                        if (response.body.status == 'failure') {
                            toastr.error(response.body.message);
                            cb(true)
                        } else {
                            toastr.success(response.body.message);
                            cb(null, true)
                        }
                    }, (response) => {
                        cb(true)
                    })

                },
                addUser(cb) {
                    let self = this;
                    var validated = [];
                    validated = this.personsAddedObj.filter(function (data) {
                        if (data['number'] == self.personsAdded['number'] || data['name'] == self.personsAdded['name']) {
                            return data;
                        }
                    });
                    if (validated.length > 0) {
                        toastr.error('Seat/User already selected');
                        cb(true);
                    } else {
                        let headers = new Headers();
                        headers.append('Content-Type', 'application/json');
                        self.$http.post('/admin/sharedOfficeProductsCheckUsers', {
                            username: self.personsAdded['name'],
                            start_time: self.personsAdded['start_time'],
                            end_time: self.personsAdded['end_time'],
                            seat_no: self.personsAdded['number'],
                            cost: 0,
                            office_id: '{{$id}}'
                        }, {
                            headers: headers
                        }).then((response) => {
                            if (response.body.status == 'failure') {
                                toastr.error(response.body.message);
                                cb(true)
                            } else {
                                self.personsAddedObj.push((self.personsAdded));


                                self.personsAdded = [];
                                cb(null, true)
                            }
                        }, (response) => {
                            cb(true)
                        });
                    }
                },
                getClass(data) {
                    console.log('time slots');
                    console.log(data);
                    if (data === 0){
                        return 'availables';
                    } else if (data === 1){
                        return 'inuse';
                    } else if (data === 2) {
                        return 'booking';
                    }
                },
                formatDate(date) {
                    var format = 'Do MMMM, h:mm a';
                    return moment(date).format(format)
                },
                addUserPanelFnc(showPanel) {
                    let self = this;
                    if (showPanel == false) {
                        // save user
                        this.addUser(function (err, success) {
                            if (!err) {
                                self.addUserPanel = showPanel;
                            }
                        });
                    } else {
                        this.addUserPanel = showPanel;
                    }
                },
                showPanel() {
                    if (this.addUserPanel) {
                        return 'display: block';
                    } else {
                        return 'display: none';
                    }
                },
                changeStart(event) {
                    this.startTimeFilter = event.target.value;
                },
                changeEnd(event) {
                    this.endTimeFilter = event.target.value;
                },
                changeStartTime(val) {
                    this.personsAdded.start_time = val;
                },
                changeEndTime(val) {
                    this.personsAdded.end_time = val;
                },
                save() {
                    let headers = new Headers();
                    headers.append('Content-Type', 'application/json');
                    let formData = new FormData();
                    let obj = {};
                    let index = 0
                    this.personsAddedObj.map(function(d){
                        let a =  {
                            end_time: d.end_time,
                            month_price: d.month_price,
                            name: d.name,
                            number: d.number,
                            product_type_name: d.product_type_name,
                            start_time: d.start_time,
                            office_id: '{{$id}}'
                        };
                        obj[index] = (a);
                        index++;
                    });

                    this.$http.post('/admin/sharedOfficeProductsAddUsers', JSON.stringify(obj),headers).then((response) => {
                        if(response.body.status == 'error') {
                            toastr.error(response.body.message);
                        } else {
                            toastr.success(response.body.message);
                            this.getDetail();
                            $('#myModal').modal('hide');
                        }
                    }, (response) => {
                        toastr.error(response.body.message);
                    });
                },
                convertToJSON(array) {
                   let obj = array.map(function(d){
                       return {
                           end_time: d.end_time,
                           month_price: d.month_price,
                           name: d.name,
                           number: d.number,
                           product_type_name: d.product_type_name,
                           start_time: d.start_time,
                       };
                   });
                    return obj;
                },
                applyFilter() {
                    let dateFilter = null;
                    let periorityFilter = null;
                    let self = this;
                    if (this.startTimeFilter) {
                        this.products.office_products = this.products.office_products.filter(function (data) {
                            if (data.startDateTime) {
                                let t1 = (moment(data.startDateTime, "HH:mm:ss"));
                                let st = (moment(self.startTimeFilter, "HH:mm:ss"));
                                let et = (moment(self.endTimeFilter, "HH:mm:ss"));
                                if ((t1.isBefore(et)) && t1.isAfter(st)) {
                                    return data;
                                }
                            }
                        });
                    }
                },
                initializeSelect2() {
                    let self = this;
                    let select = $('#seats');
                    select
                        .select2({
                            placeholder: 'Enter Desk/Room and Seat No',
                            theme: 'bootstrap',
                            width: '100%',
                            allowClear: true,
                            data: this.select2data
                        })
                        .on('change', function () {
                            let id = select.val();
                            let rec = self.productsOrg.office_products.filter(function (data) {
                                if (parseInt(data.id) === parseInt(id)) {
                                    return data;
                                }
                            });
                            self.personsAdded.product_type_name = rec[0].product_type_name;
                            self.personsAdded.month_price = rec[0].month_price;
                            self.personsAdded.number = rec[0].number;

                        });
                    select.val(this.value).trigger('change')
                },
                openSection(id)
                {
                    let self = this;
                    var link = '/admin/sharedOffice/user/details/'+id;
                    self.$http.get(link).then(function(response){
                        self.userDetails = response.body;
                        self.userSkills = response.body.skills;
                        var officalSkills = [];
                        var clientVerified = [];
                        var normalSkills = [];
                        self.userSkills.map(function(q){
                            if(q.experience >= 1)
                            {
                                officalSkills.push(q.skill.name_cn);
                            } else if(q.is_client_verified === 1)
                            {
                                clientVerified.push(q.skill.name_cn);
                            } else if(q && q.skill_id) {
                                normalSkills.push(q.skill.name_cn);
                            }
                        });
                        self.officelS = officalSkills;
                        self.clientVerifiedS = clientVerified;
                        self.normalS = normalSkills;
                    }, function(error){
                    });

                    $('.set-custom-width-col').addClass('removeCustomWidth');
                    this.hideShowCheck = true;
                },
                getSharedofficeUsers()
                {
                    let self = this;
                    var office_id = '{{$id}}';
                    var link = "/admin/sharedOffice/users/"+office_id;
                    self.$http.get(link).then(function(response){
                        self.sharedOfficeUsers = response.body.data;
                    }, function(error){
                    });
                },
                getUserImage(users)
                {
                    return users.img_avatar;
                },
                lastLoginTime(users)
                {
                    var date = moment(users.last_login_at).fromNow("m");
                    var d = date.replace('minutes','min');
                    if(d === 'an hour') {
                        d = '1 hour';
                    }
                    if(d === 'a few seconds')
                    {
                        d = '1 sec';
                    }
                    if(d === 'a minute')
                    {
                        d = '1 minute';
                    }
                    if(d === 'a day')
                    {
                        d = '1 day';
                    }
                    if(d === 'a month')
                    {
                        d = '1 month';
                    }
                    return d;
                },
                getSharedofficeChat()
                {
                    let self = this;
                    var office_id = '{{$id}}';
                    var link = '/admin/sharedOffice/room/chat/'+office_id;
                    self.$http.get(link).then(function(response){
                        this.getOfficeMessages = response.body.data;
                        this.OriginalData = response.body.data;
                    }, function(error){
                    });
                },
                getUserMessageImages(messages)
                {
                    var users = messages.users;
                    var staff = messages.staff_data;
                    var user_image = '';
                    users.map(function(data){
                        user_image = data.img_avatar;
                    });
                    staff.map(function(data){
                        user_image = '/'+data.img_avatar;
                    });
                    return user_image;
                },
                getUserData(messages)
                {
                    var users = messages.users;
                    var staff = messages.staff_data;
                    var user_data = '';
                    users.map(function(data){
                        user_data = data.name;
                    });
                    staff.map(function(data){
                        user_data = data.name;
                    });
                    return user_data;
                },
                getStaffImage(staff)
                {
                    var staff_image = '';
                    staff.map(function(data){
                        staff_image = data.img_avatar;
                    });
                    return staff_image;
                },
                getStaffData(staff)
                {
                    var staff_name = '';
                    staff.map(function(data){
                        staff_name = data.name;
                    });
                    return staff_name;
                },
                hitEnter()
                {
                    let self = this;
                    var sender_staff_id = '{!! \Auth::guard('staff')->user()->id !!}';
                    var message = this.messagesSend;
                    var office_id = '{{$id}}';
                    var link = '/admin/sharedoffice/sendMessage';
                    let form = new FormData();
                    form.append('office_id',office_id);
                    form.append('sender_staff_id',sender_staff_id);
                    form.append('message',message);
                    if(message === '')
                    {
                        toastr.error("Please enter your message");
                    } else {
                        self.$http.post(link,form).then(function(response){
                            self.messagesSend = '';
                        }, function(error){
                        });
                        this.getSharedofficeChat();
                        self.hideMessage = true;
                    }
                },
                getChatCheck(messages) {
                    let user_id = '{{\Auth::guard('staff')->user()->id}}';
                    if(parseInt(messages.sender_staff_id) === parseInt(user_id) || parseInt(messages.sender_user_id) === parseInt(user_id))
                    {
                        return true;
                    } else {
                        return false
                    }
                },
                deleteMessage(id)
                {
                    let self = this;
                    var link = "/admin/sharedoffice/delete/message/"+id;
                    self.$http.get(link).then(function(response){
                        if(response.body.success === 'success')
                        {
                            setTimeout(this.getSharedofficeChat, 20);
                            toastr.success('message deleted successfully');
                        }
                    }, function(error){
                    });
                },
                fileReader(e)
                {
                    let self = this;
                    var sender_staff_id = '{!! \Auth::guard('staff')->user()->id !!}';
                    var file = $('#file-input')[0].files[0];
                    var office_id = '{{$id}}';
                    var link = '/admin/sharedoffice/sendMessage';
                    let form = new FormData();
                    form.append('file',file);
                    form.append('office_id',office_id);
                    form.append('sender_staff_id',sender_staff_id);
                    self.$http.post(link,form).then(function(response){
                        $('#loader').show();
                        if(response.body.success === 'success')
                        {
                            $('#loader').hide();
                            toastr.success('attachment upload successfully');
                        }
                        this.getSharedofficeChat();
                    }, function(error){
                        $('#changeColor').addClass('changeColor');
                        toastr.error('attachment failed to upload');
                    });

                    var files = e.target.files;
                        var fileReader = new FileReader();
                        fileReader.onload = (function(e) {
                            var file = e.target;
                            var img = $("<img id='loader' src='/images/giphy.gif' style='display:none;height: 72px;position: absolute; z-index: 999;width: 100px;top: 39%;'><span class=\"pip\">" +
                                "<img class=\"imageThumb\" style=\"width:100px;height:70px;position:relative;top:-7px;object-fit: cover;\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>"+
                                "</span>");
                            self.hideshowImage = true;
                        });
                        fileReader.readAsDataURL(file);
                },
                resetFilter() {
                    if(this.searchBox.length > 0) {
                        this.searchBox = '';
                        this.getOfficeMessages = this.OriginalData;
                        $('#classChange').addClass('fa-search');
                        $('#classChange').removeClass('fa-close');
                    }
                },
                searchResults()
                {
                    let self = this;
                    var text = self.searchBox;
                    var arr = [];
                    if(text.length > 0)
                    {
                        $('#classChange').removeClass('fa-search');
                        $('#classChange').addClass('fa-close');
                        self.OriginalData.map(function(data){
                            if(data.message.includes(text))
                            {
                                arr.push(data);
                            }
                        });
                        self.getOfficeMessages = arr;
                    } else {
                        $('#classChange').addClass('fa-search');
                        $('#classChange').removeClass('fa-close');
                        self.getOfficeMessages = self.OriginalData;
                    }
                },
                blocked(id)
                {
                    let self = this;
                    var action = 1;
                    var office_id = '{{$id}}';
                    var user_id = id;
                    let link = '/admin/sharedoffice/chat/action/'+action;
                    let data = new FormData();
                    data.append('user_id',user_id);
                    data.append('office_id',office_id);
                    self.$http.post(link, data).then(function(response){
                        if(response.body.success === 'success') {
                            toastr.success('User Blocked');
                        }
                        if(response.body.error === 'error')
                        {
                            if(response.body.data === 1)
                            {
                                toastr.error('User already Blocked');
                            }
                        }
                    }, function(){
                    });
                },
                kicked(id){
                    let self = this;
                    var action = 0;
                    var office_id = '{{$id}}';
                    var user_id = id;
                    let data = new FormData();
                    data.append('user_id',user_id);
                    data.append('office_id',office_id);
                    let link = '/admin/sharedoffice/chat/action/'+action;
                    self.$http.post(link,data).then(function(response){
                        if(response.body.success === 'success') {
                            toastr.success('User Kicked');
                        }
                        if(response.body.error === 'error')
                        {
                            if(response.body.data === 0)
                            {
                                toastr.error('User already Kicked');
                            }
                        }
                    }, function(){
                    });
                },
                closeRightSec()
                {
                    $('.set-custom-width-col').removeClass('removeCustomWidth');
                    $('.set-custom-width-col').addClass('custom-width-slide');
                    this.hideShowCheck = false;
                },
                getFinanceDetail(startDate,endDate) {
                    startDate = new Date(startDate).toDateString();
                    endDate = new Date(endDate).toDateString();
                    this.showLoader = true;
                    let self = this;
                    setTimeout(function () {
                        self.$http.get('/admin/sharedOfficeFinances/{{$id}}?startdate=' + startDate+'&enddate='+endDate).then(function (response) {
                            self.showLoader = false;
                            self.financeProducts = response.body.data;
                            console.log('response.body.data');
                            console.log(response.body.data);
                            console.log('self.financeProducts');
                            self.financeTotalAmount = response.body.totalAmount - response.body.totalDeduct;
                            console.log('self.financeTotalAmount');
                            console.log(self.financeTotalAmount);
                        }, function (error) {
                            self.showLoader = false;
                        });
                    }, 100)
                },
                openWithDrawalModal: function(){
                    let checkData = this.checkStaffUserCurrencyExist();
                },
                Withdrawal: function(id){
                    var link = '/admin/withdraw/request';
                    var formRequest = new FormData();
                    formRequest.append('csrf_token','{{ csrf_token() }}');
                    formRequest.append('payment_request',this.payment_request);
                    formRequest.append('bank_name',this.bank_name);
                    formRequest.append('iban_number',this.iban_number);
                    formRequest.append('card_number',this.card_number);
                    formRequest.append('branch_name',this.branch_name);
                    formRequest.append('branch_code',this.branch_code);
                    formRequest.append('branch_address',this.branch_address);
                    formRequest.append('staff_id', '{{ $staff_id }}');
                    formRequest.append('office_id', '{{ $id }}');
                    if(this.bank_name === '') {
                        toastr.error('please enter bank name');
                    } else if(this.iban_number === '') {
                        toastr.error('please enter iban number');
                    } else if(this.card_number === '') {
                        toastr.error('please enter card number');
                    } else if(this.branch_name === '') {
                        toastr.error('please enter branch name');
                    } else if(this.branch_code === '') {
                        toastr.error('please enter branch code');
                    } else if(this.branch_address === '') {
                        toastr.error('please enter branch address');
                    } else if(this.payment_request === '') {
                        toastr.error('please enter request payment');
                    } else {
                        this.$http.post(link, formRequest).then(function (response) {
                            console.log('request withdraw response');
                            console.log(response);
                            if (response.body.success === true) {
                                toastr.success(response.body.message);
                                setTimeout(function () {
                                    window.location.reload();
                                }, 600);
                            }
                            if (response.body.success === false) {
                                toastr.error(response.body.message);
                            }
                        }, function (error) {
                            console.log('request withdraw error');
                            console.log(error);
                        })
                    }
                },
                checkStaffUserCurrencyExist: function () {
                    let checkdata = '{{$staffCheckCurrencyExist}}';
                    alert(checkdata);
                },
                submitUserCurrency: function(){
                    console.log(this.currency);
                    let form = new FormData();
                    form.append('_token','{{csrf_token()}}');
                    form.append('currency_id',this.currency);
                    let url = '{{route('backend.saveStaffCurrency')}}';
                    this.$http.post(url, form).then((response) => {
                        console.log('response');
                        console.log(response);
                        console.log(response.data.status);
                        if(response.data.status) {
                            toastr.success(response.data.message);
                            setTimeout(function () {
                                window.location.reload();
                            },1000);
                        }
                    }).catch((error) => {
                        console.log('error');
                        console.log(error);
                    })
                },
                openTab: function (val) {
                    if (val == 's') {
                        this.buttonActive = true;
                    } else {
                        this.buttonActive = false;
                    }
                },
                getBookings: function(){
                    this.showLoader = true;
                    let self = this;
                    setTimeout(function () {
                        let date = $('#datetime').val();
                        self.$http.get('/admin/sharedOfficeBookings/{{$id}}?date=' + date).then(function (response) {
                            self.showLoader = false;
                            self.bookings = response.body.data;
                            console.log('bookings');
                            console.log(self.bookings);

                        }, function (error) {
                            self.showLoader = false;
                        });
                    }, 100)
                },
                cancelBooking: function(id){
                    console.log(id);
                    let self = this;
                    var formRequest = new FormData();
                    formRequest.append('csrf_token','{{ csrf_token() }}');
                    formRequest.append('booking_id',id);
                    this.$http.post('/admin/sharedOfficeBookingsCancel', formRequest).then(function (response) {
                        console.log('request withdraw response');
                        console.log(response);
                        self.getBookings();
                        if (response.body.success === true) {
                            toastr.success(response.body.message);
                            self.getBookings();
                        }
                        if (response.body.success === false) {
                            toastr.error(response.body.message);
                        }
                    }, function (error) {
                        console.log('request withdraw error');
                        console.log(error);
                    })
                },
                openConfirmBooking: function(id){
                    console.log('open modal');
                    console.log(id);
                    let bid = id;
                    this.showLoader = true;
                    let self = this;
                    this.selectedBookingId = bid;
                    setTimeout(function () {
                        self.$http.get('/admin/sharedOfficeAvailaleSeats/'+bid).then(function (response) {
                            self.showLoader = false;
                            console.log('bookings');
                            console.log(response);
                            self.availableSeats = response.body.data;
                            console.log(self.availableSeats);
                        }, function (error) {
                            self.showLoader = false;
                        });
                    }, 100)
                },
                bookSeat: function(){
                    console.log(this.selectedAvailableSeat);
                    let self = this;
                    var formRequest = new FormData();
                    formRequest.append('csrf_token','{{ csrf_token() }}');
                    formRequest.append('seat_id',this.selectedAvailableSeat);
                    formRequest.append('booking_id',this.selectedBookingId);

                    this.$http.post('/admin/bookSeat', formRequest).then(function (response) {
                        console.log('request withdraw response');
                        console.log(response);
                        self.getBookings();
                        $('#confirmBooking').modal('hide')
                        if (response.body.success === true) {
                            toastr.success(response.body.message);
                            self.getBookings();
                        }
                        if (response.body.success === false) {
                            toastr.error(response.body.message);
                        }
                    }, function (error) {
                        console.log('request withdraw error');
                        console.log(error);
                    })
                }
            },
            watch: {
                startTimeFilter: function (val) {
                    this.applyFilter();
                },
                endTimeFilter: function (val) {
                    this.applyFilter();
                },
                personsAddedObj: function (val) {
                    let self = this;
                    this.personsAddedObj.map(function (data) {
                        self.totalAmount = parseInt(self.totalAmount) + parseInt(data.month_price);
                    });
                }
            },
            mounted() {
                this.openTab('s');
                this.getBookings();
                this.getFinanceDetail(new Date(),new Date());
                this.getTodaySharedOfficeRating();
                this.getDetail();
                this.getSharedofficeUsers();
                this.getSharedofficeChat();
                this.initializeSelect2();
            }
        });
    </script>
    <script>
        $(function(){
            $('.selectTime').click(function(){
                var text = $(this).text();
                $('#dropbtnSelect').html(text + '<img class="arrow-down" src="{{asset('images/arrowDowngreen.png')}}">');
            });
        });

        $('.color-dots').click(function(){
            $(this).classList.toggle("showBox");
        });
        function countryCode() {
            var val = $('#city').val();
            $.ajax({
                url: `api/v1/fetchCountryCode?city_id=${val}`,
                dataType: 'json',
                success: function (resp) {
                    $('#callingCode').val('+' + resp.callingCode);
                }
            });
        }
    </script>
@endsection

@section('content')
    <div class="tabs-headers">
        <p><a style="color:#fff;" href="{{route('backend.sharedofficedashboard',[$id])}}">{{trans('sharedoffice.office_managemnet')}}</a></p>
    </div>
    <div id="vueJs">
    <div class="portlet light bordered">
        <input type="hidden" id="datetime">
        <div class="portlet-body">
            <div class="container conatiner-tabs">
                <div class="ulLi">
                <ul class="nav nav-tabs">
                    <li class="active"><a @click="openTab('s')" data-toggle="tab" href="#todaySales">{{trans('common.avail_seats')}}</a></li>
                    <li><a data-toggle="tab" @click="openTab('a')" href="#todaysFeedback">{{trans('common.today_feedback')}}</a></li>
                    <li><a data-toggle="tab" @click="openTab('b')" href="#chat_room">{{trans('common.chat_room')}}</a></li>
                    <li><a data-toggle="tab" @click="openTab('c')" href="#office_finance">{{trans('common.office_fin')}}</a></li>
                    <li><a data-toggle="tab" @click="openTab('d')" href="#booking">{{trans('common.booking')}}</a></li>
                </ul>
                </div>

                <div class="tab-content">
                    <div id="todaySales" class="tab-pane fade in active">
                        <div class="container tab-pane-container">
                            <div class="row m-0">
                                <div class="col-md-2 calender">
                                    <i class="fa fa-calendar form_datetime2" aria-hidden="true"></i><input
                                            class="form_datetime2" placeholder="Jul 12" readonly>
                                </div>
                                <div class="col-md-10 time-table">
                                    <div class="set-scroll">
                                        <ul id="scroll-div">
                                            <li>1<br>AM</li>
                                            <li>2<br>AM</li>
                                            <li>3<br>AM</li>
                                            <li>4<br>AM</li>
                                            <li>5<br>AM</li>
                                            <li>6<br>AM</li>
                                            <li>7<br>AM</li>
                                            <li>8<br>AM</li>
                                            <li>9<br>AM</li>
                                            <li>10<br>AM</li>
                                            <li>11<br>AM</li>
                                            <li>12</li>
                                            <li>1<br>PM</li>
                                            <li>2<br>PM</li>
                                            <li>3<br>PM</li>
                                            <li>4<br>PM</li>
                                            <li>5<br>PM</li>
                                            <li>6<br>PM</li>
                                            <li>7<br>PM</li>
                                            <li>8<br>PM</li>
                                            <li>9<br>PM</li>
                                            <li>10<br>PM</li>
                                            <li>11<br>PM</li>
                                            <li>12</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2 calender">
                                </div>
                                <div class="col-md-10 availability">
                                    <div class="container container-cells">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-4 sm-0">
                                                <ul class="uordered-list">
                                                    <li>
                                                        <label class="label-container">
                                                        </label>{{trans('common.avail')}}
                                                    </li>
                                                    <li>
                                                        <label class="label-container-2">
                                                        </label>{{trans('common.book')}}
                                                    </li>
                                                    <li>
                                                        <label class="label-container-3">
                                                        </label>{{trans('common.in_use')}}
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-5 col-sm-5 sm-0 custom-width-col-5">
                                                <span class="check-seats">{{trans('common.avail_seats')}}: </span>
                                                <div class="select_box">
                                                    <select id="start_time" @change="changeStart($event)"
                                                            style="-webkit-appearance: none;">
                                                        <option value="12:00 AM">12:00 AM</option>
                                                        <option value="01:00 AM">01:00 AM</option>
                                                        <option value="02:00 AM">02:00 AM</option>
                                                        <option value="03:00 AM">03:00 AM</option>
                                                        <option value="04:00 AM">04:00 AM</option>
                                                        <option value="05:00 AM">05:00 AM</option>
                                                        <option value="06:00 AM">06:00 AM</option>
                                                        <option value="04:00 AM">04:00 AM</option>
                                                        <option value="08:00 AM">08:00 AM</option>
                                                        <option value="09:00 AM">09:00 AM</option>
                                                        <option value="10:00 AM">10:00 AM</option>
                                                        <option value="11:00 AM">11:00 AM</option>
                                                        <option value="12:00 PM">12:00 PM</option>
                                                        <option value="01:00 PM">01:00 PM</option>
                                                        <option value="02:00 PM">02:00 PM</option>
                                                        <option value="03:00 PM">03:00 PM</option>
                                                        <option value="04:00 PM">04:00 PM</option>
                                                        <option value="05:00 PM">05:00 PM</option>
                                                        <option value="06:00 PM">06:00 PM</option>
                                                        <option value="04:00 PM">04:00 PM</option>
                                                        <option value="08:00 PM">08:00 PM</option>
                                                        <option value="09:00 PM">09:00 PM</option>
                                                        <option value="10:00 PM">10:00 PM</option>
                                                        <option value="11:00 PM">11:00 PM</option>
                                                    </select>
                                                </div>
                                                <span class="divider">to</span>
                                                <div class="select_box">
                                                    <select id="end_time" @change="changeEnd($event)"
                                                            style="-webkit-appearance: none;">
                                                        <option value="12:00 AM">12:00 AM</option>
                                                        <option value="01:00 AM">01:00 AM</option>
                                                        <option value="02:00 AM">02:00 AM</option>
                                                        <option value="03:00 AM">03:00 AM</option>
                                                        <option value="04:00 AM">04:00 AM</option>
                                                        <option value="05:00 AM">05:00 AM</option>
                                                        <option value="06:00 AM">06:00 AM</option>
                                                        <option value="04:00 AM">04:00 AM</option>
                                                        <option value="08:00 AM">08:00 AM</option>
                                                        <option value="09:00 AM">09:00 AM</option>
                                                        <option value="10:00 AM">10:00 AM</option>
                                                        <option value="11:00 AM">11:00 AM</option>
                                                        <option value="12:00 PM">12:00 PM</option>
                                                        <option value="01:00 PM">01:00 PM</option>
                                                        <option value="02:00 PM">02:00 PM</option>
                                                        <option value="03:00 PM">03:00 PM</option>
                                                        <option value="04:00 PM">04:00 PM</option>
                                                        <option value="05:00 PM">05:00 PM</option>
                                                        <option value="06:00 PM">06:00 PM</option>
                                                        <option value="04:00 PM">04:00 PM</option>
                                                        <option value="08:00 PM">08:00 PM</option>
                                                        <option value="09:00 PM">09:00 PM</option>
                                                        <option value="10:00 PM">10:00 PM</option>
                                                        <option value="11:00 PM">11:00 PM</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-3 custom-alignment sm-0">
                                                <span class="check-seats">{{trans('common.pri_by')}}: </span>
                                                <div class="dropdown">
                                                    <button class="dropbtn" id="dropbtn">{{trans('common.priority')}}
                                                        <i class="fa fa-caret-down"></i>
                                                    </button>
                                                    <div class="dropdown-content">
                                                        <a class="aLink">{{trans('common.in_use')}}</a>
                                                        <a class="aLink">{{trans('common.book')}}</a>
                                                        <a class="aLink">{{trans('common.avail')}}</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="container tab-pane-inner-container">

                            <div v-if="showLoader">
                                <div class="loader">Loading...</div>
                            </div>
                            <div class="row" v-if="!showLoader" style="padding-bottom: 10px" v-cloak
                                 v-for="(data, index) in products.office_products">
                                <div class="col-md-2 rooms">
                                    <div class="container box-container">
                                        @{{ data.product_type_name }} @{{ data.number }}
                                    </div>
                                </div>
                                <div class="col-md-10 cells">
                                    <ul>
                                        <li v-for="(dataTS, indexTS) in data.timeSlots" :class="getClass(dataTS)"></li>
                                    </ul>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div id="todaysFeedback" class="tab-pane fade">

                        <div id="reviews" class="col-md-12 facilities reviews set-col-md-12-scroll-view-area" style="margin-top:-225px">

                            <div class="col-md-12 set-reiviews-section-2-padding">
                                <div class="col-md-5 col-sm-5 col-xs-12">
                                    <div class="customlySetStars">

                                <span class="float-right"><i class="fa fa-star" style="font-size:30px;padding-top:10px"></i></span>
                                <span class="float-right"><i class="fa fa-star" style="font-size:30px;padding-top:10px"></i></span>
                                <span class="float-right"><i class="fa fa-star" style="font-size:30px;padding-top:10px"></i></span>
                                <span class="float-right"><i class="fa fa-star" style="font-size:30px;padding-top:10px"></i></span>
                                <span class="float-right"><i class="fa fa-star" style="font-size:30px;padding-top:10px"></i></span>
                                <h3 style="font-size:15px;color:gray;margin-top:5px;margin-left: 20px;">@{{sharedOfficeRatingAverage}} out of 5.0</h3>
                                    </div>


                                    <div style="margin-top:25px;">
                                <div class="col-md-3"><span class="">5 stars</span></div>
                                <div class="progress" style="width:200px">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="70"
                                    aria-valuemin="0" aria-valuemax="100" :style="{'width':sharedOfficeRatingPercentage.fiveStarPercent+'%'}">
                                    <!-- <span>@{{sharedOfficeRatingPercentage.fiveStarPercent}}% Complete</span> -->
                                    </div>
                                </div>

                                <div class="col-md-3"><span class="" style="color:grey">4 stars</span></div>
                                <div class="progress" style="width:200px">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="70"
                                    aria-valuemin="0" aria-valuemax="100" :style="{'width':sharedOfficeRatingPercentage.fourStarPercent+'%'}">
                                    <!-- <span>@{{sharedOfficeRatingPercentage.fourStarPercent}}% Complete</span> -->
                                    </div>
                                </div>
                                <div class="col-md-3"><span class="" style="color:grey">3 stars</span></div>
                                <div class="progress" style="width:200px">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="70"
                                    aria-valuemin="0" aria-valuemax="100" :style="{'width':sharedOfficeRatingPercentage.threeStarPercent+'%'}">
                                    <!-- <span>@{{sharedOfficeRatingPercentage.threeStarPercent}}% Complete</span> -->
                                    </div>
                                </div>
                                <div class="col-md-3"><span class="" style="color:grey">2 stars</span></div>
                                <div class="progress" style="width:200px">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="70"
                                    aria-valuemin="0" aria-valuemax="100" :style="{'width':sharedOfficeRatingPercentage.twoStarPercent+'%'}">
                                    <!-- <span>@{{sharedOfficeRatingPercentage.twoStarPercent}}% Complete</span> -->
                                    </div>
                                </div>
                                <div class="col-md-3"><span class="" style="color:grey">1 star</span></div>
                                <div class="progress" style="width:200px">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="70"
                                    aria-valuemin="0" aria-valuemax="100" :style="{'width':sharedOfficeRatingPercentage.oneStarPercent+'%'}">
                                    <!-- <span>@{{sharedOfficeRatingPercentage.oneStarPercent}}% Complete</span> -->
                                    </div>
                                </div>
                                </div>

                            </div>
                            </div>
                        </div>

                        <h3 style="margin-top:290px;padding-top:120px;padding-left:80px"><b>{{trans('common.today_review')}}</b></h3>
                        <hr>
                        <div class="col-md-12" style="margin-top:25px">
                            <h1 style="text-align: center;" v-if="sharedOfficeRating && sharedOfficeRating.length == 0">{{trans('common.no_review_yet')}}</h1>
                        <div class="col-md-5" v-for="review in sharedOfficeRating" style="border:2px solid;height: 300px;border-radius: 25px;margin-left: 50px;margin-top:25px">

                        <div class="row" style="margin-top:20px">
                        <div class="col-md-12">
                        <div style="margin-left:25px;display:inline-block">
                            <span class="float-right"><i class="fa fa-star" :class="[review.rate >= 1?'text-warning':'greycolor']" style="padding-top:10px;font-size:20px"></i></span>
                            <span class="float-right"><i class="fa fa-star" :class="[review.rate >= 2?'text-warning':'greycolor']" style="padding-top:10px;font-size:20px"></i></span>
                            <span class="float-right"><i class="fa fa-star" :class="[review.rate >= 3?'text-warning':'greycolor']" style="padding-top:10px;font-size:20px"></i></span>
                            <span class="float-right"><i class="fa fa-star" :class="[review.rate >= 4?'text-warning':'greycolor']" style="padding-top:10px;font-size:20px"></i></span>
                            <span class="float-right"><i class="fa fa-star" :class="[review.rate >= 5?'text-warning':'greycolor']" style="padding-top:10px;font-size:20px"></i></span>
                        </div>
                        <div style="display:inline-block;margin-left:5px"><span style="color:grey">@{{review.rate}} out of 5 stars</span></div>
                        </div>
                        </div>
                        <div style="margin-left:25px;display:inline-block">
                            <span style="color:grey;font-size:20px;font-weight:bold">@{{review.rater.name}}</span>
                        </div>
                        <div style="display:inline-block">
                            <span style="color:grey;">-@{{review.updated_at}}</span>
                        </div>
                        <div style="margin-left:25px;display:inline-block">
                             <p style="color:grey">@{{review.comment}}</p>
                        </div>
                        <span style="color:grey;font-weight:bold;margin-left:25px">Helpful?</span>
                        <span style="cursor: pointer;"><img src="https://coworkspace.xenren.co/custom/css/frontend/searchCoWork/images/heart.png"></span>
                        </div>
                        </div>
                    </div>
                    <div id="chat_room" class="tab-pane fade">
                        <div class="check-that-time">
                            <div class="dropdown">
                                <button id="dropbtnSelect" class="dropbtn">Malaysia Time<img class="arrow-down" src="{{asset('images/arrowDowngreen.png')}}"></button>
                                <div class="dropdown-content">
                                    <a class="selectTime">Malaysia Time</a>
                                    <a class="selectTime">User's Time</a>
                                </div>
                            </div>
                        </div>
                        <header class="search-header-chat-room">
                            <div class="chat-user-detail-header">
                                <h3 class="chat-user-name">{{\Auth::guard('staff')->user()->name}} <img src="{{asset('images/arrowDownDDwhite.png')}}"></h3>
                                @if(\Auth::guard('staff')->user()->staff_type == \App\Models\Staff::STAFF_TYPE_ADMIN)
                                <small class="chat-user-role">Super Admin</small>
                                @else
                                <small class="chat-user-role">Admin</small>
                                @endif
                            </div>
                            <div class="chat-user-drop-down">
                                <img src="/{{\Auth::guard('staff')->user()->img_avatar }}" onerror="this.src='/images/image.png'">
                            </div>
                            <div class="search-box">
                                <input type="text" name="search" v-model="searchBox" @keyup="searchResults()" placeholder="{{trans('common.search_chat')}}" class="form-control">
                                <i @click="resetFilter()" id="classChange" class="fa fa-search fa-search-chat"></i>
                            </div>
                        </header>
                        <div class="container container-chat-section">
                            <div class="unlitimateWidth">
                            <div class="row">
                                <div class="col-md-3 custom-padding-col-md-3">
                                    <div class="chat-inner">
                                        <div class="chat-section-list">
                                            <div class="chat-user-list">
                                                <span>{{trans('common.user_list')}} (@{{ sharedOfficeUsers && sharedOfficeUsers.length }})</span>
                                            </div>
                                            <hr class="hr-chat-divider">
                                            <div class="section-inner-user-list scroll-div">
                                                <div class="chat-inner-list" id="activeClass" v-for="(users ,index) in sharedOfficeUsers" @click="openSection(users.user_id)" v-if="!users.action">
                                                    <div class="chat-image-avatar">
                                                        <img :src="getUserImage(users)">
                                                    </div>
                                                    <div class="chat-inner-details">
                                                        <span class="chat-user-inner-name">@{{ users.name }}</span><br>
                                                        <span class="chat-user-status">Hello,How you doing?...</span>
                                                    </div>
                                                    <div class="chat-user-online-status">
                                                        <span class="online-dot"></span><br>
                                                        <span class="ago-status">@{{ lastLoginTime(users) }}</span>
                                                    </div>
                                                </div>
                                                <div class="chat-inner-list" v-for="(users ,index ) in sharedOfficeUsers" @click="openSection(users.user_id)" v-if="users.action && (users.action == 1)">
                                                    <div class="chat-image-avatar">
                                                        <img :src="getUserImage(users)">
                                                    </div>
                                                    <div class="chat-inner-details">
                                                        <span class="chat-user-inner-name">@{{ users.name }}</span><br>
                                                        <span class="chat-user-status">Hello,How you doing?...</span>
                                                    </div>
                                                    <div class="chat-user-online-status">
                                                        <span class="online-dot"></span><br>
                                                        <span class="ago-status">@{{ lastLoginTime(users) }}</span>
                                                    </div>
                                                </div>
                                                <div class="chat-inner-list" v-for="(users ,index) in sharedOfficeUsers" @click="openSection(users.user_id)" v-if="users.action && (users.action == 0)">
                                                    <div class="chat-image-avatar">
                                                        <img :src="getUserImage(users)">
                                                    </div>
                                                    <div class="chat-inner-details">
                                                        <span class="chat-user-inner-name">@{{ users.name }}</span><br>
                                                        <span class="chat-user-status">Hello,How you doing?...</span>
                                                    </div>
                                                    <div class="chat-user-online-status">
                                                        <span class="online-dot"></span><br>
                                                        <span class="ago-status">@{{ lastLoginTime(users) }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="section-see-more" v-if="sharedOfficeUsers.length > 8">
                                                    {{trans('common.see_m')}}...
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 set-custom-width-col custom-width-slide" @click="closeRightSec">
                                    <h3 class="chat-group-section">{{trans('common.chat_name')}}</h3>
                                    <hr>
                                    <div class="chat-message-section scroller">
                                        @if($staff_type == \App\Models\Staff::STAFF_TYPE_ADMIN)
                                        <ul class="float-right-inner-chat" v-for="(messages, index) in getOfficeMessages" v-if="getChatCheck(messages)">
                                                <li class="inner-message-right">
                                                    <i class="fa fa-times" @click="deleteMessage(messages.id)"></i>
                                                    <div class="message-inner-right" v-if="messages.attachment == 0">
                                                        <p id="messages">
                                                            @{{ messages.message }}
                                                        </p>
                                                    </div>
                                                    <div class="message-inner-right" id="changeColor" v-if="messages.attachment == 1">
                                                        <p id="messages">
                                                            <a :href="'/'+messages.file" class="download-attachment" download target="__blank">
                                                                Download Attachment
                                                            </a>
                                                        </p>
                                                    </div>
                                                    <div class="custom-width-defined">
                                                        <img :src="'/'+getStaffImage(messages.staff_data)" onerror="this.src='/images/image.png'"><br>
                                                        <span class="inner-chat-user-name-right">@{{ getStaffData(messages.staff_data) }}</span>
                                                        <span class="color-dots">&middot;&middot;&middot;</span>
                                                    </div>
                                                </li>
                                            </ul>
                                        <br>
                                        <br>
                                        @endif
                                        <ul class="message-inner-chat" v-for="(messages, index) in getOfficeMessages" v-if="!getChatCheck(messages)">
                                            @{{ getUserData(messages) }}
                                            <li class="inner-message">
                                                <div class="image-user-chat">
                                                    <img :src="getUserMessageImages(messages)" onerror="this.src='/images/image.png'"><br>
                                                    <span class="inner-chat-user-name">@{{ getUserData(messages) }}</span>
                                                    <span class="color-dots">&middot;&middot;&middot;</span>
                                                </div>
                                                <div class="message-inner"  v-if="messages.attachment == 0">
                                                    <p>@{{ messages.message }}</p>
                                                </div>
                                                <i class="fa fa-times" @click="deleteMessage(messages.id)"></i>
                                            </li>
                                        </ul>
                                        <br>
                                        <br>
                                        @if($staff_type == \App\Models\Staff::STAFF_TYPE_STAFF)
                                        <ul class="float-right-inner-chat" v-for="(messages, index) in getOfficeMessages" v-if="getChatCheck(messages)">
                                                    <li class="inner-message-right">
                                                        <i class="fa fa-times" @click="deleteMessage(messages.id)"></i>
                                                        <div class="message-inner-right" v-if="messages.attachment == 0">
                                                            <p id="messages">
                                                                @{{ messages.message }}
                                                            </p>
                                                        </div>
                                                        <div class="message-inner-right" id="changeColor" v-if="messages.attachment == 1">
                                                            <p id="messages">
                                                                <a :href="'/'+messages.file" class="download-attachment" download target="__blank">
                                                                    Download Attachment
                                                                </a>
                                                            </p>
                                                        </div>
                                                        <div class="custom-width-defined">
                                                            <img :src="'/'+getStaffImage(messages.staff_data)" onerror="this.src='/images/image.png'"><br>
                                                            <span class="inner-chat-user-name-right">@{{ getStaffData(messages.staff_data) }}</span>
                                                            <span class="color-dots">&middot;&middot;&middot;</span>
                                                        </div>
                                                    </li>
                                                </ul>
                                        @endif
                                    </div>
                                    <div class="chat-send-message-box">
                                        <input type="text" name="messagebox" placeholder="{{trans('common.type_message')}}... " class="form-control" id="messagebox" v-model="messagesSend" v-on:keyup.13="hitEnter()">
                                        <a href="javascript:;" class="paper-clip" style="border:0px !important">
                                            <label for="file-input">
                                                <input type="file" class="file-input hide" v-on:change="fileReader" name="file-input" id="file-input">
                                                <i aria-hidden="true" class="fa fa-paperclip"></i>
                                            </label>
                                        </a>
                                        <a href="javascript:;" class="paper-send" @click="hitEnter()">
                                            <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                                <transition name="fade">
                                <div v-show="hideShowCheck" class="col-md-3 custom-padding-col-3">
                                        <div class="about-chat-user">
                                            <img :src="userDetails.img_avatar">
                                            <br>
                                            <h3 class="user-name-about-section">@{{ userDetails.name }}</h3>
                                            <span class="user-live-about-section">Lives in @{{ userDetails.country_name }}</span>
                                            <hr class="user-live-about-section-border">
                                            <div class="tabs-section-about-us">
                                                <div class="tabs-about">
                                                    <ul class="nav nav-tabs about-us-nav-tabs">
                                                        <li class="active about-us">
                                                            <a data-toggle="tab" class="about-text" href="#about">
                                                                <img src="{{asset('images/usericon.png')}}">
                                                                {{trans('common.about')}}
                                                            </a>
                                                        </li>
                                                        <li class="settings">
                                                            <a data-toggle="tab" class="setting-tab" href="#settings">
                                                                <i class="fa fa-cog"></i>
                                                                {{trans('common.setg')}}
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <br>
                                                    <div class="tab-content custom-tab-content">
                                                        <div id="about" class="tab-pane fade in active">
                                                            <div class="about-us-info">
                                                                <div class="user-icon-about-us">
                                                                    <img src="{{asset('images/uuser-icon.png')}}">
                                                                </div>
                                                                <div class="about-user-name">
                                                                    <span class="name-S">Name</span><br>
                                                                    <span class="full-name-S">@{{ userDetails.name }}</span>
                                                                </div>
                                                            </div>
                                                            <div class="phone-details">
                                                                <div class="phone-image">
                                                                    <img src="{{asset('images/phone_ringing.png')}}">
                                                                </div>
                                                                <div class="contact-info">
                                                                    <span class="number-C">Contact Number</span><br>
                                                                    <span class="full-number-C">@{{ userDetails.handphone_no }}</span>
                                                                </div>
                                                            </div>
                                                            <div class="email-details">
                                                                <div class="email-image">
                                                                    <img src="{{asset('images/envelope.png')}}">
                                                                </div>
                                                                <div class="contact-info">
                                                                    <span class="number-C">Email</span><br>
                                                                    <span class="full-number-C">@{{ userDetails.email }}</span>
                                                                </div>
                                                            </div>
                                                            <div class="phone-details">
                                                                <div class="phone-image">
                                                                    <img src="{{asset('images/blocation.png')}}">
                                                                </div>
                                                                <div class="contact-info">
                                                                    <span class="number-C">Address</span><br>
                                                                    <span class="full-number-C">@{{ userDetails.address }}</span>
                                                                </div>
                                                            </div>
                                                            <div class="phone-details">
                                                                <div class="skill-image">
                                                                    <img src="{{asset('images/skillsW.png')}}">
                                                                </div>
                                                                <div class="skill-info">
                                                                    <span class="number-C">Skills</span><br>
                                                                    <span class="full-number-C" v-for="offical in officelS">
                                                                        <div class="skill-div">
                                                                             <span class="item-skill official">
                                                                                <img src="/images/crown.png" alt="" height="24px" width="24px">
                                                                                <span>
                                                                                    @{{ offical }}
                                                                                </span>
                                                                            </span>
                                                                        </div>
                                                                    </span>
                                                                    <span class="full-number-C" v-for="clientV in clientVerifiedS">
                                                                        <div class="skill-div">
                                                                             <span class="item-skill admin-verified">
                                                                                <i class="fa fa-check"></i>
                                                                                <span>
                                                                                    @{{ clientV }}
                                                                                </span>
                                                                            </span>
                                                                        </div>
                                                                    </span>
                                                                    <span class="full-number-C" v-for="normal in normalS">
                                                                        <div class="skill-div">
                                                                             <span class="item-skill">
                                                                                <span>
                                                                                    @{{ normal }}
                                                                                </span>
                                                                            </span>
                                                                        </div>
                                                                    </span>
                                                                    <span class="full-number-C">
                                                                        <div class="skill-div">
                                                                             <span class="item-skill">
                                                                                <span>
                                                                                    HTML
                                                                                </span>
                                                                            </span>
                                                                        </div>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="settings" class="tab-pane fade">
                                                            <p class="kick-user" @click="kicked(userDetails.id)">
                                                                <span v-if="userDetails.shared_office_chat_action && (userDetails.shared_office_chat_action.action == 0)">Kicked</span>
                                                                <span v-else>{{trans('common.kick')}}</span>
                                                            </p>
                                                            <hr class="hr-tabs">
                                                            <p class="block-user" @click="blocked(userDetails.id)">
                                                                <span v-if="userDetails.shared_office_chat_action && (userDetails.shared_office_chat_action.action == 1)">Blocked</span>
                                                                <span v-else>Block User</span>
                                                            </p>
                                                            <hr class="hr-tabs">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </transition>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div id="office_finance" class="tab-pane fade">
                    <input type="hidden" id="start_date">
                        <input type="hidden" id="end_date">
                        <div class="row">
                            <div class="r-0">
                                <div class="customStyleSet2">
                                    <div class="col-md-3 custom-padding">
                                    <div class="box-one">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                        <span>{{trans('common.office_edit')}}</span>
                                    </div>
                                </div>
                                    <div class="col-md-3 custom-padding">
                                    <div class="box-two">
                                        <img src="{{asset('images/timer.png')}}">
                                        <span>{{trans('common.rent_product')}}</span>
                                    </div>
                                </div>
                                </div>
                                <div class="customDisplaySet">
                                    <div class="col-md-2 custom-width">
                                        <div class="box-three">
                                            <i class="fa fa-dollar"></i>
                                            <span>{{trans('common.income')}}</span>
                                        </div>
                                    </div>

                                    <div class="col-md-2 custom-width">
                                        <div class="box-fourth-modal-withdraw">
                                        @if(!$staffCheckCurrencyExist)
                                            <span data-toggle="modal" data-target="#staffCurrencyModal"
                                            >{{trans('common.wd')}}</span>
                                        @else
                                            <span data-toggle="modal" data-target="#withdrawl"
                                            >{{trans('common.wd')}}</span>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="custom-breadcrumb">
                                    <p>
                                        <a href="">{{trans('common.dash_shared')}}</a> | <a href="">{{trans('common.share_office')}}</a> | <span>{{trans('common.income')}}</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="r-1">
                                <div class="col-md-3 custom-width-section-two">
                                    <div class="date-time-box">
                                        <div class="right-sec">
                                            <h3>{{trans('common.start')}}</h3>
                                            <input type="text" class="reportrange" id="startedDate" placeholder="06/01/2019" readonly>
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                        </div>
                                        <span></span>
                                        <div class="left-sec">
                                            <h3>{{trans('common.end')}}</h3>
                                            <input type="text" class="reportrange" id="endedDate" placeholder="06/01/2019" readonly>
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 set-custom-padd-col-5">
                                    <div class="custom-search-box">
                                        <input type="text" placeholder="{{trans('common.search')}}" name="search" class="form-control">
                                        <i class="fa fa-search"></i>
                                    </div>
                                </div>
                                <div class="simpleOne">
                                    <div class="col-md-1 custom-width-col-firs">
                                    <div class="filter-box">
                                        <i class="fa fa-filter" aria-hidden="true"></i>
                                        <br>
                                        <span>{{trans('common.ft')}}</span>
                                    </div>
                                </div>
                                    <div class="col-md-1 custom-width-col-sec">
                                    <div class="statements-box">
                                        <i class="fa fa-bars"></i>
                                        <br>
                                        <span>{{trans('common.st')}}</span>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="portlet light bordered">
                            <div class="portlet-body">
                                <div class="table-responsive tabled">
                                    <h1>June 2019</h1>
                                    <div v-if="showLoader" class="preload" style="margin: 0 auto;display: block;width: 5%;">
                                            <img src="http://i.imgur.com/KUJoe.gif">
                                    </div>
                                    <table class="table table-striped set-table-striped">
                                        <tbody>

                                        <tr v-for="(data,index) in financeProducts" v-if="!showLoader" style="background-color:#f5f6f7">
                                            <td class="custom-align-style">
                                                @{{data.endDate}}
                                            </td>
                                            <td class="custom-style">
                                                @{{data.name ? data.name : 'Transaction Charges'}}<br>
                                                <span>{{trans('common.py')}}</span>
                                            </td>
                                            <td class="custom-color-style">
                                                @{{data.cost}}<br>
                                                <span>
                                                    USD
                                                </span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="total-price">
                                    <span>{{trans('common.tt')}} : <span class="total-bill">@{{financeTotalAmount}}</span></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="booking" class="tab-pane fade">
                    <input type="hidden" id="start_date">

                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="custom-breadcrumb">
                                    <p>
                                        <a href="">{{trans('common.dash_shared')}}</a> | <a href="">{{trans('common.share_office')}}</a> | <span>{{trans('common.booking')}}</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="portlet light bordered">
                            <div class="portlet-body">
                                <div class="table-responsive tabled">
                                    <h1>{{trans('common.booking')}}</h1>
                                    <div v-if="showLoader" class="preload" style="margin: 0 auto;display: block;width: 5%;">
                                            <img src="http://i.imgur.com/KUJoe.gif">
                                    </div>
                                    <table class="table table-striped set-table-striped">
                                        <tbody>
                                        <tr>
                                            <td>{{trans('common.email')}}</td>
                                            <td>{{trans('common.full_name')}}</td>
                                            <td>{{trans('common.phone')}} #</td>
                                            <td>{{trans('common.check_in')}}</td>
                                            <td>{{trans('common.check_out')}}</td>
                                            <td>{{trans('common.nu_of_rooms')}}</td>
                                            <td>{{trans('common.sty')}}</td>
                                            <td></td>
                                        </tr>
                                        <tr v-for="(data,index) in bookings" v-if="!showLoader" style="background-color:#f5f6f7">
                                            <td class="custom-align-style" style="text-align: left">
                                                @{{data.client_email}}
                                            </td>
                                            <td class="custom-align-style" style="text-align: left">
                                                @{{data.full_name}}<br>
                                            </td>
                                            <td class="custom-align-style" style="text-align: left">
                                                @{{data.phone_no}}
                                            </td>
                                            <td class="custom-align-style" style="text-align: left">
                                                @{{data.check_in}}
                                            </td>
                                            <td class="custom-align-style" style="text-align: left">
                                                @{{data.check_out}}
                                            </td>
                                            <td class="custom-align-style" style="text-align: left">
                                                @{{data.no_of_rooms}}
                                            </td>
                                            <td class="custom-align-style" style="text-align: left">
                                                @{{data.category_name}}
                                            </td>
                                            <td class="custom-align-style" style="text-align: left">
                                                <a href="#" @click="openConfirmBooking(data.id)" data-toggle="modal" data-target="#confirmBooking">Confirm Booking</a> ||
                                                <a href="#" @click="cancelBooking(data.id)">Cancel Booking</a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal" id="confirmBooking" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Confirm Booking</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="seats">Available Seats:</label>
                            <select name="" id="" class="from-control" v-model="selectedAvailableSeat">
                                <option value="" v-for="data in availableSeats" :value="data.id"> Seat # @{{ data.number }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" @click="bookSeat()">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content set-modal-content">
                    <div class="modal-header set-modal-header">
                        <button type="button" class="btn-times" data-dismiss="modal"><i class="fa fa-times"></i>
                        </button>
                        <h4 class="set-modal-title">Person:<span>@{{ personsAddedObj.length }}</span></h4>
                    </div>
                    <div class="modal-body set-moda-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr style="background-color:#e3e4e6;" v-for="(data, index) in personsAddedObj" v-cloak>
                                    <td>@{{ data.name }}</td>
                                    <td>
                                        @{{ formatDate(data.start_time) }} - @{{ formatDate(data.end_time) }}
                                    </td>
                                    <td>@{{ data.product_type_name }} | Seat No @{{ data.number }}</td>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                    <div class="modal-footer set-modal-footer">
                        <div id="panel">
                            <form class="form">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" placeholder="Enter User Name"
                                                   class="form-control set-form-control" name="" id="username"
                                                   v-model="personsAdded.name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select class="form-control setSelectBoxStyle" name="option">
                                                <option>Login</option>
                                                <option>Booking</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group input-group">
                                            <input type="text" value="" id="start_time" style="cursor:pointer;"
                                                   placeholder="Enter Starting Time"
                                                   class="form-control set-form-control start_time_dtp" name=""><span
                                                    class="image-arroww-down">&#8964;</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group input-group">
                                            <input type="text" style="cursor:pointer;" placeholder="Enter Ending Time"
                                                   class="form-control set-form-control end_time_dtp" name=""
                                                   id="end_time"
                                            ><span
                                                    class=" image-arroww-down">&#8964;</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <select name="" id="seats" class="form-control select2Custom">
                                                <option v-for="(data, index) in this.productsOrg.office_products"
                                                        :value="data.id" :data-id="data.id" v-cloak>
                                                    @{{ data.product_type_name }} | @{{ data.number }} ($@{{
                                                    data.time_price }})
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <button type="button" class="btn btn-default btn-more flip" @click="addUserPanelFnc(false)"
                               >
                            <i class="fa fa-plus"></i>More USER
                        </button>
                        <h3>$@{{ totalAmount }}</h3>
                        <button type="button" class="btn btn-default btn-more-confirm" @click="save()">CONFIRM
                        </button>
                    </div>
                </div>

            </div>
        </div>
        <!-- Modal -->
        <div id="withdrawl" class="modal fade" role="dialog">
            <div class="modal-dialog" id="modalDailog">

                <!-- Modal content-->
                <div class="modal-content" id="modalContent">
                    <div class="modal-header" id="modalHeader">
                        <button type="button" id="buttonClose" data-dismiss="modal">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <h4 class="modal-title" id="modalTitle">{{trans('common.Withdrawl')}}</h4>
                    </div>
                    <div class="modal-body" id="modalBody">
                        <form method="post">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{trans('common.bank_name')}}</label>
                                        <input type="text" name="bank_name" placeholder="{{trans('common.enter_bank_name')}}" v-model="bank_name" class="form-control" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{trans('common.iban_number')}}</label>
                                        <input type="number" name="iban_number" placeholder="{{trans('common.enter_iban_number')}}" v-model="iban_number" class="form-control" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{trans('common.card_number')}}</label>
                                        <input type="number" name="card_number" placeholder="{{trans('common.enter_card_number')}}" v-model="card_number" class="form-control" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{trans('common.branch_name')}}</label>
                                        <input type="text" name="branch_name" placeholder="{{trans('common.enter_branch_name')}}" v-model="branch_name" class="form-control" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{trans('common.branch_code')}}</label>
                                        <input type="number" name="branch_code" placeholder="{{trans('common.enter_branch_code')}}" v-model="branch_code" class="form-control" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{trans('common.branch_address')}}</label>
                                        <input type="text" name="branch_address" placeholder="{{trans('common.enter_branch_address')}}" v-model="branch_address" class="form-control" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{trans ('common.payment_request')}}</label>
                                        <input type="number" placeholder="{{trans ('common.enter_payment_request')}}" name="payment_request" v-model="payment_request" class="form-control" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{trans ('common.country')}}</label>
                                        <select class="form-control" name="country_id">
                                            <option selected disabled>{{trans('common.select_country')}}</option>
                                            @foreach($countries as $country)
                                                <option value="{{$country->id}}">{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer" id="modalFooter">
                        <button type="button" class="btn btn-success btnWithdrawal" name="submit" @click="Withdrawal('{{$id}}')">{{trans('common.Withdrawl')}}</button>
                    </div>
                </div>

            </div>
        </div>

        <!-- currency Model -->
        @if(!$staffCheckCurrencyExist)
            <div id="staffCurrencyModal" class="modal fade" role="dialog">
                <div class="modal-dialog" id="modalDialoge">
                    <!-- Modal content-->
                    <div class="modal-content" id="modalContent">
                        <div class="modal-header">
                            <h4>
                                Add Currency
                            </h4>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Currency Name : </label>
                                            <select required class="form-control" name="currency_name" v-model="currency">
                                                <option selected disabled>Select Your Currency</option>
                                                @foreach($currencies as $currency)
                                                    <option value="{{$currency->id}}">{{$currency->currency_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input type="submit" name="submit" value="Save" @click.prevent="submitUserCurrency" class="btn btn-success" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>

    <div class="customBtnAddUserSet" style="
    " v-if="buttonActive">
        <button class="btn-add-user customBtnAddUserDisplayD" data-toggle="modal" data-target="#myModal">
            + Login or Booking
        </button>
    </div>
    </div>
@endsection
