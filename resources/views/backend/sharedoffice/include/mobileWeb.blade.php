<div class="mobileWeb">
    <div id="vueJsAddMoreImages" class="container customContainer">
        <div class="portlet-body form">
            <form enctype="multipart/form-data">
                <div class="form-body">
                    <div class="form-group">
                        <input type="hidden" name="office_id" value="{{$model->id}}" />
                        <label class="control-label">
                            <img class="leftArrowImage" onclick="window.location.href='/admin/sharedoffice'" src="{{asset('images/leftArrow.png')}}" />
                            Add more images for shared office
                        </label>
                        <div>
                            @if($model->image)
                                <img id="image_preview" class="img-thumbnail img-preview"
                                     src="{{asset($model->image)}}" v-model="image"/>
                            @else
                                <img id="image_preview" style="display: none" v-model="image" class="img-thumbnail img-preview"                                alt="{{$main_image}}"
                                     src="{{asset($main_image)}}"/>
                            @endif
                        </div>
                        <label class="fileContainer">
                            <p style="margin: 0px;
                                display: flex;
                                flex-direction: row;
                                justify-content: center;
                                align-items: center;">
                                <img style="width: 17px;margin-right: 6px" src="{{asset('images/cameraaa.png')}}">
                                <span class="customAdd1" style="font-size: 11px">Edit</span>
                            </p>
                            <input accept="image/*"
                                   class="image-upload"
                                   target="image_preview"
                                   onChange="imageFileInputChange(this)"
                                   name="image"
                                   ref="myImage"
                                   type="file">
                        </label>
                    </div>
                    <div class="form-group customUload">
                        <label>Upload cover image</label>
                    </div>
                    <div class="form-group customSizeSelect">
                        <div class="form-group">
                            <select class="form-control" required v-model="image_size">
                                <option selected="selected">1366 X 768</option>
                                <option>683 X 768</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label></label>
                        <div class="container containerProgress">
                            <div id="percentageCount" class="percent-count"></div>
                            <div class="progress-bar" style="display: none">
                                <div class="progress" id="progress">
                                </div>
                            </div>
                        </div>
                        <div class="form-group customUload">
                            <label>Upload thumbnail images</label>
                        </div>
                        <div class="row" style="padding: 20px;padding-top: 0px;margin-top:10px;">
                            <div class="col-md-12" style="display: flex;flex: 1;flex-flow: wrap;" v-if="loadFiles">
                                <div v-for="(img ,index) in multiple" v-if="img.type == 'image'">
                                    <div class="lds-ring-loader" v-model="loadFiles"><div></div><div></div><div></div><div></div></div>
                                </div>
                            </div>
                            <div class="col-md-12" style="display: flex;flex: 1;flex-flow: wrap;" v-else>
                                <div v-for="(img ,index) in multiple" v-if="img.type == 'image'">
                                    <img class="imageThumb" :data-id="index" style="border: 1px solid #C0C0C0;width:75px;height:75px;border-radius:10px;margin-right:10px;object-fit:cover;" :src="img.src"  :title="img.name"/>
                                    <br/><span class="remove set-remove imgRemove" @click.prevent="removeImage(index,img.file,img.id)"><img src="{{asset('images/cross-image.png')}}"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                            </div>
                        </div>
                        <div class="row customClassBtn">
                            <label class="fileContainer2">
                                <p style="margin: 0px;
                                display: flex;
                                flex-direction: row;
                                justify-content: center;
                                align-items: center;">
                                    <img class="plusion" src="{{asset('images/plusIcon.png')}}" />
                                    <span class="customAdd1">Thumbnails</span>
                                </p>
                                <input type="file"
                                       accept="image/*"
                                       name="image_file[]"
                                       id="image_file"
                                       data-target="image_previeww"
                                       ref="myFile"
                                       multiple="multiple"
                                       @change="fileChanged"
                                       class="image-upload">
                            </label>
                        </div>
                        <div class="form-group customUload">
                            <label>Upload Video</label>
                        </div>
                        <div class="row" style="padding: 20px;padding-top: 0px;margin-top:10px;">
                            <div class="col-md-12" style="display: flex;flex: 1;flex-flow: wrap;" v-if="loadVideo">
                                <div v-for="(img ,index) in multiple" v-if="img.type == 'video'" style="width: 100%;height:205px;">
                                    <div class="lds-ring-loader-video" v-model="loadFiles"><div></div><div></div><div></div><div></div></div>
                                </div>
                            </div>
                            <div class="col-md-12" v-else>
                                <div v-for="(img ,index) in multiple" v-if="img.type == 'video'">
                                    <video width="400" controls style="height:205px;width:100%">
                                        <source :src="img.src" type="video/mp4">
                                    </video>
                                    <br/><span class="remove set-remove1 imgRemove" @click.prevent="removeVideo(index,img.id)"><img src="{{asset('images/cross-image.png')}}"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row customClassBtn">
                            <label class="fileContainer2">
                                <p style="margin: 0px;
                                display: flex;
                                flex-direction: row;
                                justify-content: center;
                                align-items: center;">
                                    <img class="plusion" src="{{asset('images/plusIcon.png')}}" />
                                    <span class="customAdd1">Add Video</span>
                                </p>
                                <input type="file"
                                       accept="video/*"
                                       name="image_file[]"
                                       id="image_file"
                                       data-target="image_previeww"
                                       ref="myFileVideo"
                                       @change="fileChangedUploadVideo"
                                       class="image-upload">
                            </label>
                        </div>

                        <div class="row customClassBtn1">
                            <button class="btn btn-default customBtnClass1" @click.prevent="saveAdditionalImages">
                                Cover Image
                            </button>
                        </div>

                        <div class="row customClassBtn2">
                            <button class="btn btn-default customBtnClass2">
                                Cancel
                            </button>
                        </div>
                        <div class="row customClassBtn2">
                            <button type="button" onclick="window.location.href='/admin/sharedoffice'" class="btn btn-default customBtnClass2">
                                Preview Office
                            </button>
                        </div>
                        {{--                        <div class="lds-ring-ring" v-model="loaderButton" v-if="loaderButton"><div></div><div></div><div></div><div></div></div>--}}
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
