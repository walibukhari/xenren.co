@extends('backend.layout')

@section('title')
    {{trans('sharedoffice.renting_product_by_time')}}
@endsection

@section('description')
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>

            <a href="{{ route('backend.sharedoffice.products.index', ['id' => $sharedofficeproduct->office_id]) }}">{{trans('sharedoffice.sharedofficeproduct')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedoffice.products.create', ['id' => $id]) }}">{{trans('sharedoffice.renting_product_by_time')}}</a>
        </li>
    </ul>
@endsection

@section('description')

@endsection

@section('footer')
    <script>
        +function() {
            $(document).ready(function() {
                $('#sharedoffice-form').makeAjaxForm({
                    redirectTo: '{{ route('backend.sharedoffice.products.create', ['id' => $id]) }}',
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-helper">{{trans('sharedoffice.renting_product_by_time')}}</span>
            </div>
            <div class="actions">

            </div>
        </div>
        <div>
            @php
                $status = isset($sharedofficeproduct->productstatus) ? $sharedofficeproduct->productstatus->status : '';
            @endphp
            <h3 class="">
            <span >{{trans('sharedoffice.officename')}}:</span> <span class="badge badge-info">{{ $sharedofficeproduct->office->office_name }}</span>
            <span >{{trans('sharedoffice.location')}}:</span>  <span class="badge badge-info">{{ $sharedofficeproduct->office->location }}</span>
            <span >{{trans('sharedoffice.productid')}}:</span>  <span class="badge badge-info">{{ $sharedofficeproduct->id }}</span>
            <span >{{trans('sharedoffice.categories')}}:</span>  <span class="badge badge-info">{{ $sharedofficeproduct->categories }}</span>
            <span >{{trans('sharedoffice.status')}}:</span>  <span class="badge badge-info">{{ $status }}</span>

            </h3>


        </div>
        <div class="portlet-body form">
            {!! Form::open(['method' => 'post', 'role' => 'form']) !!}
            <div class="form-body">

                <div class="form-group">
                    {{ Form::hidden('office_id', $sharedofficeproduct->office_id) }}
                    {{ Form::hidden('product_id', $sharedofficeproduct->id) }}
                    {{ Form::hidden('x', $sharedofficeproduct->x) }}
                    {{ Form::hidden('y', $sharedofficeproduct->y) }}
                    {{ Form::hidden('status', '1') }}
                    {{ Form::hidden('startdate', Carbon\Carbon::now()) }}
                 </div>

                <div class="form-group">
                    {!! Form::label('user',trans('sharedoffice.name_client'),['class'=>'control-label']) !!}
                    <select name="user" id="user" class="form-control"></select>
{{--                    {!! Form::text('user','',['class'=>'form-control','required' => '', 'placeholder' => trans('sharedoffice.name_client')]) !!}--}}
                </div>


                <div class="form-actions">
                    <button type="submit" class="btn blue">{{trans('sharedoffice.savechanges')}}</button>
                    {!! Html::linkRoute('backend.sharedoffice.products.index', trans('sharedoffice.cancel'), array($sharedofficeproduct->office_id), array('class' => 'btn btn-danger')) !!}

                </div>

                {!! Form::close() !!}

            </div>
        </div>
@push('js')
        <script>
            $('#user').select2({
	            ajax: {
		            url: '/api/v1/users',
		            minimumInputLength: 3,
		            dataType: 'json',
		            data: function (params) {
			            var query = {
				            search: params.term,
				            type: 'public'
			            };
			            return query;
		            },
		            processResults: function (data) {
			            return {
				            results: data.data
			            };
		            }
	            }
            });

        </script>
@endpush
@endsection