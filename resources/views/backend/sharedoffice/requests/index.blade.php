@extends('backend.layout')

@section('title')
    {{trans('sharedoffice.sharedoffice')}}&nbsp;<small>{{trans('sharedoffice.request')}}</small>
@endsection


@section('description')

@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.sharedoffice')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedofficerequest') }}">{{trans('sharedoffice.shared_office_request')}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <link href="/css/sharedOfficeRequest.css" rel="stylesheet" type="text/css">
@endsection

@section('footer')
    <script>
        function deleteOfficeRequest($id, event)
        {
            event.preventDefault();
            var url = 'delete/sharedofficerequest/'+$id;
            $.ajax({
               url:url,
               method:'get',

               success:function(data)
               {
                   console.log(data);
                   if(data.status === 'success')
                   {
                       toastr.success('office request deleted successfully');
                        setTimeout(function(){window.location.reload()},1000);
                   }
               }
            });
        }

        function editOfficeRequest($id)
        {
            var url = 'edit/sharedofficerequest/'+$id;
            $.ajax({
                url:url,
                method:'get',

                success:function(data)
                {
                    console.log(data);
                    if(data.status === 'success')
                    {
                        $('#editId').val(data.data.id);
                        $('#companyname').val(data.data.company_name);
                        $('#officeLocation').val(data.data.location);
                        $('#fullName').val(data.data.full_name);
                        $('#phoneNumber').val(data.data.phone_number);
                        $('#Email').val(data.data.email);
                        var img = '<img id="output" style="width:254px; height:59px;" src="../uploads/listSpaceImages/'+data.data.image+'">';
                        $('#Image').html('');
                        $('#Image').append(img);
                    }
                }
            });
        }

        var loadFile = function(event) {
            var reader = new FileReader();
            reader.onload = function(){
                var output = document.getElementById('output');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };

        $(document).ready(function(){
            $('#formData').on('submit',function(e){
                e.preventDefault();
                var id = $('#editId').val();
                var token = '{{ csrf_token() }}';
                var company_name = $('#companyname').val();
                var location  = $('#officeLocation').val();
                var full_name = $('#fullName').val();
                var phone = $('#phoneNumber').val();
                var email = $('#Email').val();
                if(company_name === '') {
                    toastr.error('Please fill company name fiels');
                } else if(location === '') {
                    toastr.error('Please select location');
                } else if(full_name === '') {
                    toastr.error('Please fill full name field');
                } else if(phone === '') {
                    toastr.error('Please fill phone number field');
                } else if(email === '') {
                    toastr.error('Please fill email field');
                } else {
                    var url = 'update/shardofficerequest/' + id;
                    $.ajax({
                        url: url,
                        method: 'post',
                        headers: {
                            Authorization: token
                        },
                        contentType: false,
                        cache: false,
                        processData: false,
                        data: new FormData(this),
                        success: function (data) {
                            console.log(data);
                            if (data.status === 'success') {
                                toastr.success('office request updated');
                                setTimeout(function () {
                                    window.location.reload()
                                }, 1000);
                            }
                        }
                    });
                }
            });

            $('input[id="checkboxed"]').click(function(){
                if($(this).prop("checked") === true){
                    window.location.href= '?show_markerd=true';
                }
                else if($(this).prop("checked") === false){
                    window.location.href= '?show_markerd=false';
                }
            });

        });
    </script>
    <script>
        @if(\Session::has('message'))
           var message = '{{\Session::get('message')}}';
           toastr.success(message);
        @endif
    </script>
@endsection
@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase">{{trans('sharedoffice.sharedoffice')}}</span>
            </div>
            <div class="caption font-green-sharp" style="float:right">
                <div class="checkbox">
                    <label>
                        <span>{{trans('sharedoffice.show_reviewed')}}</span>
                        <input type="checkbox" id="checkboxed" value="" @if($show_marked) checked @endif>
                        <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                    </label>
                </div>
            </div>
        </div>
        <div class="portlet-body">
        <div class="table-container">
            <table class="table table-striped table-bordered">
                <thead>
                <tr role="row" class="heading">
                    <th>{{trans('sharedoffice.id')}}</th>
                    <th>{{trans('sharedoffice.company_name')}}</th>
                    <th>{{trans('sharedoffice.office_location')}}</th>
                    <th>{{trans('sharedoffice.full_name')}}</th>
                    <th>{{trans('sharedoffice.phone_number')}}</th>
                    <th>{{trans('sharedoffice.email')}}</th>
                    <th>{{trans('sharedoffice.image')}}</th>
                    <th>{{trans('sharedoffice.action')}}</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($officeRequests as $requests)
                        <tr>
                            <td>{{$requests->id}}</td>
                            <td>{{$requests->company_name}}</td>
                            <td>{{$requests->location}}</td>
                            <td>{{$requests->full_name}}</td>
                            <td>{{$requests->phone_number}}</td>
                            <td>{{$requests->email}}</td>
                            <td>
                                <a href="../uploads/listSpaceImages/{{$requests->image}}" data-lightbox="roadtrip">
                                    <img class="set-imagelistspace-get" src="../uploads/listSpaceImages/{{$requests->image}}">
                                </a>
                            </td>

                            <td>
                                @if($show_marked)
                                    <a class="marked btn btn-xs btn-info">{{trans('sharedoffice.approved')}}</a>

                                @else
                                <a id="markedViewed" href="{{url('admin/markasviewed/sharedofficerequest',$requests->id)}}" class="marked btn btn-xs btn-info">{{trans('sharedoffice.mark_viewed')}}</a>
                                    <br>
                                    @if($requests->status == \App\Models\SharedOfficeRequest::SHARED_OFFICE_PERSON_EMAIL_VERIFIED_STATUS)
                                        <a class="marked btn btn-xs btn-success" style="font-size: 11px;
    border: 0px;background: #25ce0f;font-weight: bold;">{{trans('sharedoffice.approved_by_user')}}</a>
                                    @else
                                        <a class="marked btn btn-xs btn-danger" style="font-size: 11px;
    border: 0px;background: red;font-weight: bold;">{{trans('sharedoffice.not_approved_by_user')}}</a>
                                    @endif
                                @endif
                                <a href="#" data-toggle="modal" onclick="editOfficeRequest('{{$requests->id}}')" data-target="#myeditModal" class="btn btn-xs blue"><i class="fa fa-pencil"></i></a>
                                <a href="javascript:" onclick="deleteOfficeRequest('{{$requests->id}}',event)" class="uppercase btn btn-xs primary red"><i class="fa fa-trash"></i></a> </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    </div>
@endsection


@section('modal')
    <!-- myeditModal -->
    <div id="myeditModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" id="btnclose" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Shared Office Request</h4>
                </div>
                <form enctype="multipart/form-data" id="formData">
                    {{ csrf_field() }}
                    <input type="hidden" value="" id="editId">
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <label>Company Name:</label>
                                        <input type="text" class="form-control" name="company_name" id="companyname" value="">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Location:</label>
                                        <input type="text" class="form-control" name="location" id="officeLocation" value="">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <label>Full Name:</label>
                                        <input type="text" class="form-control" name="full_name" id="fullName" value="">
                                    </div>
                                    <div class="col-md-6">
                                        <label>Phone Number:</label>
                                        <input type="text" class="form-control" name="phone_number" id="phoneNumber" value="">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <label>Email:</label>
                                        <input type="email" class="form-control" name="email" id="Email" value="">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 set-col-md-12-backend">
                                    <div id="Image">

                                    </div>
                                    <label>Image:</label>
                                    <input type="file" name="file" class="form-control" id="image_name" onchange="loadFile(event)">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button id="updateOfficeRequest" type="submit"  class="btn btn-success">Update</button>
                        {{--<button type="button" class="btn btn-default" id="btnclose" data-dismiss="modal">Close</button>--}}
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection