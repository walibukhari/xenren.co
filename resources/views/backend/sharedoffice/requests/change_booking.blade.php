@extends('backend.layout')

@section('title')
    {{trans('sharedoffice.sharedoffice')}}&nbsp;<small>{{trans('common.b_request')}}</small>
@endsection

@section('description')

@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.sharedoffice')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedofficebookingrequest') }}">{{trans('sharedoffice.shared_office_request')}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <link href="/css/sharedOfficeRequest.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div id="ChangeBookingRequests" class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase">{{trans('sharedoffice.sharedoffice')}}</span>
            </div>
        </div>
        <div class="portlet-body">
            <form>
                <div class="row">
                    <div class="col-md-4">
                        <label>{{trans('common.username')}}:</label>
                        <input type="text" placeholder="{{trans('common.search_by_username')}}" name="username" class="form-control" />
                    </div>
                    <div class="col-md-4">
                        <label>{{trans('common.email')}}:</label>
                        <input type="text" name="email" placeholder="{{trans('common.s_b_email')}}" class="form-control" />
                    </div>
                </div>
                <div class="row">
                    <br>
                    <div class="col-md-6">
                        <button class="btn btn-success">{{trans('common.search')}}</button>
                        <button class="btn btn-primary">{{trans('common.reset')}}</button>
                    </div>
                </div>
            </form>
            <br>
            <div class="table-container table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr role="row" class="heading">
                        <th>#</th>
                        <th>{{trans('common.o_i')}}</th>
                        <th>{{trans('common.o_n')}}</th>
                        <th>{{trans('common.u_e')}}</th>
                        <th>{{trans('common.username')}}</th>
                        <th>{{trans('common.c_s_n')}}</th>
                        <th>{{trans('common.c_ss_n')}}</th>
                        <th>{{trans('common.cs_s')}}</th>
                        <th>{{trans('common.c_s_ss')}}</th>
                        <th>{{trans('common.check_in_date')}}</th>
                        <th>{{trans('common.check_out_date')}}</th>
                        <th style="text-align: center;">{{trans('common.action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($bookedRequests as $requests)
                        <tr>
                            <td>{{$requests->id}}</td>
                            <td>{{$requests->office_id}}</td>
                            <td>{{$requests->office->office_name}}</td>
                            <td>{{$requests->user->email}}</td>
                            <td>{{$requests->user->name}}</td>
                            <td>
                                {{$requests->currant_seat}}
                                <a href="javascript:;" @click="openCurrantSeatEditPopUp('{{$requests->currant_seat}}','{{$requests->office->id}}','{{$requests->user->id}}','{{$requests->id}}')">Edit</a>
                            </td>
                            <td>{{$requests->change_seat}}</td>
                            <td>{{$requests->booking->getCurrantStatusName($requests->currant_seat)}}</td>
                            <td>{{$requests->product->getStatusName($requests->product->status_id)}}</td>
                            <td>{{$requests->check_in}}</td>
                            <td>{{$requests->check_out}}</td>
                            <td style="width:60%;text-align:center;font-weight:500;color:#347ab7;">
                                @if($requests->status == \App\Models\ChangeBookingRequest::STATUS_DEFAULT)
                                <a href="javascript:;" @click="confirmChange('{{$requests->id}}','{{route('backend.confirmChangeBookingRequest')}}','{{$requests->book_id}}','{{$requests->check_in}}','{{$requests->check_out}}','{{$requests->seat_id}}','{{$requests->currant_seat}}')">confirm</a>
                                <span>/</span>
                                <a href="javascript:;" @click="cancelChangeRequest('{{$requests->book_id}}','{{$requests->id}}')">cancel</a>
                                <span>/</span>
                                <br>
                                <a href="javascript:;" @click="changeDateTime('{{$requests->id}}')">
                                    <img src="/images/change_date_time.png" style="width: 13px;height:12px;margin-right:10px;" />Change
                                </a>
                                @endif
                                @if($requests->status == \App\Models\ChangeBookingRequest::STATUS_CONFIRM)
                                    <a href="#">confirmed</a>
                                @endif
                                @if($requests->status == \App\Models\ChangeBookingRequest::STATUS_CANCEL)
                                    <a href="#">canceled</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- change date time modal -->
        <div class="modal fade" id="changeDateTimeModal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Change Date Time</h4>
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>
                    <form>
                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label>Check In:</label>
                                        <input type="datetime-local" v-model="check_in" class="form-control" />
                                    </div>
                                    <div class="col-md-6">
                                        <label>Check Out:</label>
                                        <input type="datetime-local" v-model="check_out" class="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            <button type="button" @click.prevent="updateDateTime" class="btn btn-success">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- change date time modal -->
        <div class="modal fade" id="openCurrantSeatEditPopUp">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Change Seat Number</h4>
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>
                    <form>
                        <!-- Modal body -->
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label>Select Seat:</label>
                                        <select class="form-control" v-model="seat_id" @change="getSelectedValue($event)">
                                            <option v-for="(products , index) in office_products"
                                            :key="index"
                                            :value="products.number"
                                            >
                                                @{{ products.number }}
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Selected Seated :</label>
                                        <br>
                                        @{{ this.currant_seat }}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            <button type="button" @click.prevent="updateSeatNumber" class="btn btn-success">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection

@push('js')
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.11"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.5.1/vue-resource.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script>
        new Vue({
            el: '#ChangeBookingRequests',
            data: {
                clientIp: '{{\Request::getClientIp(true)}}',
                id:'',
                check_in:'',
                check_out:'',
                user_id:'',
                office_id:'',
                currant_seat:'',
                office_products:[],
                seat_id:'',
                change_booking_request_id:''
            },
            methods: {
                cancelChangeRequest(id,book_id){
                    console.log('id,book_id');
                    console.log(id,book_id);
                    let url = '/admin/cancel/change/booking/requests/'+id+'/'+book_id;
                    this.$http.get(url).then((response) => {
                        console.log('response');
                        console.log(response);
                        if(response.data.status == true) {
                            console.log('response.data.data');
                            console.log(response.data.message);
                            toastr.info(response.data.message)
                            setTimeout(function () {
                                window.location.reload();
                            },1000);
                        }
                    }).catch((error) => {
                       console.log('error');
                       console.log(error);
                    });
                },
                getSelectedValue: function(event) {
                    console.log('event.target.value');
                    console.log(event.target.value);
                    this.seat_id = event.target.value;
                },
                updateSeatNumber: function(){
                  console.log('this.seat_id && office_id');
                  console.log(this.seat_id);
                  console.log(this.office_id);
                  let url = '{{ route('backend.updateSeatNumber') }}';
                  let form = new FormData();
                  form.append('currant_seat',this.seat_id);
                  form.append('office_id',this.office_id);
                  form.append('change_booking_request_id',this.change_booking_request_id);
                  form.append('user_id',this.user_id);
                  form.append('selected_seat',this.currant_seat);
                  this.$http.post(url,form).then((response) => {
                      console.log('response');
                      console.log(response);
                      if(response.data.status == true) {
                          toastr.success(response.data.message);
                          setTimeout(function () {
                                window.location.reload();
                          },1000);
                      }
                  }).catch((error) => {
                     console.log('error');
                     console.log(error);
                  });
                },
                openCurrantSeatEditPopUp: function(currant_seat,office_id,user_id,id){
                    this.currant_seat = currant_seat;
                    this.office_id = office_id;
                    this.user_id = user_id;
                    this.change_booking_request_id = id;
                    $('#openCurrantSeatEditPopUp').modal('show');
                },
                getOfficeProduct: function(){
                    let office_id = '{{$id}}';
                    console.log('office_id');
                    console.log(office_id);
                    let url = '/admin/get/office/products/'+office_id;
                    this.$http.get(url).then((response) => {
                        console.log('response');
                        console.log(response);
                        if(response.data.status == true) {
                            console.log('response.data.data');
                            console.log(response.data.data);
                            this.office_products = response.data.data;
                        }
                    }).catch((error) => {
                       console.log('error');
                       console.log(error);
                    });
                },
                changeDateTime: function(id) {
                    this.id = id;
                    $('#changeDateTimeModal').modal('show');
                },
                updateDateTime: function(){
                  console.log(this.id);
                  console.log(this.check_in);
                  console.log(this.check_out);
                  let url = '{{route('backend.updateDateTime')}}';
                  let form = new FormData();
                  form.append('id',this.id);
                  form.append('check_in',this.check_in);
                  form.append('check_out',this.check_out);
                  this.$http.post(url,form).then((response) => {
                      console.log('response');
                      console.log(response);
                      if(response.data.status == true) {
                          toastr.success(response.data.message);
                          setTimeout(function () {
                              window.location.reload();
                          },1500);
                      }
                  }).catch((error) => {
                     console.log('error');
                     console.log(error);
                  });
                },
                confirmChange: function (id,url,bookId,checkIn,checkOut,seatId ,UserSeatId) {
                    console.log('url,bookId,checkIn,checkOut,seatId');
                    console.log(url,bookId,checkIn,checkOut,seatId);
                    let form = new FormData();
                    form.append('id',id);
                    form.append('book_id',bookId);
                    form.append('check_in',checkIn);
                    form.append('check_out',checkOut);
                    form.append('change_seat_id',seatId);
                    form.append('currant_seat_id',UserSeatId);
                    this.$http.post(url,form).then((response) => {
                       console.log('response');
                       console.log(response);
                       if(response.data.status == true) {
                           toastr.success(response.data.message);
                           setTimeout(function () {
                               window.location.reload();
                           },1500);
                       }
                    }).catch((error) => {
                        console.log('error');
                        console.log(error);
                    });
                }
            },

            mounted(){
                console.log('change booking requests data');
                this.getOfficeProduct();
            }
        });
    </script>
@endpush

