@extends('backend.layout')

@section('title')
    {{trans('sharedoffice.sharedoffice')}}&nbsp;<small>{{trans('common.b_request')}}</small>
@endsection

@section('description')

@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.sharedoffice')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedofficebookingrequest') }}">{{trans('sharedoffice.shared_office_request')}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <link href="/css/sharedOfficeRequest.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase">{{trans('sharedoffice.sharedoffice')}}</span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr role="row" class="heading">
                        <th>{{trans('sharedoffice.id')}}</th>
                        <th>{{trans('common.full_name')}}</th>
                        <th>{{trans('common.phone_no')}}</th>
                        <th>{{trans('common.check_in_date')}}</th>
                        <th>{{trans('common.check_out_date')}}</th>
                        <th>{{trans('common.no_of_rooms')}}</th>
                        <th>{{trans('common.no_of_peoples')}}</th>
                        <th>{{trans('common.remarks')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($bookingRequest as $requests)
                        <tr>
                            <td>{{$requests->id}}</td>
                            <td>{{$requests->full_name}}</td>
                            <td>{{$requests->phone_no}}</td>
                            <td>{{$requests->check_in}}</td>
                            <td>{{$requests->check_out}}</td>
                            <td>{{$requests->no_of_rooms}}</td>
                            <td>{{$requests->no_of_persons}}</td>
                            <td>{{$requests->remarks}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
