@extends('backend.layout')

@section('title')
    {{trans('sharedoffice.selectqr')}}
@endsection

@section('description')
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>

        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.sharedoffice')}}</a>
        </li>
    </ul>

@endsection

@section('header')

@endsection

@section('footer')
    <script>
        var url = "{{($id === null) ? route('backend.sharedoffice.products.dt')  : route('backend.sharedoffice.products.dt', ['id' => $id]) }}";
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#sharedoffice-dt'),
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": url,
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id', orderable: true, searchable: false},
                            //{data: 'image', name: 'image', orderable: false, searchable: false},
                            {data: 'office_id', name: 'office_id'},
                            {data: 'category_id', name: 'categories'},
                            {data: 'x', name: 'x'},
                            {data: 'y', name: 'y'},
                            {data: 'number', name: 'number'},
                            {data: 'qr', name: 'qr'},
                            //{data: 'out_qr', name: 'out_qr'},

                            {data: 'month_price', name: 'month_price'},
                            {data: 'time_price', name: 'time_price'},
                            {data: 'remark', name: 'remark'},
                            {data: 'status_id', name: 'status_id'},
                            {data: 'actions',width:'20%', name: 'actions', orderable: false, searchable: false}
                        ],
                        createdRow: function(row, data, index) {
                            //check its read
                            if(data.is_read == 0) {
                                $(row).css({'font-weight':'900'});
                            }
                            /*
                             if(data.status == 'waiting') {
                             $(row).addClass('warning');
                             }else if(data.status == 'decline'){
                             $(row).addClass('danger');
                             }
                             */
                        },
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
        <div class="QRcontainer">
            <div class="innerQRleft">
                <img class="QRimg" src="null"/> 
            </div> 
            <div class="innerQRright">
                <div class="btnswtich">
                    <p style="display:inline-block; font-weight:600; font-size:12pt; color:#a0a0a0">Public</p>
                    <label class="switch">
                        <input type="checkbox">
                        <span class="slider round"></span>
                    </label>
                    <div>
                        <button class="setdefaultbtn">Set As Default</button>
                    </div>
                  </div>
                  <p class="p1">Chat Room Name:</p>
                  <p class="p1">Chat Room Password:</p>                                 
                <p class="p2">Enterprise only wish to use Chatting module, with Using XENREN hourly office Manage service.</p>
            </div>
        </div>
        <br>

        <div class="QRcontainer">
            <div class="innerQRleft">               
                <img class="QRimg" src="null"/>              
            </div> 
            <div class="innerQRright">
                <div class="btnswtich">
                    <p style="display:inline-block; font-weight:600; font-size:12pt; color:#a0a0a0">Public</p>
                    <label class="switch">
                        <input type="checkbox">
                        <span class="slider round"></span>
                    </label>
                    <div>
                        <button class="setdefaultbtn">Set As Default</button>
                    </div>
                  </div>
                <p class="p1">Chat Room Name:</p>
                <p class="p1">Chat Room Password:</p>
                <p class="p2">Enterprise using the Hourly office Manage Service, and only need chat module. without using the seat Manage QR.</p>       
            </div>
        </div>
        <br>

        <div class="QRcontainer">
            <div class="innerQRleft">
                <img class="QRimg" src="null"/> 
            </div> 
            <div class="innerQRright">
                <div class="btnswtich">
                    <p style="display:inline-block; font-weight:600; font-size:12pt; color:#a0a0a0">Public</p>
                    <label class="switch">
                        <input type="checkbox">
                        <span class="slider round"></span>
                    </label>
                    <div>
                        <button class="setdefaultbtn">Set As Default</button>
                    </div>
                  </div>
                <p class="p1">Chat Room Name:</p>
                <p class="p1">Chat Room Password:</p>
                <p class="p2">Owner wish to user the Hourly Manage Service, the Office QR and Seat QR Setup.</p>
            </div>
        </div>
@endsection

@section('modal')

@endsection