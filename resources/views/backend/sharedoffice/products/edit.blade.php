@extends('backend.layout')

@section('title')
    {{trans('sharedoffice.editproduct')}}
@endsection

@section('description')
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>

            <a href="{{ route('backend.sharedoffice.products.index', ['id' => $sharedofficeproduct->office_id]) }}">{{trans('sharedoffice.sharedofficeproduct')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedoffice.products.create', ['id' => $id]) }}">{{trans('sharedoffice.editproduct')}}</a>
        </li>
    </ul>
@endsection

@section('description')

@endsection

@section('footer')
    <script>
        +function() {
            $(document).ready(function() {
                $('#sharedoffice-form').makeAjaxForm({
                    redirectTo: '{{ route('backend.sharedoffice.products.create', ['id' => $id]) }}',
                });

                $('#category_id').change(function () {
                    var category = $('#category_id').val()
                    $.ajax({
                        type : 'get',
                        url : window.location.origin + '/admin/sharedoffice/products/get-availability/'+ {{ $sharedofficeproduct->office_id }} +'/' + category,
                        success:function(res, textStatus, xhr){
                            if (xhr.status == 200) {
                                $('#availability').val(res.data.availability)
                            }
                        },
                        error:function(){
                            $('#availability').val(0)
                        }
                    });
                });
                $("#checkduplicate").click(function(){

                    var $off_id=$("#off_id").val();

                    var  $x=$("#x").val();
                    var  $y=$("#y").val();
                    /* check for duplicate data nenad femic 29 jan. 2018*/
                    $.ajax({
                        type : 'get',
                        url : '{{ route('backend.sharedoffice.products.checkpositionedit', ['id' => $id]) }}',
                        data:{
                            'x':$x,
                            'y':$y,
                            'offid':$off_id
                        },
                        async: false,
                        success:function(data2){
                            //alert(data2);
                            if(data2=="duplicate"){
                                $('#msg2').html("<font color='red' size='3'>Position already have in database x= "+$x+" y= "+$y+"</font>");
                                return false;
                            }
                            else{
                                self.submit();
                            }
                        }
                    });
                    return false;
                });
                function calculate(hourly_price) {
                    return (hourly_price / 60) * 10;
                }
                $('#hourly_price').on('keyup', function () {
                    var val = $(this).val();
                    $('.time_price').val(calculate(val).toFixed(2));
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
{{-- {{ dd(get_defined_vars()) }} --}}
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-helper">{{trans('sharedoffice.editproduct')}}</span>
            </div>
            <div class="actions">

            </div>
        </div>
        <div>
            <h3 class=""><span >{{trans('sharedoffice.officename')}}:</span> <span class="badge badge-info">{{ $sharedofficeproduct->office->office_name }}</span> </h3>
            <h3 class=""><span >{{trans('sharedoffice.location')}}:</span>  <span class="badge badge-info">{{ $sharedofficeproduct->office->location }}</span></h3>
            <h3 class=""><span >{{trans('sharedoffice.productno')}}:</span>  <span class="badge badge-info">{{ $sharedofficeproduct->number }}</span></h3>

        </div>
        <div class="portlet-body form">
            {!! Form::open(['method' => 'post', 'role' => 'form']) !!}
            <div class="form-body">

                <input type="hidden" id="off_id" value="{{$sharedofficeproduct->office_id}}">
                <div class="form-group">
                    {{ Form::hidden('office_id', $sharedofficeproduct->office_id) }}
                    {{ Form::label('category_id', trans('sharedoffice.categories'), ['class' => 'form-spacing-top']) }}
                    {{ Form::select('category_id', $categories, $sharedofficeproduct->category_id, ['class' => 'form-control', 'id' => 'category_id', 'value' => 0]) }}
                </div>

                <div class="form-group col-md-6">
                    {!! Form::label('availability','Availability',['class'=>'control-label']) !!}
                    <input class="form-control" id="availability" name="availability" type="text" value="{{ $sharedofficeproduct->availability ? $sharedofficeproduct->availability->availability : 0 }}">
                </div>

                <div class="form-group col-md-6">
                    {!! Form::label('no_of_peoples','No. of Peoples',['class'=>'control-label']) !!}
                    {!! Form::text('no_of_peoples',$sharedofficeproduct->no_of_peoples,['class'=>'form-control','required' => '', 'placeholder' => 'Enter Quantity of People Booking']) !!}
                </div>

                <div class="form-group col-md-6">
                    {!! Form::label('month_price',trans('sharedoffice.month_price'),['class'=>'control-label']) !!}
                    {!! Form::text('month_price',$sharedofficeproduct->month_price,['class'=>'form-control','required' => '', 'placeholder' => 'Enter Month Price']) !!}
                </div>

                <div class="form-group col-md-6">
                    {!! Form::label('weekly_price',trans('sharedoffice.weekly_price'),['class'=>'control-label']) !!}
                    {!! Form::text('weekly_price',$sharedofficeproduct->month_price,['class'=>'form-control','required' => '', 'placeholder' => 'Enter Weekly Price']) !!}
                </div>

                <div class="form-group col-md-6">
                    {!! Form::label('hourly_price','Hourly price',['class'=>'control-label']) !!}
                    {!! Form::text('hourly_price',$sharedofficeproduct->hourly_price,['class'=>'form-control','required' => '', 'placeholder' => 'Enter Hourly Price', 'id' => 'hourly_price']) !!}
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('time_price',trans('sharedoffice.time_price'),['class'=>'control-label']) !!}
                    {!! Form::text('time_price',$sharedofficeproduct->time_price,['class'=>'form-control time_price','required' => '', 'placeholder' => 'Enter Time Price', 'id' => 'time_price']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('number',trans('sharedoffice.number'),['class'=>'control-label']) !!}
                    {!! Form::text('number',$sharedofficeproduct->number,['class'=>'form-control','required' => '', 'placeholder' => 'Enter Number']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('remark',trans('sharedoffice.remark'),['class'=>'control-label']) !!}
                    {!! Form::text('remark',$sharedofficeproduct->remark,['class'=>'form-control','required' => '', 'placeholder' => 'Enter Remark']) !!}
                </div>

                <!--
                <div class="form-group">
                    {!! Form::label('x','x',['class'=>'control-label']) !!}
                    {!! Form::text('x',$sharedofficeproduct->x,['class'=>'form-control','required' => '', 'placeholder' => 'Enter x','id' => 'x']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('y','y',['class'=>'control-label']) !!}
                    {!! Form::text('y',$sharedofficeproduct->y,['class'=>'form-control','required' => '', 'placeholder' => 'Enter y' ,'id' => 'y']) !!}
                </div>
                -->

                <div class="form-group">
                    {!! Form::label('List using positions','Currently used coordinates:',['class'=>'control-label']) !!}
                    @foreach($listproducts as $coordinate)
                        x={{ $coordinate->x }}, y={{ $coordinate->y }}
                        <span><b1>|</b1></span>
                    @endforeach
                </div>

                <div class="form-group">
                    {!! Form::label('Position X','Position X',['class'=>'control-label']) !!}
                    <input type="text" class="form-control" name="x" id="x" value="{{$sharedofficeproduct->x}}">
                    {{--<select id="x" class="form-control" name="x">--}}
                        {{--@for($i=0;$i<$sharedofficeproduct->office->office_size_x ;$i++)--}}
                            {{--@if($i==$sharedofficeproduct->x)--}}
                                {{--<option selected value='{{ $i }}'>{{ $i }}</option>--}}
                            {{--@else--}}
                                {{--<option value='{{ $i }}'>{{ $i }}</option>--}}
                            {{--@endif--}}
                        {{--@endfor--}}
                    {{--</select>--}}
                </div>

                <div class="form-group">
                    {!! Form::label('Position Y','Position Y',['class'=>'control-label']) !!}
                    <input type="text" id="y" class="form-control" name="y" value="{{$sharedofficeproduct->y}}">
                    {{--<select id="y" class="form-control" name="y">--}}
                        {{--@for($j=0;$j<$sharedofficeproduct->office->office_size_y ;$j++)--}}
                            {{--@if($j==$sharedofficeproduct->y)--}}
                                {{--<option selected value='{{ $j }}'>{{ $j }}</option>--}}
                            {{--@else--}}
                                {{--<option value='{{ $j }}'>{{ $j }}</option>--}}
                            {{--@endif--}}
                        {{--@endfor--}}
                    {{--</select>--}}
                </div>

                <div id="msg2"></div>

                {{ Form::label('status_id', trans('sharedoffice.status'), ['class' => 'form-spacing-top']) }}
                {{ Form::select('status_id', $status, $sharedofficeproduct->status_id, ['class' => 'form-control']) }}


                <div class="form-actions">
                    <button id="checkduplicate" type="submit" class="btn blue">{{trans('sharedoffice.savechanges')}}</button>
                    {!! Html::linkRoute('backend.sharedoffice.products.index', trans('sharedoffice.cancel'), array($sharedofficeproduct->office_id), array('class' => 'btn btn-danger')) !!}
                </div>

                {!! Form::close() !!}

            </div>
        </div>
@endsection

@section('modal')

@endsection
