@extends('backend.layout')

@section('title')
    {{trans('sharedoffice.create_product')}}
@endsection

@section('description')
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedoffice.products.index', ['id' => $sharedoffice->id]) }}">{{trans('sharedoffice.sharedofficeproduct')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedoffice.products.create', ['id' => $id]) }}">{{trans('sharedoffice.create_product')}}</a>
        </li>
    </ul>
@endsection

@section('description')

@endsection

@section('footer')
    <script>
        +function() {
            $(document).ready(function() {
                $('#sharedoffice-form').makeAjaxForm({
                    redirectTo: '{{ route('backend.sharedoffice.products.create', ['id' => $id]) }}',
                });
                $('#category_id').change(function () {
                    var category = $('#category_id').val()
                    $.ajax({
                        type : 'get',
                        url : window.location.origin + '/admin/sharedoffice/products/get-availability/'+ {{ $id }} +'/' + category,
                        success:function(res, textStatus, xhr){
                            if (xhr.status == 200) {
                                console.log('res.data.availability');
                                console.log(res.data);
                                $('#availability').val(res.data.availability)
                            }
                        },
                        error:function(){
                            $('#availability').val(0)
                        }
                    });
                });
                $("#checkduplicate").click(function(){
                    //alert({{$id}});
                    var  $x=$("#x").val();
                    var  $y=$("#y").val();
                    /* check for duplicate data nenad femic 29 jan. 2018*/
                    $.ajax({
                        type : 'get',
                        url : '{{ route('backend.sharedoffice.products.checkposition', ['id' => $id]) }}',
                        data:{
                            'x':$x,
                            'y':$y
                        },
                        async: false,
                        success:function(data2){
                            if(data2=="duplicate"){
                                $('#msg2').html("<font color='red' size='3'>Position already have in database x= "+$x+" y= "+$y+"</font>");
                                return false;
                            }
                            else{
                                self.submit();
                            }
                        }
                    });
                    return false;
                });

                $('#availability').on('keyup', function () {
                });
                $('#no_of_peoples').on('keyup', function () {
                    let no_of_people = $('#no_of_peoples').val();
                    let availability = $('#availability').val();
                    console.log('no_of_people');
                    console.log(no_of_people);
                    console.log('availability');
                    console.log(availability);
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="row">
         <div class="col-md-12">
             @if(session()->has('danger'))
                <div class="alert alert-danger">
                    {{session()->get('danger')}}
                </div>
             @endif
         </div>
    </div>
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-helper">{{trans('sharedoffice.msg3')}}</span>
            </div>
            <div class="actions">

            </div>
        </div>
        <div>
        <h3 class=""><span > {{trans('sharedoffice.officename')}}:</span> <span class="badge badge-info">{{ $sharedoffice->office_name }}</span> </h3>
            <h3 class=""><span > {{trans('sharedoffice.location')}}:</span>  <span class="badge badge-info">{{ $sharedoffice->location }}</span></h3>
        </div>
        <div class="portlet-body form">
            {!! Form::open(['method' => 'post', 'role' => 'form']) !!}
            <div class="form-body">

                <div class="form-group">
                    {{ Form::hidden('office_id', $sharedoffice->id) }}

                    {{ Form::label('category_id', trans('sharedoffice.categories')) }}
                    <select class="form-control" name="category_id" id="category_id">
                        <option value="">Select Category</option>
                        @if(Session::get('lang')=="en")
                            @foreach($categories as $cat)
                                <option value='{{ $cat->id }}'>{{ $cat->name }}</option>
                            @endforeach
                        @elseif(Session::get('lang')=="cn")
                            @foreach($categories as $cat)
                                <option value='{{ $cat->id }}'>{{ $cat->name_ch }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>

                <div class="form-group col-md-6">
                    {!! Form::label('availability','Availability',['class'=>'control-label']) !!}
                    {!! Form::text('availability',Input::old('availability'),['class'=>'form-control']) !!}
                </div>

                <div class="form-group col-md-6">
                    {!! Form::label('no_of_peoples','No. of Peoples',['class'=>'control-label']) !!}
                    {!! Form::text('no_of_peoples',Input::old('no_of_peoples'),['class'=>'form-control','required' => '', 'placeholder' => 'Enter Quantity of People Booking']) !!}
                </div>

                <div class="form-group col-md-6">
                    {!! Form::label('month_price',trans('sharedoffice.month_price'),['class'=>'control-label']) !!}
                    {!! Form::text('month_price',Input::old('month_price'),['class'=>'form-control','required' => '', 'placeholder' => trans('sharedoffice.entermonthprice')]) !!}
                </div>

                <div class="form-group col-md-6">
                    {!! Form::label('weekly_price',trans('sharedoffice.weekly_price'),['class'=>'control-label']) !!}
                    {!! Form::text('weekly_price',Input::old('weekly_price'),['class'=>'form-control','required' => '', 'placeholder' => 'Enter Weekly Price']) !!}
                </div>

                <div class="form-group col-md-6">
                    {!! Form::label('hourly_price','Hourly price',['class'=>'control-label']) !!}
                    {!! Form::text('hourly_price',Input::old('hourly_price'),['class'=>'form-control','required' => '', 'placeholder' => 'Enter Hourly Price', 'id' => 'hourly_price']) !!}
                </div>

                <div class="form-group col-md-6">
                    {!! Form::label('time_price',trans('sharedoffice.time_price'),['class'=>'control-label']) !!}
                    {!! Form::text('time_price',Input::old('time_price'),['class'=>'form-control','required' => '', 'placeholder' => trans('sharedoffice.entertimeprice')]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('remark',trans('sharedoffice.remark'),['class'=>'control-label']) !!}
                    {!! Form::text('remark',Input::old('remark'),['class'=>'form-control','required' => '', 'placeholder' => trans('sharedoffice.enterremark')]) !!}
                </div>
<!--
                <div class="form-group">
                    {!! Form::label('Position x','Position x',['class'=>'control-label']) !!}
                    {!! Form::text('x','',['class'=>'form-control', 'placeholder' => 'Enter position x']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Position y','Position y',['class'=>'control-label']) !!}
                    {!! Form::text('y','',['class'=>'form-control', 'placeholder' => 'Enter position y']) !!}
                </div>
-->

                <div class="form-group">
                    {!! Form::label('List using positions','Currently used coordinates:',['class'=>'control-label']) !!}
                    @foreach($listproducts as $coordinate)
                       x={{ $coordinate->x }}, y={{ $coordinate->y }}
                        <span><b1>|</b1></span>
                    @endforeach
                </div>

                <div class="form-group">
                    {!! Form::label('Position X','Position X',['class'=>'control-label']) !!}
                    <input type="text" class="form-control" name="x" id="x" >
                    {{--<select id="x" class="form-control" name="x">--}}
                        {{--@for($i=0;$i<$sharedoffice->office_size_x ;$i++)--}}
                            {{--@if($i =="0")--}}
                            {{--<option selected value='{{ $i }}'>{{ $i }}</option>--}}
                            {{--@else--}}
                            {{--<option value='{{ $i }}'>{{ $i }}</option>--}}
                            {{--@endif--}}
                        {{--@endfor--}}
                    {{--</select>--}}
                </div>

                <div class="form-group">
                    {!! Form::label('Position Y','Position Y',['class'=>'control-label']) !!}
                    <input type="text" id="y" class="form-control" name="y" >
                    {{--<select id="y" class="form-control" name="y">--}}
                        {{--@for($j=0;$j<$sharedoffice->office_size_y ;$j++)--}}
                            {{--@if($j =="0")--}}
                                {{--<option selected value='{{ $j }}'>{{ $j }}</option>--}}
                            {{--@else--}}
                                {{--<option value='{{ $j }}'>{{ $j }}</option>--}}
                            {{--@endif--}}
                        {{--@endfor--}}
                    {{--</select>--}}
                </div>


                <div id="msg2"></div>





                {{ Form::label('status_id', trans('sharedoffice.status')) }}
                <select class="form-control" name="status">
                    @foreach($status as $st)
                        @if($st->id =="3") <!-- 3 is empty  -->
                            @if(Session::get('lang')=="en")
                                <option value='{{ $st->id }}'>{{ $st->status }}</option>
                            @elseif(Session::get('lang')=="cn")
                                <option value='{{ $st->id }}'>{{ $st->status_ch }}</option>
                            @endif
                        @endif
                    @endforeach
                </select>

                <div class="form-actions">
                    <button id="checkduplicate" type="submit" class="btn blue">{{trans('sharedoffice.saveproduct')}}</button>
                    {!! Html::linkRoute('backend.sharedoffice.products.index', trans('sharedoffice.cancel'), array($sharedoffice->id), array('class' => 'btn btn-danger')) !!}

                </div>

                {!! Form::close() !!}

                </div>
            </div>
@endsection

@section('modal')

@endsection
