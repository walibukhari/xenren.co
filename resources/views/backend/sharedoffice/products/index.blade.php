@extends('backend.layout')

@section('title')
    {{trans('sharedoffice.sharedofficeproduct')}}
@endsection

@section('description')
    {{trans('sharedoffice.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>

        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.sharedoffice')}}</a>
        </li>
    </ul>

@endsection

@section('header')
    <style>
        .img-thumbnail {
            border: 0;
        }
    </style>
@endsection

@section('footer')
    <script>
        var url = "{{($id === null) ? route('backend.sharedoffice.products.dt')  : route('backend.sharedoffice.products.dt', ['id' => $id]) }}";
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#sharedoffice-dt'),
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": url,
                            "type": "GET"
                        },
                        columns: [
                           {data: 'id', name: 'id', orderable: true, searchable: false},
                            //{data: 'image', name: 'image', orderable: false, searchable: false},
                            {data: 'office_id', name: 'office_id'},
                            {data: 'category_id', name: 'categories'},
                            {data: 'x', name: 'x'},
                            {data: 'y', name: 'y'},
                            {data: 'number', name: 'number'},
                            {data: 'qr', name: 'qr'},
                           //{data: 'out_qr', name: 'out_qr'},

                            {data: 'month_price', name: 'month_price'},
                            {data: 'time_price', name: 'time_price'},
                            {data: 'remark', name: 'remark'},
                            {data: 'status_id', name: 'status_id'},
                            {data: 'actions',width:'20%', name: 'actions', orderable: false, searchable: false}
                        ],
                        createdRow: function(row, data, index) {
                            //check its read
                            if(data.is_read == 0) {
                                $(row).css({'font-weight':'900'});
                            }
                            /*
                             if(data.status == 'waiting') {
                             $(row).addClass('warning');
                             }else if(data.status == 'decline'){
                             $(row).addClass('danger');
                             }
                             */
                        },
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('sharedoffice.sharedofficeproduct')}}</span>
            </div>

            <div class="actions">
                <a href="{{ route('backend.sharedoffice.products.create', ['id' => $id]) }}" class="btn btn-circle red-sunglo btn-sm"><i
                            class="fa fa-plus"></i> {{trans('sharedoffice.add_sharedproduct')}}</a>
            </div>
        </div>
        <div class="actions">
            <a href="{{ route('backend.sharedoffice.finance.showfinancetable', ['id' => $id]) }}" class="btn-finance">
                <span style="color: #25ce0f;font-size: 13pt; font-weight: bold">&#65284</span>{{trans('sharedoffice.finance')}}</a>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                @if(Session::has('msgProductUsing')) <div class="alert alert-info"> {{Session::get('msgProductUsing')}} </div> @endif
                @if(Session::has('msgRentingSave')) <div class="alert alert-info"> {{Session::get('msgRentingSave')}} </div> @endif
                @if(Session::has('msgRentingMonthSave')) <div class="alert alert-info"> {{Session::get('msgRentingMonthSave')}} </div> @endif
                @if(Session::has('msgProductCreate')) <div class="alert alert-info"> {{Session::get('msgProductCreate')}} </div> @endif
                @if(Session::has('msgFinance')) <div class="alert alert-info"> {{Session::get('msgFinance')}} </div> @endif
                @if(Session::has('msgProductUpdate')) <div class="alert alert-info"> {{Session::get('msgProductUpdate')}} </div> @endif
                @if(Session::has('msgFinanceTableNull')) <div class="alert alert-info"> {{Session::get('msgFinanceTableNull')}} </div> @endif
                @if(Session::has('msgDanger')) <div class="alert alert-danger"> {{Session::get('msgDanger')}} </div>
                @endif

                <table class="table table-striped table-bordered table-hover table-checkable" id="sharedoffice-dt">
                    <thead>
                    <tr role="row" class="heading" style="color: #25ce0f !important;">
                        <th width="5%">{{trans('common.id')}}</th>
                        <th width="5%">{{trans('sharedoffice.office_id')}}</th>
                        <th width="6%">{{trans('sharedoffice.categories')}}</th>
                        <th width="6%">x</th>
                        <th width="6%">y</th>
                        <th>{{trans('sharedoffice.number')}}</th>
                        <th width="13%">{{trans('sharedoffice.qr_in')}}</th>
                        <th width="10%">{{trans('sharedoffice.month_price')}}</th>
                        <th width="10%">{{trans('sharedoffice.time_price')}}</th>
                        <th width="10%">{{trans('sharedoffice.remark')}}</th>
                        <th width="10%">{{trans('sharedoffice.status')}}</th>
                        <th width="15%">{{trans('sharedoffice.action')}}</th>
                    </tr>
                    <!-- FILTER -->
                    <tr role="row" class="filter">
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_id" placeholder="ID">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_officeid" placeholder="Office ID">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_categories" placeholder="Categories">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_x" placeholder="x">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_y" placeholder="y">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_fullname" placeholder="Fullname">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_fullname" placeholder="Fullname">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_fullname" placeholder="Fullname">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_fullname" placeholder="Fullname">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_fullname" placeholder="Fullname">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_fullname" placeholder="Fullname">
                        </td>


                        <td>
                            <div class="margin-bottom-5" style="text-align: center;">
                                <button class="btn-filter-submit">
                                    <i class="fa fa-search"></i>
                                </button>
                                <span style="display: inline-block; font-size: 15pt; font-weight: 200">|</span>
                                <button class="btn-filter-cancel">
                                    &#8634;
                                </button>
                            </div>
                        </td>
                    </tr>
                    <!-- END FILTER  -->
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection
