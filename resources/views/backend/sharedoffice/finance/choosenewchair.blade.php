@extends('backend.layout')

@section('title')
    Change chair
@endsection

@section('description')
    {{trans('sharedoffice.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedoffice.finance.showfinancetable', ['id' => $sharedoffice->id]) }}">{{trans('sharedoffice.finance_table')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            Choose other seat
        </li>
    </ul>
@endsection

@section('description')

@endsection

@section('footer')
    <script>
        +function() {
            $(document).ready(function() {
                $('#sharedoffice-form').makeAjaxForm({
                    redirectTo: '{{ route('backend.sharedoffice.products.create', ['id' => $id]) }}',
                });
                $("#checkbusy").click(function(){
                    var  $x=$("#x").val();
                    var  $y=$("#y").val();
                    var $off_id=$("#off_id").val();
                    $.ajax({
                        type : 'get',
                        url : '{{ route('backend.sharedoffice.products.checkbusy', ['id' => $id]) }}',
                        data:{
                            'x':$x,
                            'y':$y,
                            'offid':$off_id
                        },
                        async: false,
                        success:function(data2){
                            if(data2=="busy"){
                                $('#msg2').html("<font color='red' size='3'>Position already busy in database x= "+$x+" y= "+$y+"</font>");
                                return false;
                            }
                            else{
                                self.submit();
                            }
                        }
                    });
                    return false;
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">

        <div>
            <h3 class=""><span > Office_name:</span> <span >{{ $sharedoffice->office_name }}</span> </h3>
            <h3 class=""><span > Old chair:</span> <span >{{ $sharedofficefinance->product_id }}</span> </h3>
            <h3 class=""><span > Old coordinate:</span> <span >{{ $sharedofficefinance->x }},{{ $sharedofficefinance->y }}</span> </h3>
            <h3 class=""><span > Product number(QR num):</span>  <span>{{ $sharedofficefinance->productfinance->number }}</span></h3>
        </div>
        <div class="portlet-body form">
            {!! Form::open(['method' => 'post', 'role' => 'form']) !!}
            <div class="form-body">


                <div class="form-group">
                    <input type="hidden" id="off_id" name="off_id" value="{{$sharedoffice->id}}">
                    <input type="hidden" id="product_id" name="product_id" value="{{$sharedofficefinance->product_id }}">
                    <input type="hidden" id="number" name="number value="{{$sharedofficefinance->productfinance->number }}">
                    {!! Form::label('List free positions','Free positions for seats:',['class'=>'control-label']) !!}
                    @foreach($listproducts as $coordinate)
                        x={{ $coordinate->x }}, y={{ $coordinate->y }}
                        <span><b1>|</b1></span>
                    @endforeach
                </div>

                <div class="form-group">
                    {!! Form::label('Position X','Position X',['class'=>'control-label']) !!}
                    <select id="x" class="form-control" name="x">
                        @for($i=0;$i<$sharedoffice->office_size_x ;$i++)
                            @if($i ==$sharedofficefinance->x)
                                <option selected value='{{ $i }}'>{{ $i }}</option>
                            @else
                                <option value='{{ $i }}'>{{ $i }}</option>
                            @endif
                        @endfor
                    </select>
                </div>

                <div class="form-group">
                    {!! Form::label('Position Y','Position Y',['class'=>'control-label']) !!}
                    <select id="y" class="form-control" name="y">
                        @for($j=0;$j<$sharedoffice->office_size_y ;$j++)
                            @if($j ==$sharedofficefinance->y)
                                <option selected value='{{ $j }}'>{{ $j }}</option>
                            @else
                                <option value='{{ $j }}'>{{ $j }}</option>
                            @endif
                        @endfor
                    </select>
                </div>

                <div id="msg2"></div>

                <div class="form-actions">
                    <button id="checkbusy" type="submit" class="btn blue">Save new chair</button>
                    {!! Html::linkRoute('backend.sharedoffice.products.index', trans('sharedoffice.cancel'), array($sharedoffice->id), array('class' => 'btn btn-danger')) !!}

                </div>
                {!! Form::close() !!}

            </div>
        </div>
@endsection

@section('modal')

@endsection