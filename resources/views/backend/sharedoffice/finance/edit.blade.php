
@extends('backend.layout')

@section('description')
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>

            <a href="{{ route('backend.sharedoffice.products.index', ['id' => $sharedofficefinance ->productfinance->office->id]) }}">{{trans('sharedoffice.sharedofficeproduct')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>

            <a href="{{ route('backend.sharedoffice.finance.showfinancetable', ['id' => $sharedofficefinance ->productfinance->office->id]) }}">{{trans('sharedoffice.finance')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            {{trans('sharedoffice.confirmpayment')}}
        </li>
    </ul>
@endsection

@section('description')

@endsection

@section('footer')
    <script>
        +function() {
            $(document).ready(function() {
                $('#sharedoffice-form').makeAjaxForm({
                    redirectTo: '{{ route('backend.sharedoffice.finance.editfinance', ['id' => $sharedofficefinance->id]) }}',
                });
            });
        }(jQuery);
    </script>

@endsection

@section('content')
    <div class="portlet light bordered">

        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h1 class="display-3">{{trans('sharedoffice.user')}}:<span style="color:red">{{  $sharedofficefinance ->user }}</span>  {{trans('sharedoffice.cost')}}:<span style="color:red">${{  $cost }}</span>  </h1>

            </div>
        </div>

        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">{{trans('sharedoffice.officename')}}:</th>
                <th scope="col">{{trans('sharedoffice.location')}}:</th>
                <th scope="col">{{trans('sharedoffice.categories')}}:</th>
                <th scope="col">{{trans('sharedoffice.productno')}}</th>
                <th scope="col">{{trans('sharedoffice.start_time')}}</th>
                <th scope="col">{{trans('sharedoffice.end_time')}}</th>
                <th scope="col">{{trans('sharedoffice.time_price')}}</th>
                <th scope="col">{{trans('sharedoffice.user')}}</th>
                <th scope="col">{{trans('sharedoffice.currentstatus')}}</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">{{$sharedofficefinance ->productfinance->office->office_name }}</th>
                <td>{{$sharedofficefinance ->productfinance->office->location }}</td>
                <td>
                    @if(Session::get('lang')=="en")
                        {{$sharedofficefinance->productfinance->productcategory->name }}
                    @elseif(Session::get('lang')=="cn")
                        {{$sharedofficefinance->productfinance->productcategory->name_ch }}
                    @else
                        {{$sharedofficefinance->productfinance->productcategory->name }}
                    @endif
                </td>
                <td>{{ $sharedofficefinance->productfinance->number }}</td>
                <td>{{ $sharedofficefinance->start_time }}</td>
                <td>{{ \Carbon\Carbon::now() }}</td>
                <td>{{  $sharedofficefinance ->productfinance->time_price }}</td>
                <td>{{  $sharedofficefinance ->user }}</td>
                <td>
                    @if(Session::get('lang')=="en")
                        {{ $sharedofficefinance->productfinance->productstatus->status }}
                    @elseif(Session::get('lang')=="cn")
                        {{ $sharedofficefinance->productfinance->productstatus->status_ch }}
                    @else
                        {{ $sharedofficefinance->productfinance->productstatus->status }}
                    @endif
                </td>
            </tr>

            </tbody>
        </table>

        <div class="portlet-body form">
            {!! Form::open(['method' => 'post', 'role' => 'form']) !!}
            <div class="form-body">

                <div class="form-group" style="margin-top:0px;">
                    {!! Form::label('status',trans('sharedoffice.status'),['class'=>'control-label']) !!}
                    {!! Form::text('status','',['class'=>'form-control','required' => '', 'placeholder' => trans('sharedoffice.enterstatus')]) !!}
                </div>
                <div class="form-group">
                    {{ Form::hidden('id', $sharedofficefinance->id) }}
                    {{ Form::hidden('cost', $cost) }}
                    {{ Form::hidden('endtime', \Carbon\Carbon::now()) }}
                    {{ Form::hidden('product_id', $sharedofficefinance->product_id) }}
                </div>

                <div class="form-actions">
                    <button type="submit" class="btn blue">{{trans('sharedoffice.savechanges')}}</button>
                    {!! Html::linkRoute('backend.sharedoffice.finance.showfinancetable', trans('sharedoffice.cancel'), array($sharedofficefinance ->productfinance->office->id), array('class' => 'btn btn-danger')) !!}
                </div>



                {!! Form::close() !!}

            </div>
        </div>
@endsection

@section('modal')

@endsection
