@extends('backend.layout')

@section('title')
    {{trans('sharedoffice.finance_table')}}
@endsection

@section('description')
    {{trans('sharedoffice.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>

            <a href="{{ route('backend.sharedoffice.products.index', ['id' => $id]) }}">{{trans('sharedoffice.sharedofficeproduct')}}</a>
            <i class="fa fa-circle"></i>
        </li>

        <li>
            {{trans('sharedoffice.finance')}}
        </li>
    </ul>

@endsection

@section('header')

@endsection

@section('footer')
    <script>
        var url = "{{($id === null) ? route('backend.sharedoffice.finance.showfinancetable.dt')  : route('backend.sharedoffice.finance.showfinancetable.dt', ['id' => $id]) }}";
        +function() {
            $(document).ready(function() {
                setInterval(function () {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#sharedoffice-dtt'),
                    dataTable: {
                        "ajax": {
                            "url": url,
                            "type": "GET"
                        },
                        columns: [
                            {data: 'id', name: 'id', orderable: true, searchable: false},
                            {data: 'product_id', name: 'product_id'},
                            {data: 'x', name: 'x'},
                            {data: 'y', name: 'y'},
                            {data: 'number', name: 'number'},
                            {data: 'office_id', name: 'office_id'},
                            {data: 'category_id', name: 'categories'},
                            {data: 'start_time', name: 'start_time'},
                            {data: 'currentTime', name: 'currentTime'},
                            {data: 'end_time', name: 'end_time'},
                            //{data: 'month_price', name: 'month_price'},
                            {data: 'time_price', name: 'time_price'},
                            {data: 'minForPayment', name: 'minForPayment'},
                            {data: 'cost', name: 'cost'},
                            {data: 'user', name: 'user'},
                            {data: 'status', name: 'status'},
                            {data: 'paymentComment', name: 'paymentComment'},
                            {data: 'actions',width:'20%', name: 'actions', orderable: false, searchable: false}
                        ],
                        createdRow: function(row, data, index) {
                            //check its read
                            if(data.is_read == 0) {
                                $(row).css({'font-weight':'900'});
                            }
                            /*
                             if(data.status == 'waiting') {
                             $(row).addClass('warning');
                             }else if(data.status == 'decline'){
                             $(row).addClass('danger');
                             }
                             */
                        },
                    }
                });
            });
            }, 60000); // milliseconds
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase">  {{trans('sharedoffice.finance')}}</span>
            </div>


        </div>

        <div class="portlet-body">
            <div class="table-container">
                @if(Session::has('msgProductUsing')) <div class="alert alert-info"> {{Session::get('msgProductUsing')}} </div> @endif
                @if(Session::has('msgRentingSave')) <div class="alert alert-info"> {{Session::get('msgRentingSave')}} </div> @endif
                @if(Session::has('msgRentingMonthSave')) <div class="alert alert-info"> {{Session::get('msgRentingMonthSave')}} </div> @endif
                @if(Session::has('msgProductCreate')) <div class="alert alert-info"> {{Session::get('msgProductCreate')}} </div> @endif
                @if(Session::has('msgFinance')) <div class="alert alert-info"> {{Session::get('msgFinance')}} </div> @endif
                @if(Session::has('msgProductUpdate')) <div class="alert alert-info"> {{Session::get('msgProductUpdate')}} </div> @endif
                @if(Session::has('msgFinanceClose')) <div class="alert alert-info"> {{Session::get('msgFinanceClose')}} </div> @endif

                    <table class="table table-striped table-bordered table-hover table-checkable" id="sharedoffice-dtt">
                    <thead>
                    <tr role="row" class="heading">
                        <th>{{trans('common.id')}}</th>
                        <th>Product_id</th>
                        <th>x</th>
                        <th>y</th>
                        <th>No</th>
                        <th>{{trans('sharedoffice.officename')}}</th>
                        <th>{{trans('sharedoffice.categories')}}</th>
                        <th>{{trans('sharedoffice.start_time')}}</th>
                        <th>{{trans('sharedoffice.current_time')}}</th>
                        <th>{{trans('sharedoffice.end_time')}}</th>
                        <!--<th>Month price</th>-->
                        <th>{{trans('sharedoffice.time_price')}}<br>{{trans('sharedoffice.by_min')}}</th>
                        <th>{{trans('sharedoffice.payment_by_minute')}}<br>{{trans('sharedoffice.for_now')}}</th>
                        <th>{{trans('sharedoffice.cost')}}</th>
                        <th>{{trans('sharedoffice.user')}}</th>
                        <th>{{trans('sharedoffice.status')}}</th>
                        <th>{{trans('sharedoffice.payment_comment')}}</th>
                        <th>{{trans('sharedoffice.action')}}</th>
                    </tr>
                    <!--
                    <tr role="row" class="filter">
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_email" placeholder="Username">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_fullname" placeholder="Fullname">
                        </td>
                        <td>
                            <div class="input-group margin-bottom-5">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly name="filter_created_after" placeholder="From">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                            <div class="input-group">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly name="filter_created_before" placeholder="To">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                        </td>
                        <td>
                            <div class="input-group margin-bottom-5">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly name="filter_updated_after" placeholder="From">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                            <div class="input-group">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly name="filter_updated_before" placeholder="To">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                        </td>
                        <td>
                            <div class="margin-bottom-5">
                                <button class="btn btn-info-alt filter-submit">
                                    <i class="fa fa-search"></i> 过滤
                                </button>
                            </div>
                            <button class="btn btn-danger-alt filter-cancel">
                                <i class="fa fa-times"></i> 重置
                            </button>
                        </td>
                    </tr>
                    -->
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection