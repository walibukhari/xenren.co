@extends('backend.layout')

@section('title')
    Finance
@endsection

@section('description')
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>

            <a href="{{ route('backend.sharedoffice.products.index', ['id' => $sharedofficeproduct->office_id]) }}">{{trans('sharedoffice.sharedofficeproduct')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedoffice.products.create', ['id' => $id]) }}">Finance</a>
        </li>
    </ul>
@endsection

@section('description')

@endsection

@section('footer')
    <script>
        +function() {
            $(document).ready(function() {
                $('#sharedoffice-form').makeAjaxForm({
                    redirectTo: '{{ route('backend.sharedoffice.products.create', ['id' => $id]) }}',
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-helper">Fill data</span>
            </div>
            <div class="actions">
                @if ($id === null)
                    <a href="{{ route('backend.category.create') }}" class="btn btn-circle red-sunglo btn-sm" data-target="#remote-modal" data-toggle="modal"><i class="fa fa-plus"></i> 新增类别</a>
                @else
                    <a href="{{ route('backend.category.create', ['id' => $id]) }}" class="btn btn-circle red-sunglo btn-sm" data-target="#remote-modal" data-toggle="modal"><i class="fa fa-plus"></i> 新增类别</a>
                @endif
            </div>
        </div>
        <div>
            <h3 class="">
                <span >Office Name:</span> <span class="badge badge-info">{{ $sharedofficeproduct->office->office_name }}</span>
                <span >Location:</span>  <span class="badge badge-info">{{ $sharedofficeproduct->office->location }}</span>
                <span >Product ID:</span>  <span class="badge badge-info">{{ $sharedofficeproduct->id }}</span>
                <span >Category:</span>  <span class="badge badge-info">{{ $sharedofficeproduct->categories }}</span>
                <span >Status:</span>  <span class="badge badge-info">{{ $sharedofficeproduct->productstatus->status }}</span>

            </h3>
            <h3>
                <span >User:</span>  <span class="badge badge-info">{{ $sharedofficefinance->user }}</span>

            </h3>
            <p>Display for:@if($sharedofficeproduct->status_id=='1') hours calculations @else Month calculations @endif </p>
            <p>Start date:{{$sharedofficefinance->start_time }}   End Date:{{$sharedofficefinance->end_time}}</p>
        </div>
        <div class="portlet-body form">
            {!! Form::open(['method' => 'post', 'role' => 'form']) !!}
            <div class="form-body">

                <div class="form-group">
                    {{ Form::hidden('office_id', $sharedofficeproduct->office_id) }}
                    {{ Form::hidden('product_id', $sharedofficeproduct->id ) }}
                    {{ Form::hidden('statusfinance', '2') }}
                    {!! Form::label('startdate','StartDate',['class'=>'control-label']) !!}
                    {!! Form::text('startdate',$sharedofficeproduct->categories,['class'=>'datetime-picker form-control','required' => '', 'placeholder' => 'Enter start dates']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('enddate','EndDate',['class'=>'control-label']) !!}
                    {!! Form::text('enddate','',['class'=>'datetime-picker form-control ','required' => '', 'placeholder' => 'Enter end dates']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('user','User',['class'=>'control-label']) !!}
                    {!! Form::text('user','',['class'=>'form-control','required' => '', 'placeholder' => 'Enter User name']) !!}
                </div>


                <div class="form-actions">
                    <button type="submit" class="btn blue">Save changes</button>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
@endsection

@section('modal')

@endsection