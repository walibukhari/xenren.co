@extends('backend.layout')

@section('title')

@endsection


@section('description')

@endsection

<head xmlns:v-on="http://www.w3.org/1999/xhtml">
    <link  rel="stylesheet" href="{{asset('css/steps.css')}}">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" type="text/css" />
    <script src="{{asset('js/vue.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.5.1/vue-resource.min.js"></script>
    <style>
        .table-responsive {
            width: 100% !important;
            margin-bottom: 15px;
            overflow-y: hidden;
            -ms-overflow-style: -ms-autohiding-scrollbar;
            border: 1px solid #e7ecf1;
        }
        [v-cloak]{
            display: none !important;
        }
        @media (max-width:480px) {
            .customClassRowR {
                padding-right: 10px;
            }
        }
        .selectedOption{
            border: 0px !important;
        }
        .customDropBox{
            border: 1px solid #efefef;
            padding: 10px;
            border-radius: 6px;
            padding-bottom: 0px;
            position: absolute;
            width: 93%;
            background: #fff;
            z-index: 9999;
        }

        .customUl {
            list-style: none;
            padding-left: 0px;
        }

        .customUl li {
            cursor: pointer;
            list-style: none;
            padding-bottom: 6px;
            font-size: 15px;
        }

        .customName{
            position: absolute;
            margin-top: -21px;
            width: 100%;
            padding-left: 33px;
        }

        .customImage{
            width:20px;
            height:20px;
        }
        .customImageUK{
            width: 20px;
            height: 14px;
        }
        .inputBox{
            display: flex;
            align-items: center;
            border: 1px solid #efefef;
            border-radius: 5px;
            padding-left: 10px;
        }
        .customImageInput{
            width:20px;
            height:20px;
        }

        @media (max-width: 500px)
        {
            .customName {
                position: absolute;
                margin-top: -2px;
                width: 100%;
                padding-left: 33px;
                right: -19px;
            }
        }
    </style>
</head>
@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.sharedoffice')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedoffice.create') }}">{{trans('sharedoffice.create_office')}}</a>
        </li>
    </ul>
@endsection
@section('footer')
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCIasatwSA8nyOeN8Cobzil6yO1jsT13rE&callback=initMap&libraries=places,controls"></script>
    <script>
        var map; //Will contain map object.
        var marker = false; ////Has the user plotted their location marker?
        window.markers = []; ////Has the user plotted their location marker?
        window.selected = []; ////Has the user plotted their location marker?
        function initMap() {
            //The center location of our map.
            var centerOfMap = new google.maps.LatLng(52.357971, -6.516758);
            //Map options.
            var options = {
                center: centerOfMap, //Set center.
                zoom: 7, //The zoom value.
                mapTypeId: 'roadmap'
            };
            //Create the map object.
            map = new google.maps.Map(document.getElementById('map'), options);
            //Listen for any clicks on the map.
            google.maps.event.addListener(map, 'click', function(event) {
                console.log('click markers');
                console.log(event.latLng.lng());

                removeAllMarkersFromMap(window.markers, marker);

                var geocoder = new google.maps.Geocoder;
                var latlng = {lat: parseFloat(event.latLng.lat()), lng: parseFloat(event.latLng.lng())};
                geocoder.geocode({'location': latlng}, function(place, status) {
                    if (status === 'OK') {
                        pickRequiredInformation(place[0]);
                    } else {
                        window.alert('Geocoder failed due to: ' + status);
                    }
                    console.log(window.selected);
                });
                //Get the location that the user clicked.
                var clickedLocation = event.latLng;
                //If the marker hasn't been added.
                if(marker === false){
                    //Create the marker.
                    marker = new google.maps.Marker({
                        position: clickedLocation,
                        title: 'place.name',
                        map: map,
                        draggable: true //make it draggable
                    });
                    //Listen for drag events!
                    google.maps.event.addListener(marker, 'dragend', function(event){
                        // markerLocation();
                    });
                } else{
                    //Marker has already been added, so just change its location.
                    marker.setPosition(clickedLocation);
                }
                //Get the marker's location.
                // markerLocation();
            });
            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function() {
                searchBox.setBounds(map.getBounds());
            });
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();
                console.log(places);
                console.log(places[0].address_components);
                var country = '';
                var state = '';
                var city = '';
                if (places.length == 0) {
                    return;
                }

                places[0].address_components.forEach(function(v){
                    if(v.types[0] == 'country') {
                        country = {
                            'long_name': v.long_name,
                            'short_name': v.short_name
                        }
                    }

                    if(v.types[0] == 'administrative_area_level_1') {
                        state = {
                            'long_name': v.long_name,
                            'short_name': v.short_name
                        }
                    }

                    if(v.types[0] == 'locality') {
                        city = {
                            'long_name': v.long_name,
                            'short_name': v.short_name
                        }
                    }
                });
                if(!city) {
                    toastr.error('Please select city') ;
                } else {
                    console.log('city.long_name,country.long_name');
                    console.log(city,country);
                    console.log(city.long_name,country.long_name);
                    $('#pac-input').val(city.long_name + ', ' + country.long_name);
                    var arr = {
                        "city": {"long_name":city.long_name,"short_name":city.short_name},
                        "country": {"long_name":country.long_name,"short_name":country.short_name},
                        "lat":places[0].geometry.location.lat(),
                        "lng":places[0].geometry.location.lng(),
                    };
                    window.selected = arr;
                    console.log('vales');
                    $('#city_name').val(JSON.stringify(city));
                    $('#country_name').val(JSON.stringify(country));
                }
            });
        }
    </script>
    <script src="{{asset('js/vue.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vue-resource.min.js')}}" type="text/javascript"></script>
    <script>
        Vue.config.debug = true;
        const app = new Vue({
            el:'#app',
            data: {
                model: [],
                is_update: false,
                step:1,
                sharedOfficeData: {
                    version_english: '{{isset($model->version_english) && $model->version_english == 'true' ? 'true': ''}}',
                    version_chinese: '{{isset($model->version_chinese) && $model->version_chinese == 'true' ? 'true': ''}}',
                    bulletins: JSON.parse('{!! $selectedBulletins !!}'),
                    image: '',
                    lat:'{{$model->lat}}',
                    lng:'{{$model->lng}}',
                    office_name: '{{$model->office_name}}',
                    office_location: '{{$model->location}}',
                    pic_size: '{{$model->pic_size}}',
                    shfacilities: JSON.parse('{!! $selectedFacilities !!}'),
                    description: "{{str_replace("\r\n","<br>",$model->description)}}",
                    office_size_x: '{{(int)$model->office_size_x}}',
                    office_size_y: '{{(int)$model->office_size_y}}',
                    seatsData: [],
                    oldSeatsData: JSON.parse('{!! $model->seatsData !!}'),
                    office_space: '{{$model->office_space}}',
                    contact_name: '{{$model->contact_name}}',
                    contact_phone: '{{$model->contact_phone}}',
                    contact_email: '{{$model->contact_email}}',
                    contact_address: '{{$model->contact_address}}',
                    monday_opening_time: JSON.parse('{!! $model->monday_opening_time !!}'),
                    monday_closing_time: JSON.parse('{!! $model->monday_closing_time !!}'),
                    saturday_opening_time: '{{$model->saturday_opening_time}}',
                    saturday_closing_time: '{{$model->saturday_closing_time}}',
                    sunday_opening_time: '{{$model->sunday_opening_time}}',
                    sunday_closing_time: '{{$model->sunday_closing_time}}',
                    office_manager_email: '{{$model->office_manager}}',
                    office_name_cn: '{{isset($model->SharedOfficeLanguageData) ? $model->SharedOfficeLanguageData->office_name_cn: ''}}',
                    description_cn: '{{isset($model->SharedOfficeLanguageData) ? $model->SharedOfficeLanguageData->office_description_cn: ''}}',
                    office_space_cn: '{{isset($model->SharedOfficeLanguageData) ? $model->SharedOfficeLanguageData->office_space_cn: ''}}',
                    contact_name_cn: '{{isset($model->SharedOfficeLanguageData) ? $model->SharedOfficeLanguageData->contact_name_cn: ''}}',
                    contact_phone_cn: '{{isset($model->SharedOfficeLanguageData) ? $model->SharedOfficeLanguageData->contact_phone_cn: ''}}',
                    contact_email_cn: '{{isset($model->SharedOfficeLanguageData) ? $model->SharedOfficeLanguageData->contact_email_cn: ''}}',
                    contact_address_cn: '{{isset($model->SharedOfficeLanguageData) ? $model->SharedOfficeLanguageData->contact_address_cn: ''}}',

                    saturday_on: '{{isset($model->saturday_opening_time) && $model->saturday_opening_time != '' && $model->saturday_opening_time != 'OFF DAY' ? '' : '1'}}',
                    sunday_on: '{{isset($model->sunday_opening_time) && $model->sunday_opening_time != '' && $model->sunday_opening_time != 'OFF DAY' ? '' : '1'}}',

                    office_fee: JSON.parse('{!! $sharedOfficeFee !!}'),
                    hot_desk_fee: JSON.parse('{!! $hotDeskFee !!}'),
                    count_hot_desk: '{!! $countHotDesk !!}',
                    dedicated_desk_fee: JSON.parse('{!! $dedicatedDeskFee !!}'),
                    count_dedicated_desk: '{!! $countDedicatedDesk !!}',
                    office_desk_fee: JSON.parse('{!! $officeDeskFee !!}'),
                    count_office_desk: '{!! $countOfficeDesk !!}',
                    statusChecked: parseInt('{{$model->active}}'),
                    verify_info_to_scan: '{!! $model->verify_info_to_scan == 1 ? 'checked' : '' !!}',
                    require_deposit: '{!! $model->require_deposit == 1 ? 'checked' : '' !!}',
                    time_zone:'{!! $time_zone !!}',
                    currency_id:'{!! $currency_name !!}',
                    category_id:'{{$model->category_id}}',
                    categories:JSON.parse('{!! $categories  !!}')
                },
                no_of_peoples:'',
                hourly_price:'',
                daily_price:'',
                weekly_price:'',
                month_price:'',

                feeHotDesks:[],
                feeDedicatedDesks:[],
                feeOfficeDesks:[],
                feeHotDesk:[],
                feeDedicatedDesk:[],
                feeOfficeDesk:[],
                permission:false,
                selected_seat: {
                    x: 0,
                    y: 0
                },
                facility_number: 0,
                seat_number: 0,
                selected: -1,
                folders: [{
                    id: 1,
                    name: "Work",
                    isActive:true
                },
                    {
                        id: 2,
                        name: "Office",
                        isActive:''
                    },
                    {
                        id: 3,
                        name: "Fee",
                        isActive:''
                    }],
            },
            mounted: function() {
                this.getOriginalLocation();
                this.createGrid();
                this.getOfficeFee();
                if(this.feeHotDesks.length === 0) {
                    this.addSpaceHotDesk();
                }
                if(this.feeDedicatedDesks.length === 0) {
                    this.addSpaceDediDesk();
                }
                if(this.feeOfficeDesks.length === 0){
                    this.addSpaceOfficeDesk();
                }
                var that = this;
                setTimeout(function(){
                    that.initializeSeats();
                }, 300);
                this.changeVersion();
                setTimeout(function () {
                    that.prefilledDaysChecked();
                },300);
            },
            methods:{
                getOriginalLocation: function(){
                    var lat = this.sharedOfficeData.lat;
                    var lng = this.sharedOfficeData.lng;
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(
                            function (position) {
                                //do work work here
                                latitude = lat;
                                longitude = lng;
                                var geocoder;
                                var country = '';
                                var state = '';
                                var city = '';
                                geocoder = new google.maps.Geocoder();
                                var latlng = new google.maps.LatLng(latitude, longitude);
                                geocoder.geocode(
                                    {'latLng': latlng},
                                    function(results, status) {
                                        if (status === google.maps.GeocoderStatus.OK) {
                                            if (results[0]) {
                                                results[0].address_components.forEach(function(v){
                                                    console.log(v);
                                                    if(v.types[0] == 'country') {
                                                        country = {
                                                            'long_name': v.long_name,
                                                            'short_name': v.short_name
                                                        }
                                                    }

                                                    if(v.types[0] == 'administrative_area_level_1') {
                                                        state = {
                                                            'long_name': v.long_name,
                                                            'short_name': v.short_name
                                                        }
                                                    }

                                                    if(v.types[0] == 'locality') {
                                                        city = {
                                                            'long_name': v.long_name,
                                                            'short_name': v.short_name
                                                        }
                                                    }
                                                });
                                                console.log('city , country');
                                                console.log(city.long_name == undefined);
                                                console.log(city.short_name == undefined);
                                                var city_long_name,city_short_name;
                                                if(city.long_name == undefined || city.short_name == undefined) {
                                                    city_long_name = country.long_name;
                                                    city_short_name = country.short_name;
                                                } else {
                                                    city_long_name = city.long_name;
                                                    city_short_name = city.short_name;
                                                }
                                                // var vales = $('#pac-input').val(city.long_name + ', ' + country.long_name);
                                                var arr = {
                                                    "city": {"long_name":city_long_name,"short_name":city_short_name},
                                                    "country": {"long_name":country.long_name,"short_name":country.short_name},
                                                    "lat":results[0].geometry.location.lat(),
                                                    "lng":results[0].geometry.location.lng(),
                                                };
                                                console.log(arr);
                                                window.selected = arr;
                                                console.log('vales');
                                                $('#city_name').val(JSON.stringify(city));
                                                $('#country_name').val(JSON.stringify(country));
                                            }
                                        }
                                    }
                                );
                            },
                            function (error) {
                                var language = window.session.value;
                                var ip = th.clientIp;
                                // console.log(ip);
                                th.$http.get('/api/sharedoffice?sort=desc&limit=20&mobileApp=true&ip='+ip+'&language='+language, {
                                    headers: {
                                        Authorization: setToken
                                    }
                                }).then(function(response){
                                    this.sharedOfficesMobile = response.data.data;
                                    var citycountryName = [];
                                    var countryName = [];
                                    this.sharedOfficesMobile.map(function(data){
                                        latitude = data.lat;
                                        longitude = data.lng;
                                        var geocoder;
                                        geocoder = new google.maps.Geocoder();
                                        var latlng = new google.maps.LatLng(latitude, longitude);
                                        geocoder.geocode(
                                            {'latLng': latlng},
                                            function(results, status) {
                                                if (status == google.maps.GeocoderStatus.OK) {
                                                    if (results[0]) {
                                                        var add = results[0].formatted_address;
                                                        var value = add.split(",");
                                                        var names = value[2] + ',' + value[4];
                                                        var country_name = value[4];
                                                        countryName.push(country_name);
                                                        var cleanName = names.replace(/^\s+|\s+$/g, '');
                                                        citycountryName.push(cleanName);
                                                    }
                                                }
                                            }
                                        );
                                    });
                                    this.countrycity_name = citycountryName;
                                    this.country_name = countryName;
                                }, function(error){
                                });

                            },
                            {
                                enableHighAccuracy: true
                                , timeout: 5000
                            }
                        );
                    } else {
                        alert("Geolocation is not supported by this browser.");
                    }
                },
                getOfficeFee() {
                    console.log("shared office data office fee");
                    console.log(this.sharedOfficeData.hot_desk_fee);
                    this.feeHotDesks = this.sharedOfficeData.hot_desk_fee;
                    this.countHotDesk = this.sharedOfficeData.count_hot_desk;
                    this.feeDedicatedDesks = this.sharedOfficeData.dedicated_desk_fee;
                    this.countDedicatedDesk = this.sharedOfficeData.count_dedicated_desk;
                    this.feeOfficeDesks = this.sharedOfficeData.office_desk_fee;
                    this.countOfficeDesk = this.sharedOfficeData.count_office_desk;
                    console.log("shared office data");
                },
                initializeSeats: function() {
                    var obj = [];
                    this.sharedOfficeData.oldSeatsData.filter((value, key) => {
                        console.log('value');
                        console.log(value);
                        obj.push({
                            x: parseInt(value.x),
                            y: parseInt(value.y),
                            facility_number: value.facility_number,
                            seat_number: value.seat_number,
                        });
                    });
                    console.log("obj");
                    this.sharedOfficeData.seatsData = obj;
                    console.log(this.sharedOfficeData.seatsData);
                    this.sharedOfficeData.seatsData.filter((val) => {
                        var imgSrc = this.getImageSource(val.facility_number);
                        var html = "<img src='"+imgSrc+"' style='width: 34px; height:34px;'>";
                        console.log(val.x+'_'+val.y);
                        console.log("$('#'+val.x+'_'+val.y)");
                        $('#' + val.x + '_' + val.y).html(html);
                    });
                },
                prev() {
                    this.step--;
                    if(this.step === 1) {
                        this.folders[2].isActive = false;
                        this.folders[0].isActive = true;
                        this.folders[1].isActive = false;
                    }
                    else if(this.step === 2) {
                        this.folders[0].isActive = false;
                        this.folders[1].isActive = true;
                        this.folders[2].isActive = false;
                    }
                    else if(this.step === 3) {
                        this.folders[1].isActive = false;
                        this.folders[2].isActive = true;
                        this.folders[0].isActive = false;
                    }
                },
                next() {
                    this.step++;
                    if(this.step === 1) {
                        this.folders[2].isActive = false;
                        this.folders[0].isActive = true;
                        this.folders[1].isActive = false;
                    }
                    else if(this.step === 2) {
                        this.folders[0].isActive = false;
                        this.folders[1].isActive = true;
                        this.folders[2].isActive = false;
                    }
                    else if(this.step === 3) {
                        this.folders[1].isActive = false;
                        this.folders[2].isActive = true;
                        this.folders[0].isActive = false;
                    }
                },
                fileChanged (element) {
                    console.log('selected a file');
                    let file = this.$refs.myFile.files[0];
                    window.file = file;
                    console.log("working");
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var target = element.target.getAttribute('data-target');
                        $('#' + target).attr('src', e.target.result);
                    };
                    reader.readAsDataURL(file);
                },
                locationNullGetLocation(){
                    var lat = this.sharedOfficeData.lat;
                    var lng = this.sharedOfficeData.lng;
                    //do work work here
                    latitude = lat;
                    longitude = lng;
                    var geocoder;
                    var location = [];
                    geocoder = new google.maps.Geocoder();
                    var latlng = new google.maps.LatLng(latitude, longitude);
                    geocoder.geocode(
                        {'latLng': latlng},
                        function(results, status) {
                            if (status === google.maps.GeocoderStatus.OK) {
                                if (results[0]) {
                                    results[0].address_components.forEach(function(v){
                                        if(v.types[0] == 'country') {
                                            country = {
                                                'long_name': v.long_name,
                                                'short_name': v.short_name
                                            }
                                        }

                                        if(v.types[0] == 'administrative_area_level_1') {
                                            state = {
                                                'long_name': v.long_name,
                                                'short_name': v.short_name
                                            }
                                        }

                                        if(v.types[0] == 'locality') {
                                            city = {
                                                'long_name': v.long_name,
                                                'short_name': v.short_name
                                            }
                                        }
                                    });
                                    var arr = {
                                        "city": {"long_name":city.long_name,"short_name":city.short_name},
                                        "country": {"long_name":country.long_name,"short_name":country.short_name},
                                        "lat":results[0].geometry.location.lat(),
                                        "lng":results[0].geometry.location.lng(),
                                    };
                                    window.selected = arr;
                                    location.push(arr);
                                }
                            }
                        }
                    );
                    return location;
                },
                createSharedOffice: function() {
                    let currency_value = $('#selectedOption').val();
                    console.log('currency_value');
                    console.log(currency_value);
                    console.log('window.selected');
                    console.log(window.selected);
                    if(window.selected.length <= 0 ) {
                        let that = this;
                        console.log('window.selected location is null now going to get location');
                        setTimeout(function () {
                            console.log('settimeout finish');
                            that.locationNullGetLocation();
                            that.permission = true;
                            that.createSharedOffice();
                        },1000);
                    } else {
                        console.log('original_location');
                        console.log(window.selected);
                        var formData = new FormData();
                        var shf = this.sharedOfficeData.shfacilities;
                        var url = '/admin/sharedoffice/edit/' + '{{$id}}';
                        formData.append('image', window.file);
                        formData.append('office_name', this.sharedOfficeData.office_name);
                        formData.append('location', this.sharedOfficeData.office_location);
                        formData.append('office_size_x', this.sharedOfficeData.office_size_x);
                        formData.append('office_size_y', this.sharedOfficeData.office_size_y);
                        formData.append('description', this.sharedOfficeData.description);
                        formData.append('pic_size', this.sharedOfficeData.pic_size);
                        formData.append('shfacilities', this.sharedOfficeData.shfacilities);
                        formData.append('category_id', this.sharedOfficeData.category_id);
                        formData.append('seatsData', JSON.stringify(this.sharedOfficeData.seatsData));
                        formData.append('current_location', JSON.stringify(window.selected));

                        formData.append('office_manager', this.sharedOfficeData.office_manager_email);
                        formData.append('office_space', this.sharedOfficeData.office_space);

                        formData.append('contact_name', this.sharedOfficeData.contact_name);
                        formData.append('contact_phone', this.sharedOfficeData.contact_phone);
                        formData.append('contact_email', this.sharedOfficeData.contact_email);
                        formData.append('contact_address', this.sharedOfficeData.contact_address);

                        formData.append('monday_opening_time', JSON.stringify(this.sharedOfficeData.monday_opening_time));
                        formData.append('monday_closing_time', JSON.stringify(this.sharedOfficeData.monday_closing_time));

                        var saturday_opening_time = this.sharedOfficeData.saturday_on ? 'OFF DAY' : this.sharedOfficeData.saturday_opening_time;
                        var saturday_closing_time = this.sharedOfficeData.saturday_on ? 'OFF DAY' : this.sharedOfficeData.saturday_closing_time;

                        formData.append('saturday_opening_time', saturday_opening_time);
                        formData.append('saturday_closing_time', saturday_closing_time);

                        var sunday_opening_time = this.sharedOfficeData.sunday_on ? 'OFF DAY' : this.sharedOfficeData.sunday_opening_time;
                        var sunday_closing_time = this.sharedOfficeData.sunday_on ? 'OFF DAY' : this.sharedOfficeData.sunday_closing_time;

                        formData.append('sunday_opening_time', sunday_opening_time);
                        formData.append('sunday_closing_time', sunday_closing_time);

                        formData.append('saturday_on', this.sharedOfficeData.saturday_on);
                        formData.append('sunday_on', this.sharedOfficeData.sunday_on);

                        formData.append('office_name_cn', this.sharedOfficeData.office_name_cn);
                        formData.append('description_cn', this.sharedOfficeData.description_cn);
                        formData.append('office_space_cn', this.sharedOfficeData.office_space_cn);
                        formData.append('contact_name_cn', this.sharedOfficeData.contact_name_cn);
                        formData.append('contact_phone_cn', this.sharedOfficeData.contact_phone_cn);
                        formData.append('contact_email_cn', this.sharedOfficeData.contact_email_cn);
                        formData.append('contact_address_cn', this.sharedOfficeData.contact_address_cn);

                        formData.append('version_english', this.sharedOfficeData.version_english);
                        formData.append('version_chinese', this.sharedOfficeData.version_chinese);

                        formData.append('bulletins', this.sharedOfficeData.bulletins);
                        formData.append('feeHotDesks', JSON.stringify(this.feeHotDesks));
                        formData.append('feeDedicatedDesks', JSON.stringify(this.feeDedicatedDesks));
                        formData.append('feeOfficeDesks', JSON.stringify(this.feeOfficeDesks));
                        formData.append('sharedoffice_status', this.sharedOfficeData.statusChecked);
                        formData.append('sharedoffice_status', this.sharedOfficeData.statusChecked);


                        formData.append('sharedoffice_status', this.sharedOfficeData.statusChecked);
                        formData.append('verify_info_to_scan', this.sharedOfficeData.verify_info_to_scan);
                        formData.append('require_deposit', this.sharedOfficeData.require_deposit);
                        formData.append('currency_id', currency_value);
                        formData.append('time_zone', this.sharedOfficeData.time_zone);

                        var errorHotDesks = errorDedicatedDesks = errorOfficeDesks = false;
                        this.feeHotDesks.forEach(function (data) {
                            Object.entries(data).map(function (key, val) {
                                if (key[1] === "") {
                                    toastr.error("Please enter hot desk " + key[0].replace(/_/g, ' ') + " value");
                                    errorHotDesks = true;
                                }
                            })
                        });
                        this.feeDedicatedDesks.forEach(function (data) {
                            Object.entries(data).map(function (key, val) {
                                if (key[1] === "") {
                                    toastr.error("Please enter dedicated desk " + key[0].replace(/_/g, ' ') + " value");
                                    errorDedicatedDesks = true;
                                }
                            })
                        });
                        this.feeOfficeDesks.forEach(function (data) {
                            Object.entries(data).map(function (key, val) {
                                if (key[1] === "") {
                                    toastr.error("Please enter office desk " + key[0].replace(/_/g, ' ') + " value");
                                    errorOfficeDesks = true;
                                }
                            })
                        });

                        if (errorHotDesks === true || errorDedicatedDesks === true || errorOfficeDesks === true) {
                        } else if (this.sharedOfficeData.pic_size === '') {
                            toastr.error('Please select pic size');
                        } else if (this.sharedOfficeData.bulletins === '') {
                            toastr.error('Please select bulletins');
                        } else if (this.sharedOfficeData.shfacilities === '') {
                            toastr.error('Please select facilities');
                        } else if (this.sharedOfficeData.office_location === '') {
                            toastr.error('Please select location');
                        } else if (!this.sharedOfficeData.saturday_on) {
                            if (this.sharedOfficeData.saturday_opening_time === '') {
                                toastr.error('Please fill saturday opening time field');
                            } else if (this.sharedOfficeData.saturday_closing_time === '') {
                                toastr.error('Please fill saturday closgin time  field');
                            }
                        } else if (!this.sharedOfficeData.sunday_on) {
                            if (this.sharedOfficeData.sunday_opening_time === '') {
                                toastr.error('Please fill sunday opening hours field');
                            } else if (this.sharedOfficeData.sunday_closing_time === '') {
                                toastr.error('Please fill sunday closing hours field');
                            }
                        } else if (this.sharedOfficeData.monday_opening_time === '') {
                            toastr.error('Please fill operating hours field');
                        } else if (this.sharedOfficeData.monday_closing_time === '') {
                            toastr.error('Please fill operating hours field');
                        } else if (this.sharedOfficeData.office_manager_email === '') {
                            toastr.error('Please enter office manager email');
                        } else if (!this.sharedOfficeData.office_name && !this.sharedOfficeData.office_name_cn) {
                            toastr.error('Please enter office name');
                        } else if (!this.sharedOfficeData.description && !this.sharedOfficeData.description_cn) {
                            toastr.error('Please enter office description');
                        } else if (!this.sharedOfficeData.office_space && !this.sharedOfficeData.office_space_cn) {
                            toastr.error('Please enter office space');
                        } else if (!this.sharedOfficeData.contact_name && !this.sharedOfficeData.contact_name_cn) {
                            toastr.error('Please enter contact name');
                        } else if (!this.sharedOfficeData.contact_email && !this.sharedOfficeData.contact_email_cn) {
                            toastr.error('Please enter contact email');
                        } else if (!this.sharedOfficeData.contact_phone && !this.sharedOfficeData.contact_phone_cn) {
                            toastr.error('Please enter contact phone');
                        } else if (!this.sharedOfficeData.contact_address && !this.sharedOfficeData.contact_address_cn) {
                            toastr.error('Please enter contact address');
                        } else if (this.sharedOfficeData.office_manager_email === '' || this.sharedOfficeData.office_manager_email === '') {
                            toastr.error('Please enter office manager email');
                        } else if (this.sharedOfficeData.office_size_x === '' || this.sharedOfficeData.office_size_y === '') {
                            toastr.error('Please enter office size');
                        } else if (this.sharedOfficeData.seatsData === '') {
                            toastr.error('Please select seats data');
                        } else {
                            this.$http.post(url, formData).then((response) => {
                                if (response.body && response.body.status === 'success') {
                                    toastr.success(response.body.message, 'Success');
                                    setTimeout(function () {
                                        window.location.href = '/admin/sharedoffice';
                                    }, 3000)
                                } else {
                                    toastr.error(response.body.message, 'Error');
                                }
                            }, (err) => {
                                toastr.error(response.body.message, 'Error');
                            });
                        }
                    }
                },
                createGrid: function() {
                    console.log('this.sharedOfficeData.office_size_x');
                    console.log(this.sharedOfficeData.office_size_x);
                    this.sharedOfficeData.office_size_x = this.sharedOfficeData.office_size_x > 0 ? parseInt(this.sharedOfficeData.office_size_x) : 0;
                    this.sharedOfficeData.office_size_y = this.sharedOfficeData.office_size_y > 0 ? parseInt(this.sharedOfficeData.office_size_y) : 0;
                },
                getX: function(){
                    return this.sharedOfficeData.office_size_x;
                },
                checked: function(x, y) {
                    $(".innerboxes").each(function() {
                        this.style.border = '2px solid #E3E3E3';
                    });
                    $('#'+x+'_'+y).css('border' ,'2px solid #3FAF3D');
                    var newSelectedSeat = {
                        x: x,
                        y: y
                    };
                    this.selected_seat = newSelectedSeat;
                },
                setSeatData: function(val=null) {
                    var exist = this.filteredSeat();
                    if (exist.length > 0) {
                        alert('Seat number already consumed...!');
                    } else {
                        this.sharedOfficeData.seatsData.push({
                            x: this.selected_seat.x,
                            y: this.selected_seat.y,
                            facility_number: this.facility_number,
                            seat_number: this.seat_number
                        });
                        console.log('image source');
                        console.log(this.facility_number);
                        var imgSrc = this.getImageSource(this.facility_number);
                        var html = "";
                        html += "<img src='" + imgSrc + "' style='width: 34px; height:34px;'><br/>";
                        $('#' + this.selected_seat.x + '_' + this.selected_seat.y).html(html);
                        console.log('html selected');
                        console.log(html);
                    }
                },
                getSeatImage: function(data){
                    console.log('seat get image ');
                    console.log(data.facility_number);
                    var imgSrc = this.getImageSource(data.facility_number);
                    console.log(imgSrc);
                    return imgSrc;
                },
                getWidth: function() {
                    console.log('x called');
                    var widthVal = this.sharedOfficeData.office_size_x*50;
                    return { width: widthVal+'px' }
                },
                checkedboxval(){
                    if(event.target.checked)
                    {
                        this.sharedOfficeData.statusChecked = 1;
                    } else {
                        this.sharedOfficeData.statusChecked = 0;
                    }
                },
                filteredSeat(){
                    return this.sharedOfficeData.seatsData
                        .filter((value) => {
                            return (value.seat_number.match(this.seat_number));
                        });
                },
                getImageSource: function(id) {
                    var img = '';
                    var imageSource = '{!! \App\Models\SharedOfficeProductCategory::get() !!}';
                    imageSource = JSON.parse(imageSource);
                    console.log(imageSource);
                    $.each(imageSource, function (v) {
                        if(this.id == id) {
                            console.log(this.image);
                            img = this.image;
                        }
                    });
                    return '/'+img;
                },
                changeVersion: function() {
                    console.log('english version now');
                    console.log(this.sharedOfficeData.version_english);
                    console.log('chinese version now');
                    console.log(this.sharedOfficeData.version_chinese);
                },
                addSpaceHotDesk(){
                    console.log(this.feeHotDesks);
                    console.log(this.feeHotDesks.length);
                    this.feeHotDesks.push({
                        type: '{{\App\Models\SharedOfficeFee::HOT_DESK_TYPE}}',
                        category_id:'{{\App\Models\SharedOfficeProducts::CATEGORY_HOT_DESK}}',
                        people: null,
                        hourly: null,
                        weekly: null,
                        daily: null,
                        monthly: null,
                        availability_id: null,
                        number:null,
                    });
                    console.log(this.feeHotDesks);
                    console.log(this.feeHotDesks.length);
                },
                addSpaceDediDesk: function () {
                    this.feeDedicatedDesks.push({
                        type: '{{\App\Models\SharedOfficeFee::DEDICATED_DESK_TYPE}}',
                        category_id:'{{\App\Models\SharedOfficeProducts::CATEGORY_DEDICATED_DESK}}',
                        people: null,
                        hourly: null,
                        weekly: null,
                        daily: null,
                        monthly: null,
                        availability_id: null,
                        number:null,
                    });
                },
                addSpaceOfficeDesk: function () {
                    this.feeOfficeDesks.push({
                        type: '{{\App\Models\SharedOfficeFee::OFFICE_DESK_TYPE}}',
                        category_id:'{{\App\Models\SharedOfficeProducts::CATEGORY_MEETING_ROOM}}',
                        people: null,
                        hourly: null,
                        weekly: null,
                        daily: null,
                        monthly: null,
                        availability_id: null,
                        number:null,
                    });
                },
                removeElmHotDesk: function (id, index) {
                    this.removeDesk(id);
                    this.feeHotDesks.splice(index, 1);
                },
                removeElmDediDesk: function (id, index) {
                    this.removeDesk(id);
                    this.feeDedicatedDesks.splice(index, 1);
                },
                removeElmOfficeDesk: function (id, index) {
                    this.removeDesk(id);
                    this.feeOfficeDesks.splice(index, 1);
                },
                removeDesk: function(id) {
                    this.$http.get('/admin/sharedoffice/delete/removeDesk/'+id).then((response) => {
                        if (response.body.status == 'success') {
                            toastr.success(response.body.message, 'Success');
                        } else {
                            toastr.error(response.body.message, 'Error');
                        }
                    }, (err) => {
                        toastr.error(response.body.message, 'Error');
                    });
                },
                selectDays: function(value , event){
                    var time = [];
                    if(value === "monday"){
                        console.log(event.target.value);
                        let val = event.target.value;
                        if(val === '2')
                        {
                            document.getElementById("monchecked").setAttribute('disabled', 'disabled');
                            document.getElementById("moncheckedtime").setAttribute('disabled', 'disabled');
                            document.getElementById("moncheckedtimeON").setAttribute('disabled', 'disabled');
                            document.getElementById("moncheckedtimeOff").setAttribute('disabled', 'disabled');
                            document.getElementById("moncheckedtimeOffAP").setAttribute('disabled', 'disabled');
                            $('#monchangeColor').addClass('closeChecked');
                            console.log('monday');
                            this.sharedOfficeData.monday_opening_time.map(function (data) {
                                if(data.monday) {
                                    data.monday.day = 'monday';
                                    data.monday.am_pm = 'OFF DAY';
                                    data.monday.start_time = 'OFF DAY';
                                }
                            });
                            this.sharedOfficeData.monday_closing_time.map(function (data) {
                                if(data.monday) {
                                    data.monday.day = 'monday';
                                    data.monday.am_pm = 'OFF DAY';
                                    data.monday.start_time = 'OFF DAY';
                                }
                            });
                        } else {
                            document.getElementById("monchecked").removeAttribute('disabled');
                            document.getElementById("moncheckedtime").removeAttribute('disabled');
                            document.getElementById("moncheckedtimeON").removeAttribute('disabled');
                            document.getElementById("moncheckedtimeOff").removeAttribute('disabled');
                            document.getElementById("moncheckedtimeOffAP").removeAttribute('disabled');
                            $('#monchangeColor').removeClass('closeChecked');
                        }
                    } else if(value === "tuesday") {
                        console.log(event.target.value);
                        let val = event.target.value;
                        if(val === '2')
                        {
                            document.getElementById("tuechecked").setAttribute('disabled', 'disabled');
                            document.getElementById("tuecheckedtime").setAttribute('disabled', 'disabled');
                            document.getElementById("tuecheckedtimeON").setAttribute('disabled', 'disabled');
                            document.getElementById("tuecheckedtimeOff").setAttribute('disabled', 'disabled');
                            document.getElementById("tuecheckedtimeOffAP").setAttribute('disabled', 'disabled');
                            $('#tuechangeColor').addClass('closeChecked');
                            console.log('tuesday');
                            this.sharedOfficeData.monday_opening_time.map(function (data) {
                                if(data.tuesday) {
                                    data.tuesday.day = 'tuesday';
                                    data.tuesday.am_pm = 'OFF DAY';
                                    data.tuesday.start_time = 'OFF DAY';
                                }
                            });
                            this.sharedOfficeData.monday_closing_time.map(function (data) {
                                if(data.tuesday) {
                                    data.tuesday.day = 'tuesday';
                                    data.tuesday.am_pm = 'OFF DAY';
                                    data.tuesday.start_time = 'OFF DAY';
                                }
                            });
                        } else {
                            document.getElementById("tuechecked").removeAttribute('disabled');
                            document.getElementById("tuecheckedtime").removeAttribute('disabled');
                            document.getElementById("tuecheckedtimeON").removeAttribute('disabled');
                            document.getElementById("tuecheckedtimeOff").removeAttribute('disabled');
                            document.getElementById("tuecheckedtimeOffAP").removeAttribute('disabled');
                            $('#tuechangeColor').removeClass('closeChecked');
                        }
                    } else if(value === "wednesday") {
                        console.log(event.target.value);
                        let val = event.target.value;
                        if(val === '2')
                        {
                            document.getElementById("wedchecked").setAttribute('disabled', 'disabled');
                            document.getElementById("wedcheckedtime").setAttribute('disabled', 'disabled');
                            document.getElementById("wedcheckedtimeON").setAttribute('disabled', 'disabled');
                            document.getElementById("wedcheckedtimeOff").setAttribute('disabled', 'disabled');
                            document.getElementById("wedcheckedtimeOffAP").setAttribute('disabled', 'disabled');
                            $('#wedchangeColor').addClass('closeChecked');
                            console.log('wednesday');
                            this.sharedOfficeData.monday_opening_time.map(function (data) {
                                if(data.wednesday) {
                                    data.wednesday.day = 'wednesday';
                                    data.wednesday.am_pm = 'OFF DAY';
                                    data.wednesday.start_time = 'OFF DAY';
                                }
                            });
                            this.sharedOfficeData.monday_closing_time.map(function (data) {
                                if(data.wednesday) {
                                    data.wednesday.day = 'wednesday';
                                    data.wednesday.am_pm = 'OFF DAY';
                                    data.wednesday.start_time = 'OFF DAY';
                                }
                            });
                        } else {
                            document.getElementById("wedchecked").removeAttribute('disabled');
                            document.getElementById("wedcheckedtime").removeAttribute('disabled');
                            document.getElementById("wedcheckedtimeON").removeAttribute('disabled');
                            document.getElementById("wedcheckedtimeOff").removeAttribute('disabled');
                            document.getElementById("wedcheckedtimeOffAP").removeAttribute('disabled');
                            $('#wedchangeColor').removeClass('closeChecked');
                        }
                    } else if(value === "thursday") {
                        console.log(event.target.value);
                        let val = event.target.value;
                        if(val === '2')
                        {
                            document.getElementById("thuchecked").setAttribute('disabled', 'disabled');
                            document.getElementById("thucheckedtime").setAttribute('disabled', 'disabled');
                            document.getElementById("thucheckedtimeON").setAttribute('disabled', 'disabled');
                            document.getElementById("thucheckedtimeOff").setAttribute('disabled', 'disabled');
                            document.getElementById("thucheckedtimeOffAP").setAttribute('disabled', 'disabled');
                            $('#thuchangeColor').addClass('closeChecked');
                            console.log('thursday');
                            this.sharedOfficeData.monday_opening_time.map(function (data) {
                                if(data.thursday) {
                                    data.thursday.day = 'thursday';
                                    data.thursday.am_pm = 'OFF DAY';
                                    data.thursday.start_time = 'OFF DAY';
                                }
                            });
                            this.sharedOfficeData.monday_closing_time.map(function (data) {
                                if(data.thursday) {
                                    data.thursday.day = 'thursday';
                                    data.thursday.am_pm = 'OFF DAY';
                                    data.thursday.start_time = 'OFF DAY';
                                }
                            });
                        } else {
                            document.getElementById("thuchecked").removeAttribute('disabled');
                            document.getElementById("thucheckedtime").removeAttribute('disabled');
                            document.getElementById("thucheckedtimeON").removeAttribute('disabled');
                            document.getElementById("thucheckedtimeOff").removeAttribute('disabled');
                            document.getElementById("thucheckedtimeOffAP").removeAttribute('disabled');
                            $('#thuchangeColor').removeClass('closeChecked');
                        }
                    } else if(value === "friday") {
                        console.log(event.target.value);
                        let val = event.target.value;
                        if(val === '2')
                        {
                            document.getElementById("frichecked").setAttribute('disabled', 'disabled');
                            document.getElementById("fricheckedtime").setAttribute('disabled', 'disabled');
                            document.getElementById("fricheckedtimeON").setAttribute('disabled', 'disabled');
                            document.getElementById("fricheckedtimeOff").setAttribute('disabled', 'disabled');
                            document.getElementById("fricheckedtimeOffAP").setAttribute('disabled', 'disabled');
                            $('#frichangeColor').addClass('closeChecked');
                            console.log('friday');
                            this.sharedOfficeData.monday_opening_time.map(function (data) {
                                if(data.friday) {
                                    data.friday.day = 'friday';
                                    data.friday.am_pm = 'OFF DAY';
                                    data.friday.start_time = 'OFF DAY';
                                }
                            });
                            this.sharedOfficeData.monday_closing_time.map(function (data) {
                                if(data.friday) {
                                    data.friday.day = 'friday';
                                    data.friday.am_pm = 'OFF DAY';
                                    data.friday.start_time = 'OFF DAY';
                                }
                            });
                        } else {
                            document.getElementById("frichecked").removeAttribute('disabled');
                            document.getElementById("fricheckedtime").removeAttribute('disabled');
                            document.getElementById("fricheckedtimeON").removeAttribute('disabled');
                            document.getElementById("fricheckedtimeOff").removeAttribute('disabled');
                            document.getElementById("fricheckedtimeOffAP").removeAttribute('disabled');
                            $('#frichangeColor').removeClass('closeChecked');
                        }
                    } else if(value === "saturday") {
                        console.log(event.target.value);
                        let val = event.target.value;
                        if(val === '2')
                        {
                            document.getElementById("satchecked").setAttribute('disabled', 'disabled');
                            document.getElementById("satcheckedtime").setAttribute('disabled', 'disabled');
                            document.getElementById("satcheckedtimeON").setAttribute('disabled', 'disabled');
                            document.getElementById("satcheckedtimeOff").setAttribute('disabled', 'disabled');
                            document.getElementById("satcheckedtimeOffAP").setAttribute('disabled', 'disabled');
                            $('#satchangeColor').addClass('closeChecked');
                            console.log('saturday');
                            this.sharedOfficeData.monday_opening_time.map(function (data) {
                                if(data.saturday) {
                                    data.saturday.day = 'saturday';
                                    data.saturday.am_pm = 'OFF DAY';
                                    data.saturday.start_time = 'OFF DAY';
                                }
                            });
                            this.sharedOfficeData.monday_closing_time.map(function (data) {
                                if(data.saturday) {
                                    data.saturday.day = 'saturday';
                                    data.saturday.am_pm = 'OFF DAY';
                                    data.saturday.start_time = 'OFF DAY';
                                }
                            });
                        } else {
                            document.getElementById("satchecked").removeAttribute('disabled');
                            document.getElementById("satcheckedtime").removeAttribute('disabled');
                            document.getElementById("satcheckedtimeON").removeAttribute('disabled');
                            document.getElementById("satcheckedtimeOff").removeAttribute('disabled');
                            document.getElementById("satcheckedtimeOffAP").removeAttribute('disabled');
                            $('#satchangeColor').removeClass('closeChecked');
                        }
                    } else if(value === "sunday") {
                        console.log(event.target.value);
                        let val = event.target.value;
                        if(val === '2')
                        {
                            document.getElementById("myCheck").setAttribute('disabled', 'disabled');
                            document.getElementById("checkedtime").setAttribute('disabled', 'disabled');
                            document.getElementById("checkedAP").setAttribute('disabled', 'disabled');
                            document.getElementById("checkedOtime").setAttribute('disabled', 'disabled');
                            document.getElementById("checkedOAP").setAttribute('disabled', 'disabled');
                            $('#changeColor').addClass('closeChecked');
                            console.log('sunday');
                            this.sharedOfficeData.monday_opening_time.map(function (data) {
                                if(data.sunday) {
                                    data.sunday.day = 'sunday';
                                    data.sunday.am_pm = 'OFF DAY';
                                    data.sunday.start_time = 'OFF DAY';
                                }
                            });
                            this.sharedOfficeData.monday_closing_time.map(function (data) {
                                if(data.sunday) {
                                    data.sunday.day = 'sunday';
                                    data.sunday.am_pm = 'OFF DAY';
                                    data.sunday.start_time = 'OFF DAY';
                                }
                            });
                        } else {
                            document.getElementById("myCheck").removeAttribute('disabled');
                            document.getElementById("checkedtime").removeAttribute('disabled');
                            document.getElementById("checkedAP").removeAttribute('disabled');
                            document.getElementById("checkedOtime").removeAttribute('disabled');
                            document.getElementById("checkedOAP").removeAttribute('disabled');
                            $('#changeColor').removeClass('closeChecked');
                        }
                    }
                    console.log('this.sharedOfficeData.monday_opening_time');
                    console.log(this.sharedOfficeData.monday_opening_time);
                    console.log(this.sharedOfficeData.monday_closing_time);
                },
                loadData: function(id,e){
                    if(e.target.checked === true) {
                        this.sharedOfficeData.shfacilities.push(id);
                    } else {
                        var array = this.sharedOfficeData.shfacilities;
                        array.map(function (data) {
                            if(data === id) {
                                var index = array.indexOf(data);
                                array.splice(index,1);
                            }
                        });
                    }
                    console.log(this.sharedOfficeData.shfacilities);
                },
                verifyInfo: function(e){
                    if(e.target.checked === true) {
                        this.sharedOfficeData.verify_info_to_scan = 1;
                    } else {
                        this.sharedOfficeData.verify_info_to_scan = 0;
                    }
                    console.log(this.sharedOfficeData.verify_info_to_scan);
                },
                requestDeposite: function(e){
                    if(e.target.checked === true) {
                        this.sharedOfficeData.require_deposit = 1;
                    } else {
                        this.sharedOfficeData.require_deposit = 0;
                    }
                    console.log(this.sharedOfficeData.require_deposit);
                },
                selectedArea: function(val,name) {
                    if(name === "Work") {
                        this.resetFields();
                        this.step = 1;
                        this.folders[2].isActive = false;
                        this.folders[0].isActive = true;
                        this.folders[1].isActive = false;
                        console.log('checkbox checked');
                        $('#monchecked').prop('checked',true);
                    } else if(name === "Office") {
                        this.step = 2;
                        this.folders[0].isActive = false;
                        this.folders[1].isActive = true;
                        this.folders[2].isActive = false;
                    } else if(name === "Fee") {
                        this.step = 3;
                        this.folders[1].isActive = false;
                        this.folders[2].isActive = true;
                        this.folders[0].isActive = false;
                    }
                },
                arraySplice: function(val) {
                    let array = this.sharedOfficeData.monday_opening_time;
                    array.map(function (data) {
                        if(data.monday){
                            console.log('data.monday');
                            console.log(data.monday.day === val);
                            if(data.monday.day === val) {
                                let index = array.indexOf(data);
                                array.splice(index,1);
                            }
                        }
                        if(data.tuesday){
                            console.log('data.tuesday');
                            console.log(data.tuesday.day === val);
                            if(data.tuesday.day === val) {
                                let index = array.indexOf(data);
                                array.splice(index,1);
                            }
                        }
                        if(data.wednesday){
                            console.log('data.wednesday');
                            console.log(data.wednesday.day === val);
                            if(data.wednesday.day === val) {
                                let index = array.indexOf(data);
                                array.splice(index,1);
                            }
                        }
                        if(data.thursday){
                            console.log('data.thursday');
                            console.log(data.thursday.day === val);
                            if(data.thursday.day === val) {
                                let index = array.indexOf(data);
                                array.splice(index,1);
                            }
                        }
                        if(data.friday){
                            console.log('data.friday');
                            console.log(data.friday.day === val);
                            if(data.friday.day === val) {
                                let index = array.indexOf(data);
                                array.splice(index,1);
                            }
                        }
                        if(data.saturday){
                            alert(data.saturday.day);
                            console.log('data.saturday');
                            console.log(data.saturday.day === val);
                            if(data.saturday.day === val) {
                                let index = array.indexOf(data);
                                array.splice(index,1);
                            }
                        }
                        if(data.sunday){
                            console.log('data.sunday');
                            console.log(data.sunday.day === val);
                            if(data.sunday.day === val) {
                                let index = array.indexOf(data);
                                array.splice(index,1);
                            }
                        }
                    });
                },
                arraySplice1: function(val){
                    let array = this.sharedOfficeData.monday_closing_time;
                    array.map(function (data) {
                        if(data.monday){
                            console.log('data.monday');
                            console.log(data.monday.day === val);
                            if(data.monday.day === val) {
                                let index = array.indexOf(data);
                                array.splice(index,1);
                            }
                        }
                        if(data.tuesday){
                            console.log('data.tuesday');
                            console.log(data.tuesday.day === val);
                            if(data.tuesday.day === val) {
                                let index = array.indexOf(data);
                                array.splice(index,1);
                            }
                        }
                        if(data.wednesday){
                            console.log('data.wednesday');
                            console.log(data.wednesday.day === val);
                            if(data.wednesday.day === val) {
                                let index = array.indexOf(data);
                                array.splice(index,1);
                            }
                        }
                        if(data.thursday){
                            console.log('data.thursday');
                            console.log(data.thursday.day === val);
                            if(data.thursday.day === val) {
                                let index = array.indexOf(data);
                                array.splice(index,1);
                            }
                        }
                        if(data.friday){
                            console.log('data.friday');
                            console.log(data.friday.day === val);
                            if(data.friday.day === val) {
                                let index = array.indexOf(data);
                                array.splice(index,1);
                            }
                        }
                        if(data.saturday){
                            console.log('data.saturday');
                            console.log(data.saturday.day === val);
                            if(data.saturday.day === val) {
                                let index = array.indexOf(data);
                                array.splice(index,1);
                            }
                        }
                        if(data.sunday){
                            console.log('data.sunday');
                            console.log(data.sunday.day === val);
                            if(data.sunday.day === val) {
                                let index = array.indexOf(data);
                                array.splice(index,1);
                            }
                        }
                    });
                },
                daysChecked: function (val)     {
                    if(val === 'mon') {
                        if(event.target.checked)
                        {
                            var mondayStartTime = $('#moncheckedtime').val();
                            var mondayStartAmPM = $('#moncheckedtimeON').val();
                            var mondayCloseTime = $('#moncheckedtimeOff').val();
                            var mondayCloseAmPM = $('#moncheckedtimeOffAP').val();
                            var openTime = {
                                'monday':
                                    {"start_time": mondayStartTime, "am_pm": mondayStartAmPM, "day":"monday"},
                            }
                            this.sharedOfficeData.monday_opening_time.push(openTime);
                            var closeTime = {'monday':
                                    {'close_time': mondayCloseTime,'am_pm': mondayCloseAmPM, "day":"monday"},
                            };
                            this.sharedOfficeData.monday_closing_time.push(closeTime);
                        } else {
                            this.arraySplice("monday");
                            this.arraySplice1("monday");
                        }
                    }
                    if(val === 'tue') {
                        if(event.target.checked)
                        {
                            var tuesdayStartTime = $('#tuecheckedtime').val();
                            var tuesdayStartAmPM = $('#tuecheckedtimeON').val();
                            var tuesdayCloseTime = $('#tuecheckedtimeOff').val();
                            var tuesdayCloseAmPM = $('#tuecheckedtimeOffAP').val();
                            let openTime = {'tuesday':
                                    {'start_time':tuesdayStartTime,'am_pm':tuesdayStartAmPM,"day":"tuesday"},
                            };
                            this.sharedOfficeData.monday_opening_time.push(openTime);
                            let closeTime = {'tuesday':
                                    {'close_time': tuesdayCloseTime,'am_pm': tuesdayCloseAmPM,"day":"tuesday"},
                            };
                            this.sharedOfficeData.monday_closing_time.push(closeTime);
                        } else {
                            this.arraySplice("tuesday");
                            this.arraySplice1("tuesday");
                        }
                    }
                    if(val === 'wed') {
                        if(event.target.checked)
                        {
                            var wednesdayStartTime = $('#wedcheckedtime').val();
                            var wednesdayStartAmPM = $('#wedcheckedtimeON').val();
                            var wednesdayCloseTime = $('#wedcheckedtimeOff').val();
                            var wednesdayCloseAmPM = $('#wedcheckedtimeOffAP').val();
                            let openTime = {'wednesday':
                                    {'start_time':wednesdayStartTime,'am_pm':wednesdayStartAmPM,"day":"wednesday"},
                            };
                            this.sharedOfficeData.monday_opening_time.push(openTime);
                            let closeTime = {'wednesday':
                                    {'close_time': wednesdayCloseTime,'am_pm': wednesdayCloseAmPM,"day":"wednesday"},
                            };
                            this.sharedOfficeData.monday_closing_time.push(closeTime);
                        } else {
                            this.arraySplice("wednesday");
                            this.arraySplice1("wednesday");
                        }
                    }
                    if(val === 'thu') {
                        if(event.target.checked)
                        {
                            var thursdayStartTime = $('#thucheckedtime').val();
                            var thursdayStartAmPM = $('#thucheckedtimeON').val();
                            var thursdayCloseTime = $('#thucheckedtimeOff').val();
                            var thursdayCloseAmPM = $('#thucheckedtimeOffAP').val();
                            let openTime = {'thursday':
                                    {'start_time': thursdayStartTime, 'am_pm': thursdayStartAmPM,"day":"thursday"},
                            };
                            this.sharedOfficeData.monday_opening_time.push(openTime);
                            let closeTime = {'thursday':
                                    {'close_time': thursdayCloseTime,'am_pm': thursdayCloseAmPM,"day":"thursday"},
                            };
                            this.sharedOfficeData.monday_closing_time.push(closeTime);
                        } else {
                            this.arraySplice("thursday");
                            this.arraySplice1("thursday");
                        }
                    }
                    if(val === 'fri') {
                        if(event.target.checked)
                        {
                            var fridayStartTime = $('#fricheckedtime').val();
                            var fridayStartAmPM = $('#fricheckedtimeON').val();
                            var fridayCloseTime = $('#fricheckedtimeOff').val();
                            var fridayCloseAmPM = $('#fricheckedtimeOffAP').val();
                            let openTime = {'friday':
                                    {'start_time':fridayStartTime,'am_pm':fridayStartAmPM,"day":"friday"},
                            };
                            this.sharedOfficeData.monday_opening_time.push(openTime);
                            let closeTime = {'friday':
                                    {'close_time': fridayCloseTime,'am_pm': fridayCloseAmPM,"day":"friday"},
                            };
                            this.sharedOfficeData.monday_closing_time.push(closeTime);
                        } else {
                            this.arraySplice("friday");
                            this.arraySplice1("friday");
                        }
                    }
                    if(val === 'sat') {
                        if(event.target.checked)
                        {
                            var saturdayStartTime = $('#satcheckedtime').val();
                            var saturdayStartAmPM = $('#satcheckedtimeON').val();
                            var saturdayCloseTime = $('#satcheckedtimeOff').val();
                            var saturdayCloseAmPM = $('#satcheckedtimeOffAP').val();
                            let openTime = {'saturday':
                                    {'start_time':saturdayStartTime,'am_pm':saturdayStartAmPM,"day":"saturday"},
                            };
                            this.sharedOfficeData.monday_opening_time.push(openTime);
                            let closeTime = {'saturday':
                                    {'close_time': saturdayCloseTime,'am_pm': saturdayCloseAmPM, "day":"saturday"},
                            };
                            this.sharedOfficeData.monday_closing_time.push(closeTime);
                        } else {
                            this.arraySplice("saturday");
                            this.arraySplice1("saturday");
                        }
                    }
                    if(val === 'sun') {
                        if(event.target.checked)
                        {
                            var sundayStartTime = $('#checkedtime').val();
                            var sundayStartAmPM = $('#checkedAP').val();
                            var sundayCloseTime = $('#checkedOtime').val();
                            var sundayCloseAmPM = $('#checkedOAP').val();
                            let openTime = {'sunday':
                                    {'start_time':sundayStartTime,'am_pm':sundayStartAmPM,"day":"sunday"},
                            };
                            this.sharedOfficeData.monday_opening_time.push(openTime);
                            let closeTime = {'sunday':
                                    {'close_time': sundayCloseTime,'am_pm': sundayCloseAmPM,"day":"sunday"},
                            };
                            this.sharedOfficeData.monday_closing_time.push(closeTime);
                        } else {
                            this.arraySplice("sunday");
                            this.arraySplice1("sunday");
                        }
                    }
                    console.log({'start_time':this.sharedOfficeData.monday_opening_time});
                    console.log({'end_time':this.sharedOfficeData.monday_closing_time});
                },
                prefilledDaysChecked: function(){
                    console.log('this.sharedOfficeData.monday_opening_time');
                    console.log(this.sharedOfficeData.monday_opening_time);
                    this.sharedOfficeData.monday_opening_time.map(function (data) {
                        if(data.monday) {
                            if(data.monday.am_pm == 'OFF DAY') {
                                console.log('this is off day');
                                document.getElementById("monchecked").setAttribute('disabled', 'disabled');
                                document.getElementById("moncheckedtime").setAttribute('disabled', 'disabled');
                                document.getElementById("moncheckedtimeON").setAttribute('disabled', 'disabled');
                                document.getElementById("moncheckedtimeOff").setAttribute('disabled', 'disabled');
                                document.getElementById("moncheckedtimeOffAP").setAttribute('disabled', 'disabled');
                                $('#monchangeColor').addClass('closeChecked');
                            } else {
                                $('#monchecked').prop('checked', true);
                                $('#moncheckedtime').val(data.monday.start_time);
                                $('#moncheckedtimeON').val(data.monday.am_pm);
                            }
                        } else if(data.tuesday) {
                            if(data.tuesday.am_pm == 'OFF DAY') {
                                document.getElementById("tuechecked").setAttribute('disabled', 'disabled');
                                document.getElementById("tuecheckedtime").setAttribute('disabled', 'disabled');
                                document.getElementById("tuecheckedtimeON").setAttribute('disabled', 'disabled');
                                document.getElementById("tuecheckedtimeOff").setAttribute('disabled', 'disabled');
                                document.getElementById("tuecheckedtimeOffAP").setAttribute('disabled', 'disabled');
                                $('#tuechangeColor').addClass('closeChecked');
                            } else {
                                $('#tuechecked').prop('checked', true);
                                $('#tuecheckedtime').val(data.tuesday.start_time);
                                $('#tuecheckedtimeON').val(data.tuesday.am_pm);
                            }
                        } else if(data.wednesday) {
                            if(data.wednesday.am_pm == 'OFF DAY') {
                                document.getElementById("wedchecked").setAttribute('disabled', 'disabled');
                                document.getElementById("wedcheckedtime").setAttribute('disabled', 'disabled');
                                document.getElementById("wedcheckedtimeON").setAttribute('disabled', 'disabled');
                                document.getElementById("wedcheckedtimeOff").setAttribute('disabled', 'disabled');
                                document.getElementById("wedcheckedtimeOffAP").setAttribute('disabled', 'disabled');
                                $('#wedchangeColor').addClass('closeChecked');
                            } else {
                                $('#wedchecked').prop('checked', true);
                                $('#wedcheckedtime').val(data.wednesday.start_time);
                                $('#wedcheckedtimeON').val(data.wednesday.am_pm);
                            }
                        } else if(data.thursday) {
                            if(data.thursday.am_pm == 'OFF DAY') {
                                document.getElementById("thuchecked").setAttribute('disabled', 'disabled');
                                document.getElementById("thucheckedtime").setAttribute('disabled', 'disabled');
                                document.getElementById("thucheckedtimeON").setAttribute('disabled', 'disabled');
                                document.getElementById("thucheckedtimeOff").setAttribute('disabled', 'disabled');
                                document.getElementById("thucheckedtimeOffAP").setAttribute('disabled', 'disabled');
                                $('#thuchangeColor').addClass('closeChecked');
                            } else {
                                $('#thuchecked').prop('checked', true);
                                $('#thucheckedtime').val(data.thursday.start_time);
                                $('#thucheckedtimeON').val(data.thursday.am_pm);
                            }
                        } else if(data.friday) {
                            if(data.friday.am_pm == 'OFF DAY') {
                                document.getElementById("frichecked").setAttribute('disabled', 'disabled');
                                document.getElementById("fricheckedtime").setAttribute('disabled', 'disabled');
                                document.getElementById("fricheckedtimeON").setAttribute('disabled', 'disabled');
                                document.getElementById("fricheckedtimeOff").setAttribute('disabled', 'disabled');
                                document.getElementById("fricheckedtimeOffAP").setAttribute('disabled', 'disabled');
                                $('#frichangeColor').addClass('closeChecked');
                            } else {
                                $('#frichecked').prop('checked', true);
                                $('#fricheckedtime').val(data.friday.start_time);
                                $('#fricheckedtimeON').val(data.friday.am_pm);
                            }
                        } else if(data.saturday) {
                            if(data.saturday.am_pm == 'OFF DAY') {
                                document.getElementById("satchecked").setAttribute('disabled', 'disabled');
                                document.getElementById("satcheckedtime").setAttribute('disabled', 'disabled');
                                document.getElementById("satcheckedtimeON").setAttribute('disabled', 'disabled');
                                document.getElementById("satcheckedtimeOff").setAttribute('disabled', 'disabled');
                                document.getElementById("satcheckedtimeOffAP").setAttribute('disabled', 'disabled');
                                $('#satchangeColor').addClass('closeChecked');
                            } else {
                                $('#satchecked').prop('checked', true);
                                $('#satcheckedtime').val(data.saturday.start_time);
                                $('#satcheckedtimeON').val(data.saturday.am_pm);
                            }
                        } else if(data.sunday) {
                            if(data.saturday.am_pm == 'OFF DAY') {
                                document.getElementById("myCheck").setAttribute('disabled', 'disabled');
                                document.getElementById("checkedtime").setAttribute('disabled', 'disabled');
                                document.getElementById("checkedAP").setAttribute('disabled', 'disabled');
                                document.getElementById("checkedOtime").setAttribute('disabled', 'disabled');
                                document.getElementById("checkedOAP").setAttribute('disabled', 'disabled');
                                $('#changeColor').addClass('closeChecked');
                            } else {
                                $('#myCheck').prop('checked', true);
                                $('#checkedtime').val(data.sunday.start_time);
                                $('#checkedAP').val(data.sunday.am_pm);
                            }
                        }
                    });
                    console.log('this.sharedOfficeData.monday_closing_time');
                    console.log(this.sharedOfficeData.monday_closing_time);
                    this.sharedOfficeData.monday_closing_time.map(function (data) {
                        if(data.monday) {
                            $('#monchecked').prop('checked',true);
                            $('#moncheckedtimeOff').val(data.monday.close_time);
                            $('#moncheckedtimeOffAP').val(data.monday.am_pm);
                        } else if(data.tuesday) {
                            $('#tuechecked').prop('checked',true);
                            $('#tuecheckedtimeOff').val(data.tuesday.close_time);
                            $('#tuecheckedtimeOffAP').val(data.tuesday.am_pm);
                        } else if(data.wednesday) {
                            $('#wedchecked').prop('checked',true);
                            $('#wedcheckedtimeOff').val(data.wednesday.close_time);
                            $('#wedcheckedtimeOffAP').val(data.wednesday.am_pm);
                        } else if(data.thursday) {
                            $('#thuchecked').prop('checked',true);
                            $('#thucheckedtimeOff').val(data.thursday.close_time);
                            $('#thucheckedtimeOffAP').val(data.thursday.am_pm);
                        } else if(data.friday) {
                            $('#frichecked').prop('checked',true);
                            $('#fricheckedtimeOff').val(data.friday.close_time);
                            $('#fricheckedtimeOffAP').val(data.friday.am_pm);
                        } else if(data.saturday) {
                            $('#satchecked').prop('checked',true);
                            $('#satcheckedtimeOff').val(data.saturday.close_time);
                            $('#satcheckedtimeOffAP').val(data.saturday.am_pm);
                        } else if(data.sunday) {
                            $('#myCheck').prop('checked',true);
                            $('#checkedOtime').val(data.sunday.close_time);
                            $('#checkedOAP').val(data.sunday.am_pm);
                        }
                    });
                },
                resetFields: function() {
                    console.log('this.$data');
                    console.log(this.$data);
                    Object.assign(this.$data, this.$options.data.call(this));
                },
                changeTimeZone(event){
                    console.log(event);
                    console.log(event.target.value);
                    this.sharedOfficeData.time_zone = event.target.value;
                }
            }
        });
        //reset form data
        function resetForm(e)
        {
            e.preventDefault();
            $('#workForm').resetForm();
            $('#image_preview').attr('src','');
            document.getElementById("monchecked").checked = false;
            document.getElementById("monchecked").removeAttribute('disabled');
            document.getElementById("moncheckedtime").removeAttribute('disabled');
            document.getElementById("moncheckedtimeON").removeAttribute('disabled');
            document.getElementById("moncheckedtimeOff").removeAttribute('disabled');
            document.getElementById("moncheckedtimeOffAP").removeAttribute('disabled');
            $('#monchangeColor').removeClass('closeChecked');
            document.getElementById("tuechecked").checked = false;
            document.getElementById("tuechecked").removeAttribute('disabled');
            document.getElementById("tuecheckedtime").removeAttribute('disabled');
            document.getElementById("tuecheckedtimeON").removeAttribute('disabled');
            document.getElementById("tuecheckedtimeOff").removeAttribute('disabled');
            document.getElementById("tuecheckedtimeOffAP").removeAttribute('disabled');
            $('#tuechangeColor').removeClass('closeChecked');
            document.getElementById("wedchecked").checked = false;
            document.getElementById("wedchecked").removeAttribute('disabled');
            document.getElementById("wedcheckedtime").removeAttribute('disabled');
            document.getElementById("wedcheckedtimeON").removeAttribute('disabled');
            document.getElementById("wedcheckedtimeOff").removeAttribute('disabled');
            document.getElementById("wedcheckedtimeOffAP").removeAttribute('disabled');
            $('#wedchangeColor').removeClass('closeChecked');
            document.getElementById("thuchecked").checked = false;
            document.getElementById("thuchecked").removeAttribute('disabled');
            document.getElementById("thucheckedtime").removeAttribute('disabled');
            document.getElementById("thucheckedtimeON").removeAttribute('disabled');
            document.getElementById("thucheckedtimeOff").removeAttribute('disabled');
            document.getElementById("thucheckedtimeOffAP").removeAttribute('disabled');
            $('#thuchangeColor').removeClass('closeChecked');
            document.getElementById("frichecked").checked = false;
            document.getElementById("frichecked").removeAttribute('disabled');
            document.getElementById("fricheckedtime").removeAttribute('disabled');
            document.getElementById("fricheckedtimeON").removeAttribute('disabled');
            document.getElementById("fricheckedtimeOff").removeAttribute('disabled');
            document.getElementById("fricheckedtimeOffAP").removeAttribute('disabled');
            $('#frichangeColor').removeClass('closeChecked');
            document.getElementById("satchecked").checked = false;
            document.getElementById("satchecked").removeAttribute('disabled');
            document.getElementById("satcheckedtime").removeAttribute('disabled');
            document.getElementById("satcheckedtimeON").removeAttribute('disabled');
            document.getElementById("satcheckedtimeOff").removeAttribute('disabled');
            document.getElementById("satcheckedtimeOffAP").removeAttribute('disabled');
            $('#satchangeColor').removeClass('closeChecked');
            document.getElementById("myCheck").checked = false;
            document.getElementById("myCheck").removeAttribute('disabled');
            document.getElementById("checkedtime").removeAttribute('disabled');
            document.getElementById("checkedAP").removeAttribute('disabled');
            document.getElementById("checkedOtime").removeAttribute('disabled');
            document.getElementById("checkedOAP").removeAttribute('disabled');
            $('#changeColor').removeClass('closeChecked');
            $('#bulletIns option').prop('selected', function() {
                return this.defaultSelected;
            });
            $('#resetCheckboxex').prop('checked',false);
        }
        function resetOfficeForm(e)
        {
            e.preventDefault();
            $('#officeForm').resetForm();
        }
        $(function(){
           var selected = $('#selectedOption').click(function () {
               console.log('selected');
               console.log(selected);
               console.log('selected');
           });
        });
    </script>
    <script>
        function showDropBox() {
            $('#customDropBox').toggle();
        }

        function selectCountry(event){
            let c_name = event.target.innerHTML;
            console.log(c_name);
            let c_id;
            if(c_name == 'Malaysia') {
                c_id = 87;
            } else if(c_name == 'United States') {
                c_id = 167;
            } else if(c_name == 'United Kingdom') {
                c_id = 148;
            } else if(c_name == 'United Arab Emirates') {
                c_id = 68;
            }
            let imageSrc = $('.getValue-'+c_id).attr('src');
            console.log(c_id);
            console.log(imageSrc);
            $('.inputBox').html('');
            $('.inputBox').html('');
            let html = ' <img onclick="showDropBox()" class="customImageInput" src='+ imageSrc +' /><input type="text" name="currency_id" onclick="showDropBox()" class="form-control selectedOption" value="'+ c_name +'" /><input type="hidden" name="currency_id" class="form-control selectedOption" id="selectedOption" value="'+ c_id +'" />';
            console.log('complete html');
            console.log(html);
            $('.inputBox').html('');
            $('.inputBox').html(html);
            $('#customDropBox').hide();
        }
    </script>
@endsection

@section('content')
    <div id="app" v-cloak>
        <ul class="ulList">
            <li class="folderRectangle" v-for="(folder,index) in folders" v-bind:class="{activeClass:folder.isActive}" @click="selectedArea(index,folder.name,folder.isActive = !folder.isActive)">@{{folder.name}}</li>
        </ul>
        <form method="POST" id="sharedoffice-form" enctype="multipart/form-data"  role="form" novalidate>
            <div v-if="step === 1">
                <form id="workForm" enctype="multipart/form-data">
                    <section>
                        <h3 class="headOffice"> {{trans('sharedoffice.sharedoffice')}}</h3>
                        <p class="add-form-text">Fill in the Form to add a shared office</p>
                        <div class="form-group">
                            <label for="image" class="control-label image-tag">Image</label>
                            <div>
                                <img id="image_preview" alt="1366x768 or 683x768" src="{{ (isset($model) && $model->image) ? url($model->image) : '/images/empty.png' }}" class="img-thumbnail img-preview">
                            </div>
                            <label class="custom-file-upload">
                                <span>Upload Image</span>
                                <input type="file"
                                       accept="image/*"
                                       name="image_file"
                                       id="image_file"
                                       v-model="sharedOfficeData.image"
                                       data-target="image_preview"
                                       ref="myFile"
                                       @change="fileChanged"
                                       class="image-upload">
                            </label>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="row align-items">
                                <div class="col-md-2 custom-width">
                                    <span class="pic-select">Select Category</span>
                                </div>
                                <div class="col-md-4">
                                    <select class="form-control" v-model="sharedOfficeData.category_id">
                                    <option v-for="category in sharedOfficeData.categories" :value="category.id" :selected="category.id == sharedOfficeData.category_id ? 'selected' : ''" >@{{category.name}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="row align-items">
                                <div class="col-md-2 custom-width">
                                    <span class="pic-select">Picture size</span>
                                </div>
                                <div class="col-md-4">
                                    <select class="form-control" v-model="sharedOfficeData.pic_size">
                                        <option disabled selected value="">Please select Size</option>
                                        <option value="1">1366 X 768</option>
                                        <option value="2">683 X 768</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2 custom-width custom-padding">
                                    <span class="pic-select">Bulletins</span>
                                </div>
                                <div class="col-md-4">
                                    <select name="bulletIns[]" id="bulletIns" class="form-control" multiple v-model="sharedOfficeData.bulletins">
                                        @foreach($bulletins as $k => $v)
                                            <option value="{{$v->id}}">{{$v->text}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3 cust-width">
                                    <div class="set-checkbox">
                                        <label class="containerCheck">
                                            <input type="checkbox" v-model="sharedOfficeData.verify_info_to_scan" :value="0" v-on:click="verifyInfo($event)" checked>Verify Info
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="set-checkbox">
                                        <label class="containerCheck">
                                            <input type="checkbox" v-model="sharedOfficeData.require_deposit" :value="0" v-on:click="requestDeposite($event)" checked>Require Deposit
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2 custom-width">
                                    <span class="pic-select">Facilities</span>
                                </div>
                                <div class="col-md-10" style="display: inline-flex;">
                                    @foreach($shfacilities as $facility)
                                        <label class="fcontainerCheck">
                                            <input type="checkbox" v-model="sharedOfficeData.shfacilities" :value="'{{$facility->id}}'" v-on:click="loadData('{{ $facility->id }}',$event)" class="checkboxx-{{$facility->id}}">{{$facility->title}}
                                            <span class="checkmark2"></span>
                                        </label> &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="row align-items">
                                <div class="col-md-2 custom-width">
                                    <span class="pic-select">Location</span>
                                </div>
                                <div class="col-md-6">
                                    <i class="fa fa-search set-search"></i>
                                    <input type="text"
                                           name="city"
                                           id="pac-input"
                                           v-model="sharedOfficeData.office_location"
                                           autocomplete="on"
                                           class="form-control locations"
                                           placeholder="{{trans('sharedoffice.enterlocation')}}"
                                    />
                                    <input type="hidden" name="city_name" id="city_name">
                                    <input type="hidden" name="country_name" id="country_name">
                                    <div id="map" style="display: none"></div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="row align-items">
                                <div class="col-md-2 custom-width">
                                    <span class="pic-select">Time Zone</span>
                                </div>
                                <div class="col-md-6">
                                    <select class="form-control" id="timeZone" @change="changeTimeZone($event)" name="time_zone">
                                        @if(isset($time_zone))
                                            <option value="{{$time_zone->id}}">{{$time_zone->text}}</option>
                                        @else
                                            <option selected disabled>
                                                select time zone
                                            </option>
                                        @endif
                                        @foreach($timezone as $zone)
                                            <option value="{{$zone->id}}">{{$zone->text}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2 custom-width-working-days">
                                    <span class="pic-select">Working Day</span>
                                </div>
                                <div class="col-md-10 custom-padd">
                                    <div class="row align-items rowalign">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <tbody>
                                                <tr class="removeborder">
                                                    <th width="13%" scope="row">
                                                        <label class="fcontainerCheck">
                                                            <input type="checkbox" v-on:change="daysChecked('mon')" id="monchecked">Monday
                                                            <span class="checkmark2" id="monchangeColor"></span>
                                                        </label>
                                                    </th>
                                                    <td width="10%" class="aligncenter">
                                                        <span>Office Time</span>
                                                    </td>
                                                    <td width="8.8%">
                                                        <select class="form-control" id="moncheckedtime">
                                                            <option>1:00</option>
                                                            <option>2:00</option>
                                                            <option>3:00</option>
                                                            <option>4:00</option>
                                                            <option>5:00</option>
                                                            <option>6:00</option>
                                                            <option>7:00</option>
                                                            <option>8:00</option>
                                                            <option>9:00</option>
                                                            <option>10:00</option>
                                                            <option>12:00</option>
                                                        </select>
                                                    </td>
                                                    <td width="7.6%">
                                                        <select class="form-control" id="moncheckedtimeON">
                                                            <option>AM</option>
                                                            <option>PM</option>
                                                        </select>
                                                    </td>
                                                    <td width="2%" class="aligncenter">
                                                        <span>To</span>
                                                    </td>
                                                    <td width="8.8%">
                                                        <select class="form-control" id="moncheckedtimeOff">
                                                            <option>1:00</option>
                                                            <option>2:00</option>
                                                            <option>3:00</option>
                                                            <option>4:00</option>
                                                            <option>5:00</option>
                                                            <option>6:00</option>
                                                            <option>7:00</option>
                                                            <option>8:00</option>
                                                            <option>9:00</option>
                                                            <option>10:00</option>
                                                            <option>12:00</option>
                                                        </select>
                                                    </td>
                                                    <td width="7.6%">
                                                        <select class="form-control" id="moncheckedtimeOffAP">
                                                            <option>PM</option>
                                                            <option>AM</option>
                                                        </select>
                                                    </td>
                                                    <td width="6.6%" class="aligncenter">
                                                        <span>Off Day</span>
                                                    </td>
                                                    <td width="6.8%">
                                                        <select class="form-control"  @change="selectDays('monday',$event)">
                                                            <option value="1" selected>No</option>
                                                            <option value="2" >Yes</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr class="removeborder">
                                                    <th width="13%" scope="row">
                                                        <label class="fcontainerCheck">
                                                            <input type="checkbox"
                                                                   v-on:change="daysChecked('tue')"
                                                                   id="tuechecked">Tuesday
                                                            <span class="checkmark2" id="tuechangeColor"></span>
                                                        </label>
                                                    </th>
                                                    <td width="10%" class="aligncenter">
                                                        <span>Office Time</span>
                                                    </td>
                                                    <td width="8.8%">
                                                        <select class="form-control" id="tuecheckedtime">
                                                            <option>1:00</option>
                                                            <option>2:00</option>
                                                            <option>3:00</option>
                                                            <option>4:00</option>
                                                            <option>5:00</option>
                                                            <option>6:00</option>
                                                            <option>7:00</option>
                                                            <option>8:00</option>
                                                            <option>9:00</option>
                                                            <option>10:00</option>
                                                            <option>12:00</option>
                                                        </select>
                                                    </td>
                                                    <td width="7.6%">
                                                        <select class="form-control" id="tuecheckedtimeON">
                                                            <option>AM</option>
                                                            <option>PM</option>
                                                        </select>
                                                    </td>
                                                    <td width="2%" class="aligncenter">
                                                        <span>To</span>
                                                    </td>
                                                    <td width="8.8%">
                                                        <select class="form-control" id="tuecheckedtimeOff">
                                                            <option>1:00</option>
                                                            <option>2:00</option>
                                                            <option>3:00</option>
                                                            <option>4:00</option>
                                                            <option>5:00</option>
                                                            <option>6:00</option>
                                                            <option>7:00</option>
                                                            <option>8:00</option>
                                                            <option>9:00</option>
                                                            <option>10:00</option>
                                                            <option>12:00</option>
                                                        </select>
                                                    </td>
                                                    <td width="7.6%">
                                                        <select class="form-control" id="tuecheckedtimeOffAP">
                                                            <option>PM</option>
                                                            <option>AM</option>
                                                        </select>
                                                    </td>
                                                    <td width="6.6%" class="aligncenter">
                                                        <span>Off Day</span>
                                                    </td>
                                                    <td width="6.8%">
                                                        <select class="form-control" @change="selectDays('tuesday',$event)">
                                                            <option value="1" selected>No</option>
                                                            <option value="2" >Yes</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr class="removeborder">
                                                    <th width="13%" scope="row">
                                                        <label class="fcontainerCheck">
                                                            <input type="checkbox"
                                                                   v-on:change="daysChecked('wed')"id="wedchecked">Wednesday
                                                            <span class="checkmark2" id="wedchangeColor"></span>
                                                        </label>
                                                    </th>
                                                    <td width="10%" class="aligncenter">
                                                        <span>Office Time</span>
                                                    </td>
                                                    <td width="8.8%">
                                                        <select class="form-control" id="wedcheckedtime">
                                                            <option>1:00</option>
                                                            <option>2:00</option>
                                                            <option>3:00</option>
                                                            <option>4:00</option>
                                                            <option>5:00</option>
                                                            <option>6:00</option>
                                                            <option>7:00</option>
                                                            <option>8:00</option>
                                                            <option>9:00</option>
                                                            <option>10:00</option>
                                                            <option>12:00</option>
                                                        </select>
                                                    </td>
                                                    <td width="7.6%">
                                                        <select class="form-control" id="wedcheckedtimeON">
                                                            <option>AM</option>
                                                            <option>PM</option>
                                                        </select>
                                                    </td>
                                                    <td width="2%" class="aligncenter">
                                                        <span>To</span>
                                                    </td>
                                                    <td width="8.8%">
                                                        <select class="form-control" id="wedcheckedtimeOff">
                                                            <option>1:00</option>
                                                            <option>2:00</option>
                                                            <option>3:00</option>
                                                            <option>4:00</option>
                                                            <option>5:00</option>
                                                            <option>6:00</option>
                                                            <option>7:00</option>
                                                            <option>8:00</option>
                                                            <option>9:00</option>
                                                            <option>10:00</option>
                                                            <option>12:00</option>
                                                        </select>
                                                    </td>
                                                    <td width="7.6%">
                                                        <select class="form-control" id="wedcheckedtimeOffAP">
                                                            <option>PM</option>
                                                            <option>AM</option>
                                                        </select>
                                                    </td>
                                                    <td width="6.6%" class="aligncenter">
                                                        <span>Off Day</span>
                                                    </td>
                                                    <td width="6.8%">
                                                        <select class="form-control" @change="selectDays('wednesday',$event)">
                                                            <option value="1" selected>No</option>
                                                            <option value="2" >Yes</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr class="removeborder">
                                                    <th width="13%" scope="row">
                                                        <label class="fcontainerCheck">
                                                            <input type="checkbox" v-on:change="daysChecked('thu')"id="thuchecked">Thursday
                                                            <span class="checkmark2" id="thuchangeColor"></span>
                                                        </label>
                                                    </th>
                                                    <td width="10%" class="aligncenter">
                                                        <span>Office Time</span>
                                                    </td>
                                                    <td width="8.8%">
                                                        <select class="form-control" id="thucheckedtime">
                                                            <option>1:00</option>
                                                            <option>2:00</option>
                                                            <option>3:00</option>
                                                            <option>4:00</option>
                                                            <option>5:00</option>
                                                            <option>6:00</option>
                                                            <option>7:00</option>
                                                            <option>8:00</option>
                                                            <option>9:00</option>
                                                            <option>10:00</option>
                                                            <option>12:00</option>
                                                        </select>
                                                    </td>
                                                    <td width="7.6%">
                                                        <select class="form-control" id="thucheckedtimeON">
                                                            <option>AM</option>
                                                            <option>PM</option>
                                                        </select>
                                                    </td>
                                                    <td width="2%" class="aligncenter">
                                                        <span>To</span>
                                                    </td>
                                                    <td width="8.8%">
                                                        <select class="form-control" id="thucheckedtimeOff">
                                                            <option>1:00</option>
                                                            <option>2:00</option>
                                                            <option>3:00</option>
                                                            <option>4:00</option>
                                                            <option>5:00</option>
                                                            <option>6:00</option>
                                                            <option>7:00</option>
                                                            <option>8:00</option>
                                                            <option>9:00</option>
                                                            <option>10:00</option>
                                                            <option>12:00</option>
                                                        </select>
                                                    </td>
                                                    <td width="7.6%">
                                                        <select class="form-control" id="thucheckedtimeOffAP">
                                                            <option>PM</option>
                                                            <option>AM</option>
                                                        </select>
                                                    </td>
                                                    <td width="6.6%" class="aligncenter">
                                                        <span>Off Day</span>
                                                    </td>
                                                    <td width="6.8%">
                                                        <select class="form-control" @change="selectDays('thursday',$event)">
                                                            <option value="1" selected>No</option>
                                                            <option value="2" >Yes</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr class="removeborder">
                                                    <th width="13%" scope="row">
                                                        <label class="fcontainerCheck">
                                                            <input type="checkbox" v-on:change="daysChecked('fri')"
                                                                   id="frichecked">Friday
                                                            <span class="checkmark2" id="frichangeColor"></span>
                                                        </label>
                                                    </th>
                                                    <td width="10%" class="aligncenter">
                                                        <span>Office Time</span>
                                                    </td>
                                                    <td width="8.8%">
                                                        <select class="form-control" id="fricheckedtime">
                                                            <option>1:00</option>
                                                            <option>2:00</option>
                                                            <option>3:00</option>
                                                            <option>4:00</option>
                                                            <option>5:00</option>
                                                            <option>6:00</option>
                                                            <option>7:00</option>
                                                            <option>8:00</option>
                                                            <option>9:00</option>
                                                            <option>10:00</option>
                                                            <option>12:00</option>
                                                        </select>
                                                    </td>
                                                    <td width="7.6%">
                                                        <select class="form-control" id="fricheckedtimeON">
                                                            <option>AM</option>
                                                            <option>PM</option>
                                                        </select>
                                                    </td>
                                                    <td width="2%" class="aligncenter">
                                                        <span>To</span>
                                                    </td>
                                                    <td width="8.8%">
                                                        <select class="form-control" id="fricheckedtimeOff">
                                                            <option>1:00</option>
                                                            <option>2:00</option>
                                                            <option>3:00</option>
                                                            <option>4:00</option>
                                                            <option>5:00</option>
                                                            <option>6:00</option>
                                                            <option>7:00</option>
                                                            <option>8:00</option>
                                                            <option>9:00</option>
                                                            <option>10:00</option>
                                                            <option>12:00</option>
                                                        </select>
                                                    </td>
                                                    <td  width="7.6%">
                                                        <select class="form-control" id="fricheckedtimeOffAP">
                                                            <option>PM</option>
                                                            <option>AM</option>
                                                        </select>
                                                    </td>
                                                    <td width="6.6%" class="aligncenter">
                                                        <span>Off Day</span>
                                                    </td>
                                                    <td width="6.8%">
                                                        <select class="form-control" @change="selectDays('friday',$event)">
                                                            <option value="1" selected>No</option>
                                                            <option value="2" >Yes</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr class="removeborder">
                                                    <th width="13%" scope="row">
                                                        <label class="fcontainerCheck">
                                                            <input type="checkbox" v-on:change="daysChecked('sat')" id="satchecked" >Saturday
                                                            <span class="checkmark2" id="satchangeColor" ></span>
                                                        </label>
                                                    </th>
                                                    <td width="10%" class="aligncenter">
                                                        <span>Office Time</span>
                                                    </td>
                                                    <td width="8.8%">
                                                        <select class="form-control" id="satcheckedtime">
                                                            <option>1:00</option>
                                                            <option>2:00</option>
                                                            <option>3:00</option>
                                                            <option>4:00</option>
                                                            <option>5:00</option>
                                                            <option>6:00</option>
                                                            <option>7:00</option>
                                                            <option>8:00</option>
                                                            <option>9:00</option>
                                                            <option>10:00</option>
                                                            <option>12:00</option>
                                                        </select>
                                                    </td>
                                                    <td width="7.6%">
                                                        <select class="form-control" id="satcheckedtimeON">
                                                            <option>AM</option>
                                                            <option>PM</option>
                                                        </select>
                                                    </td>
                                                    <td width="2%" class="aligncenter">
                                                        <span>To</span>
                                                    </td>
                                                    <td width="8.8%">
                                                        <select class="form-control" id="satcheckedtimeOff">
                                                            <option>1:00</option>
                                                            <option>2:00</option>
                                                            <option>3:00</option>
                                                            <option>4:00</option>
                                                            <option>5:00</option>
                                                            <option>6:00</option>
                                                            <option>7:00</option>
                                                            <option>8:00</option>
                                                            <option>9:00</option>
                                                            <option>10:00</option>
                                                            <option>12:00</option>
                                                        </select>
                                                    </td>
                                                    <td  width="7.6%">
                                                        <select class="form-control" id="satcheckedtimeOffAP">
                                                            <option>PM</option>
                                                            <option>AM</option>
                                                        </select>
                                                    </td>
                                                    <td width="6.6%" class="aligncenter">
                                                        <span>Off Day</span>
                                                    </td>
                                                    <td width="6.8%">
                                                        <select class="form-control" id="satSelectDays" @change="selectDays('saturday',$event)">
                                                            <option value="1" selected>No</option>
                                                            <option value="2">Yes</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr class="removeborder">
                                                    <th width="13%" scope="row" id="checkedday" class="checkeddd">
                                                        <label class="fcontainerCheck">
                                                            <input type="checkbox" v-on:change="daysChecked('sun')" id="myCheck">Sunday
                                                            <span class="checkmark2" id="changeColor"></span>
                                                        </label>
                                                    </th>
                                                    <td width="10%" class="aligncenter">
                                                        <span>Office Time</span>
                                                    </td>
                                                    <td width="8.8%" class="checkeddd">
                                                        <select class="form-control" id="checkedtime">
                                                            <option>1:00</option>
                                                            <option>2:00</option>
                                                            <option>3:00</option>
                                                            <option>4:00</option>
                                                            <option>5:00</option>
                                                            <option>6:00</option>
                                                            <option>7:00</option>
                                                            <option>8:00</option>
                                                            <option>9:00</option>
                                                            <option>10:00</option>
                                                            <option>12:00</option>
                                                        </select>
                                                    </td>
                                                    <td width="7.6%" class="checkeddd">
                                                        <select class="form-control" id="checkedAP">
                                                            <option>AM</option>
                                                            <option>PM</option>
                                                        </select>
                                                    </td>
                                                    <td width="2%" class="aligncenter" id="checked">
                                                        <span>To</span>
                                                    </td>
                                                    <td width="8.8%" class="checkeddd">
                                                        <select class="form-control" id="checkedOtime">
                                                            <option>1:00</option>
                                                            <option>2:00</option>
                                                            <option>3:00</option>
                                                            <option>4:00</option>
                                                            <option>5:00</option>
                                                            <option>6:00</option>
                                                            <option>7:00</option>
                                                            <option>8:00</option>
                                                            <option>9:00</option>
                                                            <option>10:00</option>
                                                            <option>12:00</option>
                                                        </select>
                                                    </td>
                                                    <td  width="7.6%" class="checkeddd">
                                                        <select class="form-control" id="checkedOAP">
                                                            <option>PM</option>
                                                            <option>AM</option>
                                                        </select>
                                                    </td>
                                                    <td width="6.6%" class="aligncenter">
                                                        <span>Off Day</span>
                                                    </td>
                                                    <td width="6.8%">
                                                        <select class="form-control" id="sunSelectDays" @change="selectDays('sunday',$event)">
                                                            <option value="1"  selected>No</option>
                                                            <option value="2" >Yes</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </form>
                <div class="row customClassRowR" style="float:right">
                    <button @click.prevent="next()" class="colorsss" style="right: 121px;">Save & Next</button>
                    <button onclick="resetForm(event)" class="Cancelcolor">Cancel</button>
                </div>

            </div>

            <div v-if="step === 2">
                <form id="officeForm">
                    <section>
                        <h3 class="headOffice"> {{trans('sharedoffice.sharedoffice')}}</h3>
                        <p class="add-form-text">Fill in the Form to add a shared office</p>
                        <br>
                        <h2 class="step-heading">Office Manager</h2>
                        <div class="form-group">
                            <div class="row align-items">
                                <div class="col-md-3 custom-width">
                                    <span class="pic-select">Office Manager Email</span>
                                </div>
                                <div class="col-md-5">
                                    <input type="text" name="office_manger"
                                           class="form-control"
                                           v-model="sharedOfficeData.office_manager_email"
                                           placeholder="{{trans('sharedoffice.shared_office_manager_email')}}">
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="row align-items">
                                <div class="col-md-3 custom-width">
                                    <span class="pic-select">Language Preference</span>
                                </div>
                                <div class="col-md-2">
                                    <label class="fcontainerCheck">
                                        <input type="checkbox" name="english" value="1" v-model="sharedOfficeData.version_english" @change="changeVersion" >English
                                        <span class="checkmark2"></span>
                                    </label>
                                </div>
                                <div class="col-md-2">
                                    <label class="fcontainerCheck">
                                        <input type="checkbox" name="chinese" value="2" v-model="sharedOfficeData.version_chinese" @change="changeVersion" checked >Chinese
                                        <span class="checkmark2"></span>
                                    </label>
                                </div>
                                <div class="col-md-2">
                                    <label class="fcontainerCheck">
                                        <input type="checkbox" name="other">Other
                                        <span class="checkmark2"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <h2 class="step-heading">Office Details</h2>
                        <div v-if="this.sharedOfficeData.version_english">
                            <div class="form-group">
                                <div class="row align-items">
                                    <div class="col-md-2 custom-width">
                                        <span class="pic-select">Office Name</span>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="office_name"
                                               class="form-control"
                                               v-model="sharedOfficeData.office_name"
                                               placeholder="{{trans('sharedoffice.enterofficename')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2 custom-width">
                                        <span class="pic-select">Description</span>
                                    </div>
                                    <div class="col-md-4">
                                    <textarea name="description"
                                              class="form-control"
                                              v-model="sharedOfficeData.description"
                                              placeholder="{{trans('sharedoffice.enterdescription')}}"></textarea>
                                    </div>
                                    <div class="col-md-2 custom-width m-t-r">
                                        <span class="pic-select">Office Space<span class="italic_text">(i.e 2432m2)</span> </span>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" v-model="sharedOfficeData.office_space">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <h2 class="step-heading">Contact Details</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="row align-items">
                                            <div class="col-md-3 custom-width">
                                                <span class="pic-select">Name</span>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" name="contact_name" class="form-control"  v-model="sharedOfficeData.contact_name">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row align-items">
                                            <div class="col-md-3 custom-width">
                                                <span class="pic-select">Email</span>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="email" name="contact_email" class="form-control"  v-model="sharedOfficeData.contact_email">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row align-items">
                                            <div class="col-md-3 custom-width">
                                                <span class="pic-select">Phone</span>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="phone" name="contact_phone" class="form-control"  v-model="sharedOfficeData.contact_phone">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-2 custom-width">
                                                <span class="pic-select">Address</span>
                                            </div>
                                            <div class="col-md-10">
                                                <textarea name="contact_address" class="form-control"  v-model="sharedOfficeData.contact_address"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div v-if="this.sharedOfficeData.version_chinese">
                            <div class="form-group">
                                <div class="row align-items">
                                    <div class="col-md-2 custom-width">
                                        <span class="pic-select">Office Name</span>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="office_name"
                                               v-model="sharedOfficeData.office_name_cn"
                                               placeholder="{{trans('sharedoffice.enterofficename')}} (cn)"  class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2 custom-width">
                                        <span class="pic-select">Description</span>
                                    </div>
                                    <div class="col-md-4">
                                        <textarea class="form-control"  name="description" v-model="sharedOfficeData.description_cn" placeholder="{{trans('sharedoffice.enterdescription')}} (cn)"></textarea>
                                    </div>
                                    <div class="col-md-2 custom-width m-t-r">
                                        <span class="pic-select">Office Space<span class="italic_text">(i.e 2432m2)</span> </span>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" id="office_space" class="form-control" v-model="sharedOfficeData.office_space_cn">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <h2 class="step-heading">Contact Details</h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="row align-items">
                                            <div class="col-md-3 custom-width">
                                                <span class="pic-select">Name (cn)</span>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text"  name="contact_name" class="form-control"  v-model="sharedOfficeData.contact_name_cn">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row align-items">
                                            <div class="col-md-3 custom-width">
                                                <span class="pic-select">Email (cn)</span>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="email" name="contact_email" class="form-control"  v-model="sharedOfficeData.contact_email_cn">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row align-items">
                                            <div class="col-md-3 custom-width">
                                                <span class="pic-select">Phone (cn)</span>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="number" name="contact_phone" class="form-control"  v-model="sharedOfficeData.contact_phone_cn">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-2 custom-width">
                                                <span class="pic-select">Address (cn)</span>
                                            </div>
                                            <div class="col-md-10">
                                                <textarea class="form-control" name="contact_address" class="form-control"  v-model="sharedOfficeData.contact_address_cn"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </section>
                </form>
                <div class="row customClassRowR" style="float:right">
                    <button @click.prevent="next()" class="colorsss" style="right: 121px;">Save & Next</button>
                    <button onclick="resetFormStep2(event)" class="Cancelcolor">Cancel</button>
                </div>
            </div>

            <div v-if="step === 3">
                <section>
                    <h3 class="headOffice"> {{trans('sharedoffice.sharedoffice')}}</h3>
                    <p class="add-form-text">Fill in the Form to add a shared office</p>
                    <br>
                    <br>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2 currency-width">
                                <span class="pic-select set-css-currency">Currency</span>
                            </div>
                            <div class="col-md-4">
                                <div class="inputBox">
                                    @if(isset($currency_name))
                                        {{$currency_name->emoji}}
                                        <input type="text" name="currency_id" onclick="showDropBox()" class="form-control selectedOption" value="{{$currency_name->name}}" />
                                        <input type="hidden" name="currency_id" class="form-control selectedOption" id="selectedOption" value="{{$currency_name->id}}" />
                                    @else
                                        <input type="text" name="currency_id" onclick="showDropBox()" class="form-control selectedOption" value="" />
                                    @endif
                                </div>
                                <div class="dropbox customDropBox" id="customDropBox" style="display: none;">
                                    <ul class="customUl">
                                        <li onclick="selectCountry(event)"><img class="getValue-87 customImage" src="/images/Flags-Icon-Set/24x24/US.png" /> <div class="customName" id="87">Malaysia</div></li>
                                        <li onclick="selectCountry(event)"><img class="getValue-167 customImage" src="/images/Flags-Icon-Set/24x24/US.png" /> <div class="customName" id="167">United States</div></li>
                                        <li onclick="selectCountry(event)"><img class="getValue-148 customImage" src="/images/Flags-Icon-Set/24x24/SH.png" /> <div class="customName" id="148">United Kingdom</div></li>
                                        <li onclick="selectCountry(event)"><img class="getValue-68 customImage" src="/images/Flags-Icon-Set/24x24/AE.png" /><div class="customName" id="68">United Arab Emirates</div></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="form-group">
                        <div class="row align-items">
                            <div class="col-md-2 currency-width">
                                <span class="pic-select set-css-officeFee">Office fee</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Type</th>
                                    <th>People</th>
                                    <th>Hourly Price</th>
                                    <th>Daily Price</th>
                                    <th>Weekly Price</th>
                                    <th>Monthly Price</th>
                                    <th>Availability</th>
                                    <th colspan="2">Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                <template v-for="(feeHotDesk , index) in feeHotDesks">
                                    <tr class="setInput">
                                        <td v-if="index == 0" class="setFeedDesks" :rowspan="feeHotDesks.length" style="vertical-align: middle;">Hot Desks</td>
                                        <input type="hidden" name="type" :value="feeHotDesk.type" v-model="feeHotDesk.type">
                                        <input type="hidden" name="type" :value="feeHotDesk.number" v-model="feeHotDesk.number">
                                        <td><input type="text" name="hot_desk_peoples" v-model="feeHotDesk.no_of_peoples" class="form-control"></td>
                                        <td><input type="text" name="hot_desk_hourly_price" v-model="feeHotDesk.hourly_price" class="form-control"></td>
                                        <td><input type="text" name="hot_desk_daily_price" v-model="feeHotDesk.daily_price" class="form-control"></td>
                                        <td><input type="text" name="hot_desk_weekly_price" v-model="feeHotDesk.weekly_price" class="form-control"></td>
                                        <td><input type="text" name="hot_desk_monthly_price" v-model="feeHotDesk.month_price" class="form-control"></td>
                                        <td><input type="text" name="hot_desk_is_available" v-model="feeHotDesk.availability_id" class="form-control"></td>
                                        <td v-if="feeHotDesks.length > 1">
                                            <button type="button" class="buttonSbtruct" @click="removeElmHotDesk(feeHotDesk.id, index)">-</button>
                                        </td>
                                        <td v-if="index == 0" :rowspan="feeHotDesks.length" style="vertical-align: middle;text-align:center;">
                                            <button type="button" class="buttonadd" @click="addSpaceHotDesk">+</button>
                                        </td>
                                    </tr>
                                </template>
                                <template v-for="(feeDedicatedDesk, index) in feeDedicatedDesks">
                                    <tr class="setInput">
                                        <td v-if="index == 0" class="setDedicatedDesks" :rowspan="feeDedicatedDesks.length" style="vertical-align: middle;">Dedicated Desks</td>
                                        <input type="hidden" name="type" v-model="feeDedicatedDesk.type">
                                        <input type="hidden" name="number" :value="feeDedicatedDesk.number" v-model="feeDedicatedDesk.number">
                                        <td><input type="text" name="dedicated_desk_peoples" v-model="feeDedicatedDesk.no_of_peoples" class="form-control"></td>
                                        <td><input type="text" name="dedicated_desk_hourly_price" v-model="feeDedicatedDesk.hourly_price" class="form-control"></td>
                                        <td><input type="text" name="dedicated_desk_daily_price" v-model="feeDedicatedDesk.daily_price" class="form-control"></td>
                                        <td><input type="text" name="dedicated_desk_weekly_price" v-model="feeDedicatedDesk.weekly_price" class="form-control"></td>
                                        <td><input type="text" name="dedicated_desk_monthly_price" v-model="feeDedicatedDesk.month_price" class="form-control"></td>
                                        <td><input type="text" name="dedicated_desk_is_available" v-model="feeDedicatedDesk.availability_id" class="form-control"></td>
                                        <td v-if="feeDedicatedDesks.length > 1"><button type="button" class="buttonSbtruct" @click="removeElmDediDesk(feeDedicatedDesk.id, index)">-</button></td>
                                        <td v-if="index == 0" :rowspan="feeDedicatedDesks.length" style="vertical-align: middle;text-align:center;">
                                            <button type="button" class="buttonadd" @click="addSpaceDediDesk">+</button>
                                        </td>
                                    </tr>
                                </template>
                                <template v-for="(feeOfficeDesk, index) in feeOfficeDesks">
                                    <tr class="setInput">
                                        <td v-if="index == 0" class="setOfficeDesks" :rowspan="feeOfficeDesks.length" style="vertical-align: middle;">Office Desks</td>
                                        <input type="hidden" name="type" v-model="feeOfficeDesk.type">
                                        <input type="hidden" name="number" :value="feeOfficeDesk.number" v-model="feeOfficeDesk.number">
                                        <td><input type="text" name="office_desk_peoples" v-model="feeOfficeDesk.no_of_peoples" class="form-control"></td>
                                        <td><input type="text" name="office_desk_hourly_price" v-model="feeOfficeDesk.hourly_price" class="form-control"></td>
                                        <td><input type="text" name="office_desk_daily_price" v-model="feeOfficeDesk.daily_price" class="form-control"></td>
                                        <td><input type="text" name="office_desk_weekly_price" v-model="feeOfficeDesk.weekly_price" class="form-control"></td>
                                        <td><input type="text" name="office_desk_monthly_price" v-model="feeOfficeDesk.month_price" class="form-control"></td>
                                        <td><input type="text" name="office_desk_is_available" v-model="feeOfficeDesk.availability_id" class="form-control"></td>
                                        <td v-if="feeOfficeDesks.length > 1">
                                            <button type="button" class="buttonSbtruct" @click="removeElmOfficeDesk(feeOfficeDesk.id, index)">-</button>
                                        </td>
                                        <td v-if="index == 0" :rowspan="feeOfficeDesks.length" style="vertical-align: middle;text-align:center;">
                                            <button type="button" class="buttonadd" @click="addSpaceOfficeDesk">+</button>
                                        </td>
                                    </tr>
                                </template>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br>
                    <button type="button" class="btn btn-primary btn-sm" id="btnclick" onclick="toggleBox()"></button>
                    <div id="showAdvanced" style="display: none">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3 class="set-h3Rows">Office Size : 3000 X  &nbsp;|&nbsp;  Rows : 00  &nbsp;|&nbsp;  Columns : 00</h3>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row align-items">
                                <div class="col-md-2 custom-width-row">
                                    <span class="pic-select">Row</span>
                                </div>
                                <div class="col-md-3">
                                    <input type="text"
                                           name="office_size_x"
                                           class="form-control"
                                           v-model="sharedOfficeData.office_size_x"
                                           placeholder="Enter number rows x"
                                           autoComplete="off"
                                           v-on:blur="createGrid"
                                           v-validate="'required'"/>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-2 custom-width-column">
                                    <span class="pic-select">Columns</span>
                                </div>
                                <div class="col-md-3">
                                    <input type="text"
                                           name="office_size_y"
                                           class="form-control"
                                           v-model="sharedOfficeData.office_size_y"
                                           autoComplete="off"
                                           placeholder="Enter number rows y"
                                           v-on:blur="createGrid"
                                           v-validate="'required'"
                                    />
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                        <div class="col-md-6 facility_number_custom_right">
                            <div class="row">
                                <div class="col-md-3 set-custom-width-info">
                                    <span class="pic-select">Information</span>
                                </div>
                                <div class="col-md-6">
                                    <select v-model="facility_number" class="form-control" v-on:blur="createGrid" v-validate="'required'">
                                        <option disabled value="">Please select Type</option>
                                        @foreach($sharedOfficeProductCategories as $k => $v)
                                            <option value="{{$v->id}}">{{$v->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <input v-if="this.sharedOfficeData.office_size_x != '' && this.sharedOfficeData.office_size_y != ''"
                                           type="text"
                                           name="office_size_y"
                                           class="form-control"
                                           v-model="seat_number"
                                           autoComplete="off"
                                           placeholder="Seat Number"
                                           v-on:blur="createGrid"
                                           v-validate="'required'"/>
                                </div>
                                <div class="col-md-4">
                                    <input  v-if="this.sharedOfficeData.office_size_x != '' && this.sharedOfficeData.office_size_y != ''" type="button" class="btn btn-success" value="Save" @click="setSeatData">
                                    <br  v-if="this.sharedOfficeData.office_size_x != '' && this.sharedOfficeData.office_size_y != ''"/>
                                    <br  v-if="this.sharedOfficeData.office_size_x != '' && this.sharedOfficeData.office_size_y != ''"/>
                                </div>
                            </div>

                            <div class="row">
                                <tr class="col-md-12">
                                    <table  v-if="this.sharedOfficeData.office_size_x != '' && this.sharedOfficeData.office_size_y != ''" >
                                        @php
                                            $d = $sharedOfficeProductCategories->toArray();
                                        @endphp
                                        @foreach(array_chunk($d, 3) as $chunk)
                                            <tr>
                                                @foreach($chunk as $add)
                                                    @php
                                                        $name = explode('.', $add['name']);
                                                        $name =  $name[1];
                                                    @endphp
                                                    <td width="30px"><img src="{{asset($add['image'])}}" style="width: 30px" alt=""></td>
                                                    <td>{{$name}}</td>
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    </table>
                            </div>
                        </div>
                        <div class="col-md-6 innerboxes-main-custom" style="overflow: auto; max-height: 300px">
                            <div class="form-group boxes" :style="getWidth()" v-for="(n, x) in this.sharedOfficeData.office_size_y" style="overflow: auto; min-width: 100%;">
                                <div class="innerboxes" v-for="seatData in  sharedOfficeData.oldSeatsData" @click="checked(seatData.x, seatData.y)" v-if="">
                                    <img  style='width: 34px; height:34px;' :src="getSeatImage(seatData)" />
                                </div>
                                <div class="innerboxes" v-for="(n1, y) in getX()" @click="checked(x, y)" :id="x+'_'+y" :data-x="x" :data-y="y" v-if="">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 active-checkbox">
                            <label class="fcontainerCheck">
                                <input type="checkbox" @click="checkedboxval()"  v-model="sharedOfficeData.statusChecked">Active
                                <span class="checkmark2"></span>
                            </label>
                        </div>
                    </div>
                    </div>
                </section>
                <div class="row customClassRowR" style="float:right">
                    <button @click.prevent="prev()" class="colorsss" style="right: 121px;">Previous</button>
                    <input type="submit" name="submit" class="Cancelcolor" value="Save" @click.prevent="createSharedOffice" />
                </div>
            </div>
        </form>

        <br/><br/>
    </div>
    <script>
        let testBool = true;
        console.log('Default value of bool is',
            testBool);
        $("#btnclick").html('+');
        function toggleBox(){
            testBool = testBool ? false : true;
            console.log('Toggled bool is',
                testBool);
            $("#showAdvanced").slideToggle("slow");
            if(testBool === true) {
                $("#btnclick").html('+');
            } else {
                $("#btnclick").html('-');
            }
        }
    </script>
@endsection

@section('modal')

@endsection
