@extends('backend.layout')

@section('title')
    {{trans('sharedoffice.sharedOfficeCheckIn')}}
@endsection

@section('description')
    {{trans('sharedoffice.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>

        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.sharedOfficeCheckIn')}}</a>
        </li>
    </ul>

@endsection

@section('header')
    <style>
        .img-thumbnail {
            border: 0;
        }
        .someStyle{
            border-top-right-radius: 0px;
            border-bottom-right-radius: 0px;
        }
        .btn-circle{
            border-radius:0px !important;
            display: flex;
            align-items:center;
        }
        .formStyle {
            display: flex;
        }
    </style>
    @if(Auth::guard('staff')->user()->staff_type == \App\Models\Staff::STAFF_TYPE_STAFF)
        <link href="{{asset('custom/css/backend/staffDashboard.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('custom/css/backend/staffHeader.css')}}" rel="stylesheet" type="text/css">
    @endif
@endsection

@section('footer')

@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('sharedoffice.sharedOfficeCheckIn')}}</span>
            </div>

            <div class="actions" style="display: flex;">
                <form action="{{route('sharedofficeCheckIn')}}" method="get" class="formStyle">
                    <input type="text" name="username" class="form-control someStyle" placeholder="{{trans('common.en_u')}}">
                    <input type="submit" class="btn btn-circle red-sunglo btn-sm" value="{{trans('sharedoffice.submit')}}" />
                </form>
            </div>
        </div>
        <div class="portlet-body">
            @if($username)
            <div class="table-responsive">
                @if(count($checkIn) >= 1 )
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{trans('common.u_n')}}</th>
                            <th>{{trans('common.o_n')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($checkIn as $users)
                        <tr>
                            <td>{{$users['count']}}</td>
                            <td>{{$users['username']}}</td>
                            <td>{{$users['office_name']}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @else
                    User not found
                @endif
            </div>
            @else
                No Record Found
            @endif
        </div>
    </div>
@endsection

@section('modal')

@endsection
