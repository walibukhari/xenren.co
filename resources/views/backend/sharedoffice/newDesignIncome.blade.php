@extends('backend.layout')

@section('title')
    {{--{{trans('sharedoffice.sharedoffice')}}--}}
@endsection

@section('description')
    {{--{{trans('sharedoffice.crud')}}--}}
@endsection

@section('header')
    <link href="{{asset('css/income_admin.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/dateTimePicker.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('footer')
    <style>
        .daterangepicker .ranges {
            float: right !important;
        }
    </style>
    <script src="{{asset('js/vue.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vue-resource.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('custom/js/bs-modal-bootstrap.js')}}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script type="text/javascript">
        $(function() {
            $('#startedDate').daterangepicker({
                ranges: {
                    'Daily Sales': [moment(), moment()],
                    // 'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'This Week': [moment().startOf('week'), moment().endOf('week')],
                    'Last 90 Days': [moment().subtract(90, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'This Year': [moment().startOf('year'), moment().endOf('year')],
                },
                "alwaysShowCalendars": true,
                "maxDate":new Date(),
            }, function(start, end, label) {
                console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');

                let startDate = start.format('YYYY-MM-DD');
                let endDate = end.format('YYYY-MM-DD');
                $('#start_date').val(startDate);

                if(startDate != endDate) {
                    console.log('both dates are not same');
                    console.log('going to set end date');
                    console.log(end.format('MM/DD/YYYY'));
                    $('#endedDate').daterangepicker({
                        startDate: start,
                        endDate: end
                    });
                }

                vu.getDetail(startDate,endDate);

            });


            $('#endedDate').daterangepicker({
                ranges: {
                    'Daily Sales': [moment(), moment()],
                    // 'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'This Week': [moment().startOf('week'), moment().endOf('week')],
                    'Last 90 Days': [moment().subtract(90, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'This Year': [moment().startOf('year'), moment().endOf('year')],
                },
                "alwaysShowCalendars": true,
                "maxDate":new Date(),
            }, function(start, end, label) {
                console.log('start');
                console.log(start.format('MM/DD/YYYY'));
                let startD = start.format('MM/DD/YYYY');
                let startDate = formatDateForVue(sd);
                let endDate = startD;
                $('#end_date').val(endDate);
                console.log(startDate,endDate);

                if(startDate != endDate) {
                    console.log('both dates are not same');
                    console.log('going to set start date');
                    console.log(end.format('MM/DD/YYYY'));
                    $('#startedDate').daterangepicker({
                        startDate: start,
                        endDate: end
                    });
                }
                vu.getDetail(startDate,endDate);

            });

            function formatDateForVue(date) {
                var dd = date.getDate();
                var mm = date.getMonth() + 1; //January is 0!
                var yyyy = date.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (mm < 10) {
                    mm = '0' + mm;
                }
                return  dd + '-' + mm + '-' + yyyy;
            }
            
            let sdate = $('#startedDate').val();
            let edate = $('#endedDate').val();
            let sd = new Date(sdate);
            let ed = new Date(edate);
            let startDate = formatDateForVue(sd);
            let endDate = formatDateForVue(ed);
            $('#start_date').val(startDate);
            $('#end_date').val(endDate);
            // vu.getDetail(new Date(),new Date());
        });
    </script>

<script>

        let vu = new Vue({
            el: '#vueJs',
            data: {
                products: [],
                totalAmount: 0,
            },
            methods: {
                getDetail(startDate,endDate) {
                    startDate = new Date(startDate).toDateString();
                    endDate = new Date(endDate).toDateString();
                    this.showLoader = true;
                    let self = this;
                    setTimeout(function () {
                        self.$http.get('/admin/sharedOfficeFinances/{{$id}}?startdate=' + startDate+'&enddate='+endDate).then(function (response) {
                            self.showLoader = false;
                            self.products = response.body.data;
                            self.totalAmount = response.body.totalAmount;
                        }, function (error) {
                            self.showLoader = false;
                        });
                    }, 100)
                }
            },
            mounted() {
                let startDate = new Date().toDateString();
                let endDate = new Date().toDateString();
                console.log('_____________________');
                console.log(startDate);
                console.log('________________________');
                console.log(endDate);
                this.getDetail(startDate,endDate);
            }
        });
    </script>
@endsection

@section('content')
<div id="vueJs">
    <input type="hidden" id="start_date">
    <input type="hidden" id="end_date">
    <div class="row">
        <div class="r-0">
            <div class="col-md-3 custom-padding">
                <div class="box-one">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                    <span>Shared office editing</span>
                </div>
            </div>
            <div class="col-md-3 custom-padding">
                <div class="box-two">
                    <img src="{{asset('images/timer.png')}}">
                    <span>Rent products by time</span>
                </div>
            </div>
            <div class="col-md-2 custom-width">
                <div class="box-three">
                    <i class="fa fa-dollar"></i>
                    <span>Income</span>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="custom-breadcrumb">
                <p>
                    <a href="">Shared office dashboard</a> | <a href="">Shared office</a> | <span>Income</span>
                </p>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="r-1">
            <div class="col-md-3 custom-width-section-two">
                <div class="date-time-box">
                    <div class="right-sec">
                        <h3>Start</h3>
                        <input type="text" class="reportrange" id="startedDate" placeholder="06/01/2019" readonly>
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                    </div>
                    <span></span>
                    <div class="left-sec">
                        <h3>End</h3>
                        <input type="text" class="reportrange" id="endedDate" value="" placeholder="06/01/2019" readonly>
                        <span class="reportrange" id="endDateAppend" style="display:none;"></span>
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        <i class="fa fa-times" id="set-fa-times" aria-hidden="true" style="display:none;"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-6 set-custom-padd-col-5">
                <div class="custom-search-box">
                    <input type="text" placeholder="Search" name="search" class="form-control">
                    <i class="fa fa-search"></i>
                </div>
            </div>
            <div class="col-md-1 custom-width-col-firs">
                <div class="filter-box">
                    <i class="fa fa-filter" aria-hidden="true"></i>
                    <br>
                    <span>Filters</span>
                </div>
            </div>
            <div class="col-md-1 custom-width-col-sec">
                <div class="statements-box">
                    <i class="fa fa-bars"></i>
                    <br>
                    <span>Statements</span>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="portlet light bordered">
        <div class="portlet-body">
            <div class="table-responsive tabled">
                <h1>June 2019</h1>
                <table class="table table-striped set-table-striped">
                    <tbody>

                    <tr v-for="(data,index) in products" style="background-color:#f5f6f7">
                        <td class="custom-align-style">
                            @{{data.endDate}}
                        </td>
                        <td class="custom-style">
                            @{{data.user}}<br>
                            <span>Payment</span>
                        </td>
                        <td class="custom-color-style">
                            @{{data.cost}}<br>
                            <span>
                                USD
                            </span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="total-price">
                <span>Total : <span class="total-bill">@{{totalAmount}}</span></span>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('modal')
@endsection
