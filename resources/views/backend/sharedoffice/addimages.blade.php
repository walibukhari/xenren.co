@extends('backend.layout')

@section('title')
    {{trans('sharedoffice.sharedoffice')}}
@endsection


@section('description')

@endsection

@section('header')
    <style>
        .rowImage {
            margin:0px;
            margin-bottom: 15px;
            padding-left:12px;
            padding-right:12px;
            padding-top: 20px;
        }
        .percent-count{
            width: 70%;
            height: 21px;
            text-align: center;
            color: #555;
            position: absolute;
            font-size: 12px;
        }
        .progress-bar {
            width:100%;
            height:21px;
            background: #bbb;
            border-radius:15px;
            padding:2px;
        }
        .progress {
            width:6%;
            height:20px;
            border-radius:10px;
            background: dodgerblue;
        }
        .containerProgress{
            padding-left: 15px;
            padding-right: 15px;
        }
        .customFaPlus {
            width: 22px;
            height: 22px;
            background: #C0C0C0;
            padding: 6px;
            border-radius: 20px;
        }
        /* custom input type file styling */
        .fileContainer {
            overflow: hidden;
            position: relative;
            height: 75px;
            width: 75px;
            margin-top: 20px;
            justify-content: center;
            display: inline-flex;
            align-items: center;
            background: #F5F5F5;
            border-radius: 10px;
        }
        .customAdd{
            color: #C0C0C0;
            font-size: 11px;
            font-weight: bold;
            position: relative;
            top: 3px;
            text-align: center;
        }

        .fileContainer [type=file] {
            cursor: inherit;
            display: block;
            font-size: 999px;
            filter: alpha(opacity=0);
            min-height: 100%;
            min-width: 100%;
            opacity: 0;
            position: absolute;
            right: 0;
            text-align: right;
            top: 0;
        }
        .set-remove {
            display: inline-block;
            text-align: center;
            cursor: pointer;
            position: relative;
            top: -85px;
            right: -63px;
        }
        .set-removeWeb{
            display: inline-block;
            text-align: center;
            cursor: pointer;
            position: relative;
            top: -221px;
            right: -471px;
        }
        .set-remove1{
            display: inline-block;
            text-align: center;
            cursor: pointer;
            position: relative;
            top: -220px;
        }

        .lds-ring {
            position: relative;
            width: 100%;
            height: 80px;
            margin: 150px auto;
            display: flex;
            justify-content: center;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 64px;
            height: 64px;
            margin: 8px;
            border: 4px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #555 transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }


        .lds-ring-ring {
            position: relative;
            width: 100%;
            height: 50px;
            margin: 0px auto;
            display: flex;
            justify-content: center;
        }
        .lds-ring-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 64px;
            height: 64px;
            margin: 8px;
            border: 4px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #555 transparent transparent transparent;
        }
        .lds-ring-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
        .lds-ring-loader {
            position: relative;
            width:75px;
            height: 75px;
            display: flex;
            justify-content: center;
            align-items: center;
            border: 1px solid rgb(192, 192, 192);
            border-radius:10px;
            margin-right: 10px;
            margin-bottom: 15px;
        }
        .lds-ring-loader div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 20px;
            height: 20px;
            margin: 0px;
            border: 1px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #555 transparent transparent transparent;
        }
        .lds-ring-loader div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring-loader div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring-loader div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

        .lds-ring-loader-video {
            position: relative;
            width:100%;
            height: 205px;
            display: flex;
            justify-content: center;
            align-items: center;
            border: 1px solid rgb(192, 192, 192);
            border-radius:10px;
            margin-right: 10px;
            margin-bottom: 15px;
        }
        .lds-ring-loader-video div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 20px;
            height: 20px;
            margin: 0px;
            border: 1px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #555 transparent transparent transparent;
        }
        .lds-ring-loader-video div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring-loader-video div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring-loader-video div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
    <style>
        .pcWeb{
            display: block !important;
        }
        .mobileWeb{
            display: none !important;
        }
        @media (max-width: 460px) {
            .percent-count{
                width: 94%;
                height: 21px;
                text-align: center;
                color: #555;
                position: absolute;
                font-size: 12px;
            }
            .fileContainer2{
                overflow: hidden;
                position: relative;
                height: 45px;
                width: 175px;
                margin-top: 20px;
                justify-content: center;
                display: inline-flex;
                align-items: center;
                background: #F5F5F5;
                border-radius: 10px;
            }
            .fileContainer2 [type=file] {
                cursor: inherit;
                display: block;
                font-size: 999px;
                filter: alpha(opacity=0);
                min-height: 100%;
                min-width: 100%;
                opacity: 0;
                position: absolute;
                right: 0;
                text-align: right;
                top: 0;
            }
            .pcWeb{
                display: none !important;
            }
            .mobileWeb{
                display: block !important;
                padding: 0px;
            }
            .page-content-wrapper .page-content {
                padding: 0px!important;
                overflow: hidden;
            }
            .control-label {
                margin-top: 1px;
                font-weight: 400;
                padding: 20px;
            }
            .form .form-body, .portlet-form .form-body{
                padding: 0px;
            }
            .fileContainer {
                overflow: hidden;
                position: absolute;
                height: 33px;
                width: 75px;
                margin-top: 0px;
                justify-content: center;
                display: inline-flex;
                align-items: center;
                background: rgba(252, 252, 252, 0.5);
                border-radius: 2px;
                right: 1px;
                margin-top: -34px;
            }
            .customSizeSelect{
                padding: 20px;
                margin-bottom: 0px;
                padding-bottom: 10px;
            }
            .customUload{
                padding: 20px;
                margin-bottom: 0px;
                padding-bottom: 0px;
                padding-top: 0px;
            }
            .customClassBtn{
                display: flex;
                justify-content: center;
            }
            .customClassBtn1{
                display: flex;
                justify-content: center;
                margin-top: 30px;
            }

            .plusion{
                width: 15px;
                height: 15px;
                margin-right: 10px;
            }
            .customBtnClass{
                width: 175px;
                display: flex;
                justify-content: center;
                border-radius: 5px !important;
                background: #e6e6e6 !important;
                align-items: center;
            }
            .customBtnClass1
            {
                width: 200px;
                display: flex;
                justify-content: center;
                border-radius: 5px !important;
                background: #53C329;
                align-items: center;
                color: #fff;
            }
            .customClassBtn2{
                display: flex;
                justify-content: center;
                margin-top: 20px;
            }
            .customBtnClass2{
                width: 200px;
                display: flex;
                justify-content: center;
                border-radius: 5px !important;
                background: #fff;
                align-items: center;
                color: #53C329;
            }
            .img-preview {
                width: 100%;
                height: 300px;
                line-height: 300px;
                text-align: center;
                color: #ddd;
                font-weight: bold;
                font-size: 2.0em;
                border: 0px !important;
                padding: 0px;
            }
            .leftArrowImage{
                width: 20px;
                margin-right: 10px;
            }
            .customAdd1{
                position: relative;
                text-align: center;
                font-size: 13px;
            }
            .customContainer{
                padding: 0px !important;
            }
        }
        @media (max-width: 370px) {

        }
        .lds-ripple {
            display: inline-block;
            position: relative;
            width: 80px;
            height: 80px;
        }
        .lds-ripple div {
            position: absolute;
            border: 4px solid #555;
            opacity: 1;
            border-radius: 50%;
            animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
        }
        .lds-ripple div:nth-child(2) {
            animation-delay: -0.5s;
        }
        @keyframes lds-ripple {
            0% {
                top: 36px;
                left: 36px;
                width: 0;
                height: 0;
                opacity: 1;
            }
            100% {
                top: 0px;
                left: 0px;
                width: 72px;
                height: 72px;
                opacity: 0;
            }
        }

    </style>
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.sharedoffice')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedoffice.create') }}">{{trans('sharedoffice.create_office')}}</a>
        </li>
    </ul>
@endsection

@section('description')

@endsection

@section('footer')
    <script src="{{asset('js/vue.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vue-resource.min.js')}}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.5.1/vue-resource.min.js"></script>
    <script>
        +function() {
            {{--$(document).ready(function() {--}}
            {{--    $('#sharedoffice-form').makeAjaxForm({--}}
            {{--        redirectTo: '{{ route('backend.sharedoffice.addimages') }}',--}}
            {{--    });--}}
            {{--});--}}
        }(jQuery);

        let vu = new Vue({
            el: '#vueJsAddMoreImages',
            data: {
                postData: {
                    files: [],
                },
                multiple:[],
                progressBar:false,
                image_size:'',
                image:'',
                image_file:'',
                shared_office_images_id:'',
                loader:false,
                videoId:'',
                loadFiles:true,
                loadVideo:true,
                videoLoader:true
            },

            methods: {
                fileChangedUploadVideo() {
                    let that = this;
                    $('.progress-bar').show();
                    $('#percentageCount').show();
                    function progress() {
                        var prg = document.getElementById('progress');
                        var percent = document.getElementById('percentageCount');
                        var counter = 5;
                        var progress = 5;
                        var id = setInterval(frame, 50);

                        function frame() {
                            if (progress == 100 && counter == 100) {
                                clearInterval(id)
                            } else {

                                progress += 1;
                                counter += 1;
                                prg.style.width = progress + '%';
                                percent.innerHTML = counter + '%';
                                if(counter === 100) {
                                    $('.progress-bar').hide();
                                    $('#percentageCount').hide();
                                    that.uploadFileVideo();
                                } else {
                                    console.log('stop');
                                }
                            }
                        }
                    }
                    progress();
                },
                uploadFileVideo: function(){
                    let that = this;
                    let files = this.$refs.myFileVideo.files;
                    window.file = files;
                    that.uploadVideoFiles(files);
                },
                uploadVideoFiles: function(file){
                    let that = this;
                    that.videoLoader = true;
                    console.log('files');
                    console.log(file);
                    let url = '{{route('backend.sharedofficeAddfiles')}}';
                    console.log('files url');
                    console.log(url);
                    let formData = new FormData();
                    for (let i = 0; i < file.length; i++) {
                        let files = file[i];
                        formData.append(`files[${i+1}]`, files);
                    }
                    formData.append('office_id','{{$model->id}}');
                    that.$http.post(url,formData).then(function (response) {
                        console.log('response');
                        console.log(response);
                        console.log(response.data);
                        console.log(response.data.id);
                        that.videoId = response.data.id;
                        window.location.reload();
                    }, function (error) {
                        console.log('error');
                        console.log(error);
                    });
                },
                fileChanged (event) {
                    let that = this;
                    $('.progress-bar').show();
                    $('#percentageCount').show();
                    function progress() {
                        var prg = document.getElementById('progress');
                        var percent = document.getElementById('percentageCount');
                        var counter = 5;
                        var progress = 5;
                        var id = setInterval(frame, 50);

                        function frame() {
                            if (progress == 100 && counter == 100) {
                                clearInterval(id)
                            } else {

                                progress += 1;
                                counter += 1;
                                prg.style.width = progress + '%';
                                percent.innerHTML = counter + '%';
                                if(counter === 100) {
                                    $('.progress-bar').hide();
                                    $('#percentageCount').hide();
                                    that.uploadFile();
                                } else {
                                    console.log('stop');
                                }
                            }
                        }
                    }
                    progress();
                },
                uploadFile: function(){
                    let that = this;
                    let files = this.$refs.myFile.files;
                    window.file = files;
                    that.uploadFiles(files);
                },
                uploadFiles: function(file){
                    let that = this;
                    console.log('files');
                    console.log(file);
                    let url = '{{route('backend.sharedofficeAddfiles')}}';
                    console.log('files url');
                    console.log(url);
                    let formData = new FormData();
                    for (let i = 0; i < file.length; i++) {
                        let files = file[i];
                        formData.append(`files[${i+1}]`, files);
                    }
                    formData.append('office_id','{{$model->id}}');
                    that.$http.post(url,formData).then(function (response) {
                        console.log('response');
                        console.log(response);
                        console.log(response.data);
                        console.log(response.data.id);
                        that.videoId = response.data.id;
                        that.getImages();
                    }, function (error) {
                        console.log('error');
                        console.log(error);
                    });
                },
                getFiles: function(){
                    let url  = '/admin/sharedoffice/files/'+"{{$model->id}}";
                    this.$http.get(url).then((response) => {
                        console.log('response');
                        console.log(response);
                        console.log(response.data);
                        console.log(response.data.src);
                        this.multiple.push(response.data)
                    }, function (error) {
                        console.log('error');
                        console.log(error);
                    });
                },
                removeImage: function(id,imgId=null,idd){
                    if(!idd) {
                        let images = this.multiple;
                        images.map(function(data) {
                            let index = images.indexOf(data);
                            console.log(index);
                            console.log(parseInt(index) === parseInt(id));
                            if(parseInt(index) === parseInt(id)){
                                console.log('true');
                                images.splice(index,1)
                            }
                        });
                    } else {
                        this.loader = true;
                        this.loadFiles = true;
                        let url = "/admin/sharedoffice/removeImage/" + idd;
                        this.$http.get(url).then((response) => {
                            console.log('response');
                            console.log(response);
                            this.loader = false;
                            toastr.success('Image Deleted Successfully');
                            this.getImages();
                            this.getFiles();
                            let that = this;
                            setTimeout(function () {
                                that.loadFiles = false;
                                // window.location.reload();
                            },2000);
                        }).catch((error) => {
                            console.log('error');
                            console.log(error);
                        });
                    }
                },
                removeVideo: function(index,idd=null){
                    if(!idd) {
                        let images = this.multiple;
                        images.map(function(data) {
                            let index = images.indexOf(data);
                            console.log(index);
                            console.log(parseInt(index) === parseInt(id));
                            if(parseInt(index) === parseInt(id)){
                                console.log('true');
                                images.splice(index,1)
                            }
                        });
                    } else {
                        this.loadFiles = true;
                        let url = "/admin/sharedoffice/removeImage/" + idd;
                        this.$http.get(url).then((response) => {
                            console.log('response');
                            console.log(response);
                            this.loader = false;
                            toastr.success('Video Deleted Successfully');
                            this.getFiles();
                            this.getImages();
                            let that = this;
                            setTimeout(function () {
                                that.loadFiles = false;
                                // window.location.reload();
                            },3000);
                        }).catch((error) => {
                            console.log('error');
                            console.log(error);
                        });
                    }
                },
                getImages(){
                    const url = "{{route('backend.sharedoffice.getImages',[$model->id])}}";
                    this.$http.get(url).then((response) => {
                        console.log('response')
                        console.log(response.data.thumbnail_image)
                        var listImage = response.data.thumbnail_image
                        var tmp = []
                        listImage.map(function(value, key) {
                            var splitName = value.src.split('/')
                            value.name = splitName[splitName.length - 1]
                            value.type = response.data.type
                            delete value.compressed_image

                            tmp.push(value)
                        })
                        this.multiple = tmp
                    }).catch((error) => {
                        console.log('error');
                        console.log(error);
                    });
                },
                saveAdditionalImages(){
                    let url = "{{route('backend.sharedoffice.addimages.post',[$model->id])}}";
                    console.log(url);
                    let pic_size = this.image_size;
                    let image = this.$refs.myImage.files;
                    console.log('pic_size');
                    console.log(pic_size);
                    console.log('image');
                    console.log(image);
                    this.loader = true;
                    let data = new FormData();
                    data.append('_token', '{{csrf_token()}}');
                    data.append('pic_size', pic_size);
                    data.append('office_id', '{{$model->id}}');
                    data.append('image', image[0]);
                    this.$http.post(url , data , {
                        header : {
                            'Content-Type' : 'multipart/form-data'
                        }
                    }).then((response) => {
                        console.log('response');
                        console.log(response);
                        console.log(response.data.success);
                        this.loader = false;
                        if(response.data.success == 'false') {
                            toastr.error(response.data.message);
                            this.getImages();
                        } else {
                            toastr.success('Image Upload Successfully');
                            this.getImages();
                            setTimeout(function () {
                                window.location.reload();
                            },1000);
                        }
                    }).catch((error) => {
                        console.log('error');
                        console.log(error);
                    })
                },
                updateAdditionalImages(){
                    this.saveAdditionalImages();
                },
            },
            mounted(){
                console.log('add more images');
                this.getImages();
                this.getFiles();
                let that = this;
                setTimeout(function () {
                    that.loadFiles = false;
                    that.loadVideo = false;
                    that.videoLoader = false;
                },1000);
            }
        });
    </script>
@endsection

@section('content')
    @if(!$is_mobile)
    <div class="pcWeb">
        <div id="vueJsAddMoreImages">
            <div class="lds-ring" v-model="loader" v-if="loader"><div></div><div></div><div></div><div></div></div>

            <div class="portlet light bordered" v-if="!loader">
                <div class="portlet-title">
                    <div class="caption font-green-sharp">
                        <span class="caption-helper">{{trans('sharedoffice.msgImages')}}</span>
                    </div>
                    <div class="actions">

                    </div>
                </div>
                @if(session()->has('errorMessage'))
                    <div class="alert alert-danger">
                        {{session()->get('errorMessage')}}
                    </div>
                @endif
                @if(session()->has('successMessage'))
                    <div class="alert alert-success">
                        {{session()->get('successMessage')}}
                    </div>
                @endif
                <div class="portlet-body form">
                    <form enctype="multipart/form-data">
                        <div class="form-body">
                            <div class="form-group">
                                <input type="hidden" name="office_id" value="{{$model->id}}" />
                                <label class="control-label">{{trans('common.cv_image')}}</label>
                                <div>
                                    @if($main_image)
                                        <img id="image_preview" class="img-thumbnail img-preview"
                                             src="{{asset($main_image)}}" v-model="image"/>
                                    @else
                                        <img id="image_preview" style="display: none" v-model="image" class="img-thumbnail img-preview"                                alt="{{$main_image}}"
                                             src="{{asset($main_image)}}"/>
                                    @endif
                                </div>
                                <label class="fileContainer">
                                    <p style="margin: 0px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;">
                                        <img src="{{asset('images/plus.png')}}" class="customFaPlus" />
                                        @if(isset($main_image))
                                            <span class="customAdd">{{trans('common.ch_pic')}}</span>
                                        @else
                                            <span class="customAdd">{{trans('common.add')}}</span>
                                        @endif
                                    </p>
                                    <input accept="image/*"
                                           class="image-upload"
                                           target="image_preview"
                                           onChange="imageFileInputChange(this)"
                                           name="image"
                                           ref="myImage"
                                           type="file">
                                </label>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <select class="form-control" required v-model="image_size">
                                        <option selected="selected">1366 X 768</option>
                                        <option>683 X 768</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <div class="form-group">
                                <label>{{trans('common.u_t_i')}}</label>
                                <div class="container containerProgress" style="width: 100%;">
                                    <div id="percentageCount" class="percent-count" style="width:29%;"></div>
                                    <div class="progress-bar" style="display: none; width: 40%;">
                                        <div class="progress" id="progress">
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top:10px;">
                                    <div class="col-md-12" style="display: flex;flex: 1;flex-flow: wrap;" v-if="loadFiles">
                                        <div v-for="(img ,index) in multiple" v-if="img.type == 'image'">
                                            <div class="lds-ring-loader" v-model="loadFiles"><div></div><div></div><div></div><div></div></div>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="display: flex;flex: 1;flex-flow: wrap;" v-else>
                                        <div v-for="(img ,index) in multiple" v-if="img.type == 'image'">
                                            <img class="imageThumb" :data-id="index" style="border: 1px solid #C0C0C0;width:75px;height:75px;border-radius:10px;margin-right:10px;object-fit:cover;" :src="img.src"  :title="img.name"/>
                                            <br/><span class="remove set-remove imgRemove" @click.prevent="removeImage(index,img.file,img.id)"><img src="{{asset('images/cross-image.png')}}"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="fileContainer">
                                            <p style="margin:0px;display: flex; flex-direction: column;align-items: center; justify-content:center;">
                                                <img src="{{asset('images/plus.png')}}" class="customFaPlus" />
                                                <span class="customAdd">{{trans('common.th')}}</span>
                                            </p>
                                            <input type="file"
                                                   accept="image/*"
                                                   name="image_file[]"
                                                   id="image_file"
                                                   data-target="image_previeww"
                                                   ref="myFile"
                                                   multiple="multiple"
                                                   @change="fileChanged"
                                                   class="image-upload">
                                        </label>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="form-group">
                                    <label>{{trans('common.up_v')}}</label>
                                </div>
                                <div class="row" v-model="videoLoader" v-if="!videoLoader" style="margin-top:10px;">
                                    <div class="col-md-6" v-if="loadVideo">
                                        <div v-for="(img ,index) in multiple" v-if="img.type == 'video'" style="width: 100%;height:205px;">
                                            <div class="lds-ring-loader-video" v-model="loadFiles"><div></div><div></div><div></div><div></div></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6" v-else>
                                        <div v-for="(img ,index) in multiple" v-if="img.type == 'video'">
                                            <video width="400" controls style="height:205px;width:100%">
                                                <source :src="img.src" type="video/mp4">
                                            </video>
                                            <br/><span class="remove set-removeWeb imgRemove" @click.prevent="removeImage(index,img.file,img.id)"><img src="{{asset('images/cross-image.png')}}"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" v-if="videoLoader" style="display:flex;justify-content:center;">
                                    <div class="lds-ripple"><div></div><div></div></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="fileContainer">
                                            <p style="margin:0px;display: flex; flex-direction: column;align-items: center; justify-content:center;">
                                                <img src="{{asset('images/plus.png')}}" class="customFaPlus" />
                                                <span class="customAdd">{{trans('common.add_v')}}</span>
                                            </p>
                                            <input type="file"
                                                   accept="video/*"
                                                   name="image_file"
                                                   id="image_file"
                                                   data-target="image_previeww"
                                                   ref="myFileVideo"
                                                   @change="fileChangedUploadVideo"
                                                   class="image-upload">
                                        </label>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button @click.prevent="saveAdditionalImages" class="btn blue">{{trans('common.cv_i')}}</button>
                                    {!! Html::linkRoute('backend.sharedoffice', trans('sharedoffice.cancel'), array(), array('class' => 'btn btn-danger')) !!}
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @else
        @include('backend.sharedoffice.include.mobileWeb')
    @endif
@endsection

@section('modal')

@endsection
