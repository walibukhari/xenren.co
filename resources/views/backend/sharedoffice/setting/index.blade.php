@extends('backend.layout')

@section('title')
    {{trans('sideMenu.sharedofficeNotifications')}}
@endsection
@section('description')

@endsection
<head xmlns:v-on="http://www.w3.org/1999/xhtml">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="{{asset('js/vue.min.js')}}"></script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">
    <link href="{{asset('custom/css/backend/workingsheet.css')}}" type="text/css" rel="stylesheet">
    <style>
        [v-cloak] {
            display: none;
        }
        .filter-option > .pull-left{
            font-size: 12px !important;
        }
        .page-title{
            padding: 15px !important;
            margin-bottom: 10px !important;
            display: none !important;
        }
        .worksheet{
            box-shadow: 0px 0px 6px -3px #2b3643;
            margin-top: 12px;
            padding: 15px;
            border-radius: 10px;
        }
        .customSetStyle{
            padding: 15px;
            margin: 0px;
            font-size: 25px;
            font-weight: bold;
            color: #2b3643;
        }
        .customSetStyleN{
             margin: 0px;
             padding: 15px;
             color: rgb(37, 206, 15);
             font-weight: bold;
         }
        .saveBtnBtn{
            background-color: rgb(37, 206, 15) !important;
            border: 0px !important;
            width: 100%;
            height: 45px;
            font-size: 16px !important;
            font-weight: bold !important;
        }
        .customRowSetting{
            display: flex;
            padding: 20px;
        }
        .setDisplaycol3Set{
            display: flex;
            align-items: center;
            width: 100%;
        }
        .setDisplaycol4Set{
            display: flex;
            width: 100%;
            align-items: center;
        }
        .form-control {
            outline: 0!important;
            padding: 0px !important;
            border: 0px !important;
        }
        .setBoxOffer{
            border: 1px solid #efefef;
            padding: 0px 9px 0px 15px;
            border-radius: 3px;
            display: flex;
            align-items: center;
            width: 72px;
        }
        .Label2{
            width: 180px;
        }
        .Label1{
            width: 150px;
        }
        .checkOpt{
            width: 70px !important;
        }
        .checkSecondOpt{
            width: 70px !important;
        }



        /* custom check box area */
        /* The container */
        .containerC {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 30px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default checkbox */
        .containerC input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom checkbox */
        .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
        }

        /* On mouse-over, add a grey background color */
        .containerC:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a blue background */
        .containerC input:checked ~ .checkmark {
            background-color: rgb(37, 206, 15) !important;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .containerC input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .containerC .checkmark:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
        /* custom check box area */
        @media (max-width: 500px) {
            .setDisplaycol3Set{
                padding: 0px !important;
            }
            .customSetStyle{
                text-align: center;
            }
        }

        .lds-ring {
            display: inline-block;
            position: relative;
            width: 20px;
            height: 20px;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 20px;
            height: 20px;
            margin: 1px;
            border: 2px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
        .saveBtnBtn{
            display: flex !important;
            justify-content: center;
            align-items: center;
            width: 150px;
            height: 45px;
        }

        .lds-ring2 {
            display: inline-block;
            position: relative;
            width: 20px;
            height: 20px;
        }
        .lds-ring2 div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 20px;
            height: 20px;
            margin: 1px;
            border: 2px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }
        .lds-ring2 div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring2 div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring2 div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring2 {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
</head>
@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">{{trans('common.noti')}}</a>
            <i class="fa fa-circle"></i>
        </li>
    </ul>
@endsection
@section('footer')
    <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <script>
        $('.scroll-div').slimScroll({});
    </script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/vue.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vue-resource.min.js')}}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.15/lodash.core.min.js"></script>
    <script>
        let vu = new Vue({
            el: '#settings',
            data: {
                confirm_password:'',
                password :'',
                loaderLoad:true
            },
            methods: {
                selectOption: function (val,event) {
                        $('.checkOpt').prop('checked', false);
                        console.log(event);
                        console.log(event.target.id);
                        let id = event.target.id;
                        $('#' + id).prop('checked', true);
                        $('#book_type').val(id);
                },

                selectSecondOption: function (val,event) {
                        $('.checkSecondOpt').prop('checked', false);
                        console.log(event);
                        console.log(event.target.id);
                        let id = event.target.id;
                        $('#' + id).prop('checked', true);
                        $('#cancel_type').val(id);
                },

                selectThirdOption: function (val,event) {
                    $('.checkThirdOpt').prop('checked', false);
                    console.log(event);
                    console.log(event.target.id);
                    let id = event.target.id;
                    $('#' + id).prop('checked', true);
                    $('#cancel_type').val(id);
                }


            },
            mounted() {
            }
        });
        $('#saveBtnBtnSubmit').click(function () {
            $('#idSpanManagerSettings').hide();
            $('.lds-ring').show();
            setTimeout(function () {
                $('#submitSettingForm').submit();
            },500);
        });
        $('#saveBtnBtnSubmitManageBooking').click(function () {
            $('#idSpanManagerSettings2').hide();
            $('.lds-ring2').show();
            setTimeout(function () {
                $('#submitSettingManageBookingForm').submit();
            },500);
        });
    </script>
@endsection
@section('content')
    <div class="worksheet" id="settings">
        <h1 class="customSetStyleN">{{trans('common.noti')}}</h1>
        <h1 class="customSetStyle">{{trans('common.noti_set')}}</h1>
        <div class="col-md-12">
            @if(session()->has('success_message'))
                <div class="alert alert-success">
                    {{session()->get('success_message')}}
                </div>
            @endif
        </div>
        <div class="row customRowSetting">
            <div class="container">
                <form id="submitSettingForm" action="{{route('backend.post.sharedoffice.settings')}}" method="POST">
                    <input type="hidden" name="_token" value="{{csrf_token()}}" />
                    <input type="hidden" name="book_type" id="book_type" />
                    <input type="hidden" name="cancel_type" id="cancel_type" />
                    <div class="row">
                        <div class="col-md-4 setDisplaycol4Set">
                            <div class="form-group setDisplaycol4Set">
                                <label class="Label1">{{trans('common.pay_lt')}}</label>
                                @if(isset($settings->book_type) && $settings->book_type == \App\Models\SharedOfficeSetting::BOOK_STATUS_PAY_LATER)
                                    <label class="containerC">
                                        <input type="checkbox" class="checkOpt" @change="selectOption('a',$event)" name="book_offer" id="1" checked="checked">
                                        <span class="checkmark"></span>
                                    </label>
                                @else
                                    <label class="containerC">
                                        <input type="checkbox" class="checkOpt" id="1" name="book_offer" @change="selectOption('a',$event)">
                                        <span class="checkmark"></span>
                                    </label>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4 setDisplaycol3Set">
                            <div class="form-group setDisplaycol3Set">
                                <label class="Label2">{{trans('common.py_get_dis')}}</label>
                                @if(isset($settings->book_type) && $settings->book_type == \App\Models\SharedOfficeSetting::BOOK_STATUS_PAY_NOW)
                                    <label class="containerC">
                                        <input type="checkbox" id="2" class="checkOpt" name="book_offer" checked="checked" @change="selectOption('b',$event)">
                                        <span class="checkmark"></span>
                                    </label>
                                @else
                                    <label class="containerC">
                                        <input type="checkbox" class="checkOpt" id="2" name="book_offer" @change="selectOption('b',$event)">
                                        <span class="checkmark"></span>
                                    </label>
                                @endif
                                <div class="setBoxOffer">
                                    <input type="number" name="discount" value="{{isset($settings->discount) ? $settings->discount : '0'}}" class="form-control" />%
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 setDisplaycol4Set">
                            <div class="form-group setDisplaycol4Set">
                                <label class="Label1">{{trans('common.cancel_c')}}</label>
                                @if(isset($settings->cancel_type) && $settings->cancel_type == \App\Models\SharedOfficeSetting::CANCEL_WITH_CHARGES)
                                    <label class="containerC">
                                        <input type="checkbox" class="checkSecondOpt" @change="selectSecondOption('c',$event)" name="cancel_charges" id="3" checked="checked">
                                        <span class="checkmark"></span>
                                    </label>
                                @else
                                    <label class="containerC">
                                        <input type="checkbox" class="checkSecondOpt" id="3" name="cancel_charges" @change="selectSecondOption('c',$event)">
                                        <span class="checkmark"></span>
                                    </label>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4 setDisplaycol3Set">
                            <div class="form-group setDisplaycol3Set">
                                <label class="Label2">{{trans('common.cancel_c_w')}}</label>
                                @if(isset($settings->cancel_type) && $settings->cancel_type == \App\Models\SharedOfficeSetting::CANCEL_WITH_OUT_CHARGES)
                                    <label class="containerC">
                                        <input type="checkbox" id="4" class="checkSecondOpt" name="cancel_charges" checked="checked" @change="selectSecondOption('d',$event)">
                                        <span class="checkmark"></span>
                                    </label>
                                @else
                                    <label class="containerC">
                                        <input type="checkbox" class="checkSecondOpt" id="4" name="cancel_charges" @change="selectSecondOption('d',$event)">
                                        <span class="checkmark"></span>
                                    </label>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <button type="button" id="saveBtnBtnSubmit" class="saveBtnBtn btn btn-success btn-sm">
                                <span id="idSpanManagerSettings">{{trans('common.save')}}</span>
                                <div style="display: none;" class="lds-ring">
                                    <div></div><div></div><div></div><div></div>
                                </div>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <br>
        <br>
        <h1 class="customSetStyleN">{{trans('common.m_b')}}</h1>
        <h1 class="customSetStyle">{{trans('common.noti_set')}}</h1>
        <div class="row customRowSetting">
            <div class="container">
                <form id="submitSettingManageBookingForm" action="{{route('backend.post.sharedofficeBooking.settings')}}" method="POST">
                    <input type="hidden" name="_token" value="{{csrf_token()}}" />
                    <div class="row">
                        <div class="col-md-4 setDisplaycol4Set">
                            <div class="form-group setDisplaycol4Set">
                                <label class="Label1">{{trans('common.al_b')}}</label>
                                @if(isset($settings->allow) && $settings->allow == \App\Models\SharedOffice::MANAGE_BOOKING_ALLOW)
                                    <label class="containerC">
                                        <input type="checkbox" class="checkThirdOpt" @change="selectThirdOption('g',$event)" name="allow" id="10" checked="checked">
                                        <span class="checkmark"></span>
                                    </label>
                                @else
                                    <label class="containerC">
                                        <input type="checkbox" class="checkThirdOpt" id="10" name="allow" @change="selectThirdOption('g',$event)">
                                        <span class="checkmark"></span>
                                    </label>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4 setDisplaycol3Set">
                            <div class="form-group setDisplaycol3Set">
                                <label class="Label2">{{trans('common.not_al_b')}}</label>
                                @if(isset($settings->allow) && $settings->allow == \App\Models\SharedOffice::MANAGE_BOOKING_NOT_ALLOW)
                                    <label class="containerC">
                                        <input type="checkbox" id="11" class="checkThirdOpt" name="not_allow" checked="checked" @change="selectThirdOption('h',$event)">
                                        <span class="checkmark"></span>
                                    </label>
                                @else
                                    <label class="containerC">
                                        <input type="checkbox" class="checkThirdOpt" id="11" name="not_allow" @change="selectThirdOption('h',$event)">
                                        <span class="checkmark"></span>
                                    </label>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <button type="button" id="saveBtnBtnSubmitManageBooking" class="saveBtnBtn btn btn-success btn-sm">
                                <span id="idSpanManagerSettings2">{{trans('common.save')}}</span>
                                <div style="display: none;" class="lds-ring2">
                                    <div></div><div></div><div></div><div></div>
                                </div>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <br>
    </div>
@endsection
