@extends('backend.layout')

@section('title')
    {{trans('sharedoffice.sharedoffice')}}
@endsection

@section('description')
    {{trans('sharedoffice.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>

        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.sharedoffice')}}</a>
        </li>
    </ul>
@endsection

@section('header')
<style>
    .news-thumb {
        height: 100px;
        width:100px;
        border: 0px;
        padding: 0px;
    }
</style>
@endsection

@section('footer')
    <script>

       var url = "{{ route('backend.sharedoffice.editmoreimagesDt', ['id' => $model->id]) }}";
        +function() {
            $(document).ready(function() {
                var dTable = new Datatable();
                dTable.init({
                    src: $('#sharedoffice-dt'),
                    dataTable: {
                        @include('custom.datatable.common')
                        "ajax": {
                            "url": url,
                            "type": "GET",


                        },
                        columns: [
                            {data: 'id', name: 'id', orderable: true, searchable: false},
                            {
                                data: 'image',
                                name: 'image',
                                orderable: false, searchable: false,
                                "render": function(data) {
                                    return '<img class=\"img-thumbnail news-thumb\" src="'+data+'" />';
                                }
                            },
                            {data: 'office_id', name: 'office_id'},
                            {data: 'actions',width:'20%', name: 'actions', orderable: false, searchable: false}
                        ],
                        createdRow: function(row, data, index) {
                            //check its read
                            if(data.is_read == 0) {
                                $(row).css({'font-weight':'900'});
                            }
                            /*
                             if(data.status == 'waiting') {
                             $(row).addClass('warning');
                             }else if(data.status == 'decline'){
                             $(row).addClass('danger');
                             }
                             */
                        },
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('sharedoffice.sharedoffice')}}</span>
            </div>

            <div class="actions">
                <a href="{{ route('backend.sharedoffice.addimages', ['id' => $model->id]) }}" class="btn btn-circle red-sunglo btn-sm"><i
                            class="fa fa-plus"></i> Add more images</a>
                {!! Html::linkRoute('backend.sharedoffice', trans('sharedoffice.cancel'), array(), array('class' => 'btn btn-danger')) !!}
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="sharedoffice-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th>{{trans('common.id')}}</th>
                        <th>{{trans('sharedoffice.image')}}</th>
                        <th>office_id</th>
                        <th>{{trans('sharedoffice.action')}}</th>
                    </tr>
                    <!--
                    <tr role="row" class="filter">
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_email" placeholder="Username">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="filter_fullname" placeholder="Fullname">
                        </td>
                        <td>
                            <div class="input-group margin-bottom-5">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly name="filter_created_after" placeholder="From">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                            <div class="input-group">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly name="filter_created_before" placeholder="To">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                        </td>
                        <td>
                            <div class="input-group margin-bottom-5">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly name="filter_updated_after" placeholder="From">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                            <div class="input-group">
                                <input type="text" class="form-control form-filter input-sm datetime-picker" readonly name="filter_updated_before" placeholder="To">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                            </div>
                        </td>
                        <td>
                            <div class="margin-bottom-5">
                                <button class="btn btn-info-alt filter-submit">
                                    <i class="fa fa-search"></i> 过滤
                                </button>
                            </div>
                            <button class="btn btn-danger-alt filter-cancel">
                                <i class="fa fa-times"></i> 重置
                            </button>
                        </td>
                    </tr>
                    -->
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection
