@extends('backend.layout')

@section('title')
    Renting month product
@endsection

@section('description')
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>

            <a href="{{ route('backend.sharedoffice.products.index', ['id' => $sharedofficeproduct->office_id]) }}">{{trans('sharedoffice.sharedofficeproduct')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedoffice.products.create', ['id' => $id]) }}">Renting month product</a>
        </li>
    </ul>
@endsection

@section('description')

@endsection

@section('footer')
    <script>
        +function() {
            $(document).ready(function() {
                $('#sharedoffice-form').makeAjaxForm({
                    redirectTo: '{{ route('backend.sharedoffice.products.create', ['id' => $id]) }}',
                });
            });
        }(jQuery);
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-helper">Fill data</span>
            </div>
            <div class="actions">

            </div>
        </div>
        <div>
            <h3 class="">
                <span >Office Name:</span> <span class="badge badge-info">{{ $sharedofficeproduct->office->office_name }}</span>
                <span >Location:</span>  <span class="badge badge-info">{{ $sharedofficeproduct->office->location }}</span>
                <span >Product ID:</span>  <span class="badge badge-info">{{ $sharedofficeproduct->id }}</span>
                <span >Category:</span>  <span class="badge badge-info">{{ $sharedofficeproduct->categories }}</span>
                <span >Status:</span>  <span class="badge badge-info">{{ $sharedofficeproduct->productstatus->status }}</span>

            </h3>


        </div>
        <div class="portlet-body form">
            {!! Form::open(['method' => 'post', 'role' => 'form']) !!}
            <div class="form-body">

                <div class="form-group">
                    {{ Form::hidden('office_id', $sharedofficeproduct->office_id) }}
                    {{ Form::hidden('product_id', $sharedofficeproduct->id ) }}
                    {{ Form::hidden('statusfinance', '2') }}
                    {!! Form::label('startdate','StartDate',['class'=>'control-label']) !!}
                    {!! Form::text('startdate',$sharedofficeproduct->categories,['class'=>'datetime-picker form-control','required' => '', 'placeholder' => 'Enter start dates']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('enddate','EndDate',['class'=>'control-label']) !!}
                    {!! Form::text('enddate','',['class'=>'datetime-picker form-control ','required' => '', 'placeholder' => 'Enter end dates']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('user','User',['class'=>'control-label']) !!}
                    {!! Form::text('user','',['class'=>'form-control','required' => '', 'placeholder' => 'Enter User name ']) !!}
                </div>


                <div class="form-actions">
                    <button type="submit" class="btn blue">Save changes</button>
                    {!! Html::linkRoute('backend.sharedoffice.products.index', 'Cancel', array($sharedofficeproduct->office_id), array('class' => 'btn btn-danger')) !!}

                </div>

                {!! Form::close() !!}

            </div>
        </div>
@endsection

@section('modal')

@endsection
