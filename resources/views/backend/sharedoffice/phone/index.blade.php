@extends('backend.layout')

@section('title')
    {{trans('sideMenu.sharedofficeNotifications')}}
@endsection
@section('description')

@endsection
<head xmlns:v-on="http://www.w3.org/1999/xhtml">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="{{asset('js/vue.min.js')}}"></script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">
    <link href="{{asset('custom/css/backend/workingsheet.css')}}" type="text/css" rel="stylesheet">
    <style>
        [v-cloak] {
            display: none;
        }
        .filter-option > .pull-left{
            font-size: 12px !important;
        }
        .page-title{
            padding: 15px !important;
            margin-bottom: 10px !important;
            display: none !important;
        }
        .worksheet{
            box-shadow: 0px 0px 6px -3px #2b3643;
            margin-top: 12px;
            padding: 15px;
            border-radius: 10px;
        }
        .customSetStyle{
            padding: 15px;
            margin: 0px;
            font-size: 25px;
            font-weight: bold;
            color: #2b3643;
        }
        .customSetStyleN{
            margin: 0px;
            padding: 15px;
            color: rgb(37, 206, 15);
            font-weight: bold;
        }
        .saveBtnBtn{
            background-color: rgb(37, 206, 15) !important;
            border: 0px !important;
            width: 100%;
            height: 45px;
            font-size: 16px !important;
            font-weight: bold !important;
        }
        .customRowSetting{
            display: flex;
            padding: 20px;
        }
        .setDisplaycol3Set{
            display: flex;
            align-items: center;
            width: 100%;
        }
        .setDisplaycol4Set{
            display: flex;
            width: 100%;
            align-items: center;
        }
        .form-control {
            outline: 0!important;
            padding: 0px !important;
            border: 0px !important;
        }
        .setBoxOffer{
            border: 1px solid #efefef;
            padding: 0px 9px 0px 15px;
            border-radius: 3px;
            display: flex;
            align-items: center;
            width: 72px;
        }
        .Label2{
            width: 180px;
        }
        .Label1{
            width: 150px;
        }
        .checkOpt{
            width: 70px !important;
        }
        .checkSecondOpt{
            width: 70px !important;
        }



        /* custom check box area */
        /* The container */
        .containerC {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 30px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default checkbox */
        .containerC input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom checkbox */
        .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
        }

        /* On mouse-over, add a grey background color */
        .containerC:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a blue background */
        .containerC input:checked ~ .checkmark {
            background-color: rgb(37, 206, 15) !important;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .containerC input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .containerC .checkmark:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
        /* custom check box area */
        @media (max-width: 500px) {
            .setDisplaycol3Set{
                padding: 0px !important;
            }
            .customSetStyle{
                text-align: left;
            }
        }

        .lds-ring {
            display: inline-block;
            position: relative;
            width: 20px;
            height: 20px;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 20px;
            height: 20px;
            margin: 1px;
            border: 2px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
        .saveBtnBtn{
            display: flex !important;
            justify-content: center;
            align-items: center;
            width: 150px;
            height: 45px;
        }

        .lds-ring2 {
            display: inline-block;
            position: relative;
            width: 20px;
            height: 20px;
        }
        .lds-ring2 div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 20px;
            height: 20px;
            margin: 1px;
            border: 2px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }
        .lds-ring2 div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring2 div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring2 div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring2 {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
        .custom-set-form-input{
            border: 1px solid #c2cad8 !important;
            padding: 15px !important;
        }
        #phone_number{
            width: 90%;
        }
        .responsiveView{
            background: rgb(37, 206, 15) !important;
            border: 0px !important;
        }
    </style>
</head>
@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Notifications</a>
            <i class="fa fa-circle"></i>
        </li>
    </ul>
@endsection
@section('footer')
    <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <script>
        $('.scroll-div').slimScroll({});
    </script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/vue.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vue-resource.min.js')}}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.15/lodash.core.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#phone_number").keypress(function (e) {
                //if the letter is not digit then display error and don't type anything
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    //display error message
                    toastr.error('please enter digits only');
                    return false;
                }
            });
        });
    </script>
    <script>
        let vu = new Vue({
            el: '#phoneNumber',
            data: {
                phone_number:'{{$staff->phone_number}}'
            },
            methods: {
                createPhone: function () {
                    if(this.phone_number == '') {
                        toastr.error('please enter phone number');
                        return;
                    }
                    let form = {
                        '_token' : '{{csrf_token()}}',
                        'phone_number' : this.phone_number
                    }
                    let url = '{{route('backend.createPhoneNumber')}}';
                    this.$http.post(url,form).then((response) => {
                        console.log('response');
                        console.log(response);
                        if(response.data.status == true) {
                            toastr.success(response.data.message);
                        }
                    }).then((error) => {
                        console.log('error');
                        console.log(error);
                    })
                }
            },
            mounted() {
                console.log('add phone number page');
            }
        });
    </script>
@endsection
@section('content')
    <div class="worksheet" id="phoneNumber">
        <h1 class="customSetStyleN">{{trans('common.phone_n')}}</h1>
        <h1 class="customSetStyle">{{trans('common.ppp')}}</h1>
        <div class="col-md-12">
            @if(session()->has('success_message'))
                <div class="alert alert-success">
                    {{session()->get('success_message')}}
                </div>
            @endif
        </div>
        <div class="container">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <label>{{trans('common.phone_n')}}: <small>{{trans('common.format')}} (922345678901)</small></label><br>
                        <input type="text" id="phone_number" v-model="phone_number" name="phone_number" class="custom-set-form-input form-control" placeholder="Number Format like this 922345678901" />
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-success responsiveView" @click="createPhone()">{{trans('common.submitt')}}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
