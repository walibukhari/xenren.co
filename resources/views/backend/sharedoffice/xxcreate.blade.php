@extends('backend.layout')

@section('title')
    {{trans('sharedoffice.sharedoffice')}}
@endsection


@section('description')

@endsection

<head xmlns:v-on="http://www.w3.org/1999/xhtml">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" type="text/css" />
    <script src="{{asset('js/vue.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.5.1/vue-resource.min.js"></script>
</head>
<style>
    #map{
        width: 100%;
        height: 500px;
    }
    #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
    }
    #infowindow-content .title {
        font-weight: bold;
    }
    #infowindow-content {
        display: none;
    }
    #map #infowindow-content {
        display: inline;
    }
    .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
    }
    #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
    }
    .pac-controls {
        display: inline-block;
        padding: 5px 11px;
    }
    .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }
    #pac-input {
        background-color: #fff;
        font-size: 15px;
        font-weight: 300;
        /*margin-left: 12px;*/
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
    }
    #pac-input:focus {
        border-color: #4d90fe;
    }
    #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
    }
    #target {
        width: 345px;
    }
    /*#map div {*/
    /*position: inherit !important;*/
    /*}*/
    /*#map div div{*/
    /*position: inherit !important;*/
    /*}*/
</style>

<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }
    .facility_number_custom_right{
        background: #F7F7F7;
        padding: 15px;
    }
    .facility_number_custom_right table tr td{
        border: unset !important;
        padding: 3px 8px !important;
    }
    .innerboxes-main-custom .form-group{
        margin-bottom: 10px;
    }
    .innerboxes{
        width: 38px;
        height: 38px;
        border: 2px solid #E3E3E3;
        border-radius: 7px;
        margin-right: 10px;
        float: left;
    }
	.set-office-fee > label{
		font-weight: bold;
	}
</style>
@if(Auth::guard('staff')->user()->staff_type == \App\Models\Staff::STAFF_TYPE_STAFF)
    <link href="{{asset('custom/css/backend/staffHeader.css')}}" rel="stylesheet" type="text/css">
@endif

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.sharedoffice')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedoffice.create') }}">{{trans('sharedoffice.create_office')}}</a>
        </li>
    </ul>
@endsection

@section('description')

@endsection

@section('footer')
    <script>
			//        var VueResource = require('vue-resource');
			Vue.use(VueResource);
			Vue.config.debug = true;
			new Vue({
				el: '.bordered',
				mounted: function() {
//						toastr.success('Have fun storming the castle!', 'Miracle Max Says');
					console.log('vue initialized');
					this.addSpaceHotDesk();
					this.addSpaceDediDesk();
					this.addSpaceOfficeDesk();

				},
				data: {
					sharedOfficeData: {
						version_english: true,
						version_chinese: 0,
						image: '',
						office_name: '',
						location: '',
						pic_size: '',
						shfacilities: [],
						bulletins: [],
						description: '',
						office_size_x: '',
						office_size_y: '',
						seatsData: [],
						office_space: '',
						contact_name: '',
						contact_phone: '',
						contact_email: '',
						contact_address: '',
						monday_opening_time: '',
						monday_closing_time: '',
						saturday_opening_time: '',
						saturday_closing_time: '',
						sunday_opening_time: '',
						sunday_closing_time: '',
						office_name_cn: '',
						description_cn: '',
						office_space_cn: '',
						contact_name_cn: '',
						contact_phone_cn: '',
						contact_email_cn: '',
						contact_address_cn: '',
						saturday_on: 'checked',
						sunday_on: 'checked',
						// hot_desk_type:'',
						// hot_desk_peoples:'',
						// hot_desk_hourly_price:'',
						// hot_desk_daily_price:'',
						// hot_desk_weekly_price:'',
						// hot_desk_monthly_price:'',
						//
						// dedicated_desk_type:'',
						// dedicated_desk_peoples:'',
						// dedicated_desk_hourly_price:'',
						// dedicated_desk_daily_price:'',
						// dedicated_desk_weekly_price:'',
						// dedicated_desk_monthly_price:'',
						//
						// office_desk_type:'',
						// office_desk_peoples:'',
						// office_desk_hourly_price:'',
						// office_desk_daily_price:'',
						// office_desk_weekly_price:'',
						// office_desk_monthly_price:'',

						statusChecked:'',
                        verify_info_to_scan: 'checked',
                        require_deposit: 'checked',

					},


					no_of_peoples:'',
					hourly_price:'',
					daily_price:'',
					weekly_price:'',
					month_price:'',

					feeHotDesks:[],
					feeDedicatedDesks:[],
					feeOfficeDesks:[],
					feeHotDesk:[],
					feeDedicatedDesk:[],
					feeOfficeDesk:[],

					selected_seat: {
						x: 0,
						y: 0
					},
					facility_number: 0,
					seat_number: 0
				},
				methods: {
					createSharedOffice: function() {
						var shf = this.sharedOfficeData.shfacilities;
						var url = '/admin/sharedoffice/create';
						var formData = new FormData();
						formData.append('image', $('#image_file')[0].files[0]);
						formData.append('office_name', this.sharedOfficeData.office_name);
						formData.append('location', this.sharedOfficeData.location);
						formData.append('office_size_x', this.sharedOfficeData.office_size_x);
						formData.append('office_size_y', this.sharedOfficeData.office_size_y);
						formData.append('description', this.sharedOfficeData.description);
						formData.append('pic_size', this.sharedOfficeData.pic_size);
						formData.append('shfacilities', this.sharedOfficeData.shfacilities);
						formData.append('seatsData', JSON.stringify(this.sharedOfficeData.seatsData));
						formData.append('current_location', JSON.stringify(window.selectedPlace[0]));

						formData.append('office_manager', this.sharedOfficeData.office_manager_email);
						formData.append('office_space', this.sharedOfficeData.office_space);

						formData.append('contact_name', this.sharedOfficeData.contact_name);
						formData.append('contact_phone', this.sharedOfficeData.contact_phone);
						formData.append('contact_email', this.sharedOfficeData.contact_email);
						formData.append('contact_address', this.sharedOfficeData.contact_address);

						formData.append('monday_opening_time', this.sharedOfficeData.monday_opening_time);
						formData.append('monday_closing_time', this.sharedOfficeData.monday_closing_time);

						var saturday_opening_time = this.sharedOfficeData.saturday_on ? 'OFF DAY' : this.sharedOfficeData.saturday_opening_time;
						var saturday_closing_time = this.sharedOfficeData.saturday_on ? 'OFF DAY' : this.sharedOfficeData.saturday_closing_time;

						formData.append('saturday_opening_time', saturday_opening_time);
						formData.append('saturday_closing_time', saturday_closing_time);

						var sunday_opening_time = this.sharedOfficeData.sunday_on ? 'OFF DAY' : this.sharedOfficeData.sunday_opening_time;
						var sunday_closing_time = this.sharedOfficeData.sunday_on ? 'OFF DAY' : this.sharedOfficeData.sunday_closing_time;

						formData.append('sunday_opening_time', sunday_opening_time);
						formData.append('sunday_closing_time', sunday_closing_time);

						formData.append('saturday_on', this.sharedOfficeData.saturday_on);
						formData.append('sunday_on', this.sharedOfficeData.sunday_on);

						formData.append('office_name_cn', this.sharedOfficeData.office_name_cn);
						formData.append('description_cn', this.sharedOfficeData.description_cn);
						formData.append('office_space_cn', this.sharedOfficeData.office_space_cn);
						formData.append('contact_name_cn', this.sharedOfficeData.contact_name_cn);
						formData.append('contact_phone_cn', this.sharedOfficeData.contact_phone_cn);
						formData.append('contact_email_cn', this.sharedOfficeData.contact_email_cn);
						formData.append('contact_address_cn', this.sharedOfficeData.contact_address_cn);

						formData.append('version_english', this.sharedOfficeData.version_english);
						formData.append('version_chinese', this.sharedOfficeData.version_chinese);

						formData.append('bulletins', this.sharedOfficeData.bulletins);
						this.sharedOfficeData.image = $('#image_file')[0].files[0];
                        {{--this.sharedOfficeData.hot_desk_type = '{{\App\Models\SharedOfficeFee::HOT_DESK_TYPE}}';--}}
						{{--formData.append('hot_desk_type',this.sharedOfficeData.hot_desk_type);--}}
						{{--formData.append('hot_desk_peoples',this.sharedOfficeData.hot_desk_peoples);--}}
						{{--formData.append('hot_desk_hourly_price',this.sharedOfficeData.hot_desk_hourly_price);--}}
						{{--formData.append('hot_desk_daily_price',this.sharedOfficeData.hot_desk_daily_price);--}}
						{{--formData.append('hot_desk_weekly_price',this.sharedOfficeData.hot_desk_weekly_price);--}}
						{{--formData.append('hot_desk_monthly_price',this.sharedOfficeData.hot_desk_monthly_price);--}}
                        {{--this.sharedOfficeData.dedicated_desk_type = '{{\App\Models\SharedOfficeFee::DEDICATED_DESK_TYPE}}';--}}
						{{--formData.append('dedicated_desk_type',this.sharedOfficeData.dedicated_desk_type);--}}
						{{--formData.append('dedicated_desk_peoples',this.sharedOfficeData.dedicated_desk_peoples);--}}
						{{--formData.append('dedicated_desk_hourly_price',this.sharedOfficeData.dedicated_desk_hourly_price);--}}
						{{--formData.append('dedicated_desk_daily_price',this.sharedOfficeData.dedicated_desk_daily_price);--}}
						{{--formData.append('dedicated_desk_weekly_price',this.sharedOfficeData.dedicated_desk_weekly_price);--}}
						{{--formData.append('dedicated_desk_monthly_price',this.sharedOfficeData.dedicated_desk_monthly_price);--}}
                        {{--this.sharedOfficeData.office_desk_type = '{{\App\Models\SharedOfficeFee::OFFICE_DESK_TYPE}}';--}}
						{{--formData.append('office_desk_type',this.sharedOfficeData.office_desk_type);--}}
						{{--formData.append('office_desk_peoples',this.sharedOfficeData.office_desk_peoples);--}}
						{{--formData.append('office_desk_hourly_price',this.sharedOfficeData.office_desk_hourly_price);--}}
						{{--formData.append('office_desk_daily_price',this.sharedOfficeData.office_desk_daily_price);--}}
						{{--formData.append('office_desk_weekly_price',this.sharedOfficeData.office_desk_weekly_price);--}}
						{{--formData.append('office_desk_monthly_price',this.sharedOfficeData.office_desk_monthly_price);--}}
						formData.append('feeHotDesks',JSON.stringify(this.feeHotDesks));
						formData.append('feeDedicatedDesks',JSON.stringify(this.feeDedicatedDesks));
						formData.append('feeOfficeDesks',JSON.stringify(this.feeOfficeDesks));
						formData.append('sharedoffice_status',this.sharedOfficeData.statusChecked);
						formData.append('sharedoffice_status',this.sharedOfficeData.statusChecked);


						formData.append('sharedoffice_status',this.sharedOfficeData.statusChecked);
						formData.append('verify_info_to_scan',this.sharedOfficeData.verify_info_to_scan);
						formData.append('require_deposit',this.sharedOfficeData.require_deposit);

						var errorHotDesks = errorDedicatedDesks = errorOfficeDesks = false;
						this.feeHotDesks.forEach(function(data) {
							Object.entries(data).map(function(key, val) {
								if(key[1] === "") {
									toastr.error("Please enter hot desk "+key[0].replace(/_/g,' ')+" value");
									errorHotDesks = true;
								}
							})
						});
						this.feeDedicatedDesks.forEach(function(data) {
							Object.entries(data).map(function(key, val) {
								if(key[1] === "") {
									toastr.error("Please enter dedicated desk "+key[0].replace(/_/g,' ')+" value");
									errorDedicatedDesks = true;
								}
							})
						});
						this.feeOfficeDesks.forEach(function(data) {
							Object.entries(data).map(function(key, val) {
								if(key[1] === "") {
									toastr.error("Please enter office desk "+key[0].replace(/_/g,' ')+" value");
									errorOfficeDesks = true;
								}
							})
						});

						if (errorHotDesks === true || errorDedicatedDesks === true || errorOfficeDesks === true) {}


						if(this.sharedOfficeData.image == undefined && this.sharedOfficeData.image == null)
						{
							toastr.error("Please select image");
						}
						else if(this.sharedOfficeData.pic_size == '')
						{
							toastr.error('Please select pic size');
						}
						else if(this.sharedOfficeData.bulletins == '')
						{
							toastr.error('Please select bulletins');
						}
						else if(this.sharedOfficeData.shfacilities == '')
						{
							toastr.error('Please select facilities');
						}
						else if(this.sharedOfficeData.location == '')
						{
							toastr.error('Please select location');
						} else if(!this.sharedOfficeData.saturday_on) {
							if(this.sharedOfficeData.saturday_opening_time == '') {
								toastr.error('Please fill saturday opening time field');
							} else if(this.sharedOfficeData.saturday_closing_time == '') {
								toastr.error('Please fill saturday closgin time  field');
							}
						} else if(!this.sharedOfficeData.sunday_on) {
							if(this.sharedOfficeData.sunday_opening_time == '') {
								toastr.error('Please fill sunday opening hours field');
							} else if(this.sharedOfficeData.sunday_closing_time == '') {
								toastr.error('Please fill sunday closing hours field');
							}
						} else if(this.sharedOfficeData.monday_opening_time == '')
						{
							toastr.error('Please fill operating hours field');
						}
						else if(this.sharedOfficeData.monday_closing_time == '')
						{
							toastr.error('Please fill operating hours field');
						}
						else if(this.sharedOfficeData.office_manager_email == '')
						{
							toastr.error('Please enter office manager email');
						}
						else
						if (!this.sharedOfficeData.office_name && !this.sharedOfficeData.office_name_cn) {
							toastr.error('Please enter office name');
						}
						else if (!this.sharedOfficeData.description && !this.sharedOfficeData.description_cn) {
							toastr.error('Please enter office description');
						}
						else if (!this.sharedOfficeData.office_space && !this.sharedOfficeData.office_space_cn) {
							toastr.error('Please enter office space');
						}
						else if (!this.sharedOfficeData.contact_name && !this.sharedOfficeData.contact_name_cn) {
							toastr.error('Please enter contact name');
						}
						else if (!this.sharedOfficeData.contact_email && !this.sharedOfficeData.contact_email_cn) {
							toastr.error('Please enter contact email');
						}
						else if (!this.sharedOfficeData.contact_phone  &&!this.sharedOfficeData.contact_phone_cn) {
							toastr.error('Please enter contact phone');
						}
						else if (!this.sharedOfficeData.contact_address && !this.sharedOfficeData.contact_address_cn) {
							toastr.error('Please enter contact address');
						}
						else if(this.sharedOfficeData.office_manager_email == '' || this.sharedOfficeData.office_manager_email == '')
						{
							toastr.error('Please enter office manager email');
						}
						// else if(this.sharedOfficeData.hot_desk_peoples == '') {
						// 	toastr.error('Please enter number of peoples in hot desk');
						// } else if(this.sharedOfficeData.hot_desk_hourly_price == '') {
						// 	toastr.error('Please enter hot desk hourly price');
						// } else if(this.sharedOfficeData.hot_desk_daily_price == '') {
						// 	toastr.error('Please enter hot desk daily price');
						// } else if(this.sharedOfficeData.hot_desk_weekly_price == '') {
						// 	toastr.error('Please enter hot desk weekly price');
						// } else if(this.sharedOfficeData.hot_desk_monthly_price == '') {
						// 	toastr.error('Please enter hot desk monthly price');
						// } else if(this.sharedOfficeData.dedicated_desk_peoples == '') {
						// 	toastr.error('Please enter number of peoples in dedicated desk');
						// } else if(this.sharedOfficeData.dedicated_desk_hourly_price == '') {
						// 	toastr.error('Please enter dedicated desk hourly price');
						// } else if(this.sharedOfficeData.dedicated_desk_daily_price == '') {
						// 	toastr.error('Please enter dedicated desk daily price');
						// } else if(this.sharedOfficeData.dedicated_desk_weekly_price == '') {
						// 	toastr.error('Please enter dedicated desk weekly price');
						// } else if(this.sharedOfficeData.dedicated_desk_monthly_price == '') {
						// 	toastr.error('Please enter dedicated desk monthly price');
						// } else if(this.sharedOfficeData.office_desk_peoples == '') {
						// 	toastr.error('Please enter number of peoples in office desk');
						// } else if(this.sharedOfficeData.office_desk_hourly_price == ''){
						// 	toastr.error('Please enter office desk hourly price');
						// } else if(this.sharedOfficeData.office_desk_daily_price == '') {
						// 	toastr.error('Please enter office desk daily price');
						// } else if(this.sharedOfficeData.office_desk_weekly_price == '') {
						// 	toastr.error('Please enter office desk weekly price');
						// } else if(this.sharedOfficeData.office_desk_monthly_price == '') {
						// 	toastr.error('Please enter office desk monthly price');
						// }
						else if(this.sharedOfficeData.office_size_x == '' || this.sharedOfficeData.office_size_y == '')
                        {
                            toastr.error('Please enter office size');
                        }
						else if(this.sharedOfficeData.seatsData == '')
						{
							toastr.error('Please select seats data');
						}
						else {
							this.$http.post(url, formData).then((response) => {
								if (response.body && response.body.status == 'success') {
									toastr.success(response.body.message, 'Success');
									setTimeout(function () {
										window.location.href = '/admin/sharedoffice';
									}, 3000)
								} else {
									toastr.error(response.body.message, 'Error');
								}
							}, (err) => {
								toastr.error(response.body.message, 'Error');
							});
						}
					},
					createGrid: function() {
						this.sharedOfficeData.office_size_x = this.sharedOfficeData.office_size_x > 0 ? parseInt(this.sharedOfficeData.office_size_x) : 0;
						this.sharedOfficeData.office_size_y = this.sharedOfficeData.office_size_y > 0 ? parseInt(this.sharedOfficeData.office_size_y) : 0;
						console.log('Create Grid Called');
						console.log(this.sharedOfficeData);
					},
					getX: function(){
						return this.sharedOfficeData.office_size_x;
					},
					checked: function(x, y) {
						$(".innerboxes").each(function() {
							this.style.border = '2px solid #E3E3E3';
						});
						$('#'+x+'_'+y).css('border' ,'2px solid #3FAF3D');
						var newSelectedSeat = {
							x: x,
							y: y
						};
						this.selected_seat = newSelectedSeat;
					},
					setSeatData: function() {
						var exist = this.filteredSeat();
						if(exist.length > 0) {
							alert('Seat number already consumed...!');
						} else {
							this.sharedOfficeData.seatsData.push({
								x: this.selected_seat.x,
								y: this.selected_seat.y,
								facility_number: this.facility_number,
								seat_number: this.seat_number
							});
							console.log('image source');
							console.log(this.facility_number);
							var imgSrc = this.getImageSource(this.facility_number);
							var html = "";
							html += "<img src='"+imgSrc+"' style='width: 34px; height:34px;'><br/>";
//                                html += this.seat_number;
//                                $('#'+this.selected_seat.x+'_'+this.selected_seat.y).html("<span style='color:red'>x</span>");
							$('#'+this.selected_seat.x+'_'+this.selected_seat.y).html(html);
						}
					},
					getWidth: function() {
						console.log('x called');
						var widthVal = this.sharedOfficeData.office_size_x*50;
						// if(widthVal < 100) {
						// 	widthVal = 100
						// }
						return { width: widthVal+'px' }
					},
					filteredSeat(){
						return this.sharedOfficeData.seatsData
							.filter((value) => {
								return (value.seat_number.match(this.seat_number));
							});
					},
					getImageSource: function(id) {
						var img = '';
						var imageSource = '{!! \App\Models\SharedOfficeProductCategory::get() !!}';
						imageSource = JSON.parse(imageSource);
						console.log(imageSource);
						$.each(imageSource, function (v) {
							if(this.id == id) {
								console.log(this.image);
								img = this.image;
							}
						});
						return '/'+img;
					},

					changeVersion: function() {
						console.log('change version now');
						console.log(this.sharedOfficeData.version_english);
						console.log(this.sharedOfficeData.version_chinese);
					},

					checkedboxval(){
						if(event.target.checked)
						{
							this.sharedOfficeData.statusChecked = 1;
						} else {
							this.sharedOfficeData.statusChecked = 0;
						}
					},

					addSpaceHotDesk(){
						console.log(this.feeHotDesks);
						console.log(this.feeHotDesks.length);
						this.feeHotDesks.push({
							type: '{{\App\Models\SharedOfficeFee::HOT_DESK_TYPE}}',
							category_id:'{{\App\Models\SharedOfficeProducts::CATEGORY_HOT_DESK}}',
							people: null,
							hourly: null,
							weekly: null,
							daily: null,
							monthly: null,
							availability_id: null
						});
						console.log(this.feeHotDesks);
						console.log(this.feeHotDesks.length);
					},
					addSpaceDediDesk: function () {
						this.feeDedicatedDesks.push({
							type: '{{\App\Models\SharedOfficeFee::DEDICATED_DESK_TYPE}}',
							category_id:'{{\App\Models\SharedOfficeProducts::CATEGORY_DEDICATED_DESK}}',
							people: null,
							hourly: null,
							weekly: null,
							daily: null,
							monthly: null,
							availability_id: null
						});
					},
					addSpaceOfficeDesk: function () {
						this.feeOfficeDesks.push({
							type: '{{\App\Models\SharedOfficeFee::OFFICE_DESK_TYPE}}',
							category_id:'{{\App\Models\SharedOfficeProducts::CATEGORY_MEETING_ROOM}}',
							people: null,
							hourly: null,
							weekly: null,
							daily: null,
							monthly: null,
							availability_id: null
						});
					},
					removeElmHotDesk: function (id, index) {
						this.removeDesk(id);
						this.feeHotDesks.splice(index, 1);
					},
					removeElmDediDesk: function (id, index) {
						this.removeDesk(id);
						this.feeDedicatedDesks.splice(index, 1);
					},
					removeElmOfficeDesk: function (id, index) {
						this.removeDesk(id);
						this.feeOfficeDesks.splice(index, 1);
					},
					removeDesk: function(id) {
						this.$http.get('/admin/sharedoffice/delete/removeDesk/'+id).then((response) => {
							if (response.body.status == 'success') {
								toastr.success(response.body.message, 'Success');
							} else {
								toastr.error(response.body.message, 'Error');
							}
						}, (err) => {
							toastr.error(response.body.message, 'Error');
						});
					}

//					SaturdayUpdate: function(){
//
//						if(this.sharedOfficeData.saturday_on) {
//							$('#saturday_opening_time').attr('disabled', true);
//							$('#saturday_closing_time').attr('disabled', true);
//						} else {
//							$('#saturday_opening_time').removeAttr('disabled');
//							$('#saturday_closing_time').removeAttr('disabled');
//						}
//					},
//                    SundayUpdate: function(){
//						if(this.sharedOfficeData.sunday_on) {
//							$('#sunday_opening_time').attr('disabled', true);
//							$('#sunday_closing_time').attr('disabled', true);
//						} else {
//							$('#sunday_opening_time').removeAttr('disabled');
//							$('#sunday_closing_time').removeAttr('disabled');
//						}
//					}
				}
			})

		// $('#checkboxx').click(function(e){
		// 		if(e.target.checked === true){
		// 			alert("true");
		// 			var value1 = 1;
		// 			$(this).append(value1);
		// 		}else {
		// 			alert("false");
		// 			var value2 = 0;
		// 			$(this).append(value2);
		// 		}
		// });
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-helper">{{trans('sharedoffice.msg1')}}</span>
            </div>
            <div class="actions">

            </div>
        </div>
        <div class="portlet-body form">
            {{--{!! Form::open(['method' => 'post', 'role' => 'form', 'id' => 'sharedoffice-form']) !!}--}}
            <form method="POST" id="sharedoffice-form" v-on:submit.prevent="createSharedOffice" enctype="multipart/form-data"  role="form" novalidate>
                <div class="form-body">
                    <div class="form-group">
                        {!! Form::label('image',trans('sharedoffice.image'),['class'=>'control-label']) !!}
                        <div>
                            <img id="image_preview" class="img-thumbnail img-preview" alt="1366x768 or 683x768" src="{{ (isset($model) && $model->image) ? url($model->image) : '' }}"/>
                        </div>
                        {{--<input accept="image/*" class="image-upload" target="image_preview" onChange="imageFileInputChange(this)" name="image" type="file">--}}
                        <input type="file"
                               accept="image/*"
                               name="image_file"
                               id="image_file"
                               class="image-upload"
                               v-model="sharedOfficeData.image"
                               target="image_preview"
                               onChange="imageFileInputChange(this);"
                               v-validate="'required'"/>
                    </div>
                    <div class="form-group">
                        {!! Form::label('pic_size',trans('sharedoffice.picturesize'),['class'=>'control-label']) !!}
                        {{--                            {!! Form::select('pic_size', array('1' => '1366 X 768', '2' => '683 X 768'), '1', array('class' => 'form-control')); !!}--}}
                        <select v-model="sharedOfficeData.pic_size" class="form-control">
                            <option disabled value="">Please select Size</option>
                            <option value="1">1366 X 768</option>
                            <option value="2">683 X 768</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="bulletIns">Bulletins</label>
                        <select name="bulletIns[]" id="bulletIns" class="form-control" multiple v-model="sharedOfficeData.bulletins">
                            @foreach($bulletins as $k => $v)
                                <option value="{{$v->id}}">{{$v->text}}</option>
                            @endforeach
                        </select>
                    </div>

					<div class="form-group">
						<div class="checkbox" style="margin-left: 22px">
							<label><input type="checkbox" v-model="sharedOfficeData.verify_info_to_scan" v-value="1" checked>Verify Info </label>
						</div>
					</div>

					<div class="form-group">
						<div class="checkbox" style="margin-left: 22px">
							<label><input type="checkbox" v-model="sharedOfficeData.require_deposit" v-value="1" checked>Require Deposit</label>
						</div>
					</div>


                    {{ Form::label('shfacilities', 'Facility:') }}
                    {{--<select class="form-control select2-multi" name="shfacilities[]" multiple="multiple">--}}
                    {{--@foreach($shfacilities as $facility)--}}
                    {{--<option value='{{ $facility->id }}'>{{ $facility->title }}</option>--}}
                    {{--@endforeach--}}
                    {{--</select>--}}

                    <select v-model="sharedOfficeData.shfacilities" class="form-control" multiple>
                        <option disabled value="">Please select Size</option>
                        @foreach($shfacilities as $facility)
                            <option value='{{ $facility->id }}'>{{ $facility->title }}</option>
                        @endforeach
                    </select>

                    <div class="form-group">
                        <br/>
                        {!! Form::label('location',trans('sharedoffice.location'),['class'=>'control-label']) !!}
                        {{--<input type="text"--}}
                        {{--name="location"--}}
                        {{--class="form-control"--}}
                        {{--v-model="sharedOfficeData.location"--}}
                        {{--placeholder="{{trans('sharedoffice.enterlocation')}}"--}}
                        {{--v-validate="'required'"/>--}}

                        <input type="text"
                               name="location"
                               id="pac-input"
                               class="form-control"
                               v-model="sharedOfficeData.location"
                               placeholder="{{trans('sharedoffice.enterlocation')}}"
                               v-validate="'required'"/>

                        <div id="map"></div>

                        <div class="form-group">

                            <hr>

                            <div class="form-group">
                                <label for="bulletIns"><b>Opening Hours</b></label>
                            </div>

                            {{--<div class="form-group">--}}
                            {{--<div class="checkbox" style="display: none">--}}
                            {{--<label><input type="checkbox" value="">Option 1</label>--}}
                            {{--</div>--}}

                            {{--<div class="checkbox" style="margin-left: 22px">--}}
                            {{--<label><input type="checkbox" value="1" checked></label>--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            <div class="form-group">
                                <label for="contact_address"><b>Monday - Friday</b></label><br/>
                                <label for="contact_address">Opening Time</label>
                                <input type="time" name="monday_opening_time" id="monday_opening_time" class="form-control" v-model="sharedOfficeData.monday_opening_time">
                            </div>

                            <div class="form-group">
                                <label for="contact_address">Closing Time</label>
                                <input type="time" name="monday_closing_time" id="monday_closing_time" class="form-control" v-model="sharedOfficeData.monday_closing_time">
                            </div>

                            <div class="form-group">
                                <div class="checkbox" style="display: none">
                                    <label><input type="checkbox" value="">Option 1</label>
                                </div>

                                <div class="checkbox" style="margin-left: 22px">
                                    <label><input type="checkbox" v-model="sharedOfficeData.saturday_on" v-value="1" checked>OFFICE CLOSE</label>
                                </div>


                                <label for="contact_address"><b>Staurday</b></label><br/>
                                <label for="contact_address">Opening Time</label>
                                <input type="time" name="saturday_opening_time" id="saturday_opening_time" class="form-control" v-model="sharedOfficeData.saturday_opening_time" v-if="!sharedOfficeData.saturday_on" >
                                <input type="text" value="OFF DAY" id="saturday_opening_time_off" class="form-control" v-if="sharedOfficeData.saturday_on" readonly>
                            </div>

                            <div class="form-group">
                                <label for="contact_address">Closing Time</label>
                                <input type="time" name="saturday_closing_time" id="saturday_closing_time" class="form-control" v-model="sharedOfficeData.saturday_closing_time" v-if="!sharedOfficeData.saturday_on" >
                                <input type="text" value="OFF DAY" id="saturday_closing_time_off" class="form-control" v-if="sharedOfficeData.saturday_on" readonly>
                            </div>

                            <div class="form-group">
                                <div class="checkbox" style="display: none">
                                    <label><input type="checkbox" value="">Option 1</label>
                                </div>

                                <div class="checkbox" style="margin-left: 22px">
                                    <label><input type="checkbox" v-model="sharedOfficeData.sunday_on" v-value="1" checked>OFFICE CLOSE</label>
                                </div>


                                <label for="contact_address"><b>Sunday</b></label><br/>
                                <label for="contact_address">Opening Time</label>
                                <input type="time" name="sunday_opening_time" id="sunday_opening_time" class="form-control" v-model="sharedOfficeData.sunday_opening_time" v-if="!sharedOfficeData.sunday_on" >
                                <input type="text" value="OFF DAY" id="sunday_opening_time_off" class="form-control" v-if="sharedOfficeData.sunday_on" readonly>
                            </div>

                            <div class="form-group">
                                <label for="contact_address">Closing Time</label>
                                <input type="time" name="sunday_closing_time" id="sunday_closing_time" class="form-control" v-model="sharedOfficeData.sunday_closing_time" v-if="!sharedOfficeData.sunday_on" >
                                <input type="text" value="OFF DAY" id="sunday_closing_time_off" class="form-control" v-if="sharedOfficeData.sunday_on" readonly>

                            </div>

							<div class="tab-content" style="border: 1px solid lightgray;padding: 10px;margin-bottom: 11px;">
								<div class="form-group">
									<label for="bulletIns"><b>{{trans('sharedoffice.shared_office_manager')}}</b></label>
								</div>
								<div class="form-group">
									{!! Form::label('office_manager_email',trans('sharedoffice.shared_office_email'),['class'=>'control-label']) !!}
									<input type="text"
										   name="office_manger"
										   class="form-control"
										   v-model="sharedOfficeData.office_manager_email"
										   placeholder="{{trans('sharedoffice.shared_office_manager_email')}}"
										   v-validate="'required'"/>
								</div>
							</div>

                            <div class="form-group">
                                <label for="contact_address">Language Preference</label>
                                <div class="checkbox" style="display: none">
                                    <label><input type="checkbox" value="">Option 1</label>
                                </div>

                                <div class="checkbox" style="margin-left: 22px">
                                    <label><input type="checkbox" value="1" v-model="sharedOfficeData.version_english" @change="changeVersion" checked>English</label>
                                </div>
                                <div class="checkbox"  style="margin-left: 22px">
                                    <label><input type="checkbox" value="2" v-model="sharedOfficeData.version_chinese" @change="changeVersion">Chinese</label>
                                </div>
                            </div>



                            <hr>

                            <ul class="nav nav-pills">
                                <li class="active"><a data-toggle="pill" href="#ver_en" v-if="this.sharedOfficeData.version_english">English</a></li>
                                <li><a data-toggle="pill" href="#ver_cn"  v-if="this.sharedOfficeData.version_chinese">Chinese</a></li>
                            </ul>

                            <div class="tab-content" style="border: 1px solid lightgray;padding: 10px;margin-bottom: 11px;">
                                <div id="ver_en" class="tab-pane fade in active"  v-if="this.sharedOfficeData.version_english">
                                    <div class="form-group">
                                        <label for="bulletIns"><b>Office Detail</b></label>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('office_name',trans('sharedoffice.officename'),['class'=>'control-label']) !!}
                                        <input type="text"
                                               name="office_name"
                                               class="form-control"
                                               v-model="sharedOfficeData.office_name"
                                               placeholder="{{trans('sharedoffice.enterofficename')}}"
                                               v-validate="'required'"/>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('description',trans('sharedoffice.description'),['class'=>'control-label']) !!}
                                        <input type="text"
                                               name="description"
                                               class="form-control"
                                               v-model="sharedOfficeData.description"
                                               placeholder="{{trans('sharedoffice.enterdescription')}}"
                                               v-validate="'required'"/>
                                    </div>

                                    <div class="form-group">
                                        <label for="office_space">Office Space <small><b><i>(i.e. 2443m2)</i></b> </small></label>
                                        <input type="text" id="office_space" class="form-control" v-model="sharedOfficeData.office_space">
                                    </div>

                                    <div class="form-group">
                                        <label for="bulletIns"><b>Contact Detail</b></label>
                                    </div>

                                    <div class="form-group">
                                        <label for="contact_name">Name</label>
                                        <input type="text" id="contact_name" name="contact_name" class="form-control"  v-model="sharedOfficeData.contact_name">
                                    </div>

                                    <div class="form-group">
                                        <label for="contact_phone">Phone</label>
                                        <input type="text" id="contact_phone" name="contact_phone" class="form-control"  v-model="sharedOfficeData.contact_phone">
                                    </div>

                                    <div class="form-group">
                                        <label for="contact_email">Email</label>
                                        <input type="text" id="contact_email" name="contact_email" class="form-control"  v-model="sharedOfficeData.contact_email">
                                    </div>

                                    <div class="form-group">
                                        <label for="contact_address">Address</label>
                                        <input type="text" id="contact_address" name="contact_address" class="form-control"  v-model="sharedOfficeData.contact_address">
                                    </div>

                                </div>
                                <div id="ver_cn" class="tab-pane fade"  v-if="this.sharedOfficeData.version_chinese">
                                    <div class="form-group">
                                        <label for="bulletIns"><b>Office Detail (cn)</b></label>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('office_name',trans('sharedoffice.officename').'(cn)',['class'=>'control-label']) !!}
                                        <input type="text"
                                               name="office_name"
                                               class="form-control"
                                               v-model="sharedOfficeData.office_name_cn"
                                               placeholder="{{trans('sharedoffice.enterofficename')}} (cn)"
                                               v-validate="'required'"/>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('description (cn)',trans('sharedoffice.description').'(cn)',['class'=>'control-label']) !!}
                                        <input type="text"
                                               name="description"
                                               class="form-control"
                                               v-model="sharedOfficeData.description_cn"
                                               placeholder="{{trans('sharedoffice.enterdescription')}}"
                                               v-validate="'required'"/>
                                    </div>

                                    <div class="form-group">
                                        <label for="office_space">Office Space <small><b><i>(i.e. 2443m2)</i></b> </small> (cn)</label>
                                        <input type="text" id="office_space" class="form-control" v-model="sharedOfficeData.office_space_cn">
                                    </div>

                                    <div class="form-group">
                                        <label for="bulletIns"><b>Contact Detail</b></label>
                                    </div>

                                    <div class="form-group">
                                        <label for="contact_name">Name (cn)</label>
                                        <input type="text" id="contact_name" name="contact_name" class="form-control"  v-model="sharedOfficeData.contact_name_cn">
                                    </div>

                                    <div class="form-group">
                                        <label for="contact_phone">Phone (cn)</label>
                                        <input type="text" id="contact_phone" name="contact_phone" class="form-control"  v-model="sharedOfficeData.contact_phone_cn">
                                    </div>

                                    <div class="form-group">
                                        <label for="contact_email">Email (cn)</label>
                                        <input type="text" id="contact_email" name="contact_email" class="form-control"  v-model="sharedOfficeData.contact_email_cn">
                                    </div>

                                    <div class="form-group">
                                        <label for="contact_address">Address (cn)</label>
                                        <input type="text" id="contact_address" name="contact_address" class="form-control"  v-model="sharedOfficeData.contact_address_cn">
                                    </div>
                                </div>
                            </div>


							<div class="form-group set-office-fee">
								{!! Form::label('Office Fee',trans('sharedoffice.office_fee'),['class'=>'control-label']) !!}
								<div class="table-responsive">
									<table class="table table-bordered">
										<thead>
										<tr>
											<th>Type</th>
											<th>People</th>
											<th>Hourly Price</th>
											<th>Daily Price</th>
											<th>Weekly Price</th>
											<th>Monthly Price</th>
											<th>Availability</th>
											<th colspan="2">Action</th>

										</tr>
										</thead>
										<tbody>
										<template v-for="(feeHotDesk , index) in feeHotDesks">
											<tr>
												<td v-if="index == 0" :rowspan="feeHotDesks.length" style="vertical-align: middle;">Hot Desks</td>
												<input type="hidden" name="type" :value="feeHotDesk.type" v-model="feeHotDesk.type">
												<td><input type="text" name="hot_desk_peoples" v-model="feeHotDesk.no_of_peoples" class="form-control"></td>
												<td><input type="text" name="hot_desk_hourly_price" v-model="feeHotDesk.hourly_price" class="form-control"></td>
												<td><input type="text" name="hot_desk_daily_price" v-model="feeHotDesk.daily_price" class="form-control"></td>
												<td><input type="text" name="hot_desk_weekly_price" v-model="feeHotDesk.weekly_price" class="form-control"></td>
												<td><input type="text" name="hot_desk_monthly_price" v-model="feeHotDesk.month_price" class="form-control"></td>
												<td><input type="text" name="hot_desk_is_available" v-model="feeHotDesk.is_available" class="form-control"></td>
												<td v-if="feeHotDesks.length > 1">
													<button type="button" @click="removeElmHotDesk(feeHotDesk.id, index)">-</button>
												</td>
												<td v-if="index == 0" :rowspan="feeHotDesks.length" style="vertical-align: middle;text-align:center;">
													<button type="button" @click="addSpaceHotDesk">+</button>
												</td>
											</tr>
										</template>
										<template v-for="(feeDedicatedDesk, index) in feeDedicatedDesks">
											<tr>
												<td v-if="index == 0" :rowspan="feeDedicatedDesks.length" style="vertical-align: middle;">Dedicated Desks</td>
												<input type="hidden" name="type" v-model="feeDedicatedDesk.type">
												<td><input type="text" name="dedicated_desk_peoples" v-model="feeDedicatedDesk.no_of_peoples" class="form-control"></td>
												<td><input type="text" name="dedicated_desk_hourly_price" v-model="feeDedicatedDesk.hourly_price" class="form-control"></td>
												<td><input type="text" name="dedicated_desk_daily_price" v-model="feeDedicatedDesk.daily_price" class="form-control"></td>
												<td><input type="text" name="dedicated_desk_weekly_price" v-model="feeDedicatedDesk.weekly_price" class="form-control"></td>
												<td><input type="text" name="dedicated_desk_monthly_price" v-model="feeDedicatedDesk.month_price" class="form-control"></td>
												<td><input type="text" name="dedicated_desk_is_available" v-model="feeDedicatedDesk.is_available" class="form-control"></td>
												<td v-if="feeDedicatedDesks.length > 1"><button type="button" @click="removeElmDediDesk(feeDedicatedDesk.id, index)">-</button></td>
												<td v-if="index == 0" :rowspan="feeDedicatedDesks.length" style="vertical-align: middle;text-align:center;">
													<button type="button" @click="addSpaceDediDesk">+</button>
												</td>
											</tr>
										</template>
										<template v-for="(feeOfficeDesk, index) in feeOfficeDesks">
											<tr>
												<td v-if="index == 0" :rowspan="feeOfficeDesks.length" style="vertical-align: middle;">Office Desks</td>
												<input type="hidden" name="type" v-model="feeOfficeDesk.type">
												<td><input type="text" name="office_desk_peoples" v-model="feeOfficeDesk.no_of_peoples" class="form-control"></td>
												<td><input type="text" name="office_desk_hourly_price" v-model="feeOfficeDesk.hourly_price" class="form-control"></td>
												<td><input type="text" name="office_desk_daily_price" v-model="feeOfficeDesk.daily_price" class="form-control"></td>
												<td><input type="text" name="office_desk_weekly_price" v-model="feeOfficeDesk.weekly_price" class="form-control"></td>
												<td><input type="text" name="office_desk_monthly_price" v-model="feeOfficeDesk.month_price" class="form-control"></td>
												<td><input type="text" name="office_desk_is_available" v-model="feeOfficeDesk.is_available" class="form-control"></td>
												<td v-if="feeOfficeDesks.length > 1">
													<button type="button" @click="removeElmOfficeDesk(feeOfficeDesk.id, index)">-</button>
												</td>
												<td v-if="index == 0" :rowspan="feeOfficeDesks.length" style="vertical-align: middle;text-align:center;">
													<button type="button" @click="addSpaceOfficeDesk">+</button>
												</td>
											</tr>
										</template>

										</tbody>
									</table>
								</div>
							</div>


                            <div class="form-group">
                                {!! Form::label('Size: x - number rows  y -number columns','Office Size: x,y
X - number rows , Y -number columns',['class'=>'control-label']) !!}
                                {{--                        {!! Form::text('office_size_x','',['class'=>'form-control', 'placeholder' => 'Enter number rows x']) !!}--}}
                                {{--                        {!! Form::text('office_size_y','',['class'=>'form-control', 'placeholder' => 'Enter number columns y']) !!}--}}
                                <input type="text"
                                       name="office_size_x"
                                       class="form-control"
                                       v-model="sharedOfficeData.office_size_x"
                                       placeholder="Enter number rows x"
                                       autoComplete="off"
                                       v-on:blur="createGrid"
                                       v-validate="'required'"/>
                                <input type="text"
                                       name="office_size_y"
                                       class="form-control"
                                       v-model="sharedOfficeData.office_size_y"
                                       autoComplete="off"
                                       placeholder="Enter number rows y"
                                       v-on:blur="createGrid"
                                       v-validate="'required'"/>

                            </div>

                            <div class="row">
                                <div class="col-md-6 innerboxes-main-custom" style="overflow: auto; max-height: 300px">
                                    <div class="form-group boxes" :style="getWidth()" v-for="(n, x) in this.sharedOfficeData.office_size_y" style="overflow: auto; min-width: 100%;">
                                        <div class="innerboxes" v-for="(n1, y) in getX()" @click="checked(x, y)" :id="x+'_'+y" :data-x="x" :data-y="y">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 facility_number_custom_right">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <select v-model="facility_number" class="form-control" v-on:blur="createGrid" v-validate="'required'">
                                                <option disabled value="">Please select Type</option>
                                                @foreach($sharedOfficeProductCategories as $k => $v)
                                                    <option value="{{$v->id}}">{{$v->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <input v-if="this.sharedOfficeData.office_size_x != '' && this.sharedOfficeData.office_size_y != ''"
                                                   type="text"
                                                   name="office_size_y"
                                                   class="form-control"
                                                   v-model="seat_number"
                                                   autoComplete="off"
                                                   placeholder="Seat Number"
                                                   v-on:blur="createGrid"
                                                   v-validate="'required'"/>
                                        </div>
                                        <div class="col-md-4">
                                            <input  v-if="this.sharedOfficeData.office_size_x != '' && this.sharedOfficeData.office_size_y != ''" type="button" class="btn btn-success" value="Save" @click="setSeatData">
                                            <br  v-if="this.sharedOfficeData.office_size_x != '' && this.sharedOfficeData.office_size_y != ''"/>
                                            <br  v-if="this.sharedOfficeData.office_size_x != '' && this.sharedOfficeData.office_size_y != ''"/>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <strong> Information: </strong>
                                            <br><br>
                                        </div>
                                        <tr class="col-md-12">
                                            <div class="table-responsive">
                                            <table  v-if="this.sharedOfficeData.office_size_x != '' && this.sharedOfficeData.office_size_y != ''" >
                                                @php
                                                    $d = $sharedOfficeProductCategories->toArray();
                                                @endphp
                                                @foreach(array_chunk($d, 3) as $chunk)
                                                    <tr>
                                                        @foreach($chunk as $add)
                                                            @php
                                                                $name = explode('.', $add['name']);
                                                                $name =  $name[1];
                                                            @endphp
                                                            <td width="30px"><img src="{{asset($add['image'])}}" style="width: 30px" alt=""></td>
                                                            <td>{{$name}}</td>
                                                        @endforeach
                                                    </tr>
                                                @endforeach
                                            </table>
                                            </div>
                                        {{--<hr>--}}
                                        {{--<hr>--}}
                                        {{--<hr>--}}
                                        {{--<table  v-if="this.sharedOfficeData.office_size_x != '' && this.sharedOfficeData.office_size_y != ''" >--}}
                                        {{--<tr>--}}
                                        {{--<td width="30px"><img src="{{asset('assets/office/seat.png')}}" style="width: 30px" alt=""></td>--}}
                                        {{--<td>Hot Desk</td>--}}
                                        {{--<td width="30px"><img src="{{asset('assets/office/seat.png')}}" style="width: 30px" alt=""></td>--}}
                                        {{--<td>Dedicated Desk</td>--}}
                                        {{--<td width="30px"><img src="{{asset('assets/office/door.png')}}" style="width: 30px" alt=""></td>--}}
                                        {{--<td>Door</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                        {{--<td><img src="{{asset('assets/office/vendor_machine.png')}}" style="width: 30px" alt=""></td>--}}
                                        {{--<td>Vending Machine</td>--}}
                                        {{--<td><img src="{{asset('assets/office/phone_room.png')}}" style="width: 30px" alt=""></td>--}}
                                        {{--<td>Phone Room</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                        {{--<td><img src="{{asset('assets/office/coffee_machine.png')}}" style="width: 30px" alt=""></td>--}}
                                        {{--<td>Coffee Machine</td>--}}
                                        {{--<td><img src="{{asset('assets/office/dining_room.png')}}" style="width: 30px" alt=""></td>--}}
                                        {{--<td>Dining Room</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                        {{--<td><img src="{{asset('assets/office/vr_room.png')}}" style="width: 30px" alt=""></td>--}}
                                        {{--<td>VR Room</td>--}}
                                        {{--<td><img src="{{asset('assets/office/table.png')}}" style="width: 30px" alt=""></td>--}}
                                        {{--<td>Table</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                        {{--<td><img src="{{asset('assets/office/wall.png')}}" style="width: 30px" alt=""></td>--}}
                                        {{--<td>Wall 1</td>--}}
                                        {{--<td><img src="{{asset('assets/office/meeting_room.png')}}" style="width: 30px" alt=""></td>--}}
                                        {{--<td>Meeting Room</td>--}}
                                        {{--</tr>--}}
                                        {{--</table>--}}
                                    </div>
                                </div>
                            </div>
							<div class="row">
								<div class="col-md-12">
									<h4>Active</h4>
									<input type="checkbox" id="checkboxx" name="checkbox" @click="checkedboxval()"  v-model="sharedOfficeData.statusChecked">
								</div>
							</div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn blue">{{trans('sharedoffice.save')}}</button>
                        {!! Html::linkRoute('backend.sharedoffice', trans('sharedoffice.cancel'), array(), array('class' => 'btn btn-danger')) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
        </div>
        </form>
        {{--<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />--}}
        {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>--}}
        <script type="text/javascript">
					$('.select2-multi').select2();
					//                $('#bulletIns').select2();
        </script>
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCIasatwSA8nyOeN8Cobzil6yO1jsT13rE&callback=initMap&libraries=places,controls"></script>
        <script>
					//map.js

					//Set up some of our variables.
					var map; //Will contain map object.
					var marker = false; ////Has the user plotted their location marker?
					window.markers = []; ////Has the user plotted their location marker?
					window.selectedPlace = []; ////Has the user plotted their location marker?

					//Function called to initialize / create the map.
					//This is called when the page has loaded.
					function initMap() {

						//The center location of our map.
						var centerOfMap = new google.maps.LatLng(52.357971, -6.516758);

						//Map options.
						var options = {
							center: centerOfMap, //Set center.
							zoom: 7, //The zoom value.
							mapTypeId: 'roadmap'
						};

						//Create the map object.
						map = new google.maps.Map(document.getElementById('map'), options);

						//Listen for any clicks on the map.
						google.maps.event.addListener(map, 'click', function(event) {
							console.log('click markers');
							console.log(event.latLng.lng());

							removeAllMarkersFromMap(window.markers, marker);

							var geocoder = new google.maps.Geocoder;
							var latlng = {lat: parseFloat(event.latLng.lat()), lng: parseFloat(event.latLng.lng())};
							geocoder.geocode({'location': latlng}, function(place, status) {
								if (status === 'OK') {
									pickRequiredInformation(place[0]);
								} else {
									window.alert('Geocoder failed due to: ' + status);
								}
								console.log(window.selectedPlace);
							});
							//Get the location that the user clicked.
							var clickedLocation = event.latLng;
							//If the marker hasn't been added.
							if(marker === false){
								//Create the marker.
								marker = new google.maps.Marker({
									position: clickedLocation,
									title: 'place.name',
									map: map,
									draggable: true //make it draggable
								});
								//Listen for drag events!
								google.maps.event.addListener(marker, 'dragend', function(event){
									// markerLocation();
								});
							} else{
								//Marker has already been added, so just change its location.
								marker.setPosition(clickedLocation);
							}
							//Get the marker's location.
							// markerLocation();
						});

						// Create the search box and link it to the UI element.
						var input = document.getElementById('pac-input');
						var searchBox = new google.maps.places.SearchBox(input);
						// map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

						// Bias the SearchBox results towards current map's viewport.
						map.addListener('bounds_changed', function() {
							searchBox.setBounds(map.getBounds());
						});

						// Listen for the event fired when the user selects a prediction and retrieve
						// more details for that place.
						searchBox.addListener('places_changed', function() {
							console.log('place searched markers');
							console.log(window.markers);

							var places = searchBox.getPlaces();

							if (places.length == 0) {
								return;
							}

							// Clear out the old markers.
							window.markers.forEach(function(marker) {
								console.log('in loop of seach box')
								console.log(marker)
								marker.setMap(null);
							});
							window.markers = [];

							// For each place, get the icon, name and location.
							var bounds = new google.maps.LatLngBounds();
							places.forEach(function(place) {
								if (!place.geometry) {
									return;
								}
								var icon = {
									url: place.icon,
									size: new google.maps.Size(71, 71),
									origin: new google.maps.Point(0, 0),
									anchor: new google.maps.Point(17, 34),
									scaledSize: new google.maps.Size(25, 25)
								};

								// Create a marker for each place.
								window.markers.push(new google.maps.Marker({
									map: map,
									title: place.name,
									position: place.geometry.location,
									draggable: true
								}));

								if (place.geometry.viewport) {
									// Only geocodes have viewport.
									bounds.union(place.geometry.viewport);
								} else {
									bounds.extend(place.geometry.location);
								}
								console.log('place.geometry.location');
								pickRequiredInformation(place);
							});
							map.fitBounds(bounds);
						});
					}

					//This function will get the marker's current location and then add the lat/long
					//values to our textfields so that we can save the location.
					//    function markerLocation(){
					//     //Get location.
					//     var currentLocation = marker.getPosition();
					//     //Add lat and lng values to a field that we can save.
					//     document.getElementById('lat').value = currentLocation.lat(); //latitude
					//     document.getElementById('lng').value = currentLocation.lng(); //longitude
					// }

					function removeAllMarkersFromMap(markers, marker) {
						// Clear out the old markers.
						markers.forEach(function(marker) {
							marker.setMap(null);
						});
						markers = [];
					}

					function pickRequiredInformation(place) {
						window.selectedPlace = [];
						var city = [];
						var state = [];
						var country = [];
						window.selectedPlace = [];
						place.address_components.forEach(function(v){
							console.log(v);
							if(v.types[0] == 'country') {
								country = {
									'long_name': v.long_name,
									'short_name': v.short_name
								}
							}

							if(v.types[0] == 'administrative_area_level_1') {
								state = {
									'long_name': v.long_name,
									'short_name': v.short_name
								}
							}

							if(v.types[0] == 'locality') {
								city = {
									'long_name': v.long_name,
									'short_name': v.short_name
								}
							}
						});
						var placeName = place.name ? place.name : place.formatted_address;
						window.selectedPlace.push({
							'address': place.address_components,
							'formatted_address': place.formatted_address,
							'name': place.name,
							'city': city,
							'country': country,
							'state': state,
							'lat': place.geometry.location.lat(),
							'lng': place.geometry.location.lng()
						});
						console.log('window.selectedPlace');
						console.log(window.selectedPlace);
						$('#pac-input').val(placeName)
					}

					//Load the map when the page has finished loading.
					google.maps.event.addDomListener(window, 'load', initMap);
        </script>

@endsection

@section('modal')

@endsection


