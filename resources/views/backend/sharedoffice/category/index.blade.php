@extends('backend.layout')

@section('title')
    Shared Office Category
@endsection
@section('description')

@endsection
<head xmlns:v-on="http://www.w3.org/1999/xhtml">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="{{asset('js/vue.min.js')}}"></script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">
    <style>
        .custom-btn-category{
            background-color:rgb(37, 206, 15) !important;
        }
        .custom-row-category{
            display: flex;
            align-items: center;
        }
        [v-cloak] {
            display: none;
        }
        .filter-option > .pull-left{
            font-size: 12px !important;
        }
        .page-title{
            padding: 15px !important;
            margin-bottom: 10px !important;
            display: none !important;
        }
        .worksheet{
            box-shadow: 0px 0px 6px -3px #2b3643;
            margin-top: 12px;
            padding: 15px;
            border-radius: 10px;
        }
        .customSetStyle{
            padding: 15px;
            margin: 0px;
            font-size: 25px;
            font-weight: bold;
            color: #2b3643;
        }
        .customSetStyleN{
            margin: 0px;
            padding: 15px;
            color: rgb(37, 206, 15);
            font-weight: bold;
        }
        .saveBtnBtn{
            background-color: rgb(37, 206, 15) !important;
            border: 0px !important;
            width: 100%;
            height: 45px;
            font-size: 16px !important;
            font-weight: bold !important;
        }
        .customRowSetting{
            display: flex;
            padding: 20px;
        }
        .setDisplaycol3Set{
            display: flex;
            align-items: center;
            width: 100%;
        }
        .setDisplaycol4Set{
            display: flex;
            width: 100%;
            align-items: center;
        }
        .form-control {
            outline: 0!important;
            padding: 0px !important;
            border: 0px !important;
        }
        .setBoxOffer{
            border: 1px solid #efefef;
            padding: 0px 9px 0px 15px;
            border-radius: 3px;
            display: flex;
            align-items: center;
            width: 72px;
        }
        .Label2{
            width: 180px;
        }
        .Label1{
            width: 150px;
        }
        .checkOpt{
            width: 70px !important;
        }
        .checkSecondOpt{
            width: 70px !important;
        }



        /* custom check box area */
        /* The container */
        .containerC {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 30px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default checkbox */
        .containerC input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom checkbox */
        .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
        }

        /* On mouse-over, add a grey background color */
        .containerC:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a blue background */
        .containerC input:checked ~ .checkmark {
            background-color: rgb(37, 206, 15) !important;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .containerC input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .containerC .checkmark:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
        /* custom check box area */
        @media (max-width: 500px) {
            .setDisplaycol3Set{
                padding: 0px !important;
            }
            .customSetStyle{
                text-align: center;
            }
        }

        .lds-ring {
            display: inline-block;
            position: relative;
            width: 20px;
            height: 20px;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 20px;
            height: 20px;
            margin: 1px;
            border: 2px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
        .saveBtnBtn{
            display: flex !important;
            justify-content: center;
            align-items: center;
            width: 150px;
            height: 45px;
        }

        .lds-ring2 {
            display: inline-block;
            position: relative;
            width: 20px;
            height: 20px;
        }
        .lds-ring2 div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 20px;
            height: 20px;
            margin: 1px;
            border: 2px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }
        .lds-ring2 div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring2 div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring2 div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring2 {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
        .form-control {
            width: 100%;
            height: 34px;
            padding: 6px 12px !important;
            background-color: #fff;
            border: 1px solid #c2cad8 !important;
            border-radius: 4px;
        }
    </style>
</head>
@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Categories</a>
            <i class="fa fa-circle"></i>
        </li>
    </ul>
@endsection
@section('footer')
    <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <script>
        $('.scroll-div').slimScroll({});
    </script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/vue.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vue-resource.min.js')}}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.15/lodash.core.min.js"></script>
    <script>
        let vu = new Vue({
            el: '#categories',
            data: {
                confirm_password:'',
                password :'',
                loaderLoad:true
            },
            methods: {
            },
            mounted() {
            }
        });

        $(document).ready(function(){
            $('.openPopup').on('click',function(){
                var dataType = $(this).attr('data-type');
                var selectedTypeId , selectedTypeName , html;
                var types = @json($types);
                console.log('types');
                $('#selectTypes').html('');
                $('#selectTypes').append('');
                types.map((data) => {
                    console.log('data');
                    console.log(data);
                    if(data.id == dataType) {
                        selectedTypeId = data.id;
                        selectedTypeName = data.name;
                        html = '<option selected="selected" value='+selectedTypeId+'>'+selectedTypeName+'</option>'
                    } else {
                        html = '<option value='+data.id+'>'+data.name+'</option>'
                    }
                    $('#selectTypes').append(html);
                });
                var dataId = $(this).attr('data-id');
                var dataName = $(this).attr('data-name');
                var dataImage = $(this).attr('data-image');
                console.log('dataName dataImage');
                console.log(dataName);
                console.log(dataImage);
                $('#name').val(dataName);
                $('#image').val('/'+dataImage);
                $('#CID').val(dataId);
                $('.modalTitle').html('Edit Category')
                $('.changeBtn').html('Update');
                let url = '{{route('backend.updateCategory')}}';
                $('.formTab').attr('action',url);
                $('#EditCategoryModal').modal({show:true});
            });
        });
    </script>
@endsection
@section('content')
    <div class="worksheet" id="categories">
        <h1 class="customSetStyleN">Manage Categories</h1>
        <div class="row">
            <div class="custom-row-category">
                <div class="col-md-6">
                    <h1 class="customSetStyle">Categories</h1>
                </div>
                <div class="col-md-6 text-right">
                    <button class="btn btn-success custom-btn-category" data-toggle="modal" data-target="#addCategoryModal">Add Category</button>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            @if(session()->has('success_message'))
                <div class="alert alert-success">
                    {{session()->get('success_message')}}
                </div>
            @endif
            @if(session()->has('error_message'))
                <div class="alert alert-danger">
                    {{session()->get('error_message')}}
                </div>
            @endif
        </div>
        <div class="row customRowSetting">
            <div class="container">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($categories as $category)
                                <tr>
                                    <td>{{$category->id}}</td>
                                    <td>{{$category->name}}</td>
                                    <td>{{$category->getTypeName($category->type)}}</td>
                                    <td>
                                        <img style="width: 40px;" src="/{{$category->image}}" />
                                    </td>
                                    <td>
                                        <i class="fa fa-edit openPopup" data-id="{{$category->id}}" data-name="{{$category->name}}"
                                           data-type="{{$category->type}}"
                                           data-image="{{$category->image}}" style="cursor:pointer;"></i>
                                        <i class="fa fa-trash" style="color: red;cursor: pointer" onclick="window.location.href='{{route('backend.deleteCategory',[$category->id])}}'"></i>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <br>
        <!-- Edit Modal -->
        <div id="EditCategoryModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title modalTitle">Add Category</h4>
                </div>
                <form method="POST" class="formTab" action="{{route('backend.AddCategory')}}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id" id="CID" />
                    <div class="modal-body modalBody">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Category Name:</label>
                                    <input type="text" id="name" name="name" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Category Image:</label>
                                    <input type="file" name="image" class="form-control" />
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Type:</label>
                                    <select name="type" id="selectTypes" class="form-control">
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success changeBtn">Save</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

        <!-- Modal -->
        <div id="addCategoryModal" class="modal fade" role="dialog">

        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Category</h4>
                </div>
                <form method="POST" action="{{route('backend.AddCategory')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Category Name:</label>
                                    <input type="text" required name="name" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Category Image:</label>
                                    <input type="file" name="image" required class="form-control" />
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Type:</label>
                                    <select name="type" required class="form-control">
                                        @foreach($types as $type)
                                            <option value="{{$type['id']}}">{{$type['name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Save</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
    </div>
@endsection
