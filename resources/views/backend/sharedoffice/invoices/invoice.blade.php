@extends('backend.layout')

@section('title')
    {{trans('common.invoices')}}
@endsection
@section('description')

@endsection
<head xmlns:v-on="http://www.w3.org/1999/xhtml">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="{{asset('js/vue.min.js')}}"></script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">
    <link href="{{asset('custom/css/backend/workingsheet.css')}}" type="text/css" rel="stylesheet">
    <style>
        html,p,body,h1,span,a,button,tr,td,th{
            font-family: "Montserrat", sans-serif;
        }
        [v-cloak] {
            display: none;
        }
        .filter-option > .pull-left{
            font-size: 12px !important;
        }
        .page-title{
            padding: 15px !important;
            margin-bottom: 10px !important;
            display: none !important;
        }
        .worksheet{
            box-shadow: 0px 0px 6px -3px #2b3643;
            margin-top: 12px;
            padding: 15px;
            border-radius: 10px;
        }
        .customSetStyle{
            padding: 15px;
            margin: 0px;
            font-size: 25px;
            font-weight: bold;
            color: #2b3643;
        }
        .customSetStyleN{
            margin: 0px;
            padding: 15px;
            color: rgb(37, 206, 15);
            font-weight: bold;
        }
        .saveBtnBtn{
            background-color: rgb(37, 206, 15) !important;
            border: 0px !important;
            width: 100%;
            height: 45px;
            font-size: 16px !important;
            font-weight: bold !important;
        }
        .customRowSetting{
            display: flex;
            padding: 20px;
        }
        .setDisplaycol3Set{
            display: flex;
            align-items: center;
            width: 100%;
        }
        .setDisplaycol4Set{
            display: flex;
            width: 100%;
            align-items: center;
        }
        .form-control {
            outline: 0!important;
            padding: 0px !important;
            border: 0px !important;
        }
        .setBoxOffer{
            border: 1px solid #efefef;
            padding: 0px 9px 0px 15px;
            border-radius: 3px;
            display: flex;
            align-items: center;
            width: 72px;
        }
        .Label2{
            width: 180px;
        }
        .Label1{
            width: 150px;
        }
        .checkOpt{
            width: 70px !important;
        }
        .checkSecondOpt{
            width: 70px !important;
        }



        /* custom check box area */
        /* The container */
        .containerC {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 30px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default checkbox */
        .containerC input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom checkbox */
        .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
        }

        /* On mouse-over, add a grey background color */
        .containerC:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a blue background */
        .containerC input:checked ~ .checkmark {
            background-color: rgb(37, 206, 15) !important;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .containerC input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .containerC .checkmark:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
        /* custom check box area */
        @media (max-width: 500px) {
            .setDisplaycol3Set{
                padding: 0px !important;
            }
            .customSetStyle{
                text-align: center;
            }
        }

        .lds-ring {
            display: inline-block;
            position: relative;
            width: 20px;
            height: 20px;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 20px;
            height: 20px;
            margin: 1px;
            border: 2px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
        .saveBtnBtn{
            display: flex !important;
            justify-content: center;
            align-items: center;
            width: 150px;
            height: 45px;
        }

        .lds-ring2 {
            display: inline-block;
            position: relative;
            width: 20px;
            height: 20px;
        }
        .lds-ring2 div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 20px;
            height: 20px;
            margin: 1px;
            border: 2px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }
        .lds-ring2 div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring2 div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring2 div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring2 {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

        @media print {
            .pTag {
                color:rgb(99, 184, 13) !important;
                -webkit-print-color-adjust: exact;
            }
        }

        @media (max-width: 500px) {
            #printPDF{
                display: none;
            }
            .row {
                margin-left: -15px;
                margin-right: -15px;
                display: flex;
                align-items: center !important;
                flex-direction: column;
            }
            #custom-set-md-6{
                justify-content: center !important;r;
            }
            #custom-set-md-2{
                text-align: center !important;
                width: 100% !important;
            }
            #custom-set-md-3{
                text-align: center !important;
                width: 100% !important;
                justify-content: center !important;
            }
            .pTag2{
                justify-content: center !important;
                text-align: center !important;
            }
            #email-set{
                margin: 10px !important;
            }
            #md-6-dates{
                width: 100% !important;
                text-align: end;
            }
            #container-1{
                width: 100% !important;
            }
        }
        .custom-ST{
            height: 23px;
            justify-content: center;
            margin-top: 20px;
            text-align: center;
        }
        .trtable{
            height: 200px;
        }
    </style>
</head>
@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Invoices</a>
            <i class="fa fa-circle"></i>
        </li>
    </ul>
@endsection
@section('footer')
    <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <script>
        $('.scroll-div').slimScroll({});
    </script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/vue.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vue-resource.min.js')}}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.15/lodash.core.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
    <script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
    <script src="https://raw.githack.com/eKoopmans/html2pdf/master/dist/html2pdf.bundle.js"></script>
    <script>
        let vu = new Vue({
            el: '#invoices',
            data: {
                confirm_password:'',
                password :'',
                loaderLoad:true
            },
            methods: {
                selectOption: function (val,event) {
                    $('.checkOpt').prop('checked', false);
                    console.log(event);
                    console.log(event.target.id);
                    let id = event.target.id;
                    $('#' + id).prop('checked', true);
                    $('#book_type').val(id);
                },

                selectSecondOption: function (val,event) {
                    $('.checkSecondOpt').prop('checked', false);
                    console.log(event);
                    console.log(event.target.id);
                    let id = event.target.id;
                    $('#' + id).prop('checked', true);
                    $('#cancel_type').val(id);
                }
            },
            mounted() {
            }
        });
        function saveInvoice() {
            let invoice_no = "{{$invoice_no}}";
            let staff_id = '{{$staff->id}}'
            let total_amount = '{{$total_amount}}';
            let date = '{{$date}}'
            let form = new FormData();
            form.append('invoice_no',invoice_no);
            form.append('staff_id',staff_id);
            form.append('total_amount',total_amount);
            form.append('date',date);
            let url = '{{route('backend.saveInvoiceRecord')}}'
            $.ajax({
                url:url,
                method:'POST',
                data:form,
                processData: false,
                contentType: false,

                success: function (response) {
                    console.log('response');
                    console.log(response);
                },
                error: function (error) {
                    console.log('error');
                    console.log(error);
                }
            })
        }
        function printScreen() {
            saveInvoice();

            $('#btnbtnPDF').hide();
            $('#printPDF').hide();
            $('.customSetStyleN').css('color','#5bb75b')
            window.print();
            closeWindow();
        }
        function closeWindow() {
            $('#btnbtnPDF').show();
            $('#printPDF').show();
            window.close();
        }

        function downloadPDF(){
            generatePDF();
        };

        function generatePDF() {
            $('#btnbtnPDF').hide();
            $('#faSpinner').show();
            $('#printPDF').hide();
            setTimeout(function () {
            },500);
            // Choose the element that our invoice is rendered in.
            const element = document.getElementById("invoices");
            // Avoid page-breaks on all elements, and add one before #page2el.
            var opt = {
                margin:         0.5,
                filename:     'invoice.pdf',
                image:        { type: 'pdf', quality: 0.98 },
                html2canvas:  { scale: 5 },
                jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait' },
                pagebreak: { mode: ['avoid-all', 'css', 'legacy'] }
                // pagebreak: { mode: 'avoid-all', before: '#page2el' }
            };
            let invoicePdf = html2pdf()
                .set(opt)
                .from(element)
                .save()
                .then((response) => {
                    console.log('response');
                    console.log(response);
                    toastr.success('PDF Download Successfully');
                    $('#faSpinner').hide();
                    $('#btnbtnPDF').show();
                    $('#showSpinner').hide();
                    $('#printPDF').show();
                });
        }
    </script>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="custom-ST">
                <i class="fa fa-spinner fa-spin" id="faSpinner" style="display:none;font-size:36px;"></i>
            </div>
        </div>
    </div>
    <div class="canvas_div_pdf" id="invoices" style="width:100%;
    border:0px solid #efefef;
    margin-top: 12px;
    padding: 15px;
    border-radius: 10px;">
        <div class="container" style="max-width: 100%;padding-left: 5px;padding-right: 5px;">
            <div class="row" style="display: flex;align-items: end;">
                <div id="custom-set-md-3" class="col-md-6" style="width:50%;display: flex;align-items: center;">
                    <div style="display: flex;align-items: center;margin-bottom: 12px;padding-left: 10px;">
                        <img style="width: 50px;" src="{{asset('images/Favicon.jpg')}}" />
                        <div style="padding-left: 2px;position: relative;
    top: 2px;">
                            <p class="pTag" style="margin: 0px; font-size: 28px;font-weight: bold; letter-spacing: 0px;color:#63B80D;display: flex;
    justify-content: flex-end;">XenRen</p>
                            <p class="pTag" style="color:#63B80D;font-size:8px;margin: 0px;position: relative;
    bottom: 6px;">{{trans('common.coworker_space')}}</p>
                        </div>
                    </div>
                </div>
                <div id="custom-set-md-2" class="col-md-6" style="width:50%;    text-align: end;">
                    <div class="container" style="width: 70%;    margin: 0px;
    display: inline-block;text-align:-webkit-center;">
                        <p class="pTag2" style="
                    margin: 0px;
                    color: #000;
                    align-items: center;
                    display: flex;
                    justify-content: flex-end;
                    font-size: 30px;
                    font-weight: 600;
                    ">{{trans('common.invi')}}</p>
                        <p class="pTag2" style="margin-top:10px;margin-bottom:10px;color: #000;text-align: end;font-weight: 500;font-size: 15px;">{{trans('common.invi_details')}}</p>
                    </div>
                </div>
            </div>
            <div class="row" style="display: flex;align-items: center;">
                <div id="custom-set-md-6" class="col-md-6" style="width:50%;position:relative;bottom:45px;display: flex;align-items: center;">
                    <p style="margin: 0px;
                padding-left: 15px;
                padding-right: 5px;
                color: #000;
                font-size: 15px;
                font-weight: 500;">
                        {{trans('common.to')}}&nbsp;:
                    </p>
                    <p style="text-transform: capitalize;margin:0px;font-weight:500;font-size: 15px;color: #5C5C5C;padding-left: 0px;">
                        @if($to != '')
                            {{$to->username}}
                        @endif
                    </p>
                    <br>
                    <div style="
                    position: absolute;
                    top: 25px;
                    padding-left: 15px;"
                    >
{{--                        <p style="margin-bottom: 3px;margin-top:2px;font-weight: 400;font-size: 12px;"></p>--}}
                        <p id="email-set" style="margin: 0px; font-weight: 400;font-size: 12px;">
                            @if($to != '')
                                {{$to->email}}
                            @endif
                        </p>
                    </div>
                </div>
                <div id="md-6-dates" class="col-md-6" style="width:50%;text-align: end">
                    <div id="container-1" class="container" style="width: 200px; margin: 0px; display: inline-block;">
                        <p style="
                    text-align: left;
                    padding-bottom: 2px;
                    margin: 0px;
                    font-size: 13px;
                    ">{{trans('common.dte')}}: &nbsp; <span style="position: absolute;right:31px;">{{\Carbon\Carbon::parse($date)->format('y:m:d')}}</span></p>
                        <p style="
                    text-align: left;
                    padding-bottom: 2px;
                    margin: 0px;
                    font-size: 13px;
                    font-weight: bold;"><b style="font-weight: 500;
    color: #000;">{{trans('common.dte_du')}}:&nbsp;<span style="position: absolute;right:31px;">{{\Carbon\Carbon::now()->format('y:m:d')}}</span></b></p>
                        <p style="
                    text-align: left;
                    padding-bottom: 2px;
                    margin: 0px;
                    font-size: 13px;
                    ">{{trans('common.tt_am')}}: <span style="position: absolute;right:31px;">&nbsp; ${{number_format($total_amount,2)}}</span></p>
                        <p style="
                    margin: 0px;
                    padding-bottom: 2px;
                    text-align: left;
                    font-size: 13px;
                    font-weight: bold;"><b style="font-weight: 500;
    color: #000;">{{trans('common.tt_due')}}: <span style="position: absolute;right:31px;">&nbsp; ${{number_format($total_amount,2)}}</span></b></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            @if(session()->has('success_message'))
                <div class="alert alert-success">
                    {{session()->get('success_message')}}
                </div>
            @endif
        </div>
        <div class="row customRowSetting">
            <div class="container" style="width:100%;">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <a class="btn btn-success" id="printPDF" style="border-radius:0px;" onclick="printScreen()">
                            {{trans('common.prt')}}
                        </a>
                        <a class="btn btn-success" id="btnbtnPDF" style="border-radius:0px;" onclick="window.location.href='{{route('backend.downloadPdf')}}'">
                            {{trans('common.d_pdf')}}
                        </a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered" style="border: 1px solid #d3d3d3;">
                        <thead>
                            <tr>
                                <th style="color: #000;font-weight:500;font-size:14px;" >{{trans('common.description')}}</th>
                                <th style="text-align: center;color: #000;font-size:14px;font-weight:500;">{{trans('common.t_min')}}.</th>
                                <th style="text-align: center;color: #000;font-size:14px;font-weight:500;">{{trans('common.amount')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($invoices as $invoice)
                                @if($invoice->comment != '')
                                <tr>
                                    <td>{{$invoice->comment}}</td>
                                    <td style="width: 13%;text-align: center;">20 Mins.</td>
                                    <td style="width: 13%;text-align: center;">$ {{number_format($invoice->amount,2)}}</td>
                                </tr>
                                @endif
                            @endforeach
                            @if(isset($invoices) && count($invoices) <= 2)
                            <tr>
                                <td class="trtable"></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @endif
                        <tr>
                            <td><b style="color:#000;font-weight: 600;font-size:18px;padding-left: 12px;">{{trans('common.tt')}}</b></td>
                            <td style="width: 13%;text-align: center;color: #000;font-weight:600;">40 {{trans('common.mns')}}.</td>
                            <td style="width: 13%;text-align: center;color: #000;font-weight:600;">$ {{number_format($total_amount,2)}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div style="display: flex; align-items: center;">
                        <div style="padding-top:12px;display: flex;align-items: center;margin-bottom: 0px;padding-left: 0px;">
                            <p style="margin: 0px;
                padding-left: 15px;
                padding-right: 8px;
                color: #000;
                font-size: 18px;
                font-weight: 500;">
                                {{trans('common.from')}}&nbsp;&nbsp;:
                            </p>
                            <p style="text-transform: capitalize;margin:0px;font-weight:500;font-size: 18px;color: #000;padding-left: 0px;font-weight: 500;">
                                {{trans('common.l_l')}}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div style="display: flex; align-items: center;">
                        <div style="padding-top:0px;display: flex;align-items: center;margin-bottom: 25px;padding-left: 15px;">
{{--                            <p style="text-transform: capitalize;margin:0px;font-size: 17px;color: #000;padding-left: 0px;font-weight: 300;">--}}
{{--                                Lorem Ipsum--}}
{{--                            </p>--}}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div style="display: flex; align-items: center;">
                        <div style="padding-top:0px;display: flex;align-items: center;margin-bottom: 12px;padding-left: 15px;">
                            <p style="text-transform: capitalize;margin:0px;font-size: 17px;color: #000;padding-left: 0px;font-weight: 500;">
                                {{trans('common.invi_created')}} :
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div style="display: flex; align-items: center;">
                        <div style="display: flex;align-items: center;margin-bottom: 12px;padding-left: 10px;">
                            <img style="width: 50px;" src="{{asset('images/Favicon.jpg')}}" />
                            <div style="padding-left: 2px;position: relative;
    top: 2px;">
                                <p class="pTag" style="margin: 0px; font-size: 25px;font-weight: bold; letter-spacing: 0px;color:#63B80D;display: flex;
    justify-content: flex-end;">XenRen</p>
                                <p class="pTag" style="color:#63B80D;font-size:7px;margin: 0px;position: relative;
    bottom: 6px;">{{trans('common.coworker_space')}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
