@extends('backend.layout')

@section('title')
    {{trans('sideMenu.sharedofficeNotifications')}}
@endsection
@section('description')

@endsection
<head xmlns:v-on="http://www.w3.org/1999/xhtml">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="{{asset('js/vue.min.js')}}"></script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">
    <link href="{{asset('custom/css/backend/workingsheet.css')}}" type="text/css" rel="stylesheet">
    <style>
        [v-cloak] {
            display: none;
        }
        .filter-option > .pull-left{
            font-size: 12px !important;
        }
        @media (max-width: 480px) {

            .page-content-wrapper .page-content {
                display: flex;
                justify-content: center;
                height: fit-content;
            }
            .customSetStyle{
                text-align: center;
                margin-top: 0px;
                font-size: 21px;
                font-weight: 400;
                color: #25ce0f;
                margin-bottom: 0px !important;
            }
        }
        .colMd3{
            width: 25%;
            justify-content: center;
            display: flex;
        }
        .colMd9{
            width: 75%;
            display: flex;
            justify-content: left;
            flex-direction: column;
        }
        .customH1se{
            color: black;
            font-size: 14px;
            margin: 0px;
            margin-bottom: 4px;
        }
        .customRowSetting{
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .setImageNotification{

        }
        .customParagraph{
            margin: 0px;
            font-size: 12px;
        }
        .setRow{
            display: flex;
            justify-content: center;
            width: 90%;
            border-bottom: 1px solid #F0F0F0;
            padding-bottom: 15px;
            align-items: center;
            padding-top: 15px;
        }
        .loader {
            font-size: 10px;
            margin: 50px auto;
            text-indent: -9999em;
            width: 11em;
            height: 11em;
            border-radius: 50%;
            background: #ffffff;
            background: -moz-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            background: -webkit-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            background: -o-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            background: -ms-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            background: linear-gradient(to right, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            position: relative;
            -webkit-animation: load3 1.4s infinite linear;
            animation: load3 1.4s infinite linear;
            -webkit-transform: translateZ(0);
            -ms-transform: translateZ(0);
            transform: translateZ(0);
        }
        .loader:before {
            width: 50%;
            height: 50%;
            background: #555;
            border-radius: 100% 0 0 0;
            position: absolute;
            top: 0;
            left: 0;
            content: '';
        }
        .loader:after {
            background: #fff;
            width: 75%;
            height: 75%;
            border-radius: 50%;
            content: '';
            margin: auto;
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }
        @-webkit-keyframes load3 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @keyframes load3 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        .setNotificationsImage{
            display: flex;
            justify-content: center;
            margin-top: 50px;
            flex-direction: column;
            align-items: center;
        }

        .imageNoti{
            width:100px;
            height: 101.3px;
        }
        .customMessageNotifi{
            color: #C8C8C8 !important;
            text-align: center;
            width: 80%;
            font-size: 15px;
            font-weight: 600;
        }
    </style>
    <style>
        @media (max-width: 991px) and (min-width: 320px) {
            .responsiveView{
                display: flex;
                justify-content: center;
            }
            .responsiveColMd6{
                width: 50%;
            }
        }
        .lds-ring {
            display: inline-block;
            position: relative;
            width: 22px;
            height: 24px;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 20px;
            height: 20px;
            margin: 0px;
            border: 2px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

        .style{
            display: flex;
        }
        .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
            background-color: #fff !important;
            opacity: 1;
            padding-left: 32px;
            font-size: 12px;
        }
        .mergeLays{
            display: flex;
            align-items: center;
        }
        .UserFa{
            position: absolute;
            left:25px;
        }
        .customSetStyle{
            padding: 15px;
            margin: 0px;
            font-size: 25px;
            font-weight: bold;
            color: #2b3643;
        }
        label {
            font-weight: bold !important;
            color: #2b3643 !important;
            font-size: 12px;
        }
        .customSetStyleN{
            margin: 0px;
            padding: 15px;
            color: rgb(37, 206, 15);
            font-weight: bold;
        }
        .page-title{
            padding: 15px !important;
            margin-bottom: 10px !important;
            display: none !important;
        }
        .displayBtn{
            display: flex;
        }
        #confirmBookingA{
            background-color: rgb(37, 206, 15);
            border: 0px;
            width: 100%;
            height: 45px;
            font-size: 16px;
            font-weight: bold;
        }
        #openCancelBooking{
            background-color: #2b3643;
            border: 0px;
            width: 100%;
            height: 45px;
            font-size: 16px;
            font-weight: bold;
        }
        .page-content-white .page-content .page-bar {
            border: 0px !important;
        }
        .worksheet{
            box-shadow: 0px 0px 6px -3px #2b3643;
            margin-top: 12px;
            padding: 15px;
            border-radius: 10px;
        }
        .colMd12Bottom{
            padding-bottom: 20px;
        }


        .lds-ring {
            display: inline-block;
            position: relative;
            width: 20px;
            height: 20px;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 20px;
            height: 20px;
            margin: 1px;
            border: 2px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
</head>
@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Notification Detail</a>
            <i class="fa fa-circle"></i>
        </li>
    </ul>
@endsection
@section('footer')
    <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <script>
        $('.scroll-div').slimScroll({});
    </script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/vue.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vue-resource.min.js')}}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.15/lodash.core.min.js"></script>
    <script>
        let vu = new Vue({
            el: '#profile',
            data: {
                confirm_password:'',
                password :'',
                loaderLoad:true,
                loaders:false,
                availableSeats:[],
                selectedAvailableSeat: 0,
                selectedBookingId: 0,
                cancelBookingId:0,
                cancelReason:'',
                confirmModalLoader:false,
                cancelModalLoader:false
            },
            methods: {
                unReadRemarks: function (url) {
                    this.loaders = true;
                    console.log('url');
                    console.log(url);
                    this.$http.get(url).then(function (response) {
                        console.log('response');
                        console.log(response);
                        setTimeout(function () {
                            window.location.reload();
                        },1500);
                    }, function (error) {
                        console.log('error');
                        console.log(error);
                    });
                },
                readRemarks: function (url) {
                    this.loaders = true;
                    this.$http.get(url).then(function (response) {
                        console.log('response');
                        console.log(response);
                        setTimeout(function () {
                            window.location.reload();
                        },1500);
                    }, function (error) {
                        console.log('error');
                        console.log(error);
                    });
                },
                cancelBooking: function(id){
                    this.cancelModalLoader = true;
                    console.log(id);
                    let self = this;
                    var formRequest = new FormData();
                    formRequest.append('csrf_token','{{ csrf_token() }}');
                    formRequest.append('booking_id',this.cancelBookingId);
                    formRequest.append('remarks',this.cancelReason);
                    this.$http.post('/admin/sharedOfficeBookingsCancel', formRequest).then(function (response) {
                        console.log('request withdraw response');
                        console.log(response.body);
                        console.log(response.body.status);
                        if (response.body.status == 'success') {
                            self.cancelModalLoader = false;
                            toastr.success(response.body.message);
                            setTimeout(function () {
                                window.location.href='{{route('backend.sharedofficeNotification')}}';
                            },1000);                        }
                    }, function (error) {
                        self.cancelModalLoader = false;
                        console.log('request withdraw error');
                        console.log(error);
                    })
                },
                openConfirmBooking: function(id){
                    console.log('open modal');
                    console.log(id);
                    let bid = id;
                    this.showLoader = true;
                    let self = this;
                    this.selectedBookingId = bid;
                    setTimeout(function () {
                        self.$http.get('/admin/sharedOfficeAvailaleSeats/'+bid).then(function (response) {
                            self.loaderLoad = false;
                            console.log('bookings');
                            console.log(response);
                            self.availableSeats = response.body.data;
                            console.log(self.availableSeats);
                        }, function (error) {
                            self.loaderLoad = false;
                        });
                    }, 100)
                },
                openCancelBooking: function(id){
                    console.log('open modal');
                    console.log(id);
                    $('#cancelBooking').modal('show');
                    this.cancelBookingId = id;
                },
                bookSeat: function(){
                    this.confirmModalLoader = true;
                    console.log(this.selectedAvailableSeat);
                    let self = this;
                    var formRequest = new FormData();
                    formRequest.append('csrf_token','{{ csrf_token() }}');
                    formRequest.append('seat_id',this.selectedAvailableSeat);
                    formRequest.append('booking_id',this.selectedBookingId);

                    self.$http.post('/admin/bookSeat', formRequest).then(function (response) {
                        console.log('request withdraw response');
                        console.log(response);
                        console.log(response.body.status);
                        self.confirmModalLoader = false;
                        $('#confirmBooking').modal('hide')
                        if (response.body.status == 'success') {
                            toastr.success('booking successfully... ! ');
                            setTimeout(function () {
                                window.location.href='{{route('backend.sharedofficeNotification')}}';
                            },1000);
                        }
                    }, function (error) {
                        self.confirmModalLoader = false;
                        console.log('request withdraw error');
                        console.log(error);
                    })
                }

            },
            mounted() {
                console.log("vue initialized");
                console.log("Notifications page");
                let self = this;
                setTimeout(() => {
                    self.loaderLoad = false
                },800)
            }
        });
    </script>
@endsection
@section('content')
    <div class="worksheet" id="profile" v-cloak>
        <h1 class="customSetStyleN">Notifications</h1>
        <h1 class="customSetStyle">Detail</h1>
        <br>
        <div v-if="loaderLoad">
            <div class="loader">Loading...</div>
        </div>
        @if(isset($detail))
            <div class="row displayRowNotifications" v-if="!loaderLoad">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Name:</label>
                            <div class="mergeLays">
                                <i class="UserFa fa fa-user" aria-hidden="true"></i>
                                <input type="text" readonly value="{{$detail->full_name}}" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>E-Mail Address:</label>
                            <div class="mergeLays">
                                <i class="UserFa fa fa-envelope" aria-hidden="true"></i>
                                <input type="text" readonly value="{{$detail->client_email}}" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Phone:</label>
                            <div class="mergeLays">
                                <i class="UserFa fa fa-phone" aria-hidden="true"></i>
                                <input type="text" readonly value="{{$detail->phone_no}}" class="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Check In :</label>
                            <div class="mergeLays">
                                <i class="UserFa fa fa-calendar" aria-hidden="true"></i>
                                <input type="text" readonly value="{{$detail->check_in}}" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Check Out :</label>
                            <div class="mergeLays">
                                <i class="UserFa fa fa-calendar" aria-hidden="true"></i>
                                <input type="text" readonly value="{{$detail->check_out}}" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>No of Rooms:</label>
                            <div class="mergeLays">
                                <i class="UserFa fa fa-home" aria-hidden="true"></i>
                                <input type="text" readonly value="{{$detail->no_of_rooms}}" class="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>No of Persons :</label>
                            <div class="mergeLays">
                                <i class="UserFa fa fa-user" aria-hidden="true"></i>
                                <input type="text" readonly value="{{$detail->no_of_persons}}" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Remarks :</label>
                            <div class="mergeLays">
                                <i class="UserFa fa fa-comments-o" aria-hidden="true"></i>
                                <input type="text" readonly value="{{$detail->remarks}}" class="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 colMd12Bottom">
                    <div class="col-md-3 displayBtn">
                        <button class="btn btn-sm btn-success" id="confirmBookingA" data-toggle="modal" data-target="#confirmBooking" @click="openConfirmBooking('{{$detail->id}}')">Confirm</button>
                    </div>
                    <div class="col-md-3 displayBtn">
                        <button class="btn btn-sm btn-danger" @click="openCancelBooking('{{$detail->id}}')" id="openCancelBooking">Cancel</button>
                    </div>
                </div>
            </div>
        @endif
        <div class="modal" id="confirmBooking" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Confirm Booking</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <bold for="seats">Available Seats:</bold>
                            <select name="" id="" class="from-control" v-model="selectedAvailableSeat">
                                <option value="" v-for="data in availableSeats" :value="data.id"> Seat # @{{ data.number }}</option>
                            </select>
                        </div>
                        <div class="style">
                            <div class="form-group">
                                <bold for="seats">To:</bold>
                                {{$detail->check_in}}
                            </div>
                            <div class="form-group" style="margin-left:4px;">
                                <bold for="seats">From:</bold>
                                {{$detail->check_out}}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="    display: flex;
    justify-content: flex-end;">
                        <button type="button"
                                style="
                                    display: flex;
                                    justify-content: center;
                                    align-items: center;
                                    width: 150px;
                                "
                            class="btn btn-primary" @click="bookSeat()">
                            <span id="isSpanSaveChangeNotification" v-if="!confirmModalLoader">Save changes</span>
                            <div v-if="confirmModalLoader" class="lds-ring">
                                <div></div><div></div><div></div><div></div>
                            </div>
                        </button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" id="cancelBooking" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Cancel Reason</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <bold for="seats">Enter Reason:</bold>
                            <input type="text" v-model="cancelReason" name="reason" class="form-control" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" style="width: 100px;" class="btn btn-primary" @click="cancelBooking()">
                            <span v-if="!cancelModalLoader">Confirm</span>
                            <div v-if="cancelModalLoader" class="lds-ring">
                                <div></div><div></div><div></div><div></div>
                            </div>
                        </button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


