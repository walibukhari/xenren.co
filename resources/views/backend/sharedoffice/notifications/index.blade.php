@extends('backend.layout')

@section('title')
    {{trans('sideMenu.sharedofficeNotifications')}}
@endsection
@section('description')

@endsection
<head xmlns:v-on="http://www.w3.org/1999/xhtml">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="{{asset('js/vue.min.js')}}"></script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">
    <link href="{{asset('custom/css/backend/workingsheet.css')}}" type="text/css" rel="stylesheet">
    <style>
        [v-cloak] {
            display: none;
        }
        .filter-option > .pull-left{
            font-size: 12px !important;
        }
        @media (max-width: 480px) {

            .page-content-wrapper .page-content {
                display: flex;
                justify-content: center;
                height: fit-content;
            }
            .customSetStyle{
                text-align: center;
                margin-top: 0px;
                font-size: 21px;
                font-weight: 400;
                color: #25ce0f;
                margin-bottom: 0px !important;
            }
        }
        .colMd3{
            width: 25%;
            justify-content: center;
            display: flex;
        }
        .colMd9{
            width: 75%;
            display: flex;
            justify-content: left;
            flex-direction: column;
        }
        .customH1se{
            color: black;
            font-size: 14px;
            margin: 0px;
            margin-bottom: 4px;
        }
        .customRowSetting{
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .setImageNotification{

        }
        .customParagraph{
            margin: 0px;
            font-size: 12px;
        }
        .setRow{
            display: flex;
            justify-content: center;
            width: 90%;
            border-bottom: 1px solid #F0F0F0;
            padding-bottom: 15px;
            align-items: center;
            padding-top: 15px;
        }
        .loader {
            font-size: 10px;
            margin: 50px auto;
            text-indent: -9999em;
            width: 11em;
            height: 11em;
            border-radius: 50%;
            background: #ffffff;
            background: -moz-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            background: -webkit-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            background: -o-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            background: -ms-linear-gradient(left, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            background: linear-gradient(to right, #ffffff 10%, rgba(255, 255, 255, 0) 42%);
            position: relative;
            -webkit-animation: load3 1.4s infinite linear;
            animation: load3 1.4s infinite linear;
            -webkit-transform: translateZ(0);
            -ms-transform: translateZ(0);
            transform: translateZ(0);
        }
        .loader:before {
            width: 50%;
            height: 50%;
            background: #555;
            border-radius: 100% 0 0 0;
            position: absolute;
            top: 0;
            left: 0;
            content: '';
        }
        .loader:after {
            background: #fff;
            width: 75%;
            height: 75%;
            border-radius: 50%;
            content: '';
            margin: auto;
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }
        @-webkit-keyframes load3 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }
        @keyframes load3 {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        .setNotificationsImage{
            display: flex;
            justify-content: center;
            margin-top: 50px;
            flex-direction: column;
            align-items: center;
        }

        .imageNoti{
            width:100px;
            height: 101.3px;
        }
        .customMessageNotifi{
            color: #C8C8C8 !important;
            text-align: center;
            width: 80%;
            font-size: 15px;
            font-weight: 600;
        }
    </style>
</head>
@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">{{trans('common.noti')}}</a>
            <i class="fa fa-circle"></i>
        </li>
    </ul>
@endsection
@section('footer')
    <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <script>
        $('.scroll-div').slimScroll({});
    </script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/vue.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vue-resource.min.js')}}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.15/lodash.core.min.js"></script>
    <script>
        let vu = new Vue({
            el: '#profile',
            data: {
                confirm_password:'',
                password :'',
                loaderLoad:true
            },
            methods: {
            },
            mounted() {
                console.log("vue initialized");
                console.log("Notifications page");
                let self = this;
                setTimeout(() => {
                    self.loaderLoad = false
                },800)
            }
        });
    </script>
@endsection
@section('content')
    <div class="worksheet" id="profile" v-cloak>
        <h1 class="customSetStyle">{{trans('common.noti')}}</h1>
        <br>
        <div v-if="loaderLoad">
            <div class="loader">{{trans('common.load')}}...</div>
        </div>
    @if(!is_null($bookings))
        @if(count($bookings) > 0)
            @foreach($bookings as $booking)
                <div v-if="!loaderLoad" class="row customRowSetting">
                    <div class="setRow" style="cursor:pointer;" onclick="window.location.href='{{route('backend.notificationDetails',[$booking->id])}}'">
                        <div class="colMd3">
                            <img class="setImageNotification" src="{{asset('images/notififcations.png')}}" />
                        </div>
                        <div class="colMd9">
                            <h1 class="customH1se">{{trans('common.n_b_c')}}</h1>
                            <p class="customParagraph">{{$booking->remarks}}</p>
                        </div>
                        <div class="colMD1">
                            {{$booking->created_at}}
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div v-if="!loaderLoad" class="setNotificationsImage">
                <img class="imageNoti" src="{{asset('images/ringingBell.png')}}" />
                <br>
                <h1 class="customMessageNotifi">
                    {{trans('common.no_n_h')}}
                </h1>
            </div>
        @endif
    @else
        <div v-if="!loaderLoad" class="setNotificationsImage">
            <img class="imageNoti" src="{{asset('images/ringingBell.png')}}" />
            <br>
            <h1 class="customMessageNotifi">
                {{trans('common.no_n_h')}}
            </h1>
        </div>
    @endif
    </div>
@endsection


