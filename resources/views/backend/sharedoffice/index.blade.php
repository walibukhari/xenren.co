@extends('backend.layout')

@section('title')
    {{trans('sharedoffice.sharedoffice')}}
@endsection

@section('description')
    {{trans('sharedoffice.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>

        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.sharedoffice')}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <style>
        #example_length {
            display: none;
        }
        .dataTables_filter {
            float: right;
        }
        #example tr:first-child th {
            color: #25ce0f !important;
            background-color: #EDEDED;
        }
        .dataTables_wrapper .dataTables_processing {
            margin-left: 40%;
        }
        .img-thumbnail {
            border: 0;
        }
    </style>
    @if(Auth::guard('staff')->user()->staff_type == \App\Models\Staff::STAFF_TYPE_STAFF)
        <link href="{{asset('custom/css/backend/staffDashboard.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('custom/css/backend/staffHeader.css')}}" rel="stylesheet" type="text/css">
    @endif
    @if($is_mobile)
        <style>
            .customsharedOfficeContainer{
                width: 100%;
            }
            .customRowCss {
                display: flex;
                margin-right: 0px !important;
                width: 100% !important;
                max-width: 100%;
                margin: 0px !important;
            }
            .customColMd6{
                flex:1;
                padding-left: 0px;
            }
            .customRowColMd62{
                padding-right: 0px;
                text-align: end;
            }
            .customRowOffice{
                display: flex;
            }
            .customCol6{
                flex:1;
            }
            .customCol6 p{
                color:#53C329;
            }
            .customdefaultBtn{
                border-radius: 6px !important;
                background: #53C329 !important;
                color: #fff;
                border: 0px !important;
                width: 140px;
            }
            .customContainerRow{
                border: 1px solid #F0F0F0;
                border-radius: 15px;
                margin-top: 20px;
            }
            .custombtnrow{
                display: flex;
                justify-content: center;
                margin-bottom: 12px;
            }
            .customRow1{
                display: flex;
                border:1px solid #F0F0F0;
                border-radius: 12px;
                margin-bottom: 8px;
            }
            .customCol6Md1{
                flex:1;
            }
            .customCol6Md1 a{
                color:#53C329;
            }
            .customCol6Md1 p{
                color:#53C329;
            }
            .page-content-wrapper .page-content{
                display: block !important;
            }
            .customRow1 p{
                margin: 10px;
                margin-left: 0px;
            }
            .setPS{
                color: #C0C0C0;
                margin: 10px;
                margin-bottom: 20px;
                margin-left: 0px;
                margin-right: 0px;
            }
            .customPrevious{
                border-radius: 10px !important;
                color:#53C329;
                width: 100px;
                border: 1px solid #53C329;
            }
            .customNext{
                border-radius: 10px !important;
                background: #53C329 !important;
                color:#fff;
                width: 100px;
                border: 0px;
            }
            .customRow2{
                display: flex;
            }
            .customP{
                padding-left: 8px;
                padding-right: 0px;
            }
            .customP1{
                padding-left: 0px;
                padding-right: 8px;
            }
            .customSttl{
                font-size: 18px;
                margin-top: 0px;
                margin-bottom: 20px;
                color:#53C329;
                font-weight: 500;
                display: flex;
                align-items: center;
            }
            .leftArrowImage{
                width: 20px;
                margin-right: 10px;
            }
        </style>
        <style>
            /* Always set the map height explicitly to define the size of the div
             * element that contains the map. */
            #map {
                height: 100%;
            }

            /* Optional: Makes the sample page fill the window. */
            html, body {
                height: 100%;
                margin: 0;
                padding: 0;
            }

            #description {
                font-family: Roboto;
                font-size: 15px;
                font-weight: 300;
            }

            #infowindow-content .title {
                font-weight: bold;
            }

            #infowindow-content {
                display: none;
            }

            #map #infowindow-content {
                display: inline;
            }

            .pac-card {
                margin: 10px 10px 0 0;
                border-radius: 2px 0 0 2px;
                box-sizing: border-box;
                -moz-box-sizing: border-box;
                outline: none;
                box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
                background-color: #fff;
                font-family: Roboto;
            }

            #pac-container {
                padding-bottom: 12px;
                margin-right: 12px;
            }

            .pac-container{
                z-index: 999999999999999999;
            }

            .pac-controls {
                display: inline-block;
                padding: 5px 11px;
            }

            .pac-controls label {
                font-family: Roboto;
                font-size: 13px;
                font-weight: 300;
            }

            #pac-input {
                background-color: #fff;
                font-family: Roboto;
                font-size: 15px;
                font-weight: 300;
                padding: 0 11px 0 13px;
                text-overflow: ellipsis;
            }

            #pac-input:focus {
                border-color: #4d90fe;
            }

            #title {
                color: #fff;
                background-color: #4d90fe;
                font-size: 25px;
                font-weight: 500;
                padding: 6px 12px;
            }
        </style>
    @endif
@endsection

@section('footer')
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable( {
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": "/admin/sharedoffice/get-data",
                },
                "columns": [
                    { data: 'id', name: 'id', sort:false },
                    { data: 'office_name', name: 'office_name' },
                    { data: 'image', name: 'image' },
                    { data: 'size_x', name: 'size_x' },
                    { data: 'size_y', name: 'size_y' },
                    { data: 'location', name: 'location' },
                    { data: 'number', name: 'number' },
                    { data: 'discount', name: 'discount' },
                    { data: 'qr', name: 'qr' },
                    { data: 'action', name: 'action' },
                ]
            });
        });

        function openEditModal(val) {
            console.log('val');
            console.log(val);
            if(val == 'name') {
                $('#editModalOfficeName').modal('show');
            }
            if(val == 'size') {
                $('#editModalOfficeSize').modal('show');
            }
            if(val == 'location') {
                $('#editModalOfficeLocation').modal('show');
            }
            if(val == 'number') {
                $('#editModalOfficePhoneNumber').modal('show');
            }
        }
        function openDeleteModal() {
            $('#deleteModalOffice').modal('show');
        }
        function updateOffice(val){
            if(val == 'name') {
                let url = '{{route('backend.sharedofficeUpdateData')}}';
                let name = $('#office_name').val();
                sendUpdateRequest(url,name,'','','','');
            }
            if(val == 'size') {
                console.log('this is size modal')
                let url = '{{route('backend.sharedofficeUpdateData')}}';
                let size_x = $('#size_x').val();
                let size_y = $('#size_y').val();
                sendUpdateRequest(url,'',size_x,size_y,'','');
            }
            if(val == 'number') {
                let url = '{{route('backend.sharedofficeUpdateData')}}';
                let number = $('#number').val();
                sendUpdateRequest(url,'','','','',number);
            }
            if(val == 'location') {
                let url = '{{route('backend.sharedofficeUpdateData')}}';
                let location = $('#pac-input').val();
                sendUpdateRequest(url,'','','',location,'');
            }
        }
        function sendUpdateRequest(url,name,size_x,size_y,location,number) {
            let form;
            form = {
                'office_id':'{{isset($sharedOffice) ? $sharedOffice->id : ''}}',
                'name':name,
                'location':location,
                'size_x':size_x,
                'size_y':size_y,
                'number':number,
            }
            console.log('form data check');
            console.log(form);
            let token = '{{csrf_token()}}';
            $.ajax({
               url:url,
                headers: {
                    'X-CSRF-TOKEN':token,
                },
                method:'POST',
                data:form,

                success: function (response) {
                    console.log('response');
                    console.log(response);
                    if(response.status == true) {
                        toastr.success(response.message);
                    }
                    setTimeout(() => {
                        window.location.reload();
                    },1000);
                },

                error: function (error) {
                    console.log('error');
                    console.log(error);
                }
            });
        }
    </script>
    <script>
        $(document).ready(function () {
            $('.js-example-basic-single').select2({
                maximumSelectionLength: 10
            });
        });

        let orgLat = parseFloat('{{isset($sharedOffice) ? $sharedOffice->lat : ''}}');
        let orgLong = parseFloat('{{isset($sharedOffice) ? $sharedOffice->lng : ''}}');
        // This example requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

        function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
                center: {
                    lat: orgLat,
                    lng: orgLong
                },
                zoom: 13
            });
            var card = document.getElementById('pac-card');
            var input = document.getElementById('pac-input');
            var types = document.getElementById('type-selector');
            var strictBounds = document.getElementById('strict-bounds-selector');

            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

            var autocomplete = new google.maps.places.Autocomplete(input);

            // Bind the map's bounds (viewport) property to the autocomplete object,
            // so that the autocomplete requests use the current map bounds for the
            // bounds option in the request.
            autocomplete.bindTo('bounds', map);

            // Set the data fields to return when the user selects a place.
            autocomplete.setFields(
                ['address_components', 'geometry', 'icon', 'name']);

            var infowindow = new google.maps.InfoWindow();
            var infowindowContent = document.getElementById('infowindow-content');
            infowindow.setContent(infowindowContent);
            var marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29),
                draggable: true
            });

            marker.setVisible(false);
            marker.setPosition({
                lat: orgLat,
                lng: orgLong
            });
            marker.setVisible(true);


            autocomplete.addListener('place_changed', function () {
                console.log('place changed');
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                console.log(place);
                var lat = place.geometry.location.lat();
                var lng = place.geometry.location.lng();
                console.log(lat);
                console.log(lng);
                setSelectedLatLng(lat, lng)
                if (!place.geometry) {
                    // User entered the name of a Place that was not suggested and
                    // pressed the Enter key, or the Place Details request failed.
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);  // Why 17? Because it looks good.
                }
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }

                infowindowContent.children['place-icon'].src = place.icon;
                infowindowContent.children['place-name'].textContent = place.name;
                infowindowContent.children['place-address'].textContent = address;
                infowindow.open(map, marker);
            });

            // Sets a listener on a radio button to change the filter type on Places
            // Autocomplete.
            // function setupClickListener(id, types) {
            //     var radioButton = document.getElementById(id);
            //     radioButton.addEventListener('click', function () {
            //         autocomplete.setTypes(types);
            //     });
            // }

            // setupClickListener('changetype-all', []);
            // setupClickListener('changetype-address', ['address']);
            // setupClickListener('changetype-establishment', ['establishment']);
            // setupClickListener('changetype-geocode', ['geocode']);

            // document.getElementById('use-strict-bounds')
            //     .addEventListener('click', function () {
            //         console.log('Checkbox clicked! New state=' + this.checked);
            //         autocomplete.setOptions({strictBounds: this.checked});
            //     });


            console.log('asdasd');
            google.maps.event.addListener(marker, 'dragend', function (marker) {
                console.log(marker);
                var latLng = marker.latLng;
                currentLatitude = latLng.lat();
                currentLongitude = latLng.lng();
                console.log(currentLatitude);
                console.log(currentLongitude);
                setSelectedLatLng(currentLatitude, currentLongitude)
            });
        }

        function setSelectedLatLng(lat, lng) {
            $('#lats').val(lat);
            $('#lngs').val(lng);
        }
    </script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIasatwSA8nyOeN8Cobzil6yO1jsT13rE&libraries=places&callback=initMap"
        defer>
    </script>
@endsection

@section('content')
    @if(!$is_mobile)
        <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> {{trans('sharedoffice.sharedoffice')}}</span>
            </div>
            @if(isset($generateSharedOffice) && $generateSharedOffice == true)
                @if(\Auth::guard('staff')->user()->staff_type == \App\Models\Staff::STAFF_TYPE_STAFF)
                    <div class="actions">
                        <a style="border-radius: 20px !important;" href="{{ route('backend.sharedoffice.managercreate',[\Auth::guard('staff')->user()->id]) }}" class="btn btn-circle red-sunglo btn-sm"><i
                                class="fa fa-plus"></i> {{trans('sharedoffice.add_sharedoffice')}}</a>
                    </div>
                @else
                    <div class="actions">
                        <a style="border-radius: 20px !important;" href="{{ route('backend.sharedoffice.create') }}" class="btn btn-circle red-sunglo btn-sm"><i
                                class="fa fa-plus"></i> {{trans('sharedoffice.add_sharedoffice')}}</a>
                    </div>
                @endif
            @endif
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <div class="table-responsive">
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Office Name</th>
                        <th width="20%">Image</th>
                        <th>Size X</th>
                        <th>Size Y</th>
                        <th>Location</th>
                        <th>Number</th>
                        <th>Discount</th>
                        <th width="20%">QR</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
                </div>
            </div>
        </div>
    </div>
    @else
        @if(isset($generateSharedOffice) && $generateSharedOffice == true)
            <div class="actions">
                <a style="border-radius: 20px !important;" href="{{ route('backend.sharedoffice.create') }}" class="btn btn-circle red-sunglo btn-sm"><i
                        class="fa fa-plus"></i> {{trans('sharedoffice.add_sharedoffice')}}</a>
            </div>
            <h1>No Office Please Create your Office</h1>
        @else
            @if(isset($sharedOffice))
        <div class="container customsharedOfficeContainer">
            <div class="row">
                <h3 class="customSttl">
                    <img class="leftArrowImage" onclick="window.location.href='/admin/sharedoffice/dashboard/{{$sharedOffice->id}}'" src="{{asset('images/leftArrow.png')}}" />
                    Shared Office Entries
                </h3>
            </div>
            <div class="row customRow1">
                <div class="col-md-6 customCol6Md1">
                    <p>I'D: </p>
                </div>
                <div class="col-md-6">
                    <p>{{$sharedOffice->id}}</p>
                </div>
            </div>
            <div class="row customRow1">
                <div class="col-md-6 customCol6Md1">
                    <p>Office Name: </p>
                </div>
                <div class="col-md-6">
                    <p>
                        <i onclick="openEditModal('name')" class="fa fa-pencil"></i>&nbsp;&nbsp;&nbsp;
                        {{$sharedOffice->office_name}}
                    </p>
                </div>
            </div>
            <div class="row customRow1">
                <div class="col-md-6 customCol6Md1">
                    <p>Office Image: </p>
                </div>
                <div class="col-md-6" style="padding: 10px">
                    <i onclick="window.location.href='{{route('backend.sharedofficeOwnerAddImages',[$sharedOffice->id])}}'" class="fa fa-pencil"></i>&nbsp;&nbsp;&nbsp;

                    <img src="{{asset('').$sharedOffice->image}}" onerror="this.src='/images/no-image-available-icon-6-600x375.png'" style="width: 180px;height: 125px;"  />
                </div>
            </div>
            <div class="row customRow1">
                <div class="col-md-6 customCol6Md1">
                    <p>Size X: </p>
                </div>
                <div class="col-md-6">
                    <p>
                        <i onclick="openEditModal('size')" class="fa fa-pencil"></i>&nbsp;&nbsp;&nbsp;
                        {{$sharedOffice->office_size_x}}
                    </p>
                </div>
            </div>
            <div class="row customRow1">
                <div class="col-md-6 customCol6Md1">
                    <p>Size Y: </p>
                </div>
                <div class="col-md-6">
                    <p>
                        <i onclick="openEditModal('size')" class="fa fa-pencil"></i>&nbsp;&nbsp;&nbsp;
                        {{$sharedOffice->office_size_y}}
                    </p>
                </div>
            </div>
            <div class="row customRow1">
                <div class="col-md-6 customCol6Md1">
                    <p>Location: </p>
                </div>
                <div class="col-md-6">
                    <p style="text-align: end;">
                        <i onclick="openEditModal('location')" class="fa fa-pencil"></i>&nbsp;&nbsp;&nbsp;
                        {{$sharedOffice->location}}
                    </p>
                </div>
            </div>
            <div class="row customRow1">
                <div class="col-md-6 customCol6Md1">
                    <p>Number: </p>
                </div>
                <div class="col-md-6">
                    <p>
                        <i onclick="openEditModal('number')" class="fa fa-pencil"></i>&nbsp;&nbsp;&nbsp;
                        {{$sharedOffice->number}}
                    </p>
                </div>
            </div>
            <div class="row customRow1">
                <div class="col-md-6 customCol6Md1">
                    <p style="height:45px;">Qr: </p>
                    <a href="/admin/sharedoffice/barcodes/{{$sharedOffice->id}}">Change</a>
                </div>
                <div class="col-md-6">
                    <img src="{{$sharedOffice->qr}}" style="width: 100px;height: 100px;padding:15px;" />
                </div>
            </div>
            <div class="row customRow1">
                <div class="col-md-6 customCol6Md1">
                    <p>Action : </p>
                    <a href="{{route('backend.sharedofficeOwnerAddImages',[$sharedOffice->id])}}">DashBoard Total Images</a>
                </div>
                <div class="col-md-6" style="text-align: center">
                    <p><i class="fa fa-trash" onclick="openDeleteModal()" onclick="window.location.href='{{route('backend.sharedoffice.delete',[$sharedOffice->id])}}'" aria-hidden="true"></i></p>

                    <p><i style="color: #53C329; font-size: 14px;" onclick="window.location.href='/admin/sharedoffice/dashboard/{{$sharedOffice->id}}'" class="fa fa-comments-o" aria-hidden="true"></i></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 customP">
                    <p class="setPS">Showing 1 of 1 entries</p>
                </div>
            </div>
            <div class="row customRow2">
                <div class="col-md-6 customP " style="flex: 1">
                    <button class="btn btn-default customPrevious">Previous</button>
                </div>
                <div class="col-md-6 customP1">
                    <button class="btn btn-default customNext"
                    onclick="window.location.href='/admin/edit/sharedoffice/{{$sharedOffice->id}}'"
                    >Next</button>
                </div>
            </div>
        </div>
                @endif
        @endif
    @endif
@endsection

@section('modal')
    <!-- Modal -->

    <div id="deleteModalOffice" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Delete Office</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete...!?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    @if(isset($sharedOffice))
                    <button type="button" class="btn btn-danger" onclick="window.location.href='/admin/sharedoffice/delete/{{$sharedOffice->id}}'">Delete</button>
                    @endif
                </div>
            </div>

        </div>
    </div>


    <div id="editModalOfficeSize" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Office Size</h4>
                </div>
                <form>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <label>Size X:</label>
                                <input type="text" id="size_x" name="size_x" value="{{isset($sharedOffice) ? $sharedOffice->office_size_x : ''}}" class="form-control" placeholder="enter office size x" />
                            </div>

                            <div class="col-md-12">
                                <label>Size Y:</label>
                                <input type="text" id="size_y" name="size_y" value="{{isset($sharedOffice)? $sharedOffice->office_size_y : ''}}" class="form-control" placeholder="enter office size y" />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="updateOffice('size')">Update</button>
                    </div>
                </form>

            </div>

        </div>
    </div>

    <div id="editModalOfficeName" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Office Name</h4>
                </div>
                <form>
                    <div class="modal-body">
                        <input type="text" id="office_name" name="location" value="{{isset($sharedOffice) ?$sharedOffice->office_name : ''}}" class="form-control" placeholder="enter office name" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="updateOffice('name')">Update</button>
                    </div>
                </form>

            </div>

        </div>
    </div>

    <div id="editModalOfficePhoneNumber" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Phone Number</h4>
                </div>
                <form>
                    <div class="modal-body">
                        <input type="number" id="number" name="phone_number" value="{{isset($sharedOffice) ? $sharedOffice->number : ''}}" class="form-control" placeholder="enter your phone number" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="updateOffice('number')">Update</button>
                    </div>
                </form>

            </div>

        </div>
    </div>

    <div id="editModalOfficeLocation" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Location</h4>
                </div>
                <form>
                    <div class="modal-body">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="lab-per">Address</label>
                                <input type="text" name="latitude" id="lats" value="{{isset($sharedOffice) ? $sharedOffice->lat : ''}}" style="display: none">
                                <input type="text" name="longitude" id="lngs" value="{{isset($sharedOffice) ? $sharedOffice->lng : ''}}" style="display: none">
                                <input type="text" id="pac-input" name="address" class="custom-form-control form-control"
                                       value="{{isset($sharedOffice) ? $sharedOffice->location : ''}}"/>
                            </div>
                        <div id="map" style="height: 300px;"></div>
                        <div id="infowindow-content">
                            <img src="" width="16" height="16" id="place-icon">
                            <span id="place-name" class="title"></span><br>
                            <span id="place-address"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="updateOffice('location')">Update</button>
                    </div>
                </form>

            </div>

        </div>
    </div>

@endsection
