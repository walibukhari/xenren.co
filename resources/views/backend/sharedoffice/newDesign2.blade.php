@extends('backend.layout')

@section('title')
    {{trans('sharedoffice.sharedoffice')}}
@endsection

@section('description')
    {{trans('sharedoffice.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>

        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.sharedoffice')}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <style>
        /*over write js tabs classes*/
        .nav-tabs a{
            font-family: Montserrat, Helvetica, sans-serif;
            border: 0px !important;
            font-size: 18px !important;
            color:#979797 !important;
            margin-right: 0px !important;
            font-weight: 600;
        }

        .active , .active a{
            margin-right: 0px;
            color:#25ce0f !important;
            border-bottom-left-radius: 0px;
            border-bottom-right-radius: 0px;
            border-bottom: 1px solid #25ce0f !important;
        }

        .conatiner-tabs{
            width: 100%;
        }

        .conatiner-tabs ul li a:hover{
            background-color: transparent !important;
        }

        .tab-pane-container{
            width: 100%;
            padding: 10px;
            border: 0.5px solid #ebecec;
            box-shadow: 0 2px 10px 1px #ebecec;
            border-radius: 4px;
            border-top: 0px;
            padding-right:0px;
        }

        #todaySales{
            border: 0px !important;
        }

        .datetimepicker {
            padding: 4px;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            direction: ltr;
            border: 1px solid #25ce0f;
            margin-top: 13px;
            margin-left: -10px;
        }

        .tab-pane-container .fa-calendar{
            padding-right:10px;
            color:#25ce0f;
            padding-left: 20px;
        }

        .tab-pane-container input{
            font-family: Montserrat, Helvetica, sans-serif;
            color:#979797 !important;
            font-weight: 100;
            width:52px;
            border:0px;
            font-size: 12px;
        }

        .tab-pane-container .time-table {
            padding-left:0px;
            padding-right:0px;
            width:84.5%;
        }

        .tab-pane-container .calender{
            padding-left: 30px;
            width: 13%;
            text-align: center;
            padding: 10px;
        }
        .time-table ul li {
            float: left;
            width: 3.88%;
            cursor: pointer;
            margin-bottom: 2px;
            font-size: 9px;
            font-weight: bold;
            list-style: none;
            background-color: #f5f6f7;
            color: #585858;
            border: 1px solid #e3e4e6;
            padding:0px 6px 0px 6px;
            margin-right: 2px;
            min-height: 30px;
            max-height: 30px;
            display: grid;
            text-align: center;
            align-items: center;
        }
        .time-table ul {
            margin: 0px;
            padding-left: 0px;
            width: 100%;
        }

        .available-container{
            width:100%;
        }

        .available-container .uordered-list{
            float: right;
        }

        .tab-pane-container .row{
            display:flex;
            align-items: center;
        }

        .uordered-list{
            padding-left: 0px;
        }


        .uordered-list li{
            list-style: none;
            font-family: Montserrat, Helvetica, sans-serif !important;
            font-size: 12px !important;
            color:#979797;
            float: left;
            padding-right: 15px;
        }


        .label-container{
            background-color: #25ce0f;
            height: 13px;
            width: 13px;
            margin-bottom: 0px;
            margin-right: 6px;
        }

        .label-container-2{
            background-color: #fcb817;
            height: 13px;
            width: 13px;
            margin-bottom: 0px;
            margin-right: 6px;
        }

        .label-container-3{
            background-color: #e3e4e6;
            height: 13px;
            width: 13px;
            margin-bottom: 0px;
            margin-right: 6px;
        }



        /*inner container*/
        .tab-pane-inner-container{
            width: 100%;
            padding-right:0px !important;
            padding: 10px;
            border: 0.5px solid #e6e6e6;
            border-radius: 0px;
        }

        .rooms{
            width: 13%;
        }

        .box-container{
            padding-left: 30px;
            background-color: #f5f6f7;
            width: 100%;
            font-size: 13px;
            font-family: Montserrat, Helvetica, sans-serif !important;
            padding: 20px;
            text-align: center;
            color: #585858;
            font-weight: 600;
            word-wrap: break-word;
        }

        .cells{
            padding-left:0px;
            padding-right: 0px;
            width:84.5%;
        }

        .cells ul{
            padding-left: 0px;
            margin:0px;
        }

        .cells ul li {
            float: left;
            list-style: none;
            background-color: #25ce0f;
            margin-right: 2px;
            min-height: 30px;
            max-height: 30px;
            width:3.88%;
            display: grid;
            text-align: center;
            align-items: center;
            padding: 0px 16px 0px 15px;
            margin-bottom: 3px;
        }

        .tab-pane-inner-container .row{
            display: flex;
            align-items: center;
        }
        /*inner container*/

        @media (max-width:2000px) and (min-width:1528px)
        {
            .cells ul li {
                float: left;
                list-style: none;
                background-color: #25ce0f;
                margin-right: 2px;
                min-height: 30px;
                max-height: 30px;
                width:3.97%;
                display: grid;
                text-align: center;
                align-items: center;
                padding: 0px 16px 0px 15px;
                margin-bottom: 3px;
            }
            .time-table ul li {
                float: left;
                width: 3.97%;
                cursor: pointer;
                margin-bottom: 2px;
                font-size: 9px;
                font-weight: bold;
                list-style: none;
                background-color: #f5f6f7;
                color: #585858;
                border: 1px solid #e3e4e6;
                padding: 0px 6px 0px 6px;
                margin-right: 2px;
                min-height: 30px;
                max-height: 30px;
                display: grid;
                text-align: center;
                align-items: center;
            }
            .custom-alignment {
                padding-right: 0px;
                text-align: right;
                padding-left: 7px;
            }
        }

        @media (max-width:1527px) and (min-width:1400px)
        {
            .cells ul li {
                float: left;
                list-style: none;
                background-color: #25ce0f;
                margin-right: 2px;
                width: 3.94%;
                min-height: 30px;
                max-height: 30px;
                display: grid;
                text-align: center;
                align-items: center;
                padding: 0px 16px 0px 15px;
                margin-bottom: 3px;
            }
            .time-table ul li {
                float: left;
                width: 3.94%;
                cursor: pointer;
                margin-bottom: 2px;
                font-size: 9px;
                font-weight: bold;
                list-style: none;
                background-color: #f5f6f7;
                color: #585858;
                border: 1px solid #e3e4e6;
                padding: 0px 6px 0px 6px;
                margin-right: 2px;
                min-height: 30px;
                max-height: 30px;
                display: grid;
                text-align: center;
                align-items: center;
            }
        }
        @media (max-width:991px){
            .set-scroll {
                overflow: scroll;
                overflow-y: auto;
                overflow-x: auto;
            }
            #scroll-div {
                width: max-content;
            }
            .availability{
                width:100%;
            }
            .tab-pane-inner-container .row {
                display: block;
                align-items: center;
            }
            .cells {
                padding-left: 15px;
                padding-right: 15px;
                width: 100%;
            }

            .tab-pane-container .time-table {
                padding-left: 0px;
                padding-right: 15px;
                width: 84.5%;
            }

            .box-container {
                padding-left: 30px;
                background-color: #f5f6f7;
                width: 90%;
                font-size: 13px;
                font-family: Montserrat, Helvetica, sans-serif !important;
                padding: 20px;
                text-align: center;
                color: #585858;
                font-weight: 600;
                word-wrap: break-word;
                margin-bottom: 10px;
            }
            .time-table ul li{
                width:29px;
            }

            .rooms {
                width: 100%;
            }
            .custom-alignment {
                padding-right: 13px;
                text-align: -webkit-center;
                padding-left: 7px;
            }

            .sm-0{
                width:100%;
            }
        }

        @media (max-width:447px)
        {
            .tab-pane-container .row {
                display: block;
                align-items: center;
            }
            .tab-pane-container .calender {
                text-align: center;
                padding-left: 30px;
                width: 100%;
            }
            .cells {
                padding-left: 15px;
                padding-right: 5px;
                width:100%;
            }
            .tab-pane-container .time-table {
                padding-left: 11px;
                padding-right: 0px;
                width:94%;
            }
            .sm-0 {
                width: 100%;
                text-align: center;
                padding-left: 50px;
                display: inline-block;
                margin-bottom: 10px;
            }
        }
        /*jquery table settings*/
        .table-condensed thead tr{
            display: -webkit-box;
        }
        .table-condensed tbody tr{
            display: inherit;
        }
        /*jquery table settings*/
        .datetimepicker table tr td.active.active, .datetimepicker table tr td.active.disabled, .datetimepicker table tr td.active.disabled.active, .datetimepicker table tr td.active.disabled.disabled, .datetimepicker table tr td.active.disabled:active, .datetimepicker table tr td.active.disabled:hover, .datetimepicker table tr td.active.disabled:hover.active, .datetimepicker table tr td.active.disabled:hover.disabled, .datetimepicker table tr td.active.disabled:hover:active, .datetimepicker table tr td.active.disabled:hover:hover, .datetimepicker table tr td.active.disabled:hover[disabled], .datetimepicker table tr td.active.disabled[disabled], .datetimepicker table tr td.active:active, .datetimepicker table tr td.active:hover, .datetimepicker table tr td.active:hover.active, .datetimepicker table tr td.active:hover.disabled, .datetimepicker table tr td.active:hover:active, .datetimepicker table tr td.active:hover:hover, .datetimepicker table tr td.active:hover[disabled], .datetimepicker table tr td.active[disabled]{
            background-color: #25ce0f !important;
            border-radius: 20px;
            border-bottom: 0px !important;
            font-weight: 600 !important;
            color: #fff !important;
            font-size:12px !important;
        }

        .datepicker table td, .datepicker table th, .datetimepicker table td, .datetimepicker table th {
            font-family: 'Open Sans'!important;
            padding: 10px !important;
            border-radius: 20px;
        }

        .datetimepicker tfoot tr:first-child th, .datetimepicker thead tr:first-child th {
            cursor: pointer;
            width: 89px;
        }
    </style>
@endsection

@section('footer')
    <script src="{{asset('custom/js/bs-modal-bootstrap.js')}}"></script>

    <script type="text/javascript">
        $(function () {
            $(".form_datetime2").datetimepicker({
                format: "M dd",
                // altFormat: 'dd-mm-yy',
                altField: "#hiddenField",
                autoClose: true,
                pickTime: false,
                minView: 2
            }).on('changeDate', function(e){
                $('.form_datetime2').datetimepicker('hide');
            });

        });
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        {{--<div class="portlet-title">--}}
        {{--<div class="caption font-green-sharp">--}}
        {{--<span class="caption-subject bold uppercase"> {{trans('sharedoffice.sharedoffice')}}</span>--}}
        {{--</div>--}}
        {{--@if(isset($generateSharedOffice) && $generateSharedOffice == true)--}}
        {{--<div class="actions">--}}
        {{--<a href="{{ route('backend.sharedoffice.create') }}" class="btn btn-circle red-sunglo btn-sm"><i--}}
        {{--class="fa fa-plus"></i> {{trans('sharedoffice.add_sharedoffice')}}</a>--}}
        {{--</div>--}}
        {{--@endif--}}
        {{--</div>--}}
        <div class="portlet-body">
            <div class="container available-container">
                <ul class="uordered-list">
                    <li>
                        <label class="label-container">
                        </label>Available
                    </li>
                    <li>
                        <label class="label-container-2">
                        </label>Booked
                    </li>
                    <li>
                        <label class="label-container-3">
                        </label>In use
                    </li>
                </ul>
            </div>
            <div class="container conatiner-tabs">
                <div id="todaySales" class="tab-pane">
                        <div class="container tab-pane-container">
                            <div class="row">
                                <div class="col-md-2 calender">
                                    <i class="fa fa-calendar form_datetime2" aria-hidden="true"></i><input class="form_datetime2" placeholder="Jul 12" readonly>
                                </div>
                                <div class="col-md-10 time-table">
                                    <div class="set-scroll">
                                        <ul id="scroll-div">
                                            <li>1<br>AM</li>
                                            <li>2<br>AM</li>
                                            <li>3<br>AM</li>
                                            <li>4<br>AM</li>
                                            <li>5<br>AM</li>
                                            <li>6<br>AM</li>
                                            <li>7<br>AM</li>
                                            <li>8<br>AM</li>
                                            <li>9<br>AM</li>
                                            <li>10<br>AM</li>
                                            <li>11<br>AM</li>
                                            <li>12</li>
                                            <li>1<br>PM</li>
                                            <li>2<br>PM</li>
                                            <li>3<br>PM</li>
                                            <li>4<br>PM</li>
                                            <li>5<br>PM</li>
                                            <li>6<br>PM</li>
                                            <li>7<br>PM</li>
                                            <li>8<br>PM</li>
                                            <li>9<br>PM</li>
                                            <li>10<br>PM</li>
                                            <li>11<br>PM</li>
                                            <li>12</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="container tab-pane-inner-container">
                            <div class="row">
                                <div class="col-md-2 rooms">
                                    <div class="container box-container">
                                        Meeting Room 31
                                    </div>
                                </div>
                                <div class="col-md-10 cells">
                                    <ul>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#fcb817;"></li>
                                        <li style="background-color:#fcb817;"></li>
                                        <li style="background-color:#fcb817;"></li>
                                        <li style="background-color:#fcb817;"></li>
                                        <li style="background-color:#fcb817;"></li>
                                        <li style="background-color:#fcb817;"></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                    </ul>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-2 rooms">
                                    <div class="container box-container">
                                        Hot desk 15
                                    </div>
                                </div>
                                <div class="col-md-10 cells">
                                    <ul>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#fcb817;"></li>
                                        <li style="background-color:#fcb817;"></li>
                                        <li style="background-color:#fcb817;"></li>
                                        <li style="background-color:#fcb817;"></li>
                                        <li style="background-color:#fcb817;"></li>
                                        <li style="background-color:#fcb817;"></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                    </ul>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-2 rooms">
                                    <div class="container box-container">
                                        Meeting Room 11
                                    </div>
                                </div>
                                <div class="col-md-10 cells">
                                    <ul>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li style="background-color:#fcb817;"></li>
                                        <li style="background-color:#fcb817;"></li>
                                        <li style="background-color:#fcb817;"></li>
                                        <li style="background-color:#fcb817;"></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                    </ul>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-2 rooms">
                                    <div class="container box-container">
                                        Hot desk 15
                                    </div>
                                </div>
                                <div class="col-md-10 cells">
                                    <ul>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                    </ul>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-2 rooms">
                                    <div class="container box-container">
                                        Hot desk 24
                                    </div>
                                </div>
                                <div class="col-md-10 cells">
                                    <ul>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                    </ul>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-2 rooms">
                                    <div class="container box-container">
                                        Hot desk 15
                                    </div>
                                </div>
                                <div class="col-md-10 cells">
                                    <ul>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                    </ul>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-2 rooms">
                                    <div class="container box-container">
                                        Meeting Room 17
                                    </div>
                                </div>
                                <div class="col-md-10 cells">
                                    <ul>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li style="background-color:#e3e4e6;"></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
@endsection
