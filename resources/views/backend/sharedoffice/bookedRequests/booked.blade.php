@extends('backend.layout')

@section('title')
    {{trans('sharedoffice.sharedoffice')}}&nbsp;<small>booking request</small>
@endsection

@section('description')

@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sharedoffice.dashboard')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedoffice') }}">{{trans('sharedoffice.sharedoffice')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.sharedofficebookingrequest') }}">{{trans('sharedoffice.shared_office_request')}}</a>
        </li>
    </ul>
@endsection

@section('header')
    <link href="/css/sharedOfficeRequest.css" rel="stylesheet" type="text/css">
    <style>
        .dangerBtn{
            background-color: red;
            border: 0px;
        }
        .dangerBtn:hover{
            background-color: red;
            border: 0px;
        }
        .dangerBtn:focus{
            background-color: red;
            border: 0px;
        }
    </style>
@endsection

@section('footer')
    <script>
        function cancelConfirm(id) {
            $('#uniqueID').val(id);
            $('#cancelConfirm').modal('show');
        }
    </script>
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase">{{trans('sharedoffice.sharedoffice')}}</span>
            </div>
        </div>
        <div class="col-md-12">
            @if(session()->has('success_message'))
                <div class="alert alert-success">
                    {{session()->get('success_message')}}
                </div>
            @endif
        </div>
        <div class="portlet-body">
            <div class="table-container table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr role="row" class="heading">
                        <th>{{trans('sharedoffice.id')}}</th>
                        <th>{{trans('common.full_name')}}</th>
                        <th>{{trans('common.phone_no')}}</th>
                        <th>{{trans('common.check_in_date')}}</th>
                        <th>{{trans('common.check_out_date')}}</th>
                        <th>{{trans('common.no_of_rooms')}}</th>
                        <th>{{trans('common.no_of_peoples')}}</th>
                        <th>{{trans('common.remarks')}}</th>
                        <th>{{trans('common.status')}}</th>
                        <th>{{trans('common.action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($bookedRequests as $requests)
                        <tr>
                            <td>{{$requests->id}}</td>
                            <td>{{$requests->full_name}}</td>
                            <td>{{$requests->phone_no}}</td>
                            <td>{{$requests->check_in}}</td>
                            <td>{{$requests->check_out}}</td>
                            <td>{{$requests->no_of_rooms}}</td>
                            <td>{{$requests->no_of_persons}}</td>
                            <td>{{$requests->remarks}}</td>
                            <td>{{isset($requests->status) && $requests->status == \App\Models\SharedOfficeBooking::STATUS_BOOKED ? 'Booked' : ''}}</td>
                            <td>
                                <a href="#" onclick="cancelConfirm({{$requests->id}})" class="btn btn-danger dangerBtn">
                                    cancel Request
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="cancelConfirm" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <form action="{{route('backend.sharedoffice.request.cancel')}}">
                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                <input type="hidden" name="_id" id="uniqueID" value="" />
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p>Are you want to confirm booking request</p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success" >confirm</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">cancel</button>
                </div>
            </div>
            </form>
        </div>
    </div>
@endsection
