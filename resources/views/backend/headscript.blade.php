<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
      type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Montserrat:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">
<link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
{{--<link href="/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet"--}}
{{--      type="text/css"/>--}}
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
{{--<link href="/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet"--}}
{{--      type="text/css"/>--}}
{{--<link href="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />--}}
{{--<link href="/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />--}}
{{--<link href="/assets/global/plugins/jquery-multi-select/css/multi-select.css" rel="stylesheet" type="text/css" />--}}
{{--<link href="/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />--}}
{{--<link href="/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />--}}
{{--<link href="/assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />--}}
{{--<link href="/assets/global/plugins/jcrop/css/jquery.Jcrop.min.css" rel="stylesheet" type="text/css" />--}}

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
{{--<link href="/assets/global/css/components-rounded.min.css" rel="stylesheet" id="style_components"--}}
{{--      type="text/css"/>--}}
{{--<link href="/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css"/>--}}
{{--<link href="/custom/plugins/anytimepicker/anytimepicker.min.css" rel="stylesheet" type="text/css"/>--}}
{{--<link href="/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />--}}
{{--<link href="/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />--}}
{{--<link href="/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />--}}
{{--<link href="/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />--}}
{{--<link href="/custom/plugins/anytimepicker/flick.css" rel="stylesheet" type="text/css"/>--}}
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
{{--<link href="/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css"/>--}}
{{--<link href="/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css"--}}
{{--      id="style_color"/>--}}
{{--<link href="/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css"/>--}}
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="/favicon.ico"/>
{{--<link href="/css/global.css" rel="stylesheet" type="text/css"/>--}}
{{--<link href="/css/backend.css" rel="stylesheet" type="text/css"/>--}}
<link href="{{asset('css/MainBackend.css')}}" rel="stylesheet" type="text/css"/>
