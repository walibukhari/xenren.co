<div class="modal fade modal-scroll" id="remote-modal" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>
<div class="modal fade bs-modal-lg modal-scroll" id="remote-modal-large" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

        </div>
    </div>
</div>
<div class="modal fade modal-scroll" id="remote-modal-full" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-full">
        <div class="modal-content">

        </div>
    </div>
</div>
<div class="modal fade modal-scroll" id="award-job-modal-full" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>
            <div class="modal-body">
                <form action="Javascript:;" id="award-job-form" method="POST">
                    <div class="form-group">
                        <label for="email">Number Of Shares Per Hour:</label>
                        <input type="text" class="form-control" id="num_of_shares" name="num_of_shares">
                    </div>
                    <button type="button" id="btn-submit" class="btn btn-success">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="confirm-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer confirm-modal-detele">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('common.cancel')}}</button>
                <a class="btn btn-default btn-ok">{{trans('common.submit')}}</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="message-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('user.取消')}}</button>
                <a class="btn btn-danger btn-ok">{{trans('user.确定')}}</a>
            </div>
        </div>
    </div>
</div>
