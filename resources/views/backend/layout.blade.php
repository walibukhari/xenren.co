<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>@yield('title')</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="Cache-control" content="public">
    <meta content="@yield('description')" name="description"/>
    <meta content="@yield('author')" name="author"/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <link href="/images/Favicon.jpg" rel="icon" type="image/x-icon">
    @include('backend.headscript')
    @section('header') @show
    @stack('css')
{{--    @if(Auth::guard('staff')->user()->staff_type == \App\Models\Staff::STAFF_TYPE_STAFF)--}}
        <link href="{{asset('custom/css/backend/staffHeader.css')}}" rel="stylesheet" type="text/css">
        <style>
            #nav-icon1 {
                width: 30px;
                height: 28px;
                position: relative;
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
                -webkit-transition: .5s ease-in-out;
                -moz-transition: .5s ease-in-out;
                -o-transition: .5s ease-in-out;
                transition: .5s ease-in-out;
                cursor: pointer;
                top:10px;
            }

            #nav-icon1 span{
                display: block;
                position: absolute;
                height: 4px;
                width: 100%;
                background: #fff;
                border-radius: 9px;
                opacity: 1;
                left: 0;
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
                -webkit-transition: .25s ease-in-out;
                -moz-transition: .25s ease-in-out;
                -o-transition: .25s ease-in-out;
                transition: .25s ease-in-out;
            }

            #nav-icon1 span:nth-child(1) {
                top: 0px;
            }

            #nav-icon1 span:nth-child(2) {
                top: 10px;
            }

            #nav-icon1 span:nth-child(3) {
                top: 20px;
            }

            #nav-icon1.open span:nth-child(1) {
                top: 10px;
                -webkit-transform: rotate(135deg);
                -moz-transform: rotate(135deg);
                -o-transform: rotate(135deg);
                transform: rotate(135deg);
            }

            #nav-icon1.open span:nth-child(2) {
                opacity: 0;
                left: -60px;
            }

            #nav-icon1.open span:nth-child(3) {
                top: 10px;
                -webkit-transform: rotate(-135deg);
                -moz-transform: rotate(-135deg);
                -o-transform: rotate(-135deg);
                transform: rotate(-135deg);
            }
            .open-box li {
                list-style: none;
                cursor: pointer;
                padding-top: 12px;
                padding-bottom: 12px;
                border-bottom: 1px solid #efefef;
            }
            .open-box li:hover {
                list-style: none;
                background: #efefef;
                cursor: pointer;
                padding-top: 12px;
                padding-bottom: 12px;
                border-left: 5px solid #25ce0f;
                border-bottom: 1px solid #efefef;
            }
            .open-box li a{
                text-decoration: none;
                color: #25CE0F !important;
                padding-left: 10px;
            }
            .open-box li a:hover{
                padding-left: 10px;
            }
            .open-box{
                padding-left: 0px;
                height: auto;
                background: #fff;
                border: 1px solid #efefef;
            }
        </style>
{{--    @endif--}}
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="{{ route('backend.index') }}">
                <img src="{{url('xenren-assets/images/logo/logo_2.png')}}" alt="logo" class="logo-default" height="30px"/>
            </a>
            <div class="menu-toggler sidebar-toggler"></div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
{{--        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"--}}
{{--           data-target=".navbar-collapse">--}}
{{--            <img src="{{asset('images/fabars.png')}}" />--}}
{{--        </a>--}}
        <div class="custombarssetiing responsive-toggler" id="nav-icon1" style="display:none;" data-toggle="collapse" data-target=".navbar-collapse">
            <span></span>
            <span></span>
            <span></span>
{{--            <ul>--}}
{{--               --}}
{{--                <li class="bar3"></li>--}}
{{--            </ul>--}}
        </div>
        <a style="display: none;" class="showMobileWeb" href="{{url('/adminlang/'.((\Session::get('lang') == 'en')?"cn" : "en"))}}"> {{(\Session::get('lang') == 'en')?"EN" : "CN"}}</a>
        <img class="caretDownIcon" style="display: none" onclick="displayPopup()" src="{{asset('images/caretDown.png')}}">
        <div class="userControlePopUp" style="display: none">
            <ul>
                <li>
                    <i class="fa fa-user" aria-hidden="true"></i>
                    <a href="{{route('backend.sharedofficeProfile')}}">User</a>
                </li>
                <li>
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <a href="{{route('backend.sharedofficeNotification')}}">Inbox</a>
                </li>
                @if(Auth::guard('staff')->user()->staff_type == \App\Models\Staff::STAFF_TYPE_ADMIN)
                    <li>
                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                        <a href="{{ route('staff.logout') }}">Logout</a>
                    </li>
                @else
                    <li>
                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                        <a href="{{route('staff.mobile.logout')}}">Logout</a>
                    </li>
                @endif
            </ul>
        </div>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li>
                        <a href="{{url('/adminlang/'.((\Session::get('lang') == 'en')?"cn" : "en"))}}"> {{(\Session::get('lang') == 'en')?"EN" : "CN"}}</a>
                    </li>
                @if(!isset($token))
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                       data-close-others="true">
                        <span class="username username-hide-on-mobile"> {{ Auth::guard('staff')->user()->username }} </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="{{ route('staff.profile') }}" data-toggle="modal" data-target="#remote-modal">
                                <i class="icon-user"></i> {{trans('user.personal_info')}} </a>
                        </li>
                        <li>
                            <a href="{{ route('backend.inbox') }}">
                                <i class="fa fa-paper-plane"></i> {{trans('sideMenu.inbox')}}
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ route('staff.logout') }}">
                                <i class="icon-key"></i> {{trans('user.sign_out')}} </a>
                        </li>
                    </ul>
                </li>
                @endif
                <!-- END USER LOGIN DROPDOWN -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"></div>
<!-- END HEADER & CONTENT DIVIDER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    @include('backend.sidebar')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                @section('breadcrumb')
                @show
            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h3 class="page-title">
                <small class="titleTT">@yield('title')</small>
                <small class="descTT">@yield('description')</small>
            </h3>
            @include('backend.flash')
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            @yield('content')
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner"> {{ Date('Y') }} &copy; by xenren.</div>
{{--    <div class="mobileFooter" style="display: none;">--}}
{{--        <ul class="customUl">--}}
{{--            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.index']) ? ' activeCustom' : '' }}">--}}
{{--                @if(in_array(Route::currentRouteName(), ['backend.index']))--}}
{{--                    <img style="width: 15px;height: 15px;" src="/images/homeIcon.png" />--}}
{{--                @else--}}
{{--                    <img style="width: 15px;height: 15px;" src="/images/homeIcongray.png" />--}}
{{--                @endif--}}
{{--                <a href="{{ route('backend.index') }}" class="nav-link nav-toggle customfontSize">--}}
{{--                    Home--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.working_sheet']) ? ' activeCustom' : '' }}">--}}
{{--                <i class="fa fa-file-text-o customFontSizeIcon" aria-hidden="true"></i>--}}
{{--                <a href="{{ route('backend.working_sheet') }}" class="nav-link nav-toggle customfontSize">--}}
{{--                    Report--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.sharedofficeNotification']) ? ' activeCustom' : '' }}">--}}
{{--                <i class="fa fa-bell customFontSizeIcon" aria-hidden="true"></i>--}}
{{--                <a href="{{ route('backend.sharedofficeNotification') }}" class="nav-link nav-toggle customfontSize">--}}
{{--                    Notification--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class="nav-item{{ in_array(Route::currentRouteName(), ['backend.sharedofficeProfile']) ? ' activeCustom' : '' }}">--}}
{{--                <i class="fa fa-user customFontSizeIcon" aria-hidden="true"></i>--}}
{{--                <a href="{{ route('backend.sharedofficeProfile') }}" class="nav-link nav-toggle customfontSize">--}}
{{--                    Profile--}}
{{--                </a>--}}
{{--            </li>--}}
{{--        </ul>--}}
{{--    </div>--}}
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
    @include('backend.footscript')
    @section('footer')@show
    @include('backend.modal')
    @section('modal')@show
    {!! Form::open(['id' => 'p-key-form']) !!}
    {!! Form::close() !!}
@stack('js')

<script>
    $(document).ready(function(){
        $('#nav-icon1').click(function(){
            $(this).toggleClass('open');
        });
    });
</script>
<script>
    function displayPopup(){
        $('.userControlePopUp').toggle();
    }
	$(function () {
		$('#award-job-modal-full').on('shown.bs.modal', function (e) {
            $('#award-job-form').attr('action', $(e.relatedTarget).attr('data-href'));
            $('#award-job-form').makeAjaxForm({
                submitBtn: '#btn-submit',
                alertContainer: '#portfolio-alert-container',
	            clearForm: true,
            });
        })
	});
    $('#sharedOffice').click(function () {
        $('.drop-down-menu-finance').slideUp();
        $('.drop-down-menu-management').slideUp();
        $('.drop-down-menu-community').slideUp();
        $('.drop-down-menu-coins').slideUp();
        $('.drop-down-menu-milestone').slideUp();
        $('.drop-down-menu-users').slideUp();
        $('.drop-down-menu-operator').slideUp();
        $('.drop-down-menu-shared-office').slideToggle();
        $('.drop-down-menu-shared-office').addClass('open-box')
    });
    $('#management').click(function () {
        $('.drop-down-menu-finance').slideUp();
        $('.drop-down-menu-shared-office').slideUp();
        $('.drop-down-menu-coins').slideUp();
        $('.drop-down-menu-milestone').slideUp();
        $('.drop-down-menu-users').slideUp();
        $('.drop-down-menu-operator').slideUp();
        $('.drop-down-menu-community').slideUp();
        $('.drop-down-menu-management').slideToggle();
        $('.drop-down-menu-management').addClass('open-box')
    });

    $('#coins').click(function () {
        $('.drop-down-menu-finance').slideUp();
        $('.drop-down-menu-shared-office').slideUp();
        $('.drop-down-menu-management').slideUp();
        $('.drop-down-menu-milestone').slideUp();
        $('.drop-down-menu-users').slideUp();
        $('.drop-down-menu-operator').slideUp();
        $('.drop-down-menu-community').slideUp();
        $('.drop-down-menu-coins').slideToggle();
        $('.drop-down-menu-coins').addClass('open-box')
    });
    $('#finance').click(function () {
        $('.drop-down-menu-shared-office').slideUp();
        $('.drop-down-menu-coins').slideUp();
        $('.drop-down-menu-management').slideUp();
        $('.drop-down-menu-milestone').slideUp();
        $('.drop-down-menu-users').slideUp();
        $('.drop-down-menu-operator').slideUp();
        $('.drop-down-menu-community').slideUp();
        $('.drop-down-menu-finance').slideToggle();
        $('.drop-down-menu-finance').addClass('open-box')
    });
    $('#community').click(function () {
        $('.drop-down-menu-finance').slideUp();
        $('.drop-down-menu-shared-office').slideUp();
        $('.drop-down-menu-coins').slideUp();
        $('.drop-down-menu-management').slideUp();
        $('.drop-down-menu-milestone').slideUp();
        $('.drop-down-menu-users').slideUp();
        $('.drop-down-menu-operator').slideUp();
        $('.drop-down-menu-community').slideToggle();
        $('.drop-down-menu-community').addClass('open-box')
    });
    $('#usersU').click(function () {
        $('.drop-down-menu-shared-office').slideUp();
        $('.drop-down-menu-coins').slideUp();
        $('.drop-down-menu-management').slideUp();
        $('.drop-down-menu-milestone').slideUp();
        $('.drop-down-menu-operator').slideUp();
        $('.drop-down-menu-community').slideUp();
        $('.drop-down-menu-finance').slideUp();
        $('.drop-down-menu-users').slideToggle();
        $('.drop-down-menu-users').addClass('open-box')
    });
    $('#milestone').click(function () {
        $('.drop-down-menu-shared-office').slideUp();
        $('.drop-down-menu-coins').slideUp();
        $('.drop-down-menu-management').slideUp();
        $('.drop-down-menu-users').slideUp();
        $('.drop-down-menu-operator').slideUp();
        $('.drop-down-menu-community').slideUp();
        $('.drop-down-menu-finance').slideUp();
        $('.drop-down-menu-milestone').slideToggle();
        $('.drop-down-menu-milestone').addClass('open-box')
    });

    $('#operator').click(function () {
        $('.drop-down-menu-shared-office').slideUp();
        $('.drop-down-menu-coins').slideUp();
        $('.drop-down-menu-milestone').slideUp();
        $('.drop-down-menu-management').slideUp();
        $('.drop-down-menu-users').slideUp();
        $('.drop-down-menu-community').slideUp();
        $('.drop-down-menu-finance').slideUp();
        $('.drop-down-menu-operator').slideToggle();
        $('.drop-down-menu-operator').addClass('open-box')
    });
</script>
</body>

</html>
