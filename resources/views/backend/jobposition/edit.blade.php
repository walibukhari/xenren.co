@extends('ajaxmodal')

@section('title')
    {{trans("jobposition.修改主职")}}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                $('#job-position-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit-skill'
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit-skill" class="btn btn-primary blue">{{trans("jobposition.修改")}}</button>
@endsection

@section('content')
    {!! Form::open(['route' => ['backend.jobposition.edit.post', 'id' => $model->id], 'method' => 'post', 'role' => 'form', 'id' => 'job-position-form', 'files' => true]) !!}
        <div class="form-body">
            <div class="form-group">
                <label>{{trans("jobposition.主职名称-中文")}}</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-building"></i>
                    </span>
                    {!! Form::text('name_cn', $model->name_cn, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>{{trans("jobposition.主职名称-英文")}}</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-building"></i>
                    </span>
                    {!! Form::text('name_en', $model->name_en, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>{{trans("jobposition.主职说明-中文")}}</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-building"></i>
                    </span>
                    {!! Form::text('remark_cn', $model->remark_cn, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>{{trans("jobposition.主职说明-英文")}}</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-building"></i>
                    </span>
                    {!! Form::text('remark_en', $model->remark_en, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection