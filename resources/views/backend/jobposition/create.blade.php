@extends('ajaxmodal')

@section('title')
    {{trans("jobposition.新增主职")}}
@endsection

@section('script')
    <script>
        +function() {
            $(document).ready(function() {
                $('#job-position-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit-job-position'
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btn-submit-job-position" class="btn btn-primary blue">{{trans("jobposition.新增")}}</button>
@endsection

@section('content')
    {!! Form::open(['route' => ['backend.jobposition.create.post'], 'method' => 'post', 'role' => 'form', 'id' => 'job-position-form', 'files' => true]) !!}
        <div class="form-body">
            <div class="form-group">
                <label>{{trans('jobposition.主职名称-中文')}}</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-building"></i>
                    </span>
                    {!! Form::text('name_cn', '', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>{{trans('jobposition.主职名称-英文')}}</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-building"></i>
                    </span>
                    {!! Form::text('name_en', '', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>{{trans("jobposition.主职名称-中文")}}</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-building"></i>
                    </span>
                    {!! Form::text('remark_cn', '', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <label>{{trans("jobposition.主职名称-英文")}}</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-building"></i>
                    </span>
                    {!! Form::text('remark_en', '', ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection

@section('modal')

@endsection