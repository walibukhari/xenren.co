@extends('backend.layout')

@section('title')
    Pending Withdrawals
@endsection

@section('description')
    {{trans('user.crud')}}
@endsection

@section('breadcrumb')
    <ul class="page-breadcrumb">
        <li>
            <a href="javascript:;">{{trans('sideMenu.后台')}}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('backend.user') }}">Pending Withdrawals</a>
        </li>
    </ul>
@endsection

@section('header')
    Pending Withdrawals
@endsection

@section('footer')

@endsection

@section('content')
    @if(\Session::has('error'))
        <div class="alert alert-danger">{{\Session::get('error')}}</div>
    @endif
    @if(\Session::has('success'))
        <div class="alert alert-success">{{\Session::get('success')}}</div>
    @endif
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> Pending Withdrawals</span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container">
                <table class="table table-striped table-bordered table-hover table-checkable" id="user-dt">
                    <thead>
                    <tr role="row" class="heading">
                        <th width="20%">{{trans('common.email')}}</th>
                        <th>{{trans('member.real_name')}}</th>
                        <th>{{trans('common.amount')}}</th>
                        <th>{{trans('common.status')}}</th>
                        <th>{{trans('common.actions')}}</th>
                    </tr>
                    @if(count($data) < 1)
                        <tr>
                            <td colspan="5"> No pending transactions</td>
                        </tr>
                    @endif
                    @foreach($data as $k => $v)
                        @if(isset($v->sender->email))
                        <tr role="row">
                            <td>
                                {{$v->sender->email}}
                            </td>
                            <td>
                                {{isset($v->sender->name) && $v->sender->name != '' ? $v->sender->name : 'N/A'}}
                            </td>
                            <td>
                                {{$v->amount}}
                            </td>
                            <td>
                                {{$v->status_string}}
                            </td>
                            <td>
                                <a href="{{route('backend.approvepayment', [$v->id])}}" class="btn btn-primary"> Approve</a>
                                <a href="{{route('backend.declinepayment', [$v->id])}}" class="btn btn-warning"> Reject</a>
                            </td>
                        </tr>
                        @endif
                    @endforeach
                    </thead>
                </table>
                {{$data->links()}}
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection