<!DOCTYPE html>
<html lang="{{ trans('common.lang') }}">

<head>
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '522786281936287');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=522786281936287&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
    <?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start('ob_gzhandler'); else ob_start(); ?>
    @include('analytics')
    <meta charset="utf-8">
    {{--<title>Xenren.co - Find the Co-workers & Co-work spaces.</title>--}}
    <title>@yield('title')</title>
    <meta name="description" content="{{ trans('home.landing_page_description') }}">
    <meta name="msvalidate.01" content="DAC0A9F3A3973E8BE8A5EB7FA07899EE" />
    <meta name="yandex-verification" content="22f470e43f8cfbf9" />
    <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <meta content="@yield('description')" name="description"/>
    <meta content="@yield('keywords')" name="keywords"/>
    <meta content="@yield('author')" name="author"/>
    <meta property="wb:webmaster" content="4bfa78a5221bb48a" />
    <meta property="qc:admins" content="2552431241756375" />
    <meta property="og:url" content="https://www.xenren.co/" />
    <meta property="og:image" content="https://www.xenren.co/images/landingpageMaincopy.png">
    <meta property="og:type" content="article" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:locale:alternate" content="fr_FR" />
    <meta property="og:locale:alternate" content="es_ES" />
    <meta property="og:image:width" content="300" />
    <meta property="og:image:height" content="200" />
    <link href="/images/Favicon.jpg" rel="icon" type="image/x-icon" />
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+SC&display=swap&subset=chinese-simplified" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    @include('frontend-xenren.includes.headscript')
    @include('frontend-xenren.includes.headscript-new')
{{--    <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css" />--}}
    @section('header') @show
    <!-- Global site tag (gtag.js) - Google Ads: 959593830 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-959593830" defer="defer"></script>
    <script src="https://sdk.pushy.me/web/1.0.5/pushy-sdk.js" defer="defer"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'AW-959593830');
    </script>
    <!-- Event snippet for Visit conversion page -->
    <script defer="defer">
        gtag('event', 'conversion', {'send_to': 'AW-959593830/73ZDCMm3jKMBEOb6yMkD'});
    </script>
    <!-- FAVICON -->
    <style>
        .set-reload-page-show-content{
            padding:0px;
        }
        .set-row-margin-show-content-page-reload {
            padding-top: 40px;
            padding-bottom: 40px;
        }
        @media (max-width: 500px) {
            .set-row-margin-show-content-page-reload {
                padding-left:0px;
                padding-right:0px;
                padding-top: 13px;
            }
        }
    </style>
</head>

<body onload="checkCookie()">
    <!-- Navigation -->
    @include('frontend-xenren.includes.header-new')
    <!--/#Navigation-->
    @if(\Auth::user() && (\Auth::user()->name == '' || is_null(\Auth::user()->name) || \Auth::user()->about_me == '' || is_null(\Auth::user()->about_me)))
    <div class="container set-reload-page-show-content" id="set-reload-page-content">
        <div class="row set-row-margin-show-content-page-reload">
            <div class="col-md-12">
                {{trans('common.fill_info')}}
                <a href="/personalInformation">{{trans('common.click_here')}}</a>
            </div>
        </div>
    </div>
    @endif
    <!-- Content -->
    @yield('content')
    <!-- /Content -->


    <!-- Footer -->
    @include('frontend-xenren.includes.footer-new')
    @include('frontend.includes.cookiesModal')
    <!-- Java Script -->
    @include('frontend-xenren.includes.footscript')
    <script src="{{asset('js/vue.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vue-resource.min.js')}}" type="text/javascript"></script>
    @section('footer') @show
    @section('modal')@show
    @if(\Auth::guard('users')->check())
        <script>
            localStorage.setItem('username',"{{\Auth::user()->id}}");
        </script>
        <script src="/js/cookies.js" type="text/javascript"></script >
    @endif
    @if(Auth::check())
        <script src="{{asset('js/pleaseCompleteInfo.js')}}" defer="defer"></script>
        <script defer="defer">
            $(function(){
                // Register device for push notifications
                Pushy.register({ appId: '5cc990d8a3c809350df6bc01' }).then(function (deviceToken) {
                    // Print device token to console
                    console.log('Pushy device token: ' + deviceToken);
                    var  curruntUserId = '{{\Auth::user()->id}}';
                    console.log('currunt user');
                    var url = '/save/device/token/'+curruntUserId+'/'+deviceToken;
                    $.ajax({
                       url:url,
                       Type:'GET',

                        success: function(response){
                            console.log('response');
                            console.log(response);
                            console.log('response');
                        },
                        error: function(error){
                            console.log('response');
                            console.log(error);
                            console.log('response');
                        },
                    });
                    // Send the token to your backend server via an HTTP GET request
                    //fetch('https://your.api.hostname/register/device?token=' + deviceToken);
                    // Succeeded, optionally do something to alert the user
                }).catch(function (err) {
                    // Handle registration errors
                    // console.error(err);
                    console.error('permission denay user cannot subscribed ');
                });

                // Check if the device is registered
                if (Pushy.isRegistered()) {
                    // Subscribe the device to a topic
                    Pushy.subscribe('news').catch(function (err) {
                        // Handle subscription errors
                        console.error('Subscribe failed:', err);
                    });
                }
            });
        </script>
    @endif

    @if(Auth::check())
        <script defer="defer">
            var user = {!! ($_G['user']!=null || isset($_G['user'])) ? $_G['user'] : null  !!};
        </script>

        @stack('js')
        @stack('vue')

        <script defer="defer">

            Vue.http.headers.common['X-CSRF-TOKEN'] = $('input[name="_token"]').val();
            new Vue({
                el: "#changeUserStatus",
                data: {
                    user: user,
                },
                methods: {
                    change: function (val) {
                        var input = {
                            'val': this.user[val],
                            'column': val,
                        };
                        this.$http.post('/personalInformation/change', input).then((response) => {
                            window.location.reload();
                        }, (response) => {
                        });
                    },
                }
            });

            <!-- detect andriod devices -->
            if( isMobile.any() ) alert('Mobile');
            var isMobile = {
                Android: function () {
                    return navigator.userAgent.match(/Android/i);
                },
            }
            <!-- detect android devices-->

        </script>
    @endif

    <script defer="defer">
        $("body").on('click', function () {
                $(".setCustomCollapse").collapse('hide');
        });
    </script>

{{--    <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>--}}
    <script defer="defer">
        $('.scroll-div').slimScroll({});
    </script>
</body>
</html>
