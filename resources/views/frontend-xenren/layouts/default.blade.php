<!DOCTYPE html>
<html lang="{{ trans('common.lang') }}">

<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta name="description" content="{{ trans('home.landing_page_description') }}">
    <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <meta content="@yield('description')" name="description"/>
    <meta content="@yield('keywords')" name="keywords"/>
    <meta content="@yield('author')" name="author"/>
    <meta property="wb:webmaster" content="4bfa78a5221bb48a" />
    <meta property="qc:admins" content="2552431241756375" />
    <!-- FAVICON -->
    <link href="{{$_G['xenren_path']}}images/logo/fav_new.png" rel="icon" type="image/x-icon" />
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    @include('frontend-xenren.includes.headscript')
    @section('header') @show

</head>

<body>
    <!-- Navigation -->
    @include('frontend-xenren.includes.header')
    <!--/#Navigation-->

    <!-- Content -->
    @yield('content')
    <!-- /Content -->
    

    <!-- Footer -->
    @include('frontend-xenren.includes.footer')

    <!-- Java Script -->
    @include('frontend-xenren.includes.footscript')
    @section('footer') @show
    @section('modal')@show

    <script src="{{asset('js/vue.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vue-resource.min.js')}}" type="text/javascript"></script>

    @if(Auth::check())
        <script>
            var user = {!! ($_G['user']!=null || isset($_G['user'])) ? $_G['user'] : null  !!};
        </script>

        @stack('js')
        @stack('vue')

        <script>

            Vue.http.headers.common['X-CSRF-TOKEN'] = $('input[name="_token"]').val();
            new Vue({
                el: "#changeUserStatus",
                data: {
                    user: user,
                },
                methods: {
                    change: function (val) {
                        var input = {
                            'val': this.user[val],
                            'column': val,
                        };
                        this.$http.post('/personalInformation/change', input).then((response) => {
                            window.location.reload();
                        }, (response) => {
                        });
                    },
                }
            });


        </script>
    @endif
</body>
</html>