@if ($paginator->lastPage() > 1)
    <div class="pull-right">
        {{$paginator->links()}}
    </div>
@endif