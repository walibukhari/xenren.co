
    <footer class="footer-main">
    <div class="container">
        <div class="row">
        	<div class="col-md-3 col-sm-3 col-xs-4 link setLink1Col3">
                <h3>{{ trans('home.network') }}</h3>
                <ul>
                    <li><a href="javascript:void(0);">{{ trans('home.sitemap') }}</a></li>
                    <li><a href="{{ route('frontend.findexperts' , \Session::get('lang')) }}">{{ trans('common.find_experts') }}</a></li>
                    <li><a href="{{\Session::get('lang')}}/instructionHiring">{{ trans('home.escrow') }}</a></li>
                    <li><a href="http://f.amap.com/f7qK_0DE2dKb">{{ trans('home.location') }}</a></li>
                    <li><a href="{{route('faq.faq-buyer.freelancer',[\Session::get('lang')])}}">Cowokers FAQ</a></li>
                    <li><a href="{{route('faq.faq-buyer.employer',[\Session::get('lang')])}}">Employer FAQ</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-4 link setLink2Col3">
	            <h3>{{ trans('home.about_xenren') }}</h3>
	            <ul>
	                <li>
	                    @if(App::isLocale('cn'))
	                        <a style="position: relative; margin-left:-4px;" href="{{ route('frontend.chinese-company-culture', \Session::get('lang')) }}">{{ trans('home.company_culture') }}</a>
	                    @elseif(App::isLocale('en'))
	                        <a style="position: relative; margin-left:-4px;" href="{{ route('frontend.company-culture', \Session::get('lang')) }}">{{ trans('home.company_culture') }}</a>
	                    @endif
	                </li>
	                <li><a href="{{ route('frontend.about-us', \Session::get('lang')) }}">{{ trans('home.about_us') }}</a></li>
	                <li><a href="{{ route('frontend.hiring-page', \Session::get('lang')) }}">{{ trans('home.careers') }}</a></li>
	                <li><a href="{{ route('frontend.terms-service', \Session::get('lang')) }}">{{ trans('home.terms_of_service') }}</a></li>
	                <li><a href="{{ route('frontend.privacy-policy', \Session::get('lang')) }}">{{ trans('home.privacy_policy') }}</a></li>
	                <li><a href="{{ route('frontend.meet-the-team2', \Session::get('lang')) }}">{{ trans('home.meet_the_team') }}</a></li>
	                <li><a href="{{ route('frontend.contact-us', \Session::get('lang')) }}">{{ trans('home.contact_us') }}</a></li>
	                <li><a href="{{ route('frontend.Community', \Session::get('lang')) }}">Community Home</a></li>
	            </ul>
	        </div>
	        <div class="col-md-3 col-sm-3 link setLink3Col3">
                <h3>{{ trans('home.additional_service') }}</h3>
                <ul>
                    <li>
                        <a href="{{ route('frontend.meet-sales-team', \Session::get('lang')) }}">{{ trans('home.enterprise_solutions') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('frontend.cooperate-page', \Session::get('lang')) }}">{{ trans('home.offline_business_cooperation') }}</a>
                    </li>
                </ul>

                <h3>{{ trans('home.connect_with_us') }}</h3>
                <ul>
                    <li><a href="{{ route('faq.faq-buyer', \Session::get('lang')) }}">{{ trans('home.customer_support') }}</a>
                    </li>
                    <li><a href="javascript:void(0);">{{ trans('home.api_center') }}</a></li>
                    <li><a href="javascript:void(0);">{{ trans('home.mobile_app') }}</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3 link setSocialIcons">
                <h3>{{ trans('home.social_media') }}</h3>
                <div class="social-icon">
                    <a href="javascript:void(0);" class="social-icons"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a href="javascript:void(0);" class="social-icons"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                    <a href="javascript:void(0);" class="social-icons"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    <a href="javascript:void(0);" class="social-icons"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                </div>

                <h3>{{ trans('home.mobile_apph') }}</h3>
                <div class="mob-app">
	                <a href="https://xenren.app.link/100Coworkers" target="_blank">
                        <div class="setImagedropdown">
	                        <img class="setiphone" src="/images/Mob-app01.png">
                            <div class="setImage-dropdown-content">
                                <i class="fa fa-caret-down setCaretUP2"></i>
                                <p class="setIphoneP">{{trans('common.get_notifications')}}!</p>
                                <img class="setIPhonImage" src="{{asset('images/iphoneNotifications.jpg')}}">
                                <img class="set-qr-images-home-footer" src="{{asset('images/iphoneStore.jpg')}}">
                            </div>
                        </div>
	                </a>
	                <a href="https://xenren.app.link/100Coworkers" target="_blank">
                        <div class="setImagedropdown">
	                        <img src="/images/Mob-app02.png">
                            <div class="setImage-dropdown-content-2">
                                <i class="fa fa-caret-down setCaretUP2"></i>
                                <p class="setAndriodP">{{trans('common.get_notifications')}}!</p>
                                <img class="setAndriodImage" src="{{asset('images/iphoneNotifications.jpg')}}">
                                <img class="set-qr-images-home-footer-andriod" src="{{asset('images/qr-code.jpg')}}">
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container footer-sub-first">
    	<div class="row">
    		<div class="col-md-2 col-sm-6 col-xs-4 setWidthW">
                <div class="footer-list-custom">
                    <h6>{{ trans('home.customer_support') }}:</h6>
                    <p>support@xenren.co</p>
                </div>
            </div>
            <div class="col-md-2 col-sm-6 col-xs-4 setWidthW">
                <div class="footer-list-custom">
                    <h6>{{ trans('home.service_line') }}:</h6>
                    <p>{{ trans('home.service_line_m') }}</p>
                </div>
            </div>
            <div class="col-md-2 col-sm-6 setCol-md-6-sm-6">
                <div class="footer-list-custom">
                    <h6>{{ trans('home.working_hour') }}:</h6>
                    <p>{{ trans('home.working_hour_t') }}</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 setWidthW">
                <div class="footer-list-custom setCompanyAddress">
                	<h6>{{ trans('home.company_address') }}:</h6>
                	<address>
                        {!! trans('home.company_address_s') !!}
                    </address>
            	</div>
            </div>
    	</div>
    	<div class="row mrg-top-10">
            <div class="col-md-6 col-sm-6 setCol-md-6-sm-6">
	        	<div class="row">
                    <div class="col-md-2 col-sm-2">
                        <img src="{{asset('images/Logo_White-1619x1619.png')}}" class="setMLIP" width="100%">
                    </div>
                    <div class="col-md-10 col-sm-10 setCol-md-6-sm-6">
                        {{--<img src="/xenren-assets/images/logo/new-main-L.png" class="setMLIP" width="100%">--}}
                        <a href="javascript:void(0);">
                            <h3>{{ trans('home.company_name') }}</h3>
                            <p>{{ trans('home.company_register_id') }}</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 text-right mrg-top-10 setalignment">
            	{{ trans('home.copyright', ['currentYear' => date('Y')]) }}
            </div>
    	</div>
    </div>
</footer>

<!-- Back to top -->
<a href="javascript:void(0);" id="back-to-top" title="Back to top"><i class="ti-angle-up"></i></a>
<!-- /Back to top -->

<!-- path to asset -->
<input type="hidden" name="xenren_path" class="xenren_path" value="{{$_G['xenren_path']}}">
<!-- /path to asset -->
