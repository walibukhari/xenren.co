
<nav class="main-nav menu-dark menu-transparent js-transparent">
    <div class="container">
        <div class="navbar">

            <div class="brand-logo">
                <a class="navbar-brand" href="javascript:void(0);">
                    <img src="{{$_G['xenren_path']}}images/logo/logo_2.png" alt="Logo">
                </a>
            </div>
            <!-- brand-logo -->

            <div class="navbar-header">
                <div class="inner-nav right-nav">
                    <ul>
                       <li class="dropdown classic-dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">{{ trans('common.language')}} <i class="ti-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li class="submenu dropdown">
                                    <a href="{{ route('setlang', ['lang' => 'en', 'redirect' => url('')]) }}">{{ trans('common.lang_en') }}</a>
                                    {{--<a href="http://lgdlair.com">--}}
                                        {{--{{ trans('common.lang_en') }}--}}
                                    {{--</a>--}}
                                </li>
                                <li class="submenu dropdown">
                                    <a href="{{ route('setlang', ['lang' => 'cn', 'redirect' => url('')]) }}">{{ trans('common.lang_cn') }}</a>
{{--                                    <a href="http://xenren.co">{{ trans('common.lang_cn') }}</a>--}}
                                </li>
                            </ul>
                            <!-- /dropdown-menu -->
                        </li>
                        <!-- /dropdown -->
                        
                        <li class="hide-desktop hidden">
                            <a href="#/" class="ti-search search-trigger"></a>
                        </li>
                        
                        <form action='#' id='search' method='get'>
                            <div class="input">
                                <div class="container">
                                    <input class="search" placeholder='Search...' type='text'>
                                    <button class="submit ti-search" type="submit" value="close"></button>
                                </div>
                                <!-- /container -->
                            </div>
                            <!-- /input -->
                            <button class="ti-close" id="close" type="reset"></button>
                        </form>
                        <!-- /Search -->
                    
                        <li class="navbar-toggle">
                            <button type="button" data-toggle="collapse" data-target=".collapse">
                                <span class="sr-only"></span>
                                <i class="ti-menu"></i>
                            </button>
                        </li>
                        <!-- /collapse-menu -->
                    </ul>
                </div>
                <!-- /right-nav -->
            </div>
            <!-- navbar-header -->

            <div class="custom-collapse navbar-collapse collapse inner-nav">
                <ul class="nav navbar-nav">
                    <li class="dropdown active">
                        <a href="{{ route('home') }}">{{ trans('common.home')}}</a>
                    </li>
                    <!-- /dropdown -->

                    <li class="dropdown classic-dropdown">
                        <a href="{{ route('frontend.jobs.index') }}">{{ trans('common.jobs')}}</a>
                        <ul class="dropdown-menu">
                            <li class="submenu dropdown">
                                <a href="{{ route('frontend.jobs.index') }}?project_type=2">
                                    {{ trans('common.official_project') }}
                                </a>
                            </li>
                            <li class="submenu dropdown">
                                <a href="{{ route('frontend.skillverification') }}">
                                    {{ trans('common.skill_verification') }}
                                </a>
                            </li>
                            <li class="submenu dropdown">
                                <a href="{{ route('frontend.favouriteJobs') }}">
                                    {{ trans('common.favourite_jobs') }}
                                </a>
                            </li>                            
                        </ul>
                    </li>

                    <li class="dropdown classic-dropdown">
                        <a href="{{ route('frontend.findexperts') }}">{{ trans('common.find_experts')}}</a>
                        <ul class="dropdown-menu">
                            <li class="submenu dropdown">
                                <a href="{{ route('frontend.favouriteFreelancers') }}">
                                    {{ trans('common.favourite_freelancers') }}
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown classic-dropdown">
                        <a href="{{ route('frontend.postproject') }}">{{ trans('common.post_project')}}</a>
                        <ul class="dropdown-menu">
                            <li class="submenu dropdown">
                                <a href="{{ route('frontend.postproject') }}">
                                    <img src="{{ asset('images/p1.jpg') }}"/>
                                    <span>
                                        {{ trans('common.post_project') }}
                                    </span>
                                </a>
                            </li>
                            <li class="submenu dropdown">
                                <a href="{{ route('frontend.mypublishorder') }}">
                                    <img src="{{ asset('images/p2.jpg') }}"/>
                                    <span>
                                        {{ trans('common.award_project') }}
                                    </span>
                                </a>
                            </li>
                            <li class="submenu dropdown receivedProjects">
                                <a href="{{ route('frontend.myreceiveorder') }}">
                                    <img src="{{ asset('images/p3.jpg') }}"/>
                                    <span>
                                        {{ trans('common.received_project') }}
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- /dropdown -->

                    @if (Auth::guard('users')->guest())
                        <li class="dropdown mega-menu">
                            <a href="{{ route('register') }}">{{ trans('member.register') }}</a>
                        </li>
                        <!-- /dropdown -->

                        <li class="dropdown classic-dropdown">
                            <a href="{{ route('login') }}">{{ trans('member.login') }}</a>
                        </li>
                        <!-- /dropdown -->
                    @else
                        <li class="dropdown classic-dropdown">
                            <a href="{{ route('frontend.usercenter') }}">{{ trans('common.user_center') }}</a>
                            <ul class="dropdown-menu">
                                <li class="submenu dropdown">
                                    <a id="changeStatusMenu" href="javascript:;" data-toggle="modal"
                                       data-target="#modalChangeUserStatus">
                                        {{ trans('common.change_status') }}
                                    </a>
                                </li>
                                <li class="submenu dropdown">
                                    <a href="{{ route('frontend.usercenter') }}">
                                        {{ trans('common.personal_information') }}
                                    </a>
                                </li>
                                <li class="submenu dropdown">
                                    <a href="{{ route('frontend.skillverification') }}">
                                        {{ trans('common.skill_verification') }}
                                    </a>
                                </li>
                                <li class="submenu dropdown">
                                    <a href="{{ route('frontend.myaccount') }}">
                                        {{ trans('common.receive_payment_details') }}
                                    </a>
                                </li>
                                <li class="submenu dropdown">
                                    <a href="{{ route('logout') }}">
                                        {{ trans('member.logout') }}
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @endif
                    
                    <li class="hide-tablet hidden">
                        <a href="#/" class="ti-search search-trigger"></a>
                    </li>
                </ul>
                <!-- /nav -->
            </div>
            <!-- /collapse -->
        </div>
        <!-- /navbar -->
    </div>
    <!-- /container -->
</nav>
