<style>
    .lds-ring {
        display: inline-block;
        position: relative;
        width: 20px;
        height: 20px;
    }
    .lds-ring div {
        box-sizing: border-box;
        display: block;
        position: absolute;
        width: 15px;
        height: 15px;
        margin: 1px;
        border: 2px solid #fff;
        border-radius: 50%;
        animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
        border-color: #fff transparent transparent transparent;
    }
    .lds-ring div:nth-child(1) {
        animation-delay: -0.45s;
    }
    .lds-ring div:nth-child(2) {
        animation-delay: -0.3s;
    }
    .lds-ring div:nth-child(3) {
        animation-delay: -0.15s;
    }
    @keyframes lds-ring {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }
</style>
<nav class="main-nav menu-dark menu-transparent js-transparent">
    <div class="container-fluid">
        <div class="navbar set-navbar-upadtes">

            <div class="brand-logo">
                <a class="navbar-brand" href="">
                    {{--javascript:void(0);--}}
                    <img src="{{asset('images/homelogo.jpg')}}" alt="Logo">
                </a>
            </div>
            <!-- brand-logo -->

            <div class="navbar-header">
                <div class="inner-nav right-nav setInnerNavNHPH">
                    @if (Auth::guard('users')->guest())
                        <ul>
                            <li class="dropdown classic-dropdown setLNGDD">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="{{ trans('common.language_flg')}}"> <i class="ti-angle-down settiAngle"></i>
                                </a>
                                <ul class="dropdown-menu setDDLNGUL">
                                    <li class="submenu dropdown">
                                        <a href="{{ route('setlang', ['lang' => 'en', 'redirect' => url('')]) }}">
                                            <img src="{{ trans('common.language_flg_en')}}">
                                            {{ trans('common.lang_en') }}
                                        </a>
                                        {{--<a href="http://lgdlair.com">--}}
                                        {{--{{ trans('common.lang_en') }}--}}
                                        {{--</a>--}}
                                    </li>
                                    <li class="submenu dropdown">
                                        <a href="{{ route('setlang', ['lang' => 'cn', 'redirect' => url('')]) }}">
                                            <img src="{{ trans('common.language_flg_cn')}}">
                                            {{ trans('common.lang_cn') }}
                                        </a>
                                        {{--                                    <a href="http://xenren.co">{{ trans('common.lang_cn') }}</a>--}}
                                    </li>
                                </ul>
                                <!-- /dropdown-menu -->
                            </li>
                            <!-- /dropdown -->

                            <li style="display: none;" class="hide-desktop hidden">
                                <a style="display:none;" href="#/" class="ti-search search-trigger"></a>
                            </li>

                            <form action='#' id='search' method='get'>
                                <div class="input">
                                    <div class="container">
                                        <input class="search" placeholder='Search...' type='text'>
                                        <button class="submit ti-search" type="submit" value="close"></button>
                                    </div>
                                    <!-- /container -->
                                </div>
                                <!-- /input -->
                                <button class="ti-close" id="close" type="reset"></button>
                            </form>
                            <!-- /Search -->

                            <li class="navbar-toggle">
                                <button type="button" data-toggle="collapse" data-target=".collapse">
                                    <span class="sr-only"></span>
                                    {{--<i class="ti-menu set-ti-menu-head-new"></i>--}}
                                    <i class="ti-menu set-ti-menu-home-header"></i>
                                </button>
                            </li>
                            <!-- /collapse-menu -->
                        </ul>
                    @else
                        <ul class="nav navbar-nav pull-right setNFIA">
                            <li class="separator hide"> </li>
                            <li class="usercenter-li f-s-16 setHeader_fed" id="header_feedback">
                                <a href="{{ route('frontend.feedback', \Session::get('lang')) }}"
                                   class="normal-link setLINKKK setNormalLink {{ isset($_G['route_name']) && $_G['route_name'] == 'frontend.feedback' ? ' active' : '' }} {{ isset($_G['route_name']) && $_G['route_name'] == 'frontend.feedbackChat' ? ' active' : '' }}">
                                <span class="fee">
                                {{ trans('common.feedback') }}
                                </span>
                                </a>
                            </li>


                            <li class="dropdown dropdown-extended dropdown-notification dropdown-dark update_notification setNotification" id="header_notification_bar">
                                @include('frontend.includes.notification')
                            </li>
                            <!-- END NOTIFICATION DROPDOWN -->
                            <li class="separator hide"> </li>
                            <li class="dropdown dropdown-user dropdown-dark setALUL">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" onclick="window.location.href='/resume'" >
                                    <img id="imgAvatar" onerror="this.src='/images/MainLogoDefault.png'" alt="" class="img-circle setHEADIMAGEAVATAR1" src="{{ $_G['user']->getCompressedImageAttribute() }}" />
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default dropdown-menu-logout setDMLOGOUT">
                                    <li class="user set-User-Li-Home">
                                        <div class="user-img text-center setUCENTERIMAGE" style="cursor: pointer"  onclick="window.location.href='/resume'">
                                            <img alt="" onerror="this.src='/images/MainLogoDefault.png'"  class="img-circle" src="{{ $_G['user']->getAvatar() }}" />
                                        </div>
                                        <div class="user-name">
                                                <span class="username username-hide-on-mobile">
                                                {{ Auth::guard('users')->user()->getName() }}
                                                </span><br>
                                            {{--<small class="setSMALLHNew">--}}
                                                {{--{{ trans('common.level') }} : 50--}}
                                            {{--</small>--}}
                                        </div>
                                        <div class="setlngec">
                                            <a class="setaImgEng" href="{{ route('setlang', ['lang' => 'en', 'redirect' => url('')]) }}">
                                                <img src="{{ trans('common.language_flg_en')}}">
                                                {{ trans('common.lang_en') }}
                                            </a>
                                            <a class="setaImgChin" href="{{ route('setlang', ['lang' => 'cn', 'redirect' => url('')]) }}">
                                                <img src="{{ trans('common.language_flg_cn')}}">
                                                {{ trans('common.lang_cn') }}
                                            </a>
                                        </div>
                                        <div class="setHeadCBOTH"></div>
                                    </li>
                                    {{--<hr class="sethr11">--}}
                                    <li class="change-status setchangeStatus set-hover">
                                        <a href="javascript:;" data-toggle="modal"
                                           data-target="#modalChangeUserStatus">
                                            <img class="setchangestatusImage" src="/images/icons/al_co-founder1_yellow.png">
                                            <span class="spnChangeStatus">
                                                {{ trans('common.change_status') }}
                                                </span>
                                        </a>
                                    </li>
                                    <li class="set-hover">
                                        <a href="{{ route('frontend.usercenter' , \Session::get('lang')) }}">
                                            <img src="/images/icons/id_card_filled_100px.png">
                                            <span class="spn">
                                                {{ trans('common.personal_information') }}
                                                </span>
                                        </a>
                                    </li>
                                    <li class="set-hover">
                                        <a href="{{ route('frontend.skillverification', \Session::get('lang')) }}">
                                            <img src="/images/icons/tasks_100px.png">
                                            <span class="spn">
                                                {{ trans('common.skill_verification') }}
                                                </span>
                                        </a>
                                    </li>
                                    <li class="set-hover">
                                        <a href="{{ route('frontend.myaccount', \Session::get('lang')) }}">
                                            <img src="/images/icons/bank_card_back_side_100px.png">
                                            <span class="spn">
                                                {{ trans('common.receive_payment_details') }}
                                                </span>
                                        </a>
                                    </li>
                                    <li class="set-hover">
                                        <a href="{{ route('logout') }}">
                                            <img src="/images/icons/logout_rounded_left_96px.png">
                                            <span class="spn">
                                                {{ trans('member.logout') }}
                                                </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="navbar-toggle setNAVBARTOG">
                                <button type="button" data-toggle="collapse" data-target=".collapse">
                                    <span class="sr-only"></span>
                                    <i class="ti-menu set-tr-t-i-menu"></i>
                                </button>
                            </li>
                        </ul>
                    @endif
                </div>
                <!-- /right-nav -->
            </div>
            <!-- navbar-header -->
            @if(Auth::guard('users')->guest())
                <a href="{{url('https://coworkspace.xenren.co')}}" class="coworking-spaces">{{ trans('common.co_work_space')}}</a>
            @endif
            <div id="closeDropDown" class="custom-collapse navbar-collapse collapse inner-nav setCustomCollapse">
                <ul class="nav navbar-nav setNAVBARHOMEPAGE">
                    @if(Auth::guard('users')->check())
                    <li class="dropdown setCoWorkSpace">
                        <a href="{{url('https://coworkspace.xenren.co')}}">{{ trans('common.co_work_space')}}</a>
                    </li>
                    @else
                        <li class="dropdown setCoWorkSpace authGuardGuest">
                            <a href="{{url('https://coworkspace.xenren.co')}}">{{ trans('common.co_work_space')}}</a>
                        </li>
                    @endif
                    <li class="dropdown active setFLI setHOme">
                        <a href="{{ URL('/'.Session::get('lang')) }}">{{ trans('common.home')}}</a>
                    </li>
                    <!-- /dropdown -->

                    <li class="jobsHide dropdown classic-dropdown setClassics setFLI" id="classic">
                        <a href="{{ route('frontend.jobs.index', \Session::get('lang')) }}" class="setHover">
                            {{ trans('common.jobs')}} <i id="ti-angel" class="ti-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu setDropDHP">
                            <li id="submenu" class="submenu dropdown">
                                <a href="{{ route('frontend.jobs.index', \Session::get('lang')) }}">
                                    {{ trans('common.find_job') }}
                                </a>
                            </li>
                            <li class="submenu dropdown">
                                <a href="{{ route('frontend.jobs.official' , \Session::get('lang')) }}?project_type=2">
                                    {{ trans('common.official_project') }}
                                </a>
                            </li>
                            <li class="submenu dropdown">
                                <a href="{{ route('frontend.skillverification', \Session::get('lang')) }}">
                                    {{ trans('common.skill_verification') }}
                                </a>
                            </li>
                            <li class="submenu dropdown">
                                <a href="{{ route('frontend.favouriteJobs', \Session::get('lang')) }}">
                                    {{ trans('common.favourite_jobs') }}
                                </a>
                            </li>
                            <li class="submenu dropdown">
                                <a href="{{ route('frontend.mypublishorder', \Session::get('lang')) }}">
                                    {{ trans('common.isued_job') }}
                                </a>
                            </li>
                            <li class="submenu dropdown">
                                <a href="{{ route('frontend.myreceiveorder', \Session::get('lang')) }}">
                                    {{ trans('common.recieved_job') }}
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="FindExpertHide dropdown classic-dropdown setClassics setFLI">
                        <a href="{{ route('frontend.findexperts', \Session::get('lang')) }}" class="setHover">
                            {{ trans('common.find_experts')}} <i id="ti-angel" class="ti-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu find-expert setFEDM">
                            <li>
                                <a href="{{ route('frontend.findexperts', \Session::get('lang')) }}">
                                    <span>
                                        {{ trans('common.find_experts') }}
                                        {{--{{trans('common.find')}} {{trans('common.expert')}}--}}
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('frontend.favouriteFreelancers', \Session::get('lang')) }}">
                                    <span>
                                        {{ trans('common.favourite_freelancers') }}
                                        {{--{{trans('common.favorite')}} {{trans('common.freelancerrr')}}--}}
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown classic-dropdown setClassics setFLI">
                        <a href="{{ route('frontend.postproject', \Session::get('lang')) }}" class="setHover">
                            {{ trans('common.post_project')}} <i id="ti-angel" class="ti-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu setPPUlli" style="width:auto; min-width:225px">
                            <li class="submenu dropdown">
                                <a href="{{ route('frontend.postproject', \Session::get('lang')) }}">
                                    <img src="{{ asset('images/p1.jpg') }}"/>
                                    <span>
                                        {{ trans('common.post_project') }}
                                    </span>
                                </a>
                            </li>
                            <li class="submenu">
                                <a class="awardedProjects" href="{{ route('frontend.mypublishorder', \Session::get('lang')) }}">
                                    <img src="{{ asset('images/p2.jpg') }}"/>
                                    <span>
                                        {{ trans('common.award_project') }}
                                    </span>
                                    @if(\Auth::user())
                                    <i class="fa fa-bell notificationBell setNB1" style="position:relative;right:15px"></i>
                                        <span class="badge badge-info notificationBellBubble" id="unread-msg-counter" style="right:-10px; width: 20px; height: 20px; padding: 4px!important; font-size:11px">
                                                @if($_G['user']->getMyPublishOrderCount() != null && $_G['user']->getMyPublishOrderCount() != 0)
                                                    @php
                                                        $publishPrderCount = $_G['user']->getMyPublishOrderCount();
                                                        $publishPrderCount = $publishPrderCount > 0 ? $publishPrderCount : 0;
                                                    @endphp
                                                    @php echo e($publishPrderCount); @endphp

                                                 @else

                                                 @endif
                                         </span>
                                    @endif
                                </a>
                            </li>
                            <li class="submenu">
                                <a class="receivedProjects" href="{{ route('frontend.myreceiveorder', \Session::get('lang')) }}">
                                    <img src="{{ asset('images/p3.jpg') }}"/>
                                    <span>
                                        {{ trans('common.received_project') }}
                                    </span>
                                    @if(\Auth::user())
                                        <i class="fa fa-bell notificationBell setNB1" style="position:relative;right:15px"></i>
                                        <span class="badge badge-info notificationBellBubble" id="unread-msg-counter" style="right:-10px; width: 20px; height: 20px; padding: 4px!important; font-size:11px">
                                                @if($_G['user']->getMyReceivedOrderCount() != null && $_G['user']->getMyReceivedOrderCount() != 0)
                                                    @php
                                                        $myReceivedOrderCount = $_G['user']->getMyReceivedOrderCount();
                                                        $yReceivedOrderCount = $myReceivedOrderCount > 0 ? $myReceivedOrderCount : 0;
                                                    @endphp
                                                    @php echo e($yReceivedOrderCount); @endphp

                                                 @else

                                                 @endif
                                         </span>
                                    @endif
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- /dropdown -->
                    <li class="dropdown classic-dropdown setClassics setFLI">
                        <a href="{{route('frontend.Blog',\Session::get('lang'))}}" class="setHover">
                            {{trans('common.blog')}}
                        </a>
                    </li>
                    @if (Auth::guard('users')->guest())
                        <li class="dropdown mega-menu setRLP setALink1">
                            <a href="{{ route('register' , \Session::get('lang')) }}" class="setA">
                                {{ trans('member.register') }}
                            </a>
                        </li>
                        <!-- /dropdown -->

                        <li class="dropdown classic-dropdown setRLP setALink2" id="dropdown-login">
                            {{--<a href="{{ route('login') }}" class="setB" data-hover="dropdown">--}}
                                {{--{{ trans('member.login') }}--}}
                            {{--</a>--}}
                            <a href="{{ route('login', \Session::get('lang'))  }}" class="set-dropdown-toggle setB"> {{ trans('member.login') }}</a>
                            <ul class="dropdown-menu setDropdownMenu">
                                <i class="fa fa-caret-up setCaretUP"></i>
                                <form class="form setFORMBODY" role="form" method="POST" action="{{ route('login.post') }}" id="login-form">
                                    {!! csrf_field() !!}
                                    <div id="errorMsg" class="alert alert-danger" style="display: none;"></div>
                                    <li class="setLiNew1">
                                        <label>{{trans('common.email_address')}}</label>
                                        <img class="setEUImage" src="{{asset('images/newUser.png')}}">
                                        <input type="text" name="phone_or_email" id="phone_or_email" class="form-control setEmailBox" value="{{ old('phone_or_email') }}">
                                    </li>
                                    <li class="setLiNew2">
                                        <label>{{trans('common.password')}}</label>
                                        <img class="setPUImage" src="{{asset('images/newLock.png')}}">
                                        <input type="password" name="password" id="pass" class="form-control setPassBox" value="">
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-md-12 setCol12NewRMF">
                                                <div class="col-md-7 setCol7Npsd">
                                                    <input type="checkbox" name="remember" id="remember" class="setCheckbox form-control">
                                                    <label class="setRMLabel">{{ trans('member.remember_me') }}</label>
                                                </div>
                                                <div class="col-md-5 setCol5Npsd p-0">
                                                    <a href="{{ route('password.reset') }}" class="setAFGP" style="margin:0!important">{{ trans('member.forget_password') }}?</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-md-12 setCol12NPsd">
                                                <button type="button" style="display: flex;justify-content: center;align-items: center;" class="setbtnbtnNPsd btn btn-block" id="submit-login" >
                                                    <span class="idSpanLoginHover">{{ trans('member.login_now') }}</span>
                                                    <div style="display: none;" class="lds-ring">
                                                        <div></div><div></div><div></div><div></div>
                                                    </div>
                                                </button>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <span class="setSpanNeP">{{trans('common.dont_have_account')}}? <a href="{{ url('/register') }}" class="sertSUL" style="left:54%">{{trans('common.sign_up')}}</a></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <a href="{{ route('facebook.login') }}" class="setALinkNPsd">
                                                    <img class="ImageFB" src="{{asset('images/facebook.jpg')}}">
                                                    <span class="setSpanFB">{{trans('common.connect_with_facebook')}}</span>
                                                </a>
                                            </div>
                                        </div>
                                    </li>
                                </form>
                            </ul>
                        </li>
                        <!-- /dropdown -->
                    {{--@else--}}
                        {{--<li class="dropdown classic-dropdown setUseCEnter">--}}
                            {{--<a href="{{ route('frontend.usercenter') }}">{{ trans('common.user_center') }}--}}
                                {{--<i id="ti-angel" class="ti-angle-down"></i>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu">--}}
                                {{--<li class="submenu dropdown">--}}
                                    {{--<a id="changeStatusMenu" href="javascript:;" data-toggle="modal"--}}
                                       {{--data-target="#modalChangeUserStatus">--}}
                                        {{--{{ trans('common.change_status') }}--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li class="submenu dropdown">--}}
                                    {{--<a href="{{ route('frontend.usercenter') }}">--}}
                                        {{--{{ trans('common.personal_information') }}--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li class="submenu dropdown">--}}
                                    {{--<a href="{{ route('frontend.skillverification') }}">--}}
                                        {{--{{ trans('common.skill_verification') }}--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li class="submenu dropdown">--}}
                                    {{--<a href="/transactionHistory">--}}
                                        {{--{{trans('common.transcation_history')}}--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li class="submenu dropdown">--}}
                                    {{--<a href="{{ route('frontend.myaccount') }}">--}}
                                        {{--{{ trans('common.receive_payment_details') }}--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li class="submenu dropdown">--}}
                                    {{--<a href="{{ route('logout') }}">--}}
                                        {{--{{ trans('member.logout') }}--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    @endif

                    <li class="hide-tablet hidden">
                        <a href="#/" class="ti-search search-trigger"></a>
                    </li>
                </ul>
                <!-- /nav -->
            </div>
            <!-- /collapse -->
        </div>
        <!-- /navbar -->
    </div>
    <!-- /container -->
</nav>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $('.set-dropdown-toggle').click(function(e) {
        if ($(document).width() > 768) {
            e.preventDefault();

            var url = $(this).attr('href');


            if (url !== '#') {

                window.location.href = url;
            }

        }
    });
</script>
<script>
    +function () {
        $(document).ready(function () {
            $("#submit-login").click(function(e){
                e.preventDefault()
                ajaxProceed();
                $('.idSpanLoginHover').hide();
                $('.lds-ring').show();
            });

            function ajaxProceed() {
                var _token = $("input[name='_token']").val()
                var phone_or_email = $('#phone_or_email').val()
                var pass = $('#pass').val()
                var errField = $('.err-msg')

                $.ajax({
                    url: "{{ route('login.post') }}",
                    type:'POST',
                    data: {_token:_token, phone_or_email:phone_or_email, pass:pass},
                    success: function(data) {
                        errField.remove()
                        window.location.href='{{ route('home') }}';
                    },
                    error: function(err) {
                        $('.idSpanLoginHover').show();
                        $('.lds-ring').hide();
                        errField.remove()
                        printErrorMsg(err)
                    }
                });
            }

            function printErrorMsg (err) {
                var fields = $('.setFIELD')
                console.log(err)
                $.each(err.responseJSON, function (i, error) {
                    if ($.type(error) == 'object') {
                        var phone_or_email = $('#phone_or_email')
                        var pass = $('#pass')

                        $.each(error, function (key, val) {
                            var el = $('#'+key)
                            el.fadeIn().after($('<div><span class="err-msg" style="color: red;">' + val + '</span></div>'))
                            el.removeClass('hasContent')
                            el.addClass('hasContent-error')
                        })

                        if (error.phone_or_email == null) setBtn(true, phone_or_email)
                        else setBtn(false, phone_or_email)

                        if (error.pass == null) setBtn(true, pass)
                        else setBtn(false, pass)

                    } else {
                        var errorText = ''
                        $.each(error, function(index, el) {
                            errorText += el
                        })
                        swal("Warning!", errorText, "warning")
                        // $('#errorMsg').fadeIn().text(error)
                        // $('.err-msg').remove()
                        $.each(fields, function (key, val) {
                            $(this).removeClass('hasContent')
                            $(this).addClass('hasContent-error')
                        })
                    }
                })
            }

            $('#dropdown-login').hover(function() {
                $('#dropdown-login #phone_or_email').focus();
            }, function() {
                /* Stuff to do when the mouse leaves the element */
            });

            $('#dropdown-login input').blur(function(event) {
                $('#dropdown-login').removeClass('hover')
            }).focus(function(event) {
                $('#dropdown-login').addClass('hover')
            });
        });
    }(jQuery);

    $('#pass').bind('keypress',function(e){
        if(e.keyCode === 13) {
            $('#submit-login').click();
        }
    });

</script>
