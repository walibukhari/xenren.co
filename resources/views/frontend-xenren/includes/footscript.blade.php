{{--<script type="text/javascript" src="{{asset('xenren-assets/js/jquery-2.1.4.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/js/bootstrap.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/js/jquery.fitvids.js')}}" defer="defer"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/js/jquery.mb.YTPlayer.js')}}" defer="defer"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/js/owl.carousel.min.js')}}" defer="defer"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/js/wow.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/js/jquery.parallax-1.1.3.js')}}" defer="defer"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/js/jquery.countTo.js')}}" defer="defer"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/js/jquery.countdown.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/js/jquery.appear.js')}}" defer="defer"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/js/smoothscroll.js')}}" defer="defer"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/js/jquery.magnific-popup.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/components/revolutionslider/js/jquery.themepunch.revolution.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/components/revolutionslider/js/jquery.themepunch.tools.min.js')}}" defer="defer"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/components/revolutionslider/js/extensions/revolution.extension.video.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/components/revolutionslider/js/extensions/revolution.extension.slideanims.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/components/revolutionslider/js/extensions/revolution.extension.actions.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/components/revolutionslider/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/components/revolutionslider/js/extensions/revolution.extension.kenburn.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/components/revolutionslider/js/extensions/revolution.extension.navigation.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/components/revolutionslider/js/extensions/revolution.extension.migration.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/components/revolutionslider/js/extensions/revolution.extension.parallax.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/components/revolutionslider/js/setup.js')}}" defer="defer"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/js/jquery.particleground.js')}}"></script>--}}
<script type="text/javascript" src="{{asset('xenren-assets/js/setup.particleground.js')}}" defer="defer"></script>
{{--<script type="text/javascript" src="{{asset('xenren-assets/js/imagesloaded.pkgd.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/js/isotope.pkgd.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/js/main.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{asset('xenren-assets/js/custom-main.js')}}"></script>--}}

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-99673167-1', 'auto');
    ga('send', 'pageview');

    var g_lang = {
        success : "{{ trans('common.success') }}",
        error : "{{ trans('common.error')  }}",
        notice : "{{ trans('common.notice')  }}",
        search : "{{ trans('common.search')  }}"
    };
</script>
{{--<script type="text/javascript" src="{{asset('assets/global/plugins/jquery.blockui.min.js')}}" ></script>--}}
{{--<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.js')}}" ></script>--}}
{{--<script type="text/javascript" src="{{asset('assets/global/plugins/magicsuggest/magicsuggest-min.js')}}" ></script>--}}

{{--<script type="text/javascript" src="{{asset('js/ajaxform.min.js')}}" ></script>--}}
<script type="text/javascript" src="{{asset('js/app.js')}}"></script>
<script type="text/javascript" src="{{asset('js/minifiedHome.js')}}"></script>
{{--<script type="text/javascript" src="{{asset('js/global.js')}}"></script>--}}

<!--To fix ajax token mismatch problem -->
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
