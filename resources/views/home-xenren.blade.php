@extends('frontend-xenren.layouts.default-new')

@section('title')
    {{ trans('home.title') }}
@endsection

@section('keywords')
    {{ trans('home.keywords') }}
@endsection

@section('description')
    {{ trans('home.description') }}
@endsection

@section('author')
    Xenren
@endsection

@section('header')
    <link href="/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css">
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- <link href="/custom/css/frontend/home.css" rel="stylesheet" type="text/css" /> -->
    <!-- <link href="/custom/css/frontend/home-new-custom.css" rel="stylesheet" type="text/css" /> -->
{{--    <link href="/custom/css/frontend/modalSetUsername.css" rel="stylesheet" type="text/css"/>--}}
    <link href="/custom/css/frontend/modalSetContactInfo.css" rel="stylesheet" type="text/css"/>
{{--    <link href="/custom/css/frontend/modalAddNewSkillKeywordv2.css" rel="stylesheet" type="text/css"/>--}}
{{--    <link href="/css/sales-version.css" rel="stylesheet" type="text/css"/>--}}
{{--    <link href="/custom/css/frontend/searchButtons.css" rel="stylesheet" type="text/css"/>--}}
    <link href="/custom/css/frontend/homePagePopup.css" rel="stylesheet" type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <link href="{{asset('assets/css/chatbot.css')}}" rel="stylesheet" >
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
{{--    <link rel="stylesheet" href="{{asset('style.css')}}">--}}
{{--    <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css" />--}}
    <script src="{{asset('autocomplete.multiselect.js')}}" defer="defer"></script>

    <style>
        .ui-menu-item:focus {
            outline: none !important;
        }
        .ui-menu-item {
            outline: none !important;
        }
        .ui-menu-item .ui-menu-item-wrapper.ui-state-active {
            border: 0px solid white;
        }
        #header_feedback span {
            border: 2px solid #57b029;
            padding: 9px 20px;
            border-radius: 5px;
        }
        #myAutocomplete {
            width: 100%;
        }

        .ui-autocomplete-multiselect.ui-state-default {
            display: block;
            background: #fff;
            border: 1px solid #ccc;
            padding: 0px 3px;
            overflow: hidden;
            cursor: text;
            min-height: 50px;
            margin: 0px auto;
            width: 100% !important;
            border-radius: 30px;
            text-align: left;
        }

        .ui-menu-item .ui-menu-item-wrapper.ui-state-active {
            background: #ffffff !important;
            font-weight: bold !important;
            color: #57b029 !important;
        }

        .autoooComplete {
            top:2px;
            margin-left: 5px;
            background: transparent !important;
            width:auto;
            position:absolute;
            border-radius: 36px;
            padding-top: 10px;
            display: inline-block;
            overflow: auto;
            padding-bottom: 8px;
            padding-left: 11px;
        }

        .dropdownInput{
            top:39px;
            margin-left:5px;
            background: transparent !important;
            width:auto;
            position:static;
            border-radius: 36px;
            padding-top: 10px;
            display: inline-block;
            overflow: auto;
            padding-bottom: 2px;
            padding-left: 11px;
        }
        .ui-autocomplete-multiselect .ui-autocomplete-multiselect-item .ui-icon {
            float: right;
            cursor: pointer;
            position: relative;
            top:3px;
        }


        .setAutoComplete{
            width: 41.66666667%;
            margin: 0px auto !important;
            float: unset;
        }
        .adujustAutoComplete{
            width: 96.966667%;
            margin: 0px auto !important;
            float: unset;
        }
        .audjustAutoComplete1{
            width: 96.2%;
            margin: 0px auto !important;
            float: unset;
        }
        .audjustAutoComplete2{
            width: 92.966667%;
            margin: 0px auto !important;
            float: unset;
        }

        .audjustAutoComplete3{
            width: 92.466667%;
            margin: 0px auto !important;
            float: unset;
        }
        .audjustAutoComplete4{
            width: 92.1%;
            margin: 0px auto !important;
            float: unset;
        }
        .audjustAutoComplete5{
            width: 91.5%;
            margin: 0px auto !important;
            float: unset;
        }
        .audjustAutoComplete6{
            width: 90.5%;
            margin: 0px auto !important;
            float: unset;
        }

        .ui-icon, .ui-widget-content .ui-icon{
            background-image: url(../images/ms-close.png);
            width: 7px;
            cursor: pointer;
            height: 7px;
            float: right;
            margin: 6px 2px 0 10px;
            background-position: 0 -7px;
        }

        .ui-autocomplete-multiselect .ui-autocomplete-multiselect-item {
            display: inline-block;
            border: 1px solid #57b029;
            border-radius: 14px;
            margin-right: 4px;
            margin-bottom: 0px;
            color: #fff;
            background-color: #57b029;
            position: relative;
            top: 10px;
            left: 10px;
            font-size: 12px;
            padding: 1px 10px;
        }

        .ui-autocomplete-multiselect input {
            display: inline-block;
            border: none;
            outline: none;
            height: auto;
            margin: 0;
            overflow: visible;
            margin-bottom: 0;
            vertical-align: bottom;
            text-align: left;
            margin-left: 10px;
            color:#25ce0f !important;
        }

        .ui-autocomplete-multiselect.ui-state-active {
            outline: none;
            border: 1px solid #7ea4c7;
            -moz-box-shadow: 0 0 5px rgba(50,150,255,0.5);
            -webkit-box-shadow: 0 0 5px rgba(50,150,255,0.5);
            -khtml-box-shadow: 0 0 5px rgba(50,150,255,0.5);
            box-shadow: 0 0 5px rgba(50,150,255,0.5);
        }

        .ui-autocomplete {
            border-top: 0;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }

        .ui-widget.ui-widget-content {
            max-height: 200px !important;
            height: auto !important;
            min-height: 80px !important;
            overflow: scroll;
            overflow-x: hidden;
            overflow-y: inherit;
            left:0px !important;
            margin: 0 auto;
            padding-right: 15px;
            padding-left: 15px;
            border-bottom-right-radius: 25px;
            border-bottom-left-radius: 25px;
        }

        .ui-widget.ui-widget-content::-webkit-scrollbar-thumb
        {
            border-radius: 10px;
            -webkit-box-shadow: #fff;
            height: 5px !important;
            display: none !important;
            width: 4px;
            background-color: #fff;
            overflow: hidden;
        }
        .ui-widget.ui-widget-content::-webkit-scrollbar
        {
            width: 4px;
            background-color: #fff;
            height: 5px !important;
            border-radius: 10px;
            padding-bottom: 10px;
            position: relative;
            bottom: 10px;
            overflow: hidden;
            display: none !important;
        }
        .ui-widget.ui-widget-content::-webkit-scrollbar-track
        {
            width: 4px;
            overflow: hidden;
            height: 50% !important;
            max-height: 50%;
            background-color: transparent !important;
            display: none !important;
        }
        .search-type-en {
            font-size: 12px;
        }
        @media only screen and (max-width: 991px) and (min-width: 769px) {

            #findFreelancer {
                width: 100%;
                font-size: 100%;
            }
            #findFreelancer {
                max-width: 100%;
                font-size: 100%;
            }
            #findCoFounder {
                max-width: 100%;
                font-size: 100%;
            }
        }
        @media only screen and (max-width: 450px) {
            #findProject {
                max-width: 100%;
                font-size: 60%;
            }
            #findFreelancer {
                max-width: 100%;
                font-size: 60%;
            }
            #findCoFounder {
                max-width: 100%;
                font-size: 60%;
            }
        }
        .ui-autocomplete-multiselect .ui-autocomplete-multiselect-item .ui-icon {
            float: right;
            cursor: pointer;
            position: relative;
            top:3px;
        }


        .setAutoComplete{
            width: 41.66666667%;
            margin: 0px auto !important;
            float: unset;
        }
        .adujustAutoComplete{
            width: 96.966667%;
            margin: 0px auto !important;
            float: unset;
        }
        .audjustAutoComplete1{
            width: 96.2%;
            margin: 0px auto !important;
            float: unset;
        }
        .audjustAutoComplete2{
            width: 92.966667%;
            margin: 0px auto !important;
            float: unset;
        }

        .audjustAutoComplete3{
            width: 92.466667%;
            margin: 0px auto !important;
            float: unset;
        }
        .audjustAutoComplete4{
            width: 92.1%;
            margin: 0px auto !important;
            float: unset;
        }
        .audjustAutoComplete5{
            width: 91.5%;
            margin: 0px auto !important;
            float: unset;
        }
        .audjustAutoComplete6{
            width: 90.5%;
            margin: 0px auto !important;
            float: unset;
        }

        .ui-widget.ui-widget-content {
            max-height: 200px !important;
            height: auto !important;
            min-height: 80px !important;
            overflow: scroll;
            overflow-x: hidden;
            overflow-y: inherit;
            left: 0px !important;
            margin: 0 auto;
            padding-right: 15px;
            padding-left: 15px;
            border-bottom-right-radius: 25px;
            border-bottom-left-radius: 25px;
        }
        .autoooComplete {
            top: 2px;
            margin-left: 5px;
            background: transparent !important;
            width: auto;
            position: absolute;
            border-radius: 36px;
            padding-top: 10px;
            display: inline-block;
            overflow: auto;
            padding-bottom: 8px;
            padding-left: 11px;
        }
        .ui-autocomplete-multiselect.ui-state-default {
            display: block;
            background: #fff;
            border: 1px solid #ccc;
            padding: 0px 3px;
            overflow: hidden;
            cursor: text;
            min-height: 50px;
            margin: 0px auto;
            width: 100% !important;
            border-radius: 30px;
            text-align: left;
        }
        .ui-autocomplete-multiselect input {
            display: inline-block;
            border: none;
            outline: none;
            height: auto;
            margin: 0;
            overflow: visible;
            margin-bottom: 0;
            vertical-align: bottom;
            text-align: left;
            margin-left: 10px;
            color: #25ce0f !important;
        }
        .highlightSearched {
            background-color: #fcc33a !important;
        }
        .selecteditem{
            background-color: green;
            color:#fff;
        }
        .fa.fa-bell.notificationBell {
                width: auto !important;
                font-size: 16px !important;
                padding: 6.5px 6.5px;
            }
        @media only screen and (max-width: 540px) {
            .right-nav ul li {
                /*display: inline-table;*/
            }
        }
        @media only screen and (min-width:500px){
            .floating-chat {
                margin-right: 5px !important;
            }
        }
        @media only screen and (min-width:300px){
            .floating-chat {
                margin-right: 5px !important;
            }
        }
        @media (max-width:1199px) and (min-width:992px)
        {

            .section .setMPHSC {
                width: 100% !important;
                text-align: -webkit-center;
                margin-left:0px !important;
            }
        }

        @media (max-width:991px)
        {
            .setSectionServices{
                padding-left:3%;
            }
            footer img {
                width: 40px;
                height: 36px;
            }
            .setPeopleShortStory{
                width:100%;
                text-align: center;
            }
            .section .setMPHSC {
                width: 100% !important;
                text-align: -webkit-center;
                margin-left:0px !important;
            }
        }

        .setHPROW{
            margin-right: 0px !important;
            margin-left: 0px !important;
        }
        .setHPIMAGENL{
            width:100px;
            margin-bottom:15px;
        }
        .setHPMSHELPER{
            display:none;
        }
        .setMSCTNFOCUSC,.ms-res-ctn,.dropdown-menu{
            margin-top:2px !important;
        }
        .setHPMSERACHBOX{
            width:492px;
        }
        .setHPUBTN4{
            position: absolute;
            top: 150px;
        }
        .setHPSTYPE{
            display:none;
        }
        .setcolMDOFS1Col9ColMD10 {
            padding-left:13%;
            padding-right:13%;
        }
        .setHPDIVTOC{
            margin-top:10px !important;
        }
        .setHPFCHART{
            margin-right: 100px;
            margin-bottom: 20px;
            z-index: 99999999999;
        }
        .setFCHARTIMAGE{
            height:42px;
            width: 42px;
        }
        .setFCHARTBTN{
            margin: 0 0px -50px 250px;
            z-index: 99;
        }
        .setFCHARADIV{
            position:relative;
            bottom: 7px;
        }
        .setHPAUTHCIALINK{
            display:none;
        }
        .setHPAUTHCIALINK2{
            display:block;
        }
        .setHPDIVROWMSEARCH{
            display:none;
        }

        #msSearchBar > .ms-res-ctn.dropdown-menu{
            border-top: 0px !important;
            margin-top: -10px !important;
            border-radius: 0px !important;
            border-bottom-left-radius: 25px !important;
            border-bottom-right-radius: 25px !important;
        }

        .ms-ctn {
            background-color: #fff;
            border-radius: 25px;
            border: 0px solid #fff;
            box-shadow: 0px 0px 15px 0px #B6B3B1;
        }

        /* width */
        #msSearchBar > .ms-res-ctn.dropdown-menu::-webkit-scrollbar {
            width: 4px;
            height: 10px;
            max-height: 10px;
            min-height: 15px;
            border-radius: 20%;
        }

        /* Track */
        #msSearchBar > .ms-res-ctn.dropdown-menu::-webkit-scrollbar-track {
            height: 10px;
            max-height: 10px;
            min-height: 15px;
            border-radius: 20%;
        }

        .ms-res-ctn{
            display: none;
        }

        .displayBlock{
            display: block;
        }
        @media (max-width: 1295px) {
            .search-type-btn-default:hover, .search-type-btn-default {
                background-color: #2B3E4D;
                color: #FFF;
                border: 0px;
                font-weight: 500;
                width: 148px;
                font-size: 12px;
                padding: 5px 0px;
                border-radius: 50px;
                text-transform: capitalize;
            }
        }
        @media (max-width: 1240px) {
            .search-type-btn-default:hover, .search-type-btn-default {
                background-color: #2B3E4D;
                color: #FFF;
                border: 0px;
                font-weight: 500;
                width: 128px;
                font-size: 12px;
                padding: 5px 0px;
                border-radius: 50px;
                text-transform: capitalize;
            }
        }
        @media (max-width: 1240px) {
            .search-type-btn-default:hover, .search-type-btn-default {
                background-color: #2B3E4D;
                color: #FFF;
                border: 0px;
                font-weight: 500;
                width: 111px;
                font-size: 12px;
                padding: 5px 0px;
                border-radius: 50px;
                text-transform: capitalize;
            }
        }
    </style>
    <style>
        .lds-ring {
            display: inline-block;
            position: relative;
            width: 20px;
            height: 20px;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 20px;
            height: 20px;
            margin: 1px;
            border: 2px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
@endsection

@section('content')
    @include('popups.bindFacebook')
    {{-- for popup design --}}
    <div class="setHPUBTN4">
        {{--<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#facebookSuccessModel">Bind Facebook Popup</button><br>--}}
        {{--<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#change-status-model">Change Status Popup</button><br>--}}
        {{--<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#add-skill-model">Add Skill Popup</button><br>--}}
        {{--<button type="button" class="btn btn-danger openPP" id="openPP">Email Popup</button>--}}

    </div>

    <!-- Content -->
    <!-- Revolution slider -->
    <section id="slider">
        <div class="video-play-btn">
            <button class="btn" data-toggle="modal" data-target="#modalPlayVideo"><i class="fa fa-play"></i> {{trans('common.play_v')}}
            </button>
        </div>
        <div class="slider-logo">
            {{--<img src="{{asset('images/HomePageLogo.png')}}" alt="Logo" class="setHPIMAGENL"/>--}}
            <img src="{{asset('images/Logo_Black - 500 x 500.jpg')}}" alt="Logo" class="setHPIMAGENL"/>
            <h2 class="{{ $language == 1 ? 'big-text' : '' }}">{{ trans('home.legends_lair') }}</h2>
            <h3 class="{{ $language == 1 ? 'big-text' : '' }} setBigText">{{ trans('home.hall_of_talent') }}</h3>
        </div>

        <div class="row setHPROW">
            <div class="col-md-5 search-main setHPSMAIN">
                <form autocomplete="off" id="autoCompleteForm">
                    <input id="autocomplete" class="autoooComplete" style="border:0px;">
                </form>
                <div id="warnings"></div>


{{--                                <div id="msSearchBar">--}}
                    {{--load to slow, temporary design--}}
{{--                    <div class="ms-ctn form-control  ms-no-trigger ms-ctn-focus setMSCTNFOCUSC" id="msSearchBar">--}}
{{--                        <span class="ms-helper setHPMSHELPER"></span>--}}

{{--                        <div class="ms-sel-ctn" id="addClass">--}}
{{--                            <input class="search-box-home setHPMSERACHBOX" placeholder="{{ trans('common.search') }}"--}}
{{--                                   type="text">--}}
{{--                            <div class="setHPDIVROWMSEARCH"></div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
                <br>
                <i id="searchIc" class="fa fa-search btn-searchjqueryauto" style="cursor:pointer;"></i>
                <img id="searchIcLdr" src="{{asset('sl.gif')}}" alt="" style="width: 3%;position: absolute;top: 16px;right: 31px; display: none">
{{--                <i class="fa fa-search btn-search"></i>--}}
                <div class="row">
                    <div class="col-xs-12 col-md-12 search-type-div {{Session::get('lang')=='cn'? 'search-type-cn' : 'search-type-en' }}">
                        <div class="col-xs-4 col-md-4">
                            <button id="findProject"
                                    class="search-type-btn-default find-project-btn search-type-text pull-left target"
                                    data-search-type="find-project">
                                {{ trans('common.find_project') }}
                            </button>
                        </div>
                        <div class="col-xs-4 col-md-4">
                            <button id="findFreelancer"
                                    class="search-type-btn-default find-freelancer-btn search-type-text target"
                                    data-search-type="find-freelancer">
                                {{ trans('common.find_freelancer') }}
                            </button><!-- as -->
                        </div>
                        <div class="col-xs-4 col-md-4">
                            <button id="findCoFounder"
                                    class="search-type-btn-default find-co-founder-btn search-type-text pull-right target"
                                    data-search-type="find-co-founder">
                                {{ trans('common.find_co_founder') }}
                            </button>
                        </div>
                    </div>
                </div>

                <span id="search-type" class="setHPSTYPE"></span>
            </div>
        </div>
        <input type="hidden" id="skill_id_list" name="skill_id_list"
               value="{{ isset($filter['skill_id_list'])? $filter['skill_id_list']: '' }}">
    </section>
    <!-- /#Revolution slider -->
    <!-- Testimonial -->
    {{--<section id="testimonial" class="section testimonial">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12 setcolMDOFS1Col9ColMD10">--}}
                    {{--<h2 class="heading"> {{ trans('home.people_say') }}</h2>--}}
                    {{--<h2 class="heading" data-toggle="modal" data-target="#addSkill"> {{ trans('home.people_say') }}</h2>--}}
                    {{--<div class="modal fade add_skill_popup" id="addSkill" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">--}}
                      {{--<div class="modal-dialog">--}}
                        {{--<div class="modal-content">--}}
                          {{--<div class="modal-header">--}}
                            {{--<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>--}}
                            {{--<h4 class="modal-title" id="myModalLabel">Add Skill</h4>--}}
                          {{--</div>--}}
                          {{--<div class="addskill-content">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-sm-12">--}}
                                    {{--<div class="add_skill_field">--}}
                                        {{--<label>Skill</label>--}}
                                        {{--<select>--}}
                                            {{--<option>PHP</option>--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                    {{----}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="add_skill_footer">--}}
                                {{--<p>Didn’t found the keyword? please help us improve.</p>--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col-sm-4">--}}
                                        {{--<div class="skill-skip skill-btn">--}}
                                            {{--<a href="#" data-dismiss="modal">Skip</a>--}}
                                        {{--</div>     --}}
                                    {{--</div>--}}
                                    {{--<div class="col-sm-4">--}}
                                        {{--<div class="skill-save skill-btn">--}}
                                            {{--<a href="#">Save Changes</a>--}}
                                        {{--</div>     --}}
                                    {{--</div>--}}
                                    {{--<div class="col-sm-4">--}}
                                        {{--<div class="skill-cancel skill-btn">--}}
                                            {{--<a href="#" data-dismiss="modal">Cancel</a>--}}
                                        {{--</div>     --}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                             {{----}}
                            {{----}}
                          {{--</div>--}}
                        {{--</div>--}}
                      {{--</div>--}}
                    {{--</div>--}}
                    {{--<h2 class="heading-line"></h2>--}}

                    {{--<div class="testimonial-1 owl-carousel owl-single-all owl-pagi-1 owl-nav-1 pagi-center owl testimonial-1 mrg-top-50 setHPDIVTOC">--}}
                        {{--@if (Session::get('lang')=="en")--}}
                            {{--<div class="item">--}}
                                {{--<div class="row setColRowColMS">--}}
                                    {{--<div class="col-md-4 col-sm-6 col-xs-12 people-shortstory-img text-center setPeopleShortStory">--}}
                                        {{--<img src="/images/N-team1.png">--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-8 col-sm-6 col-xs-12 people-shortstory setPeopleShortStory">--}}
                                        {{--<h3 class="setMPPH3">{{ trans('home.people_helen') }}</h3>--}}
                                        {{--<h5 class="setMPPH3">{{ trans('home.entrepreneur') }}</h5>--}}
                                        {{--<p class="setPPPP">“{{ trans('home.people_helen_say') }}”</p>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="item">--}}
                                {{--<div class="row setColRowColMS">--}}
                                    {{--<div class="col-md-4 col-sm-6 col-xs-12 people-shortstory-img text-center setPeopleShortStory">--}}
                                        {{--<img src="/images/N-team2.png">--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-8 col-sm-6 col-xs-12 people-shortstory setPeopleShortStory">--}}
                                        {{--<h3  class="setMPPH3">{{ trans('home.people_xubing') }}</h3>--}}
                                        {{--<h5  class="setMPPH3">{{ trans('home.hr') }}</h5>--}}
                                        {{--<p class="setPPPP">“{{ trans('home.people_xubing_say') }}”</p>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="item">--}}
                                {{--<div class="row setColRowColMS">--}}
                                    {{--<div class="col-md-4 col-sm-6 col-xs-12 people-shortstory-img text-center setPeopleShortStory">--}}
                                        {{--<img src="/images/N-team3.png">--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-8 col-sm-6 col-xs-12 people-shortstory setPeopleShortStory">--}}
                                        {{--<h3  class="setMPPH3">{{ trans('home.people_khairul') }}</h3>--}}
                                        {{--<h5  class="setMPPH3">{{ trans('home.freelancer') }}</h5>--}}
                                        {{--<p class="setPPPP">“{{ trans('home.people_khairul_say') }}”</p>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--@else--}}
                            {{--<div class="item">--}}
                                {{--<div class="row setColRowColMS">--}}
                                    {{--<div class="col-md-4 col-sm-6 col-xs-12 people-shortstory-img text-center setPeopleShortStory">--}}
                                        {{--<img src="/images/N-team17.png">--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-8 col-sm-6 col-xs-12 people-shortstory setPeopleShortStory">--}}
                                        {{--<h3  class="setMPPH3 setMPPH3Chinese">{{ trans('home.people_MsWang') }}</h3>--}}
                                        {{--<h5  class="setMPPH3">{{ trans('home.entrepreneur') }}</h5>--}}
                                        {{--<p class="setPPPP">“{{ trans('home.people_MsWang_say') }}”</p>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="item">--}}
                                {{--<div class="row setColRowColMS">--}}
                                    {{--<div class="col-md-4 col-sm-6 col-xs-12 people-shortstory-img text-center setPeopleShortStory">--}}
                                        {{--<img src="/images/N-team15.png">--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-8 col-sm-6 col-xs-12 people-shortstory setPeopleShortStory">--}}
                                        {{--<h3  class="setMPPH3 setMPPH3Chinese">{{ trans('home.people_MsLi') }}</h3>--}}
                                        {{--<h5  class="setMPPH3">{{ trans('home.Technicalstaff') }}</h5>--}}
                                        {{--<p class="setPPPP">“{{ trans('home.people_MsLi_say') }}”</p>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="item">--}}
                                {{--<div class="row setColRowColMS">--}}
                                    {{--<div class="col-md-4 col-sm-6 col-xs-12 people-shortstory-img text-center setPeopleShortStory">--}}
                                        {{--<img src="/images/N-team16.png">--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-8 col-sm-6 col-xs-12 people-shortstory setPeopleShortStory">--}}
                                        {{--<h3  class="setMPPH3 setMPPH3Chinese">{{ trans('home.people_MrZhang') }}</h3>--}}
                                        {{--<h5  class="setMPPH3">{{ trans('home.entrepreneur') }}</h5>--}}
                                        {{--<p class="setPPPP">“{{ trans('home.people_MrZhang_say') }}”</p>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!--  /testimonial  -->--}}
        {{--</div>--}}
        {{--<!-- /container -->--}}
    {{--</section>--}}
    <!-- /testimonial -->

    {{--<!-- Team 1 -->--}}
    {{--<section id="team" class="section">--}}
    {{--<div class="container">--}}
    {{--<h2 class="heading">{{ trans('home.meet_our_team') }}</h2>--}}
    {{--<h2 class="heading-line"></h2>--}}
    {{--<div class="row team-main">--}}
    {{--<div class="col-md-3 team-sub text-center">--}}
    {{--<div class="team-block">--}}
    {{--<img src="/images/N-team2.png">--}}
    {{--<a href="#" class="overlay-team"><i class="fa fa-expand" aria-hidden="true"></i></a>--}}
    {{--</div>--}}
    {{--<h3>john doe</h3>--}}
    {{--<small>{{ trans('home.ui_designer') }}</small>--}}
    {{--</div>--}}
    {{--<div class="col-md-3 team-sub text-center">--}}
    {{--<div class="team-block">--}}
    {{--<img src="/images/N-team1.png">--}}
    {{--<a href="#" class="overlay-team"><i class="fa fa-expand" aria-hidden="true"></i></a>--}}
    {{--</div>--}}
    {{--<h3>Samntha smith</h3>--}}
    {{--<small>{{ trans('home.co_owner') }}</small>--}}
    {{--</div>--}}
    {{--<div class="col-md-3 team-sub text-center">--}}
    {{--<div class="team-block">--}}
    {{--<img src="/images/N-team3.png">--}}
    {{--<a href="#" class="overlay-team"><i class="fa fa-expand" aria-hidden="true"></i></a>--}}
    {{--</div>--}}
    {{--<h3>Anna Mcdoogle</h3>--}}
    {{--<small>{{ trans('home.content_writer') }}</small>--}}
    {{--</div>--}}
    {{--<div class="col-md-3 team-sub text-center">--}}
    {{--<div class="team-block">--}}
    {{--<img src="/images/N-team4.png">--}}
    {{--<a href="#" class="overlay-team"><i class="fa fa-expand" aria-hidden="true"></i></a>--}}
    {{--</div>--}}
    {{--<h3>dan wood</h3>--}}
    {{--<small>{{ trans('home.ui_designer') }}</small>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<!-- /container -->--}}
    {{--</section>--}}
    {{--<!-- /Team 1-->--}}

    {{--<!-- About -->--}}
    {{--<section id="about" class="section">--}}
    {{--<div class="container">--}}
    {{--<h2 class="heading">{{ trans('home.about_us') }}</h2>--}}
    {{--<h2 class="heading-line"></h2>--}}
    {{--<div class="row about-main">--}}
    {{--<div class="col-md-12 about-text">--}}
    {{--<p> {{ strip_tags(trans('home.team_desc'), '<br/>') }} </p>--}}
    {{--</div>--}}
    {{--<div class="col-md-3 about-sub text-center">--}}

    {{--<a href="{{ URL::route('frontend.meet-the-team2') }}">--}}
    {{--<div class="row our-teams">--}}
    {{--<div class="col-md-4 col-xs-12 col-sm-4 team-img">--}}
    {{--<div class="img">--}}
    {{--<img src="/images/fong.jpg" style="margin: 10px 0px 0px 6px;">--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<h3>{{ trans('home.team_fong') }}</h3>--}}
    {{--<small>{{ trans('home.team_CEO') }}</small>--}}
    {{--</a>--}}

    {{--<br>--}}
    {{--<a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>--}}
    {{--<a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a>--}}
    {{--<a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>--}}
    {{--</div>--}}
    {{--<div class="col-md-3 about-sub text-center">--}}
    {{--<a href="{{ URL::route('frontend.meet-the-team2') }}">--}}
    {{--<div class="row our-teams">--}}
    {{--<div class="col-md-4 col-xs-12 col-sm-4 team-img">--}}
    {{--<div class="img">--}}
    {{--<img src="/images/marsel.jpg" style="margin:25px 0px 0px -8px;">--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<h3>{{ trans('home.team_Xenren') }}</h3>--}}
    {{--<small>{{ trans('home.team_CTO') }}</small>--}}
    {{--</a>a--}}
    {{--<br>--}}
    {{--<a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>--}}
    {{--<a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a>--}}
    {{--<a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>--}}
    {{--</div>--}}
    {{--<div class="col-md-3 about-sub text-center">--}}
    {{--<a href="{{ URL::route('frontend.meet-the-team2') }}">--}}
    {{--<div class="row our-teams">--}}
    {{--<div class="col-md-4 col-xs-12 col-sm-4 team-img">--}}
    {{--<div class="img">--}}
    {{--<img src="/images/hasper.jpg" style="margin:20px 0px 0px 0px;">--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<h3>{{ trans('home.team_chenlou') }}</h3>--}}
    {{--<small>{{ trans('home.team_CPO') }}</small>--}}
    {{--</a>--}}
    {{--<br>--}}
    {{--<a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>--}}
    {{--<a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a>--}}
    {{--<a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>--}}
    {{--</div>--}}
    {{--<div class="col-md-3 about-sub text-center">--}}
    {{--<a href="{{ URL::route('frontend.meet-the-team2') }}">--}}
    {{--<div class="row our-teams">--}}
    {{--<div class="col-md-4 col-xs-12 col-sm-4 team-img">--}}
    {{--<div class="img">--}}
    {{--<img src="/images/niew.jpg" style="margin:10px 0px 0px -8px;">--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<h3>{{ trans('home.team_chenkun') }}</h3>--}}
    {{--<small>{{ trans('home.team_CCO') }}</small>--}}
    {{--</a>--}}
    {{--<br>--}}
    {{--<a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>--}}
    {{--<a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a>--}}
    {{--<a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<!-- /container -->--}}
    {{--</section>--}}
    {{--<!-- /About-->--}}

    <!-- About -->
    <section id="services" class="section setSectionServices">
        <div class="container setMPHSC">
            <h2 class="heading">{{ trans('home.services_security') }}</h2>
            <h2 class="heading-line"></h2>
            <div class="row services-main">
                <div class="col-md-6">
                    <div class="row home-services-main">
                        <div class="col-md-3 home-services-main-l">
                            <a href="{{ URL::route('frontend.about-us' , \Session::get('lang')) }}">
                                <div class="home-services-main-l-icon">
                                    <img src="/images/N-icon1.png" data-src="money" alt="money guarantee"/>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-9 home-services-main-r">
                            <a href="{{ URL::route('frontend.about-us' , \Session::get('lang')) }}">
                                {{ trans('home.money_guarantee') }}
                            </a>
                            <p>
                                {{ trans('home.money_guarantee_text') }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row home-services-main">
                        <div class="col-md-3 home-services-main-l">
                            <a href="{{ URL::route('frontend.about-us' , \Session::get('lang')) }}">
                                <div class="home-services-main-l-icon">
                                    <img src="/images/N-icon4.png" data-src="money" alt="money guarantee"/>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-9 home-services-main-r">
                            <a href="{{ URL::route('frontend.about-us' , \Session::get('lang')) }}">
                                {{ trans('home.skill_verify') }}
                            </a>
                            <p>
                                {{ trans('home.skill_verify_text') }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row services-main">
                <div class="col-md-6">
                    <div class="row home-services-main">
                        <div class="col-md-3 home-services-main-l">
                            <a href="{{ URL::route('frontend.about-us' , \Session::get('lang')) }}">
                                <div class="home-services-main-l-icon">
                                    <img src="/images/N-icon3.png" data-src="money" alt="image update"
                                         rel="stylesheet" />
                                </div>
                            </a>
                        </div>
                        <div class="col-md-9 home-services-main-r">
                            <a href="{{ URL::route('frontend.about-us' , \Session::get('lang')) }}">
                                {{ trans('home.tech_support') }}
                            </a>
                            <p>
                                {{ trans('home.tech_support_text') }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row home-services-main">
                        <div class="col-md-3 home-services-main-l">
                            <a href="{{ URL::route('frontend.about-us' , \Session::get('lang')) }}">
                                <div class="home-services-main-l-icon">
                                    <img src="/images/N-icon2.png" data-src="money" alt="money guarantee"/>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-9 home-services-main-r">
                            <a href="{{ URL::route('frontend.about-us' , \Session::get('lang')) }}">
                                {{ trans('home.customer_service') }}
                            </a>
                            <p>
                                {{ trans('home.customer_service_text') }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row services-main">
                <div class="col-md-6">
                    <div class="row home-services-main">
                        <div class="col-md-3 home-services-main-l">
                            <a href="{{ URL::route('frontend.about-us' , \Session::get('lang')) }}">
                                <div class="home-services-main-l-icon">
                                    <img src="/images/N-icon5.png" data-src="money" alt="money guarantee"/>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-9 home-services-main-r">
                            <a href="{{ URL::route('frontend.about-us' , \Session::get('lang')) }}">
                                {{ trans('home.skillfull_person') }}
                            </a>
                            <p>
                                {{ trans('home.skillfull_person_text') }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row home-services-main">
                        <div class="col-md-3 home-services-main-l">
                            <div class="home-services-main-l-icon order">
                                <a href="{{ URL::route('login',\Session::get('lang')) }}"
                                   class="centering pagination-centered">{!! trans('home.get_started') !!} <br> <i
                                            class="fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                        <div class="col-md-9 home-services-main-r">
                            <p>
                                {!! trans('home.team_desc') !!}
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>



        <!-- /container -->

        <!---chatbot--->

        <div class="floating-chat setHPFCHART">
            <img src="{{asset('images/loginImage/chat_bot_logo.jpg')}}"
                 class="setFCHARTIMAGE"
                 aria-hidden="true" />
            <div class="chat">
                <div class="header">

                    <button onclick="close()" class="setFCHARTBTN">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </button>

                </div>

                <div class="setFCHARADIV">
                    <iframe
                            allow="microphone;"
                            width="280"
                            height="470"
                            src="https://console.dialogflow.com/api-client/demo/embedded/e7b66656-2c08-43b4-8425-e4b6ad2804d3">
                    </iframe>
                </div>

            </div>
        </div>
        <!----end chatbot----->


    </section>
    <!-- /About-->

    <input id="user-id" type="hidden" value="{{ isset($_G['user']->id)? $_G['user']->id : '' }}">
    <input id="user-status" type="hidden"
           value="{{ isset($_G['user']) && $_G['user']->getStatusIds() != "" ? $_G['user']->getStatusIds() : '' }}">

    @include('frontend.userCenter.modalChangeUserStatus')
    @include('frontend.includes.modalShowContactInfoModal',[
        'user' => $_G['user']
    ])
    @include('frontend.includes.modalAddNewSkillKeywordv2', [
        'tagColor' => \App\Models\Skill::TAG_COLOR_WHITE
    ])

    @include('frontend.includes.modalAddNewSkillKeywordv2', [
        'tagColor' => \App\Models\Skill::TAG_COLOR_GOLD
    ])

    @if( \Auth::check())
        <input id="username" type="hidden" value="{{ isset($_G['user']->name)? $_G['user']->name : '' }}">
        {{--<a id="editUsername"--}}
           {{--href="{{ route('frontend.modal.setusername', ['user_id' => $_G['user']->id ] ) }}"--}}
           {{--data-toggle="modal"--}}
           {{--data-target="#modalSetUsername"--}}
           {{--class="setHPAUTHCIALINK">--}}
        {{--</a>--}}

        <input id="isContactInfoExist" type="hidden"
               value="{{ $_G['user']->isContactInfoExist() == true? 'true' : 'false' }}">
        <a id="editContactInfo"
           href="{{ route('frontend.modal.setcontactinfo', ['user_id' => $_G['user']->id ] ) }}"
           data-toggle="modal"
           data-target="#modalSetContactInfo"
           class="setHPAUTHCIALINK2">
        </a>

        <input id="isFirstTimeSkillSubmit" type="hidden"
               value="{{ isset($_G['user']->is_first_time_skill_submit)? $_G['user']->is_first_time_skill_submit : '0' }}">
        @include('frontend.userCenter.modalChangeUserSkill', [ 'isSkipButtonAllow' => true ,'skillList' => $skillList ])
    @endif

@endsection

@section('modal')




    {{--<div id="modalSetUsername" class="modal fade" role="dialog">--}}
        {{--<div class="modal-dialog">--}}
            {{--<div class="modal-content">--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

{{--    <div id="modalSetContactInfo" class="modal fade" role="dialog">--}}
{{--        <div class="modal-dialog">--}}
{{--            <div class="modal-content setModalDailogeCI">--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    {{-- Video Modal --}}
    <div id="modalPlayVideo" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <video width="100%" height="100%" controls autoplay id="homePageVideo">
                                    <source src="{{ asset('video/xenren_home.mp4') }}" type="video/mp4">
                                    Your browser does not support the video tag.
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- SELECT2 Script for city multi select in facebook binding modal --}}

    <style>
        .select2-container {
            width: 66% !important;
            text-align: left !important;
            border: 1px solid #ddd !important;
            padding: 5px !important;
            border-radius: 5px !important;
            font-size: 12px !important;
            float: left !important;
            margin-bottom: 15px !important;
            height: 47px !important;
        }
    </style>

    <script defer="defer">
                @if(Session::has('status'))
        let type = "{{Session::get('status')}}";
        alertSuccess(type);
        @endif
                        @if(Session::has('verified'))
                let type = "{{Session::get('verified')}}";
                alertSuccess(type);
        @endif
    </script>
    <script defer="defer">
        console.log('duffer');

        function searchAutocomplete() {
            $('#searchIc').hide();
            $('#searchIcLdr').show();

            console.log("check idd is on");
            var idD = [];
            window.skillIdData.map(function (d) {
                idD.push(d.id);
            });
            var skillIdList = idD.join();
            console.log(skillIdList);
            console.log('skillIdList');
            if( skillIdList == null )
            {
                var postVal = window.skillIdData[0].id;
                console.log('postVal');
                console.log(postVal);
                console.log('postVal');
                $('.autoooComplete').attr('data-skill-id-list', postVal);
                skillIdList = $('.autoooComplete').attr('data-skill-id-list')
            }

            var searchType = window.localStorage.getItem('selectBtn');
            // if( searchType == null )
            // {
            //     $('#search-type').show();
            //     $('#search-type').text(lang.please_select_freelancer_project_cofounder);
            //
            //     // alertError(lang.please_choose_search_type);
            //     return true;
            // }
            // else
            // {
            $('#search-type').text("");
            $('#search-type').hide();
            // }

            if( searchType == 'find-project' )
            {
                window.location.href = url.jobs + '?project_type=1&order=1&type=1&skill_id_list=' + skillIdList;
                return false;
            }
            else if( searchType == 'find-freelancer' )
            {
                window.location.href = url.findExperts + '?skill_id_list=' + skillIdList;
                return false;
            }
            else if( searchType == 'find-co-founder' )
            {
                window.location.href = url.findExperts + '?skill_id_list=' + skillIdList + '&status_id=1';
                return false;
            }
            else if(searchType == null) {
                window.location.href = url.findExperts + '?skill_id_list=' + skillIdList;
                return false;
            }
        }

        $('.autoooComplete').on('keypress', function (e) {
            if(e.which == 13) {
                searchAutocomplete()
            }
        })

        $('#autoCompleteForm').submit(function(e) {
            e.preventDefault()
            searchAutocomplete()
        });

        window.cityData = null;
        window.city2Select = null;
        window.localStorage.selectBtn =  'find-project'
        function getLocation(callBack) {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(callBack);
            } else {
                console.log("Geolocation is not supported by this browser.");
            }
        }

        function showCityInFacebookBinding(position) {
            var url = "https://nominatim.openstreetmap.org/reverse?format=json&lat=" + position.coords.latitude + "&lon=" + position.coords.longitude + "&addressdetails=1";
            return $.ajax({
                url: url,
                dataType: 'jsonp',
                jsonp: 'json_callback',
                cache: true,
                success: function (res) {
                    console.log(res);
                    var city = res.address.city;
                    console.log(city);
                    window.selectedObject = null;

                    for (var a = 0; a < window.cityData.results.length; a++) {
                        if (window.cityData.results[a].text.indexOf(city) !== -1) {
                            console.log(window.cityData.results[a]);
                            window.selectedObject = window.cityData.results[a];
                            break;
                        }
                    }

                    if (window.selectedObject != null) {
                        $("#js-example-data-array").select2("trigger", "select", {
                            data: window.selectedObject
                        });
                    }
                },
                error: function (err) {
                    console.log(err);
                }
            });
        }

        $(function () {
            $.ajax({
                url: '/fetchCity',
                dataType: 'json',
                success: function (data) {
                    // console.log(data);
                    window.cityData = data;
                }
            });

            setTimeout(function () {
                window.city2Select = $(".js-example-data-array").select2({
                    minimumInputLength: 3,
                    language: {
                        inputTooShort: function () {
                            return '{{trans('member.please_enter_3_or_more_characters')}}';
                        }
                    },
                    placeholder: '{{trans('member.choose_location')}}',
                    ajax: {
                        url: '/fetchCity',
                        dataType: 'json',
                        processResults: function (data) {
                            console.log(data);
                            window.cityData = data;
                            // Tranforms the top-level key of the response object from 'items' to 'results'
                            return data;
                        }
                    }
                });
            }, 2000);
        });
    </script>
    {{-- /SELECT2 Script for city multi select in facebook binding modal --}}

    @if(Auth::user())
        @include('popups.SetPassword')
    @endif
@endsection

@section('footer')
    <script>
        $('#bindFaceBook').click(function () {
            $('.idSpanBindFaceBook').hide();
            $('.lds-ring').show();
        });
    </script>
    <!-- settings area -->
    <script defer="defer">
        var user_ui_preference = '{{\Auth::user() ? \Auth::user()->uipreference: false }}';
        var user_ui_preference_both = '{{\App\Models\User::USER_UI_PREFERENCE_BOTH}}';
        var user_ui_preference_freelancer = '{{\App\Models\User::USER_UI_PREFERENCE_FREELANCER}}';
        var user_ui_preference_employer = '{{\App\Models\User::USER_UI_PREFERENCE_EMPLOYER}}';
        var user_email_notification = '{{\Auth::user() ? \Auth::user()->email_notification: false }}';
        var user_wechat_notification = '{{\Auth::user() ? \Auth::user()->wechat_notification: false }}';
        var user_push_notification = '{{\Auth::user() ? \Auth::user()->push_notification: false }}';
        var user_close_account = '{{\Auth::user() ? \Auth::user()->close_account: false }}';
        var push_notification = '{{\App\Models\User::APP_PUSH_NOTIFICATION}}';
        var close_account = '{{\App\Models\User::CLOSE_MY_ACCOUNT}}';
        var email_notification = '{{\App\Models\User::USER_EMAIL_NOTIFICATION}}';
        var wechat_notification = '{{\App\Models\User::USER_WECHAT_NOTIFICATION}}';
        var checked_in_space = '{{\App\Models\User::CHECKED_IN_CO_WORK_SPACE}}';
        var checked_in_COWORK = '{{\Auth::user() ? \Auth::user()->checked_in_cowork_space : false}}';
    </script>
    <script type="text/javascript" src="/custom/js/frontend/settings.js" defer="defer"></script>
    <!-- settings area -->

    {{--Dialog Flow Chat Bot--}}
{{--    <script src="{{asset('assets/js/chatbot.js')}}"></script>--}}

    <script src="{{asset('custom/js/contactInfo.js')}}" defer="defer"></script>

    <script defer="defer">
        $(function () {
            $('#modalPlayVideo').on('hidden.bs.modal', function () {
                var video = document.getElementById('homePageVideo');
                video.pause();
            });
            $('#modalPlayVideo').on('shown.bs.modal', function () {
                var video = document.getElementById('homePageVideo');
                video.play();
            });
        });
    </script>


    <script type="text/javascript" defer="defer">
        var lang = {
            error: '{{trans('common.unknown_error')}}',
            please_choose_search_type: "{{ trans('common.please_choose_search_type') }}",
            please_select_skills: "{{ trans('common.please_select_skills') }}",
            help_us_improve_skill_keyword: "{{ trans('common.help_us_improve_skill_keyword') }}",
            unable_to_request: "{{ trans('common.unable_to_request') }}",
            please_select_freelancer_project_cofounder: "{{ trans('common.please_select_freelancer_project_cofounder') }}",
            freelancer_require_what_skill: "{{ trans('common.freelancer_require_what_skill') }}",
            what_skill_you_have: "{{ trans('common.what_skill_you_have') }}",
            co_founder_require_what_skill: "{{ trans('common.co_founder_require_what_skill') }}",
            new_skill_keywords: "{{ trans('common.new_skill_keywords') }}",
            please_use_wiki_or_baidu_support_your_keyword: "{{ trans('common.please_use_wiki_or_baidu_support_your_keyword') }}",
            one_of_skill_input_empty: "{{ trans('common.one_of_skill_input_empty') }}",
            maximum_ten_skill_keywords_allow_to_submit: "{{ trans('common.maximum_ten_skill_keywords_allow_to_submit') }}",
        }

        var url = {
            jobs: '{{ route('frontend.jobs.index') }}',
            findExperts: '{{ route('frontend.findexperts', \Session::get("lang")) }}',
            login: '{{ route('login') }}',
            api_skipAddUserSkill: '{{ route('api.skipadduserskill') }}',
            personalInformationChange: '{{ route('frontend.personalinformation.change') }}',
            {{--weixinweb: '{{ route('weixin.login') }}'--}}
        }

        var skillList = {!! $skillList !!};
        var skill_id_list = '{{ isset($_G['user'])? $_G['user']->getUserSkillList(): '' }}';

        var is_guest = {{ Auth::guard('users')->guest() == 1? 'true': 'false' }};

        function checkFirstTimeFilling(userId) {
            var userStatus = $('#user-status').val();
            var username = $('#username').val();
            var isFirstTimeSkillSubmit = $('#isFirstTimeSkillSubmit').val();
            var isContactInfoExist = $('#isContactInfoExist').val();

            if (userId > 0 && userStatus == '0' || userId > 0 && userStatus == '') {
                $('#modalChangeUserStatus').modal('show');
            }
            else if (userId > 0 && username == '') {
                $('#editUsername').trigger('click');
            }
            else if (userId > 0 && isFirstTimeSkillSubmit == 0) {
                $('#modalChangeUserSkill').modal('show');
            }
            else if (userId > 0 && isContactInfoExist == 'false') {
                $('#modalSetContactInfo').modal('show');
            }
        }

        window.onload = function () {
            if (isWeiXin() && is_guest == true) {
//                alert("weixin browser");
//                 window.location.href = url.weixinweb;
            }
//            else
//            {
//                alert("normal browser");
//            }
        }

        function isWeiXin() {
            var ua = window.navigator.userAgent.toLowerCase();
            if (ua.match(/MicroMessenger/i) == 'micromessenger') {
                return true;
            }
            else {
                return false;
            }
        }

    </script>
    <script>
        new Vue({
            el: ".homeXenren",
            data: {
                skills: [],
                addSkillLoader:false,
            },
            methods: {
                changeSkill: function (val) {
                    this.addSkillLoader = true;
                    var input = {
                        'val': $('#skill_id_list').val(),
                        'column': val,
                    };
                    this.$http.post('/personalInformation/change', input).then((response)=> {
                        window.location.reload();
                    }, (response)=> {
                        console.log('error');
                    });
                },
            },
            mounted()
            {
                console.log('home-xenren-new-blade.php');
                console.log(skillList);
            }
        });
    </script>
    <script src="{{asset('js/select2.min.js')}}" defer="defer"></script>
    <script src="/custom/js/frontend/home.js" type="text/javascript" defer="defer"></script>
{{--    <script src="/custom/js/frontend/modalChangeUserStatus.js" type="text/javascript"></script>--}}
    {{--<script src="/custom/js/frontend/modalAddNewSkillKeyword.js" type="text/javascript"></script>--}}
    <script src="/custom/js/frontend/modalAddUserSkill.js" type="text/javascript" defer="defer"></script>
    <script src="/custom/js/frontend/modalAddNewSkillKeywordv2.js" type="text/javascript" defer="defer"></script>
    <script src="/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript" defer="defer"></script>
    @if(\Auth::user())
        <script>
            +function () {
                $(document).ready(function () {


                    $('#tbxSkypeId').blur(function(e){
                        e.preventDefault();
                        var formData = $('#set-contact-info-form').serialize();
                        var action = '{{route('frontend.modal.setcontactinfo.post')}}';

                        $.ajax({
                            url:action,
                            type:'POST',
                            data:formData,
                            success: function(data){
                                console.log(data);
                                alertSuccess("skype id save successfully");
                            }
                        });
                    });


                    $('#tbxWechatId').blur(function(e){
                        e.preventDefault();
                        var formData = $('#set-contact-info-form').serialize();
                        var action = '{{route('frontend.modal.setcontactinfo.post')}}';

                        $.ajax({
                            url:action,
                            type:'POST',
                            data:formData,
                            success: function(data){
                                console.log(data);
                                alertSuccess("wechat id save successfully");
                            }
                        });
                    });


                    $('#tbxlineId').blur(function(e){
                        e.preventDefault();
                        var formData = $('#set-contact-info-form').serialize();
                        var action = '{{route('frontend.modal.setcontactinfo.post')}}';

                        $.ajax({
                            url:action,
                            type:'POST',
                            data:formData,
                            success: function(data){
                                console.log(data);
                                alertSuccess("line id save successfully");
                            }
                        });
                    });

                    $('#tbxHandphoneNo').blur(function(e){
                        e.preventDefault();
                        var formData = $('#set-contact-info-form').serialize();
                        var action = '{{route('frontend.modal.setcontactinfo.post')}}';

                        $.ajax({
                            url:action,
                            type:'POST',
                            data:formData,
                            success: function(data){
                                console.log(data);
                                alertSuccess("phone no save successfully");
                            }
                        });
                    });


                    // $('#set-contact-info-form').makeAjaxForm({
                    //     inModal: true,
                    //     closeModal: true,
                    //     submitBtn: '#btn-submit',
                    //     alertContainer : '#model-alert-container',
                    //     afterSuccessFunction: function (response, $el, $this) {
                    //         if( response.status == 'OK'){
                    //             $('#modalSetContactInfo').modal('toggle');
                    //             $('#isContactInfoExist').val('true');
                    //             checkFirstTimeFilling(response.user_id);
                    //         }else{
                    //             App.alert({
                    //                 type: 'danger',
                    //                 icon: 'warning',
                    //                 message: response.message,
                    //                 container: '#model-alert-container',
                    //                 reset: true,
                    //                 focus: true
                    //             });
                    //         }
                    //
                    //         App.unblockUI('#set-contact-info-form');
                    //     }
                    // });

                    $('#ckbIsContactAllowView').bootstrapSwitch();

                    $('#ckbIsContactAllowView').on('switchChange.bootstrapSwitch', function (event, state) {
                        //true = show, false = hide
                        if (state == true)
                        {
                            $('#is_contact_allow_view').val('1');
                        }
                        else
                        {
                            $('#is_contact_allow_view').val('0');
                        }
                    });
                });
            }(jQuery);
        </script>

        <script defer="defer">

            $(function () {
                var password = '{{\Auth::user()->password}}';
                var facebookId = '{{Auth::user()->facebook_id}}';
                if (!password) {
                    $('.modal').modal('hide');
//	                $('#modalChangeUserStatus').modal('hide');
                    $('#emailModel').modal({
//                        backdrop: 'static',
//                        keyboard: false,
                        show: true
                    });
                }
                if (!facebookId) {
                    $('.modal').modal('hide');
//	                $('#modalChangeUserStatus').modal('hide');
                    $('#facebookModal').modal({
//		                backdrop: 'static',
//		                keyboard: false,
                        show: true
                    });
                }

                $('.confirmPassword').on('click', function () {
                    if ($('#pwd').val() == $('#pwd1').val()) {
                        var settings = {
                            "url": "updatePassword",
                            "method": "POST",
                            "data": {
                                "password": $('#pwd').val()
                            }
                        };
                        $.ajax(settings).done(function (response) {
                            console.log(response);
                            $('#emailModel').modal('hide');
                            $('#modalChangeUserStatus').modal('show');
                        });
                    } else {
                        $('#exception').show();
                    }
                });



            });
        </script>
    @endif
@endsection
