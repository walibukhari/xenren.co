@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.find_experts') }}
@endsection

@section('description')
    {{ trans('common.find_experts') }}
@endsection

@section('author')
    Xenren
@endsection

@section('header')
    <link href="/assets/global/plugins/bootstrap-star-rating/css/star-rating.css" media="all" rel="stylesheet" type="text/css"/>
    <link href="/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/findExperts.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/findExpertsNew.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/select2/css/select2.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/custom/css/frontend/modalInviteToWork.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/modalSendOffer.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/findExpertsSearch.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE BASE CONTENT -->
            <!-- Start New design -->
            <div class="portlet find-expert-box find-expert-box-new">
                <div class="row personal-profile">
                    <div class="profile-image col-md-2">
                        <div class="profile-userpic p-t-5 text-center">
                            <a href="">
                            <img src="/images/Young_man_01.jpg" class="img-responsive img-circle" alt=""/>
                                    <i class="fa fa-check"></i>
                            </a>
                        </div>
                        <div class="col-md-12 latest-login">
                            <span class="text-capitalize find-expert-text">Last Login Time: </span>
                            <span class="find-expert-text-two">
                                18-12-2016 13:20am
                            </span>
                        </div>
                    </div>
                    <div class="user-info col-md-7">
                        <div class="row basic-info">
                            <div class="col-md-6 name-main">
                                <span class="find-expert-name">
                                Adam John
                                </span><br>
                            </div>
                            <div class="col-md-6 country-main">
                                <span class="find-expert-country">
                                Country 
                                </span>
                                <img src="/images/Flags-Icon-Set/24x24/CN.jpg">
                            </div>
                            <div class="col-md-12">
                                <span class="status">
                                LOOKING FOR PROJECT
                                </span>
                            </div>
                            <div class="col-md-12">    
                                <p class="description">
                                I have excellent experience in web designing, web developing, wordpress <a href="">Read More...</a>
                                </p>
                            </div>
                            <div class="col-md-12">
                                <div class="skill-div">
                                <span class="item-skill official"> <img src="/images/crown.png" alt="" height="24px" width="24px"> <span>PHP</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill success"> <img src="/images/checked.png" alt="" height="24px" width="24px"> <span>JavaScript</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill"><span>HTML5</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill"><span>Adobe ilusstrator</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill"><span>Adobe Photoshop</span></span>
                                </div>
                            </div>    
                        </div>
                    </div>
                    <div class="user-info col-md-3">
                        <div class="row details-container">
                            <div class="col-md-12 per-hr">
                                <i class="fa fa-heart pull-right"></i>
                                <h3>$15.00 /hr</h3>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>

            <div class="portlet find-expert-box find-expert-box-new">
                <div class="row personal-profile">
                    <div class="profile-image col-md-2">
                        <div class="profile-userpic p-t-5 text-center">
                            <a href="">
                            <img src="/images/User-Avatar1-.jpg" class="img-responsive img-circle" alt=""/>
                                    <i class="fa fa-exclamation gray-i"></i>
                            </a>
                        </div>
                        <div class="col-md-12 latest-login">
                            <span class="text-capitalize find-expert-text">Last Login Time: </span>
                            <span class="find-expert-text-two">
                                18-12-2016 13:20am
                            </span>
                        </div>
                    </div>
                    <div class="user-info col-md-7">
                        <div class="row basic-info">
                            <div class="col-md-6 name-main">
                                <span class="find-expert-name">
                                Adam Johnson
                                </span><br>
                            </div>
                            <div class="col-md-6 country-main">
                                <span class="find-expert-country">
                                Country 
                                </span>
                                <img src="/images/Flags-Icon-Set/24x24/CN.jpg">
                            </div>
                            <div class="col-md-12">
                                <span class="status">
                                LOOKING FOR PROJECT
                                </span>
                            </div>
                            <div class="col-md-12">    
                                <p class="description">
                                I have excellent experience in web designing, web developing, wordpress <a href="">Read More...</a>
                                </p>
                            </div>
                            <div class="col-md-12">
                                <div class="skill-div">
                                <span class="item-skill official"> <img src="/images/crown.png" alt="" height="24px" width="24px"> <span>PHP</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill success"> <img src="/images/checked.png" alt="" height="24px" width="24px"> <span>JavaScript</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill"><span>HTML5</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill"><span>Adobe ilusstrator</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill"><span>Adobe Photoshop</span></span>
                                </div>
                            </div>    
                        </div>
                    </div>
                    <div class="user-info col-md-3">
                        <div class="row details-container">
                            <div class="col-md-12 per-hr">
                                <i class="fa fa-heart heart-red pull-right"></i>
                                <h3>$15.00 /hr</h3>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>

            <div class="portlet find-expert-box find-expert-box-new">
                <div class="row personal-profile">
                    <div class="profile-image col-md-2">
                        <div class="profile-userpic p-t-5 text-center">
                            <a href="">
                            <img src="/images/User-Avatar.jpg" class="img-responsive img-circle" alt=""/>
                                    <i class="fa fa-check"></i>
                            </a>
                        </div>
                        <div class="col-md-12 latest-login">
                            <span class="text-capitalize find-expert-text">Last Login Time: </span>
                            <span class="find-expert-text-two">
                                18-12-2016 13:20am
                            </span>
                        </div>
                    </div>
                    <div class="user-info col-md-7">
                        <div class="row basic-info">
                            <div class="col-md-6 name-main">
                                <span class="find-expert-name">
                                Jessica Kardashan
                                </span><br>
                            </div>
                            <div class="col-md-6 country-main">
                                <span class="find-expert-country">
                                Country 
                                </span>
                                <img src="/images/Flags-Icon-Set/24x24/CN.jpg">
                            </div>
                            <div class="col-md-12">
                                <span class="status">
                                LOOKING FOR PROJECT
                                </span>
                            </div>
                            <div class="col-md-12">    
                                <p class="description">
                                I have excellent experience in web designing, web developing, wordpress <a href="">Read More...</a>
                                </p>
                            </div>
                            <div class="col-md-12">
                                <div class="skill-div">
                                <span class="item-skill official"> <img src="/images/crown.png" alt="" height="24px" width="24px"> <span>PHP</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill success"> <img src="/images/checked.png" alt="" height="24px" width="24px"> <span>JavaScript</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill"><span>HTML5</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill"><span>Adobe ilusstrator</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill"><span>Adobe Photoshop</span></span>
                                </div>
                            </div>    
                        </div>
                    </div>
                    <div class="user-info col-md-3">
                        <div class="row details-container">
                            <div class="col-md-12 per-hr">
                                <i class="fa fa-heart pull-right"></i>
                                <h3>$15.00 /hr</h3>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>

            <div class="portlet find-expert-box find-expert-box-new">
                <div class="row personal-profile">
                    <div class="profile-image col-md-2">
                        <div class="profile-userpic p-t-5 text-center">
                            <a href="">
                            <img src="/images/Young_man_01.jpg" class="img-responsive img-circle" alt=""/>
                                    <i class="fa fa-check"></i>
                            </a>
                        </div>
                        <div class="col-md-12 latest-login">
                            <span class="text-capitalize find-expert-text">Last Login Time: </span>
                            <span class="find-expert-text-two">
                                18-12-2016 13:20am
                            </span>
                        </div>
                    </div>
                    <div class="user-info col-md-7">
                        <div class="row basic-info">
                            <div class="col-md-6 name-main">
                                <span class="find-expert-name">
                                Willson Perl
                                </span><br>
                            </div>
                            <div class="col-md-6 country-main">
                                <span class="find-expert-country">
                                Country 
                                </span>
                                <img src="/images/Flags-Icon-Set/24x24/US.png">
                            </div>
                            <div class="col-md-12">
                                <span class="status">
                                LOOKING FOR PROJECT
                                </span>
                            </div>
                            <div class="col-md-12">    
                                <p class="description">
                                I have excellent experience in web designing, web developing, wordpress <a href="">Read More...</a>
                                </p>
                            </div>
                            <div class="col-md-12">
                                <div class="skill-div">
                                <span class="item-skill official"> <img src="/images/crown.png" alt="" height="24px" width="24px"> <span>PHP</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill success"> <img src="/images/checked.png" alt="" height="24px" width="24px"> <span>JavaScript</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill"><span>HTML5</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill"><span>Adobe ilusstrator</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill"><span>Adobe Photoshop</span></span>
                                </div>
                            </div>    
                        </div>
                    </div>
                    <div class="user-info col-md-3">
                        <div class="row details-container">
                            <div class="col-md-12 per-hr">
                                <i class="fa fa-heart pull-right"></i>
                                <h3>$15.00 / hr</h3>
                            </div>
                        </div>
                        <button type="button" class="btn btn-success find-experts-success pull-right">
                        invite for work
                        </button>
                        <button type="button" class="btn find-experts-offer pull-right">
                        Send Offer
                        </button>
                        <button type="button" class="btn find-experts-offer find-experts-request pull-right">
                        <i class="fa fa-envelope"></i>REQUEST
                        </button> 
                    </div>
                </div>
            </div>

            <div class="portlet find-expert-box find-expert-box-new">
                <div class="row personal-profile">
                    <div class="profile-image col-md-2">
                        <div class="profile-userpic p-t-5 text-center">
                            <a href="">
                            <img src="/images/Young_man_01.jpg" class="img-responsive img-circle" alt=""/>
                                    <i class="fa fa-check"></i>
                            </a>
                        </div>
                        <div class="col-md-12 latest-login">
                            <span class="text-capitalize find-expert-text">Last Login Time: </span>
                            <span class="find-expert-text-two">
                                18-12-2016 13:20am
                            </span>
                        </div>
                    </div>
                    <div class="user-info col-md-7">
                        <div class="row basic-info">
                            <div class="col-md-6 name-main">
                                <span class="find-expert-name">
                                Willson Perl
                                </span><br>
                            </div>
                            <div class="col-md-6 country-main">
                                <span class="find-expert-country">
                                Country 
                                </span>
                                <img src="/images/Flags-Icon-Set/24x24/US.png">
                            </div>
                            <div class="col-md-12">
                                <span class="status">
                                LOOKING FOR PROJECT
                                </span>
                            </div>
                            <div class="col-md-12">    
                                <p class="description">
                                I have excellent experience in web designing, web developing, wordpress <a href="">Read More...</a>
                                </p>
                            </div>
                            <div class="col-md-12">
                                <div class="skill-div">
                                <span class="item-skill official"> <img src="/images/crown.png" alt="" height="24px" width="24px"> <span>PHP</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill success"> <img src="/images/checked.png" alt="" height="24px" width="24px"> <span>JavaScript</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill"><span>HTML5</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill"><span>Adobe ilusstrator</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill"><span>Adobe Photoshop</span></span>
                                </div>
                            </div>    
                        </div>
                    </div>
                    <div class="user-info col-md-3">
                        <div class="row details-container">
                            <div class="col-md-12 per-hr">
                                <i class="fa fa-heart pull-right"></i>
                                <h3>$15.00 / hr</h3>
                            </div>
                        </div>
                        <button type="button" class="btn btn-success find-experts-success pull-right">
                        invite for work
                        </button>
                        <button type="button" class="btn find-experts-offer pull-right">
                        Send Offer
                        </button>
                        <button type="button" class="btn find-experts-offer find-experts-check pull-right">
                        <i class="fa fa-envelope"></i>CHECK
                        </button> 
                    </div>
                </div>
            </div>

            <div class="portlet find-expert-box find-expert-box-new">
                <div class="row personal-profile">
                    <div class="profile-image col-md-2">
                        <div class="profile-userpic p-t-5 text-center">
                            <a href="">
                            <img src="/images/Young_man_01.jpg" class="img-responsive img-circle" alt=""/>
                                    <i class="fa fa-check"></i>
                            </a>
                        </div>
                        <div class="col-md-12 latest-login">
                            <span class="text-capitalize find-expert-text">Last Login Time: </span>
                            <span class="find-expert-text-two">
                                18-12-2016 13:20am
                            </span>
                        </div>
                    </div>
                    <div class="user-info col-md-7">
                        <div class="row basic-info">
                            <div class="col-md-6 name-main">
                                <span class="find-expert-name">
                                Willson Perl
                                </span><br>
                            </div>
                            <div class="col-md-6 country-main">
                                <span class="find-expert-country">
                                Country 
                                </span>
                                <img src="/images/Flags-Icon-Set/24x24/US.png">
                            </div>
                            <div class="col-md-12">
                                <span class="status">
                                LOOKING FOR PROJECT
                                </span>
                            </div>
                            <div class="col-md-12">    
                                <p class="description">
                                I have excellent experience in web designing, web developing, wordpress Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<a href="">Read Less...</a>
                                </p>
                            </div>
                            <div class="col-md-12">
                                <div class="skill-div">
                                <span class="item-skill official"> <img src="/images/crown.png" alt="" height="24px" width="24px"> <span>PHP</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill success"> <img src="/images/checked.png" alt="" height="24px" width="24px"> <span>JavaScript</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill"><span>HTML5</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill"><span>Adobe ilusstrator</span></span>
                                </div>
                                <div class="skill-div">
                                <span class="item-skill"><span>Adobe Photoshop</span></span>
                                </div>
                            </div>    
                        </div>
                    </div>
                    <div class="user-info col-md-3">
                        <div class="row details-container">
                            <div class="col-md-12 per-hr">
                                <i class="fa fa-heart pull-right"></i>
                                <h3>$15.00 / hr</h3>
                            </div>
                        </div>
                        <button type="button" class="btn btn-success find-experts-success pull-right">
                        invite for work
                        </button>
                        <button type="button" class="btn find-experts-offer pull-right">
                        Send Offer
                        </button>
                        <button type="button" class="btn find-experts-offer find-experts-check pull-right">
                        <i class="fa fa-envelope"></i>CHECK
                        </button> 
                    </div>
                </div>
            </div>

            <!-- End New design -->
             
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@endsection


