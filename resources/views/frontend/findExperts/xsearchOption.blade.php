<div class="row">
    <div class="col-md-3 search-title-container">
        <span class="find-experts-search-title text-uppercase"> {{ trans('common.search')}} </span>
    </div>
    <div class="col-md-9">
        <span class="find-experts-search-title text-uppercase">
            {{ trans('common.experts') }}
            <small class="experts-counts">({{ $count }})</small>
        </span>

        @include('frontend.includes.pagination', ['paginator' => $users])
    </div>
</div>
<br>
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <div class="col-md-12 p-tb-15">
            {!! Form::open(['route' => ['frontend.findexperts.post'], 'method' => 'post', 'id' => 'search-form']) !!}
            <div class="portlet-body">
                {{--<label>{{ trans('common.skills') }}</label>--}}
                {{--<div class="search-selectbox">--}}
                    {{--<select class="form-control" name="slcSkill" id="slcSkill">--}}
                        {{--<option value="0">{{ trans('common.select') }}</option>--}}
                        {{--@foreach( $skills as $key => $skill )--}}
                        {{--<option value="{{ $skill->id }}" {{ isset($filter['skill_id']) && $filter['skill_id'] == $skill->id? 'selected':''}}>{{ $skill->getName() }}</option>--}}
                        {{--@endforeach--}}
                    {{--</select>--}}
                {{--</div>--}}
                {{--<br/>--}}

                <label>{{ trans('common.skills') }}</label>
                <div id="magicsuggest"></div>
                <input type="hidden" id="skill_id_list" name="skill_id_list" value="{{ isset($filter['skill_id_list'])? $filter['skill_id_list']: '' }}">
                <br/>

                <label>{{ trans('common.job_position') }}</label>
                <div class="search-selectbox">
                    <select class="form-control" name="slcJobPosition" id="slcJobPosition">
                        <option value="0">{{ trans('common.select') }}</option>
                        @foreach( $jobPositions as $key => $jobPosition )
                        <option value="{{ $jobPosition->id }}" {{ isset($filter['job_position_id']) && $filter['job_position_id'] == $jobPosition->id? 'selected':''}}>{{ $jobPosition->getName() }}</option>
                        @endforeach
                    </select>
                </div>
                <br/>

                <label>{{ trans('common.status') }}</label>
                <div class="search-selectbox">
                    <select class="form-control" name="slcStatus" id="slcStatus" >
                        <option value="0">{{ trans('common.select') }}</option>
                        @foreach( $statusList as $key => $status )
                        <option value="{{ $key }}" {{ isset($filter['status_id']) && $filter['status_id'] == $key? 'selected':''}} >
                            {{ $status }}
                        </option>
                        @endforeach
                    </select>
                </div>    
                <br/>

                <!-- <label>Nickname</label>
                <input name="tbxNickname" id="tbxNickname" type="text" value="{{ isset($filter['nick_name'])? $filter['nick_name']: '' }}" class="form-control" >
                <br/> -->

                <label>{{ trans('common.hourly_rate') }}</label>
                <div>
                    <input name="tbxHourlyRate" id="tbxHourlyRate" type="text" value="{{ isset($filter['hourly_rate'])? $filter['hourly_rate']: '' }}" class="form-control" >
                </div>
                <br/>

                <label>{{ trans('common.review_type') }}</label>
                <div class="search-selectbox">
                    <select class="form-control" name="slcReviewType" id="slcReviewType">
                        <option value="0">{{ trans('common.select') }}</option>
                        @foreach( \App\Constants::getReviewTypeLists() as $key => $reviewType )
                        <option value="{{ $key}}" {{ isset($filter['review_type_id']) && $filter['review_type_id'] == $key? 'selected':''}}>{{ $reviewType }}</option>
                        @endforeach
                    </select>
                </div>
                <br/>

                <label>{{ trans('common.last_activities') }}</label>
                <div class="search-selectbox">
                    <select class="form-control" name="slcLastActivities" id="slcLastActivities">
                        <option value="0">{{ trans('common.select') }}</option>
                        @foreach( \App\Constants::getLastActivityLists() as $key => $lastActivity )
                        <option value="{{ $key }}" {{ isset($filter['last_activities_id']) && $filter['last_activities_id'] == $key? 'selected':''}}>{{ $lastActivity }}</option>
                        @endforeach
                    </select>
                </div>
                <br/>

                <label>{{ trans('common.location') }}</label>
                <div class="">
                    <select class="form-control" name="slcCity" id="slcCity">

                        @if( $filter != null )
                            <option value="{{ isset($filter['city_id'])? $filter['city_id'] : ''}}" selected="selected">
                                {{ isset($filter['city_name'])? $filter['city_name']: '' }}
                            </option>
                        @endif
                    </select>
                </div>
                <br/>

                <a href="javascript:;" class="btn btn-orange btn-search-custom" id="btn-confirm" style="margin-top: 20px; width: 100%;">
                    {{ trans('common.search') }}
                </a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>