<!-- Modal -->
<div id="modal-direct-hire" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            {!! Form::open(['route' => 'frontend.findexperts.directhire', 'id' => 'direct-hire-user-form', 'method' => 'post', 'class' => 'form-horizontal']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        Direct Hire
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="form-body" style="padding: 10px;">
                        <div class="form-group">
                            <label class="col-md-3 control-label">
                                Choose Project
                            </label>
                            <div class="col-md-9">
                                @if( $projects != null )
                                    <select id="slcProjectId" class="bs-select form-control" name="projectId">
                                        @foreach( $projects as $key => $project )
                                            <option value="{{ $project->id }}" >{{ $project->name }}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12" id="add-user-status-errors"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">How would you like to pay?</label>
                            <ul class="list-inline" style="padding: 10px;">
                                @foreach($pay_types as $key => $pay_type)
                                    <li>{!! Form::radio('pay_type', $key, false, ['class'=>'icheck pay_ty', 'id' => 'pay-type-' . $key ] ) !!}
                                        <label class="pay-type-label" for="pay-type-{!!$key!!}">
                                            <img src="/images/clock1.jpg">
                                            <p>{{ $pay_type }}</p>
                                            </label>
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                        <div class="form-group div-price">
                            <label class="control-label">Price</label>
                                    <div>
                                        {!! Form::text('reference_price', '', ['class'=>'form-control col-xs-4', 'id' => 'reference_price' ] ) !!}
                                    </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btn-submit-invite" type="submit" class="btn btn-success m-b-0">
                        {{ trans('common.confirm') }}
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        {{ trans('member.cancel') }}
                    </button>
                </div>
            {!! Form::hidden('receiverId', '', ['id' => 'hire-receiverId'] ) !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>
