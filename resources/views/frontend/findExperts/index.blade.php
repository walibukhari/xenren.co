@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.find_experts_title') }}
@endsection

@section('keywords')
    {{ trans('common.find_experts_keyword') }}
@endsection

@section('description')
    {{ trans('common.find_experts_desc') }}
@endsection

@section('author')
    Xenren
@endsection

@section('url')
    https://www.xenren.co/findExperts
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
{{--    <link href="/assets/global/plugins/bootstrap-star-rating/css/star-rating.css" media="all" rel="stylesheet" type="text/css"/>--}}
{{--    <link href="/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="/custom/css/frontend/findExperts.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="/custom/css/frontend/findExpertsNew.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="/assets/global/plugins/select2/css/select2.css" rel="stylesheet" type="text/css"/>--}}
{{--    <link href="/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css"/>--}}
{{--    <link href="/custom/css/frontend/modalInviteToWork.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="/custom/css/frontend/modalSendOffer.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="/custom/css/frontend/findExpertsSearch.css" rel="stylesheet" type="text/css" />--}}
    <link href="{{asset('css/minifiedFindExpert.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .mt-10 {
            margin-top: 10px;
        }

        .setH3FXXP123{
            margin: 0px !important;
            color:#5ec329 !important;
            width: auto !important;
            text-align: right !important;
        }

        .setH3FXXP{}

        .search-box-field label {
            padding-bottom: 0;
        }
        .setclear-filter {
            cursor: pointer;
        }
        .setImageDefault{
            width: 100%;
            height: 230px;
            border-radius: 5px;
        }
        .viewAllPortfolio{
            position: absolute;
            background: rgba(0,0,0,0.5);
            height: 30px;
            padding: 10px;
            width: 93%;
            bottom: 0px;
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
            color: #fff;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .user-info{
            /*margin-bottom: ;*/
        }
        .port-not-avail{
            position: absolute;
            bottom: 30px;
            display: flex;
            justify-content: center;
            align-items: center;
            text-align: center;
            width: 93%;
            color: gray;
        }
        .contactInfoModal button {
            background-color: #d7d7d7;
            font-size: 18px;
            font-weight: bold;
            /* height: 30px !important; */
            padding: 3px 13px !important;
            margin-bottom: 10px;
            right: 10px;
        }
        .contactInfoModal .modal-header{
            background: #FCFCFC !important;
            padding: 14px 5px 0px 16px !important;
            border-radius: 5px 5px 0px 0px;
        }
        .contactInfoModal .modal-header h4 {
            color: #676767;
            text-transform: uppercase;
            font-weight: 600 !important;
            font-family: "Raleway",Sans-Serif !important;
            font-size: 14px;
            text-align: left;
        }
        .contactInfoModal .modal-title{
            padding-top: 14px;
        }
        .contactInfoModal .modal-body .content-group .input-group-addon {
            min-width: 50px;
            background: #F7F7F7;
            border-color: #e0e8ea;
            height: 40px;
        }
        .tbxSkypeId{
            height: 40px !important;
        }
    </style>
@endsection
@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper" style="margin-bottom: 15px;">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE BASE CONTENT -->
            @if( $isSuccessPost )
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12 alert alert-success alert-dismissable notification-box">
                            <div class="col-md-1">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="col-md-10 notification">
                                <div class="success-title">{{ trans('common.success') }}!</div>
                                <div class="success-message">{!! trans('common.success_job_post_message') !!}</div>
                            </div>
                            <div class="col-md-1 btn-close-box">
                                <button type="button" class="btn-close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        <!-- Start New design -->
            @if($users != null)
                @foreach($users as $key=>$user)
                    <div class="portlet find-expert-box find-expert-box-new btn-check-detail setFEPPortlet" data-reurl="{{ route('frontend.resume.getdetails',['lang' => \Session::get('lang'),'id' => $user->user_link] ) }}">
                        <div class="row personal-profile">
                            <div class="profile-image col-md-2">
                                <div class="profile-userpic p-t-5 text-center">
                                <span>
                                    <img src="
                                        @if(!empty($user->getCompressedImageAttribute()))
                                            {{$user->getCompressedImageAttribute()}}
                                        @else
                                            \images\avatar_xenren.png
                                        @endif
                                    "
                                    class="img-responsive img-circle" alt=""/>
                                    @if( $user->getLatestIdentityStatus() == \App\Models\UserIdentity::STATUS_APPROVED )
                                        <i class="fa fa-check"></i>
                                    @endif
                                </span>
                                </div>
                                <div class="col-md-12 latest-login">
                                    <span class="text-capitalize find-expert-text">{{ trans('common.last_login_time') }}: </span>
                                    <span class="find-expert-text-two">
                                    {{ $user->last_login_at}}
                                </span>
                                </div>
                            </div>
                            <div class="user-info @if($user->job_position_id == \App\Models\Portfolio::PORTFOLIO_TYPE_DESIGNER) col-md-6 @else col-md-7 @endif">
                                <div class="row basic-info">
                                    <div class="col-md-6 name-main">
                                    <span class="find-expert-name">
                                    {{$user->name}}
                                    </span><br>
                                    </div>
                                    <div class="col-md-5 country-main setFEPPCM">
                                    <span class="find-expert-country">
                                    {{ trans('common.country') }}
                                    </span>
                                    @if($user->job_position_id == \App\Models\Portfolio::PORTFOLIO_TYPE_DESIGNER)
                                        @if(!empty($user->hourly_pay))
                                            <h3 class="setH3FXXP123 setH3FXXP">
                                                @if($user->currency == 1)
                                                    $
                                                @else
                                                    &#165;
                                                @endif
                                                {{ $user->hourly_pay }}&nbsp;/&nbsp; {{trans('common.hr')}}
                                            </h3>
                                        @else
                                            <h3>
                                                &nbsp;
                                            </h3>
                                        @endif
                                    @endif
                                        @php
                                            try {
                                                $countryCode = strtoupper($user->country()->first()->code);
                                            } catch (\Exception $e) {
                                                $countryCode = '';
                                            }
                                        @endphp
                                        @if($countryCode != '')
{{--                                            <img src="/images/Flags-Icon-Set/24x24/{{ strtoupper($countryCode) }}.png">--}}
                                        @endif
                                        @if(is_null($user->last_scanned_office))
                                        @else
                                        <div class="sharedOfficePopover">
                                            <img class="set-users-icon-image" src="{{asset('images/use-good-markr-icon.png')}}">
                                            @if($user->checked_in_cowork_space == \App\Models\User::CHECKED_IN_CO_WORK_SPACE)
                                                <div class="sharedOfficeBox">
                                                    <i class="fa fa-caret-left set-fa-fa-caret-left"></i>
                                                    <div class="row">
                                                        <div class="co-md-5">
                                                            <img class="sharedofficeImage" src="/{{isset($user->last_scanned_office->office->image) ? $user->last_scanned_office->office->image : asset('images/noimage.png')}}">
                                                        </div>
                                                        <div class="col-md-7 set-col-md-7-shared-office-rating">

                                                            @php
                                                                $activeStars = 0;
                                                                $inActiveStars = 0;
                                                                $rate = $user->sharedofficeRating();
                                                                $activeStars = number_format($rate, 0);
                                                                $total = 5;
                                                                $inActiveStars = $total -  $rate;
                                                            @endphp

                                                            @for($i=0; $i < $activeStars; $i ++)
                                                                <i class="fa fa-star set-fa-fa-star-sharedOffice set-fa-fa-star-sharedOffice-active" aria-hidden="true"></i>
                                                            @endfor

                                                            @for($i=0; $i < $inActiveStars; $i ++)
                                                                <i class="fa fa-star set-fa-fa-star-sharedOffice" aria-hidden="true"></i>
                                                            @endfor
                                                            <br>
                                                            <span class="set-reviews-shared-office-box">{{isset($activeStars) ?$activeStars : 0}} Reviews</span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        @php
                                                            if($user->last_scanned_office && $user->last_scanned_office->office) {
                                                                 $officeName = $user->last_scanned_office->office->office_name;
                                                                 $location = $user->last_scanned_office->office->location;
                                                            } else {
                                                                $officeName = '';
                                                                $location = '';
                                                            }
                                                        @endphp
                                                        <div class="col-md-12 set-col-md-12-shared-0ffice-name">
                                                            <span class="set-shared-office-name-fEp">{{$officeName}}</span>
                                                        </div>
                                                        <div class="col-md-12 set-col-md-12-shared-0ffice-location">
                                                            <span><img class="set-shared-office-location-image" src="{{asset('images/location-image.png')}}"></span><span  style="padding: 10px;" class="location-name-shared-office">{{$location}},{{$user->sharedofficeCityWithCountry()}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="sharedOfficeBox">
                                                    <p class="set-p-fep">User have checked in couple cowork space , however he doesn't want to show the location.</p>
                                                </div>
                                            @endif
                                        </div>
                                        @endif
                                    </div>
                                    <div class="col-md-12">
                                    <span class="status">
                                        {{--{{dd(is_null($user->status))}}--}}
                                        {{--{{dd(isset($user->status))}}--}}
                                        @if(is_null($user->status == true) && isset($statusList[$user->status]))
                                            {{ $statusList[$user->status] }}
                                        @endif
                                    </span>
                                    </div>
                                    <div class="col-md-12">
                                        <p class="description">
                                            @if(preg_match("/\p{Han}+/u", $user->about_me))
                                                @if(preg_match_all('/./u', $user->about_me, $matches) > 75)
                                                    <span class="read-more-wrap">
                                                    {{ $aboutMe[$user->id] }}
                                                </span>
                                                    <span class="read-more-target">
                                                    {{ $user->about_me }}
                                                </span>
                                                    <span class="read-more-trigger" data-id="{{$user->id}}" id="readMore_{{$user->id}}" > {{ trans('common.read_more') }} </span>
                                                @else
                                                    {{ $user->about_me }}
                                                @endif
                                            @else
                                                @if(strlen($user->about_me) > 75)
                                                    <span class="read-more-wrap">
                                                    {{ substr($user->about_me, 0, 75) }}
                                                </span>
                                                    <span class="read-more-target">
                                                    {{ $user->about_me }}
                                                </span>
                                                    <span class="read-more-trigger" data-id="{{$user->id}}" id="readMore_{{$user->id}}" > {{ trans('common.read_more') }} </span>
                                                @else
                                                    {{ $user->about_me }}
                                                @endif
                                            @endif
                                        </p>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="scroll-div" style="width:99%;">
                                            @foreach($user->skills as $user_skill)
                                                <div class="skill-div setFEPPSkillDiv">
                                            <span class="
                                            @if($user_skill->experience >= 1)
                                                {{ 'item-skill official' }}
                                            @elseif($user_skill->is_client_verified == 1)
                                                {{ 'item-skill success' }}
                                            @else
                                                {{ 'item-skill' }}
                                            @endif
                                            ">
                                                @if($user_skill->experience >= 1)
                                                    <img src="/images/crown.png" alt="" height="24px" width="24px">
                                                @elseif($user_skill->is_client_verified == 1)
                                                    <img src="/images/checked.png" alt="" height="24px" width="24px">
                                                @endif

                                                @if(isset($user_skill->skill) && !is_null($user_skill->skill))
                                                <span>
                                                    {{ $user_skill->skill->getName() }}
                                                </span>
                                                @endif
                                            </span>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if($user->job_position_id == \App\Models\Portfolio::PORTFOLIO_TYPE_DESIGNER)
                            <div class="col-md-4">
                                @if(isset($user->portfolios) && count($user->portfolios) > 0)
                                    <img class="setImageDefault" onclick="window.location.href='{{ route('frontend.resume.getdetails',['lang' => \Session::get('lang'),'id' => $user->user_link] ) }}'" src="{{$user->getPortfolioImage($user->portfolios)}}" onerror="this.src='/images/default_portfolio.png'" />
                                    <div class="viewAllPortfolio" onclick="window.location.href='{{ route('frontend.resume.getdetails',['lang' => \Session::get('lang'),'id' => $user->user_link] ) }}'">
                                        View Full Portfolio
                                    </div>
                                @else
                                    <img class="setImageDefault" src="/images/default_portfolio.png" onerror="this.src='/images/default_portfolio.png'" />
                                    <span class="port-not-avail">
                                        Portfolio not available!
                                    </span>
                                @endif
                            </div>
                            @else
                            <div class="user-info col-md-3">
                                <div class="row details-container btn-check-detail">
                                    <div class="col-md-12 per-hr">

                                        @if(\Auth::check())
                                            <a class="btn-add-fav-freelancer setHeart{{ $user->isFavouriteFreelance()? 'activeFree' : '' }}" href="javascript:;" data-url="{{ $user->isFavouriteFreelance()? route('frontend.removefavouritefreelancer.post') : route('frontend.addfavouritefreelancer.post') }}" data-freelancer-id="{{ $user->id }}">
                                                <i class="fa fa-heart pull-right {{ $user->isFavouriteFreelance()? 'heart-red' : '' }}"></i>
                                            </a>
                                        @else
                                            <i class="fa pull-right"></i>
                                        @endif
                                        @if(!empty($user->hourly_pay))
                                            <h3 class="setH3FXXP">
                                                @if($user->currency == 1)
                                                    $
                                                @else
                                                    &#165;
                                                @endif
                                                {{ $user->hourly_pay }}&nbsp;/&nbsp; {{trans('common.hr')}}
                                            </h3>
                                        @else
                                            <h3>
                                                &nbsp;
                                            </h3>
                                        @endif
                                    </div>
                                    <div id="button-list showHideButtonList" class="col-md-12 setCol12FEP" style="display: none">
                                        <div class="col-md-12">
                                            @if(\Auth::user())
                                                <a href="{{ route('frontend.findexperts.invitetowork', ['user_id' => $user->id, 'invitation_project_id' => 0 ] ) }}"
                                                   data-toggle="modal"
                                                   data-target="#modalInviteToWork"
                                                >
                                                    <button id="btnInviteForWork-{{ $user->id }}" type="button" class="setFEPPBtn btn btn-success find-experts-success pull-right" >
                                                        {{ trans('common.invite_for_work') }}
                                                    </button>
                                                </a>
                                            @else
                                                <a href="{{ route('login')}}" onclick="clickFirst();">
                                                    <button id="btnInviteForWork-{{ $user->id }}" type="button" class="setFEPPBtn btn btn-success find-experts-success pull-right" >
                                                        {{ trans('common.invite_for_work') }}
                                                    </button>
                                                </a>
                                            @endif
                                        </div>
                                        <div class="col-md-12">
                                            @if(\Auth::user())
                                                <a href="{{ route('frontend.findexperts.sendoffer', ['user_id' => $user->id ] ) }}"
                                                   data-toggle="modal"
                                                   data-target="#modalSendOffer"
                                                >
                                                    <button type="button" class="btn find-experts-offer pull-right setFEPPBtn">
                                                        {{ trans('common.send_offer') }}
                                                    </button>
                                                </a>
                                            @else
                                                <a href="{{ route('login')}}" onclick="clickFirst();">
                                                    <button type="button" class="btn find-experts-offer pull-right setFEPPBtn">
                                                        {{ trans('common.send_offer') }}
                                                    </button>
                                                </a>
                                            @endif
                                        </div>
                                        <div class="col-md-12">
                                            @if(!empty($user->skype_id) || !empty($user->wechat_id) || !empty($user->line_id) || !empty($user->handphone_no))
                                                <button type="button" data-toggle="modal" data-target=".showContactInfoModal" data-user="{{$user->id}}" data-skype="{{$user->skype_id}}" data-wechat="{{$user->wechat_id}}" data-line="{{$user->line_id}}" data-handphone="{{$user->handphone_no}}" class="contactInfoModalOpen setFEPPBtn btn find-experts-offer find-experts-check pull-right">
                                                    <i class="fa fa-envelope set-fa-fa-fa-envelope"></i> {{ trans('common.contact_info') }}
                                                </button>
                                            @else
                                                @if(\Auth::check())
                                                <button type="button" class="setFEPPBtn btn find-experts-offer send-request-for-contact-info pull-right" data-receiver-id="{{ $user->id }}">
                                                    <i class="fa fa-envelope-open set-fa-fa-fa-envelope"></i> {{ trans('common.request') }}
                                                </button>
                                                @else
                                                <button type="button" onclick="window.location.href='/{{\Session::get('lang')}}/login'" class="setFEPPBtn btn find-experts-offer pull-right">
                                                    <i class="fa fa-envelope-open set-fa-fa-fa-envelope"></i> {{ trans('common.request') }}
                                                </button>
                                                @endif
                                            @endif

                                            @if( $user->is_contact_allow_view == \App\Models\User::CONTACT_INFO_ALLOW_TO_VIEW )

                                            <!-- new TMP DISABLED -->
                                             @if($user->handphone_no != '' || $user->skype_id != '' || $user->wechat_id != '' || $user->line_id != '')

                                                <!-- new TMP DISABLED -->
                                                <!-- <button type="button" data-toggle="collapse" data-target="#contact-{{$user->id}}" class="setFEPPBtn btn find-experts-offer find-experts-check pull-right">
                                                    <i class="fa fa-envelope set-fa-fa-fa-envelope"></i> {{ trans('common.contact_info') }}
                                                </button> -->
                                            @elseif( $user->is_contact_allow_view == \App\Models\User::CONTACT_INFO_NOT_ALLOW_TO_VIEW && \Auth::user())

                                            <!-- new TMP DISABLED -->
                                             @elseif($user->handphone_no == '' && $user->skype_id == '' && $user->wechat_id == '' && $user->line_id == '')

                                            <!-- new TMP DISABLED -->
                                            <!-- <button type="button" class="setFEPPBtn btn find-experts-offer find-experts-request send-request-for-contact-info pull-right" data-receiver-id="{{ $user->id }}">
                                                    <i class="fa fa-envelope set-fa-fa-fa-envelope"></i> {{ trans('common.request') }}
                                                </button> -->
                                            @endif

                                            <!-- new TMP DISABLED-->
                                             @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <!-- TEMP DISABLED -->
                            {{-- @if( $user->is_contact_allow_view == \App\Models\User::CONTACT_INFO_ALLOW_TO_VIEW ) --}}
{{--                                <div class="user-info col-md-12">--}}
{{--                                    <div style="border-radius: 20px;" id="contact-{{$user->id}}" class="contact collapse" data-user-id="{{ isset( $_G['user'] )? $_G['user']->id: ''  }}" data-ip-address="{{ $_SERVER['REMOTE_ADDR'] }}" data-target-user-id="{{$user->id}}">--}}
{{--                                        <div class="col-md-12 contacts-info">--}}
{{--                                            <div class="col-md-10 col-md-10">--}}
{{--                                                <div class="row">--}}
{{--                                                    <div class="col-md-3 col-md-3 contacts-info-sub">--}}
{{--                                                        <img src="/images/skype-icon-5.png">--}}
{{--                                                        <span>Skype</span>--}}
{{--                                                        <p>{{ empty($user->skype_id) ? '-' : $user->skype_id }}</p>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="col-md-3 col-md-3 contacts-info-sub">--}}
{{--                                                        <img src="/images/wechat-logo.png">--}}
{{--                                                        <span>WeChat</span>--}}
{{--                                                        <p>{{ empty($user->wechat_id) ? '-' : $user->wechat_id }}</p>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="col-md-3 col-md-3 contacts-info-sub">--}}
{{--                                                        <img src="/images/line.png">--}}
{{--                                                        <span>line</span>--}}
{{--                                                        <p>{{ empty($user->line_id) ? '-' : $user->line_id }}</p>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="col-md-3 col-md-3 contacts-info-sub">--}}
{{--                                                        <img src="/images/MetroUI_Phone.png">--}}
{{--                                                        <span>Phone</span>--}}
{{--                                                        <p>{{ empty($user->handphone_no) ? '-' : $user->handphone_no }}</p>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            {{-- @endif --}}
                        </div>
                    </div>
            @endforeach
            @include('frontend.includes.pagination', ['paginator' => $users])
        @endif

        {{-- contact info popup --}}
        <!-- Modal -->
            <div class="showContactInfoModal contactInfoModal modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button class="setFEPPBtn123 btn pull-right" data-dismiss="modal">
                                <i class="setFEPPBtn123 fa fa-close" aria-hidden="true"></i>
                            </button>
                            <h4 class="modal-title">
                                Contacts Info
                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-body">
                                <div class="form-group content-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="input-group setInputGroupMSCI">
                                                                <span class="input-group-addon">
                                                                    <img src="/images/skype-icon-5.png">
                                                                </span>
                                                <input class="form-control text-center tbxSkypeId" id="skypeId" placeholder="Skype" readonly="readonly" name="skype_id" type="text" value="">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-group setInputGroupMSCI">
                                                                <span class="input-group-addon">
                                                                    <img src="/images/wechat-logo.png">
                                                                </span>
                                                <input class="form-control text-center tbxSkypeId" id="wechatId" placeholder="Skype" readonly="readonly" name="wechat_id" type="text" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="input-group setInputGroupMSCI">
                                                                <span class="input-group-addon">
                                                                    <img src="/images/line.png">
                                                                </span>
                                                <input class="form-control text-center tbxSkypeId" id="lineId" placeholder="Skype" readonly="readonly" name="line_id" type="text" value="">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-group setInputGroupMSCI">
                                                                <span class="input-group-addon">
                                                                    <img src="/images/MetroUI_Phone.png">
                                                                </span>
                                                <input class="form-control text-center tbxSkypeId" id="handphoneId" placeholder="Skype" readonly="readonly" name="handphone_id" type="text" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="modal-footer" style="text-align: left;padding-left:22px;color: red;font-size: 15px;padding-top: 0px;padding-bottom: 0px;">
                            <p>Work Out Of Xenren Will Have Risk</p>
                        </div>
                    </div>

                </div>
            </div>
        {{-- contact info popup --}}


        <!-- End New design -->
            {{-- @if( $users != null )
                @foreach($users as $key => $user)
                    @include('frontend.findExperts.userDetails', [
                        'user' => $user,
                        'projects' => $projects,
                        'invitationProject' => $invitationProject
                    ] )
                @endforeach

                {!! $users->render() !!}
            @endif --}}
        <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    {{--    @include('frontend.includes.modalInvite', ['projects' => $projects ])--}}
@endsection

@section('modal')
    <div id="modalInviteToWork" class="modal fade apply-job-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>

    <div id="modalSendOffer" class="modal fade apply-job-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
@endsection

@section('footer')
{{--    <script src="/assets/global/plugins/bootstrap-star-rating/js/star-rating.js" type="text/javascript"></script>--}}
{{--    <script src="/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>--}}
{{--    <script src="/assets/global/plugins/select2/js/i18n/zh-CN.js" type="text/javascript"></script>--}}
{{--    <script src="/assets/global/plugins/readmore/readmore.min.js" type="text/javascript"></script>--}}
{{--    <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>--}}
    <script>
			$('.scroll-div').slimScroll({});
    </script>
    <script>
            $('.contactInfoModalOpen').click(function () {
                console.log("$(this).data('user-id')");
                let userId = $(this).attr('data-user')
                let skype = $(this).attr('data-skype')
                let wechat = $(this).attr('data-wechat')
                let line = $(this).attr('data-line')
                let handphone = $(this).attr('data-handphone')
                console.log(userId,skype,wechat,line,handphone);
                $('#skypeId').val(skype);
                $('#wechatId').val(wechat);
                $('#lineId').val(line);
                $('#handphone').val(handphone);
            });

			function clickFirst(){
				event.stopPropagation();
				// $('#modalInviteToWork').modal('show');
			}

			var api_city_url = "{{url('api/cities')}}";
			var url = {
				api_isAllowToCheckContact: "{{ route('api.isallowtocheckcontact') }}",
				addFavouriteFreelancer: "{{ route('frontend.addfavouritefreelancer.post') }}",
				removeFavouriteFreelancer: "{{ route('frontend.removefavouritefreelancer.post') }}",
                loginPage: "{{ route('login') }}"
			};

			var lang = {
				'select_city' : "{{ trans('common.select_city') }}",
				'make_selection' : "{{ trans('common.make_selection')  }}",
				'unable_to_request' : "{{ trans('common.unable_to_request') }}",
				'only_choose_one' :"{{ trans('common.only_choose_one') }}",
				'read_more' : "{{ trans('common.read_more') }}",
				'read_less' : "{{ trans('common.read_less') }}",
				'select' : "{{ trans('common.select') }}",
				'enter' : "{{ trans('common.enter') }}"
			};

			var language = "{{ trans('common.lang') }}";
			if( language == "en" )
			{
				language = "en";
			}
			else
			{
				language = "zh-CN";
			}

			var skillList = {!! $skillList !!};
			var languages = {!! $languages !!};
			var showAdvanceFilterSection = {{ $showAdvanceFilterSection }};

			$(document).ready(function(){
				$('.read-more-trigger').click(function(){
					event.stopPropagation();
					var s = $(this);
					var originaltext= s.text();
                    var x = $(this).data("id");
                    var val  = $("#readMore_"+x).html();
                    if(val == ' Read More... ') {
                        var d = $("#readMore_"+x).html('Read less ...');
                    } else {
                        var d = $("#readMore_"+x).html(' Read More... ');
                    }
                    s.siblings('.read-more-target').toggle();
					// if(s.text()) {
                    //     alert(2);
                    //     s.html(lang.read_less);
                    //     s.siblings('.read-more-target').show();
                    //     s.siblings('.read-more-wrap').hide();
					// }
					// else {
					//     alert(1);
                    //     // var b = document.getElementById("read_more").innerText;
                    //     // alert(b);
                    //     // var a = document.getElementById("read_more").innerText = 'Read less ...';
                    //     // b.append(a);
                    //     // s.html(lang.read_more);
                    //     // s.siblings('.read-more-target').hide();
                    //     // s.siblings('.read-more-wrap').show();
					// }
				});

				$('#tbxName').keyup(function(e){
					if(e.keyCode == 13)
					{
						$('#btn-confirm').click()
					}
				});
			});
            // $('#magicsuggest').magicSuggest({
            //     placeholder: '{{trans('common.type_or_click_here')}}'
            // });
	</script>
    <script>
        window.session == '{{\Session::get('lang')}}';
    </script>
{{--    <script src="/custom/js/frontend/findExperts.js" type="text/javascript"></script>--}}
<script src="{{asset('js/minifiedFinExpert.js')}}" type="text/javascript"></script>
@endsection
