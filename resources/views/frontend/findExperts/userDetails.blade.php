<div class="portlet find-expert-box">
    <div class="row personal-profile">
        <div class="profile-image col-md-3">
            <div class="profile-userpic p-t-5 text-center">
                <a href="">
                    <img src="{{ $user->getAvatar() }}" alt=""/>
                </a>


                @if(\Auth::user())
                    {{--user login--}}
                    @if( $invitationProject )
                        {{--have invitation project id --}}
                        @if( ! $user->isJobInvitationReceived($invitationProject->id) )
                            {{--project creator still not send the invitation--}}
                            <a id="lnkInviteForWork-{{ $user->id }}" href="{{ route('frontend.findexperts.invitetowork', ['user_id' => $user->id, 'invitation_project_id' => $invitationProject->id ] ) }}"
                                data-toggle="modal"
                                data-target="#modalInviteToWork">
                                <button id="btnInviteForWork-{{ $user->id }}" type="button" class="btn btn-success find-experts-success">
                                    {{ trans('common.invite_for_work') }}
                                </button>
                            </a>
                        @else
                            {{--project creator already send the invitation--}}
                            <a href="javascript:;">
                                <button type="button" class="btn btn-success invite-freelance-gray">
                                   {{ trans('common.invite_for_work') }}
                               </button>
                            </a>
                        @endif
                    @else
                        {{--dont have invitation project id--}}
                        <a id="lnkInviteForWork-{{ $user->id }}" href="{{ route('frontend.findexperts.invitetowork', ['user_id' => $user->id, 'invitation_project_id' => 0 ] ) }}"
                            data-toggle="modal"
                            data-target="#modalInviteToWork">
                            <button id="btnInviteForWork-{{ $user->id }}" type="button" class="btn btn-success find-experts-success">
                                {{ trans('common.invite_for_work') }}
                            </button>
                        </a>
                    @endif
                @else
                    {{--user not login--}}
                    <a href="{{ route('login')}}" >
                        <button type="button" class="btn btn-success find-experts-success">
                            {{ trans('common.invite_for_work') }}
                        </button>
                    </a>
                @endif


                @if(\Auth::user())
                <a href="{{ route('frontend.findexperts.sendoffer', ['user_id' => $user->id ] ) }}"
                    data-toggle="modal"
                    data-target="#modalSendOffer">
                @else
                <a href="{{ route('login')}}" >
                @endif
                    <button type="button" class="btn find-experts-offer">
                        {{ trans('common.send_offer') }}
                    </button>
                </a>

                @if( $user->is_contact_allow_view == \App\Models\User::CONTACT_INFO_ALLOW_TO_VIEW )
                <button type="button" data-toggle="collapse" data-target="#contact-{{$user->id}}"
                        class="btn btn-white-bg-green-text m-t-15">
                    {{ trans('common.contact_info') }}
                </button>
                @elseif( $user->is_contact_allow_view == \App\Models\User::CONTACT_INFO_NOT_ALLOW_TO_VIEW && \Auth::user())
                <button type="button" class="btn btn-white-bg-yellow-text send-request-for-contact-info m-t-15"
                    data-receiver-id="{{ $user->id }}">
                    {{ trans('common.request_contact_info') }}
                </button>
                @endif
            </div>
            <br>
        </div>
        <div class="user-info col-md-6">
            <div class="row basic-info">
                <div class="col-md-12">
                    <span class="find-expert-name">
                        {{ $user->getName() }}

                        @if( $user->getLatestIdentityStatus() == \App\Models\UserIdentity::STATUS_APPROVED )
                            <span class="fa fa-check-circle"></span>
                        @endif
                    </span>
                    <br>
                </div>
                <div class="col-md-12">
                    <span class="status">
                        @if ($user->status >= 1)
                            {{ $user->explainStatus() }}
                        @endif
                    </span>
                </div>
                <div class="col-md-12">
                    <p class="description setUDETAILSDESC">
                        {{ $user->getAboutMe() }}
                    </p>
                </div>
                <div class="col-md-12">
                    {{--<div class="skill-div">--}}
                    {{--<span class="item-skill official"> <img src="/images/logo_2.png" alt="" height="24px" width="24px"> <span>PHP</span></span>--}}
                    {{--</div>--}}
                    {{--<div class="skill-div">--}}
                    {{--<span class="item-skill success"> <img src="/images/checked.png" alt="" height="24px" width="24px"> <span>JavaScript</span></span>--}}
                    {{--</div>--}}
                    {{--<div class="skill-div">--}}
                    {{--<span class="item-skill"><span>HTML5</span></span>--}}
                    {{--</div>--}}
                    {{--<div class="skill-div">--}}
                    {{--<span class="item-skill"><span>Adobe Photoshop</span></span>--}}
                    {{--</div>--}}
                    {{--<div class="skill-div">--}}
                    {{--<span class="item-skill"><span>Adobe ilusstrator</span></span>--}}
                    {{--</div>--}}
                    @foreach ( $user->skills as $key => $userSkill)
                        <div class="skill-div">
                            @if(isset($userSkill->skill) && !is_null($userSkill->skill))
                            @if ( $userSkill->getSkillVerifyStatus() == \App\Models\UserSkill::SKILL_UNVERIFIED )
                            <span class="item-skill unverified">
                                <span>{{$userSkill->skill->getName()}}</span>
                            </span>
                            @elseif ( $userSkill->getSkillVerifyStatus() == \App\Models\UserSkill::SKILL_ADMIN_VERIFIED )
                            <span class="item-skill admin-verified">
                                <img src="/images/crown.png" alt="" height="24px" width="24px">
                                <span>{{$userSkill->skill->getName()}}</span>
                            </span>
                            @elseif ( $userSkill->getSkillVerifyStatus() == \App\Models\UserSkill::SKILL_CLIENT_VERIFIED )
                            <span class="item-skill client-verified">
                                <img src="/images/checked.png" alt="" height="24px" width="24px">
                                <span>{{$userSkill->skill->getName()}}</span>
                            </span>
                            @endif
                            @endif
                        </div>
                    @endforeach
                </div>

                <div class="col-md-12 latest-login">
                    <span class="text-capitalize find-expert-text">{{ trans('common.last_login_time') }}: </span>
                    <span class="find-expert-text-two">
                        {{ $user->getLastLogin() }}
                    </span>
                </div>
                @if( isset($user->country) )
                    <div class="col-md-12">
                    <span class="text-capitalize find-expert-text">{{ trans('common.country') }}:
                        <img src="{{asset('images/Flags-Icon-Set/24x24/' . strtoupper($user->country->code).'.png')}}" alt=""
                              class="setFEXPERTPAGEIMAGE">
                    </span>
                    </div>
                @endif
            </div>
        </div>
        <div class="user-info col-md-3">
            <div class="row details-container">
                @if( \Auth::check())
                <div class="favourite-freelancer">
                    <a class="btn-add-fav-freelancer {{ $user->isFavouriteFreelance()? 'active': '' }}" href="javascript:;" data-url="{{ route('frontend.addfavouritefreelancer.post') }}" data-freelancer-id="{{ $user->id }}">
                        <span class="fa fa-star"></span>
                    </a>
                </div>
                @endif

                <div class="col-md-12 per-hr">
                    <h3>{{ $user->getHourlyPay() }}</h3>
                </div>
                <div class="col-md-12 details-container-s">
                    <span class="title clearfix">{{ trans('common.age') }}</span>
                    <span class="definition">
                        {{ $user->age }}
                    </span>
                </div>
                <div class="col-md-12 details-container-s">
                    <span class="title clearfix">{{ trans('common.experience_year') }}</span>
                    <span class="definition">
                        {{ $user->getExperience() }} {{ trans_choice('common.years', $user->getExperience()) }}
                    </span>
                </div>
                <div class="col-md-12 details-container-s">
                    <span class="title clearfix">{{ trans('common.project_completed') }}</span>
                    <span class="definition">0</span>
                </div>
            </div>
            <br>
            <button type="button" class="btn btn-check-detail"
                    data-url="{{ route('frontend.resume.getdetails',['lang' => \Session::get('lang'),'id' => $user->id] ) }}">
                {{ trans('common.check_detail') }}
            </button>
        </div>
        @if( $user->is_contact_allow_view == \App\Models\User::CONTACT_INFO_ALLOW_TO_VIEW )
        <div class="user-info col-md-12">
            <div id="contact-{{$user->id}}" class="contact collapse" data-user-id="{{ isset( $_G['user'] )? $_G['user']->id: ''  }}" data-ip-address="{{ $_SERVER['REMOTE_ADDR'] }}" data-target-user-id="{{$user->id}}">
                <div class="col-md-12 contacts-info">
                    <div class="col-md-10 col-md-10">
                        <div class="row">

                            <div class="col-md-3 col-md-3 contacts-info-sub">
                                <img src="/images/skype-icon-5.png">
                                <span>Skype</span>
                                <p>{{$user->skype_id}}</p>
                            </div>

                            <div class="col-md-3 col-md-3 contacts-info-sub">
                                <img src="/images/wechat-logo.png">
                                <span>WeChat</span>
                                <p>{{$user->wechat_id}}</p>
                            </div>
                            <div class="col-md-3 col-md-3 contacts-info-sub">
                                <img src="/images/Tencent_QQ.png">
                                <span>QQ</span>
                                <p>{{$user->qq_id}}</p>
                            </div>


                            <div class="col-md-3 col-md-3 contacts-info-sub">
                                <img src="/images/MetroUI_Phone.png">
                                <span>Phone</span>
                                <p>{{$user->handphone_no}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>

{{--<div class="portlet find-expert-box">--}}
{{--<div class="row personal-profile">--}}
{{--<div class="profile-image col-md-3">--}}
{{--<div class="profile-userpic p-t-5 text-center">--}}
{{--<a href="{{ route('frontend.resume.getdetails', $user->id ) }}">--}}
{{--<img src="{{ $user->getAvatar() }}" class="img-responsive img-circle" alt=""/>--}}
{{--</a>--}}
{{--<br>--}}
{{--@if( count($projects) != 0 )--}}
{{--<button id="btn-invite-{{ $user->id }}" type="button" class="btn btn-success find-experts-success" data-target="#modalInvite" >--}}
{{--{{ trans('common.invite_for_work') }}--}}
{{--</button>--}}
{{--@else--}}
{{--<button type="button" class="btn btn-success btn-login find-experts-success" data-url="{{ route('login') }}">--}}
{{--{{ trans('common.invite_for_work') }}--}}
{{--</button>--}}
{{--@endif--}}
{{--</div>--}}
{{--<br>--}}
{{--</div>--}}
{{--<div class="user-info col-md-6">--}}
{{--<div class="basic-info">--}}
{{--<span class="find-expert-name">--}}
{{--{{ $user->nick_name }}&emsp;--}}
{{--@if( $user->title != null )--}}
{{--{{ $user->title }}--}}
{{--@endif--}}
{{--@if( $user->getLatestIdentityStatus() == \App\Models\UserIdentity::STATUS_APPROVED )--}}
{{--<span class="fa fa-check-circle"></span>--}}
{{--@endif--}}
{{--</span>--}}
{{--<br>--}}
{{--<span class="status">--}}
{{--@if ($user->status >= 1)--}}
{{--{{ $user->explainStatus() }}--}}
{{--@endif--}}
{{--</span>--}}
{{--<div class="row details-container">--}}
{{--<div class="col-md-2">--}}
{{--<span class="title clearfix">{{ trans('common.age') }}</span>--}}
{{--<span class="definition">--}}
{{--{{ $user->getYearsOld() }}--}}
{{--</span>--}}
{{--</div>--}}
{{--<div class="col-md-4">--}}
{{--<span class="title clearfix">{{ trans('common.experience') }}</span>--}}
{{--<span class="definition">{{ $user->getExperience() }} {{ trans_choice('common.years', $user->getExperience()) }}</span>--}}
{{--</div>--}}
{{--<div class="col-md-6">--}}
{{--<span class="title clearfix">{{ trans('common.project_completed') }}</span>--}}
{{--<span class="definition">0</span>--}}
{{--</div>--}}
{{--</div>--}}

                {{--@if( $user->job_position != null )--}}
                 {{--- <span class="job-remark" title="{{ $user->job_position->getRemark() }}">{{ $user->job_position->getName() }}</span>--}}
                {{--@endif--}}

                {{--<div class="skill-div">--}}
                    {{--<span class="item-skill official"> <img src="/images/crown.png" alt=""> <span>HTML</span></span>--}}
                {{--</div>--}}
                {{--<div class="skill-div">--}}
                    {{--<span class="item-skill success"> <img src="/images/checked.png" alt=""> <span>CSS</span></span>--}}
                {{--</div>--}}
                {{--<div class="skill-div">--}}
                    {{--<span class="item-skill"><span>JavaScript</span></span>--}}
                {{--</div>--}}
                {{--<div class="skill-div">--}}
                    {{--<span class="item-skill"><span>Adobe Photoshop</span></span>--}}
                {{--</div>--}}
                {{--<div class="skill-div">--}}
                    {{--<span class="item-skill"><span>Adobe Photoshop</span></span>--}}
                {{--</div>--}}

{{--@foreach( $user->skills as $key => $userSkill )--}}
{{--<div class="skill-div">--}}
{{--<span class="item-skill">--}}
{{--<span> {{ $userSkill->skill->getName() }}</span>--}}
{{--</span>--}}
{{--</div>--}}
{{--@endforeach--}}

{{--<div class="clearfix"></div>--}}
{{--<br>--}}
{{--<div class="row">--}}
{{--<div class="col-md-9">--}}
{{--<span class="text-capitalize find-expert-text">{{ trans('common.last_login_time') }}:</span>--}}
{{--<span class="find-expert-text-two">--}}
{{--{{ $user->getLastLogin() }}--}}
{{--</span>--}}
{{--</div>--}}
{{--@if( isset($user->country) )--}}
{{--<div class="col-md-3">--}}
{{--<span class="text-capitalize find-expert-text">--}}
{{--{{ trans('common.country') }}:--}}
{{--</span>--}}
{{--<span>--}}
{{--<img src="{{asset('images/Flags-Icon-Set/24x24/' . $user->country->code.'.png')}}" alt="" style="float:right;">--}}
{{--</span>--}}
{{--</div>--}}
{{--@endif--}}
{{--</div>--}}
                {{--{{ $user->getYearsOld() }}--}}
                {{--{{ trans('member.years_old') }}--}}
                {{--&emsp;--}}

                {{--Experience {{ $user->experience }} {{ trans('member.year') }}--}}
                {{--&emsp;&emsp;--}}

                {{--{{ $user->hourly_pay  }}/hr--}}
                {{--<br/>--}}

                {{--<span class="location">--}}
                    {{--{{ $user->address }}--}}
                {{--</span>--}}
                {{--<br/>--}}
                {{--@if ($user->is_validated == 1)--}}
                {{--<span>--}}
                    {{--Already Authenticate<img src="/images/auth-logo.png" class="img-auth" alt="" >--}}
                {{--</span>--}}
                {{--@endif--}}

                {{--@if ($user->status >= 1)--}}
                    {{--<br/>--}}
                    {{--Status : {{ $user->explainStatus() }}--}}
                {{--@endif--}}

                {{--@if ($user->address != '' )--}}
                    {{--<br/>--}}
                    {{--Map :--}}
                    {{--<a href="{{ route('frontend.map.user', $user->id ) }}">--}}
                        {{--<span class="f-s-20">--}}
                            {{--<i class="fa fa-map-marker"></i>--}}
                        {{--</span>--}}
                    {{--</a>--}}
                {{--@endif--}}

                {{--<ul>--}}
                    {{--@foreach ( $user->skills as $key => $userSkill)--}}
                        {{--<li class="item-skill">{{ $userSkill->skill->getName() }}</li>--}}
                    {{--@endforeach--}}
                {{--</ul>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="text-center user-info col-md-3">--}}
{{--<a href="{{ route('frontend.resume.getdetails', $user->id ) }}">--}}
{{--<img src="{{ $user->getResumeQRCode() }}" class="img-auth p-t-10 m-b-10" alt="" width="150px" height="150px">--}}
{{--</a>--}}
{{--<button type="button" class="btn btn-check-detail" data-url="{{ route('frontend.resume.getdetails', $user->id ) }}" >--}}
{{--{{ trans('common.check_detail') }}--}}
{{--</button>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
