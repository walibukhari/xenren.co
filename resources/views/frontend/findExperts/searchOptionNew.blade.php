<div class="row">
    <div class="col-md-12 search-title-container">
        <h1 class="find-experts-search-title text-uppercase"> {{ trans('common.searchExpert')}} </h1>
    </div>
</div>

<div class="portlet search-box-main find-expert-box-search">
    <div class="row personal-profile">
        {!! Form::open(['route' => ['frontend.findexperts.post','lang' => \Session::get('lang')], 'method' => 'post', 'id' => 'search-form']) !!}
            <div class="portlet-body">
                {{-- name --}}
                <div class="col-md-12 search-box-field">
                        <div class="row">
                            <div class="col-md-9 setcol9FEPPG">
                                <label>{{ trans('common.search') }}</label>
                                <div class="input-group stylish-input-group">
                                    <input name="tbxName" id="tbxName" type="text" value="{{ old('name') }}" placeholder="{{ trans('common.search_freelancer') }}" class="form-control">
                                    <span class="input-group-addon">
                                        <a href="#" id="btn-conform-2" class="send-search">
                                        <button type="button" >
                                            {{--<span class="glyphicon glyphicon-search"></span>--}}
                                            <i class="fa fa-search set-fa-fa-fa-serach" aria-hidden="true"></i>
                                        </button>
                                            </a>
                                </span>
                                </div>
                            </div>
                        <!-- <div class="col-md-3 clear-filter-title clear-filter-title-top clear-filter">
                            {{ trans('common.clear_all_filter') }} <i class="fa fa-times-circle"></i>
                        </div> -->
                            <div class="col-md-3 clear-filter-title clear-filter-title-top clear-filter setclear-filter">
                                {{ trans('common.clear_all_filter') }} <i class="fa fa-times-circle"></i>
                            </div>
                        </div>
                    </div>

                {{-- skills --}}
                <div class="col-md-3 search-box-field">
                    <div class="row">
                        <div class="col-md-12">
                            <label>{{ trans('common.skills') }}</label>
                            <div id="magicsuggest"></div>
                            {{--<input name="skill" type="text" value="" placeholder="Write" class="form-control">--}}
                            <input type="hidden" id="skill_id_list" name="skill_id_list" value="{{ old('skill_id_list') ?: '' }}" >
                        </div>
                    </div>
                </div>

                {{-- job position --}}
                <div class="col-md-3 search-box-field">
                    <label>{{ trans('common.job_position') }}</label>
                    <div class="customize dropdown">
                        <a role="button" data-toggle="dropdown" class="btn custom-select-btn" data-target="#">
                            <label id="job_position_name_label" class="pull-left">
                                {{ old('job_position_name') ?: trans('common.select') }}
                            </label>
                        </a>
                        <ul class="job-position dropdown-menu">
                            <li>
                                <a class="item" data-id="0" data-name="{{ trans('common.select' ) }}">
                                   {{ trans('common.select' ) }}
                                </a>
                            </li>
                            @foreach ($jobPositions as $jobPosition )
                            <li>
                                <a class="item" data-id="{{ $jobPosition->id }}" data-name="{{ $jobPosition->getName() }}">
                                    {{ $jobPosition->getName() }}
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <input type="hidden" id="slcJobPosition" name="slcJobPosition" value="{{ old('job_position_id') ?: '0' }}" />
                    <input type="hidden" id="job_position_name" name="job_position_name" value="{{ old('job_position_name') }}" />
                </div>

                {{-- hourly rate --}}
                <div class="col-md-3 search-box-field">
                    <label>{{ trans('common.hourly_rate') }}</label>
                    <div class="customize dropdown">
                        <a role="button" data-toggle="dropdown" class="btn custom-select-btn" data-target="#">
                            <label id="hourly_range_name_label" class="pull-left">
                                {{ old('hourly_range_name') ?: trans('common.select') }}
                            </label>
                        </a>
                        <ul class="hourly-range dropdown-menu">
                            <li>
                                <a class="item" data-id="0" data-name="{{ trans('common.select' ) }}">
                                   {{ trans('common.select' ) }}
                                </a>
                            </li>
                            @foreach ($hourlyRanges as $key => $hourlyRange )
                            <li>
                                <a class="item" data-id="{{ $key }}" data-name="{{ $hourlyRange }}">
                                    {{ $hourlyRange }}
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <input type="hidden" id="slcHourlyRange" name="slcHourlyRange" value="{{ old('hourly_range_id') ?: '0' }}" />
                    <input type="hidden" id="hourly_range_name" name="hourly_range_name" value="{{ old('hourly_range_name') }}" />
                </div>

                {{--<div class="col-md-3 clear-filter-title clear-filter-title-top clear-filter">--}}
                    {{--{{ trans('common.clear_all_filter') }} <i class="fa fa-times-circle"></i>--}}
                {{--</div>--}}

                {{-- <div class="col-md-3 search-box-field">
                    <label>{{ trans('common.last_activities') }}</label>
                    <div class="search-selectbox">
                        <select class="form-control" name="slcLastActivities" id="slcLastActivities" >
                            <option value="0">{{ trans('common.select') }}</option>
                            @foreach( \App\Constants::getLastActivityLists() as $key => $lastActivity )
                            <option value="{{ $key }}" {{ isset($filter['last_activities_id']) && $filter['last_activities_id'] == $key ? 'selected':''}}>{{ $lastActivity }}</option>
                            @endforeach
                        </select>
                    </div>
                </div> --}}
                {{-- <br> --}}
                <div class="col-md-12 setcol12searchBoxP">
                    <div class="row">
                        <div class="col-md-3 advance-filter-title setAdvanceFT">
                            <a data-toggle="collapse" data-target="#advanceFilterSect" aria-expanded="false" aria-controls="collapseExample" class="advance-filter-collapse">
                                {{ trans('common.advance_filters') }}
                                <i class="fa fa-chevron-circle-down"></i>
                            </a>
                        </div>
                        {{-- <div class="col-md-2 clear-filter-title clear-filter setADCF2">
                            {{ trans('common.clear_all_filter') }} <i class="fa fa-times-circle"></i>
                        </div> --}}
                    </div>
                </div>

                <div class="collapse" id="advanceFilterSect">
                    {{-- review type --}}
                    <div class="col-md-3 mt-10 search-box-field">
                        <label>{{ trans('common.review_type') }}</label>
                        <div class="customize dropdown">
                            <a role="button" data-toggle="dropdown" class="btn custom-select-btn" data-target="#">
                                <label id="review_type_name_label" class="pull-left">
                                    {{ old('review_type_name') ?: trans('common.select') }}
                                </label>
                            </a>
                            <ul class="review-type dropdown-menu">
                                <li>
                                    <a class="item" data-id="0" data-name="{{ trans('common.select' ) }}">
                                       {{ trans('common.select' ) }}
                                    </a>
                                </li>
                                @foreach (\App\Constants::getReviewTypeLists() as $key => $reviewType )
                                <li>
                                    <a class="item" data-id="{{ $key }}" data-name="{{ $reviewType }}">
                                        {{ $reviewType }}
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <input type="hidden" id="slcReviewType" name="slcReviewType" value="{{ old('review_type_id') ?: '0' }}" />
                        <input type="hidden" id="review_type_name" name="review_type_name" value="{{ old('review_type_name') }}" />
                    </div>

                    {{-- job title --}}
                    <div class="col-md-3 mt-10 search-box-field">
                        <label>{{ trans('common.job_title') }}</label>
                        <input name="tbxJobTitle" id="tbxJobTitle" type="text" placeholder="" class="form-control" value="{{ old('job_title') ?: '' }}">
                    </div>

                    {{-- country --}}
                    <div class="col-md-3 mt-10 search-box-field">
                        <label>{{ trans('common.country') }}</label>
                        {{--<div class="search-selectbox">--}}
                            {{--<select class="form-control" name="slcCountry" id="slcCountry" >--}}
                                {{--<option value="0">{{ trans('common.select') }}</option>--}}
                                {{--@foreach( $countries as $key => $country )--}}
                                {{--<option value="{{ $key }}" {{ isset($filter['country_id']) && $filter['country_id'] == $key? 'selected':''}}>{{ $country }}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        <div class="customize dropdown">
                            <a role="button" data-toggle="dropdown" class="btn custom-select-btn" data-target="#">
                                <label id="country_name_label" class="pull-left">
                                    {{ old('country_name') ?: trans('common.select') }}
                                </label>
                            </a>
                            <ul class="country dropdown-menu">
                                <li>
                                    <a class="item" data-id="0" data-name="{{ trans('common.select' ) }}">
                                       {{ trans('common.select' ) }}
                                    </a>
                                </li>
                                @foreach ( $countries as $key => $country  )
                                <li>
                                    <a class="item" data-id="{{ $country->id }}" data-name="{{ $country->name }}">
                                        {{ $country->name }}
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <input type="hidden" id="slcCountry" name="slcCountry" value="{{ old('country_id') ?: '0' }}" />
                        <input type="hidden" id="country_name" name="country_name" value="{{ old('country_name') }}" />
                    </div>

                    {{-- language --}}
                    <div class="col-md-3 mt-10 search-box-field">
                        <label>{{ trans('common.language_2') }}</label>
                        {{--<div class="search-selectbox">--}}
                            {{--<select class="form-control" name="slcLanguage" id="slcLanguage" >--}}
                                {{--<option value="0">{{ trans('common.select') }}</option>--}}
                                {{--@foreach( $languages as $key => $language )--}}
                                {{--<option value="{{ $key }}" {{ isset($filter['language_id']) && $filter['language_id'] == $key? 'selected':''}}>{{ $language }}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        <div id="languageInput" style="margin-top:0px !important;"></div>
                        {{ Form::hidden('hidLanguageId', old('language_id') ?: '', ['id'=>'hidLanguageId']) }}
                    </div>
                    {{-- <div class="col-md-3 search-box-field"></div> --}}
                </div>

                <div class="col-md-12">
                    <a href="#" class="btn btn-search-custom pull-right send-search setSEARCHBOXNEWBTN" id="btn-confirm">
                        {{ trans('common.search') }}
                    </a>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>

{{-- expert result section --}}
<div class="row">
    <div class="col-md-12">
        @if($count <= 1000)
        <span class="find-experts-search-title text-uppercase">
            {{ trans('common.expert') }}
            <small class="experts-counts">({{ $count }})</small>
        </span>
        @else
            <span class="find-experts-search-title text-uppercase">
            {{ trans('common.expert') }}
                <small class="experts-counts">1000++</small>
        </span>
        @endif

        @include('frontend.includes.pagination', ['paginator' => $users])
    </div>
</div>
<br>
