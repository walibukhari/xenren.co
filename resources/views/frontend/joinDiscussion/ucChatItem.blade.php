<li class="in">
    <img class="avatar" alt="" src="{{ $avatar }}">
    <div class="message">
        <a href="javascript:void(0);" class="name">
            {{ $realName }}
        </a>
        <p class="datetime">
            {{ $time }}
        </p>
        <p class="body">
            {{ $message }}
            <br>
            <a href="{{ $file }}" data-lightbox="image-undefined">
                <img src="{{ $file }}" class="img-thumbnail" data-lightbox="image-undefined" width="75px">
            </a>
        </p>
    </div>
</li>