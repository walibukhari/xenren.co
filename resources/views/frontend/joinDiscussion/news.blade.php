@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.official_project_news') }}
@endsection
php
@section('description')
    {{ trans('common.official_project_news') }}
@endsection

@section('author')
    Xenren
@endsection

@section('header')
    <link href="/custom/css/frontend/joinDiscussion.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">
                <div class="col-md-12 usercenter link-div">
                    <a href="{{ route('frontend.joindiscussion.getproject', [ 'slug' => $project->name, 'project_id' => $project->id ]) }}" class="text-uppercase bold raleway">
                        <span class="fa fa-angle-left"></span>
                        {{ trans('common.back_to_join_discussion') }}
                    </a>
                </div>
            </div>
            @include('frontend.flash')

            <div class="portlet box job ">
                <div class="portlet-title">
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12 news">
                            @if( $project->type == \App\Models\Project::PROJECT_TYPE_OFFICIAL && count($officialProjectNewsList) >= 1)
                            <div class="panel panel-default">
                                <div class="panel-heading">{{ trans('common.news')}}</div>
                                <div class="panel-body">
                                    @foreach( $officialProjectNewsList as $key => $officialProjectNews)
                                        @if( $key != 0 )
                                        <hr>
                                        @endif
                                        <div class="row">
                                            <div class="col-md-12 news-des">
                                                {{ $officialProjectNews->content }}
                                            </div>
                                            <div class="col-md-12 news-date">
                                                <span>{{ $officialProjectNews->getDate() }}</span>
                                                {!! $officialProjectNews->getTypeSymbol() !!}
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="panel-footer text-center">

                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection

@section('modal')
@endsection

@section('footer')
@endsection