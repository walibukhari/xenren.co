<!-- Modal -->
<div id="modalChangePostDescription" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            {!! Form::open(['route' => 'frontend.joindiscussion.change', 'id' => 'change-post-description-form', 'method' => 'post', 'class' => 'form-horizontal']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    {{trans('common.description')}}
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4 text-right">
                                <label class="control-label">
                                    {{ trans('common.describe_the_work_to_be_done') }}
                                </label>
                            </div>
                            <div class="col-md-8">
                                <div id="snDescription">
                                    {{$description}}
                                </div>
                                    <input type="hidden" name="description" v-model="applicant.description" class="form-control custom-input-text" id="tbxDescription" >
                                <span class="pull-right left-char-count"><span id="noOfCharDescription">5000</span> {{ trans('common.characters_left') }}</span>
                                </br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btn-change-description-submit" type="button" class="btn btn-success m-b-0"
                        v-on:click="change('description')">
                    {{ trans('common.save_change') }}
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    {{ trans('member.cancel') }}
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

