
@if( $project->type == \App\Models\Project::PROJECT_TYPE_OFFICIAL )
    <div id="sec-official-project" class="portlet box job job-rock official-pro set-projectTypeOffical">
        <div class="portlet-title">
            <div class="row">
                <div class="col-md-12">
                    <div class="span-info-3 col-span-12">
                        <div class="title text-center f-s-14 f-c-green text-uppercase">
                            {{ trans('common.check_details') }}
                        </div>
                    </div>
                </div>
            </div>
            <hr>
        </div>
        <div class="portlet-body">
            <div class="row setJDPOPPPDROW">
                <div class="col-md-3 official-pro-left set-col-3-offical-pro-left">
                    <div class="row">
                        <div class="col-md-12 user-img">
                            <img src="{{ $project->officialProject->getIcon() }}" onerror="this.src='/images/logo_3.png'" width="90%" height="120px">
                        </div>
                        <div class="col-md-12 desc-div">
                            <label class="uppercase set-share-uppercase-officalp">{{ trans('common.shares') }}</label><br/>
                            <div class="price setJDPOPPPRICEA"
                                 >{{ $project->officialProject->stock_total }}</div>
                            <span>{{ trans('common.issued') }}: {{ $project->officialProject->stock_share }}%</span>
                        </div>
                        <div class="col-md-12 btn-div">
                            <a href="{{ $project->getContract() }}" target="_blank">
                                <button class="btn btn-color-custom uppercase">
                                    {!! trans('common.download_basic_agreement') !!}
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 official-pro-right">
                    <div class="portlet-title">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-span-12">
                                    <div class="f-c-green title-green-c pull-left raleway f-s-24 f-m">
                                        <img src="/images/badge.png">
                                        {{ $project->officialProject->title }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 price-div top-info f-c-gray">
                                <div class="row p-t-10">
                                    <div class="col-md-4">
                                    <span>{{ trans('common.project_type') }}:
                                        <span class="f-b">
                                            {{ trans('common.official_project') }}
                                        </span>
                                    </span>
                                    </div>
                                    <div class="col-md-4">
                                    <span>{{ trans('common.time_frame') }}:
                                        <span class="f-b">
                                            {{ $project->officialProject->getTimeFrame() }} days
                                        </span>
                                    </span>
                                    </div>
                                    <div class="col-md-4">
                                    <span>{{ trans('common.end_date') }}:
                                        <span class="f-b">
                                            {{ $project->officialProject->getRecruitEndDate() }}
                                        </span>
                                    </span>
                                    </div>
                                </div>

                                <div class="row p-tb-15">
                                    @if( $project->officialProject->budget_switch == 1)
                                        <div class="col-md-4">
                                    <span>{{ trans('common.budget') }}:
                                        <span class="f-b">
                                            ${{ $project->officialProject->budget_from }}
                                            - ${{ $project->officialProject->budget_to }}
                                        </span>
                                    </span>
                                        </div>
                                    @endif

                                    <div class="col-md-4">
                                    <span>{{ trans('common.funding_stage') }}:
                                        <span class="f-b">
                                            {{ \App\Constants::translateFundingStage($project->officialProject->funding_stage) }}
                                        </span>
                                    </span>
                                    </div>
                                    <div class="col-md-4">
                                    <span>{{ trans('common.location') }}:
                                        <span class="f-b">
                                            {{$project->getLocation()}}
                                        </span>
                                    </span>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12 description set-col-12-officalpdesc">
                                {{ $project->officialProject->description }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <img src="/images/signal.png">
                                <div class="label">
                                <span class="uppercase">
                                    {{ trans('common.difficulty') }}
                                </span>
                                </div>
                                <div class="difficulty-star p-t-10">
                                    @for ($i = 0; $i < $project->officialProject->project_level; $i++)
                                        <i class="fa fa-star"></i>
                                    @endfor

                                    @for ($i = 0; $i < ( 6 - $project->officialProject->project_level ); $i++)
                                        <i class="fa fa-star-o"></i>
                                    @endfor
                                </div>
                            </div>
                            <div class="col-md-4">
                                <img src="/images/user-secret.png">
                                <div class="label">
                                <span class="uppercase">
                                    {{ trans('common.position_needed') }}
                                </span>
                                </div>
                                <div class="position p-t-10">
                                <span class="f-c-green f-sb">
                                @foreach( $project->officialProject->projectPositions as $key => $projectPosition)
                                        @if( $key != 0 )
                                            {{ ", " }}
                                        @endif

                                        {{  $projectPosition->jobPosition->getName() }}
                                    @endforeach
                                    <span>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <img src="/images/hands.png">
                                <div class="label">
                                <span class="uppercase">
                                    {{ trans('common.skill_request') }}
                                </span>
                                </div>
                                <div class="skill-div skill-main p-t-20">
                                    @foreach( $project->projectSkills as $projectSkill)
                                        <span class="item-skill">
                                    <span>{{ $projectSkill->getSkillName( $projectSkill->skill_id ) }}</span>
                                </span>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="row p-t-30">
                            <div class="col-md-12">
                                <img src="/images/info.png">
                                <div class="label">
                                <span class="uppercase">
                                    {{ trans('common.project_info') }}
                                </span>
                                </div>
                            </div>
                            <div class="col-md-12 f-c-gray p-t-10 row">
                                <div class="col-md-3">
                                    <span class="uppercase">{{ trans('common.project_status') }}</span>: <span
                                            class="f-sb">{{$project->getStage()}}</span>
                                </div>
                                <div class="col-md-3">
                                    <span class="uppercase">{{ trans('common.project_stage') }}</span>: <span
                                            class="f-sb">A</span>
                                </div>
                                <div class="col-md-3">
                                    <span class="uppercase">{{ trans('common.project_value') }}</span>: <span
                                            class="f-sb">{{$project->getValue()}}</span>
                                </div>
                                <div class="col-md-3">
                                    <span class="uppercase">{{ trans('common.attandance') }}</span>: <span class="f-sb">
                                        {{$project->getApplicantsCount()}}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endif
