<!-- Modal -->
<div id="modalChangePostPrice" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            {!! Form::open(['route' => 'frontend.joindiscussion.change', 'id' => 'change-user-detail-form', 'method' => 'post', 'class' => 'form-horizontal']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    {{trans('common.make_offer')}}
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-body">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label" id="label-div-price-fix">
                                @if( app()->getLocale() == "en" )
                                    {{ trans('common.price_usd') }}
                                @elseif( app()->getLocale() == "cn" )
                                    {{ trans('common.price_cny') }}
                                @endif
                            </label>
                        </div>
                        <div class="col-md-8">
                            <input class="form-control" name="reference_price" id="price" placeholder="{{ trans('common.input_hourly_pay') }}" v-model="applicant.reference_price">
                        </div>

                        <div class="col-md-4">
                            <select class="form-control" name="reference_price_type" id="price_type" v-model="applicant.reference_price_type" required>
                                <option value="1">{{ trans('common.pay_by_the_hour') }}</option>
                                <option value="2">{{ trans('common.pay_a_fixed_price') }}</option>
                                <option value="3">{{ trans('common.make_offer') }}</option>
                            </select>

                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button id="btn-change-rate-submit" type="button" class="btn btn-success m-b-0"
                        v-on:click="change('reference_price')">
                    {{ trans('common.save_change') }}
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    {{ trans('member.cancel') }}
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

