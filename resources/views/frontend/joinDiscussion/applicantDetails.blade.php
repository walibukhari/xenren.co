<div class="portlet box">
    <div class="row personal-profile">
        <div class="profile-image col-md-2">
            <div class="profile-userpic p-t-5">
                <img src="{{ $user->getAvatar() }}" class="img-responsive setJDPADAREAIMAGE" alt=""/>
            </div>
        </div>
        <div class="user-info col-md-7">
            <div class="basic-info">
                <span class="f-b">
                    {{ $user->getName() }}&emsp;
                </span>
                @if( $user->title != null && $user->job_position !== null )
                {{ $user->title }} - <span class="job-remark" title="{{ $user->job_position->getRemark() }}">{{ $user->job_position->getName() }}</span>
                @endif
                <br/>

                {{ $user->age }}
                {{ trans('member.years_old') }}
                &emsp;

                Experience {{ $user->experience }} {{ trans('member.year') }}
                &emsp;&emsp;

                {{ $user->hourly_pay  }}/hr
                <br/>

                <span class="location">
                    {{ $user->address }}
                </span>
                <br/>
                @if ($user->is_validated == 1)
                <span>
                    Already Authenticate<img src="/images/auth-logo.png" class="img-auth" alt="" >
                </span>
                @endif

                @if ($user->status >= 1)
                    <br/>
                    Status : {{ $user->explainStatus() }}
                @endif

                @if ($user->address != '' )
                    <br/>
                    Map :
                    <a href="{{ route('frontend.map.user', $user->id ) }}">
                        <span class="f-s-20">
                            <i class="fa fa-map-marker"></i>
                        </span>
                    </a>
                @endif

                <ul>
                    @foreach ( $user->skills as $key => $userSkill)
                        <li class="item-skill">{{ $userSkill->skill->getName() }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="user-info col-md-3">
            <div class="text-center">
                @if(! $candidateAlreadySelected)
                    <button id="btn-choose-{{ $user->id }}" type="button" class="btn btn-success">
                        Choose
                    </button>
                    <br/>
                @endif

                @if( $candidateUserId == $user->id )
                    <button id="" type="button" class="btn btn-success">
                        Selected
                    </button>
                    <br/>
                @endif

                <img src="/xenren-assets/images/icon/qrcode.png" class="img-auth" alt="" width="100px" height="100px">
            </div>
        </div>
    </div>
</div>