@extends('frontend.layouts.default')

@section('title')
    Join Discussion
@endsection

@section('description')
    Join Discussion
@endsection

@section('author')
    Xenren
@endsection

@section('header')
    <link href="/assets/global/plugins/bootstrap-star-rating/css/star-rating.css" media="all" rel="stylesheet"
          type="text/css"/>
    <link href="/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css"/>
    <link href="/custom/css/frontend/findExperts.css" rel="stylesheet" type="text/css"/>
    <link href="/custom/css/frontend/joinDiscussion.css" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">
                <div class="col-md-12 usercenter link-div">
                    <a href="" class="text-uppercase raleway"><span class="fa fa-angle-left"></span> back to search
                        result</a>
                </div>
            </div>
            @include('frontend.flash')

            <div class="portlet box job div-job-{{ $project->id }} div-jobDiscussion-main">
                <div class="portlet-title">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="span-info-3 col-span-12">
                                        <div class="project-name pull-left">{{ $project->name }}</div>
                                        <span class="fa fa-star pull-right"></span>
                                        <button type="button" class="btn pull-right btn-oficial-project">
                                            Official Project
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 price-div">
                                    @if( $project->pay_type == \App\Constants::PAY_TYPE_FIXED_PRICE )
                                        <span class="top-smoll">
                                            Fixed Price:
                                            <span class="price">
                                                <i class="fa fa-cny" aria-hidden="true"></i>
                                                {{ $project->reference_price }} .
                                            </span>
                                        </span>
                                    @elseif ( $project->pay_type == \App\Constants::PAY_TYPE_HOURLY_PAY )
                                        <span class="top-smoll">
                                            Hourly Pay:
                                            <span class="price">
                                                <i class="fa fa-cny" aria-hidden="true"></i>
                                                {{ $project->reference_price }}.
                                            </span>
                                        </span>
                                    @else
                                        <span class="top-smoll">
                                            Request Quotation
                                        </span>
                                    @endif
                                    <span class="top-smoll">Project Type: <span class="type">Public.</span></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn pull-right btn-make-offer"
                                    id="btn-make-offer-{{ $project->id }}" data-target="#modalMakeOffer"
                                    data-toggle="modal">
                                Make Offer
                            </button>
                            <br>
                            <br>
                            <button type="button" class="btn btn-share-pro pull-right">
                                Share this project
                            </button>
                        </div>
                    </div>
                    <hr>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-9 portlet-body-title">
                            <h5>Description</h5>
                        </div>

                        <div class="col-md-9 description">
                            {{ $project->description }}
                        </div>

                        <div class="col-md-9 portlet-body-title">
                            <h5>skill request</h5>
                        </div>

                        <div class="col-md-9 skill-main">
                            <div class="skill-div">
                                @foreach( $project->projectSkills as $key => $projectSkill )
                                    <span class="item-skill"><span>{{ $projectSkill->getSkillName( $projectSkill->skill_id ) }}</span></span>
                                @endforeach
                            </div>
                        </div>

                        <div class="col-md-9 portlet-body-title">
                            <h5>attachment <span>Download All (2)</span></h5>
                        </div>

                        <div class="col-md-9 attachment-main">
                            <div class="attachment-sub">
                                <div class="attachment-file">
                                    <a href=""><img src="/images/doc.jpg"></a>
                                </div>
                                <div class="attachment-name">
                                    <a href="">test1.doc</a>
                                </div>
                            </div>
                            <div class="attachment-sub">
                                <div class="attachment-file">
                                    <a href=""><img src="/images/xls.jpg"></a>
                                </div>
                                <div class="attachment-name">
                                    <a href="">longte..xls</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-9 posted-by">
                            <img alt="" class="img-circle" src="{{ $project->creator->getAvatar() }}">
                            <span>Posted 5 minute ago by Martin</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box job div-job-{{ $project->id }}">
                <div class="portlet-body main-tabs-div">
                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs ">
                                <li class="active">
                                    <a href="#tab_default_1" data-toggle="tab">Discuss Room</a>
                                </li>
                                <li>
                                    <a href="#tab_default_2" data-toggle="tab">Freelancer Detail</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_default_1">

                                    <div class="row form-group freelance-detail-section" style="display:none">
                                        <div class="col-md-12">
                                            @if( $projectApplicants != null )
                                                @foreach( $projectApplicants as $key => $projectApplicant)
                                                    @include('frontend.joinDiscussion.applicantDetails', [
                                                        'user' => $projectApplicant->user,
                                                        'candidateAlreadySelected' => $candidateAlreadySelected,
                                                        'candidateUserId' => $candidateUserId
                                                    ])
                                                @endforeach

                                                {!! $projectApplicants->render() !!}
                                            @endif
                                        </div>
                                    </div>

                                    {{-- chat messages --}}
                                    <div class="row form-group chat-section" style="display:block">
                                        <div class="col-md-8 chat-room-div" id="chat">
                                            <h5 class="title">chat room</h5>
                                            @include('frontend.includes.projectChatMessage2',[
                                                'projectId' => $project->id,
                                                'projectChatRoomId' => $projectChatRoomId
                                            ])
                                        </div>
                                        {{--chat room Members --}}
                                        <div class="col-md-4">
                                            <h5 class="title">Chat Room Members (3)</h5>
                                            <div class="portlet light bordered chat-room-memb-div">
                                                <div class="portlet-body">
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="tab_actions_pending">
                                                            <!-- BEGIN: Actions -->
                                                            <div class="mt-actions">
                                                                @foreach( $projectChatFollowers as $key => $projectChatFollower )
                                                                    <div class="mt-action">
                                                                        <div class="mt-action-img">
                                                                            <img alt="" class="img-circle"
                                                                                 src="{{ $projectChatFollower->user->getAvatar() }}"
                                                                                 style="width: 55px; height: 55px;">
                                                                        </div>
                                                                        {{--project owner --}}
                                                                        @if( $projectChatFollower->user->id == $project->user_id )
                                                                            <div class="mt-action-body">
                                                                                <div class="mt-action-row">
                                                                                    <div class="mt-action-info ">
                                                                                        <div class="mt-action-details ">
                                                                                            <span class="mt-action-author">{{ $projectChatFollower->user->getName() }}
                                                                                                harshad pathak <span
                                                                                                        class="fa fa-check-circle"></span></span>
                                                                                            <p class="mt-action-desc">
                                                                                                Project Creator</p>
                                                                                            <a href=""
                                                                                               class="more">More</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @else
                                                                            {{--project folower--}}
                                                                            <div class="mt-action-body">
                                                                                <div class="mt-action-row">
                                                                                    <div class="mt-action-info ">
                                                                                        <div class="mt-action-details ">
                                                                                            <span class="mt-action-author">{{ $projectChatFollower->user->getName() }}
                                                                                                <span class="fa fa-check-circle"></span></span>
                                                                                            <p class="mt-action-desc">
                                                                                                Skill List</p>
                                                                                            <a href=""
                                                                                               class="more">More</a>
                                                                                        <!-- @if( $projectChatFollower->user->getProjectApplicant() == null )
                                                                                            <p class="mt-action-desc">Still not give any quote price</p>
                                                                                            @else
                                                                                            <p class="mt-action-desc">
                                                                                                Price : {{ $projectChatFollower->user->getProjectApplicant()->quote_price }} RMB<br/>
                                                                                        About Me : {{ $projectChatFollower->user->getProjectApplicant()->getAboutMe() }}
                                                                                                    </p>
                                                                                                    @endif -->
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                            <!-- END: Actions -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- detail freelancer--}}
                                <div class="tab-pane" id="tab_default_2">
                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            Attachment :
                                            @if( $project->getAttachment() != ''  )
                                                <a href="{{ $project->getAttachment() }}">File Download</a>
                                            @else
                                                no attachment
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

    @include('frontend.includes.modalMakeOffer')
    {!! Form::hidden('info', $project->projectQuestions->toJson(), ['class'=>'question-'. $project->id ]) !!}
    {!! Form::hidden('line-id', $user->line_id, ['class'=>'line-id' ]) !!}
    {!! Form::hidden('wechat-id', $user->wechat_id, ['class'=>'wechat-id' ]) !!}
    {!! Form::hidden('skype-id', $user->skype_id, ['class'=>'skype-id' ]) !!}
    {!! Form::hidden('handphone-no', $user->handphone_no, ['class'=>'handphone-no' ]) !!}

    {!! Form::hidden('project_id', $project->id, [ 'id' => 'projectId'] ) !!}
    {!! Form::hidden('choose_url', route('frontend.joindiscussion.choosecandidate'), [ 'id' => 'choose_url'] ) !!}
@endsection

@push('vue')
<script src="/custom/js/frontend/joinDiscussion.js" type="text/javascript"></script>
<script src="{{asset('js/socket.io.min.js')}}" type="text/javascript"></script>

<script>
    var socket = io('https://xenren.co:3000');
    var room = {!! $projectChatRoomId !!};
    var project_id = {!! $project->id !!};
    {{--var user = {!! $user !!}--}}

</script>
<script src="{{asset('js/project-chat.js')}}" type="text/javascript"></script>
@endpush