@extends('frontend.joinDiscussion.ajaxmodal')

@section('title')
    {{trans('common.more_details')}}
@endsection

@section('script')
    <script>
        +function () {
            $(document).ready(function () {
                $('.btn-reject').click(function (e) {
                    e.preventDefault();
                    var url = $(this).data('url');
                    $.ajax({
                        url: url,
                        dataType: 'json',
                        method: 'post',
                        success: function (result) {
                            if (result.status == "success") {
                                alertSuccess(lang.submit_successful);
                                setTimeout(function () {
                                    location.reload();
                                }, 1000);
                            }
                        },
                        error: function (a, b) {
                        }
                    });
                });

                $('.btn-award').click(function (e) {
                    e.preventDefault();
                    var url = $(this).data('url');
                    $.ajax({
                        url: url,
                        dataType: 'json',
                        method: 'post',
                        success: function (result) {
                            if (result.status == "success") {
                                alertSuccess(lang.submit_successful);
                                setTimeout(function () {
                                    location.reload();
                                }, 1000);
                            }
                        },
                        error: function (a, b) {
                        }
                    });
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <!--Only project creator can do the below action-->
    {{--@if( $projectApplicant->project->user_id == \Auth::id() )--}}
        {{--<div class="pull-left">--}}
            {{--<button class="btn btn-award"--}}
                    {{--data-url="{{ route('frontend.joindiscussion.awardjob',  ['applicant_id' => $projectApplicant->id ]) }}"--}}
                    {{--style="min-width: 100px">--}}
                {{--{{ trans('common.award_job') }}--}}
            {{--</button>--}}

            {{--<button class="btn btn-reject"--}}
                    {{--data-url="{{ route('frontend.joindiscussion.reject',  ['applicant_id' => $projectApplicant->id ]) }}"--}}
                    {{--style="min-width: 100px">--}}
                {{--{{ trans('common.reject') }}--}}
            {{--</button>--}}

        {{--</div>--}}
        {{--<div class="pull-right">--}}
            {{--<a href="{{ route('frontend.joindiscussion.sendmessage', ['applicant_id' => $projectApplicant->id ]) }}"--}}
               {{--data-toggle="modal" data-target="#remote-modal" data-dismiss="modal">--}}
                {{--<img src="/images/icons/ic_msg-copy.png">--}}
            {{--</a>--}}
        {{--</div>--}}
    {{--@endif--}}
@endsection

@section('content')
    {!! Form::open(['method' => 'post', 'role' => 'form', 'id' => 'send-message-form']) !!}
    <div class="form-body">
        <div class="form-group">
            <div id="model-alert-container">
            </div>
        </div>

        <div class="form-group col-md-12" style="padding: unset;">
            <div class="col-md-9">
                <div class="col-md-1" style="padding: unset;">
                    <img class="img-circle" alt=""
                         src="{{ $projectApplicant->user->getAvatar() }}"
                         width="50px" height="50px">
                </div>

                <div class="col-md-11">
                <div class="col-md-6">
                    <span class="full-name raleway">
                        {{ $projectApplicant->user->getName() }}

                        @if( $projectApplicant->user->getLatestIdentityStatus() == \App\Models\UserIdentity::STATUS_APPROVED )
                            <i class="fa fa-check-circle"></i>
                        @endif
                    </span>

                    <span class="send-message-sect">
                        <a href="{{ route('frontend.joindiscussion.sendmessage', ['applicant_id' => $projectApplicant->id ]) }}"
                           data-toggle="modal" data-target="#remote-modal" data-dismiss="modal">
                            <img src="/images/icons/ic_msg-copy.png" style="width: 23px;">
                        </a>
                    </span>
                    <br/>

                    <span class="job_position">
                        {{ $projectApplicant->user->getJobPosition() }}
                    </span>
                </div>
                <div class="col-md-6">
                    <!--Only project creator can view all the price-->
                    <!--Applicant can only view their own submit price-->
                    @if( $projectApplicant->user_id == \Auth::id() || $projectApplicant->project->user_id == \Auth::id() )
                        <span class="top-smoll pull-right div-price">
                            @if( $projectApplicant->project->pay_type == \App\Constants::PAY_TYPE_FIXED_PRICE )
                                {{ trans('common.fixed_price') }}:
                            @elseif ( $projectApplicant->project->pay_type == \App\Constants::PAY_TYPE_HOURLY_PAY )
                                {{ trans('common.pay_hourly') }}:
                            @endif
                            <span class="price">
                                <i class="fa fa-usd" aria-hidden="true"></i>
                                {{ $projectApplicant->quote_price }}
                            </span>
                        </span>
                    @endif
                </div>
            </div>
                <br/><br/>

                <div class="row col-md-12 p-t-20">
                    <label class="bold">{{ trans('common.about_me') }}</label>
                    <div style="min-height: 100px;">
                        {{ $projectApplicant->user->getAboutMe() }}
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                @include('frontend.joinDiscussion.ucThumbStatus', [ 'projectChatFollower' =>$projectChatFollower ])

                {{--<div class="col-md-12">--}}
                    {{--<a class="btn btn-thumb active">--}}
                        {{--<i class="fa fa-thumbs-o-up"></i>--}}
                    {{--</a>--}}
                    {{--<a class="btn btn-thumb">--}}
                        {{--<i class="fa fa-thumbs-o-down"></i>--}}
                    {{--</a>--}}
                {{--</div>--}}
                <div class="col-md-12 details-container-s">
                    <span class="title clearfix">{{ trans('common.age') }}</span>
                    <span class="definition">
                        {{ $projectApplicant->user->age }}
                    </span>
                </div>
                <div class="col-md-12 details-container-s">
                    <span class="title clearfix">{{ trans('common.experience_year') }}</span>
                    <span class="definition">
                        {{ $projectApplicant->user->getExperience() }} {{ trans_choice('common.years', $projectApplicant->user->getExperience()) }}
                    </span>
                </div>
                <div class="col-md-12 details-container-s">
                    <span class="title clearfix">{{ trans('common.project_completed') }}</span>
                    <span class="definition">0</span>
                </div>
                <div class="col-md-12 p-t-10">
                    <input type="button" class="btn btn-award" value="{{ trans('common.award_job') }}"
                        {{--data-url="{{ route('frontend.joindiscussion.awardjob',  ['applicant_id' => $projectApplicant->id ]) }}"--}}
                        data-url="/joinDiscussion/awardJob/{{$projectApplicant->id}}"
                        style="min-width: 100px">

                    </input>
                </div>
                <div class="col-md-12 p-t-10">
                    <button class="btn btn-reject"
                        data-url="{{ route('frontend.joindiscussion.reject',  ['applicant_id' => $projectApplicant->id ]) }}"
                        style="min-width: 100px">
                        {{ trans('common.reject') }}
                    </button>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <hr/>
        </div>

        @foreach ( $projectApplicant->project->projectQuestions as $key => $projectQuestion)
        <div class="form-group">
            <div class="col-md-12 p-t-10">
                <div class="col-md-12 questions-info">
                    <span class="uppercase bold">{{ trans('common.question') }} {{$key + 1 }}:</span><br>
                    <div class="question-div">
                        {{ $projectQuestion->question }}
                    </div>
                    <div class="answer-sect">
                        <span class="uppercase bold">{{ trans('common.answer') }}</span>:<br/>
                        <div class="answer-div">
                            {{ $projectApplicant->getAnswer($projectQuestion->id) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach

        {{--@foreach ( $projectApplicant->project->projectQuestions as $key => $projectQuestion)--}}
            {{--<div class="form-group">--}}
                {{--<label class="bold">{{ trans('common.question') }} {{$key + 1 }}--}}
                    {{--: {{ $projectQuestion->question }}</label>--}}
                {{--<div>--}}
                    {{--{{ $projectApplicant->getAnswer($projectQuestion->id) }}--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--@endforeach--}}

        <div class="form-group">
            <div class="col-md-12 p-t-20 content-group">
                <label class="bold">{{ trans('common.contact') }} : </label>

                <div class="col-md-12 contact-info">
                    <div class="col-md-6 contact-info-item">
                        <img src="/images/Tencent_QQ.png" width="25px" height="25px">
                        <span>
                                {{ $projectApplicant->user->qq_id }}
                            </span>
                    </div>

                    <div class="col-md-6 contact-info-item">
                        <img src="/images/skype-icon-5.png" width="25px" height="25px">
                        <span>
                                {{ $projectApplicant->user->skype_id }}
                            </span>
                    </div>

                    <div class="col-md-6 contact-info-item">
                        <img src="/images/wechat-logo.png" width="25px" height="25px">
                        <span>
                                {{ $projectApplicant->user->wechat_id }}
                            </span>
                    </div>

                    <div class="col-md-6 contact-info-item">
                        <img src="/images/MetroUI_Phone.png" width="25px" height="25px">
                        <span>
                                {{ $projectApplicant->user->handphone_no }}
                            </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-12 p-t-20">
                <label class="bold">{{ trans('common.portfolio') }} : </label>

                @include('frontend.userCenter.comment', [
                    'is_allow_edit_info' => false,
                    'adminReviews' => $adminReviews,
                    'reviews' => $reviews,
                    'job_positions' => $job_positions,
                    'user' => $projectApplicant->user,
                    'portfolios' => $portfolios
                ])
            </div>
        </div>

        {{--<div class="form-group col-md-12" style="padding:unset;">--}}
            {{--<hr>--}}
        {{--</div>--}}
    </div>
    {!! Form::close() !!}
@endsection
