<!-- Modal -->
<div id="modalChangePostSkill" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            {!! Form::open(['route' => 'frontend.joindiscussion.change', 'id' => 'change-post-skill-form', 'method' => 'post', 'class' => 'form-horizontal']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    {{trans('common.add_skill')}}
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-body">
                    <div class="form-group">
                        <div class="col-md-12" id="add-user-status-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label setJDPMODALCPDLABEL">
                            {{trans('common.skill')}}
                        </label>
                        <div class="col-md-9">
                            <div id="msSkills"></div>
                            <input type="hidden" name="skill_id_string" v-model="applicant.skill_id_string" id="skill_id_list">
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btnChangeSkill btn-green-bg-white-text" v-on:click="change('skill_id_string')" >
                    {{ trans('common.save_change') }}
                </button>
                <button type="button" class="btn btn-red-bg-white-text" data-dismiss="modal">
                    {{ trans('member.cancel') }}
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

