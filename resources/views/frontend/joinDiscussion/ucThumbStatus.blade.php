<div class="col-md-12 btn-thumbs">
    <button type="button" class="btn-thumb-up-{{ $projectChatFollower->id }} btn {{ $projectChatFollower->thumb_status == \App\Models\ProjectChatFollower::THUMB_STATUS_THUMB_UP? "btn-thumbs-color": "btn-thumbs-default"}}"
        data-url = "{{ route('frontend.joindiscussion.thumbup.post', ['chat_follower_id' => $projectChatFollower->id ]) }}"
        data-id = "{{ $projectChatFollower->id }}">
        <i class="fa fa-thumbs-o-up"></i>
    </button>
    <button type="button" class="btn-thumb-down-{{ $projectChatFollower->id }} btn {{ $projectChatFollower->thumb_status == \App\Models\ProjectChatFollower::THUMB_STATUS_THUMB_DOWN? "btn-thumbs-color": "btn-thumbs-default"}}"
        data-url = "{{ route('frontend.joindiscussion.thumbdown.post', ['chat_follower_id' => $projectChatFollower->id ]) }}"
        data-id = "{{ $projectChatFollower->id }}">
        <i class="fa fa-thumbs-o-down"></i>
    </button>
</div>