<!-- Modal -->
<div id="modalChangePostTitle" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            {!! Form::open(['route' => 'frontend.joindiscussion.change', 'id' => 'change-post-title-form', 'method' => 'post', 'class' => 'form-horizontal']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    {{trans('common.project_title')}}
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">
                            {{trans('common.title')}}
                        </label>
                        <div class="col-md-9">
                            <input class="form-control" name="title" v-model="applicant.title" />
                        </div>
                    </div>


                </div>
            </div>
            <div class="modal-footer">
                <button id="btn-change-title-submit" type="button" class="btn btn-success m-b-0"
                        v-on:click="change('title')">
                    {{ trans('common.save_change') }}
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    {{ trans('member.cancel') }}
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

