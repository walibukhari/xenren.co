@extends('frontend.layouts.default')

@section('title')
    {{ $project->name }} || {{ $project->name }}
@endsection

@section('keywords')
    {{ trans('common.jobsDis_keyword') }}
@endsection

@section('description')
    <?php
    echo trans('common.jobsDis_desc');
    if (trans('common.lang') == 'en') {
        echo preg_replace('/[^A-Za-z0-9\-]/', ' ',strip_tags($project->name)).'.'.preg_replace('/[^A-Za-z0-9\-]/', ' ',strip_tags($project->description));
    } else {
        echo preg_replace('/[^A-Za-z0-9\-]/', ' ' ,strip_tags($project->name)).'.'.preg_replace('/[^A-Za-z0-9\-]/', ' ',$project->description);
    }
    ?>
@endsection

@section('author')
    Xenren
@endsection

@section('url')
    https://www.xenren.co/joinDiscussion/getProject/
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('ogtitle')
    {{ empty($applicant->title)?$project->name:$applicant->title }}
@endsection

@section('ogdescription')
    {{strip_tags($project->description)}}
@endsection

@section('header')
{{--    <link href="/assets/global/plugins/bootstrap-star-rating/css/star-rating.css" media="all" rel="stylesheet"--}}
{{--          type="text/css"/>--}}
{{--    <link href="/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css"/>--}}
{{--    <link href="/custom/css/frontend/findExperts.css" rel="stylesheet" type="text/css"/>--}}
{{--    <link href="/custom/css/frontend/joinDiscussion.css" rel="stylesheet" type="text/css"/>--}}
{{--    <link href="/assets/global/plugins/lightbox2/dist/css/lightbox.min.css" rel="stylesheet" type="text/css"/>--}}
{{--    <link href="/custom/css/frontend/modalMakeOffer.css" rel="stylesheet" type="text/css"/>--}}
{{--    <link href="/custom/css/frontend/modalMakeOfferDetail.css" rel="stylesheet" type="text/css"/>--}}
{{--    <link href="/custom/css/frontend/personalPortfolio.css" rel="stylesheet" type="text/css"/>--}}
{{--    <link href="/custom/css/frontend/skill_review_review_portfolio.css" rel="stylesheet" type="text/css"/>--}}
{{--    <link href="/custom/css/frontend/modalSendPrivateMessage.css" rel="stylesheet" type="text/css"/>--}}
{{--    <link href="/assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css"/>--}}
{{--    <link href="{{asset('css/100-coworkers.css')}}" rel="stylesheet">--}}
    <link href="{{asset('css/JoinDiscussion.css')}}" rel="stylesheet" type="text/css">
    <style>
        /*tooltip*/
        .set-tooltip {
            position: relative;
            display: inline-block;
        }

        .tabbable-panel{
            box-shadow: 0px 1px 2px 0px #e9e9e9 !important;
            border: 1px solid #e9e9e9 !important;
        }

        .set-projects-color{
            color:#b0b0b0 !important;
        }

        .set-class-filters{
            color:#4c4c4c !important;
            word-spacing: 10px;
        }

        .setNAVNAVTABS{
            background-color: #fafafa !important;
        }

        .set-tooltip .tooltiptext {
            visibility: hidden;
            width: 120px;
            background-color: #fff;
            color: #C9C8C9;
            text-align: center;
            border-radius: 6px;
            padding: 5px 0;
            position: absolute;
            z-index: 1;
            bottom: 95%;
            left: 50%;
            margin-left: -60px;
            opacity: 0;
            transition: opacity 0.3s;
        }

        .set-tooltip .tooltiptext::after {
            content: "";
            position: absolute;
            top: 100%;
            left: 50%;
            margin-left: -5px;
            border-width: 5px;
            border-style: solid;
            border-color: #C9C8C9 transparent transparent transparent;
        }

        .set-tooltip:hover .tooltiptext {
            visibility: visible;
            opacity: 1;
        }
        /*tooltip*/

        .set-content-change-status-popup{
            margin-bottom: 0px !important;
            margin-top: 0px !important;
            top: -23px !important;
            position: relative;
        }
        .newMsgDiv {
            background: gainsboro;
            height: 29px;
            width: 140px;
            text-align: center;
            padding-top: 4px;
            border-radius: 7px;
            position: absolute;
            top: 94%;
            right: 50%;
            cursor: pointer;
        }
        @media screen and (max-width: 1000px){
            #btn-official-project-project{
                top: 40px;
            }
        }
        @media screen and (max-width: 551px){
            #btn-official-project-project{
                top: 10px;
            }
        }
        @media screen and (max-width: 400px){
            #btn-official-project-project{
                top: 15px;
            }
        }
        @media screen and (max-width: 993px){
            #message-icon{
                top: 5px;
            }
        }
        @media screen and (max-width: 993px){
            #message-icon-2{
                top: -6px;
                left:55px;
            }
        }
        @media screen and (max-width: 993px){
            #quit-interview-1{
                top: 3px;
            }
        }
    </style>
    <script>
        window.newMsgCount = 0;
        {{--var pusher = new Pusher("{{config('pusher.connections.main.auth_key')}}");--}}
        {{--var channel = pusher.subscribe('project_chat');--}}
        {{--channel.bind('{{$projectChatRoomId}}', function (data) {--}}
        {{--window.projectChatRoomId = '{{$projectChatRoomId}}';--}}
        {{--if (data.sender_id != '{{\Auth::user()->id}}') {--}}
        {{--window.newMsgCount++;--}}
        {{--$('.chats').append('<div class="newMsgDiv" onClick="reloadChat()">' + window.newMsgCount + ' new messages</div>');--}}
        {{--} else {--}}
        {{--showChat(data);--}}
        {{--}--}}
        {{--});--}}

        {{--function reloadChat() {--}}
        {{--window.newMsgCount = 0;--}}
        {{--showChat();--}}
        {{--}--}}

        function showChat() {
            var form = new FormData();
            form.append("message", $('#message').val());
            form.append("channel", 'project_chat');
            form.append("event", window.projectChatRoomId);

            var settings = {
                "url": "/projectChat?channel=project_chat&projectChatRoomId={{$projectChatRoomId}}",
                "method": "GET",
                "data": form,
                "processData": false,
                "contentType": false,
                "mimeType": "multipart/form-data",
            }

            $.ajax(settings).done(function (response) {
                $('.chats').html(response);
                if (window.newMsgCount > 0) {
                    $('.chats').append('<div class="newMsgDiv">' + window.newMsgCount + ' new messages</div>');
                }
            });
        }

        function sendMessage() {
            var form = new FormData();
            form.append("message", $('#message').val());
            form.append("channel", 'project_chat');
            form.append("event", '{{$projectChatRoomId}}');

            var settings = {
                "url": "/sendMessage?channel=project_chat&projectChatRoomId={{$projectChatRoomId}}",
                "method": "POST",
                "data": form,
                "processData": true,
                "contentType": true,
                "mimeType": "multipart/form-data",
            }

            $.ajax(settings).done(function (response) {
                $('.chats').html(response);
                $('#message').val('')
            });
        }
    </script>
@endsection

@section('content')

    <style>
        .setJDButton{
            text-align:center;
            position:relative;
            top: -40px;
        }

        @media (max-width:480px) and (min-width:391px)
        {
            .setJDButton button{
                margin-top:0px !important;
                float:none !important;
                width:81% !important;
            }
        }
        @media (max-width:390px){
            .setJDButton button{
                margin-top:0px !important;
                width:86% !important;
                padding: 20px 23px !important;
            }
        }
        .setJDPSHAREDDM{
            top:40px;
        }
        .setJDPICLINKURL{
            display:none;
        }
        .setJDPTABCRFG{
            display:none;
        }
        .setJDPRFGCSECTION{
            display:block;
        }
        .setJDPMTAIMAGEC{
            width: 61px;
            height: 61px;
        }
        .setJDPMTACINFODC{
            display:unset;
        }
        .setJDPDIVCPR{
            position: relative;
            padding-right: 20px;
            padding-top: 20px;
        }
        .setJDPAFDDM{
            min-width: 230px;
            position: absolute;
            margin-right: 10px;
            margin-bottom: 50px !important;
        }
        .setJDPMTACTIONDB{
            display:unset;
        }
        .setJDPSPANPC{
            color: #58af2a;
        }
        .setJDPROWPFF{
            padding: 10px 10px;
        }
        .setJDPCol8ROWWW{
            padding-right: 0px;
        }
        .setJDPOPPPDROW{
            margin: 0px;
        }
        .setJDPOPPPRICEA{
            padding-top: 6px;
        }
        .setJDPADAREAIMAGE{
            width: 100px;
            height: 100px;
        }
        .setJDPPCHATMSGS{
            height: 525px;
        }
        .setJDPPCHATMSGEM{
            color:#ff0000;
        }
        .setJDPMODALCPDLABEL{
            padding-top: 20px;
        }
    </style>
    <meta id="token" name="token" value="{{ csrf_token() }}">
    <div class="col-md-9 usercenter link-div">
        @include('frontend.includes.100-coworkers')
        <a href="{{ $redirectUrl }}" class="text-uppercase raleway">
        <span class="fa fa-angle-left">
        </span>
            {!! $redirectBackName  !!}
        </a>
    </div>
    <div id="joinDiscussion">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                {{--<div class="row">--}}
                {{--<div class="col-md-12 usercenter link-div">--}}
                {{--<a href="{{ route('frontend.jobs.index') }}" class="text-uppercase bold raleway">--}}
                {{--<span class="fa fa-angle-left"></span>--}}
                {{--{{ trans('common.back_to_job_list') }}--}}
                {{--</a>--}}
                {{--</div>--}}
                {{--</div>--}}
                @include('frontend.flash')

                <div id="sec-common-project"
                     class="portlet box job div-job-{{ $project->id or '' }} div-jobDiscussion-main">

                    @if($project->type == \App\Models\Project::PROJECT_TYPE_COMMON)
                        <div class="portlet-title">
                            {{--<div class="qr_code_image">--}}
                                {{--<img class="img-tag" src="{{$qr_code}}">--}}
                                {{--<div id="cross-div-qr" class="cross-area-img-div">x</div>--}}
                            {{--</div>--}}
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="span-info-3 col-span-12">
                                                <div class="col-md-6 set-left-padding-col-6">
                                                    <div class="project-name pull-left">

                                                        @php
                                                            $projectUserId = isset($project->user_id) ? $project->user_id : 0;
                                                            $userId = isset(\Auth::user()->id) ? \Auth::user()->id : '';
                                                            $pType = isset($post_type) ? $post_type : '';
                                                            $appDisplayOrder = isset($applicant->display_order) ? $applicant->display_order : '';
                                                            $appId = isset($applicant->id) ? $applicant->id : 0;
                                                        @endphp
                                                        {{--@if( $projectUserId == $userId && $appDisplayOrder == \App\Models\ProjectApplicant::DISPLAY_ORDER_PUBLISHED && $pType == 'applicant')--}}
                                                        {{--<span class="edit-link-trigger">--}}
                                                        {{--<span class="edit-target">--}}
                                                        {{--{{ empty($applicant->title)?$project->name:$applicant->title }}--}}
                                                        {{--<a class="f-s-14 edit-link-show" href="javascript:;"--}}
                                                        {{--data-toggle="modal" data-target="#modalChangePostTitle"--}}
                                                        {{--data-applicant="{{$appId}}">--}}
                                                        {{--<i class="fa fa-pencil edit"></i>--}}
                                                        {{--</a>--}}
                                                        {{--</span>--}}
                                                        {{--</span>--}}
                                                        {{--@else--}}
                                                        <h1 class="set-title-of-project">{{ empty($applicant->title)?$project->name:$applicant->title }}</h1>
                                                        {{--@endif--}}
                                                        <div class="icon-group f-s-20 f-c-gray-2">
                                                            <?php
                                                            $isFav = $project->isYourFavouriteJob()? 'active': '';
                                                            $route = $isFav == 'active' ? route('frontend.removefavouritejob.post') : route('frontend.addfavouritejob.post');
                                                            $style = $isFav == 'active' ? '#f4b00e' : '';
                                                            ?>
                                                            @if(\Auth::guard('users')->check())
                                                                <a class="btn-remove-fav-job {{$isFav}}" href="javascript:;" data-url="{{ $route }}" data-project-id="{{ $project->id }}" >
                                                                    <span class="fa fa-star set-fa-fa-fa-starsss" style="{{$style}}"></span>
                                                                </a>
                                                            @else
                                                                <a class="btn-remove-fav-job {{ $isFav }}" href="javascript:;" onclick="window.location.href='/login'" >
                                                                    <span class="fa fa-star set-fa-fa-fa-starsss" style="color: {{$style}}"></span>
                                                                </a>
                                                            @endif
                                                        </div>

                                                        <br>
                                                        <div class="col-md-12 price-div" style="padding-left:0px">
                                                            @if( \Auth::guard('users')->check() && $project->user_id == Auth::user()->id && isset($applicant) && $applicant->display_order == \App\Models\ProjectApplicant::DISPLAY_ORDER_PUBLISHED && $post_type == 'applicant')


                                                                @if( empty($applicant->reference_price_type) )
                                                                    @if( $project->pay_type == \App\Constants::PAY_TYPE_FIXED_PRICE )
                                                                        <i class="set-price-price-type">{{ trans('common.fixed_price_p') }}:</i>  <span class="set-color-green-price">{{ $project->getReferencePrice() }}</span>&nbsp;&nbsp;&nbsp;&nbsp;

                                                                    @elseif ( $project->pay_type == \App\Constants::PAY_TYPE_HOURLY_PAY )
                                                                        <i class="set-price-price-type"> {{ trans('common.hourly_price_p') }}:</i>  <span class="set-color-green-price">{{ $project->getReferencePrice() }}</span>&nbsp;&nbsp;&nbsp;&nbsp;

                                                                    @else
                                                                        <i class="set-price-price-typ set-margin-typee">
                                                                            {{ trans('common.request_quotation') }}
                                                                        </i>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    @endif
                                                                @else
                                                                    @if( isset($applicant) && $applicant->reference_price_type == \App\Constants::PAY_TYPE_FIXED_PRICE )
                                                                        <i class="set-price-price-type">{{ trans('common.fixed_price_p') }}:</i>  <span class="set-color-green-price">{{ $applicant->reference_price }}</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    @elseif (isset($applicant) && $applicant->reference_price_type == \App\Constants::PAY_TYPE_HOURLY_PAY )
                                                                        <i class="set-price-price-type"> {{ trans('common.hourly_price_p') }}:</i> <span class="set-color-green-price">{{ $applicant->reference_price }}</span>&nbsp;&nbsp;&nbsp;&nbsp;

                                                                    @else
                                                                        <i class="set-price-price-type set-margin-type">
                                                                            {{ trans('common.request_quotation') }}</i>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    @endif
                                                                @endif


                                                            @else

                                                                @if( empty($applicant->reference_price_type) )
                                                                    @if( $project->pay_type == \App\Constants::PAY_TYPE_FIXED_PRICE )
                                                                        <i class="set-price-price-type">{{ trans('common.fixed_price_p') }}:</i> <span class="set-color-green-price">{{ $project->getReferencePrice() }}</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    @elseif ( $project->pay_type == \App\Constants::PAY_TYPE_HOURLY_PAY )
                                                                        <i class="set-price-price-type"> {{ trans('common.hourly_price_p') }}:</i>  <span class="set-color-green-price">{{ $project->getReferencePrice() }}</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    @else
                                                                        <i class="set-price-price-type set-margin-type">
                                                                            {{ trans('common.request_quotation') }}</i>@endif
                                                                @else
                                                                    @if(isset($applicant) && $applicant->reference_price_type == \App\Constants::PAY_TYPE_FIXED_PRICE )
                                                                        <i class="set-price-price-type">{{ trans('common.fixed_price_p') }}:</i>  <span class="set-color-green-price">{{ $applicant->reference_price }}</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    @elseif (isset($applicant) && $applicant->reference_price_type == \App\Constants::PAY_TYPE_HOURLY_PAY )
                                                                        <i class="set-price-price-type"> {{ trans('common.hourly_price_p') }}: </i> <span class="set-color-green-price">{{ $applicant->reference_price }}</span>&nbsp;&nbsp;&nbsp;&nbsp;

                                                                    @else

                                                                        <i class="set-price-price-type set-margin-type">
                                                                            {{ trans('common.request_quotation') }}</i>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                @endif
                                                            @endif
                                                        @endif

                                                        <!-- <div class="p-t-5">
                                        <div class="price-icon">
                                            <span aria-hidden="true" class="icon-tag f-c-green2 f-s-24" ></span>
                                        </div>

                                        @if( empty($applicant->reference_price_type) )
                                                            @if( $project->pay_type == \App\Constants::PAY_TYPE_FIXED_PRICE )
                                                                <span class="top-smoll">
{{ trans('common.fixed_price') }}
                                                                        <br/>
                                                                        <span class="price">
{{--<i class="fa fa-usd" aria-hidden="true"></i>--}}
                                                                {{ $project->getReferencePrice() }}
                                                                        </span>
                                                                    </span>
@elseif ( $project->pay_type == \App\Constants::PAY_TYPE_HOURLY_PAY )
                                                                <span class="top-smoll">
{{ trans('common.pay_hourly') }}
                                                                        <br/>
                                                                        <span class="price">
{{--<i class="fa fa-usd" aria-hidden="true"></i>--}}
                                                                {{ $project->getReferencePrice() }}
                                                                        </span>
                                                                    </span>
@else
                                                                <span class="top-smoll">
{{ trans('common.request_quotation') }}
                                                                        </span>
@endif
                                                        @else
                                                            @if( isset($applicant) && $applicant->reference_price_type == \App\Constants::PAY_TYPE_FIXED_PRICE )
                                                                <span class="top-smoll">
{{ trans('common.fixed_price') }}
                                                                        <br/>
                                                                        <span class="price">
{{--<i class="fa fa-usd" aria-hidden="true"></i>--}}
                                                                {{ $applicant->reference_price }}
                                                                        </span>
                                                                    </span>
@elseif ( isset($applicant) && $applicant->reference_price_type == \App\Constants::PAY_TYPE_HOURLY_PAY )
                                                                <span class="top-smoll">
{{ trans('common.pay_hourly') }}
                                                                        <br/>
                                                                        <span class="price">
{{--<i class="fa fa-usd" aria-hidden="true"></i>--}}
                                                                {{ $applicant->reference_price }}
                                                                        </span>
                                                                    </span>
@else
                                                                <span class="top-smoll">
{{ trans('common.request_quotation') }}
                                                                        </span>
@endif
                                                        @endif
                                                                </div>
-->
                                                            <i class="set-price-price-type">{{ trans('common.project_type') }}: </i> <span class="set-public-type">{{ trans('common.public') }}</span>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 set-right-padding-col-6">
                                                    <div class="setJDButton1" >
                                                        @if(\Auth::guard('users')->check())
                                                            @if( $project->user_id == \Auth::id() )
                                                                <a href="Javascript:;">
                                                                    <button type="button" class="btn btn-make-offer set-btn-btn-join-discussion"
                                                                            onclick="window.location='{{ route('frontend.findexperts', ["skill_id_list" => $skill_id_list, "invitation_project_id" => $project->id]) }}'">
                                                                        {{ trans('common.invite_freelancers') }}
                                                                    </button>
                                                                </a>
                                                                <br>
                                                            @else
                                                                <a href="{{ route('frontend.modal.makeoffer', [
                                                                            'project_id' => $project->id,
                                                                            'user_id' => \Auth::id(),
                                                                            'inbox_id' => 0
                                                                        ] ) }}"
                                                                   data-toggle="modal"
                                                                   data-target="#modalMakeOffer">
                                                                    @php
                                                                        $userNo = \Auth::id();
                                                                        $projectNo = $project->id;
                                                                        $count = App\Models\ProjectApplicant::GetProjectApplicant()
                                                                            ->where('project_id', $projectNo)
                                                                            ->where('user_id', $userNo)->count();
                                                                    @endphp
                                                                    @if ($count >= 1)
                                                                        <button id="btn-make-offer-{{ $project->id }}" class="btn btn-make-offer set-btn-btn-join-discussion" style="color:#808080; background-color:#ccc6c4 !important; border:2px solid #ccc6c4 !important;">
                                                                            {{ trans('common.offer_made') }}
                                                                        </button>
                                                                    @else
                                                                        <button id="btn-make-offer-{{ $project->id }}" class="btn btn-make-offer set-btn-btn-join-discussion">
                                                                            {{ trans('common.make_offer') }}
                                                                        </button>
                                                                @endif
                                                                <!--<button id="btn-make-offer-{{ $project->id }}" class="btn btn-make-offer set-btn-btn-join-discussion">
                                                                        {{ trans('common.make_offer') }}
                                                                        </button>-->
                                                                </a>
                                                                <br>
                                                            @endif
                                                        @else
                                                            <button id="btn-make-offer-{{ $project->id }}" onclick="window.location.href='/login'" class="btn btn-make-offer set-btn-btn-join-discussion">
                                                                {{ trans('common.make_offer') }}
                                                            </button>
                                                        @endif
                                                    </div>
                                                    <div class="setJDButton3" >
                                                            <div id="disableBtn" data-toggle="modal" data-target="#shareModal" class="set-a-link-share-project">
                                                                {{trans('common.share_this_office')}}
                                                            </div>
                                                            <!-- Modal -->
                                                            <div id="shareModal" class="modal fade" role="dialog">
                                                                <div class="modal-dialog">
                                                                    <!-- Modal content-->

                                                                    <div class="modal-content set-modal-content-share-links">
                                                                        <div class="modal-header set-modal-header-share-modal">
                                                                            <button type="button" class="close set-close-btn-share" data-dismiss="modal">&times;</button>
                                                                            <p class="share-this-property">Share this Project</p>
                                                                        </div>
                                                                        <div class="modal-body set-modal-body-share-modal">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="col-md-4 set-col-md-4-share-facebook" style="padding-right: 0px;">
                                                                                            <div data-network="facebook" class="st-custom-button set-facebook-color-share">
                                                                                                <img src="{{asset('images/icons/facebook.jpg')}}">
                                                                                                <span class="set-span-text-share">Facebook</span>
                                                                                            </div>
                                                                                    </div>
                                                                                    <div class="col-md-3"></div>
                                                                                    <div class="col-md-4 set-col-md-4-share-twitter" style="padding-left: 0px;">
                                                                                            <div data-network="twitter" class="st-custom-button set-twitter-color-share">
                                                                                                <img src="{{asset('images/icons/twitter.png')}}">
                                                                                                <span class="set-span-text-share">Twitter</span>
                                                                                            </div>
                                                                                    </div>
                                                                                </div>
                                                                                <br>
                                                                                <div class="col-md-12">
                                                                                    <div class="col-md-4 set-col-md-4-share-wechat" style="padding-right: 0px;">
                                                                                            <div data-network="wechat" class="st-custom-button set-wechat-color-share">
                                                                                                <img src="{{asset('images/icons/wechat.png')}}">
                                                                                                <span class="set-span-text-share">We Chat</span>
                                                                                            </div>
                                                                                    </div>
                                                                                    <div class="col-md-4 set-col-md-4-share-email" style="padding-left: 0px;">
                                                                                            <div data-network="email" data-target="_blank" class="st-custom-button set-email-color-share">                                                  <img src="{{asset('images/icons/envelope.jpg')}}">
                                                                                                <span class="set-span-text-share">Email</span>
                                                                                            </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <div class="col-md-4 set-col-md-4-share-wechat" style="padding-right: 0px;">
                                                                                        @if(\Auth::guard('users')->check())
                                                                                            <input type="text" id="currentLink" value="{{\URL::current()}}" class="setJDPICLINKURL">
                                                                                            <button aria-hidden="true" style="font-family: Simple-Line-Icons !important;" class="icon-link set-fa-fa-link-share set-copylink-color-share" data-clipboard-target="#currentLink">
                                                                                                <span class="set-copy-link-span">copy link</span>
                                                                                            </button>
                                                                                            @else
                                                                                            <input type="text" id="currentLink" value="{{\URL::current()}}" class="setJDPICLINKURL">
                                                                                            <button aria-hidden="true" style="font-family: Simple-Line-Icons !important;" onclick="Copy();" class="icon-link set-fa-fa-link-share set-copylink-color-share" data-clipboard-target="#currentLink">
                                                                                                <span class="set-copy-link-span">copy link</span>
                                                                                            </button>
                                                                                        @endif
                                                                                    </div>

                                                                                    <div class="col-md-4 set-col-md-4-share-email" style="padding-left: 0px;">
                                                                                            <div data-network="messenger" class="st-custom-button set-messaengerl-color-share">
                                                                                                <img src="{{asset('images/icons/messenger-icon.png')}}">
                                                                                                <span class="set-span-text-share">Messenger</span>
                                                                                            </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer set-modal-footer-share">
                                                                                <img class="img-tag-qr-image" src="{{$qr_code}}">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <hr>
                            <br>
                        </div>
                    @elseif($project->type == \App\Models\Project::PROJECT_TYPE_OFFICIAL)
                        <div class="portlet-title">
                            <div class="qr_code_image">
                                <img class="img-tag" src="{{$qr_code}}">
                                <div id="cross-div-qr" class="cross-area-img-div">x</div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="span-info-3 col-span-12">
                                                <div class="project-name pull-left">

                                                    @php
                                                        $projectUserId = isset($project->user_id) ? $project->user_id : 0;
                                                        $userId = isset(\Auth::user()->id) ? \Auth::user()->id : '';
                                                        $pType = isset($post_type) ? $post_type : '';
                                                        $appDisplayOrder = isset($applicant->display_order) ? $applicant->display_order : '';
                                                        $appId = isset($applicant->id) ? $applicant->id : 0;
                                                    @endphp
                                                    {{--@if( $projectUserId == $userId && $appDisplayOrder == \App\Models\ProjectApplicant::DISPLAY_ORDER_PUBLISHED && $pType == 'applicant')--}}
                                                    {{--<span class="edit-link-trigger">--}}
                                                    {{--<span class="edit-target">--}}
                                                    {{--{{ empty($applicant->title)?$project->name:$applicant->title }}--}}
                                                    {{--<a class="f-s-14 edit-link-show" href="javascript:;"--}}
                                                    {{--data-toggle="modal" data-target="#modalChangePostTitle"--}}
                                                    {{--data-applicant="{{$appId}}">--}}
                                                    {{--<i class="fa fa-pencil edit"></i>--}}
                                                    {{--</a>--}}
                                                    {{--</span>--}}
                                                    {{--</span>--}}
                                                    {{--@else--}}
                                                    <span class="set-title-of-project">{{ empty($applicant->title)?$project->name:$applicant->title }}</span>
                                                    {{--@endif--}}
                                                    <br/>

                                                    @if( $project->creator != null )
                                                        <div class="f-s-14 f-c-gray-4" style="cursor: auto">
                                                            {!! trans('common.posted_by_2', ['dateTime' => \Carbon\Carbon::createFromTimeStamp(strtotime($project->created_at))->diffForHumans()  ,'who' => $project->creator->getName() ]) !!}
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="icon-group pull-right f-s-20 f-c-gray-2 set-icon-group-office-p">
                                                    @php
                                                        $isFav = $project->isYourFavouriteJob()? 'active': '';
                                                        $route = $isFav == 'active' ? route('frontend.removefavouritejob.post') : route('frontend.addfavouritejob.post');
                                                        $style = $isFav == 'active' ? '#f4b00e' : '';
                                                    @endphp
                                                    @if(\Auth::guard('users')->check())
                                                        <a class="btn-remove-fav-job {{$isFav}}" href="javascript:;" data-url="{{ $route }}" data-project-id="{{ $project->id }}" >
                                                            <span class="fa fa-star set-fa-fa-fa-starsss" style="{{$style}}"></span>
                                                        </a>
                                                    @else
                                                        <a class="btn-remove-fav-job" href="javascript:;" onclick="window.location.href='/login'" >
                                                            <span class="fa fa-star"></span>
                                                        </a>
                                                    @endif
                                                    @if(\Auth::guard('users')->check())
                                                        <span aria-hidden="true" class="icon-share dropdown-toggle"
                                                              data-toggle="dropdown"></span>
                                                    @else
                                                        <span aria-hidden="true" class="icon-share dropdown-toggle"
                                                              onclick="window.location.href='/login'"></span>
                                                    @endif
                                                    <ul class="share dropdown-menu setJDPSHAREDDM">
                                                        <li class="change-status">
                                                            <a href="javascript:void(0);" id="disableBtn-offical">
                                                                {{--<img src="https://static.bshare.cn/frame/images/logos/mp2/weixin.gif">--}}
                                                                <img class="sharedimages" src="{{asset('images/logo-menu/wechat.png')}}">
                                                                <span>
                                                            {{ trans('common.share_on_wechat') }}
                                                        </span>
                                                            </a>
                                                        </li>
                                                        <hr/>
                                                        <li>
                                                            <a href="javascript:;" id="disableBtn-offical-fb">
                                                                <img class="sharedimages" src="{{asset('images/facebook23.png')}}">
                                                                <span>
                                                            {{ trans('common.share_on_facebook') }}
                                                        </span>
                                                            </a>
                                                        </li>
                                                        <hr/>
                                                        <li>
                                                            <a href="javascript:;" id="disableBtn-offical-tweet">
                                                                {{--<img src="https://static.bshare.cn/frame/images/logos/mp2/twitter.gif">--}}<img class="sharedimages" src="{{asset('images/logo-menu/twitter.png')}}">
                                                                <span>
                                                            {{ trans('common.share_on_twitter') }}
                                                        </span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <input type="text" id="currentLink" value="{{\URL::current()}}" class="setJDPICLINKURL">
                                                    <span aria-hidden="true" class="icon-link" data-clipboard-target="#currentLink"></span>
                                                </div>

                                                @if(\Auth::guard('users')->check() && $project->type == \App\Models\Project::PROJECT_TYPE_OFFICIAL)
                                                    <button type="button" class="btn pull-right btn-oficial-project set-btnbtn-officalp" id="btn-official-project-project setBtnBtnOffical" style="cursor: auto">
                                                        {{ trans('common.official_project') }}
                                                    </button>
                                                @else
                                                    <button type="button" class="btn pull-right btn-oficial-project set-btnbtn-officalp" onclick="window.location.href='/login'" style="cursor: auto">
                                                        {{ trans('common.official_project') }}
                                                    </button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 price-div">
                                            @if( \Auth::guard('users')->check() && $project->user_id == Auth::user()->id && isset($applicant) && $applicant->display_order == \App\Models\ProjectApplicant::DISPLAY_ORDER_PUBLISHED && $post_type == 'applicant')
                                                <div class="p-t-5 edit-box-adjust edit-target-box adjust-left">
                                                    <a href="javascript:;" class="action pull-right f-s-14 edit-target-link"
                                                       data-toggle="modal" data-target="#modalChangePostPrice">
                                                        + {{trans('common.edit')}}
                                                    </a>
                                                    <div class="price-icon">
                                                        <span aria-hidden="true" class="icon-tag f-c-green2 f-s-24"></span>
                                                    </div>

                                                    @if( empty($applicant->reference_price_type) )
                                                        @if( $project->pay_type == \App\Constants::PAY_TYPE_FIXED_PRICE )
                                                            <span class="top-smoll">
                                                    {{ trans('common.fixed_price') }}
                                                            <br/>
                                                    <span class="price">
                                                        {{--<i class="fa fa-usd" aria-hidden="true"></i>--}}
                                                        {{ $project->getReferencePrice() }}
                                                    </span>
                                                </span>
                                                        @elseif ( $project->pay_type == \App\Constants::PAY_TYPE_HOURLY_PAY )
                                                            <span class="top-smoll">
                                                    {{ trans('common.pay_hourly') }}
                                                            <br/>
                                                    <span class="price">
                                                        {{--<i class="fa fa-usd" aria-hidden="true"></i>--}}
                                                        {{ $project->getReferencePrice() }}
                                                    </span>
                                                </span>
                                                        @else
                                                            <span class="top-smoll">
                                                    {{ trans('common.request_quotation') }}
                                                </span>
                                                            <br/>
                                                            <span>
                                                    &nbsp;
                                                </span>
                                                        @endif
                                                    @else
                                                        @if( isset($applicant) && $applicant->reference_price_type == \App\Constants::PAY_TYPE_FIXED_PRICE )
                                                            <span class="top-smoll">
                                                    {{ trans('common.fixed_price') }}
                                                            <br/>
                                                    <span class="price">
                                                        {{--<i class="fa fa-usd" aria-hidden="true"></i>--}}
                                                        {{ $applicant->reference_price }}
                                                    </span>
                                                </span>
                                                        @elseif (isset($applicant) && $applicant->reference_price_type == \App\Constants::PAY_TYPE_HOURLY_PAY )
                                                            <span class="top-smoll">
                                                    {{ trans('common.pay_hourly') }}
                                                            <br/>
                                                    <span class="price">
                                                        {{--<i class="fa fa-usd" aria-hidden="true"></i>--}}
                                                        {{ $applicant->reference_price }}
                                                    </span>
                                                </span>
                                                        @else
                                                            <span class="top-smoll">
                                                    {{ trans('common.request_quotation') }}
                                                </span>
                                                            <br/>
                                                            <span>
                                                    &nbsp;
                                                </span>
                                                        @endif
                                                    @endif


                                                </div>
                                            @else
                                                <div class="p-t-5">
                                                    <div class="price-icon">
                                                        <span aria-hidden="true" class="icon-tag f-c-green2 f-s-24"></span>
                                                    </div>
                                                    @if( empty($applicant->reference_price_type) )
                                                        @if( $project->pay_type == \App\Constants::PAY_TYPE_FIXED_PRICE )
                                                            <span class="top-smoll">
                                                    {{ trans('common.fixed_price') }}
                                                            <br/>
                                                    <span class="price">
                                                        {{--<i class="fa fa-usd" aria-hidden="true"></i>--}}
                                                        {{ $project->getReferencePrice() }}
                                                    </span>
                                                </span>
                                                        @elseif ( $project->pay_type == \App\Constants::PAY_TYPE_HOURLY_PAY )
                                                            <span class="top-smoll">
                                                    {{ trans('common.pay_hourly') }}
                                                            <br/>
                                                    <span class="price">
                                                        {{--<i class="fa fa-usd" aria-hidden="true"></i>--}}
                                                        {{ $project->getReferencePrice() }}
                                                    </span>
                                                </span>
                                                        @else
                                                            <span class="top-smoll">
                                                    {{ trans('common.request_quotation') }}
                                                </span>
                                                            <br/>
                                                            <span>
                                                    &nbsp;
                                                </span>
                                                        @endif
                                                    @else
                                                        @if(isset($applicant) && $applicant->reference_price_type == \App\Constants::PAY_TYPE_FIXED_PRICE )
                                                            <span class="top-smoll">
                                                    {{ trans('common.fixed_price') }}
                                                            <br/>
                                                    <span class="price">
                                                        {{--<i class="fa fa-usd" aria-hidden="true"></i>--}}
                                                        {{ $applicant->reference_price }}
                                                    </span>
                                                </span>
                                                        @elseif (isset($applicant) && $applicant->reference_price_type == \App\Constants::PAY_TYPE_HOURLY_PAY )
                                                            <span class="top-smoll">
                                                    {{ trans('common.pay_hourly') }}
                                                            <br/>
                                                    <span class="price">
                                                        {{--<i class="fa fa-usd" aria-hidden="true"></i>--}}
                                                        {{ $applicant->reference_price }}
                                                    </span>
                                                </span>
                                                        @else
                                                            <span class="top-smoll">
                                                    {{ trans('common.request_quotation') }}
                                                </span>
                                                            <br/>
                                                            <span>
                                                    &nbsp;
                                                </span>
                                                        @endif
                                                    @endif
                                                </div>
                                        @endif

                                        <!-- <div class="p-t-5">
                                        <div class="price-icon">
                                            <span aria-hidden="true" class="icon-tag f-c-green2 f-s-24" ></span>
                                        </div>

                                        @if( empty($applicant->reference_price_type) )
                                            @if( $project->pay_type == \App\Constants::PAY_TYPE_FIXED_PRICE )
                                                <span class="top-smoll">
{{ trans('common.fixed_price') }}
                                                        <br/>
                                                        <span class="price">
{{--<i class="fa fa-usd" aria-hidden="true"></i>--}}
                                                {{ $project->getReferencePrice() }}
                                                        </span>
                                                    </span>
@elseif ( $project->pay_type == \App\Constants::PAY_TYPE_HOURLY_PAY )
                                                <span class="top-smoll">
{{ trans('common.pay_hourly') }}
                                                        <br/>
                                                        <span class="price">
{{--<i class="fa fa-usd" aria-hidden="true"></i>--}}
                                                {{ $project->getReferencePrice() }}
                                                        </span>
                                                    </span>
@else
                                                <span class="top-smoll">
{{ trans('common.request_quotation') }}
                                                        </span>
@endif
                                        @else
                                            @if( isset($applicant) && $applicant->reference_price_type == \App\Constants::PAY_TYPE_FIXED_PRICE )
                                                <span class="top-smoll">
{{ trans('common.fixed_price') }}
                                                        <br/>
                                                        <span class="price">
{{--<i class="fa fa-usd" aria-hidden="true"></i>--}}
                                                {{ $applicant->reference_price }}
                                                        </span>
                                                    </span>
@elseif ( isset($applicant) && $applicant->reference_price_type == \App\Constants::PAY_TYPE_HOURLY_PAY )
                                                <span class="top-smoll">
{{ trans('common.pay_hourly') }}
                                                        <br/>
                                                        <span class="price">
{{--<i class="fa fa-usd" aria-hidden="true"></i>--}}
                                                {{ $applicant->reference_price }}
                                                        </span>
                                                    </span>
@else
                                                <span class="top-smoll">
{{ trans('common.request_quotation') }}
                                                        </span>
@endif
                                        @endif
                                                </div>
-->
                                            <div class="p-t-5">
                                                <div class="project-type">
                                                <span aria-hidden="true"
                                                      class="icon-briefcase f-c-green2 f-s-24"></span>
                                                </div>
                                                <span class="top-smoll">
                                            {{ trans('common.project_type') }}
                                                <br/>
                                            <span class="type">
                                                {{ trans('common.public') }}
                                            </span>
                                        </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--<div class="col-md-3 col-xs-12">--}}
                                {{--<button type="button" class="btn btn-share-pro pull-right">--}}
                                {{--{{ trans('common.share_this_project') }}--}}
                                {{--</button>--}}
                                {{--<div class="bshare-custom pull-right icon-medium-plus">--}}
                                {{--<a title="{{ trans('common.share_to_wechat') }}" class="bshare-weixin" onmouseover="javascript:bShare.share(event,'weixin',0);return false;"></a>--}}
                                {{--</div>--}}
                                {{--<script type="text/javascript" charset="utf-8" src="https://static.bshare.cn/b/buttonLite.js#style=-1&amp;uuid=&amp;pophcol=2&amp;lang=zh"></script>--}}
                                {{--<script type="text/javascript" charset="utf-8" src="https://static.bshare.cn/b/bshareC0.js"></script>--}}

                                {{--</div>--}}
                            </div>
                            <br/>

                            <hr>
                            {{--style="text-align: center; position: relative; top: -40px;"--}}
                            <div class="setJDButton" >
                                @if(\Auth::guard('users')->check())
                                    @if( $project->user_id == \Auth::id() )
                                        <a href="Javascript:;">
                                            <button type="button" class="btn btn-make-offer"
                                                    onclick="window.location='{{ route('frontend.findexperts', ["skill_id_list" => $skill_id_list, "invitation_project_id" => $project->id]) }}'">
                                                {{ trans('common.invite_freelancers') }}
                                            </button>
                                        </a>
                                        <br>
                                    @else
                                        <a href="{{ route('frontend.modal.makeoffer', [
                                'project_id' => $project->id,
                                'user_id' => \Auth::id(),
                                'inbox_id' => 0
                            ] ) }}"
                                           data-toggle="modal"
                                           data-target="#modalMakeOffer">
                                            <button id="btn-make-offer-{{ $project->id }}" class="btn btn-make-offer">
                                                {{ trans('common.make_offer') }}
                                            </button>
                                        </a>
                                        <br>
                                    @endif
                                @else
                                    <button id="btn-make-offer-{{ $project->id }}" onclick="window.location.href='/login'" class="btn btn-make-offer">
                                        {{ trans('common.make_offer') }}
                                    </button>
                                @endif
                            </div>

                        </div>
                    @endif

                    <div class="portlet-body">
                        <div class="row">
                            <div class="row col-md-12">
                                @if(\Auth::guard('users')->check() && $project->user_id == Auth::user()->id && isset($applicant) && $applicant->display_order == \App\Models\ProjectApplicant::DISPLAY_ORDER_PUBLISHED)
                                    <div class="edit-target-box">
                                        <a href="javascript:;" class="action pull-right f-s-14 edit-target-link"
                                           data-toggle="modal" data-target="#modalChangePostDescription">
                                            + {{trans('common.edit')}}
                                        </a>

                                        {{--<div class="portlet-body-title p-lr-15">--}}
                                        {{--<h5>{{ trans('common.description') }}</h5>--}}

                                        {{--</div>--}}

                                        <div class="col-md-12">
                                            <div class="scroller setScroller">
                                                <div class="description">
                                                    @if(count($path) == 0)
                                                        <a href="#" rel="" data-lightbox="roadtrip">
                                                            {!! empty($applicant->description)?$project->description:$applicant->description !!}
                                                        </a>
                                                    @else
                                                        @foreach($path as $src)
                                                            <a href="{{$src}}" rel="" data-lightbox="roadtrip">
                                                                {!! empty($applicant->description)?$project->description:$applicant->description !!}
                                                            </a>
                                                        @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        @else

                                            {{--<div class="portlet-body-title p-lr-15">--}}
                                            {{--<h5 style="margin-left: 1%">{{ trans('common.description') }}</h5>--}}
                                            {{--</div>--}}
                                            <div class="col-md-12">
                                                <div id="sec-chat" class="set-portlet-body-border-end-jdp portlet box job div-job-{{ $project->id }}">
                                                    <div class="tabbable-line">
                                                        <ul class="nav nav-tabs uppercase">
                                                            <li class="active set-tabs-li-jobDP set-tabs-li-jobDP1" onclick="activefunction('a')">
                                                                <div href="#tab_default_0"
                                                                     data-toggle="tab" class="job-description-image-width"></div>
                                                                <div href="#tab_default_0"
                                                                     data-toggle="tab" class="clickedImage" style="display: none;"></div>
                                                                <a class="add-hover-effect" href="#tab_default_0"
                                                                   data-toggle="tab">{{ trans('common.job_description') }}</a>
                                                            </li>
                                                            <li class="set-tabs-li-jobDP set-tabs-li-jobDP2"onclick="activefunction('b')">
                                                                <div class="discuss-room-width" href="#tab_default_1" data-toggle="tab"></div>
                                                                <div href="#tab_default_1" data-toggle="tab" class="clickedImage2" style="display:none;"></div>
                                                                <a class="add-hover-effect" href="#tab_default_1"
                                                                   data-toggle="tab">{{ trans('common.discuss_room') }}</a>
                                                            </li>
                                                            @if(\Auth::user() && $project->user_id == \Auth::user()->id)
                                                            <li class="set-tabs-li-jobDP set-tabs-li-jobDP3" onclick="activefunction('c')">
                                                                <div href="#tab_default_2"
                                                                     data-toggle="tab" class="free-lance-details-width"></div>
                                                                <div href="#tab_default_2"
                                                                     data-toggle="tab" class="clickedImage3"></div>
                                                                <a class="add-hover-effect" href="#tab_default_2"
                                                                   data-toggle="tab">{{ trans('common.freelancer_detail') }}</a>
                                                            </li>
                                                            @endif
                                                        </ul>
                                                        <div class="tab-content">
                                                            <div class="tab-pane active" id="tab_default_0">
                                                                <div class="description" id="lightboxset">
                                                                    @if(count($path) == 0)
                                                                        <a href="#" rel="" data-lightbox="roadtrip">
                                                                            {!! empty($applicant->description)?$project->description:$applicant->description !!}
                                                                        </a>
                                                                    @else
                                                                        @foreach($path as $src)
                                                                            <a href="{{$src}}" rel="" data-lightbox="roadtrip">
                                                                                {!! empty($applicant->description)?$project->description:$applicant->description !!}
                                                                            </a>
                                                                        @endforeach
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane" id="tab_default_1">

                                                                <div class="row form-group freelance-detail-section setJDPTABCRFG">
                                                                    <div class="col-md-12">
                                                                        @if( $projectApplicants != null )
                                                                            @foreach( $projectApplicants as $key => $projectApplicant)
                                                                                @include('frontend.joinDiscussion.applicantDetails', [
                                                                                    'user' => $projectApplicant->user,
                                                                                    'candidateAlreadySelected' => $candidateAlreadySelected,
                                                                                    'candidateUserId' => $candidateUserId
                                                                                ])
                                                                            @endforeach

                                                                            {!! $projectApplicants->render() !!}
                                                                        @endif
                                                                    </div>
                                                                </div>

                                                                {{-- chat messages --}}
                                                                <div class="row form-group chat-section setJDPRFGCSECTION">
                                                                    @if(\Auth::guard('users')->check())
                                                                        <div class="col-md-8 chat-room-div">
                                                                            <h5 class="title">{{ trans('common.chat_room') }}</h5>
                                                                            @include('frontend.includes.projectChatMessage',[
                                                                                'projectId' => $project->id,
                                                                                'projectChatRoomId' => $projectChatRoomId
                                                                            ])
                                                                            @else
                                                                                <div class="col-md-12 chat-room-div">
                                                                                    <h5 class="title">{{ trans('common.chat_room') }}</h5>
                                                                                    <div class="row project-conversation">
                                                                                        <div class="col-md-12 clearfix">
                                                                                            <div class="portlet box gray-color set-without-login-section">
                                                                                                <div class="portlet-body form">
                                                                                                    <div class="container set-without-login-container">
                                                                                                        <div class="col-md-12 text-center">
                                                                                                            <p class="set-p-without-login">{{trans('common.login_in_to_contact_manager')}}</p>
                                                                                                        </div>
                                                                                                        <form class="form setFORMBODY" role="form" method="POST" action="{{ route('login.post') }}" id="login-formm">
                                                                                                            {!! csrf_field() !!}
                                                                                                            <div class="form-group">
                                                                                                                <div class="col-md-12" id="login-alert-container"></div>
                                                                                                            </div>
                                                                                                            <div class="col-md-12 text-center form-group set-form-group-bottom-1 {{ $errors->has('phone_or_email') ? ' has-error' : '' }}">
                                                                                                                <div class="set-col-md-5-without-login-5">
                                                                                                                    <div class="set-col-md-image-2"></div>
                                                                                                                    <input tabindex="1" type="text" placeholder="{{trans('common.adminna')}}" id="click-input-box-1" name="phone_or_email" value="{{ old('phone_or_email') }}" class="input-box-1 form-control">
                                                                                                                    @if ($errors->has('phone_or_email'))
                                                                                                                        <span class="help-block">
                                                                <strong>{{ $errors->first('phone_or_email') }}</strong>
                                                        </span>
                                                                                                                    @endif
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-12 text-center form-group set-form-group-bottom {{ $errors->has('password') ? ' has-error' : '' }}">
                                                                                                                <div class="set-col-md-5-without-login-5">
                                                                                                                    <div class="set-col-md-image-pass-2"></div>
                                                                                                                    <input tabindex="2" type="password" name="password" id="click-input-box-2" placeholder="{{trans('common.pussword')}}" class="input-box-2 form-control">
                                                                                                                    @if ($errors->has('password'))
                                                                                                                        <span class="help-block">
                                                                <strong>{{ $errors->first('password') }}</strong>
                                                            </span>
                                                                                                                    @endif
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="form-group">
                                                                                                                <button type="button" tabindex="3" class="btn blue btn-block btn-without-login" id="submit-loginn">
                                                                                                                    {{ trans('common.continue') }}
                                                                                                                </button>
                                                                                                            </div>
                                                                                                        </form>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    @endif

                                                                                    @if(\Auth::user() && (\Auth::user()->name == '' || is_null(\Auth::user()->name) || \Auth::user()->about_me == '' || is_null(\Auth::user()->about_me)))
                                                                                        <div id="myModal" class="modal fade" role="dialog" style="position:absolute">
                                                                                            <div class="modal-dialog">

                                                                                                <!-- Modal content-->
                                                                                                <div class="modal-content">
                                                                                                    <div class="modal-body">
                                                                                                        <p>Please complete your profile before to join conversaion...!</p>
                                                                                                    </div>
                                                                                                    <div class="modal-footer">
                                                                                                        <button type="button" onclick="window.location.href='/personalInformation'" class="btn btn-default" data-dismiss="modal">Click Here</button>
                                                                                                    </div>
                                                                                                </div>

                                                                                            </div>
                                                                                        </div>
                                                                                    @endif


                                                                                </div>
                                                                                {{--chat room Members --}}
                                                                                @if(\Auth::guard('users')->check())
                                                                                    <div class="col-md-4 chat-room-memb">
                                                                                        <h5 class="title">{{ trans('common.chat_room_members') }}
                                                                                            ({{count($projectChatFollowers)}})</h5>

                                                                                        <div class="portlet light bordered chat-room-memb-div setchat-room-memb-div setPDIVONSCROLL scroller">
                                                                                            <div class="portlet-body">
                                                                                                <div class="tab-content">
                                                                                                    <div class="tab-pane active chat-room-memb-parent"
                                                                                                         id="tab_actions_pending">
                                                                                                        <!-- BEGIN: Actions -->
                                                                                                        {{--<div class="mt-action chat-room-user-info">--}}
                                                                                                        {{--<div class="row">--}}
                                                                                                        {{--<div class="col-md-3">--}}
                                                                                                        {{--<img class="setChat_user_info_image Image_highlight_sliver Image_highlight_brown Image_highlight_gold" src="{{asset('images/svster.png')}}">--}}
                                                                                                        {{--<span class="highlight_brown highlight_gold  highlight_silver"></span>--}}
                                                                                                        {{--</div>--}}
                                                                                                        {{--<div class="col-md-9 setNewSectionCol9">--}}
                                                                                                        {{--<i class="fa fa-star active_fa_starts" aria-hidden="true"></i>--}}
                                                                                                        {{--<i class="fa fa-star active_fa_starts" aria-hidden="true"></i>--}}
                                                                                                        {{--<i class="fa fa-star active_fa_starts" aria-hidden="true"></i>--}}
                                                                                                        {{--<i class="fa fa-star setFaStars" aria-hidden="true"></i>--}}
                                                                                                        {{--<i class="fa fa-star setFaStars" aria-hidden="true"></i>--}}
                                                                                                        {{--<span class="setSpn01OTS">4.98 of 35 reviews</span>--}}
                                                                                                        {{--<br>--}}
                                                                                                        {{--<span class="setSpnCNJP">China &nbsp;<span>185 jobs posted</span></span>--}}
                                                                                                        {{--<br>--}}
                                                                                                        {{--<span class="setSpn01OTS">Over $10.000 total spent</span>--}}
                                                                                                        {{--</div>--}}
                                                                                                        {{--</div>--}}
                                                                                                        {{--</div>--}}
                                                                                                        {{--project owner --}}
                                                                                                        <div class="mt-actions chat-room-memb-child set-mt-action-owner" style="max-height:150px !important;min-height: 100px !important;">
                                                                                                            @foreach($getOwnerfirst as $owner)
                                                                                                                @if(isset($owner->user))
                                                                                                                    @php

                                                                                                                        $userLink = isset($owner->user) && isset($owner->user->user_link) ? $owner->user->user_link : '';
                                                                                                                        $imageClass = '';
                                                                                                                        $colorClass = '';
                                                                                                                        $linkhref = route('frontend.resume.getdetails', [ 'id' => $userLink, 'lang' => \Session::get('lang')]);
                                                                                                                        $icon = $owner->user->getAvatar();
                                                                                                                        if(\Auth::guard('users')->check() && $owner->user->id == $owner->user_id ){
                                                                                                                          $messageCount = \App\Models\UserInboxMessages::where('from_user_id', '=', $owner->user->id)->where('to_user_id', '=', \Auth::user()->id)->where('project_id', '=', $owner->id)->count();
                                                                                                                          if($messageCount < 1) {
                                                                                                                            $icon = '/images/svster.png';
                                                                                                                            $linkhref = 'javascript:;';
                                                                                                                          }
                                                                                                                          $earnings = \App\Models\Transaction::where('performed_by', '=', $owner->user->id)->sum('amount');

                                                                                                                          if($earnings >= 500 && $earnings < 5000) {
                                                                                                                            $imageClass = 'Image_highlight_brown';
                                                                                                                            $colorClass = 'highlight_brown';
                                                                                                                          }
                                                                                                                          if($earnings >= 5000 && $earnings < 10000) {
                                                                                                                            $imageClass = 'Image_highlight_sliver';
                                                                                                                            $colorClass = 'highlight_silver';
                                                                                                                          }
                                                                                                                          if($earnings >= 10000) {
                                                                                                                            $imageClass = 'Image_highlight_gold';
                                                                                                                            $colorClass = 'highlight_gold';
                                                                                                                          }
                                                                                                                        }
                                                                                                                         $city = \App\Models\WorldCities::with('country')->where('id', '=', $owner->user->city_id)->first();
                                                                                                                         $country_name = isset($city->country->name) ? $city->country->name : '--';

                                                                                                                    @endphp
                                                                                                                    <div class="mt-action chat-follower-{{ $owner->id }}">
                                                                                                                        <div class="mt-action-img" onclick="window.location.href = '{{$linkhref}}'">
                                                                                                                            <a href="{{$linkhref}}">
                                                                                                                                <img alt="" class="setChat_user_info_image {{$imageClass}}"
                                                                                                                                     src="{{ $icon }}"
                                                                                                                                >
                                                                                                                                <span class="{{$colorClass}}"></span>
                                                                                                                            </a>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="mt-action-body project chat-ownersss">
                                                                                                                        <div class="mt-action-row">
                                                                                                                            <div class="mt-action-info ">
                                                                                                                                @php
                                                                                                                                    $employerReviews = \App\Models\FreelancerReview::where('reviewer_id', '=', $owner->user->id)->count();
                                                                                                                                    if($employerReviews <= 1 || !isset($employerReviews))
                                                                                                                                        $employerReviews = 0;
                                                                                                                                    $employerReviewsAvg = \App\Models\FreelancerReview::where('reviewer_id', '=', $owner->user->id)->avg('recommendation');
                                                                                                                                     if($employerReviewsAvg <= 1 || !isset($employerReviewsAvg))
                                                                                                                                        $employerReviewsAvg = 0;

                                                                                                                                    $employerReviewsAvgCiel = ceil($employerReviewsAvg);
                                                                                                                                    $employerReviewsAvgCielEmpty = 5 - $employerReviewsAvg;
                                                                                                                                    $jobsPosted = \App\Models\Project::where('user_id', '=', $owner->user->id)->count();
                                                                                                                                @endphp
                                                                                                                                <div class="mt-action-details setJDPMTACINFODC"
                                                                                                                                >
                                                                                                                                    <div class="row">
                                                                                                                                        <div class="col-md-12 setCol12NewD">
                                                                                                                                            @for($i=0; $i < $employerReviewsAvgCiel; $i++)
                                                                                                                                                <i class="fa fa-star active_fa_starts " aria-hidden="true"></i>
                                                                                                                                            @endfor
                                                                                                                                            @for($i=0; $i < $employerReviewsAvgCielEmpty; $i++)
                                                                                                                                                <i class="fa fa-star setFaStars " aria-hidden="true"></i>
                                                                                                                                            @endfor
                                                                                                                                            <span class="setSpn01OTS">{{$employerReviewsAvg}} of {{$employerReviews}} reviews</span>
                                                                                                                                            <br>
                                                                                                                                            <span class="setSpnCNJP">
                        {{ $country_name }} &nbsp;<span>{{$jobsPosted}} jobs posted</span></span>
                                                                                                                                            <br>
                                                                                                                                            @if(\Auth::guard('users')->check())
                                                                                                                                                <span class="setSpn01OTS">Over ${{$earnings}} total spent</span>

                                                                                                                                            @else
                                                                                                                                                <span class="setSpn01OTS">Over 0 total spent</span>

                                                                                                                                            @endif
                                                                                                                                            {{--<span class="mt-action-author">--}}
                                                                                                                                            {{--@php--}}
                                                                                                                                            {{--try {--}}
                                                                                                                                            {{--$n = $projectChatFollower->user->getName();--}}
                                                                                                                                            {{--} catch (\Exception $e) {--}}
                                                                                                                                            {{--$n = '';--}}
                                                                                                                                            {{--}--}}
                                                                                                                                            {{--@endphp--}}
                                                                                                                                            {{--{{ $n }}--}}

                                                                                                                                            {{--@if( $projectChatFollower->user->getLatestIdentityStatus() == \App\Models\UserIdentity::STATUS_APPROVED )--}}
                                                                                                                                            {{--<span class="fa fa-check-circle"></span>--}}
                                                                                                                                            {{--@endif--}}
                                                                                                                                            {{--</span>--}}
                                                                                                                                        </div>
                                                                                                                                        {{--<div class="col-md-4">--}}
                                                                                                                                        {{--<span class="send-message-sect">--}}
                                                                                                                                        {{--@if( $projectChatFollower->user->id != \Auth::id() )--}}
                                                                                                                                        {{--<a href="{{ route('frontend.sendprivatemessage', ['user_id' => $projectChatFollower->user->id ]) }}" data-toggle="modal" data-target="#remote-modal" data-dismiss="modal">--}}
                                                                                                                                        {{--<img src="{{asset('images/messageLogo.png')}}" id="message-icon">--}}
                                                                                                                                        {{--<i class="fa fa-comments-o f-s-20 f-c-green2" id="message-icon" >--}}

                                                                                                                                        {{--</i>--}}

                                                                                                                                        {{--</a>--}}
                                                                                                                                        {{--@endif--}}
                                                                                                                                        {{--</span>--}}
                                                                                                                                        {{--</div>--}}
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                <p class="mt-action-desc">{{ trans('common.project_creator') }}</p>
                                                                                                                                <div>
                                                                                                                                    <br/>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                @endif
                                                                                                            @endforeach
                                                                                                        </div>
                                                                                                        {{-- project chat owner section end --}}

                                                                                                        {{--project follower--}}
                                                                                                        <div class="mt-actions chat-room-memb-child">
                                                                                                            @foreach( $projectChatFollowers as $key => $projectChatFollower )
                                                                                                                @if(isset($projectChatFollower->user) && $projectChatFollower->user->id != $project->user_id)
                                                                                                                    @php
                                                                                                                        $userLink = isset($projectChatFollower->user) && isset($projectChatFollower->user->user_link) ? $projectChatFollower->user->user_link : '';
                                                                                                                        $imageClass = '';
                                                                                                                        $colorClass = '';
                                                                                                                        $linkhref = route('frontend.resume.getdetails', [ 'id' => $userLink, 'lang' => \Session::get('lang')]);
                                                                                                                        $icon = $projectChatFollower->user->getAvatar();
                                                                                                                        if(\Auth::guard('users')->check() && $projectChatFollower->user->id == $project->user_id ){
                                                                                                                          $messageCount = \App\Models\UserInboxMessages::where('from_user_id', '=', $projectChatFollower->user->id)->where('to_user_id', '=', \Auth::user()->id)->where('project_id', '=', $project->id)->count();
                                                                                                                          if($messageCount < 1) {
                                                                                                                            $icon = '/images/svster.png';
                                                                                                                            $linkhref = 'javascript:;';
                                                                                                                          }
                                                                                                                          $earnings = \App\Models\Transaction::where('performed_by', '=', $projectChatFollower->user->id)->sum('amount');

                                                                                                                          if($earnings >= 500 && $earnings < 5000) {
                                                                                                                            $imageClass = 'Image_highlight_brown';
                                                                                                                            $colorClass = 'highlight_brown';
                                                                                                                          }
                                                                                                                          if($earnings >= 5000 && $earnings < 10000) {
                                                                                                                            $imageClass = 'Image_highlight_sliver';
                                                                                                                            $colorClass = 'highlight_silver';
                                                                                                                          }
                                                                                                                          if($earnings >= 10000) {
                                                                                                                            $imageClass = 'Image_highlight_gold';
                                                                                                                            $colorClass = 'highlight_gold';
                                                                                                                          }
                                                                                                                        }
                                                                                                                         $city = \App\Models\WorldCities::with('country')->where('id', '=', $projectChatFollower->user->city_id)->first();
                                                                                                                         $country_name = isset($city->country->name) ? $city->country->name : '--';

                                                                                                                    @endphp

                                                                                                                    <div class="mt-action chat-follower-{{ $projectChatFollower->id }}">
                                                                                                                        @if( $projectChatFollower->user->id != $project->user_id )
                                                                                                                            <div class="mt-action-img" onclick="window.location.href = '{{$linkhref}}'">
                                                                                                                                <a href="{{$linkhref}}">
                                                                                                                                    <img alt="" class="setChat_user_info_image {{$imageClass}}"
                                                                                                                                         src="{{ $icon }}"
                                                                                                                                    >
                                                                                                                                    <span class="{{$colorClass}}"></span>
                                                                                                                                </a>
                                                                                                                            </div>
                                                                                                                        @endif

                                                                                                                        @if( $projectChatFollower->user->id != $project->user_id )

                                                                                                                            {{--project follower--}}
                                                                                                                            @if( $project->user_id == \Auth::id() )
                                                                                                                                <div class="pull-right setJDPDIVCPR"
                                                                                                                                > <a href="javascript:;"
                                                                                                                                     class="dropdown-toggle"
                                                                                                                                     data-toggle="dropdown">
                                                                                                                                        <i class="fa fa-ellipsis-v f-s-20 set-fc-green"></i>
                                                                                                                                    </a>
                                                                                                                                    <ul class="admin-feature dropdown-menu setJDPAFDDM">
                                                                                                                                        @if( $projectChatFollower->user->getProjectApplicant($project->id) != null && $project->user_id == \Auth::id())
                                                                                                                                            @if($projectChatFollower->user->getProjectApplicant($project->id)->status != \App\Models\ProjectApplicant::STATUS_CREATOR_SELECTED)
                                                                                                                                                <li class="btn-reject"
                                                                                                                                                    data-url="{{ route('frontend.joindiscussion.reject',  ['applicant_id' => $projectChatFollower->user->getProjectApplicant($project->id)->id ]) }}">
                                                                                                                                                    <a href="javascript:;">
                            <span aria-hidden="true"
                                  class="icon icon-dislike"></span>
                                                                                                                                                        <span>
            {{ trans('common.reject_the_offer') }}
        </span>
                                                                                                                                                    </a>
                                                                                                                                                </li>
                                                                                                                                                <hr/>
                                                                                                                                            @else
                                                                                                                                                <li>
                                                                                                                                                    <a data-toggle="modal"
                                                                                                                                                       data-target="#modalUserMakeOfferDetail"
                                                                                                                                                       href="{{ route('frontend.joindiscussion.applicantdetail',  ['applicant_id' => $projectChatFollower->user->getProjectApplicant($project->id)->id ]) }}">
                            <span aria-hidden="true"
                                  class="icon icon-briefcase"></span>
                                                                                                                                                        <span>
            {{ trans('common.hire_for_the_job') }}
        </span>
                                                                                                                                                    </a>
                                                                                                                                                </li>
                                                                                                                                                <hr/>
                                                                                                                                            @endif
                                                                                                                                        @endif
                                                                                                                                        <li>
                                                                                                                                            <a href="{{ route('frontend.sendprivatemessage', ['user_id' => $projectChatFollower->user->id ]) }}"
                                                                                                                                               data-toggle="modal"
                                                                                                                                               data-target="#remote-modal"
                                                                                                                                               data-dismiss="modal">
                        <span aria-hidden="true"
                              class="icon icon-bubble"></span>
                                                                                                                                                <span>
            {{ trans('common.chat_in_private_room') }}
        </span>
                                                                                                                                            </a>
                                                                                                                                        </li>
                                                                                                                                    </ul>
                                                                                                                                </div>
                                                                                                                            @endif

                                                                                                                            <div class="mt-action-body">
                                                                                                                                <div class="mt-action-row">
                                                                                                                                    <div class="mt-action-info ">
                                                                                                                                        <div class="mt-action-details setJDPMTACTIONDB">
                                                                                                                                            <div class="row">
                                                                                                                                                <div class="col-md-8 setCol8NewD">
                                                     <span class="mt-action-author">
                                                     @php
                                                         try {
                                                         $n = $projectChatFollower->user->getName();
                                                         } catch (\Exception $e) {
                                                         $n = '';
                                                         }
                                                     @endphp
                                                         {{ $n }}
                                                         @if( $projectChatFollower->user->getLatestIdentityStatus() == \App\Models\UserIdentity::STATUS_APPROVED )
                                                             <span class="fa fa-check-circle"></span>
                                                         @endif
                                                     </span>
                                                                                                                                                </div>
                                                                                                                                                <div class="col-md-4">
                                                     <span class="send-message-sect">
                                                            @if( $projectChatFollower->user->id != \Auth::id() )
                                                             @if(\Auth::guard('users')->check())
                                                                 @if( $project->user_id != \Auth::id())
                                                                     <a href="{{ route('frontend.sendprivatemessage', ['user_id' => $projectChatFollower->user->id ]) }}"
                                                                        data-toggle="modal"
                                                                        data-target="#remote-modal"
                                                                        data-dismiss="modal">
                                                                        {{--<img src="/images/icons/ic_msg-copy.png" style="width: 23px;">--}}
                                                                                      <!-- <img src="{{asset('images/messageLogo.png')}}" id="message-icon"> -->
                                                                                     {{--<i class="fa fa-comments-o f-s-20 f-c-green2" id="message-icon-2"></i>--}}
                                                                        </a>
                                                                 @else
                                                                 @endif
                                                             @else
                                                                 <a href="{{ url('/login')}}">
                                                                        {{--<img src="/images/icons/ic_msg-copy.png" style="width: 23px;">--}}
                                                                          <img src="{{asset('images/messageLogo.png')}}" id="message-icon">
                                                                         {{--<i class="fa fa-comments-o f-s-20 f-c-green2" id="message-icon-2"></i>--}}
                                                                        </a>
                                                             @endif
                                                         @else
                                                             @if(\Auth::guard('users')->check())
                                                                 @if( $projectChatFollower->user->id == \Auth::id() && $projectChatFollower->user->id != $project->user_id )
                                                                     <a id="btnQuitInterview"
                                                                        href="javascript:;"
                                                                        data-url="{{ route('frontend.joindiscussion.quitprojectdiscussion.post') }}"
                                                                        data-id="{{ $projectChatFollower->id }}">
                                                                       {{--<p class="status quit-interview" id="quit-interview-1">{{ trans('common.quit') }}</p>--}}
                                                                            </a>
                                                                 @endif
                                                             @else
                                                                 <a id="btnQuitInterview"
                                                                    href="javascript:;"
                                                                    data-url="{{ url('/login') }}"
                                                                    data-id="{{ $projectChatFollower->id }}">
                                                                       {{--<p class="status quit-interview" id="quit-interview-1">{{ trans('common.quit') }}</p>--}}
                                                                            </a>
                                                             @endif
                                                         @endif
                                                     </span>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                            <p class="mt-action-desc">
                                                                                                                                                @php
                                                                                                                                                    try {
                                                                                                                                                        $jPosition = $projectChatFollower->user->getJobPosition();
                                                                                                                                                    } catch(\Exception $e){
                                                                                                                                                        $jPosition = 'N/A';
                                                                                                                                                    }
                                                                                                                                                @endphp
                                                                                                                                                {{ $jPosition }}
                                                                                                                                            </p>
                                                                                                                                            @if ( $projectChatFollower->user->getProjectApplicant($project->id) != null && $project->user_id == \Auth::id() )
                                                                                                                                                <div class="">{{ trans('common.price') }}
                                                                                                                                                    :
                                                                                                                                                    <span class="setJDPSPANPC">
                                                        {{ $projectChatFollower->getQuotePrice($project->id) }}
                                                    </span>
                                                                                                                                                </div>
                                                                                                                                            @endif
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>

                                                                                                                            @if( $projectChatFollower->user->getProjectApplicant($project->id) != null && $project->user_id == \Auth::id())
                                                                                                                                <div class="mt-action-footer">
                                                                                                                                    @if( $projectChatFollower->user->getProjectApplicant($project->id)->status == \App\Models\ProjectApplicant::STATUS_APPLY )
                                                                                                                                    @elseif( $projectChatFollower->user->getProjectApplicant($project->id)->status == \App\Models\ProjectApplicant::STATUS_CREATOR_SELECTED )
                                                                                                                                        <span class="text-success bold">{{ trans('common.creator_selected') }}</span>
                                                                                                                                    @elseif( $projectChatFollower->user->getProjectApplicant($project->id)->status == \App\Models\ProjectApplicant::STATUS_CREATOR_REJECTED )
                                                                                                                                        <span class="text-danger bold">{{ trans('common.project_creator_reject') }}</span>
                                                                                                                                    @elseif( $projectChatFollower->user->getProjectApplicant($project->id)->status == \App\Models\ProjectApplicant::STATUS_APPLICANT_ACCEPTED )
                                                                                                                                        <span class="text-success bold">{{ trans('common.applicant_accept') }}</span>
                                                                                                                                    @elseif( $projectChatFollower->user->getProjectApplicant($project->id)->status == \App\Models\ProjectApplicant::STATUS_APPLICANT_REJECTED )
                                                                                                                                        <span class="text-danger bold">{{ trans('common.applicant_reject') }}</span>
                                                                                                                                    @endif
                                                                                                                                </div>
                                                                                                                            @endif
                                                                                                                            <div class="row setJDPROWPFF"
                                                                                                                            >
                                                                                                                                @if(isset($projectChatFollower) && isset($projectChatFollower->user) && isset($projectChatFollower->user->skills))
                                                                                                                                    @php
                                                                                                                                        $skillCount = 0;
                                                                                                                                        $skills = collect($projectChatFollower->user->skills);
                                                                                                                                        $skills = $skills->sortByDesc('sort_by_skill')->all();
                                                                                                                                    @endphp
                                                                                                                                    <div class="col-md-10 set-Col-10Md">
                                                                                                                                        @foreach($skills as $user_skill)
                                                                                                                                            @php
                                                                                                                                                try{
                                                                                                                                                if(app()->getLocale() == 'cn') {
                                                                                                                                                    $skillName = !is_null($user_skill) && (isset($user_skill->skill->name_cn)) ? $user_skill->skill->name_cn : '';
                                                                                                                                                } else {
                                                                                                                                                    $skillName = !is_null($user_skill) && (isset($user_skill->skill->name_en)) ? $user_skill->skill->name_en : '';
                                                                                                                                                }
                                                                                                                                                }catch (\Exception $e) {
                                                                                                                                                    $skillName = '';
                                                                                                                                                }
                                                                                                                                            @endphp
                                                                                                                                            @if($skillName != '' && $skillCount < 6)
                                                                                                                                                <div class="skill-div">
                    <span class="
                        @if($user_skill->experience >= 1)
                    {{ 'item-skill official' }}
                    @elseif($user_skill->is_client_verified == 1)
                    {{ 'item-skill success' }}
                    @else
                    {{ 'item-skill' }}
                    @endif
                            "
                    >
                    @if($user_skill->experience >= 1)
                            <img src="/images/crown.png" alt=""
                                 height="24px" width="24px">
                        @elseif($user_skill->is_client_verified == 1)
                            <img src="/images/checked.png"
                                 alt="" height="24px"
                                 width="24px">
                        @endif
                    <span>
                        {{$skillName}}
                    </span>
                    </span>
                                                                                                                                                </div>
                                                                                                                                            @endif
                                                                                                                                            @php
                                                                                                                                                $skillCount++
                                                                                                                                            @endphp
                                                                                                                                        @endforeach
                                                                                                                                    </div>
                                                                                                                                    <div class="col-md-2 setCol2More">
                                                                                                                                        @if($skillCount > 6)
                                                                                                                                            <a class="setDotsLinks" href="{{url("/resume/getDetails",\Session::get('lang'),$projectChatFollower->user->user_link)}}">{{trans('common.more')}}</a>

                                                                                                                                        @endif
                                                                                                                                    </div>
                                                                                                                                @endif
                                                                                                                            </div>
                                                                                                                        @endif
                                                                                                                    </div>
                                                                                                                @if($projectChatFollower->user->id != $project->user_id)
                                                                                                                    <hr id="hrChatRoomUserInfo">
                                                                                                                @endif
                                                                                                                @endif
                                                                                                            @endforeach
                                                                                                        </div>
                                                                                                        <!-- END: Actions -->
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                @endif
                                                                        </div>
                                                                </div>

                                                                {{-- detail freelancer--}}
                                                                <div class="tab-pane" id="tab_default_2">

                                                                    <div class="row form-group chat-section freelancer-detail">
                                                                        <div class="col-md-12 chat-room-div proposals">
                                                                            <h5 class="title">{{ trans('common.proposals') }}</h5>
                                                                            <div class="portlet box gray-color setJDPortletBox">
                                                                                @if(\Auth::guard('users')->check())
                                                                                    @foreach( $projectChatFollowers as $key => $projectChatFollower )
                                                                                        @if(isset($projectChatFollower->user))
                                                                                            @if($projectChatFollower->user->id != $project->user_id)
                                                                                                <div class="row proposals-mein-div">
                                                                                                    <div class="col-md-2 img">
                                                                                                        <img src="{{ $projectChatFollower->user->getAvatar() }}">
                                                                                                    </div>
                                                                                                    <div class="col-md-10 descriptions">
                                                                                                        <div class="row">
                                                                                                            <div class="col-md-12">
                                                                                                                <div class="row">
                                                                                                                    <div class="col-md-9">
                                                                                                                        <div class="row">
                                                                                                                            <div class="col-md-6 user-name">
                                                                                                                                <h5>{{ $projectChatFollower->user->getName() }}</h5>
                                                                                                                                <h6>{{ $projectChatFollower->user->title }}</h6>
                                                                                                                            </div>
                                                                                                                            @if( $projectChatFollower->user->getProjectApplicant($project->id) != null )
                                                                                                                                <div class="col-md-6 price">
                                                                                                                                    <p>{{ trans('common.price') }}
                                                                                                                                        :
                                                                                                                                        <span>{{ $projectChatFollower->getQuotePrice($project->id) }}</span>
                                                                                                                                    </p>
                                                                                                                                </div>
                                                                                                                            @endif
                                                                                                                            <div class="col-md-12">
                                                                                                                                <p class="user-cover-latter">
                                                                                                                                    {{ $projectChatFollower->user->getAboutMe() }}
                                                                                                                                </p>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-12 description-skill">

                                                                                                                                @foreach( $projectChatFollower->user->skills as $userSkill)
                                                                                                                                    <div class="skill-div setJDPCSkillDiv">
                                                                    <span class="item-skill">
                                                                        <span>
                                                                             @php
                                                                                 try{
                                                                                 if(app()->getLocale() == 'cn') {
                                                                                     $skillName = !is_null($userSkill) && isset($userSkill->skill->name_cn) ? $userSkill->skill->name_cn : '';
                                                                                 } else {
                                                                                    $skillName = !is_null($userSkill) && isset($userSkill->skill->name_en) ? $userSkill->skill->name_en : '';
                                                                                 }
                                                                                 }catch (\Exception $e) {
                                                                                     $skillName = '';
                                                                                 }
                                                                             @endphp
                                                                            {{$skillName}}
                                                                        </span>
                                                                    </span>
                                                                                                                                    </div>
                                                                                                                                @endforeach

                                                                                                                            </div>
                                                                                                                            <div class="col-md-12 setJDPRDailyUpdate">
                                                                                                                                <br/>
                                                                                                                                <div><b>{{trans('common.require_daily_update')}}:</b> {{isset($projectApplicant->daily_update) ? $projectApplicant->daily_update : 'N/A'}}
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-3 setJDLBCol3">
                                                                                                                        <div class="row">
                                                                                                                            @include('frontend.joinDiscussion.ucThumbStatus', [ 'projectChatFollower' =>$projectChatFollower ])
                                                                                                                            {{--<div class="col-md-12 btn-thumbs">--}}
                                                                                                                            {{--<button class="btn-thumb-up btn {{ $projectChatFollower->thumb_status == \App\Models\ProjectChatFollower::THUMB_STATUS_THUMB_UP? "btn-thumbs-color": "btn-thumbs-default"}}"--}}
                                                                                                                            {{--data-url = "{{ route('frontend.joindiscussion.thumbup.post', ['chat_follower_id' => $projectChatFollower->id ]) }}"--}}
                                                                                                                            {{--id="btn-thumb-up-{{ $projectChatFollower->id }}"--}}
                                                                                                                            {{--data-id = "{{ $projectChatFollower->id }}">--}}
                                                                                                                            {{--<i class="fa fa-thumbs-o-up"></i>--}}
                                                                                                                            {{--</button>--}}
                                                                                                                            {{--<button class="btn-thumb-down btn {{ $projectChatFollower->thumb_status == \App\Models\ProjectChatFollower::THUMB_STATUS_THUMB_DOWN? "btn-thumbs-color": "btn-thumbs-default"}}"--}}
                                                                                                                            {{--data-url = "{{ route('frontend.joindiscussion.thumbdown.post', ['chat_follower_id' => $projectChatFollower->id ]) }}"--}}
                                                                                                                            {{--id="btn-thumb-down-{{ $projectChatFollower->id }}"--}}
                                                                                                                            {{--data-id = "{{ $projectChatFollower->id }}">--}}
                                                                                                                            {{--<i class="fa fa-thumbs-o-down"></i>--}}
                                                                                                                            {{--</button>--}}
                                                                                                                            {{--</div>--}}
                                                                                                                            <div class="col-md-12 desc-right">
                                                                                                                                <span class="title clearfix">{{ trans('common.age') }}</span>
                                                                                                                                <span class="definition">
                                                        {{ $projectChatFollower->user->age }}
                                                    </span>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-12 desc-right">
                                                                                                                                <span class="title clearfix">{{ trans('common.experience_year') }}</span>
                                                                                                                                <span class="definition">
                                                        {{ $projectChatFollower->user->getExperience() }} {{ trans_choice('common.years', $projectChatFollower->user->getExperience()) }}
                                                    </span>
                                                                                                                            </div>
                                                                                                                            <div class="col-md-12 desc-right">
                                                                                                                                <span class="title clearfix">{{ trans('common.project_completed') }}</span>
                                                                                                                                <span class="definition">0</span>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-12 bottom-all-btn">
                                                                                                                <div class="row">
                                                                                                                    <div class="col-md-8 setJDPCol8ROWWW"
                                                                                                                    >
                                                                                                                        @if( $projectChatFollower->user->getProjectApplicant($project->id) != null && $project->user_id == \Auth::id())
                                                                                                                            @if( $projectChatFollower->user->getProjectApplicant($project->id)->status == \App\Models\ProjectApplicant::STATUS_APPLY )
                                                                                                                                <button class="btn btn-custom btn-default btn-reject"
                                                                                                                                        data-url="{{ route('frontend.joindiscussion.reject',  ['applicant_id' => $projectChatFollower->user->getProjectApplicant($project->id)->id ]) }}">
                                                                                                                                    {{ trans('common.reject') }}
                                                                                                                                </button>

                                                                                                                                <button class="btn btn-custom btn-color btn-award"
                                                                                                                                        data-url="{{ route('frontend.joindiscussion.awardjob',  ['applicant_id' => $projectChatFollower->user->getProjectApplicant($project->id)->id ]) }}">
                                                                                                                                    {{ trans('common.award_job') }}
                                                                                                                                </button>
                                                                                                                            @endif
                                                                                                                        @endif

                                                                                                                        <div class="last-login setJDLLT">{{ trans('common.last_login_time') }}
                                                                                                                            : <br>
                                                                                                                            <span>
                                                    {{ $projectChatFollower->user->getLastLogin() }}
                                                </span>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-4">
                                                                                                                        <button class="btn btn-custom btn-default btn-check-detail setJDPBtnCol4"
                                                                                                                                type="button"
                                                                                                                                data-url="{{ route('frontend.resume.getdetails', [ 'id' => $projectChatFollower->user->user_link, 'lang' => \Session::get('lang')] ) }}">

                                                                                                                            {{ trans('common.check_detail') }}
                                                                                                                        </button>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            @endif
                                                                                        @endif
                                                                                    @endforeach

                                                                                @else

                                                                                    <div class="container set-without-login-container">
                                                                                        <div class="col-md-12 text-center">
                                                                                            <p class="set-p-without-login">{{trans('common.login_in_to_contact_manager')}}</p>
                                                                                        </div>
                                                                                        <form class="form setFORMBODY" role="form" method="POST" action="{{ route('login.post') }}" id="login-form">
                                                                                            {!! csrf_field() !!}
                                                                                            <div class="form-group">
                                                                                                <div class="col-md-12" id="login-alert-container"></div>
                                                                                            </div>
                                                                                            <div class="col-md-12 text-center form-group set-form-group-bottom-2 {{ $errors->has('phone_or_email') ? ' has-error' : '' }}">
                                                                                                <div class=" set-col-md-5-without-login">
                                                                                                    <div class="set-col-md-image"></div>
                                                                                                    <input tabindex="1" type="text" placeholder="{{trans('common.adminna')}}" id="phone_email" name="phone_or_email" value="{{ old('phone_or_email') }}" class="input-box-1 form-control">
                                                                                                    @if ($errors->has('phone_or_email'))
                                                                                                        <span class="help-block">
                                                                <strong>{{ $errors->first('phone_or_email') }}</strong>
                                                            </span>
                                                                                                    @endif
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-md-12 text-center form-group set-form-group-bottom-3 {{ $errors->has('password') ? ' has-error' : '' }}">
                                                                                                <div class=" set-col-md-5-without-login">
                                                                                                    <div class="set-col-md-image-pass"></div>
                                                                                                    <input tabindex="2" type="password" name="password" id="click-box-pass" placeholder="{{trans('common.pussword')}}" class="input-box-2 form-control">
                                                                                                    @if ($errors->has('password'))
                                                                                                        <span class="help-block">
                                                                <strong>{{ $errors->first('password') }}</strong>
                                                            </span>
                                                                                                    @endif
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <button type="button" tabindex="3" class="btn blue btn-block btn-without-login" style="top:1px" id="submit-login">
                                                                                                    {{ trans('common.continue') }}
                                                                                                </button>
                                                                                            </div>
                                                                                        </form>
                                                                                    </div>

                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(\Auth::guard('users')->check() && $project->user_id == Auth::user()->id && isset($applicant) && $applicant->display_order == \App\Models\ProjectApplicant::DISPLAY_ORDER_PUBLISHED)
                                            <div class="col-md-12 edit-target-box">
                                                <a href="javascript:;" class="action pull-right f-s-14 edit-target-link"
                                                   data-toggle="modal" data-target="#modalChangePostSkill">
                                                    + {{trans('common.edit')}}
                                                </a>
                                                <div class="portlet-body-title">
                                                    <h5>{{ trans('common.skill_request') }}</h5>
                                                </div>

                                                <div class="skill-main">
                                                    <div class="skill-div scroll-div">
                                                        @if(empty($postSkill))
                                                            @foreach( $project->projectSkills as $key => $projectSkill )
                                                                @php
                                                                    $skillName = $projectSkill->getSkillName( $projectSkill->skill_id );
                                                                @endphp
                                                                @if($skillName != '')
                                                                    <span class="item-skill">
                                        <span>
                                            {{ $projectSkill->getSkillName( $projectSkill->skill_id ) }}
                                        </span>
                                    </span>
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            @foreach( $postSkill as $key => $projectSkill )
                                                                @php
                                                                    try{
                                                                        $skillName = !is_null($projectSkill) ? $projectSkill->getName() : '';
                                                                    }catch (\Exception $e) {
                                                                        $skillName = '';
                                                                    }
                                                                @endphp
                                                                @if($skillName != '')
                                                                    <span class="item-skill">
                                        <span>
                                            {{$skillName}}
                                        </span>
                                    </span>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                            <div class="col-md-12">
                                                <div class="portlet-body-title">
                                                    <h5>{{ trans('common.skill_request') }}</h5>
                                                </div>

                                                <div class="skill-main">
                                                    <div class="skill-div set-Scroller-join-d-Page">
                                                        @if(empty($postSkill))
                                                            @foreach( $project->projectSkills as $key => $projectSkill )
                                                                <span class="item-skill set-skill-div-join-discussion-page">
                                <span>
                                    {{ $projectSkill->getSkillName( $projectSkill->skill_id ) }}
                                </span>
                            </span>
                                                            @endforeach
                                                        @else
                                                            @foreach( $postSkill as $key => $projectSkill )
                                                                <span class="item-skill">
                                <span>
                                     @php
                                         try{
                                             $skillName = !is_null($projectSkill) ? $projectSkill->getName() : '';
                                         }catch (\Exception $e) {
                                             $skillName = '';
                                         }
                                     @endphp
                                    {{$skillName}}
                                </span>
                            </span>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="col-md-12 portlet-body-title" id="attachment-download-all">
                                            <h5>{{ trans('common.attachment') }}
                                                <a href="/downloadProjectDocuments?projectId={{ $project->id }}"
                                                   target="_blank">
                    <span class="set-download-span">
                        <i>
                        {{ trans('common.download_all') }}
                        ({{ count($project->projectFiles) }})
                        </i>
                    </span>
                                                </a>
                                            </h5>
                                        </div>

                                        <div class="col-md-12 attachment-main">
                                            @foreach( $project->projectFiles as $projectFile)
                                                <div class="attachment-sub">
                                                    <div class="attachment-file">
                                                        <a href="/{{ $projectFile->file }}" target="_blank">
                                                            <img src="{{ $projectFile->getFileTypeImage() }}">
                                                        </a>
                                                    </div>
                                                    <div class="attachment-name">
                                                        <a href="/{{ $projectFile->file }}" target="_blank">
                                                            {{ $projectFile->getShortFilename() }}
                                                        </a>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                        @if($project->type == \App\Models\Project::PROJECT_TYPE_COMMON)
                                        @php
                                        $projectOwnerId = $project->creator->id;
                                        $currentProjectId = $project->id;
                                        if(\Auth::user()) {
                                            $UserInboxMessages = \App\Models\UserInboxMessages::where('from_user_id', '=', $projectOwnerId)->where('to_user_id' , auth()->user()->id)->where('project_id',$currentProjectId)->first();
                                        } else {
                                            $UserInboxMessages = null;
                                        }
                                        @endphp
                                            <div class="col-md-12 posted-by-area">
                                                <div class="set-tooltip">
                                                    <span class="tooltiptext">{{trans('common.display_after_contacted')}}</span>
                                                    <img class="set-project-creator-img" src="{{!(auth()->user() && $UserInboxMessages)?'/images/svster.png':$project->creator->img_avatar}}">
                                                </div>
                                                @if( $project->creator != null )
                                                    <div class="f-s-14 f-c-gray-4 posted-by-area" style="cursor: auto">
                                                        <i>{!! trans('common.posted_by_2', ['dateTime' => \Carbon\Carbon::createFromTimeStamp(strtotime($project->created_at))->diffForHumans() ,'who' => (auth()->user() && $UserInboxMessages)?$project->creator->getName():'a member' ]) !!}</i>
                                                    </div>
                                                @endif
                                            </div>
                                        @endif

                                    </div>
                                    <div class="col-md-4 news">
                                        @if( $project->type == \App\Models\Project::PROJECT_TYPE_OFFICIAL && count($officialProjectNewsList) >= 1)
                                            <div class="panel panel-default">
                                                <div class="panel-heading">{{ trans('common.news')}}</div>
                                                <div class="panel-body">
                                                    @foreach( $officialProjectNewsList as $key => $officialProjectNews)
                                                        @if( $key != 0 )
                                                            <hr>
                                                        @endif
                                                        <div class="row">
                                                            <div class="col-md-12 news-des">
                                                                {{ truncateSentence($officialProjectNews->content, 100) }}
                                                            </div>
                                                            <div class="col-md-12 news-date">
                                                                <span>{{ $officialProjectNews->getDate() }}</span>
                                                                {!! $officialProjectNews->getTypeSymbol() !!}
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                    {{--<hr>--}}
                                                    {{--<div class="row">--}}
                                                    {{--<div class="col-md-12 news-des">--}}
                                                    {{--Telling story of dummy text dol or sit ament, consectetur adipis icing edit,--}}
                                                    {{--sed do tempor...--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-md-12 news-date">--}}
                                                    {{--<span>12 january 2016</span>--}}
                                                    {{--<img src="/images/icons/ic_hot.png" width="12px" class="pull-right">--}}
                                                    {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<hr>--}}
                                                    {{--<div class="row">--}}
                                                    {{--<div class="col-md-12 news-des">--}}
                                                    {{--Telling story of dummy text dol or sit ament, consectetur adipis icing edit,--}}
                                                    {{--sed do tempor...--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-md-12 news-date">--}}
                                                    {{--<span>12 january 2016</span>--}}
                                                    {{--<span class="fa fa-star star pull-right"></span>--}}
                                                    {{--</div>--}}
                                                    {{--</div>--}}
                                                    {{--<hr>--}}
                                                    {{--<div class="row">--}}
                                                    {{--<div class="col-md-12 news-des">--}}
                                                    {{--Telling story of dummy text dol or sit ament, consectetur adipis icing edit,--}}
                                                    {{--sed do tempor...--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-md-12 news-date">--}}
                                                    {{--<span>12 january 2016</span>--}}
                                                    {{--<span class="fa fa-star star pull-right"></span>--}}
                                                    {{--</div>--}}
                                                    {{--</div>--}}
                                                </div>
                                                <div class="panel-footer text-center">
                                                    <a href="{{ route('frontend.joindiscussion.news', [ 'project_id' => $project->id ]) }}">
                                                        {{ trans('common.see_all') }}
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                            </div>
                        </div>
                    </div>

                @include('frontend.joinDiscussion.officialProjectInfo')


                <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                {!! Form::hidden('project_id', $project->id, [ 'id' => 'projectId'] ) !!}
                {!! Form::hidden('choose_url', route('frontend.joindiscussion.choosecandidate'), [ 'id' => 'choose_url'] ) !!}
                @include('frontend.joinDiscussion.modalChangePostTitle', ['title' => isset($applicant) ? $applicant->title : ''])
                @include('frontend.joinDiscussion.modalChangePostPrice', ['type' => $type, 'pay_types'=>$pay_types])
                @include('frontend.joinDiscussion.modalChangePostDescription', ['description' => empty($applicant->description)?$project->description:$applicant->description])
                @include('frontend.joinDiscussion.modalChangePostSkill', ['post_skill' => $skill_id_list])
            </div>
        </div>
        @endsection

        @section('modal')
            <div id="modalMakeOffer" class="modal fade apply-job-modal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                    </div>
                </div>
            </div>

            <div id="modalUserMakeOfferDetail" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                    </div>
                </div>
            </div>

        @endsection

        @section('footer')
            <script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5d2c670907f9ac0012ebd969&product=custom-share-buttons"></script>

            <script>

                function Copy()
                {
                    var dummy = document.createElement('input'),
                        text = window.location.href;
                    document.body.appendChild(dummy);
                    dummy.value = text;
                    console.log(dummy.select());
                    document.execCommand('copy');
                    document.body.removeChild(dummy);
                    alertSuccess('Link Copied!');
                }

                function activefunction(val)
                {
                    if(val === 'a')
                    {
                        $('.clickedImage').html('');
                        $('.job-description-image-width').hide();
                        $('.clickedImage2').css('display','none');
                        $('.clickedImage3').css('display','none');
                        $('.discuss-room-width').show();
                        $('.free-lance-details-width').show();
                        var img = '<img style="width: 31px;\n' +
                            '    margin-top: 10px;position:relative; top:10px; margin-bottom: 4px;" src="{{asset('images/jobDescription.png')}}">';
                        $('.clickedImage').css('display','block');
                        $('.clickedImage').html(img);
                    }
                    if(val === 'b')
                    {
                        $('.clickedImage2').html('');
                        $('.clickedImage').css('display','none');
                        $('.clickedImage3').css('display','none');
                        $('.job-description-image-width').show();
                        $('.free-lance-details-width').show();
                        $('.discuss-room-width').hide();
                        var img = '<img style="width: 56px;\n' +
                            '    margin-top: 10px; position:relative; top:8px;" src="{{asset('/images/disscussRoomgreen.png')}}">';
                        $('.clickedImage2').css('display','block');
                        $('.clickedImage2').html(img);
                    }
                    if(val === 'c')
                    {
                        $('.clickedImage3').html('');
                        $('.clickedImage2').css('display','none');
                        $('.clickedImage').css('display','none');
                        $('.job-description-image-width').show();
                        $('.discuss-room-width').show();
                        $('.free-lance-details-width').hide();
                        var img = '<img style="width: 31px;\n' +
                            '    margin-top: 11px; position:relative; top:8px;" src="{{asset('/images/freeLanceDetailsgreen.png')}}">';
                        $('.clickedImage3').css('display','block');
                        $('.clickedImage3').html(img);
                    }
                }

                setTimeout(function(){
                    activefunction('a');
                },5);

                $(document).ready(function(){

                    $ ('.set-tabs-li-jobDP').hover(function(){
                        $('.job-description-image-width').addClass("job-hover-icon-class");
                    }, function(){
                        $('.job-description-image-width').removeClass("job-hover-icon-class");
                    });

                    $('.set-tabs-li-jobDP2').hover(function(){
                        $('.discuss-room-width').addClass("room-hover-icon-class");
                    }, function(){
                        $('.discuss-room-width').removeClass("room-hover-icon-class");
                    });

                    $('.set-tabs-li-jobDP3').hover(function(){
                        $('.free-lance-details-width').addClass("details-hover-icon-class");
                    }, function(){
                        $('.free-lance-details-width').removeClass("details-hover-icon-class");
                    });

                    $('#disableBtn').click(function(){
                        $('.qr_code_image').toggle();
                    })

                    $('#cross-div-qr').click(function(){
                        $('.qr_code_image').hide();
                    });

                    $('#disableBtn-offical').mouseover(function(){
                        $('.qr_code_image').show();
                    });
                    $('#disableBtn-offical-tweet').mouseover(function(){
                        $('.qr_code_image').show();
                    });
                    $('#disableBtn-offical-fb').mouseover(function(){
                        $('.qr_code_image').show();
                    });
                });

                $('#click-input-box-1').hover(function(){
                    $('.set-col-md-image-2').addClass('hover-class');
                }, function() {
                    $('.set-col-md-image-2').removeClass('hover-class');
                });

                $('#phone_email').hover(function(){
                    $('.set-col-md-image').addClass('hover-class');
                }, function() {
                    $('.set-col-md-image').removeClass('hover-class');
                });

                $('#click-input-box-2').hover(function(){
                    $('.set-col-md-image-pass-2').addClass('hover-class1');
                }, function() {
                    $('.set-col-md-image-pass-2').removeClass('hover-class1');
                });

                $('#click-box-pass').hover(function(){
                    $('.set-col-md-image-pass').addClass('hover-class1');
                }, function() {
                    $('.set-col-md-image-pass').removeClass('hover-class1');
                });
            </script>
            <script>
                +function () {
                    $(document).ready(function () {
                        $('#login-form').makeAjaxForm({
                            submitBtn: '#submit-login',
                            alertContainer: '#login-alert-container',
                            redirectTo: '{{ route('home') }}',
                            afterSuccessFunction: function (response, $el, $this) {
                                console.log('success');
                                window.location.href = '{{ route('home') }}';
                            }

                        });
                    });
                }(jQuery);
            </script>
            <script>
                +function () {
                    $(document).ready(function () {
                        $('#login-formm').makeAjaxForm({
                            submitBtn: '#submit-loginn',
                            alertContainer: '#login-alert-container',
                            redirectTo: '{{ route('home') }}',
                            afterSuccessFunction: function (response, $el, $this) {
                                console.log('success');
                                window.location.href = '{{ route('home') }}';
                            }

                        });
                    });
                }(jQuery);
            </script>

            {{--<script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>--}}
            <script>
                $('.scroll-div').slimScroll({});
            </script>
            <script src="{{asset('js/vue.min.js')}}"></script>
            <script src="{{asset('js/socket.io.js')}}"></script>
            <script src="{{asset('assets/global/plugins/lightbox2/dist/js/lightbox.min.js')}}" type="text/javascript"></script>
            <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"
                    type="text/javascript"></script>
            @if(\Auth::guard('users')->check())
                <script type="text/javascript" src="{{asset('assets/global/plugins/typeahead/typeahead.bundle.min.js')}}"></script>
                <script type="text/javascript"
                        src="{{asset('assets/global/plugins/bootstrap-tagsinput-latest/dist/bootstrap-tagsinput.min.js')}}"></script>
                <script src="{{asset('js/socket.io.min.js')}}" type="text/javascript"></script>
                <script src="{{asset('js/minifiedJoinDiscussion.js')}}" type="text/javascript"></script>
{{--                <script src="/custom/js/frontend/joinDiscussion.js" type="text/javascript"></script>--}}
{{--                <script type="text/javascript" src="/custom/js/frontend/modalAddNewSkillKeywordv2.js"></script>--}}

                <script type="text/javascript">

                    var lang = {
                        submit_successful: "{{ trans('common.submit_successful') }}",
                        chat_room_is_empty: "{{ trans('common.chat_room_is_empty') }}",
                        download: "{{ trans('common.download') }}",
                        recommended_screenshot_tool_for_project: "{{ trans('common.recommended_screenshot_tool_for_project') }}",
                        recommended_management_tool_for_project: "{{ trans('common.recommended_management_tool_for_project') }}",
                    };

                    var socket = io('https://xenren.co:3000');
                    var room = {!! $projectChatRoomId !!};
                    var project_id = {!! $project->id !!};

                    var _self = this;
                    //handlers
                    document.addEventListener('paste', function (e) {
                        _self.paste_auto(e);
                    }, false);

                    //on paste
                    this.paste_auto = function (e) {
                        var paste = (e.clipboardData || window.clipboardData).getData('text');
                        console.log(paste);
                        if(paste){
                            var text = new FormData();
                            text.append(paste);
                        }
                        if (e.clipboardData) {

                            var items = e.clipboardData.items;
                            if (!items) {
                                return;
                            } else {

                                //access data directly
                                for (var i = 0; i < items.length; i++) {
                                    if (items[i].type.indexOf("image") !== -1) {
                                        //image
                                        var blob = items[i].getAsFile();

                                        var URLObj = window.URL || window.webkitURL;
                                        var source = URLObj.createObjectURL(blob);
                                        //					this.paste_createImage(source);
                                        var fd = new FormData();
                                        var attrs = $("#add-project-file-chat").serialize().split("&");
                                        for (i = 0; i < attrs.length - 1; i++) {
                                            var temp = attrs[i].split('=');
                                            fd.append(temp[0], temp[1]);
                                        }
                                        fd.append("message", "PrintScreen");
                                        fd.append('file', blob);
                                        $.ajax({
                                            type: 'POST',
                                            url: '{{ route('frontend.projectchat.postprojectchatfile')}}',
                                            data: fd,
                                            processData: false,
                                            contentType: false
                                        }).done(function (data) {
                                            $('#content-chat ul').append(data.contents);

                                            //always scroll to bottom
                                            var scrollHeight = $('#content-chat').prop('scrollHeight');
                                            $("#content-chat").slimScroll({scrollTo: scrollHeight});
                                        });
                                    }
                                }
                                e.preventDefault();
                            }
                        }
                    };

                    function setUser() {
                        socket.emit('register', user);
                    }

                    function initListener() {
                        socket.on('default_room', function (msg) {
//                console.log(msg);

                            if (msg.project_chat_room_id == room && msg.sender_user_id != user.id) {
                                var image = '';
                                if (msg.isImage == 1) {
                                    image = '<br><a href="/' + msg.file + '" data-lightbox="image-' + msg.id + '"><img src="/' + msg.file + '" class"img-thumbnail"border-radius:16%; width="75px" data-lightbox="image-' + msg.id + '"></a>'
                                }
                                var audio = '';
                                if (msg.isAudio == 1) {
                                    audio = '<br><audio controls><source src="/' + msg.file + '" type="audio/ogg"><source src="' + msg.file + '" type="audio/mp3">Your browser does not support the audio element.</audio>'
                                }
                                $('#content-chat ul').append('<li class="in">' +
                                    '<img class="avatar" alt="" src="' + msg.avatar + '" />' +
                                    '<div class="message">' +
                                    '<a href="javascript:void(0);" class="name"> ' + msg.email + ' </a>' +
                                    '<p class="datetime"> ' + msg.time + '</p>' +
                                    '<p class="body"> ' + msg.content + ' ' + image + '' + audio + '</p>' +
                                    '</div>' +
                                    '</li>');
                                $('.chat-form .message').val('');
                                var scrollHeight = $('#content-chat').prop('scrollHeight');
                                $("#content-chat").slimScroll({scrollTo: scrollHeight});

                            }

                        })
                    }

                    $(document).ready(function () {
                        setUser();
                        initListener();
                    });

                    console.log('console socket k lia');
                    new Vue({
                        el: '.page-content',
                        data: {

                        },
                        methods: {},
                        mounted: function (e) {
                            /**
                             * Redis Listener Example....!
                             * */
                            socket.emit('global-channel:project_chat');
                            that = this;
                            socket.on('global-channel:project_chat', function (data) {
                                console.log('socket c data aa gaya ha');
                                window.newMsgCount = 0;
                                showChat();
                            });
                        }
                    });

                </script>


{{--                <script src="{{asset('custom/js/frontend/OnOffswitch.js')}}" type="text/javascript"></script>--}}
                <script>
                    $(document).ready(function () {
                        $(".checkbox1").Sswitch({
                            onSwitchChange: function () {
                            }
                        });
                        $(".checkbox2").Sswitch({
                            onSwitchChange: function () {
                            }
                        });
                        $(".checkbox3").Sswitch({
                            onSwitchChange: function () {
                            }
                        });
                    });
                </script>

                <script src="/assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
                <script src="/assets/global/plugins/bootstrap-summernote/lang/summernote-zh-CN.js" type="text/javascript"></script>
{{--                <script src="/custom/js/frontend/favouriteJobs.js" type="text/javascript"></script>--}}
                <script>
                    $(function () {
                        $('body').on('click', '.sendMessage', function(){
                            var form = new FormData();
                            form.append("message", $('#msg').val());
                            var settings = {
                                "url": "/sendPrivateMessage/{{isset(\Illuminate\Support\Facades\Auth::user()->id) ? \Illuminate\Support\Facades\Auth::user()->id : ''}}",
                                "method": "POST",
                                "data": form,
                                "processData": false,
                                "contentType": false,
                                "mimeType": "multipart/form-data",
                            }

                            $.ajax(settings).done(function (response) {
                                toastr.success('Message Sent');
                                $('#remote-modal').modal('hide');
                            });
                        })


                    })
                </script>
                <script>
                    var lang = {
                        type_or_click_here: "{{ trans('common.type_or_click_here') }}",
                        characters_left: "{{ trans('common.characters_left') }}",
                        please_select: "{{ trans('common.please_select') }}",
                        unable_to_request: "{{ trans('common.unable_to_request') }}",
                        max_upload_eight_documents: "{{ trans('common.max_upload_eight_documents') }}",
                        please_input_project_name: "{{ trans('common.please_input_project_name') }}",
                        please_choose_category: "{{ trans('common.please_choose_category') }}",
                        please_choose_country: "{{ trans('common.please_choose_country') }}",
                        please_choose_language: "{{ trans('common.please_choose_language') }}",
                        please_input_description: "{{ trans('common.please_input_description') }}",
                        please_choose_skill: "{{ trans('common.please_choose_skill') }}",
                        please_choose_pay_type: "{{ trans('common.please_choose_pay_type') }}",
                        please_choose_commitment_time: "{{ trans('common.please_choose_commitment_time') }}",
                        please_choose_freelance_type: "{{ trans('common.please_choose_freelance_type') }}",
                        please_input_question: "{{ trans('common.please_input_question') }}",
                        please_input_reference_price: "{{ trans('common.please_input_reference_price') }}",
                        remove: "{{ trans('common.remove') }}",
                        image_is_bigger_than_5mb: "{{ trans('common.image_is_bigger_than_5mb') }}",
                        you_can_not_upload_any_more_files: "{{ trans('common.you_can_not_upload_any_more_files') }}",
                        drop_files_here_to_upload: "{{ trans('common.drop_files_here_to_upload') }}",
                        cancel_upload: "{{ trans('common.cancel_upload') }}",
                        invalid_file_type_for_post_project: "{{ trans('common.invalid_file_type_for_post_project') }}",
                        example_job_description: "{{ trans('common.example_job_description') }}",
                        field_required: "{{ trans('common.field_required') }}",
                        new_skill_keywords: "{{ trans('common.new_skill_keywords') }}",
                        please_use_wiki_or_baidu_support_your_keyword: "{{ trans('common.please_use_wiki_or_baidu_support_your_keyword') }}",
                        one_of_skill_input_empty: "{{ trans('common.one_of_skill_input_empty') }}",
                        maximum_ten_skill_keywords_allow_to_submit: "{{ trans('common.maximum_ten_skill_keywords_allow_to_submit') }}",
                        submit_successful: "{{ trans('common.submit_successful') }}",
                        help_us_improve_skill_keyword: "help",
                    };

                    $(document).ready(function () {

                        var applicant = '{!! $applicant !!}';
                        $('#desc-summer').summernote('code', applicant.description);
                        $('#desc-summer').summernote('destroy');

                        $(".checkbox1").Sswitch({
                            onSwitchChange: function () {
                            }
                        });
                        $(".checkbox2").Sswitch({
                            onSwitchChange: function () {
                            }
                        });
                        $(".checkbox3").Sswitch({
                            onSwitchChange: function () {
                            }
                        });

                        $('.edit-link-trigger').mouseenter(function () {
                            $(this).find('.edit-link-show').show();
                            $(this).find(".edit-target").addClass('edit-target-info');
                            $(this).find(".edit-target-status").addClass('edit-target-status-hover')
                        });

                        $('.edit-link-trigger').mouseleave(function () {
                            $(this).find('.edit-link-show').hide();
                            $(this).find(".edit-target").removeClass('edit-target-info');
                            $(this).find(".edit-target-status").removeClass('edit-target-status-hover')
                        });

                        $(".edit-target-link").hide();

                        $('.edit-target-box').mouseenter(function () {
                            $(this).addClass("personal-portfolio-boxes");
                            $(this).find(".edit-target-link").show();
                        });

                        $('.edit-target-box').mouseleave(function () {
                            $(this).removeClass("personal-portfolio-boxes");
                            $(this).find(".edit-target-link").hide();
                        });

                        $("#price_type").change(function () {
                            if ($("#price_type option:selected").val() == '3') {
                                $("#price").prop('disabled', true);
                                $("#label-div-price-fix").hide();
                            }
                            else {
                                $("#price").prop('disabled', false);
                                $("#label-div-price-fix").show();
                            }
                        });

                        //skill
                        // var skillList = <?php echo $skillList; ?>;
                        // var skill_id_list = $('#skill_id_list').val();
                        // var selectedSkillList = [];
                        // if(skill_id_list != ""){
                        //     selectedSkillList = skill_id_list.split(",")
                        // }
                        // var ms = $('#magicsuggest').magicSuggest({
                        //     allowFreeEntries: false,
                        //     allowDuplicates: false,
                        //     placeholder: lang.type_or_click_here,
                        //     data: skillList,
                        //     value: selectedSkillList,
                        //     method: 'get',
                        //     renderer: function(data){
                        //         if( data.description == null )
                        //         {
                        //             return '<b>' + data.name + '</b>' ;
                        //         }
                        //         else
                        //         {
                        //             var name = data.name.split("|");

                        //             return '<b>' + name[0] + '</b> | <span class="search-desc">' + data.description + '</span>';
                        //         }

                        //     },
                        //     selectionRenderer: function(data){
                        //         result = data.name;
                        //         name = result.split("|")[0];
                        //         return name;
                        //     }
                        // });
                        // $(ms).on(
                        //     'selectionchange', function(e, cb, s){
                        //         $('#skill_id_list').val(cb.getValue());
                        //         if($('#magicsuggest').hasClass('error-border') && $('input[name="skill_id_list"]').val() != 0){
                        //             $('#magicsuggest').removeClass('error-border');
                        //             $("#magicsuggest").attr('style', '');
                        //             $("#skill-require").hide();
                        //         }
                        // });

                        if ($('#tbxDescription').val() != null)
                            updateCountTbxDescription();

                        var language = $('html').attr('lang');
                        var summernoteLanguage = '';
                        if (language == "cn") {
                            summernoteLanguage = "zh-CN";
                        }
                        else {
                            summernoteLanguage = "en-US";
                        }

                        $('#snDescription').summernote({
                            placeholder: lang.example_job_description,
                            lang: summernoteLanguage,
                            height: 300,
                            toolbar: [
                                ['style', ['bold', 'fontsize']],
                                ['insert', ['link', 'picture', 'codeview']],
                            ],
                            callbacks: {
                                onChange: function (contents, $editable) {
                                    var stripHtmlTag = contents.replace(/<(?:.|\n)*?>/gm, '');

                                    $('#tbxDescription').val(contents);
                                    updateCountTbxDescription();

                                    // if(stripHtmlTag.length > 0 )
                                    // {
                                    //     if($('.note-editor').hasClass('error-border') && $('#tbxDescription').val() != '' ){
                                    //         $('.note-editor').removeClass('error-border');
                                    //         $("#description-require").hide();
                                    //     }
                                    // }
                                    // else
                                    // {
                                    //     $('.note-editor').addClass('error-border');
                                    //     $("#description-require").show();
                                    // }
                                },
                                onImageUpload: function (files) {
                                    for (var i = 0; i < files.length; i++) {
                                        send(files[i]);
                                    }
                                }
                            }
                        });

                        function updateCountTbxDescription(e) {
                            var qText = $("#tbxDescription").val();

                            if (qText.length <= 5000) {
                                $("#noOfCharDescription").html(5000 - qText.length);
                            } else {
                                jQuery("#noOfCharDescription").html(0);
                                $("#tbxDescription").val(qText.substring(0, 5000));
                            }
                        }

                        function updateCountTbxQuestion(e) {
                            var questionId = parseInt(this.id.replace("tbxQuestion-", ""));

                            var qText = $("#tbxQuestion-" + questionId).val();

                            if (qText.length <= 256) {
                                $("#noOfCharQuestion-" + questionId).html(256 - qText.length);
                            } else {
                                jQuery("#noOfCharQuestion-" + questionId).html(0);
                                $("#tbxQuestion-" + questionId).val(qText.substring(0, 256));
                            }
                        }

                        var myHtml = $('#snDescription').summernote('code');
                        $('input[name="description"]').val(myHtml);

                        var applicant = '{!! $applicant !!}';
                        var skillList = '{!! $skillList !!}';
                        if (applicant && applicant.skill_id_string == null)
                            applicant.skill_id_string = '{!! $skill_id_list !!}';

                        if (applicant && applicant.skill_id_string == null)
                            var skill_id_list = applicant.skill_id_string;
                        var selectedSkillList = [];
                        if (skill_id_list != "") {
                            try {
                                selectedSkillList = skill_id_list.split(",")
                            } catch (e) {
                                console.log(e);
                            }
                        }
                        var noSuggestionText = "<a href='javascript:;' data-toggle='modal' data-target='#modelAddNewSkillKeyword'>" + lang.help_us_improve_skill_keyword + "</a>";
                        var msSkills = $('#msSkills').magicSuggest({
                            allowFreeEntries: true,
                            allowDuplicates: false,
                            placeholder: g_lang.search,
                            data: skillList,
                            value: selectedSkillList,
                            method: 'get',
                            highlight: true,
                            noSuggestionText: noSuggestionText,
                            renderer: function (data) {
                                if (data.description == null) {
                                    return '<b>' + data.name + '</b>';
                                }
                                else {
                                    var name = data.name.split("|");

                                    return '<b>' + name[0] + '</b> | <span class="search-desc">' + data.description + '</span>';
                                }

                            },
                            selectionRenderer: function (data) {
                                result = data.name;
                                name = result.split("|")[0];
                                return name;
                            }
                        });
                        $(msSkills).on(
                            'selectionchange', function (e, cb, s) {
                                $('#skill_id_list').val(cb.getValue());
                            }
                        );
                    });
                </script>
            @endif
        @endsection

        @push('vue')
            <style>
                .chat-room-div
                {
                    position:relative;
                    /*// so that .modal & .modal-backdrop gets positioned relative to it*/
                }
                .modal-backdrop {

                }
                {
                    width:100%;
                    height:200px;
                }
                .modal-backdrop.in{
                    opacity: .9;
                    /*position: absolute;*/
                }
                .modal-body {
                    text-align: center !important;
                }
                .modal-body p {
                    margin-top: 40px !important;
                }
            </style>
            @if(\Auth::guard('users')->check())
                <script>
                    // setTimeout(function(){
                    //     alert("working");
                    //     $('#myModal').modal('show');
                    // }, 5000);

                    $(function () {
                        setTimeout(function(){
                                    {{--alert()--}}
                            var avatar = '{{\Auth::user()->img_avatar}}';
                            var name = '{{\Auth::user()->name}}';
                            var city = '{{\Auth::user()->city_id}}';

                            console.log(name);
                            console.log(avatar);
                            console.log(city);

                            if(!name || !avatar || !city) {
                                $("#myModal").modal({backdrop: 'static', keyboard: false});
                                //appending modal background inside the blue div
                                $('.modal-backdrop').appendTo('.chat-room-div');

                                //remove the padding right and modal-open class from the body tag which bootstrap adds when a modal is shown
                                $('body').removeClass("modal-open")
                                $('body').css("padding-right", "");
                            }
                        }, 4000)

                    })
                    var applicant = '{!! $applicant !!}';
                    var project = '{!! $project !!}';

                    if (applicant.title == null)
                        applicant.title = project.name;

                    if (applicant.description == null)
                        applicant.description = project.description;

                    if (applicant.skill_id_string == null)
                        applicant.skill_id_string = {!! $skill_id_list !!};

                    if (applicant.reference_price == null)
                        applicant.reference_price = applicant.quote_price;

                    if (applicant.reference_price_type == null)
                        applicant.reference_price_type = applicant.pay_type;

                    try {
                        applicant.description = applicant.description.trim();
                    } catch (e) {
                        console.log(e);
                    }
                    Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
                    // Vue.http.headers.common['X-CSRF-TOKEN'] =
                    new Vue({
                        el: "#joinDiscussion",
                        data: {
                            applicant: applicant,
                            project: project,
                            skill: [],
                        },
                        methods: {
                            change: function (val) {
                                // alert("123");
                                //                e.preventDefault();
                                if (val === 'title') {
                                    var input = {
                                        'id': this.applicant["id"],
                                        'val': this.applicant[val],
                                        'column': val,
                                    };
                                }
                                else if (val === 'description') {
                                    var input = {
                                        'id': this.applicant['id'],
                                        'val': $('input[name="description"]').val(),
                                        'column': val,
                                    };
                                }
                                else if (val === 'reference_price') {
                                    var input = {
                                        'id': this.applicant["id"],
                                        'val': this.applicant[val],
                                        'column': val,
                                        'reference_price_type': this.applicant['reference_price_type'],
                                    }

                                }
                                else if (val === 'skill_id_string') {
                                    var input = {
                                        'id': this.applicant['id'],
                                        'val': $('input[name="skill_id_string"]').val(),
                                        'column': val,
                                    };
                                }
                                this.$http.post('/joinDiscussion/change', input).then((response) => {
                                    window.location.reload();
                                    // $('#modalChangePostTitle').modal('toggle');
                                }, (response) => {
                                    //                   error
                                });
                            }
                        }
                    });
                </script>
    @endif
    @endpush
