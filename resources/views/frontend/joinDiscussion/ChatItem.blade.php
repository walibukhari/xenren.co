@foreach($data as $val)
<li class="in">
    <img class="avatar" alt="" src="{{ $val['avatar'] }}">
    <div class="message">
        <a href="javascript:void(0);" class="name">
            {{ $val['email'] }}
        </a>
        <p class="datetime">
            {{ $val['time'] }}
        </p>
        <p class="body">
            {{ $val['content'] }}
            <br>
            {{--<a href="{{ $file }}" data-lightbox="image-undefined">--}}
                {{--<img src="{{ $file }}" class="img-thumbnail" data-lightbox="image-undefined" width="75px">--}}
            {{--</a>--}}
        </p>
    </div>
</li>
@endforeach