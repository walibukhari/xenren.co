@extends('frontend.layouts.default')

@section('title')
    End Contract | Employer Review
@endsection

@section('description')
    Employer End Contract
@endsection

@section('author')
    michael
@endsection

@section('header')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rating/1.4.0/bootstrap-rating.min.css" rel="stylesheet" type="text/css" />
    <style>
        .btn-green{
            background-color: #58af2a !important;
            color: #fff !important;
        }
        .btn-green:hover{
            background-color: #4B9320 !important;

        }
        .btn-green-border{
            border: solid thin #58af2a !important;
            background-color: transparent;
            color:  #58af2a !important;
            margin-left: 20px;;
        }
        .btn-green-border:hover{
            border: solid thin #4B9320 !important;
        }

    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3 search-title-container">
            <span class="find-experts-search-title text-uppercase">Filter</span>
        </div>
        <div class="col-md-9 right-top-main">

        </div>
    </div>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div>
                <form method="post" action="{{route("frontend.endcontract.employer")}}">
                    {{csrf_field()}}
                    <h3>
                        End Contract {{$project->name}}
                    </h3>
                    <h5>Share your experience! Your honest feedback provides helpful information to both the employer and the Xenren community
                    </h5>
                    <div class="row" style="padding: 10px;">
                        <h3>
                            <span class="icon-lock"></span> Private Feedback
                        </h3>
                        <h5>
                            This feedback will be kept anonymous and never shared directly with the employer. <a href="#">Learn more</a>

                        </h5>
                        <div class="form-group col-sm-4">
                            <label for="reasondropdown">Reason for ending contract</label>
                            <select class="form-control" id="reasondropdown">
                                <option value="project completed">Project completed</option>
                                <option value="no response">No response</option>
                                <option value="project cancelled">Project cancelled</option>
                                <option value="terms and conditions changed">Terms and condition changed</option>
                            </select>
                        </div>
                        <div class="col-sm-8"></div>
                        <div class="form-group col-sm-12">
                            <label for="recommend_val">How likely are you to recommend this freelancer to a friend or colleague
                            </label>
                            <div>
                                <label class="form-check-inline">
                                    <small>
                                        Not at all likely
                                    </small>
                                </label>
                                <label class="form-check-inline">
                                    <input class="form-check-input" type="radio" name="recommendation_val" id="recommend_val" value="1"> 1
                                </label>
                                <label class="form-check-inline">
                                    <input class="form-check-input" type="radio" name="recommendation_val" id="recommend_val" value="2"> 2
                                </label>
                                <label class="form-check-inline">
                                    <input class="form-check-input" type="radio" name="recommendation_val" id="recommend_val" value="3"> 3
                                </label>
                                <label class="form-check-inline">
                                    <input class="form-check-input" type="radio" name="recommendation_val" id="recommend_val" value="4"> 4
                                </label>
                                <label class="form-check-inline">
                                    <input class="form-check-input" type="radio" name="recommendation_val" id="recommend_val" value="5"> 5
                                </label>
                                <label class="form-check-inline">
                                    <input class="form-check-input" type="radio" name="recommendation_val" id="recommend_val" value="6"> 6
                                </label>
                                <label class="form-check-inline">
                                    <input class="form-check-input" type="radio" name="recommendation_val" id="recommend_val" value="7"> 7
                                </label>
                                <label class="form-check-inline">
                                    <input class="form-check-input" type="radio" name="recommendation_val" id="recommend_val" value="8"> 8
                                </label>
                                <label class="form-check-inline">
                                    <input class="form-check-input" type="radio" name="recommendation_val" id="recommend_val" value="9"> 9
                                </label>
                                <label class="form-check-inline">
                                    <input class="form-check-input" type="radio" name="recommendation_val" id="recommend_val" value="10"> 10
                                </label>
                                <label class="form-check-inline">
                                    <small>
                                        Extremely likely
                                    </small>
                                </label>
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="recommend_val">	Rate their English proficiency (Reading and Comprehension)
                            </label>
                            <div>
                                <label class="form-check-inline">
                                    <input class="form-check-input" type="radio" name="proficiency_val" id="" value="difficult to understand"> Difficult to understand
                                </label>
                                <label class="form-check-inline">
                                    <input class="form-check-input" type="radio" name="proficiency_val" id="" value="acceptable"> Acceptable
                                </label>
                                <label class="form-check-inline">
                                    <input class="form-check-input" type="radio" name="proficiency_val" id="" value="fluent"> Fluent
                                </label>
                                <label class="form-check-inline">
                                    <input class="form-check-input" type="radio" name="proficiency_val" id="" value="i didn't speak to freelancer"> I didn't speak to freelancer
                                </label>

                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding: 10px;">
                        <h3>
                            <span class="icon-lock-open"></span> Public Feedback
                        </h3>
                        <h5>
                            This feedback will be shared on your employer's profile only after they have left feedback for you.  <a href="#">Learn more</a>
                        </h5>
                        <h3>Feedback to employer</h3>
                        <div class="form-group">
                            <input type="hidden" class="rating feedback" name="payment_time" value="1"/> <span style="margin-left:10px;">Payment on time</span>
                        </div>
                        <div class="form-group">
                            <input type="hidden" class="rating feedback" name="communication" value="1"/> <span style="margin-left:10px;">Communication</span>
                        </div>
                        <div class="form-group">
                            <input type="hidden" class="rating feedback" name="availability" value="1"/> <span style="margin-left:10px;">Availability</span>
                        </div>
                        <div class="form-group">
                            <input type="hidden" class="rating feedback" name="trustworthy" value="1"/> <span style="margin-left:10px;">Trustworthy</span>
                        </div>
                        <div class="form-group">
                            <input type="hidden" class="rating feedback" name="clear_requirement" value="1"/> <span style="margin-left:10px;">Clear Requirement</span>
                        </div>
                        <div class="form-group">
                            <h5>TOTAL SCORE: <span id="ratings-total" class="text-success h4"></span></h5>
                        </div>

                        <div class="form-group col-sm-8">
                            <h5> Share your experience with the Xenren community</h5>
                            <textarea name="experience" placeholder="Share your experience" rows="7" cols="70" class="input-xs"></textarea>
                        <div class="col-sm-4"></div>
                        <div> <a href="#"><small>
                                        See an example
                                    </small>
                                </a>
                            <h5>Ending this contract will permanently lock your work out of this project. We will let your employer know the job is done and send you a final statement for any unpaid work</h5>
                        </div>
                    </div>
                </div>

                    <button class="btn btn-green" type="submit">End Contract</button>
                    <button class="btn btn-green-border " type="reset">Cancel</button>


                </form>
            </div>

        </div>
    </div>
    <!-- END CONTENT -->

@endsection


@section('footer')
    {{--<script src="/assets/global/plugins/jquery-countdown/jquery.countdown.min.js" type="text/javascript"></script>--}}
    {{--<script src="/custom/js/frontend/myOrders.js" type="text/javascript"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rating/1.4.0/bootstrap-rating.min.js"></script>

    <script>
        +function () {
            $(document).ready(function () {
                function add(a, b) {
                    return parseInt(a) + parseInt(b);
                }

                    $(".rating.feedback").on('change', function(){
                        var values = [];
                            $(".rating.feedback").each(function() {
                                values.push($(this).val());
                            });
                        var sum = (values.reduce(add,0));
                        var avgRating = (sum/5).toFixed(2);
                        $("#ratings-total").html(avgRating);

                    });

                $('span.counter').each (function (index, value) {
                    $(this).countdown($(this).data('countdown'), function(event) {
                        $(this).html(
                                event.strftime('%D {{ trans('member.days') }} %H {{ trans('member.hours') }} %M {{ trans('member.minutes') }} %S {{ trans('member.seconds') }}')
                        );
                    });
                });
                $('body').on('click', '.delete-apply', function () {
                    var applicant_id = $(this).data('applicantid');
                    var $this = $(this);
                    $.ajax({
                        url: '{{ route('frontend.order.applydelete') }}' + '/' + applicant_id,
                        dataType: 'json',
                        data: {'_token': '{!! csrf_token() !!}'},
                        method: 'post',
                        beforeSend: function () {
                            $this.prop('disabled', true);
                        },
                        error: function (response, statusText, xhr, formElm) {
                            if (typeof response !== 'undefined' && typeof response.status !== 'undefined' && typeof response.responseText !== 'undefined' && typeof response.responseJSON !== 'undefined') {
                                if (response.status == 422) {
                                    $.each(response.responseJSON, function(i) {
                                        $.each(response.responseJSON[i], function(key, value) {
                                            alertError(value, false);
                                        });
                                    });
                                } else {
                                    alertError('{{ trans('order.unable_to_delete_apply') }}', true);
                                }
                            } else {
                                alertError('{{ trans('order.unable_to_delete_apply') }}', true);
                            }
                            $this.prop('disabled', false);
                        },
                        success: function (resp) {
                            if (resp.status == 'success') {
                                var ct = $this.parent('.tailor-action');

                                //Append the delete apply job button
                                var btn = '<button type="button" class="btn default apply-order" data-orderid="' + resp.data.order_id + '">{{ trans('member.apply_for_order') }}</button>';

                                //Remove the delete apply button
                                ct.find('span.font-green01').remove();
                                ct.find('span.font-apply').remove();
                                ct.find('a.delete-apply').remove();

                                ct.prepend(btn);
                                alertSuccess('{{ trans('order.successfully_delete_apply') }}', true);
                            } else {
                                alertError('{{ trans('order.unable_to_delete_apply') }}', true);
                            }
                        }
                    });
                });
            });
        }(jQuery);
    </script>
@endsection




