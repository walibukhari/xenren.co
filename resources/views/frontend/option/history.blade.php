@extends('frontend.layouts.default')

@section('title')
@endsection

@section('keywords')
    {{ trans('common.jobs_keyword') }}
@endsection

@section('description')
@endsection
@section('ogdescription')
@endsection

@section('author')
    Xenren
@endsection

@section('url')
    https://www.xenren.co/options
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
    <link href="{{asset('css/minifiedJobs.css')}}" rel="stylesheet">
@endsection

@section('customStyle')
    <style>
        .setSideMenuScroll{
            display: none !important;
        }
        .page-md .page-sidebar.navbar-collapse, .page-md .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover.navbar-collapse {
            display: none !important;
        }
        .topnav {
            overflow: hidden;
            background-color: #5ec329;
            margin: -19px;
            color: white;
            display: none !important;
        }
        .page-content{
            text-align: center;
        }
        .setppppp{
            font-size: 30px;
        }
        .h4pp{
            margin-bottom: 40px;
        }
        .set-text-center{
            display: flex;
            justify-content:center;
        }
        @media (max-width: 991px) {
            .custom-form-css{
                background: #fff;
                padding: 20px;
                border-radius: 12px;
                width: 100%;
                display: inline-block;
            }
            .set-text-center {
                display: flex;
                justify-content: center;
                padding: 0px !important;
            }
            .custom-css-col-6{
                width: 100%;
                padding: 0px !important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="page-content">
        <h2 class="h4pp">User History</h2>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <td>Office Name</td>
                        <td>Seat Image</td>
                        <td>Seat Name</td>
                        <td>Seat No</td>
                        <td>Cost</td>
                        <td>Start Time</td>
                        <td>End Time</td>
                    </tr>
                </thead>
                <tbody>
                @foreach($history as $data)
                    <tr>
                        <td>{{$data->office->office_name}}</td>
                        <td><img style="width: 50px;" src="{{$data->office->product->getProductImage($data->office->product->category_id)}}" /></td>
                        <td>{{$data->office->product->getProductName($data->office->product->category_id)}}</td>
                        <td>{{$data->office->product->number}}</td>
                        <td>{{$data->cost}}</td>
                        <td>{{$data->start_time}}</td>
                        <td>{{$data->end_time}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('footer')

@endsection
