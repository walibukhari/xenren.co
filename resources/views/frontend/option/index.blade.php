@extends('frontend.layouts.default')

@section('title')
@endsection

@section('keywords')
    {{ trans('common.jobs_keyword') }}
@endsection

@section('description')
@endsection
@section('ogdescription')
@endsection

@section('author')
    Xenren
@endsection

@section('url')
    https://www.xenren.co/options
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
    <link href="{{asset('css/minifiedJobs.css')}}" rel="stylesheet">
@endsection

@section('customStyle')
    <style>
        .setSideMenuScroll{
            display: none !important;
        }
        .page-md .page-sidebar.navbar-collapse, .page-md .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover.navbar-collapse {
            display: none !important;
        }
        .topnav {
            overflow: hidden;
            background-color: #5ec329;
            margin: -19px;
            color: white;
            display: none !important;
        }
        .page-content{
            text-align: center;
        }
        .setppppp{
            font-size: 30px;
        }
        .h4pp{
            margin-bottom: 40px;
        }
    </style>
@endsection

@section('content')
    <div class="page-content">
        <h2 class="h4pp">Choose Your option</h2>
        <p class="setppppp">
            <a href="https://xenren.app.link/100Coworkers">Exist</a>
        </p>
        <p class="setppppp">
            <a href="{{route('gotToPhone',[\Session::get('lang'),$seatId,$officeId])}}">Not Exist</a>
        </p>
    </div>
@endsection

@section('footer')

@endsection
