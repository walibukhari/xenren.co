@extends('frontend.layouts.default')

@section('title')
@endsection

@section('keywords')
    {{ trans('common.jobs_keyword') }}
@endsection

@section('description')
@endsection
@section('ogdescription')
@endsection

@section('author')
    Xenren
@endsection

@section('url')
    https://www.xenren.co/options
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
    <link href="{{asset('css/minifiedJobs.css')}}" rel="stylesheet">
@endsection

@section('customStyle')
    <style>
        .setSideMenuScroll{
            display: none !important;
        }
        .page-md .page-sidebar.navbar-collapse, .page-md .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover.navbar-collapse {
            display: none !important;
        }
        .topnav {
            overflow: hidden;
            background-color: #5ec329;
            margin: -19px;
            color: white;
            display: none !important;
        }
        .page-content{
            text-align: center;
        }
        .setppppp{
            font-size: 30px;
        }
        .h4pp{
            margin-bottom: 40px;
        }
        .set-text-center{
            display: flex;
            justify-content:center;
            align-items: center;
        }
        .set-inline{
            display: flex;
            align-items: center;
        }
        .set-width{
            width:60px !important;
        }
        .custom_set_css{
            width: 20px;
        }
        .set-direction-column{
        }
        .custom-form-css{
            background: #fff;
            padding: 20px;
            border-radius: 12px;
            width: 60%;
            display: inline-block;
        }
        @media (max-width: 991px) {
            .custom-form-css{
                background: #fff;
                padding: 20px;
                border-radius: 12px;
                width: 100%;
                display: inline-block;
            }
            .set-text-center {
                display: flex;
                justify-content: center;
                padding: 0px !important;
            }
            .custom-css-col-6{
                width: 100%;
                padding: 0px !important;
            }
        }
        .display-inline{}
        [v-cloak]{
            display: none !important;
        }
        .setCustomPrimaryBntHistory{
            position: absolute !important;
            right: 20px;
            top: 40px;
        }
    </style>
@endsection

@section('content')
    <div class="page-content" id="select-seat">
        <h2 class="h4pp" v-if="check_timer == false">Select Your Seat</h2>
        <h2 class="h4pp" v-else>Tracking Your Time</h2>
        <button class="setCustomPrimaryBntHistory btn btn-primary" onclick="window.location.href='{{route('userHistory',[$user->id])}}'">History</button>
        <div class="row">
            <div class="col-md-12">
                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{session()->get('error')}}
                    </div>
                @endif
            </div>
        </div>
        <form class="custom-form-css" v-if="check_timer == false">
            @csrf
            <input type="hidden" name="seat_id" id="seat_name">
            <div class="display-inline">

            </div>
            @foreach($office_products as $data)
            <div class="row">
                <div class="col-md-12 set-text-center">
                    <div class="col-md-6 custom-css-col-6">
                        <div class="form-group set-inline">
                            <input type="checkbox" id="{{$data->number}}" @change="selectOption('a',$event)" name="{{$data->category_id}}" class="checkOpt set-width" />
                            <p class="set-direction-column">
                                <img src="{{$data->getProductImage($data->category_id)}}" class="custom_set_css" />
                                {{$data->getProductName($data->category_id)}}
                            </p>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <b>{{$data->hourly_price}} / hr</b>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="row">
                <button type="button" class="btn btn-success" @click="submitForm">Submit</button>
            </div>
        </form>
        <form v-else>
            <div v-if="timer_stop == false">
                <p>@{{ hours }} : @{{ mins }} : @{{ secs }}</p>
                <div class="row">
                    <div class="col-md-12">
                        <button type="button" @click="checkout">Checkout</button>
                    </div>
                </div>
            </div>
            <div v-else>
                <p style="font-weight: bold;font-size: 20px;">Total Cost : @{{ total_cost }}</p>
            </div>
        </form>
    </div>
@endsection

@section('footer')
    <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <script>
        $('.scroll-div').slimScroll({});
    </script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/vue.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vue-resource.min.js')}}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.15/lodash.core.min.js"></script>
    <script>
        let vu = new Vue({
            el: '#select-seat',
            data: {
                loaderLoad:true,
                office_id:'{{$office->id}}',
                seat_id:'',
                start_time:'{{\Carbon\Carbon::now()->format('H:i:s')}}',
                end_time:'',
                start_date:'{{\Carbon\Carbon::now()->format('Y-m-d h:i:s')}}',
                end_date:'',
                scan_type:'1',
                user_token:'',
                access_token:`Bearer {{$access_token}}`,
                user_id:'{{$user->id}}',
                check_timer:false,
                countUp:0,
                hours:'00',
                mins:'00',
                secs:'00',
                time:0,
                running:0,
                timer_stop:false,
                total_cost:0
            },
            methods: {
                selectOption: function (val,event) {
                    $('.checkOpt').prop('checked', false);
                    console.log(event);
                    console.log(event.target.id);
                    let id = event.target.id;
                    $('#' + id).prop('checked', true);
                    this.seat_id = id;
                    $('#seat_name').val(id);
                },
                checkUserAlreadyCheckIn: function(){
                    console.log('this.user_id');
                    console.log(this.user_id);
                    let url = '/api/checkUserAlreadyCheckIn/'+this.user_id;
                    this.$http.get(url).then((response) => {
                        if(response.body.status == true) {
                            this.check_timer = true;
                            this.running = 1;
                        }
                        console.log('timer checking & running');
                        console.log(this.check_timer);
                        console.log(this.running);
                    }).catch((error) => {
                        console.log('error');
                        console.log(error);
                    });
                },
                submitForm: function () {
                    let access_token = this.access_token;
                    console.log(access_token);
                    console.log(this.office_id);
                    console.log(this.seat_id);
                    console.log(this.start_time);
                    console.log(this.start_date);
                    console.log(this.scan_type);
                    let url = "/api/sharedofficeQrSeat";
                    let form = new FormData();
                    form.append('office_id', this.office_id);
                    form.append('qr_seat', this.seat_id);
                    form.append('start_time', this.start_time);
                    form.append('end_time', this.end_time);
                    form.append('start_date', this.start_date);
                    form.append('end_date', this.end_date);
                    form.append('scan_type', this.scan_type);
                    form.append('access_token', this.scan_type);
                    form.append('other_scanner', 1);
                    this.$http.post(url, form , {
                        headers: {
                            Authorization: access_token,
                            Accept: 'application/json',
                        },
                    }).then((response) => {
                        console.log('response');
                        console.log(response);
                        if(response.data.status == 'success') {
                            window.location.reload();
                        } else {
                            toastr.error(response.data.message);
                        }
                    }).catch((error) => {
                        console.log('response');
                        console.log(error);
                    });
                },
                checkout: function(){
                    window.running = false;
                    console.log('going to checkout and get bill');
                    console.log("this.hours + ':' + this.mins + ':' + this.secs");
                    console.log(this.hours + ':' + this.mins + ':' + this.secs);
                    console.log(window.running);
                    clearInterval(window.tracking);
                    this.stopTimer();
                },
                stopTimer: function(){
                    let access_token = this.access_token;
                  let url = "{{ route('api.sharedofficeStopTimer') }}";
                  this.$http.get(url,{
                      headers: {
                          Authorization: access_token,
                          Accept: 'application/json',
                      },
                  }).then((response) => {
                      console.log('response stop timer api');
                      console.log(response.data.timeCost);
                      console.log(response.data.status);
                      if(response.data.status == 'success') {
                            this.timer_stop = true;
                            console.log(response.data.timeCost.totalCost)
                          this.total_cost = response.data.timeCost.totalCost;
                      }
                  }).catch((error) => {
                     console.log('error');
                     console.log(error);
                  });
                },
                starting: function() {
                    if (this.time > 0) {
                        var hours = Math.floor(this.time / 3600);
                        var mins = Math.floor(this.time % 3600 / 60);
                        var secs = Math.floor(this.time % 3600 % 60);
                        if (hours < 10) {
                            this.hours = 0 + hours;
                        }
                        if (mins < 10) {
                            this.mins = 0 + mins;
                        }
                        if (secs < 60) {
                            this.secs = 0 + secs;
                        }
                    }
                },
                convertSecondToTime: function(d){
                    d = Number(d);
                    var h = Math.floor(d / 3600);
                    var m = Math.floor(d % 3600 / 60);
                    var s = Math.floor(d % 3600 % 60);

                    if(m < 10){
                        m = "0" + m;
                    }
                    return h + 'h ' + m  +'m';
                },
                increment: function(run=null){
                    this.time = Number(this.time);
                    this.time++;
                    console.log('starting increment');
                    console.log(this.time);
                    this.starting();
                }
            },
            mounted() {
                console.log('start works');
                let that = this;
                setTimeout(function () {
                    that.checkUserAlreadyCheckIn();
                }, 500);
                setTimeout(function () {
                    console.log('check timer running');
                    console.log('that.check_timer');
                    console.log(that.check_timer);
                    if (that.check_timer == true) {
                        console.log('starting track time');
                        window.tracking = setInterval(that.increment, 1000);
                    }
                }, 2000);
            }
        });
    </script>
@endsection

