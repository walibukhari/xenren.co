@extends('frontend.layouts.default')

@section('title')
@endsection

@section('keywords')
    {{ trans('common.jobs_keyword') }}
@endsection

@section('description')
@endsection
@section('ogdescription')
@endsection

@section('author')
    Xenren
@endsection

@section('url')
    https://www.xenren.co/options
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
    <link href="{{asset('css/minifiedJobs.css')}}" rel="stylesheet">
@endsection

@section('customStyle')
    <style>
        .setSideMenuScroll{
            display: none !important;
        }
        .page-md .page-sidebar.navbar-collapse, .page-md .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover.navbar-collapse {
            display: none !important;
        }
        .topnav {
            overflow: hidden;
            background-color: #5ec329;
            margin: -19px;
            color: white;
            display: none !important;
        }
        .page-content{
            text-align: center;
        }
        .setppppp{
            font-size: 30px;
        }
        .h4pp{
            margin-bottom: 40px;
        }
        .set-text-center{
            display: flex;
            justify-content:center;
        }
        @media (max-width: 991px) {
            .custom-form-css{
                background: #fff;
                padding: 20px;
                border-radius: 12px;
                width: 100%;
                display: inline-block;
            }
            .set-text-center {
                display: flex;
                justify-content: center;
                padding: 0px !important;
            }
            .custom-css-col-6{
                width: 100%;
                padding: 0px !important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="page-content">
        <h2 class="h4pp">Enter Your Phone Number</h2>
        @if(session()->has('error'))
            <div class="alert alert-danger">
                {{session()->get('error')}}
            </div>
        @endif
        <form action="{{route('savePhoneData')}}" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-12 set-text-center">
                    <div class="col-md-6 custom-css-col-6">
                        <div class="form-group">
                            <input type="hidden" name="seat_id" value="{{$seatId}}" />
                            <input type="hidden" name="office_id" value="{{$officeId}}" />
                            <input type="number" name="phone_number" class="form-control" placeholder="+923360796074" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                    <button class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
@endsection

@section('footer')

@endsection
