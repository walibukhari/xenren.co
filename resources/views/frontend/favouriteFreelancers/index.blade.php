@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.favourite_freelancers_title') }}
@endsection

@section('keywords')
    {{ trans('common.favourite_freelancers_keyword') }}
@endsection

@section('description')
    {{ trans('common.favourite_freelancers_desc') }}
@endsection

@section('author')
    Xenren
@endsection

@section('url')
    https://www.xenren.co/favouriteFreelancers
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
{{--    <link href="/custom/css/frontend/favouriteFreelancers.css" rel="stylesheet" type="text/css"/>--}}
{{--    <link href="/custom/css/frontend/modalInviteToWork.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="/custom/css/frontend/modalSendOffer.css" rel="stylesheet" type="text/css" />--}}
    <link href="{{asset('css/minifiedFavouriteFreelancer.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div class="row setColRow12">
        <div class="col-md-3 search-title-container">
            <span class="find-experts-search-title text-uppercase setMAINMENU">{{ trans('common.main_action') }}</span>
        </div>
        <div class="col-md-9 right-top-main setCol9RPP">
            <div class="row">
                <div class="col-md-12 top-tabs-menu">
                    @if ($favouriteFreelancers->lastPage() > 1)
                    <div class="find-experts-pagination pull-right setFEPPR">
                        {{$favouriteFreelancers->links()}}
                    </div>
                    @endif
                    <div class="caption">
                        <span class="find-experts-search-title text-uppercase setCAPTION">{{ trans('common.favourite_freelancers') }}</span>
                        <span class="count-record">({{ $totalCount }})</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-content-wrapper setFFPageContentWrapper">
        <div class="page-content">
            @if( $totalCount == 0 )
                <div class="portlet light text-center">
                    <div class="setFFPAGEFFYET">
                        {{ trans('common.you_dont_have_favourite_freelancer_yet') }}
                        <div class="setFFPAGEIMAGE">
                            <img src="\images\no-fav-freelancer-icon.jpg">
                        </div>
                    </div>
                </div>
            @else
                @foreach( $favouriteFreelancers as $favouriteFreelancer )
                <div id="freelancer-{{ $favouriteFreelancer->freelancer->id }}" class="portlet find-expert-box setPageContent">
                    <div class="row personal-profile">
                        <div class="profile-image col-md-3">
                            <div class="profile-userpic p-t-5 text-center">
                                <a href="">
                                    <img src="{{ $favouriteFreelancer->freelancer->getAvatar() }}" class="user-img img-responsive img-circle"
                                         alt=""/>
                                </a>

                                @if( $favouriteFreelancer->freelancer->getLatestIdentityStatus() == \App\Models\UserIdentity::STATUS_APPROVED )
                                <img src="/images/icons/verify.png" class="verify"/>
                                @endif
                            </div>
                            <div class="profile-name p-t-5 text-center">
                                <h3 class="setFFreelanceName">{{ $favouriteFreelancer->freelancer->getName() }}</h3>
                                <h4>{{ '$' . $favouriteFreelancer->freelancer->hourly_pay . ' /hr' }}</h4>
                            </div>
                        </div>
                        <div class="user-info col-md-6 setFF">
                            <div class="row basic-info">
                                <div class="col-md-12">
                                    <h1 class="find-expert-name">
                                        {{ $favouriteFreelancer->freelancer->getJobPosition() }}
                                    </h1>
                                </div>
                                <div class="col-md-12">
                                    <p class="description">
                                        {{ $favouriteFreelancer->freelancer->getAboutMe() }}
                                    </p>
                                </div>
                                <div class="col-md-12 setDivSkillCol">
                                    @foreach ( $favouriteFreelancer->freelancer->skills as $key => $userSkill)
                                    <div class="skill-div setDivSkill">
                                        <span class="item-skill">
                                            <span>{{ $userSkill->skill->getName() }}</span>
                                        </span>
                                    </div>
                                    @endforeach
                                    {{--<div class="skill-div">--}}
                                        {{--<span class="item-skill official">--}}
                                            {{--<img src="/images/logo_2.png" alt="" height="24px" width="24px">--}}
                                            {{--<span>PHP</span>--}}
                                        {{--</span>--}}
                                    {{--</div>--}}
                                    {{--<div class="skill-div">--}}
                                        {{--<span class="item-skill success">--}}
                                            {{--<img src="/images/checked.png" alt="" height="24px" width="24px">--}}
                                            {{--<span>JavaScript</span>--}}
                                        {{--</span>--}}
                                    {{--</div>--}}
                                    {{--<div class="skill-div">--}}
                                        {{--<span class="item-skill">--}}
                                            {{--<span>HTML5</span>--}}
                                        {{--</span>--}}
                                    {{--</div>--}}
                                    {{--<div class="skill-div">--}}
                                        {{--<span class="item-skill">--}}
                                            {{--<span>Adobe Photoshop</span>--}}
                                        {{--</span>--}}
                                    {{--</div>--}}
                                    {{--<div class="skill-div">--}}
                                        {{--<span class="item-skill">--}}
                                            {{--<span>Adobe ilusstrator</span>--}}
                                        {{--</span>--}}
                                    {{--</div>--}}
                                </div>

                                <div class="col-md-12 latest-login setFFLatestLogin">
                                    <span class="text-capitalize setFFTextCap">{{ trans('common.last_login_time') }} : </span>
                                    <span class="time setFFTime">
                                    {{ $favouriteFreelancer->freelancer->getLastLogin() }}
                                </span>
                                </div>
                            </div>
                        </div>
                        <div class="user-info col-md-3 text-right right-info setFFButton">
                            <div class="right-info-start setFFStar">
                                <a class="btn-remove-fav-freelancer {{ $favouriteFreelancer->freelancer->isFavouriteFreelance()? 'active': '' }}" href="javascript:;" data-url="{{ route('frontend.removefavouritefreelancer.post') }}" data-freelancer-id="{{ $favouriteFreelancer->freelancer->id }}">
                                    <span class="fa fa-star"></span>
                                </a>
                            </div>

                            @if(\Auth::user())
                            <a href="{{ route('frontend.findexperts.invitetowork', ['user_id' => $favouriteFreelancer->freelancer->id, 'invitation_project_id' => 0 ] ) }}"
                                data-toggle="modal"
                                data-target="#modalInviteToWork">
                            @else
                            <a href="{{ route('login')}}" >
                            @endif
                                <button type="button" class="btn btn-success btn-green-bg-white-text setFFBtn">
                                    {{ trans('common.invite_for_work') }}
                                </button>
                            </a>

                            @if(\Auth::user())
                            <a class="ffALink" href="{{ route('frontend.findexperts.sendoffer', ['user_id' => $favouriteFreelancer->freelancer->id ] ) }}"
                                data-toggle="modal"
                                data-target="#modalSendOffer">
                            @else
                            <a href="{{ route('login')}}" >
                            @endif
                                <button type="button" class="btn btn-white-bg-green-text m-t-5">
                                    {{ trans('common.send_offer') }}
                                </button>
                            </a>

                            <br>
                            <button type="button" class="btn btn-white-bg-green-text btn-check-detail setFFBtn3"
                                data-url="{{ route('frontend.resume.getdetails', [\Session::get('lang'),$favouriteFreelancer->freelancer->user_link] ) }}">
                                {{ trans('common.check_detail') }}
                            </button>
                        </div>
                    </div>
                </div>
                @endforeach
            @endif
            {{--<div class="portlet find-expert-box">--}}
                {{--<div class="row personal-profile">--}}
                    {{--<div class="profile-image col-md-3">--}}
                        {{--<div class="profile-userpic p-t-5 text-center">--}}
                            {{--<a href="">--}}
                                {{--<img src="/images/favouriteFreelancers1.png" class="user-img img-responsive img-circle"--}}
                                     {{--alt=""/>--}}
                            {{--</a>--}}
                            {{--<img src="/images/icons/verify.png" class="verify"/>--}}
                        {{--</div>--}}
                        {{--<div class="profile-name p-t-5 text-center">--}}
                            {{--<h3>Adam John</h3>--}}
                            {{--<h4>$15.00 /hr</h4>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="user-info col-md-6">--}}
                        {{--<div class="row basic-info">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<h1 class="find-expert-name">--}}
                                    {{--web developer, web designer--}}
                                {{--</h1>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-12">--}}
                                {{--<p class="description">--}}
                                    {{--I have excellent experience in web designing, web developing. Wordpress. HTML. PHP.--}}
                                    {{--Java Scripts....--}}
                                {{--</p>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-12">--}}
                                {{--<div class="skill-div">--}}
                                    {{--<span class="item-skill official"> <img src="/images/logo_2.png" alt=""--}}
                                                                            {{--height="24px" width="24px"> <span>PHP</span></span>--}}
                                {{--</div>--}}
                                {{--<div class="skill-div">--}}
                                    {{--<span class="item-skill success"> <img src="/images/checked.png" alt=""--}}
                                                                           {{--height="24px"--}}
                                                                           {{--width="24px"> <span>JavaScript</span></span>--}}
                                {{--</div>--}}
                                {{--<div class="skill-div">--}}
                                    {{--<span class="item-skill"><span>HTML5</span></span>--}}
                                {{--</div>--}}
                                {{--<div class="skill-div">--}}
                                    {{--<span class="item-skill"><span>Adobe Photoshop</span></span>--}}
                                {{--</div>--}}
                                {{--<div class="skill-div">--}}
                                    {{--<span class="item-skill"><span>Adobe ilusstrator</span></span>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-md-12 latest-login">--}}
                                {{--<span class="text-capitalize">Last Login Time: </span>--}}
                                {{--<span class="time">--}}
                                {{--18-12-2016 13:20am--}}
                            {{--</span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="user-info col-md-3 text-right right-info">--}}
                        {{--<div class="right-info-start">--}}
                            {{--<span class="fa fa-star"></span>--}}
                        {{--</div>--}}

                        {{--<button type="button" class="btn btn-success-color">Invite for Work</button>--}}
                        {{--<button type="button" class="btn btn-default-color">Send Offer</button>--}}

                        {{--<br><br>--}}
                        {{--<button type="button" class="btn btn-default-color">CHECK DETAIL</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="portlet find-expert-box">--}}
                {{--<div class="row personal-profile">--}}
                    {{--<div class="profile-image col-md-3">--}}
                        {{--<div class="profile-userpic p-t-5 text-center">--}}
                            {{--<a href="">--}}
                                {{--<img src="/images/favouriteFreelancers2.png" class="user-img img-responsive img-circle"--}}
                                     {{--alt=""/>--}}
                            {{--</a>--}}
                            {{--<img src="/images/icons/verify.png" class="verify"/>--}}
                        {{--</div>--}}
                        {{--<div class="profile-name p-t-5 text-center">--}}
                            {{--<h3>Pieter Hees</h3>--}}
                            {{--<h4>$15.00 /hr</h4>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="user-info col-md-6">--}}
                        {{--<div class="row basic-info">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<h1 class="find-expert-name">--}}
                                    {{--web developer, web designer--}}
                                {{--</h1>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-12">--}}
                                {{--<p class="description">--}}
                                    {{--I have excellent experience in web designing, web developing. Wordpress. HTML. PHP.--}}
                                    {{--Java Scripts....--}}
                                {{--</p>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-12">--}}
                                {{--<div class="skill-div">--}}
                                    {{--<span class="item-skill official"> <img src="/images/logo_2.png" alt=""--}}
                                                                            {{--height="24px" width="24px"> <span>PHP</span></span>--}}
                                {{--</div>--}}
                                {{--<div class="skill-div">--}}
                                    {{--<span class="item-skill success"> <img src="/images/checked.png" alt=""--}}
                                                                           {{--height="24px"--}}
                                                                           {{--width="24px"> <span>JavaScript</span></span>--}}
                                {{--</div>--}}
                                {{--<div class="skill-div">--}}
                                    {{--<span class="item-skill"><span>HTML5</span></span>--}}
                                {{--</div>--}}
                                {{--<div class="skill-div">--}}
                                    {{--<span class="item-skill"><span>Adobe Photoshop</span></span>--}}
                                {{--</div>--}}
                                {{--<div class="skill-div">--}}
                                    {{--<span class="item-skill"><span>Adobe ilusstrator</span></span>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-md-12 latest-login">--}}
                                {{--<span class="text-capitalize">Last Login Time: </span>--}}
                                {{--<span class="time">--}}
                                {{--18-12-2016 13:20am--}}
                            {{--</span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="user-info col-md-3 text-right right-info">--}}
                        {{--<div class="right-info-start">--}}
                            {{--<span class="fa fa-star"></span>--}}
                        {{--</div>--}}

                        {{--<button type="button" class="btn btn-success-color">Invite for Work</button>--}}
                        {{--<button type="button" class="btn btn-default-color">Send Offer</button>--}}

                        {{--<br><br>--}}
                        {{--<button type="button" class="btn btn-default-color">CHECK DETAIL</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="portlet find-expert-box">--}}
                {{--<div class="row personal-profile">--}}
                    {{--<div class="profile-image col-md-3">--}}
                        {{--<div class="profile-userpic p-t-5 text-center">--}}
                            {{--<a href="">--}}
                                {{--<img src="/images/favouriteFreelancers3.png" class="user-img img-responsive img-circle"--}}
                                     {{--alt=""/>--}}
                            {{--</a>--}}
                            {{--<img src="/images/icons/verify1.png" class="verify"/>--}}
                        {{--</div>--}}
                        {{--<div class="profile-name p-t-5 text-center">--}}
                            {{--<h3>Cha Rapadas</h3>--}}
                            {{--<h4>$15.00 /hr</h4>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="user-info col-md-6">--}}
                        {{--<div class="row basic-info">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<h1 class="find-expert-name">--}}
                                    {{--web developer, web designer--}}
                                {{--</h1>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-12">--}}
                                {{--<p class="description">--}}
                                    {{--I have excellent experience in web designing, web developing. Wordpress. HTML. PHP.--}}
                                    {{--Java Scripts....--}}
                                {{--</p>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-12">--}}
                                {{--<div class="skill-div">--}}
                                    {{--<span class="item-skill official"> <img src="/images/logo_2.png" alt=""--}}
                                                                            {{--height="24px" width="24px"> <span>PHP</span></span>--}}
                                {{--</div>--}}
                                {{--<div class="skill-div">--}}
                                    {{--<span class="item-skill success"> <img src="/images/checked.png" alt=""--}}
                                                                           {{--height="24px"--}}
                                                                           {{--width="24px"> <span>JavaScript</span></span>--}}
                                {{--</div>--}}
                                {{--<div class="skill-div">--}}
                                    {{--<span class="item-skill"><span>HTML5</span></span>--}}
                                {{--</div>--}}
                                {{--<div class="skill-div">--}}
                                    {{--<span class="item-skill"><span>Adobe Photoshop</span></span>--}}
                                {{--</div>--}}
                                {{--<div class="skill-div">--}}
                                    {{--<span class="item-skill"><span>Adobe ilusstrator</span></span>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-md-12 latest-login">--}}
                                {{--<span class="text-capitalize">Last Login Time: </span>--}}
                                {{--<span class="time">--}}
                                {{--18-12-2016 13:20am--}}
                            {{--</span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="user-info col-md-3 text-right right-info">--}}
                        {{--<div class="right-info-start">--}}
                            {{--<span class="fa fa-star"></span>--}}
                        {{--</div>--}}

                        {{--<button type="button" class="btn btn-success-color">Invite for Work</button>--}}
                        {{--<button type="button" class="btn btn-default-color">Send Offer</button>--}}

                        {{--<br><br>--}}
                        {{--<button type="button" class="btn btn-default-color">CHECK DETAIL</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="portlet find-expert-box">--}}
                {{--<div class="row personal-profile">--}}
                    {{--<div class="profile-image col-md-3">--}}
                        {{--<div class="profile-userpic p-t-5 text-center">--}}
                            {{--<a href="">--}}
                                {{--<img src="/images/favouriteFreelancers4.png" class="user-img img-responsive img-circle"--}}
                                     {{--alt=""/>--}}
                            {{--</a>--}}
                            {{--<img src="/images/icons/verify1.png" class="verify"/>--}}
                        {{--</div>--}}
                        {{--<div class="profile-name p-t-5 text-center">--}}
                            {{--<h3>Jennifer M</h3>--}}
                            {{--<h4>$15.00 /hr</h4>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="user-info col-md-6">--}}
                        {{--<div class="row basic-info">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<h1 class="find-expert-name">--}}
                                    {{--web developer, web designer--}}
                                {{--</h1>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-12">--}}
                                {{--<p class="description">--}}
                                    {{--I have excellent experience in web designing, web developing. Wordpress. HTML. PHP.--}}
                                    {{--Java Scripts....--}}
                                {{--</p>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-12">--}}
                                {{--<div class="skill-div">--}}
                                    {{--<span class="item-skill official"> <img src="/images/logo_2.png" alt=""--}}
                                                                            {{--height="24px" width="24px"> <span>PHP</span></span>--}}
                                {{--</div>--}}
                                {{--<div class="skill-div">--}}
                                    {{--<span class="item-skill success"> <img src="/images/checked.png" alt=""--}}
                                                                           {{--height="24px"--}}
                                                                           {{--width="24px"> <span>JavaScript</span></span>--}}
                                {{--</div>--}}
                                {{--<div class="skill-div">--}}
                                    {{--<span class="item-skill"><span>HTML5</span></span>--}}
                                {{--</div>--}}
                                {{--<div class="skill-div">--}}
                                    {{--<span class="item-skill"><span>Adobe Photoshop</span></span>--}}
                                {{--</div>--}}
                                {{--<div class="skill-div">--}}
                                    {{--<span class="item-skill"><span>Adobe ilusstrator</span></span>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-md-12 latest-login">--}}
                                {{--<span class="text-capitalize">Last Login Time: </span>--}}
                                {{--<span class="time">--}}
                                {{--18-12-2016 13:20am--}}
                            {{--</span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="user-info col-md-3 text-right right-info">--}}
                        {{--<div class="right-info-start">--}}
                            {{--<span class="fa fa-star"></span>--}}
                        {{--</div>--}}

                        {{--<button type="button" class="btn btn-success-color">Invite for Work</button>--}}
                        {{--<button type="button" class="btn btn-default-color">Send Offer</button>--}}

                        {{--<br><br>--}}
                        {{--<button type="button" class="btn btn-default-color">CHECK DETAIL</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

        </div>
    </div>
@endsection

@section('modal')
    <div id="modalInviteToWork" class="modal fade apply-job-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>

    <div id="modalSendOffer" class="modal fade apply-job-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script>
        var lang = {
            unable_to_request : "{{ trans('common.unable_to_request') }}"
        };
    </script>
    <script src="{{asset('js/minifiedFavouriteFreelancer.js')}}"></script>
{{--    <script src="/custom/js/frontend/favouriteFreelancers.js" type="text/javascript"></script>--}}
@endsection
