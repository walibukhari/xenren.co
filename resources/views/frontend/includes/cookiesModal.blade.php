<style>
    #cookiesModal{
        width: 100%;
        justify-content: center;
        align-items: flex-end;
        padding-right: 0px !important;
    }
    #modal-dialogCookiesModal{
        width: 100%;
        margin:0px;
    }
</style>
<div id="cookiesModal" class="modal fade" role="dialog">
    <div class="modal-dialog" id="modal-dialogCookiesModal">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <p>Hi there! Xenren.co uses cookies to ensure the website functions properly. Third parties can also put these cookies in place. In our <a href="" style="text-decoration:underline;color:#555;font-weight: bold;">privacy statement</a> we explain which cookies are used and why, which cookies requires your consent and how can delete cookies. By clicking 'OK' , you agree to the use of cookies.</p>
            </div>
            <div class="modal-footer">
                <button type="button" id="closeOk" class="btn btn-success">OK</button>
                <button type="button" id="cancelP" class="btn btn-default" data-dismiss="modal">No, I decline</button>
            </div>
        </div>

    </div>
</div>
