<!-- Modal -->
<div id="modalPersonalInfo" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content set-modal-content-personal-info-when-post-job">
            <div class="modal-header setPPPMODALPINFO">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="setPPPAGEMPINFOOOIMG">
                    <img src="{{ asset('images/post-project-modal/popup1.png') }}">
                    <h2 class="modal-title">{{ trans('common.do_you_know') }}</h2>
                </div>
                <h4 class="text-center setCOLORPPAGETC">
                    {{ trans('common.you_can_increase_your_exposure_by') }}
                </h4>
            </div>
            <div class="modal-body">
                <table class="table ">
                    <tr class="green hide avatar-g" onclick="window.location.href='/resume'">
                        <td scope="row" >
                            <img src="{{ asset('images/post-project-modal/popup2-G.png') }}" alt="">
                        </td>
                        <td>
                            {{ trans('common.modal_avatar') }}
                        </td>
                        <td>
                            <img src="{{ asset('images/post-project-modal/popup6.png') }}" alt="">
                        </td>
                    </tr>
                    <tr class="red hide avatar-r" onclick="window.location.href='/personalInformation'">
                        <td scope="row">
                            <img src="{{ asset('images/post-project-modal/popup2-R.png') }}" alt="">
                        </td>
                        <td>
                            {{ trans('common.modal_avatar') }}
                        </td>
                        <td>
                            <img src="{{ asset('images/post-project-modal/popup7.png') }}" alt="">
                        </td>
                    </tr>
                    <tr class="green hide verify_account-g" onclick="window.location.href='/personalInformation'">
                        <td scope="row">
                            <img src="{{ asset('images/post-project-modal/popup3-G.png') }}" alt="">
                        </td>
                        <td>
                            {{ trans('common.modal_verify_your_personal_info') }}
                        </td>
                        <td>
                            <img src="{{ asset('images/post-project-modal/popup6.png') }}" alt="">
                        </td>
                    </tr>
                    <tr class="red hide verify_account-r"  onclick="window.location.href='/personalInformation'">
                        <td scope="row">
                            <img src="{{ asset('images/post-project-modal/popup3-R.png') }}" alt="">
                        </td>
                        <td>
                            {{ trans('common.modal_verify_your_personal_info') }}
                        </td>
                        <td>
                            <img src="{{ asset('images/post-project-modal/popup7.png') }}" alt="">
                        </td>
                    </tr>
                    <tr class="green hide resume-g">
                        <td scope="row">
                            <img src="{{ asset('images/post-project-modal/popup2-G.png') }}" alt="">
                        </td>
                        <td>
                            {{ trans('common.modal_resume') }}
                        </td>
                        <td>
                            <img src="{{ asset('images/post-project-modal/popup6.png') }}" alt="">
                        </td>
                    </tr>
                    <tr class="red hide resume-r">
                        <td scope="row">
                            <img src="{{ asset('images/post-project-modal/popup2-R.png') }}" alt="">
                        </td>
                        <td>
                            {{ trans('common.modal_resume') }}
                        </td>
                        <td>
                            <img src="{{ asset('images/post-project-modal/popup7.png') }}" alt="">
                        </td>
                    </tr>
                    <tr class="green hide skill_info-g">
                        <td scope="row">
                            <img src="{{ asset('images/post-project-modal/popup4-G.png') }}" alt="">
                        </td>
                        <td>
                            {{ trans('common.modal_skill_info') }}
                        </td>
                        <td>
                            <img src="{{ asset('images/post-project-modal/popup6.png') }}" alt="">
                        </td>
                    </tr>
                    <tr class="red hide skill_info-r">
                        <td scope="row">
                            <img src="{{ asset('images/post-project-modal/popup4-R.png') }}" alt="">
                        </td>
                        <td>
                            {{ trans('common.modal_skill_info') }}
                        </td>
                        <td>
                            <img src="{{ asset('images/post-project-modal/popup7.png') }}" alt="">
                        </td>
                    </tr>
                    <tr class="green hide clear_budget-g">
                        <td scope="row">
                            <img src="{{ asset('images/post-project-modal/popup5-G.png') }}" alt="">
                        </td>
                        <td>
                            {{ trans('common.modal_clear_budget') }}
                        </td>
                        <td>
                            <img src="{{ asset('images/post-project-modal/popup6.png') }}" alt="">
                        </td>
                    </tr>
                    <tr class="red hide clear_budget-r">
                        <td scope="row">
                            <img src="{{ asset('images/post-project-modal/popup5-R.png') }}" alt="">
                        </td>
                        <td>
                            {{ trans('common.modal_clear_budget') }}
                        </td>
                        <td>
                            <img src="{{ asset('images/post-project-modal/popup7.png') }}" alt="">
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" id="modal_draft_project" class="btn button-submit btn-green-custom setMPERSONALINFBTN modal_draft_project">{{ trans('common.make_a_draft') }}</button>
                <button type="button" class="btn btn-default setMCPINFOPAGESETBTN2" id="modal_post_project" data-dismiss="modal">{{ trans('common.continue_posting') }}</button>

            </div>
        </div>
    </div>
</div>