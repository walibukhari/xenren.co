{{--<!-- Modal -->--}}
{{--<div id="modalInvite" class="modal fade" role="dialog">--}}
    {{--<div class="modal-dialog">--}}
        {{--<!-- Modal content-->--}}
        {{--<div class="modal-content">--}}
            {{--{!! Form::open(['route' => 'frontend.findexperts.invite', 'id' => 'add-user-status-form', 'method' => 'post', 'class' => 'form-horizontal']) !!}--}}
                {{--<div class="modal-header">--}}
                    {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
                    {{--<h4 class="modal-title">--}}
                        {{--Invite--}}
                    {{--</h4>--}}
                {{--</div>--}}
                {{--<div class="modal-body">--}}
                    {{--<div class="form-body">--}}
                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-12" id="add-user-status-errors"></div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                            {{--<label class="col-md-3 control-label">--}}
                                {{--Choose Project--}}
                            {{--</label>--}}
                            {{--<div class="col-md-9">--}}
                                {{--@if( $projects != null )--}}
                                {{--<select id="slcProjectId" class="bs-select form-control" name="projectId">--}}
                                    {{--@foreach( $projects as $key => $project )--}}
                                    {{--<option value="{{ $project->id }}" >{{ $project->name }}</option>--}}
                                    {{--@endforeach--}}
                                {{--</select>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="modal-footer">--}}
                    {{--<button id="btn-submit-invite" type="button" class="btn btn-success m-b-0">--}}
                        {{--{{ trans('common.confirm') }}--}}
                    {{--</button>--}}
                    {{--<button type="button" class="btn btn-default" data-dismiss="modal">--}}
                        {{--{{ trans('member.cancel') }}--}}
                    {{--</button>--}}
                {{--</div>--}}
                {{--{!! Form::hidden('receiverId', '', ['id' => 'receiverId'] ) !!}--}}
            {{--{!! Form::close() !!}--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

@extends('frontend.includes.ajaxmodal')

@section('title')
    {{ trans('common.invite_for_work') }}
@endsection

@section('script')
    <script>
        +function () {
            $(document).ready(function () {
                $('#send-invitation-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btnSubmit',
                    alertContainer : '#model-alert-container',
                    afterSuccessFunction: function (response, $el, $this) {
                        if( response.status == 'OK'){
                            App.alert({
                                type: 'success',
                                icon: 'check',
                                message: response.msg,
                                place: 'append',
                                container: '#model-alert-container',
                                reset: true,
                                focus: true
                            });
                            $('#modalInviteToWork').modal('toggle');
                            $('#lnkInviteForWork-' + response.receiverId).attr('href', 'javascript:;');
                            $('#btnInviteForWork-' + + response.receiverId).removeClass('find-experts-success')
                                .addClass('invite-freelance-gray');
                            alertSuccess(response.msg);
                        }else{
                            App.alert({
                                type: 'danger',
                                icon: 'warning',
                                message: response.msg,
                                container: '#model-alert-container',
                                reset: true,
                                focus: true
                            });
	                        alertError(response.msg);
                        }

                        App.unblockUI('#send-invitation-form');
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <div class="pull-left">
        <button id="btnSubmit" class="btn btn-green-bg-white-text btn-send-invitation">
            {{ trans('common.send_invitation')}}
        </button>
    </div>
@endsection

@section('content')
    {!! Form::open(['route' => 'frontend.findexperts.invitetowork.post', 'method' => 'post', 'role' => 'form', 'id' => 'send-invitation-form']) !!}
    <div class="form-body">
        <div class="form-group">
            <div id="model-alert-container">
            </div>
        </div>

        <div class="col-md-12" style="padding: unset;">
            <div class="col-md-2" style="padding: unset;">
                <img class="img-circle" alt=""
                     src="{{ $user->getAvatar() }}"
                     width="100px" height="100px">
                @if( $user->getLatestIdentityStatus() == \App\Models\UserIdentity::STATUS_APPROVED )
                    <i class="fa fa-check-circle"></i>
                @endif
            </div>
            <input type="hidden" name="receiverId" value="{{ $user->id }}">

            <div class="col-md-10">
                <div class="col-md-6">
                    <div class="full-name raleway">
                        {{ $user->getName() }}
                    </div>
                    <div class="job_position">
                        {{ $user->getJobPosition() }}
                    </div>
                </div>
                <div class="col-md-6">
                    <span class="top-smoll pull-right div-price">
                        <div class="price">
                            <i class="fa fa-usd" aria-hidden="true"></i>
                            {{ $user->hourly_pay }} /{{trans('common.hr')}}
                        </div>
                    </span>
                </div>
            </div>

            <div class="col-md-12">
                <div class="row">
                    <hr>
                </div>
            </div>
        </div>

        <div class="form-group">
            <a href="{{ route('frontend.postproject') }}" target="_blank">
                <span class="create-project">{{ trans('common.create_new_job_and_invite') }}</span>
            </a>
        </div>

        <div class="form-group">
            <label class="bold">{{ trans('common.write_message_to_freelancer') }}</label>
            <textarea name="senderMessage" class="form-control" cols="50" rows="10">{!! trans('common.example_message_to_freelancer', ['username' => \Auth::user()->real_name]) !!}</textarea>
        </div>

        <div class="form-group m-b-20">
            <label class="bold">{{ trans('common.choose_job') }}</label>
            <div class="form-group search-selectbox">
                @if( $projects != null )
                    <select id="slcProjectId" class="bs-select form-control" name="projectId">
                        @foreach ( $projects as $key => $project)
                            <option value="{{ $project->id }}" {{ isset($invitationProjectId) && $invitationProjectId == $project->id? 'selected': '' }} >{{ $project->name }}</option>
                        @endforeach
                    </select>
                @endif
            </div>
        </div>

        <div class="form-group col-md-12" style="padding:unset;">
            <hr>
        </div>
    </div>
    {!! Form::close() !!}
@endsection


