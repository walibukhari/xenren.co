@extends('frontend.includes.ajaxmodal')

@section('title')
    {{trans('common.cropping_image')}}
@endsection

@section('script')
    <script>
        +function () {
            $(document).ready(function () {

                $('#cropping-image-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-crop-image-submit',
                    alertContainer : '#model-alert-container',
                    afterSuccessFunction: function (response, $el, $this) {
                        if( response.status == 'OK'){
                            $('#modalCropImage').modal('toggle');
                            $('#portfolio-img-' + response.id ).attr('src', response.thumbnailPath);
                        }else{
                            App.alert({
                                type: 'danger',
                                icon: 'warning',
                                message: response.message,
                                container: '#model-alert-container',
                                reset: true,
                                focus: true
                            });
                        }

                        App.unblockUI('#cropping-image-form');
                    }
                });

                $('#imgOriginalImage').Jcrop({
                    onSelect:    updateCoords,
                    bgColor:     'black',
                    bgOpacity:   .4,
                    setSelect:   [ 100, 100, 50, 50 ],
                    aspectRatio: 250 / 180,
                    addClass: 'jcrop-centered'
                });
            });

            function updateCoords(c)
            {
                $('#crop_x').val(c.x);
                $('#crop_y').val(c.y);
                $('#crop_x2').val(c.x2);
                $('#crop_y2').val(c.y2);
                $('#crop_w').val(c.w);
                $('#crop_h').val(c.h);
            };
        }(jQuery);
    </script>
@endsection

@section('footer')
    <div style="margin-top:0 px;">
        <hr/>
    </div>
    <div class="text-center">
         {{--<button type="button" class="btn btn-green-bg-white-text" id="btn-crop-image-submit">{{trans('jobs.make_offer')}}</button>--}}
        <button type="button" id="btn-crop-image-submit" class="btn btn-green-bg-white-text" >{{trans('common.cropping_image')}}</button>
    </div>
@endsection

@section('content')
    {!! Form::open(['route' => ['frontend.modal.cropImage.post'], 'method' => 'post', 'role' => 'form', 'id' => 'cropping-image-form']) !!}

        <div class="form-body">
            <div class="form-group">
                <div id="model-alert-container">
                </div>
            </div>


            <div class="form-group">
                <div class="crop-image-wrapper">
                    <img src="{{ $url }}" id="imgOriginalImage" />
                </div>
            </div>
        </div>
        {!! Form::hidden('id', $id) !!}
        {!! Form::hidden('type', $type) !!}

        <input id="crop_x" name="x" type="hidden">
        <input id="crop_y" name="y" type="hidden">
        <input id="crop_x2" name="x2" type="hidden">
        <input id="crop_y2" name="y2" type="hidden">
        <input id="crop_w" name="w" type="hidden">
        <input id="crop_h" name="h" type="hidden">
    {!! Form::close() !!}
@endsection


