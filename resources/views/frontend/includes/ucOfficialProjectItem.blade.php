@if( $officialProject->design_type == \App\Models\OfficialProject::DESIGN_TYPE_BANNER_WITHOUT_ICON_TOGETHER)
<div class="portlet box div-job-main">
    <div class="row header-cantent">
        <div class="col-md-12 header-bg">
            <img src="{{ $officialProject->getBanner() }}" onerror="this.src='/images/MainLogoDefault.png'">
        </div>
        <div class="col-md-12 header-cantent-sub setJPHeaderContent">
            <div class="row">
                <div class="col-md-12">
                    <img src="/images/icons/icon-copy-2.png">
                    <a href="{{ route('frontend.joindiscussion.getproject', [\Session::get('lang'),$officialProject->name, $officialProject->project_id]) }}">
                        <h3 class="job-title">
                            {{ $officialProject->title }}
                        </h3>
                    </a>

                    <i class="fa fa-mail-forward"></i>

                    @if( \Auth::check())

                    

                    <a class="btn-add-fav-job {{ $officialProject->project->isYourFavouriteJob()? 'active': '' }}" href="javascript:;" data-url="{{ $officialProject->project->isYourFavouriteJob()? route('frontend.removefavouritejob.post') : route('frontend.addfavouritejob.post') }}" data-project-id="{{ $officialProject->project->id }}">
                        <i class="fa fa-heart"></i>
                    </a>

                    <a class="btn-add-down-job-{{ $officialProject->id }}" href="javascript:void(0);" data-url="{{ route('frontend.addthumbdownjob.post') }}" data-project-id="{{ $officialProject->id }}" data-project-type="{{ $projectType }}" data-toggle="modal" data-target="#modalThumbdownJob">
                        <i class="fa fa-thumbs-down"></i>
                    </a>
                    @endif
                </div>
            </div>
            <div class="row details">
                <div class="col-md-4">
                    <p>{{ trans('common.project_type') }} : <strong>{{ trans('common.official_project') }}</strong></p>
                </div>
                <div class="col-md-4">
                    <p>{{ trans('common.time_frame') }} :
                        <strong>
                            {{ trans_choice('common.how_many_days', $officialProject->getTimeFrame(),['howMany' => $officialProject->getTimeFrame()] ) }}
                        </strong>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>{{ trans('common.funding_stage') }} :
                        <strong>
                            {{ \App\Constants::translateFundingStage($officialProject->funding_stage) }}
                        </strong>
                    </p>
                </div>
                @if( $officialProject->budget_switch == 1)
                <div class="col-md-4">
                    <p>
                        {{ trans('common.budget') }} :
                        <strong>
                            {{--${{ $officialProject->budget_from }}--}}
                            {{--- ${{ $officialProject->budget_to }}--}}
                            {{  $officialProject->getBudgetRange() }}
                        </strong>
                    </p>
                </div>
                @endif
                <div class="col-md-4">
                    <p>{{ trans('common.end_date') }} : <strong>{{ $officialProject->getRecruitEndDate() }}</strong></p>
                </div>
                @if( isset($officialProject->city) )
                <div class="col-md-4">
                    <p>{{ trans('common.location') }} :
                        <a href="{{ route('frontend.map.officialprojectlocation', [ 'official_project_id' => $officialProject->id ]) }}">
                            <strong>
                                {{ $officialProject->city }}
                            </strong>
                        </a>
                    </p>
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="row body-cantent">
        <div class="col-md-2 left text-center">
            <img src="{{ $officialProject->getIcon() }}" onerror="this.src='/images/MainLogoDefault.png'">

            <a href="{{ route('frontend.joindiscussion.getproject', [\Session::get('lang'), $officialProject->getSlugByTitle(), $officialProject->project_id]) }}">
                <button class="btn btn-make-Offer setJPBTNJD">
                    {{ trans('common.join_discuss') }}
                </button>
            </a>
        </div>
        <div class="col-md-10 right setJPCol10">
            <p class="desc">
                {{ $officialProject->description }}
            </p>

            <div class="row">
                <div class="col-md-3 budget">
                    <span class="share-title alignment">{{ trans('common.total_share_value') }}</span>
                    <h3 class="alignment">{{ $officialProject->getStockTotal() }}</h3>
                    <span class="share alignment">{{ trans('common.number_share_issued') }}: {{ $officialProject->stock_share }}%</span>
                </div>
                <div class="col-md-2 defficulty setJPSSCol4">
                    <img src="/images/signal.png">
                    <span class="top-smoll">{{ trans('common.difficulty') }}:</span>
                    <div class="sub-con">
                        @for ($i = 0; $i < $officialProject->project_level; $i++)
                        <i class="fa fa-star"></i>
                        @endfor

                        @for ($i = 0; $i < ( 6 - $officialProject->project_level ); $i++)
                        <i class="fa fa-star-o"></i>
                        @endfor
                    </div>
                </div>
                <div class="col-md-3 possition setJPSSCol4">
                    <img src="/images/user-secret.png">
                    <span class="top-smoll">{{ trans('common.position_needed') }}:</span>
                    <div class="sub-con">
                        <span>
                            {{--Graphic Designer, UI Designer--}}
                            @foreach( $officialProject->projectPositions as $key => $projectPosition)
                                @if( $key != 0 )
                                {{ ", " }}
                                @endif

                                {{  $projectPosition->jobPosition->getName() }}
                            @endforeach
                        </span>
                    </div>
                </div>
                <div class="col-md-4 skill setJPSSCol4">
                    <img src="/images/hands.png">
                    <span class="top-smoll">{{ trans('common.skill_request') }}:</span>

                    <div class="skill-div setJPSkillDiv">
                        @foreach( $officialProject->project->projectSkills as $projectSkill)
                        <span class="item-skill {{ $projectSkill->skill->tag_color == \App\Models\Skill::TAG_COLOR_GOLD? 'yellow': 'white' }}">
                            <span>
                                {{ $projectSkill->getSkillName( $projectSkill->skill_id ) }}
                            </span>
                        </span>
                        @endforeach
                        {{--<span class="item-skill"><span>Adobe Ilusstrator</span></span>--}}
                        {{--<span class="item-skill"><span>Photoshop</span></span>--}}
                        {{--<span class="item-skill"><span>CSS</span></span>--}}
                        {{--<span class="item-skill"><span>HTML</span></span>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@else
<div class="portlet box div-job-main div-job-mains">
    <div class="row header-cantent">
        <div class="col-md-12 header-bg">
            <img src="{{ $officialProject->getBanner() }}">
        </div>
        <div class="col-md-2 header-img text-center setHeaderImage">
            <img src="{{ $officialProject->getIcon() }}">
        </div>
        <div class="col-md-10 header-cantent-sub setHeaderContent">
            <div class="row">
                <div class="col-md-12 setJBDPCol12">
                    <img src="/images/icons/icon-copy-2.png">
                    <h3 class="job-title">{{ $officialProject->title }}</h3>

                    <i class="fa fa-mail-forward"></i>

                    @if( \Auth::check())
                    <a class="btn-add-fav-job {{ $officialProject->project->isYourFavouriteJob()? 'active': '' }}" href="javascript:;" data-url="{{ $officialProject->project->isYourFavouriteJob()? route('frontend.removefavouritejob.post') : route('frontend.addfavouritejob.post') }}" data-project-id="{{ $officialProject->project->id }}">
                        <i class="fa fa-heart"></i>
                    </a>

                     <a class="btn-add-down-job-{{ $officialProject->id }}" href="javascript:;" data-url="{{ route('frontend.addthumbdownjob.post') }}" data-project-id="{{ $officialProject->id }}" data-project-type="{{ $projectType }}" data-toggle="modal" data-target="#modalThumbdownJob">
                        <i class="fa fa-thumbs-down"></i>
                    </a>
                    @endif
                </div>
            </div>
            <div class="row details">
                <div class="col-md-4">
                    <p>{{ trans('common.project_type') }} : <strong>{{ trans('common.official_project') }}</strong></p>
                </div>
                <div class="col-md-4">
                    <p>{{ trans('common.time_frame') }} :
                        <strong>
                            {{ trans_choice('common.how_many_days', $officialProject->getTimeFrame(), ['howMany' => $officialProject->getTimeFrame()] ) }}
                        </strong>
                    </p>
                </div>
                <div class="col-md-4">
                    <p>{{ trans('common.funding_stage') }} : <strong>{{ \App\Constants::translateFundingStage($officialProject->funding_stage) }}</strong></p>
                </div>
                @if( $officialProject->budget_switch == 1)
                <div class="col-md-4">
                    <p>{{ trans('common.budget') }} :
                        <strong>
{{--                                            ${{ $officialProject->budget_from }} - ${{ $officialProject->budget_to }}--}}
                            {{  $officialProject->getBudgetRange() }}
                        </strong>
                    </p>
                </div>
                @endif
                <div class="col-md-4">
                    <p>{{ trans('common.end_date') }} : <strong>{{ $officialProject->getRecruitEndDate() }}</strong></p>
                </div>
                <div class="col-md-4">
                    <p>{{ trans('common.location') }} :
                        <a href="{{ route('frontend.map.officialprojectlocation', [ 'official_project_id' => $officialProject->id ]) }}">
                            <strong>
                                {{ $officialProject->city }}
                            </strong>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row body-cantent setRowBodyContent">
        <div class="col-md-2 left">
            <div class="budget">
                    <span class="share-title">{{ trans('common.total_share_value') }}</span>
                    <h3>{{ $officialProject->getStockTotal() }}</h3>
                    <span class="share">{{ trans('common.number_share_issued') }}: {{ $officialProject->stock_share }}%</span>
            </div>
            <a href="{{ route('frontend.joindiscussion.getproject', [\Session::get('lang'),$officialProject->getSlugByTitle(), $officialProject->project_id]) }}">
                <button class="btn btn-make-Offer">
                    {{ trans('common.join_discuss') }}
                </button>
            </a>
        </div>
        <div class="col-md-10 right">
            <p class="desc">
                {{ $officialProject->description }}
            </p>

            <div class="row setColJOFFICALP">
                <div class="col-md-3 defficulty">
                    <img src="/images/signal.png">
                    <span class="top-smoll">{{ trans('common.difficulty') }}:</span>
                    <div class="sub-con setSubIcon">
                        @for ($i = 0; $i < $officialProject->project_level; $i++)
                        <i class="fa fa-star"></i>
                        @endfor

                        @for ($i = 0; $i < ( 6 - $officialProject->project_level ); $i++)
                                <i class="far fa-star"></i>
                        @endfor
                        {{--<i class="fa fa-star"></i>--}}
                        {{--<i class="fa fa-star"></i>--}}
                        {{--<i class="fa fa-star"></i>--}}
                        {{--<i class="fa fa-star"></i>--}}
                        {{--<i class="fa fa-star-o"></i>--}}
                    </div>
                </div>
                <div class="col-md-4 possition">
                    <img src="/images/user-secret.png">
                    <span class="top-smoll">{{ trans('common.position_needed') }}:</span>
                    <div class="sub-con setSubIcon">
                        <span>
                            @foreach( $officialProject->projectPositions as $key => $projectPosition)
                                @if( $key != 0 )
                                {{ ", " }}
                                @endif

                                {{  $projectPosition->jobPosition->getName() }}
                            @endforeach
                            {{--Graphic Designer, UI Designer--}}
                        </span>
                    </div>
                </div>
                <div class="col-md-5 skill">
                    <img src="/images/hands.png" class="setSkillImage">
                    <span class="top-smoll">{{ trans('common.skill_request') }}:</span>

                    <div class="skill-div setSkillDivScroll">
                        @foreach( $officialProject->project->projectSkills as $projectSkill)
                        <span class="item-skill setItemSkill {{ $projectSkill->skill->tag_color == \App\Models\Skill::TAG_COLOR_GOLD? 'yellow': 'white' }}">
                            <span>
                                {{ $projectSkill->getSkillName( $projectSkill->skill_id ) }}
                            </span>
                        </span>
                        @endforeach
                        {{--<span class="item-skill"><span>Photoshop</span></span>--}}
                        {{--<span class="item-skill"><span>CSS</span></span>--}}
                        {{--<span class="item-skill"><span>HTML</span></span>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif