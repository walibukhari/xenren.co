<div id="modalSetContactInfo" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content setModalDailogeCI">
        <div class="modal-header">
            <button type="button" class="btn pull-right" data-dismiss="modal">
                <span class="setspantimesclose">×</span>
            </button>
            <h4 class="modal-title">    Edit Contact Info
            </h4>
        </div>
        <div class="modal-body">
            @if($user != '')
            {!! Form::open(['route' => ['frontend.modal.setcontactinfo.post'], 'method' => 'post', 'role' => 'form', 'id' => 'set-contact-info-form']) !!}

            <div class="form-body">
                <div class="form-group">
                    <div id="model-alert-container">
                    </div>
                </div>
                @if($user->email == null || $user->email == "" )
                    <div class="form-group">
                        <div class="action pull-left">
                            <span style="color: red">*</span> <span>{{ trans('common.must_fill') }}</span>
                        </div>
                        <br/>
                    </div>

                    <div class="form-group content-group">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group">
                            <span class="input-group-addon">
                                <img src="/images/email.png" style="width: 20px;">
                            </span>
                                    {!! Form::text('email', $user->email, ['class' => 'form-control text-center', 'id' => 'tbxEmail', 'placeholder' => trans('common.email')]) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                        </div>
                    </div>
                @endif

                <div class="form-group">
                    <div class="row action pull-left setSEtCIFG">
                        <span style="color: red">*</span> <span>{{ trans('common.at_least_one_contact_info') }}</span>
                        <br>
                        <span style="color: red">*</span> <span>{{trans('common.during_off_mode')}}</span>
                    </div>
                </div>

                <div class="form-group content-group">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group setInputGroupMSCI">
                            <span class="input-group-addon">
                                <img src="/images/skype-icon-5.png" style="width: 20px;">
                            </span>
                                {!! Form::text('skype_id', $user->skype_id, ['class' => 'form-control text-center', 'id' => 'tbxSkypeId', 'placeholder' => 'Skype']) !!}
                                <div class="switch2">
                                    <div class="toggle-btn-2" id="skype" onclick="myskype()">
                                        <div class="inner-circle-2"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group setInputGroupMSCI">
                            <span class="input-group-addon">
                                <img src="/images/wechat-logo.png" style="width: 20px;">
                            </span>
                                {!! Form::text('wechat_id', $user->wechat_id, ['class' => 'form-control text-center', 'id' => 'tbxWechatId', 'placeholder' => trans('common.wechat')]) !!}

                                <div class="switch3">
                                    <div class="toggle-btn-3" id="wechat" onclick="mywechat()">
                                        <div class="inner-circle-3"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        {{--<div class="col-md-6">--}}
                        {{--<div class="input-group setInputGroupMSCI">--}}
                        {{--<span class="input-group-addon">--}}
                        {{--<img src="/images/Tencent_QQ.png">--}}
                        {{--</span>--}}
                        {{--{!! Form::text('qq_id', $user->qq_id, ['class' => 'form-control text-center', 'id' => 'tbxQQId', 'placeholder' => 'QQ' ]) !!}--}}
                        {{--<div class="switch1">--}}
                        {{--<div class="toggle-btn" id="qq" onclick="myqq()">--}}
                        {{--<div class="inner-circle"></div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        <div class="col-md-6">
                            <div class="input-group setInputGroupMSCI">
                            <span class="input-group-addon">
                                <img src="/images/line.png" style="width: 20px;">
                            </span>
                                {!! Form::text('line_id', $user->line_id, ['class' => 'form-control text-center', 'id' => 'tbxlineId', 'placeholder' => trans('common.line') ]) !!}
                                <div class="switch1">
                                    <div class="toggle-btn" id="line" onclick="myline()">
                                        <div class="inner-circle"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group setInputGroupMSCI">
                            <span class="input-group-addon">
                                <img src="/images/MetroUI_Phone.png" style="width: 20px;">
                            </span>
                                {!! Form::text('handphone_no', $user->handphone_no, ['class' => 'form-control text-center', 'id' => 'tbxHandphoneNo', 'placeholder' => trans('common.phone')]) !!}

                                <div class="switch4">
                                    <div class="toggle-btn-4" id="phoneii" onclick="myphoneii()">
                                        <div class="inner-circle-4"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{--<div class="form-group">--}}
                {{--<div class="action pull-left">--}}
                {{--<input type="checkbox"--}}
                {{--class="make-switch form-control"--}}
                {{--name="ckbIsContactAllowView"--}}
                {{--id="ckbIsContactAllowView"--}}
                {{--data-size="switch-size"--}}
                {{--data-on-text="{{ trans('common.show') }}"--}}
                {{--data-off-text="{{ trans('common.hide') }}"--}}
                {{--data-on-color="success" data-off-color="danger"--}}
                {{--{{ isset($user->is_contact_allow_view) && $user->is_contact_allow_view == 1?'checked' : '' }} >--}}
                {{--<input type="hidden" id="is_contact_allow_view" name="is_contact_allow_view" value="{{ isset($user->is_contact_allow_view)? $user->is_contact_allow_view: '' }}">--}}
                {{--</div>--}}
                {{--</div>--}}

                <div class="clearfix"></div>

                <div class="form-group">
                    <div class="m-b-20"></div>
                </div>

                <div class="clearfix"></div>
            </div>
            {!! Form::hidden('user_id', $user->id) !!}
            {!! Form::close() !!}
            @endif
        </div>
        </div>
    </div>
</div>
