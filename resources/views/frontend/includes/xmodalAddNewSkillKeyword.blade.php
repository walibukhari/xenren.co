<!-- Modal -->
<div id="modelAddNewSkillKeyword" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            {!! Form::open(['route' => 'frontend.postproject.addNewSkillKeyword', 'id' => 'add-new-skill-keyword-form', 'method' => 'post', 'class' => 'form-horizontal']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        {{ trans('common.add_new_skill_keyword') }}
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">
                        <div class="col-md-12" id="add-new-skill-keyword-errors"></div>

                        <div class="row">
                            <label class="col-md-3 control-label">
                                {{ trans('common.new_skill_keywords') }}
                            </label>
                            <div class="col-md-8" style="padding-bottom: 40px;">
                                {!! form::text('tbxNewSkillKeywords', '', [
                                    'class' => 'form-control',
                                    'placeholder' => trans('common.use_comma_to_seperate_the_skills'),
                                    'id' => 'tbxNewSkillKeywords'
                                ] ) !!}
                                <br/>
                                {!! form::button('OK', ['class' => 'btn btn-success m-b-0', 'id' => 'btnInputNewSkillKeywordOk' ]) !!}
                            </div>
                        </div>

                        <div class="row div-keyword" style="display:none">

                            <label class="col-md-3 control-label">{{ trans('common.keywords') }}</label>
                            <label class="col-md-9 text-left p-t-7">{{ trans('common.please_use_wiki_or_baidu_support_your_keyword') }}</label>


                            <div class="keyword-section">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btn-submit-new-skill-keyword" type="button" class="btn btn-success m-b-0" data-submit-url="{{ route('frontend.postproject.addNewSkillKeyword') }}">
                        {{ trans('common.confirm') }}
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        {{ trans('member.cancel') }}
                    </button>
                </div>
                {!! form::hidden('locale', App::getLocale() ) !!}
                {!! form::hidden('tag_color', $tagColor ) !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>

