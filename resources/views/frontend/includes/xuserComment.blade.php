<div class="user-comment portlet box gray-box">
    <div class="portlet-title">
        <div class="col-md-3">
            <table class="user-rate">
                <tr>
                    <td>{{ trans('member.commentor_name') }}</td>
                </tr>
            </table>
        </div>
        <div class="col-md-3 p-t-10">
            2016-10-10 14:30
        </div>
        <div class="order-id col-md-4">
            Project ID : NGC201354251456
        </div>
        <div class="post-datetime col-md-2">
            <input type="text" class="rating-loading user-rated-val" value="2.5" data-size="12" title="" data-show-clear="false" data-readonly="true">
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-2">
                <div class="profile-image">
                    <div class="profile-userpic">
                        <img src="/assets/pages/media/profile/profile_user.jpg" class="img-responsive" alt="">
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <p>
                    一条简短的评价，一条简短的评价，一条简短的评价，一条简短的评价，一条简短的评价，一条简短的评价，一条简短的评价 不超过100个字, 不超过100个字, 不超过100个字
                </p>
                <div class="pull-left">
                   <ul class="commenter p-l-0 p-t-10">
                        <li class="item-skill">PHP</li>
                        <li class="item-skill">MSSQL</li>
                        <li class="item-skill">EXCEL</li>
                   </ul>
                </div>
                <div class="pull-right">
                    <button type="submit" class="btn btn-success">
                        Rate
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>