<div class="row project-conversation" data-url="{{ route('frontend.projectchat.pullprojectchat',$projectChatRoomId)}}" data-urlfile="{{ route('frontend.projectchat.pullprojectchatfile',$projectId)}}">
    <div class="col-md-12 clearfix">
        <div class="portlet box gray-color">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <div class="scroller setJDPPCHATMSGS" id="content-chat" data-always-visible="1" data-rail-visible="0">
                    {{--<center><h3>{{ trans('common.chat_content') }} ...</h3></center>--}}

                    <div class="ajaxloader preloader_bg cls setAJAXLOADER">
                        <svg version="1" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink"
                             width="150px" height="150px" viewBox="0 0 28 28">
                            <g class="qp-circular-loader">
                                <path class="qp-circular-loader-path" fill="none"
                                      d="M 14,1.5 A 12.5,12.5 0 1 1 1.5,14" stroke-width="3"
                                      stroke-linecap="round"></path>
                            </g>
                        </svg>
                    </div>

                </div>
                <div class="chat-form chat-form-custom">
                    <div class="error-msg setJDPPCHATMSGEM"></div>
                    <form action="{{ route('frontend.projectchat.postprojectchat') }}" method="post" class="">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <input type="hidden" name="projectChatRoomId" id="projectChatRoomId" value="{{$projectChatRoomId}}">
                        
                        <div class="row chat-form-main">
                            <div class="col-md-1 chat-form-l">
                                {{--<!-- <div class="upload-file upload-btn-custom">--}}
                                    {{--<a href="javascript:;" id="chat-message-file" data-url='{{ route('frontend.projectchat.postprojectchatimage') }}'>--}}
                                        {{--<i class="fa fa-folder-open"></i>--}}
                                    {{--</a>--}}
                                {{--</div> -->--}}
                                <div class="upload-picture upload-btn-custom">
                                    <a href="javascript:;" id="chat-message-img" data-url='{{ route('frontend.projectchat.postprojectchatimage') }}'>
                                        <i class="fa fa-paperclip" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <div class="upload-btn-custom">
                                    <div class="dropup">
                                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
                                            <i class="fa fa-cog" aria-hidden="true"></i>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a>{{trans('common.block_user')}}<input type="checkbox" name="checkbox1" class="checkbox1 make-switch" checked="checked" data-size="small" /></a></li>
                                            <li><a>{{trans('common.user_mail_notification')}} <input type="checkbox" name="checkbox2" class="checkbox2 make-switch" checked="checked" data-size="small" /></a></li>
                                            <li><a>{{trans('common.user_wechat_notification')}}<input type="checkbox" name="checkbox3" class="checkbox3 make-switch" checked="checked" data-size="small" /></a></li>
                                            <li><a href="#">{{trans('common.report_user_abuse')}}</a></li>
                                            <li><a href="#">{{trans('common.Comment')}}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-11 chat-form-r">
                                <div class="input-cont">
                                    <input autocomplete="off" class="form-control message" name="message" id="message" type="text" placeholder="{{ trans('member.leave_message') }}..." />
                                    <button type="submit" class="btn btn-submit sendMessage">
                                        <i class="fa fa-send" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        
                    </form>
                </div>
                <!-- END FORM-->
            </div>
        </div>
    </div>
</div>
<!-- <button type="button" class="btn blue btn-helpdesk" id="invite-helpdesk" data-url="{{ route('frontend.projectchat.postinvitehelpdeskchat',$projectChatRoomId) }}">{{ trans('member.contact_helpdesk') }}</button> -->
@push('js')
<script>
    $('#message').keydown(function(e) {
        if(e.which == 38) {
            console.log('up key');
            console.log('{{$projectChatRoomId}}');
            var settings = {
                "url": "/getLastMessage?id={{$projectChatRoomId}}",
                "method": "POST",
                dataType: 'json',
                "content-type": "multipart/formdata",
                cache : false,
                processData: false
            };
            $.ajax(settings).done(function (response) {
                console.log(response);
                if(response.status == 'success') {
                    $('#message').val(response.message);
                    $('.sendMessage').attr('data-edit', true);
                    $('.sendMessage').attr('data-id', response.id);
                    $('.sendMessage').attr('data-chatroomid', response.chatroomid);
                } else {
                    $('.sendMessage').attr('data-edit', false)
                }
            });
            e.preventDefault(); // prevent the default action (scroll / move caret)
        }
        if($('#message').val() == '') {
            $('.sendMessage').attr('data-edit', false)
        }
    });
</script>
@endpush
<!-- Modal -->
<div id="modalAddFileChat" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            {!! Form::open(['route' => 'frontend.projectchat.postprojectchatfile', 'id' => 'add-project-file-chat', 'method' => 'post', 'class' => 'form-horizontal', 'files' => true]) !!}
                <input type="hidden" name="projectChatRoomId" value="{{$projectChatRoomId}}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{ trans('member.add_file') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">
                        <div class="modal-alert-container"></div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">{{ trans('member.filename') }}</label>
                            <div class="col-md-9">
                                {!! Form::text('message', '', ['class' => 'form-control', 'placeholder' => 'Message']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">{{ trans('member.file') }}</label>
                            <div class="col-md-9">
                                {!! Form::file('file', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn blue" id="btn-submit-file">{{ trans('member.confirm_add') }}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('member.cancel') }}</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>