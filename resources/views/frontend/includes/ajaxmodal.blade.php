<div class="modal-header">
    <button type="button" class="btn pull-right" data-dismiss="modal">
       <span class="setspantimesclose">&times;</span>
    </button>
    <h4 class="modal-title">@section('title')@show</h4>
</div>
<div class="modal-body">
    <div class="modal-alert-container"></div>
    @section('content')@show
</div>
<div class="modal-footer setfooterforContactinfoPopup">
    @section('footer')@show
</div>
@section('script')@show