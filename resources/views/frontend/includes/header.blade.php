<input type="hidden" id="session" value="{{\Session::get('lang')}}">
@if(Route::currentRouteName() == 'login')
<div class="page-header navbar navbar-fixed-top settopHeds" style="min-height: 0 !important;">
@else
<div class="page-header navbar navbar-fixed-top settopHeds">
@endif
    <!-- BEGIN HEADER INNER -->
        @php
            $lang = \Session::get('lang');
        @endphp

    <div class="page-header-inner set-page-header-inner">
    <!-- @if(isset($_G['route_name']) && ($_G['route_name'] == 'home' || $_G['route_name'] == 'register' || $_G['route_name'] == 'login' ))
        <div style="height:5px; background-color:#ffffff"></div>
        @else
        <div style="height:5px; background-color:#008fd5"></div>
        @endif -->
        <div class="container-fluid set-container">
            <div class="row page-header-row setRow">
                <div class="col-md-1 pull-left">
                    <div class="logo setLOGO">
                        <a href="https://www.xenren.co">
                            <img src="{{asset('images/homelogo.jpg')}}" style="width: 45px">
                        </a>
                    </div>
                </div>
                <div class="col-md-3 setCol3H">
                    @if(Route::currentRouteName() != 'login')
                    <div class="searchBar">
                        <div id="msHeaderSearchBar"></div>
                        <i class="fa fa-search btn-search header-search-bar" style="display: none" id="s1" data-base-url="{{ route('frontend.findexperts', \Session::get('lang')) }}"></i>
                        <i class="fafaSearch fa fa-search" style="display: block" id="s2"></i>
                    </div>
                    @endif
                </div>
                <div class="col-md-6 setColMD6H">
                    <div class="setCol6Inner">
                        <ul class="settoplinks">
                        <li class="setHOme">
                            <a href="{{ url($lang) }}" class="{{ isset($_G['route_name']) && in_array($_G['route_name'], ['loggedin.home', 'home']) ? ' active' : '' }}">
                                {{ trans('common.home')}}
                            </a>
                        </li>
                        <li class="dropdown dropdown-user dropdown-dark setJ0BSLink">
                            <a id="main-menu-jobs" href="{{url($lang.'/remote-jobs-online') }}" data-href="{{ route('frontend.jobs.index') }}" class="jobsHide {{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.jobs.index']) ? ' active' : '' }} {{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.joindiscussion.getproject']) ? ' active' : '' }} {{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.instruction.hiring']) ? ' active' : '' }}"
                               data-toggle="dropdown" data-hover="dropdown" data-close-others="true" >
                                {{ trans('common.jobs')}} <i id="ti-angel" class="ti-angle-down setTIAN"></i>
                            </a>
                            {{--data-href="Javascript:void(0)"--}}
                            <ul class="dropdown-menu setFJJOBPAGE">
                                <li class="submenu dropdown setLILI">
                                    <a href="{{ url($lang.'/remote-jobs-online') }}">
                                        {{ trans('common.find_job') }}
                                    </a>
                                </li>
                                <li class="submenu dropdown setLILI">
                                    <a href="{{ url($lang.'/company-official-projects') }}?project_type=2">
                                        {{ trans('common.official_project') }}
                                    </a>
                                </li>
                                <li class="submenu dropdown setLILI">
                                    <a href="{{ url($lang.'/skillVerification') }}">
                                        {{ trans('common.skill_verification') }}
                                    </a>
                                </li>
                                <li class="submenu dropdown setLILI">
                                    <a href="{{ url($lang.'/favouriteJobs') }}" class="setFVJ">
                                        {{ trans('common.favourite_jobs') }}
                                    </a>
                                </li>
                                {{--<li class="submenu dropdown setLILI">--}}
                                    {{--<a href="{{ route('frontend.mypublishorder') }}">--}}
                                        {{--{{ trans('common.isued_job') }}--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li class="submenu dropdown setLILI">--}}
                                    {{--<a href="{{ route('frontend.myreceiveorder') }}">--}}
                                        {{--{{ trans('common.recieved_job') }}--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            </ul>
                        </li>



                        <li class="dropdown dropdown-user dropdown-dark">
                            <a id="main-menu-find-experts" href="{{ url($lang.'/Coworkers') }}" data-href="{{ route('frontend.findexperts') }}" class="FindExpertHide {{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.findexperts']) ? ' active' : '' }} {{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.resume.getdetails']) ? ' active' : '' }}"
                               data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                {{ trans('common.find_experts')}}  <i id="ti-angel" class="ti-angle-down setTIAN"></i>
                            </a>
                            {{--data-href="Javascript:;"--}}
                            <ul class="dropdown-menu find-expert setFINDEXPPAGELI">
                                <li class="setLILI">
                                    <a href="{{ url($lang.'/Coworkers') }}">
                                        <span>
                                        {{ trans('common.find_experts') }}
                                            {{--{{trans('common.find')}} {{trans('common.expert')}}--}}
                                        </span>
                                    </a>
                                </li>
                                <li class="setLILI">
                                    <a class="setFFL" href="{{ url($lang.'/favouriteFreelancers') }}">
                                        <span>
                                        {{ trans('common.favourite_freelancers') }}
                                            {{--{{trans('common.favorite')}} {{trans('common.freelancerrr')}}--}}
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="dropdown dropdown-user dropdown-dark setPOSTPMENU">
                            <a  class="{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.postproject']) ? ' active' : '' }}" href="{{ url($lang.'/postProject') }}" data-href="{{ route('frontend.postproject') }}" data-hover="dropdown">
                                {{ trans('common.post_project')}} <i id="ti-angel" class="ti-angle-down setTIAN"></i>
                            </a>
                            {{--data-href="Javascript:;"--}}
                            <ul class="dropdown-menu setHeadDDMENU">
                                <li class="setLILI">
                                    <a href="{{ url($lang.'/postProject') }}" class="postProject-pad">
                                        <img src="{{ asset('images/p1.jpg') }}"/>
                                        <span>
                                        {{ trans('common.post_project') }}
                                        </span>
                                    </a>
                                </li>
                                <li class="setLILI">
                                    <a href="{{ url($lang.'/myPublishOrder') }}" class="awardedProjects">
                                        <img src="{{ asset('images/p2.jpg') }}"/>
                                        <span>
                                        {{ trans('common.award_project') }}
                                        </span>
                                        @if(\Auth::user())
                                            <i class="glyphicon glyphicon-bell newBellIcon1" style="font-size: 16px !important;"></i>
                                            <span class="badge badge-info notificationBellBubble" id="unread-msg-counter">
                                            @if(\Auth::user())
                                                @if ($_G['user']->getMyPublishOrderCount() != null && $_G['user']->getMyPublishOrderCount() != 0)
                                                    @php
                                                        $publishPrderCount = $_G['user']->getMyPublishOrderCount();
                                                        $publishPrderCount = $publishPrderCount > 0 ? $publishPrderCount : 0;
                                                    @endphp
                                                    {{$publishPrderCount}}
                                                @endif
                                            @else
                                                0
                                            @endif
                                         </span>
                                        @endif
                                    </a>
                                </li>
                                <li class="setLILI">
                                    <a href="{{ url($lang.'/myReceiveOrder') }}" class="receivedProjects setMRO">
                                        <img src="{{ asset('images/p3.jpg') }}"/>
                                        <span>
                                            {{ trans('common.received_project') }}
                                        </span>
                                        @if(\Auth::user())
                                            <i class="glyphicon glyphicon-bell newBellIcon2" style="font-size: 16px !important;"></i>
                                            <span class="badge badge-info notificationBellBubble" id="unread-msg-counter">
                                            @if(\Auth::user())
                                                @if ($_G['user']->getMyReceivedOrderCount() != null && $_G['user']->getMyReceivedOrderCount() != 0)
                                                    @php
                                                        $myReceivedOrderCount = $_G['user']->getMyReceivedOrderCount();
                                                        $yReceivedOrderCount = $myReceivedOrderCount > 0 ? $myReceivedOrderCount : 0;
                                                    @endphp
                                                    {{$yReceivedOrderCount}}
                                                @endif
                                            @else
                                                0
                                            @endif
                                        </span>
                                        @endif
                                    </a>
                                </li>
                            </ul>
                        </li>

                            <li class="dropdown dropdown-user dropdown-dark"><a href="{{route('frontend.Blog',\Session::get('lang'))}}">{{trans('common.blog')}}</a></li>

                    </ul>
                    </div>
                    <!-- end col-md-6 -->
                    <div class="menu-toggler sidebar-toggler hide">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <div class="setDivrighttopMenu">
                    <!-- Authentication Links -->
                        @if (Auth::guard('users')->guest())
                    <!-- Right Side Of Navbar -->
                    <div class="beforeLogin">
                        <ul class="nav navbar-nav navbar-right hidden-xs hidden-sm hidden-md setULH">
                            <li>
                                <a href="{{ route('register') }}" class="setLinkbtn2{{ isset($_G['route_name']) && $_G['route_name'] == 'register' ? ' active' : '' }}">
                                    <button class="btn btn-block setBNTN2">{{ trans('member.register') }}</button>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('login') }}" class="setLinkbtn1 {{ isset($_G['route_name']) && $_G['route_name'] == 'login' ? ' active' : '' }}">
                                    <button class="btn btn-block setBNTN1">{{ trans('member.login') }}</button>
                                </a>
                            </li>
                            <li class="dropdown classic-dropdown setLANGUAGE">
                                <a href="javascript:void(0);" class="dropdown-toggle setLNGDDT" data-toggle="dropdown">
                                    <img class="setLFLG" src="{{ trans('common.language_flg')}}"> <i id="ti-angel" class="ti-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu setH2DDM">
                                    <li class="submenu dropdown">
                                        <a class="setH2DDMLI" href="{{ route('setlang', ['lang' => 'en', 'redirect' => url('')]) }}">
                                            <img class="setEngImage" src="{{ trans('common.language_flg_en')}}">
                                            {{ trans('common.lang_en') }}
                                        </a>
                                    </li>
                                    <li class="submenu dropdown">
                                        <a class="setH2DDMLI" href="{{ route('setlang', ['lang' => 'cn', 'redirect' => url('')]) }}">
                                            <img class="setchinesImage" src="{{ trans('common.language_flg_cn')}}">
                                            {{ trans('common.lang_cn') }}
                                        </a>
                                        {{--                                    <a href="http://xenren.co">{{ trans('common.lang_cn') }}</a>--}}
                                    </li>
                                </ul>
                                <!-- /dropdown-menu -->
                            </li>
                        </ul>
                    </div>
                        @else
                    <!-- BEGIN TOP NAVIGATION MENU -->
                        <div class="top-menu setTopmenuAfterLogin">
                            <ul class="nav navbar-nav pull-right setAfterLMenu">
                                <li class="separator hide"> </li>
                                <li class="usercenter-li f-s-16 setHeader_fed" id="header_feedback">
                                    <a href="{{ route('frontend.feedback', \Session::get('lang') ) }}" class="setFee"
                                       class="normal-link setNormalLink {{ isset($_G['route_name']) && $_G['route_name'] == 'frontend.feedback' ? ' active' : '' }} {{ isset($_G['route_name']) && $_G['route_name'] == 'frontend.feedbackChat' ? ' active' : '' }}">
                                <span class="fee setFee">
                                {{ trans('common.feedback') }}
                                </span>
                                    </a>
                                </li>

                                <li class="dropdown dropdown-extended dropdown-notification dropdown-dark update_notification setNotification" id="header_notification_bar">
                                    @include('frontend.includes.notification')
                                </li>
                                <!-- END NOTIFICATION DROPDOWN -->
                                <li class="separator hide"> </li>
                                <li class="dropdown dropdown-user dropdown-dark setALUL">
                                    <a href="javascript:;" class="dropdown-toggle set-DropDown-User-Area" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" onclick="window.location.href='/resume'">
                                        <img id="imgAvatar" alt=""  onerror="this.src='/images/MainLogoDefault.png'"  class="img-circle setHEADIMAGEAVATAR1" src="{{ $_G['user']->getAvatar() }}" />
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-default dropdown-menu-logout">
                                        <li class="user set-User-Li">
                                            <div class="user-img text-center" style="cursor: pointer"  onclick="window.location.href='/resume'" >
                                                <img alt="" onerror="this.src='/images/MainLogoDefault.png'" class="img-circle setHEADIMAGEAVATAR2" src="{{ $_G['user']->getCompressedImageAttribute() }}" />
                                            </div>
                                            <div class="user-name">
                                                <span class="username username-hide-on-mobile">
                                                {{ Auth::guard('users')->user()->getName() }}
                                                </span><br>
                                                {{--<small>--}}
                                                    {{--{{ trans('common.level') }} : 50--}}
                                                {{--</small>--}}
                                            </div>


                                            <div class="setlngec">
                                                <a class="setaImgEng" href="{{ route('setlang', ['lang' => 'en', 'redirect' => url('')]) }}">
                                                    <img src="{{ trans('common.language_flg_en')}}">
                                                    {{ trans('common.lang_en') }}
                                                </a>
                                                <a class="setaImgChin" href="{{ route('setlang', ['lang' => 'cn', 'redirect' => url('')]) }}">
                                                    <img src="{{ trans('common.language_flg_cn')}}">
                                                    {{ trans('common.lang_cn') }}
                                                </a>
                                            </div>


                                            <div class="setHeadCBOTH"></div>
                                        </li>
                                        <li class="change-status set-HHover">
                                            <a href="javascript:;" data-toggle="modal"
                                               data-target="#modalChangeUserStatus">
                                                <img src="/images/icons/al_co-founder1_yellow.png">
                                                <span>
                                                {{ trans('common.change_status') }}
                                                </span>
                                            </a>
                                        </li>
                                        <li class="set-HHover">
                                            <a href="{{ route('frontend.personalinformation', \Session::get('lang') ) }}">
                                                <img src="/images/icons/id_card_filled_100px.png">
                                                <span>
                                                {{ trans('common.personal_information') }}
                                                </span>
                                            </a>
                                        </li>
                                        <li class="set-HHover">
                                            <a href="{{ route('frontend.skillverification', \Session::get('lang') ) }}">
                                                <img src="/images/icons/tasks_100px.png">
                                                <span>
                                                {{ trans('common.skill_verification') }}
                                                </span>
                                            </a>
                                        </li>
                                        <li class="set-HHover">
                                            <a href="{{ route('frontend.myaccount', \Session::get('lang') ) }}">
                                                <img src="/images/icons/bank_card_back_side_100px.png">
                                                <span>
                                                {{ trans('common.receive_payment_details') }}
                                                </span>
                                            </a>
                                        </li>
                                        <li class="set-HHover">
                                            <a href="{{ route('logout') }}">
                                                <img src="/images/icons/logout_rounded_left_96px.png">
                                                <span>
                                                {{ trans('member.logout') }}
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown dropdown-extended quick-sidebar-toggler hide">
                                    <span class="sr-only">Toggle Quick Sidebar</span>
                                    <i class="icon-logout"></i>
                                </li>
                                <!-- END QUICK SIDEBAR TOGGLER -->
                            </ul>
                        </div>
                        <!-- END TOP NAVIGATION MENU -->
                    @endif
                </div>
                <!-- responsive top-side menu -->
                {{--<div class="col-md-2 setCol-Md-2">--}}
                <div class="setCollapseAreaNew">
                    @if (Auth::guard('users')->guest())
                        {{--<a href="javascript:;" class="menu-toggler responsive-toggler setGuestMenuToggler" data-toggle="dropdown" data-target=".guest-menu"> </a>--}}
                        <a class="setbtnbtnColllapseN" data-toggle="collapse" data-target="#demo1">
                            <span class="sr-only"></span>
                            <i class="ti-menu"></i>
                        </a>
                        {{--<div class="custom-collapse navbar-collapse collapse inner-nav guest-menu setCustomCollapse">--}}
                        <div id="demo1" class="setCustomCollapse2">
                            {{--<ul class="dropdown-menu pull-right setGUESTMUL" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">--}}
                            <ul class="nav navbar-nav setNAVBARHOMEPAGE">
                                <li class="nav-item start">
                                    <a href="{{ route('register') }}" class="{{ isset($_G['route_name']) && $_G['route_name'] == 'register' ? ' activatedLink' : '' }} nav-link nav-toggle setNavLinkToggle">
                                        <span class="title">{{ trans('member.register') }}</span>
                                        <span class="selected"></span>
                                    </a>
                                </li>
                                <li class="nav-item start">
                                    <a href="{{ route('login') }}" class="{{ isset($_G['route_name']) && $_G['route_name'] == 'login' ? ' activatedLink' : ''}} nav-link nav-toggle setNavLinkToggle">
                                        <span class="title">{{ trans('member.login') }}</span>
                                        <span class="selected"></span>
                                    </a>
                                </li>
                                <li class="nav-item start">
                                    <a href="{{ route('setlang', ['lang' => 'en', 'redirect' => Request::url()]) }}" class="nav-link nav-toggle setNavLinkToggle">
                                        <span class="title">{{ trans('common.lang_en') }}</span>
                                        <span class="selected"></span>
                                    </a>
                                </li>
                                <li class="nav-item start">
                                    <a href="{{ route('setlang', ['lang' => 'cn', 'redirect' => Request::url()]) }}" class="nav-link nav-toggle setNavLinkToggle">
                                        <span class="title">{{ trans('common.lang_cn') }}</span>
                                        <span class="selected"></span>
                                    </a>
                                </li>
                                <li role="presentation" class="divider"></li>
                                <li class="nav-item start">
                                    <a href="{{ route('home') }}" class="nav-link nav-toggle setNavLinkToggle">
                                        <span class="title">{{ trans('common.home')}}</span>
                                        <span class="selected"></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    @else
                        {{--<a href="javascript:;" class="menu-toggler responsive-toggler setMEnuToggler" data-toggle="dropdown" data-target=".user-menu"> </a>--}}
                        <a class="setbtnbtnColllapseN2" data-toggle="collapse" data-target="#demo">
                            <span class="sr-only"></span>
                            <i class="ti-menu"></i>
                        </a>
                        <div id="demo" class="setCustomCollapse3">
                            {{--<ul class="dropdown-menu pull-right setAfterLoginMenu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">--}}
                            <ul class="nav navbar-nav setNAVBARHOMEPAGE1">
                                <li class="nav-item start setLiInnerNav setLiInnerNav">
                                    {{--<a href="{{ route('home') }}" class="nav-link nav-toggle">--}}
                                        {{--<span class="title">{{ trans('common.home')}}</span>--}}
                                        {{--<span class="selected"></span>--}}
                                    {{--</a>--}}
                                    <a href="{{ route('home') }}" class="{{ isset($_G['route_name']) && in_array($_G['route_name'], ['loggedin.home', 'home']) ? ' active' : '' }} setNewCTNB">
                                        {{ trans('common.home')}}
                                    </a>
                                </li>
                                <li class="nav-item start setLiInnerNav">
                                    {{--<a href="{{ route('frontend.tailororders') }}" class="nav-link nav-toggle">--}}
                                    {{--<span class="title">{{ trans('common.tailor_orders')}} </span>--}}
                                    {{--<span class="selected"></span>--}}
                                    </a>
                                    <a href="{{ route('frontend.jobs.index') }}" class="{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.jobs.index']) ? ' active' : '' }} {{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.joindiscussion.getproject']) ? ' active' : '' }} {{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.instruction.hiring']) ? ' active' : '' }} setNewCTNB">
                                        {{ trans('common.jobs')}}
                                    </a>
                                </li>
                                <li class="nav-item start setLiInnerNav">
                                    {{--<a href="{{ route('frontend.createorder') }}" class="nav-link nav-toggle">--}}
                                        {{--<span class="title">{{ trans('common.create_order')}}</span>--}}
                                        {{--<span class="selected"></span>--}}
                                    {{--</a>--}}
                                    <a href="{{ route('frontend.findexperts') }}" class="{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.findexperts']) ? ' active' : '' }} {{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.resume.getdetails']) ? ' active' : '' }} setNewCTNB">
                                        {{ trans('common.find_experts')}}
                                    </a>
                                </li>
                                <li class="nav-item start setLiInnerNav">
                                    <a href="{{ route('frontend.postproject') }}" class="{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.postproject']) ? ' active' : '' }} setNewCTNB">
                                    {{ trans('common.post_project')}}
                                    <!-- <i style="text-align:center;margin: 10px 0 -5px 0" class="fa fa-plus"></i> -->
                                    </a>
                                </li>

                                <li class="nav-item start setLiInnerNav">
                                    <a href="{{ route('frontend.usercenter') }}" class="setNewCTNB">{{ trans('common.user_center') }}
                                    </a>
                                </li>
                                <li class="nav-item start setLiInnerNav">
                                    <a href="{{ route('logout') }}" class="setNewCTNB" >{{trans('member.logout')}}</a>
                                </li>
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
