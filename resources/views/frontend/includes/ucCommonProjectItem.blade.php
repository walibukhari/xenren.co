
<div class="portlet box set-job-div-cursor job div-job-{{ $commonProject->id }} div-job-main check-job-div-main"
     data-reurl="{{ route('frontend.joindiscussion.getproject', [ 'slug' => $commonProject->getSlug(), 'id' => $commonProject->id, 'lang' => \Session::get('lang') ])  }}" data-project-id="{{$commonProject->id}}">
    <div class="portlet-title">
        <div class="row">
            <div class="col-md-12">
                <div class="span-info-{{ $commonProject->id }} col-span-12">
                    <a href="{{ route('frontend.joindiscussion.getproject', [$commonProject->getSlug(), $commonProject->id, 'lang' => \Session::get('lang')]) }}"> </a>
                        <div class="project-name pull-left title-section">
                            <span>{{ $commonProject->name }}</span>
                            @if(\Auth::user())
                                @php
                                  $generalService = new \App\Services\GeneralService();
                                  $score = $generalService->calculateScore($commonProject);
                                @endphp

                                @if($score['score'] >= 80)
                                    <img class="set-per-text openDialog" src="{{ asset('images/icons/award-Green-per.jpg') }}">
                                    <span data-scoredetail="{{$score['detail']}}" class="per-text openDialog ">{{$score['score']}}<sub class="setper-text openDialog">%</sub></span>
                                @elseif($score['score'] >= 60)
                                    <img class="set-per-text openDialog award-yellow-per" src="{{ asset('images/icons/award-Yellow-per.jpg') }}">
                                    <span data-scoredetail="{{$score['detail']}}"  class="per-text  openDialog award-yellow-per">{{$score['score']}}<sub class="setper-text openDialog">%</sub></span>
                                @elseif($score['score'] >= 50)
                                    <img class="set-per-text openDialog award-Red-per" src="{{ asset('images/icons/award-Red-per.jpg') }}">
                                    <span data-scoredetail="{{$score['detail']}}"   class="per-text  openDialog award-Red-per">{{$score['score']}}<sub class="setper-text openDialog">%</sub></span>
                                @else
                                    <img class="set-per-text openDialog award-Red-per" src="{{ asset('images/icons/award-Red-per-full.jpg') }}">
                                    <span data-scoredetail="{{$score['detail']}}"  class="per-text  openDialog award-Red-per">{{$score['score']}}<sub class="setper-text openDialog">%</sub></span>
                                @endif
                            @endif
                        </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 price-div">
                <span class="top-smoll">
                    {{ $commonProject->explainPayType() }}
                    @if( $commonProject->pay_type == \App\Constants::PAY_TYPE_FIXED_PRICE || $commonProject->pay_type == \App\Constants::PAY_TYPE_HOURLY_PAY )
                    -
                    <span class="price">
                        {{--<i class="fa fa-usd" aria-hidden="true"></i>--}}
                        {{ $commonProject->getReferencePrice() }}.
                    </span>
                    @endif
                </span>
            </div>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">

        </div>
        <div class="row">
            <div class="skill">
                <div class="col-md-8">
                    <div class="skill-div div-skill-{{ $commonProject->id }} setJPSKILLDIVCol8" >
                        @foreach( $commonProject->projectSkills as $key => $projectSkill )
                            <span class="item-skill {{ $projectSkill->skill->tag_color == \App\Models\Skill::TAG_COLOR_GOLD? 'yellow': 'white' }}">
                                <span>
                                    {{ $projectSkill->getSkillName( $projectSkill->skill_id ) }}
                                </span>
                            </span>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4 btn-sect setBtnSect">
                    @if( \Auth::guard('users')->check())
                        <button class="btn btn-white-bg-green-text setJBtn1" onClick="window.location='{{ route('frontend.joindiscussion.getproject', [\Session::get('lang'),$commonProject->getSlug(), $commonProject->id]) }}'">
                            {{ trans('common.join_discuss') }}
                        </button>

                        <a class="setJBtn2" href="{{ route('frontend.modal.makeoffer', [
                                'project_id' => $commonProject->id,
                                'user_id' => \Auth::id(),
                                'inbox_id' => 0
                            ] ) }}"
                            data-toggle="modal"
                            data-target="#modalMakeOffer">
                            @php
                                $userNo = \Auth::id();
                                $projectNo = $commonProject->id;
                                $count = App\Models\ProjectApplicant::GetProjectApplicant()
                                    ->where('project_id', $projectNo)
                                    ->where('user_id', $userNo)->count();
                            @endphp
                            @if ($count >= 1)
                            <button id="btn-make-offer-{{ $commonProject->id }}" class="btn btn-green-bg-white-text" style="color:#808080; background-color:#ccc6c4 !important; border:2px solid #ccc6c4 !important;">
                                {{ trans('common.offer_made') }}
                            </button>
                            @elseif(App\Models\Project::GetProject()
                                    ->where('user_id', $userNo)
                                    ->where('id', $projectNo)
                                    ->exists())
                            <!--<button id="btn-make-offer-{{ $commonProject->id }}" class="btn btn-green-bg-white-text" style="color:#808080; background-color:#ccc6c4 !important; border:2px solid #ccc6c4 !important;">
                                {{ trans('common.creator') }}
                            </button>-->
                            @else
                            <button id="btn-make-offer-{{ $commonProject->id }}" class="btn btn-green-bg-white-text">
                                {{ trans('common.make_offer') }}
                            </button>
                            @endif
                        </a>

                    @else
                        <button class="btn btn-login btn-white-bg-green-text setJBtn1"
                            data-redirect="{{ route('login').'?redirect_url=' . route('frontend.joindiscussion.getproject', [ 'slug' => $commonProject->getSlug(), 'id' => $commonProject->id, 'lang' => \Session::get('lang') ])}}" onclick="window.location='{{ route('login').'?redirect_url=' . route('frontend.joindiscussion.getproject', [ 'slug' => $commonProject->getSlug(), 'id' => $commonProject->id, 'lang' => \Session::get('lang') ])}}'">
                            {{ trans('common.join_discuss') }}
                        </button>

                        <button class="btn btn-login btn-green-bg-white-text setJBtn2beforeLogin"  data-redirect="{{ route('login') }}">
                            {{ trans('common.make_offer') }}
                        </button>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1">
                <div class="col-md-1 user-img">
                    <div class="user-imgs">
                        @php
                            $employerReviews = \App\Models\FreelancerReview::where('reviewer_id', '=', $commonProject->creator->id)->count();
                            if($employerReviews <= 1 || !isset($employerReviews))
                                $employerReviews = 0;
                            $employerReviewsAvg = \App\Models\FreelancerReview::where('reviewer_id', '=', $commonProject->creator->id)->avg('recommendation');
                             if($employerReviewsAvg <= 1 || !isset($employerReviewsAvg))
                                $employerReviewsAvg = 0;

                            $employerReviewsAvgCiel = ceil($employerReviewsAvg);
                            $employerReviewsAvgCielEmpty = 5 - $employerReviewsAvg;
                            $jobsPosted = \App\Models\Project::where('user_id', '=', $commonProject->creator->id)->count();
                            $earnings = \App\Models\Transaction::where('performed_by', '=', $commonProject->creator->id)->sum('amount');
                            $imageClass = '';
                            $colorClass = '';
                            if($earnings >= 500 && $earnings < 5000) {
                                $imageClass = 'Image_highlight_brown';
                                $colorClass = 'highlight_brown';
                            }
                            if($earnings >= 5000 && $earnings < 10000) {
                                $imageClass = 'Image_highlight_sliver';
                                $colorClass = 'highlight_silver';
                            }
                            if($earnings >= 10000) {
                                $imageClass = 'Image_highlight_gold';
                                $colorClass = 'highlight_gold';
                            }
                        @endphp
                        <div class="set-tooltip">
                            <span class="tooltiptext">{{trans('common.display_after_contacted')}}</span>
                            <img class="set_user_images_CP {{$imageClass}}" src="/images/svster.png">
                        </div>
                        <span class="{{$colorClass}}  {{ $commonProject->creator->getLatestIdentityStatus() == \App\Models\UserIdentity::STATUS_APPROVED? 'green' :'gray' }}"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-11">
                <hr>
                <div class="col-md-9 setucCPcol9">
                        {{--<span class="user-name">{{ $commonProject->creator->getName() }}</span>--}}
                            {{--{!! $commonProject->creator->getRatingStar() !!}--}}
                            {{--<span class="budget"><span class="budget-value">$1000</span> Budget</span> |--}}

                            {{--country--}}
                            {{--@if(count( $commonProject->projectCountries ) != 0 )--}}
                                {{--|--}}
                            {{--@endif--}}
                            @for($i=0; $i < $employerReviewsAvgCiel; $i++)
                            <i class="fa fa-star active_fa_starts " aria-hidden="true"></i>
                            @endfor
                            @for($i=0; $i < $employerReviewsAvgCielEmpty; $i++)
                            <i class="fa fa-star setFaStars " aria-hidden="true"></i>
                            @endfor
                            <span class="setSpn01OTS">{{$employerReviewsAvg}} of {{$employerReviews}} {{trans('common.reviews')}}</span>
                            <br>
                            <span class="country setSpnCNJP">
                                @foreach( $commonProject->projectCountries as $key => $projectCountry )
                                @if( $key != 0 )
                                ,
                                @endif
                                @php
                                try {
                                  $countryName = $projectCountry->country->getName();
                                } catch(\Exception $e) {
                                  $countryName = '';
                                }
                                @endphp
                                {{ $countryName }}
                                @endforeach &nbsp;
                             <span>{{$jobsPosted}} {{trans('common.jobs_posted')}}</span></span><br>
                            <span class="setSpn01OTS">{{trans('common.Over')}} ${{$earnings}} {{trans('common.total_spent')}}</span>
                            {{--languages--}}
                            {{--@if(count( $commonProject->projectLanguages ) != 0 )--}}
                                {{--|--}}
                            {{--@endif--}}
                            {{--<span class="country">--}}
                                {{--@foreach( $commonProject->projectLanguages as $key => $projectLanguage )--}}
                                {{--@if( $key != 0 )--}}
                                {{--,--}}
                                {{--@endif--}}
                                {{--{{ $projectLanguage->language->getName() }}--}}
                                {{--@endforeach--}}
                            {{--</span>--}}
                </div>
                <div class="col-md-3">
                    <div class="post-date pull-right">
                        {{ trans('common.posted') }} : <span class="post-date-result">{{  $commonProject->getCreatedAt_v2() }}</span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="fav-job pull-right">
                        @if( \Auth::check() )
                        <a id="btn-thumb-down" class="btn-thumb-down btn-add-down-job-{{ $commonProject->id }}" href="javascript:;" data-url="{{ route('frontend.addthumbdownjob.post') }}" data-project-id="{{ $commonProject->id }}" data-project-type="{{ $projectType }}" data-toggle="modal" data-target="#modalThumbdownJob">
                            <i class="fa fa-eye-slash" title="clicking this icon job will be removed"></i>
                        </a>
                            {{--{{ $commonProject->isYourFavouriteJob()? 'active': '' }}--}}
                        <a id="btn-fa-heart" class="btn-add-fav-job" href="javascript:;" data-url="{{ $commonProject->isYourFavouriteJob()? route('frontend.removefavouritejob.post') : route('frontend.addfavouritejob.post') }}" data-project-id="{{ $commonProject->id }}">
                            <i class="fa fa-heart pull-right " title="favourite job"></i>
                        </a>
                        {{--<a href="#">--}}
                            {{--<span class="fa fa-thumbs-down pull-right"></span>--}}
                        {{--</a>--}}
                        @endif
                    </div>
                </div>

            </div>
            {{--<div class="col-md-6 span-info-{{ $commonProject->id }} posted-by">--}}
                {{--{{  $commonProject->getProjectCreatedInfo() }}--}}

                {{--<img alt="" class="img-circle" src="{{ $commonProject->creator->getAvatar() }}" width="35px" height="35px">--}}
            {{--</div>--}}
        </div>

    </div>
</div>
