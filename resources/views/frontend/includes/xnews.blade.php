@foreach( $newsList as $key => $news )
    @if ( $news->pic_size == 2 )
    <div class="col-md-2 small-justify-block">
        <figure class="post-thumbnail">
            <a href="{{ $news->link }}" class="post-thumbnail__link post-thumbnail--fullwidth">
                <img src="{{ $news->image }}" class="post-thumbnail__img wp-post-image" alt="photo-1445970389378-eb24802687f7" height="153" width="183" style="object-fit: cover; object-position: 50% 0%;">
            </a>
        </figure>
        <div class="post-content_wrap">
            <header class="entry-header">
                <h4 class="entry-title">
                    <a href="{{ $news->link }}" title="{{ $news->title }}" rel="bookmark">
                        {{ $news->title  }}
                    </a>
                </h4>
            </header>
            <div class="entry-meta small-bold">
                <span class="post__date">
                    <a href="{{ $news->link }}" class="post-date__link">
                        <time >{{ $news->createdAt() }}</time>
                    </a>
                </span>
                <span class="post__author">
                    <a href="{{ $news->link }}" class="post-author" rel="author">
                        {{ $news->stuff->name  }}
                    </a>
                </span>
            </div>
        </div>
    </div>
    @endif

    @if ($news->pic_size == 1)
    <div class="col-md-4 big-justify-block">
        <figure class="post-thumbnail">
            <div class="post-content_wrap">
                <header class="entry-header">
                    <h4 class="entry-title">
                        <a href="{{ $news->link }}" title="{{ $news->title  }}" >
                            {{ $news->title  }}
                        </a>
                    </h4>
                </header>
                <div class="entry-meta small-bold">
                    <span class="post__date">
                        <a href="{{ $news->link }}" class="post-date__link">
                            <time>{{ $news->createdAt() }}</time>
                        </a>
                    </span>
                    <span class="post__author">
                        <a href="{{ $news->link }}" class="post-author" rel="author">
                            {{ $news->stuff->name  }}
                        </a>
                    </span>
                </div>
            </div>
            <a href="{{ $news->link }}" class="post-thumbnail__link post-thumbnail--fullwidth" >
                <img src="{{ $news->image }}" class="post-thumbnail__img wp-post-image" alt="photo-1441123100240-f9f3f77ed41b" height="200" width="372" style="object-fit: cover; object-position: 50% 0%;">
            </a>
        </figure>

    </div>
    @endif
@endforeach