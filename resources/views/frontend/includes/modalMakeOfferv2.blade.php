@extends('frontend.includes.ajaxmodal')

@section('title')
    {{trans('jobs.apply_job')}}
    <link rel="stylesheet" href="http://xen/custom/css/frontend/modalSendOffer.css">
    <style>
        .lds-ring {
            display: inline-block;
            position: relative;
            width: 20px;
            height: 20px;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 20px;
            height: 20px;
            margin: 1px;
            border: 2px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
        #btn-submit{
            display: flex;
            justify-content: center;
            align-items: center;
            width: 150px;
        }
    </style>
@endsection

@section('script')
    <script>
        +function () {
            $(document).ready(function () {
                $('#ckbPayType').bootstrapSwitch();

                $('#ckbPayType').on('switchChange.bootstrapSwitch', function (event, state) {
                    //true = hourly pay, false = fixed price
                    if( state == true )
                    {
                        var hourly_pay = $("#hourly_pay").val();
                        $('#price').val(hourly_pay);
                        $('#pay_type').val("1");
                    }
                    else
                    {
                        $('#price').val("0.00");
                        $('#pay_type').val("2");
                    }

                });

                $('#btn-submit').click(function(){
                    $('.idSpanMakeOffer').hide();
                    $('.lds-ring').show();
                });

                $('#make-offer-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit',
                    alertContainer : '#model-alert-container',
                    afterSuccessFunction: function (response, $el, $this) {
                        if( response.status == 'OK'){
                            $('#modalMakeOffer').modal('toggle');
                            alertSuccess(response.message);

                            $inbox_id = $('#inbox_id').val();
                            if( $inbox_id > 0 )
                            {
                                setTimeout(function(){
                                    location.reload();
                                },1000);
                            }
                        }else{
                            $('.idSpanMakeOffer').show();
                            $('.lds-ring').hide();
                            App.alert({
                                type: 'danger',
                                icon: 'warning',
                                message: response.message,
                                container: '#model-alert-container',
                                reset: true,
                                focus: true
                            });
                        }

                        App.unblockUI('#make-offer-form');
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <div style="margin-top:0px;">
        <hr/>
    </div>
    <div class="text-center" style="display:flex;justify-content:center;">
         <button type="submit" class="btn btn-green-bg-white-text" id="btn-submit">
             <span class="idSpanMakeOffer">{{trans('jobs.make_offer')}}</span>
             <div style="display: none;" class="lds-ring">
                 <div></div><div></div><div></div><div></div>
             </div>
         </button>
    </div>
@endsection

@section('content')
    {!! Form::open(['route' => ['frontend.modal.makeoffer.post'], 'method' => 'post', 'role' => 'form', 'id' => 'make-offer-form']) !!}

        <div class="form-body">
            <div class="form-group">
                <div id="model-alert-container">
                </div>
            </div>

            <div class="col-md-7" style="padding:0px">
                <div class="form-group">
                    <label>{{ trans('common.price') }} ({{ trans('common.currency_symbol')  }})</label>
                    {!! Form::text('price', isset($user->hourly_pay)? $user->getHourlyPay_v2(true): '' , ['class' => 'form-control', 'id' => 'price']) !!}
                    <input type="hidden" id="hourly_pay" value="{{ isset($user->hourly_pay)? $user->getHourlyPay_v2(true): '' }}">
                </div>
            </div>

            <div class="col-md-5" style="padding-right: 0px;">
                <div class="form-group">
                    <label>{{ trans('common.pay_type') }}</label><br/>
                    <input type="checkbox"
                        class="make-switch form-control"
                        name="ckbPayType"
                        id="ckbPayType"
                        data-size="switch-size"
                        data-on-text="{{ trans('common.pay_hourly') }}" data-off-text="{{ trans('common.fixed_price') }}"
                        data-on-color="green" data-off-color="green"
                        value="false"
                        checked >
                    <input type="hidden" id="pay_type" name="pay_type" value="1">
                </div>
            </div>

            <div class="form-group">
                <label>{{ trans('common.about_me') }}</label>
                {!! Form::textarea('about_me', isset($user->about_me)? $user->about_me: '' , ['class' => 'form-control']) !!}
            </div>


            @if(!$is_official)
            <div class="form-group">
                <label class="bold">{{trans('common.offer')}}</label>
                <div class="">
                    <div class="radio-item">
                        <input type="radio" id="daily_update_chkBox" name="daily_update" value="1" checked="">
                        <label for="daily_update_chkBox">
                            <span class="radio-labe1">{{trans('common.yes')}}</span>
                        </label>
                    </div>

                    <div class="radio-item">
                        <input type="radio" id="daily_update_chkBox_1" name="daily_update" value="0">
                        <label for="daily_update_chkBox_1">
                            <span class="radio-labe1">{{trans('common.no')}}</span>
                        </label>
                    </div>

                    <br>
                </div>
            </div>
            @endif

            <div class="question-section">
                @foreach( $project->projectQuestions as $key => $projectQuestion )
                <div class="form-group">
                    <label>{{ trans('common.question') }} {{ $key + 1 }} : {{ $projectQuestion->question }} </label>
                    <input class="form-control" name="answer[]" value="" type="text">
                    <input class="form-control" name="questionId[]" value="{{ $projectQuestion->id }}" type="hidden">
                </div>
                @endforeach
            </div>

            <div class="form-group content-group">
                <label>{{ trans('common.contact') }}</label>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <img src="/images/line.png">
                            </span>
                            {!! Form::text('line_id', $user->line_id, ['class' => 'form-control text-center', 'id' => 'tbxlineId', 'placeholder' => 'Line' ]) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <img src="/images/skype-icon-5.png">
                            </span>
                            {!! Form::text('skype_id', $user->skype_id, ['class' => 'form-control text-center', 'id' => 'tbxSkypeId', 'placeholder' => 'Skype']) !!}
                        </div>
                    </div>
                    <br>
                    <br>
                    <br>
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <img src="/images/wechat-logo.png">
                            </span>
                            {!! Form::text('wechat_id', $user->wechat_id, ['class' => 'form-control text-center', 'id' => 'tbxWechatId', 'placeholder' => 'Wechat']) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <img src="/images/MetroUI_Phone.png">
                            </span>
                            {!! Form::text('handphone_no', $user->handphone_no, ['class' => 'form-control text-center', 'id' => 'tbxHandphoneNo', 'placeholder' => 'Phone']) !!}
                        </div>
                    </div>
                </div>


            </div>
        </div>
        {!! Form::hidden('project_id', $project->id, ['class' => 'modal-project-id']) !!}
        {!! Form::hidden('inbox_id', $inbox_id, ['id' => 'inbox_id']) !!}
    {!! Form::close() !!}
@endsection


