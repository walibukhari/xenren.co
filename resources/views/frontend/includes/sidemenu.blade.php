<style type="text/css">
    .page-sidebar-wrapper{
        position: absolute;
        top: 60px;
    }
</style>
<style>

    .topnav {
        overflow: hidden;
        background-color: #5ec329;
        margin: -19px;
        color: white;
    }

    .topnav a {
        color: #fff;
        font-size: 16px;
        font-weight: 500;
        padding: 13px;
    }


    .topnav a:hover {
        color: white;
        text-decoration:underline;
    }

    .active {
        color: #fff;
        margin-top:10px;
        text-decoration:underline;
        text-decoration-style: solid;
        text-decoration-color: white;
        font-size: 20px;
    }

    .topnav .icon {
        display: none;
    }
    #menu {
        display:none;
    }

    .setSideMEnu li{
        text-decoration: none !important;
        padding-left:0px !important;
    }

    @media screen and (max-width:6000px) and (min-width:992px) {
        .topnav a:not(:first-child) {display: none;}
    }

    @media screen and (max-width: 991px) {
        .topnav a:not(:first-child) {display: none;}
        .topnav a.icon {
            float: right;
            display: block;
        }
        #menu{
            display:block
        }
        .topnav a {
            float: left;
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none !important;
            text-decoration-style:unset !important;
            font-size: 21px;
            font-weight:500;
        }
    }

    @media screen and (max-width: 991px) {
        .topnav.responsive {position: relative;}
        .topnav.responsive .icon {
            position: absolute;
            right: 0;
            top: 0;
        }
        .topnav.responsive a {
            float: none;
            display: block;
            text-align: left;
        }
    }

    .setImageColor{
        filter:white;
    }

    #myTopnav a .title span{
        background-color: white;
        color: #5ec329;
    }

</style>
<div class="topnav" id="myTopnav">
    <a href="#" id="menu">User Center</a>
    <a href="{{ route('frontend.inbox' , \Session::get('lang')) }}" class="{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.inbox']) ? ' active open' : '' }}">
        <hr>
        <img src="/images/icons/inbox.png" class="setImageColor" style="width:20px;">
        <span class="title f-s-14">
                        &nbsp;{{ trans('common.inbox') }}
            @if (isset($pendingNotification) && $pendingNotification != null && $pendingNotification != 0)
                <span class="badge setBadge">{{ $pendingNotification }}</span>
            @endif
                    </span>
    </a>


    <a href="{{ route('frontend.mypublishorder', \Session::get('lang')) }}" class="awardedProjects {{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.mypublishorder']) ? ' active open' : '' }}">
        <img src="/images/icons/award.png" style="width: 20px">
        <span class="title f-s-14">
                        {{trans('common.award_projects')}}
                        @if (isset($_G['user']) && $_G['user']->getMyPublishOrderCount() != null && $_G['user']->getMyPublishOrderCount() != 0)
                <span class="badge setBadge">{{ $_G['user']->getMyPublishOrderCount() }}</span>
            @endif
                    </span>
    </a>

    <a href="{{ route('frontend.myreceiveorder', \Session::get('lang')) }}" class="receivedProjects {{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.myreceiveorder']) ? ' active open' : '' }}">
        <img src="/images/icons/my_receive_order.png" style="width:20px;">
        <span class="title f-s-14">
                        &nbsp;{{ trans('common.receive_project') }}
            @if (isset($_G['user']) && $_G['user']->getMyReceivedOrderCount() != null && $_G['user']->getMyReceivedOrderCount() != 0)
                <span class="badge badge-danger setBadge">{{ $_G['user']->getMyReceivedOrderCount() }}</span>
            @endif
                    </span>
    </a>


    <a href="{{ url('/resume', \Session::get('lang')) }}" class="{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.resume', 'frontend.usercenter']) ? ' active open' : '' }}">
        <img src="/images/icons/inbox.png" style="width:20px;">
        <span class="title f-s-14">
                        &nbsp;{{ trans('common.resume') }}
                    </span>
    </a>


    <a href="{{ url('/personalInformation') }}" class="{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.personalinformation']) ? ' active open' : '' }}">
        <img style="width:20px;" src="/images/icons/personal_information.png">
        <span class="title f-s-14">
                        &nbsp;{{ trans('common.personal_information') }}
                    </span>
    </a>

    <a href="{{ route('frontend.identityauthentication', \Session::get('lang')) }}" class="{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.identityauthentication']) ? ' active open' : '' }}">
        <img style="width:20px;" src="/images/icons/identity_authentication.png">
        <span class="title f-s-14">
                        &nbsp;{{ trans('common.authentication') }}
            @if(isset($_G['user']) && !($_G['user']->getLatestIdentityStatus() == \App\Models\UserIdentity::STATUS_APPROVED ||
                $_G['user']->getLatestIdentityStatus() == \App\Models\UserIdentity::STATUS_REJECTED)
            )
                <i class="fa fa-exclamation-circle identity-authentication"></i>
            @endif
                    </span>
    </a>

    <a href="{{ route('frontend.skillverification') }}" class="{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.skillverification']) ? ' active open' : '' }}">
        <img style="width:20px;" src="/images/icons/skill_verification.png">
        <span class="title f-s-14">
                        &nbsp;{{ trans('common.skill_verification') }}
            @if(isset($_G['user']) && $_G['user']->isSkillVerifiedByAdmin() == false )
                <i class="fa fa-exclamation-circle skill"></i>
            @endif
                    </span>
    </a>

    <a href="{{ route('frontend.myaccount', \Session::get('lang')) }}" class="{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.myaccount']) ? ' active open' : '' }}">
        <img src="/images/icons/my_bank_account.png" style="width:20px;" >
        <span class="title f-s-14">&nbsp;{{ trans('common.receive_payment_details') }}</span>
        <span class="selected"></span>
    </a>

    <a href="{{ route('frontend.transactionHistory') }}" class="{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.transactionDetails']) ? ' active open' : '' }}">
        <img style="width:20px;" src="/images/icons/favourite_jobs.png">
        <span class="title f-s-14">&nbsp;{{trans('common.transaction_details')}}</span>
        <span class="selected"></span>
    </a>

    <a href="{{ route('frontend.favouriteJobs', \Session::get('lang')) }}" class="{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.favouriteJobs']) ? ' active open' : '' }}">
        <img style="width:20px;" src="/images/icons/favourite_jobs.png">
        <span class="title f-s-14">&nbsp;{{ trans('common.favourite_jobs') }}</span>
        <span class="selected"></span>
    </a>

    <a href="{{ route('frontend.favouriteFreelancers', \Session::get('lang')) }}" class="{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.favouriteFreelancers']) ? ' active open' : '' }}">
        <img style="width:20px;" src="/images/icons/favourite_freelancers.png">
        <span class="title f-s-14">&nbsp;{{ trans('common.favourite_freelancers') }}</span>
        <span class="selected"></span>
    </a>

    <a href="{{ route('frontend.invite',['lang' => \Session::get('lang')]) }}" class="{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.invite']) ? ' active open' : '' }}">
        <img style="width:20px;" src="/images/icons/invite_to_join.png">
        <span class="title f-s-14">&nbsp;{{ trans('common.invite_to_join') }}</span>
        <span class="selected"></span>
    </a>

    <a href="{{ route('frontend.setting', ['lang' =>  \Session::get('lang')]) }}" class="{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.setting']) ? ' active open' : '' }}">
        <img style="width:20px; color:white;" src="/images/icons/my_receive_order.png">
        <span class="title f-s-14">&nbsp;{{ trans('common.setting') }}</span>
        <span class="selected"></span>
    </a>

    <a href="{{ route('logout') }}" class="{{ isset($_G['route_name']) && in_array($_G['route_name'], ['logout']) ? ' active open' : '' }}">
        <img src="/images/icons/logout.png" style="width:20px;">
        <span class="title f-s-14">&nbsp;{{ trans('member.logout') }}</span>
        <span class="selected"></span>
    </a>

    <a href="javascript:void(0);" class="icon" onclick="myFunction()">
        <i class="fa fa-bars"></i>
    </a>
</div>
<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper floating-menu">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse setSideMenuScroll">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu page-sidebar-menu-custom   setSideMEnu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            {{--@if (isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.inbox', 'frontend.systemmessage', 'frontend.trash']))--}}
            {{--<li class="nav-item{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.inbox']) ? ' active open' : '' }}">--}}
                {{--<a href="{{ route('frontend.inbox') }}" class="nav-link nav-toggle">--}}
                    {{--<img src="/images/icons/inbox.png">--}}
                    {{--<span class="title f-s-14">&nbsp;{{ trans('common.inbox') }}</span>--}}
                    {{--<span class="selected"></span>--}}
                    {{--@if( $pendingNotification != 0 )--}}
                    {{--<span class="badge badge-danger">{{ $pendingNotification }}</span>--}}
                    {{--@endif--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="nav-item{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.systemmessage']) ? ' active open' : '' }}">--}}
                {{--<a href="{{ route('frontend.systemmessage') }}" class="nav-link nav-toggle">--}}
                    {{--<i class="icon-screen-desktop"></i>--}}
                    {{--<span class="title f-s-14">&nbsp;{{ trans('common.system_message') }}</span>--}}
                    {{--<span class="selected"></span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="nav-item{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.trash']) ? ' active open' : '' }}">--}}
                {{--<a href="{{ route('frontend.trash') }}" class="nav-link nav-toggle">--}}
                    {{--<i class="fa fa-trash-o"></i>--}}
                    {{--<span class="title f-s-14">&nbsp;{{ trans('common.trash') }}</span>--}}
                    {{--<span class="selected"></span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--@else--}}
            <li class="nav-item start{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.inbox']) ? ' active open' : '' }}">
                <a href="{{ route('frontend.inbox', \Session::get('lang')) }}" class="nav-link nav-toggle">
                    <img src="/images/icons/inbox.png">
                    <span class="title f-s-14">
                        &nbsp;{{ trans('common.inbox') }}
                        @if (isset($pendingNotification) && $pendingNotification != null && $pendingNotification != 0)
                            <span class="badge setBadge">{{ $pendingNotification }}</span>
                        @endif
                    </span>
                </a>
            </li>

            {{--<li class="nav-item start{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.myprogressiveprojects']) ? ' active open' : '' }}">--}}
                {{--<a href="{{ route('frontend.myprogressiveprojects') }}" class="nav-link nav-toggle">--}}
                    {{--<img src="/images/icons/my_progressing_project.png">--}}
                    {{--<span class="title f-s-14">--}}
                        {{--&nbsp;{{ trans('common.progressive_project') }}--}}
                        {{--@if ($_G['user']->getMyProgressiveOrderCount() != null && $_G['user']->getMyProgressiveOrderCount() != 0)--}}
                            {{--<span class="badge">{{ $_G['user']->getMyProgressiveOrderCount() }}</span>--}}
                        {{--@endif--}}
                    {{--</span>--}}
                {{--</a>--}}
            {{--</li>--}}

            <li class="nav-item start awardedProjects {{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.mypublishorder']) ? ' active open' : '' }}">
                <a href="{{ route('frontend.mypublishorder', \Session::get('lang') ) }}" class="nav-link nav-toggle">
                    <img src="/images/icons/award.png" style="width: 33px;margin-left: -5px;">
                    <span class="title f-s-14 setAwadProjects">
                         {{trans('common.award_projects')}}
                        @if (isset($_G['user']) && $_G['user']->getMyPublishOrderCount() != null && $_G['user']->getMyPublishOrderCount() != 0)
                            <span class="badge setBadge">{{ $_G['user']->getMyPublishOrderCount() }}</span>
                        @endif
                    </span>
                </a>
            </li>
            <li class="receivedProjects  nav-item{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.myreceiveorder']) ? ' active open' : '' }}">
                <a href="{{ route('frontend.myreceiveorder', \Session::get('lang') ) }}" class="nav-link nav-toggle">
                    <img src="/images/icons/my_receive_order.png">
                    <span class="title f-s-14">
                        &nbsp;{{ trans('common.receive_project') }}
                        @if (isset($_G['user']) && $_G['user']->getMyReceivedOrderCount() != null && $_G['user']->getMyReceivedOrderCount() != 0)
                            <span class="badge badge-danger setBadge">{{ $_G['user']->getMyReceivedOrderCount() }}</span>
                        @endif
                    </span>
                </a>
            </li>
            <li class="nav-item{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.resume', 'frontend.usercenter']) ? ' active open' : '' }}">
                <a href="{{ url(\Session::get('lang').'/resume') }}" class="nav-link nav-toggle">
                    <img src="/images/icons/inbox.png">
                    <span class="title f-s-14">
                        &nbsp;{{ trans('common.resume') }}
                    </span>
                </a>
            </li>
            <li class="nav-item{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.personalinformation']) ? ' active open' : '' }}">
                <a href="{{ url(\Session::get('lang').'/personalInformation') }}" class="nav-link nav-toggle">
                    <img src="/images/icons/personal_information.png">
                    <span class="title f-s-14">
                        &nbsp;{{ trans('common.personal_information') }}
                    </span>
                </a>
            </li>
            <li class="nav-item{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.identityauthentication']) ? ' active open' : '' }}">
                <a href="{{ route('frontend.identityauthentication', \Session::get('lang')) }}" class="nav-link nav-toggle">
                    <img src="/images/icons/identity_authentication.png">
                    <span class="title f-s-14">
                        &nbsp;{{ trans('common.authentication') }}
                        @if(isset($_G['user']) && !($_G['user']->getLatestIdentityStatus() == \App\Models\UserIdentity::STATUS_APPROVED ||
                            $_G['user']->getLatestIdentityStatus() == \App\Models\UserIdentity::STATUS_REJECTED)
                        )
                        <i class="fa fa-exclamation-circle identity-authentication"></i>
                        @endif
                    </span>
                </a>
            </li>
            <li class="nav-item{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.skillverification']) ? ' active open' : '' }}">
                <a href="{{ route('frontend.skillverification' , \Session::get('lang')) }}" class="nav-link nav-toggle">
                    <img src="/images/icons/skill_verification.png">
                    <span class="title f-s-14">
                        &nbsp;{{ trans('common.skill_verification') }}
                        @if(isset($_G['user']) && $_G['user']->isSkillVerifiedByAdmin() == false )
                        <i class="fa fa-exclamation-circle skill"></i>
                        @endif
                    </span>
                </a>
            </li>
            <li class="nav-item{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.myaccount']) ? ' active open' : '' }}">
                <a href="{{ route('frontend.myaccount' , \Session::get('lang')) }}" class="nav-link nav-toggle " style="text-align: left">
                    <img src="/images/icons/my_bank_account.png">
                    <span class="title f-s-14">&nbsp;{{ trans('common.receive_payment_details') }}</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.transactionDetails']) ? ' active open' : '' }}">
                <a href="{{ route('frontend.transactionHistory' , \Session::get('lang')) }}" class="nav-link nav-toggle">
                    <img src="/images/icons/favourite_jobs.png">
                    <span class="title f-s-14">&nbsp;{{trans('common.transaction_details')}}</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.favouriteJobs']) ? ' active open' : '' }}">
                <a href="{{ route('frontend.favouriteJobs', \Session::get('lang')) }}" class="nav-link nav-toggle">
                    <img src="/images/icons/favourite_jobs.png">
                    <span class="title f-s-14">&nbsp;{{ trans('common.favourite_jobs') }}</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.favouriteFreelancers']) ? ' active open' : '' }}">
                <a href="{{ route('frontend.favouriteFreelancers', \Session::get('lang')) }}" class="nav-link nav-toggle">
                    <img src="/images/icons/favourite_freelancers.png">
                    <span class="title f-s-14">&nbsp;{{ trans('common.favourite_freelancers') }}</span>
                <span class="selected"></span>
            </a>
            </li>
            <li class="nav-item{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.invite']) ? ' active open' : '' }}">
                <a href="{{ route('frontend.invite', \Session::get('lang')) }}" class="nav-link nav-toggle">
                    <img src="/images/icons/invite_to_join.png">
                    <span class="title f-s-14">&nbsp;{{ trans('common.invite_to_join') }}</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item{{ isset($_G['route_name']) && in_array($_G['route_name'], ['frontend.setting']) ? ' active open' : '' }}">
                <a href="{{ route('frontend.setting' , ['lang' => \Session::get('lang')]) }}" class="nav-link nav-toggle">
                    <img src="/images/icons/my_receive_order.png">
                    <span class="title f-s-14">&nbsp;{{ trans('common.setting') }}</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item{{ isset($_G['route_name']) && in_array($_G['route_name'], ['logout']) ? ' active open' : '' }}">
                <a href="{{ route('logout') }}" class="nav-link nav-toggle">
                    <img src="/images/icons/logout.png">
                    <span class="title f-s-14">&nbsp;{{ trans('member.logout') }}</span>
                    <span class="selected"></span>
                </a>
            </li>
            <!--
            <li class="nav-item start active open">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start active open">
                        <a href="index.html" class="nav-link ">
                            <i class="icon-bar-chart"></i>
                            <span class="title">Dashboard 1</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li class="nav-item start ">
                        <a href="dashboard_2.html" class="nav-link ">
                            <i class="icon-bulb"></i>
                            <span class="title">Dashboard 2</span>
                            <span class="badge badge-success">1</span>
                        </a>
                    </li>
                    <li class="nav-item start ">
                        <a href="dashboard_3.html" class="nav-link ">
                            <i class="icon-graph"></i>
                            <span class="title">Dashboard 3</span>
                            <span class="badge badge-danger">5</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="heading">
                <h3 class="uppercase">Features</h3>
            </li>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-diamond"></i>
                    <span class="title">UI Features</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="ui_colors.html" class="nav-link ">
                            <span class="title">Color Library</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ui_general.html" class="nav-link ">
                            <span class="title">General Components</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ui_buttons.html" class="nav-link ">
                            <span class="title">Buttons</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ui_confirmations.html" class="nav-link ">
                            <span class="title">Popover Confirmations</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ui_icons.html" class="nav-link ">
                            <span class="title">Font Icons</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ui_socicons.html" class="nav-link ">
                            <span class="title">Social Icons</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ui_typography.html" class="nav-link ">
                            <span class="title">Typography</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ui_tabs_accordions_navs.html" class="nav-link ">
                            <span class="title">Tabs, Accordions & Navs</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ui_timeline.html" class="nav-link ">
                            <span class="title">Timeline</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ui_tree.html" class="nav-link ">
                            <span class="title">Tree View</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <span class="title">Page Progress Bar</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item ">
                                <a href="ui_page_progress_style_1.html" class="nav-link "> Flash </a>
                            </li>
                            <li class="nav-item ">
                                <a href="ui_page_progress_style_2.html" class="nav-link "> Big Counter </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item  ">
                        <a href="ui_blockui.html" class="nav-link ">
                            <span class="title">Block UI</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ui_bootstrap_growl.html" class="nav-link ">
                            <span class="title">Bootstrap Growl Notifications</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ui_notific8.html" class="nav-link ">
                            <span class="title">Notific8 Notifications</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ui_toastr.html" class="nav-link ">
                            <span class="title">Toastr Notifications</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ui_bootbox.html" class="nav-link ">
                            <span class="title">Bootbox Dialogs</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ui_alerts_api.html" class="nav-link ">
                            <span class="title">Metronic Alerts API</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ui_session_timeout.html" class="nav-link ">
                            <span class="title">Session Timeout</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ui_idle_timeout.html" class="nav-link ">
                            <span class="title">User Idle Timeout</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ui_modals.html" class="nav-link ">
                            <span class="title">Modals</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ui_extended_modals.html" class="nav-link ">
                            <span class="title">Extended Modals</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ui_tiles.html" class="nav-link ">
                            <span class="title">Tiles</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ui_datepaginator.html" class="nav-link ">
                            <span class="title">Date Paginator</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ui_nestable.html" class="nav-link ">
                            <span class="title">Nestable List</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-puzzle"></i>
                    <span class="title">Components</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="components_date_time_pickers.html" class="nav-link ">
                            <span class="title">Date & Time Pickers</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="components_color_pickers.html" class="nav-link ">
                            <span class="title">Color Pickers</span>
                            <span class="badge badge-danger">2</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="components_select2.html" class="nav-link ">
                            <span class="title">Select2 Dropdowns</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="components_bootstrap_select.html" class="nav-link ">
                            <span class="title">Bootstrap Select</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="components_multi_select.html" class="nav-link ">
                            <span class="title">Multi Select</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="components_bootstrap_select_splitter.html" class="nav-link ">
                            <span class="title">Select Splitter</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="components_typeahead.html" class="nav-link ">
                            <span class="title">Typeahead Autocomplete</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="components_bootstrap_tagsinput.html" class="nav-link ">
                            <span class="title">Bootstrap Tagsinput</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="components_bootstrap_switch.html" class="nav-link ">
                            <span class="title">Bootstrap Switch</span>
                            <span class="badge badge-success">6</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="components_bootstrap_maxlength.html" class="nav-link ">
                            <span class="title">Bootstrap Maxlength</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="components_bootstrap_fileinput.html" class="nav-link ">
                            <span class="title">Bootstrap File Input</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="components_bootstrap_touchspin.html" class="nav-link ">
                            <span class="title">Bootstrap Touchspin</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="components_form_tools.html" class="nav-link ">
                            <span class="title">Form Widgets & Tools</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="components_context_menu.html" class="nav-link ">
                            <span class="title">Context Menu</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="components_editors.html" class="nav-link ">
                            <span class="title">Markdown & WYSIWYG Editors</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="components_code_editors.html" class="nav-link ">
                            <span class="title">Code Editors</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="components_ion_sliders.html" class="nav-link ">
                            <span class="title">Ion Range Sliders</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="components_noui_sliders.html" class="nav-link ">
                            <span class="title">NoUI Range Sliders</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="components_knob_dials.html" class="nav-link ">
                            <span class="title">Knob Circle Dials</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-settings"></i>
                    <span class="title">Form Stuff</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="form_controls.html" class="nav-link ">
                            <span class="title">Bootstrap Form
                                <br>Controls</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="form_controls_md.html" class="nav-link ">
                            <span class="title">Material Design
                                <br>Form Controls</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="form_validation.html" class="nav-link ">
                            <span class="title">Form Validation</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="form_validation_states_md.html" class="nav-link ">
                            <span class="title">Material Design
                                <br>Form Validation States</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="form_validation_md.html" class="nav-link ">
                            <span class="title">Material Design
                                <br>Form Validation</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="form_layouts.html" class="nav-link ">
                            <span class="title">Form Layouts</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="form_input_mask.html" class="nav-link ">
                            <span class="title">Form Input Mask</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="form_editable.html" class="nav-link ">
                            <span class="title">Form X-editable</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="form_wizard.html" class="nav-link ">
                            <span class="title">Form Wizard</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="form_icheck.html" class="nav-link ">
                            <span class="title">iCheck Controls</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="form_image_crop.html" class="nav-link ">
                            <span class="title">Image Cropping</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="form_fileupload.html" class="nav-link ">
                            <span class="title">Multiple File Upload</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="form_dropzone.html" class="nav-link ">
                            <span class="title">Dropzone File Upload</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-bulb"></i>
                    <span class="title">Elements</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="elements_steps.html" class="nav-link ">
                            <span class="title">Steps</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="elements_lists.html" class="nav-link ">
                            <span class="title">Lists</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="elements_ribbons.html" class="nav-link ">
                            <span class="title">Ribbons</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-briefcase"></i>
                    <span class="title">Tables</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <span class="title">Static Tables</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item ">
                                <a href="table_static_basic.html" class="nav-link "> Basic Tables </a>
                            </li>
                            <li class="nav-item ">
                                <a href="table_static_responsive.html" class="nav-link "> Responsive Tables </a>
                            </li>
                            <li class="nav-item ">
                                <a href="table_bootstrap_master.html" class="nav-link "> Bootstrap Table </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <span class="title">Datatables</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item ">
                                <a href="table_datatables_managed.html" class="nav-link "> Managed Datatables </a>
                            </li>
                            <li class="nav-item ">
                                <a href="table_datatables_buttons.html" class="nav-link "> Buttons Extension </a>
                            </li>
                            <li class="nav-item ">
                                <a href="table_datatables_colreorder.html" class="nav-link "> Colreorder Extension </a>
                            </li>
                            <li class="nav-item ">
                                <a href="table_datatables_rowreorder.html" class="nav-link "> Rowreorder Extension </a>
                            </li>
                            <li class="nav-item ">
                                <a href="table_datatables_scroller.html" class="nav-link "> Scroller Extension </a>
                            </li>
                            <li class="nav-item ">
                                <a href="table_datatables_fixedheader.html" class="nav-link "> FixedHeader Extension </a>
                            </li>
                            <li class="nav-item ">
                                <a href="table_datatables_responsive.html" class="nav-link "> Responsive Extension </a>
                            </li>
                            <li class="nav-item ">
                                <a href="table_datatables_editable.html" class="nav-link "> Editable Datatables </a>
                            </li>
                            <li class="nav-item ">
                                <a href="table_datatables_ajax.html" class="nav-link "> Ajax Datatables </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="nav-item  ">
                <a href="?p=" class="nav-link nav-toggle">
                    <i class="icon-wallet"></i>
                    <span class="title">Portlets</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="portlet_boxed.html" class="nav-link ">
                            <span class="title">Boxed Portlets</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="portlet_light.html" class="nav-link ">
                            <span class="title">Light Portlets</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="portlet_solid.html" class="nav-link ">
                            <span class="title">Solid Portlets</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="portlet_ajax.html" class="nav-link ">
                            <span class="title">Ajax Portlets</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="portlet_draggable.html" class="nav-link ">
                            <span class="title">Draggable Portlets</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-bar-chart"></i>
                    <span class="title">Charts</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="charts_amcharts.html" class="nav-link ">
                            <span class="title">amChart</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="charts_flotcharts.html" class="nav-link ">
                            <span class="title">Flot Charts</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="charts_flowchart.html" class="nav-link ">
                            <span class="title">Flow Charts</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="charts_google.html" class="nav-link ">
                            <span class="title">Google Charts</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="charts_echarts.html" class="nav-link ">
                            <span class="title">eCharts</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="charts_morris.html" class="nav-link ">
                            <span class="title">Morris Charts</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <span class="title">HighCharts</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item ">
                                <a href="charts_highcharts.html" class="nav-link "> HighCharts </a>
                            </li>
                            <li class="nav-item ">
                                <a href="charts_highstock.html" class="nav-link "> HighStock </a>
                            </li>
                            <li class="nav-item ">
                                <a href="charts_highmaps.html" class="nav-link "> HighMaps </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-pointer"></i>
                    <span class="title">Maps</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="maps_google.html" class="nav-link ">
                            <span class="title">Google Maps</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="maps_vector.html" class="nav-link ">
                            <span class="title">Vector Maps</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="heading">
                <h3 class="uppercase">Layouts</h3>
            </li>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-layers"></i>
                    <span class="title">Page Layouts</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="layout_language_bar.html" class="nav-link ">
                            <span class="title">Header Language Bar</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="layout_footer_fixed.html" class="nav-link ">
                            <span class="title">Fixed Footer</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="layout_boxed_page.html" class="nav-link ">
                            <span class="title">Boxed Page</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-feed"></i>
                    <span class="title">Sidebar Layouts</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="layout_sidebar_menu_hover.html" class="nav-link ">
                            <span class="title">Hover Sidebar Menu</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="layout_sidebar_reversed.html" class="nav-link ">
                            <span class="title">Reversed Sidebar Page</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="layout_sidebar_fixed.html" class="nav-link ">
                            <span class="title">Fixed Sidebar Layout</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="layout_sidebar_closed.html" class="nav-link ">
                            <span class="title">Closed Sidebar Layout</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class=" icon-wrench"></i>
                    <span class="title">Custom Layouts</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="layout_disabled_menu.html" class="nav-link ">
                            <span class="title">Disabled Menu Links</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="heading">
                <h3 class="uppercase">Pages</h3>
            </li>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-basket"></i>
                    <span class="title">eCommerce</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="ecommerce_index.html" class="nav-link ">
                            <i class="icon-home"></i>
                            <span class="title">Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ecommerce_orders.html" class="nav-link ">
                            <i class="icon-basket"></i>
                            <span class="title">Orders</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ecommerce_orders_view.html" class="nav-link ">
                            <i class="icon-tag"></i>
                            <span class="title">Order View</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ecommerce_products.html" class="nav-link ">
                            <i class="icon-graph"></i>
                            <span class="title">Products</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="ecommerce_products_edit.html" class="nav-link ">
                            <i class="icon-graph"></i>
                            <span class="title">Product Edit</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-docs"></i>
                    <span class="title">Apps</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="app_todo.html" class="nav-link ">
                            <i class="icon-clock"></i>
                            <span class="title">Todo 1</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="app_todo_2.html" class="nav-link ">
                            <i class="icon-check"></i>
                            <span class="title">Todo 2</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="app_inbox.html" class="nav-link ">
                            <i class="icon-envelope"></i>
                            <span class="title">Inbox</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="app_calendar.html" class="nav-link ">
                            <i class="icon-calendar"></i>
                            <span class="title">Calendar</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="app_ticket.html" class="nav-link ">
                            <i class="icon-notebook"></i>
                            <span class="title">Support</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-user"></i>
                    <span class="title">User</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="page_user_profile_1.html" class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">Profile 1</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="page_user_profile_1_account.html" class="nav-link ">
                            <i class="icon-user-female"></i>
                            <span class="title">Profile 1 Account</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="page_user_profile_1_help.html" class="nav-link ">
                            <i class="icon-user-following"></i>
                            <span class="title">Profile 1 Help</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="page_user_profile_2.html" class="nav-link ">
                            <i class="icon-users"></i>
                            <span class="title">Profile 2</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="icon-notebook"></i>
                            <span class="title">Login</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item ">
                                <a href="page_user_login_1.html" class="nav-link " target="_blank"> Login Page 1 </a>
                            </li>
                            <li class="nav-item ">
                                <a href="page_user_login_2.html" class="nav-link " target="_blank"> Login Page 2 </a>
                            </li>
                            <li class="nav-item ">
                                <a href="page_user_login_3.html" class="nav-link " target="_blank"> Login Page 3 </a>
                            </li>
                            <li class="nav-item ">
                                <a href="page_user_login_4.html" class="nav-link " target="_blank"> Login Page 4 </a>
                            </li>
                            <li class="nav-item ">
                                <a href="page_user_login_5.html" class="nav-link " target="_blank"> Login Page 5 </a>
                            </li>
                            <li class="nav-item ">
                                <a href="page_user_login_6.html" class="nav-link " target="_blank"> Login Page 6 </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item  ">
                        <a href="page_user_lock_1.html" class="nav-link " target="_blank">
                            <i class="icon-lock"></i>
                            <span class="title">Lock Screen 1</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="page_user_lock_2.html" class="nav-link " target="_blank">
                            <i class="icon-lock-open"></i>
                            <span class="title">Lock Screen 2</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-social-dribbble"></i>
                    <span class="title">General</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="page_general_about.html" class="nav-link ">
                            <i class="icon-info"></i>
                            <span class="title">About</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="page_general_contact.html" class="nav-link ">
                            <i class="icon-call-end"></i>
                            <span class="title">Contact</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="icon-notebook"></i>
                            <span class="title">Portfolio</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item ">
                                <a href="page_general_portfolio_1.html" class="nav-link "> Portfolio 1 </a>
                            </li>
                            <li class="nav-item ">
                                <a href="page_general_portfolio_2.html" class="nav-link "> Portfolio 2 </a>
                            </li>
                            <li class="nav-item ">
                                <a href="page_general_portfolio_3.html" class="nav-link "> Portfolio 3 </a>
                            </li>
                            <li class="nav-item ">
                                <a href="page_general_portfolio_4.html" class="nav-link "> Portfolio 4 </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item  ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="icon-magnifier"></i>
                            <span class="title">Search</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item ">
                                <a href="page_general_search.html" class="nav-link "> Search 1 </a>
                            </li>
                            <li class="nav-item ">
                                <a href="page_general_search_2.html" class="nav-link "> Search 2 </a>
                            </li>
                            <li class="nav-item ">
                                <a href="page_general_search_3.html" class="nav-link "> Search 3 </a>
                            </li>
                            <li class="nav-item ">
                                <a href="page_general_search_4.html" class="nav-link "> Search 4 </a>
                            </li>
                            <li class="nav-item ">
                                <a href="page_general_search_5.html" class="nav-link "> Search 5 </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item  ">
                        <a href="page_general_pricing.html" class="nav-link ">
                            <i class="icon-tag"></i>
                            <span class="title">Pricing</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="page_general_faq.html" class="nav-link ">
                            <i class="icon-wrench"></i>
                            <span class="title">FAQ</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="page_general_blog.html" class="nav-link ">
                            <i class="icon-pencil"></i>
                            <span class="title">Blog</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="page_general_blog_post.html" class="nav-link ">
                            <i class="icon-note"></i>
                            <span class="title">Blog Post</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="page_general_invoice.html" class="nav-link ">
                            <i class="icon-envelope"></i>
                            <span class="title">Invoice</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="page_general_invoice_2.html" class="nav-link ">
                            <i class="icon-envelope"></i>
                            <span class="title">Invoice 2</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-settings"></i>
                    <span class="title">System</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="page_system_coming_soon.html" class="nav-link " target="_blank">
                            <span class="title">Coming Soon</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="page_system_404_1.html" class="nav-link ">
                            <span class="title">404 Page 1</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="page_system_404_2.html" class="nav-link " target="_blank">
                            <span class="title">404 Page 2</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="page_system_404_3.html" class="nav-link " target="_blank">
                            <span class="title">404 Page 3</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="page_system_500_1.html" class="nav-link ">
                            <span class="title">500 Page 1</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="page_system_500_2.html" class="nav-link " target="_blank">
                            <span class="title">500 Page 2</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-folder"></i>
                    <span class="title">Multi Level Menu</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="icon-settings"></i> Item 1
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item">
                                <a href="?p=dashboard-2" target="_blank" class="nav-link">
                                    <i class="icon-user"></i> Arrow Toggle
                                    <span class="arrow nav-toggle"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            <i class="icon-power"></i> Sample Link 1</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            <i class="icon-paper-plane"></i> Sample Link 1</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">
                                            <i class="icon-star"></i> Sample Link 1</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="icon-camera"></i> Sample Link 1</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="icon-link"></i> Sample Link 2</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="icon-pointer"></i> Sample Link 3</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="?p=dashboard-2" target="_blank" class="nav-link">
                            <i class="icon-globe"></i> Arrow Toggle
                            <span class="arrow nav-toggle"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="icon-tag"></i> Sample Link 1</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="icon-pencil"></i> Sample Link 1</a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="icon-graph"></i> Sample Link 1</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="icon-bar-chart"></i> Item 3 </a>
                    </li>
                </ul>
            </li>
            -->
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->
