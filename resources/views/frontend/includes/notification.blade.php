<a href="javascript:void(0);" class="dropdown-toggle chat-bell setChatBell" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
    <i class="icon-bell setICONBELL"></i>
    <span class="badge badge-info setSpnBg" id="unread-msg-counter">{{ $pendingNotification }}</span>
</a>
{{--<i class="up setUPARROW"></i>--}}
<ul class="dropdown-menu notificationsList setHNFLDM">
    <li class="external setExternal">
        <h3>
            @if( $pendingNotification >= 1 )
                {!! trans('common.count_new_notification', ['count' => $pendingNotification]) !!}
            @else
                {{ trans('common.no_new_notification') }}
            @endif
        </h3>
        <a class="setEXLINK" href="{{ route('frontend.inbox' , \Session::get('lang')) }}">{{ trans('common.view_all') }}</a>
    </li>
    <li>
        <ul class="dropdown-menu-list scroll-div notification-box setDDMLIST" style="height: 250px;" data-handle-color="#637283">
            @foreach($userInboxMessages as $key => $userInboxMessage )
            <li class="setHHHO">
                @php
                    $is_read = '';
                    if($userInboxMessage->is_read == 1){
                        $is_read = 'is_read';
                    }
                @endphp

                <a class="setISREAD {{ $is_read }}" href="{{ $userInboxMessage->getHref() }}" style="border-bottom: 1px solid #e4e4e4 !important;">
                    <div class="row">
                        <span class="photo col-md-2">
                            @if( $userInboxMessage->fromUser == null )
                                <img src="/assets/layouts/layout3/img/system-admin-notification-512-new.png" class="img-circle setSPNINERIM" alt="" width="45px" height="45px">
                            @else
                                <img src="{{ $userInboxMessage->fromUser->getAvatar() }}" class="img-circle setSPNINERIM" alt="" width="45px" height="45px">
                            @endif
                        </span>
                        <span class="details col-md-10">
                            <span class="time setTIME">
                                {{ $userInboxMessage->getCreatedDate() }}
                            </span>
                            @if( $userInboxMessage->fromUser == null )
                                <span class="setAdmin">Admin</span>
                                <br/>
                            @else
                                <span class="setSPanNotiFICATION setSPnV">{{ $userInboxMessage->fromUser->getName() }}</span>
                                <br/>
                            @endif

                            <span class="setSPanNoti setSPnV1">{!! $userInboxMessage->getTitle() !!}</span>

                         </span>
                    </div>
                </a>
            </li>
            @endforeach
            <li style="position: absolute;
    width: 100%;
    bottom: 0;
    padding: 0px !important;
    margin:0px !important;
">
                <a href="{{ route('markAllRead',[\Session::get('lang')]) }}" class="markAllRead markAllRead2" style="
                color:#6AA3CE !important;">
                    Mark all as read
                </a>
            </li>
        </ul>
    </li>
</ul>
