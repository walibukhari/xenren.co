@extends('frontend.includes.ajaxmodal')

@section('title')
    {{ trans('common.send_offer_to', [ 'who' => $user->getName() ]) }}
@endsection

@section('script')
    <script>
        +function () {
            $(document).ready(function () {
                $('#send-offer-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btnSubmit',
                    alertContainer : '#model-alert-container',
                    afterSuccessFunction: function (response, $el, $this) {
                        if( response.status == 'OK'){
                            alertSuccess('Offer Send Successfully');
                            $('#modalSendOffer').modal('toggle');
                        }else{
                            alertError(response.message);
                            App.alert({
                                type: 'danger',
                                icon: 'warning',
                                message: response.message,
                                container: '#model-alert-container',
                                reset: true,
                                focus: true
                            });
                        }

                        App.unblockUI('#send-offer-form');
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <div class="pull-left">
        <button id="btnSubmit" class="btn btn-green-bg-white-text btn-send-invitation">
            {{ trans('common.send_offer')}}
        </button>

        <a data-dismiss="modal">
            <span class="cancel">{{ trans('common.cancel') }}</span>
        </a>
    </div>
@endsection

@section('content')
    {!! Form::open(['route' => 'frontend.findexperts.sendoffer.post', 'method' => 'post', 'role' => 'form', 'id' => 'send-offer-form']) !!}
    <div class="form-body">
        <div class="form-group">
            <div id="model-alert-container">
            </div>
        </div>

        <input type="hidden" name="receiverId" value="{{ $user->id }}">

        <div class="form-group">
            <label class="bold m-b-20">{{ trans('common.job_type') }}</label>
            <div class="m-b-20">
                <div class="radio-item">
                    <input type="radio" id="ritema" name="pay_type" value="1" checked>
                    <label for="ritema">
                        <span class="radio-labe1">{{ trans('common.hourly') }}</span>
                    </label>
                </div>
                <br/>
                <span class="description">{{ trans('common.pay_by_hour_and_verify') }}</span>
            </div>
            <div class="m-b-30">
                <div class="radio-item">
                    <input type="radio" id="ritemb" name="pay_type" value="2">
                    <label for="ritemb">
                        <span class="radio-labe1">{{ trans('common.fixed_price') }}</span>
                    </label>
                </div>
                <br/>
                <span class="description">{{ trans('common.pay_by_the_milestone') }}</span>
            </div>
        </div>

        <div class="form-group">
            <label class="bold">{{ trans('common.require_daily_update') }}</label>
            <div class="">
                <div class="radio-item">
                    <input type="radio" id="daily_update_chkBox" name="daily_update" value="1" checked>
                    <label for="daily_update_chkBox">
                        <span class="radio-labe1">{{trans('common.yes')}}</span>
                    </label>
                </div>

                <div class="radio-item">
                    <input type="radio" id="daily_update_chkBox_1" name="daily_update" value="0">
                    <label for="daily_update_chkBox_1">
                        <span class="radio-labe1">{{trans('common.no')}}</span>
                    </label>
                </div>

                <br/>
            </div>
        </div>

        <div class="form-group">
            <label class="bold">{{ trans('common.amount') }} ( {{ app()->getLocale() == "cn"? trans('common.cny'): trans('common.usd') }} ) </label>
            <input type="text" class="form-control amount" name="amount">
        </div>

        <div class="form-group m-b-20">
            <label class="bold">{{ trans('common.link_offer_to_job_post') }}</label>
            <div class="form-group search-selectbox">
                @if( $projects != null )
                    <select id="slcProjectId" class="bs-select form-control" name="projectId">
                        <option value="0" >{{ trans('common.select_job_post')  }}</option>
                        @foreach ( $projects as $key => $project)
                            <option value="{{ $project->id }}" >{{ $project->name }}</option>
                        @endforeach
                    </select>
                @endif
            </div>
        </div>

        <div class="form-group">
            <a href="{{ route('frontend.postproject') }}" target="_blank">
                <span class="create-project">{{ trans('common.create_new_job_and_invite') }}</span>
            </a>
        </div>

        <div class="form-group">
            <label class="bold">{{ trans('common.write_message_to_freelancer') }}</label>
            <textarea name="senderMessage" class="form-control" cols="50" rows="10">{!! trans('common.example_message_to_freelancer') !!} </textarea>
        </div>

        <div class="form-group col-md-12" style="padding:unset;">
            <hr>
        </div>
    </div>
    {!! Form::close() !!}
@endsection


