<!-- BEGIN FOOTER -->
    <footer class="fixed-bottom"
    style="
    position: absolute;
    width: 100%;
    display: flex;
    margin: 0px !important;
    left: 0px;"
    >
    <div class="container">
      <p>&copy; 2017 - 2020 {{trans('home.all_reversed')}}</p>
      <ul class="list-inline">
        <li class="list-inline-item">
          <a href="{{ route('frontend.about-us', \Session::get('lang') ) }}">{{ trans('home.about_us') }}</a>
        </li>
        <li class="list-inline-item">
          <a href="{{ route('frontend.contact-us', \Session::get('lang') ) }}">{{ trans('home.contact_us') }}</a>
        </li>
        <li class="list-inline-item">
          <a href="{{ route('home', \Session::get('lang') ) }}">{{ trans('home.landing_page') }}</a>
        </li>
        <li class="list-inline-item">
          <a href="{{ route('frontend.hiring-page', \Session::get('lang') ) }}">{{ trans('home.hiring_notice') }}</a>
        </li>
        <li class="list-inline-item">
          <a href="{{ route('frontend.terms-service', \Session::get('lang') ) }}">{{ trans('home.tnc') }}</a>
        </li>
        <li class="list-inline-item">
          <a href="{{ route('frontend.privacy-policy', \Session::get('lang') ) }}">{{ trans('home.privacy_policy') }}</a>
        </li>
        <li class="list-inline-item">
          <a href="http://ditu.amap.com/place/B0FFFA23H5">{{ trans('home.location') }}</a>
        </li>
      </ul>
    </div>
  </footer>
    @if (isset($_G['route_name']) && $_G['route_name'] == 'home')
    <div class="row col-md-12 div-btnCreateOrder">
        <a href="{{ route('frontend.postproject') }}" class="btn btn-lg btnOrange">
            {{ trans('member.create_project_now') }}<i class="fa fa-plus"></i>
        </a>
    </div>
    @endif
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>


