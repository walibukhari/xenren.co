 @if ($paginator->lastPage() > 1)
    <div class="pull-right">
        @php
            $links = $paginator->appends(request()->except('page'))->links();
        @endphp
        {!! str_replace('%2C', ',', $links) !!}
    </div>
 @endif
