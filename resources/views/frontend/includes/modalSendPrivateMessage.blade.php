@extends('ajaxmodal')

@section('title')
    {{trans("common.send_message")}}
@endsection

@section('script')
    <script>
        +function () {
            $(document).ready(function () {
                $('#send-message-form').makeAjaxForm({
                    inModal: true,
                    closeModal: false,
                    submitBtn: '#btnSubmit'
                });
            });

            //scroll box
            $('.inbox-message-section').slimScroll({
                height: '300px',
                start: 'bottom'
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <button type="button" id="btnSubmit" class="btn green sendMessage">{{ trans('common.send_message') }}</button>
@endsection

@section('content')
    <div id="privateMessage">
        @if( $inboxMessages )
        <div class="inbox-message-section">
            @include('frontend.inbox.inboxMessageItem')
        </div>
        <br/>
        @endif

        {!! Form::open(['route' => ['frontend.sendprivatemessage.post', 'user_id' => $user->id], 'method' => 'post', 'role' => 'form', 'id' => 'send-message-form']) !!}
        <div class="form-body">

            <div class="row">
                <div class="col-md-12">
                    <label>{{ trans('common.message') }}</label>
                    <div class="input-group">
                        {{ Form::textarea('message', null, ['class' => 'form-control', 'cols' => '100', 'rows' => '3', 'id' => 'msg']) }}
                    </div>
                </div>
            </div>

        </div>
        <input type="hidden" name="user_inbox_id" value="{{ isset($userInbox->id) ? $userInbox->id :'0'  }}">
        {!! Form::close() !!}
    </div>
@endsection

@section('modal')

@endsection