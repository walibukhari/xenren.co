<!--[if lt IE 9]>
<script src="/assets/global/plugins/respond.min.js"></script>
<script src="/assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<?php
    $currentPageURL = URL::current();
    $pageArray = explode('/', $currentPageURL);
    $pageActive = isset($pageArray[3]) ? $pageArray[3] : '/';
?>
{{--<script src="/assets/global/plugins/jquery.min.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>--}}
@if($pageActive != 'identityAuthentication')
{{--<script src="/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>--}}
@endif
{{--<script src="/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/magicsuggest/magicsuggest-min.js" type="text/javascript"></script>--}}
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
{{--<script src="/assets/global/plugins/moment.min.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>--}}
{{--<script src="/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>--}}
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-99673167-1', 'auto');
    ga('send', 'pageview');

    var g_lang = {
        success : "{{ trans('common.success') }}",
        error : "{{ trans('common.error')  }}",
        notice : "{{ trans('common.notice')  }}",
        search : "{{ trans('common.search')  }}"
    };

    var headerSearchSkillList = {!! $_G['headerSearchSkillList'] !!};

    var g_url = {
        'user_center' : '{{ route('frontend.usercenter') }}',
        {{--'weixinweb': '{{ route('weixin.login') }}',--}}
        'get_new_notification' : '{{ route('frontend.getnewnotification.post') }}',
    }

    var is_guest = {{ Auth::guard('users')->guest() == 1? 'true': 'false' }};
    // window.onload = function()
    // {
        // if(isWeiXin() && is_guest == true)
        // {
        //     window.location.href = g_url.weixinweb;
        // }
    // }

    function isWeiXin()
    {
        var ua = window.navigator.userAgent.toLowerCase();
        if(ua.match(/MicroMessenger/i) == 'micromessenger')
        {
            return true;
        }
        else
        {
            return false;
        }
    }
</script>
{{--<script src="/js/ajaxform.min.js" type="text/javascript"></script>--}}
<script src="{{asset('js/innerMinifiedHome.js')}}" type="text/javascript"></script>
<script src="{{asset('js/app.js')}}" type="text/javascript"></script>
{{--<script src="/js/global.js" type="text/javascript"></script>--}}
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
{{--<script src="/assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>--}}
{{--<script src="/assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>--}}
{{--<script src="/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>--}}
<!-- END THEME LAYOUT SCRIPTS -->
<!-- chat module -->
{{--<script src="/custom/js/frontend/myChatPullRequest.js" type="text/javascript"></script>--}}
<script src="/custom/js/frontend.js?lm=150820170817" type="text/javascript"></script>


<!--To fix ajax token mismatch problem -->
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
@if (Auth::user())
    <script type="text/javascript">
        var notification;
        //refresh the inbox notification every 5 sec and retrieve new notification
        function refresh_notification() {
            notification = window.setInterval(function(){
                $.ajax({
                    url: "{{ route('frontend.getnewnotification.post') }}",
                    dataType: 'json',
                    data: {},
                    method: 'POST',
                    success: function (response) {
                        if(response.status == 'success') {
                            if( response.result != '' )
                            {
                                $('#header_notification_bar').html(response.result);
                                $('.scroller').slimscroll({});
                            }
                        }
                    }
                });
            }, 5000);
        }
//        refresh_notification();



        $(".update_notification").hover(
           function () {
                //clear refresh notification
                clearInterval(notification);

                //update notification to is_read
                $.ajax({
                    url: "{{ route('frontend.updatenotificationtoisread.post') }}",
                    data: {},
                    method: 'POST',
                    success: function (response) {


                        $("#unread-msg-counter").text("0");
                        // alert(response);
                    }
                });

                // //append old notification when scroll to bottom
                // var $scrollable = $('.scroller');
                // $scrollable.slimScroll().bind('slimscrolling', function (e, pos) {
                //     if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                //         var offset = $(".notification-box li").length;
                //         $.ajax({
                {{-- //             url: "{{ route('frontend.getoldnotification.post') }}", --}}
                //             // dataType: 'json',
                //             data: {
                //                 offset : offset
                //             },
                //             method: 'POST',
                //             success: function (response) {
                //                 $.each(response.result, function(index, value){
                //                     $(".notification-box").append('<li>'+
                //                             '<a class="is_read" href="" style="border-bottom: 1px solid #e4e4e4 !important;">'+
                //                                 '<div class="row">'+
                //                                     '<span class="photo col-md-2">'+
                //                                         '<img src=" '+ value.getAvatar +'" class="img-circle" alt="" width="45px" height="45px">'+
                //                                     '</span>'+
                //                                     '<span class="details col-md-10">'+
                //                                         '<span class="time">'+
                //                                             value.getCreatedDate +
                //                                         '</span>'+
                //                                             value.getName + '<br/>' +  value.getTitle +
                //                                      '</span>'+
                //                                 '</div>'+
                //                             '</a>'+
                //                         '</li>');
                //                 });
                //             }
                //         });
                //     }
                //     return false;
                // })
           },
           function () {
//                refresh_notification();
           }
        );
    </script>
@endif
