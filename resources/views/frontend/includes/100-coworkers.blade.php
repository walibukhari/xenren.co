
    <div class="set-container-100-coworker navbar-fixed-top" id="myHeader" style="{{$bothSideMargin}}">
        <div class="custom-container">
            <div class="row">
                @guest
                <div class="col-md-12 col-xl-12 col-lg-12">
                        <div class="download-100-coworkers-url" onclick="window.open('https://xenren.app.link/100Coworkers')">
                            <p><img src="{{asset('/images/bellIconImage.png')}}"> Download app to get mobile notice!</p>
                        </div>
                        <div class="video-area" data-toggle="modal" data-target="#myVideoPopup">
                            <img src="{{asset('images/film-action.png')}}">
                        </div>
                    </div>
                @endguest
            </div>
        </div>
    </div>
{{-- Video Modal --}}
<div id="myVideoPopup" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            {{--<i class="fa fa-times fa-time-close"></i>--}}
            <img class="fa-time-close" src="{{asset('images/cross-times.png')}}">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <video width="100%" height="100%" controls autoplay id="homePageVideo">
                                <source src="{{ asset('video/xenren_home.mp4') }}" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- SELECT2 Script for city multi select in facebook binding modal --}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    $(function(){
        if (window.matchMedia("(max-width: 991px)").matches) {
            window.onscroll = function() {myFunction()};

            var header = document.getElementById("myHeader");
            var sticky = header.offsetTop;

            function myFunction() {
                if (window.pageYOffset > sticky) {
                    header.classList.add("sticky");
                } else {
                    header.classList.remove("sticky");
                }
            }
        }

        $('.fa-time-close').click(function(){
            $('#myVideoPopup').modal('hide');
        });
    });
</script>
