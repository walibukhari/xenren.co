<div class="row project-conversation" data-url="{{ route('frontend.officialprojectchat.pullprojectchat',$officialProjectChatRoomId)}}" data-urlfile="{{ route('frontend.officialprojectchat.pullprojectchatfile',$officialProjectId)}}">
    <div class="col-md-12 clearfix">
        <div class="portlet box gray-color">
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <div class="scroller" id="content-chat" style="height: 525px" data-always-visible="1" data-rail-visible="0">
                    <center><h3>chat content ...</h3></center>

                </div>
                <div class="chat-form">
                    <div style="color:#ff0000" class="error-msg"></div>
                    <form action="{{ route('frontend.officialprojectchat.postprojectchat') }}" method="post" class="">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <input type="hidden" name="officialProjectChatRoomId" id="officialProjectChatRoomId" value="{{$officialProjectChatRoomId}}">
                        
                        <div class="row chat-form-main">
                            <div class="col-md-1 chat-form-l">
                                <!-- <div class="upload-file upload-btn-custom">
                                    <a href="javascript:;" id="chat-message-file" data-url='{{ route('frontend.officialprojectchat.postprojectchatimage') }}'>
                                        <i class="fa fa-folder-open"></i>
                                    </a>
                                </div> -->
                                <div class="upload-picture upload-btn-custom">
                                    <a href="javascript:;" id="chat-message-img" data-url='{{ route('frontend.officialprojectchat.postprojectchatimage') }}'>
                                        <i class="fa fa-paperclip" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <div class="upload-btn-custom">
                                    <a href="javascript:;">
                                        <i class="fa fa-cog" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-11 chat-form-r">
                                <div class="input-cont">
                                    <input class="form-control message" name="message" id="message" type="text" placeholder="{{ trans('member.leave_message') }}..." />
                                    <button type="submit" class="btn btn-submit">
                                        <i class="fa fa-send" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        
                    </form>
                </div>
                <!-- END FORM-->
            </div>
        </div>
    </div>
</div>
<!-- <button type="button" class="btn blue btn-helpdesk" id="invite-helpdesk" data-url="{{ route('frontend.officialprojectchat.postinvitehelpdeskchat',$officialProjectChatRoomId) }}">{{ trans('member.contact_helpdesk') }}</button> -->

<!-- Modal -->
<div id="modalAddFileChat" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            {!! Form::open(['route' => 'frontend.officialprojectchat.postprojectchatfile', 'id' => 'add-project-file-chat', 'method' => 'post', 'class' => 'form-horizontal', 'files' => true]) !!}
                <input type="hidden" name="officialProjectChatRoomId" value="{{$officialProjectChatRoomId}}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{ trans('member.add_file') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">
                        <div class="modal-alert-container"></div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">{{ trans('member.filename') }}</label>
                            <div class="col-md-9">
                                {!! Form::text('message', '', ['class' => 'form-control', 'placeholder' => 'Message']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">{{ trans('member.file') }}</label>
                            <div class="col-md-9">
                                {!! Form::file('file', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn blue" id="btn-submit-file">{{ trans('member.confirm_add') }}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('member.cancel') }}</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
