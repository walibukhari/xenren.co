<!-- Modal -->
<style>
    /*.modal-backdrop, .modal-backdrop.fade.in{*/
    /*    display: none !important;*/
    /*}*/
</style>
<div id="modelAddNewSkillKeyword" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content set-modal-skill-keyword" style="margin-top:30px;border-radius: 2px !important;">
            <div id="body-alert-container"></div>
            {!! Form::open(['route' => 'frontend.postproject.addNewSkillKeyword', 'id' => 'add-new-skill-keyword-form', 'method' => 'post', 'class' => 'form-horizontal']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"></button>
                    <center>
                        <div class="modal-title f-s-26">
                            {{ trans('common.new_skill_keywords') }}
                        </div>
                        <div class="f-s-16">
                            {{ trans('common.please_enter_keyword_and_help_us_improve') }}
                        </div>
                    </center>
                </div>
                <div class="modal-body">
                    <div class="form-body">
                        <div class="col-md-12" id="add-new-skill-keyword-errors"></div>

                        <div class="add-more-section">
                            <div class="row">
                                <label class="col-md-12">
                                    {{ trans('common.new_skill_keywords') }}
                                </label>
                                <div class="col-md-12">
                                    {!! form::text('tbxSkillKeyword[]', '', [
                                        'class' => 'form-control'
                                    ] ) !!}
                                </div>
                            </div>

                            <div class="row">
                                <label class="col-md-12 p-t-20">
                                    {{ trans('common.please_use_wiki_or_baidu_support_your_keyword') }}
                                </label>
                                <div class="col-md-12">
                                    {!! form::text('tbxReferenceUrl[]', '', [
                                        'class' => 'form-control'
                                    ] ) !!}
                                </div>
                            </div>

                        </div>

{{--                        <div class="row">--}}
{{--                            <label class="col-md-12 p-t-20">--}}
{{--                                {!! form::button(trans('common.add_more'), ['class' => 'btn btn-success m-b-0', 'id' => 'btnAddMore' ]) !!}--}}
{{--                            </label>--}}
{{--                            <input type="hidden" id="hidInputCount" name="hidInputCount" value="1">--}}
{{--                        </div>--}}
                    </div>
                </div>
                <div class="modal-footer">
                    @if(!\Auth::check())
                        <button type="button" class="btn btn-success m-b-0 f-s-14" onclick="window.location.href='{{\Session::get('lang')}}/login'">
                            {{ trans('common.submit') }}
                        </button>
                    @else
                        <button id="btn-submit-new-skill-keyword" type="button" class="btn btn-success m-b-0 f-s-14" data-submit-url="{{ route('frontend.postproject.addNewSkillKeyword') }}">
                            {{ trans('common.submit') }}
                        </button>
                    @endif
                </div>
                {!! form::hidden('locale', App::getLocale() ) !!}
                {!! form::hidden('tag_color', $tagColor ) !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>

