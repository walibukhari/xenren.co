<!-- Modal -->
<div id="modalMakeOffer" class="modal fade apply-job-modal" role="dialog">

    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            {!! Form::open(['route' => ['frontend.jobs.makeoffer.post'], 'method' => 'post', 'role' => 'form', 'id' => 'make-offer-form']) !!}
                <div class="modal-header">
                    <button type="button" class="btn pull-right" data-dismiss="modal">
                        <i class="fa fa-close" aria-hidden="true"></i>
                    </button>
                    <h4 class="modal-title"> {{trans('jobs.apply_job')}} </h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">
                        <div class="form-group">
                            <div id="model-alert-container">
                            </div>
                        </div>

                        <div class="col-md-7" style="padding:0px">
                            <div class="form-group">
                                <label>{{ trans('common.price') }}</label>
                                {!! Form::text('price', isset($user->hourly_pay)? $user->hourly_pay: '' , ['class' => 'form-control', 'id' => 'price']) !!}
                                <input type="hidden" id="hourly_pay" value="{{ isset($user->hourly_pay)? $user->hourly_pay: '' }}">
                            </div>
                        </div>

                        <div class="col-md-5" style="padding-right: 0px;">
                            <div class="form-group">
                                <label>{{ trans('common.pay_type') }}</label><br/>
                                <input type="checkbox"
                                    class="make-switch form-control"
                                    name="ckbPayType"
                                    id="ckbPayType"
                                    data-size="switch-size"
                                    data-on-text="{{ trans('common.pay_hourly') }}" data-off-text="{{ trans('common.fixed_price') }}"
                                    data-on-color="primary" data-off-color="success"
                                    value="false"
                                    checked >
                                <input type="hidden" id="pay_type" name="pay_type" value="1">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>{{ trans('common.about_me') }}</label>
                            {!! Form::textarea('about_me', isset($user->about_me)? $user->about_me: '' , ['class' => 'form-control']) !!}
                        </div>

                        <div class="question-section">
                            <!--dynamic generate the question by javascript-->
                        </div>

                        <div class="form-group content-group">
                            <label>{{ trans('common.contact') }}</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <img src="/images/Tencent_QQ.png">
                                        </span>
                                        {!! Form::text('qq_id', '', ['class' => 'form-control text-center', 'id' => 'tbxQQId', 'placeholder' => 'QQ' ]) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <img src="/images/skype-icon-5.png">
                                        </span>
                                        {!! Form::text('skype_id', '', ['class' => 'form-control text-center', 'id' => 'tbxSkypeId', 'placeholder' => 'Skype']) !!}
                                    </div>
                                </div>
                                <br>
                                <br>
                                <br>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <img src="/images/wechat-logo.png">
                                        </span>
                                        {!! Form::text('wechat_id', '', ['class' => 'form-control text-center', 'id' => 'tbxWechatId', 'placeholder' => 'Wechat']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <img src="/images/MetroUI_Phone.png">
                                        </span>
                                        {!! Form::text('handphone_no', '', ['class' => 'form-control text-center', 'id' => 'tbxHandphoneNo', 'placeholder' => 'Phone']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-body text-center">
                        <button type="button" class="btn btn-submit"
                                id="btn-submit">{{trans('jobs.make_offer')}}</button>
                    </div>
                </div>
                {!! Form::hidden('project_id', '', ['class' => 'modal-project-id']) !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>