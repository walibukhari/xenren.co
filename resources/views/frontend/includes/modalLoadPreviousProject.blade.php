<!-- Modal -->
<div id="modalLoadPreviousProject" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            {!! Form::open(['route' => 'frontend.postproject', 'id' => 'load-project-form', 'method' => 'post', 'class' => 'form-horizontal']) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{ trans('common.load_previous_job_post') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">
                        <div class="form-group">
                            {{ trans('common.are_you_sure_load_selected_project') }}<br/>
                            {{ trans('common.current_changes_would_be_overwritten') }}<br/><br/>

                            {{ trans('common.project_id') }} : {!! Form::label('modal_project_id', '',['id'=>'modal_project_id']) !!}<br/>
                            {{ trans('common.project_name') }} : {!! Form::label('modal_project_name', '',['id'=>'modal_project_name']) !!}<br/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn blue" id="btn_modal_load">{{ trans('common.load') }}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.cancel') }}</button>
                    {{ Form::hidden('modal_pre_project_id', '0', ['id'=>'modal_pre_project_id']) }}
                    {{ Form::hidden('modal_pre_project_name', '0', ['id'=>'modal_pre_project_name']) }}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>