@extends('frontend.layouts.default')

@section('title')
@endsection

@section('keywords')
@endsection

@section('description')
@endsection

@section('author')
@endsection

@section('header')
    <link href="/custom/css/frontend/evidence.css" rel="stylesheet" type="text/css" />
@endsection

@section('customStyle')
    <style type="text/css">
        .page-content-wrapper{
            margin-top:15px;
        }
        .portlet.light .portlet-body{
            padding:0px 15px;
        }
        .page-content-wrapper .page-content{
            margin:0px;
            padding:0px;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <span class="find-experts-search-title text-uppercase">EVIDENCE</span>
        </div>
    </div>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="portlet light bordered">
                        <div class="portlet-body">
                            <div class="row evidence-main-part">
                                <div class="col-md-12 evidence-title-section text-center">
                                    <img src="/images/evidenceTitleImage.png">
                                    <h1>
                                        PLEASE UPLOAD ALL YOUR EVIDENCE TO <br>
                                        DEFEND YOUR RIGHT.
                                    </h1>
                                </div>
                                <div class="col-md-12 evidence-box-main p-0">
                                    <div class="row">
                                        <div class="col-md-4 evidence-box-section text-center">
                                            <div class="evidence-box">
                                                <p>Amount in dispute</p>
                                                <span>${{$amount}}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 evidence-box-section text-center">
                                            <div class="evidence-box">
                                                <p>Project</p>
                                                <span>{{$projectName}}</span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 evidence-box-section text-center">
                                            <div class="evidence-box">
                                                <p>Due Date</p>
                                                <span>{{$dueDate}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 evidence-admin-section plr-0">
                                    <div class="row">
                                        <div class="col-md-2 evidence-admin">
                                            <div class="text-center">
                                                <img src="/images/svster.png" class="img-circle">
                                                <span class="label label-success admin-label">ADMIN</span>
                                            </div>
                                        </div>
                                        <div class="col-md-10 evidence-description-section pl-0">
                                            <p>Reason of making this final judgment</p>
                                            <span>{{isset($adminComment) && $adminComment ? $adminComment : 'Application still under review'}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 evidence-user-description-section-main">
                                    <div class="row">
                                        <div class="col-md-2 evidence-user-section">
                                            <img src="{{$applicant->img_avatar}}" onerror="this.src='/images/avatar_xenren.png'">

                                        </div>
                                        <div class="col-md-10 evidence-user-description-section pl-0">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-5 evidence-user-description-left">
                                                            <h2>{{$applicant->real_name}}</h2>
                                                            {{--<p class="label label-success user-label">Released To Plaintiff</p>--}}
                                                        </div>
                                                        <div class="col-md-7 evidence-user-description-left text-right">
                                                            <p class="evidence p-0 m-0">
                                                                <span class="update-date">Last update : {!! isset($applicantComment) && count($applicantComment) > 0 ? \Carbon\Carbon::parse($applicantComment->created_at)->toDateString() : 'N/A' !!}</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 user-discription">
                                                    <span>{!! isset($applicantComment) && count($applicantComment) > 0 ? $applicantComment->comment : 'N/A' !!}</span>
                                                </div>
                                                <div class="col-md-12 upload-box-section">
                                                    @foreach($applicantAttachments as $k => $v)
                                                        <div class="upload-box">
                                                            <i class="fa fa-search searchFa" style="cursor: pointer" aria-hidden="true"  onclick="window.open('{{asset('uploads/disputes/'.$v->attachment)}}')">
                                                                <img src="{{asset('uploads/disputes/'.$v->attachment)}}" style="cursor: pointer" class="setImageDisputeFileE">
                                                            </i>
                                                            @if(\Auth::user()->id == $applicant->id)
                                                                <i class="fa fa-trash removeAttachment" data-disputeid={{$detail->id}} data-attachmentid={{$v->id}} aria-hidden="true" style="color: red; cursor: pointer" onclick=";event.preventDefault();"></i>
                                                            @endif
                                                        </div>
                                                    @endforeach
                                                </div>
                                                @if(\Auth::user()->id == $applicant->id)
                                                    <div class="col-md-12 download-btn">
                                                        {{--<button class="btn btn-color-custom">Upload</button>--}}
                                                        <button class="btn btn-color-non-custom comment" data-disputeid={{$detail->id}} data-userid={{$applicant->id}}>Edit</button>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 evidence-user-description-section-main">
                                    <div class="row">
                                        <div class="col-md-2 evidence-user-section">
                                            <img src="{{$employer->img_avatar}}" onerror="this.src='/images/avatar_xenren.png'">
                                        </div>
                                        <div class="col-md-10 evidence-user-description-section pl-0">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-5 evidence-user-description-left">
                                                            <h2 class="second-user-title">{{$employer->name}}</h2>
                                                        </div>
                                                        <div class="col-md-7 evidence-user-description-left text-right">
                                                            <p class="evidence p-0 m-0">
                                                                <span class="update-date">Last update : {!! isset($employerComment) && count($employerComment) > 0 ? \Carbon\Carbon::parse($employerComment->created_at)->toDateString() : 'N/A' !!}</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 user-discription">
                                                    <span>{!! isset($employerComment) && count($employerComment) > 0 ? $employerComment->comment : 'N/A' !!}</span>
                                                </div>
                                                <div class="col-md-12 upload-box-section">
                                                    @foreach($employerAttachments as $k => $v)
                                                        <div class="upload-box">
                                                            <i class="fa fa-search" style="cursor: pointer"  aria-hidden="true" onclick="window.open('{{asset('uploads/disputes/'.$v->attachment)}}')"></i>
                                                            @if(\Auth::user()->id == $employer->id)
                                                                <i class="fa fa-trash removeAttachment" data-disputeid={{$detail->id}} data-attachmentid={{$v->id}} aria-hidden="true" style="color: red; cursor: pointer" onclick=";event.preventDefault();"></i>
                                                            @endif
                                                        </div>
                                                    @endforeach

                                                </div>
                                                @if(\Auth::user()->id == $employer->id)
                                                    <div class="col-md-12 download-btn">
                                                        {{--<button class="btn btn-color-custom">Upload</button>--}}
                                                        <button class="btn btn-color-non-custom comment"  data-disputeid={{$detail->id}} data-userid={{$employer->id}}>Edit</button>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->


    <div class="modal fade addComment" id="" role="dialog">
        <div class="modal-dialog setWHMD">
            <div class="modal-content">
                <div class="modal-body confirm-box-model-contant" style="background-color: white">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <img src="/images/icons/questionMarkIcon.png">
                        </div>
                        <div class="col-md-12 text-center">
                            <h1 class="setfiledispute">File Dispute</h1>
                            <p class="textforpara">Tell us what's happening</p>
                        </div>
                        <form>
                        <div class="col-md-12 released-milestone setFDPP">
                                <div class="form-group setFormFileDisputeM">
                                    <textarea class="form-control usercomment" rows="5" id="" placeholder="Write down all reasons to file dispute"></textarea>
                                </div>
                                <div class="form-group setformgroup1">
                                    <span class="modal-label setspnfileD0">Terms & Conditions</span>
                                </div>
                                <div class="form-group setformgroup1">
                                    <span class="modal-label setspnfileD1">Screenshot of the reason to file a dispute</span>
                                </div>
                                <div class="form-group gallery setformgroup1 scroll-div">

                                </div>
                            <div class="form-group setformgroup01">
                                <div class="col-sm-3 uploadImage">
                                    <i class="fa fa-plus plusIcon" onclick="$('#screenshotEmployer').click()"></i>
                                </div>
                                <input type="file" name="screenshot" id="screenshotEmployer" style="display: none" multiple>
                            </div>
                        </div>
                        <div class="col-md-12 confirm-box-model-btn-section">
                            <div class="row">
                                <div class="col-md-12 setCol12FDP">
                                    <div class="col-md-7 setCol7Width">
                                        <button class="btn btn-success btn-green-bg-white-text btn-block updateComment setbtnbtnfiledP1" type="button">Confirm</button>
                                    </div>
                                    <div class="col-md-5 setcol5With">
                                        <button class="btn btn-success btn-white-bg-green-text btn-block setbtnbtnfiledP2" type="button"  data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('footer')
    <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <script>
        $('.scroll-div').slimScroll({});
    </script>
    <script>
			$(document).ready(function(){
				$('[data-toggle="tooltip"]').tooltip();

				$('.removeAttachment').on('click', function(){
                    var confirm = window.confirm('Are you sure you want to remove this attachment?');
                    if(confirm) {
	                    var form = new FormData();
	                    form.append("dispute_id", $(this).data('disputeid'));
	                    form.append("attachment_id", $(this).data('attachmentid'));
	                    form.append("_token", '{{csrf_token()}}');

	                    $.ajax({
		                    url: '/update-dispute-attachment',
		                    "data": form,
		                    "headers": {
			                    "Cache-Control": "no-cache"
		                    },
		                    "processData": false,
		                    "contentType": false,
		                    "mimeType": "multipart/form-data",
		                    method: 'POST',
		                    error: function () {
//			                    alert(lang.unable_to_request);
		                    },
		                    success: function (result) {
			                    result = JSON.parse(result);
			                    console.log(result);
			                    if (result.status == 'success') {
				                    alertSuccess(result.message);
				                    setTimeout(function () {
					                    window.location.reload()
				                    }, 3000)
			                    } else {
				                    alertError(result.msg);
			                    }
		                    }
	                    });
                    }
				});

				$('.updateComment').on('click', function(){
					var form = new FormData();
					var fileInput = document.getElementById('screenshotEmployer');
//alert($('.usercomment').val());
					var file = fileInput.files;
					form.append("user_id", window.userid);
					form.append("dispute_id", window.disputeid);
					form.append("_token", '{{csrf_token()}}');
					form.append("attachments_length", file.length);
					form.append("comment", $('.usercomment').val());
					for (var i=0; i<file.length; i++) {
						console.log(file[i]);
						form.append("attachment_"+i, file[i]);
					}
					console.log(form);
//					alert(1);
					$.ajax({
						url: '/update-dispute-comment',
						"data": form,
						"headers": {
							"Cache-Control": "no-cache"
						},
						"processData": false,
						"contentType": false,
						"mimeType": "multipart/form-data",
						method: 'POST',
						error: function () {
//							alert(lang.unable_to_request);
						},
						success: function (result) {
							result = JSON.parse(result);
							console.log(result);
							if(result.status == 'success') {
								alertSuccess(result.message);
								setTimeout(function(){
									window.location.reload()
								}, 3000)
							}else{
								alertError(result.message);
							}
						}
					});

				});

				$('.comment').on('click', function(){
					window.disputeid = ($(this).data('disputeid'));
					window.userid = ($(this).data('userid'));
					$('.addComment').modal('show');
				})

                    // Multiple images preview in browser
                    var imagesPreview = function(input, placeToInsertImagePreview) {

                        if (input.files) {
                            var filesAmount = input.files.length;

                            for (i = 0; i < filesAmount; i++) {
                                var reader = new FileReader();

                                reader.onload = function(event) {
                                    var html="<div class='col-sm-3 thumbImages'>" +
                                        "<img style='width: 108%; height: 80px; border-radius: 5px; margin-bottom:10px;' src='"+event.target.result+"'/>" +
                                        "</div>";
                                    $($.parseHTML(html)).prependTo(placeToInsertImagePreview);
                                }

                                reader.readAsDataURL(input.files[i]);
                            }
                        }

                    };

                    $('#screenshotEmployer').on('change', function() {
                        $('.thumbImages').remove();
                        imagesPreview(this, 'div.gallery');
                    });
			});
    </script>
@endsection




