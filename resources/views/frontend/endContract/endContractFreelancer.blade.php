@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.end_contract_who', ['who' => trans('common.freelancer') ]) }}
@endsection

@section('description')
    {{ trans('common.end_contract_who', ['who' => trans('common.freelancer') ]) }}
@endsection

@section('author')
    Xenren
@endsection

@section('header')
    <link href="/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
    <link href="/css/end-contract.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <?php
    $review = isset($project->review) ? $project->review : false;
    ?>
    <div class="row page-contant">
        <div class="col-md-12 title">
            <h3>
                {{ trans('common.end_contract_with', [
                    'who' => $who,
                    'project_name' => $projectName,
                    'pay_type'=> $payType]) }}
            </h3>
            <h4>
                {{ trans('common.share_you_experience', ['who' => trans('common.client')]) }}
            </h4>
        </div>

        <form action="{{route("frontend.endcontract.freelancer.post")}}" method="POST" class="horizontal-form" id="freelancer-form">
            {{csrf_field()}}
            <input type="hidden" name="projectId" value="{{ $project->id }}">
            <input type="hidden" name="projectApplicantId" value="{{ $projectApplicant->id }}">
            <div class="col-md-12 body-box">
                <div class="row box-title">
                    <div class="col-md-12">
                        <div class="left">
                            <img src="/images/ic_lock.png" width="17px">
                        </div>
                        <div class="right">
                            <h3>{{ trans('common.private_feedback') }}</h3>
                            <span>{{ trans('common.private_feedback_description', ['who' => trans('common.client')]) }}
                                <a href="javascript:;"> {{ trans('common.learn_more') }} </a> </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-field reason">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-5 text-right">
                                    <label>{{ trans('common.reason_for_ending_contract') }} : </label>
                                </div>
                                <div class="col-md-6">
                                    <div class="search-selectbox custom-days-selectbox">
                                        <select class="form-control" name="reason">
                                            <option value="">{{ trans('common.please_select') }}</option>
                                            @foreach( $reasons as $key => $reason )
                                                <option value="{{ $key }}"
                                                @if(isset($review) && $review && $review->reason == $key) selected @endif>{{ $reason }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 form-field likely">
                        <label for="reason"> {{ trans('common.how_likely_to_recommend', ['who' => trans('common.client')]) }} </label>
                        <?php
                        $recomendation = isset($review->recommendation) ? (int)$review->recommendation : 6;
                        ?>
                        <div class="progress">
                            <div class="one one0 success-color">
                                <label>1<input type="radio" class="recommendation" name="recommendation" value="1" @if($recomendation == 1) checked="checked" @endif></label>
                            </div>
                            <div class="one one1 success-color">
                                <label>2<input type="radio" class="recommendation" name="recommendation" value="2"  @if($recomendation == 2) checked="checked" @endif></label>
                            </div>
                            <div class="one one2 success-color">
                                <label>3<input type="radio" class="recommendation" name="recommendation" value="3"  @if($recomendation == 3) checked="checked" @endif></label>
                            </div>
                            <div class="one one3 success-color">
                                <label>4<input type="radio" class="recommendation" name="recommendation" value="4"  @if($recomendation == 4) checked="checked" @endif></label>
                            </div>
                            <div class="one one4 success-color">
                                <label>5<input type="radio" class="recommendation" name="recommendation" value="5"  @if($recomendation == 5) checked="checked" @endif></label>
                            </div>
                            <div class="one one5 success-color">
                                <label>6<input type="radio" class="recommendation" name="recommendation" value="6" @if($recomendation == 6) checked="checked" @endif></label>
                            </div>
                            <div class="one one6 success-color">
                                <label>7<input type="radio" class="recommendation" name="recommendation" value="7" @if($recomendation == 7) checked="checked" @endif></label>
                            </div>
                            <div class="one one7 success-color">
                                <label>8<input type="radio" class="recommendation" name="recommendation" value="8" @if($recomendation == 8) checked="checked" @endif></label>
                            </div>
                            <div class="one one8 success-color">
                                <label>9<input type="radio" class="recommendation" name="recommendation" value="9" @if($recomendation == 9) checked="checked" @endif></label>
                            </div>
                            <div class="one one9 success-color">
                                <label>10<input type="radio" class="recommendation" name="recommendation" value="10" @if($recomendation == 10) checked="checked" @endif></label>
                            </div>
                            <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div>
                        <span class="span1">{{ trans('common.not_at_all_likely') }}</span>
                        <span class="span2">{{ trans('common.extremely_likely') }}</span>
                    </div>

                    <div class="col-md-6 form-field rate">
                        <div class="form-group">
                            <label for="reason">{{ trans('common.rate_their_english') }} </label>

                            <div class="radio-list">
                                <?php
                                $profiency = isset($review) && $review ? $review->proficiency : 0;
                                ?>
                                @foreach( $englishProficiencies as $key => $englishProficiency )
                                    <div class="radios">
                                        <input type="radio" class="proficiency" name="english_proficiency" value="{{ $key }}" @if($key == $profiency) checked="checked" @endif>
                                        <label for="radio1">{{ $englishProficiency }}</label>
                                    </div>
                                @endforeach
                                {{--<div class="radios">--}}
                                {{--<input type="radio" name="optradio" checked="checked" id="radio2">--}}
                                {{--<label for="radio2">Acceptable</label>--}}
                                {{--</div>--}}
                                {{--<div class="radios">--}}
                                {{--<input type="radio" name="optradio" id="radio3">--}}
                                {{--<label for="radio3">Fluent</label>--}}
                                {{--</div>--}}
                                {{--<div class="radios">--}}
                                {{--<input type="radio" name="optradio" id="radio4">--}}
                                {{--<label for="radio4">I didn't speak to freelancer</label>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 body-box">
                <div class="row box-title">
                    <div class="col-md-12">
                        <div class="left">
                            <img src="/images/ic_megamike.png" width="28px">
                        </div>
                        <div class="right">
                            <h3>{{ trans('common.public_feedback') }} </h3>
                            <span>{{ trans('common.public_feedback_description', [ 'who' => trans('common.client') ]) }}
                                <a href="javascript:;"> {{ trans('common.learn_more') }} </a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-field">
                        <div class="form-group">
                            <label>{{ trans('common.feedback_to', [ 'who' => trans('common.client')]) }}:</label>
                            <div class=" ratting-custom">
                                @foreach( $rateCategories as $key => $rateCategory )
                                    @if(isset($review) && $review )
                                        @if($key == 1)
                                            <?php
                                            $val = isset($review->payment_on_time) && $review  ? $review->payment_on_time : '3';
                                            ?>
                                        @endif
                                        @if($key == 2)
                                            <?php
                                            $val = isset($review->communication) && $review  ? $review->communication : '3';
                                            ?>
                                        @endif
                                        @if($key == 1)
                                            <?php
                                            $val = isset($review->availability) && $review  ? $review->availability : '3';
                                            ?>
                                        @endif
                                        @if($key == 1)
                                            <?php
                                            $val = isset($review->trustworthy) && $review  ? $review->trustworthy : '3';
                                            ?>
                                        @endif
                                        @if($key == 1)
                                            <?php
                                            $val = isset($review->clear_requirment) && $review ? $review->clear_requirment : '3';
                                            ?>
                                        @endif
                                    @else
                                        <?php
                                        $val = 0;
                                        ?>
                                    @endif
                                    <div class="ratting">
                                        <input type="hidden" class="rating feedback" value="{{$val}}" name="rate_type_{{ $key }}" value="1" data-filled="fa fa-star" data-empty="fa fa-star star-o"/>
                                        <span>{{ $rateCategory }}</span>
                                    </div>
                                @endforeach

                                {{--<div class="ratting">--}}
                                {{--<i class="fa fa-star"></i>--}}
                                {{--<i class="fa fa-star"></i>--}}
                                {{--<i class="fa fa-star"></i>--}}
                                {{--<i class="fa fa-star"></i>--}}
                                {{--<i class="fa fa-star star-o"></i>--}}
                                {{--<span>Communication</span>--}}
                                {{--</div>--}}
                                {{--<div class="ratting">--}}
                                {{--<i class="fa fa-star"></i>--}}
                                {{--<i class="fa fa-star"></i>--}}
                                {{--<i class="fa fa-star"></i>--}}
                                {{--<i class="fa fa-star"></i>--}}
                                {{--<i class="fa fa-star last-star-fill"></i>--}}
                                {{--<span>Availability</span>--}}
                                {{--</div>--}}
                                {{--<div class="ratting">--}}
                                {{--<i class="fa fa-star"></i>--}}
                                {{--<i class="fa fa-star star-o"></i>--}}
                                {{--<i class="fa fa-star star-o"></i>--}}
                                {{--<i class="fa fa-star star-o"></i>--}}
                                {{--<i class="fa fa-star star-o"></i>--}}
                                {{--<span>Trustworthy</span>--}}
                                {{--</div>--}}
                                {{--<div class="ratting">--}}
                                {{--<i class="fa fa-star"></i>--}}
                                {{--<i class="fa fa-star"></i>--}}
                                {{--<i class="fa fa-star"></i>--}}
                                {{--<i class="fa fa-star star-o"></i>--}}
                                {{--<i class="fa fa-star star-o"></i>--}}
                                {{--<span>Clear Requirement</span>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 form-field">
                        <div class="total uppercase">
                            {{ trans('common.total_score') }}:
                            <h1 id="ratings-total">{{isset($review) && $review ? $review->total_score : '3.00'}}</h1>
                            <input type="hidden" class="total_score" name="total_score" value="3.00">
                        </div>
                    </div>

                    <div class="col-md-12 form-field">
                        <div class="form-group">
                            <label> {{ trans('common.share_your_experience', [ 'who' => trans('common.client') ]) }} </label>
                            <textarea class="form-control" name="experience">{{isset($review) && $review  ? $review->experience : ''}}</textarea>

                            <a href="javascript:;" class="see-ex">{{ trans('common.see_an_example') }}</a>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-12 body-footer">
                <h4>
                    {{ trans('common.ending_contract_description', [ 'task' => trans('common.work'), 'who' => trans('common.client') ]) }}
                </h4>

                <button class="btn btn-color-custom" type="button" id="btn-submit">
                    {{ trans('common.end_contract') }}
                </button>

                <a href="{{ route('frontend.myreceiveorder') }}">
                    <button class="btn btn-color-non-custom" type="button">
                        {{ trans('common.cancel') }}
                    </button>
                </a>
            </div>

        </form>

    </div>
@endsection

@section('footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rating/1.4.0/bootstrap-rating.min.js"></script>
    <script>
        +function () {
            $(document).ready(function () {
                function add(a, b) {
                    return parseInt(a) + parseInt(b);
                }

                $(".rating.feedback").on('change', function(){
                    var values = [];
                    $(".rating.feedback").each(function() {
                        values.push($(this).val());
                    });
                    var sum = (values.reduce(add,0));
                    var avgRating = (sum/5).toFixed(2);
                    $("#ratings-total").html(avgRating);
                    $(".total_score").val(avgRating);
                });

                $('#freelancer-form').makeAjaxForm({
                    inModal: false,
                    closeModal: false,
                    submitBtn: '#btn-submit',
                    alertContainer: '#body-alert-container',
                    clearForm: true
                });
            });
        }(jQuery);
    </script>
@endsection