@extends('frontend.layouts.default')

@section('title')
	{{ trans('common.end_contract_who', ['who' => trans('common.client') ]) }}
@endsection

@section('description')
	{{ trans('common.end_contract_who', ['who' => trans('common.client') ]) }}
@endsection

@section('author')
Xenren
@endsection

@section('header')
    <link href="/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
    <link href="/css/end-contract.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row page-contant">
        <div class="col-md-12 title">
            <h3>
               {{ trans('common.end_contract_with', [
                    'who' => $who,
                    'project_name' => $projectName,
                    'pay_type'=> $payType]) }}
            </h3>
            <h4>
                {{ trans('common.share_you_experience', ['who' => trans('common.freelancer')]) }}
            </h4>
        </div>

        <form action="{{route("frontend.endcontract.employer.post")}}" method="POST" class="horizontal-form" id="employer-form">
            {{csrf_field()}}
            <input type="hidden" name="projectId" value="{{ $project->id }}">
            <input type="hidden" name="projectApplicantId" value="{{ $projectApplicant->id }}">
            <div class="col-md-12 body-box">
                <div class="row box-title">
                    <div class="col-md-12">
                        <div class="left">
                            <img src="/images/ic_lock.png" width="17px">
                        </div>
                        <div class="right">
                            <h3>{{ trans('common.private_feedback') }}</h3>
                            <span>
                                {{ trans('common.private_feedback_description', ['who' => trans('common.freelancer')]) }}
                                <a href="javascript:;">{{ trans('common.learn_more') }}</a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-field reason">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-5 text-right">
                                    <label>{{ trans('common.reason_for_ending_contract') }} :</label>
                                </div>
                                <div class="col-md-6">
                                    <div class="search-selectbox custom-days-selectbox">
                                        <select class="form-control" name="reason">
                                           <option value="">{{ trans('common.please_select') }}</option>
                                           @foreach( $reasons as $key => $reason )
                                           <option value="{{ $key }}">{{ $reason }}</option>
                                           @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 form-field likely">
                        <label for="reason">{{ trans('common.how_likely_to_recommend', ['who' => trans('common.freelancer')]) }}</label>
                        <div class="progress">
                            <div class="one one0 success-color">
                                <label>1<input type="radio" class="recommendation" name="recommendation" value="1"></label>
                            </div>
                            <div class="one one1 success-color">
                                <label>2<input type="radio" class="recommendation" name="recommendation" value="2"></label>
                            </div>
                            <div class="one one2 success-color">
                                <label>3<input type="radio" class="recommendation" name="recommendation" value="3"></label>
                            </div>
                            <div class="one one3 success-color">
                                <label>4<input type="radio" class="recommendation" name="recommendation" value="4"></label>
                            </div>
                            <div class="one one4 success-color">
                                <label>5<input type="radio" class="recommendation" name="recommendation" value="5"></label>
                            </div>
                            <div class="one one5 success-color">
                                <label>6<input type="radio" class="recommendation" name="recommendation" value="6" checked="checked"></label>
                            </div>
                            <div class="one one6 success-color">
                                <label>7<input type="radio" class="recommendation" name="recommendation" value="7"></label>
                            </div>
                            <div class="one one7 success-color">
                                <label>8<input type="radio" class="recommendation" name="recommendation" value="8"></label>
                            </div>
                            <div class="one one8 success-color">
                                <label>9<input type="radio" class="recommendation" name="recommendation" value="9"></label>
                            </div>
                            <div class="one one9 success-color">
                                <label>10<input type="radio" class="recommendation" name="recommendation" value="10"></label>
                            </div>
                            <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                        </div>
                        <span class="span1">{{ trans('common.not_at_all_likely') }}</span>
                        <span class="span2">{{ trans('common.extremely_likely') }}</span>
                    </div>

                    <div class="col-md-6 form-field rate">
                        <div class="form-group">
                            <label for="reason">{{ trans('common.rate_their_english') }}</label>

                            <div class="radio-list">
                                @foreach( $englishProficiencies as $key => $englishProficiency )
                                <div class="radios">
                                    <input type="radio" class="proficiency" name="english_proficiency" value="{{ $key }}">
                                    <label for="radio1">{{ $englishProficiency }}</label>
                                </div>
                                @endforeach
                                {{--<div class="radios">--}}
                                    {{--<input type="radio" name="optradio" id="radio1">--}}
                                    {{--<label for="radio1">Difficult to understand</label>--}}
                                {{--</div>--}}
                                {{--<div class="radios">--}}
                                    {{--<input type="radio" name="optradio" checked="checked" id="radio2">--}}
                                    {{--<label for="radio2">Acceptable</label>--}}
                                {{--</div>--}}
                                {{--<div class="radios">--}}
                                    {{--<input type="radio" name="optradio" id="radio3">--}}
                                    {{--<label for="radio3">Fluent</label>--}}
                                {{--</div>--}}
                                {{--<div class="radios">--}}
                                    {{--<input type="radio" name="optradio" id="radio4">--}}
                                    {{--<label for="radio4">I didn't speak to freelancer</label>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 body-box">
                <div class="row box-title">
                    <div class="col-md-12">
                        <div class="left">
                            <img src="/images/ic_megamike.png" width="28px">
                        </div>
                        <div class="right">
                            <h3>{{ trans('common.public_feedback') }} </h3>
                            <span>
                                {{ trans('common.public_feedback_description', [ 'who' => trans('common.freelancer') ]) }}
                                <a href="javascript:;"> {{ trans('common.learn_more') }} </a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-field">
                        <div class="form-group">
                            <label>{{ trans('common.feedback_to', [ 'who' => trans('common.freelancer')]) }}:</label>
                            <div class=" ratting-custom">
                                @foreach( $rateCategories as $key => $rateCategory )
                                <div class="ratting">
                                    <input type="hidden" class="rating feedback" value="3" name="rate_type_{{ $key }}" value="1" data-filled="fa fa-star" data-empty="fa fa-star star-o"/>
                                    <span>{{ $rateCategory }} </span>
                                </div>
                                @endforeach
                            </div>
                            {{--<div class=" ratting-custom">--}}
                                {{--<div class="ratting">--}}
                                    {{--<i class="fa fa-star"></i>--}}
                                    {{--<i class="fa fa-star"></i>--}}
                                    {{--<i class="fa fa-star"></i>--}}
                                    {{--<i class="fa fa-star star-o"></i>--}}
                                    {{--<i class="fa fa-star star-o"></i>--}}
                                    {{--<span>Payment on time</span>--}}
                                {{--</div>--}}
                                {{--<div class="ratting">--}}
                                    {{--<i class="fa fa-star"></i>--}}
                                    {{--<i class="fa fa-star"></i>--}}
                                    {{--<i class="fa fa-star"></i>--}}
                                    {{--<i class="fa fa-star"></i>--}}
                                    {{--<i class="fa fa-star star-o"></i>--}}
                                    {{--<span>Communication</span>--}}
                                {{--</div>--}}
                                {{--<div class="ratting">--}}
                                    {{--<i class="fa fa-star"></i>--}}
                                    {{--<i class="fa fa-star"></i>--}}
                                    {{--<i class="fa fa-star"></i>--}}
                                    {{--<i class="fa fa-star"></i>--}}
                                    {{--<i class="fa fa-star last-star-fill"></i>--}}
                                    {{--<span>Availability</span>--}}
                                {{--</div>--}}
                                {{--<div class="ratting">--}}
                                    {{--<i class="fa fa-star"></i>--}}
                                    {{--<i class="fa fa-star star-o"></i>--}}
                                    {{--<i class="fa fa-star star-o"></i>--}}
                                    {{--<i class="fa fa-star star-o"></i>--}}
                                    {{--<i class="fa fa-star star-o"></i>--}}
                                    {{--<span>Trustworthy</span>--}}
                                {{--</div>--}}
                                {{--<div class="ratting">--}}
                                    {{--<i class="fa fa-star"></i>--}}
                                    {{--<i class="fa fa-star"></i>--}}
                                    {{--<i class="fa fa-star"></i>--}}
                                    {{--<i class="fa fa-star star-o"></i>--}}
                                    {{--<i class="fa fa-star star-o"></i>--}}
                                    {{--<span>Clear Requirement</span>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </div>

                    <div class="col-md-6 form-field">
                        <div class="total uppercase">
                            {{ trans('common.total_score') }}:
                            <h1 id="ratings-total">3.00</h1>
                            <input type="hidden" class="total_score" name="total_score" value="3.00">
                        </div>
                    </div>

                    <div class="col-md-12 form-field">
                        <div class="form-group">
                            <label>{{ trans('common.skill_freelancer_most_experience') }}:</label>
                            <div class="label-custom">
                                @foreach( $projectSkills as $projectSkill)
                                <label id="lbl-skill-tag-{{ $projectSkill->skill->id }}" class="label-default">
                                    <span>
                                        {{ $projectSkill->skill->getName() }}
                                    </span>
                                </label>
                                @endforeach
                                <input class="review_skills" type="hidden" name="review_skills" value="">
                                {{--<label class="label-color">--}}
                                    {{--<span>--}}
                                        {{--User Experience Design--}}
                                    {{--</span>--}}
                                {{--</label>--}}
                                {{--<label class="label-default">--}}
                                    {{--<span>--}}
                                        {{--Graphic Design--}}
                                    {{--</span>--}}
                                {{--</label>--}}
                                {{--<label class="label-default">--}}
                                    {{--<span>--}}
                                        {{--Technical Research--}}
                                    {{--</span>--}}
                                {{--</label>--}}
                                {{--<label class="label-default">--}}
                                    {{--<span>--}}
                                        {{--Wireframe--}}
                                    {{--</span>--}}
                                {{--</label>--}}
                                {{--<label class="label-default">--}}
                                    {{--<span>--}}
                                        {{--Phototype Development--}}
                                    {{--</span>--}}
                                {{--</label>--}}
                                {{--<label class="label-color">--}}
                                    {{--<span>--}}
                                        {{--User Interface Design--}}
                                    {{--</span>--}}
                                {{--</label>--}}
                                {{--<label class="label-default">--}}
                                    {{--<span>--}}
                                        {{--Website Design--}}
                                    {{--</span>--}}
                                {{--</label>--}}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 form-field">
                        <div class="form-group">
                            <label>{{ trans('common.share_your_experience', [ 'who' => trans('common.freelancer') ]) }} :</label>
                            <textarea class="form-control" name="experience"></textarea>

                            <a href="javascript:;" class="see-ex">{{ trans('common.see_an_example') }}</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 body-footer">
                <h4>
                    {{ trans('common.ending_contract_description', [ 'task' => trans('common.freelancer_work_diary'), 'who' => trans('common.freelancer') ]) }}
                </h4>

                <button class="btn btn-color-custom" type="button" id="btn-submit">
                    {{ trans('common.end_contract') }}
                </button>

                <a href="{{ route('frontend.mypublishorder') }}">
                    <button class="btn btn-color-non-custom" type="button">
                        {{ trans('common.cancel') }}
                    </button>
                </a>
            </div>

        </form>

    </div>
@endsection

@section('footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rating/1.4.0/bootstrap-rating.min.js"></script>
    <script>
        +function () {
            $(document).ready(function () {
                var lang = {
                    'choose_max_three_skills': "{{ trans('common.choose_max_three_skills') }}"
                }

                function add(a, b) {
                    return parseInt(a) + parseInt(b);
                }

                $(".rating.feedback").on('change', function(){
                    var values = [];
                    $(".rating.feedback").each(function() {
                        values.push($(this).val());
                    });
                    var sum = (values.reduce(add,0));
                    var avgRating = (sum/6).toFixed(2);
                    $("#ratings-total").html(avgRating);
                    $(".total_score").val(avgRating);
                });

                $('#employer-form').makeAjaxForm({
                    inModal: false,
                    closeModal: false,
                    submitBtn: '#btn-submit',
                    alertContainer: '#body-alert-container',
                    clearForm: true
                });


                $('label[id^="lbl-skill-tag-"]').click(function(){
                    var skillId = parseInt(this.id.replace("lbl-skill-tag-", ""));
                    var existingReviewSkills = $('.review_skills').val();
                    if( existingReviewSkills == "")
                    {
                        var newReviewSkills = [];
                    }
                    else
                    {
                        var newReviewSkills = existingReviewSkills.split(',');
                    }

                    if( $(this).hasClass('label-default'))
                    {
                        if( newReviewSkills.length >= 3)
                        {
                            alertError(lang.choose_max_three_skills);
                            return false;
                        }

                        $(this).removeClass('label-default').addClass('label-color');
                        newReviewSkills.push(skillId);
                    }
                    else
                    {
                        $(this).removeClass('label-color').addClass('label-default');
                        var index = newReviewSkills.indexOf('' + skillId + '');
                        if (index > -1) {
                            newReviewSkills.splice(index, 1);
                        }
                    }
                    $('.review_skills').val(newReviewSkills);

                });
            });
        }(jQuery);
    </script>
@endsection