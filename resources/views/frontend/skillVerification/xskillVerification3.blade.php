@extends('frontend.layouts.default')

@section('title')
Skill Verification
@endsection

@section('description')
Skill Verification
@endsection

@section('author')
Xenren
@endsection

@section('header')
    <link href="/custom/css/frontend/newJobs.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/skillVerification.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
	<div class="row">
        <div class="col-md-3 search-title-container">
            <span class="find-experts-search-title text-uppercase">MAIN ACTION</span>
        </div>
        <div class="col-md-9">
            <span class="find-experts-search-title text-uppercase">SKILL VERIFICATION</span>
        </div>
    </div>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="portlet light bordered skill-verification">
                      	<div class="row">
                      		<div class="col-md-12">
                      			<div class="input-group stylish-input-group">
  				                    <input type="text" class="form-control"  placeholder="Adobe Photoshop">
  				                    <span class="input-group-addon">
                                        <button type="submit">
                                           <span class="glyphicon glyphicon-search"></span>
                                        </button>
  				                    </span>
                                </div>
                      		</div>
                      	</div>

                        <section class="main-well-custom">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="tabbable-panel">
                                <div class="tabbable-line">
                                  <ul class="nav nav-tabs skill-verification-nav-ul">
                                    <li class="active"><a href="#tab_default_1" data-toggle="tab" class="f-s-16">Official project</a></li> 
                                    <li><a href="#tab_default_2" data-toggle="tab" class="f-s-16">common project</a></li> 
                                  </ul> 
                                  <div class="tab-content skill-verification-tab">
                                    <div id="tab_default_1" class="tab-pane filter-option active">
                                      <div class="portlet box div-job-main skill-verification-div-job-main">
                                        <div class="row header-cantent">
                                            <div class="col-md-12 header-bg">
                                              <img src="/images/job-title-bg01.png">
                                            </div>
                                            <div class="col-md-12 header-cantent-sub">
                                              <div class="row">
                                                <div class="col-md-12">
                                                  <img src="/images/icons/icon-copy-2.png">
                                                  <h3 class="job-title">infographic for dog ear cleaner product</h3>

                                                  <i class="fa fa-mail-forward"></i>
                                                  <i class="fa fa-star"></i>
                                                </div>
                                              </div>
                                              <div class="row details">
                                                <div class="col-md-4">
                                                  <p>project type : <strong>oficial project</strong></p>
                                                </div>
                                                <div class="col-md-4">
                                                  <p>time frame : <strong>7 days</strong></p>
                                                </div>
                                                <div class="col-md-4">
                                                  <p>project state : <strong>pre a</strong></p>
                                                </div>
                                                <div class="col-md-4">
                                                  <p>price : <strong>$5,000</strong></p>
                                                </div>
                                                <div class="col-md-4">
                                                  <p>end date : <strong>2016-10-10</strong></p>
                                                </div>
                                                <div class="col-md-4">
                                                  <p>location : <strong>zhang hai</strong></p>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                        <div class="row body-cantent">
                                            <div class="col-md-2 left text-center">
                                              <img src="/images/Alibaba-ico.png">

                                              <button class="btn btn-make-Offer">join discuss</button>
                                            </div>
                                            <div class="col-md-10 right">
                                              <p class="desc">
                                                Looking for someone who will make some banners for my jewelry site. I need 3 existing banners resized to fit mobile screen 300 X 250. Then I need 3 banners with placed on a woman (see attachment) in the size 645X967. Also I need 1 side banner, Also I need 1 side banner...
                                              </p>

                                              <div class="row">
                                                <div class="col-md-3 budget">
                                                      <span>BUDGET</span>
                                                      <h3>$100,000</h3>
                                                      <span class="share">10$ OF TOTAL SHARE</span>
                                                </div>
                                                <div class="col-md-2 defficulty">
                                                      <img src="/images/signal.png">
                                                      <span class="top-smoll">Difficulty:</span>
                                                      <div class="sub-con">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star-o"></i>
                                                      </div>
                                                    </div>
                                                <div class="col-md-3 possition">
                                                  <img src="/images/user-secret.png">
                                                      <span class="top-smoll">possition needed:</span>
                                                      <div class="sub-con">
                                                        <span>Graphic Designer, UI Designer</span>
                                                      </div>  
                                                    </div>
                                                    <div class="col-md-4 skill">
                                                      <img src="/images/hands.png">
                                                      <span class="top-smoll">skill request:</span>
                                                      
                                                      <div class="skill-div">
                                                            <span class="item-skill"><span>Adobe Ilusstrator</span></span>
                                                            <span class="item-skill"><span>Photoshop</span></span>
                                                            <span class="item-skill"><span>CSS</span></span>
                                                            <span class="item-skill"><span>HTML</span></span>
                                                        </div>
                                                    </div>
                                              </div>
                                            </div>
                                        </div>
                                      </div>

                                      <div class="portlet box div-job-main div-job-mains skill-verification-div-job-main">
                                        <div class="row header-cantent">
                                            <div class="col-md-12 header-bg">
                                              <img src="/images/job-title-bg02.png">
                                            </div>
                                            <div class="col-md-2 header-img text-center">
                                              <img src="/images/Alibaba-ico02.png">
                                            </div>
                                            <div class="col-md-10 header-cantent-sub">
                                              <div class="row">
                                                <div class="col-md-12">
                                                  <img src="/images/icons/icon-copy-2.png">
                                                  <h3 class="job-title">food items creative adaption needed</h3>

                                                  <i class="fa fa-mail-forward"></i>
                                                  <i class="fa fa-star"></i>
                                                </div>
                                              </div>
                                              <div class="row details">
                                                <div class="col-md-4">
                                                  <p>project type : <strong>oficial project</strong></p>
                                                </div>
                                                <div class="col-md-4">
                                                  <p>time frame : <strong>7 days</strong></p>
                                                </div>
                                                <div class="col-md-4">
                                                  <p>project state : <strong>pre a</strong></p>
                                                </div>
                                                <div class="col-md-4">
                                                  <p>price : <strong>$5,000</strong></p>
                                                </div>
                                                <div class="col-md-4">
                                                  <p>end date : <strong>2016-10-10</strong></p>
                                                </div>
                                                <div class="col-md-4">
                                                  <p>location : <strong>zhang hai</strong></p>
                                                </div>
                                              </div>
                                            </div>
                                        </div>

                                        <div class="row body-cantent">
                                            <div class="col-md-2 left">
                                              <div class="budget skill-verification-budget">
                                                      <span>BUDGET</span>
                                                      <h3>$100,000</h3>
                                                      <span class="share">10$ OF TOTAL SHARE</span>
                                              </div>
                                              <button class="btn btn-make-Offer">join discuss</button>
                                            </div>
                                            <div class="col-md-10 right">
                                              <p class="desc">
                                                Looking for someone who will make some banners for my jewelry site. I need 3 existing banners resized to fit mobile screen 300 X 250. Then I need 3 banners with placed on a woman (see attachment) in the size 645X967. Also I need 1 side banner, Also I need 1 side banner...
                                              </p>

                                              <div class="row">
                                                <div class="col-md-3 defficulty">
                                                      <img src="/images/signal.png">
                                                      <span class="top-smoll">Difficulty:</span>
                                                      <div class="sub-con">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star-o"></i>
                                                      </div>
                                                    </div>
                                                <div class="col-md-4 possition">
                                                  <img src="/images/user-secret.png">
                                                      <span class="top-smoll">possition needed:</span>
                                                      <div class="sub-con">
                                                        <span>Graphic Designer, UI Designer</span>
                                                      </div>  
                                                    </div>
                                                    <div class="col-md-5 skill">
                                                      <img src="/images/hands.png">
                                                      <span class="top-smoll">skill request:</span>
                                                      
                                                      <div class="skill-div">
                                                            <span class="item-skill"><span>Adobe Ilusstrator</span></span>
                                                            <span class="item-skill"><span>Photoshop</span></span>
                                                            <span class="item-skill"><span>CSS</span></span>
                                                            <span class="item-skill"><span>HTML</span></span>
                                                        </div>
                                                    </div>
                                              </div>
                                            </div>
                                        </div>
                                      </div>

                                    </div> 
                                    <div id="tab_default_2" class="tab-pane">
                                      <div class="portlet box div-job-main skill-verification-common-project-main">
                                        <div class="portlet-title">
                                          <div class="row">
                                              <div class="col-md-12">      
                                                <h4>Pinterest standard Graphic Designer Needed 3 - 20USD per Hour</h4> 
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-md-12 price-div">
                                                <span class="top-smoll">
                                                    FIXED PRICE -
                                                    <span class="price"><i class="fa fa-usd" aria-hidden="true"></i>150.00.</span>
                                                </span>  
                                              </div>
                                          </div>
                                         </div>
                                         <div class="portlet-body">
                                            <div class="row">
                                              <div class="col-md-12 common-project-tab-desc">
                                                Convert old templetes into the new templetes that work with our new DND template software. Complete instructions provided. Must have an eye to detail and is a good problem solver. Must have designed eamils for hign deliver-ability
                                              </div>
                                            </div>
                                            <div class="row">
                                              <div class="col-md-6">
                                                <div class="common-project-tab-skill-div">
                                                  <span class="item-skill"><span>PHP</span></span>
                                                  <span class="item-skill"><span>JavaScript</span></span>
                                                  <span class="item-skill"><span>HTML5</span></span>
                                                  <span class="item-skill"><span>Adobe Photoshop</span></span>
                                                  <span class="item-skill"><span>Adobe Ilusstrator</span></span>
                                                </div>
                                              </div>
                                              <div class="col-md-6 common-project-tab-btn" align="right">
                                                <button class="btn make-an-offer">Make an Offer</button><br />
                                                <button class="btn join-discuss">Join Discuss</button>
                                              </div>
                                            </div>
                                            <div class="row common-project-tab-footer-main">
                                              <div class="col-md-2">
                                                <div class="user-img">
                                                  <img src="/images/f-user-icon1.png" class="img-circle">
                                                  <i class="fa fa-check"></i>
                                                </div>
                                              </div>
                                              <div class="col-md-10 common-project-tab-footer">
                                                <div class="row">
                                                  <div class="col-md-6 common-project-tab-user">
                                                    <h4>Adam John</h4>
                                                    <div class="star">
                                                      <i class="fa fa-star"></i>
                                                      <i class="fa fa-star"></i>
                                                      <i class="fa fa-star"></i>
                                                      <i class="fa fa-star"></i>
                                                      <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="spent">
                                                      <span style="color: #333343;">$1000</span> Spent
                                                    </div>  
                                                    <div class="country">
                                                      Malatsia
                                                    </div>  
                                                  </div>
                                                  <div class="col-md-6 common-project-tab-time" align="right">
                                                    <span>Posted: 2 Hours ago</span><br />
                                                    <i class="fa fa-thumbs-up"></i>
                                                    <i class="fa fa-tag"></i>
                                                  </div>
                                                </div>
                                              </div>

                                            </div>
                                         </div>

                                      </div>
                                    </div> 

                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </section>

                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection

@push('js')
@endpush
