@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.skill_verification_title') }}
@endsection

@section('description')
    {{ trans('common.skill_verification') }}
@endsection

@section('author')
    Xenren
@endsection

@section('url')
    https://www.xenren.co/skillVerification
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection


@section('header')
    <link href="/custom/css/frontend/modalMakeOffer.css" rel="stylesheet" type="text/css"/>
    <link href="/custom/css/frontend/newJobs.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/skillVerification.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row setROWCOL">
        <div class="col-md-3 search-title-container">
            <span class="find-experts-search-title text-uppercase setMAinMENU">
                {{ trans('common.main_action') }}
            </span>
        </div>
        <div class="col-md-9 setCol9RPP">
            <span class="find-experts-search-title text-uppercase setSKILLVERI">
                {{ trans('common.skill_verification') }}
            </span>
        </div>
    </div>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">

            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row setPageContent">
                <div class="col-md-12 col-sm-12">
                    <div class="portlet light bordered skill-verification">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group stylish-input-group setINPUTSKILLVERI">
                                    <input id="txtKeyword"
                                           type="text"
                                           class="form-control"
                                           placeholder="{{ trans('common.what_skills_you_wish_to_prove_youself') }}"
                                           value="{{ isset( $keyword ) && $keyword != "" ? $keyword : ''}}">
                                    <span class="input-group-addon">
				                        <button type="submit" id="btnSearch" class="setBTNSEARCH">
                                                            <span class="glyphicon glyphicon-search" style="font-family:Glyphicons Halflings"></span>
				                        </button>  
				                    </span>
                                </div>
                            </div>
                        </div>

                        @if( $keyword == null )
                            <div class="row">
                                <div class="col-md-12 skill-verification-finger-img">
                                    <img src="/images/icons/skill_verification0.png">
                                    <p>{{ trans('common.prove_your_skill_is_important') }} <br /> {{ trans('common.highly_increase_the_chance_been_hire') }}</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="setPADDING1"></div>
                            </div>

                        @else
                            @if( count($jobs) == 0 )
                                <div class="row">
                                    <div class="col-md-12 skill-verification-keyword">
                                        <p class="first-p">{{ trans('common.didnt_found_any_project_related_to_your_skill') }}</p>
                                        <p>{{ trans('common.please_recommend_us') }}</p>

                                        <div id="magicsuggest"></div>
                                        {{ Form::hidden('skill_id_list', null, ['id'=>'skill_id_list']) }}

                                        <a href="javascript:;" class="f-s-10" data-toggle="modal" data-target="#modelAddNewSkillKeyword" >
                                            <button class="btn">{{ trans('common.submit_keyword') }}</button>
                                        </a>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="setPADDING2"></div>
                                </div>
                            @else
                                <section class="main-well-custom">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tabbable-panel">
                                                <div class="tabbable-line">
                                                    <ul class="nav nav-tabs skill-verification-nav-ul">
                                                        <li class="{{ $projectType == \App\Models\Project::PROJECT_TYPE_OFFICIAL? 'active': '' }}">
                                                            {{--<a href="{{ route('frontend.skillverification') }}?keyword={{ $keyword }}&project_type={{ \App\Models\Project::PROJECT_TYPE_OFFICIAL }}" class="f-s-16">--}}
                                                                {{--{{ trans('common.official_project') }}--}}
                                                            {{--</a>--}}
                                                            <a href="{{ route('frontend.jobs.official', \Session::get('lang')) }}?project_type={{ \App\Models\Project::PROJECT_TYPE_OFFICIAL }}" class="f-s-16">
                                                                {{ trans('common.official_project') }}
                                                            </a>
                                                        </li>
                                                        <li class="{{ $projectType == \App\Models\Project::PROJECT_TYPE_COMMON? 'active': '' }}">
                                                            {{--<a href="{{ route('frontend.skillverification') }}?keyword={{ $keyword }}&project_type={{ \App\Models\Project::PROJECT_TYPE_COMMON }}" class="f-s-16">--}}
                                                                {{--{{ trans('common.common_project') }}--}}
                                                            {{--</a>--}}
                                                            <a href="{{ route('frontend.jobs.index', \Session::get('lang')) }}?project_type={{ \App\Models\Project::PROJECT_TYPE_COMMON }}" class="f-s-16">
                                                                {{ trans('common.common_project') }}
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <div class="tab-content skill-verification-tab">
                                                        <div id="tab_default_1" class="tab-pane filter-option active">
                                                            @if( isset($jobs) )
                                                                @foreach($jobs as $job)
                                                                    @if( $projectType == \App\Models\Project::PROJECT_TYPE_OFFICIAL )
                                                                        @include('frontend.includes.ucOfficialProjectItem', [
                                                                            'officialProject' => $job
                                                                        ])
                                                                    @else
                                                                        @include('frontend.includes.ucCommonProjectItem', [
                                                                            'commonProject' => $job
                                                                        ])
                                                                    @endif
                                                                @endforeach
                                                            @endif

                                                            {{--<div class="portlet box div-job-main skill-verification-div-job-main">--}}
                                                            {{--<div class="row header-cantent">--}}
                                                            {{--<div class="col-md-12 header-bg">--}}
                                                            {{--<img src="/images/job-title-bg01.png">--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-12 header-cantent-sub">--}}
                                                            {{--<div class="row">--}}
                                                            {{--<div class="col-md-12">--}}
                                                            {{--<img src="/images/icons/icon-copy-2.png">--}}
                                                            {{--<h3 class="job-title">infographic for dog ear cleaner product</h3>--}}

                                                            {{--<i class="fa fa-mail-forward"></i>--}}
                                                            {{--<i class="fa fa-star"></i>--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="row details">--}}
                                                            {{--<div class="col-md-4">--}}
                                                            {{--<p>project type : <strong>oficial project</strong></p>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-4">--}}
                                                            {{--<p>time frame : <strong>7 days</strong></p>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-4">--}}
                                                            {{--<p>project state : <strong>pre a</strong></p>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-4">--}}
                                                            {{--<p>price : <strong>$5,000</strong></p>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-4">--}}
                                                            {{--<p>end date : <strong>2016-10-10</strong></p>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-4">--}}
                                                            {{--<p>location : <strong>zhang hai</strong></p>--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="row body-cantent">--}}
                                                            {{--<div class="col-md-2 left text-center">--}}
                                                            {{--<img src="/images/Alibaba-ico.png">--}}

                                                            {{--<button class="btn btn-make-Offer">join discuss</button>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-10 right">--}}
                                                            {{--<p class="desc">--}}
                                                            {{--Looking for someone who will make some banners for my jewelry site. I need 3 existing banners resized to fit mobile screen 300 X 250. Then I need 3 banners with placed on a woman (see attachment) in the size 645X967. Also I need 1 side banner, Also I need 1 side banner...--}}
                                                            {{--</p>--}}

                                                            {{--<div class="row">--}}
                                                            {{--<div class="col-md-3 budget">--}}
                                                            {{--<span>BUDGET</span>--}}
                                                            {{--<h3>$100,000</h3>--}}
                                                            {{--<span class="share">10$ OF TOTAL SHARE</span>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-2 defficulty">--}}
                                                            {{--<img src="/images/signal.png">--}}
                                                            {{--<span class="top-smoll">Difficulty:</span>--}}
                                                            {{--<div class="sub-con">--}}
                                                            {{--<i class="fa fa-star"></i>--}}
                                                            {{--<i class="fa fa-star"></i>--}}
                                                            {{--<i class="fa fa-star"></i>--}}
                                                            {{--<i class="fa fa-star"></i>--}}
                                                            {{--<i class="fa fa-star-o"></i>--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-3 possition">--}}
                                                            {{--<img src="/images/user-secret.png">--}}
                                                            {{--<span class="top-smoll">possition needed:</span>--}}
                                                            {{--<div class="sub-con">--}}
                                                            {{--<span>Graphic Designer, UI Designer</span>--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-4 skill">--}}
                                                            {{--<img src="/images/hands.png">--}}
                                                            {{--<span class="top-smoll">skill request:</span>--}}

                                                            {{--<div class="skill-div">--}}
                                                            {{--<span class="item-skill"><span>Adobe Ilusstrator</span></span>--}}
                                                            {{--<span class="item-skill"><span>Photoshop</span></span>--}}
                                                            {{--<span class="item-skill"><span>CSS</span></span>--}}
                                                            {{--<span class="item-skill"><span>HTML</span></span>--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}

                                                            {{--<div class="portlet box div-job-main div-job-mains skill-verification-div-job-main">--}}
                                                            {{--<div class="row header-cantent">--}}
                                                            {{--<div class="col-md-12 header-bg">--}}
                                                            {{--<img src="/images/job-title-bg02.png">--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-2 header-img text-center">--}}
                                                            {{--<img src="/images/Alibaba-ico02.png">--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-10 header-cantent-sub">--}}
                                                            {{--<div class="row">--}}
                                                            {{--<div class="col-md-12">--}}
                                                            {{--<img src="/images/icons/icon-copy-2.png">--}}
                                                            {{--<h3 class="job-title">food items creative adaption needed</h3>--}}

                                                            {{--<i class="fa fa-mail-forward"></i>--}}
                                                            {{--<i class="fa fa-star"></i>--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="row details">--}}
                                                            {{--<div class="col-md-4">--}}
                                                            {{--<p>project type : <strong>oficial project</strong></p>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-4">--}}
                                                            {{--<p>time frame : <strong>7 days</strong></p>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-4">--}}
                                                            {{--<p>project state : <strong>pre a</strong></p>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-4">--}}
                                                            {{--<p>price : <strong>$5,000</strong></p>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-4">--}}
                                                            {{--<p>end date : <strong>2016-10-10</strong></p>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-4">--}}
                                                            {{--<p>location : <strong>zhang hai</strong></p>--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}

                                                            {{--<div class="row body-cantent">--}}
                                                            {{--<div class="col-md-2 left">--}}
                                                            {{--<div class="budget skill-verification-budget">--}}
                                                            {{--<span>BUDGET</span>--}}
                                                            {{--<h3>$100,000</h3>--}}
                                                            {{--<span class="share">10$ OF TOTAL SHARE</span>--}}
                                                            {{--</div>--}}
                                                            {{--<button class="btn btn-make-Offer">join discuss</button>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-10 right">--}}
                                                            {{--<p class="desc">--}}
                                                            {{--Looking for someone who will make some banners for my jewelry site. I need 3 existing banners resized to fit mobile screen 300 X 250. Then I need 3 banners with placed on a woman (see attachment) in the size 645X967. Also I need 1 side banner, Also I need 1 side banner...--}}
                                                            {{--</p>--}}

                                                            {{--<div class="row">--}}
                                                            {{--<div class="col-md-3 defficulty">--}}
                                                            {{--<img src="/images/signal.png">--}}
                                                            {{--<span class="top-smoll">Difficulty:</span>--}}
                                                            {{--<div class="sub-con">--}}
                                                            {{--<i class="fa fa-star"></i>--}}
                                                            {{--<i class="fa fa-star"></i>--}}
                                                            {{--<i class="fa fa-star"></i>--}}
                                                            {{--<i class="fa fa-star"></i>--}}
                                                            {{--<i class="fa fa-star-o"></i>--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-4 possition">--}}
                                                            {{--<img src="/images/user-secret.png">--}}
                                                            {{--<span class="top-smoll">possition needed:</span>--}}
                                                            {{--<div class="sub-con">--}}
                                                            {{--<span>Graphic Designer, UI Designer</span>--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="col-md-5 skill">--}}
                                                            {{--<img src="/images/hands.png">--}}
                                                            {{--<span class="top-smoll">skill request:</span>--}}

                                                            {{--<div class="skill-div">--}}
                                                            {{--<span class="item-skill"><span>Adobe Ilusstrator</span></span>--}}
                                                            {{--<span class="item-skill"><span>Photoshop</span></span>--}}
                                                            {{--<span class="item-skill"><span>CSS</span></span>--}}
                                                            {{--<span class="item-skill"><span>HTML</span></span>--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}
                                                            {{--</div>--}}

                                                        </div>
                                                        <div id="tab_default_2" class="tab-pane">
                                                            <div class="portlet box div-job-main skill-verification-common-project-main">
                                                                <div class="portlet-title">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <h4>Pinterest standard Graphic Designer Needed 3 - 20USD per Hour</h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12 price-div">
                                                                        <span class="top-smoll">
                                                                            FIXED PRICE -
                                                                            <span class="price"><i class="fa fa-usd" aria-hidden="true"></i>150.00.</span>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="portlet-body">
                                                                    <div class="row">
                                                                        <div class="col-md-12 common-project-tab-desc">
                                                                            Convert old templetes into the new templetes that work with our new DND template software. Complete instructions provided. Must have an eye to detail and is a good problem solver. Must have designed eamils for hign deliver-ability
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="common-project-tab-skill-div">
                                                                                <span class="item-skill"><span>PHP</span></span>
                                                                                <span class="item-skill"><span>JavaScript</span></span>
                                                                                <span class="item-skill"><span>HTML5</span></span>
                                                                                <span class="item-skill"><span>Adobe Photoshop</span></span>
                                                                                <span class="item-skill"><span>Adobe Ilusstrator</span></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6 common-project-tab-btn" align="right">
                                                                            <button class="btn make-an-offer">Make an Offer</button><br />
                                                                            <button class="btn join-discuss">Join Discuss</button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row common-project-tab-footer-main">
                                                                        <div class="col-md-2">
                                                                            <div class="user-img">
                                                                                <img src="/images/f-user-icon1.png" class="img-circle">
                                                                                <i class="fa fa-check"></i>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-10 common-project-tab-footer">
                                                                            <div class="row">
                                                                                <div class="col-md-6 common-project-tab-user">
                                                                                    <h4>Adam John</h4>
                                                                                    <div class="star">
                                                                                        <i class="fa fa-star"></i>
                                                                                        <i class="fa fa-star"></i>
                                                                                        <i class="fa fa-star"></i>
                                                                                        <i class="fa fa-star"></i>
                                                                                        <i class="fa fa-star"></i>
                                                                                    </div>
                                                                                    <div class="spent">
                                                                                        <span class="setSPAN1">$1000</span> Spent
                                                                                    </div>
                                                                                    <div class="country">
                                                                                        Malatsia
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 common-project-tab-time" align="right">
                                                                                    <span>Posted: 2 Hours ago</span><br />
                                                                                    <i class="fa fa-thumbs-up"></i>
                                                                                    <i class="fa fa-tag"></i>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            @endif
                        @endif


                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    @include('frontend.includes.modalAddNewSkillKeywordv2', [
        'tagColor' => \App\Models\Skill::TAG_COLOR_GOLD
    ])
@endsection

@section('modal')
    <div id="modalMakeOffer" class="modal fade apply-job-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <!-- settings area -->
    <script>
        var user_ui_preference = '{{\Auth::user() ? \Auth::user()->uipreference: false }}';
        var user_ui_preference_both = '{{\App\Models\User::USER_UI_PREFERENCE_BOTH}}';
        var user_ui_preference_freelancer = '{{\App\Models\User::USER_UI_PREFERENCE_FREELANCER}}';
        var user_ui_preference_employer = '{{\App\Models\User::USER_UI_PREFERENCE_EMPLOYER}}';
        var user_email_notification = '{{\Auth::user() ? \Auth::user()->email_notification: false }}';
        var user_wechat_notification = '{{\Auth::user() ? \Auth::user()->wechat_notification: false }}';
        var user_push_notification = '{{\Auth::user() ? \Auth::user()->push_notification: false }}';
        var user_close_account = '{{\Auth::user() ? \Auth::user()->close_account: false }}';
        var push_notification = '{{\App\Models\User::APP_PUSH_NOTIFICATION}}';
        var close_account = '{{\App\Models\User::CLOSE_MY_ACCOUNT}}';
        var email_notification = '{{\App\Models\User::USER_EMAIL_NOTIFICATION}}';
        var wechat_notification = '{{\App\Models\User::USER_WECHAT_NOTIFICATION}}';
        var checked_in_space = '{{\App\Models\User::CHECKED_IN_CO_WORK_SPACE}}';
        var checked_in_COWORK = '{{\Auth::user() ? \Auth::user()->checked_in_cowork_space : false}}';

        console.log(user_email_notification);
        console.log(user_wechat_notification);

    </script>
    <script type="text/javascript" src="/custom/js/frontend/settings.js"></script>
    <!-- settings area -->
    <script>
			var skillList = {!! $skillList !!};
			window.session = '{{\Session::get('lang')}}';
			var lang = {
				error: '{{trans('common.unknown_error')}}',
				please_choose_search_type : "{{ trans('common.please_choose_search_type') }}",
				please_select_skills  : "{{ trans('common.please_select_skills') }}",
				help_us_improve_skill_keyword : "{{ trans('common.help_us_improve_skill_keyword') }}",
				unable_to_request : "{{ trans('common.unable_to_request') }}",
				please_select_freelancer_project_cofounder : "{{ trans('common.please_select_freelancer_project_cofounder') }}",
				freelancer_require_what_skill : "{{ trans('common.freelancer_require_what_skill') }}",
				what_skill_you_have : "{{ trans('common.what_skill_you_have') }}",
				co_founder_require_what_skill : "{{ trans('common.co_founder_require_what_skill') }}",
				search_keyword_can_not_empty : "{{ trans('common.search_keyword_can_not_empty') }}",
				one_of_skill_input_empty : "{{ trans('common.one_of_skill_input_empty') }}",
				maximum_ten_skill_keywords_allow_to_submit : "{{ trans('common.maximum_ten_skill_keywords_allow_to_submit') }}",
				new_skill_keywords : "{{ trans('common.new_skill_keywords') }}",
				please_use_wiki_or_baidu_support_your_keyword : "{{ trans('common.please_use_wiki_or_baidu_support_your_keyword') }}",
			}
			if( is_guest == false )
			{
				var noSuggestionText = "<a href='javascript:;' data-toggle='modal' data-target='#modelAddNewSkillKeyword'>" + lang.help_us_improve_skill_keyword + "</a>";
			}
			else
			{
				var noSuggestionText = "<a href='" + url.login + "' >" + lang.help_us_improve_skill_keyword + "</a>";
			}
			var ms = $('#txtKeyword').magicSuggest({
				allowFreeEntries: false,
				allowDuplicates: false,
				hideTrigger: true,
				placeholder: g_lang.search,
				data: skillList,
				method: 'get',
				highlight: true,
				noSuggestionText: noSuggestionText,
				renderer: function(data){
                if( data.description == null )
					{
						return '<b>' + data.name + '</b>';
						// return '<b>' + data.name + '</b>' + '<i class="fa fa-plus pull-right add-more"></i>';
					}
					else
					{
						var name = data.name.split("|");

						return '<b>' + name[0] + '</b> | <span class="search-desc">' + data.description + '<i class="fa fa-plus search-plus "></i></span>';
						// return '<b>' + name[0] + '</b> | <span class="search-desc">' + data.description + '</span>' + '<i class="fa fa-plus pull-right add-more"></i>';
					}

				},
				selectionRenderer: function(data){
					console.log('selectionRenderer');
					console.log(data);
					console.log(data.name);
					result = data.name;
					name = result.split("|")[0];
					window.searchSkills.push(name);
					console.log('window.searchSkills');
					console.log(window.searchSkills);
					return name;
				},
			});

			$(ms).on(
				'selectionchange', function(e, cb, s){
					$('.btn-search').attr('data-skill-id-list', cb.getValue());
				}
			);


			$(ms).on('keydown', function(e,m,v){
				if(v.keyCode === 13 || v.which  === 13 ){
					var skillIdList = $('.btn-search').data('skill-id-list');
					if( skillIdList == null )
					{
						return true;
					}

					var searchType = $('.btn-search').data('search-type');
					if( searchType == null )
					{
						return true;
					}

					$('.btn-search').trigger('click');
				}
			});

    </script>
    <script src="/custom/js/frontend/modalAddNewSkillKeywordv2.js" type="text/javascript" ></script>
    <script type="text/javascript" src="/custom/js/frontend/skillVerification.js"></script>
    <script type="text/javascript" src="/custom/js/frontend/ucProjectItem.js"></script>

@endsection




