<!-- Modal -->
<div id="modalChangeUserAboutMe" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="border-radius: 2px !important;">
            {!! Form::open(['route' => 'frontend.personalinformation.change', 'id' => 'change-user-detail-form', 'method' => 'post', 'class' => 'form-horizontal']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    {{trans('common.about-me')}}
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-body">
                    <div class="form-group">
                        <div class="col-md-12" id="add-user-status-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">
                            {{trans('common.about-me')}}

                        </label>
                        <div class="col-md-9">
                            <textarea class="form-control about_me" name="about_me" v-model="user.about_me" cols="50"
                                      rows="10"></textarea>

                        </div>
                    </div>


                </div>
            </div>
            <div class="modal-footer"
                 style="display: flex;
                 justify-content: flex-end;"
            >
                <button id="btn-add-status-submit"
                        style="width:20%;
                        display: flex;
                        align-items: center;
                        justify-content: center;"
                        type="button" class="btn btn-success m-b-0"
                        v-on:click="change('about_me')">
                    <span v-if="!aboutMeLoader">{{ trans('member.confirm_add') }}</span>
                    <div v-if="aboutMeLoader" class="lds-ring-aboutMe lds-ring">
                        <div></div><div></div><div></div><div></div>
                    </div>
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    {{ trans('member.cancel') }}
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

