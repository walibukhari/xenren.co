@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.personal_info_title') }}
@endsection

@section('keywords')
    {{ trans('common.personal_info_keyword') }}
@endsection

@section('description')
    {{ trans('common.personal_info_desc') }}
@endsection

@section('author')

@endsection

@section('url')
    https://www.xenren.co/userCenter
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection


@section('header')
    <link href="{{asset('assets/global/plugins/bootstrap-tagsinput-latest/dist/bootstrap-tagsinput.css')}}" rel="stylesheet" />
    <link href="/assets/global/plugins/dropzone-4.0.1/min/dropzone.min.css" rel="stylesheet"/>
    <link href="/assets/global/plugins/dropzone-4.0.1/min/basic.min.css" rel="stylesheet"/>
    <link href="/assets/global/plugins/lightbox2/dist/css/lightbox.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/global/plugins/jcrop/css/jquery.Jcrop.min.css" rel="stylesheet"/>
    <link href="/custom/css/frontend/findExperts.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/modalSetContactInfo.css" rel="stylesheet" type="text/css"/>
    <link href="/custom/css/general.css" rel="stylesheet" />
    <link href="/custom/css/frontend/personal-info.css" rel="stylesheet" />
    <link href="/custom/css/frontend/userCenter.css" rel="stylesheet" />
    <link href="https://cdn.rawgit.com/konpa/devicon/master/devicon.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" rel="stylesheet" />
    <link href="/custom/css/frontend/personalPortfolioHover.css" rel="stylesheet" />
    <link href="/custom/css/frontend/personalPortfolio.css" rel="stylesheet" />
    <link href="/custom/css/frontend/modalAddNewSkillKeywordv2.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/select2/css/select2.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <style>
        .lds-ring {
            display: inline-block;
            position: relative;
            width: 20px;
            height: 20px;
        }

        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 20px;
            height: 20px;
            margin: 1px;
            border: 2px solid #555;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #555 transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
@endsection

@section('content')
    <style>
        .dropzone .dz-preview{
            min-height: 100px;
            height: 130px !important;
            width: auto !important;
        }
        .dropzone .dz-preview .dz-image{
            width: auto !important;
        }
        .add-file-symbol {
            color: #58AF2A;
            /* border: #58AF2A solid 1px; */
            padding: 20px 20px;
            text-align: center;
            margin-top: 0px !important;
        }
        .set-dollar{
            align-items: center;
            display: flex;
        }
        .setMAINMENU{
            margin-top:5%;
        }
        /*@media (max-width:1029px)*/
        /*{*/
            /*.setTHISPERSONAI {*/
                /*margin-left: 38%;*/
            /*}*/
            /*.setCol9PI{*/
                /*margin-top:-2%;*/
            /*}*/
        /*}*/

        .setMUPPAGESPAN{
            font-size: 30px;
        }
        .setMOCKUPPAGEUAVA{
            display:none !important;
        }
        .setMUPAGELBL{
            display:block;
        }
        .setMOCKUPPAGEIFC{
            display:none;
        }
        .setMUPPAGEALINKBTN{
            padding-top: 2px;
            padding-right: 0px;
            font-size: 15px;
        }
        .setMUPPAGESPANNAME{
            padding-right: 3px;
            margin: 0px;
        }
        .setMUPPAGEGRAPH{
            width:100%;
        }
        .setCOMMENTPAGEUT{
            border: none;
        }
        .setCOMMENTPAGEGITHUB{
            font-size: 18px;
        }
        .setMODALCUS{
            padding-top: 20px;
        }
        .setMCUSKILL{
            display:none;
        }
        .setPAgeContent{
            margin-bottom: 11px;
        }
    </style>
    <div id="userCenter">
        <div class="row setCol9PI">
            <div class="col-md-3 search-title-container">
                <span class="find-experts-search-title text-uppercase setMAinMENU">{{trans('common.main_action')}}</span> <a class="leftSide-menu-list"><i class="fa fa-bars"></i></a>
            </div>
            <div class="col-md-9">
                <span class="find-experts-search-title text-uppercase setTHISPERSONAI">{{trans('common.personal_info')}}</span>
            </div>
        </div>

        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">

            <!-- BEGIN CONTENT BODY -->
            <div class="page-content setPAgeContent">
                @if(isset(\Auth::user()->face_image) &&  \Auth::user()->face_image == \App\Models\FaceDetect::STATUS_FACE_DETECT_FALSE)
                    <div class="alert alert-danger setALPIP">{{trans('common.face_image')}}</div>
                @elseif(isset(\Auth::user()->face_image) &&  \Auth::user()->face_image == \App\Models\FaceDetect::STATUS_FACE_DETECT_TRUE)
                    <div class="alert alert-success setALPIP">Face Detection Successfully</div>
                @else
                    <div class="alert alert-danger setALPIP">{{trans('common.face_image')}}</div>
                @endif
            <!-- BEGIN PAGE BASE CONTENT -->
                @if(  $user->getLatestIdentityStatus() != \App\Models\UserIdentity::STATUS_APPROVED && $user->close_notice <= 2  )
                    <section class="main div-container m-b-20 notice">
                        <div class="row">
                            <div class="col-md-1">
                                <span aria-hidden="true" class="icon-user f-c-green2 p-t-20 setMUPPAGESPAN"></span>
                            </div>

                            <div class="col-md-10">
                                <span class="f-s-24">{{ trans('common.changing_your_avatar') }}</span><br/>
                                <span class="f-c-gray">{!! trans('common.upload_real_picture_to_gain_traffic', ['url' => route('frontend.identityauthentication')]) !!}</span>
                            </div>

                            <div class="col-md-1">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            </div>
                        </div>
                    </section>
                @endif

                <section class="main div-container well setMDIVCW">
                    <div class="row user-img-main">
                        <div class="col-md-3 text-center">
                            <form id="form-avatar">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <img  id="setuserPICIMAGE" class="profile-picture img-circle pull-left" onerror="this.src='/images/MainLogoDefault.png'" src="{{ $user->getAvatar() }}" alt="" width="160px" height="160px">
                                <input class="uploadAvatar setMOCKUPPAGEUAVA" type="file" name="avatar">
                                <input type="hidden" name="user_id" value="{{ $user->id }}"/>
                            </form>
                            <div class="clearfix"></div>
                            <div class="p-t-20 m-t-30 col-md-12">
                                <button class="btn btn-color-custom setMUPCCBTNN1"
                                        href="{{ route('frontend.modal.setcontactinfo', ['user_id' => $_G['user']->id ] ) }}"
                                        data-toggle="modal"
                                        data-target="#modalSetContactInfo">
                                    {{trans("common.contact_info")}}
                                </button>

                                <div id="modalSetContactInfo" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content setModalDailogeCI">
                                        </div>
                                    </div>
                                </div>
                                {{--<div class="row dropdown-menu">--}}
                                    {{--<div class="col-md-12 contacts-info">--}}
                                        {{--<div class="row">--}}
                                            {{--<div class="col-md-10 col-md-10">--}}
                                                {{--<div class="row">--}}
                                                    {{--<div class="col-md-3 col-md-3 contacts-info-sub">--}}
                                                        {{--<img src="/images/Tencent_QQ.png">--}}
                                                        {{--<span>QQ</span>--}}
                                                        {{--<p name="lblQQId" class="setMUPAGELBL" >{{$user->qq_id}}</p>--}}
                                                        {{--<input name="tbxQQId" value="{{$user->qq_id}}" class="m-t-10 form-control setMOCKUPPAGEIFC" />--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-md-3 col-md-3 contacts-info-sub">--}}
                                                        {{--<img src="/images/wechat-logo.png">--}}
                                                        {{--<span>{{ trans('common.wechat') }}</span>--}}
                                                        {{--<p name="lblWechatId" class="setMUPAGELBL" >{{$user->wechat_id}}</p>--}}
                                                        {{--<input name="tbxWechatId" value="{{$user->wechat_id}}" class="m-t-10 form-control setMOCKUPPAGEIFC" />--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-md-3 col-md-3 contacts-info-sub">--}}
                                                        {{--<img src="/images/skype-icon-5.png">--}}
                                                        {{--<span>Skype</span>--}}
                                                        {{--<p name="lblSkypeId" class="setMUPAGELBL" >{{$user->skype_id}}</p>--}}
                                                        {{--<input name="tbxSkypeId" value="{{$user->skype_id}}" class="m-t-10 form-control setMOCKUPPAGEIFC" />--}}
                                                    {{--</div>--}}
                                                    {{--<div class="col-md-3 col-md-3 contacts-info-sub">--}}
                                                        {{--<img src="/images/MetroUI_Phone.png">--}}
                                                        {{--<span>{{ trans('common.phone') }}</span>--}}
                                                        {{--<p name="lblHandphoneNo" class="setMUPAGELBL" >{{$user->handphone_no}}</p>--}}
                                                        {{--<input name="tbxHandphoneNo" value="{{$user->handphone_no}}" class="m-t-10 form-control setMOCKUPPAGEIFC" />--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-md-2 col-md-2 contacts-info-edit">--}}
                                                {{--<div class="action pull-right">--}}
                                                    {{--<input type="checkbox"--}}
                                                           {{--class="make-switch form-control"--}}
                                                           {{--name="ckbIsContactAllowView"--}}
                                                           {{--id="ckbIsContactAllowView"--}}
                                                           {{--data-size="switch-size"--}}
                                                           {{--data-on-text="{{ trans('common.show') }}"--}}
                                                           {{--data-off-text="{{ trans('common.hide') }}"--}}
                                                           {{--data-on-color="success" data-off-color="danger"--}}
                                                            {{--{{ isset($user->is_contact_allow_view) && $user->is_contact_allow_view == 1?'checked' : '' }} >--}}
                                                {{--</div>--}}
                                                {{--<div>--}}
                                                    {{--<a href="javascript:;" class="action pull-right" style="padding-top: 5px;"--}}
                                                    {{--data-toggle="modal" data-target="#modalChangeUserInfo">--}}
                                                    {{--<i class="ti-pencil"></i> {{trans('common.edit')}}--}}
                                                    {{--</a>--}}
                                                    {{--<a href="javascript:;" class="action pull-right btnAction setMUPPAGEALINKBTN">--}}
                                                        {{--<i class="ti-pencil"></i> {{trans('common.edit')}}--}}
                                                    {{--</a>--}}
                                                    {{--<input type="hidden" id="hidAction" value="edit">--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            </div>

                        </div>
                        <div class="col-md-9">
                            {{--<div class="col-md-5 full-name">--}}
                            {{--{{ $user->getName() }}--}}

                            {{--@if( isset($userIdentity) && $userIdentity->isApproved() )--}}
                            {{--<i class="fa fa-check-circle"></i>--}}
                            {{--@endif--}}
                            {{--</div>--}}
                            {{--<div class="col-md-3 title_and_position">--}}
                            {{--<div>--}}
                            {{--<b>{{ $user->title }}</b>--}}
                            {{--</div>--}}
                            {{--<div >--}}
                            {{--@if( isset($user->job_position) )--}}
                            {{--<span class="job_position">{{$user->job_position->getName() }}</span>--}}
                            {{--@endif--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            <div class="col-md-8 setCol8UCPMD">
                                <div class="row setCol8MdRowUCP">
                                    <span class="full-name edit-link-trigger setMUPPAGESPANNAME">
                                       <span class="edit-target-status">{{ $user->name }}
                                        <a class="f-s-14 edit-link-show" href="javascript:;" data-toggle="modal" data-target="#modalChangeUserName">
                                                <i class="fa fa-pencil edit"></i>
                                            </a>
                                       </span>
                                        @if( isset($userIdentity) && $userIdentity->isApproved() )
                                            <i class="fa fa-check-circle set-fa-fa-fa-circle"></i>
                                        @endif
                                    </span>
                                        <span class="divider">
                                    </span>
                                        <span class="title_and_position">
                                        <span class="title edit-link-trigger">
                                            <b class="set-dropdown edit-target-status setTitleChangeUserTitle">
                                                {{ $user->title }}
                                             <a class="f-s-14 edit-link-show" href="javascript:;" data-toggle="modal" data-target="#modalChangeUsertitle">
                                                <i class="fa fa-pencil edit"></i>
                                            </a>
                                            </b>
                                        </span>
                                        <span class="position">
                                            @if( isset($user_job_position_id) )
                                                <span class="job_position set-dropdown" onclick="opendropPositions()">
                                                    {{$name}}
                                                </span>
                                            <b>
                                                <ul class="dropdown-content" style="padding: 0px;">
                                                    @foreach($position as $positions)
                                                        <li class="setHoveLi1" id="liIndex" onclick="position('{{$positions->id}}',event)">{{ $positions->getName() }}</li>
                                                    @endforeach
                                                    {{--<li class="setHoveLi1">{{trans('common.investor')}}<i class="fa fa-caret-up"></i></li>--}}
                                                    {{--<hr class="setNewhr1" >--}}
                                                    {{--<li>{{trans('common.designer')}}</li>--}}
                                                    {{--<hr class="setNewhr1">--}}
                                                    {{--<li>{{trans('common.software_engineer')}}</li>--}}
                                                    {{--<hr class="setNewhr1">--}}
                                                    {{--<li>{{trans('common.product_manager')}}</li>--}}
                                                    {{--<hr class="setNewhr1">--}}
                                                    {{--<li>{{trans('common.marketing')}}</li>--}}
                                                </ul>
                                            </b>
                                                </span>
                                            @endif
                                        </span>
                                    </span>
                                </div>

                            </div>
                            <div class="col-md-4 div-personal-info setBTNP">
                                <a href="{{ route('frontend.personalinformation', \Session::get('lang')) }} ">
                                    <button class="btn btn-personal-info setMUPBTNN2" type="button">
                                        <i class="fa fa-pencil"></i>
                                        {{trans('common.edit_info')}}
                                    </button>
                                </a>
                            </div>
                            <div class="row personal-info setMUPIPUINFOR">
                                <div class="col-md-8 col-sm-8 left-div setCOLMDSM8LDIV">
                                    <div class="row edit-link-trigger">
                                        <div class="col-md-3">
                                            <span class="label">{{trans('common.status')}} : </span>
                                        </div>
                                        <div class="col-md-6">
                                        <span class="edit-target-status">
                                            <span class="user-info status">{!!$user->explainStatus()!!}</span>
                                            <a class="f-s-14 edit-link-show" href="javascript:;" data-toggle="modal" data-target="#modalChangeUserStatus">
                                                <i class="fa fa-pencil edit"></i>
                                            </a>
                                        </span>
                                        </div>
                                        {{--<div class="col-md-3 text-right">--}}
                                        {{--<a class="f-s-14" href="javascript:;" data-toggle="modal" data-target="#modalChangeUserStatus">--}}
                                        {{--<i class="fa fa-user-secret"></i> {{trans('common.change')}}--}}
                                        {{--</a>--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="row edit-link-trigger">
                                        <div class="col-md-3">
                                            <span class="label">{{trans('common.rate')}} : </span>
                                        </div>
                                        <div class="col-md-6">
                                        <span class="edit-target">
                                            <span>
                                                {{--{{($user->currencySign)}} --}}
                                                {{ $user->getHourlyPay() }}
                                                <a class="f-s-14 edit-link-show" href="javascript:;" data-toggle="modal" data-target="#modalChangeUserRate">
                                                    <i class="fa fa-pencil edit"></i>
                                                </a>
                                                {{--/hr--}}
                                            </span>
                                        </span>
                                        </div>
                                        {{--<div class="col-md-3 text-right">--}}
                                        {{--<a class="f-s-14" href="javascript:;" data-toggle="modal" data-target="#modalChangeUserRate">--}}
                                        {{--<i class="fa fa-user-secret"></i> {{trans('common.change')}}--}}
                                        {{--</a>--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="row edit-link-trigger">
                                        <div class="col-md-3">
                                            <span class="label">{{trans('common.age')}} : </span>
                                        </div>
                                        <div class="col-md-6">
                                        <span class="edit-target">
                                            <span>{{$user->date_of_birth}}</span>
                                            <a class="f-s-14 edit-link-show" href="javascript:;" data-toggle="modal" data-target="#modalChangeUserAge">
                                                <i class="fa fa-pencil edit"></i>
                                            </a>
                                        </span>
                                        </div>
                                        {{--<div class="col-md-3 text-right">--}}
                                        {{--<a class="f-s-14" href="javascript:;" data-toggle="modal" data-target="#modalChangeUserAge">--}}
                                        {{--<i class="fa fa-user-secret"></i> {{trans('common.change')}}--}}
                                        {{--</a>--}}
                                        {{--</div>--}}
                                    </div>

                                    <div class="row edit-link-trigger">
                                        <div class="col-md-3">
                                            <span class="label">{{trans('common.experience')}} : </span>
                                        </div>
                                        <div class="col-md-6">
                                        <span class="edit-target">
                                            <span>{{$user->experience}} {{trans('member.year')}}</span>
                                            <a class="f-s-14 edit-link-show" href="javascript:;" data-toggle="modal"
                                               data-target="#modalChangeUserExperience">
                                                <i class="fa fa-pencil edit"></i>
                                            </a>
                                        </span>
                                        </div>
                                        {{--<div class="col-md-3 text-right">--}}
                                        {{--<a class="f-s-14" href="javascript:;" data-toggle="modal"--}}
                                        {{--data-target="#modalChangeUserExperience">--}}
                                        {{--<i class="fa fa-user-secret"></i> {{trans('common.change')}}--}}
                                        {{--</a>--}}
                                        {{--</div>--}}
                                    </div>

                                </div>
                                <div class="col-md-4 col-sm-4 setQRCMUP">
                                    <img src="{{ $user->getResumeQRCode() }}" class="qrcode" alt="" width="135px" height="135px">
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                            </div>
                        </div>
                    </div>
                <!-- <div class="row">
                    <div class="col-md-12">
                        <img class="profile-picture img-circle pull-left" src="{{ $user->getAvatar() }}" alt="" width="160px">
                        <span class="full-name pull-left">{{$user->real_name}} <i class="fa fa-check-circle"></i></span>
                        <br>
                        <br>
                    </div>
                </div> -->

                    <div class="clearfix"></div>
                    <div class="row profile-details">
                        <!-- <div class="row profile-info"> -->
                        <div class="col-md-12 edit-target-box">
                            <div class="label">
                                <img src="/images/info.png" alt=""><span>{{trans('common.info')}}</span>
                            </div>
                            <a href="javascript:;" class="action pull-right f-s-14 edit-target-link" data-toggle="modal"
                               data-target="#modalChangeUserAboutMe">
                                + {{trans('common.edit')}}
                            </a>
                            <div class="margin-25"></div>
                            <div class="clearfix"></div>
                            <div id="sectAboutMe">
                                {{ $user->getAboutMe() }}
                            </div>
                        </div>
                        <!-- </div> -->


                        <div class="col-md-12 edit-target-box">
                            <div class="label">
                                <img src="/images/hands.png" alt=""><span>{{trans('common.skill')}}</span>
                                {{--<a href="javascript:;" class="f-s-10" data-toggle="modal" data-target="#modelAddNewSkillKeyword" >--}}
                                {{--( {{ trans('common.help_us_improve_skill_keyword') }} )--}}
                                {{--</a>--}}
                            </div>
                            <a href="javascript:;" class="action pull-right f-s-14 edit-target-link" data-toggle="modal"
                               data-target="#modalChangeUserSkill">
                                + {{trans('common.add_new')}}
                            </a>
                            <!-- <div class="margin-30"></div> -->

                            <div class="clearfix"></div>
                            <div class="divSkills">
                            @foreach($userSkills as $userSkill)
                                {{--<i class="devicon-{{strtolower($skill->skill->name_en)}}-plain colored"></i>--}}

                                <div class="skill-div">
                                    @if ( $userSkill->getSkillVerifyStatus() == \App\Models\UserSkill::SKILL_UNVERIFIED )
                                        <span class="item-skill seetItemSkill unverified">
                                    <span>{{$userSkill->skill->getName()}}</span>
                                </span>
                                    @elseif ( $userSkill->getSkillVerifyStatus() == \App\Models\UserSkill::SKILL_ADMIN_VERIFIED )
                                        <span class="item-skill seetItemSkill admin-verified">
                                    <img src="/images/crown.png" alt="" height="24px" width="24px">
                                    <span>{{$userSkill->skill->getName()}}</span>
                                </span>
                                    @elseif ( $userSkill->getSkillVerifyStatus() == \App\Models\UserSkill::SKILL_CLIENT_VERIFIED )
                                        <span class="item-skill seetItemSkill client-verified">
                                    <img src="/images/checked.png" alt="" height="24px" width="24px">
                                    <span>{{$userSkill->skill->getName()}}</span>
                                </span>
                                    @endif
                                </div>
                            @endforeach
                            </div>
                        </div>
                        <div class="col-md-12 edit-target-box setETBMUPCol12">
                            <div class="label">
                                <img src="/images/earth.png" alt=""><span>{{trans('common.language_2')}}</span>
                            </div>
                            <a href="javascript:;" class="action pull-right f-s-14 edit-target-link" data-toggle="modal"
                               data-target="#modalChangeUserLanguage">
                                + {{trans('common.add_new')}}</a>
                            <!-- <div class="margin-10"></div> -->
                            <div class="clearfix"></div>
                            <br>
                            <div class="language_sect">
                                @foreach($user->languages as $lang)
                                @if(isset($lang->language))
                                    <div class="language">
                                        @php
                                         $code = strtoupper($lang->language->country->code);
                                        @endphp
                                        <img src="{{asset('images/Flags-Icon-Set/24x24/'.strtoupper($code).'.png')}}" alt="">
                                        <span>{{((Lang::locale() =='cn') && !empty($lang->language->language->name_cn))? $lang->language->language->name_cn: $lang->language->language->name}}</span>
                                        <i style="color:red;" class="fa fa-trash custom-set-language-trash-icon" aria-hidden="true" @click="deleteLanguage('{{$lang->id}}')"></i>
                                    </div>
                                @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row profile-info">
                    <!-- <div class="col-md-6 personal-portfolio-boxes">
                        <div class="label">
                            <img src="/images/info.png" alt=""><span>{{trans('common.info')}}</span>
                        </div>
                        <a href="javascript:;" class="action pull-right f-s-14" data-toggle="modal"
                           data-target="#modalChangeUserAboutMe">
                            + {{trans('common.edit')}}
                            </a>
                            <div class="margin-25"></div>
                            <div class="clearfix"></div>
                            <div id="sectAboutMe">
{{ $user->getAboutMe() }}
                            </div>
                        </div> -->
                        <div class="col-md-4">
                            <div class="label">
                                <i class="glyphicon glyphicon-star" aria-hidden="true"></i>
                                <span>{{ trans('common.rating') }}</span>
                            </div>
                            <div class="margin-25"></div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 text-center">
                                <span class="rating"> {{ $reviewInfo['averageRating'] }}</span>
                                <div class="star text-center">
                                    {!! $reviewInfo['ratingStar'] !!}
                                </div>
                                <span class="review-count">( {{ $reviewInfo['totalCountReview'] }} {{ trans('common.times_rating') }})</span>
                            </div>
                        </div>
                        <div class="col-md-8 graph-container">
                            <div class="graph">
                                <i class="fa fa-star"></i>
                                <span class="number">5</span>

                                <?php
                                $display_5_star = $reviewInfo['totalCount5Star'] == 0 ? "none" : "normal";
                                $display_4_star = $reviewInfo['totalCount4Star'] == 0 ? "none" : "normal";
                                $display_3_star = $reviewInfo['totalCount3Star'] == 0 ? "none" : "normal";
                                $display_2_star = $reviewInfo['totalCount2Star'] == 0 ? "none" : "normal";
                                $display_1_star = $reviewInfo['totalCount1Star'] == 0 ? "none" : "normal";
                                ?>

                                <div class="bar five-star setMUPPAGEFIVESTAR" style="width: {{ $reviewInfo['pct5Star'] }}%; display: {{ $display_5_star }};"></div> <!-- for graph-->
                                <span class="count">{{ $reviewInfo['totalCount5Star'] }}</span>
                            </div>
                            <div class="graph">
                                <div class="setMUPPAGEGRAPH">
                                    <i class="fa fa-star"></i>
                                    <span class="number">4</span>

                                    <div class="bar four-star" style="width:{{ $reviewInfo['pct4Star'] }}%; display: {{ $display_4_star }};"></div> <!-- for graph-->
                                </div>

                                <span class="count">{{ $reviewInfo['totalCount4Star'] }}</span>
                            </div>
                            <div class="graph">
                                <div class="setMUPPAGEGRAPH">
                                    <i class="fa fa-star"></i>
                                    <span class="number">3</span>
                                    <div class="bar three-star" style="width:{{ $reviewInfo['pct3Star'] }}%; display: {{ $display_3_star }};"></div> <!-- for graph-->
                                </div>
                                <span class="count">{{ $reviewInfo['totalCount3Star'] }}</span>
                            </div>
                            <div class="graph">
                                <i class="fa fa-star"></i>
                                <span class="number">2</span>
                                <div class="bar two-star" style="width:{{ $reviewInfo['pct2Star'] }}%; display: {{ $display_2_star }};"></div> <!-- for graph-->
                                <span class="count">{{ $reviewInfo['totalCount2Star'] }}</span>
                            </div>
                            <div class="graph">
                                <i class="fa fa-star"></i>
                                <span class="number">1</span>
                                <div class="bar one-star" style="width:{{ $reviewInfo['pct1Star'] }}%; display: {{ $display_1_star }};"></div> <!-- for graph-->
                                <span class="count">{{ $reviewInfo['totalCount1Star'] }}</span>
                            </div>
                            <div class="graph">
                                <div class="bar graph-alignment"></div>
                            </div>
                        </div>
                    </div>
                </section>

            @include('frontend.userCenter.comment', [
                'is_allow_edit_info' => true,
                'name' => $name,
                'reviews' => $reviews,
                'job_positions' => $job_positions,
                'user' => $user,
                'portfolios' => $portfolios
            ])
            <!-- END PAGE BASE CONTENT -->
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->

        {{--@include('frontend.userCenter.modalChangeUserStatus')--}}
        @include('frontend.userCenter.modalChangeUserSkill', ['isSkipButtonAllow' => false])
        @include('frontend.userCenter.modalChangeUserLanguage')
        @include('frontend.userCenter.modalChangeUserAboutMe', ['about_me' => $user->about_me])
        @include('frontend.userCenter.modalChangeUserAge')
        @include('frontend.userCenter.modalChangeUserRate')
        @include('frontend.userCenter.modalChangeUserName')
        @include('frontend.userCenter.modalChangeUsertitle')
        @include('frontend.userCenter.modalChangeUserExperience')
        {{--        @include('frontend.userCenter.modalChangeUserInfo')--}}
        @include('frontend.includes.modalAddNewSkillKeywordv2', [
            'tagColor' => \App\Models\Skill::TAG_COLOR_WHITE
        ])
    </div>
@endsection

@section('modal')
    <div id="modalCropImage" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
            </div>
        </div>
    </div>
@endsection

@push('js')
<script type="text/javascript">
    var url = {
        'getThumbnail': "{{ route('frontend.portfolio.getthumbnail') }}",
        'delete' : "{{ route('frontend.portfolio.delete') }}",
        'personalInformationChange' : '{{ route('frontend.personalinformation.change') }}',
        'closeNotice' : '{{ route('frontend.usercenter.closenotice') }}',
    }

    var lang = {
        'unable_to_request' : "{{ trans('common.unable_to_request') }}",
        'image_file_too_big' : "{{ trans('common.image_file_too_big') }}",
        'remove' : "{{ trans('common.remove') }}",
        'image_is_bigger_than_5mb' : "{{ trans('common.image_is_bigger_than_5mb') }}",
        'you_can_not_upload_any_more_files' : "{{ trans('common.you_can_not_upload_any_more_files') }}",
        'drop_files_here_to_upload' : "{{ trans('common.drop_files_here_to_upload') }}",
        'cancel_upload' : "{{ trans('common.cancel_upload') }}",
        'help_us_improve_skill_keyword' : "{{ trans('common.help_us_improve_skill_keyword') }}",
        'read_more' : "{{ trans('common.read_more') }}",
        'read_less' : "{{ trans('common.read_less') }}",
        'per_hour_pay' : "{{ trans('common.per_hour_pay') }}",
        'invalid_file_type_for_user_avatar' : "{{ trans('common.invalid_file_type_for_user_avatar') }}",
        'file_too_big' : "{{ trans('common.file_too_big') }}",
        'new_skill_keywords' : "{{ trans('common.new_skill_keywords') }}",
        'please_use_wiki_or_baidu_support_your_keyword' : "{{ trans('common.please_use_wiki_or_baidu_support_your_keyword') }}",
        'one_of_skill_input_empty' : "{{ trans('common.one_of_skill_input_empty') }}",
        'maximum_ten_skill_keywords_allow_to_submit' : "{{ trans('common.maximum_ten_skill_keywords_allow_to_submit') }}",
    }

    var arrUploadFile = {!!$jsonUploadFile!!};
    var fileUploadFeatureType = {{ \App\Models\UploadFile::TYPE_POST_PROJECT }};

    var skillList = {!! $skillList !!};
    var skill_id_list = '{{$_G['user']->getUserSkillList()}}';
</script>

<script type="text/javascript" src="{{asset('assets/global/plugins/lightbox2/dist/js/lightbox.min.js')}}" ></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/typeahead/typeahead.bundle.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-tagsinput-latest/dist/bootstrap-tagsinput.min.js')}}"></script>
<script type="text/javascript" src="/custom/js/frontend/modalAddNewSkillKeywordv2.js" defer="defer"  ></script>
<script type="text/javascript" src="/custom/js/frontend/portfolio.js"></script>
<script type="text/javascript" src="/assets/global/plugins/dropzone-4.0.1/dropzone.js" ></script>
<script type="text/javascript" src="/assets/global/plugins/jcrop/js/jquery.Jcrop.min.js" ></script>
<script type="text/javascript" src="/assets/global/plugins/readmore/readmore.min.js" ></script>
<script type="text/javascript" src="/assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js" ></script>
<script type="text/javascript" src="/custom/js/dropzone-config-portfolio.js" ></script>
<script type="text/javascript" src="/custom/js/frontend/modalAddUserSkill.js" defer="defer" ></script>
<script type="text/javascript" src="/custom/js/frontend/modalChangeUserStatus.js" defer="defer"></script>
<script src="/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script>
    function opendropPositions() {
        $('.dropdown-content').toggle();
    }
    function changePortfolioTab(id) {
        let locale = '{{getLocale()}}';
        window.location.href='/'+locale+'/resume?job_position='+id;
    }
</script>
<style>
    .select2-dropdown {
        z-index: 9999999;
    }
</style>
@endpush

@push('vue')
<script>

    Vue.http.headers.common['X-CSRF-TOKEN'] = $('input[name="_token"]').val();
    new Vue({
        el: "#userCenter",
        data: {
            user: user,
            skills: [],
            languages: [],
            username :user.name,
            f_name:'',
            first_name:'',
            last_name:'',
            experienceLoader:false,
            ageLoader:false,
            nameLoader:false,
            rateLoader:false,
            aboutMeLoader:false,
            addSkillLoader:false,
            languageLoader:false
        },
        methods: {
            deleteLanguage: function(id){
              console.log('datadata');
              console.log(id);
              let url = '/personalInformation/delete/'+id;
              this.$http.post(url).then((response) => {
                  console.log('response');
                  console.log(response);
                  window.location.reload();
                  $('.language_sect').html(response.data.data.language_sect_value);
              }).catch((error) => {
                  console.log('error');
                  console.log(error);
              })
            },
            getStringSplit()
            {
                this.f_name = this.username.split(" ");
                for( var i = 1; i <= this.f_name.length; i++){
                    var name = this.f_name[0];
                    var lname = this.f_name[1];
                }

                this.first_name = name;
                this.last_name = lname;
            },

            change: function (val) {
                if (val === 'info') {
                    this.aboutMeLoader = true;
                    var input = {
                        line_id: this.user['line_id'],
                        skype_id: this.user['skype_id'],
                        wechat_id: this.user['wechat_id'],
                        handphone_no: this.user['handphone_no'],
                        column: val,
                    }
                } else if (val === 'hourly_pay') {
                    this.rateLoader = true;
                    var hourly_pay = $('input[name="tbx_hourly_pay"]').val();
                    var input = {
                        hourly_pay: hourly_pay,
                        currency: this.user['currency'],
                        column: val,
                    }
                }  else if (val === 'date_of_birth') {
                    this.ageLoader = true;
                    var date_of_birth = $('input[name="date_of_birth"]').val();
                    var input = {
                        val: date_of_birth,
                        column: 'date_of_birth'
                    }
                } else if(val === 'name'){
                    this.nameLoader = true;
                    var b= $('#last_name').val();
                    var a = $('#first_name').val();
                    var c = a+' '+b;
                    var input = {
                        'val': c,
                        'column':val,
                    };
                } else if(val === 'about_me') {
                    this.aboutMeLoader = true;
                    var input = {
                        'val': $('.about_me').val(),
                        'column': 'about_me',
                    };
                } else if(val === 'title') {
                    var input = {
                        'val': $('.titleTxt').val(),
                        'column': 'title',
                    };
                } else if(val === 'experience') {
                    this.experienceLoader = true;
                    var input = {
                        'val': $('.experience').val(),
                        'column': 'experience',
                    };
                }
                this.$http.post('/personalInformation/change', input).then((response)=> {
                    window.location.reload();
                }, (response)=> {
                    //                   error
                });
            },
            changeSkill: function (val) {
                //                e.preventDefault();
                this.addSkillLoader = true;
                var input = {
                    'val': $('#skill_id_list').val(),
                    'column': val,
                };
                this.$http.post('/personalInformation/change', input).then((response)=> {
                    window.location.reload();
            }, (response)=> {
                    console.log('error');
                });
            },
            changeLanguage: function (val) {
                //                e.preventDefault();
                this.languageLoader = true;
                var input = {
                    'val': $('#lang_id_list').val(),
                    'column': val,
                };
                this.$http.post('/personalInformation/change', input).then((response)=> {
                                       window.location.reload();
                    $('#modalChangeUserLanguage').modal('toggle');
                $('.language_sect').html(response.data.data.language_sect_value);
            }, (response)=> {
                    //                   error
                });
            }
        },
        mounted()
        {
            this.getStringSplit();
        }
    });


</script>

<script src="{{asset('custom/js/contactInfo.js')}}"></script>

<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            todayHighlight: true,
            autoclose: true,
        });

        try {
            //language
            var languages = '{!! $langList !!}'
            var lan = JSON.parse(languages);
            // console.log(languages);
            var lang_id_list = '{{$_G['user']->getUserLanguageList()}}';
            var selectedlang = [];
            if (lang_id_list != "") {
                selectedlang = lang_id_list.split(",")
            }
        } catch (e) {
            console.log(e);
        }

        $('#lang_id_list').select2({
          data: lan
        });

        var lms = $('#languageInput').magicSuggest({
            allowFreeEntries: false,
            allowDuplicates: false,
            data: languages,
            value: selectedlang,
            method: 'get',
            renderer: function (data) {
	            console.log(data);
	            return '<img src="images/Flags-Icon-Set/24x24/' + data.code.toUpperCase() + '.png" />' + data.name;
            },
            resultAsString: false,
            selectionRenderer: function (data) {
                return '<img src="images/Flags-Icon-Set/24x24/' + data.code.toUpperCase() + '.png" />' + data.name;
            }
        });

        $(lms).on(
            'selectionchange', function (e, cb, s) {
                $('#lang_id_list').val(cb.getValue());
            }
        );

        $('.profile-picture').click(function(){
            $('.uploadAvatar').trigger('click');
        });

        $(".uploadAvatar").change(function (){
            var fileName = $(this).val();

            var file = this.files[0];
            name = file.name;
            size = file .size;
            type = file.type;

            if(file.name.length < 1)
            {
                return;
            }
            else if(file.size > 5000000)
            {
                alert(lang.file_too_big);
            }
            else if(file.type != 'image/png' && file.type != 'image/jpg' && file.type != 'image/gif' && file.type != 'image/jpeg' )
            {
                alert(lang.invalid_file_type_for_user_avatar);
            }
            else
            {
                var formData = new FormData($('#form-avatar')[0]);
                App.blockUI({
                    animate: true
                });
                $.ajax({
                    url: '/personalInformation/updateAvatar',
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: formData,
                    error: function (err) {
                        console.log(err);
                        alert(lang.unable_to_request);
                    },
                    success: function (response) {
                        if(response.status == false) {
                            alertError(response.message)
                            setTimeout(function(){
                                window.location.reload();
                            }, 1000)
                        } else {
                            alertSuccess(response.message)
                            setTimeout(function() {
                                window.location.reload();
                            })
                        }
//						App.blockUI({
//							animate: false
//						});
                    }
                });
            }

        });

        $('#ckbIsContactAllowView').bootstrapSwitch();

        $('#ckbIsContactAllowView').on('switchChange.bootstrapSwitch', function (event, state) {
            var is_contact_alow_view = 0;
            //true = show, false = hide
            if (state == true) {
                is_contact_alow_view = 1;
            }
            else {
                is_contact_alow_view = 0;
            }

            $.ajax({
                url: '/personalInformation/updateContactView',
                type: 'POST',
                dataType: 'json',
                data: {user_id: user.id, isContactAllowView: is_contact_alow_view},
                error: function () {
                    alert('Unable right now, please try again later');
                },
                success: function (response) {
                }
            });
        });

        $('.btnAction').on('click', function(){
            var status = $('#hidAction').val();
            if( status == 'edit' )
            {
                $('p[name="lblQQId"]').attr('style', 'display:none');
                $('input[name="tbxQQId"]').attr('style', 'display:block');

                $('p[name="lblWechatId"]').attr('style', 'display:none');
                $('input[name="tbxWechatId"]').attr('style', 'display:block');

                $('p[name="lblSkypeId"]').attr('style', 'display:none');
                $('input[name="tbxSkypeId"]').attr('style', 'display:block');

                $('p[name="lblHandphoneNo"]').attr('style', 'display:none');
                $('input[name="tbxHandphoneNo"]').attr('style', 'display:block');

                var content = '<i class="ti-pencil"></i>' + '{{ trans('common.confirm') }}';
                $('.btnAction').html(content);
                $('#hidAction').val('save');
            }
            else if( status == 'save' )
            {
                var line_id = $('input[name="tbxQQId"]').val();
                var wechat_id = $('input[name="tbxWechatId"]').val();
                var skype_id = $('input[name="tbxSkypeId"]').val();
                var handphone_no = $('input[name="tbxHandphoneNo"]').val();

                $.ajax({
                    url: url.personalInformationChange,
                    dataType: 'json',
                    data: {
                        column: 'info',
                        line_id: line_id,
                        wechat_id: wechat_id,
                        skype_id: skype_id,
                        handphone_no: handphone_no
                    },
                    method: 'post',
                    error: function () {
                        alert(lang.unable_to_request);
                    },
                    success: function (result) {
                        if(result.status == 'success')
                        {
                            $('p[name="lblQQId"]').html(line_id);
                            $('p[name="lblWechatId"]').html(wechat_id);
                            $('p[name="lblSkypeId"]').html(skype_id);
                            $('p[name="lblHandphoneNo"]').html(handphone_no);

                            $('p[name="lblQQId"]').attr('style', 'display:block');
                            $('input[name="tbxQQId"]').attr('style', 'display:none');

                            $('p[name="lblWechatId"]').attr('style', 'display:block');
                            $('input[name="tbxWechatId"]').attr('style', 'display:none');

                            $('p[name="lblSkypeId"]').attr('style', 'display:block');
                            $('input[name="tbxSkypeId"]').attr('style', 'display:none');

                            $('p[name="lblHandphoneNo"]').attr('style', 'display:block');
                            $('input[name="tbxHandphoneNo"]').attr('style', 'display:none');

                            var content = '<i class="ti-pencil"></i>' + '{{ trans('common.edit') }}';
                            $('.btnAction').html(content);
                            $('#hidAction').val('edit');
                        }
                    }
                });


            }
        });

        $('#sectAboutMe').readmore({
            moreLink: '<a href="#" class="f-c-green2">' + '{{trans('common.read_more')}}' + '</a>',
            lessLink: '<a href="#" class="f-c-green2">' + '{{trans('common.read_less')}}' + '</a>',
            speed: 500,
            collapsedHeight: 50
        });

        var delay = (function(){
            var timer = 0;
            return function(callback, ms){
                clearTimeout (timer);
                timer = setTimeout(callback, ms);
            };
        })();
        $(document).on('keyup', 'input[name="tbx_hourly_pay"]', function(e){
            // if(e.keyCode == 8) //backspace keypress code
            // {
            //     var value = parseFloat($('input[name="tbx_hourly_pay"]').val());
	        //     console.log(value);
	        //     var reduceValue = Math.floor(value / 10);
            //     if( reduceValue < 0 )
            //     {
            //         reduceValue = 0;
            //     }
            //     $('input[name="tbx_hourly_pay"]').val(reduceValue);
            // }

            $('#divCalculateMessage').show();
            // delay(function(){
                var hourlyPay = parseFloat($('input[name="tbx_hourly_pay"]').val());
                if(isNaN(hourlyPay) )
                {
                    hourlyPay = 0;
                }

                var userCurrency = $('#currency').val();
                convertCurrency('USD', userCurrency, function(c){
                    console.log("user-curruncy-get");
                    console.log(c);
                    console.log("user-curruncy-get");
                    var monthlyPay = hourlyPay * 176;
                    var getmonthly  = monthlyPay * c;
                    console.log(getmonthly);
                    $('input[name="tbx_monthly_pay"]').val(getmonthly.toFixed(2));
                    // $('input[name="hourly_pay"]').val(hourlyPay.toFixed(2))
                    $('#divCalculateMessage').hide();
                });
	            // $('input[name="tbx_monthly_pay"]').val(monthlyPay.toFixed(2));

                var hourlyPayMessage = hourlyPay + '{{trans('common.per_hour_pay')}}';
                // $('input[name="tbx_hourly_pay"]').val(hourlyPayMessage)

            // }, 200 );
        });

        $(document).on('keyup', 'input[name="tbx_monthly_pay"]', function(){
            $('#divCalculateMessage').show();
            var monthlyPay = parseFloat($('input[name="tbx_monthly_pay"]').val());
            if( monthlyPay > 0 )
            {
                var hourlyPay = monthlyPay / 176;

                var currency = $('#currency').val();
                var c = convertCurrency(currency,'USD', function(c){
                    console.log('in c');
                    console.log(c);
                    var amount = c * hourlyPay;
                    var hourlyPayMessage = amount.toFixed(2) + '{{trans('common.per_hour_pay')}}';
                    $('input[name="tbx_hourly_pay"]').val(hourlyPayMessage);
                    $('input[name="hourly_pay"]').val(hourlyPay.toFixed(2));
                });

            }
            else
            {
                $('input[name="tbx_hourly_pay"]').val('0');
                $('input[name="hourly_pay"]').val('0');
            }
            $('#divCalculateMessage').hide();
        });

        $('.notice .close').click(function()
        {
            $.ajax({
                url: url.closeNotice,
                type: 'POST',
                dataType: 'json',
                data: {},
                error: function () {
                    alert(lang.unable_to_request);
                },
                success: function (response) {
                    $('.notice').hide();
                }
            });

        });

        $('.edit-link-trigger').mouseenter(function(){
            $(this).find('.edit-link-show').show();
            $(this).find(".edit-target").addClass('edit-target-info');
            $(this).find(".edit-target-status").addClass('edit-target-status-hover')
        });

        $('.edit-link-trigger').mouseenter(function(){
            $(this).find('.edit-link-show').show();
            $(this).find(".edit-target").addClass('edit-target-info');
            $(this).find(".edit-target-status").addClass('edit-target-status-hover');
        });

        $('.edit-link-trigger').mouseleave(function(){
            $(this).find('.edit-link-show').hide();
            $(this).find(".edit-target").removeClass('edit-target-info');
            $(this).find(".edit-target-status").removeClass('edit-target-status-hover')
        });

        $(".edit-target-link").hide();

        $('.edit-target-box').mouseenter(function(){
            $(this).addClass("personal-portfolio-boxes");
            $(this).find(".edit-target-link").show();
        });

        $('.edit-target-box').mouseleave(function(){
            $(this).removeClass("personal-portfolio-boxes");
            $(this).find(".edit-target-link").hide();
        });
    });

    function position(data,event){
        event.preventDefault();
        var url = '/job-position/userCenter/'+data;
        $.ajax({
            headers: {
                '_token': '{{csrf_token()}}'
            },
            type:'post',
            url:url,
            success:function(data){
                console.log(data);
                window.location.reload();
            }
        });
    }

    var https = require('https');
    function convertCurrency(fromCurrency, toCurrency, cb)
    {
        // var monthlyPay = parseFloat($('input[name="tbx_monthly_pay"]').val());
        var url = '/currency';
        $.ajax({

            type:'get',
            data:{
                from: fromCurrency,
                to: toCurrency
            },
            url:url,
            success: function(response)
            {
                console.log(JSON.parse(response));
                var resp = JSON.parse(response);
                console.log(typeof resp);
                console.log('----');
                var a = Object.keys(resp).map(k => resp[k])
                cb(a[0]);
            }
        });

    }
</script>
@endpush
