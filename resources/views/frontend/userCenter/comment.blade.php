<section class="div-container well main-well-custom setRCPDCCol">
    <div class="row">
        <div class="col-md-12">
            <div class="tabbable-panel">
                <div class="tabbable-line">
                    <ul class="nav nav-tabs ">
                        @if( \Request::route()->getName() == 'frontend.usercenter' )
                        <li class="active">
                            <a href="#tab_portfolio" data-toggle="tab" class="f-s-16">{{ trans('common.personal_portfolio') }} ({{ count($portfolios) }})</a>
                        </li>
                        <li>
                            <a href="#tab_admin_review" data-toggle="tab" class="f-s-16">{{ trans('common.official_comment') }} ({{ $adminReviews->count() }})</a>
                        </li>
                        <li>
                            <a href="#tab_review" data-toggle="tab" class="f-s-16">{{ trans('common.comment') }} ({{ count($reviews) }})</a>
                        </li>
                        @elseif( \Request::route()->getName() == 'frontend.joindiscussion.applicantdetail' ||
                            \Request::route()->getName() == 'frontend.resume.getdetails' )
                        <li class="">
                            <a href="#tab_admin_review" data-toggle="tab" class="f-s-16">{{ trans('common.official_comment') }} ({{ $adminReviews->count() }})</a>
                        </li>
                        <li class="">
                            <a href="#tab_portfolio" data-toggle="tab" class="f-s-16">{{ trans('common.personal_portfolio') }} ({{ count($portfolios) }})</a>
                        </li>
                        <li>
                            <a href="#tab_review" data-toggle="tab" class="f-s-16">{{ trans('common.comment') }} ({{ count($reviews) }})</a>
                        </li>
                        @else
                        <li class="active">
                            <a href="#tab_admin_review" data-toggle="tab" class="f-s-16">{{ trans('common.official_comment') }} ({{ $adminReviews->count() }})</a>
                        </li>
                        <li>
                            <a href="#tab_review" data-toggle="tab" class="f-s-16">{{ trans('common.comment') }} ({{ count($reviews) }})</a>
                        </li>
                        <li class="">
                            <a href="#tab_portfolio" data-toggle="tab" class="f-s-16">{{ trans('common.personal_portfolio') }} ({{ count($portfolios) }})</a>
                        </li>
                        @endif
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active {{ \Request::route()->getName() == 'frontend.joindiscussion.applicantdetail' || \Request::route()->getName() == 'frontend.resume.getdetails'? 'active': '' }}" id="tab_admin_review">
                            @if(!empty($adminReviews) && $adminReviews->count())
                                <div class="review">

                                    @foreach( $adminReviews as $key => $adminReview )
                                    <div class="row review-main">
                                        <div class="col-md-2 review-main-l text-left">
                                            <img src="{{ $adminReview->staff->getAvatar() }}" onerror="this.src='/images/image.png'" alt="..." class="img-circle" width="90px" height="90px">
                                        </div>
                                        <div class="col-md-10 review-main-r">
                                            <div>
                                                <div class="text-right"><small class="date">{{ $adminReview->created_at }}</small></div>
                                                <span class="full-name">{{ $adminReview->staff->username }}
                                                    <span class="fa fa-check-circle"></span>
                                                    @if(!empty($adminReview->staff->title))
                                                        <span class="user-title setCOMMENTPAGEUT">{{ $adminReview->staff->title }}</span>
                                                    @endif
                                                </span>
                                            </div>
                                            @if($adminReview->official_project_id > 0 )
                                            <span class="project-id">{{ trans('common.project_Id') }} : {{ $adminReview->official_project_id }}</span><br>
                                            @endif

                                            <br>

                                            {!! \App\Constants::getScoreOverallHtml($adminReview->score_overall) !!}

                                            <blockquote style="word-wrap: break-word">
                                                “{{ $adminReview->comment }}”
                                            </blockquote>

                                            <div class="skill-tag set">
                                                <label class="label-title-sub f-s-14">{{ trans('common.project_needed_skill') }}</label>

                                                @foreach($adminReview->skills as $skill )
                                                <div class="skill f-s-14 item-skill tag setSKILLFS14">
                                                    <span>{{ $skill->skill->getName() }}</span>
                                                </div>
                                                @endforeach
                                            </div>
                    <i class="setCOMMENTPAGEGITHUB" onclick="window.open('{{$adminReview->github_link}}')"></i>

                                            <div class="attachment">
                                                @if( $adminReview->files->count() != 0)
                                                <label class="label-title-sub f-s-12">{{ trans('common.attachment') }}</label>


                                                <a href="downloadDocuments?adminReviewID={{ $adminReview->id }}" target="_blank">
                                                    <div class="attachment-file-download f-s-12 setTRCPPFDAF">
                                                        <label>{{ trans('common.download_all') }} ({{ $adminReview->files->count() }})</label>
                                                    </div>
                                                </a>
                                                @endif
                                                @if(isset($adminReview->video_name))
                                                    <div class="attachment-sub" onclick="window.location='/uploads/videos/{{$adminReview->video_name}}'">
                                                        <div class="attachment-file">
                                                            <img class="img-popup-full" src="{{asset('uploads/videos/').'/'.$adminReview->video_thumb}}" onerror="this.src='https://image.flaticon.com/icons/svg/526/526510.svg'">
                                                        </div>

                                                    </div>
                                                @endif
                                                @foreach($adminReview->files as $file)
                                                <div class="attachment-sub setASRCPP">
                                                    <div class="attachment-file">
                                                        @if($file->thumbnail_file)
                                                            <img class="img-popup" src="{{ url('/') }}/{{ $file->thumbnail_file }}">
                                                        @else
                                                            <img class="img-popup" src="{{ url('/') }}/{{ $file->path }}">
                                                        @endif
                                                            <img class="img-popup-full hide" src="{{ url('/') }}/{{ $file->path }}">
                                                    </div>

                                                </div>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>

                                    @if( $adminReviews->count() - 1 != $key )
                                    <hr>
                                    @endif
                                    @endforeach

                                </div>
                            @else
                                <div class="personal-portfolio-empty">
                                    <div class="row">
                                        <div class="col-md-12 uppercase">
                                            <h5>{{ trans('common.user_havent_got_official_review') }}</h5>
                                            <img src="/images/icons/potfolio-emptys.png">
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="tab-pane" id="tab_review">
                            @if(!empty($reviews) && $reviews->count())
                                <div class="review">
                                    @foreach( $reviews as $review  )
                                    <div class="row review-main">
                                        <div class="col-md-2 review-main-l text-left">
                                            @if ( $review->reviewer_user != null )
                                            <img src="{{ $review->reviewer_user->getAvatar() }}" alt="" class="img-circle" width="90px" height="90px">
                                            @elseif ( $review->reviewer_staff != null )
                                            <img src="{{ $review->reviewer_staff->getAvatar() }}" alt="" class="img-circle" width="90px" height="90px">
                                            @endif
                                        </div>
                                        <div class="col-md-10 review-main-r">
                                            <div>
                                                <span class="full-name">
                                                    @if ( $review->reviewer_user != null )
                                                    {{ $review->reviewer_user->getName() }}
                                                    @elseif ( $review->reviewer_staff != null )
                                                    {{ $review->reviewer_staff->name }}
                                                    @endif

                                                    @if( (isset($review->reviewer_user) && $review->reviewer_user->getLatestIdentityStatus() == \App\Models\UserIdentity::STATUS_APPROVED ) ||
                                                        isset($review->reviewer_staff)
                                                     )
                                                    <span class="fa fa-check-circle"></span>
                                                    @endif
                                                    <small class="date"> {{ $review->created_at }}</small>
                                                </span>
                                            </div>
                                            <span class="project-id">{{ trans('common.project') }} : {{ $review->project->name }}</span><br>
                                            <br>
                                            {!! $review->getYellowRateStar()  !!}

                                            <blockquote>
                                                “{{ $review->experience }}“
                                            </blockquote>

                                            <div class="label-custom">
                                                @foreach( $review->reviewSkills as $reviewSkill)
                                                <label class="label-color">
                                                    <span>
                                                        {{ $reviewSkill->skill->getName() }}
                                                    </span>
                                                </label>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    @endforeach
                                </div>
                            @else
                                <div class="personal-portfolio-empty">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5>{{ trans('common.user_havent_got_review') }}</h5>
                                            <img src="/images/icons/potfolio-emptys.png">
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="tab-pane personal_portfolio_tab {{ \Request::route()->getName() == 'frontend.usercenter'? 'active': '' }}" id="tab_portfolio">

                            @if( $is_allow_edit_info == true)
                            <div class="row menu">
                                <div class="col-md-12">
                                    <ul>
                                        @foreach( $job_positions as $job_position )
                                        <li class="{{ isset($user) && $user_job_position_id == $job_position->id? 'active': '' }}">
                                            <a data-id="{{ $job_position->id }}" onclick="changePortfolioTab({{$job_position->id}})" name="job-position" href="javascript:;" class="f-s-16">
                                                {{ $job_position->name_en }}
                                            </a>
                                        </li>
                                        @endforeach

                                    </ul>
                                </div>
                            </div>
                            @endif

                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_default_33">
                                    @if( $is_allow_edit_info == true)
                                    <div class="row add-more-pro">
                                        <div class="col-md-12">
                                            {!! Form::open(['route' => 'frontend.portfolio.create.post', 'id' => 'create-portfolio-form', 'method' => 'post', 'class' => 'horizontal-form setFRCP', 'files' => true]) !!}
                                                <div id="portfolio-alert-container">
                                                </div>

                                                <div class="form-group">
                                                    <label for="title">{{ trans('common.project_title')}}</label>
                                                    <input id="title" name="title" type="text" class="form-control" value="" placeholder="{{ trans('common.example_project_title') }}">
                                                </div>

                                                <div class="form-group">

                                                    <div class="dropzone" id="portfolio-dropzone">
                                                        <div class="fallback">
                                                            <input name="file" type="file" multiple />
                                                        </div>
                                                    </div>
                                                    <div id="file-sect"></div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="link">{{ trans('common.project_link')}}</label>
                                                    <input id="url" name="url" type="text" class="form-control" value="" placeholder="www.behance.com/legendlair">
                                                </div>

                                                <div style="display:flex;justify-content:center;" class="form-group form-group-btn">
                                                    <button style="display:flex;justify-content:center;align-items:center;" id="btn-submit" type="button" class="btn btn-white-bg-green-text">
                                                    <span class="idSpanPortfolioSubmit">{{ trans('common.submit')}}</span>
                                                    <div style="display: none;" class="lds-ring lds-ring-portfolio">
                                                        <div></div><div></div><div></div><div></div>
                                                    </div>
                                                    </button>
                                                </div>

                                                <input id="job_position_id" name="job_position_id" type="hidden" value="{{ $user_job_position_id }}"/>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                    @endif

                                    <div id="portfolio-list" class="row project-list">
                                        @if( count($portfolios) != 0 )
                                        <div class="col-md-12 title"><h4>{{ $name }}</h4></div>
                                        @endif
                                        @if($portfolios && count($portfolios) != 0)
                                        @foreach( $portfolios as $key => $portfolio)
                                        <div id="portfolio-sect-{{ $portfolio->id }}">
                                            <div class="col-md-12 sub-title">
                                                @if( $is_allow_edit_info == true)
                                                <div data-id="{{ $portfolio->id }}" class="btn-delete-portfolio pull-right p-t-10">
                                                    <i class="fa fa-trash"></i>
                                                </div>
                                                @endif
                                                <h4>{{ $portfolio->title }}</h4>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="row">
                                                    @foreach( $portfolio->files as $uploadFile)
                                                        @if( $uploadFile->isImage() == true )

                                                            <div class="col-md-4 setRCPCol4AI">
                                                                <a href="{{$uploadFile->getThumbnailFile()}}" data-lightbox="portfolio">
                                                                    <div class="link-img">
                                                                        <img id="portfolio-img-{{ $uploadFile->id }}" class="img-popup-thumb" src="{{ $uploadFile->getThumbnailFile()}}">

                                                                    </div>
                                                                </a>
{{--                                                                <a href="{{ route('frontend.modal.cropImage', ['type' => "portfolio", 'id' => $uploadFile->id ] ) }}"--}}
{{--                                                                    data-toggle="modal"--}}
{{--                                                                    data-target="#modalCropImage">--}}
{{--                                                                    @if( \Auth::check() && $portfolio->user_id == \Auth::guard('users')->user()->id )--}}
{{--                                                                    <div class="tools">--}}
{{--                                                                        <i class="fa fa-scissors"></i>--}}
{{--                                                                    </div>--}}
{{--                                                                    @endif--}}
{{--                                                                </a>--}}
                                                            </div>
                                                        @elseif( $uploadFile->isPdfFile() == true )
                                                            <div class="col-md-4">
                                                                <a href="{{ $uploadFile->file }}" target="_blank">
                                                                    <div class="link-doc">
                                                                        <img src="{{ $uploadFile->getFileTypeImage() }}">
                                                                        <br>
                                                                        <span>{{ $uploadFile->getShortFilename() }}</span>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        @else
                                                            <div class="col-md-4">
                                                                <a
                                                                    href="/download?filename={{ $uploadFile->file }}"
                                                                    data-title="{{ trans('common.you_sure_want_to_download') }}"
                                                                    data-toggle="confirmation"
                                                                    data-placement="bottom"
                                                                    data-btn-ok-label="{{ trans('common.yes') }}"
                                                                    data-btn-ok-class="btn-success"
                                                                    data-btn-cancel-label="{{ trans('common.no') }}"
                                                                    data-btn-cancel-class="btn-danger" >
                                                                    <div class="link-doc">
                                                                        <img src="{{ $uploadFile->getFileTypeImage() }}">
                                                                        <br>
                                                                        <span>{{ $uploadFile->getShortFilename() }}</span>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        @endif
                                                    @endforeach

                                                </div>
                                            </div>

                                            <div class="col-md-12 project-link setProjectLPR">
                                                <a href="{{ $portfolio->getUrl() }}" target="_blank" class="link">
                                                    {{ $portfolio->getUrl() }}
                                                    <img src="/images/icons/link7-copy.png">
                                                </a>
                                            </div>

                                            <div class="col-md-12 simale-line"></div>
                                        </div>
                                        @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>

                            @if( count($portfolios) == 0 )
                                <div class="personal-portfolio-empty">
                                    <div class="row">
                                        <div class="col-md-12">
{{--                                            <h5>{{ trans('common.user_no_portfolio') }}</h5>--}}
{{--                                            <img src="/images/icons/potfolio-emptys.png">--}}
                                            <img style="width:5%;height:auto" src="/images/icons/bag.png"><span class="addColorClass" style="color:#555;">{{trans('common.have_not_upload')}}</span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


{{-- popup --}}
<div class="modal fade" id="img-popup-tmp" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content text-center">
            <div class="modal-body">
                <img class="img-responsive center-block" src="">
            </div>
        </div>
    </div>
</div>
