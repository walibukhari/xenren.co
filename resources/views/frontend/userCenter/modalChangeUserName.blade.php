<!-- Modal -->
<div id="modalChangeUserName" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="border-radius: 2px !important;">
            {!! Form::open(['route' => 'frontend.personalinformation.change', 'id' => 'change-user-detail-form', 'method' => 'post', 'class' => 'form-horizontal']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    {{trans('common.name')}}
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div  class="col-md-12">
                        <div class="col-md-6">
                            <label>{{trans('member.first_name')}}:</label>
                            <input type="text" class="form-control" name="name" id="first_name" v-model="this.first_name">
                        </div>
                        <div class="col-md-6">
                            <label>{{trans('member.last_name')}}:</label>
                            <input type="text" class="form-control" name="last_name" id="last_name" v-model="this.last_name">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer"
                 style="display: flex;
                 justify-content: flex-end;"
            >
                <button id="btn-change-status-submit"
                        style="width:20%;
                        display: flex;
                        align-items: center;
                        justify-content: center;"
                        type="button" class="btn btn-success m-b-0 set-ModalChangeUserName"
                        v-on:click="change('name')">
                    <span v-if="!nameLoader">{{ trans('member.confirm_add') }}</span>
                    <div v-if="nameLoader" class="lds-ring-name lds-ring">
                        <div></div><div></div><div></div><div></div>
                    </div>
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    {{ trans('member.cancel') }}
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

