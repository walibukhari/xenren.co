<style>
    /*.ms-ctn {*/
        /*border-radius: 3%;*/
    /*}*/
    .btn-red-bg-white-text, .btn-red-bg-white-text:hover, .btn-red-bg-white-text:active {
        background-color: #d9534f;
        color: #fff;
        border: 2px solid #d9534f;
        min-width: 150px;
        padding: 9px 0px;
        border-radius: 5px;
    }
    #msSkills{
        width: 100%;
         height: auto;
        border-radius: 4px !important;
        box-shadow: 0px 4px 5px -1px #efefef !important;
        padding: 3px !important;
        border: 1px solid #e8e8e8;
    }
    .btn-green-bg-white-text, .btn-green-bg-white-text:hover, .btn-green-bg-white-text:active {
        background-color: #57b029 !important;
        color: #fff;
        border: 2px solid #57b029 !important;
        min-width: 150px;
        padding: 9px 0px;
        border-radius: 5px;
        top: 0px;
    }
    .ms-ctn-focus, #magicsuggest:hover, #languageInput:hover {
        border-color: #64B12B !important;
    }
    .modal .modal-header {
        border-bottom: 1px solid #EFEFEF;
    }
    #modalChangeUserSkill h4 {
        font-size: 18px;
    }
    .ms-ctn {
        padding: 8px 12px;
    }
    #modalChangeUserSkill .btn {
        margin-right: 0px;
        margin-bottom: 0px;
        font-size: 12px;
        height: 40px;
        width: 20%;
    }
</style>
<!-- Modal -->
<div id="modalChangeUserSkill" class="modal fade homeXenren" role="dialog" style="z-index: 9999999" >
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content setModalChangeSkillContent" style="border-radius: 2px !important;margin-top: 100px;">
            {!! Form::open(['route' => 'frontend.personalinformation.addskill', 'id' => 'add-user-skill-form', 'method' => 'post', 'class' => 'form-horizontal']) !!}
            <div class="modal-header">
                <a  class="setCloseChangeuserSkill" data-dismiss="modal">
                    <img src="{{asset('images/closeImage.png')}}" style="float: right">
                </a>
                <h4 class="modal-title setmodalTitlcus">
                    {{trans('common.add_skill')}}
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-body">
                    <div class="form-group">
                        <div class="col-md-12" id="add-user-status-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            {{trans('common.skill')}}
                        </label>
                        <div class="col-md-9">
                            <div id="msSkills"></div>
                            @if( $isSkipButtonAllow == true )
                                {{ Form::hidden('skill_id_list', $skillList, ['id'=>'skill_id_list']) }}</div>
                            @else
                                {{ Form::hidden('skill_id_list', $_G['user']->getUserSkillList(), ['id'=>'skill_id_list']) }}</div>
                            @endif
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-md-offset-1">
                <label class="setCUSLabel">{{trans('common.didnot_found_keyword')}}.</label>
            </div>
            <div class="modal-footer setModalFooterCUS"
                 style="display: flex;
                 justify-content: flex-end;"
            >
                @if( $isSkipButtonAllow == true )
                <button type="button" class="btn btnSkip btn-yellow-bg-white-text changeColor" style="border-radius: 5px;">
                    {{ trans('common.skip') }}
                </button>
                @endif
                <button type="button" class="btn btn-red-bg-white-text" data-dismiss="modal">
                    {{ trans('member.cancel') }}
                </button>
                <button type="button"
                        style="width:20%;
                        display: flex;
                        align-items: center;
                        justify-content: center;"
                        class="btn btnChangeSkill btn-green-bg-white-text" @click="changeSkill('skill')">
                    <span v-if="!addSkillLoader">{{ trans('common.save_change') }}</span>
                    <div v-if="addSkillLoader" class="lds-ring-experience lds-ring">
                        <div></div><div></div><div></div><div></div>
                    </div>
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

