<div class="col-md-12 personal-info">
    <div class="row personal-profile">
        <div class="profile-image col-md-2">
            <div class="profile-userpic">
                <img src="{{ $user->getAvatar() }}" class="img-responsive" alt="" style="width: 100px; height: 100px; "/>
            </div>
        </div>
        <div class="user-info col-md-7">
            <h1>Adam John<span class="fa fa-check-circle"></span></h1>
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-3">
                            <small>Rate:</small>
                        </div>
                        <div class="col-md-6">
                            <span class="details">$10.00/hr</span>
                        </div>
                        <div class="col-md-3">
                            <small>change</small>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <small>Rate:</small>
                        </div>
                        <div class="col-md-6">
                            <span class="details">$10.00/hr</span>
                        </div>
                        <div class="col-md-3">
                            <small>change</small>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <small>Rate:</small>
                        </div>
                        <div class="col-md-6">
                            <span class="details">$10.00/hr</span>
                        </div>
                        <div class="col-md-3">
                            <small>change</small>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <small>Rate:</small>
                        </div>
                        <div class="col-md-6">
                            <span class="details">$10.00/hr</span>
                        </div>
                        <div class="col-md-3">
                            <small>change</small>
                        </div>
                    </div>
                </div>
                <div class="user-info col-md-4">
                    <img src="/xenren-assets/images/icon/qrcode.png" class="img-auth" alt="" width="100px" height="100px">
                </div>
            </div>
        </div>

    </div>

    <div class="row personal-profile">
        <div class="col-md-6">
            <span>Skills</span> <a href="">+Add new</a>
            <br>
            <div class="skill-div">
                <span class="item-skill official"> <img src="/images/crown.png" alt=""> <span>HTML</span></span>
            </div>
            <div class="skill-div">
                <span class="item-skill official"> <img src="/images/crown.png" alt=""> <span>HTML</span></span>
            </div>
            <div class="skill-div">
                <span class="item-skill official"> <img src="/images/crown.png" alt=""> <span>HTML</span></span>
            </div>
            <div class="skill-div">
                <span class="item-skill official"> <img src="/images/crown.png" alt=""> <span>HTML</span></span>
            </div>
            <div class="skill-div">
                <span class="item-skill official"> <img src="/images/crown.png" alt=""> <span>HTML</span></span>
            </div>
        </div>
        <div class="col-md-6">
            <span>Languages</span> <a href="">+Add new</a>
            <br>
            <div class="media-left media-middle item-languages">
                <a href="#">
                    <img class="media-object" src="http://placehold.it/50x30" alt="...">
                </a>
                <span class="pull-left">English</span>
            </div>
            <div class="media-left media-middle item-languages">
                <a href="#">
                    <img class="media-object" src="http://placehold.it/50x30" alt="...">
                </a>
                <span class="pull-left">Germany</span>
            </div>
        </div>
    </div>
    <div class="row personal-profile">
        <div class="col-md-6">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut cum distinctio dolorem dolorum ducimus magnam placeat praesentium quisquam quos voluptatum!
        </div>
        <div class="col-md-2">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, temporibus!
        </div>
        <div class="col-md-4">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, temporibus!
        </div>
    </div>
</div>
<div class="clearfix"></div>
<br>
<div class="col-md-12 personal-info">
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur autem consequatur corporis, ducimus eligendi excepturi facilis fugiat harum magni maxime nihil non, optio quas quasi, quidem reprehenderit saepe sunt veniam.
</div>