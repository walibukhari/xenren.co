<div id="portfolio-sect-{{ $portfolio->id }}">
    <div class="col-md-12 sub-title">
        <div data-id="{{ $portfolio->id }}" class="btn-delete-portfolio pull-right p-t-10">
            <i class="fa fa-trash"></i>
        </div>
        <h4>{{ $portfolio->title }}</h4>
    </div>

    <div class="col-md-12">
        <div class="row">
            @foreach( $portfolio->files as $uploadFile)
                @if( $uploadFile->isImage() == true )
                    <div class="col-md-4">
                        <a href="{{ $uploadFile->getFile() }}" data-lightbox="portfolio">
                            <div class="link-img">
                                <img id="portfolio-img-{{ $uploadFile->id }}" src="{{ $uploadFile->getThumbnailFile() }}">
                            </div>
                        </a>
                        <a href="{{ route('frontend.modal.cropImage', ['type' => "portfolio", 'id' => $uploadFile->id ] ) }}"
                            data-toggle="modal"
                            data-target="#modalCropImage">
                            @if( $portfolio->user_id == \Auth::guard('users')->user()->id )
                            <div class="tool">
                                <i class="fa fa-scissors"></i>
                            </div>
                            @endif
                        </a>
                    </div>
                @elseif( $uploadFile->isPdfFile() == true )
                    <div class="col-md-4">
                        <a href="{{ $uploadFile->file }}" target="_blank">
                            <div class="link-doc">
                                <img src="{{ $uploadFile->getFileTypeImage() }}">
                                <br>
                                <span>{{ $uploadFile->getShortFilename() }}</span>
                            </div>
                        </a>
                    </div>
                @else
                    <div class="col-md-4">
                        <a href="/download?filename={{ $uploadFile->file }}"
                            data-title="{{ trans('common.you_sure_want_to_download') }}"
                            data-toggle="confirmation"
                            data-placement="bottom"
                            data-btn-ok-label="{{ trans('common.yes') }}"
                            data-btn-ok-class="btn-success"
                            data-btn-cancel-label="{{ trans('common.no') }}"
                            data-btn-cancel-class="btn-danger" >
                            <div class="link-doc">
                                <img src="{{ $uploadFile->getFileTypeImage() }}">
                                <br>
                                <span>{{ $uploadFile->getShortFilename() }}</span>
                            </div>
                        </a>
                    </div>
                @endif
            @endforeach
        </div>
    </div>

    <div class="col-md-12 project-link">
        <a href="{{ $portfolio->getUrl() }}" target="_blank" class="link">
            {{ $portfolio->getUrl() }}
            <img src="/images/icons/link7-copy.png">
        </a>
    </div>

    <div class="col-md-12 simale-line"></div>
</div>