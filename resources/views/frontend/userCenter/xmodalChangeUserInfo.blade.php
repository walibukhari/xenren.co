<!-- Modal -->
<div id="modalChangeUserInfo" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            {!! Form::open(['route' => 'frontend.personalinformation.change', 'id' => 'change-user-detail-form', 'method' => 'post', 'class' => 'form-horizontal']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    {{trans('common.rate')}}
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-body">
                    <div class="form-group">
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">
                            QQ
                            <img src="/images/Tencent_QQ.png" style="max-height: 25px">
                        </label>
                        <div class="col-md-9">
                            <input class="form-control" name="skype_id" v-model="user.qq_id">

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">
                            Skype
                            <img src="/images/skype-icon-5.png" style="max-height: 25px">
                        </label>
                        <div class="col-md-9">
                            <input class="form-control" name="skype_id" v-model="user.skype_id">

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">
                            Wechat
                            <img src="/images/wechat-logo.png" style="max-height: 25px">
                        </label>
                        <div class="col-md-9">
                            <input class="form-control" name="wechat_id" v-model="user.wechat_id">

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">
                            Phone
                            <img src="/images/MetroUI_Phone.png" style="max-height: 25px">
                        </label>
                        <div class="col-md-9">
                            <input class="form-control" name="handphone_no" v-model="user.handphone_no">
                        </div>
                    </div>


                </div>
            </div>
            <div class="modal-footer">
                <button id="btn-change-rate-submit" type="button" class="btn btn-success m-b-0"
                        v-on:click="change('info')">
                    {{ trans('member.confirm_add') }}
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    {{ trans('member.cancel') }}
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

