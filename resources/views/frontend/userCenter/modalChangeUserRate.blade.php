<!-- Modal -->
<div id="modalChangeUserRate" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="border-radius: 2px !important;">
            {!! Form::open(['route' => 'frontend.personalinformation.change', 'id' => 'change-user-detail-form', 'method' => 'post', 'class' => 'form-horizontal']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    {{trans('common.rate_please')}}
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-body">
                    <div class="form-group">
                        <div class="col-md-12" id="add-user-status-errors"></div>
                    </div>
                    <div class="form-group">
                        {{--<label class="col-md-3 control-label">--}}
                            {{--{{trans('common.rate')}}--}}

                        {{--</label>--}}

                        <div class="col-md-8">
                            <h1 class="set-currency-rate">{{trans('common.currency')}}</h1>
                            <span>{{trans('common.You_can_choose_currency_manually_from_list')}}</span>
                        </div>
                        <div class="col-md-4">
                            <select class="form-control" name="currency" id="currency" v-model="user.currency">
                                <option value="" selected="selected">Select Currency</option>
                                @foreach($currency_code as $code)
                                    @if(isset($code->currency_code) && $code->currency_code != '')
                                    <img src="{{asset('images/Flags-Icon-Set/24x24/'.strtoupper($code->code).'.png')}}" alt="">
                                    <option value="{{$code->currency_code}}">
                                        {{$code->currency_code}}
                                    </option>
                                    @endif
                                @endforeach
                            </select>

                        </div>
                    </div>
                    <hr>

                    <div class="form-group">
                        <div class="col-md-8">
                            <h1 class="set-hourly-rate">{{trans('common.hourly_pay')}}</h1>
                            <span>{{trans('common.you_will_earn_this_amount_in_an_hour')}}</span>
                        </div>

                        <div class="col-md-4">
                            <input class="form-control" id="tbxHourly" name="tbx_hourly_pay" placeholder="{{ trans('common.input_hourly_pay') }}" v-model="user.hourly_pay">
                            <input type="hidden" name="hourly_pay" value="">
                        </div>
                    </div>

                    <div class="form-group set-exchange-icon">
                        <img class="exchange-icon" src="{{asset('images/exchangeIcon.png')}}">
                    </div>

                    <div class="form-group">
                        <div class="col-md-8">
                            <h1 class="set-monthly-rate">{{trans('common.monthly_pay')}}</h1>
                            <span>{{trans('common.you_will_earn_this_amount_in_one_month')}}</span>
                        </div>

                        <div class="col-md-4" style="display:flex;">
                            <span class="set-dollar">$</span>
                            <input class="form-control" id="tbx_monthly_pay" name="tbx_monthly_pay" placeholder="/Mth" v-model="user.monthly_pay">
                        </div>
                    </div>

                    <div class="form-group set-calculate-form-group">
                        <br/>
                        <div id="divCalculateMessage" class="col-md-12 text-center setMCUSKILL">
                            {{ trans('common.calculating') }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer set-padd-modal-footer-set-rate"
                 style="display: flex;
                 justify-content: flex-end;"
            >
                <button type="button" id="setvancekbtn" class="btn btn-default" data-dismiss="modal">
                    {{ trans('member.cancel') }}
                </button>

                <button id="btn-change-rate-submit"
                        style="width:20%;
                        display: flex;
                        align-items: center;
                        justify-content: center;"
                        type="button" class="btn btn-success m-b-0 setConfirmAdd"
                        v-on:click="change('hourly_pay')">
                    <span v-if="!rateLoader">{{ trans('common.update') }}</span>
                    <div v-if="rateLoader" class="lds-ring-experience lds-ring">
                        <div></div><div></div><div></div><div></div>
                    </div>
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

