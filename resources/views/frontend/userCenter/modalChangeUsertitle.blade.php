<!-- Modal -->
<div id="modalChangeUsertitle" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="border-radius: 2px !important;">
            {!! Form::open(['route' => 'frontend.personalinformation.change', 'id' => 'change-user-detail-form', 'method' => 'post', 'class' => 'form-horizontal']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    {{trans('common.title')}}
                </h4>
            </div>
            <div class="modal-body">
                <label>{{trans('common.title')}}:</label>
                <input type="text" class="form-control titleTxt" name="title" v-model="user.title">
            </div>
            <div class="modal-footer">
                <button id="btn-change-status-submit" type="button" class="btn btn-success m-b-0"
                        v-on:click="change('title')">
                    {{ trans('member.confirm_add') }}
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    {{ trans('member.cancel') }}
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

