@if( $isImage == true )
<div class="col-md-4 upload-file">
    <a href="">
        <div class="link-img">
            <img class="img-{{ $itemId }}" src="/images/website3.png">
        </div>
    </a>
</div>
@else
<div class="col-md-4 upload-file">
    <a href="">
        <div class="link-doc">
            <img src="{{ $imageFileType }}">
            <br>
            <span>{{ $shortFileName }}</span>
        </div>
    </a>
</div>
@endif
<div id="btn-add-new-file" class="col-md-4">
    <a href="javascript:;" class="">
        <div class="link-a">
            <i class="fa fa-plus"></i>
        </div>
    </a>
</div>

{{--<div class="attachment upload-file" id="fu-item-{{ $itemId }}">--}}
    {{--<span class="btn-minus-file-custom" id="btn-delete-{{ $itemId }}">--}}
        {{--<span class="p-l-1">--}}
            {{--<i class="fa fa-close" aria-hidden="true"></i>--}}
        {{--</span>--}}
    {{--</span>--}}
    {{--<div class="attachment-file">--}}
        {{--<img src="{{ $imageFileType }}">--}}
    {{--</div>--}}
    {{--<div class="attachment-name text-center">--}}
        {{--<a href="">{{ $shortFileName }}</a>--}}
    {{--</div>--}}
{{--</div>--}}
{{--<div id="btn-add-new-file" class="attachment">--}}
    {{--<div class="attachment-no-file">--}}
        {{--<span class="fileinput-new p-l-1">--}}
            {{--<span class="btn-file btn-add-file-custom">--}}
                {{--<i class="fa fa-plus" aria-hidden="true"></i>--}}
            {{--</span>--}}
        {{--</span>--}}
    {{--</div>--}}
{{--</div>--}}

