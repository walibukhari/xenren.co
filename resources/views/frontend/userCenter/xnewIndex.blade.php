@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.new_personal_info_title') }}
@endsection

@section('keywords')
    {{ trans('common.new_personal_info_keyword') }}
@endsection

@section('description')
    {{ trans('common.new_personal_info_desc') }}
@endsection

@section('author')

@endsection

@section('header')
	<link href="/custom/css/frontend/findExperts.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/general.css" rel="stylesheet">
    <link href="/custom/css/frontend/personal-info.css" rel="stylesheet">
    <link href="/custom/css/frontend/userCenter.css" rel="stylesheet">
    <link href="/custom/css/frontend/personalPortfolio.css" rel="stylesheet">
@endsection

@section('content')
<div id="userCenter">
	<div class="row">
	    <div class="col-md-3 search-title-container">
	        <span class="find-experts-search-title text-uppercase">main action</span> <a class="leftSide-menu-list"><i class="fa fa-bars"></i></a>
	    </div>
	    <div class="col-md-9">
	        <span class="find-experts-search-title text-uppercase">personal info</span>
	    </div>
	</div>

	<!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE BASE CONTENT -->
            <section class="main div-container well">
                <div class="row user-img-main">
                    <div class="col-md-3 text-center">
                        <div class="profile-picture-main">
                    	   <img class="profile-picture img-circle pull-left" src="/images/people-avatar.png" alt="" width="160px" height="160px">
                            <a href="" class="camera-icon"><i class="fa fa-camera"></i></a>
                        </div>

                    	<button class="btn btn-color-custom" data-hover="dropdown">Contact Info</button>

                    	<div class="row dropdown-menu">
                            <div class="col-md-12 contacts-info">
                                <div class="row">
                                    <div class="col-md-10 col-md-10">
                                        <div class="row">

                                            <div class="col-md-3 col-md-3 contacts-info-sub">
                                                <img src="/images/skype-icon-5.png">
                                                <span>Skype</span>
                                                <p>adamjohn982</p>
                                            </div>

                                            <div class="col-md-3 col-md-3 contacts-info-sub">
                                                <img src="/images/wechat-logo.png">
                                                <span>WeChat</span>
                                                <p>adamjohnhs</p>
                                            </div>

                                            <div class="col-md-3 col-md-3 contacts-info-sub">
                                                <img src="/images/Tencent_QQ.png">
                                                <span>QQ</span>
                                                <p>1851215421</p>
                                            </div>


                                            <div class="col-md-3 col-md-3 contacts-info-sub">
                                                <img src="/images/MetroUI_Phone.png">
                                                <span>Phone</span>
                                                <p>+00 1234 567 89</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-md-2 contacts-info-edit">
                                        <div>
                                            <a href="javascript:;" class="action pull-right" style="padding-top: 5px;">
                                                <i class="ti-pencil"></i> Edit
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                    		<div class="col-md-4">
		                        <span class="full-name pull-left">
		                            Adam John
		                            <i class="fa fa-check-circle"></i>
		                        </span>
		                    </div>
                            <div class="col-md-4 personal-info-designer">
                                <p class="desh">|</p>
                                <span class="first">UI UX Designer</span>
                                <p class="designer">Designer</p>
                            </div>
                            <div class="col-md-4 personal-info-btn">
                                <a href="" class="btn btn-custom pull-right"><i class="fa fa-pencil"></i> PERSONAL INFO</a>
                            </div>
                        </div>

                        <div class="row personal-info">
                            <div class="col-md-8 col-sm-8 left-div">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span class="label">Status : </span>
                                    </div>
                                    <div class="col-md-9">
                                        <span class="status" style="color: #58AF2A !important;">Looking For Project</span>
                                        <a class="f-s-14 edit-icon-link" href="javascript:;">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <span class="label">Rate : </span>
                                    </div>
                                    <div class="col-md-9">
                                        <span class="status">$10.00 /hr</span>
                                        <a class="f-s-14 edit-icon-link" href="javascript:;">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <span class="label">Age : </span>
                                    </div>
                                    <div class="col-md-9">
                                        <span class="status">36</span>
                                        <a class="f-s-14 edit-icon-link" href="javascript:;">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <span class="label">Experience : </span>
                                    </div>
                                    <div class="col-md-9">
                                        <span class="status">3 years</span>
                                        <a class="f-s-14 edit-icon-link" href="javascript:;">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <img src="/images/QRCODE.jpg" class="qrcode" alt="" width="135px" height="135px">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr>
                <div class="row profile-details">
                    <div class="col-md-6">
                        <div class="label">
                            <img src="/images/hands.png" alt=""><span>Skill</span>
                            <!-- <a href="javascript:;" class="f-s-10">
                                ( Didn't found the keyword? Please help us improve. )
                            </a> -->
                        </div>
                        <a href="javascript:;" class="action pull-right f-s-14">
                            + Add New
                        </a>
                        <div class="margin-30"></div>

                        <div class="clearfix"></div>
                        <div class="skill logo2">
                            <span><img src="/images/crown.png"> PHP</span>
                        </div>
                        <div class="skill checked">
                            <span><img src="/images/checked.png"> JavaScript</span>
                        </div>
                        <div class="skill">
                            <span>HTML5</span>
                        </div>
                        <div class="skill">
                            <span>Adobe Photoshop</span>
                        </div>
                        <div class="skill">
                            <span>Adobe Illusstrator</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="label">
                            <img src="/images/earth.png" alt=""><span>Language</span>
                        </div>
                        <a href="javascript:;" class="action pull-right f-s-14">
                            + Add New</a>
                        <div class="margin-10"></div>
                        <div class="clearfix"></div>
                        <br>
                        <div class="language">
                        	<img src="/images/Flags-Icon-Set/24x24/GB.png" alt="">
                            <span>English</span>
                        </div>
                        <div class="language">
                        	<img src="/images/Flags-Icon-Set/24x24/DE.png" alt="">
                            <span>Germany</span>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row profile-info">
                    <div class="col-md-6">
                        <div class="label">
                            <img src="/images/info.png" alt=""><span>Info</span>
                        </div>
                        <a href="javascript:;" class="action pull-right f-s-14">
                            <i class="ti-pencil"></i> Edit
                        </a>
                        <div class="margin-25"></div>
                        <div class="clearfix"></div>
                        <div class="desc">
	                        Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a plece of classical Latin literature from 45 BC. making It over.
	                        <br>
	                        A plece of classical Latin literature from 45 BC. making it over.
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="label">
                            <i class="glyphicon glyphicon-star" aria-hidden="true"></i>
                            <span>rating</span>
                        </div>
                        <div class="margin-25"></div>
                        <div class="clearfix"></div>
                        <span class="rating"> 4.0</span>
                        <div class="star text-center">
                            <i class="fa fa-star" style="padding-left: 2px; padding-right: 2px;"></i>
                            <i class="fa fa-star" style="padding-left: 2px; padding-right: 2px;"></i>
                            <i class="fa fa-star" style="padding-left: 2px; padding-right: 2px;"></i>
                            <i class="fa fa-star" style="padding-left: 2px; padding-right: 2px;"></i>
                            <i class="fa fa-star-o" style="padding-left: 2px; padding-right: 2px;"></i>
                        </div>
                        <span class="review-count">(2152 review)</span>
                    </div>
                    <div class="col-md-4 graph-container">
                        <div class="graph">
                            <span class="count">789</span>
                            <div class="bar five-star" style="height:65px"></div> <!-- for graph-->
                            <i class="fa fa-star"></i>
                            <span class="number">5</span>
                        </div>
                        <div class="graph">
                            <span class="count">1230</span>
                            <div class="bar four-star" style="height:100px"></div> <!-- for graph-->
                            <i class="fa fa-star"></i>
                            <span class="number">4</span>
                        </div>
                        <div class="graph">
                            <span class="count">962</span>
                            <div class="bar three-star" style="height:90px"></div> <!-- for graph-->
                            <i class="fa fa-star"></i>
                            <span class="number">3</span>
                        </div>
                        <div class="graph">
                            <span class="count">452</span>
                            <div class="bar two-star" style="height:25px"></div> <!-- for graph-->
                            <i class="fa fa-star"></i>
                            <span class="number">2</span>
                        </div>
                        <div class="graph">
                            <span class="count">320</span>
                            <div class="bar one-star" style="height:15px"></div> <!-- for graph-->
                            <i class="fa fa-star"></i>
                            <span class="number">1</span>
                        </div>
                    </div>
                </div>
            </section>

            <section class="div-container well main-well-custom">
			    <div class="row">
			        <div class="col-md-12 personal-info-tab">
			            <div class="tabbable-panel">
			                <div class="tabbable-line">
			                    <ul class="nav nav-tabs">
			                        <li>
			                            <a href="#tab_default_1" data-toggle="tab" class="f-s-16">Official Comment (21)</a>
			                        </li>
			                        <li>
			                            <a href="#tab_default_2" data-toggle="tab" class="f-s-16">Comment (315)</a>
			                        </li>
			                        <li class="active">
			                            <a href="#tab_default_3" data-toggle="tab" class="f-s-16">Personal Portfolio (5)</a>
			                        </li>
			                    </ul>
			                    <div class="tab-content">
		                        	<div class="tab-pane Official-comment" id="tab_default_1">
                                        <div class="personal-portfolio-empty">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h5>USER HAVEN"T GOT ANY PORTFOLIO YET</h5>
                                                    <img src="/images/icons/potfolio-emptys.png">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane all-comments" id="tab_default_2">
                                        <div class="personal-portfolio-empty">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h5>USER HAVEN"T GOT ANY PORTFOLIO YET</h5>
                                                    <img src="/images/icons/potfolio-emptys.png">
                                                </div>
                                            </div>
                                        </div>
		                            </div>
		                            <div class="tab-pane active personal_portfolio_tab" id="tab_default_3">
					                    <div class="row menu">
					                    	<div class="col-md-12">
					                    		<ul>
							                        <li>
							                            <a href="#tab_default_31" data-toggle="tab" class="f-s-16">Coder</a>
							                        </li>
							                        <li>
							                            <a href="#tab_default_32" data-toggle="tab" class="f-s-16">Product Manager</a>
							                        </li>
							                        <li class="active">
							                            <a href="#tab_default_33" data-toggle="tab" class="f-s-16">Designer</a>
							                        </li>
							                        <li>
							                            <a href="#tab_default_34" data-toggle="tab" class="f-s-16">Invester</a>
							                        </li>
							                        <li>
							                            <a href="#tab_default_35" data-toggle="tab" class="f-s-16">Marketer</a>
							                        </li>
							                    </ul>
					                    	</div>
					                    </div>

					                    <div class="tab-content">
					                    	<div class="tab-pane" id="tab_default_31">
				                            </div>
				                            <div class="tab-pane" id="tab_default_32">
				                            </div>
				                            <div class="tab-pane active" id="tab_default_33">
				                            	<div class="row add-more-pro">
						                    		<div class="col-md-12">
						                            	<form>
														  	<div class="form-group">
														    	<label for="title">Project Title</label>
														    	<input type="text" class="form-control" id="title" value="Graphic Design for Lagend Lair Website">
														  	</div>

														  	<div class="form-group">
														    	<div class="row">
														    		<div class="col-md-4">
														    			<a href="">
															    			<div class="link-img">
															    				<img src="/images/website2.png">
															    			</div>
															    		</a>
														    		</div>
														    		<div class="col-md-4">
														    			<a href="">
															    			<div class="link-img">
															    				<img src="/images/website3.png">
															    			</div>
														    			</a>
														    		</div>
														    		<div class="col-md-4">
														    			<a href="">
															    			<div class="link-a">
															    				<i class="fa fa-plus"></i>
															    			</div>
														    			</a>
														    		</div>
														    	</div>
														  	</div>

														  	<div class="form-group">
														    	<label for="link">Project Link</label>
														    	<input type="text" class="form-control" id="link" value="www.behance.com/legendlair">
														  	</div>

														  	<div class="form-group form-group-btn">
																<button type="submit" class="btn btn-make-Offer">Submit</button>
																<button class="btn btn-join-discuss">Add More Project</button>
														  	</div>
														</form>
							                        </div>
							                    </div>
							                    <div class="row project-list">
							                    	<div class="col-md-12 title"><h4>Designer</h4></div>

							                    	<div class="col-md-12 sub-title">
							                    		<h4>Graphic Design for Lagend Lair Website</h4>
							                    	</div>

							                    	<div class="col-md-12">
							                    		<div class="row">
												    		<div class="col-md-4">
												    			<a href="">
													    			<div class="link-img">
													    				<img src="/images/website2.png">
													    			</div>
													    		</a>
												    		</div>
												    		<div class="col-md-4">
												    			<a href="">
													    			<div class="link-img">
													    				<img src="/images/website3.png">
													    			</div>
												    			</a>
												    		</div>
												    		<div class="col-md-4">
												    			<a href="">
													    			<div class="link-img">
													    				<img src="/images/website4.png">
													    			</div>
												    			</a>
												    		</div>
												    	</div>
							                    	</div>
							                    	<div class="col-md-12 project-link">
						                    			<a href="" class="link">
						                    				www.behance.com/legendlair
						                    				<img src="/images/icons/link7-copy.png">
						                    			</a>
							                    	</div>

							                    	<div class="col-md-12 simale-line"></div>

							                    	<div class="col-md-12 sub-title">
							                    		<h4>UI UX Design for CCTV Website</h4>
							                    	</div>

							                    	<div class="col-md-12">
							                    		<div class="row">
												    		<div class="col-md-4">
												    			<a href="">
													    			<div class="link-img">
													    				<img src="/images/website5.png">
													    			</div>
													    		</a>
												    		</div>
												    		<div class="col-md-4">
												    			<a href="">
													    			<div class="link-img">
													    				<img src="/images/website6.png">
													    			</div>
												    			</a>
												    		</div>
												    		<div class="col-md-4">
												    			<a href="">
													    			<div class="link-img">
													    				<img src="/images/website1.png">
													    			</div>
												    			</a>
												    		</div>
												    	</div>
							                    	</div>
							                    	<div class="col-md-12 project-link">
						                    			<a href="" class="link">
						                    				www.behance.com/legendlair/project/demo
						                    				<img src="/images/icons/link7-copy.png">
						                    			</a>
							                    	</div>


							                    	<div class="col-md-12 title"><h4>Investor</h4></div>

							                    	<div class="col-md-12 sub-title">
							                    		<h4>Legendlair App</h4>
							                    	</div>

							                    	<div class="col-md-12">
							                    		<div class="row">
												    		<div class="col-md-4">
												    			<a href="">
													    			<div class="link-doc">
													    				<img src="/images/doc.png">
													    				<br>
													    				<span>document.docx</span>
													    			</div>
												    			</a>
												    		</div>
												    		<div class="col-md-4">
												    			<a href="">
													    			<div class="link-img">
													    				<img src="/images/website2.png">
													    			</div>
												    			</a>
												    		</div>
												    	</div>
							                    	</div>
							                    	<div class="col-md-12 project-link">
						                    			<a href="" class="link">
						                    				www.legendlair.com
						                    				<img src="/images/icons/link7-copy.png">
						                    			</a>
							                    	</div>
							                    	<div class="col-md-12 simale-line"></div>

							                    </div>
				                            </div>
				                            <div class="tab-pane" id="tab_default_34">
				                            </div>
				                            <div class="tab-pane" id="tab_default_35">
				                            </div>
					                    </div>
		                            </div>
		                        </div>
			                </div>
			            </div>
			        </div>
			    </div>
			</section>
        </div>
    </div>
</div>
@endsection
