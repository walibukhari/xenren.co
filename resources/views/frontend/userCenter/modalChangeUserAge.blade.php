<!-- Modal -->
<div id="modalChangeUserAge" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="border-radius: 2px !important;">
            {!! Form::open(['route' => 'frontend.personalinformation.change', 'id' => 'change-user-detail-form', 'method' => 'post', 'class' => 'form-horizontal']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    {{trans('common.age')}}
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-body">
                    <div class="form-group">
                        <div class="col-md-12" id="add-user-status-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">
                            {{trans('common.date_of_birth')}}

                        </label>
                        <div class="col-md-9">
                            <input class="form-control datepicker" name="date_of_birth" v-model="user.date_of_birth" placeholder="{{ trans('common.birth_date_format') }}">

                        </div>
                    </div>


                </div>
            </div>
            <div class="modal-footer"
                 style="display: flex;
                 justify-content: flex-end;"
            >
                <button id="btn-change-age-submit"
                        style="width:20%;
                        display: flex;
                        align-items: center;
                        justify-content: center;"
                        type="button" class="btn btn-success m-b-0"
                        v-on:click="change('date_of_birth')">
                    <span v-if="!ageLoader">{{ trans('member.confirm_add') }}</span>
                    <div v-if="ageLoader" class="lds-ring-age lds-ring">
                        <div></div><div></div><div></div><div></div>
                    </div>
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    {{ trans('member.cancel') }}
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

