<!-- Modal -->
<div id="modalChangeUserLanguage" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="border-radius: 2px !important;">
            {!! Form::open(['route' => 'frontend.personalinformation.addskill', 'id' => 'add-user-language-form', 'method' => 'post', 'class' => 'form-horizontal']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    {{trans('common.language_2')}}
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-body">
                    <div class="form-group">
                        <div class="col-md-12" id="add-user-status-errors"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">
                            {{trans('common.language_2')}}
                        </label>
                        <div class="col-md-9">
                            <div id="languageInputSeelct">
                                <input type="text" name="language" id="lang_id_list" multiple>
                            </div>
                            {{--<input type="text" name="language" class="form-control" v-model="languages"/>--}}

                            {{ Form::hidden('lang_id_list', $_G['user']->getUserLanguageList(), ['id'=>'lang_id_list']) }}

                        </div>
                    </div>

                    {{--<div class="form-group">--}}
                    {{--<label class="col-md-3 control-label">--}}
                    {{--{{trans('common.experience')}}--}}
                    {{--</label>--}}
                    {{--<div class="col-md-9">--}}

                    {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
            <div class="modal-footer"
                 style="display: flex;
                 justify-content: flex-end;"
            >
                <button id="btn-add-status-submit"
                        style="width:20%;
                        display: flex;
                        align-items: center;
                        justify-content: center;"
                        type="button" class="btn btn-submit"
                        @click="changeLanguage('language')" >
                    <span v-if="!languageLoader">{{ trans('common.save_change') }}</span>
                    <div v-if="languageLoader" class="lds-ring-experience lds-ring">
                        <div></div><div></div><div></div><div></div>
                    </div>
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    {{ trans('member.cancel') }}
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

