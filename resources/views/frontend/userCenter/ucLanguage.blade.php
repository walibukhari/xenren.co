@foreach($languages as $lang)
    @if(isset($lang->language) && isset($lang->language->country))
    <div class="language">
        @php
            $code = strtoupper($lang->language->country->code);
        @endphp
        <img src="{{asset('images/Flags-Icon-Set/24x24/'.strtoupper($code).'.png')}}" alt="">
        <span>{{((Lang::locale() =='cn') && !empty($lang->language->language->name_cn))? $lang->language->language->name_cn: $lang->language->language->name}}</span>
    </div>
    @endif
@endforeach