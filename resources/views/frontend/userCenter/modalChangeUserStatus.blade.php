@if(\Auth::user())
    <style>
        .custombtnSaveChangeUserstatus{
            display: flex;
            justify-content: center;
            align-items: center;
            background: #57B029;
            width: 50%;
            height: 48px;
            border: 0px;
            border-radius: 4px;
            color: #fff;
        }
        .lds-ring {
            display: inline-block;
            position: relative;
            width: 20px;
            height: 20px;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 20px;
            height: 20px;
            margin: 1px;
            border: 2px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
    <!-- Change status Modal -->
    <div class="modal fade" id="modalChangeUserStatus" role="dialog" style="border-radius: 2px !important;">
        <div class="modal-dialog change-status-dialog">
            <div class="modal-content change-status-content" style="border-radius: 2px !important;">
                <div class="modal-header change-status-header text-right">
                    <h4 class="modal-title change-status-title">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <a href="" data-dismiss="modal" class="close-btn change-status-close-btn"><img src="/images/Delete-100.jpg"></a>
                                <h2 class="p-0 m-0">{{trans('common.change_your_status')}}</h2>
                                <p>{{trans('common.can_select_more_than_one_option')}}</p>
                            </div>
                        </div>
                    </h4>
                </div>
                <div class="modal-body change-status-body">
                    {!! Form::open(['route' => 'frontend.personalinformation.change', 'id' => 'add-user-status-form', 'method' => 'post', 'class' => 'form-horizontal']) !!}
                    <input type="hidden" name="column" value="status">
                    <div class="row">
                        <div class="col-md-4 text-center change-status-body-box">
                            <input id="project" type="checkbox" name="project"  @if(\Auth::user()->isStatusSelected(1))checked="" @endif />
                            <div class="div">
                                <label for="project">
                                    <div class="box text-center set-box">
                                        <div class="change-status-bg-img"></div>
                                        <p class="set-content-change-status-popup">{!! trans('common.looking_for_projects') !!}</p>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4 text-center change-status-body-box">
                            <input id="coFounder" type="checkbox" name="coFounder" @if(\Auth::user()->isStatusSelected(2))checked=""@endif />
                            <div>
                                <label for="coFounder">
                                    <div class="box text-center set-box">
                                        <div class="change-status-bg-img-second"></div>
                                        <p class="set-content-change-status-popup">{!!trans('common.looking_for_co_founders')!!}</p>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4 text-center change-status-body-box">
                            <input id="job" type="checkbox" name="job"  @if(\Auth::user()->isStatusSelected(3))checked="" @endif />
                            <div>
                                <label for="job">
                                    <div class="box text-center set-box">
                                        <div class="change-status-bg-img-third"></div>
                                        <p class="set-content-change-status-popup">{!!trans('common.looking_for_full_time_job')!!}</p>
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer change-status-footer text-center">
                        <div class="row">
                            <div class="col-md-12 text-center" style="display: flex;justify-content: center;">
                                <button style="display: flex;justify-content: center;align-items: center;" type="button" class="custombtnSaveChangeUserstatus" onclick="saveChangeUserStatus()">
                                    <span class="idSpanChangeUserStatus">
{{--                                        <img src="{{ asset('/images/icons/Checkmark Filled.png') }}">--}}
                                        &nbsp; {{trans('common.save_changes')}}
                                    </span>
                                    <div style="display: none;" class="lds-ring">
                                        <div></div><div></div><div></div><div></div>
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endif

<script>
    function saveChangeUserStatus(){
        $('.idSpanChangeUserStatus').hide();
        $('.lds-ring').show();
        $('#add-user-status-form').submit();
    }
</script>
