@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.personal_info_title') }}
@endsection

@section('keywords')
    {{ trans('common.personal_info_keyword') }}
@endsection

@section('description')
    {{ trans('common.personal_info_desc') }}
@endsection

@section('author')
    
@endsection

@section('header')
    <link href="{{asset('assets/global/plugins/bootstrap-tagsinput-latest/dist/bootstrap-tagsinput.css')}}" rel="stylesheet" />
    <link href="/assets/global/plugins/dropzone-4.0.1/min/dropzone.min.css" rel="stylesheet"/>
    <link href="/assets/global/plugins/dropzone-4.0.1/min/basic.min.css" rel="stylesheet"/>
    <link href="/assets/global/plugins/lightbox2/dist/css/lightbox.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/global/plugins/jcrop/css/jquery.Jcrop.min.css" rel="stylesheet"/>
    <link href="/custom/css/frontend/findExperts.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/general.css" rel="stylesheet">
    <link href="/custom/css/frontend/personal-info.css" rel="stylesheet">
    <link href="/custom/css/frontend/userCenter.css" rel="stylesheet">
    <link href="https://cdn.rawgit.com/konpa/devicon/master/devicon.min.css" rel="stylesheet" >
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" rel="stylesheet" />
    <link href="/custom/css/frontend/personalPortfolio.css" rel="stylesheet">
    <link href="/custom/css/frontend/personalPortfolioHover.css" rel="stylesheet">

@endsection

@section('content')
<div id="userCenter">
    <div class="row">
        <div class="col-md-3 search-title-container">
            <span class="find-experts-search-title text-uppercase">{{trans('common.main_action')}}</span>
        </div>
        <div class="col-md-9">
            <span class="find-experts-search-title text-uppercase">{{trans('common.personal_info')}}</span>
        </div>
    </div>

    <div class="page-content-wrapper">
    	<div class="page-content">
    		<section class="main div-container well">
                <div class="row user-img-main">
                    <div class="col-md-2">
                        <img class="profile-picture img-circle pull-left" src="/images/people-avatar.png" alt="" width="120px" height="120px">
                    </div>
                    <div class="col-md-7 user-des-center">
                    	<div class="row">
                    		<div class="col-md-12">
                    			<div class="custom-hover full-name">
				                	<h2>
				                        Doam John
				                        <img src="/images/icons/check.png"> 
				                        <br>
				                    	<span class="sub-name">UI UX Designer</span> 
				                    </h2>
				                    <div class="edit-btn"><a href="#"> <img src="/images/icons/icons8-Edit.png"></a></div>
		                        </div>
                    		</div>
                    		<div class="col-md-12">
                    			<div class="custom-hover location-status">
				                    <img src="/images/icons/icons8-Marker.png" class="location-icon"> 
				                	<h3>
				                        Location
				                        <br>
				                    	<span class="sub-name">India, Delhi</span> 
				                    </h3>
				                    <div class="edit-btn"><a href="#"><img src="/images/icons/icons8-Edit.png"></a></div>
		                        </div>
                    		</div>
                    		<div class="col-md-12">
                    			<div class="custom-hover location-status">
				                    <img src="/images/icons/icons8-Find-User-Male.png" class="location-icon"> 
				                	<h3>
				                        Status
				                        <br>
				                    	<span class="sub-name">Looking For Job</span> 
				                    </h3>
				                    <div class="edit-btn"><a  href="javascript:;" data-toggle="modal" data-target="#modalChangeUserStatus"><img src="/images/icons/icons8-Edit.png"></a></div>
		                        </div>
                    		</div>
                    	</div>
                    </div>
                    <div class="col-md-3 user-des-right">
                    	<div class="custom-hover pull-right">
                    		<a href="javascript:;" data-toggle="modal" data-target="#modalChangeUserRate" class="edit-img-a"><img src="/images/icons/icons8-Edit.png" class="pull-left edit-img"></a>
                    		<h2>$44/hr</h2>
                    	</div>
                    	<img src="/images/QRCODE.jpg" class="qrcode" alt="" width="135px" height="135px">
                    </div>
                    <div class="col-md-12 edit-profile">
	                    <div class="edit-profile-div">
	                    	<a href="{{ route('frontend.personalinformation') }}"><button class="edit-profile-btn">Edit Profile Info</button></a>
	                    </div>
                    </div>
                    <div class="col-md-12 custom-hover user-description">
                    	<h4>ABOUT</h4>
                    	
                    	<a href="javascript:;" data-toggle="modal" data-target="#modalChangeUserAboutMe" class="edit-img-a"><img src="/images/icons/icons8-Edit.png" class="edit-img"></a>

                    	<p class="desc">
                    		Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has
                    	</p>
                    </div>
                    <div class="col-md-12 custom-hover user-description">
                    	<h4>SKILLS</h4>

                    	<a href="javascript:;" data-toggle="modal" data-target="#modalChangeUserSkill" class="edit-img-a"><img src="/images/icons/icons8-Edit.png" class="edit-img"></a>

                         <div class="item-skill skill-success">
                            <span class="success"> <i class="fa fa-check"></i> <span>PHP</span></span>
                        </div>
                        <div class="item-skill skill-official">
                            <span class="official"> <i class="fa fa-star-o"></i> <span>CSS</span></span>
                        </div>
                        <div class="item-skill">
                            <span class=""><span>Adobe ilusstrator</span></span>
                        </div>
                        <div class="item-skill">
                            <span class=""><span>Adobe Photoshop</span></span>
                        </div>
                        <div class="item-skill">
                            <span class=""><span>JSON</span></span>
                        </div>
                        
                    </div>
                    <div class="col-md-12 custom-hover user-description">
                    	<h4>LANGUAGES</h4>
                    	
                    	<a href="javascript:;" data-toggle="modal" data-target="#modalChangeUserLanguage" class="edit-img-a"><img src="/images/icons/icons8-Edit.png" class="edit-img"></a>

                    	<div class="language-main">
                            <img src="/images/icons/United-Kingdom.png"> <span>English</span>    
                        </div>
                        <div class="language-main">
                            <img src="/images/icons/Serbia.png"> <span>Serbian</span>    
                        </div>
                        <div class="language-main">
                            <img src="/images/icons/China.png"> <span>Chinese</span>    
                        </div>
                        <div class="language-main">
                            <img src="/images/icons/Zambia.png"> <span>Zambian</span>    
                        </div>
                    </div>

                    <div class="col-md-12 user-description">
                        <h4>RATING</h4>
                        
                        <div class="row profile-info">
                            <div class="col-md-3">
                                <span class="rating"> 4.0</span>
                                <div class="star text-left">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                                <span class="review-count">(2125 review)</span>
                            </div>
                            <div class="col-md-9">
                                <div class="graph-container">
                                    <div class="graph">
                                        <i class="fa fa-star"></i> 
                                        <span class="number">1</span>
                                        <div class="bar one-star" style="width: 15%;"></div>  
                                    </div> 
                                    <div class="graph">
                                        <i class="fa fa-star"></i> 
                                        <span class="number">2</span>
                                        <div class="bar two-star" style="width: 30%;"></div>  
                                    </div> 
                                    <div class="graph">
                                        <i class="fa fa-star"></i> 
                                        <span class="number">3</span>
                                        <div class="bar three-star" style="width: 60%;"></div>  
                                    </div> 
                                    <div class="graph">
                                        <i class="fa fa-star"></i> 
                                        <span class="number">4</span>
                                        <div class="bar four-star" style="width: 80%;"></div>  
                                    </div> 
                                    <div class="graph">
                                        <i class="fa fa-star"></i> 
                                        <span class="number">5</span>
                                        <div class="bar five-star" style="width: 55%;"></div>  
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>            
            <section class="div-container well main-well-custom tab-content">
                <div class="tabbable-panel">    
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_portfolio" data-toggle="tab" class="f-s-16" aria-expanded="true">Personal Portfolio</a></li>
                            <li class=""><a href="#tab_admin_review" data-toggle="tab" class="f-s-16" aria-expanded="false">Official Comment</a></li>
                            <li class=""><a href="#tab_review" data-toggle="tab" class="f-s-16" aria-expanded="false">Comment</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab_portfolio" class="tab-pane personal_portfolio_tab active">
                                <div class="row add-more-pro">
                                    <div class="col-md-12">
                                        <form method="POST" action="" accept-charset="UTF-8" id="create-portfolio-form" enctype="multipart/form-data" class="horizontal-form">
                                            
                                            <div class="form-group">
                                                <label for="title">Project Field</label> 
                                                <div class="customize dropdown">
                                                    <a role="button" data-toggle="dropdown" class="btn custom-select-btn" data-target="#">
                                                        <label class="pull-left">
                                                            Design
                                                        </label>
                                                    </a>
                                                    <ul class="dropdown-menu">
                                                        <li><a class="item">Design 1</a></li>
                                                        <li><a class="item">Design 2</a></li>
                                                        <li><a class="item">Design 3</a></li>
                                                        <li><a class="item">Design 4</a></li>
                                                        <li><a class="item">Design 5</a></li>
                                                    </ul>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label for="title">Project Title</label> 
                                                <input id="title" name="title" value="" placeholder="" class="form-control" type="text">
                                            </div> 

                                            <div class="form-group dropzone-form">
                                                <div id="portfolio-dropzone" class="dropzone dz-clickable">
                                                    <div class="dz-default dz-message">
                                                        <span>
                                                            Drop files or <a href="">browse</a> To upload
                                                        </span>
                                                    </div>
                                                </div> 
                                                <div id="file-sect"></div>
                                            </div> 

                                            <div class="form-group">
                                                <label for="link">Project Link</label> 
                                                <input id="link" name="link" value="" placeholder="" class="form-control" type="text">
                                            </div> 

                                            <div class="form-group form-group-btn text-center">
                                                <button id="btn-submit" type="button" class="btn upload-btn">UPLOAD PROJECT</button>
                                            </div> 
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div id="tab_admin_review" class="tab-pane personal_portfolio_tab"></div>
                            <div id="tab_review" class="tab-pane personal_portfolio_tab"></div>
                        </div>
                    </div>
                </div>
            </section>
    	</div>
    </div>

    @include('frontend.userCenter.modalChangeUserAboutMe')
    @include('frontend.userCenter.modalChangeUserLanguage')
    @include('frontend.userCenter.modalChangeUserSkill', ['isSkipButtonAllow' => false])
    @include('frontend.userCenter.modalChangeUserRate')
</div>
@endsection