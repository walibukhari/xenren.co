@extends('frontend.layouts.default')

@section('title')
    {{trans('common.receive_project_title')}}
@endsection

@section('keywords')
@endsection

@section('description')
@endsection

@section('author')
@endsection

@section('url')
    https://www.xenren.co/myReceiveOrder
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
    <link href="../custom/css/frontend/awardedProject.css" rel="stylesheet" type="text/css" />
@endsection

@section('customStyle')
    <style type="text/css">
        .portlet{
            padding:0px !important;
        }
        .setRowCol12{
            margin-top:4.4%;
        }
        @media (max-width:2000px) and (min-width:1414px)
        {
            .setRowCol12{
                margin-top:2.7%;
            }
        }
        .setHRP{
            display: block;
        }
        .setISKILLMRPAGE{
            display: inline-block !important;
        }
        .setLABELMRJPTIT{
            margin-left:10px !important;
        }
        @media (max-width:1029px)
        {
            .setRECEVIEDORDERS {
                margin-left: 38%;
            }
        }
    </style>
@endsection

@section('content')
    <style>
        .fa-trash {
            border: none;
        }
        .fa-pencil {
            border: none;
        }
    </style>
    <div class="row setRowCol12">
        <div class="col-md-3 search-title-container">
            <span class="find-experts-search-title text-uppercase setMAinMENU">{{ trans('common.main_action') }}</span>
        </div>
        <div class="col-md-9 setCol9RPP">
            <span class="find-experts-search-title text-uppercase setRECEVIEDORDERS">{{trans('common.recevied_projects')}}</span>
        </div>
    </div>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row setPageeContent">
                <div class="col-md-12 col-sm-12">
                    <div class="portlet light bordered">
                        <div class="portlet-body award-body setMRPPPBAB">
                            <div class="row p-0 m-0">
                                <div class="col-md-12 award-hedding-title award-section">
                                    <h1 class="p-0 m-0 ">{{trans('common.recevied_offers')}}<span class="label label-success label-title setLABELMRJPTIT">
                                            {{count($offers)}}
                                        </span></h1>
                                </div>
                            </div>
                            @if($offers->isEmpty())
                                <div class="no-record-yet">
                                    <p>{{trans('common.no-record-received')}}</p>
                                    <img src="{{asset('images/norecord2.png')}}">
                                </div>
                            @else
                                <div class="row p-0 m-0">
                                    @if(count($offers) > 0)
                                        @foreach($offers as $k => $v)
                                            @if($v->project)
                                                <div class="col-md-12 award-section award-description">
                                                    <div class="row">
                                                        <div class="col-md-9">
                                                            @php
                                                                $projectType = $v->project->type == \App\Models\Project::PROJECT_TYPE_COMMON ? 'Public': 'Official';
                                                                $projectPayType = $v->project->pay_type == \App\Models\Project::PAY_TYPE_FIXED_PRICE ? trans('common.fixed'): trans('common.hourly');
                                                            @endphp
                                                            <h2 class="m-0">{{$v->project->name}}</h2>
                                                            <p  class="hourly-rate-part setHRP">{{$projectPayType}} : <span class="hour-part">{{isset($v->project->reference_price) ? $v->project->reference_price : ''}} USD / hr</span>
                                                                @if(count($v->project->projectSkills) > 0)
                                                                    @foreach($v->project->projectSkills as $projectSkill)
                                                                        <span class="item-skill white setISKILLMRPAGE"><span>{{$projectSkill->skill->name_en}}</span></span>
                                                                    @endforeach
                                                                @endif
                                                            </p>
                                                            <p class="description-part">{{trans('common.description')}} : {!! str_limit($v->project->description, $limit = 250, $end = '...') !!}</p>
                                                        </div>
                                                        <div class="col-md-3 received-left-icon-section pull-right award-description-btn-section setOFFERRBTN">
                                                <span>
                                                <a href="#"data-toggle="tooltip" title="Message">
                                                    <img onclick="window.location.href='{{route('frontend.inbox',['lang' => \Session::get('lang')])}}'" style="width:39px;" src="{{asset('images/icons/messages copy.png')}}">
                                                    <span class="set-counter-span-receivedprojectinboxmessages">{{count($v->messages)}}</span>
                                                </a></span>

                                                            <span><a href="{{ route('frontend.joindiscussion.getproject', ['lang' => \Session::get('lang'), 'slug' => $v->project->name, 'id' => $v->project->id ]) }}#sec-chat" data-toggle="tooltip" title="Join Discussion"><img src="{{asset('images/icons/join-discussion.png')}}"></a></span>
                                                            <a class="acceptSendOffer"
                                                               data-url="{{ route('frontend.inbox.acceptsendoffer') }}"
                                                               data-receiver-id="{{ $v->from_user_id }}"
                                                               data-inbox-id="{{ $v->id }}">
                                                                <img src="{{asset('images/icons/accept.png')}}" data-toggle="tooltip" title="Accept">
                                                            </a>
                                                            <a class="rejectSendOffer"
                                                               data-url="{{ route('frontend.inbox.rejectsendoffer') }}"
                                                               data-receiver-id="{{ $v->from_user_id }}"
                                                               data-inbox-id="{{ $v->id }}">
                                                                <img src="{{asset('images/icons/reject.png')}}" data-toggle="tooltip" title="Reject">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach

                                    @endif

                                    <div class="col-md-12 footer-section">
                                        <div class="row">
                                            @if(!$viewAll)
                                                <div class="col-md-6 footer-section-right">
                                                    <a href="?viewAll=true">{{trans('common.view_all')}}</a>
                                                </div>
                                            @endif

                                            <div class="col-md-6 footer-section-left-paginate text-right">
                                                {{$offers->links()}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            </div>
                        </div>



                        <div class="portlet light bordered">
                            <div class="portlet-body award-second-section setMRPPPBAB">
                                <div class="row p-0 m-0">
                                    <div class="col-md-12 award-second-section-title award-section">
                                        <h1 class="p-0 m-0 seth1pp">{{trans('common.progressing_projects')}}<span class="label label-success label-title setLABELMRJPTIT">{{count($progressingProjects)}}</span></h1>
                                    </div>
                                </div>
                                @if($progressingProjects->isEmpty())
                                    <div class="no-record-yet">
                                        <p>{{trans('common.no-record-progressing')}}</p>
                                        <img src="{{asset('images/norecord.png')}}">
                                    </div>
                                @else
                                    <div class="row p-0 m-0">
                                        @if(count($progressingProjects) > 0)
                                            @foreach($progressingProjects as $k => $progressingProject)
                                                @php
                                                    $projectPayType = isset($progressingProject->pay_type) && $progressingProject->pay_type == \App\Models\Project::PAY_TYPE_FIXED_PRICE ? trans('common.fixed'): trans('common.hourly');
                                                @endphp
                                                @if(isset($progressingProject->project) && isset($progressingProject->project->id))
                                                    <div class="col-lg-12 award-second-section-description award-section-second">
                                                        <div class="row">
                                                            <div class="col-md-1">
                                                                <img src="{{$progressingProject->user->img_avatar?$progressingProject->user->img_avatar:'images/N-team2.png'}}" class="user-icon">
                                                            </div>
                                                            <div class="col-md-5 award-second-section-center setAPCol8">
                                                                <h3 class="p-0 m-0">{{$progressingProject->user->name}}
                                                                    @if( $progressingProject->status == \App\Models\ProjectApplicant::STATUS_APPLY )
                                                                        <span class="label label-pill label-success">{{ trans('common.offer_send') }}</span>
                                                                    @elseif( $progressingProject->status == \App\Models\ProjectApplicant::STATUS_COMPLETED )
                                                                        <span class="label label-pill label-completed">{{ trans('common.completed') }}</span>
                                                                    @else
                                                                        <span class="label label-pill label-proccesing">{{ trans('common.in_progress') }}</span>
                                                                    @endif
                                                                    @if(isset($progressingProject->project->type) && $progressingProject->project->type == \App\Models\Project::PROJECT_TYPE_OFFICIAL)
                                                                        <img src="{{asset('images/budget.png')}}" alt="">
                                                                    @endif
                                                                </h3>
                                                                <p class="setMRPPP">{{isset($progressingProject->project->name) ? $progressingProject->project->name : ''}} <span class="project-price">{{$projectPayType}}</span> <span class="project-time">{{isset($progressingProject->project->reference_price) ? $progressingProject->project->reference_price : ''}}$</span></p>
                                                            </div>
                                                            <div class="col-md-3 second-part-icon pull-right award-description-btn-section award-description-btn-section-second setCol3MPRPBTN setCol3PRAD2">
                                                                @if(isset($progressingProject->project) && isset($progressingProject->project->id))
                                                                    <span><a href="{{ route('frontend.myreceiveorder.jobdetail', ['id' => $progressingProject->project->id, 'userId' => \Auth::user()->id    ]) }}#sec-common-project" data-toggle="tooltip" title="Order Details"><img src="{{asset('images/icons/edit-awarded.png')}}"></a></span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        @endif
                                        <div class="col-md-12 footer-section">
                                            <div class="row">
                                                <div class="col-md-6 footer-section-right">
                                                    @if(!$viewAllProgressing)
                                                        <div class="col-md-6 footer-section-right">
                                                            <a href="?viewAllProgressing=true">{{trans('common.view_all')}}</a>
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="col-md-6 footer-section-left-paginate text-right">
                                                    {{$progressingProjects->links()}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE BASE CONTENT -->
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
        @endsection


        @section('footer')
            <script>
                $(document).ready(function(){
                    $('[data-toggle="tooltip"]').tooltip();
                });

                $('.acceptSendOffer').click(function(){
                    var receiverId = $(this).data('receiver-id');
                    var inboxId = $(this).data('inbox-id');
                    var url = $(this).data('url');
                    $.ajax({
                        url: url,
                        dataType: 'json',
                        data: {receiverId: receiverId, inboxId: inboxId},
                        method: 'POST',
                        error: function () {
                            alert(lang.unable_to_request);
                        },
                        success: function (result) {
                            if(result.status == 'success') {
                                alertSuccess(result.msg);
                                setTimeout(function(){
                                    location.reload();
                                },1000);
                            }else{
                                alertError(result.msg);
                            }
                        }
                    });
                });

                $('.rejectSendOffer').click(function(){
                    var receiverId = $(this).data('receiver-id');
                    var inboxId = $(this).data('inbox-id');
                    var url = $(this).data('url');
                    $.ajax({
                        url: url,
                        dataType: 'json',
                        data: {receiverId: receiverId, inboxId: inboxId},
                        method: 'POST',
                        error: function () {
                            alert(lang.unable_to_request);
                        },
                        success: function (result) {
                            if(result.status == 'success') {
                                alertSuccess(result.msg);
                                setTimeout(function(){
                                    location.reload();
                                },1000);
                            }else{
                                alertError(result.msg);
                            }
                        }
                    });
                });
            </script>
@endsection


