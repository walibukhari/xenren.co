@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.receive_project_title') }}
@endsection

@section('keywords')
    {{ trans('common.receive_project_keyword') }}
@endsection

@section('description')
    {{ trans('common.receive_project_desc') }}
@endsection

@section('author')
    Xenren
@endsection

@section('header')
    <link href="/custom/css/frontend/myReceiveOrder.css" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3 search-title-container">
            <span class="find-experts-search-title text-uppercase">{{ trans('common.main_action') }}</span>
        </div>
        <div class="col-md-9 right-top-main">
            <div class="row">
                <div class="col-md-12 top-tabs-menu">
                    <div class="order-status-menu pull-right">
                        <ul>
                            <li {{ isset($status) && $status == 0 ? "class=active": ''}}>
                                <a href="{{ route('frontend.myreceiveorder')}}?status=0 ">
                                    {{ trans('common.all')  }}
                                </a>
                            </li>
                            <li {{ isset($status) && ( $status == \App\Models\ProjectApplicant::STATUS_APPLY ) ? "class=active": ''}}>
                                <a href="{{ route('frontend.myreceiveorder')}}?status={{ \App\Models\ProjectApplicant::STATUS_APPLY }}">
                                    {{ trans('common.offer_send') }}
                                </a>
                            </li>
                            <li {{ isset($status) && $status == \App\Models\ProjectApplicant:: STATUS_APPLICANT_ACCEPTED ? "class=active": ''}}>
                                <a href="{{ route('frontend.myreceiveorder')}}?status={{ \App\Models\ProjectApplicant:: STATUS_APPLICANT_ACCEPTED }}">
                                    {{ trans('common.progressing') }}
                                </a>
                            </li>
                            <li {{ isset($status) && $status == \App\Models\ProjectApplicant:: STATUS_COMPLETED ? "class=active": ''}}>
                                <a href="{{ route('frontend.myreceiveorder')}}?status={{ \App\Models\ProjectApplicant:: STATUS_COMPLETED }}">
                                    {{ trans('common.completed') }}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="caption">
                        <span class="find-experts-search-title text-uppercase">{{ trans('common.receive_project') }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
        @include('frontend.flash')
        <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row tab-content">
                <div class="col-md-12 col-sm-12 tab-pane active" id="tab_all">
                    <div class="found-count-top">
                        @if ($projectApplicants->lastPage() > 1)
                            <div class="pull-right">
                                {{ $projectApplicants->appends($filter)->links() }}
                            </div>
                        @endif

                        <div class="search-selectbox custom-days-selectbox">
                            <select class="form-control" id="lastNumberDays">
                                <option value="0"> {{ trans('common.all') }}</option>
                                @foreach ($lastNumberDayOptions as $key => $lastNumberDayOption)
                                    <option value="{{ $key }}"> {{ $lastNumberDayOption }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div style="padding-top: 5px;">
                            <span class="text">
                                @if( $status == 0 )
                                    {{ trans('common.all') }}
                                @elseif( $status == \App\Models\ProjectApplicant:: STATUS_APPLY  )
                                    {{ trans('common.offer_send') }}
                                @elseif( $status == \App\Models\ProjectApplicant:: STATUS_APPLICANT_ACCEPTED )
                                    {{ trans('common.in_progress') }}
                                @elseif( $status == \App\Models\ProjectApplicant:: STATUS_COMPLETED )
                                    {{ trans('common.completed') }}
                                @endif

                            </span>
                            <span class="number"> ({{ $projectCount }})</span>
                        </div>

                    </div>
                    <div class="portlet light bordered">
                        <div class="portlet-body">
                            <div class="jobs-list">
                                @if( $projectCount > 0 )
                                    @foreach($projectApplicants as $projectApplicant)
                                        @if( $projectApplicant->project->type == \App\Models\Project::PROJECT_TYPE_COMMON )
                                            <div class="row jobs-main-div">
                                                <div class="col-md-12 jobs-main-div-title">
                                                    <div class="row">
                                                        <div class="col-md-8 project-name">
                                                            <h4>{{ $projectApplicant->project->name }}</h4><br/>
                                                            <div class="project-type">
                                                                @if( $projectApplicant->project->pay_type == \App\Constants::PAY_TYPE_HOURLY_PAY)
                                                                    {{ trans('common.pay_hourly') }}
                                                                    <span> ${{ $projectApplicant->project->reference_price }}</span>
                                                                @elseif( $projectApplicant->pay_type == \App\Constants::PAY_TYPE_FIXED_PRICE)
                                                                    {{ trans('common.fixed_price') }}
                                                                    <span> ${{ $projectApplicant->project->reference_price }}</span>
                                                                @elseif( $projectApplicant->project->pay_type == \App\Constants::PAY_TYPE_MAKE_OFFER)
                                                                    {{ trans('common.request_quotation') }}
                                                                @endif
                                                            </div>
                                                            <div class="project-type">
                                                                {{ trans('common.project_type') }}
                                                                <span>{{ trans('common.public') }}</span>
                                                            </div>
                                                            <div class="is_read">
                                                                @if( $projectApplicant->is_read == 0 &&
                                                                    $projectApplicant->user_id ==\Auth::guard('users')->user()->id )
                                                                    <i class="fa fa-circle"></i>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="posted-by">
                                                                <img src="{{ $projectApplicant->project->creator->getAvatar() }}"
                                                                     class="img-circle pull-right" alt="" width="40px"
                                                                     height="40px">
                                                                <span>{{ $projectApplicant->project->getCreatedAt() }}</span>
                                                                <br><span>{!! $projectApplicant->project->getByCreator() !!}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 jobs-main-div-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            {{--<a href="{{ route('frontend.joindiscussion.getproject', ['id' => $projectApplicant->project->id ]) }}#sec-chat">--}}
                                                            {{--<button class="btn btn-join-discuss">--}}
                                                            {{--{{ trans('common.join_discuss_room') }}--}}
                                                            {{--</button>--}}
                                                            {{--</a>--}}

                                                            @if($projectApplicant->project->awardedApplicant->user_id == \Auth::user()->id)

                                                                <a href="{{ route('frontend.myreceiveorder.jobdetail', ['id' => $projectApplicant->project->id ]) }}">
                                                                    <button class="btn btn-color-custom">
                                                                        {{ trans('common.job_details') }}
                                                                    </button>
                                                                </a>
                                                            @endif

                                                            @if( $projectApplicant->status == \App\Models\ProjectApplicant::STATUS_APPLY  )
                                                                <a href="{{ route('frontend.myreceiveorder.removeoffer', ['project_id' => $projectApplicant->project->id, 'user_id' => \Auth::id() ]) }}"
                                                                   class="small btn btn-join-discuss" id="withdraw"
                                                                   data-project="{{ $projectApplicant->project->id }}">
                                                                    {{ trans('common.remove_offer') }}
                                                                </a>
                                                            @endif

                                                            @if( $projectApplicant->status == \App\Models\ProjectApplicant::STATUS_APPLICANT_ACCEPTED )
                                                                <a href="{{ route('frontend.endcontract.freelancer', ['projectApplicant_id' => $projectApplicant->id ]) }}"
                                                                   class="small btn btn-join-discuss" >
                                                                    {{ trans('common.end_contract') }}
                                                                </a>
                                                            @endif

                                                            @if( $projectApplicant->status == \App\Models\ProjectApplicant::STATUS_APPLY ||
                                                                $projectApplicant->status == \App\Models\ProjectApplicant::STATUS_APPLICANT_ACCEPTED
                                                            )
                                                                <a href="{{ route('frontend.joindiscussion.getproject', ['lang'=>\Session::get('lang'),'slug' => $projectApplicant->project->name,'id' => $projectApplicant->project->id ]) }}#sec-chat">
                                                                    <button class="btn btn-join-discuss">
                                                                        {{ trans('common.join_discuss_room') }}
                                                                    </button>
                                                                </a>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-6 right">
                                                            <div class="pull-right">
                                                                @if( $projectApplicant->status == \App\Models\ProjectApplicant::STATUS_APPLY )
                                                                    <div class="job-in job-in-submitted">
                                                                        <i class="fa fa-check-circle"
                                                                           aria-hidden="true"></i>
                                                                        <span>{{ trans('common.offer_send') }}</span>
                                                                    </div>
                                                                @elseif( $projectApplicant->status == \App\Models\ProjectApplicant::STATUS_APPLICANT_ACCEPTED )
                                                                    <div class="job-in job-in-progress">
                                                                        <i class="fa fa-check-circle"
                                                                           aria-hidden="true"></i>
                                                                        <span>{{ trans('common.in_progress') }}</span>
                                                                    </div>
                                                                @elseif( $projectApplicant->status == \App\Models\ProjectApplicant::STATUS_COMPLETED )
                                                                    <div class="job-in job-in-completed">
                                                                        <i class="fa fa-check-circle"
                                                                           aria-hidden="true"></i>
                                                                        <span>{{ trans('common.completed') }}</span>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @elseif( $projectApplicant->project->type == \App\Models\Project::PROJECT_TYPE_OFFICIAL )
                                            <div class="row jobs-main-div jobs-main-div-a">
                                                <div class="col-md-3 jobs-main-div-img" style="padding-left: 0px">
                                                    <img src="{{ $projectApplicant->project->officialProject->out_cover }}" alt="">
                                                    <div class="shares">
                                                        <span>{{ trans('common.shares') }}:</span>
                                                        <h2>{{ $projectApplicant->project->officialProject->stock_total }}</h2>
                                                    </div>
                                                </div>
                                                <div class="col-md-9 jobs-main-div-rightc">
                                                    <div class="row">
                                                        <div class="col-md-8 project-name">
                                                            <h4>{{ $projectApplicant->project->officialProject->title }}</h4>
                                                            <img src="/images/aword-icon.png">

                                                            <p class="project-deta">
                                                                <span>
                                                                    {{ trans('common.project_type') }}
                                                                    <strong>{{ trans('common.official_project') }}</strong>
                                                                </span>
                                                                <span>
                                                                    {{ trans('common.time_frame') }} <strong> {{ $projectApplicant->project->officialProject->getTimeFrame() }}
                                                                        days</strong>
                                                                </span>
                                                                <span class="is_read">
                                                                    @if( $projectApplicant->is_read == 0 &&
                                                                        $projectApplicant->user_id ==\Auth::guard('users')->user()->id )
                                                                        <i class="fa fa-circle"></i>
                                                                    @endif
                                                                </span>
                                                                <br>
                                                                <span>
                                                                    {{ trans('common.end_date') }}
                                                                    <strong>{{ $projectApplicant->project->officialProject->getRecruitEndDate() }}</strong>
                                                                </span>
                                                                @if( $projectApplicant->project->officialProject->budget_switch == 1)
                                                                    <span>
                                                                    {{ trans('common.budget') }}
                                                                        <strong> ${{ $projectApplicant->project->officialProject->budget_from }}
                                                                            - ${{ $projectApplicant->project->officialProject->budget_to }}</strong>
                                                                </span>
                                                                @endif
                                                                <br>
                                                                <span>
                                                                    {{ trans('common.funding_stage') }}
                                                                    <strong> {{ \App\Constants::translateFundingStage($projectApplicant->project->officialProject->funding_stage) }} </strong>
                                                                </span>
                                                                <span>
                                                                    {{ trans('common.location') }}
                                                                    <strong> Zhang Hai</strong>
                                                                </span>
                                                            </p>
                                                        </div>
                                                        <div class="col-md-6 position">
                                                            {{ trans('common.job_position') }}:
                                                            <strong>
                                                                @foreach( $projectApplicant->project->officialProject->projectPositions as $key => $projectPosition)
                                                                    @if( $key != 0 )
                                                                        {{ ", " }}
                                                                    @endif

                                                                    {{  $projectPosition->jobPosition->getName() }}
                                                                @endforeach
                                                            </strong>
                                                        </div>
                                                        <div class="col-md-6 dificulty">
                                                            <span> {{ trans('common.difficulty') }} : </span>
                                                            @for ($i = 0; $i < $projectApplicant->project->officialProject->project_level; $i++)
                                                                <i class="fa fa-star fill"></i>
                                                            @endfor

                                                            @for ($i = 0; $i < ( 6 - $projectApplicant->project->officialProject->project_level ); $i++)
                                                                <i class="fa fa-star"></i>
                                                            @endfor
                                                        </div>
                                                        <div class="col-md-6 posted-by">
                                                            <div class="row" style="margin-right: -30px;">
                                                                <img src="/assets/layouts/layout/img/avatar9.jpg"
                                                                     class="img-circle pull-right" alt="">
                                                                <span>
                                                                    {{ $projectApplicant->project->getCreatedAt() }}
                                                                </span>
                                                                <br>
                                                                <span>
                                                                    {!! $projectApplicant->project->getByCreator() !!}
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 jobs-main-div-body">
                                                            <div class="row">
                                                                <div class="col-md-9">
                                                                    {{--<a href="{{ route('frontend.joindiscussion.getproject', ['id' => $projectApplicant->project->id ]) }}#sec-official-project">--}}
                                                                    {{--<button class="btn btn-color-custom">--}}
                                                                    {{--{{ trans('common.job_details') }}--}}
                                                                    {{--</button>--}}
                                                                    {{--</a>--}}

                                                                    @if( $projectApplicant->status == \App\Models\ProjectApplicant::STATUS_APPLICANT_ACCEPTED )
                                                                        <a href="{{ route('frontend.endcontract.freelancer', ['projectApplicant_id' => $projectApplicant->id ]) }}" class="small btn btn-join-discuss" >
                                                                            {{ trans('common.end_contract') }}
                                                                        </a>
                                                                    @endif

                                                                    @if( $projectApplicant->status == \App\Models\ProjectApplicant::STATUS_APPLY  )
                                                                        <a href="{{ route('frontend.myreceiveorder.removeoffer', ['project_id' => $projectApplicant->project->id, 'user_id' => \Auth::id() ]) }}"
                                                                           class="small btn btn-join-discuss" id="end"
                                                                           data-project="">
                                                                            {{ trans('common.remove_offer') }}
                                                                        </a>
                                                                    @endif

                                                                    @if($projectApplicant->status == \App\Models\ProjectApplicant::STATUS_APPLY ||
                                                                        $projectApplicant->status == \App\Models\ProjectApplicant::STATUS_APPLICANT_ACCEPTED
                                                                    )
                                                                        <a href="{{ route('frontend.joindiscussion.getproject', ['lang'=>\Session::get('lang'),'slug' => $projectApplicant->project->name, 'id' => $projectApplicant->project->id ]) }}#sec-chat">
                                                                            <button class="btn btn-join-discuss">
                                                                                {{ trans('common.join_discuss_room') }}
                                                                            </button>
                                                                        </a>
                                                                    @endif
                                                                </div>
                                                                <div class="col-md-3 right">
                                                                    <div class="pull-right">
                                                                        @if( $projectApplicant->status == \App\Models\ProjectApplicant::STATUS_APPLICANT_ACCEPTED )
                                                                            <div class="job-in job-in-progress">
                                                                                <i class="fa fa-check-circle"
                                                                                   aria-hidden="true"></i>
                                                                                <span>{{ trans('common.in_progress') }}</span>
                                                                            </div>
                                                                        @endif

                                                                        @if( $projectApplicant->status == \App\Models\ProjectApplicant::STATUS_APPLY )
                                                                            <div class="job-in job-in-submitted">
                                                                                <i class="fa fa-check-circle"
                                                                                   aria-hidden="true"></i>
                                                                                <span>{{ trans('common.offer_send') }}</span>
                                                                            </div>
                                                                        @endif

                                                                        @if( $projectApplicant->status == \App\Models\ProjectApplicant::STATUS_COMPLETED )
                                                                            <div class="job-in job-in-completed">
                                                                                <i class="fa fa-check-circle"
                                                                                   aria-hidden="true"></i>
                                                                                <span>{{ trans('common.completed') }}</span>
                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                @else
                                    <div class="text-center" style="padding:250px 0px">
                                        {{ trans('common.no_receive_project') }}
                                        <br/>
                                        <img src="\images\no-receive-project-icon.jpg">
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT -->

@endsection


@section('footer')
    <script src="/assets/global/plugins/jquery-countdown/jquery.countdown.min.js" type="text/javascript"></script>
    <script src="/custom/js/frontend/myReceiveOrder.js" type="text/javascript"></script>
    <script>
        +function () {
            $(document).ready(function () {
                $('#lastNumberDays').on('change', function (e) {
                    var optionSelected = $("option:selected", this);
                    var valueSelected = this.value;

                    var url = "{{ route('frontend.myreceiveorder') }}" + "?status=" + "{{ $status }}" + "&lastNumberDays=" + valueSelected;
                    window.location.href = url;
                });
            });
        }(jQuery);
    </script>
@endsection




