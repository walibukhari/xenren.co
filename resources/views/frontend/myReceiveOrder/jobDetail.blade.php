@extends('frontend.layouts.default')

@section('title')
    Published Job Milestone
@endsection

@section('description')
    Published Job Milestone
@endsection

@section('author')
    Xenren
@endsection

@section('url')
    https://www.xenren.co/myReceiveOrder/jobdetail
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
{{--    <link href="/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="/custom/css/frontend/userCenter.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="/custom/css/frontend/fixedPriceEscrowModule.css" rel="stylesheet">--}}
{{--    <link href="/custom/css/frontend/jobDetails.css" rel="stylesheet">--}}
{{--    <link href="/custom/css/frontend/jobMilestone.css" rel="stylesheet" type="text/css">--}}
{{--    <link href="/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />--}}
<link rel="stylesheet" href="{{asset('css/JobDetail.css')}}">
    <style>
        .desc {
            color: #333;
        }
    </style>
@endsection
@section('modal')
])
@include('frontend.modals.disputeModal',[
    'message' => 'Tell us what\'s happening',
])

@include('frontend.modals.modal',[
    'message' => 'Are you sure you want to refund?',
    'id' => 'refundModal',
    'successClass' => 'confirmRefundMilestoneBtn'
])

@include('frontend.modals.modal',[
    'message' => 'Are you sure you want to refund?',
    'id' => 'refundRequestFromEmployerModal',
    'successClass' => 'confirmRequestFromEmployerMilestoneBtn',
    'reason' => true
])

@include('frontend.modals.modal',[
    'message' => 'Are you sure you want to refund?',
    'id' => 'rejectRefundFreelancer',
    'successClass' => 'confirmRejectRefundFreelancer',
    'commentId' => 'rejectRefundFreelancerComment'
])

@include('frontend.modals.modal',[
    'message' => 'Are you sure you want cancel dispute request?',
    'id' => 'cancelDisputeModel',
    'successClass' => 'confirmCancelDisputeFreelancer',
    'commentId' => 'confirmCancelDisputeFreelancerComment',
    'reason' => true
])

@endsection
@section('content')

    <div class="page-content-wrapper setPageContentWrapperMROPAGE">
        <div class="col-md-12 usercenter link-div">
            <a href="{{url('myReceiveOrder')}}" class="text-uppercase raleway">
	        <span class="fa fa-angle-left">
	        </span>
                {{trans('common.back_to_my_orders')}}
            </a>
        </div>
        <div class="clearfix"></div>
        <br>
        <?php
        $data = isset($project) ?$project : false;
        $projectId = isset($data->id) ?$data->id : 0;
        $applicantId = isset($applicant->id) ? $applicant->id : 0;
        $languages = isset($data->awardedApplicant->user->languages) ? $data->awardedApplicant->user->languages : false;
        ?>
        <script>
            window.ProjectId = '{{$projectId}}';
            window.token = '{{csrf_token()}}';
        </script>

        <div class="col-md-12">
            <div class="row main-div">
                <div class="col-md-12 personal-profile setPPJDPAGEMRO">
                    <div class="row">
                        <div class="col-md-8 setspn1JMPP">
                            <span class="set-mainrow-span1">{{trans('common.about_the_client')}}</span>
                        </div>
                        <div class="col-md-8 pull-right set-col-8-right-area">
                            <div class="row user-info">
                                <div class="col-md-2 text-center">
                                    <div class="profile-userpic setProfileUserPICJDP">
                                        <img alt="" class="img-responsive" src={{asset($data->creator->img_avatar)}}>
                                    </div>
                                </div>
                                <div class="col-md-10 setuserbasicinfo basic-info">
                                    <div class="row">
                                        <div class="col-md-12 user-full-name">
                                            <span class="full-name">{{$data->creator->name}}</span>
                                            <img src="/images/icons/check.png">
                                        </div>
                                        <div class="col-md-10">
                                            <small class="set-This-small">Member since {{\Carbon\Carbon::parse($data->creator->created_at)->toDateTimeString()}}</small>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="sub-con set-sub-con">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-o"></i>
                                                <i class="fa fa-star-o"></i>

                                                <span class="set-spnof-JMp">
													4.98 of 35 reviews
												</span>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row set-row-main">
                                        <div class="col-md-4 set-col-4-top">
                                            <small class="setSmall">{{isset($data->creator->country) ? $data->creator->country->name : 'N/A'}}</small><br>
                                            <span class="set-spn2-JMP">{{isset($data->creator->city) ? $data->creator->city->name : 'N/A'}} {{--11:25 am--}} </span>
                                        </div>
                                        <div class="col-md-8 set-col-btn-8">
                                            {{--<button class="btn btn-success btn-green-bg-white-text setBNTNPJMBP" type="button" onClick="window.location='/inbox'"><i class="fa fa-comments set-fa-comments"></i>{{trans('common.chat')}}</button>--}}
                                            <button class="btn btn-success btn-green-bg-white-text setBNTNPJMBP" type="button" data-toggle="modal" data-target="#sendPrivateMessage"><i class="fa fa-comments set-fa-comments"></i>{{trans('common.chat')}}</button>

                                            <!-- private message modal -->
                                            <!-- Modal -->
                                            <div id="sendPrivateMessage" class="modal fade" role="dialog">
                                                <div class="modal-dialog">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <a type="button" class="set-cross-click-pjmp" data-dismiss="modal">&times;</a>
                                                            <h4 class="modal-title text-left">{{ trans('common.send_message') }}</h4>
                                                        </div>

                                                        {!! Form::open(['route' => ['frontend.sendprivatemessage.post', 'user_id' => $data->user_id], 'method' => 'post', 'role' => 'form', 'id' => 'send-message-form']) !!}
                                                        <div class="modal-body">
                                                            <div class="form-body">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <label>{{ trans('common.message') }}</label>
                                                                        <div class="input-group">
                                                                            {{ Form::textarea('message', null, ['class' => 'form-control', 'cols' => '100', 'rows' => '3', 'id' => 'msg']) }}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="project_id" value="{{$data->id}}">
                                                        <input type="hidden" name="to_user_id" id="user_id" value="{{$data->user_id}}">
                                                        {!! Form::close() !!}
                                                        <div class="modal-footer">
                                                            <button class="btn btn-success sendMEssagebtn" id="btnSubmit">Send Message</button>
                                                            <button type="button" class="btn btn-default btndefault-sendMessage" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!-- private message modal -->
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row set-row-main-2">
                                        <div class="col-md-12">
                                            @php
                                                $totalProjects = \App\Models\Project::where('user_id', '=', $data->creator->id)->count();
                                                $activeProjects = \App\Models\Project::where('user_id', '=', $data->creator->id)->where('status', '=', \App\Models\Project::STATUS_HIRED)->orWhere('status', '=', \App\Models\Project::STATUS_SELECTED)->count();
                                                $awardedProjects = \App\Models\Project::where('user_id', '=', $data->creator->id)->whereHas('awardedApplicant', function($q){
                                                    $q->where('status', '=', \App\Models\ProjectApplicant::STATUS_CREATOR_SELECTED)
                                                        ->orWhere('status', '=', \App\Models\ProjectApplicant::STATUS_APPLICANT_ACCEPTED);
                                                })->count();
                                                $openJobs = \App\Models\Project::where('user_id', '=', $data->creator->id)->whereHas('awardedApplicant', function($q){
                                                    $q->where('status', '=', \App\Models\ProjectApplicant::STATUS_CREATOR_SELECTED)
                                                        ->orWhere('status', '=', \App\Models\ProjectApplicant::STATUS_APPLICANT_ACCEPTED);
                                                })->count();
                                                $avgHireRate = ($awardedProjects / $totalProjects) * 100;
                                                $awardedProjectsAmount = \App\Models\ProjectApplicant::whereHas('project', function($q) use ($data) {
                                                    $q->whereHas('creator', function($q) use ($data) {
                                                        $q->where('id', '=', $data->creator->id);
                                                    });
                                                })->avg('quote_price');
                                            @endphp
                                            <small class="setSmall">{{\App\Models\Project::where('user_id', '=', $data->creator->id)->count()}} jobs posted</small>
                                        </div>
                                        <div class="col-md-12">
                                            <span class="set-spn2-JMP">{{$avgHireRate}}% hire rate, {{$totalProjects-$awardedProjects}} open jobs</span>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row set-row-main-3">
                                        <div class="col-md-12">
                                            <small class="setSmall">Over ${{\App\Models\Transaction::where('performed_by', '=', $data->creator->id)->where(function($q){
                                                $q->where('type', '=', \App\Models\Transaction::TYPE_PROJECT_PAYMENT)->orWhere('type', '=', \App\Models\Transaction::TYPE_MILESTONE_PAYMENT);
                                            })->sum('amount')}} total spent</small>
                                        </div>
                                        <div class="col-md-12">
                                            <span class="set-spn2-JMP">{{$totalProjects}} hires, {{$activeProjects}} active</span>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row set-row-main-4">
                                        <div class="col-md-12">
                                            <small class="setSmall">${{round($awardedProjectsAmount, 2)}}/hr avg hourly rate paid</small>
                                        </div>
                                        <div class="col-md-12">
                                            {{--<span class="set-spn2-JMP">3.904 hours</span>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 contacts-info setContactInfo">
                                    <div class="row">
                                        <div class="col-md-3 col-md-3 contacts-info-sub setPHMPPP">
                                            <img src="/images/skype-icon-5.png"> <span class="setSP0">{{trans('common.skype')}}</span>
                                            <br><br>
                                            <i>{{$data->creator->skype_id or 'N/A'}}</i>
                                        </div>
                                        
                                        <div class="col-md-3 col-md-3 contacts-info-sub setPHMPPP">
                                            <img src="/images/wechat-logo.png"> <span class="setSP0">{{trans('common.wechat')}}</span>
                                            <br><br>
                                            <i>{{$data->creator->wechat_id or 'N/A'}}</i>
                                        </div>
                                        
                                        <div class="col-md-3 col-md-3 contacts-info-sub setPHMPPP">
                                            <img src="/images/line.png"> <span class="setSP0">{{trans('common.line')}}</span>
                                            <br><br>
                                            <i>{{$data->creator->line_id or 'N/A'}}</i>
                                        </div>
                                        
                                        <div class="col-md-3 col-md-3 contacts-info-sub setPHMPPP">
                                            <img src="/images/MetroUI_Phone.png"> <span class="setSP0">{{trans('common.phone')}}</span>
                                            <br><br>
                                            <i>{{$data->creator->phone_number}}</i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vr-1">&nbsp;</div>
                        <div class="col-md-4 pull-left setCol-md-4sla">
                            <div class="row basic-info setROWBINFO">
                                <div class="col-md-12 basic-info-title text-left setSKILLMRECEVIXOPAGE">
                                    <img alt="" src="/images/hands.png"> &nbsp;<span class="subtitle setSubtitle">{{trans('common.skill')}}</span>
                                </div>


                                <div class="col-md-12 scroll-div setCol12MRECEVIEORDEPAAGE">
                                    @foreach($data->awardedApplicant->user->skills as $v)
                                        <div class="skill-div">
	                                <span class="item-skill unverified">
	                    				<span>{{$v->skill->name_cn}}</span>
	                				</span>
                                        </div>
                                    @endforeach


                                    <div class="clearfix"></div>
                                </div>

                                <div class="col-md-12 basic-info-title text-left setLANGUAGEMORPAEG">
                                    <br><img alt="" src="/images/earth.png"> &nbsp;<span class="subtitle">{{trans('common.languagee')}}</span>
                                </div>

                                <div class="col-md-12 media similar-profiles text-left setLANGCol12">
                                    @foreach($languages as $v)
                                        <div class="media-left media-middle item-languages">
                                            <img class="pull-left media-object" src="{{isset($v->languageDetail->alpha2) ? url('/images/Flags-Icon-Set/24x24/'.strtoupper($v->languageDetail->alpha2).'.png') : url('/images/Flags-Icon-Set/24x24/HK.png')}}" alt="" onerror="this.src='{{url('/images/Flags-Icon-Set/24x24/HK.png')}}'" >
                                            <span class="pull-left">{{isset($v->languageDetail->name) ? $v->languageDetail->name : 'N/A'}}</span>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 prising-section text-center">
                    <div class="row">
                        <div class="col-md-4 prising-section-sub">
                            <img src="/images/icons/doller-icon.png"><br>
                            <p class="setSP0">{{trans('common.budget_for_the_project')}}</p>
                            <span class="setSP">${{$data->reference_price}} USD</span>
                        </div>
                        <div class="setB2"></div>
                        <div class="col-md-4 prising-section-sub">
                            <img src="/images/icons/time-icon.png"><br>
                            <p class="setSP0">{{trans('common.current_milestone')}}</p>
                            @php
                                $currentMilestoneAmount = 0;
                            @endphp
                            @foreach($data->milestones as $v)
                                @if(isset($v->milestoneStatus) && ($v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_INPROGRESS ||  $v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_CANCEL_REQUEST_BY_EMPLOYER))
                                   @php
                                       $currentMilestoneAmount = $v->amount;
                                   @endphp
                                @endif
                            @endforeach
                            <span class="setSP">{{$currentMilestoneAmount}}</span>
                        </div>
                        <div class="setB3"></div>
                            @if($data->status != \App\Models\Project::STATUS_COMPLETED)
                                <div class="col-md-4 prising-section-sub">
                                    <img src="/images/icons/doller-icon.png"><br>
                                    {{--{{dd($applicant->pay_type)}}--}}
                                    @if($applicant->pay_type == \App\Models\Project::PAY_TYPE_HOURLY_PAY)
                                        <p class="setSP0">{{ trans('common.hourly_price') }}</p>
                                    @else
                                        <p class="setSP0">{{ trans('common.fixed_price') }}</p>
                                    @endif
                                </div>
                            @endif
                    </div>
                </div>

                <div class="col-md-12 description">
                    <h4>{{$data->name}}</h4>
                </div>
                <div class="col-md-12 setDESC">
                <!-- <h5>{!!$data->description!!}</h5> -->
                    <div class="scroll-div">
                        <p>
                            {!!$data->description!!}
                        </p>
                    </div>
                </div>
                @if($data->status != \App\Models\Project::STATUS_COMPLETED)
                    <div class="col-md-12 milestone-desc">
                        <div  class="setULNAVTBS">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active text-center"><a href="#milestone" class="setLI" aria-controls="milestone" role="tab" data-toggle="tab">{{trans('common.milestone')}}</a></li>
                                <li role="presentation" class="text-center"><a href="#dispute" class="setLI" aria-controls="dispute" role="tab" data-toggle="tab">{{trans('common.refund')}}</a></li>
                                <li role="presentation" class="text-center"><a href="#contract-control-panel" class="setLI" aria-controls="contract-control-panel" role="tab" data-toggle="tab">{{trans('common.contract_control_panel')}}</a></li>
                                <li role="presentation" class="text-center"><a href="#end-contract" class="setLI" aria-controls="end-contract" role="tab" data-toggle="tab">{{trans('common.end_contract')}}</a></li>
                            </ul>
                        </div>
                        <div class="tab-content tabpanel-custom setROWNEWMILSTONE">
                            <div role="tabpanel" class="tab-pane active" id="milestone">

                                {{--<div class="row new-milestone-add">--}}
                                    {{--<div class="col-md-12">--}}
                                        {{--<label class="milestone-subpart-title setclientrequestDispute">{{trans('common.request_milestone')}}</label>--}}
                                        {{--<form accept="">--}}
                                            {{--<div class="form-group row">--}}

                                                {{--<div class="col-xs-3 setColXS">--}}
                                                    {{--<label for="ex2">{{trans('common.milestone_name')}}</label>--}}
                                                    {{--<select class="form-control" name="milestones_name" id="milestones_name">--}}
                                                        {{--@foreach($data->milestones as $v)--}}
                                                            {{--@if($v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_INPROGRESS)--}}
                                                                {{--<option value="{{ $v->id }}" data-amount="{{$v->amount}}">{{ $v->name }}</option>--}}
                                                            {{--@endif--}}
                                                        {{--@endforeach--}}
                                                    {{--</select>--}}
                                                    {{--<input type="hidden" id="_token" value="{{ csrf_token() }}">--}}
                                                    {{--<input class="form-control" id="milestones_name" type="text">--}}
                                                {{--</div>--}}
                                                {{--<div class="col-xs-4 setColXS">--}}
                                                    {{--<label for="ex1">{{trans('common.enter_amount')}}</label>--}}
                                                    {{--<input class="form-control" id="milestones_amount" type="text" />--}}
                                                {{--</div>--}}

                                                {{--<div class="col-xs-3 setColXS4">--}}
                                                    {{--<label for="ex2">{{trans('common.select_due_date')}}</label>--}}
                                                    {{--<div class="input-group date form_datetime">--}}
                                                        {{--<input id="milestones_duedate" type="text" class="form-control" readonly="" size="16">--}}
                                                        {{--<span class="input-group-btn setbtnJDP">--}}
															{{--<button type="button" class="btn default date-set">--}}
																{{--<i class="fa fa-calendar"></i>--}}
															{{--</button>--}}
                                                		{{--</span>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}

                                                {{--<div class="col-xs-5 setColXS">--}}
                                                    {{--<br>--}}
                                                    {{--<button class="btn btn-success btn-green-bg-white-text setBNBNJDP" id="confirmMilestoneFreelancer" type="button">{{trans('common.request')}}</button>--}}
                                                    {{--<button class="btn btn-success btn-white-bg-green-text setBNBNJDP" id="cancelMilestone" type="button">{{trans('common.Cancel')}}</button>--}}
                                                {{--</div>--}}

                                            {{--</div>--}}
                                        {{--</form>--}}
                                    {{--</div>--}}
                                {{--</div>--}}


                                <div class="col-md-12">
                                    <div id="currentmsdiv">
                                        <br/>
                                        <label class="milestone-subpart-title">{{trans('common.current_milestone')}}</label>
                                        <div  class="setTBLEMRJOP">
                                            <table class="table custom-table">
                                                @foreach($data->milestones as $v)
                                                    @if(isset($v->milestoneStatus) && ($v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_INPROGRESS ||  $v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_CANCEL_REQUEST_BY_EMPLOYER))
                                                        <tr>
                                                            <td width="30px"><i class="fa fa-circle"></i></td>
                                                            <td>
                                                                <span class="title">{{trans('common.amount')}}</span><br>
                                                                <span class="desc">${{$v->amount}}</span>
                                                            </td>
                                                            <td>
                                                                <span class="title">{{trans('milestone_name')}}</span><br>
                                                                <span class="desc">{{$v->name}}</span>
                                                            </td>
                                                            <td>
                                                                <span class="title">{{trans('common.milestone_description')}}</span><br>
                                                                <span class="desc">{{$v->description}}</span>
                                                            </td>
                                                            <td>
                                                                <span class="title">{{trans('common.due_date')}}</span><br>
                                                                <span class="desc">{{$v->due_date}}</span>
                                                            </td>
                                                            @if($v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_REJECTED)
                                                                @if(isset($data) && $data && \Auth::user() && $data->user_id == \Auth::user()->id)
                                                                    <td>
                                                                        <label for="">{{trans('common.change_status')}}</label>
                                                                        <select name="" id="changeStatus" class="form-control changeStatus" data-milestone="{{$v->id}}">
                                                                            <option value="">{{trans('common.select_status')}}</option>
                                                                            <option value="1">{{trans('common.in_progress')}}</option>
                                                                            <option value="2">{{trans('common.confirm')}}</option>
                                                                        </select>
                                                                    </td>
                                                                @endif
                                                            @endif
                                                            @if($v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_REQUESTED)
                                                                @if(isset($data) && $data && \Auth::user() && $data->user_id == \Auth::user()->id)
                                                                    <td>
                                                                        <select name="" id="changeStatus" class="form-control changeStatus" data-milestone="{{$v->id}}">
                                                                            <option value="">{{trans('common.select_status')}}</option>
                                                                            <option value="{{\App\Models\ProjectMilestones::STATUS_REJECTED}}">{{trans('common.confirm')}}</option>
                                                                            <option value="{{\App\Models\ProjectMilestones::STATUS_INPROGRESS}}">{{trans('common.Cancel')}}</option>
                                                                        </select>
                                                                    </td>
                                                                @endif
                                                            @endif
                                                            @if($v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_CANCEL_REQUEST_BY_EMPLOYER)
                                                                <td class="setTABLETDJDP" style="color:black">
                                                                    <span class="title">{{trans('common.employer_request_to_cancel')}}</span><br>
                                                                    <select name="" id="changeStatus" class="form-control changeStatus" data-milestone="{{$v->id}}">
                                                                        <option value="">{{trans('common.select_status')}}</option>
                                                                        <option value="{{\App\Models\ProjectMilestones::STATUS_REJECTED}}">{{trans('common.confirm')}}</option>
                                                                        <option value="{{\App\Models\ProjectMilestones::STATUS_INPROGRESS}}">{{trans('common.Cancel')}}</option>
                                                                    </select>
                                                                    @if(isset($v->milestoneStatus->comment))
                                                                        <p  style="color: black"><b style="color: black">Reason:</b> {{$v->milestoneStatus->comment}}</p>
                                                                    @endif
                                                                </td>
                                                            @endif
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row release-milestone ">
                                    <div class="col-md-12">
                                        <label class="milestone-subpart-title">{{trans('common.milestone_that_already_earned')}} <span class="green" id="tt"> $0</span></label>
                                        <div class="setTBLEMRJOP">
                                            <table class="table custom-table">
                                            <?php
                                            $total = 0;
                                            $md = 0;
                                            $msd=0;
                                            ?>

                                            @foreach($data->milestones as $v)
                                                    @if($v->milestoneStatus->status == \App\Models\Transaction::STATUS_APPROVED || $v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_DISPUTED || $v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_FREELANCER_REJECTED_REFUND)
                                                    <?php
                                                    $total = $total + $v->amount;
                                                    ?>
                                                    @if($v->amount > 0)
                                                    <tr>
                                                        <td width="30px"><i class="fa fa-circle setTTTTDIFAFA"></i></td>
                                                        <td>
                                                            <span class="title">{{trans('common.amount')}}</span><br>
                                                            <span class="desc">${{$v->amount}}</span>
                                                        </td>
                                                        <td>
                                                            <span class="title">{{trans('common.milestone_name')}}</span><br>
                                                            <span class="desc">{{$v->name}}</span>
                                                        </td>
                                                        <td>
                                                            <span class="title">{{trans('common.due_date')}}</span><br>
                                                            <span class="desc">{{$v->due_date}}</span>
                                                        </td>
                                                        <td width="300px">
                                                            <span class="title">{{trans('common.description')}}</span><br>
                                                            <span class="desc">{{$v->description}}</span>
                                                        </td>
                                                        <td>
                                                            @if(!is_null($v->filedDisputes))
                                                                <b class="setFDJDPBC" id="ttr">{{trans('common.filed_dispute')}}</b>
                                                                <a href="/project/disputeDetail/{{$v->filedDisputes->id}}">{{trans('common.see_detail')}}</a><br/>
                                                                @if(isset($v->filedDisputes->status) && $v->filedDisputes->status == \App\Models\ProjectFileDisputes::STATUS_FILE_DISPUTE_IN_APPROVED)
                                                                    <span class="setSPANNN1JDP">{{trans('common.approved')}}</span>
                                                                @endif
                                                                @if(isset($v->filedDisputes->status) && $v->filedDisputes->status == \App\Models\ProjectFileDisputes::STATUS_FILE_DISPUTE_IN_REJECTED)
                                                                    <span class="setSPANNN2JDP">{{trans('common.rejected')}}</span>
                                                                @endif
                                                            @else
                                                            <button class="btn btn-success btn-green-bg-white-text refundMilestoneBtn" id="refund-milestone-btn-{{$v->id}}" type="button" data-milestone="{{$v->id}}" >{{trans('common.refund')}}</button>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endif
                                                @endif
                                            @endforeach

                                        </table>
                                        </div>
                                    </div>
                                </div>


                                <div class="row release-milestone">
                                    <div class="col-md-12">
                                        <label class="milestone-subpart-title">{{trans('common.refund_requests')}}</label>
                                        <div class="setTBLEMRJOP">
                                            <table class="table custom-table">

                                            @foreach($data->milestones as $v)
                                                @if($v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_DISPUTED)
                                                    <tr>
                                                        <td width="30px"><i class="fa fa-circle"></i></td>
                                                        <td>
                                                            <span class="title">{{trans('common.released_amount')}}</span><br>
                                                            <span class="desc">{{$v->amount}}$</span>
                                                        </td>
                                                        <td>
                                                            <span class="title">{{trans('common.milestone_name')}}</span><br>
                                                            <span class="desc">{{$v->name}}</span>
                                                        </td>
                                                        <td>
                                                            <span class="title">{{trans('common.due_date')}}</span><br>
                                                            <span class="desc">{{$v->due_date}}</span>
                                                        </td>
                                                        <td width="300px">
                                                            <span class="title">{{trans('common.description')}}</span><br>
                                                            <span class="desc">{{$v->description}}</span>
                                                        </td>
                                                        <td width="20%" style="color:black">

                                                            <button  class="btn btn-success btn-green-bg-white-text refundDisputeMilestoneBtn setJDPBUTTON1" id="refund-milestone-btn-{{$v->id}}" type="button" data-milestone="{{$v->id}}" >{{trans('common.refund')}}</button>

                                                            <button  class="btn btn-success btn-white-bg-green-text rejectRefundRequestBtn setJDPBUTTON2" id="refund-milestone-btn-{{$v->id}}" type="button" data-milestone="{{$v->id}}" >{{trans('common.reject')}}</button><br/>

                                                            <b>Reason:</b> {{$v->milestoneStatus->comment}}
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach

                                        </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row release-milestone">
                                    <div class="col-md-12">
                                        <label class="milestone-subpart-title">{{trans('common.requesting_milestones')}}</label>
                                        <div class="setTBLEMRJOP">
                                            <table class="table custom-table">
                                            @php
                                                $requestedMilestones = collect($data->milestones)->sortByDesc('updated_at');
                                            @endphp
                                            @foreach($requestedMilestones as $v)
                                                    @php
                                                        $transactions = collect($v->transactions)->sortByDesc('updated_at');
                                                    @endphp
                                                    @foreach($transactions as $v1)
                                                        @if(
                                                        $v1->type != \App\Models\Transaction::TYPE_SERVICE_FEE &&
                                                        (
                                                           $v1->status != \App\Models\Transaction::STATUS_APPROVED_TRANSACTION &&
                                                           $v1->status != \App\Models\Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM &&
                                                           $v1->status != \App\Models\Transaction::STATUS_IN_PROGRESS_MILESTONE
                                                        )
                                                        )
                                                            <tr>
                                                                <td width="30px">
                                                                    @if($v1->status==\App\Models\Transaction::STATUS_REJECTED)
                                                                        <i class="fa fa-circle setTTTTDIFAFA"></i>
                                                                    @elseif($v1->status==\App\Models\Transaction::STATUS_REQUESTED)
                                                                        <i class="fa fa-circle setTTTTDIFAFA"></i>
                                                                    @elseif($v1->status==\App\Models\Transaction::STATUS_APPROVED)
                                                                        <i class="fa fa-circle setTTTTDIFAFA"></i>
                                                                    @else($v1->status==\App\Models\Transaction::STATUS_APPROVED)
                                                                        <i class="fa fa-circle setTTTTDIFAFA"></i>
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    <span class="title">{{trans('common.amount')}}</span><br>
                                                                    <span class="desc">${{$v1->amount}}</span>
                                                                </td>
                                                                <td>
                                                                    <span class="title">{{trans('common.milestone_name')}}</span><br>
                                                                    <span class="desc">{{$v->name}}</span>
                                                                </td>
                                                                <td width="300px">
                                                                    <span class="title">{{trans('common.milestone_description')}}</span><br>
                                                                    <span class="desc">{{$v->description}}</span>
                                                                </td>
                                                                <td>
                                                                    <span class="title">{{trans('common.due_date')}}</span><br>
                                                                    <span class="desc">{{$v->due_date}}</span>
                                                                </td>
                                                                <td>
                                                                    <span class="title">{{trans('common.status')}}</span><br>
                                                                    @if($v1->status==\App\Models\Transaction::STATUS_REJECTED)
                                                                        @php
                                                                            $status = $v1->status;
                                                                        @endphp
                                                                        @if(
                                                                        $status == \App\Models\Transaction::STATUS_DISPUTE_REJECTED ||
                                                                        $status == \App\Models\Transaction::STATUS_DISPUTE_APPROVED ||
                                                                        $status == \App\Models\Transaction::STATUS_DISPUTE_CANCELED_BY_FREELANCER ||
                                                                        $status == \App\Models\Transaction::STATUS_IN_PROGRESS_DISPUTE
                                                                        )
                                                                            @if($v->transaction->filedTransactionDispute->status == \App\Models\ProjectFileDisputes::STATUS_FILE_DISPUTE_IN_PROGRESS)
                                                                                <input type="button" class="btn btn-success btn-green-bg-white-text cancelDispute setBTNBTNJDP" value="{{trans('common.cancel_dispute')}}" data-transactionid="{{$v1->id}}" data-project="{{$project->id}}" data-milestone="{{$v->id}}" style="background: #57b029 !important"><br/>
                                                                            @elseif($v->transaction->filedTransactionDispute->status == \App\Models\ProjectFileDisputes::STATUS_FILE_DISPUTE_IN_REJECTED)
                                                                                <b class="setBOLDCOLOR2" >{{trans('common.rejected')}}</b>
                                                                            @elseif($v->transaction->filedTransactionDispute->status == \App\Models\ProjectFileDisputes::STATUS_FILE_DISPUTE_IN_APPROVED)
                                                                                <b class="setBOLDCOLOR3">{{trans('common.approved')}}</b>
                                                                            @elseif($v->transaction->filedTransactionDispute->status == \App\Models\ProjectFileDisputes::STATUS_FILE_DISPUTE_IN_PROGRESS)
                                                                                <b class="setBOLDCOLOR3">{{trans('common.in_progress')}}</b>
                                                                            @else
                                                                                {{$v->transaction->filedTransactionDispute->status}}
                                                                            @endif
                                                                        @else
                                                                            @if($status == \App\Models\Transaction::STATUS_REJECTED)
                                                                                <span class="desc setSPNN3" >{{trans('common.rejected')}}</span><br>
                                                                                <span style="color: black"><b>Reason:</b> {{$v1->comment}}</span><br/>
                                                                                @if($disputedCount <= 3)
                                                                                <input type="button" class="btn btn-success btn-green-bg-white-text fileDispute setBTNBTNJDP" value="File Dispute" data-transactionid="{{$v1->id}}" data-project="{{$project->id}}" data-milestone="{{$v->id}}" style="background: #57b029 !important"><br/>
                                                                                 @else
                                                                                    <b class="setBOLDCOLOR2" >{{trans('common.dispute_limit')}}</b>
                                                                                @endif
                                                                            @else
                                                                                <b class="setBOLDCOLOR">{{trans('common.filed_dispute')}} <-- {{$status}} > {{$v->id}}</b>
                                                                            @endif
{{--                                                                            <a href="/project/disputeDetail/{{$v->transaction->filedTransactionDispute->id}}">{{trans('common.see_detail')}}</a><br/>--}}
                                                                        @endif


                                                                    @elseif($v1->status==\App\Models\Transaction::STATUS_IN_PROGRESS_DISPUTE)
                                                                        <b class="setBOLDCOLOR">{{trans('common.filed_dispute')}}</b>
                                                                        @php
                                                                            $tid = isset($v->transaction->filedTransactionDispute->id) ? $v->transaction->filedTransactionDispute->id : 0;
                                                                        @endphp
                                                                        <a href="/project/disputeDetail/{{$tid}}">{{trans('common.see_detail')}}</a><br/>
                                                                        <input type="button" class="btn btn-success btn-green-bg-white-text cancelDispute setBTNBTNJDP" value="{{trans('common.cancel_dispute')}}" data-transactionid="{{$v1->id}}" data-project="{{$project->id}}" data-milestone="{{$v->id}}" style="background: #57b029 !important"><br/>

                                                                    @elseif($v1->status==\App\Models\Transaction::STATUS_DISPUTE_APPROVED)
                                                                        <b class="setBOLDCOLOR">{{trans('common.filed_dispute')}}</b>
                                                                        @php
                                                                            $tid = isset($v->transaction->filedTransactionDispute->id) ? $v->transaction->filedTransactionDispute->id : 0;
                                                                        @endphp
                                                                        <a href="/project/disputeDetail/{{$tid}}">{{trans('common.see_detail')}}</a><br/>
                                                                        <b class="setBOLDCOLOR3">{{trans('common.approved')}}</b>
                                                                    @elseif($v1->status==\App\Models\Transaction::STATUS_DISPUTE_REJECTED)
                                                                        <b class="setBOLDCOLOR">{{trans('common.filed_dispute')}}</b>
                                                                        @php
                                                                            $tid = isset($v->transaction->filedTransactionDispute->id) ? $v->transaction->filedTransactionDispute->id : 0;
                                                                        @endphp
                                                                        <a href="/project/disputeDetail/{{$tid}}">{{trans('common.see_detail')}}</a><br/>
                                                                    @elseif($v1->status==\App\Models\Transaction::STATUS_DISPUTE_CANCELED_BY_FREELANCER)
                                                                        <span class="setSPANNN2JDP">{{trans('common.rejected')}}</span>
                                                                    @elseif($v1->status==\App\Models\Transaction::STATUS_REQUESTED)
                                                                        <span class="desc setSPNN4" >{{trans('common.requesting')}} </span>
                                                                    @elseif($v1->status==\App\Models\Transaction::STATUS_APPROVED)
                                                                        <span class="desc setSPNN5" >{{trans('common.approved')}}</span>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                            @endforeach

                                        </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div id="currentmsdiv">
                                        <br/>
                                        <label class="milestone-subpart-title">{{trans('common.refunded_milestones')}}</label>
                                        @include('frontend.fixedPriceEscrowModule.currentMilestones',array(
                                            'milestones' => $data->milestones,
                                            'status' => \App\Models\ProjectMilestones::STATUS_REFUNDED,
                                            'type' => 2
                                        ))
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="dispute">

                                <div class="row client-request">
                                    <div class="col-md-12">
                                        <label class="milestone-subpart-title setclientRequest">{{trans('common.client_request_deposit')}}</label>
                                        <form>
                                        <div class="form-group row">
                                            <div class="col-xs-3 setColXS">
                                                <label for="ex1">{{trans('common.amount')}}</label>
                                                <input class="form-control" id="milestones_amount" type="text" value="" >
                                            </div>
                                            <div class="col-xs-3 setColXS">
                                                <label for="ex2">{{trans('common.milestone_name')}}</label>
                                                <input class="form-control" id="milestones_name" type="text" value="" >
                                            </div>
                                            <div class="col-xs-4 setColXS">
                                                <label for="ex2">{{trans('common.milestone_description')}}</label>
                                                <input class="form-control" id="milestones_name" type="text" value="" >
                                            </div>
                                            <div class="col-xs-2 setColXS">
                                                <label for="ex2">{{trans('common.date')}} & {{trans('common.time')}}</label>
                                                <div class="input-group date form_datetime">
                                                    <input id="milestones_duedate" type="text" class="form-control" readonly="" size="16">
                                                    <span class="input-group-btn">
                                                            <button type="button" class="btn default date-set">
                                                            <i class="fa fa-calendar"></i>
                                                            </button>
                                                            </span>
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <br/>
                                                <button class="btn btn-success btn-green-bg-white-text updateMilestoneDispute setBNBNJDP"  data-id="" data-status="2" >{{trans('common.agree')}}</button>
                                                <button class="btn btn-success btn-white-bg-green-text updateMilestoneDispute setBNBNJDP" data-id="" data-status="3">{{trans('common.reject')}}</button>
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="row client-request">
                                        <div class="col-md-12">
                                            <label class="milestone-subpart-title setmilstonethatAlready">{{trans('common.milestone_that_already_released')}} &nbsp;&nbsp;&nbsp;&nbsp; <span class="setspncolordollar">$1400</span></label>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-xs-1 setColXs1 setColXS">
                                                    <i class="fa fa-circle-o set-fa-circle" aria-hidden="true"></i>
                                                </div>
                                                <div class="col-xs-3 SetCol-md-3 setColXS">
                                                    <label class="milestone-subpart-title">{{trans('common.release_amount')}}</label>
                                                    <br>
                                                    <span class="setspldiv">700$</span>
                                                </div>
                                                <div class="col-xs-3 setColXS">
                                                    <label class="milestone-subpart-title">{{trans('common.milestone_name')}}</label>
                                                    <br>
                                                    <span class="setspldiv">Completed E-Commerce</span>
                                                </div>
                                                <div class="col-xs-3 setColXS">
                                                    <label class="milestone-subpart-title">{{trans('common.date')}}</label>
                                                    <br>
                                                    <span class="setspldiv">23/3/2019</span>
                                                </div>
                                                <div class="col-xs-2 setColXS">
                                                    <span class="dot set-dot-active"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <hr id="setHrWidth">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-xs-1 setColXs1 setColXS">
                                                    <i class="fa fa-circle-o set-fa-circle" aria-hidden="true"></i>
                                                </div>
                                                <div class="col-xs-3 SetCol-md-3 setColXS">
                                                    <label class="milestone-subpart-title">{{trans('common.release_amount')}}</label>
                                                    <br>
                                                    <span class="setspldiv">700$</span>
                                                </div>
                                                <div class="col-xs-3 setColXS">
                                                    <label class="milestone-subpart-title">{{trans('common.milestone_name')}}</label>
                                                    <br>
                                                    <span class="setspldiv">Completed E-Commerce</span>
                                                </div>
                                                <div class="col-xs-3 setColXS">
                                                    <label class="milestone-subpart-title">{{trans('common.date')}}</label>
                                                    <br>
                                                    <span class="setspldiv">23/3/2019</span>
                                                </div>
                                                <div class="col-xs-2 setColXS">
                                                    <span class="dot"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <hr id="setHrWidth">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-4 setColXS">
                                                    <label class="milestone-subpart-title">{{trans('common.total_amount_able_to_refund')}} &nbsp;&nbsp;&nbsp; <span class="setspncolordollar">700 USD</span></label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-md-3 setcolMD3 setColXS">
                                                    <label class="milestone-subpart-title">{{trans('common.amount_you_wish_to_refund')}}</label>
                                                    <input type="text" class="form-control setinputBox001">
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="col-md-3 setcolMD3 setColXS">
                                                    <label class="milestone-subpart-title">{{trans('common.comment')}}</label>
                                                    <textarea class="form-control setTextarea">

                                                    </textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12 setBtndiv setColXS">
                                                <a href="#" class="btn btn-success btn-green-bg-white-text setrefunfbtn" type="submit" data-toggle="modal" data-target="#refundModal" id="">{{trans('common.refund')}}</a>&nbsp;
                                                <button class="btn btn-success btn-white-bg-green-text setBNBNJDP setcancelbuttonbtn" id="" type="button">{{trans('common.Cancel')}}</button>
                                            </div>
                                        </div>
                                </div>

                                <!-- refund - Modal -->
                                <div id="refundModalNew" class="modal fade" role="dialog">
                                    <div class="modal-dialog setModalDailog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header setHeaderModal">
                                                <button type="button" class="close setcloseModal" data-dismiss="modal">&times;
                                                    <i class="fa fa-times setfa-times" aria-hidden="true"></i>
                                                </button>


                                                <h4 class="modal-title setModalTitle">Rejecting the Refund</h4>
                                                <small class="setsmallmodal">Please fill out the info below</small>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-12 setCol12Modal">
                                                       <div class="col-md-6 setCol4MModal pull-left text-left">
                                                           <label>Reason</label>
                                                           <textarea class="form-control settextareamodal">
                                                            </textarea>
                                                           <br>
                                                           <button type="button" class="btn btn-block setbtnmodal">send</button>
                                                       </div>
                                                        <div class="col-md-6 setCol8MModal pull-right">
                                                            <label class="setsmall2modal">Screenshot of the reason to Reject</label>
                                                            <div class="row setModalRowImage">
                                                                <div class="col-md-8 setModalCol8">
                                                                    <div class="scroll-div">
                                                                        <div class="col-md-4 col-xs-4 setModalImagecol4">
                                                                            <img class="setImagesModal" src="{{asset('/images/authentication-logo.png')}}">
                                                                        </div>
                                                                        <div class="col-md-4 col-xs-4 setModalImagecol4">
                                                                            <img class="setImagesModal" src="{{asset('/images/Alibaba-ico02.png')}}">
                                                                        </div>

                                                                        <div class="col-md-4 col-xs-4 setModalImagecol4">
                                                                            <img class="setImagesModal" src="{{asset('/images/authentication-logo.png')}}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="image-upload">
                                                                        <label for="file-input">
                                                                            <img src="{{asset('/images/cross copy 2.png')}}"/>
                                                                        </label>
                                                                        <input id="file-input" type="file"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- end refund Modal -->

                                <div class="row client-request">

                                    @if(!empty($project_disputes))
                                        @foreach($project_disputes as $dispute)
                                            @if($dispute->status == \App\Models\ProjectDispute::STATUS_INPROGRESS)
                                                <div class="col-md-12">
                                                    <label class="milestone-subpart-title">{{trans('common.client_request_deposit')}}</label>
                                                    {{--<form accept="">--}}
                                                    <div class="form-group row">
                                                        <div class="col-xs-3 setColXS">
                                                            <label for="ex1">{{trans('common.amount')}}</label>
                                                            {{--<label>{{ $dispute->type == \App\Models\ProjectDispute::TYPE_MANUAL_DISPUTE ? $dispute->amount : $dispute->milestone->amount }}</label>--}}
                                                            {{--<input class="form-control" id="milestones_amount" type="text" value="{{ $dispute->type == \App\Models\ProjectDispute::TYPE_MANUAL_DISPUTE ? $dispute->amount : $dispute->milestone->amount }}">--}}
                                                        </div>
                                                        <div class="col-xs-3 setColXS">
                                                            <label for="ex2">{{trans('common.milestone_name')}}</label>
                                                            <label for="ex2">{{ $dispute->type == \App\Models\ProjectDispute::TYPE_MILESTONE_DISPUTE ? $dispute->milestone->name : 'N/A' }}</label>
                                                            {{--<input class="form-control" id="milestones_name" type="text" value="{{ $dispute->milestone->name }}">--}}
                                                        </div>
                                                        <div class="col-xs-3 setColXS">
                                                            <label for="ex2">{{trans('common.milestone_description')}}</label>
                                                            <label for="ex2">{{ $dispute->type == \App\Models\ProjectDispute::TYPE_MILESTONE_DISPUTE ? $dispute->milestone->description : 'N/A' }}</label>
                                                            {{--<input class="form-control" id="milestones_name" type="text" value="{{ $dispute->milestone->description }}">--}}
                                                        </div>
                                                        <div class="col-xs-3 setColXS">
                                                            <label for="ex2">{{trans('common.date')}} & {{trans('common.time')}}</label>
                                                            <label for="ex2">{{ $dispute->type == \App\Models\ProjectDispute::TYPE_MILESTONE_DISPUTE ? \Carbon\Carbon::parse($dispute->milestone->due_date)->toDateString() : 'N/A' }}</label>
                                                            {{--<div class="input-group date form_datetime">--}}
                                                            {{--<input id="milestones_duedate" type="text" class="form-control" readonly="" size="16">--}}
                                                            {{--<span class="input-group-btn">--}}
                                                            {{--<button type="button" class="btn default date-set">--}}
                                                            {{--<i class="fa fa-calendar"></i>--}}
                                                            {{--</button>--}}
                                                            {{--</span>--}}
                                                            {{--</div>--}}
                                                            <div class="input-group date form_datetime">
                                                                <input id="milestones_duedate" type="text" class="form-control" readonly="" size="16">
                                                                <span class="input-group-btn">
                                                            <button type="button" class="btn default date-set">
                                                            <i class="fa fa-calendar"></i>
                                                            </button>
                                                            </span>
                                                            </div>

                                                        </div>
                                                        <div class="col-xs-12">
                                                            <br/>
                                                            <button class="btn btn-success btn-green-bg-white-text updateMilestoneDispute setBNBNJDP"  data-id="{{ $dispute->id }}" data-status="2" >{{trans('common.agree')}}</button>
                                                            <button class="btn btn-success btn-white-bg-green-text updateMilestoneDispute setBNBNJDP" data-id="{{ $dispute->id }}" data-status="3">{{trans('common.reject')}}</button>
                                                        </div>
                                                    </div>
                                                    {{--</form>--}}
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif

`
                                </div>

                                <div class="row client-request">
                                    @if(!empty($project_disputes))
                                        @foreach($project_disputes as $dispute)
                                            @if($dispute->status == \App\Models\ProjectDispute::STATUS_CONFIRM)
                                                <div class="col-md-12">
                                                    <label class="milestone-subpart-title">{{trans('common.request_deposited')}}</label>
                                                    {{--<form accept="">--}}
                                                    <div class="form-group row">
                                                        <div class="col-xs-3">
                                                            <label for="ex1">{{trans('common.amount')}}</label>
                                                            {{--<label>{{ $dispute->type == \App\Models\ProjectDispute::TYPE_MANUAL_DISPUTE ? $dispute->amount : $dispute->milestone->amount }}</label>--}}
                                                            {{--<input class="form-control" id="milestones_amount" type="text" value="{{ $dispute->type == \App\Models\ProjectDispute::TYPE_MANUAL_DISPUTE ? $dispute->amount : $dispute->milestone->amount }}">--}}
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <label for="ex2">{{trans('common.milestone_name')}}</label>
                                                            <label for="ex2">{{ $dispute->type == \App\Models\ProjectDispute::TYPE_MILESTONE_DISPUTE ? $dispute->milestone->name : 'N/A' }}</label>
                                                            {{--<input class="form-control" id="milestones_name" type="text" value="{{ $dispute->milestone->name }}">--}}
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <label for="ex2">{{trans('common.milestone_description')}}</label>
                                                                <label for="ex2">{{ $dispute->type == \App\Models\ProjectDispute::TYPE_MILESTONE_DISPUTE ? $dispute->milestone->description : 'N/A' }}</label>
                                                            {{--<input class="form-control" id="milestones_name" type="text" value="{{ $dispute->milestone->description }}">--}}
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <label for="ex2">{{trans('common.date')}} & {{trans('common.time')}}</label>
                                                            <label for="ex2">{{ $dispute->type == \App\Models\ProjectDispute::TYPE_MILESTONE_DISPUTE ? \Carbon\Carbon::parse($dispute->milestone->due_date)->toDateString() : 'N/A' }}</label>
                                                            {{--<div class="input-group date form_datetime">--}}
                                                            {{--<input id="milestones_duedate" type="text" class="form-control" readonly="" size="16">--}}
                                                            {{--<span class="input-group-btn">--}}
                                                            {{--<button type="button" class="btn default date-set">--}}
                                                            {{--<i class="fa fa-calendar"></i>--}}
                                                            {{--</button>--}}
                                                            {{--</span>--}}
                                                            {{--</div>--}}
                                                        </div>
                                                    </div>
                                                    {{--</form>--}}
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>

                            </div>
                            <div role="tabpanel" class="tab-pane" id="contract-control-panel">
                                <br/>
                                <div class="col-md-12">
                                    <label class="milestone-subpart-title setcontractPanel">{{trans('common.contract_control_panel')}}</label>
                                </div>
                                <div class="col-md-12 form-group">
                                    <div class="SETContainer">
                                        <div class="toggle-btn" id="daily_update" onclick="this.classList.toggle('activee')">
                                            <div class="inner-circle"></div>
                                        </div>
                                    </div>
                                    <label class="setdaily_updates">
                                        {{--<input type="checkbox" class="daily_update" id="daily_update">--}}
                                        {{trans('common.daily_update')}}
                                    </label>
                                    <p for="" class="setcolorP">
                                       {{trans('common.employer_require_daily_update')}}
                                    </p>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="end-contract">
                                <div class="row end-contract">
                                    <div class="col-md-6">
                                        <label class="setLable"><strong>{{trans('common.end_contract')}}</strong> </label>
                                        <p class="setpcolor">{{trans('common.after_click_confirm_will_jump_to_leave_feedback')}}. </p>
                                        @if(isset($applicantId) && $applicantId)
                                            <form accept="">
                                                <div class="form-group row">
                                                    <div class="col-xs-12">
                                                        <br>
                                                        <a href="/endContract/freelancer/{{ $applicantId }}" class="btn btn-success btn-green-bg-white-text" type="submit" id="endContract" data-user="{{\Auth::user()->id}}">{{trans('common.confirm')}}</a>
                                                        <input type="hidden" value="freelancer" id="freelancer" />
                                                        <button class="btn btn-success btn-white-bg-green-text setBNBNJDP" id="cancelMilestone" type="button">{{trans('common.Cancel')}}</button>
                                                    </div>
                                                </div>
                                            </form>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{asset('custom/js/frontend/sendMessages.js')}}"></script>
    <script src="/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
    <script src="{{asset('js/global.js')}}" type="text/javascript"></script>
    <script>
        window.total = '{{isset($total) ? $total : 0}}'
        window.applicantId = '{{$applicantId}}';
    </script>
    <script src="{{asset('custom/js/frontend/orderDetail.js')}}" type="text/javascript"></script>
    <script>
        setTimeout(function(){
            $('#tt').html('${{isset($total) ? $total : 0}}');
            $('#md').html('${{isset($md) ? $md : 0}}');
            $('#msd').html('${{isset($msd) ? $msd : 0}}');
        }, 100);



    </script>
    <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <script>
        $('.scroll-div').slimScroll({});
    </script>
@endsection
