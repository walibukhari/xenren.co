<div class="row new-milestone-add" style="height:500px;">
    <div class="col-md-12" style="color: black">
        <label class="milestone-subpart-title">End Contract</label><br/>

        <div class="row">
            <div class="col-md-8">
                <label class="milestone-subpart-title">After click confirm, will jump to Leave Feedback. Must Complete feedback then able to end contract</label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <label class="milestone-subpart-title">Reason of end contract</label>
                <textarea name="" class="form-control" rows="7"></textarea>
            </div>
        </div>
        <div class="row">
            <div class="form-group">

                <div class="col-xs-6 setColXsEC">
                    <br/>
                    <button class="btn btn-success btn-green-bg-white-text setBNBNJDP" id="confirmMilestoneFreelancer" type="button">Confirm</button>
                    <button class="btn btn-success btn-white-bg-green-text setBNBNJDP" id="cancelMilestone" type="button">Cancel</button>
                </div>

            </div>
        </div>
    </div>
</div>