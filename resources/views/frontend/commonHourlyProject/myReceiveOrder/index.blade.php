@extends('frontend.layouts.default')

@section('title')
    Published Job Milestone
@endsection

@section('description')
    Published Job Milestone
@endsection

@section('author')
    Xenren
@endsection

@section('header')
    <link href="/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/userCenter.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/fixedPriceEscrowModule.css" rel="stylesheet">
    <link href="/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/commonProjects.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('calender/public/styles/css/bundle.min.css')}}">
    <style>
        .desc {
            color: #333;
        }
    </style>
@endsection
@section('modal')
    ])
    @include('frontend.modals.disputeModal',[
        'message' => 'Tell us what\'s happening',
    ])

    @include('frontend.modals.modal',[
        'message' => 'Are you sure you want to refund?',
        'id' => 'refundModal',
        'successClass' => 'confirmRefundMilestoneBtn'
    ])

    @include('frontend.modals.modal',[
        'message' => 'Are you sure you want to refund?',
        'id' => 'refundRequestFromEmployerModal',
        'successClass' => 'confirmRequestFromEmployerMilestoneBtn',
        'reason' => true
    ])

    @include('frontend.modals.modal',[
        'message' => 'Are you sure you want to refund?',
        'id' => 'rejectRefundFreelancer',
        'successClass' => 'confirmRejectRefundFreelancer',
        'commentId' => 'rejectRefundFreelancerComment'
    ])

@endsection
@section('content')

    <div class="page-content-wrapper setPageContentWrapperMROPAGE">
        <div class="col-md-12 usercenter link-div">
            <a href="#" class="text-uppercase raleway">
	        <span class="fa fa-angle-left">
	        </span>
                {{trans('common.back_to_my_orders')}}
            </a>
        </div>
        <div class="clearfix"></div>
        <br>
        <?php
        $data = isset($project) ?$project : false;
        $projectId = isset($data->id) ?$data->id : 0;
        $applicantId = isset($data->awardedApplicant->id) ? $data->awardedApplicant->id : 0;
        $languages = isset($data->awardedApplicant->user->languages) ? $data->awardedApplicant->user->languages : false;
        ?>
        <script>
            window.ProjectId = '{{$projectId}}';
            window.token = '{{csrf_token()}}';
        </script>

        <div class="col-md-12">
            <div class="row main-div">
                <div class="col-md-12 personal-profile setPPJDPAGEMRO">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row user-info">
                                <div class="col-md-3 text-center">
                                    <div class="profile-userpic setProfileUserPICJDP">
                                        <img alt="" class="img-responsive" src={{asset($data->awardedApplicant->user->img_avatar)}}>
                                    </div>
                                </div>
                                <div class="col-md-9 basic-info">
                                    <div class="row">
                                        <div class="col-md-12 user-full-name">
                                            <span class="full-name">{{$data->awardedApplicant->user->name}}</span>
                                            <img src="/images/icons/check.png">
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <small class="setSmall">{{trans('common.status')}}:</small>
                                        </div>
                                        <div class="col-md-9">
                                            <span class="status setStats">{{trans('common.on_your_project')}}</span>
                                            <button class="btn btn-success btn-green-bg-white-text setBTNJDPAGEMROP" type="button" onClick="window.location='/inbox'">
                                                {{trans('common.chat')}}
                                            </button>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <small class="setSmall" >{{trans('common.rate')}}:</small>
                                        </div>
                                        <div class="col-md-9">
                                            <span class="details">${{$data->awardedApplicant->user->hourly_pay}} /{{trans('common.hr')}}</span>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <small class="setSmall">{{trans('common.age')}}:</small>
                                        </div>
                                        <div class="col-md-9">
                                            <?php
                                            $date = \Carbon\Carbon::parse($data->awardedApplicant->user->date_of_birth);
                                            $age = $date->diff(\Carbon\Carbon::now())->format('%y');
                                            ?>
                                            <span class="details">{{$age}}</span>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <small class="setSmall">{{trans('common.experience')}}:</small>
                                        </div>
                                        <div class="col-md-9">
                                            <span class="details">{{$data->awardedApplicant->user->experience}} {{trans('common.years')}}</span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 contacts-info setContactsINFO">
                                    <div class="row">

                                        <div class="col-md-3 col-md-3 contacts-info-sub">
                                            <img src="/images/skype-icon-5.png"> <span class="setSP0">{{trans('common.skype')}}</span>
                                            <p class="setMRECEOPJOBDAP">{{$data->awardedApplicant->user->skype_id}}</p>
                                        </div>

                                        <div class="col-md-3 col-md-3 contacts-info-sub">
                                            <img src="/images/wechat-logo.png"> <span class="setSP0">{{trans('common.wechat')}}</span>
                                            <p class="setMRECEOPJOBDAP">{{$data->awardedApplicant->user->wechat_id}}</p>
                                        </div>


                                        <div class="col-md-3 col-md-3 contacts-info-sub">
                                            <img src="/images/line.png"> <span class="setSP0">Line</span>
                                            <p class="setMRECEOPJOBDAP">{{$data->awardedApplicant->user->line_id}}</p>
                                        </div>



                                        <div class="col-md-3 col-md-3 contacts-info-sub">
                                            <img src="/images/MetroUI_Phone.png"> <span class="setSP0">{{trans('common.phone')}}</span>
                                            <p class="setMRECEOPJOBDAP">{{$data->awardedApplicant->user->phone_no or 'N/A'}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row basic-info setROWBINFO">
                                <div class="col-md-12 basic-info-title text-left setSKILLMRECEVIXOPAGE">
                                    <img alt="" src="/images/hands.png"> &nbsp;<span class="subtitle">{{trans('common.skill')}}</span>
                                </div>


                                <div class="col-md-12 scroll-div setCol12MRECEVIEORDEPAAGE">
                                    @foreach($data->awardedApplicant->user->skills as $v)
                                        <div class="skill-div">
	                                <span class="item-skill unverified">
	                    				<span>{{$v->skill->name_cn}}</span>
	                				</span>
                                        </div>
                                    @endforeach


                                    <div class="clearfix"></div>
                                </div>

                                <div class="col-md-12 basic-info-title text-left setLANGUAGEMORPAEG">
                                    <br><img alt="" src="/images/earth.png"> &nbsp;<span class="subtitle">{{trans('common.language')}}</span>
                                </div>

                                <div class="col-md-12 media similar-profiles text-left setLANGCol12">
                                    @foreach($languages as $v)
                                        <div class="media-left media-middle item-languages">
                                            <img class="pull-left media-object" src="{{isset($v->languageDetail->alpha2) ? url('/images/Flags-Icon-Set/24x24/'.strtoupper($v->languageDetail->alpha2).'.png') : url('/images/Flags-Icon-Set/24x24/HK.png')}}" alt="" onerror="this.src='{{url('/images/Flags-Icon-Set/24x24/HK.png')}}'" >
                                            <span class="pull-left">{{isset($v->languageDetail->name) ? $v->languageDetail->name : 'N/A'}}</span>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 prising-section text-center">
                    <div class="row">
                        <div class="col-md-4 prising-section-sub">
                            <img src="/images/icons/doller-icon.png"><br>
                            <p class="setSP0">{{trans('common.budget_for_the_project')}}</p>
                            <span class="setSP">${{$data->reference_price}} USD</span>
                        </div>
                        <div class="setB2"></div>
                        <div class="col-md-4 prising-section-sub">
                            <img src="/images/icons/time-icon.png"><br>
                            <p class="setSP0">{{trans('common.current_milestone')}}</p>
                            <span class="setSP">$700USD</span>
                        </div>
                        <div class="setB3"></div>
                        @if($data->status != \App\Models\Project::STATUS_COMPLETED)
                            <div class="col-md-4 prising-section-sub">
                                <img src="/images/icons/doller-icon.png"><br>
                                <p class="setSP0">{{$data->pay_type != 1 ? trans('common.fixed_price') : trans('common.hourly_price') }}</p>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="col-md-12 description">
                    <h4>{{$data->name}}</h4>
                </div>
                <div class="col-md-12 setDESC">
                <!-- <h5>{!!$data->description!!}</h5> -->
                    <div class="scroll-div">
                        <p>
                            {!!$data->description!!}
                        </p>
                    </div>
                </div>
                @if($data->status != \App\Models\Project::STATUS_COMPLETED)
                    <div class="col-md-12 milestone-desc">
                        <div  class="setULNAVTBS">
                            <ul class="nav nav-tabs" role="tablist">

                                {{--<li role="presentation" class="active text-center">--}}
                                    {{--<a href="#logWorkingHour" class="setLI" aria-controls="milestone" role="tab" data-toggle="tab">Time log</a>--}}
                                {{--</li>--}}

                                <li role="presentation" class="active text-center">
                                    <a href="#PauseContract" class="setLI" aria-controls="milestone" role="tab" data-toggle="tab">{{trans('common.pause_contract')}}</a>
                                </li>

                                {{--<li role="presentation" class="text-center">--}}
                                {{--<div class="vr">&nbsp;</div>--}}
                                {{--<a href="#disputeAndRefund" class="setLI" aria-controls="dispute" role="tab" data-toggle="tab">Dispute & Refund</a>--}}
                                {{--</li>--}}

                                <li role="presentation" class="text-center">
                                    <div class="vr">&nbsp;</div>
                                    <a href="#TopUpSalary" class="setLI" aria-controls="dispute" role="tab" data-toggle="tab">{{trans('common.top_up_salary')}}</a>
                                </li>

                                <li role="presentation" class="text-center">
                                    <div class="vr">&nbsp;</div>
                                    <a href="#increaseHourlyRate" class="setLI" aria-controls="end-contract" role="tab" data-toggle="tab">{{trans('common.increase_hourly_rate')}}</a>
                                </li>


                                <li role="presentation" class="text-center">
                                    <div class="vr">&nbsp;</div>
                                    <a href="#adjustWorkingHour" class="setLI" aria-controls="end-contract" role="tab" data-toggle="tab">{{trans('common.adjust_working_hour')}}</a>
                                </li>

                                <li role="presentation" class="text-center">
                                    <a href="#dispute" class="setLI" aria-controls="contract-control-panel" role="tab" data-toggle="tab">{{trans('common.dispute')}}</a>
                                </li>

                                {{--<li role="presentation" class="text-center">--}}
                                    {{--<div class="vr">&nbsp;</div>--}}
                                    {{--<a href="#timeSheet" class="setLI" aria-controls="contract-control-panel" role="tab" data-toggle="tab">Time Sheet</a>--}}
                                {{--</li>--}}

                                <li role="presentation" class="text-center">
                                    <div class="vr">&nbsp;</div>
                                    <a href="#timeSheet" class="setLI" aria-controls="contract-control-panel" role="tab" data-toggle="tab">{{trans('common.view_time_sheet')}}</a>
                                </li>

                                {{--<li role="presentation" class="text-center">--}}
                                {{--<div class="vr">&nbsp;</div>--}}
                                {{--<a href="#endContractTab" class="setLI" aria-controls="end-contract" role="tab" data-toggle="tab">End Contract</a>--}}
                                {{--<div class="setvr">&nbsp;</div>--}}
                                {{--</li>--}}

                                <li role="presentation" class="text-center">
                                    <div class="vr">&nbsp;</div>
                                    <a href="#EndContract" class="setLI" aria-controls="end-contract" role="tab" data-toggle="tab">{{trans('common.end_contract')}}</a>
                                    {{--<div class="setvr">&nbsp;</div>--}}
                                </li>

                                <li role="presentation" class="text-center">
                                    <div class="vr">&nbsp;</div>
                                        <a href="#contractControlPanel" class="setLI" aria-controls="end-contract" role="tab" data-toggle="tab">{{trans('common.contract_control_panel')}}</a>
                                </li>

                                {{--<li role="presentation" class="text-center">--}}
                                    {{--<div class="vr">&nbsp;</div>--}}
                                    {{--<a href="#hourlyRate" class="setLI" aria-controls="end-contract" role="tab" data-toggle="tab">Hourly Rate</a>--}}
                                {{--</li>--}}

                                {{--<li role="presentation" class="text-center">--}}
                                    {{--<div class="vr">&nbsp;</div>--}}
                                    {{--<a href="#workingLimit" class="setLI" aria-controls="end-contract" role="tab" data-toggle="tab">Working Hour</a>--}}
                                {{--</li>--}}

                                {{--<li role="presentation" class="text-center">--}}
                                    {{--<div class="vr">&nbsp;</div>--}}
                                    {{--<a href="#contractControlPanel" class="setLI" aria-controls="end-contract" role="tab" data-toggle="tab">Contract Control Panel</a>--}}
                                {{--</li>--}}

                            </ul>
                        </div>
                        <div class="tab-content tabpanel-custom setROWNEWMILSTONE">
                            <div role="tabpanel" class="tab-pane active" id="PauseContract">
                                {{--@include('frontend.commonHourlyProject.myReceiveOrder.partials.logWorkingHours')--}}
                                @include('frontend.commonHourlyProject.myReceiveOrder.partial.pauseContract')
                            </div>
                            <div role="tabpanel" class="tab-pane" id="TopUpSalary">
                                {{--@include('frontend.commonHourlyProject.myReceiveOrder.partials.disputeAndRefund')--}}
                                @include('frontend.commonHourlyProject.myReceiveOrder.partial.topUpSalary')
                            </div>
                            <div role="tabpanel" class="tab-pane" id="timeSheet">
                                {{--@include('frontend.commonHourlyProject.myReceiveOrder.partials.timeSheet')--}}
                                @include('frontend.commonHourlyProject.myReceiveOrder.partial.timeSheet')
                            </div>
                            <div role="tabpanel" class="tab-pane" id="increaseHourlyRate">
                                {{--@include('frontend.commonHourlyProject.myReceiveOrder.partials.hourlyRate')--}}
                                @include('frontend.commonHourlyProject.myReceiveOrder.partial.increseHourlyRate')
                            </div>
                            <div role="tabpanel" class="tab-pane" id="adjustWorkingHour">
                                {{--@include('frontend.commonHourlyProject.myReceiveOrder.partials.hourlyRate')--}}
                                @include('frontend.commonHourlyProject.myReceiveOrder.partial.adjustWorkingHour')
                            </div>
                            <div role="tabpanel" class="tab-pane" id="dispute">
                                {{--@include('frontend.commonHourlyProject.myReceiveOrder.partials.workingLimit')--}}
                                @include('frontend.commonHourlyProject.myReceiveOrder.partial.dispute')
                            </div>
                            <div role="tabpanel" class="tab-pane" id="EndContract">
                                {{--@include('frontend.commonHourlyProject.myReceiveOrder.partials.endContract')--}}
                                @include('frontend.commonHourlyProject.myReceiveOrder.partial.endContract')
                            </div>
                            <div role="tabpanel" class="tab-pane" id="contractControlPanel">
                                {{--@include('frontend.commonHourlyProject.myReceiveOrder.partials.contractControlPanel')--}}
                                @include('frontend.commonHourlyProject.myReceiveOrder.partial.contractControlPanel')
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection

@section('footer')
    {{--<script src="/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>--}}
    {{--<script src="/js/moment.min.js"></script>--}}
    {{--<script src="/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>--}}
    {{--<script src="/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>--}}
    {{--<link rel="stylesheet" href="https://uxsolutions.github.io/bootstrap-datepicker/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">--}}
    {{--<script src="{{asset('js/global.js')}}" type="text/javascript"></script>--}}

    <script src="/js/moment.min.js"></script>
    <script src="/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://uxsolutions.github.io/bootstrap-datepicker/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">
    <script src="{{asset('js/global.js')}}" type="text/javascript"></script>
    <script>
        window.total = '{{isset($total) ? $total : 0}}'
        window.applicantId = '{{$applicantId}}';
    </script>
    <script src="{{asset('custom/js/frontend/orderDetail.js')}}" type="text/javascript"></script>
    <script>
        setTimeout(function(){
            $('#tt').html('${{isset($total) ? $total : 0}}');
            $('#md').html('${{isset($md) ? $md : 0}}');
            $('#msd').html('${{isset($msd) ? $msd : 0}}');
        }, 100);



    </script>
    <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <script>
        $('.scroll-div').slimScroll({});
    </script>


    <script type="text/javascript">
        $(function () {
            window.selectedWeek = [];
            window.isChange = true;
            var weekpicker, start_date, end_date;
            weekpicker = $('#datetimepicker12').datepicker();
            weekpicker.on('changeDate', function (e) {
                if(window.isChange) {
                    window.isChange =false;
                    set_week_picker(e.date);
                }
            });
            weekpicker.on('changeMonth', function (e) {
                $('.headerCalMonth').html(moment(e.date, 'M').format('MMMM Y'))
            });
            function set_week_picker(date) {
                var datas = [
                    new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 7),
                    new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6),
                    new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 5),
                    new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 4),
                    new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 3),
                    new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 2),
                    new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 1),
                ];
                var datas1 = `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate() - date.getDay() + 1}`;
                window.selectedWeek = datas1;
                weekpicker.datepicker('setDates', datas)
                window.isChange = true;
            }
            $('#applyFilter').on('click', function(){
                window.location = `?date=${window.selectedWeek}#timeSheet`;
            });
            {{--var filter = '{!! $filters !!}'--}}
            {{--if(filter) {--}}
                {{--console.log('{!! $date1 !!}');--}}
                {{--set_week_picker(new Date('{!! $date1 !!}'))--}}
            {{--}--}}

            $('.incrementMonth').on('click', function () {
                var d = $('#datetimepicker12').data('datepicker').getFormattedDate('yyyy-mm-dd');
                if(d) {
                    var date = d.split(',')[0]
                    var sdate = date.split('-');
                    var year = sdate[0];
                    var month = sdate[1];
                    window.isChange = false;
                    weekpicker.datepicker('setDates', new Date(year,month,'01'))
                } else {
                    var month = moment().format('M');
                    var year = moment().format('YYYY');
                    window.isChange = false;
                    weekpicker.datepicker('setDates', new Date(year,month,'01'))
                }
                var d = $('#datetimepicker12').data('datepicker').getFormattedDate('yyyy-mm-dd');
                $('.headerCalMonth').html(moment(d).format('MMMM Y'))
                // weekpicker.val("").datepicker("update");
                setTimeout(function () {
                    window.isChange = true;
                },100);
            });

            $('.decrementMonth').on('click', function () {
                var d = $('#datetimepicker12').data('datepicker').getFormattedDate('yyyy-mm-dd');
                if(d) {
                    var date = d.split(',')[0]
                    var sdate = date.split('-');
                    var year = sdate[0];
                    var month = sdate[1] - 2;
                    var monthM = sdate[1];
                    window.isChange = false;
                    weekpicker.datepicker('setDates', new Date(year,month,'01'))
                } else {
                    var month = moment().format('M') - 2;
                    var monthM = moment().format('M');
                    var year = moment().format('YYYY');
                    window.isChange = false;
                    weekpicker.datepicker('setDates', new Date(year,monthM,'01'))
                }
                var d = weekpicker.data('datepicker').getFormattedDate('yyyy-mm-dd');
                $('.headerCalMonth').html(moment(d).format('MMMM Y'))
                // weekpicker.val("").datepicker("update");
                setTimeout(function () {
                    window.isChange = true;
                },100);
            })


        });
    </script>



    <script>
        $('.set-btn-toggle').click(function() {
            $(this).find('.set-btn').toggleClass('active');

            if ($(this).find('.btn-primary').size()>0) {
                $(this).find('.set-btn').toggleClass('btn-primary');
            }
            if ($(this).find('.btn-danger').size()>0) {
                $(this).find('.set-btn').toggleClass('btn-danger');
            }
            if ($(this).find('.btn-success').size()>0) {
                $(this).find('.set-btn').toggleClass('btn-success');
            }
            if ($(this).find('.btn-info').size()>0) {
                $(this).find('.set-btn').toggleClass('btn-info');
            }
            $(this).find('.set-btn').toggleClass('btn-default');
        });

        $('form').submit(function(){
            alert($(this["options"]).val());
            return false;
        });

        $('.set-btn-toggle-2').click(function() {
            $(this).find('.set-btn-2').toggleClass('active');

            if ($(this).find('.btn-primary').size()>0) {
                $(this).find('.set-btn-2').toggleClass('btn-primary');
            }
            if ($(this).find('.btn-danger').size()>0) {
                $(this).find('.set-btn-2').toggleClass('btn-danger');
            }
            if ($(this).find('.btn-success').size()>0) {
                $(this).find('.set-btn-2').toggleClass('btn-success');
            }
            if ($(this).find('.btn-info').size()>0) {
                $(this).find('.set-btn-2').toggleClass('btn-info');
            }
            $(this).find('.set-btn-2').toggleClass('btn-default');
        });

        $('form').submit(function(){
            alert($(this["options"]).val());
            return false;
        });

    </script>

    <script>

        var showed_box = 0;
        console.log(showed_box);
        function fNext(){

            var a = showed_box += -100;
            console.log(a);
            if(showed_box < -800)
                var c =showed_box = 5;
                console.log(c);
            document.getElementById('sld').style.transform = "translateY("+showed_box+"px)";
        }

        var showed_boxDown = 0;
        function fPrevious(){

            var b =showed_boxDown += 100;
            console.log(b);
            if(showed_boxDown > 800)
                var z =showed_boxDown = -5;
                console.log(z);
            document.getElementById('sld').style.transform = "translateY("+showed_boxDown+"px)";
        }

    </script>
@endsection
