<div class="row new-milestone-add setRowNewMileS setPauseContractH" xmlns="http://www.w3.org/1999/html">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12 setFullCol12">
                <form class="form">
                    <label class="setPauseContract">{{trans('common.pause_contract')}}</label>
                    <p class="setPforPauseC">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    <button class="btn btn-block setbtnPauseContract">{{trans('common.restart_contract')}}</button>
                </form>
            </div>
        </div>
    </div>
</div>