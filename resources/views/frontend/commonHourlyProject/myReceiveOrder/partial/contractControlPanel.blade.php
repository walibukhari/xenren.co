<div class="row new-milestone-add setRowNewMileS setCCPH" xmlns="http://www.w3.org/1999/html">
    <div class="col-md-12 setFullCol12">
        <div class="row">
            <div class="col-md-12">
                <label class="setlabel1CCP">{{trans('common.contract_control_panel')}}</label>
            </div>
            <br>
            <br>
            <div class="col-md-12">
                <div class="container-btn">
                    <div class="btn-toggle chclick">
                        <div class="set-inner-circle">
                        </div>
                    </div>
                </div>
                <label class="setlabelCHCCP">{{trans('common.commit_hour')}}</label>
                <br>
                <label class="setlabeltextPCCP">{{trans('common.employer_require_pay_extra_if_freelancer')}}.<i>{{trans('common.please_go_to_topup_salary')}}.</i></label>
            </div>
            <br>
            <br>
            <br>
            <br>
            <div class="col-md-12">
                <div class="container-btn2">
                    <div class="btn-toggle2" onclick="this.classList.toggle('btn-active2')">
                        <div class="set-inner-circle2">
                        </div>
                    </div>
                </div>
                <label class="setlabelCHCCP">{{trans('common.daily_update')}}</label>
                <br>
                <label class="setlabeltextPCCP">{{trans('common.employer_require_daily_update')}}.</label>
            </div>
        </div>
    </div>

        <!--Commit Hour Modal 1-->
        <div id="myModalCH" class="modal fade" role="dialog">
            <div class="modal-dialog setModalDialogCCP">
                <!-- Modal content-->
                <div class="modal-content setModalContentCCP1">
                    <div class="modal-header setModalHeaderCCP">
                        <button type="button" class="setCloseModalbtn" data-dismiss="modal">
                            <i class="fa fa-close setCloseFa" aria-hidden="true"></i>
                        </button>
                        <h4 class="modal-title set-modal-titlte-ch">{{trans('common.commit_hour')}}</h4>
                    </div>
                    <div class="modal-body setModalBodyCCP">
                        <p class="setYRP">{{trans('common.you_request_send')}}</p>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <img src="{{asset('images/icons/doller-icon.png')}}">
                                    <p>{{trans('common.basic_hourly_rate')}}</p>
                                    <span class="setspn0CCP">$15 USD</span>
                                </div>
                                <div class="col-md-4">
                                    <img src="{{asset('images/icons/doller-icon.png')}}">
                                    <p>{{trans('common.commit_hourly_bonus')}}</p>
                                    <span class="setspn0CCP">$2 USD</span>
                                </div>
                                <div class="col-md-4">
                                    <img src="{{asset('images/icons/time-icon.png')}}">
                                    <p>{{trans('common.request_work_weekly')}}</p>
                                    <span class="setspn0CCP">40 {{trans('common.hours')}}</span>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <label class="setlabel1CCPModal">{{trans('common.total_amount')}}: <span>650USD</span></label>
                            </div>
                        </div>
                    </div>
                    {{--<div class="modal-footer">--}}
                        {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                    {{--</div>--}}
                </div>

            </div>
        </div>
        <!-- end CH Modal 1-->

        <!--Commit Hour Modal 2-->
        <div id="myModalCH2" class="modal fade" role="dialog">
            <div class="modal-dialog setModalDialog2CCP">
                <!-- Modal content-->
                <div class="modal-content setmodalContentHCCP">
                    <div class="modal-header setModalHeaderCCP">
                        <button type="button" class="setCloseModalbtn2" data-dismiss="modal">
                           &times;
                        </button>
                        <h4 class="modal-title set-modal-titlte-ch">{{trans('common.commit_hour')}}</h4>
                    </div>
                    <div class="modal-body setModalBodyCCP">
                        <p class="setYRP">{{trans('common.send_request')}}</p>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-4 text-center">
                                    <img src="{{asset('images/icons/doller-icon.png')}}">
                                    <p>{{trans('common.basic_hourly_rate')}}</p>
                                    <input type="text" name="" class="form-control setinput3CCPCH1" value="$15 USD">
                                </div>
                                <div class="col-md-4 text-center">
                                    <img src="{{asset('images/icons/doller-icon.png')}}">
                                    <p>{{trans('common.commit_hourly_bonus')}}</p>
                                    <input type="text" name="" class="form-control setinput3CCPCH1" value="$2 USD">
                                </div>
                                <div class="col-md-4 text-center">
                                    <img src="{{asset('images/icons/time-icon.png')}}">
                                    <p>{{trans('common.request_work_weekly')}}</p>
                                    <input type="text" class="form-control setinput3CCPCH1" value="40 Hours">
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row setRowCCPCH2">
                            <div class="col-md-12">
                                <label class="setlabel2CCPModal">{{trans('common.total_amount')}}: <span>650USD</span></label>
                            </div>
                        </div>
                        <br>
                        <div class="row setRow2CCPCH2">
                            <div class="col-md-12">
                                <button class="btn btn-block setbtnbtnMCCPCH2">
                                   {{trans('common.send')}}
                                </button>
                            </div>
                        </div>
                    </div>
                    {{--<div class="modal-footer">--}}
                        {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                    {{--</div>--}}
                </div>

            </div>
        </div>
        <!-- end CH Modal 2-->
</div>