<div class="row new-milestone-add setRowNewMileS set-rowicreasehourlyrate" xmlns="http://www.w3.org/1999/html">
    <div class="col-md-12 setFullCol12">
        <div class="row">
            <div class="col-md-12">
                <label class="setincreasehourlyRatelabel">{{trans('common.increase_hourly_rate')}}</label>
            </div>
            <br>
            <br>
            <div class="row">
                <div class="col-md-9 setihrcol9">
                    <div class="createBoxincreasehourlyRate">
                        <div class="row">
                            <div class="col-md-12">
                                <label class="setincreasehCr">{{trans('common.currunt_rate')}}</label>
                                <hr class="setHrihr">
                            </div>
                            <div class="col-md-12 setcurruntrateihrBox">
                                <div class="col-md-4 setihpsucol4">
                                    <label class="setchr">{{trans('common.currunt_hourly_rate')}}</label>
                                    <br>
                                    <div class="setchrcb1">$4.0/{{trans('common.hours')}}</div>
                                </div>
                                <div class="col-md-8 setincreasehourlyRcol7">
                                    <label class="setcb">{{trans('common.comitted_bonus')}}</label>
                                    <br>
                                    <span class="setPlus">+</span>
                                    <div class="setchrcb2">$4.0/{{trans('common.hours')}} &nbsp;<span class="settotalph">= $8.0/{{trans('common.hours')}}</span></div>
                                </div>
                            </div>
                            <div class="col-md-12">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 set-Col3ihr">
                    <div class="createcommitBoxbonus">
                        <div class="row">
                            <div class="col-md-12">
                                <label class="setcommitBonus">{{trans('common.commit_hour_bonus')}}</label>
                                <div class="SETContainer">
                                    <div class="toggle-btn" onclick="this.classList.toggle('activee')">
                                        <div class="inner-circle"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row seticreashourlyraterow2">
                <div class="col-md-1 set-col1ihr">
                    <span class="setdot"></span>
                </div>
                <div class="col-md-11">
                    <label class="setihrl1">{{trans('common.your')}} 1st {{trans('common.offer')}} <span class="setspnihrspn">10USD/{{trans('common.hours')}} + 2 USD {{trans('common.weekly_bonus')}}</span>{{trans('common.has_been_accepted')}}</label>
                </div>
            </div>
            <div class="row seticreashourlyraterow2">
                <div class="col-md-1 set-col1ihr">
                    <span class="setdotred"></span>
                </div>
                <div class="col-md-11">
                    <label class="setihrl2">{{trans('common.your')}} 1st {{trans('common.offer')}}<span class="setspnihrspn">10USD/{{trans('common.hours')}} + 2 USD {{trans('common.weekly_bonus')}}</span> {{trans('common.has_been_rejected')}}</label>
                </div>
            </div>
            <div class="row seticreashourlyraterow2">
                <div class="col-md-1 set-col1ihr">
                    <span class="setdotyellow"></span>
                </div>
                <div class="col-md-11">
                    <label class="setihrl3">{{trans('common.your')}} 1st {{trans('common.offer')}}<span class="setspnihrspn">11USD/{{trans('common.hours')}} + 2 USD {{trans('common.weekly_bonus')}}</span>{{trans('common.is_an_waiting')}}.</label>
                </div>
            </div>
            <br>
            <div class="row">
                <form class="form">
                    <div class="col-md-3">
                        <label class="setnewhourlyr">{{trans('common.new_hourly_rate')}}</label>
                        <input type="text" name="hourly_rate" id="hourly_rate" class="form-control hourly_rate">
                    </div>
                    <div class="col-md-3">
                        <label class="setcommitHourBonus">{{trans('common.commit_hour_bonus')}}</label>
                        <input type="text" name="commit_hour_bonus" id="commit_hour_bonus" class="form-control commit_hour_bonus">
                    </div>
                    <div class="col-md-2">
                        <div class="setisequalto">=<span class="settotalpihr">$ 8.0/{{trans('common.hours')}}</span></div>
                    </div>
                    <div class="col-md-4">
                    </div>
                </form>
            </div>
            <div class="row setrowlessthen">
                <label class="setlessthenhourlyrate">{{trans('common.less_then_hourly_Rate')}}</label>
                <br>
                <button class="btn btn-block setincreasehourlyratebtn">{{trans('common.confirm')}}</button>
            </div>
        </div>
    </div>
</div>