<div class="row new-milestone-add setRowNewMileS setEndContractH" xmlns="http://www.w3.org/1999/html">
    <div class="col-md-12 setFullCol12">
        <div class="row">
            <div class="col-md-12">
                <label class="setLabel1endC">{{trans('common.end_contract')}}</label>
            </div>
            <div class="col-md-12 setcol12endC">
                <label class="setlabel2endC">{{trans('common.after_click')}}.</label>
            </div>
            <div class="col-md-12">
                <label class="setlabelrofeC">{{trans('common.reasons_of_end_Contract')}}</label>
            </div>
            <form class="form">
                <div class="col-md-12">
                    <textarea class="form-group settextAenDc"></textarea>
                </div>
                <br>
                <br>
                <div class="col-md-12">
                    <button class="btn btn-block setbtnendC">
                        {{trans('common.submit')}}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>