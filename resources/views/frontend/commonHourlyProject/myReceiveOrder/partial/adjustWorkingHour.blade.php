<div class="row new-milestone-add setRowNewMileS setlenghtofawhb" xmlns="http://www.w3.org/1999/html">
    <div class="col-md-12 setFullCol12">
        <div class="row">
            <div class="col-md-12">
                <label class="setlabel1awh">{{trans('common.adjust_working_hour')}}</label>
            </div>
            <div class="col-md-12">
                <label class="setlabeldwna">{{trans('common.descrease_will_not_allowed')}}+0 : 00</label>
            </div>
            <div class="col-md-12 setcol12hwb">
                <div class="col-md-1 setcol1hwb">
                    <span class="setdotawh"></span>
                </div>
                <div class="col-md-11 setcol11hwb">
                    <label class="setlabel3hwb">{{trans('common.hour_with_bouns_rate')}} 4USD + 1USD {{trans('common.per_hour')}} x 30{{trans('common.hours')}} = <span class="setspn0hwb">150USD</span></label>
                </div>
            </div>
            <div class="col-md-12 setcol12hwb">
                <div class="col-md-1 setcol1hwb">
                    <span class="setdotawh1"></span>
                </div>
                <div class="col-md-11 setcol11hwb">
                    <label class="setlabel3hwb">{{trans('common.hour_with_normal_rate')}} 4USD {{trans('common.per_hour')}} x 10{{trans('common.hours')}} = <span class="setspn1hwb">40USD</span></label>
                </div>
            </div>
            <div class="col-md-12 setcol12hwb">
                <div class="col-md-1 setcol1hwb">
                    <span class="setdotawh2"></span>
                </div>
                <div class="col-md-11 setcol11hwb">
                    <label class="setlabel3hwb">{{trans('common.freelance_spent')}} = <span class="setspn2hwb">4:30 (21.5USD)</span></label>
                </div>
            </div>
            <br>
            <!-- progress bar -->
            <div class="col-md-12">
                <div class="card setCard">
                    <div class="container set-cardcontainer">
                        <div class="row setawhrow1">
                            <div class="col-md-2 setawhcol2">
                                <label class="setlabel5pb">4:30 {{trans('common.hours')}}</label>
                                <div class="w3-grey setw3colo1"></div>
                            </div>
                            <div class="col-md-4 setawhcol4">
                                <label class="setlabel5pb1">25:30 {{trans('common.hours')}}</label>
                                <div class="w3-grey setw3colo2"></div>
                            </div>
                            <div class="col-md-3 setawhcol3">
                                <label class="setlabel5pb2">10:00 {{trans('common.hours')}}</label>
                                <div class="w3-grey setw3colo3"></div>
                            </div>
                            <div class="col-md-3 setawh3col">
                                <label class="setlabel5th">{{trans('common.total')}}: 40 {{trans('common.hours')}} <span class="setspnawh">$190</span></label>
                            </div>
                            {{--<div class="w3-border setW3Border">--}}
                                {{--<div class="w3-grey setw3colo1"></div>--}}
                                {{--<div class="w3-grey setw3colo2"></div>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                    <div class="card-footer setfooter">
                        <label class="setlabel0bh">{{trans('common.once_bonus_level')}}</label>
                    </div>
                </div>
            </div>
            <!-- end progress bar -->
        </div>
        <br><br>
        <div class="row">
            <div class="col-md-12 setcolwhb">
                <form class="form">
                    <div class="col-md-6 setcol6whb">
                        <input type="text" class="setinput6wcbh form-control" name="weekly_commit" placeholder="Weekly Commit Bonus Hour Limit">
                    </div>
                    <div class="col-md-6 setcol6whb2">
                        <input type="text" class="setinput7wcbh form-control" name="weekly_commit" placeholder="Weekly Hour Limit">
                    </div>
                </form>
            </div>
        </div>
        <br>
        <div class="row">
            <form class="form">
                <div class="col-md-12 setcolwhb">
                    <div class="col-md-4 setcol4whb">
                        <input type="text" name="_amount" class="form-control setinput1col4whb" placeholder="Amount" id="_amount">
                    </div>
                    <div class="col-md-1 setcolwhb1">
                        <span class="ssetspnPluswhb">+</span>
                    </div>
                    <div class="col-md-4 setcol4whb">
                        <input type="text" name="_amount" class="setinput2col4whb form-control" placeholder="Amount" id="_amount">
                    </div>
                    <div class="col-md-3 setcolmd3whb">
                        <span class="setspnwbh2">= XX {{trans('common.hours')}}</span>
                    </div>
                </div>
                <br>
                <div class="col-md-12 setcolwhb12">
                    <div class="col-md-6 setcolwhb6">
                        <button class="btn btn-block setwhbbtn1">
                           {{trans('common.effect_im')}}
                        </button>
                    </div>
                    <div class="col-md-6 setcolwhb6">
                        <button class="btn btn-block setwhbbtn2">
                            {{trans('common.start_next_week')}}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>