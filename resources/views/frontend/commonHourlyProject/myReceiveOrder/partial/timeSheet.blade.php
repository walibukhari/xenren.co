<p class="setPPpPpP">View Time Sheet</p>
<div class="">
    <div class="row new-milestone-add setDIVCOl12W">
        <div class="col-md-12" style="color: black">
            <div class="row">
                {{--<div id="calendar-wrap" class="setCalenderWrap">--}}
                {{--</div>--}}
                <div id="calendar-wrap" class="setCalenderWrap">
                    <div id="calendar" class="calendar">
                        <header class="calendar__head">
                            <div class="calendar__nav">
                                <div id="calendar-left-btn" class="calendar__btn">
                                    <i class="fa fa-angle-left setFa decrementMonth"></i>
                                </div>
                                <div class="calendar__head-text">
                                    <span id="calendar-month" class="calender-header-text-month headerCalMonth">February 2019</span>
                                </div>
                                <div id="calendar-right-btn" class="calendar__btn">
                                    <i class="fa fa-angle-right setFa incrementMonth"></i>
                                </div>
                            </div>
                        </header>
                        <div class="row">
                            <div class="col-md-4 col-sm-12 col-xs-12 set-main-row-col-4calender">
                                <div style="overflow:hidden;">
                                    <div id="datetimepicker12"></div>
                                    <div class="setbtnApply">
                                        <button type="button" class="btn btn-sm btn-block" id="applyFilter" style="" >{{trans('common.apply')}}</button></div>
                                </div>
                            </div>

                            <div class="vr02">&nbsp;</div>
                            <!--<div class="clear"></div>-->
                            <div class="col-md-4 col-sm-12 col-xs-12 setcolmd4">
                                <span class="setSPNCOLOR">{{trans('common.status')}}</span>
                                <div class="tbleWidth">
                                    <table class="table table-borderless setTBLE">
                                        <thead>
                                        <tr>
                                            <th scope="col" class="setTh-1">{{trans('common.week')}}</th>
                                            <th scope="col" class="setTh-2">{{trans('common.hour_work')}}</th>
                                            <th scope="col" class="setTh-3">{{trans('common.earn')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="setTRLast" >
                                            <td>1st {{trans('common.week')}}</td>
                                            <td> {{trans('common.hours')}}</td>
                                            <td>540 USD</td>
                                        </tr>
                                        <tr class="setTRLast" >
                                            <td>2nd {{trans('common.week')}}</td>
                                            <td> {{trans('common.hours')}}</td>
                                            <td>540 USD</td>
                                        </tr>
                                        <tr class="setTRLast">
                                            <td>3rd {{trans('common.week')}}</td>
                                            <td> {{trans('common.hours')}}</td>
                                            <td>540 USD</td>
                                        </tr>
                                        <tr  class="setTRLast">
                                            <td>4th {{trans('common.week')}}</td>
                                            <td> {{trans('common.hours')}}</td>
                                            <td>540 USD</td>
                                        </tr>
                                        <tr  class="setTRLast">
                                            <td>5th {{trans('common.week')}}</td>
                                            <td> {{trans('common.hours')}}</td>
                                            <td>540 USD</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-12 col-xs-12 set-md-col">
                                <div class="box">
                                    <div class="row">
                                        <span class="setThisMoth">{{trans('common.this_month')}}</span>
                                        <hr class="setHRHRHR">
                                    </div>
                                    <div class="setTotalHours">
                                        <p class="setTotalHOUR">{{trans('common.total_hours')}}</p>
                                        <span>0 {{trans('common.hours')}}</span>
                                    </div>
                                    <div class="totalEarning">
                                        <p class="setTOTALEarning">{{trans('common.total_earn_shares')}}</p>
                                        <span>0</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row setroow" style="margin-bottom: 16px">
                    <div class="col-md-12">
                        <div class="col-md-4 col-sm-12 col-xs-12 setSetRowP1 pull-left text-left">{{trans('common.weekly_screen_shot')}}: &nbsp; <span class="setSPNCOLOR0">125</span></div>
                        <div class="col-md-4 col-sm-12 col-xs-12 setSetRowP2 text-center">Events: &nbsp; <span class="setSPNCOLOR0">3400 Keyboards | </span><span class="setSPNCOLOR0">400 Mouse</span></div>
                        <div class="col-md-4 col-sm-12 col-xs-12 setSetRowP3 pull-right text-right">{{trans('common.weekly_logged')}}: &nbsp; <span class="setSPNCOLOR0">25:30 {{trans('common.hours')}}</span></div>
                    </div>
                </div>

                <div class="row set-row-section">
                    <div class="col-md-4 col-sm-12 col-xs-12 pull-left text-left set-col-md-date">
                        <span>Sun 03-FEB</span> - <span>Sat 09-FEB</span>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 text-center">
                        <div class="btn-group set-btn-toggle">
                            <button class="set-btn btn-lg btn-default">12-{{trans('common.hours')}}</button>
                            <button class="set-btn btn-lg btn-primary active">24-{{trans('common.hours')}}</button>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 pull-right text-right set-row-btn-toggle-2">
                        <div class="btn-group set-btn-toggle-2">
                            <button class="set-btn-2 btn-lg btn-default">{{trans('common.my_local_time')}}</button>
                            <button class="set-btn-2 btn-lg btn-primary active">{{trans('common.client_time')}}</button>
                        </div>
                    </div>
                </div>
                <div class="row set-tb-pills-row">
                    <ul class="pil nav nav-pills set-nav-pills">
                        <li class="active"><a data-toggle="pill" href="#menu1">SUN 01</a></li>
                        <li><a data-toggle="pill" href="#menu3">MON 01</a></li>
                        <li><a data-toggle="pill" href="#menu4">TUE 01</a></li>
                        <li><a data-toggle="pill" href="#menu2">WED 01</a></li>
                        <li><a data-toggle="pill" href="#menu5">THU 01</a></li>
                        <li><a data-toggle="pill" href="#menu6">FRI 01</a></li>
                        <li><a data-toggle="pill" href="#menu7">SAT 01</a></li>
                    </ul>
                </div>

                <div class="tab-content">
                    <div id="menu1" class="tab-pane fade in active">
                        <div class="row" style="padding: 10px">
                            <div class="col-md-12 set-col-md-12-box2 text-center">
                                <h3>SUNDAY 03, FEBRUARY</h3>
                            </div>
                            <div class="col-md-12 set-col-md-12-box3 text-center">
                                <h1>24:25 HOURS</h1>
                            </div>
                            <div class="col-md-12 set-col-md-12-box4 text-center">
                                <div class="row set-para">
                                    <div class="col-md-4 set-row-col-6-fa-circle-1">
                                        <i class="fa fa-circle set-fa-circle1"></i>
                                        <span class="set-span-1">
                                                {{trans('common.track_time')}}:
                                                <span class="set-Hours">&nbsp;10:10 {{trans('common.hours')}}</span>
                                            </span>
                                    </div>

                                    <div class="col-md-3">
                                        <i class="fa fa-circle set-fa-circle2"></i>
                                        <span class="set-span-2">
                                                {{trans('common.manual')}}
                                                <span class="set-Hours">&nbsp;10:10 {{trans('common.hours')}}</span>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row set-col-row-screen-shots text-center">
                            <span class="set-spn-screen-shots">{{trans('common.Screen_shot')}}</span>
                        </div>

                        <div class="row set-row-box-last">
                            <div class="col-md-12 set-col-md-12-box-last">
                                <div class="col-md-12 set-col-md-12-box-last-2">
                                    <div class="col-md-3 col-sm-3 col-xs-3 set-col-md-3">

                                        <div class="row set-col-md-2-box-last" style="margin: 0; margin-bottom: 3px">
                                            <span>Memo-1</span>
                                        </div>
                                        <div class="row set-row-imge-screen-shoots">
                                            <img class="set-Image-screen-shoot" src="{{asset('images/3.jpg')}}" alt="avatar">
                                            <div class="col-md-12 setCol-md-12timeDate">
                                                <div class="col-md-10 col-sm-10 col-xs-10 set-col-timeDate">
                                                                <span class="set-time-screen-shot">
                                                                     14:00 - 22:99
                                                </span>
                                                </div>
                                                <div class="col-md-2 set-delete-icon-row">
                                                    <span class="set-delete-icon"><i class="fa fa-trash" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                            <div class="battery" style="top: 16%">
                                                <div class="battery-charge-cell"></div>
                                                <div class="battery-charge-cell active-battery"></div>
                                                <div class="battery-charge-cell active-battery"></div>
                                                <div class="battery-charge-cell active-battery"></div>
                                                <div class="battery-charge-cell active-battery"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="menu2" class="tab-pane fade">
                        <div class="row" style="padding: 10px">
                            <div class="col-md-12 set-col-md-12-box2 text-center">
                                <h3>SUNDAY 03, FEBRUARY</h3>
                            </div>
                            <div class="col-md-12 set-col-md-12-box3 text-center">
                                <h1>10:10 HOURS</h1>
                            </div>
                            <div class="col-md-12 set-col-md-12-box4 text-center">
                                <div class="row set-para">
                                    <div class="col-md-4 set-row-col-6-fa-circle-1">
                                        <i class="fa fa-circle set-fa-circle1"></i>
                                        <span class="set-span-1">
                                                {{trans('common.track_time')}}:
                                                <span class="set-Hours">&nbsp;10:10 {{trans('common.hours')}}</span>
                                            </span>
                                    </div>

                                    <div class="col-md-3">
                                        <i class="fa fa-circle set-fa-circle2"></i>
                                        <span class="set-span-2">
                                               {{trans('common.manual')}}:
                                                <span class="set-Hours">&nbsp;10:10 {{trans('common.hours')}}</span>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row set-col-row-screen-shots text-center">
                            <span class="set-spn-screen-shots">{{trans('common.Screen_shot')}}</span>
                        </div>

                        <div class="row set-row-box-last">
                            <div class="col-md-12 set-col-md-12-box-last">
                                <div class="col-md-12 set-col-md-12-box-last-2">
                                    <div class="col-md-3 colsm-3 col-xs-3 set-col-md-3">

                                        <div class="row set-col-md-2-box-last" style="margin: 0; margin-bottom: 3px">
                                            <span>Memo-2</span>
                                        </div>
                                        <div class="row set-row-imge-screen-shoots">
                                            <img class="set-Image-screen-shoot" src="{{asset('images/3.jpg')}}" alt="avatar">
                                            <div class="col-md-12 setCol-md-12timeDate">
                                                <div class="col-md-10 col-sm-10 col-xs-10 set-col-timeDate">
                                                                <span class="set-time-screen-shot">
                                                                    43:00 - 52:99
                                                </span>
                                                </div>
                                                <div class="col-md-2 set-delete-icon-row">
                                                    <span class="set-delete-icon"><i class="fa fa-trash" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                            <div class="battery" style="top: 16%">
                                                <div class="battery-charge-cell"></div>
                                                <div class="battery-charge-cell active-battery"></div>
                                                <div class="battery-charge-cell active-battery"></div>
                                                <div class="battery-charge-cell active-battery"></div>
                                                <div class="battery-charge-cell active-battery"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="menu3" class="tab-pane fade">
                        <div class="row" style="padding: 10px">
                            <div class="col-md-12 set-col-md-12-box2 text-center">
                                <h3>SUNDAY 03, FEBRUARY</h3>
                            </div>
                            <div class="col-md-12 set-col-md-12-box3 text-center">
                                <h1>12:21 HOURS</h1>
                            </div>
                            <div class="col-md-12 set-col-md-12-box4 text-center">
                                <div class="row set-para">
                                    <div class="col-md-4 set-row-col-6-fa-circle-1">
                                        <i class="fa fa-circle set-fa-circle1"></i>
                                        <span class="set-span-1">
                                                {{trans('common.track_time')}}:
                                                <span class="set-Hours">&nbsp;10:10 {{trans('common.hours')}}</span>
                                            </span>
                                    </div>

                                    <div class="col-md-3">
                                        <i class="fa fa-circle set-fa-circle2"></i>
                                        <span class="set-span-2">
                                               {{trans('common.manual')}}:
                                                <span class="set-Hours">&nbsp;10:10 {{trans('common.hours')}}</span>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row set-col-row-screen-shots text-center">
                            <span class="set-spn-screen-shots">{{trans('common.Screen_shot')}}</span>
                        </div>

                        <div class="row set-row-box-last">
                            <div class="col-md-12 set-col-md-12-box-last">
                                <div class="col-md-12 set-col-md-12-box-last-2">
                                    <div class="col-md-3 col-sm-3 col-xs-3 set-col-md-3">

                                        <div class="row set-col-md-2-box-last" style="margin: 0; margin-bottom: 3px">
                                            <span>Memo-3</span>
                                        </div>
                                        <div class="row set-row-imge-screen-shoots">
                                            <img class="set-Image-screen-shoot" src="{{asset('images/3.jpg')}}" alt="avatar">
                                            <div class="col-md-12 setCol-md-12timeDate">
                                                <div class="col-md-10 col-sm-10 col-xs-10 set-col-timeDate">
                                                                <span class="set-time-screen-shot">
                                                                     33:03 - 12:99
                                                </span>
                                                </div>
                                                <div class="col-md-2 set-delete-icon-row">
                                                    <span class="set-delete-icon"><i class="fa fa-trash" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                            <div class="battery" style="top: 16%">
                                                <div class="battery-charge-cell"></div>
                                                <div class="battery-charge-cell"></div>
                                                <div class="battery-charge-cell"></div>
                                                <div class="battery-charge-cell active-battery"></div>
                                                <div class="battery-charge-cell active-battery"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="menu4" class="tab-pane fade">

                        <div class="row" style="padding: 10px">
                            <div class="col-md-12 set-col-md-12-box2 text-center">
                                <h3>SUNDAY 03, FEBRUARY</h3>
                            </div>
                            <div class="col-md-12 set-col-md-12-box3 text-center">
                                <h1>23:32 HOURS</h1>
                            </div>
                            <div class="col-md-12 set-col-md-12-box4 text-center">
                                <div class="row set-para">
                                    <div class="col-md-4 set-row-col-6-fa-circle-1">
                                        <i class="fa fa-circle set-fa-circle1"></i>
                                        <span class="set-span-1">
                                                {{trans('common.track_time')}}:
                                                <span class="set-Hours">&nbsp;2:213 {{trans('common.hours')}}</span>
                                            </span>
                                    </div>

                                    <div class="col-md-3">
                                        <i class="fa fa-circle set-fa-circle2"></i>
                                        <span class="set-span-2">
                                               {{trans('common.manual')}}:
                                                <span class="set-Hours">&nbsp;12:21 {{trans('common.hours')}}</span>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row set-col-row-screen-shots text-center">
                            <span class="set-spn-screen-shots">{{trans('common.Screen_shot')}}</span>
                        </div>

                        <div class="row set-row-box-last">
                            <div class="col-md-12 set-col-md-12-box-last">
                                <div class="col-md-12 set-col-md-12-box-last-2">
                                    <div class="col-md-3 col-sm-3 col-xs-3 set-col-md-3">

                                        <div class="row set-col-md-2-box-last" style="margin: 0; margin-bottom: 3px">
                                            <span>Memo-4</span>
                                        </div>
                                        <div class="row set-row-imge-screen-shoots">
                                            <img class="set-Image-screen-shoot" src="{{asset('images/3.jpg')}}" alt="avatar">
                                            <div class="col-md-12 setCol-md-12timeDate">
                                                <div class="col-md-10 col-sm-10 col-xs-10 set-col-timeDate">
                                                                <span class="set-time-screen-shot">
                                                                     30:00 - 42:99
                                                </span>
                                                </div>
                                                <div class="col-md-2 set-delete-icon-row">
                                                    <span class="set-delete-icon"><i class="fa fa-trash" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                            <div class="battery">
                                                <div class="battery-charge-cell active-battery"></div>
                                                <div class="battery-charge-cell active-battery"></div>
                                                <div class="battery-charge-cell active-battery"></div>
                                                <div class="battery-charge-cell active-battery"></div>
                                                <div class="battery-charge-cell active-battery"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="menu5" class="tab-pane fade">

                        <div class="row" style="padding: 10px">
                            <div class="col-md-12 set-col-md-12-box2 text-center">
                                <h3>SUNDAY 03, FEBRUARY</h3>
                            </div>
                            <div class="col-md-12 set-col-md-12-box3 text-center">
                                <h1>21:123 HOURS</h1>
                            </div>
                            <div class="col-md-12 set-col-md-12-box4 text-center">
                                <div class="row set-para">
                                    <div class="col-md-4 set-row-col-6-fa-circle-1">
                                        <i class="fa fa-circle set-fa-circle1"></i>
                                        <span class="set-span-1">
                                                {{trans('common.track_time')}}:
                                                <span class="set-Hours">&nbsp;21:123 {{trans('common.hours')}}</span>
                                            </span>
                                    </div>

                                    <div class="col-md-3">
                                        <i class="fa fa-circle set-fa-circle2"></i>
                                        <span class="set-span-2">
                                                {{trans('common.manual')}}:
                                                <span class="set-Hours">&nbsp;21:123 {{trans('common.hours')}}</span>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row set-col-row-screen-shots text-center">
                            <span class="set-spn-screen-shots">{{trans('common.Screen_shot')}}</span>
                        </div>

                        <div class="row set-row-box-last">
                            <div class="col-md-12 set-col-md-12-box-last">
                                <div class="col-md-12 set-col-md-12-box-last-2">
                                    <div class="col-md-3 col-sm-3 col-xs-3 set-col-md-3">

                                        <div class="row set-col-md-2-box-last" style="margin: 0; margin-bottom: 3px">
                                            <span>Memo</span>
                                        </div>
                                        <div class="row set-row-imge-screen-shoots">
                                            <img class="set-Image-screen-shoot" src="{{asset('images/3.jpg')}}" alt="avatar">
                                            <div class="col-md-12 setCol-md-12timeDate">
                                                <div class="col-md-10 col-sm-10 col-xs-10 set-col-timeDate">
                                                            <span class="set-time-screen-shot">
                                                                 13:00 - 12:99
                                        </span>
                                                </div>
                                                <div class="col-md-2 set-delete-icon-row">
                                                    <span class="set-delete-icon"><i class="fa fa-trash" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                            <div class="battery">
                                                <div class="battery-charge-cell"></div>
                                                <div class="battery-charge-cell active-battery"></div>
                                                <div class="battery-charge-cell active-battery"></div>
                                                <div class="battery-charge-cell active-battery"></div>
                                                <div class="battery-charge-cell active-battery"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="menu6" class="tab-pane fade">
                        <div class="row" style="padding: 10px">
                            <div class="col-md-12 set-col-md-12-box2 text-center">
                                <h3>SUNDAY 03, FEBRUARY</h3>
                            </div>
                            <div class="col-md-12 set-col-md-12-box3 text-center">
                                <h1>02:10 HOURS</h1>
                            </div>
                            <div class="col-md-12 set-col-md-12-box4 text-center">
                                <div class="row set-para">
                                    <div class="col-md-4 set-row-col-6-fa-circle-1">
                                        <i class="fa fa-circle set-fa-circle1"></i>
                                        <span class="set-span-1">
                                                 {{trans('common.track_time')}}:
                                                <span class="set-Hours">&nbsp;01:01 {{trans('common.hours')}}</span>
                                            </span>
                                    </div>

                                    <div class="col-md-3">
                                        <i class="fa fa-circle set-fa-circle2"></i>
                                        <span class="set-span-2">
                                                 {{trans('common.manual')}}:
                                                <span class="set-Hours">&nbsp;100:001 {{trans('common.hours')}}</span>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row set-col-row-screen-shots text-center">
                            <span class="set-spn-screen-shots">{{trans('common.Screen_shot')}}</span>
                        </div>

                        <div class="row set-row-box-last">
                            <div class="col-md-12 set-col-md-12-box-last">
                                <div class="col-md-12 set-col-md-12-box-last-2">
                                    <div class="col-md-3 col-sm-3 col-xs-3 set-col-md-3">

                                        <div class="row set-col-md-2-box-last" style="margin: 0; margin-bottom: 3px">
                                            <span>Memo-6</span>
                                        </div>
                                        <div class="row set-row-imge-screen-shoots">
                                            <img class="set-Image-screen-shoot" src="{{asset('images/3.jpg')}}" alt="avatar">
                                            <div class="col-md-12 setCol-md-12timeDate">
                                                <div class="col-md-10 col-sm-10 col-xs-10 set-col-timeDate">
                                                                <span class="set-time-screen-shot">
                                                                     13:20 - 12:99
                                                </span>
                                                </div>
                                                <div class="col-md-2 set-delete-icon-row">
                                                    <span class="set-delete-icon"><i class="fa fa-trash" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                            <div class="battery">
                                                <div class="battery-charge-cell"></div>
                                                <div class="battery-charge-cell"></div>
                                                <div class="battery-charge-cell active-battery"></div>
                                                <div class="battery-charge-cell active-battery"></div>
                                                <div class="battery-charge-cell active-battery"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="menu7" class="tab-pane fade">
                        <div class="row" style="padding: 10px">
                            <div class="col-md-12 set-col-md-12-box2 text-center">
                                <h3>SUNDAY 03, FEBRUARY</h3>
                            </div>
                            <div class="col-md-12 set-col-md-12-box3 text-center">
                                <h1>12:21 HOURS</h1>
                            </div>
                            <div class="col-md-12 set-col-md-12-box4 text-center">
                                <div class="row set-para">
                                    <div class="col-md-4 set-row-col-6-fa-circle-1">
                                        <i class="fa fa-circle set-fa-circle1"></i>
                                        <span class="set-span-1">
                                               {{trans('common.track_time')}}:
                                                <span class="set-Hours">&nbsp;03:30 {{trans('common.hours')}}</span>
                                            </span>
                                    </div>

                                    <div class="col-md-3">
                                        <i class="fa fa-circle set-fa-circle2"></i>
                                        <span class="set-span-2">
                                                {{trans('common.manual')}}:
                                                <span class="set-Hours">&nbsp;23:30 {{trans('common.hours')}}</span>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row set-col-row-screen-shots text-center">
                            <span class="set-spn-screen-shots">{{trans('common.Screen_shot')}}</span>
                        </div>

                        <div class="row set-row-box-last">
                            <div class="col-md-12 set-col-md-12-box-last">
                                <div class="col-md-12 set-col-md-12-box-last-2">
                                    <div class="col-md-3 col-sm-3 col-xs-3 set-col-md-3">

                                        <div class="row set-col-md-2-box-last" style="margin: 0; margin-bottom: 3px">
                                            <span>Memo-7</span>
                                        </div>
                                        <div class="row set-row-imge-screen-shoots">
                                            <img class="set-Image-screen-shoot" src="{{asset('images/3.jpg')}}" alt="avatar">
                                            <div class="col-md-12 setCol-md-12timeDate">
                                                <div class="col-md-10 col-sm-10 col-xs-10 set-col-timeDate">
                                                                <span class="set-time-screen-shot">
                                                                    13:00 - 12:99
                                                </span>
                                                </div>
                                                <div class="col-md-2 set-delete-icon-row">
                                                    <span class="set-delete-icon"><i class="fa fa-trash" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                            <div class="battery">
                                                <div class="battery-charge-cell"></div>
                                                <div class="battery-charge-cell"></div>
                                                <div class="battery-charge-cell"></div>
                                                <div class="battery-charge-cell active-battery"></div>
                                                <div class="battery-charge-cell active-battery"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>