<div class="row new-milestone-add setRowNewMileS setTopUpSalaryH" xmlns="http://www.w3.org/1999/html">
    <div class="col-md-12 setFullCol12">
        <div class="row">
            <div class="col-md-12">
                <label class="setTopUpSalary">{{trans('common.top_up_salary')}}</label>
                <p class="settopUpclassp">{{trans('common.only_after_you_top_up')}}</p>
            </div>
            <div class="col-md-12">
                <div class="createBoxtopupsalary">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="setblancetopupsalary">{{trans('common.balance')}}</label>
                            <hr class="setHrbalance">
                        </div>
                    </div>
                    <form class="form">
                        <div class="row setrowflb">
                            <div class="col-md-5">
                                <label class="setbffletpus">{{trans('common.your_total_balance')}}</label>
                                <br>
                                <span class="settotalpricetpus">1000 USD</span>
                            </div>
                            <div class="col-md-7 settpsucol7">
                                <label class="setbfftfletpus">{{trans('common.balance_for_freelancer')}}</label>
                                <br>
                                <span class="setpriceforfreelance">300 USD</span>
                                <br>
                                <span class="setpriceperhour">$ 4.0 / {{trans('common.hr')}} = 37.7 {{trans('common.hours')}}</span>
                            </div>
                        </div>
                        <br>
                        <div class="row settpsurow1">
                                <label class="setlabelfortpus">{{trans('common.top_up_amount')}}</label>
                                <input type="text" class="form-control settpusforminput" name="freelance_amount" id="freelance_amount" value="">
                        </div>
                        <br>
                        <div class="row settpsurow2">
                            <button class="btn btn-block setbtntpsubtn1">
                               {{trans('common.confirm')}}
                            </button>
                            <button class="btn btn-block setbtntpsubtn2">
                                {{trans('common.Cancel')}}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>