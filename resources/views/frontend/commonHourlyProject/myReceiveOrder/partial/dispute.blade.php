<div class="row new-milestone-add setRowNewMileS setDPAGE" xmlns="http://www.w3.org/1999/html">
    <div class="col-md-12 setFullCol12">

        <div class="row">
            <div class="col-md-12 setCol12001DD">
                <label class="setlabel1D">{{trans('common.dispute')}}</label>
            </div>
            <div class="col-md-12 setCol12001DD">
                <label class="setlabel2D">{{trans('common.please_contact')}}</label>
            </div>
        </div>
        <br>
        <div class="row setcol12DR">
            <div class="col-md-12 setRowDcol12">
                <div class="col-md-1 setcol1DisputeCol12Col1">
                    <span class="Createdot"></span>
                </div>
                <div class="col-md-11 setCol11-D2">
                    <label class="setlabel3d">{{trans('common.remain_balance')}}: &nbsp; <span>210 USD</span></label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 setCol122D">
                <div class="col-md-6 setCol6-2D">
                    <label class="setlabel4DT">{{trans('common.amount_to_dispute')}}</label>
                    <input type="text" name="text" class="form-control setinputad">
                </div>
                <div class="col-md-6 setcol6dt">
                    <label class="setlabel5DT">{{trans('common.total_request_dispute')}}</label>
                    <br>
                    <span class="setspn0d">$ 300 USD</span>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12 setcol12dPad">
                <div class="col-md-1 setcoldotD">
                    <span class="setdotD"></span>
                </div>
                <div class="col-md-6 setCol6WidthD">
                    <label class="setlabel7DL">{{trans('common.last_days_amount')}}: &nbsp; <span>2000 USD</span></label>
                <br>
                    <div class="slideshow-container">
                        <div class="set-slider-container">
                            <div class="inner-container" id="sld">
                                <div class="scrollSlider">
                                    <div class="mySlides">
                                        <div class="row">
                                            <div class="col-md-12 setCol12DMSLIDER">
                                                <div class="col-md-11">
                                                    <label  class="setlabelCarousal1">21-02-2017 Time SS:MM:HH 2:00 Hours = 20 USD</label>
                                                </div>
                                                <div class="col-md-1 setDotCol2">
                                                    <span class="setDotCD" ></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mySlides">
                                        <div class="row">
                                            <div class="col-md-12 setCol12DMSLIDER">
                                                <div class="col-md-11">
                                                    <label  class="setlabelCarousal1">22-02-2017 Time SS:MM:HH 2:00 Hours = 20 USD</label>
                                                </div>
                                                <div class="col-md-1 setDotCol2">
                                                    <span class="setDotCD" ></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mySlides">
                                        <div class="row">
                                            <div class="col-md-12 setCol12DMSLIDER">
                                                <div class="col-md-11">
                                                    <label  class="setlabelCarousal1">23-02-2017 Time SS:MM:HH 2:00 Hours = 20 USD</label>
                                                </div>
                                                <div class="col-md-1 setDotCol2">
                                                    <span class="setDotCD" ></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mySlides">
                                        <div class="row">
                                            <div class="col-md-12 setCol12DMSLIDER">
                                                <div class="col-md-11">
                                                    <label  class="setlabelCarousal1">24-02-2017 Time SS:MM:HH 2:00 Hours = 20 USD</label>
                                                </div>
                                                <div class="col-md-1 setDotCol2">
                                                    <span class="setDotCD" ></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mySlides">
                                        <div class="row">
                                            <div class="col-md-12 setCol12DMSLIDER">
                                                <div class="col-md-11">
                                                    <label  class="setlabelCarousal1">25-02-2017 Time SS:MM:HH 2:00 Hours = 20 USD</label>
                                                </div>
                                                <div class="col-md-1 setDotCol2">
                                                    <span class="setDotCD" ></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mySlides">
                                        <div class="row">
                                            <div class="col-md-12 setCol12DMSLIDER">
                                                <div class="col-md-11">
                                                    <label  class="setlabelCarousal1">26-02-2017 Time SS:MM:HH 2:00 Hours = 20 USD</label>
                                                </div>
                                                <div class="col-md-1 setDotCol2">
                                                    <span class="setDotCD" ></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mySlides">
                                        <div class="row">
                                            <div class="col-md-12 setCol12DMSLIDER">
                                                <div class="col-md-11">
                                                    <label  class="setlabelCarousal1">27-02-2017 Time SS:MM:HH 2:00 Hours = 20 USD</label>
                                                </div>
                                                <div class="col-md-1 setDotCol2">
                                                    <span class="setDotCD" ></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mySlides">
                                        <div class="row">
                                            <div class="col-md-12 setCol12DMSLIDER">
                                                <div class="col-md-11">
                                                    <label  class="setlabelCarousal1">28-02-2017 Time SS:MM:HH 2:00 Hours = 20 USD</label>
                                                </div>
                                                <div class="col-md-1 setDotCol2">
                                                    <span class="setDotCD" ></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mySlides">
                                        <div class="row">
                                            <div class="col-md-12 setCol12DMSLIDER">
                                                <div class="col-md-11">
                                                    <label  class="setlabelCarousal1">29-02-2017 Time SS:MM:HH 2:00 Hours = 20 USD</label>
                                                </div>
                                                <div class="col-md-1 setDotCol2">
                                                    <span class="setDotCD" ></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mySlides">
                                        <div class="row">
                                            <div class="col-md-12 setCol12DMSLIDER">
                                                <div class="col-md-11">
                                                    <label  class="setlabelCarousal1">30-02-2017 Time SS:MM:HH 2:00 Hours = 20 USD</label>
                                                </div>
                                                <div class="col-md-1 setDotCol2">
                                                    <span class="setDotCD" ></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mySlides">
                                        <div class="row">
                                            <div class="col-md-12 setCol12DMSLIDER">
                                                <div class="col-md-11">
                                                    <label  class="setlabelCarousal1">21-02-2017 Time SS:MM:HH 2:00 Hours = 20 USD</label>
                                                </div>
                                                <div class="col-md-1 setDotCol2">
                                                    <span class="setDotCD" ></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mySlides">
                                        <div class="row">
                                            <div class="col-md-12 setCol12DMSLIDER">
                                                <div class="col-md-11">
                                                    <label  class="setlabelCarousal1">22-02-2017 Time SS:MM:HH 2:00 Hours = 20 USD</label>
                                                </div>
                                                <div class="col-md-1 setDotCol2">
                                                    <span class="setDotCD" ></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mySlides">
                                        <div class="row">
                                            <div class="col-md-12 setCol12DMSLIDER">
                                                <div class="col-md-11">
                                                    <label  class="setlabelCarousal1">23-02-2017 Time SS:MM:HH 2:00 Hours = 20 USD</label>
                                                </div>
                                                <div class="col-md-1 setDotCol2">
                                                    <span class="setDotCD" ></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mySlides">
                                        <div class="row">
                                            <div class="col-md-12 setCol12DMSLIDER">
                                                <div class="col-md-11">
                                                    <label  class="setlabelCarousal1">24-02-2017 Time SS:MM:HH 2:00 Hours = 20 USD</label>
                                                </div>
                                                <div class="col-md-1 setDotCol2">
                                                    <span class="setDotCD" ></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mySlides">
                                        <div class="row">
                                            <div class="col-md-12 setCol12DMSLIDER">
                                                <div class="col-md-11">
                                                    <label  class="setlabelCarousal1">25-02-2017 Time SS:MM:HH 2:00 Hours = 20 USD</label>
                                                </div>
                                                <div class="col-md-1 setDotCol2">
                                                    <span class="setDotCD" ></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mySlides">
                                        <div class="row">
                                            <div class="col-md-12 setCol12DMSLIDER">
                                                <div class="col-md-11">
                                                    <label  class="setlabelCarousal1">26-02-2017 Time SS:MM:HH 2:00 Hours = 20 USD</label>
                                                </div>
                                                <div class="col-md-1 setDotCol2">
                                                    <span class="setDotCD" ></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mySlides">
                                        <div class="row">
                                            <div class="col-md-12 setCol12DMSLIDER">
                                                <div class="col-md-11">
                                                    <label  class="setlabelCarousal1">27-02-2017 Time SS:MM:HH 2:00 Hours = 20 USD</label>
                                                </div>
                                                <div class="col-md-1 setDotCol2">
                                                    <span class="setDotCD" ></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mySlides">
                                        <div class="row">
                                            <div class="col-md-12 setCol12DMSLIDER">
                                                <div class="col-md-11">
                                                    <label  class="setlabelCarousal1">28-02-2017 Time SS:MM:HH 2:00 Hours = 20 USD</label>
                                                </div>
                                                <div class="col-md-1 setDotCol2">
                                                    <span class="setDotCD" ></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mySlides">
                                        <div class="row">
                                            <div class="col-md-12 setCol12DMSLIDER">
                                                <div class="col-md-11">
                                                    <label  class="setlabelCarousal1">29-02-2017 Time SS:MM:HH 2:00 Hours = 20 USD</label>
                                                </div>
                                                <div class="col-md-1 setDotCol2">
                                                    <span class="setDotCD" ></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mySlides">
                                        <div class="row">
                                            <div class="col-md-12 setCol12DMSLIDER">
                                                <div class="col-md-11">
                                                    <label  class="setlabelCarousal1">30-02-2017 Time SS:MM:HH 2:00 Hours = 20 USD</label>
                                                </div>
                                                <div class="col-md-1 setDotCol2">
                                                    <span class="setDotCD" ></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a class="prev" onclick="fNext()"><img src="{{asset('images/arrows/arrowUp.png')}}"></a>
                        <a class="next" onclick="fPrevious()"><img src="{{asset('images/arrows/arrowDown.png')}}"></a>
                    </div>
                </div>
                <div class="col-md-5 setCol5WidthD">
                    <form>
                    <label class="setlabel6RDD">{{trans('common.reason_to_dispute')}}</label>
                    <br>
                        <textarea class="form-control settextareaD"></textarea>
                    <br>
                        <button class="btn btn-block setbtnbtnDT">{{trans('common.submit')}}</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 setcol12ATDDPAGE">
                <label class="setLabelATDD">{{trans('common.amount_to_dispute')}}</label>
            </div>
            <div class="col-md-12">
                <input type="text" name="text" class="form-control setInputDDATD">
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="scrollDDPAGE">
                    <table class="table table-striped setTableDD">
                    <thead class="setTheadDD">
                    <tr>
                        <th>{{trans('common.time')}}</th>
                        <th>{{trans('common.reason')}}</th>
                        <th>{{trans('common.amount_time_dispute')}}</th>
                        <th>{{trans('common.status')}}</th>
                        <th>{{trans('common.reply')}}</th>
                        <th>{{trans('common.action')}}</th>
                    </tr>
                    </thead>
                    <tbody class="setTbodyDD">
                    <tr>
                        <td>28-12-2019</td>
                        <td>{{trans('common.reason_of_dispute_shown_here')}}</td>
                        <td>{{trans('common.request_refund_amount')}}:100 USD</td>
                        <td>{{trans('common.wait_for_response')}}:72:00</td>
                        <td>{{trans('common.wait_reply')}}</td>
                        <td class="pendingTDD">{{trans('common.pending')}}</td>
                    </tr>
                    <tr>
                        <td>28-12-2019</td>
                        <td>{{trans('common.reason_of_dispute_shown_here')}}</td>
                        <td>{{trans('common.request_refund_amount')}}:100 USD</td>
                        <td>{{trans('common.employee_agree_refund')}}:00.00 USD</td>
                        <td>{{trans('common.nope_no_agree')}}</td>
                        <td class="askCustomerTDD">{{trans('common.ask_to_check')}}</td>
                    </tr>
                    <tr>
                        <td>28-12-2019</td>
                        <td>{{trans('common.reason_of_dispute_shown_here')}}</td>
                        <td>{{trans('common.request_refund_amount')}}:100 USD</td>
                        <td>{{trans('common.employee_agree_refund')}}:00.00 USD</td>
                        <td>{{trans('common.i_agree')}}</td>
                        <td class="tdCompleted">{{trans('common.completed')}}</td>
                    </tr>
                    </tbody>
                </table>
                </div>
            </div>
        </div>

    </div>
</div>