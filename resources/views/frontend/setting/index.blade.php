@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.setting') }}
@endsection

@section('description')
    {{ trans('common.setting') }}
@endsection

@section('author')
    Xenren
@endsection

@section('url')
    https://www.xenren.co/setting
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
    <link href="/custom/css/frontend/modalMakeOffer.css" rel="stylesheet" type="text/css"/>
    <link href="/custom/css/frontend/newJobs.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/skillVerification.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/settings.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
	<div class="row setCOLROWS">
        <div class="col-md-3 search-title-container">
            <span class="find-experts-search-title text-uppercase setMAinMENU">
                {{ trans('common.main_action') }}
            </span>
        </div>
        <div class="col-md-9 setCol9SETTINGP">
            <span class="find-experts-search-title text-uppercase setSETTTING">
                {{ trans('common.setting') }}
            </span>
        </div>
    </div>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">

            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row setSPAGECOl12">
                <div class="col-md-12 col-sm-12">
                    <div class="portlet light bordered skill-verification">


                        <div class="row setHeightSP1">
                            <div class="col-md-12">
                                <div class="col-md-1 setColMd1SP">
                                    <img src="{{asset('images/settingInbox.png')}}" alt="avatar">
                                </div>
                                <div class="col-md-5 setCol5SettingP">
                                    <span>{{trans('common.email_notification')}}</span>
                                </div>
                                <div class="col-md-6">
                                    <div class="container-btn2">
                                        <div class="btn-toggle2" id="emailNotification" onclick="store('email')">
                                            <div class="set-inner-circle2">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row setHeightSP setUIPrefernce">
                            <div class="col-md-12">
                                <div class="col-md-1 setColMd1SP">
                                    <img src="{{asset('images/NotificationLogo.png')}}" alt="avatar">
                                </div>
                                <div class="col-md-5 setCol5SettingP">
                                    <span>{{trans('common.ui_preference')}}</span>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-12 setCol12Col6">
                                        <div class="col-md-4 setCol4SettingP">
                                            <span>{{trans('common.as_employee')}}</span>
                                            <div class="container-btn3">
                                                <div class="btn-toggle3" id="asEmployee" onclick="store('employee')">
                                                    <div class="set-inner-circle3">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 setCol4SettingP2">
                                            <span class="setCol4Span">{{trans('common.as_cowork')}}</span>
                                            <div class="container-btn3">
                                                <div class="btn-toggle3" id="asFreelance" onclick="store('freelance')">
                                                    <div class="set-inner-circle3">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 setCol4SettingP3">
                                            <span class="setSpanBoth">{{trans('common.both')}}</span>
                                            <div class="container-btn4">
                                                <div class="btn-toggle4" id="asBoth" onclick="store('uipreference')">
                                                    <div class="set-inner-circle4">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row setHeightSP">
                            <div class="col-md-12">
                                <div class="col-md-1 setColMd1SP">
                                    <img src="{{asset('images/mobileLogo.png')}}" alt="avatar">
                                </div>
                                <div class="col-md-5 setCol5SettingP">
                                    <span>{{trans('common.mobile_app_push')}}</span>
                                </div>
                                <div class="col-md-6">
                                    <div class="container-btn2">
                                        <div class="btn-toggle2" id="push_notification" onclick="store('push_notification')">
                                            <div class="set-inner-circle2">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row setHeightSP">
                            <div class="col-md-12">
                                <div class="col-md-1 setColMd1SP">
                                    <img src="{{asset('images/meeting-icons.png')}}" alt="avatar">
                                </div>
                                <div class="col-md-5 setCol5SettingP">
                                    <span>{{trans('common.show_my_checked')}}</span>
                                </div>
                                <div class="col-md-6">
                                    {{--<span class="setspanPSP">{{trans('common.proceed')}}</span>--}}
                                    <div class="container-btn2">
                                        <div class="btn-toggle2" id="checked_in_cowork_space" onclick="store('checked_in_cowork_space')">
                                            <div class="set-inner-circle2">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row setHeightSP">
                            <div class="col-md-12">
                                <div class="col-md-1 setColMd1SP">
                                    <img src="{{asset('images/HumanPic.png')}}" alt="avatar">
                                </div>
                                <div class="col-md-5 setCol5SettingP">
                                    <span>{{trans('common.close_my_account')}}</span>
                                </div>
                                <div class="col-md-6">
                                    <span class="setspanPSP">{{trans('common.proceed')}}</span>
                                    <div class="container-btn2">
                                        <div class="btn-toggle2" id="close_account" onclick="store('close_account')">
                                            <div class="set-inner-circle2">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        {{--<div class="row">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<button id="btn-confirm" class="btn btn-green-bg-white-text">--}}
                                    {{--{{ trans('common.confirm') }}--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                      	{{--{!! Form::close() !!}--}}
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@endsection

@section('modal')
@endsection

@section('footer')
    <script >
	    var user_ui_preference = '{{\Auth::user() ? \Auth::user()->uipreference: false }}';
	    var user_ui_preference_both = '{{\App\Models\User::USER_UI_PREFERENCE_BOTH}}';
	    var user_ui_preference_freelancer = '{{\App\Models\User::USER_UI_PREFERENCE_FREELANCER}}';
	    var user_ui_preference_employer = '{{\App\Models\User::USER_UI_PREFERENCE_EMPLOYER}}';
	    var user_email_notification = '{{\Auth::user() ? \Auth::user()->email_notification: false }}';
	    var user_wechat_notification = '{{\Auth::user() ? \Auth::user()->wechat_notification: false }}';
	    var user_push_notification = '{{\Auth::user() ? \Auth::user()->push_notification: false }}';
	    var user_close_account = '{{\Auth::user() ? \Auth::user()->close_account: false }}';
	    var push_notification = '{{\App\Models\User::APP_PUSH_NOTIFICATION}}';
	    var close_account = '{{\App\Models\User::CLOSE_MY_ACCOUNT}}';
	    var email_notification = '{{\App\Models\User::USER_EMAIL_NOTIFICATION}}';
	    var wechat_notification = '{{\App\Models\User::USER_WECHAT_NOTIFICATION}}';
	    var checked_in_space = '{{\App\Models\User::CHECKED_IN_CO_WORK_SPACE}}';
	    var checked_in_COWORK = '{{\Auth::user() ? \Auth::user()->checked_in_cowork_space : false}}';
    </script>
    <script type="text/javascript" src="/custom/js/frontend/settings.js" defer="defer"></script>
@endsection




