@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.inbox_title') }}
@endsection

@section('keywords')
    {{ trans('common.inbox_keyword') }}
@endsection

@section('description')
    {{ trans('common.inbox_desc') }}
@endsection

@section('author')
Xenren
@endsection

@section('url')
    https://www.xenren.co/inbox
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
{{--    <link href="/custom/css/frontend/inbox.css" rel="stylesheet" type="text/css" />--}}
    <link href="{{asset('css/minifiedInbox.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
	<div class="row setRowCol12">
        <div class="col-md-3 search-title-container">
            <span class="find-experts-search-title text-uppercase setMAinMENU">{{ trans('common.main_action') }}</span>
        </div>
        <div class="col-md-9 right-top-main set-right-top-main">
        	<div class="row">
                <div class="col-md-12 top-tabs-menu setIBOXMPCol12">
                    <div class="order-status-menu pull-right setINBOXMPOSPR">
                        <ul>
                            <li class="{{ $tab == 1? 'active': '' }}">
                                <a href="{{ route('frontend.inbox', \Session::get('lang')) }}?category=1">{{ trans('common.inbox') }}</a>
                            </li>
                            <li class="{{ $tab == 2? 'active': '' }}">
                                <a href="{{ route('frontend.inbox', \Session::get('lang')) }}?category=2">{{ trans('common.system_message') }}</a>
                            </li>
                            <li class="{{ $tab == 3? 'active': '' }}">
                                <a href="{{ route('frontend.inbox', \Session::get('lang')) }}?isTrash=1">{{ trans('common.trash')}}</a>
                            </li>
                        </ul>
                    </div>
                    <div class="caption setIBOXMPTC">
                        <span class="find-experts-search-title text-uppercase">{{ trans('common.inbox') }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
	        <div class="tab-content">
	        	<div class="tab-pane active inbox-main-section" id="inbox">
	            	<div class="row header-action">
		        		<div class="col-md-6 ">
                           	{{--<div class="checkbox-all">--}}
	                           	{{--<span>--}}
	                                {{--<input type="checkbox" value="" name="" class="checkboxes">--}}
	                            {{--</span>--}}
                           	{{--</div>--}}
                           	<div class="col-md-1 checkbox-sub checkbox-all">
                                <span class="spnAllItemTick">
                                    <input type="checkbox" value="" name="chkAllItemTick" class="chkAllItemTick">
                                </span>
                            </div>

		        			<div class="search-selectbox">
			                    <select class="form-control" name="slcActions" id="slcActions">
			                        <option value="0">{{ trans('common.operate') }}</option>
			                        @foreach( $actions as $key => $action )
			                        <option value="{{ $key }}">{{ $action }}</option>
			                        @endforeach
			                    </select>
			                </div>
		        		</div>
		        		<div class="col-md-6 set-pagination-inbox-page">
		        		    @if ($myInbox->lastPage() > 1)
		        			<div class="pull-right">
						        {{ $myInbox->links() }}
						    </div>
						    @endif
		        		</div>
		        	</div>
	            	<div class="portlet box setINBOXMPPB">
		        		<div class="portlet-body">
                            <div class="row inbox-mail">
                                <div style="display: flex;justify-content: flex-end">
                                    <span style="color:#555;font-size:13px;font-weight:bold;padding-right:60px;">Your Average Response Time : {{$response_time}}
                                    </span>
                                </div>
                            </div>
                            @foreach ($myInbox as $key => $var)
		        		    <div class="row inbox-mail {{ $var->getLatestInboxMessageIsRead(true, Auth::guard('users')->user()->id) == 0? 'striped': '' }}" id="inbox-{{ $var->id }}">
                                <div class="col-md-1 checkbox-sub">
                                    <span class="spnItemTick-{{ $var->id }}">
                                        <input type="checkbox" value="{{ $var->id }}" name="chkItemTick[]" class="chkItemTick">
                                    </span>
                                </div>
                                <div class="col-md-11 inbox-mail-detail">
                                    <a href="{{ route('frontend.inbox.messages', ['inbox_id' => $var->id ]) }}">
                                        <div class="row">
                                            @if( \Auth::user()->id == $var->from_user_id)
                                                @php
                                                    $person = $var->getReceiverInfo()
                                                @endphp
                                            @else
                                                @php
                                                    $person = $var->getSenderInfo()
                                                @endphp
                                            @endif
                                            <!-- @php
                                                $sender = $var->getSenderInfo()
                                            @endphp -->
                                            <div class="col-md-1 user-img">
                                                <div class="user-imgs">
                                                    @php
                                                        $url = \URL::to('/');
                                                        $image = str_replace($url,'',$person['avatar']);
                                                    @endphp
                                                    <img src="
                                                    @if(file_exists(public_path().$image))
                                                        {{$person['avatar']}}
                                                    @else
                                                        \images\image.png
                                                    @endif">
                                                    <i class="fa fa-circle offline"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-4 user-name">
                                                <h5>
                                                    {{ $person['name'] }}
                                                </h5>
                                                @if( isset($var->project) )
                                                <h6>{{ truncateSentence($var->project->name, 30) }}</h6>
                                                @endif
                                            </div>
                                            <div class="col-md-5 user-message">
                                                <p>{{ $var->getLatestInboxMessageSenderName() }}:
                                                    <span class="green-color">
                                                        {!! truncateSentence($var->getTitle(), 50) !!}
                                                    </span>
                                                </p>
                                            </div>
                                            <div class="col-md-2 time setINBOXMPPBTCol2">
                                                <span>
                                                    {{ $var->getLatestInboxMessageDate() }}
                                                </span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            @endforeach

                            @if(count($myInbox) == 0 )
                            <div class="text-center no-inbox-sect setINBOXPPBNISECT">
                                @if( $tab == 1 )
                                {{ trans('common.inbox_is_empty') }}
                                @elseif( $tab == 2 )
                                {{ trans('common.system_message_is_empty') }}
                                @elseif( $tab == 3 )
                                {{ trans('common.trash_is_empty') }}
                                @endif
                                <br/>
                                <img src="\images\no-inbox.png" class="setINBOXPAGEINBOXIMAGE"/>
                            </div>
                            @endif
	            			{{--<div class="row inbox-mail striped">--}}
                                {{--<div class="col-md-1 checkbox-sub">--}}
                                    {{--<span class="checked">--}}
                                        {{--<input type="checkbox" value="" name="" class="checkboxes" checked="checked">--}}
                                    {{--</span>--}}
                                {{--</div>            --}}
                                {{--<div class="col-md-11 inbox-mail-detail">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-1 user-img">--}}
                                            {{--<div class="user-imgs">--}}
                                                {{--<img src="/images/f-user-icon1.png">--}}
                                                {{--<i class="fa fa-circle online"></i>--}}
                                            {{--</div>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-4 user-name">--}}
                                            {{--<h5>Adam Johnson</h5>--}}
                                            {{--<h6>Pinterest standard Graphic ...</h6>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-5 user-message">--}}
                                            {{--<p>Adam: <span> Hi, how are you? for the first 50 and...</span></p>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-2 time">--}}
                                            {{--<span>Just now</span>--}}
                                        {{--</div>                    --}}
                                    {{--</div>--}}
                                {{--</div>            --}}
                            {{--</div>--}}

                            {{--<div class="row inbox-mail striped">--}}
                                {{--<div class="col-md-1 checkbox-sub">--}}
                                    {{--<span class="">--}}
                                        {{--<input type="checkbox" value="" name="" class="checkboxes">--}}
                                    {{--</span>--}}
                                {{--</div>            --}}
                                {{--<div class="col-md-11 inbox-mail-detail">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-1 user-img">--}}
                                            {{--<div class="user-imgs">--}}
                                                {{--<img src="/images/f-user-icon2.png">--}}
                                                {{--<i class="fa fa-circle online"></i>--}}
                                            {{--</div>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-4 user-name">--}}
                                            {{--<h5>Adam Johnson</h5>--}}
                                            {{--<h6>iOS Developer to hire on ...</h6>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-5 user-message">--}}
                                            {{--<p>you: <span> Yes, I have uploadede fiels on drive.</span></p>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-2 time">--}}
                                            {{--<span>1 hour ago</span>--}}
                                        {{--</div>                    --}}
                                    {{--</div>--}}
                                {{--</div>            --}}
                            {{--</div>--}}

                            {{--<div class="row inbox-mail striped">--}}
                                {{--<div class="col-md-1 checkbox-sub">--}}
                                    {{--<span class="checked">--}}
                                        {{--<input type="checkbox" value="" name="" class="checkboxes">--}}
                                    {{--</span>--}}
                                {{--</div>            --}}
                                {{--<div class="col-md-11 inbox-mail-detail">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-1 user-img">--}}
                                            {{--<div class="user-imgs">--}}
                                                {{--<img src="/images/f-user-icon3.png">--}}
                                                {{--<i class="fa fa-circle offline"></i>--}}
                                            {{--</div>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-4 user-name">--}}
                                            {{--<h5>Maria Doe</h5>--}}
                                            {{--<h6>Graphic & Web designer at...</h6>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-5 user-message">--}}
                                            {{--<p>Keithy: <span class="green-color"> Send offer to the Project!</span></p>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-2 time">--}}
                                            {{--<span>Yestraday</span>--}}
                                        {{--</div>                    --}}
                                    {{--</div>--}}
                                {{--</div>            --}}
                            {{--</div>--}}

                            {{--<div class="row inbox-mail">--}}
                                {{--<div class="col-md-1 checkbox-sub">--}}
                                    {{--<span class="">--}}
                                        {{--<input type="checkbox" value="" name="" class="checkboxes">--}}
                                    {{--</span>--}}
                                {{--</div>            --}}
                                {{--<div class="col-md-11 inbox-mail-detail">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-1 user-img">--}}
                                            {{--<div class="user-imgs">--}}
                                                {{--<img src="/images/f-user-icon4.png">--}}
                                                {{--<i class="fa fa-circle offline"></i>--}}
                                            {{--</div>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-4 user-name">--}}
                                            {{--<h5>Lee Fernandis</h5>--}}
                                            {{--<h6>Website Design for start...</h6>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-5 user-message">--}}
                                            {{--<p>Lee: <span class="green-color"> Send offer to the Project!</span></p>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-2 time">--}}
                                            {{--<span>10 Jan 2016</span>--}}
                                        {{--</div>                    --}}
                                    {{--</div>--}}
                                {{--</div>            --}}
                            {{--</div>--}}

                            {{--<div class="row inbox-mail">--}}
                                {{--<div class="col-md-1 checkbox-sub">--}}
                                    {{--<span class="">--}}
                                        {{--<input type="checkbox" value="" name="" class="checkboxes">--}}
                                    {{--</span>--}}
                                {{--</div>            --}}
                                {{--<div class="col-md-11 inbox-mail-detail">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-1 user-img">--}}
                                            {{--<div class="user-imgs">--}}
                                                {{--<img src="/images/f-user-icon5.png">--}}
                                                {{--<i class="fa fa-circle offline"></i>--}}
                                            {{--</div>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-4 user-name">--}}
                                            {{--<h5>Will Maths</h5>--}}
                                            {{--<h6>Website Design for start...</h6>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-5 user-message">--}}
                                            {{--<p>Will: <span class="green-color"> Request you for contact visible!</span></p>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-2 time">--}}
                                            {{--<span>10 Jan 2016 </span>--}}
                                        {{--</div>                    --}}
                                    {{--</div>--}}
                                {{--</div>            --}}
                            {{--</div>--}}

                            {{--<div class="row inbox-mail">--}}
                                {{--<div class="col-md-1 checkbox-sub">--}}
                                    {{--<span class="checked">--}}
                                        {{--<input type="checkbox" value="" name="" class="checkboxes" checked="checked">--}}
                                    {{--</span>--}}
                                {{--</div>            --}}
                                {{--<div class="col-md-11 inbox-mail-detail">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-1 user-img">--}}
                                            {{--<div class="user-imgs">--}}
                                                {{--<img src="/images/f-user-icon1.png">--}}
                                                {{--<i class="fa fa-circle online"></i>--}}
                                            {{--</div>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-4 user-name">--}}
                                            {{--<h5>Adam Johnson</h5>--}}
                                            {{--<h6>Pinterest standard Graphic ...</h6>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-5 user-message">--}}
                                            {{--<p>You: <span> Hi, how are you? for the first 50 and...</span></p>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-2 time">--}}
                                            {{--<span>09 Jan 2016</span>--}}
                                        {{--</div>                    --}}
                                    {{--</div>--}}
                                {{--</div>            --}}
                            {{--</div>--}}
                            {{----}}
                            {{--<div class="row inbox-mail">--}}
                                {{--<div class="col-md-1 checkbox-sub">--}}
                                    {{--<span class="">--}}
                                        {{--<input type="checkbox" value="" name="" class="checkboxes">--}}
                                    {{--</span>--}}
                                {{--</div>            --}}
                                {{--<div class="col-md-11 inbox-mail-detail">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-1 user-img">--}}
                                            {{--<div class="user-imgs">--}}
                                                {{--<img src="/images/f-user-icon2.png">--}}
                                                {{--<i class="fa fa-circle online"></i>--}}
                                            {{--</div>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-4 user-name">--}}
                                            {{--<h5>Adam Johnson</h5>--}}
                                            {{--<h6>iOS Developer to hire on ...</h6>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-5 user-message">--}}
                                            {{--<p>you: <span> Yes, I have uploadede fiels on drive.</span></p>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-2 time">--}}
                                            {{--<span>09 Jan 2016</span>--}}
                                        {{--</div>                    --}}
                                    {{--</div>--}}
                                {{--</div>            --}}
                            {{--</div>--}}

                            {{--<div class="row inbox-mail">--}}
                                {{--<div class="col-md-1 checkbox-sub">--}}
                                    {{--<span class="checked">--}}
                                        {{--<input type="checkbox" value="" name="" class="checkboxes">--}}
                                    {{--</span>--}}
                                {{--</div>            --}}
                                {{--<div class="col-md-11 inbox-mail-detail">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-1 user-img">--}}
                                            {{--<div class="user-imgs">--}}
                                                {{--<img src="/images/f-user-icon3.png">--}}
                                                {{--<i class="fa fa-circle offline"></i>--}}
                                            {{--</div>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-4 user-name">--}}
                                            {{--<h5>Maria Doe</h5>--}}
                                            {{--<h6>Graphic & Web designer at...</h6>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-5 user-message">--}}
                                            {{--<p>Keithy: <span class="green-color"> Send offer to the Project!</span></p>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-2 time">--}}
                                            {{--<span>09 Jan 2016</span>--}}
                                        {{--</div>                    --}}
                                    {{--</div>--}}
                                {{--</div>            --}}
                            {{--</div>--}}

                            {{--<div class="row inbox-mail">--}}
                                {{--<div class="col-md-1 checkbox-sub">--}}
                                    {{--<span class="">--}}
                                        {{--<input type="checkbox" value="" name="" class="checkboxes">--}}
                                    {{--</span>--}}
                                {{--</div>            --}}
                                {{--<div class="col-md-11 inbox-mail-detail">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-1 user-img">--}}
                                            {{--<div class="user-imgs">--}}
                                                {{--<img src="/images/f-user-icon4.png">--}}
                                                {{--<i class="fa fa-circle offline"></i>--}}
                                            {{--</div>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-4 user-name">--}}
                                            {{--<h5>Lee Fernandis</h5>--}}
                                            {{--<h6>Website Design for start...</h6>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-5 user-message">--}}
                                            {{--<p>Lee: <span class="green-color"> Send offer to the Project!</span></p>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-2 time">--}}
                                            {{--<span>08 Jan 2016</span>--}}
                                        {{--</div>                    --}}
                                    {{--</div>--}}
                                {{--</div>            --}}
                            {{--</div>--}}

                            {{--<div class="row inbox-mail">--}}
                                {{--<div class="col-md-1 checkbox-sub">--}}
                                    {{--<span class="">--}}
                                        {{--<input type="checkbox" value="" name="" class="checkboxes">--}}
                                    {{--</span>--}}
                                {{--</div>            --}}
                                {{--<div class="col-md-11 inbox-mail-detail">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-1 user-img">--}}
                                            {{--<div class="user-imgs">--}}
                                                {{--<img src="/images/f-user-icon5.png">--}}
                                                {{--<i class="fa fa-circle offline"></i>--}}
                                            {{--</div>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-4 user-name">--}}
                                            {{--<h5>Will Maths</h5>--}}
                                            {{--<h6>Website Design for start...</h6>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-5 user-message">--}}
                                            {{--<p>Adam: <span class="green-color"> Request you for contact visible!</span></p>--}}
                                        {{--</div>            --}}
                                        {{--<div class="col-md-2 time">--}}
                                            {{--<span>08 Jan 2016 </span>--}}
                                        {{--</div>                    --}}
                                    {{--</div>--}}
                                {{--</div>            --}}
                            {{--</div>--}}
		                </div>
		        	</div>
		        </div>
	            <div class="tab-pane" id="system-message">
	            	System Message
	            </div>
	            <div class="tab-pane" id="trash">
	            	Trash
	            </div>
            </div>
        </div>
    </div>

@endsection

@section('footer')
<script type="text/javascript" defer="defer">
    var url = {
        'trashAll': "{{ route('frontend.inbox.trashall') }}",
        'untrashAll': "{{ route('frontend.inbox.untrashall') }}",
        'markAsRead': "{{ route('frontend.inbox.markasread') }}",
        'removeAll': "{{ route('frontend.inbox.removeall') }}"
    }
    $(document).ready(function () {
        $('.chkAllItemTick').on('click', function () {
            if ($(this)[0].checked) {
                $('.chkItemTick').each(function () {
//                    var id = $(this).val();
//                    $('.spnItemTick-' + id ).addClass('checked');
                    if( $(this).prop('checked') == false ) {
                        $(this).trigger('click');
                    }
                });

                $('.spnAllItemTick').addClass('checked');
            } else {
                $('.chkItemTick').each(function () {
//                    var id = $(this).val();
//                    $('.spnItemTick-' + id ).removeClass('checked');
                    if( $(this).prop('checked') == true ) {
                        $(this).trigger('click');
                    }
                });

                $('.spnAllItemTick').removeClass('checked');
            }
        });

        $('.chkItemTick').on('click', function () {
            var id = $(this).val();
            if( $(this).prop('checked') == true ) {
                $('.spnItemTick-' + id ).addClass('checked');
            } else {
                $('.spnItemTick-' + id ).removeClass('checked');
            }
        });

        $('#slcActions').on('change', function(e){

            var data = { 'inboxIds[]' : []};
            $(".chkItemTick:checked").each(function() {
                data['inboxIds[]'].push($(this).val());
            });

            if( $(this).val() == '1' )
            {
                //mark as read
                $.ajax({
                    url: url.markAsRead,
                    dataType: 'json',
                    data: data,
                    method: 'POST',
                    error: function () {
                        alert(lang.unable_to_request);
                    },
                    success: function (result) {
                        if(result.status == 'success') {
                            alertSuccess(result.msg);

                            $(".chkItemTick:checked").each(function() {
                                $('#inbox-' + $(this).val()).removeClass('striped');
                            });
                        }else{
                            alertError(result.msg);
                        }
                    }
                });
            }
            else if( $(this).val() == '2' )
            {
                //move to trash
                $.ajax({
                    url: url.trashAll,
                    dataType: 'json',
                    data: data,
                    method: 'POST',
                    error: function () {
                        alert(lang.unable_to_request);
                    },
                    success: function (result) {
                        if(result.status == 'success') {
                            alertSuccess(result.msg);

                            $(".chkItemTick:checked").each(function() {
                                $('#inbox-' + $(this).val()).hide();
                            });
                        }else{
                            alertError(result.msg);
                        }
                    }
                });
            }
            else if( $(this).val() == '3' )
            {
                //cancel trash and move back to inbox
                $.ajax({
                    url: url.untrashAll,
                    dataType: 'json',
                    data: data,
                    method: 'POST',
                    error: function () {
                        alert(lang.unable_to_request);
                    },
                    success: function (result) {
                        if(result.status == 'success') {
                            alertSuccess(result.msg);

                            $(".chkItemTick:checked").each(function() {
                                $('#inbox-' + $(this).val()).hide();
                            });
                        }else{
                            alertError(result.msg);
                        }
                    }
                });
            }
            else if( $(this).val() == '4' )
            {
                //remove inbox and inbox message
                $.ajax({
                    url: url.removeAll,
                    dataType: 'json',
                    data: data,
                    method: 'POST',
                    error: function () {
                        alert(lang.unable_to_request);
                    },
                    success: function (result) {
                        if(result.status == 'success') {
                            alertSuccess(result.msg);

                            $(".chkItemTick:checked").each(function() {
                                $('#inbox-' + $(this).val()).hide();
                            });
                        }else{
                            alertError(result.msg);
                        }
                    }
                });
            }
        });
    });
</script>
@endsection
