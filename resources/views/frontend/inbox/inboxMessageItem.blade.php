@foreach( $inboxMessages as $key => $inboxMessage )
    <div class="col-md-12 message-section">
        <div class="row">
            @php
                $senderInfo = $inboxMessage->getSenderInfo()
            @endphp
            <div class="col-md-1 user-img">
                <img src="{{ $senderInfo['avatar'] }}">
            </div>
            <div class="col-md-10 user-detail">
                <h5>{{ $senderInfo['name'] }}</h5>
                <span>{{ $inboxMessage->getCreatedDate() }}</span>

                <p>
                    {!! $inboxMessage->getCustomMessage() !!}
                </p>

                @if( $inboxMessage->file != null )
                <p>
                    <a href="{{ $inboxMessage->getFile() }}" data-lightbox="image" >
                        <img src="{{ $inboxMessage->getFile() }}" width="100px">
                    </a>
                </p>
                @endif

                @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_APPLICANT_SELECTED )
                <div class="row message-section-sub">
                    <div class="col-md-12 message-section-subs">
                        <h5>{{ trans('common.congrat_to_selected_user') }}</h5>
                        {{--<p>Pinterest standard Graphic designer for long term.</p>--}}
                        {{--<p>Budget: <span>$ 4500</span> </p>--}}
                        <br>
                        @if( $inboxMessage->from_user_id != $user->id && !isset($is_awarded))
                        <a class="acceptJob"
                            data-url="{{ route('frontend.inbox.acceptjob') }}"
                            data-project-id="{{ $inbox->project->id }}"
                            data-inbox-id="{{ $inbox->id }}">
                            <button class="btn-customs-all btn-green-custom margin-r-10 setbtnbtnInboxMessages">
                                {{ trans('common.accept') }}
                            </button>
                        </a>

                        <a class="rejectJob"
                            data-url="{{ route('frontend.inbox.rejectjob') }}"
                            data-project-id="{{ $inbox->project->id }}"
                            data-inbox-id="{{ $inbox->id }}">
                            <button class="btn-customs-all btn-default-custom setbtnbtnInboxMessages">
                                {{ trans('common.reject') }}
                            </button>
                        </a>
                        @endif
                    </div>
                </div>
                @endif

                @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_NEW_MESSAGE  )
                @endif

                @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_CONFIRMED_COMPLETED )
                <div class="row message-section-sub">
                    <div class="col-md-12 message-section-subs">
                        <h5>{{ trans('common.project_completed') }}</h5>
                        <br>
                    </div>
                </div>
                @endif

                @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_INVITE_YOU )
                    @if(isset($inboxMessage->project) && isset($inboxMessage->project->name))
                    <div class="row message-section-sub">
                        <div class="col-md-12 message-section-subs">
                            <h5>{{ trans('common.send_invite_by_the_project') }}</h5>
                            <p>{{ $inboxMessage->project->name }}</p>
                            <p>Require Daily Update: {{isset($inbox->daily_update) && $inbox->daily_update && $inbox->daily_update == 1 ? 'Yes': 'No'}}</p>
                            <p>Skills: @foreach($inboxMessage->project->projectskills as $v) {{$v->skill->name_cn}} &nbsp;   @endforeach</p>

                            {{--<p>Budget: <span>$ 4500</span> </p>--}}
                            <br>

                            @if( $inboxMessage->from_user_id != $user->id  && !isset($is_awarded))
                            <a href="{{ route('frontend.modal.makeoffer', [
                                    'project_id' => $inboxMessage->project->id,
                                    'user_id' => \Auth::id(),
                                    'inbox_id' => $inboxMessage->inbox_id
                                ] ) }}"
                                data-toggle="modal"
                                data-target="#modalMakeOffer">
                                <button class="btn-customs-all btn-green-custom margin-r-10 setbtnbtnInboxMessages">
                                    {{ trans('common.accept') }}
                                </button>
                            </a>
                            <a class="rejectInvite"
                                data-url="{{ route('frontend.inbox.rejectinvite') }}"
                                data-receiver-id="{{ $inboxMessage->from_user_id }}"
                                data-inbox-id="{{ $inbox->id }}">
                                <button class="btn-customs-all btn-default-custom setbtnbtnInboxMessages">
                                    {{ trans('common.reject') }}
                                </button>
                            </a>
                            @endif
                        </div>
                    </div>
                    @endif
                @endif

                @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_SEND_OFFER )
                    @if(isset($inboxMessage->project) && isset($inboxMessage->project->name))
                    <div class="row message-section-sub">
                    <div class="col-md-12 message-section-subs">
                        <h5>{{ trans('common.send_offer_by_the_project') }}</h5>
                        <p>{{ $inboxMessage->project->name }}</p>
                        <p>{!!  $inboxMessage->project->description !!}</p>
                        @if ( $inboxMessage->quote_price != null )
                        <p> {{ trans('common.require_daily_update') }}: <span>{{isset($inbox->daily_update) && $inbox->daily_update && $inbox->daily_update == 1 ? 'Yes': 'No'}}</p></span> </p>
                        <p> {{ trans('common.offer') }}: <span>$ {{ $inboxMessage->quote_price }} ( {{ \App\Constants::translatePayType( $inboxMessage->pay_type ) }} )</span> </p>
                        @endif
                        <br>

                        @if( $inboxMessage->from_user_id != $user->id )
                            @if($checkUserInboxMessages->type == \App\Models\UserInboxMessages::TYPE_PROJECT_ACCEPT_SEND_OFFER || $checkUserInboxMessages->type == \App\Models\UserInboxMessages::TYPE_PROJECT_REJECT_SEND_OFFER )
                                <a style="display:none" class="acceptSendOffer"
                                   data-url="{{ route('frontend.inbox.acceptsendoffer') }}"
                                   data-receiver-id="{{ $inboxMessage->from_user_id }}"
                                   data-inbox-id="{{ $inbox->id }}"
                                   data-project-type="{{ $inboxMessage->pay_type }}"
                                >
                                    <button class="btn-customs-all btn-green-custom margin-r-10 setbtnbtnInboxMessages">
                                        {{ trans('common.accept') }}
                                    </button>
                                </a>
                                <a style="display:none" class="rejectSendOffer"
                                   data-url="{{ route('frontend.inbox.rejectsendoffer') }}"
                                   data-receiver-id="{{ $inboxMessage->from_user_id }}"
                                   data-inbox-id="{{ $inbox->id }}">
                                    <button class="btn-customs-all btn-default-custom setbtnbtnInboxMessages">
                                        {{ trans('common.reject') }}
                                    </button>
                                </a>
                            @else
                                <a class="acceptSendOffer"
                                   data-url="{{ route('frontend.inbox.acceptsendoffer') }}"
                                   data-receiver-id="{{ $inboxMessage->from_user_id }}"
                                   data-inbox-id="{{ $inbox->id }}"
                                   data-project-type="{{ $inboxMessage->pay_type }}"
                                >
                                    <button class="btn-customs-all btn-green-custom margin-r-10 setbtnbtnInboxMessages">
                                        {{ trans('common.accept') }}
                                    </button>
                                </a>
                                <a class="rejectSendOffer"
                                   data-url="{{ route('frontend.inbox.rejectsendoffer') }}"
                                   data-receiver-id="{{ $inboxMessage->from_user_id }}"
                                   data-inbox-id="{{ $inbox->id }}">
                                    <button class="btn-customs-all btn-default-custom setbtnbtnInboxMessages">
                                        {{ trans('common.reject') }}
                                    </button>
                                </a>
                            @endif

                        @endif
                    </div>
                </div>
                    @endif
                @endif

                @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_RECEIVE_OFFER )
                    @if(isset($inboxMessage->project) && isset($inboxMessage->project->name))
                    <div class="row message-section-sub">
                    <div class="col-md-12 message-section-subs">
                        <h5>{{ trans('common.send_offer_by_the_project') }}</h5>
                        <p>{{ $inboxMessage->project->name }}</p>
                        <p>{!!  $inboxMessage->project->description !!}</p>
                        @if ( $inboxMessage->quote_price != null )
                        <p> {{ trans('common.require_daily_update') }}: <span>{{isset($inbox->daily_update) && $inbox->daily_update && $inbox->daily_update == 1 ? 'Yes': 'No'}} </span> </p>
                        <p> {{ trans('common.offer') }}: <span>$ {{ $inboxMessage->quote_price }} ( {{ \App\Constants::translatePayType( $inboxMessage->pay_type ) }} )</span> </p>
                        @endif
                        <br>

                        @if(isset($inboxMessage->project->projectQuestions) && count($inboxMessage->project->projectQuestions) > 0)
                            @foreach($inboxMessage->project->projectQuestions as $k => $v)
                            <h5>Q#{{($loop->index) + 1}}:  {{$v->question}}</h5>
                            @php
                                try {
                                    $answer = $v->applicantAnswer;
                                    $applicantId = \App\Models\ProjectApplicant::where('project_id', '=', $inbox->project->id)->where('user_id', '=', $inbox->getSenderInfo()['id'])->first();
                                    $answer = $answer->where('project_applicant_id', $applicantId->id)->values();
                                    $ans = ($answer[0]->answer);
                                } catch (\Exception $e) {
                                    $ans = '';
                                }
                            @endphp
                            <p>{{($ans)}}</p>
                            @endforeach
                        @endif

                        @if( $inboxMessage->from_user_id != $user->id )
                        <br/>
                        <a class="acceptSendOffer"
                            data-url="{{ route('frontend.inbox.acceptreceiveoffer') }}"
                            data-receiver-id="{{ $inboxMessage->from_user_id }}"
                            data-inbox-id="{{ $inbox->id }}">
                            <button class="btn-customs-all btn-green-custom margin-r-10 setbtnbtnInboxMessages">
                                {{ trans('common.accept') }}
                            </button>
                        </a>
                        <a class="rejectSendOffer"
                            data-url="{{ route('frontend.inbox.rejectrecieveoffer') }}"
                            data-receiver-id="{{ $inboxMessage->from_user_id }}"
                            data-inbox-id="{{ $inbox->id }}">
                            <button class="btn-customs-all btn-default-custom setbtnbtnInboxMessages">
                                {{ trans('common.reject') }}
                            </button>
                        </a>
                        @endif
                    </div>
                </div>
                    @endif
                @endif

                @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_ASK_FOR_CONTACT &&
                    $inboxMessage->from_user_id != $user->id )
                <div class="row message-section-sub">
                    <div class="col-md-12 message-section-subs">
                        <h5>{{ trans('common.request_for_contact_visible') }}</h5>
                        <p> {{ trans('common.email') }} : <span>{{ $user->email }}</span> </p>
                        <p> Line : <span>{{ $user->line_id }}</span> </p>
                        <p> {{ trans('common.wechat') }} : <span>{{ $user->wechat_id }}</span> </p>
                        <p> Skype : <span>{{ $user->skype_id }}</span> </p>
                        <br>
                        <a class="acceptContactInfoRequest"
                            data-url="{{ route('frontend.inbox.acceptrequestforcontactinfo') }}"
                            data-receiver-id="{{ $inboxMessage->from_user_id }}"
                            data-inbox-id="{{ $inbox->id }}">
                            <button class="btn-customs-all btn-green-custom margin-r-10 setbtnbtnInboxMessages">
                                {{ trans('common.accept') }}
                            </button>
                        </a>
                        <a class="rejectContactInfoRequest"
                            data-url="{{ route('frontend.inbox.rejectrequestforcontactinfo') }}"
                            data-receiver-id="{{ $inboxMessage->from_user_id }}"
                            data-inbox-id="{{ $inbox->id }}">
                            <button class="btn-customs-all btn-default-custom setbtnbtnInboxMessages">
                                 {{ trans('common.reject') }}
                            </button>
                        </a>
                    </div>
                </div>
                @endif

                @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_AGREE_SEND_CONTACT)
                <div class="row message-section-sub">
                    <div class="col-md-12 message-section-subs">
                        <h5>{{ trans('common.accepted_contact_visibility') }}</h5>
                        <p> {{ trans('common.email') }} : <span>{{ $inboxMessage->fromUser->email }}</span> </p>
                        <p> Line : <span>{{ $inboxMessage->fromUser->line_id }}</span> </p>
                        <p> {{ trans('common.wechat') }} : <span>{{ $inboxMessage->fromUser->wechat_id }}</span> </p>
                        <p> Skype : <span>{{ $inboxMessage->fromUser->skype_id }}</span> </p>
                        <br>
                        {{--<button class="btn-customs-all btn-green-custom margin-r-10">Confirm</button>--}}
                        {{--<button class="btn-customs-all btn-default-custom">Cancel</button>--}}
                    </div>
                </div>
                @endif

                @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_REJECT_SEND_CONTACT)
                <div class="row message-section-sub">
                    <div class="col-md-12 message-section-subs">
                        <h5>{{ trans('common.rejected_contact_visibility') }}</h5>
                        <br>
                    </div>
                </div>
                @endif

                @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_INVITE_ACCEPT)
                <div class="row message-section-sub">
                    <div class="col-md-12 message-section-subs">
                        <h5>{{ trans('common.invite_accept') }}</h5>
                        <br>
                    </div>
                </div>
                @endif

                @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_INVITE_REJECT)
                <div class="row message-section-sub">
                    <div class="col-md-12 message-section-subs">
                        <h5>{{ trans('common.invite_reject') }}</h5>
                        <br>
                    </div>
                </div>
                @endif

                @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_ACCEPT_SEND_OFFER)
                <div class="row message-section-sub">
                    <div class="col-md-12 message-section-subs">
                        @if($inboxMessage->project->user_id != \Auth::user()->id)
                        <h5>{{ trans('common.accept_send_offer') }}</h5>
                        @else
                        @php
                            $fromUser = $inboxMessage->getFromUserData($inboxMessage->from_user_id);
                            $getQuotePrice = $inboxMessage->getQuotePrice($inboxMessage->from_user_id,$inboxMessage->to_user_id);
                        @endphp
                        <h5>{{ trans('common.accept_send_offer') }}</h5>
                        <br>
                        <p>{{$fromUser->name}}</p>
                        <h5>Have Accept Your Offer</h5>
                        <p><b>Project Name :</b>{{$inboxMessage->project->name}}</p>
                        <p><b>Project Type :</b>{{$inboxMessage->project->getProjectType($inboxMessage->project->pay_type)}}</p>
                        <p>
                            <b>Quote Price :</b>$ {{$getQuotePrice->quote_price}}
                        </p>
                        <br>
                        @endif
                    </div>
                </div>
                @endif

                @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_REJECT_SEND_OFFER)
                <div class="row message-section-sub">
                    <div class="col-md-12 message-section-subs">
                        <h5>{{ trans('common.reject_send_offer') }}</h5>
                        <br>
                    </div>
                </div>
                @endif

                @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_APPLICANT_ACCEPTED)
                <div class="row message-section-sub">
                    <div class="col-md-12 message-section-subs">
                        <h5>{{ trans('common.you_already_accept_job') }}</h5>
                        <br>
                    </div>
                </div>
                @endif

                @if( $inboxMessage->type == \App\Models\UserInboxMessages::TYPE_PROJECT_APPLICANT_REJECTED)
                <div class="row message-section-sub">
                    <div class="col-md-12 message-section-subs">
                        <h5>{{ trans('common.you_already_reject_job') }}</h5>
                        <br>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
@endforeach
