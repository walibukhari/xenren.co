@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.inbox_messages') }}
    {{ trans('member.add_file') }}
@endsection

@section('description')
    {{ trans('common.inbox_messages') }}
@endsection

@section('author')
Xenren
@endsection

@section('url')
    https://www.xenren.co/inbox/messages
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
{{--    <link href="{{asset('assets/global/plugins/lightbox2/dist/css/lightbox.min.css')}}" rel="stylesheet" type="text/css"/>--}}
{{--    <link href="/custom/css/frontend/inbox.css" rel="stylesheet" type="text/css" />--}}
    <link href="{{asset('css/minifiedInbox.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/minifiedInboxMessage.css')}}" rel="stylesheet" type="text/css" />
{{--    <link href="/custom/css/frontend/InboxMessages.css" rel="stylesheet" type="text/css">--}}
{{--    <link href="/custom/css/frontend/modalMakeOffer.css" rel="stylesheet" type="text/css"/>--}}
@endsection

@section('content')
    <script src="{{asset('js/vue.min.js')}}"></script>
    <script src="{{asset('js/socket.io.js')}}"></script>
    <script>
	    console.log('console socket k lia');
	    console.log('{{$inbox->id}}');
        var socket = io('https://xenren.co:3000');
        new Vue({
	        el: '.page-content',
	        data: {

	        },
	        methods: {},
	        mounted: function (e) {
		        /**
		         * Redis Listener Example....!
		         * */
		        socket.emit('global-channel:private-message');
		        that = this;
		        socket.on('global-channel:private-message', function (data) {
					        console.log('socket c data aa gaya ha');
					        console.log({{$inbox->id}});
					        fetchInboxMessages('{{$inbox->id}}')
		        });
	        }
        });

        function fetchInboxMessages(inboxId) {
            var form = new FormData();
            form.append("message", $('#message').val());

            var settings = {
                "url": "/privateChat?channel=private_chat&inobxId="+inboxId,
                "method": "GET",
                "data": form,
                "processData": false,
                "contentType": false,
                "mimeType": "multipart/form-data",
            };

            $.ajax(settings).done(function (response) {
                $('.inbox-message-section').html(response);
            });
        }
    </script>
    <div class="row setRowIMNFCSSMessage">
    <div class="col-md-3 search-title-container">
        <span class="find-experts-search-title text-uppercase setMAinMENU">{{ trans('common.main_action') }}</span>
    </div>
    <div class="col-md-9 right-top-main">
    	<div class="row ">
            <div class="col-md-12 top-tabs-menu setIBOXMPCol12">
                <div class="order-status-menu pull-right setINBOXMPOSPR">
                    <ul>
                        <li class="{{ $inbox->isNormalMessage()? 'active': '' }}">
                            <a href="{{ route('frontend.inbox', \Session::get('lang')) }}?category=1">
                                {{ trans('common.inbox') }}
                            </a>
                        </li>
                        <li class="{{ $inbox->isSystemMessage()? 'active': '' }}">
                            <a href="{{ route('frontend.inbox', \Session::get('lang')) }}?category=2">
                                {{ trans('common.system_message') }}
                            </a>
                        </li>
                        <li class="{{ $inbox->isTrash()? 'active': '' }}">
                            <a href="{{ route('frontend.inbox', \Session::get('lang')) }}?isTrash=1">
                                {{ trans('common.trash') }}
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="caption">
                    <span class="find-experts-search-title text-uppercase">{{ trans('common.inbox_messages') }}</span>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
	<div class="page-content">
	    <div class="tab-content">
	    	<div class="tab-pane active chat-details-section" id="inbox">
	        	<div class="row header-action">
	        		<div class="col-md-12">
	        		    @if( $inbox->isNormalMessage() )
	        		    <a href="{{ route('frontend.inbox',\Session::get('lang')) }}?category=1" class="back">
	        		    @elseif( $inbox->isSystemMessage() )
	        		    <a href="{{ route('frontend.inbox',\Session::get('lang')) }}?category=2" class="back">
	        		    @elseif( $inbox->isTrash() )
	        		    <a href="{{ route('frontend.inbox',\Session::get('lang')) }}?isTrash=1" class="back">
	        		    @endif
                        <a href="{{ URL::previous() }}" class="back" onclick="history.go(-1);">
	                   		<i class="fa fa-chevron-left" aria-hidden="true"></i>
	                   		<span>
	                   		    {{ trans('common.back_to_inbox') }}
                            </span>
	                   	</a>
	        		</div>
	        	</div>
	        	<div class="portlet box setPortletBoxMessage" id="inbox-{{ $inbox->id }}">
	        		<div class="portlet-body">
		                <div class="row header-section">
                            <div style="display: flex;justify-content: flex-end">
                                    <span style="color:#555;font-size:13px;font-weight:bold;padding-right:60px;">Your Average Response Time : {{$response_time}}
                                    </span>
                            </div>
                        </div>
                        <div class="row header-section">
		                	<div class="col-md-7">
		                		<h5>{{ $inbox->getSenderInfo()['name'] }}</h5>
		                		@if( isset($inbox->project) )
                                <h6>{{ $inbox->project->name }}</h6>
                                @endif
		                	</div>
		                	<div class="col-md-5 text-right setALL3BTNMP">
		                	    @if( isset($inbox->project) )
		                	    <a href="{{ route('frontend.joindiscussion.getproject', [ 'lang' => \Session::get('lang') ,'slug' => $inbox->project->name,'id' => $inbox->project->id ]) }}">
                                    <button class="btn-customs-all btn-green-custom margin-r-10 setJOBDETAILS">
                                        {{ trans('common.job_details') }}
                                    </button>
                                </a>
                                @endif

                                @if( $inbox->is_trash == 0 )
                                <a class="btn-delete" data-url="{{ route('frontend.inbox.trash') }}" data-inbox-id="{{ $inbox->id }}">
                                    <button class="btn-customs-all btn-default-custom setDELETEBTN">
                                        {{ trans('common.delete') }}
                                    </button>
                                </a>
                                @else
                                <a class="btn-undelete" data-url="{{ route('frontend.inbox.untrash') }}" data-inbox-id="{{ $inbox->id }}">
                                    <button class="btn-customs-all btn-default-custom setUNDELETEBTN">
                                        {{ trans('common.undelete') }}
                                    </button>
                                </a>
                                @endif
		                	</div>
		                </div>

		                <div class="row body-section inbox-message-section setROWBODYSECTION">
		                	@include('frontend.inbox.inboxMessageItem')
                            <input id="last-message-id" type="hidden" value="{{ $lastMessageID }}">
		                </div>

		                <div class="chat-form setMSGPAGECF">
		                    @if( $inbox->isNormalMessage() || $inbox->type == \App\Models\UserInbox::TYPE_PROJECT_ASK_FOR_CONTACT )
		                    {{--<form action="" method="post" class="">--}}
		                        <div class="row chat-form-main">
		                            <div class="col-md-1 chat-form-l">
		                                <div class="upload-picture upload-btn-custom setUPMPICONS">
		                                    <a href="javascript:;" id="uploadFileBtn">
		                                        <i class="fa fa-paperclip" aria-hidden="true"></i>
		                                    </a>
		                                </div>
                                        <div class="upload-btn-custom">
                                            <div class="dropup">
                                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
                                                    <i class="fa fa-cog" aria-hidden="true"></i>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li><a>Block User <input type="checkbox" name="checkbox1" class="checkbox1 make-switch" checked="checked" data-size="small" /></a></li>
                                                    <li><a>User's mail notification <input type="checkbox" name="checkbox2" class="checkbox2 make-switch" checked="checked" data-size="small" /></a></li>
                                                    <li><a>User's WeChat notification <input type="checkbox" name="checkbox3" class="checkbox3 make-switch" checked="checked" data-size="small" /></a></li>
                                                    <li><a href="#">Report for abuse</a></li>
                                                    {{--<li><a href="#">Comment</a></li>--}}
                                                </ul>
                                            </div>
                                        </div>
		                            </div>
		                            <div class="col-md-10 chat-form-r">
		                                <div class="input-cont setFORMICMP">
		                                    <input class="form-control tbxMessage" name="message" id="message" placeholder="" type="text">
		                                </div>
                                        <input type="file" id="fileUploadInputField" class="setINPUTFMSGP">
		                            </div>
		                            <div class="col-md-1 submit-btn-custom">
	                                    <button type="button" class="btn btn-submit btnSendMessage setBTNMP"
	                                        data-inbox-id="{{ $inbox->id }}"
                                                @php
                                                    $check = $inbox->getReceiver(true, Auth::guard('users')->user()->id);
                                                    $id = $check['id'];
                                                @endphp
                                                @if(isset($id))
                                                 data-receiver-id = "{{ $id }}"
                                                @endif
	                                        data-url="{{ route('frontend.inbox.sendmessage') }}">
	                                        <i class="fa fa-send" aria-hidden="true"></i>
	                                    </button>
		                            </div>
		                        </div>

		                    {{--</form>--}}
		                    @endif
		                </div>
		            </div>
		        </div>
	        </div>
	    </div>
	</div>
</div>
@endsection

@section('modal')
    <div id="modalMakeOffer" class="modal fade apply-job-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>

    <div id="modalAddFileChat" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
@endsection

@section('footer')
{{--    <script src="{{asset('assets/global/plugins/lightbox2/dist/js/lightbox.min.js')}}" type="text/javascript"></script>--}}
{{--    <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>--}}
    <script type="text/javascript">
        var lang = {
            'unable_to_request' : "{{ trans('common.unable_to_request') }}",
            'invalid_file_type_for_inbox_message' : "{{ trans('common.invalid_file_type_for_inbox_message') }}",
        };

        var url = {
            'get_new_inbox_message' : "{{ route('frontend.inbox.getnewinboxmessage.post') }}",
        };

        var inboxId = '{{ $inbox->id }}';
    </script>
    <script src="{{asset('js/minifiedInboxMessage.js')}}" type="text/javascript"></script>
{{--    <script src="/custom/js/frontend/inboxMessages.js?lm=220820171348" defer="defer" type="text/javascript"></script>--}}
    <script type="text/javascript" defer="defer">
        +function () {
            $(document).ready(function () {
                var _self = this;

                //handlers
                document.addEventListener('paste', function (e) {
                    _self.paste_auto(e);
                }, false);

                //on paste
                this.paste_auto = function (e) {
                    if (e.clipboardData) {
                        var items = e.clipboardData.items;
                        if (!items) return;

                        //access data directly
                        for (var i = 0; i < items.length; i++) {
                            if (items[i].type.indexOf("image") !== -1) {
                                //image
                                var blob = items[i].getAsFile();

                                var URLObj = window.URL || window.webkitURL;
                                var source = URLObj.createObjectURL(blob);

                                var f = document.createElement("form");
                                f.setAttribute('id',"upload-image-form");
                                f.setAttribute('method',"post");
                                f.setAttribute('enctype',"multipart/form-data");

                                var fd = new FormData();
                                var attrs = $("#upload-image-form").serialize().split("&");
                                for (i = 0; i < attrs.length - 1; i++) {
                                    var temp = attrs[i].split('=');
                                    fd.append(temp[0], temp[1]);
                                }
                                fd.append("inbox-id", "{{ $inbox != null? $inbox->id: 0 }}");
                                fd.append("receiver-id", "{{ $inbox->getReceiver(true, Auth::guard('users')->user()->id )['id'] }}");
                                fd.append('file', blob);
                                $.ajax({
                                    type: 'POST',
                                    url: '{{ route('frontend.inbox.chatimage.post')}}',
                                    data: fd,
                                    processData: false,
                                    contentType: false,
                                    error: function () {
                                        alert(lang.unable_to_request);
                                    },
                                    success: function (result) {
                                        if (result.status == 'success') {
                                            alertSuccess(result.msg);
                                            setTimeout(function () {
                                                location.reload();
                                            }, 1000);
                                        } else{
                                            alertError(result.msg);
                                        }
                                    }
                                });
                            }
                        }
                        e.preventDefault();
                    }
                };
            });
        }(jQuery);
    </script>
    <script defer="defer">
        $(function (){
            $('#uploadFileBtn').click(function (e){
                e.preventDefault();
                $('#fileUploadInputField').click();
            });
            $('#fileUploadInputField').on('change', function (e){
                upload();
            });
        });
        function upload() {
            var upload = document.getElementById('fileUploadInputField');
            var image = upload.files[0];
            if(image.type != 'image/png' && image.type != 'image/jpg' && image.type != 'image/gif' && image.type != 'image/jpeg' )
            {
                alert(lang.invalid_file_type_for_inbox_message);
                return false;
            }
            var form = new FormData();
            form.append('file', image);
            form.append('_token', '{{ csrf_token() }}');
            form.append('inbox-id', '{{ $inbox->id }}');
            form.append('receiver-id', '{{ $inbox->getReceiver(true, Auth::guard('users')->user()->id )['id'] }}');
            $.ajax({
                url:"/inbox/chatImage",
                type: "POST",
                data: form,
                contentType:false,
                cache: false,
                processData:false,
                success:function (msg) {
//                    console.log(msg);
                    if(msg.status == 'success') {
                        window.location.reload();
                    }
                },
                error: function (err) {
                    console.log(err);
                }
            });
        };
    </script>
@endsection
