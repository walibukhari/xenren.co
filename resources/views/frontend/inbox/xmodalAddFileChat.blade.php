@extends('frontend.includes.ajaxmodal')

@section('title')
    {{ trans('member.add_file') }}
@endsection

@section('script')
    <script>
        +function () {
            $(document).ready(function () {
                $('#add-file-chat').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit',
                    alertContainer : '#model-alert-container',
                    beforeFunction: function () {
                        var file = $('#upload-file')[0].files[0];
                        if(file.type != 'image/png' && file.type != 'image/jpg' && file.type != 'image/gif' && file.type != 'image/jpeg' )
                        {
                            alert(lang.invalid_file_type_for_inbox_message);
                            return false;
                        }
                        return true;
                    },
                    afterSuccessFunction: function (response, $el, $this) {
                        if( response.status == 'success'){
                            $('#modalAddFileChat').modal('toggle');
                            alertSuccess(response.msg);

                            setTimeout(function () {
                                location.reload();
                            }, 1000);
                        }else{
                            App.alert({
                                type: 'danger',
                                icon: 'warning',
                                message: response.msg,
                                container: '#model-alert-container',
                                reset: true,
                                focus: true
                            });
                        }

                        App.unblockUI('#add-file-chat');
                    }
                });

                $("#upload-file").trigger('click');
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <div style="margin-top:0 px;">
        <hr/>
    </div>
    <div>
        <button type="submit" class="btn btn-green-bg-white-text" id="btn-submit">
            {{ trans('member.confirm_add') }}
        </button>
        <button type="button" class="btn btn-white-bg-green-text" data-dismiss="modal">
            {{ trans('member.cancel') }}
        </button>
    </div>
@endsection

@section('content')
    {!! Form::open(['route' => 'frontend.inbox.chatimage.post', 'id' => 'add-file-chat', 'method' => 'post', 'class' => 'form-horizontal', 'files' => true]) !!}
        <input type="hidden" name="inbox-id" value="{{ $inbox_id }}">
        <input type="hidden" name="receiver-id" value="{{ $receiver_id }}">
        <div class="modal-body">
            <div class="form-body">
                <div class="modal-alert-container"></div>
                {{--<div class="form-group">--}}
                    {{--<label class="col-md-3 control-label">{{ trans('member.filename') }}</label>--}}
                    {{--<div class="col-md-9">--}}
                        {{--{!! Form::text('message', '', ['class' => 'form-control', 'placeholder' => trans('common.message')]) !!}--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="form-group">
                    <label class="col-md-3 control-label">{{ trans('member.file') }}</label>
                    <div class="col-md-9">
                        {!! Form::file('file', ['class' => 'form-control', 'id' => 'upload-file' ]) !!}
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection


