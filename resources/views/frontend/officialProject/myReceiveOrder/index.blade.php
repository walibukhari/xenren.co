@extends('frontend.layouts.default')

@section('title')
    Published Job Milestone
@endsection

@section('description')
    Published Job Milestone
@endsection

@section('author')
    Xenren
@endsection

@section('url')
    https://www.xenren.co/myReceiveOrder
@endsection

@section('images')
    https://www.xenren.co/images/facebook_thumbnail.png
@endsection

@section('header')
{{--    <link href="/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="/custom/css/frontend/userCenter.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="/custom/css/frontend/fixedPriceEscrowModule.css" rel="stylesheet">--}}
{{--    <link href="/custom/css/frontend/jobDetails.css" rel="stylesheet">--}}
{{--    <link href="/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />--}}
{{--    <link rel="stylesheet" href="{{asset('calender/public/styles/css/bundle.min.css')}}">--}}
    <link rel="stylesheet" href="{{asset('css/OfficalJobDetail.css')}}">

    <style>
        .desc {
            color: #333;
        }
        table {
            color: #7D7D7D;
        }
        .clrGreen {
            color: #57b029;
        }
        table thead tr {
            background: #F6F6F6;
        }
    </style>
@endsection
@section('modal')
    ])
    @include('frontend.modals.disputeModal',[
        'message' => 'Tell us what\'s happening',
    ])

    @include('frontend.modals.modal',[
        'message' => 'Are you sure you want to refund?',
        'id' => 'refundModal',
        'successClass' => 'confirmRefundMilestoneBtn'
    ])

    @include('frontend.modals.modal',[
        'message' => 'Are you sure you want to refund?',
        'id' => 'refundRequestFromEmployerModal',
        'successClass' => 'confirmRequestFromEmployerMilestoneBtn',
        'reason' => true
    ])

    @include('frontend.modals.modal',[
        'message' => 'Are you sure you want to refund?',
        'id' => 'rejectRefundFreelancer',
        'successClass' => 'confirmRejectRefundFreelancer',
        'commentId' => 'rejectRefundFreelancerComment'
    ])

@endsection
@section('content')

    <div class="page-content-wrapper setPageContentWrapperMROPAGE">
        <div class="col-md-12 usercenter link-div">
            <a href="#" class="text-uppercase raleway">
	        <span class="fa fa-angle-left">
	        </span>
               {{trans('common.back_to_my_orders')}}
            </a>
        </div>
        <div class="clearfix"></div>
        <br>
			<?php
			$data = isset($project) ?$project : false;
			$projectId = isset($data->id) ?$data->id : 0;
			$applicantId = isset($data->awardedApplicant->id) ? $data->awardedApplicant->id : 0;
			$languages = isset($data->awardedApplicant->user->languages) ? $data->awardedApplicant->user->languages : false;
			?>
        <script>
					window.ProjectId = '{{$projectId}}';
					window.token = '{{csrf_token()}}';
        </script>

        <div class="col-md-12">
            <div class="row main-div">

                <div id="sec-official-project">
                    <div class="portlet-title">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="">
                                    <div class="title text-center f-s-14 f-c-green text-uppercase">
                                        {{ trans('common.check_details') }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-2 set-Row-Portlet-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <img src="{{ $project->officialProject->getIcon() }}" width="150px" height="150px">
                                    </div>
                                    <div class="col-md-12 setdesc-div">
                                        <label>{{ trans('common.budget') }}</label><br/>
                                        <div class="price set-price"
                                        >${{ $project->officialProject->stock_total }}</div>
                                        <span class="setSpN-share-amount">{{ trans('common.share_amount') }}: {{ $project->officialProject->stock_share }}%</span>
                                    </div>
                                    <br>
                                    <div class="col-md-12 btn-div set-btn-div">
                                        <a href="#">
                                            <button class="btn btn-color-custom uppercase set-btnbtn">
                                                {!! trans('common.download_basic_agreement') !!}
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10 official-pro-right">
                                <div class="portlet-title">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-span-12">
                                                <div class="f-c-green title-green-c pull-left raleway f-s-24 f-m">
                                                    <img src="/images/badge.png" class="set-f-c-green">
                                                    <span class="set-title-name">{{ $project->officialProject->title }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 price-div top-info f-c-gray setf-c-gray-price-div set-price-div">
                                            <div class="row p-t-10">
                                                <div class="col-md-4">
                                                    <span>{{ trans('common.project_type') }}:
                                                        <span class="f-b">
                                                            {{ trans('common.official_project') }}
                                                        </span>
                                                    </span>
                                                </div>
                                                <div class="col-md-4">
                                                    <span>{{ trans('common.time_frame') }}:
                                                        <span class="f-b">
                                                            {{ $project->officialProject->getTimeFrame() }} days
                                                        </span>
                                                    </span>
                                                </div>
                                                <div class="col-md-4">
                                                    <span>{{ trans('common.end_date') }}:
                                                        <span class="f-b">
                                                            {{ $project->officialProject->getRecruitEndDate() }}
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="row p-tb-15">
                                                <div class="col-md-4">
                                                        <span>{{ trans('common.budget') }}:
                                                            <span class="f-b">
                                                                {{ $project->officialProject->getBudgetRange() }}
                                                            </span>
                                                        </span>
                                                </div>

                                                <div class="col-md-4">
                                                    <span>{{ trans('common.funding_stage') }}:
                                                        <span class="f-b">
                                                            {{ \App\Constants::translateFundingStage($project->officialProject->funding_stage) }}
                                                        </span>
                                                    </span>
                                                </div>
                                                <div class="col-md-4">
                                                    <span>{{ trans('common.location') }}:
                                                        <span class="f-b">
                                                            {{ $project->officialProject->getLocationLngLngWithDefault() }}
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="col-md-12 description set-description">
                                            {{ $project->officialProject->description }}
                                        </div>
                                    </div>

                                    <div class="row set-RoW-area">
                                        <div class="col-md-3 ">
                                            <img src="/images/signal.png">
                                            <div class="label">
                                                <span class="uppercase set-spn">
                                                    {{ trans('common.difficulty') }}
                                                </span>
                                            </div>
                                            <div class="difficulty-star p-t-10 set-difficulty-star">
                                                @for ($i = 0; $i < $project->officialProject->project_level; $i++)
                                                    <i class="fa fa-star"></i>
                                                @endfor

                                                @for ($i = 0; $i < ( 6 - $project->officialProject->project_level ); $i++)
                                                    <i class="fa fa-star-o"></i>
                                                @endfor
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <img src="/images/user-secret.png">
                                            <div class="label">
                                                <span class="uppercase set-spn">
                                                    {{ trans('common.position_needed') }}
                                                </span>
                                            </div>
                                            <div class="position p-t-10">
                                                <span class="f-c-green f-sb">
                                                @foreach( $project->officialProject->projectPositions as $key => $projectPosition)
                                                        @if( $key != 0 )
                                                            {{ ", " }}
                                                        @endif

                                                        {{  $projectPosition->jobPosition->getName() }}
                                                    @endforeach
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <img src="/images/hands.png">
                                            <div class="label">
                                                    <span class="uppercase set-spn">
                                                        {{ trans('common.skill_request') }}
                                                    </span>
                                            </div>
                                            <div class="setScrollDiv">
                                                <div class="scroll-div">
                                                    <div class="skill-div skill-main set-skill-div">
                                                        @foreach( $project->projectSkills as $projectSkill)
                                                            <span class="item-skill">
                                                                <span>{{ $projectSkill->getSkillName( $projectSkill->skill_id ) }}</span>
                                                            </span>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row p-t-30 set-Row-PT">
                                        <div class="col-md-12">
                                            <img src="/images/info.png">
                                            <div class="label">
                                            <span class="uppercase set-spn">
                                                {{ trans('common.project_info') }}
                                            </span>
                                            </div>
                                        </div>
                                        <div class="col-md-12  f-c-gray p-t-10 row">
                                            <div class="col-md-5">
                                                <span class="uppercase">{{ trans('common.project_status') }}</span>: <span
                                                        class="f-sb">{{  $project->officialProject->explainStatus()  }}</span>
                                            </div>
                                            <div class="col-md-3">
                                                <span class="uppercase">{{ trans('common.project_stage') }}</span>: <span
                                                        class="f-sb">
                                                    {{  $project->officialProject->getfundingStage()  }}
                                                </span>
                                            </div>
                                            {{--<div class="col-md-3 ">--}}
                                            {{--<span class="uppercase">{{ trans('common.project_value') }}</span>: <span--}}
                                            {{--class="f-sb">{{  $project->officialProject->getfundingStage()  }}</span>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-md-3">--}}
                                            {{--<span class="uppercase">{{ trans('common.attandance') }}</span>: <span class="f-sb">32</span>--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                {{--<div class="col-md-12 prising-section text-center">--}}
                {{--<div class="row">--}}
                {{--<div class="col-md-4 prising-section-sub">--}}
                {{--<img src="/images/icons/doller-icon.png"><br>--}}
                {{--<p class="setSP0">{{trans('common.budget_for_the_project')}}</p>--}}
                {{--<span class="setSP">${{$data->reference_price}} USD</span>--}}
                {{--</div>--}}
                {{--<div class="col-md-4 prising-section-sub">--}}
                {{--<img src="/images/icons/time-icon.png"><br>--}}
                {{--<p class="setSP0">{{trans('common.current_milestone')}}</p>--}}
                {{--<span class="setSP">$700USD</span>--}}
                {{--</div>--}}
                {{--@if($data->status != \App\Models\Project::STATUS_COMPLETED)--}}
                {{--<div class="col-md-4 prising-section-sub">--}}
                {{--<img src="/images/icons/doller-icon.png"><br>--}}
                {{--<p class="setSP0">{{$data->pay_type != 1 ? trans('common.fixed_price') : trans('common.hourly_price') }}</p>--}}
                {{--</div>--}}
                {{--@endif--}}
                {{--</div>--}}
                {{--</div>--}}

                <div class="col-md-12 description setDEscriptionMORP">
                    <h4>{{$data->name}}</h4>
                <!-- <h5>{!!$data->description!!}</h5> -->
                    <p class="setPppP">{{trans('common.description')}}</p>
                    <br>
                    <div class="setDESC">
                        <div class="scroll-div">
                            <p class="setDYND">
                                {!!$data->description!!}
                            </p>
                        </div>
                    </div>
                </div>
                @if($data->status != \App\Models\Project::STATUS_COMPLETED)
                    <div class="col-md-12 milestone-desc">
                        <div  class="setULNAVTBS">
                            <ul class="nav nav-tabs" role="tablist">

                                <li role="presentation" class="active text-center">
                                    <a href="#logWorkingHour" class="setLI" aria-controls="milestone" role="tab" data-toggle="tab">{{trans('common.time_log')}}</a>
                                </li>

                                {{--<li role="presentation" class="text-center">--}}
                                {{--<div class="vr">&nbsp;</div>--}}
                                {{--<a href="#disputeAndRefund" class="setLI" aria-controls="dispute" role="tab" data-toggle="tab">Dispute & Refund</a>--}}
                                {{--</li>--}}

                                <li role="presentation" class="text-center">
                                    <div class="vr">&nbsp;</div>
                                    <a href="#timeSheet" class="setLI" aria-controls="contract-control-panel" role="tab" data-toggle="tab">{{trans('common.time_sheet')}}</a>
                                </li>

                                {{--<li role="presentation" class="text-center">--}}
                                {{--<div class="vr">&nbsp;</div>--}}
                                {{--<a href="#hourlyRate" class="setLI" aria-controls="end-contract" role="tab" data-toggle="tab">Hourly Rate</a>--}}
                                {{--</li>--}}

                                <li role="presentation" class="text-center">
                                    <div class="vr">&nbsp;</div>
                                    <a href="#workingLimit" class="setLI" aria-controls="end-contract" role="tab" data-toggle="tab">{{trans('common.working_hour')}}</a>
                                </li>

                                {{--<li role="presentation" class="text-center">--}}
                                {{--<div class="vr">&nbsp;</div>--}}
                                {{--<a href="#contractControlPanel" class="setLI" aria-controls="end-contract" role="tab" data-toggle="tab">Contract Control Panel</a>--}}
                                {{--</li>--}}

                                <li role="presentation" class="text-center">
                                    <div class="vr">&nbsp;</div>
                                    <a href="#endContractTab" class="setLI" aria-controls="end-contract" role="tab" data-toggle="tab">{{trans('common.end_contract')}}</a>
                                    {{--<div class="setvr">&nbsp;</div>--}}
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content tabpanel-custom setROWNEWMILSTONE">
                            <div role="tabpanel" class="tab-pane active" id="logWorkingHour">
                                @include('frontend.officialProject.myReceiveOrder.partials.logWorkingHours')
                            </div>
                            <div role="tabpanel" class="tab-pane" id="disputeAndRefund">
                                @include('frontend.officialProject.myReceiveOrder.partials.disputeAndRefund')
                            </div>
                            <div role="tabpanel" class="tab-pane" id="timeSheet">
                                @include('frontend.officialProject.myReceiveOrder.partials.timeSheet')
                            </div>
                            <div role="tabpanel" class="tab-pane" id="hourlyRate">
                                @include('frontend.officialProject.myReceiveOrder.partials.hourlyRate')
                            </div>
                            <div role="tabpanel" class="tab-pane" id="workingLimit">
                                @include('frontend.officialProject.myReceiveOrder.partials.workingLimit')
                            </div>
                            <div role="tabpanel" class="tab-pane" id="contractControlPanel">
                                @include('frontend.officialProject.myReceiveOrder.partials.contractControlPanel')
                            </div>
                            <div role="tabpanel" class="tab-pane" id="endContractTab">
                                @include('frontend.officialProject.myReceiveOrder.partials.endContract')
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{asset('js/minifiedOfficalJobDetail.js')}}"></script>
{{--    <script src="/js/moment.min.js"></script>--}}
{{--    <script src="/js/bootstrap-datepicker.min.js" type="text/javascript"></script>--}}
{{--    <link rel="stylesheet" href="https://uxsolutions.github.io/bootstrap-datepicker/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">--}}
{{--        <script src="{{asset('custom/js/frontend/orderDetail.js')}}" type="text/javascript"></script>--}}
{{--        <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>--}}
{{--    <script src="{{asset('js/global.js')}}" type="text/javascript"></script>--}}
{{--    <script src="{{asset('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}" type="text/javascript"></script>--}}


    <script>
			window.total = '{{isset($total) ? $total : 0}}'
			window.applicantId = '{{$applicantId}}';
    </script>
    <script>
			setTimeout(function(){
				$('#tt').html('${{isset($total) ? $total : 0}}');
				$('#md').html('${{isset($md) ? $md : 0}}');
				$('#msd').html('${{isset($msd) ? $msd : 0}}');
			}, 100);
    </script>
    <script>
			$('.scroll-div').slimScroll({});
    </script>
    <script>
			var totalhours = "{{$totalTimeLoggedHours}}";
			var totalearning = "{{$totalEarning}}";
    </script>
    <script type="text/javascript">
			$(function () {
				window.selectedWeek = [];
				window.isChange = true;
				var weekpicker, start_date, end_date;
				weekpicker = $('#datetimepicker12').datepicker();
                weekpicker.on('changeDate', function (e) {
					if(window.isChange) {
						window.isChange =false;
						set_week_picker(e.date);
					}
				});
                weekpicker.on('changeMonth', function (e) {
                    $('.headerCalMonth').html(moment(e.date, 'M').format('MMMM Y'))
                });
				function set_week_picker(date) {
					var datas = [
						new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 7),
						new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6),
						new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 5),
						new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 4),
						new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 3),
						new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 2),
						new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 1),
					];
					var datas1 = `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate() - date.getDay() + 1}`;
					window.selectedWeek = datas1;
					weekpicker.datepicker('setDates', datas)
					window.isChange = true;
				}
				$('#applyFilter').on('click', function(){
					window.location = `?date=${window.selectedWeek}#timeSheet`;
				});
				var filter = '{!! $filters !!}'
				if(filter) {
                    console.log('{!! $date1 !!}');
                    set_week_picker(new Date('{!! $date1 !!}'))
				}

				$('.incrementMonth').on('click', function () {
                    var d = $('#datetimepicker12').data('datepicker').getFormattedDate('yyyy-mm-dd');
                    if(d) {
                        var date = d.split(',')[0]
                        var sdate = date.split('-');
                        var year = sdate[0];
                        var month = sdate[1];
                        window.isChange = false;
                        weekpicker.datepicker('setDates', new Date(year,month,'01'))
                    } else {
                        var month = moment().format('M');
                        var year = moment().format('YYYY');
                        window.isChange = false;
                        weekpicker.datepicker('setDates', new Date(year,month,'01'))
                    }
                    var d = $('#datetimepicker12').data('datepicker').getFormattedDate('yyyy-mm-dd');
                    $('.headerCalMonth').html(moment(d).format('MMMM Y'))
                    // weekpicker.val("").datepicker("update");
                    setTimeout(function () {
                        window.isChange = true;
                    },100);
                });

				$('.decrementMonth').on('click', function () {
                    var d = $('#datetimepicker12').data('datepicker').getFormattedDate('yyyy-mm-dd');
                    if(d) {
                        var date = d.split(',')[0]
                        var sdate = date.split('-');
                        var year = sdate[0];
                        var month = sdate[1] - 2;
                        var monthM = sdate[1];
                        window.isChange = false;
                        weekpicker.datepicker('setDates', new Date(year,month,'01'))
                    } else {
                        var month = moment().format('M') - 2;
                        var monthM = moment().format('M');
                        var year = moment().format('YYYY');
                        window.isChange = false;
                        weekpicker.datepicker('setDates', new Date(year,monthM,'01'))
                    }
                    var d = weekpicker.data('datepicker').getFormattedDate('yyyy-mm-dd');
                    $('.headerCalMonth').html(moment(d).format('MMMM Y'))
                    // weekpicker.val("").datepicker("update");
                    setTimeout(function () {
                        window.isChange = true;
                    },100);
                })


			});
    </script>
    {{--<script src="{{asset('calender/public/scripts/calendar.js')}}"></script>--}}
    <script>
			//        let calendar = new Calendar('calendar-wrap')
    </script>

    <script>
			$('.set-btn-toggle').click(function() {
				$(this).find('.set-btn').toggleClass('active');

				if ($(this).find('.btn-primary').size()>0) {
					$(this).find('.set-btn').toggleClass('btn-primary');
				}
				if ($(this).find('.btn-danger').size()>0) {
					$(this).find('.set-btn').toggleClass('btn-danger');
				}
				if ($(this).find('.btn-success').size()>0) {
					$(this).find('.set-btn').toggleClass('btn-success');
				}
				if ($(this).find('.btn-info').size()>0) {
					$(this).find('.set-btn').toggleClass('btn-info');
				}
				$(this).find('.set-btn').toggleClass('btn-default');
			});

			$('form').submit(function(){
				return false;
			});

			$('.set-btn-toggle-2').click(function() {
				$(this).find('.set-btn-2').toggleClass('active');

				if ($(this).find('.btn-primary').size()>0) {
					$(this).find('.set-btn-2').toggleClass('btn-primary');
				}
				if ($(this).find('.btn-danger').size()>0) {
					$(this).find('.set-btn-2').toggleClass('btn-danger');
				}
				if ($(this).find('.btn-success').size()>0) {
					$(this).find('.set-btn-2').toggleClass('btn-success');
				}
				if ($(this).find('.btn-info').size()>0) {
					$(this).find('.set-btn-2').toggleClass('btn-info');
				}
				$(this).find('.set-btn-2').toggleClass('btn-default');
			});

			$('form').submit(function(){
				return false;
			});

			//
			//        $('#applyFilter').bind('click',function(){
			//        	var ca = new Calendar('calendar-wrap')
			//            var mn = ca.getSelectedDate();
			//        });
    </script>

@endsection
