<p class="setPPpPpP">View Time Sheet</p>
<div class="">
    <div class="row new-milestone-add setDIVCOl12W">
        <div class="col-md-12" style="color: black">
            <div class="row">
                <div class="row setrowmarginoff">
                    <div id="calendar-wrap" class="setCalenderWrap">
                        <div id="calendar" class="calendar">
                            <header class="calendar__head">
                                <div class="calendar__nav">
                                    <div id="calendar-left-btn" class="calendar__btn">
                                        <i class="fa fa-angle-left setFa decrementMonth"></i>
                                    </div>
                                    <div class="calendar__head-text">
                                        <span id="calendar-month" class="calender-header-text-month headerCalMonth">{{\Carbon\Carbon::now()->format('M Y')}}</span>
                                    </div>
                                    <div id="calendar-right-btn" class="calendar__btn">
                                        <i class="fa fa-angle-right setFa incrementMonth"></i>
                                    </div>
                                </div>
                            </header>
                            <div class="row">
                                <div class="col-md-4 col-sm-12 col-xs-12 set-main-row-col-4calender">
                                    <div style="overflow:hidden;">
                                        <div id="datetimepicker12"></div>
                                        <div class="setbtnApply">
                                            <button type="button" class="btn btn-sm btn-block" id="applyFilter" style="" >{{trans('common.apply')}}</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="vr01">&nbsp;</div>
                                <!--<div class="clear"></div>-->
                                <div class="col-md-4 col-sm-12 col-xs-12 setcolmd4">
                                    <span class="setSPNCOLOR">{{trans('common.status')}}</span>
                                    <div class="tbleWidth">
                                        <table class="table table-borderless setTBLE">
                                            <thead>
                                            <tr>
                                                <th scope="col" class="setTh-1">{{trans('common.week')}}</th>
                                                <th scope="col" class="setTh-2">{{trans('common.hour_work')}}</th>
                                                <th scope="col" class="setTh-3">{{trans('common.earn')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr @if($weekOfMonth == 1) class="setTRLast" @endif>
                                                <td>1st {{trans('common.week')}}</td>
                                                <td>{{$firstWeekHours}} {{trans('common.hours')}}</td>
                                                <td>{{$firstWeekEarning}}</td>
                                            </tr>
                                            <tr @if($weekOfMonth == 2) class="setTRLast" @endif>
                                                <td>2nd {{trans('common.week')}}</td>
                                                <td>{{$secondWeekHours}} {{trans('common.hours')}}</td>
                                                <td>{{$secondWeekEarning}}</td>
                                            </tr>
                                            <tr @if($weekOfMonth== 3) class="setTRLast" @endif>
                                                <td>3rd {{trans('common.week')}}</td>
                                                <td>{{$thirdWeekHours}} {{trans('common.hours')}}</td>
                                                <td>{{$thirdWeekEarning}}</td>
                                            </tr>
                                            <tr @if($weekOfMonth == 4) class="setTRLast" @endif>
                                                <td>4th {{trans('common.week')}}</td>
                                                <td>{{$fourthWeekHours}} {{trans('common.hours')}}</td>
                                                <td>{{$fourthWeekEarning}}</td>
                                            </tr>
                                            <tr @if($weekOfMonth == 5) class="setTRLast" @endif>
                                                <td>5th {{trans('common.week')}}</td>
                                                <td>{{$fifthWeekHours}} {{trans('common.hours')}}</td>
                                                <td>{{$fifthWeekEarning}}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-12 col-xs-12 set-md-col">
                                    <div class="box">
                                        <div class="row">
                                            <span class="setThisMoth">{{trans('common.this_month')}}</span>
                                            <hr class="setHRHRHR">
                                        </div>
                                        <div class="setTotalHours">
                                            <p class="setTotalHOUR">{{trans('common.total_hours')}}</p>
                                            <span>{{$totalHours}} {{trans('common.hours')}}</span>
                                        </div>
                                        <div class="totalEarning">
                                            <p class="setTOTALEarning">{{trans('common.total_earn_shares')}}</p>
                                            <span>{{$totalEarning}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row setroow" style="margin-bottom: 16px">
                    <div class="col-md-12">
                        <div class="col-md-4 col-sm-12 col-xs-12 setSetRowP1 pull-left text-left">{{trans('common.weekly_screen_shot')}}: &nbsp; <span class="setSPNCOLOR0">
                                {{\App\Models\ImageModel::whereHas('trackerDetail', function($q) use($project){
                                    $q->where('project_id', '=', $project->id);
                                    $q->where(function($q1){
                                        $q1->where('created_at', '>=', \Carbon\Carbon::now()->startOfWeek());
                                        $q1->where('created_at', '<=', \Carbon\Carbon::now()->endOfWeek());
                                    });
                                })->count()}}
                            </span>
                        </div>
                        {{--<div class="col-md-4 col-sm-12 col-xs-12 setSetRowP2 text-center">Events: &nbsp; <span class="setSPNCOLOR0">3400 Keyboards | </span><span class="setSPNCOLOR0">400 Mouse</span></div>--}}
                        <div class="col-md-4 col-sm-12 col-xs-12 setSetRowP3 pull-right text-right">{{trans('common.weekly_logged')}}: &nbsp; <span class="setSPNCOLOR0">
                                @if($weekOfMonth == 1)
                                    {{$firstWeekHours}} {{trans('common.hours')}}
                                @endif
                                @if($weekOfMonth == 2)
                                    {{$secondWeekHours}} {{trans('common.hours')}}
                                @endif
                                @if($weekOfMonth== 3)
                                    {{$thirdWeekHours}} {{trans('common.hours')}}
                                @endif
                                @if($weekOfMonth == 4)
                                    {{$fourthWeekHours}} {{trans('common.hours')}}
                                @endif
                                @if($weekOfMonth == 5)
                                    {{$fifthWeekHours}} {{trans('common.hours')}}
                                @endif
                            </span>
                        </div>
                    </div>
                </div>

                <div class="row set-row-section">
                    <div class="col-md-4 col-sm-12 col-xs-12 pull-left text-left set-col-md-date">
                        <span>{{$date->startOfWeek()->format('D d-M')}}</span> - <span>{{$date->endOfWeek()->format('D d-M')}}</span>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 text-center">
                        <div class="btn-group set-btn-toggle">
                            <button class="set-btn btn-lg btn-default">12-{{trans('common.hours')}}</button>
                            <button class="set-btn btn-lg btn-primary active">24-{{trans('common.hours')}}</button>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12 pull-right text-right set-row-btn-toggle-2">
                        <div class="btn-group set-btn-toggle-2">
                            <button class="set-btn-2 btn-lg btn-default">{{trans('common.my_local_time')}}</button>
                            <button class="set-btn-2 btn-lg btn-primary active">{{trans('common.client_time')}}</button>
                        </div>
                    </div>
                </div>
                <div class="row set-tb-pills-row">
                    <ul class="pil nav nav-pills set-nav-pills">
                        @php
                            $currentDay = \Illuminate\Support\Carbon::today()->dayOfWeek;
                        @endphp
                        <li @if($currentDay == 1 ) class="active" @endif><a data-toggle="pill" href="#menu1">{{$date->startOfWeek()->addDay(0)->format('D d')}}</a></li>
                        <li @if($currentDay == 2 ) class="active" @endif><a data-toggle="pill" href="#menu2">{{$date->startOfWeek()->addDay(1)->format('D d')}}</a></li>
                        <li @if($currentDay == 3 ) class="active" @endif><a data-toggle="pill" href="#menu3">{{$date->startOfWeek()->addDay(2)->format('D d')}}</a></li>
                        <li @if($currentDay == 4 ) class="active" @endif><a data-toggle="pill" href="#menu4">{{$date->startOfWeek()->addDay(3)->format('D d')}}</a></li>
                        <li @if($currentDay == 5 ) class="active" @endif><a data-toggle="pill" href="#menu5">{{$date->startOfWeek()->addDay(4)->format('D d')}}</a></li>
                        <li @if($currentDay == 6 ) class="active" @endif><a data-toggle="pill" href="#menu6">{{$date->startOfWeek()->addDay(5)->format('D d')}}</a></li>
                        <li @if($currentDay == 7 ) class="active" @endif><a data-toggle="pill" href="#menu7">{{$date->startOfWeek()->addDay(6)->format('D d')}}</a></li>
                    </ul>
                </div>

                <div class="tab-content">
                    @php
                        $currentDay = \Illuminate\Support\Carbon::today()->dayOfWeek;
                    @endphp
                    <div id="menu1" class="tab-pane fade @if($currentDay == 1 ) in active @endif ">
                        @php
                            $day = 0;
                            $dateToday = $date->startOfWeek()->addDay($day)->format('D d M');
                            $day1Data = collect();
                            $totalTimeLogged = 0;
                            foreach ($recordedTime as $k => $v) {
                                if($k == $date->startOfWeek()->addDay($day)->toDateString()) {
                                    $day1Data = $v;
                                }
                            }
                            foreach($day1Data as $k => $v) {
                                $startTime = isset($v->start_time)  ? Carbon\Carbon::parse($v->start_time) : Carbon\Carbon::now();
                                $endTime = isset($v->end_time)  ? Carbon\Carbon::parse($v->end_time) : Carbon\Carbon::now();
                                $totalTimeDiff = $endTime->diffInSeconds($startTime);
                                $totalTimeLogged += $totalTimeDiff;
                            }
                            $len = $day1Data->count();
                            if($len > 0) {
                                $hours  = gmdate('H:i', $totalTimeLogged);
                                $exploded = explode(':', $hours);
                                $hours = $exploded[0].'h ' . $exploded[1].'m';
                                $minutes  = ceil($totalTimeLogged/60);
                                $totalDiff  = gmdate('H:i', $totalTimeLogged);
                            } else {
                                $hours  = '00h 00m';
                                $minutes  = 00;
                                $totalDiff  = '0H0m';
                            }
                        @endphp
                        <div class="row" style="padding: 10px">
                            <div class="col-md-12 set-col-md-12-box2 text-center">
                                {{--<h3>SUNDAY 03, FEBRUARY</h3>--}}
                                <h3>
                                    {{$dateToday}}
                                </h3>
                            </div>
                            <div class="col-md-12 set-col-md-12-box3 text-center">
                                <h1>{{$hours}}</h1>
                            </div>
                            <div class="col-md-12 set-col-md-12-box4 text-center">
                                <div class="row set-para">
                                    <div class="col-md-4 set-row-col-6-fa-circle-1">
                                        <i class="fa fa-circle set-fa-circle1"></i>
                                        <span class="set-span-1">
                                               {{trans('common.track_time')}}:
                                                <span class="set-Hours">&nbsp;{{$totalDiff}} {{trans('common.hours')}}</span>
                                            </span>
                                    </div>

                                    <div class="col-md-3">
                                        <i class="fa fa-circle set-fa-circle2"></i>
                                        <span class="set-span-2">
                                                {{trans('common.manual')}}:
                                                <span class="set-Hours">0 {{trans('common.hours')}}</span>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row set-col-row-screen-shots text-center">
                            <span class="set-spn-screen-shots">{{trans('common.Screen_shot')}}</span>
                        </div>

                        <div class="row set-row-box-last">
                            <div class="col-md-12 set-col-md-12-box-last">
                                @foreach($day1Data as $k1 => $v1)
                                    @php
                                        $pictures = $v1->pictures->toArray();
                                    @endphp
                                    @foreach(array_chunk($pictures , 4) as $chunk)
                                        <div class="col-md-12 set-col-md-12-box-last-2">
                                            @foreach($chunk as $k => $v)
                                                <div class="col-md-3 col-sm-3 col-xs-3 set-col-md-3">

                                                    <div class="row set-col-md-2-box-last" style="margin: 0; margin-bottom: 3px">
                                                        <span>{{$v1['memo']}}</span>
                                                    </div>
                                                    <div class="row set-row-imge-screen-shoots">
                                                        <a href="{{$v['text']}}" data-lightbox="image-1">
                                                            <img class="set-Image-screen-shoot" src="{{$v['text']}}" alt="avatar">
                                                        </a>
                                                        <div class="col-md-12 setCol-md-12timeDate">
                                                            <div class="col-md-10 col-sm-10 col-xs-10 set-col-timeDate">
                                                                <span class="set-time-screen-shot">
                                                {{\Carbon\Carbon::parse($v['created_at'])->subMinute(5)->toTimeString()}} - {{\Carbon\Carbon::parse($v['created_at'])->toTimeString()}}
                                                </span>
                                                            </div>
                                                            <div class="col-md-2 set-delete-icon-row">
                                                                <span class="set-delete-icon delete_time" data-id="{{$v['id']}}" style="cursor:pointer;"><i class="fa fa-trash" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="battery" style="top: 16%">
                                                            @php
                                                                $total = 5;
                                                                $inactive = isset($v['activity_level']) && $v['activity_level'] >=0 ? $v['activity_level'] : 5;
                                                                $active = $total - $inactive;
                                                            @endphp

                                                            @for($i=0; $i<$inactive; $i++)
                                                                <div class="battery-charge-cell"></div>
                                                            @endfor
                                                            @for($z=0; $z<$active; $z++)
                                                                <div class="battery-charge-cell active-battery"></div>
                                                            @endfor
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div id="menu2" class="tab-pane fade @if($currentDay == 2 ) in active @endif ">
                        @php
                            $day = 1;
                            $dateToday = $date->startOfWeek()->addDay($day)->format('D d M');
                            $day1Data = collect();
                            $totalTimeLogged = 0;
                            foreach ($recordedTime as $k => $v) {
                                if($k == $date->startOfWeek()->addDay($day)->toDateString()) {
                                    $day1Data = $v;
                                }
                            }
                            foreach($day1Data as $k => $v) {
                                $startTime = isset($v->start_time)  ? Carbon\Carbon::parse($v->start_time) : Carbon\Carbon::now();
                                $endTime = isset($v->end_time)  ? Carbon\Carbon::parse($v->end_time) : Carbon\Carbon::now();
                                $totalTimeDiff = $endTime->diffInSeconds($startTime);
                                $totalTimeLogged += $totalTimeDiff;
                            }
                            $len = $day1Data->count();
                            if($len > 0) {
                                $hours  = gmdate('H:i', $totalTimeLogged);
                                $exploded = explode(':', $hours);
                                $hours = $exploded[0].'h ' . $exploded[1].'m';
                                $minutes  = ceil($totalTimeLogged/60);
                                $totalDiff  = gmdate('H:i', $totalTimeLogged);
                            } else {
                                $hours  = '00h 00m';
                                $minutes  = 00;
                                $totalDiff  = '0H0m';
                            }
                        @endphp
                        <div class="row" style="padding: 10px">
                            <div class="col-md-12 set-col-md-12-box2 text-center">
                                {{--<h3>SUNDAY 03, FEBRUARY</h3>--}}
                                <h3>
                                    {{$dateToday}}
                                </h3>
                            </div>
                            <div class="col-md-12 set-col-md-12-box3 text-center">
                                <h1>{{$hours}}</h1>
                            </div>
                            <div class="col-md-12 set-col-md-12-box4 text-center">
                                <div class="row set-para">
                                    <div class="col-md-4 set-row-col-6-fa-circle-1">
                                        <i class="fa fa-circle set-fa-circle1"></i>
                                        <span class="set-span-1">
                                                {{trans('common.track_time')}}:
                                                <span class="set-Hours">&nbsp;{{$totalDiff}} {{trans('common.hours')}}</span>
                                            </span>
                                    </div>

                                    <div class="col-md-3">
                                        <i class="fa fa-circle set-fa-circle2"></i>
                                        <span class="set-span-2">
                                                 {{trans('common.manual')}}:
                                                <span class="set-Hours">0 {{trans('common.hours')}}</span>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row set-col-row-screen-shots text-center">
                            <span class="set-spn-screen-shots">{{trans('common.Screen_shot')}}</span>
                        </div>

                        <div class="row set-row-box-last">
                            <div class="col-md-12 set-col-md-12-box-last">
                                @foreach($day1Data as $k1 => $v1)
                                    @php
                                        $pictures = $v1->pictures->toArray();
                                    @endphp
                                    @foreach(array_chunk($pictures , 4) as $chunk)
                                        <div class="col-md-12 set-col-md-12-box-last-2">
                                            @foreach($chunk as $k => $v)
                                                <div class="col-md-3 colsm-3 col-xs-3 set-col-md-3">

                                                    <div class="row set-col-md-2-box-last" style="margin: 0; margin-bottom: 3px">
                                                        <span>{{$v1['memo']}}</span>
                                                    </div>
                                                    <div class="row set-row-imge-screen-shoots">
                                                        <a href="{{$v['text']}}" data-lightbox="image-1">
                                                            <img class="set-Image-screen-shoot" src="{{$v['text']}}" alt="avatar">
                                                        </a>
                                                        <div class="col-md-12 setCol-md-12timeDate">
                                                            <div class="col-md-10 col-sm-10 col-xs-10 set-col-timeDate">
                                                                <span class="set-time-screen-shot">
                                                {{\Carbon\Carbon::parse($v['created_at'])->subMinute(5)->toTimeString()}} - {{\Carbon\Carbon::parse($v['created_at'])->toTimeString()}}
                                                </span>
                                                            </div>
                                                            <div class="col-md-2 set-delete-icon-row">
                                                                <span class="set-delete-icon"><i class="fa fa-trash" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="battery" style="top: 16%">
                                                            @php
                                                                $total = 5;
                                                                $inactive = isset($v['activity_level']) && $v['activity_level'] >=0 ? $v['activity_level'] : 5;
                                                                $active = $total - $inactive;
                                                            @endphp

                                                            @for($i=0; $i<$inactive; $i++)
                                                                <div class="battery-charge-cell"></div>
                                                            @endfor
                                                            @for($z=0; $z<$active; $z++)
                                                                <div class="battery-charge-cell active-battery"></div>
                                                            @endfor
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div id="menu3" class="tab-pane fade @if($currentDay == 3 ) in active @endif ">
                        @php
                            $day = 2;
                            $dateToday = $date->startOfWeek()->addDay($day)->format('D d M');
                            $day1Data = collect();
                            $totalTimeLogged = 0;
                            foreach ($recordedTime as $k => $v) {
                                if($k == $date->startOfWeek()->addDay($day)->toDateString()) {
                                    $day1Data = $v;
                                }
                            }
                            $len = $day1Data->count();
                            foreach($day1Data as $k => $v) {
                                $startTime = isset($v->start_time)  ? Carbon\Carbon::parse($v->start_time) : Carbon\Carbon::now();
                                $endTime = isset($v->end_time)  ? Carbon\Carbon::parse($v->end_time) : Carbon\Carbon::now();
                                $totalTimeDiff = $endTime->diffInSeconds($startTime);
                                $totalTimeLogged += $totalTimeDiff;
                            }
                            if($len > 0) {
                                $hours  = gmdate('H:i', $totalTimeLogged);
                                $exploded = explode(':', $hours);
                                $hours = $exploded[0].'h ' . $exploded[1].'m';
                                $minutes  = ceil($totalTimeLogged/60);
                                $totalDiff  = gmdate('H:i', $totalTimeLogged);
                            } else {
                                $hours  = '00h 00m';
                                $minutes  = 00;
                                $totalDiff  = '0H0m';
                            }
                        @endphp
                        <div class="row" style="padding: 10px">
                            <div class="col-md-12 set-col-md-12-box2 text-center">
                                {{--<h3>SUNDAY 03, FEBRUARY</h3>--}}
                                <h3>
                                    {{$dateToday}}
                                </h3>
                            </div>
                            <div class="col-md-12 set-col-md-12-box3 text-center">
                                <h1>{{$hours}}</h1>
                            </div>
                            <div class="col-md-12 set-col-md-12-box4 text-center">
                                <div class="row set-para">
                                    <div class="col-md-4 set-row-col-6-fa-circle-1">
                                        <i class="fa fa-circle set-fa-circle1"></i>
                                        <span class="set-span-1">
                                                {{trans('common.track_time')}}:
                                                <span class="set-Hours">&nbsp;{{$totalDiff}} {{trans('common.hours')}}</span>
                                            </span>
                                    </div>

                                    <div class="col-md-3">
                                        <i class="fa fa-circle set-fa-circle2"></i>
                                        <span class="set-span-2">
                                                {{trans('common.manual')}}:
                                                <span class="set-Hours">0 {{trans('common.hours')}}</span>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row set-col-row-screen-shots text-center">
                            <span class="set-spn-screen-shots">{{trans('common.Screen_shot')}}</span>
                        </div>

                        <div class="row set-row-box-last">
                            <div class="col-md-12 set-col-md-12-box-last">
                                @foreach($day1Data as $k1 => $v1)
                                    @php
                                        $pictures = $v1->pictures->toArray();
                                    @endphp
                                    @foreach(array_chunk($pictures , 4) as $chunk)
                                        <div class="col-md-12 set-col-md-12-box-last-2">
                                            @foreach($chunk as $k => $v)
                                                <div class="col-md-3 col-sm-3 col-xs-3 set-col-md-3">

                                                    <div class="row set-col-md-2-box-last" style="margin: 0; margin-bottom: 3px">
                                                        <span>{{$v1['memo']}}</span>
                                                    </div>
                                                    <div class="row set-row-imge-screen-shoots">
                                                        <a href="{{$v['text']}}" data-lightbox="image-1">
                                                            <img class="set-Image-screen-shoot" src="{{$v['text']}}" alt="avatar">
                                                        </a>
                                                        <div class="col-md-12 setCol-md-12timeDate">
                                                            <div class="col-md-10 col-sm-10 col-xs-10 set-col-timeDate">
                                                                <span class="set-time-screen-shot">
                                                {{\Carbon\Carbon::parse($v['created_at'])->subMinute(5)->toTimeString()}} - {{\Carbon\Carbon::parse($v['created_at'])->toTimeString()}}
                                                </span>
                                                            </div>
                                                            <div class="col-md-2 set-delete-icon-row">
                                                                <span class="set-delete-icon"><i class="fa fa-trash" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="battery" style="top: 16%">
                                                            @php
                                                                $total = 5;
                                                                $inactive = isset($v['activity_level']) && $v['activity_level'] >=0 ? $v['activity_level'] : 5;
                                                                $active = $total - $inactive;
                                                            @endphp

                                                            @for($i=0; $i<$inactive; $i++)
                                                                <div class="battery-charge-cell"></div>
                                                            @endfor
                                                            @for($z=0; $z<$active; $z++)
                                                                <div class="battery-charge-cell active-battery"></div>
                                                            @endfor
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div id="menu4" class="tab-pane fade @if($currentDay == 4 ) in active @endif ">
                        @php
                            $day = 3;
                            $dateToday = $date->startOfWeek()->addDay($day)->format('D d M');
                            $day1Data = collect();
                            $totalTimeLogged = 0;
                            foreach ($recordedTime as $k => $v) {
                                if($k == $date->startOfWeek()->addDay($day)->toDateString()) {
                                    $day1Data = $v;
                                }
                            }
                            foreach($day1Data as $k => $v) {
                                $startTime = isset($v->start_time)  ? Carbon\Carbon::parse($v->start_time) : Carbon\Carbon::now();
                                $endTime = isset($v->end_time)  ? Carbon\Carbon::parse($v->end_time) : Carbon\Carbon::now();
                                $totalTimeDiff = $endTime->diffInSeconds($startTime);
                                $totalTimeLogged += $totalTimeDiff;
                            }
                            $len = $day1Data->count();
                            if($len > 0) {
                                $hours  = gmdate('H:i', $totalTimeLogged);
                                $exploded = explode(':', $hours);
                                $hours = $exploded[0].'h ' . $exploded[1].'m';
                                $minutes  = ceil($totalTimeLogged/60);
                                $totalDiff  = gmdate('H:i', $totalTimeLogged);
                            } else {
                                $hours  = '00h 00m';
                                $minutes  = 00;
                                $totalDiff  = '0H0m';
                            }
                        @endphp
                        <div class="row" style="padding: 10px">
                            <div class="col-md-12 set-col-md-12-box2 text-center">
                                {{--<h3>SUNDAY 03, FEBRUARY</h3>--}}
                                <h3>
                                    {{$dateToday}}
                                </h3>
                            </div>
                            <div class="col-md-12 set-col-md-12-box3 text-center">
                                <h1>{{$hours}}</h1>
                            </div>
                            <div class="col-md-12 set-col-md-12-box4 text-center">
                                <div class="row set-para">
                                    <div class="col-md-4 set-row-col-6-fa-circle-1">
                                        <i class="fa fa-circle set-fa-circle1"></i>
                                        <span class="set-span-1">
                                                {{trans('common.track_Time')}}:
                                                <span class="set-Hours">&nbsp;{{$totalDiff}} {{trans('common.hours')}}</span>
                                            </span>
                                    </div>

                                    <div class="col-md-3">
                                        <i class="fa fa-circle set-fa-circle2"></i>
                                        <span class="set-span-2">
                                                {{trans('common.manual')}}:
                                                <span class="set-Hours">0 {{trans('common.hours')}}</span>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row set-col-row-screen-shots text-center">
                            <span class="set-spn-screen-shots">{{trans('common.Screen_shot')}}</span>
                        </div>

                        <div class="row set-row-box-last">
                            <div class="col-md-12 set-col-md-12-box-last">
                                @foreach($day1Data as $k1 => $v1)
                                    @php
                                        $pictures = $v1->pictures->toArray();
                                    @endphp
                                    @foreach(array_chunk($pictures , 4) as $chunk)
                                        <div class="col-md-12 set-col-md-12-box-last-2">
                                            @foreach($chunk as $k => $v)
                                                <div class="col-md-3 col-sm-3 col-xs-3 set-col-md-3">

                                                    <div class="row set-col-md-2-box-last" style="margin: 0; margin-bottom: 3px">
                                                        <span>{{$v1['memo']}}</span>
                                                    </div>
                                                    <div class="row set-row-imge-screen-shoots">
                                                        <a href="{{$v['text']}}" data-lightbox="image-1">
                                                            <img class="set-Image-screen-shoot" src="{{$v['text']}}" alt="avatar">
                                                        </a>
                                                        <div class="col-md-12 setCol-md-12timeDate">
                                                            <div class="col-md-10 col-sm-10 col-xs-10 set-col-timeDate">
                                                                <span class="set-time-screen-shot">
                                                {{\Carbon\Carbon::parse($v['created_at'])->subMinute(5)->toTimeString()}} - {{\Carbon\Carbon::parse($v['created_at'])->toTimeString()}}
                                                </span>
                                                            </div>
                                                            <div class="col-md-2 set-delete-icon-row">
                                                                <span class="set-delete-icon"><i class="fa fa-trash" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="battery">
                                                            @php
                                                                $total = 5;
                                                                $inactive = isset($v['activity_level']) && $v['activity_level'] >=0 ? $v['activity_level'] : 5;
                                                                $active = $total - $inactive;
                                                            @endphp

                                                            @for($i=0; $i<$inactive; $i++)
                                                                <div class="battery-charge-cell"></div>
                                                            @endfor
                                                            @for($z=0; $z<$active; $z++)
                                                                <div class="battery-charge-cell active-battery"></div>
                                                            @endfor
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div id="menu5" class="tab-pane fade @if($currentDay == 5 ) in active @endif ">
                        @php
                            $day = 4;
                            $dateToday = $date->startOfWeek()->addDay($day)->format('D d M');
                            $day1Data = collect();
                            $totalTimeLogged = 0;
                            foreach ($recordedTime as $k => $v) {
                                if($k == $date->startOfWeek()->addDay($day)->toDateString()) {
                                    $day1Data = $v;
                                }
                            }
                            foreach($day1Data as $k => $v) {
                                $startTime = isset($v->start_time)  ? Carbon\Carbon::parse($v->start_time) : Carbon\Carbon::now();
                                $endTime = isset($v->end_time)  ? Carbon\Carbon::parse($v->end_time) : Carbon\Carbon::now();
                                $totalTimeDiff = $endTime->diffInSeconds($startTime);
                                $totalTimeLogged += $totalTimeDiff;
                            }
                            $len = $day1Data->count();
                            if($len > 0) {
                                $hours  = gmdate('H:i', $totalTimeLogged);
                                $exploded = explode(':', $hours);
                                $hours = $exploded[0].'h ' . $exploded[1].'m';
                                $minutes  = ceil($totalTimeLogged/60);
                                $totalDiff  = gmdate('H:i', $totalTimeLogged);
                            } else {
                                $hours  = '00h 00m';
                                $minutes  = 00;
                                $totalDiff  = '0H0m';
                            }
                        @endphp
                        <div class="row" style="padding: 10px">
                            <div class="col-md-12 set-col-md-12-box2 text-center">
                                {{--<h3>SUNDAY 03, FEBRUARY</h3>--}}
                                <h3>
                                    {{$dateToday}}
                                </h3>
                            </div>
                            <div class="col-md-12 set-col-md-12-box3 text-center">
                                <h1>{{$hours}}</h1>
                            </div>
                            <div class="col-md-12 set-col-md-12-box4 text-center">
                                <div class="row set-para">
                                    <div class="col-md-4 set-row-col-6-fa-circle-1">
                                        <i class="fa fa-circle set-fa-circle1"></i>
                                        <span class="set-span-1">
                                               {{trans('common.track_time')}}:
                                                <span class="set-Hours">&nbsp;{{$totalDiff}} {{trans('common.hours')}}</span>
                                            </span>
                                    </div>

                                    <div class="col-md-3">
                                        <i class="fa fa-circle set-fa-circle2"></i>
                                        <span class="set-span-2">
                                                {{trans('common.manual')}}:
                                                <span class="set-Hours">0 {{trans('common.hours')}}</span>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row set-col-row-screen-shots text-center">
                            <span class="set-spn-screen-shots">{{trans('common.Screen_shot')}}</span>
                        </div>

                        <div class="row set-row-box-last">
                            <div class="col-md-12 set-col-md-12-box-last">
                                @foreach($day1Data as $k1 => $v1)
                                    @php
                                        $pictures = $v1->pictures->toArray();
                                    @endphp
                                    @foreach(array_chunk($pictures , 4) as $chunk)
                                        <div class="col-md-12 set-col-md-12-box-last-2">
                                            @foreach($chunk as $k => $v)
                                                <div class="col-md-3 col-sm-3 col-xs-3 set-col-md-3">

                                                    <div class="row set-col-md-2-box-last" style="margin: 0; margin-bottom: 3px">
                                                        <span>{{$v1['memo']}}</span>
                                                    </div>
                                                    <div class="row set-row-imge-screen-shoots">
                                                        <a href="{{$v['text']}}" data-lightbox="image-1">
                                                            <img class="set-Image-screen-shoot" src="{{$v['text']}}" alt="avatar">
                                                        </a>
                                                        <div class="col-md-12 setCol-md-12timeDate">
                                                            <div class="col-md-10 col-sm-10 col-xs-10 set-col-timeDate">
                                                            <span class="set-time-screen-shot">
                                        {{\Carbon\Carbon::parse($v['created_at'])->subMinute(5)->toTimeString()}} - {{\Carbon\Carbon::parse($v['created_at'])->toTimeString()}}
                                        </span>
                                                            </div>
                                                            <div class="col-md-2 set-delete-icon-row">
                                                                <span class="set-delete-icon"><i class="fa fa-trash" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="battery">
                                                            @php
                                                                $total = 5;
                                                                $inactive = isset($v['activity_level']) && $v['activity_level'] >=0 ? $v['activity_level'] : 5;
                                                                $active = $total - $inactive;
                                                            @endphp

                                                            @for($i=0; $i<$inactive; $i++)
                                                                <div class="battery-charge-cell"></div>
                                                            @endfor
                                                            @for($z=0; $z<$active; $z++)
                                                                <div class="battery-charge-cell active-battery"></div>
                                                            @endfor
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div id="menu6" class="tab-pane fade @if($currentDay == 6 ) in active @endif ">
                        @php
                            $day = 5;
                            $dateToday = $date->startOfWeek()->addDay($day)->format('D d M');
                            $day1Data = collect();
                            $totalTimeLogged = 0;
                            foreach ($recordedTime as $k => $v) {
                                if($k == $date->startOfWeek()->addDay($day)->toDateString()) {
                                    $day1Data = $v;
                                }
                            }
                            foreach($day1Data as $k => $v) {
                                $startTime = isset($v->start_time)  ? Carbon\Carbon::parse($v->start_time) : Carbon\Carbon::now();
                                $endTime = isset($v->end_time)  ? Carbon\Carbon::parse($v->end_time) : Carbon\Carbon::now();
                                $totalTimeDiff = $endTime->diffInSeconds($startTime);
                                $totalTimeLogged += $totalTimeDiff;
                            }
                            $len = $day1Data->count();
                            if($len > 0) {
                                $hours  = gmdate('H:i', $totalTimeLogged);
                                $exploded = explode(':', $hours);
                                $hours = $exploded[0].'h ' . $exploded[1].'m';
                                $minutes  = ceil($totalTimeLogged/60);
                                $totalDiff  = gmdate('H:i', $totalTimeLogged);
                            } else {
                                $hours  = '00h 00m';
                                $minutes  = 00;
                                $totalDiff  = '0H0m';
                            }
                        @endphp
                        <div class="row" style="padding: 10px">
                            <div class="col-md-12 set-col-md-12-box2 text-center">
                                {{--<h3>SUNDAY 03, FEBRUARY</h3>--}}
                                <h3>
                                    {{$dateToday}}
                                </h3>
                            </div>
                            <div class="col-md-12 set-col-md-12-box3 text-center">
                                <h1>{{$hours}}</h1>
                            </div>
                            <div class="col-md-12 set-col-md-12-box4 text-center">
                                <div class="row set-para">
                                    <div class="col-md-4 set-row-col-6-fa-circle-1">
                                        <i class="fa fa-circle set-fa-circle1"></i>
                                        <span class="set-span-1">
                                                {{trans('common.track_time')}}:
                                                <span class="set-Hours">&nbsp;{{$totalDiff}} {{trans('common.hours')}}</span>
                                            </span>
                                    </div>

                                    <div class="col-md-3">
                                        <i class="fa fa-circle set-fa-circle2"></i>
                                        <span class="set-span-2">
                                                {{trans('common.manual')}}:
                                                <span class="set-Hours">0 {{trans('common.hours')}}</span>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row set-col-row-screen-shots text-center">
                            <span class="set-spn-screen-shots">{{trans('common.Screen_shot')}}</span>
                        </div>

                        <div class="row set-row-box-last">
                            <div class="col-md-12 set-col-md-12-box-last">
                                @foreach($day1Data as $k1 => $v1)
                                    @php
                                        $pictures = $v1->pictures->toArray();
                                    @endphp
                                    @foreach(array_chunk($pictures , 4) as $chunk)
                                        <div class="col-md-12 set-col-md-12-box-last-2">
                                            @foreach($chunk as $k => $v)
                                                <div class="col-md-3 col-sm-3 col-xs-3 set-col-md-3">

                                                    <div class="row set-col-md-2-box-last" style="margin: 0; margin-bottom: 3px">
                                                        <span>{{$v1['memo']}}</span>
                                                    </div>
                                                    <div class="row set-row-imge-screen-shoots">
                                                        <a href="{{$v['text']}}" data-lightbox="image-1">
                                                            <img class="set-Image-screen-shoot" src="{{$v['text']}}" alt="avatar">
                                                        </a>
                                                        <div class="col-md-12 setCol-md-12timeDate">
                                                            <div class="col-md-10 col-sm-10 col-xs-10 set-col-timeDate">
                                                                <span class="set-time-screen-shot">
                                                {{\Carbon\Carbon::parse($v['created_at'])->subMinute(5)->toTimeString()}} - {{\Carbon\Carbon::parse($v['created_at'])->toTimeString()}}
                                                </span>
                                                            </div>
                                                            <div class="col-md-2 set-delete-icon-row">
                                                                <span class="set-delete-icon"><i class="fa fa-trash" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="battery">
                                                            @php
                                                                $total = 5;
                                                                $inactive = isset($v['activity_level']) && $v['activity_level'] >=0 ? $v['activity_level'] : 5;
                                                                $active = $total - $inactive;
                                                            @endphp

                                                            @for($i=0; $i<$inactive; $i++)
                                                                <div class="battery-charge-cell"></div>
                                                            @endfor
                                                            @for($z=0; $z<$active; $z++)
                                                                <div class="battery-charge-cell active-battery"></div>
                                                            @endfor
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div id="menu7" class="tab-pane fade @if($currentDay == 7 ) in active @endif ">
                        @php
                            $day = 6;
                            $dateToday = $date->startOfWeek()->addDay($day)->format('D d M');
                            $day1Data = collect();
                            $totalTimeLogged = 0;
                            foreach ($recordedTime as $k => $v) {
                                if($k == $date->startOfWeek()->addDay($day)->toDateString()) {
                                    $day1Data = $v;
                                }
                            }
                            foreach($day1Data as $k => $v) {
                                $startTime = isset($v->start_time)  ? Carbon\Carbon::parse($v->start_time) : Carbon\Carbon::now();
                                $endTime = isset($v->end_time)  ? Carbon\Carbon::parse($v->end_time) : Carbon\Carbon::now();
                                $totalTimeDiff = $endTime->diffInSeconds($startTime);
                                $totalTimeLogged += $totalTimeDiff;
                            }
                            $len = $day1Data->count();
                            if($len > 0) {
                                $hours  = gmdate('H:i', $totalTimeLogged);
                                $exploded = explode(':', $hours);
                                $hours = $exploded[0].'h ' . $exploded[1].'m';
                                $minutes  = ceil($totalTimeLogged/60);
                                $totalDiff  = gmdate('H:i', $totalTimeLogged);
                            } else {
                                $hours  = '00h 00m';
                                $minutes  = 00;
                                $totalDiff  = '0H0m';
                            }
                        @endphp
                        <div class="row" style="padding: 10px">
                            <div class="col-md-12 set-col-md-12-box2 text-center">
                                {{--<h3>SUNDAY 03, FEBRUARY</h3>--}}
                                <h3>
                                    {{$dateToday}}
                                </h3>
                            </div>
                            <div class="col-md-12 set-col-md-12-box3 text-center">
                                <h1>{{$hours}}</h1>
                            </div>
                            <div class="col-md-12 set-col-md-12-box4 text-center">
                                <div class="row set-para">
                                    <div class="col-md-4 set-row-col-6-fa-circle-1">
                                        <i class="fa fa-circle set-fa-circle1"></i>
                                        <span class="set-span-1">
                                               {{trans('common.track_Time')}}:
                                                <span class="set-Hours">&nbsp;{{$totalDiff}} {{trans('common.hours')}}</span>
                                            </span>
                                    </div>

                                    <div class="col-md-3">
                                        <i class="fa fa-circle set-fa-circle2"></i>
                                        <span class="set-span-2">
                                                {{trans('common.manual')}}:
                                                <span class="set-Hours">0 {{trans('common.hours')}}</span>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row set-col-row-screen-shots text-center">
                            <span class="set-spn-screen-shots">{{trans('common.Screen_shot')}}</span>
                        </div>

                        <div class="row set-row-box-last">
                            <div class="col-md-12 set-col-md-12-box-last">
                                @foreach($day1Data as $k1 => $v1)
                                    @php
                                        $pictures = $v1->pictures->toArray();
                                    @endphp
                                    @foreach(array_chunk($pictures , 4) as $chunk)
                                        <div class="col-md-12 set-col-md-12-box-last-2">
                                            @foreach($chunk as $k => $v)
                                                <div class="col-md-3 col-sm-3 col-xs-3 set-col-md-3">

                                                    <div class="row set-col-md-2-box-last" style="margin: 0; margin-bottom: 3px">
                                                        <span>{{$v1['memo']}}</span>
                                                    </div>
                                                    <div class="row set-row-imge-screen-shoots">
                                                        <a href="{{$v['text']}}" data-lightbox="image-1">
                                                            <img class="set-Image-screen-shoot" src="{{$v['text']}}" alt="avatar">
                                                        </a>
                                                        <div class="col-md-12 setCol-md-12timeDate">
                                                            <div class="col-md-10 col-sm-10 col-xs-10 set-col-timeDate">
                                                                <span class="set-time-screen-shot">
                                                {{\Carbon\Carbon::parse($v['created_at'])->subMinute(5)->toTimeString()}} - {{\Carbon\Carbon::parse($v['created_at'])->toTimeString()}}
                                                </span>
                                                            </div>
                                                            <div class="col-md-2 set-delete-icon-row">
                                                                <span class="set-delete-icon"><i class="fa fa-trash" aria-hidden="true"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="battery">
                                                            @php
                                                                $total = 5;
                                                                $inactive = isset($v['activity_level']) && $v['activity_level'] >=0 ? $v['activity_level'] : 5;
                                                                $active = $total - $inactive;
                                                            @endphp

                                                            @for($i=0; $i<$inactive; $i++)
                                                                <div class="battery-charge-cell"></div>
                                                            @endfor
                                                            @for($z=0; $z<$active; $z++)
                                                                <div class="battery-charge-cell active-battery"></div>
                                                            @endfor
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@push('js')
<link href="{{asset('assets/global/plugins/lightbox2/dist/css/lightbox.min.css')}}" rel="stylesheet" type="text/css"/>
<script src="{{asset('assets/global/plugins/lightbox2/dist/js/lightbox.min.js')}}" type="text/javascript"></script>
<script>
	$('.delete_time').on('click', function(){
		var a = window.confirm('Are you sure you want to remove your time?');
		if(a) {
			var pictureId = $(this).data('id');
			var form = new FormData();
			form.append("id", pictureId);

			var settings = {
				"url": "/remove-image",
				"method": "POST",
				"data": form,
				"processData": false,
				"contentType": false,
				"mimeType": "multipart/form-data",
			}

			$.ajax(settings).done(function (response) {
				var res = JSON.parse(response);
				console.log(res);
				if (res.status == 'success') {
					alertSuccess('Image Removed');
					window.location.href = '?#timeSheet'
				}
			});
		}
	})
</script>
@endpush
