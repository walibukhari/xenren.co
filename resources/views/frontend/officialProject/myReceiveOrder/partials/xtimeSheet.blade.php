<p class="setPPpPpP">View Time Sheet</p>
<div class="">
    <div class="row new-milestone-add setDIVCOl12W">
        <div class="col-md-12" style="color: black">
            <div class="row">
                <div id="calendar-wrap" class="setCalenderWrap">
                </div>
                <div class="row setroow">
                    <div class="col-md-12">
                        <div class="col-md-4 col-sm-12 col-xs-12 setSetRowP1 pull-left text-left">Weekly Total Screenshots: &nbsp; <span class="setSPNCOLOR0">125</span></div>
                        <div class="col-md-4 col-sm-12 col-xs-12 setSetRowP2 text-center">Events: &nbsp; <span class="setSPNCOLOR0">3400 Keyboards | </span><span class="setSPNCOLOR0">400 Mouse</span></div>
                        <div class="col-md-4 col-sm-12 col-xs-12 setSetRowP3 pull-right text-right">Weekly Logged: &nbsp; <span class="setSPNCOLOR0">25:30 HOURS</span></div>
                    </div>
                </div>

                <div class="container box2">
                    <div class="row set-row-section">
                        <div class="col-md-3 col-sm-12 col-xs-12 setBox2PP2 pull-left text-left">Sun <span>07-08-2017</span></div>
                        <div class="col-md-3 col-sm-12 col-xs-12 setBox2PP2 text-right pull-right">Day Logged: &nbsp; <span class="setSPNCOLOR0">05:30</span></div>
                        <hr class="setBox2hr">
                    </div>
                    @foreach($recordedTime as $k => $v)
                        @php
                            $startTime = \Carbon\Carbon::parse($project->timeTracked->first()['start_time']);
                            $endTime = is_null($project->timeTracked->last()['end_time']) ? \Carbon\Carbon::now() : \Carbon\Carbon::parse($project->timeTracked->last()['end_time']);
                            $diff = $endTime->diffInSeconds($startTime);
                            $diff = gmdate('H:i', $diff);
                        @endphp
                        <div class="row set-row-box2">
                            <div class="col-md-3 pull-left text-left setBox2PP1"> {{\Carbon\Carbon::parse($k)->format('D')}} <span>{{\Carbon\Carbon::parse($k)->toDateString()}}</span></div>
                            <div class="col-md-3 pull-right text-right setBox2PP2">Day Logged: &nbsp; <span class="setSPNCOLOR0">{{$diff}}</span></div>
                            <hr class="setBox2hr">
                        </div>

                        <div class="row">
                            {{--<div class="col-sm-1 setColsm1">--}}
                            {{--<span class="setspan0">8 PM</span>--}}
                            {{--</div>--}}
                            <div class="col-md-11">
                                @foreach($v as $k1 => $v1)
                                    <div class="col-md-12">
                                        <div class="col-sm-2 setCol2sm"><span class=""><input type="checkbox" value=""> {{Carbon\Carbon::parse($v1['created_at'])->format('g:i A')}} <img src="{{$v1['picture']['text']}}" alt="" style="width: 60px; position: absolute"> </span>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="battery">
                                                <div class="charge charge1"></div>
                                                <div class="charge charge2"></div>
                                                <div class="charge charge3"></div>
                                                <div class="charge charge4 inactive"></div>
                                                <div class="charge charge5 inactive"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 setColmd6">
                                            <p class="setcolparagraph">{{$v1['memo']}}</p>
                                        </div>
                                    </div>
                                    <hr class="setHR">
                                @endforeach
                            </div>
                        </div>
                        <hr>
                        <br>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>