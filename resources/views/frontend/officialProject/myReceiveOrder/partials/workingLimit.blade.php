<div class="row new-milestone-add" class="setRowNewMileS" >
    <div class="col-md-12" class="setCOLORBLACK">
        <label class="milestone-subpart-title">{{trans('common.working_limit')}}</label> <br/>
        <label class="milestone-subpart-title">{{trans('common.total_hours')}}: &nbsp; &nbsp; <span class="clrGreen">80 {{trans('common.hours')}} 500{{trans('common.usd')}}</span> </label>
        <div class="row setTBLESCROLL">
            <div class="form-group">
                <br/>
                <div class="setWidth">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th scope="row" class="setCOLORBLACK" >{{trans('common.commit_bonus_hour')}}</th>
                            <td class="clrGreen">40 {{trans('common.hours')}}</td>
                            <td class="clrGreen">300 {{trans('common.usd')}} ( $200 + $100 )</td>
                            <td>{{trans('common.work')}} 20 {{trans('common.hours')}}</td>
                            <td> 150 {{trans('common.usd')}} ( $150 + $50 )</td>
                        </tr>
                        <tr>
                            <th scope="row" class="setCOLORBLACK" >{{trans('common.commit_bonus_hour')}}</th>
                            <td class="clrGreen">40 {{trans('common.hours')}}</td>
                            <td class="clrGreen">300 {{trans('common.usd')}} ( $200 + $100 )</td>
                            <td>{{trans('common.work')}} 20 {{trans('common.hours')}}</td>
                            <td> 150 {{trans('common.usd')}} ( $150 + $50 )</td>
                        </tr>
                        <tr>
                            <th scope="row" class="setCOLORBLACK">{{trans('common.commit_bonus_hour')}}</th>
                            <td class="clrGreen">40 {{trans('common.hours')}}</td>
                            <td class="clrGreen">300 {{trans('common.usd')}} ( $200 + $100 )</td>
                            <td>{{trans('common.work')}} 20 {{trans('common.hours')}}</td>
                            <td> 150 {{trans('common.usd')}} ( $150 + $50 )</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>