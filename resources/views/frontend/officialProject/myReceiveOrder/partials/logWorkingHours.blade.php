<div class="row new-milestone-add setRowNewMileS" xmlns="http://www.w3.org/1999/html">
    <div class="col-md-12 setCol12LWHP">
        <label class="milestone-subpart-title setLABEL1LWHP">{{trans('common.manual_log')}}</label>
        {{--<label class="milestone-subpart-title">Total Target working Hour: <span class="setSPN1LWHP">6.7 Hours</span> &nbsp;&nbsp;&nbsp; &nbsp; Extra: <span class="setSPN2LWHP">1 USD / Hours</span> </label>--}}
        <div class="row setRowMWHLP">
            <div class="form-group">

                <div class="col-xs-5 setColMd2LWHP">
                    <label for="ex2">{{trans('common.start_time')}}</label>
                    <div class="input-group">
                        <input type="text" class="form-control start" />
                        <span class="input-group-btn">
                            <button type="button" class="btn default date-set">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                </div>

                <div class="col-xs-5 setColMd2LWHP">
                    <label for="ex1">{{trans('common.end_time')}}</label>
                    <div class="input-group">
                        <input type="text" class="form-control end" />
                        <span class="input-group-btn">
                            <button type="button" class="btn default date-set">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                </div>

                <div class="col-xs-2 setColXsWLP">
                    <label for="ex1">{{trans('common.summary')}}</label><br/>
                    <label for="ex1">$9 + $3 = 12</label>
                </div>

            </div>
        </div>
    </div>
</div>

@section('javascript')
    <script>
        $(function () {
	        $(".start, .end").datetimepicker({
		        autoclose: true,
		        isRTL: App.isRTL(),
		        pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
		        minView : 1,
                defaultDate: "11/1/2013",
	        }).on('changeDate', function(ev) {
		        console.log(ev);
		        console.log(ev.date);
	        });
        })
    </script>
@endsection