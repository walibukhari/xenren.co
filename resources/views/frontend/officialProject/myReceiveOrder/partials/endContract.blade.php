<div class="row new-milestone-add" style="height:500px;">
    <div class="col-md-12" style="color: black">
        <label class="milestone-subpart-title">{{trans('common.end_contract')}}</label><br/>

        <div class="row">
            <div class="col-md-8">
                <label class="milestone-subpart-title">{{trans('common.after_click')}}</label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <label class="milestone-subpart-title">{{trans('common.reasons_of_end_Contract')}}</label>
                <textarea name="" class="form-control" rows="7"></textarea>
            </div>
        </div>
        <div class="row">
            <div class="form-group">

                <div class="col-xs-6 setColXsEC">
                    <br/>
                    <button class="btn btn-success btn-green-bg-white-text setBNBNJDP" id="confirmMilestoneFreelancer" type="button">{{trans('common.confirm')}}</button>
                    <button class="btn btn-success btn-white-bg-green-text setBNBNJDP" id="cancelMilestone" type="button">{{trans('common.Cancel')}}</button>
                </div>

            </div>
        </div>
    </div>
</div>