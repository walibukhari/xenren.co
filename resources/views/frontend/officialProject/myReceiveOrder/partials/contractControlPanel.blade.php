<div class="row new-milestone-add" class="setRowNewMileS">
    <div class="col-md-12" class="setCOLORBLACK">
        {{--<label class="milestone-subpart-title">Contract Control Panel</label><br/>--}}
        {{--<label class="milestone-subpart-title">Total Target working Hour: <span class="setColorGreen">6.7 Hours</span> &nbsp;&nbsp;&nbsp; &nbsp; Extra: <span class="setColorGreen">1 USD / Hours</span> </label>--}}
        <div class="row">
            <div class="form-group">
                <br/>
                <div class="SETContainer">
                    <div class="toggle-btn" onclick="this.classList.toggle('activee')">
                        <div class="inner-circle">

                        </div>
                    </div>
                </div>
                <div class="col-xs-9 setColxsCCP">
                    <label class="setLBLBLB">
                        {{trans('common.commit_hour')}}<br/>
                        <div class="setDIVIV">
                   {{trans('common.employer_require_pay_extra_if_freelancer')}}
                        </div>
                        <br/>
                    <span class="clrGreen">{{trans('common.daily_average')}}</span>
                    </label>
                </div>

                <div class="col-xs-3 setColxsCCP">
                    <button class="btn btn-success btn-green-bg-white-text setBNBNJDP" id="confirmMilestoneFreelancer" type="button">{{trans('common.accept')}}</button>
                    <button class="btn btn-success btn-white-bg-green-text setBNBNJDP" id="cancelMilestone" type="button">{{trans('common.reject')}}</button>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <br/>
                <div class="SETContainer2">
                    <div class="toggle-bbtn" onclick="this.classList.toggle('activeee')">
                        <div class="iinner-circle">

                        </div>
                    </div>
                </div>
                <div class="col-xs-9 setColxsCCP">
                    <label class="setLBLBLB">
                        {{trans('common.daily_update')}}<br/>
                        <div class="setDVIVI2">
                   {{trans('common.employee_require_daily_update')}}</span>
                        </div>
                    </label>
                </div>

                <div class="col-xs-3 setColxsCCP">
                    <button class="btn btn-success btn-green-bg-white-text setBNBNJDP" id="confirmMilestoneFreelancer" type="button">{{trans('common.accept')}}</button>
                    <button class="btn btn-success btn-white-bg-green-text setBNBNJDP" id="cancelMilestone" type="button">{{trans('common.reject')}}</button>
                </div>

            </div>
        </div>
    </div>
</div>