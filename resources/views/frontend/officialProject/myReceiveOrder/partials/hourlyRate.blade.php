<div class="row new-milestone-add" style="height:500px;">
    <div class="col-md-12" style="color: black">
        <label class="milestone-subpart-title">Hourly Rate</label><br/>
        <div class="row">
            <div class="form-group">

                <div class="col-xs-3 setColxsHRP">
                    <label for="ex2">Currently Hourly Pay</label><br/>
                    <label for="ex2" class="clrGreen">$8</label>
                </div>

                <div class="col-xs-3 setColxsHRP">
                    <label for="ex2">Basic</label><br/>
                    <label for="ex2" class="clrGreen">$8 / Hour</label>
                </div>

                <div class="col-xs-2 setColxsHRP">
                    <label for="ex2">Commit Bounce</label><br/>
                    <label for="ex2" class="clrGreen">$2 / Hour</label>
                </div>

                <div class="col-xs-3 setColxsHRP">
                    <label for="ex2">Require weekly commit hours</label><br/>
                    <label for="ex2" class="clrGreen">20</label>
                </div>

            </div>
        </div>
        <div class="row setTBLESCROLL">
            {{--<br/>--}}
            <div class="setWidth>">
                <table class="table">
                <thead class="thead-light">
                <tr class="setHRPTR">
                    <th scope="col" width="10%">Status</th>
                    <th scope="col" width="10%">Basic</th>
                    <th scope="col" width="15%">Commit Bonus</th>
                    <th scope="col" width="10%">Total Cost</th>
                    <th scope="col" width="15%">Require weekly commit hours</th>
                    <th scope="col" width="10%">Starts from</th>
                    <th scope="col" width="35%">Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">New Offer</th>
                    <td>$8 / Hour </td>
                    <td>Extra $1 / Hour</td>
                    <td>11 USD</td>
                    <td>20</td>
                    <td>16-02-2017</td>
                    <td class="setBTNTDHRP">
                        <button class="btn btn-success btn-green-bg-white-text setBNBNJDP" id="confirmMilestoneFreelancer" type="button">{{trans('common.confirm')}}</button>
                        <button class="btn btn-success btn-white-bg-green-text setBNBNJDP" id="cancelMilestone" type="button">{{trans('common.Cancel')}}</button>
                    </td>
                </tr>
                </tbody>
            </table>
            </div>
        </div>
        <div class="row">
            <div class="form-group">

                {{--<div class="col-xs-6">--}}
                    {{--<br/>--}}
                    {{--<button class="btn btn-success btn-green-bg-white-text setBNBNJDP" id="confirmMilestoneFreelancer" type="button">Submit</button>--}}
                    {{--<button class="btn btn-success btn-white-bg-green-text setBNBNJDP" id="cancelMilestone" type="button">Cancel</button>--}}
                {{--</div>--}}

            </div>
        </div>
    </div>
</div>