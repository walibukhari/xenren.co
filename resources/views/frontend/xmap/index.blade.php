@extends('frontend.layouts.default')

@section('title')
    Map
@endsection

@section('description')
    Map
@endsection

@section('author')
    Xenren
@endsection

@section('header')
    <link href="/assets/global/plugins/bootstrap-star-rating/css/star-rating.css" media="all" rel="stylesheet" type="text/css"/>
    <link href="/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
    <link href="http://cache.amap.com/lbs/static/main1119.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/map.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE BASE CONTENT -->
            <div id="container" style="width:1100px; height:700px"></div>
            <div id="tip">
                <span id="result"></span>
            </div>
            <input id="user-address" type="hidden" value="{{ $address }}">
            {{--<input id="user-address" type="hidden" value="北京市海淀区苏州街">--}}
            {{--<input id="user-address" type="hidden" value="北京市朝阳区望京街道方恒国际中心A座北京方恒假日酒店">--}}
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection


@section('footer')
    <script src="/assets/global/plugins/bootstrap-star-rating/js/star-rating.js" type="text/javascript"></script>
    <script src="http://webapi.amap.com/maps?v=1.3&key=3cd6322db54c3092b3e9601574597e9d&plugin=AMap.Geocoder" type="text/javascript" ></script>
    <script src="/custom/js/frontend/map.js" type="text/javascript"></script>
    <script src="http://cache.amap.com/lbs/static/addToolbar.js" type="text/javascript" ></script>
@endsection




