@extends('frontend.layouts.default')

@section('title')
    Map
@endsection

@section('description')
    Map
@endsection

@section('author')
    Xenren
@endsection

@section('header')
    <link href="http://cache.amap.com/lbs/static/main1119.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <body onload="geocoder()"></body>
            <!-- BEGIN PAGE BASE CONTENT -->
            <div id="container" style="width:1100px; height:500px"></div>
            <div id="tip">
                <span id="result"></span>
            </div>

            <form class="form-horizontal">
                <div class="form-body" style="position: absolute;margin-top: 500px;">
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <input type="text" class="form-control" value="" id="address" placeholder="address">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6">
                            <div class="col-md-6">
                                Longitude
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" value="" id="longitude" readonly>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="col-md-6">
                                Latitude
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" value="" id="latitude" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection


@section('footer')
    <script src="http://webapi.amap.com/maps?v=1.3&key=3cd6322db54c3092b3e9601574597e9d&plugin=AMap.Geocoder" type="text/javascript" ></script>

    <script type="text/javascript">

        var map = new AMap.Map("container", {
            resizeEnable: true
        });

        var prevMarker = null;

        function geocoder() {
            var geocoder = new AMap.Geocoder({
                city: "010", //城市，默认：“全国”
                radius: 1000 //范围，默认：500
            });

            //地理编码,返回地理编码结果
            geocoder.getLocation("北京市朝阳区望京街道方恒国际中心A座北京方恒假日酒店", function(status, result) {
                if (status === 'complete' && result.info === 'OK') {
                    geocoder_CallBack(result);
                }
            });
        }

        function addMarker(i, d) {
            var marker = new AMap.Marker({
                map: map,
                position: [ d.location.getLng(),  d.location.getLat()]
            });
            var infoWindow = new AMap.InfoWindow({
                content: d.formattedAddress,
                offset: {x: 0, y: -30}
            });
            marker.on("mouseover", function(e) {
                infoWindow.open(map, marker.getPosition());
            });

            prevMarker = marker;
        }

        //地理编码返回结果展示
        function geocoder_CallBack(data) {
            var resultStr = "";
            //地理编码结果数组
            var geocode = data.geocodes;
            for (var i = 0; i < geocode.length; i++) {
                //拼接输出html
                resultStr += "<span style=\"font-size: 12px;padding:0px 0 4px 2px; border-bottom:1px solid #C1FFC1;\">" +
                    "<b>地址</b>：" + geocode[i].formattedAddress + "" +
                    "&nbsp;&nbsp;<b>的地理编码结果是:</b><b>&nbsp;&nbsp;&nbsp;&nbsp;坐标</b>：" +
                    geocode[i].location.getLng() + ", " + geocode[i].location.getLat() + "" +
                    "<b>&nbsp;&nbsp;&nbsp;&nbsp;匹配级别</b>：" + geocode[i].level + "</span>";

                addMarker(i, geocode[i]);

                $('#longitude').val(geocode[i].location.getLng());
                $('#latitude').val(geocode[i].location.getLat());
            }
            map.setFitView();
            document.getElementById("result").innerHTML = resultStr;

        }

        $('body').on('blur', 'input[id=address]', function () {
            prevMarker.setMap(null);
            var address = $('#address').val();

            var geocoder = new AMap.Geocoder({
                city: "010", //城市，默认：“全国”
                radius: 200 //范围，默认：500
            });

            //地理编码,返回地理编码结果
            geocoder.getLocation(address, function(status, result) {
                if (status === 'complete' && result.info === 'OK') {
                    geocoder_CallBack(result);
                }
            });
        });
    </script>
@endsection




