<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta property="wb:webmaster" content="4bfa78a5221bb48a" />
    <meta property="qc:admins" content="2552431241756375" />
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>Accepted Invitation</title>
    <style>

        #button {
            background-color: #5ec329;
            border: none;
            color: white;
            padding: 10px 70px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
            border-radius: 6px;
        }
        span{
            color: #5ec329;
            font-weight: bold;
            font-size: 25px
        }
        hr{
            margin: 14px 0px 13px 0px;
            color:#6F779F;
            opacity: .5;
        }

        #inviteFriend{
            margin-top: 40px;
            font-family: Helvetica, Arial, sans-serif;
        }
        #checkInfo{
            margin-top:-10px; margin-bottom:20px;
        }
        #checkInfo> small{
            font-family: Helvetica, Arial, sans-serif; font-weight: bold;
        }

        #cd> small{
            font-weight: bolder;
        }
        #lll{
            color: #5ec329;
            font-family: Helvetica, Arial, sans-serif;
        }
        #ufx{
            color:#6F779F;
            margin-top: 10px;
            font-weight: bolder;
            font-family: Helvetica, Arial, sans-serif;
        }
        #body{
            background-color:#F6F6F6;
        }
        #container{
            width:50%;
            background-color:white;
            margin:40px 0px 25px 230px;

        }
        #image{
            margin-top: 35px;
        }
        #offset{
            margin-top: 40px;
        }
        #aiimage{
            margin-top:40px;
        }
        label{
            opacity: .4;  font-weight:normal;
            margin-left:20px;
            font-size:15px;
            color:#6F779F;
            font-family:Helvetica, Arial, sans-serif;
        }
        #set{
            margin-top:4px;
            margin-left:20px;
            font-size: 15px;
            font-family: Helvetica, Arial, sans-serif;
        }
        #ex {
            display: inline-block;
            width:auto;
            color:white;
            padding:0px 8px;
            border: 3px solid #5ec329;
            background-color: #5ec329;
            border-radius:14px;
            font-size:14px;
        }

        #cd{
            width: 50%;
            margin:40px 0px 25px 230px;
        }
        #cd hr{
            opacity: .8;
            width:50%;
            border: 0px;
            border-top:1px solid #C0C0C0;
        }


        #img{
            margin: 0px 0px 0px 500px;
        }

        @media (max-width: 1366px){
            #container{
                width:50%;
                background-color:white;
                margin:40px 0px 25px 230px;

            }

            #cd{
                width: 50%;
                margin:40px 0px 25px 230px;
            }

            #img{
                margin: 0px 0px 0px 500px;
            }
        }

        @media (max-width: 1024px){
            #container{
                width:50%;
                background-color:white;
                margin:40px 0px 25px 230px;

            }

            #cd{
                width: 50%;
                margin:40px 0px 25px 230px;
            }

            #img{
                margin: 0px 0px 0px 450px;
            }
        }


        @media (max-width: 768px){
            #container{
                width:50%;
                background-color:white;
                margin:40px 0px 25px 230px;

            }

            #cd{
                width: 50%;
                margin:40px 0px 25px 230px;
            }

            #img{
                margin: 0px 0px 0px 370px;
            }
        }
        @media (max-width: 823px){
            #img{
                margin: 0px 0px 0px 390px;
            }
        }
        @media (max-width: 812px){
            #container{
                width:50%;
                background-color:white;
                margin:40px 0px 25px 200px;

            }

            #cd{
                width: 50%;
                margin:40px 0px 25px 200px;
            }

            #img{
                margin: 0px 0px 0px 360px;
            }
        }

        @media (max-width: 736px){

            #img{
                margin: 0px 0px 0px 340px;
            }
        }

        @media (max-width:667px)
        {
            #img{
                margin: 0px 0px 0px 330px;
            }
        }

        @media (max-width:568px) {

            #container{
                width:70%;
                background-color:white;
                margin:40px 0px 25px 90px;

            }

            #cd{
                width: 100%;
                margin:40px 0px 25px 0px;
            }

            #img {
                margin: 0px 0px 0px 250px;
            }
        }

        @media (max-width: 414px){
            #container{
                width:100%;
                background-color:white;
                margin:40px 0px 25px 0px;

            }

            #cd{
                width: 100%;
                margin:40px 0px 25px 0px;
            }

            #img{
                margin: 0px 0px 0px 160px;
            }
        }
        @media (max-width: 375px){
            #img{
                margin: 0px 0px 0px 140px;
            }
        }

        @media (max-width: 320px) {
            #container {
                width: 100%;
                background-color: white;
                margin: 40px 0px 25px 0px;

            }

            #cd {
                width: 100%;
                margin: 40px 0px 25px 0px;
            }

            #img {
                margin: 0px 0px 0px 120px;
            }
        }

    </style>
</head>
<body id="body">
<div id="img">
    <img id="image" src="htt://www.xenren.co/images/email-template-images/logo.png" alt="">
</div>
<div id="container">

        <div id="offset">

            <div>
                <div align="center">
                    <img id="aiimage"  src="{{asset('images/email-template-images/user-icon.png')}}" alt="">
                </div>
                <div id="inviteFriend" align="center">
                    <span>Your Friend {{Auth::guard('users')->user()->name}} invites you</span><br>
                    <span>to join Xenren</span>
                </div>
                <br>
                <div id="checkInfo" align="center">
                    <small>Please check your info below and click continue button</small>
                </div>
                <hr>
                <div>
                    <label>Name</label>
                </div>
                <div id="set">
                    {{$d["name"]}}
                </div>
                <hr>
                <div>
                    <label>Skills</label>
                </div>
                <div style="margin-left:20px;">
                        @if(isset($d['skill']) && $d['skill'] != '')
                            @foreach($d['skill'] as $k => $v)
                                @php
                                    $lan =   \App\Models\Skill::select(['id', 'name_en'])->where('id', '=', $v)->first();
                                @endphp
                            <div id="ex">
                                {{$lan->name_en}}
                            </div>
                        @endforeach
                        @endif
                </div>
                <hr>
                <div>
                    <label>Phone</label>
                </div>
                <div id="set">
                    {{$d["phone"]}}
                </div>
                <hr>
                <div>
                    <label>Email</label>
                </div>
                <div id="set">
                    {{$d["email"]}}
                </div>
                <hr>
                <div>
                    <label >Language</label>
                </div>
                <div id="set">
                    @if(isset($d['language']) && $d['language'] != '')
                        @foreach($d['language'] as $k => $v)
                            @php
                                $lan =   \App\Models\Language::select(['id', 'name'])->where('id', '=', $v)->first();
                            @endphp
                            <div id="ex">
                            {{$lan->name}}
                            </div>
                        @endforeach
                    @endif
                </div>
                <hr>
                <div>
                    <label>About</label>
                </div>
                <div id="set">
                   {{$d["about"]}}
                </div>
                <hr>
                <div align="center">
                    <a href="{{$link}}" id="button">Continue</a>
                </div>
                    <br><br>
            </div>
        </div>
    </div>
</div>

<div id="cd">
            <div align="center">
                <small id="ufx" style="opacity: .5;">Unsubscribe from Xenren</small>
                <hr id="hr">
                <small id="lll">Legends Lair Limited &copy; 2004-2017</small>
                <br>
                <small style="color:#6F779F; font-family: Helvetica, Arial, sans-serif; opacity: .5">Company Register N0:2099572</small> <i style="color:#6F779F;">|</i> <small style="font-family: Helvetica, Arial, sans-serif; color:#6F779F;opacity: .5" > Customer Hotline:075523320228</small>
            </div>
</div>
<br><br>
</body>
</html>
