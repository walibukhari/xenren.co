@extends('frontend.layouts.default')

@section('title')
    {!! trans('common.login_title') !!}
@endsection

@section('keywords')
    {!! trans('common.login_keyword') !!}
@endsection

@section('description')
    {!! trans('common.login_desc') !!}
@endsection

@section('content')
    <style>
        html:lang(cn) body, .password-btn button{
            font-family: 'Raleway', sans-serif !important;
        }
        .form input {
            height: 50px !important;
            border: 2px solid #c1c1c1 !important
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-6 col-md-offset-4 col-sm-offset-4 col-xs-offset-4" style="margin-top: 20px">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <br>
                        <div class="row">
                            <h3 class="text-center" style="font-family: 'Montserrat', sans-serif !important; color: #5ec329">{{trans("common.set_your_password")}}</h3>
                        </div>
                        <div class="row">
                            <div class="portlet-body personal-info">
                                <div class="portlet-body form">
                                    <form role="form" method="POST" action="{{ route('setPassword.post') }}" class="form-horizontal" id="">
                                        {!! csrf_field() !!}
                                        <div class="form-body">
                                            <div class="">
                                                <div class="col-md-3 col-xs-3 col-sm-3">
                                                    <label for="" style="color: #c1c1c1;">{{trans("common.email")}}</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                              <div class="">
                                                <div class="col-md-11 col-xs-11 col-sm-11" style="left: 10px">
                                                    <input class="form-control" placeholder=common.your_email@.gmail.com" name="email" type="text" value="{{$email}}" readonly>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="" style="margin-left: 10px">
                                                    <label for="" style="color: #c1c1c1;margin-left: 4px">{{trans("common.your_email_will_be_your_login_info")}}</label>
                                            </div>
                                            <div class="margin-password">
                                                <div class="col-md-3 col-xs-3 col-sm-3">
                                                    <label for="" style="color: #c1c1c1">{{trans("common.password")}}</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                             <div class="">
                                                <div class="col-md-11 col-xs-11 col-sm-11" style="left: 10px">
                                                    <input class="form-control" name="password" type="password" value="" required>
                                                </div>
                                             </div>
                                            </div>
                                            <div class="margin-password">
                                                <div class="col-md-6 col-xs-6 col-sm-6">
                                                    <label for="" style="color: #c1c1c1">{{trans("common.re_password")}}</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                            <div class="">
                                                <div class="col-md-11 col-xs-11 col-sm-11" style="left: 10px">
                                                    <input class="form-control" name="reTypePassword" type="password" value="" required>
                                                </div>
                                                </div>
                                            </div>
                                                <div class="row">
                                                       <div class="col-md-11" style="left: 10px">
                                                    <div class="password-btn">
                                                            <button type="submit" class="btn btn-block" style="color: white;background-color: #5ec329" id="form-submit">{{trans("common.confirm")}}</button>
                                                       </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


