@extends('frontend.layouts.default')

@section('title')
    Invite Friend Form
@endsection

@section('header')
    <link href="/custom/css/frontend/userRefer.css" rel="stylesheet" type="text/css" />
    <style>
        html:lang(cn) body, html:lang(cn) h3, .profile-userpic-upload, .submit-btn{
            font-family: 'Raleway', sans-serif !important;
        }
        .btn {
            border-radius: 6px;
            background-color: #5ec329;
            color: white;
             margin-left: 0px !important;
        }
        .setBRNTN {
            position: relative;
            left: 99px;
            width:120px;
            height:50px;
        }

        .setBNTN2 {
            border: 2px solid #5ec329 !important;
            background-color: #FFFFFF !important;
            border-radius: 6px !important;
            color: #5ec329 !important;
            margin-top: -7px;
            text-transform: capitalize;
            padding: 8px 14px 6px 14px;
            width: 85px !important;
        }
        .setBNTN1 {
            border: 2px solid #5ec329 !important;
            background-color: #5ec329 !important;
            border-radius: 6px !important;
            text-transform: capitalize !important;
            color: #FFFFFF !important;
            margin-top: -7px !important;
            padding: 8px 14px 6px 14px !important;
            width: 80px !important;
        }
        .ms-ctn {
            /*height: 50px !important;*/
            border: 2px solid #c1c1c1 !important;
        }
        .ms-ctn .ms-trigger {
            border-left: 2px solid #CCC;
        }
        #about {
            /*height: 50px !important;*/
            border: 2px solid #c1c1c1 !important;
        }
    </style>
@endsection

@section('content')
    <style>

        .portlet.light {
            padding: 0px 15px 15px;
            background-color: #fff;
        }
        #image{
            background-image: url('/images/invite_friend_with_logo.png');
            background-repeat:no-repeat;
            -moz-background-size: contain;
            -webkit-background-size: cover;
            -o-background-size: cover;
            background-size:100% auto;
            height:400px;
            width:100% !important;
        }
        .text-center {
            color: white;
            margin-top: 36%;
            margin-right: -21%;
        }

        #hft{
            font-size: 10px;
            margin-bottom:10px;
            color: white;
        }
        #legendsLair {
            font-size: 18px;
        }
        .text-span{
            font-family: Arial, Helvetica, sans-serif;
            font-size:18px;
            line-height: 0;
            margin: 20px;
            padding: 1px;
        }
        .user-pic {
            margin-top: -21px;
            margin-left: -10%;
        }
        #profileImage{
            width:143px;
            height:143px;
            margin-left:40%;
        }

        .pt35{
            width: 143px;
            margin-left: -20px;
            margin-top: -40px;
        }
        .setBlock{
            background-color: #EBE8E8;
            border-radius: 30px;
            height: auto;
            padding:20px;
            width:100%;
            margin-top:8px;
        }
        #setText{
            margin-top: -10px;
        }
        .fbIcon{
            border-radius: 5px;
            cursor: pointer;
        }
        #rowPadding{
            padding: 20px;
            margin-top: -20px;
        }
        #setfb{
            margin-top:30px;
            background-color:#EBE8E8;
            padding: 10px 0px 13px 261px;
        }

        form input {
            height: 50px !important;
            border: 2px solid #c1c1c1 !important
        }

        #span img{
            width:20px;
            height:20px;
        }
        #span{
            height:50px;
            border-radius: 4px;
            border:1px solid #c1c1c1;
            padding:9px;
        }

        .ms-ctn .ms-sel-item {
            background: #58AF2A;
            color: white;
            float: left;
            font-size: 12px;
            padding: 4px 7px;
            border-radius: 11px;
            border: 1px solid #DDD;
            margin: 1px 5px 1px 0;
        }

        .padding{
            padding: 0px 3px -1px 5px;
            margin-left:-16px;
        }
        .pA2{
            padding: 0px 3px -1px 5px;
            margin-left:-28px;
        }
        .setCol3{
            width: 112px;
            margin-left: 60px;
            margin-top: -20px;
        }

        .button{
            border-radius: 6px;
            border: 1px solid #5ec329;
            background-color: #5ec329;
            color:white;
            width:70px;
            height:28px;
            padding:5px;
            font-size:13px;
            margin-left:15px;
        }
        #hft{
            margin-top: -10px;
        }
        form .ms-ctn input {
            border: none !important;
            height: 1px;
            margin: -10px 0px 0px;
            display: none !important;
        }


        @media (max-width:1366px)
        {
            #setfb {
                margin-top: 30px;
                background-color: #EBE8E8;
                padding: 10px 0px 13px 260px;
            }
        }
        @media (max-width:1024px)
        {
            .text-center{
                color: white;
                margin-top: 37%;
                margin-right: -19%;
            }
            #setfb {
                margin-top: 30px;
                background-color: #EBE8E8;
                padding: 10px 0px 13px 195px;
            }
            .setCol3 {
                width: 112px;
                margin-left: 266px;
                margin-top: -20px;
            }

            .setCol9 {
                width: 100%;
                margin-top: 11px;
            }
        }


        @media (max-width:991px)
        {
            .text-center {
                margin-top: 18%;
                margin-right: 44%;
            }
            #legendsLair {
                font-size:18px;
            }
            .text-span{
                font-size:18px;
            }
            .setCol8{
                padding: 0px 13px 0px 91px;
            }


            #nameCome {
                padding: 0px 239px;
            }
            .user-pic {
                margin-top: -55px;
                margin-left: -10%;
            }

            #profileImage {
                width: 143px !important;
                margin-left: 44%;
                height: 143px !important;
            }
            .padding{
                padding: 0px 3px -1px 5px;
                margin-left:0px;
            }
            .pA2{
                padding: 0px 3px -1px 5px;
                margin-left:0px;
            }
        }

        @media (max-width:823px)
        {
            .text-center{
                margin-top: 18%;
                margin-right: 44%;
            }
            #legendsLair {
                font-size:18px;
            }
            .text-span{
                font-size: 15px;
            }
            #nameCome {
                padding: 0px 199px;
            }
            .page-header.navbar .menu-toggler.responsive-toggler {
                margin: -44.5px 56px 10px 7px;
            }
            .user-pic {
                margin-top: -19px;
                margin-left: 0%;
            }
            .setCol3 {
                padding: 0px 0px 8px 0px;
                margin-left: 220px;
            }

            #profileImage {
                width: 143px;
                margin-left: 37%;
                margin-top: -138px;
                height:143px;
            }
            #setfb {
                margin-top: 30px;
                background-color: #EBE8E8;
                padding: 10px 0px 13px 128px;
            }
            .setCol8 {
                padding: 0px 13px 0px 17px;
            }
            .setColbtn {
                padding: 6px 0px 0px 0px;
                margin-left: -182px;
            }
        }

        @media (max-width:767px)
        {
            .page-header.navbar .menu-toggler.responsive-toggler {
                margin: -103.5px 56px 10px 7px;
            }
        }
        @media (max-width:768px)
        {
            .text-center {
                margin-top: 18%;
                margin-right: 44%;
            }
            #hft {
                margin-top: -5px;
            }
            #legendsLair {
                font-size: 18px;
            }
            .text-span{
                font-family: Arial, Helvetica, sans-serif;
                font-size:16px;
            }
            .setCol3 {
                padding: 0px 0px 10px 0px;
                margin-left: 193px;
            }
            .user-pic {
                margin-top: -31px;
                margin-left: 0%;
            }
            #nameCome {
                padding: 0px 180px;
            }

            #setfb {
                margin-top: 30px;
                background-color: #EBE8E8;
                padding: 10px 0px 13px 111px;
            }
        }

        @media (max-width:736px)
        {
            .text-center {
                margin-top: 18%;
                margin-right:44%;
            }

            #legendsLair {
                font-size: 18px;
            }
            .text-span{
                font-family: Arial, Helvetica, sans-serif;
                font-size: 14px;
            }
            .setCol3 {
                padding: 0px 0px 10px 0px;
            }
            .user-pic {
                margin-top: -34px;
                margin-left: 0%;
            }
            .setCol8 {
                padding: 0px 13px 0px 24px;
            }
            #nameCome {
                padding: 0px 164px;
            }
            #profileImage {
                width: 143px;
                margin-left: 36%;
                margin-top: -138px;
            }
            #setfb {
                margin-top: 30px;
                background-color: #EBE8E8;
                padding: 10px 0px 13px 101px;
            }

        }

        @media (max-width:731px)
        {
            .text-center {
                margin-top: 18%;
                margin-right: 44%;
            }

            #nameCome {
                padding: 0px 162px;
            }
            .user-pic {
                margin-top: -39px;
                margin-left: 0%;
            }
            .setCol8 {
                padding: 0px 13px 0px 24px;
            }
            #setfb {
                margin-top: 30px;
                background-color: #EBE8E8;
                padding: 10px 0px 13px 96px;
            }
            .setCol3 {
                padding: 0px 0px 10px 0px;
                margin-left: 172px;
            }

        }

        @media (max-width: 667px)
        {
            #legendsLair {
                font-size: 16px;
            }
            .text-span{
                font-family: Arial, Helvetica, sans-serif;
                font-size: 12px;
            }
            .user-pic {
                margin-top: -62px;
                margin-left: 0%;
            }
            #nameCome {
                padding: 0px 144px;
            }
            .setCol3 {
                padding: 0px 0px 10px 0px;
            }
            #setfb {
                margin-top: 30px;
                background-color: #EBE8E8;
                padding: 10px 0px 13px 76px;
            }
        }

        @media (max-width:640px)
        {
            .setCol3 {
                padding: 0px 0px 10px 0px;
                margin-top: 10px;
                margin-left: 147px;
            }
            #nameCome {
                padding: 0px 136px;
            }
            #setfb {
                margin-top: 30px;
                background-color: #EBE8E8;
                padding: 10px 0px 13px 70px;
            }
            #legendsLair {
                font-size: 15px;
            }
            .text-span{
                font-family: Arial, Helvetica, sans-serif;
                font-size:10px;
                margin: 14px;
            }
            .user-pic {
                margin-top: -71px;
                margin-left: 0%;
            }
            #profileImage {
                width: 143px;
                margin-left: 34%;
            }
            .setCol8 {
                padding: 0px 13px 0px 20px;
            }
            .text-center {
                margin-top: 18%;
                margin-right: 45%;
            }
        }

        @media (max-width:568px)
        {
            .setCol3 {
                padding: 0px 0px 10px 16px;
                margin-left: 113px;
            }
            #nameCome {
                padding: 0px 101px;
            }
            #profileImage {
                width: 143px;
                margin-left: 29%;
            }
            #legendsLair {
                font-size: 13px;
            }
            .text-span{
                font-family: Arial, Helvetica, sans-serif;
                font-size: 9px;
                margin-top:10px;
            }
            #hft {
                font-size: 8px;
                margin-bottom: 10px;
                color: white;
            }
            #setfb {
                margin-top: 30px;
                background-color: #EBE8E8;
                padding: 10px 0px 13px 45px;
            }

            .setCol8{
                padding: 0px 13px 0px 20px;
                margin-top: -22px;
            }
        }

        @media (max-width:446px)
        {
            .text-span {
                font-family: Arial, Helvetica, sans-serif;
                font-size: 8px;
                margin: 8px;
            }

        }

        @media (max-width:414px){
            #setfb {
                margin-top: 30px;
                background-color: #EBE8E8;
                padding: 10px 0px 13px 73px;
            }
            .text-span {
                /* margin-top: 6px; */
                padding: 0px;
                font-size: 8px;
                margin: 13px;
            }
            .setCol3 {
                padding: 0px 0px 10px 0px;
                margin-left: 119px;
            }
            #legendsLair {
                font-size: 10px;
            }
            #profileImage {
                width: 143px;
                margin-left: 26%;
                margin-top: -161px;
            }
            #hft {
                font-size: 6px;
                color: white;
            }
            #nameCome {
                padding: 0px 108px;
                font-size: 18px;
            }
            #setText{
                font-size:12px;
            }
            .fbIcon {
                border-radius: 5px;
                cursor: pointer;
                width: 149px;
            }
            .text-center {
                margin-top: 19%;
                margin-right: 44%;
            }
            .user-pic {
                margin-top: -77px;
                margin-left: 0%;
            }
            #main {
                width: 100% !important;
            }
            .page-header.navbar .menu-toggler.responsive-toggler {
                margin: -103.5px 32px 0px 3px;
            }
        }

        @media (max-width:411px){
            #setfb {
                margin-top: 30px;
                background-color: #EBE8E8;
                padding: 10px 0px 13px 77px;
            }
            .setCol3 {
                padding: 0px 0px 10px 0px;
            }

            .text-span {
                margin: 14px;
                padding: 0px;
                font-size: 8px;
            }
            #setText {
                font-size: 10px;
            }

            #profileImage {
                width: 143px;
                margin-left: 28%;
                margin-top: -161px;
            }

            #nameCome {
                padding: 0px 116px;
                font-size: 17px;
            }
            .fbIcon {
                border-radius: 5px;
                cursor: pointer;
                width: 149px;
            }


            .page-header.navbar .menu-toggler.responsive-toggler {
                margin: -103.5px 42px 10px 7px;
            }
        }

        @media (max-width:375px)
        {
            .text-span {
                margin: 10px;
                padding: 0px;
                font-size: 7px;
            }

            .setCol3 {
                padding: 0px 0px 10px 0px;
            }
            #nameCome {
                padding: 0px 104px;
                font-size: 17px;
            }
            #setfb {
                margin-top: 30px;
                background-color: #EBE8E8;
                padding: 10px 0px 13px 56px;
            }
            .page-header.navbar .menu-toggler.responsive-toggler {
                margin: -116.5px 42px 10px 7px;
            }
        }

        @media (max-width:360px)
        {
            .text-center {
                margin-top: 19%;
                margin-right: 46%;
            }
            #nameCome {
                padding: 0px 100px;
                font-size: 17px;
            }
            .page-header.navbar .menu-toggler.responsive-toggler {
                margin: -108.5px 26px 10px 7px;
            }

            #profileImage {
                width: 143px;
                margin-left: 25%;
                margin-top: -162px;
            }
            .setCol3 {
                padding: 0px 0px 10px 0px;
                margin-top: 10px;
                margin-left: 88px;
            }
            .setCol8 {
                padding: 0px 7px 0px 11px;
            }
            .text-span {
                margin: 11px;
                padding: 0px;
                font-size: 6px;
            }
            #setfb {
                margin-top: 30px;
                background-color: #EBE8E8;
                padding: 10px 0px 13px 27px;
            }
            .fbIcon {
                border-radius: 5px;
                cursor: pointer;
                width: 195px;
            }
        }
        @media (max-width: 320px){
            .ms-ctn {
                height: auto !important;
                border: 2px solid #c1c1c1 !important;
            }
            .setCol3 {
                padding: 0px 0px 10px 0px;
                margin-left: 72px;
            }

            #profileImage {
                width: 143px;
                margin-left: 14%;
                margin-top: -212px;
            }
            #nameCome{
                padding: 0px 68px;
            }
            .fbIcon {
                border-radius: 5px;
                cursor: pointer;
                width: 148px;
            }
            #setfb {
                margin-top: 30px;
                background-color: #EBE8E8;
                padding: 10px 0px 13px 32px;
            }
        }

        @media (max-width:991px) and (min-width:320px){
            #msHeaderSearchBar.ms-ctn {
                padding: 2px 23px;
                border-radius: 5px;
                border: 1px solid #e5e5e5;
                width: 100%;
                margin-left: 0%;
            }
            .fa-search.header-search-bar {
                position: absolute;
                 right: 10%;
                top: 30px;
                color: #5ec329;
                font-size: 18px;
            }
            .setICol9 .dropdown-menu {
                margin-top:5px;
            }
            .btn {
                margin-left: 0px !important;
                left:0px !important;
            }
            .setBRNTN{
                border-radius: 6px;
                width: 100% !important;
                height: 50px;
                background-color: #5ec329;
                color: white;
            }
        }

        @media (max-width:560px) and (min-width:415px)
        {
            .page-header.navbar .menu-toggler.responsive-toggler {
                margin: -126.5px 84px 10px 7px;
            }
        }

    </style>

    <div style="width:75%" id="main" class="container-fluid">
        <div class="portlet light bordered">
            <div class="row justify-content-md-center">
                <div class="col-md-12" id="image">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text-center">
                                    <h3 id="legendsLair"><b>{{ trans('common.legends_lairs') }}</b></h3>
                                <p id="hft">{{ trans('common.hall_of_telents') }}</p>
                                <p class="text-span">{{ trans("common.xenren_the_professional") }}</p>
                                <p class="text-span">{{ trans("common.paltform_for_freelancer_and") }}</p>
                                <p class="text-span">{{ trans("common.co_founders") }}</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="user-pic">
                    <div class="col-md-4">
                        <img id="profileImage" src="{{asset($user->img_avatar)}}" class="img-circle" class="img-avatar" alt="" onerror="this.src='/images/avatar_xenren.png'">
                    </div>
                    <div class="col-md-8 setCol8">
                            <h3 id="nameCome">{{$user->name}}</h3>
                        <div class="setBlock">
                            <span id="setText" >{{ trans("common.hy_i_think_this_great_platformis_just_for_you") }}</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" id="rowPadding">
                <div class="col-md-12" id="setfb">
                    <img src="{{asset('images/fb image.png')}}" alt=""  onclick="window .location.href='{{ route('facebook.login') }}'" class="fbIcon">
                </div>
            </div>
            <div align="center" style="margin-top:10px;">
                <i style="color:#a19e9e;">or create new account</i>
            </div>
            <br><br><br>
            <div class="row">

                <div class="col-md-3 setCol3">
                    <div class="form-group">
                        <div id="fb-root"></div>
                        <script>(function(d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s); js.id = id;
                                js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2&appId=776809885767161&autoLogAppEvents=1';
                                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                        <div class="profile-userpic">
                            <img src="{{asset('uploads/temp-user/' . $userData->image)}}" class="img-circle pt35" alt="" onerror="this.src='/images/avatar_xenren.png'">
                        </div>
                    </div>
                    <button type="button" class="button" onclick="$('.uploadAvatar').click()">UPLOAD</button>
                </div>

                <div class="col-md-9 setCol9">
                    <form role="form" method="POST" action="{{ route('inviteFriend.post') }}" class="form-horizontal" id="unique-registered-form" style="display: block;" enctype="multipart/form-data">
                        <input type="file" style="display: none" class="uploadAvatar" name="avatar" value="{{asset($userData->image)}}">
                        <input type="text" style="display: none" class="uploadAvatar" name="old-avatar" value="{{($userData->image)}}">
                        {!! csrf_field() !!}
                        <input type="hidden" name="refer_id" value="{{$userData->refer_by}}">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="email">{{trans("common.name")}}</label>
                            <div class="col-md-9 col-sm-10">
                                <input id="name" name="name" value="{{$userData->name}}" type="text" class="form-control" placeholder="{{trans("common.please_enter_your_name")}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="about">{{ trans("common.about") }}</label>
                            <div class="col-md-9 col-sm-10">
                                <textarea id="about" rows="8" name="about" type="text" class="form-control" placeholder="{{trans('common.please_tell_about_yourself')}}" style="resize: none">{{ $userData->about }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="skill">{{ trans("common.skill") }}</label>
                            <div class="col-md-9 col-sm-10 setICol9">
                                <select placeholder="{{trans("common.enter_your_skills")}}" name="skill" id="skill" class="form-control">
                                    @foreach($skill as $k =>  $v)
                                        <option value="{{$v['id']}}" name="skill"  @if(in_array($v['id'], $userData['skills'])) selected @endif>
                                            {{$v['name_en']}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="phone">{{ trans("common.phone") }}</label>
                            <div class="col-md-9 col-sm-10">
                                <input name="phone" id="phone" value="{{ $userData->phone_number }}" type="text" class="form-control" placeholder="{{ trans("common.enter_your_phone") }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="about">{{ trans("common.email") }}</label>
                            <div class="col-md-9 col-sm-10">
                                <input name="email" id="email" value="{{ $userData->email }}" type="email" class="form-control" placeholder="{{ trans("common.enter_your_email") }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="language">{{ trans("common.language") }}</label>
                            <div class="col-md-9 col-sm-10 setICol9">
                                <div id="magicsuggest"></div>
                                <select placeholder="{{trans("common.enter_your_language")}}" name="language" id="language" class="form-control">
                                    @foreach($languages as $k =>  $v)
                                        <option value="{{$v['id']}}" @if(in_array($v['id'], $userData['language'])) selected @endif>
                                            {{$v['name']}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group marginl">
                            <div class="row col-md-offset-2 set">
                                <div class="col-md-6 padding">
                                    <div class="col-md-2 cmd2">
                                        <span class="input-group-addon" id="span"><img src="/images/skype-icon-5.png"></img></span>
                                    </div>
                                    <div class="col-md-10 clr">
                                        <input name="skype" id="skype" type="text" class="form-control" placeholder="{{trans('common.enter_skype')}}">
                                    </div>
                                </div>
                                <div class="col-md-6 pA2">
                                    <div class="col-md-2 cmd2">
                                        <span class="input-group-addon" id="span"><img src="/images/line.png"></img></span>
                                    </div>
                                    <div class="col-md-10 clr">
                                        <input name="line" id="line" type="text" class="form-control" placeholder="{{trans("common.enter_qq")}}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group marginl">
                            <div class="row col-md-offset-2 set">
                                <div class="col-md-6 padding">
                                    <div class="col-md-2 cmd2">
                                        <span class="input-group-addon" id="span"><img src="/images/wechat-logo.png"></img></span>
                                    </div>
                                    <div class="col-md-10 clr">
                                        <input name="wechat" id="wechat" type="text" class="form-control" placeholder="{{trans("common.enter_wechat")}}">
                                    </div>
                                </div>
                                <div class="col-md-6 pA2">
                                    <div class="col-md-2 cmd2">
                                        <span class="input-group-addon" id="span"><img src="/images/MetroUI_Phone.png"></img></span>
                                    </div>
                                    <div class="col-md-10 clr">
                                        <input name="phone" id="phone_no" type="text" class="form-control" placeholder="{{trans("common.enter_phone")}}" value="{{ $userData->phone_number }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="type" value="{{$type}}">

                        <div class="form-group">
                            <div class="col-md-12">
                                <button class="btn submit-btn setBRNTN" id=unique-submit-name" >{{trans("common.submit")}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('footer')
    <link href="assets/magicsuggest/magicsuggest-min.css" rel="stylesheet">
    <script src="assets/magicsuggest/magicsuggest-min.js"></script>
    <script>
        {{--var skill = '{!!  $userData->skills !!}';--}}
                {{--var lang = '{{ $userData->language }}';--}}
                {{--console.log(skill);--}}
            +function () {
            var sk = $('#skill').magicSuggest({
            });
            var lg = $('#language').magicSuggest({

            });

        }(jQuery);
    </script>
@endsection