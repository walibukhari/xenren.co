@extends('frontend.layouts.default')

@section('title')
    User Refer link
@endsection

@section('header')
    <link href="/custom/css/frontend/userRefer.css" rel="stylesheet" type="text/css" />
    <style>
        .color-class{
            color: #c1c1c1;
        }
        .ms-ctn .ms-sel-item {
            background: #58AF2A;
            color: white;
            float: left;
            font-size: 12px;
            padding: 4px 7px;
            border-radius: 11px;
            border: 1px solid #DDD;
            margin: 1px 5px 1px 0;
        }
        .ms-ctn input {
            border: none !important;
        }
        .setImageAvater{
            width: 100px;height: 100px; border-radius: 50%;
        }
    </style>
@endsection

@section('content')
    <div class="page-content text-center" style="min-height:719px">
        <div class="inner-content-refer">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light bordered">
                        <div class="row">
                            {{--<div class="caption text-center">--}}
                                {{--<h2 style="font-family: 'Montserrat', sans-serif !important; color: #5ec329">Help your friend to fill and invite to join!</h2>--}}
                                {{--<h3 style="font-family: 'Montserrat', sans-serif !important; color: #5ec329;font-weight: bold; margin-top: 5px"> At the same time earn xencoins </h3>--}}
                            {{--</div>--}}

                            <div class="portlet-body personal-info">
                                <div class="portlet-body form" id="displayImage">
                                    <div id="identity-alert-container"></div>
                                    <div class="col-md-2 col-sm-3 setCol2UseRefer">
                                        <div class="form-group">
                                            <div class="profile-userpic">
                                                {{--<img src="{{asset('images/avatar_xenren.png')}}" class="img-avatar" alt="" style="width: 100px;height: 100px">--}}
                                                <img :src="avater" class="img-avatar setImageAvater" onerror="this.src='/images/avatar_xenren.png'">
                                                {{--<input type="file" style="display: none" class="uploadAvatar" name="avatar">--}}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button type="button" class="profile-userpic-upload setPtofileBtnUR"
                                                    onclick="$('.uploadAvatar').click()">{{trans('common.upload')}}</button>
                                        </div>
                                    </div>
                                    <div class="col-md-10 col-sm-9 setCol9URP">
                                        <div class="row">
                                            <div class="caption text-center">
                                                <h2 style="font-family: 'Montserrat', sans-serif !important; color: #5ec329">{{trans('common.help_your_friend')}}!</h2>
                                                <h3 style="font-family: 'Montserrat', sans-serif !important; color: #5ec329;font-weight: bold; margin-top: 5px">{{trans('common.at_same_time')}}</h3>
                                            </div>
                                        </div>
                                        <form role="form" method="POST" action="{{ route('refer.post') }}" class="form-horizontal" id="login-form" style="display: block;"  >
                                            {{--<input type="file" style="display: none" class="uploadAvatar" name="avatar">--}}
                                            <input type="file"  name="avatar" style="display: none"  class="uploadAvatar"  @change="OnfileSelected"/>
                                            {!! csrf_field() !!}
                                            <div class="form-body user-refer setFormBodyUserRefer">
                                                    <div class="form-group">
                                                        <label class="col-md-2 col-sm-3 col-xs-3 control-label color-class setGCol">{{trans('common.name')}}</label>
                                                        <div class="col-md-10 col-sm-9 col-xs-9 setGCol">
                                                            <input id="name" name="name" value="" type="text" class="form-control" placeholder="{{trans('common.enter_name')}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 col-sm-3 col-xs-3 control-label color-class setGCol">{{trans('common.about')}}</label>
                                                        <div class="col-md-10 col-sm-9 col-xs-9 setGCol">
                                                            <textarea id="about" rows="8" name="about" type="text" class="form-control" placeholder="{{trans('common.please_tell_your_self')}}" style="resize: none"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 col-sm-3 col-xs-3 control-label color-class setGCol">{{trans('common.skill')}}</label>




                                                        <div class="col-md-10 col-sm-9 col-xs-9 setGCol setSelect2Border">
                                                            <div id="magicsuggest"></div>
                                                            {{--{{ Form::hidden('skill_id_list', \Auth::user()->getUserSkillList(), ['id'=>'skill_id_list']) }}--}}
                                                            <select name="skill" id="skill" class="form-control" >
                                                                @foreach($skill as $k =>  $v)
                                                                    <option value="{{$v['id']}}" name="skill">
                                                                        {{$v['name_en']}}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 col-sm-3 col-xs-3 control-label color-class setGCol">{{trans('common.phone')}}</label>
                                                        <div class="col-md-10 col-sm-9 col-xs-9 setGCol">
                                                            <input id="phone" name="phone" type="text" class="form-control" placeholder="{{trans('common.enter_your_phone')}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 col-sm-3 col-xs-3 control-label color-class setGCol">{{trans('common.email')}}</label>
                                                        <div class="col-md-10 col-sm-9 col-xs-9 setGCol">
                                                            <input name="email" type="email" class="form-control"
                                                                   placeholder="{{trans('common.enter_your_email')}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 col-sm-3 col-xs-3 control-label color-class setGCol">{{trans('common.languageaa')}}</label>
                                                        <div class="col-md-10 col-sm-9 col-xs-9 setGCol setSelect2Border">
                                                            <select name="language" id="language" class="form-control">
                                                                @foreach($languages as $k =>  $v)
                                                                    <option value="{{$v['id']}}" name="language">
                                                                        {{$v['name']}}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                            <div class="row user-refer-btn">
                                                                <div class="col-md-5 col-sm-12">
                                                                    <button type="button" class="btn submit green-btn" id="generate_link" >{{trans('common.generate_link')}}</button>
                                                                </div>
                                                                <div class="col-md-5 col-sm-12">
                                                                    <button type="button" class="btn submit white-btn" id="submit-invite">{{trans('common.post_his_email')}}</button>
                                                                </div>
                                                                <div class="col-md-2"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- END PAGE BASE CONTENT -->
    </div>
@endsection


@section('footer')
    <link href="assets/magicsuggest/magicsuggest-min.css" rel="stylesheet">
    <script src="assets/magicsuggest/magicsuggest-min.js"></script>
    <script src="{{asset('js/vue.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vue-resource.min.js')}}" type="text/javascript"></script>
    <script>

        var vm = new Vue ({
            el: '#displayImage',

            props: {},
            data: {
                debug: true,
                file:'',
                avater:''
            },
            methods:{
                OnfileSelected(e){
                    console.log(e.target.files[0]);
                    let image = e.target.files[0];
                    this.avater = image;
                    this.file = this.avater;
                    let fileReader = new FileReader();
                    fileReader.readAsDataURL(image);
                    fileReader.onload = e => {
                        console.log(e);
                        this.avater = e.target.result;
                    }
                },
            },

            mounted: function () {
                console.log('mounted is ready');
            }

        });


        $('#skill').magicSuggest({
            placeholder: '{{trans('common.select_skill')}}',
        });

        $('#language').magicSuggest({
            placeholder: '{{trans('common.select_language')}}',
        });


        +function () {
            $('#generate_link').on('click',function () {
                  var data = $("#login-form").serialize();
                 var generateLink = {
                        "async": true,
                        "crossDomain": true,
                        "url": "{{url('/generateLink')}}",
                        "method": "POST",
                        "data": data
                    };
                 console.log("generateLink");
                 console.log(generateLink);
                 console.log("generateLink");
                    $.ajax(generateLink).done(function (response) {
                        var status = response.status;

                        var link = response.link;

                        window.prompt("Copy the Following Link to send Your Friend Invitation link", link);

                });
            });

            $(document).ready(function () {
                $('#skill').magicSuggest({});
                $('#language').magicSuggest({});

                $('#login-form').makeAjaxForm({
                    submitBtn: '#submit-invite',
                    errorFunction: function (response, $el, $this) {
                        console.log('errorFunction');
                        $('.blockUI').remove()
                    },
                    afterSuccessFunction: function (response, $el, $this) {
                        console.log('afterSuccessFunction');
                        $('.blockUI').remove()
                    }
                });
            });
        }(jQuery);
    </script>

@endsection