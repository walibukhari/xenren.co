@extends('frontend.layouts.default')

@section('title')
    Invite Friend Form
@endsection

@section('header')
    <link href="/custom/css/frontend/userRefer.css" rel="stylesheet" type="text/css" />
    <style>
        html:lang(cn) body, html:lang(en) h3, .profile-userpic-upload, .submit-btn{
            font-family: 'Raleway', sans-serif !important;
        }
        .color-class{
            color: #c1c1c1;
        }
        .portlet.light {
            padding: 0px 15px 15px;
            background-color: #fff;
        }
        .text-span{
            font-size: 20px;
            color: white
        }
        .col-md-12{
            background-image: url('/images/invite-friend-1.png');
            background-repeat:no-repeat;
            -moz-background-size: contain;
            -webkit-background-size: cover;
            -o-background-size: cover;
        }
        @media screen and (max-width: 420px){
            #main{
                width: 100%;
            }
        }
        @media screen and (max-width: 375px){
            #main{
                width: 100%;
            }
        }
        .ms-ctn .ms-sel-item {
            background: #58AF2A;
            color: white;
            float: left;
            font-size: 12px;
            padding: 4px 7px;
            border-radius: 11px;
            border: 1px solid #DDD;
            margin: 1px 5px 1px 0;
        }

        .ms-ctn input {
            border: none !important;
        }
    </style>
@endsection

@section('content')
<div class="text-center" >
    <img src="/images/fav.png" alt="">
</div>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3" style="margin-top: 40px">
            <div class="portlet light bordered">
                <div class="text-center">
                    <img src="/images/acceptinvitation.png" alt="" style="margin-top: 25px">
                </div>
                <div class="text-center" style="margin-top: 40px">
                    <span>{{trans("common.you_have_accepted_your")}}</span><br>
                    <span>{{trans("common.friend_invitation")}}</span>
                </div>
                <div class="text-center">
                    <p style="font-weight: bold">{{trans("common.please_check_your_mail_box_to_verify_your_email")}}</p>
                </div>
                <br><br><br><br>
            </div>
        </div>
    </div>
</div>
@endsection