<div class="form-horizontal">
    <div class="form-body">
        <div class="form-group">
            <div class="col-md-12 text-center">
                <img class="setiAIMG" data-toggle="modal" data-target="#mySucessAuthModal" src="{{asset('images/logoIdentitication.png')}}">
                <br>
                <label class="control-label setSuccessLabel">
                    {{trans('common.successful_authentication')}}
                </label>
                <br>
                <br>
                {{--<a href="{{ route('frontend.identityauthentication.delete') }}" class="btn setbtnReAuth">{{trans("common.re_authenticated")}}</a>--}}
                <a href="Javascript:;" onclick="" class="btn setbtnReAuth reauth">{{trans("common.re_authenticated")}}</a>
                <br/><br/>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="mySucessAuthModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Identity Cards</h4>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="col-md-12">
                        <div class="col-md-6 text-right">
                            <img class="setgetImage" src="{{ $identity->id_image }}">
                        </div>
                        <div class="col-md-6">
                            <img class="setgetImage" src="{{$identity->hold_id_image}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>