<div id="resubmit-container">
    <div class="container" style="width: 100%">
        <div class="col-md-12">
            <div class="col-md-6 text-right">
                <img class="setgetImage" src="{{ $identity->id_image }}">
            </div>
            <div class="col-md-6">
                <img class="setgetImage" src="{{$identity->hold_id_image}}">
            </div>
        </div>
    </div>
    <div class="form-horizonta  l" >
        <div class="form-body container" style="width: 100%">
            <div class="form-group">
                <div class="col-md-12 text-center">
                    <div class="checkbox-list">
                        <label class="checkbox-inline setwaiting">
                            {{ trans('member.waiting_approval') }}
                        </label>
                    </div>
                    <a href="{{ route('frontend.identityauthentication.delete') }}" class="btn red-sunglo">{{trans('member.resubmit_authentication')}}</a>
                    {{--<button type="button" class="btn red-sunglo" id="resubmit-identity">{{ trans('member.resubmit_authentication') }}</button>--}}
                </div>
            </div>
        </div>
    </div>
</div>
