@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.identity_authentication_title') }}
@endsection

@section('keywords')
    {{ trans('common.identity_authentication_keyword') }}
@endsection

@section('description')
    {{ trans('common.identity_authentication_desc') }}
@endsection

@section('author')
    Xenren
@endsection

@section('url')
    https://www.xenren.co/identityAuthentication
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
    <link href="/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/select2/css/select2.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/custom/css/frontend/identityAuthentication.css" rel="stylesheet" type="text/css" />
    <style>
        .text-center-display{
            display: flex;
            justify-content: center;
        }
        .display_hard_inline{
        }
        .display_hard_none{
            display: none !important;
        }
    </style>
    <style>
        .lds-ring {
            display: inline-block;
            position: relative;
            width: 20px;
            height: 20px;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 20px;
            height: 20px;
            margin: 1px;
            border: 2px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
@endsection

@section('content')
    <style>
        .input-medium {
            width: 100% !important;
        }
    </style>
    <div class="row setCOLROW">
        <div class="col-md-3 search-title-container">
            <span class="find-experts-search-title text-uppercase setMAinMENU">{{trans('common.main_action')}}</span>
        </div>
        <div class="col-md-9">
            <span class="find-experts-search-title text-uppercase setAUTHENTICATION">{{trans('common.authentication')}}</span>
        </div>
    </div>

    <!-- BEGIN CONTENT -->
    <div id="userIdentity" class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
        @include('frontend.flash')
        <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row setPageContent">
                <div class="col-md-12 col-sm-12">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-blue bold uppercase setLINEHIA">{{ trans('common.identity_authentication') }}</span>
                                <!-- <p>Please fill out the fields, and then <br />click the submit button</p> -->
                                <p> {{ trans('common.please_fill_click_submit') }} </p>
                            </div>
                        </div>
                        <div class="portlet-body personal-info setPortletBodyPI">
                            <div class="portlet-body form setportletBodyForm">
                                <div id="identity-alert-container"></div>
                                @if ($identity && $identity->isPending() && $identity->is_submit == 1 )
                                    @include('frontend.identityAuthentication.pending')
                                @elseif ($identity && $identity->isApproved())
                                    @include('frontend.identityAuthentication.approved')
                                @elseif ($identity && $identity->isRejected())
                                    @include('frontend.identityAuthentication.rejected')
                                @endif

                                {!! Form::open(['route' => ['api.identityauthentication.submit'], 'id' => 'submit_form', 'method' => 'post', 'class' => 'form-horizontal', 'files' => true,
                                'style' => $identity->isNullStatus() == false && ($identity->isApproved() ||  $identity->isRejected() ) ? 'display: none;' : 'display: block;']) !!}
                                <div class="form-body">

                                    <div class="form-group">
                                        <label class="col-md-3 control-label set-name-title-area">{{ trans('member.real_name') }}</label>
                                        <div class="col-md-8">
                                            <input id="real_name" name="real_name" value="{{ $identity ? $identity->name : '' }}" type="text" class="form-control" placeholder="{{ trans('member.phd_real_name') }}"><br /><br />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label set-name-title-area">{{ trans('member.id_card_no') }}</label>
                                        <div class="col-md-8">
                                            <input id="id_card_no" name="id_card_no" value="{{ $identity ? $identity->id_card_no : '' }}" type="text" class="form-control" placeholder="{{ trans('member.phd_id_card_no') }}"><br /><br />
                                        </div>
                                    </div>
                                    <div class="form-group setGenderBox">
                                        <label class="col-md-3 control-label set-name-title-area">{{ trans('member.gender') }}</label>
                                        <div class="col-md-9">
                                            <div class="xc-radio-btn">
                                                <input type="radio" name="gender" id="male" value="0" {{  $identity && $identity->gender == 0 ? 'checked = checked' : '' }}>
                                                <label for="male">{{ trans('member.male') }}</label>
                                            </div>

                                            <div class="xc-radio-btn">
                                                <input type="radio" name="gender" id="female" value="1" {{  $identity && $identity->gender == 1 ? 'checked = checked' : '' }}>
                                                <label for="female">{{ trans('member.female') }}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group setBirthDay">
                                        <label class="col-md-3 control-label set-name-title-area">{{ trans('member.birth_date') }}</label>
                                        <div class="col-md-8">
                                            <div class="input-group input-medium date date">
                                                <input id="date_of_birth" name="date_of_birth" value="{{ $identity ? $identity->date_of_birth : '' }}" type="text" class="form-control" readonly>
                                                <span class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </span>
                                            </div><br /><br />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label set-name-title-area">{{ trans('member.address') }}</label>
                                        <div class="col-md-8">
                                            <input id="address" name="address" value="{{ $identity ? $identity->address : '' }}"  type="text" class="form-control" placeholder="{{ trans('member.phd_address') }}"><br /><br />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label set-name-title-area">{{ trans('member.handphone') }}</label>
                                        <div class="col-md-8">
                                            <input id="handphone_no" name="handphone_no" value="{{ $identity ? $identity->handphone_no : '' }}" type="text" class="form-control" placeholder="{{ trans('member.phd_handphone') }}"><br /><br />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label set-name-title-area">{{ trans('common.country') }}</label>
                                        <div class="col-md-8">
                                            <select class="form-control" name="country_id" id="country">

                                            </select><br /><br />
                                        </div>
                                    </div>

                                    <div class="form-group setCardBarcode">
                                        <label for="exampleInputFile" class="col-md-3 control-label upload-label">
                                            {{ trans('member.id_card_upload') }}
                                        </label>
                                        <div class="col-md-3">
                                            <label class="control-label id-card">
                                                {{ trans('member.front_id_card') }}
                                            </label>
                                            <br/>
                                            <div class="fileinput @if($identity && $identity->getIdImage() != null) fileinput-exists @else fileinput-new @endif input-layout" data-provides="fileinput">
                                                {{--<span class="set-cross-identityAuth">--}}
                                                    {{--<img src="{{asset('images/crossimage.png')}}" class="frontCard">--}}
                                                {{--</span>--}}
                                                <div class="fileinput-new thumbnail id-photo setFILEINPUTAUTHNP frontCardDefaultImage" >
                                                    <img class="changeImage" src="/images/idCard_image.png"/>
                                                    <div id="results_front_image-a">
                                                    </div>
                                                </div>
                                                <div class="img_front_id fileinput-preview fileinput-exists thumbnail setFILEINPUTEAUTHNP frontCardImage">
                                                    @if ($identity && $identity->getIdImage() != null)
                                                        <img class="changeImage" src="{{ $identity->getIdImage() }}" id="set-image-padding-identityAuth-1 frontCard" alt="" />
                                                        <div id="results_front_image-a1">
                                                        </div>
                                                    @endif
                                                    {{--there is no way to upload the picture without select due to security problem--}}
                                                    {{--even you can show previous but you can retrieve the value so better upload a new pic--}}
                                                </div>
                                                <div class="text-center">
                                                            <span class="btn browse-btn btn-file">
                                                                <span class="fileinput-new"> {{ trans('member.select_img') }} </span>
                                                                <span class="fileinput-exists"> {{ trans('member.change') }} </span>
                                                                <input type="file" name="id_image">
                                                            </span>
                                                    {{--<a href="javascript:;" class="btn red-sunglo fileinput-exists browse-btn frontCard" --}}{{--data-dismiss="fileinput"--}}{{--> {{ trans('member.remove') }} </a>--}}
                                                </div>
                                                <div class="text-center">
                                                    <span>or</span>
                                                </div>
                                                <div class="text-center">
                                                    <div @click="openCameraModal('a')" class="fileinput-new btn browse-btn btn-file">
                                                            <span> Take a Picture </span>
                                                    </div>
                                                    <div @click="openCameraModal('a1')" class="fileinput-exists btn browse-btn btn-file">
                                                        <span> Take a Picture </span>
                                                    </div>
                                                </div>
                                            </div>
                                            @if($identity && $identity->status == \App\Models\UserIdentity::STATUS_PENDING)
                                                <div class="identityStatus identityStatusPending">
                                                    <i class="fa fa-refresh set-refresh-icon" aria-hidden="true"></i>
                                                    {{trans('common.pending')}}</div>
                                            @elseif($identity && $identity->status == \App\Models\UserIdentity::STATUS_APPROVED)
                                                <div class="identityStatus identityStatusApproved">
                                                    <i class="fa fa-check set-fa-facheck" aria-hidden="true"></i>
                                                    {{trans('common.approved')}}</div>
                                            @elseif($identity && $identity->status == \App\Models\UserIdentity::STATUS_REJECTED)
                                                <div class="identityStatus identityStatusRejected">{{trans('common.rejected')}}</div>
                                            @endif
                                        </div>
                                        <div class="col-md-1 set-col-md-1-identity-auth-divider">

                                        </div>
                                        <div class="col-md-3">
                                            <label class="control-label id-card">
                                                {{ trans('common.scan_qr_code_upload_id') }}
                                            </label>
                                            <br/>
                                            <div class="fileinput fileinput-new qrcode-layout" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail qrcode-box setImage1 setIAUTHPAGEQRC">
                                                    <img src="{{ $qrLink }}" alt="" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group setCardBarcode2">
                                        <label for="exampleInputFile" class="col-md-3 control-label upload-label">
                                            {!! trans('member.holding_id_card') !!}
                                        </label>
                                        <div class="col-md-3">
                                            <label class="control-label id-card">
                                                {{ trans('member.front_id_card') }}
                                            </label>
                                            <br/>
                                            <div class="fileinput @if($identity && $identity->getHoldIdImage() != null) fileinput-exists @else fileinput-new @endif input-layout" data-provides="fileinput">
                                                {{--<span class="set-cross-identityAuth">--}}
                                                     {{--<img src="{{asset('images/crossimage.png')}}" class="backCard">--}}
                                                {{--</span>--}}
                                                <div class="fileinput-new thumbnail id-photo setAUTHPAGENT backCardDefaultImage">
                                                    <img style="display: inline-block;" class="changeImage2" src="/images/holdIdCard_image.png" />
                                                    <div id="results_back_image-b">
                                                    </div>
                                                </div>
                                                @if ($identity && $identity->getHoldIdImage() != null)
                                                <div class="img_front_id fileinput-preview fileinput-exists thumbnail setAUTHPAGENT backCardImage">
                                                        <img style="display: inline-block"  src="{{ $identity->getHoldIdImage() }}" class="changeImage2 set-image-padding-identityAuth backcard" alt="" />
                                                    <div id="results_back_image-b1">
                                                    </div>
                                                </div>
                                                @endif
                                                <div class="text-center">
                                                            <span class="btn browse-btn btn-file">
                                                                <span class="fileinput-new"> {{ trans('member.select_img') }} </span>
                                                                <span class="fileinput-exists"> {{ trans('member.change') }} </span>
                                                                <input type="file" class="id_image" name="hold_id_image">
                                                            </span>
                                                </div>
                                                <div class="text-center">
                                                    <spna>or</spna>
                                                </div>
                                                <div class="text-center">
                                                    <div @click="openCameraModal('b')" class=" fileinput-new btn browse-btn btn-file">
                                                        <span> Take a Picture </span>
                                                    </div>
                                                    <div @click="openCameraModal('b1')" class="fileinput-exists btn browse-btn btn-file">
                                                        <span> Take a Picture </span>
                                                    </div>
                                                    {{--<a href="javascript:;" class="btn red-sunglo fileinput-exists browse-btn backCard" --}}{{--data-dismiss="fileinput"--}}{{--> {{ trans('member.remove') }} </a>--}}
                                                </div>
                                            </div>
                                            @if($identity && $identity->status == \App\Models\UserIdentity::STATUS_PENDING)
                                                <div class="identityStatus identityStatusPending">
                                                    <i class="fa fa-refresh set-refresh-icon" aria-hidden="true"></i>
                                                    {{trans('common.pending')}}</div>
                                            @elseif($identity && $identity->status == \App\Models\UserIdentity::STATUS_APPROVED)
                                                <div class="identityStatus identityStatusApproved">
                                                    <i class="fa fa-check set-fa-facheck" aria-hidden="true"></i>
                                                    {{trans('common.approved')}}</div>
                                            @elseif($identity && $identity->status == \App\Models\UserIdentity::STATUS_REJECTED)
                                                <div class="identityStatus identityStatusRejected">{{trans('common.rejected')}}</div>
                                            @endif
                                        </div>
                                        <div class="col-md-1 set-col-md-1-identity-auth-divider">

                                        </div>
                                        <div class="col-md-3">
                                            <label class="control-label id-card">
                                                {{ trans('common.scan_qr_code_upload_id') }}
                                            </label>
                                            <br/>
                                            <div class="fileinput fileinput-new qrcode-layout" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail qrcode-box setImage2 setAUTHPAGENT">
                                                    <img src="{{ $qrLink }}" alt="" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <br/><br/><br/>
                                    <div class="form-group">
                                        <div class="col-md-12 text-center" style="display: flex;justify-content: center">
                                            <button style="display: flex;justify-content: center;align-items: center;" type="button" class="btn submit setSUBMITBTN" id="identity-submit-btn">
                                                <i class="idSpanIdentityAuth fa fa-check"></i>
                                                <span class="idSpanIdentityAuth">{{ trans('member.submit_for_approval') }}</span>
                                                <div style="display:none;" class="lds-ring">
                                                    <div></div><div></div><div></div><div></div>
                                                </div>
                                            </button>
                                            <br/><br/><br/><br/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="user_identity_id" id="user_identity_id" value="{{ $identity? $identity->id: 0 }}">
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->

        <!-- camera modal -->
        <div id="openCamera" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Take Picture</h4>
                    </div>
                    <div class="modal-body">
                        <div class="text-center-display">
                            <div id="my_camera" @click="openCamera">
                            </div>
                        </div>
                        <div v-if="this.a === 'a'">
                            <button @click="takeSnapshot('a')">
                                Take a Picture
                            </button>
                        </div>
                        <div v-else>
                            <button @click="takeSnapshot('a1')">
                                Take a Picture
                            </button>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>


        <!-- back side card camera -->
        <div id="openCamera2" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Take Picture</h4>
                    </div>
                    <div class="modal-body">
                        <div class="text-center-display">
                            <div id="my_camera2" @click="openCamera2">
                            </div>
                        </div>
                        <div v-if="this.b == 'b'">
                            <button @click="takeSnapshot2('b')">
                                Take a Picture
                            </button>
                        </div>
                        <div v-else>
                            <button @click="takeSnapshot2('b1')">
                                Take a Picture
                            </button>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- END CONTENT -->
@endsection


@section('footer')
    <script>
			var lang = {
				'select_country' : '{{ trans('common.select_country') }}'
			};

			var url = {
				'api_identity_authentication_change' : '{{ route('api.identityauthentication.change') }}',
				'api_countries' : '{{ url('api/countries') }}'
			}
    </script>
    <script src="/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    @if (app()->getLocale() == 'cn')
        <script src="/assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-CN.min.js" type="text/javascript"></script>
        <script src="/assets/global/plugins/select2/js/i18n/zh-CN.js" type="text/javascript"></script>
    @endif
{{--    <script src="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>--}}
    <script>
			var countryPlaceHolder = "{{isset($identity->country) && isset($identity->country->name) ? $identity->country->name : trans('common.select_country')}}";
			window.successMessage = "{{trans('member.successfully_submit_authentication')}}";
    </script>
    <script src="/custom/js/frontend/identityAuth.js" type="text/javascript"></script>
    @if ($identity && $identity->isApproved())
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    @endif
    <script src="{{asset('webCam/webcam.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.11"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.5.1/vue-resource.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script>
        window.camera_on = false;
        new Vue({
            el: '#userIdentity',
            data: {
                showCamera: true,
                camera_on: false,
                start: '1',
                b:'',
                b1:'',
                a:'',
                a1:'',
                hold_Id_Image:'',
                front_id_image:''
            },
            methods: {
                openCameraModal: function (val) {
                    if(val === 'a') {
                        this.a = 'a';
                        $('#openCamera').modal('show');
                        this.openCamera('a');
                    } if(val === 'a1') {
                        $('#openCamera').modal('show');
                        this.openCamera('a1');
                        this.a1 = 'a1';
                    } if(val === 'b') {
                        $('#openCamera2').modal('show');
                        this.openCamera2('b');
                        this.b = 'b';
                    } if(val === 'b1') {
                        $('#openCamera2').modal('show');
                        this.openCamera2('b1');
                        this.b1 = 'b1';
                    }
                },
                openCamera: function () {
                    $('#openCamera').modal('show');
                    Webcam.set({
                        width: 490,
                        height: 390,
                        image_format: 'jpeg',
                        jpeg_quality: 100,
                        focused:true
                    });
                    Webcam.attach( '#my_camera' );
                },
                openCamera2: function () {
                    $('#openCamera2').modal('show');
                    Webcam.set({
                        width: 490,
                        height: 390,
                        image_format: 'jpeg',
                        jpeg_quality: 100,
                        focused:true
                    });
                    Webcam.attach( '#my_camera2' );
                },
                stopCamera: function(){
                    Webcam.reset();
                },
                takeSnapshot: function (val) {
                    let that = this;
                    that.loaders = true;
                    Webcam.snap( function(data_uri) {
                        $('.changeImage').hide();
                        // $(".changeImage").val(data_uri);
                        let file = dataURLtoFile(data_uri);
                        console.log('check file name');
                        console.log(file);
                        that.front_id_image = file;
                        document.getElementById('results_front_image-'+val).innerHTML = '<img style="  ' +
                            'display: block;\n' +
                            'border-radius: 6px;\n' +
                            'width: 198px;\n' +
                            'height: 131px;\n' +
                            'margin-left: auto;\n' +
                            'margin-right: auto;" id="captureImage1" src="'+data_uri+'"/>';
                    });

                    function dataURLtoFile(dataurl, filename) {
                        var arr = dataurl.split(','),
                            mime = arr[0].match(/:(.*?);/)[1],
                            bstr = atob(arr[1]),
                            n = bstr.length,
                            u8arr = new Uint8Array(n);
                        while(n--){
                            u8arr[n] = bstr.charCodeAt(n);
                        }
                        return new File([u8arr], filename, {type:mime});
                    }
                    this.stopCamera();
                    this.idImage(this.front_id_image);
                    $('#openCamera').modal('hide');
                },

                idImage: function(file){
                    console.log('going to  inset front image');
                    var id = $('#user_identity_id').val();
                    var image = file;
                    var formData = new FormData();
                    formData.append('id_image', image);
                    formData.append('column', 'id_image');
                    formData.append('id', id);
                    $('.frontCardDefaultImage').hide();
                    $('.frontCardImage').show();

                    $.ajax({
                        url: url.api_identity_authentication_change,
                        dataType: 'json',
                        data: formData,
                        method: 'POST',
                        processData: false,
                        contentType: false,
                        //enctype: 'multipart/form-data',
                        error: function () {
                            toastr.error('Something went wrong while uploading your image, please try again alter');
                        },
                        success: function (response) {
                            if ( response.status == "success" )
                            {
                                window.location.reload();
                                //alert('update image success');
                            }
                        }
                    });
                },

                takeSnapshot2: function (val) {
                    let that = this;
                    that.loaders = true;
                    Webcam.snap( function(data_uri) {
                        $('.changeImage2').addClass('display_hard_none');
                        // $(".image-tag").val(data_uri);
                        let file = dataURLtoFile(data_uri);
                        console.log('check file name');
                        console.log(file);
                        that.hold_Id_Image = file;
                        document.getElementById('results_back_image-'+val).innerHTML = '<img style="  ' +
                            'display: block;\n' +
                            'border-radius: 6px;\n' +
                            'width: 198px;\n' +
                            'height: 131px;\n' +
                            'margin-left: auto;\n' +
                            'margin-right: auto;" id="captureImage" src="'+data_uri+'"/>';
                    });

                    function dataURLtoFile(dataurl, filename) {
                        var arr = dataurl.split(','),
                            mime = arr[0].match(/:(.*?);/)[1],
                            bstr = atob(arr[1]),
                            n = bstr.length,
                            u8arr = new Uint8Array(n);
                        while(n--){
                            u8arr[n] = bstr.charCodeAt(n);
                        }
                        return new File([u8arr], filename, {type:mime});
                    }
                    this.stopCamera();
                    this.holdIImage(this.hold_Id_Image);
                    $('#openCamera2').modal('hide');
                },

                holdIImage: function(file){

                    console.log('going to  inset back image');
                    var id = $('#user_identity_id').val();
                    var image = file;
                    var formData = new FormData();
                    formData.append('hold_id_image', image);
                    formData.append('column', 'hold_id_image');
                    formData.append('id', id);
                    $('.backCardDefaultImage').hide();
                    $('.backCardImage').show();
                    $.ajax({
                        url: url.api_identity_authentication_change,
                        dataType: 'json',
                        data: formData,
                        method: 'POST',
                        processData: false,
                        contentType: false,
                        //enctype: 'multipart/form-data',
                        error: function () {
                            toastr.error('Something went wrong while uploading your image, please try again alter');
                        },
                        success: function (response) {
                            if ( response.status == "success" )
                            {
                                window.location.reload();
                                //alert('update image success');
                            }
                        }
                    });
                }
            },
            mounted(){
                console.log('mounted run in faceDetectApi');
                console.log('this.start');
                console.log(this.start);
            },
        });
    </script>
    <script>
			$(function () {
				$('.reauth').on('click', function(){
                        const swalWithBootstrapButtons = Swal.mixin({
                            customClass: {
                                confirmButton: 'btn setbtnReAuth reauth'
                            },
                            buttonsStyling: false,
                        });
                        swalWithBootstrapButtons.fire('{{trans('common.contact_support')}}');
				})
			})
    </script>
@endsection




