@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.invite_to_join_title') }}
@endsection

@section('keywords')
    {{ trans('common.invite_to_join_keyword') }}
@endsection

@section('description')
    {{ trans('common.invite_to_join_desc') }}
@endsection

@section('author')
Xenren
@endsection

@section('url')
    https://www.xenren.co/invite
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
    <link href="/custom/css/frontend/invite.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row setCol12Row">
        <div class="col-md-3 search-title-container">
            <span class="find-experts-search-title text-uppercase setMAINMENU">{{ trans('common.main_action') }}</span>
        </div>
        <div class="col-md-9 right-top-main setITJOIN">
            <div class="row">
                <div class="col-md-12 top-tabs-menu">
                    <div class="caption">
                        <span class="find-experts-search-title text-uppercase setIU">{{ trans('common.invite_to_join') }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="rows setPageContent">
                <div class="portlet light qrcode-info">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-4">
                                <span class="qr-title">{{ trans('common.your_qr_code') }}</span>
                                <div class="gray text-center">
                                    <div class="qr-image">
                                        <img src="{{ $qrImageLink }}" width="160px" height="170px">
                                    </div>
                                    <a href="{{ route('frontend.newDesign.userRefer') }}">
                                        <span class="btn btn-green-bg-white-text">
                                            {{ trans('common.invite_your_friend') }}
                                        </span>
                                    </a>
                                    <br/><br/>
                                    <button id="btn-invite-friend" class="btn btn-green-bg-white-text" data-register-url="{{ $registerUrl }}">
                                        &nbsp; {{trans('common.generate_shared_link')}} &nbsp;
                                    </button>

                                </div>
                            </div>

                            <div class="col-md-8">
                                <span class="qr-status-title">{{ trans('common.invite_status') }}</span>
                                <div class="portlet light bordered invite-status">
                                    <div class="portlet-body">
                                        <div>
                                            <span class="available-xen">{{ trans('common.available_xen') }}</span>
                                            <span class="available-xen-value">
                                                <i class="fa fa-money"></i>
                                                {{ $xenCoin }}
                                            </span>
                                            <hr>
                                            <div class="how-many-people">{{ trans('common.how_many_people_i_have_introduce') }}</div>
                                            <br/>

                                            @foreach( $invitations as $invitation )
                                            <div class="rows p-t-10">
                                                <div class="col-md-8">
                                                    <img class="img-circle setIPAGEAVATAR" src="{{ $invitation->invitee->getAvatar() }}">
                                                    <span class="name">{{ $invitation->invitee->name }}</span>
                                                </div>
                                                <div class="col-md-4">
                                                    @if( $invitation->invitee->is_validated  == 1)
                                                    <div class="btn-green-bg-white-text is-verified">
                                                        {{ trans('common.verified') }}
                                                    </div>
                                                    @else
                                                    <div class="btn-yellow-bg-white-text is-verified">
                                                        {{ trans('common.not_yet_verified') }}
                                                    </div>
                                                    @endif
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <hr>
                                            @endforeach

                                            {{--<div class="rows p-t-10">--}}
                                                {{--<div class="col-md-8">--}}
                                                    {{--<img class="img-circle" src="http://xenrencc.dev/uploads/user/13/avatar/BZ250vc8zwu0Q52TJ6NGrVXgDgJVCzu0L1vzS4Vo.jpg" style="width: 39px; height: 39px;">--}}
                                                    {{--<span class="name">John Doe</span>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-md-4">--}}
                                                    {{--<div class="btn-yellow-bg-white-text is-verified">--}}
                                                        {{--{{ trans('common.not_yet_verified') }}--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<div class="clearfix"></div>--}}
                                            {{--</div>--}}
                                            {{--<hr>--}}

                                            <div class="rows xen-description">
                                                 {{ trans('common.xen_coin_usage') }}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{--<!-- BEGIN PAGE BASE CONTENT -->--}}
            {{--<div class="rows">--}}
                {{--<div class="col-md-5">--}}
                    {{--<div class="portlet box gray-color">--}}
                        {{--<div class="portlet-title">--}}
                            {{--<div class="caption">--}}
                                {{--{{ trans('member.your_2d_code') }}--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="portlet-body form">--}}
                            {{--<!-- BEGIN FORM-->--}}
                            {{--<form action="#" class="form-horizontal">--}}
                                {{--<div class="form-body">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<div class="text-center">--}}
                                            {{--<img src="../images/2D-Code.png" alt="logo" width="200px" height="220px" class="logo-default" />--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="form-group">--}}
                                        {{--<div class="row">--}}
                                            {{--<div class="text-center">--}}
                                                {{--<button type="submit" class="btn yellow-gold"> {{ trans('member.click_share') }} </button>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</form>--}}
                            {{--<!-- END FORM-->--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-7">--}}
                    {{--<div class="portlet box gray-color">--}}
                        {{--<div class="portlet-title">--}}
                            {{--<div class="caption">--}}
                                {{--{{ trans('member.invite_status') }}--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="portlet-body form">--}}
                            {{--<!-- BEGIN FORM-->--}}
                            {{--<form action="#" class="form-horizontal">--}}
                                {{--<div class="form-body">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="col-md-6 control-label">{{ trans('member.total_earning') }} : 12306&#165;</label>--}}

                                        {{--<div class="pull-right tranfer">--}}
                                            {{--<button type="submit" class="btn blue">{{ trans('member.transfer_balance') }} </button>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<hr>--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label class="col-md-6 control-label">{{ trans('member.spread_number') }} : 4 {{ trans('member.people') }}</label>--}}
                                        {{--<div class="pull-right chk-detail">--}}
                                            {{--<button type="submit" class="btn blue">{{ trans('member.check_details') }}</button>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="form-group">--}}
                                        {{--<div class="col-md-3">--}}
                                        {{--</div>--}}
                                        {{--<div class="table-scrollable table-scrollable-borderless col-md-6">--}}
                                            {{--<table class="table table-hover table-light">--}}
                                                {{--<thead>--}}
                                                    {{--<tr class="uppercase">--}}
                                                        {{--<th colspan="2"> {{ trans('member.username') }} </th>--}}
                                                        {{--<th> {{ trans('member.total_earning') }} </th>--}}
                                                        {{--<th> {{ trans('member.completed_cases') }} </th>--}}
                                                        {{--<th> {{ trans('member.publish_order') }} </th>--}}
                                                    {{--</tr>--}}
                                                {{--</thead>--}}
                                                {{--<tr>--}}
                                                    {{--<td class="fit">--}}
                                                        {{--<img class="user-pic" src="../assets/pages/media/users/avatar4.jpg"> </td>--}}
                                                    {{--<td>--}}
                                                        {{--<a href="javascript:;" class="primary-link">Brain</a>--}}
                                                    {{--</td>--}}
                                                    {{--<td> $345 </td>--}}
                                                    {{--<td> 45 </td>--}}
                                                    {{--<td> 124 </td>--}}
                                                {{--</tr>--}}
                                                {{--<tr>--}}
                                                    {{--<td class="fit">--}}
                                                        {{--<img class="user-pic" src="../assets/pages/media/users/avatar5.jpg"> </td>--}}
                                                    {{--<td>--}}
                                                        {{--<a href="javascript:;" class="primary-link">Nick</a>--}}
                                                    {{--</td>--}}
                                                    {{--<td> $560 </td>--}}
                                                    {{--<td> 12 </td>--}}
                                                    {{--<td> 24 </td>--}}
                                                {{--</tr>--}}
                                                {{--<tr>--}}
                                                    {{--<td class="fit">--}}
                                                        {{--<img class="user-pic" src="../assets/pages/media/users/avatar6.jpg"> </td>--}}
                                                    {{--<td>--}}
                                                        {{--<a href="javascript:;" class="primary-link">Tim</a>--}}
                                                    {{--</td>--}}
                                                    {{--<td> $1,345 </td>--}}
                                                    {{--<td> 450 </td>--}}
                                                    {{--<td> 46 </td>--}}
                                                {{--</tr>--}}
                                                {{--<tr>--}}
                                                    {{--<td class="fit">--}}
                                                        {{--<img class="user-pic" src="../assets/pages/media/users/avatar7.jpg"> </td>--}}
                                                    {{--<td>--}}
                                                        {{--<a href="javascript:;" class="primary-link">Tom</a>--}}
                                                    {{--</td>--}}
                                                    {{--<td> $645 </td>--}}
                                                    {{--<td> 50 </td>--}}
                                                    {{--<td> 89 </td>--}}
                                                {{--</tr>--}}
                                            {{--</table>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-3">--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</form>--}}
                            {{--<!-- END FORM-->--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- END PAGE BASE CONTENT -->--}}
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection


@section('footer')
    <script>
        var lang = {
            already_copy_and_use_in_paste : '{{ trans('common.already_copy_and_use_in_paste')  }}'
        }
    </script>
    <script src="/custom/js/frontend/invite.js" type="text/javascript"></script>
@endsection




