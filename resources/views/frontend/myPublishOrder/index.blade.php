@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.published_project_title') }}
@endsection

@section('keywords')
    {{ trans('common.published_project_keyword') }}
@endsection

@section('description')
    {{ trans('common.published_project_desc') }}
@endsection

@section('author')
    Xenren
@endsection

@section('url')
    https://www.xenren.co/myPublishOrder
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
    <link href="/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/bootstrap-star-rating/css/star-rating.css" media="all" rel="stylesheet" type="text/css"/>
    <link href="/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/myPublishOrder.css" rel="stylesheet" type="text/css" />
    <link href="../custom/css/frontend/awardedProject.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row setRowCol12">
        <div class="col-md-3 search-title-container">
            <span class="find-experts-search-title text-uppercase setMAinMENU">{{ trans('common.main_action') }}</span>
        </div>

        <div class="col-md-9 right-top-main set-pagination-publish-order-page">
            <div class="row set-paginationrow">
                <div class="col-md-12 top-tabs-menu setMPCol12">
                    <div class="order-status-menu pull-right setMPPOSMPR">
                        <ul>
                            <li {{ isset($status) && $status == 'all' ? "class=active": ''}}>
                                <a href="{{ route('frontend.mypublishorder' , \Session::get('lang'))}}?status=all ">
                                    {{ trans('common.all')  }}
                                </a>
                            </li>
                            <li {{ isset($status) && ( $status == 'published') ? "class=active": ''}}>
                                <a href="{{ route('frontend.mypublishorder', \Session::get('lang'))}}?status=published">
                                    {{ trans('common.published') }}
                                </a>
                            </li>
                            <li {{ isset($status) && $status == 'progressing' ? "class=active": ''}}>
                                <a href="{{ route('frontend.mypublishorder', \Session::get('lang'))}}?status=progressing">
                                    {{ trans('common.progressing') }}
                                </a>
                            </li>
                            <li {{ isset($status) && $status == 'completed' ? "class=active": ''}}>
                                <a href="{{ route('frontend.mypublishorder', \Session::get('lang'))}}?status=completed">
                                    {{ trans('common.completed') }}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="caption setMPC" style="margin-top: 7px">
                        <span class="find-experts-search-title text-uppercase">{{ trans('common.pproject') }}</span>
                    </div>
                </div>
            </div>
            {{--<div class="row">--}}

            {{--<div class="col-md-12 found-count-top">--}}
            {{--<span class="text">--}}
            {{--@if( $status == 'all' )--}}
            {{--{{ trans('common.all') }}--}}
            {{--@elseif( $status == 'published' )--}}
            {{--{{ trans('common.published') }}--}}
            {{--@elseif( $status == 'progressing' )--}}
            {{--{{ trans('common.in_progress') }}--}}
            {{--@elseif( $status == 'completed' )--}}
            {{--{{ trans('common.completed') }}--}}
            {{--@endif--}}
            {{--</span>--}}
            {{--<span class="number">({{ count($projects) }})</span>--}}
            {{--<div class="search-selectbox custom-days-selectbox">--}}
            {{--<select class="form-control" id="lastNumberDays">--}}
            {{--<option value="0"> {{ trans('common.all') }}</option>--}}

            {{--@foreach ($lastNumberDayOptions as $key => $lastNumberDayOption)--}}
            {{--@if($lastNumberDay == $key)--}}
            {{--<option value="{{ $key }}" selected> {{ $lastNumberDayOption }}</option>--}}
            {{--@else--}}
            {{--<option value="{{ $key }}"> {{ $lastNumberDayOption }}</option>--}}
            {{--@endif--}}
            {{--@endforeach--}}
            {{--</select>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
        @include('frontend.flash')
        <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row setPageContent">
                <div class="col-md-12 col-sm-12">
                    <div class="portlet light bordered">
                        <div class="portlet-body award-body setPBAB">
                            <div class="row p-0 m-0">
                                <div class="col-md-12 award-hedding-title award-section">
                                    <h1 class="p-0 m-0">{{trans('common.published_projects')}}<span class="label label-success label-title setLABELTITIT">{{count($projects)}}</span></h1>
                                </div>
                            </div>
                            @if($projects->isEmpty())
                                <div class="no-record-yet">
                                    <p>{{trans('common.no-record-published')}}</p>
                                    <img src="{{asset('images/norecord.png')}}">
                                </div>
                            @else
                                <div class="row p-0 m-0">
                                    @php $projNo = -1 ; @endphp
                                    @foreach($projects as $k => $projectInfo)
                                        @if(isset($adminSettings) && $adminSettings->value == \App\Models\Project::PAY_TYPE_HOURLY_PAY)
                                            @if($projectInfo->pay_type != \App\Models\Project::PAY_TYPE_HOURLY_PAY)
                                        @php $projNo = $projNo +1 ; @endphp
                                        <div class="col-md-12 award-section award-description">
                                            <div class="row">
                                                <div class="col-md-9">
                                                    @php
                                                        $projectType = $projectInfo->type == \App\Models\Project::PROJECT_TYPE_COMMON ? 'Public': 'Official';
                                                        $projectPayType = $projectInfo->pay_type == \App\Models\Project::PAY_TYPE_FIXED_PRICE ? 'Fixed': 'Hourly';
                                                    @endphp
                                                    <h2 class="m-0">{{$projectInfo->name}}</h2>
                                                    <div style="padding-bottom: 10px">
                                                    <!--{!!html_entity_decode($projectInfo->description)!!}-->
                                                        @php
                                                            $longString = html_entity_decode($projectInfo->description);
                                                            $len = strlen($longString);
                                                            $initial_desc = substr($longString,0,100);
                                                            $rest_desc = substr($longString,100,$len);
                                                        @endphp
                                                        @if ($len>100)
                                                            <div>{!!html_entity_decode($initial_desc)!!}</div>
                                                            <div id="moreDesc" style="display:none">{!!html_entity_decode($rest_desc)!!}</div>
                                                            <div id="dots">...</div>
                                                            <a onclick="readMore({{$projNo}})" id="moreLink" style="color: #58af2a">Read More</a>
                                                        @else
                                                            <div>{!!html_entity_decode($longString)!!}</div>
                                                        @endif
                                                    </div>
                                                    <span class="setSpnMP">{{trans('common.proposals')}}:<div class="label label-success label-title">
     @php
         $countProposal =(
         \App\Models\ProjectApplicant::where('project_id', '=', $projectInfo->id)
         ->where(function($q){
             $q->where('status','=', \App\Models\ProjectApplicant::STATUS_APPLY);
         })
         ->count());

     @endphp
                                                            {{$countProposal}}
                                                    </div></span>
                                                    <span class="setSpnMP">{{trans('common.interview')}}:<div class="label label-success label-title">
                                                          {{--{{ count($projectInfo->projectChatRoom->chatFollower) }}--}}
                                                            @php
                                                                $countInterview =(
                                                                \App\Models\ProjectApplicant::where('project_id', '=', $projectInfo->id)
                                                                ->where(function($q){
                                                                    $q->where('status','=', \App\Models\ProjectApplicant::STATUS_INTERVIEW);
                                                                })
                                                                ->count());

                                                            @endphp
                                                            {{$countInterview}}
                                                    </div></span>
                                                    <span class="setSpnMP">{{trans('common.hired')}}:<div class="label label-success label-title">
                                                        {{--{{dd($projectInfo)}}--}}
                                                            @php
                                                                $count =(
                                                                \App\Models\ProjectApplicant::where('project_id', '=', $projectInfo->id)
                                                                ->where(function($q){
                                                                    $q->where('status','=', \App\Models\ProjectApplicant::STATUS_APPLICANT_ACCEPTED)->orWhere('status','=', \App\Models\ProjectApplicant::STATUS_CREATOR_SELECTED);
                                                                })
                                                                ->count());

                                                            @endphp
                                                            {{$count}}
                                                    </div></span>
                                                    <div style="padding-top:10px">
                                                        <span class="posted-title">{{trans('common.posted')}} {{\Carbon\Carbon::parse($projectInfo->created_at)->diffForHumans()}} &nbsp;&nbsp; {{trans('common.project_type')}} : {{$projectType}} / {{$projectPayType}} </span>
                                                    </div>
                                                </div>

                                                <div class="dropdown pull-right" style="margin-right:2%">
                                                    <button class="dropbtn"><img class="m-0" src="/images/dropdownbtn.png"></button>
                                                    <div id="myDropdown" class="dropdown-content">
                                                        <a href="{{ route('frontend.joindiscussion.getprojectapplicant', ['id' => $projectInfo->id ,'project_name' => $projectInfo->name]) }}#sec-chat">{{trans('common.join_discussion')}}</a>
                                                        <hr class="m-0">
                                                        <a href="#">{{trans('common.messages')}}</a>
                                                        <hr class="m-0">
                                                        @if( $projectInfo->status == \App\Models\Project::STATUS_PUBLISHED)
                                                            <a href="{{ route('frontend.mypublishorder.deleteproject', ['id' => $projectInfo->id ]) }}#sec-common-project">{{trans('common.remove_project')}}</a>
                                                            <hr class="m-0">
                                                            <a href="{{ route('frontend.mypublishorder.editproject', ['id' => $projectInfo->id ]) }}#sec-common-project">{{trans('common.edit_project')}}</a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            @endif
                                        @else
                                            @php $projNo = $projNo +1 ; @endphp
                                            <div class="col-md-12 award-section award-description">
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        @php
                                                            $projectType = $projectInfo->type == \App\Models\Project::PROJECT_TYPE_COMMON ? 'Public': 'Official';
                                                            $projectPayType = $projectInfo->pay_type == \App\Models\Project::PAY_TYPE_FIXED_PRICE ? 'Fixed': 'Hourly';
                                                        @endphp
                                                        <h2 class="m-0">{{$projectInfo->name}}</h2>
                                                        <div style="padding-bottom: 10px">
                                                        <!--{!!html_entity_decode($projectInfo->description)!!}-->
                                                            @php
                                                                $longString = html_entity_decode($projectInfo->description);
                                                                $len = strlen($longString);
                                                                $initial_desc = substr($longString,0,100);
                                                                $rest_desc = substr($longString,100,$len);
                                                            @endphp
                                                            @if ($len>100)
                                                                <div>{!!html_entity_decode($initial_desc)!!}</div>
                                                                <div id="moreDesc" style="display:none">{!!html_entity_decode($rest_desc)!!}</div>
                                                                <div id="dots">...</div>
                                                                <a onclick="readMore({{$projNo}})" id="moreLink" style="color: #58af2a">Read More</a>
                                                            @else
                                                                <div>{!!html_entity_decode($longString)!!}</div>
                                                            @endif
                                                        </div>
                                                        <span class="setSpnMP">{{trans('common.proposals')}}:<div class="label label-success label-title">
     @php
         $countProposal =(
         \App\Models\ProjectApplicant::where('project_id', '=', $projectInfo->id)
         ->where(function($q){
             $q->where('status','=', \App\Models\ProjectApplicant::STATUS_APPLY);
         })
         ->count());

     @endphp
                                                                {{$countProposal}}
                                                    </div></span>
                                                        <span class="setSpnMP">{{trans('common.interview')}}:<div class="label label-success label-title">
                                                          {{--{{ count($projectInfo->projectChatRoom->chatFollower) }}--}}
                                                                @php
                                                                    $countInterview =(
                                                                    \App\Models\ProjectApplicant::where('project_id', '=', $projectInfo->id)
                                                                    ->where(function($q){
                                                                        $q->where('status','=', \App\Models\ProjectApplicant::STATUS_INTERVIEW);
                                                                    })
                                                                    ->count());

                                                                @endphp
                                                                {{$countInterview}}
                                                    </div></span>
                                                        <span class="setSpnMP">{{trans('common.hired')}}:<div class="label label-success label-title">
                                                        {{--{{dd($projectInfo)}}--}}
                                                                @php
                                                                    $count =(
                                                                    \App\Models\ProjectApplicant::where('project_id', '=', $projectInfo->id)
                                                                    ->where(function($q){
                                                                        $q->where('status','=', \App\Models\ProjectApplicant::STATUS_APPLICANT_ACCEPTED)->orWhere('status','=', \App\Models\ProjectApplicant::STATUS_CREATOR_SELECTED);
                                                                    })
                                                                    ->count());

                                                                @endphp
                                                                {{$count}}
                                                    </div></span>
                                                        <div style="padding-top:10px">
                                                            <span class="posted-title">{{trans('common.posted')}} {{\Carbon\Carbon::parse($projectInfo->created_at)->diffForHumans()}} &nbsp;&nbsp; {{trans('common.project_type')}} : {{$projectType}} / {{$projectPayType}} </span>
                                                        </div>
                                                    </div>

                                                    <div class="dropdown pull-right" style="margin-right:2%">
                                                        <button class="dropbtn"><img class="m-0" src="/images/dropdownbtn.png"></button>
                                                        <div id="myDropdown" class="dropdown-content">
                                                            <a href="{{ route('frontend.joindiscussion.getprojectapplicant', ['id' => $projectInfo->id ,'project_name' => $projectInfo->name]) }}#sec-chat">{{trans('common.join_discussion')}}</a>
                                                            <hr class="m-0">
                                                            <a href="#">{{trans('common.messages')}}</a>
                                                            <hr class="m-0">
                                                            @if( $projectInfo->status == \App\Models\Project::STATUS_PUBLISHED)
                                                                <a href="{{ route('frontend.mypublishorder.deleteproject', ['id' => $projectInfo->id ]) }}#sec-common-project">{{trans('common.remove_project')}}</a>
                                                                <hr class="m-0">
                                                                <a href="{{ route('frontend.mypublishorder.editproject', ['id' => $projectInfo->id ]) }}#sec-common-project">{{trans('common.edit_project')}}</a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach

                                    <div class="col-md-12 footer-section">
                                        <div class="row">
                                            @if(!$viewAll)
                                                <div class="col-md-6 footer-section-right">
                                                    <a href="?viewAll=true" class="setSectionFooter">{{trans('common.view_all')}}</a>
                                                </div>
                                            @endif

                                            <div class="col-md-6 footer-section-left-paginate text-right">
                                                {{--{{$projects->links()}}--}}

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="portlet light bordered">
                        <div class="portlet-body award-second-section setCol12ASSTAS">
                            <div class="row p-0 m-0">
                                <div class="col-md-12 award-second-section-title award-section">
                                    <h2 class="p-0 m-0 setH2PP">{{trans('common.awarded_projects')}} <span class="label label-success label-title setLABELTITIT1">{{count($awardedProjects)}}</span></h2>
                                </div>
                            </div>

                            @if($awardedProjects->isEmpty())
                                <div class="no-record-yet">
                                    <p>{{trans('common.no-record-awarded')}}</p>
                                    <img src="{{asset('images/norecord2.png')}}">
                                </div>
                            @else
                                <div class="row p-0 m-0" style="text-align: center">
                                    @if(count($awardedProjects) > 0)
                                        @php $key = 0; @endphp
                                        @foreach($awardedProjects as $k => $awardedProject)
                                            @if($awardedProject['pay_type']== \App\Models\Project::PAY_TYPE_FIXED_PRICE)
                                                @php $key = $key +1 ; @endphp
                                                @php
                                                    $fixedPayAwardees = $key;
                                                @endphp
                                            @endif
                                        @endforeach
                                    @endif

                                    <button class="tab-button tablink tab-button-green" onclick="openTab(event, 'FixedPrice')">
                                        <div style="display: block;">
                                            <div style="padding-right:3%; display:inline-block">{{trans('common.fixed_price')}}</div><div class="pay-type-count">{{$fixedPayAwardees ?? 0}}</div>
                                        </div>
                                    </button>
                                    @if(count($awardedProjects) > 0)
                                        @php $key = 0; @endphp
                                        @foreach($awardedProjects as $k => $awardedProject)
                                            @if($awardedProject['pay_type']== \App\Models\Project::PAY_TYPE_HOURLY_PAY)
                                                @php $key = $key +1 ; @endphp
                                                @php
                                                    $hourlyPayAwardees = $key;
                                                @endphp
                                            @endif
                                        @endforeach
                                    @endif
                                    @if(isset($adminSettings) && $adminSettings->value != \App\Models\Project::PAY_TYPE_HOURLY_PAY)
                                    <button class="tab-button tablink" onclick="openTab(event, 'Hourly')">
                                        <div style="display: block;">
                                            <div style="padding-right:3%; display:inline-block">{{trans('common.hourly')}}</div><div class="pay-type-count">{{$hourlyPayAwardees ?? 0}}</div>
                                        </div>
                                    </button>
                                    @endif
                                </div>

<div class="row p-0 m-0 Tab" id="FixedPrice">
    @if(count($awardedProjects) > 0)
        @php $key = 0; @endphp
        @foreach($awardedProjects as $k => $awardedProject)
            @php

                $awardedProjectId =  $awardedProject['id'];
                $awardedProjectUserId = $awardedProject['user']['id'];

                $userInboxData = \App\Models\UserInboxMessages::where('project_id','=',$awardedProjectId)

                                ->where(function($q) use ($awardedProjectUserId){
                                    $q->where('from_user_id',$awardedProjectUserId);
                                   $q->OrWhere('to_user_id',$awardedProjectUserId);
                                })->where('from_staff_id',null)
                                  ->first();
                $userInboxId = $userInboxData['inbox_id'];
                $key = $key +1 ;
            @endphp
            @if($key <= 10)
                {{--<!--@php--}}
                {{--$projectPayType = $awardedProject['project']['pay_type']== \App\Models\Project::PAY_TYPE_FIXED_PRICE ? trans('common.fixed_price') : trans('common.hourly');--}}
                {{--@endphp-->--}}
                @if($awardedProject['pay_type'] == \App\Models\Project::PAY_TYPE_FIXED_PRICE)
                    <div class="col-lg-12 award-second-section-description award-section-second">
                        <div class="row">
                            <div class="col-md-1 setAPCol1">
                                <img src="{{$awardedProject['user']['img_avatar']?$awardedProject['user']['img_avatar']:'/images/avatar_xenren.png'}}" class="user-icon" onerror="this.src='/images/avatar_xenren.png'">
                            </div>
                            <div class="col-md-5 award-second-section-center setAPCol8">
                                <h3 class="p-0 m-0">{{$awardedProject['user']['real_name']}}
                                    {{--<span class="label label-pill label-pendding">Pending</span>--}}
                                    @if( $awardedProject['status'] == \App\Models\ProjectApplicant::STATUS_APPLY )
                                        <span class="label label-pill label-success setMPPPAGELABEL" >{{ trans('common.common_pending') }}</span>
                                    @elseif( $awardedProject['status'] == \App\Models\UserInbox::TYPE_PROJECT_SEND_OFFER)
                                        <span class="label label-pill label-success setMPPPAGELABEL">{{ trans('common.common_pending') }}</span>
                                    @elseif( $awardedProject['status'] == \App\Models\ProjectApplicant::STATUS_APPLICANT_ACCEPTED )
                                        <span class="label label-pill label-proccesing">{{ trans('common.in_progress') }}</span>
                                    @elseif( $awardedProject['status'] == \App\Models\ProjectApplicant::STATUS_CREATOR_SELECTED )
                                        <span class="label label-pill label-proccesing">{{ trans('common.in_progress') }}</span>
                                    @elseif( $awardedProject['status'] == \App\Models\ProjectApplicant::STATUS_COMPLETED )
                                        <span class="label label-pill label-completed">{{ trans('common.completed') }}</span>
                                    @endif
                                </h3>
                                <p>{{$awardedProject['project']['name']}} <span class="project-price">{{trans('common.fixed_price')}}</span> <span class="project-time">:&nbsp;{{$awardedProject['project']['reference_price']}}$</span></p>
                            </div>
                            <div class="col-md-6" style="padding-left: 0">
                                <div class="dropdown pull-right" style="margin-right:2%">
                                    <button class="dropbtn"><img class="m-0" src="/images/dropdownbtn.png"></button>
                                    <div id="myDropdown" class="dropdown-content">
                                        <a href="{{ route('frontend.joindiscussion.getproject', [
                                    'slug' => $awardedProject['project']['name'],
                                    'id' => $awardedProject['project']['id'],
                                    'lang' => \Session::get('lang')
                                    ]) }}#sec-chat">{{trans('common.join_discussion')}}</a>
                                        <hr class="m-0" >
                                        <a href="{{ route('frontend.inbox.messages', ['inbox_id' => $userInboxId])}}">{{trans('common.messages')}}</a>
                                        <hr class="m-0" >
                                        @if(isset($awardedProject['awarded']) && $awardedProject['awarded'] == true)
                                            <a href="{{ route('frontend.mypublishorder.orderDetail', ['id' => $awardedProject['project']['id'], 'userId' => $awardedProject['user']['id']]) }}#sec-common-project">{{trans('common.order_details')}}</a>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-9 p-0" style="margin-top:20px">
                                    @if( $awardedProject['status'] == \App\Models\ProjectApplicant::STATUS_APPLICANT_ACCEPTED )
                                        <a class="milestoneBtn createMilestone"
                                           onclick="newMilestoneModalFixed('{{$awardedProject['project']['id']}}','{{$awardedProject['user']['id']}}')">

                                            {{trans('common.new_milestone')}}
                                        </a>&nbsp;
                                        <a class="milestoneBtn" data-toggle="modal" data-target="#releaseMilestoneModal{{$awardedProject['project']['id']}}-{{$awardedProject['user']['id']}}">{{trans('common.release_milestone')}}</a>
                                    @endif

                                    <div id="releaseMilestoneModal{{$awardedProject['project']['id']}}-{{$awardedProject['user']['id']}}" class="modal fade">
                                        <div class="milestonemodal-content">
                                            <a class="closeModal" data-dismiss="modal">&#10005;</a>

                                            <h class="col-md-11 modaltitle">{{trans('common.release_milestone')}}</h>
                                            <p class="col-md-12" style="text-align: center; margin-top: 0; font-size: 9pt;">
                                                {{trans('common.release_mile_simple_click')}}</p>
                                            <p class="col-md-12" style="font-weight: 600; margin:15px 0">{{trans('common.release_mile_detail')}}</p>

                                            @php
                                                $miles = $milestones->sortByDesc('created_at');
                                                $currentMilesNumber = 0;
                                            @endphp
                                            @foreach($miles as $v)
                                                @if($v->project_id == $awardedProject['project']['id'] &&  $v->applicant_id == $awardedProject['user']['id'])
                                                    @if($v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_INPROGRESS ||  $v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_CANCEL_REQUEST_BY_EMPLOYER)
                                                        @php $currentMilesNumber = $currentMilesNumber +1 ; @endphp
                                                        <div class="tableOutline" style="overflow-x:auto; width:100%">
                                                            <table id="milestoneTable" class="table">
                                                                <tr style="border-bottom: 1px solid #cccccc; border-top: solid white">
                                                                    <td class="col-md-3" style="font-weight:600; margin-top:10px">{{trans('common.amount')}}</td>
                                                                    <td class="col-md-6" style="font-weight:600; margin-top:10px; text-align:center; padding-right:8%">
                                                                        {{trans('common.description')}}</td>
                                                                    <td class="col-md-3" style="font-weight: 600; margin-top:10px">{{trans('common.request_date')}}</td>
                                                                <tr>
                                                                <tr>
                                                                    <td class="milestoneAmount-{{$v->project_id}}">${{$v->amount ?? N/A}}</td>
                                                                    <td class="milestonedescription" style="text-align: center; padding-right:8%">{{$v->description ?? N/A}}</td>
                                                                    <td class="dueData">{{$v->due_date ?? N/A}}</td>
                                                                </tr>
                                                                <input type="hidden" value="{{$v->project_id}}" class="projectid">
                                                                <input type="hidden" value="{{$v->applicant_id}}" class="applicantId">
                                                                <input type="hidden" value="{{$v->name}}" class="milestoneName">
                                                            </table>
                                                        </div>
                                                        <div class="col-md-12 p-0">
                                                            <div class="col-md-4 p-0">
                                                                <div class="col-md-12" style="padding:15px 0px; border-bottom:1px solid #cccccc">
                                                                    <input type="number" class="col-md-12 bonusinput bonusAmount-{{$v->project_id}}" placeholder="{{trans('common.give_bonus')}} ({{trans('common.optional')}})" oninput="bonusplusmilestone('{{$v->project_id}}')"/>
                                                                </div>
                                                                <strong class="col-md-12" style="padding-top:8px">{{trans('common.total')}}:<span class="bonusplusmilestone-{{$v->project_id}}"></span></strong>
                                                            </div>

                                                            <div class="password-{{$v->id}}" style="margin-top: 85px;margin-left:140px;display:none">
                                                                <input type="password" autocomplete="off" class="form-control pass" name="password" placeholder="Please Confirm your password" required>
                                                            </div>
                                                            <div class="error" style="margin-top: 10px;margin-left:140px;display:none">
                                                                <p class="error" style="color:red">Incorrect Password</p>
                                                            </div>
                                                            <div class="col-md-8" style="text-align:right; padding-right:0; padding-left:0; padding-top:35px">
                                                                <a href="javascript:;" onclick="myConfirmMilestone('{{$v->id}}')" type="button" class="btn approve-pay-btn" style="padding:15px">{{trans('common.approve_pay')}}</a>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endif
                                            @endforeach
                                            @if($currentMilesNumber == 0)
                                                <p class="col-md-12" style="text-align:center; margin:10px 0;">{{trans('common.no_current_milestone')}}</p>
                                            @endif

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                @endif
            @endif
        @endforeach
    @endif

        <!-- newrealseMilestonefixedRate -->
        <div id="relaseMilestoneModalfixed" class="modal fade">
            <div class="milestonemodal-content2">
                <form accept="">
                    <input type="hidden" name="applicant_id"
                           class="fixedapplicant_id"
                           value="">
                    <input type="hidden" name="project_id"
                           class="fixedproject_id"
                           value="">
                    <a class="closeModal" data-dismiss="modal">&#10005;</a>
                    <h class="col-md-11 modaltitle">{{trans('common.create_milestone')}}</h>
                    <p class="col-md-12"
                       style="text-align: center; margin-top: 0; font-size: 9pt;">
                        {{trans('common.create_milestone_simple_click')}}</p>
                    <p class="col-md-12"
                       style="font-weight: 600; margin:15px 0">{{trans('common.milestone')}}</p>

                    <div class="col-md-3"
                         style="padding: 2px 5px">
                        <input class="form-control milestones_amount"
                               style="background:url(/images/icons/$icon.png) no-repeat; background-position:2% 45%; text-align:center"
                               id="milestones_amount"
                               type="text"
                               placeholder="{{trans('common.deposit_amount')}}">
                    </div>

                    <div class="col-md-3"
                         style="padding: 2px 5px">
                        <input class="form-control milestones_name"
                               id="milestones_name"
                               type="text"
                               placeholder="{{trans('common.milestone_name')}}">
                    </div>

                    <div class="col-md-3"
                         style="padding: 2px 5px">
                        <input class="form-control milestones_description"
                               id="milestones_description"
                               type="text"
                               placeholder="{{trans('common.milestone_description')}}">
                    </div>


                    <div class="col-md-3 date form_datetime"
                         style="padding: 2px 5px">
                        <input id="milestones_duedate"
                               type="text" class="form-control milestones_duedate"
                               readonly="" size="16">
                        <span class="input-group-btn"
                              style="padding-top: 2px">
                                                <button type="button" class="btn default date-set p-0">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                        </span>
                    </div>

                    <div class="col-md-3 pull-right"
                         style="padding-top:5%">
                        <button class="btn mile-submit-btn confirmMilestonefixed"
                                type="submit">{{trans('common.submit')}}</button>
                    </div>
                </form>

            </div>
        </div>
        <!-- newrealseMilestonefixedRate -->

    <div class="col-md-12 footer-section">
        <div class="row">
            <div class="col-md-6 footer-section-right">
                @if(!$viewAllAwarded)
                    <div class="col-md-6 footer-section-right">
                        <a href="?viewAllAwarded=true">{{trans('common.view_all')}}</a>
                    </div>
                @endif
            </div>
            <div class="col-md-6 footer-section-left-paginate text-right">
                @if ($awardedProjects->lastPage() > 1)
                    <div class="pull-right">
                        {{$awardedProjects->links()}}
                    </div>
                @endif
                {{--{{$awardedProjects->links()}}--}}
            </div>
        </div>
    </div>
</div>

<div class="row p-0 m-0 Tab" id="Hourly" style="display:none">
                                    @if(count($awardedProjects) > 0)
                                        @php $key = 0; @endphp
                                        @foreach($awardedProjects as $k => $awardedProject)
                                            @php $key = $key +1 ; @endphp
                                            @if($key <= 10)
                                                @if($awardedProject['pay_type']== \App\Models\Project::PAY_TYPE_HOURLY_PAY)
                                                    <div class="col-lg-12 award-second-section-description award-section-second">
                                                        <div class="row">
                                                            <div class="col-md-1 setAPCol1">
                                                                <img src="{{$awardedProject['user']['img_avatar']?$awardedProject['user']['img_avatar']:'/images/avatar_xenren.png'}}" class="user-icon" onerror="this.src='/images/avatar_xenren.png'">
                                                            </div>
                                                            <div class="col-md-5 award-second-section-center setAPCol8">
                                                                <h3 class="p-0 m-0">{{$awardedProject['user']['real_name']}}
                                                                    {{--<span class="label label-pill label-pendding">Pending</span>--}}
                                                                    @if( $awardedProject['status'] == \App\Models\ProjectApplicant::STATUS_APPLY )
                                                                        <span class="label label-pill label-success setMPPPAGELABEL" >{{ trans('common.common_pending') }}</span>
                                                                    @elseif( $awardedProject['status'] == \App\Models\UserInbox::TYPE_PROJECT_SEND_OFFER)
                                                                        <span class="label label-pill label-success setMPPPAGELABEL">{{ trans('common.common_pending') }}</span>
                                                                    @elseif( $awardedProject['status'] == \App\Models\ProjectApplicant::STATUS_APPLICANT_ACCEPTED )
                                                                        <span class="label label-pill label-proccesing">{{ trans('common.in_progress') }}</span>
                                                                    @elseif( $awardedProject['status'] == \App\Models\ProjectApplicant::STATUS_CREATOR_SELECTED )
                                                                        <span class="label label-pill label-proccesing">{{ trans('common.in_progress') }}</span>
                                                                    @elseif( $awardedProject['status'] == \App\Models\ProjectApplicant::STATUS_COMPLETED )
                                                                        <span class="label label-pill label-completed">{{ trans('common.completed') }}</span>
                                                                    @endif
                                                                </h3>
                                                                <p>{{$awardedProject['project']['name']}} <span class="project-price">{{trans('common.hourly')}}</span> <span class="project-time">:&nbsp;{{$awardedProject['project']['reference_price']}}$</span></p>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="dropdown pull-right" style="margin-right:2%">
                                                                    <button class="dropbtn"><img class="m-0" src="/images/dropdownbtn.png"></button>
                                                                    <div id="myDropdown" class="dropdown-content">
                                                                        <a href="{{ route('frontend.joindiscussion.getproject', [
                                                                    'slug' => $awardedProject['project']['name'],
                                                                    'id' => $awardedProject['project']['id'],
                                                                    'lang' => \Session::get('lang')
                                                                    ]) }}#sec-chat">{{trans('common.join_discussion')}}</a>
                                                                        <hr class="m-0" >
                                                                        <a href="#">{{trans('common.messages')}}</a>
                                                                        <hr class="m-0" >
                                                                        @if(isset($awardedProject['awarded']) && $awardedProject['awarded'] == true)
                                                                            <a href="{{ route('frontend.mypublishorder.orderDetail', ['id' => $awardedProject['project']['id'], 'userId' => $awardedProject['user']['id']]) }}#sec-common-project">{{trans('common.order_details')}}</a>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-8" style="margin-top:20px">
                                                                    @if( $awardedProject['status'] == \App\Models\ProjectApplicant::STATUS_APPLICANT_ACCEPTED )
                                                                    <a class="milestoneBtn createMilestone" onclick="newMilestoneModalHorly('{{$awardedProject['project']['id']}}','{{$awardedProject['user']['id']}}')">
                                                                        {{trans('common.new_milestone')}}
                                                                    </a> &nbsp;
                                                                    <a class="milestoneBtn" data-toggle="modal" data-target="#releaseMilestoneModalHourly{{$awardedProject['project']['id']}}-{{$awardedProject['user']['id']}}">{{trans('common.release_milestone')}}</a>
                                                                    @endif

                                                                    <div id="releaseMilestoneModalHourly{{$awardedProject['project']['id']}}-{{$awardedProject['user']['id']}}" class="modal fade">
                                                                        <div class="milestonemodal-content">
                                                                            <a class="closeModal" data-dismiss="modal">&#10005;</a>
                                                                            <h class="col-md-11 modaltitle">{{trans('common.release_milestone')}}</h>
                                                                            <p class="col-md-12" style="text-align: center; margin-top: 0; font-size: 9pt;">
                                                                                {{trans('common.release_mile_simple_click')}}</p>
                                                                            <p class="col-md-12" style="font-weight: 600; margin:15px 0">{{trans('common.release_mile_detail')}}</p>

                                                                            @php
                                                                                $miles = $milestones->sortByDesc('created_at');
                                                                                $currentMilesNumber = 0;
                                                                            @endphp
                                                                            @foreach($miles as $v)
                                                                                @if($v->project_id == $awardedProject['project']['id'] &&  $v->applicant_id == $awardedProject['user']['id'])
                                                                                    @if($v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_INPROGRESS)
                                                                                        @php $currentMilesNumber = $currentMilesNumber + 1;@endphp
                                                                                        <div class="tableOutline" style="overflow-x:auto; width:100%">
                                                                                            <table id="milestoneTable" class="table">
                                                                                                <tr style="border-bottom: 1px solid #cccccc; border-top: solid white">
                                                                                                    <td class="col-md-3" style="font-weight:600; margin-top:10px">{{trans('common.amount')}}</td>
                                                                                                    <td class="col-md-6" style="font-weight:600; margin-top:10px; text-align:center; padding-right:8%">
                                                                                                        {{trans('common.description')}}</td>
                                                                                                    <td class="col-md-3" style="font-weight: 600; margin-top:10px">{{trans('common.request_date')}}</td>
                                                                                                <tr>
                                                                                                <tr>
                                                                                                    <td class="milestoneAmounthourly-{{$v->project_id}}">${{$v->amount ?? N/A}}</td>
                                                                                                    <td style="text-align: center; padding-right:8%">{{$v->description ?? N/A}}</td>
                                                                                                    <td class="dueDatahourly">{{$v->due_date ?? N/A}}</td>
                                                                                                </tr>
                                                                                                <input type="hidden" value="{{$v->project_id}}" class="projectidhourly">
                                                                                                <input type="hidden" value="{{$v->applicant_id}}" class="applicantIdhourly">
                                                                                                <input type="hidden" value="{{$v->name}}" class="milestoneNamehourly">
                                                                                            </table>
                                                                                        </div>
                                                                                        <div class="col-md-12 p-0">
                                                                                            <div class="col-md-4 p-0">
                                                                                                <div class="col-md-12" style="padding:15px 0px; border-bottom:1px solid #cccccc">
                                                                                                    <input type="number" class="col-md-12 bonusinputhourly-{{$v->project_id}}" oninput="bonusplusmilestonehourly('{{$v->project_id}}')" placeholder="{{trans('common.give_bonus')}} ({{trans('common.optional')}})"/>

                                                                                                </div>
                                                                                                <strong class="col-md-12" style="padding-top:8px">{{trans('common.total')}}:<span class="bonusplusmilestonehourly-{{$v->project_id}}"></span></strong>
                                                                                            </div>
                                                                                            <div class="passwordhourly-{{$v->id}}" style="margin-top: 85px;margin-left:140px;display:none">
                                                                                                <input type="password" autocomplete="off" class="form-control passhourly" name="password" placeholder="Please Confirm your password" required>
                                                                                            </div>
                                                                                            <div class="error" style="margin-top: 10px;margin-left:140px;display:none">
                                                                                                <p class="error" style="color:red">Incorrect Password</p>
                                                                                            </div>
                                                                                            <div class="col-md-8" style="text-align:right; padding-right:0; padding-left:0; padding-top:35px">
                                                                                                <a href="javascript:;" onclick="myConfirmMilestonehourly('{{$v->id}}')" type="button" class="btn approve-pay-btn" style="padding:15px">{{trans('common.approve_pay')}}</a>
                                                                                            </div>
                                                                                        </div>
                                                                                    @endif
                                                                                @endif
                                                                            @endforeach
                                                                            @if($currentMilesNumber == 0)
                                                                                <p class="col-md-12" style="text-align:center; margin:10px 0;">{{trans('common.no_current_milestone')}}</p>
                                                                            @endif

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif
                                        @endforeach
                                    @endif

                                    <!-- newrelaseMileStoneHourlyRate -->
                                        <div id="releaseMilestoneModalHourly" class="modal fade">
                                            <div class="milestonemodal-content2">
                                                <form accpet="">
                                                    <input type="hidden" name="applicant_id"
                                                           class="hourlyapplicant_id"
                                                           value="">
                                                    <input type="hidden" name="project_id"
                                                           class="hourlyproject_id"
                                                           value="">
                                                    <a class="closeModal" data-dismiss="modal">&#10005;</a>
                                                    <h class="col-md-11 modaltitle">{{trans('common.create_milestone')}}</h>
                                                    <p class="col-md-12"
                                                       style="text-align: center; margin-top: 0; font-size: 9pt;">
                                                        {{trans('common.create_milestone_simple_click')}}</p>
                                                    <p class="col-md-12"
                                                       style="font-weight: 600; margin:15px 0">{{trans('common.milestone')}}</p>

                                                    <div class="col-md-3"
                                                         style="padding: 2px 5px">
                                                        <input class="form-control hourlymilestones_amount"
                                                               style="background:url(/images/icons/$icon.png) no-repeat; background-position:2% 45%; text-align:center"
                                                               type="text"
                                                               placeholder="{{trans('common.deposit_amount')}}">
                                                    </div>

                                                    <div class="col-md-3"
                                                         style="padding: 2px 5px">
                                                        <input class="form-control hourlymilestones_name"
                                                               type="text"
                                                               placeholder="{{trans('common.milestone_name')}}">
                                                    </div>

                                                    <div class="col-md-3"
                                                         style="padding: 2px 5px">
                                                        <input class="form-control hourlymilestones_description"
                                                               type="text"
                                                               placeholder="{{trans('common.milestone_description')}}">
                                                    </div>


                                                    <div class="col-md-3 date form_datetime"
                                                         style="padding: 2px 5px">
                                                        <input
                                                                type="text" class="form-control hourlymilestones_duedate"
                                                                readonly="" size="16">
                                                        <span class="input-group-btn"
                                                              style="padding-top: 2px">
                                                                                <button type="button" class="btn default date-set p-0">
                                                                                    <i class="fa fa-calendar"></i>
                                                                                </button>
                                                                        </span>
                                                    </div>

                                                    <div class="col-md-3 pull-right"
                                                         style="padding-top:5%">
                                                        <button class="btn mile-submit-btn confirmMilestoneHourly"
                                                                type="submit">{{trans('common.submit')}}</button>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    <!-- newrelaseMileStoneHourlyRate -->

                                    <div class="col-md-12 footer-section">
                                        <div class="row">
                                            <div class="col-md-6 footer-section-right">
                                                @if(!$viewAllAwarded)
                                                    <div class="col-md-6 footer-section-right">
                                                        <a href="?viewAllAwarded=true">{{trans('common.view_all')}}</a>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-md-6 footer-section-left-paginate text-right">
                                                @if ($awardedProjects->lastPage() > 1)
                                                    <div class="pull-right">
                                                        {{$awardedProjects->links()}}
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>

                </div>
                <!-- END PAGE BASE CONTENT -->
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->

        <!-- password modal -->

        @include('frontend.includes.modalCreateExtraSize')
        @include('frontend.includes.modalEvaluationOrder')
    </div>
    <!-- END CONTENT -->
@endsection

@section('footer')
    <script src="/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" defer="defer" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-star-rating/js/star-rating.js" defer="defer" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js" type="text/javascript"></script>
    <script src="/custom/js/frontend/myPublishOrder.js" defer="defer" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
    <script>

        {{--function () {--}}
        {{--$(document).ready(function () {--}}
        {{--$('#lastNumberDays').on('change', function (e) {--}}
        {{--var optionSelected = $("option:selected", this);--}}
        {{--var valueSelected = this.value;--}}

        {{--var url = "{{ route('frontend.mypublishorder') }}" + "?status=" + "{{ $status }}" + "&lastNumberDays=" + valueSelected;--}}
        {{--window.location.href = url;--}}
        {{--});--}}
        {{--});--}}
        {{--}(jQuery);--}}

        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });

    </script>

    <script>
          var dots = document.querySelectorAll("#dots");
          var moreDesc = document.querySelectorAll("#moreDesc");
          var moreLink = document.querySelectorAll("#moreLink");
          function readMore($projNo) {
            var i = $projNo
            if (dots[i].style.display === "none") {
            dots[i].style.display = "inline";
            moreLink[i].innerHTML = "Read More";
            moreDesc[i].style.display = "none";
          } else {
            dots[i].style.display = "none";
            moreLink[i].innerHTML = "Read Less";
            moreDesc[i].style.display = "inline";
          }
        }
    </script>

    <script>
        function bonusplusmilestone(id){
            let previousAmount = $('.milestoneAmount-'+id).html().replace('$','');
            let amount = $('.bonusAmount-'+id).val();
            let addAmount = Number(previousAmount) + Number(amount);
            $('.bonusplusmilestone-'+id).html('$'+addAmount);
            console.log(addAmount);
        }
        function bonusplusmilestonehourly(id)
        {
            let previousAmount = $('.milestoneAmounthourly-'+id).html().replace('$','');
            let amount = $('.bonusinputhourly-'+id).val();
            let addAmount = Number(previousAmount) + Number(amount);
            $('.bonusplusmilestonehourly-'+id).html('$'+addAmount);
            console.log(addAmount);
        }
        function openTab(evt, TabName) {
            var i, x, tablinks;
            x = document.getElementsByClassName("Tab");
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablink");
            for (i = 0; i < x.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" tab-button-green", "");
            }
            document.getElementById(TabName).style.display = "block";
            evt.currentTarget.className += " tab-button-green";
        }
    </script>
    <script>
            $(function(){
                $('.createMilestone').on('click', function(){
                    console.log($(this).attr('data-projectId'));
                    window.projectId = $(this).attr('data-projectId');
                    window.applicant = $(this).attr('data-applicantId');
                });
            });

            function newMilestoneModalFixed(projectId, applicantId) {
                $('#relaseMilestoneModalfixed').modal('show');
                $('.fixedapplicant_id').val(applicantId);
                $('.fixedproject_id').val(projectId);
            }

            $('.confirmMilestonefixed').on('click', function(e){
                let projectId = $('.fixedproject_id').val();
                let applicantId = $('.fixedapplicant_id').val();
                let milestones_amount = $('.milestones_amount').val();
                let milestones_name = $('.milestones_name').val();
                let milestones_description = $('.milestones_description').val();
                let milestones_duedate = $('.milestones_duedate').val();
                $.ajax({
                    url:'/createMilestone/'+projectId,
                    type:'POST',
                    data:{
                        "amount" : milestones_amount,
                        "name" : milestones_name,
                        "description" : milestones_description,
                        "d_date" : milestones_duedate,
                        "project_id": projectId,
                        "applicant_id": applicantId,
                    },
                    success: function(response) {
                        if(response.status == 'failure') {
                            toastr.error(response.message);
                        } else {
                            toastr.success(response.message);
                            $('.modal').modal('hide');
                            $('.milestones_amount').val('');
                            $('.milestones_name').val('');
                            $('.milestones_description').val('');
                            $('.milestones_duedate').val('');
                        }
                    },
                });
                e.preventDefault();
                return false;

            });


            function newMilestoneModalHorly(projectId , applicantId)
            {
                $('#releaseMilestoneModalHourly').modal('show');
                $('.hourlyapplicant_id').val(applicantId);
                $('.hourlyproject_id').val(projectId);
            }

            $('.confirmMilestoneHourly').click(function (e) {
                let applicantId = $('.hourlyapplicant_id').val();
                let projectId = $('.hourlyproject_id').val();
                let hourlymilestones_amount = $('.hourlymilestones_amount').val();
                let hourlymilestones_name = $('.hourlymilestones_name').val();
                let hourlymilestones_description = $('.hourlymilestones_description').val();
                let hourlymilestones_duedate = $('.hourlymilestones_duedate').val();
                $.ajax({
                    url:'/createMilestone/'+projectId,
                    type:'POST',
                    data:{
                        "amount" : hourlymilestones_amount,
                        "name" : hourlymilestones_name,
                        "description" : hourlymilestones_description,
                        "d_date" : hourlymilestones_duedate,
                        "project_id": projectId,
                        "applicant_id": applicantId,
                    },
                    success: function(response) {
                        if(response.status == 'failure') {
                            toastr.error(response.message);
                        } else {
                            toastr.success(response.message);
                            $('.modal').modal('hide');
                            $('.milestones_amount').val('');
                            $('.milestones_name').val('');
                            $('.milestones_description').val('');
                            $('.milestones_duedate').val('');
                        }
                    },
                });
                e.preventDefault();
                return false;
            });

            function myConfirmMilestonehourly(val){
                if($('.passwordhourly-'+val).css('display') == 'none'){
                    $('.passhourly').val("");
                }
                $('.passwordhourly-'+val).css('display','block');
                var password = $('.passhourly').val();
                if(password !== '')
                {
                    var projectId = $('.projectidhourly').val();
                    var Amount = $('.milestoneAmounthourly-'+projectId).val();
                    var amount = Amount.replace('$', "");
                    var dueData = $('.dueDatahourly').val();
                    var milestoneId = val;
                    var applicantId = $('.applicantIdhourly').val();
                    var milestoneName = $('.milestoneNamehourly').val();
                    $.ajax({
                        url:'/giveBonusP/'+projectId,
                        type:'POST',
                        data:{
                            'amount':amount,
                            'milestone_id':milestoneId,
                            'applicant_id':applicantId,
                            'due_date':dueData,
                            'name':milestoneName,
                            'password':password
                        },

                        success: function(response)
                        {
                            console.log(response);
                            if(response.status=='error'){
                                alertError(response.message);
                            }
                            if(response.status == 'success') {
                                toastr.success('{{trans('common.transaction_completed')}}');
                                $('.modal').modal('hide');
                                setTimeout(function(){
                                    window.location.reload();
                                }, 3000)
                            }

                        },

                    });
                } else {
                    toastr.error('please enter your confirm password');
                }
            }


            function myConfirmMilestone(val){
                if($('.password-'+val).css('display') == 'none'){
                    $('.pass').val("");
                }
                $('.password-'+val).css('display','block');
                var password = $('.pass').val();
                if(password !== '')
                    {
                        var projectId = $('.projectid').val();
                        var Amount = $('.bonusAmount-'+projectId).val();
                        var amount = Amount.replace('$', "");
                        var milestoneId = val;
                        var applicantId = $('.applicantId').val();
                        var dueData = $('.dueData').text();
                        var milestoneName = $('.milestoneName').val();
                        $.ajax({
                    url:'/giveBonusP/'+projectId,
                    type:'POST',
                    data:{
                        'amount':amount,
                        'milestone_id':milestoneId,
                        'applicant_id':applicantId,
                        'due_date':dueData,
                        'name':milestoneName,
                        'password':password
                    },

                    success: function(response)
                    {
                        console.log(response);
                        if(response.status=='error'){
                            alertError(response.message);
                        }
                        if(response.status == 'success') {
                            toastr.success('{{trans('common.transaction_completed')}}');
                            $('.modal').modal('hide');
                            setTimeout(function(){
                                window.location.reload();
                            }, 3000)
                        }

                    },

                });
            } else {
                    toastr.error('please enter your confirm password');

                }
            }

            //confirmMileStone



    </script>
@endsection
