@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.post_project_title') }}
@endsection

@section('keywords')
    {{ trans('common.post_project_keyword') }}
@endsection

@section('description')
    {{ trans('common.post_project_desc') }}
@endsection

@section('author')
    Xenren
@endsection

@section('header')
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<link href="/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
	<!-- END PAGE LEVEL PLUGINS -->
    <link href="/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
	<link href="/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
	<link href="/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.css" rel="stylesheet" type="text/css" />
	<link href="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/dropzone-4.0.1/min/dropzone.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/global/plugins/dropzone-4.0.1/min/basic.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css"/>
    <link href="/custom/css/frontend/postProject.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/modalAddNewSkillKeywordv2.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <style>
        .setMPPAGEEDITDIV{
            clear:both;
        }
    </style>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            @include('frontend.flash')
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
            	<div class="col-md-12">
            		<div class="portlet light bordered">
            			<div class="portlet-body form">
							{!! Form::open(['route' => 'frontend.mypublishorder.posteditproject', 'id' => 'add-project-form', 'window.confirm("false")', 'method' => 'post', 'class' => 'horizontal-form', 'action' => 'frontend.mypublishorder']) !!}
            					<div class="form-wizard" >
            						<div class="form-body">
                                        <div id="project-alert-container"></div>
            							<h4>
            							    <strong>
                                                <i class="fa fa-file-text-o" aria-hidden="true"></i> 
                                                {{ trans('common.edit_post') }}
                                            </strong> <!-- Need to change -->
            							<hr>
                                        </h4>
            							<div class="tab-content">
            								<div class="alert alert-danger display-none">
            									<button class="close" data-dismiss="alert"></button> {{ trans('order.some_error') }} </div>
            								<div class="alert alert-success display-none">
            									<button class="close" data-dismiss="alert"></button> {{ trans('order.validate_success') }} </div>

                                            <div class="form-group full-headding-div">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h4>
                                                            {{ trans('common.jobs_description') }}
                                                        </h4>
                                                    </div>
                                                </div>
                                           </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4 text-right">
                                                        <label class="control-label">{{ trans('common.name_your_project_posting') }}</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        {!! Form::text('name', empty($applicant->title)?$project->name: $applicant->title, [
                                                            'class' => 'form-control custom-input-text',
                                                            'placeholder' => trans('common.project_name'),
                                                            'id'=>'tbxName'
                                                        ]) !!}
                                                        </br>
                                                        <span id="project-name-require" class="error-message"><i class="fa fa-exclamation-circle"></i> {{ trans('common.field_required') }}</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4 text-right">
                                                        <label class="control-label">
                                                            {{ trans('common.describe_the_work_to_be_done') }}
                                                        </label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div id="snDescription">{{empty($applicant->description)?strip_tags($project->description):strip_tags($applicant->description)}}</div>
                                                        {{--{!!--}}
                                                            {{--Form::textarea('description', '', [--}}
                                                                {{--'class' => 'form-control custom-input-text',--}}
                                                                {{--'placeholder' => trans('common.example_job_description'),--}}
                                                                {{--'maxlength' => '5000',--}}
                                                                {{--'rows' => '5',--}}
                                                                {{--'id'=>'tbxDescription'--}}
                                                                {{--])--}}
                                                        {{--!!}--}}
                                                        {!!
                                                            Form::hidden('description', empty($applicant->description)?strip_tags($project->description):strip_tags($applicant->description), [
                                                                'class' => 'form-control custom-input-text',
                                                                'id'=>'tbxDescription'
                                                            ])
                                                        !!}
                                                        <span class="pull-right left-char-count"><span id="noOfCharDescription">5000</span> {{ trans('common.characters_left') }}</span>
                                                        </br>
                                                        <span id="description-require" class="error-message"><i class="fa fa-exclamation-circle"></i> {{ trans('common.field_required') }}</span>
                                                    </div>
                                                </div>    
                                            <hr >
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4 text-right">
                                                        <label class="control-label"> 
                                                            {{ trans('common.enter_skill_needed_optional') }} <br/>
                                                            <a href="javascript:;" class="f-s-10" data-toggle="modal" data-target="#modelAddNewSkillKeyword" >{{ trans('common.dont_found_the_keyword') }}</a>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div id="magicsuggest"></div>
                                                        
                                                        {{ Form::hidden('skill_id_list', empty($applicant->skill_id_string)?$skillIdListString: $applicant->skill_id_string, ['id'=>'skill_id_list']) }}

                                                        </br>
                                                        <span id="skill-require" class="error-message"><i class="fa fa-exclamation-circle"></i> {{ trans('common.field_required') }}</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group full-headding-div">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h4>
                                                            {{ trans('common.rate_and_availability') }}
                                                        </h4>
                                                    </div>
                                                </div>
                                           </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label class="control-label">{{ trans('common.how_would_you_like_to_pay') }}</label>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    @foreach( $pay_types as $key => $pay_type )
                                                    <div class="col-md-3 pay-type-custom">
                                                        {!! Form::radio('pay_type', $key, ($key == $type) ? true : false, ['class'=>'icheck pay_ty', 'id' => 'pay-type-' . $key
                                                        ] ) !!}
                                                        <label class="pay-type-label" for="pay-type-{!!$key!!}"
                                                            style="
                                                            @if($key == $type)
                                                                {{'border: 1px solid rgb(88, 175, 42);'}}
                                                            @endif
                                                            "
                                                        >
                                                            <img src="/images/clock1.jpg">
                                                            <p>{{ $pay_type }}</p>
                                                        </label>
                                                    </div>
                                                    @endforeach
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                    <span id="pay-require" class="error-message"><i class="fa fa-exclamation-circle"></i> {{ trans('common.field_required') }}</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group div-price" style=" @if($type == 3) {{ 'display:none'}} @endif ">
                                                <label class="control-label" id="label-div-price-fix" style="@if($type == 3) {{ 'display:none'}} @endif">
                                                    @if( app()->getLocale() == "en" )
                                                        {{ trans('common.price_usd') }}
                                                    @elseif( app()->getLocale() == "cn" )
                                                        {{ trans('common.price_cny') }}
                                                    @endif
                                                </label>
                                                <label class="control-label" id="label-div-price-hour" style="@if($type == 3) {{ 'display:none'}} @endif">
                                                    @if( app()->getLocale() == "en" )
                                                        {{ trans('common.price_usd_per_hour') }}
                                                    @elseif( app()->getLocale() == "cn" )
                                                        {{ trans('common.price_cny_per_hour') }}
                                                    @endif
                                                </label>

                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div>
                                                            {!! Form::text('reference_price', empty($applicant->reference_price)?$project->reference_price: $applicant->reference_price, ['class'=>'form-control', 'id' => 'reference_price' ] ) !!}
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                    </br>
                                                    <span id="fixed-price-require" class="error-message"><i class="fa fa-exclamation-circle"></i> {{ trans('common.field_required') }}</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <hr>
                                                <button type="button" id="project-submit-btn"
                                                        class="btn button-submit btn-green-custom full-btn"> {{ trans('common.save_change') }}
                                                </button>
                                                <p></p>
                                                <a href = "Javascript:;"
                                                    class="btn btn-red-custom full-btn" id="cancelbtn">
                                                        {{trans('common.cancel')}}
                                                </a>
                                            </div>

                                            <div class="setMPPAGEEDITDIV"></div>

                                            {{ Form::hidden('applicant_id', $project->id) }}
                                        </div>
                                    </div>
            					</div>
							{!! Form::close() !!}
            			</div>
            		</div>
            	</div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->
    @include('frontend.includes.modalLoadPreviousProject')
    @include('frontend.includes.modalAddNewSkillKeywordv2', [
        'tagColor' => \App\Models\Skill::TAG_COLOR_WHITE
    ])
@endsection


@section('footer')
    <script type="text/javascript">

        var lang = {
            type_or_click_here: "{{ trans('common.type_or_click_here') }}",
            characters_left: "{{ trans('common.characters_left') }}",
            please_select: "{{ trans('common.please_select') }}",
            unable_to_request : "{{ trans('common.unable_to_request') }}",
            max_upload_eight_documents : "{{ trans('common.max_upload_eight_documents') }}",
            please_input_project_name : "{{ trans('common.please_input_project_name') }}",
            please_choose_category : "{{ trans('common.please_choose_category') }}",
            please_choose_country : "{{ trans('common.please_choose_country') }}",
            please_choose_language : "{{ trans('common.please_choose_language') }}",
            please_input_description : "{{ trans('common.please_input_description') }}",
            please_choose_skill : "{{ trans('common.please_choose_skill') }}",
            please_choose_pay_type : "{{ trans('common.please_choose_pay_type') }}",
            please_choose_commitment_time : "{{ trans('common.please_choose_commitment_time') }}",
            please_choose_freelance_type : "{{ trans('common.please_choose_freelance_type') }}",
            please_input_question : "{{ trans('common.please_input_question') }}",
            please_input_reference_price : "{{ trans('common.please_input_reference_price') }}",
            remove : "{{ trans('common.remove') }}",
            image_is_bigger_than_5mb : "{{ trans('common.image_is_bigger_than_5mb') }}",
            you_can_not_upload_any_more_files : "{{ trans('common.you_can_not_upload_any_more_files') }}",
            drop_files_here_to_upload : "{{ trans('common.drop_files_here_to_upload') }}",
            cancel_upload : "{{ trans('common.cancel_upload') }}",
            invalid_file_type_for_post_project : "{{ trans('common.invalid_file_type_for_post_project') }}",
            example_job_description : "{{ trans('common.example_job_description') }}",
            field_required : "{{ trans('common.field_required') }}",
            new_skill_keywords : "{{ trans('common.new_skill_keywords') }}",
            please_use_wiki_or_baidu_support_your_keyword : "{{ trans('common.please_use_wiki_or_baidu_support_your_keyword') }}",
            one_of_skill_input_empty : "{{ trans('common.one_of_skill_input_empty') }}",
            maximum_ten_skill_keywords_allow_to_submit : "{{ trans('common.maximum_ten_skill_keywords_allow_to_submit') }}",
            submit_successful: "{{ trans('common.submit_successful') }}",
        };

        var url = {
            getInputFile : "{{ route('frontend.postproject.getinputfile') }}",
            uploadDescriptionImage : "{{ route('frontend.postproject.uploaddescriptionimage') }}",
        };
    </script>

    <script src="/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-summernote/lang/summernote-zh-CN.js" type="text/javascript"></script>

    <script src="/custom/js/frontend/modalAddNewSkillKeywordv2.js" type="text/javascript"  ></script>
    <script src="/assets/global/plugins/dropzone-4.0.1/dropzone.js" type="text/javascript" ></script>
    <script src="/custom/js/dropzone-config-portfolio.js" type="text/javascript" ></script>

    <script type="text/javascript">

        $('#cancelbtn').on('click',function(){
           if(window.ischange){
              var a = window.confirm('{{ trans('common.are_you_sure_you_want_to_discard_your_changes') }}');
              if (a){
                  window.location.href = '{{route('frontend.mypublishorder')}}';
              }
           }else {
               window.location.href = '{{route('frontend.mypublishorder')}}';
           };
        });

        $(document).ready(function () {
            window.ischange = false;
            $("#add-project-form :input").change(function() {
                window.ischange = true;
            });
            $('.btn-add-more-question').click();
            
            });

            if($("#pay_type_2").is(':checked') ){
                alert("123");
            }

         if( $("#pay_type_1").is(':checked') ){
                $("input[name='pay_type'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
         } else if( $("#pay_type_2").is(':checked') ){
                $("input[name='pay_type'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
         } else if( $("#pay_type_3").is(':checked') ){
                $("input[name='pay_type'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
         } 

        
        $("input[name='pay_type']").on('change', function () {
            if ($(this).val() == '1') {
                $("input[name='pay_type'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
                $("input[name='pay_type'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='pay_type'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');

            } else if ($(this).val() == '2') {
                $("input[name='pay_type'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
                $("input[name='pay_type'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='pay_type'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');

            }
            if ($(this).val() == '3') {
                $("input[name='pay_type'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
                $("input[name='pay_type'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='pay_type'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');

            }

        });
        //                $('.pay_ty').on('focusout', function(){
        //                    $(this).parent('.pay-type-custom').find('.pay-type-label').css('border','1px solid #e1e1e1');
        //
        //                });

    </script>
    <script type="text/javascript">
        +function () {

            $('.error-message').hide();
            

            //skill
            var skillList = <?php echo $skillList; ?>;
            var skill_id_list = $('#skill_id_list').val();
            var selectedSkillList = [];
            if(skill_id_list != ""){
                selectedSkillList = skill_id_list.split(",")
            }
            var ms = $('#magicsuggest').magicSuggest({
                allowFreeEntries: false,
                allowDuplicates: false,
                placeholder: lang.type_or_click_here,
                data: skillList,
                value: selectedSkillList,
                method: 'get',
                renderer: function(data){
                    if( data.description == null )
                    {
                        return '<b>' + data.name + '</b>' ;
                    }
                    else
                    {
                        var name = data.name.split("|");

                        return '<b>' + name[0] + '</b> | <span class="search-desc">' + data.description + '</span>';
                    }

                },
                selectionRenderer: function(data){
                    result = data.name;
                    name = result.split("|")[0];
                    return name;
                }
            });
            $(ms).on(
                'selectionchange', function(e, cb, s){
                    $('#skill_id_list').val(cb.getValue());
                    if($('#magicsuggest').hasClass('error-border') && $('input[name="skill_id_list"]').val() != 0){
                        $('#magicsuggest').removeClass('error-border');
                        $("#magicsuggest").attr('style', '');
                        $("#skill-require").hide();
                    }
            });

            

            $('#btn-reset-project').click(function(e){
                $('.error-message').hide();
                // $('#prev_project_name').html(lang.please_select);
                $('.btn.fa-close').hide();

                //reset previous project
                //reused previous job
                // $('#pre_project_id').val(0);

                //project name
                $('#tbxName').val("");

                //category and subcategory
                // $('#category_name').html(lang.please_select);
                // $('#category_id').val(0);

                //description
                $('#tbxDescription').val("");
                $('#noOfCharDescription').html(5000);
                $('#snDescription').summernote('code', '');

                //attachment
                // var addNewFileButton = '<div id="btn-add-new-file" class="attachment">' +
                //                             '<div class="attachment-no-file">' +
                //                                 '<span class="fileinput-new p-l-1">' +
                //                                     '<span class="btn-file btn-add-file-custom">' +
                //                                         '<i class="fa fa-plus" aria-hidden="true"></i>' +
                //                                     '</span>' +
                //                                 '</span>' +
                //                             '</div>' +
                //                         '</div>';
                // $('#attachments-sect').html(addNewFileButton);


                //skills
                var skillList = [];
                var ms = $('#magicsuggest').magicSuggest({});
                ms.clear();
                ms.setValue(skillList);

                //pay type
                $("input[name='pay_type'][value='1']").prop("checked", false);
                $("input[name='pay_type'][value='2']").prop("checked", false);
                $("input[name='pay_type'][value='3']").prop("checked", false);
                $("input[name='pay_type']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $(".div-price").hide();

                // //time commitment
                // $("input[name='time_commitment'][value='1']").prop("checked",false);
                // $("input[name='time_commitment'][value='2']").prop("checked",false);
                // $("input[name='time_commitment'][value='3']").prop("checked",false);
                // $("input[name='time_commitment']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');

                // //freelance type
                // $("input[name='freelance_type'][value='1']").prop("checked",false);
                // $("input[name='freelance_type'][value='2']").prop("checked",false);
                // $("input[name='freelance_type'][value='3']").prop("checked",false);

                // //questions
                // $("textarea[id ^='tbxQuestion-']").each(function(){
                //     var questionId = parseInt(this.id.replace("tbxQuestion-", ""));
                //     $('#question-' + questionId).remove();
                // });

                // //country
                // var countryIdList = [];
                // var msCountries = $('#msCountries').magicSuggest({});
                // msCountries.clear();
                // msCountries.setValue(countryIdList);

                // //language
                // var languageIdList = [];
                // var msLanguages = $('#msLanguages').magicSuggest({});
                // msLanguages.clear();
                // msLanguages.setValue(languageIdList);
            });

            if($('#tbxDescription').val() != null)
                updateCountTbxDescription();


            // $.each(arrUploadFile, function( index, value ) {
            //     var mockFile = {name: value.name, size: value.size, type: value.type };
            //     myDropzone.emit("addedfile", mockFile);
            //     myDropzone.createThumbnailFromUrl(mockFile, '/uploads/temp/' + value.name);
            //     myDropzone.emit("complete", mockFile);
            //     getFileTypeIcon(value.type);

            //     $('<input>', {
            //         'type' : 'hidden',
            //         'name' : 'hidUploadedFile[]',
            //         'value' : value.name
            //     }).appendTo('#file-sect');
            // });

            $('#add-project-form').makeAjaxForm({
                submitBtn: '#project-submit-btn',
                alertContainer: '#project-alert-container',
                beforeFunction: validate,
                afterSuccessFunction: function (response, $el, $this) {
                    if( response.status == 'success')
                    {
                        alertSuccess(lang.submit_successful);
                        var url = '/myPublishOrder?success_post=true&country_id='+response.country_id;
                        setTimeout(function () {
                            window.location.href = url;
                        },2000);
                    }
                }
            });

            var language = $('html').attr('lang');
            var summernoteLanguage = '';
            if( language == "cn" )
            {
                summernoteLanguage = "zh-CN";
            }
            else
            {
                summernoteLanguage = "en-US";
            }

            $('#snDescription').summernote({
                placeholder: lang.example_job_description,
                lang: summernoteLanguage,
                height: 300,
                toolbar:[
                    ['style',['bold','fontsize']],
                    ['insert', ['link', 'picture','codeview']],
                ],
                callbacks: {
                    onChange: function(contents, $editable) {
                        var stripHtmlTag = contents.replace(/<(?:.|\n)*?>/gm, '');

                        $('#tbxDescription').val(contents);
                        updateCountTbxDescription();

                        if(stripHtmlTag.length > 0 )
                        {
                            if($('.note-editor').hasClass('error-border') && $('#tbxDescription').val() != '' ){
                                $('.note-editor').removeClass('error-border');
                                $("#description-require").hide();
                            }
                        }
                        else
                        {
                            $('.note-editor').addClass('error-border');
                            $("#description-require").show();
                        }
                    },
                    onImageUpload: function(files) {
                        for (var i = 0; i < files.length; i++) {
                            send(files[i]);
                        }
                    }
                }
            });

            function send(file)
            {
                if (file.type.includes('image'))
                {
                    var name = file.name.split(".");
                    name = name[0];
                    var data = new FormData();
                    data.append('file', file);
                    $.ajax({
                        url: url.uploadDescriptionImage,
                        type: 'POST',
                        contentType: false,
                        cache: false,
                        processData: false,
                        dataType: 'JSON',
                        data: data,
                        success: function (resp) {
                            if(resp.status == "success")
                            {
                                $('#snDescription').summernote('insertImage', resp.data['url']);
                            }
                            else
                            {
                                alert(resp.msg);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert(textStatus + " " + errorThrown);
                        }
                    });
                }
            }

            
            $("#reference_price").blur(function(){
                if($(this).hasClass('error-border') && $(this).val() != '' ){
                    $(this).removeClass('error-border');
                    $("#fixed-price-require").hide();
                }
            });
            

            $('input[id^="pay-type-"]').click(function() {

                if($('#pay-type-1').is(':checked') ){

                    $('.div-price').show();
                    $('#label-div-price-hour').show();
                    ;
                    $('#label-div-price-fix').hide();

                }else if( $('#pay-type-2').is(':checked') ){

                    $('.div-price').show();

                    $('#label-div-price-hour').hide();
                    $('#label-div-price-fix').show();

                }else if( $('#pay-type-3').is(':checked') ){

                    $('.div-price').hide();
                }

                $('#reference_price').val('');

                if($("input[name='pay_type']").parent('.pay-type-custom').find('.pay-type-label').hasClass("error-border")){
                    $("input[name='pay_type']").parent('.pay-type-custom').find('.pay-type-label').removeClass("error-border");
                    $("#pay-require").hide();
                }
            });

            function updateCountTbxDescription (e) {
                var qText = $("#tbxDescription").val();

                if(qText.length <= 5000) {
                    $("#noOfCharDescription").html(5000 - qText.length );
                } else {
                    jQuery("#noOfCharDescription").html(0);
                    $("#tbxDescription").val(qText.substring(0,5000));
                }
            }

            function updateCountTbxQuestion (e) {
                var questionId = parseInt(this.id.replace("tbxQuestion-", ""));

                var qText = $("#tbxQuestion-" + questionId ).val();

                if(qText.length <= 256) {
                    $("#noOfCharQuestion-" + questionId ).html(256 - qText.length );
                } else {
                    jQuery("#noOfCharQuestion-" + questionId).html(0);
                    $("#tbxQuestion-" + questionId).val(qText.substring(0,256));
                }
            }

    function validate(){
        errName = lang.please_input_project_name;
        errDescription = lang.please_input_description;
        errSkillId = lang.please_choose_skill;
        errPayType = lang.please_choose_pay_type;
        errReferencePrice = lang.please_input_reference_price;
        err = g_lang.error;

        var errors = [];
        var scrollToError = "";

        if ( $('input[name="reference_price"]').val() == '' &&
                (
                    $('#pay-type-1').is(':checked') ||
                    $('#pay-type-2').is(':checked')
                )
        )
        {
            errors.push(errReferencePrice);
            $("#reference_price").addClass('error-border');
            $("#fixed-price-require").show();
            scrollToError = "reference_price";
        }

        if (!$('input[name="pay_type"]').is(':checked')) {
            $("input[name='pay_type']").parent('.pay-type-custom').find('.pay-type-label').addClass('error-border');
            $("#pay-require").show();
            errors.push(errPayType);
            scrollToError = "pay_type";
        }

        if ($('input[name="skill_id_list"]').val() == 0) {
            errors.push(errSkillId);
            $("#magicsuggest").attr('style', 'border-color:red !important;');
            $('#magicsuggest').addClass('error-border');
            $("#skill-require").show();
            scrollToError = "skill";
        }

        //if ($('textarea[name="description"]').val() == '') {
        //    errors.push(errDescription);
        //    $("#tbxDescription").addClass('error-border');
        //    $("#description-require").show();
        //    scrollToError = "description";
        //}

        if ($('input[name="description"]').val() == '') {
            errors.push(errDescription);
            $(".note-editor.note-frame").addClass('error-border');
            $("#description-require").show();
            scrollToError = "description";
        }

        if ($('input[name="name"]').val() == '') {
            errors.push(errName);
            $("#tbxName").addClass('error-border');
            $("#project-name-require").show();
            scrollToError = "name";
        }

        //reset the error
        $("#tbxName").attr('style', '');
        $("#slcCountry").attr('style', '');
        $("#slcLanguage").attr('style', '');
        $("#slcCategory").attr('style', '');
        $("#tbxDescription").attr('style', '');
        // $("#magicsuggest").attr('style', '');
        $("input[name='pay_type']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
        if($('#pay-type-1').is(':checked'))
        {
            $("input[name='pay_type'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
        }
        else if($('#pay-type-2').is(':checked'))
        {
            $("input[name='pay_type'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
        }
        else if($('#pay-type-3').is(':checked'))
        {
            $("input[name='pay_type'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
        }

        $("#reference_price").attr('style', '');
        $("input[name='time_commitment']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
        if($('#time-commitment-1').is(':checked'))
        {
            $("input[name='time_commitment'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
        }
        else if($('#time-commitment-2').is(':checked'))
        {
            $("input[name='time_commitment'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
        }
        else if($('#time-commitment-3').is(':checked'))
        {
            $("input[name='time_commitment'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
        }

        // $("div[name='rdFreelanceType']").attr('style', '');
        $("textarea[name='question[]']").attr('style', '');

        switch(scrollToError)
        {
            case "name":
                App.scrollTo($('#tbxName'));
                break;
            case "category":
                App.scrollTo($('#slcCategory'));
                break;
            case "description":
                //App.scrollTo($('#tbxDescription'));
                App.scrollTo($('.note-editor'));
                break;
            case "skill":
                App.scrollTo($('#magicsuggest'));
                break;
            case "pay_type":
                App.scrollTo($("input[name='pay_type']"));
                break;
            case "reference_price":
                App.scrollTo($('#reference_price'));
                break;
            case "time_commitment":
                App.scrollTo($("input[name='time_commitment']"));
                break;
            case "freelance_type":
                App.scrollTo($("div[name='rdFreelanceType']"));
                break;
            case "question":
                App.scrollTo($("textarea[name='question[]']"));
                break;

        }

        if( scrollToError == "" )
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    var showError = function (val) {
        if (val instanceof Array) {
            $(val).each(function (index, value) {
                showError(value);
            });
        } else {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "1000",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }

            toastr["error"](val, err);
        }
    }
            

        }(jQuery);
    </script>
@endsection




