@extends('frontend.layouts.default')

@section('title')
    Blog Article
@endsection

@section('keywords')
    {{ trans('common.find_experts_keyword') }}
@endsection

@section('description')
    {{ trans('common.find_experts_desc') }}
@endsection

@section('author')
    Xenren
@endsection

@section('url')
    https://www.xenren.co/Blog
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
    <style>
        .customStyle{
            font-size:17px;
            margin-top: 0px;
            word-wrap: break-word;
        }
        .customRow{
            margin-bottom: 15px;
        }
        .customStyleBlog{
            float: right;
            display: flex;
            justify-content: flex-end;
        }
        .customStyleCol6{
            display: flex;
        }
        .classCustom{
            display: flex;
            justify-content: center;
            border-top-left-radius: 0px;
            border-bottom-left-radius: 0px;
        }
        .customInputTag {
            border-top-right-radius: 0px;
            border-bottom-right-radius: 0px;
        }
        .customForm{
            width: 100%;
            display: flex;
        }
    </style>
@endsection
@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper" style="margin-bottom: 15px;">
        <!-- BEGIN CONTENT BODY -->
            <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12 customStyleBlog">
                <div class="col-md-4 customStyleCol6">
                    <form action="{{route('searchBlogData',\Session::get('lang'))}}" class="customForm" method="GET">
                        <input type="search" name="searchBlog" value="" class="form-control customInputTag">
                        <button class="btn btn-success classCustom">Search</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="container">
            @foreach($article as $articles)
                <div class="row" style="    padding-left: 20px;
    padding-right: 20px;">
                    @php
                      $title = str_replace(' ','-',$articles->title);
                    @endphp
                    <h1 style="cursor: pointer;    display: inline-block; width: 40%;">
                        <a style="color:#555;" href="{{route('frontend.detail',[\Session::get('lang'),'title' => $title,'id' => $articles->id])}}">{{$articles->title}}</a>
                    </h1>
                    <hr>
                </div>
                 <div class="row customRow">
                     <div class="col-md-5">
                        <img onclick=window.location.href="{{route('frontend.detail',[\Session::get('lang'),'title' => $title,'id' => $articles->id])}}" src="/uploads/articles/{{$articles->files}}" style="width:100%;height: 345px">
                     </div>
                     <div class="col-md-7 customStyle">
                         @php
                           $article = $articles->article_description;
                           $html =  explode( "<img>", $article );
                           $html = preg_replace('/<[^>]*>/', '', $html[0]);
                             if(strlen($html) > 1000) {
                                 $stringCut = substr($html, 0, 1000);
                                 $endPoint = strrpos($stringCut, ' ');
                                 $html = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                echo $html .= '... <a href="'.route('frontend.detail',['id' => $articles->id, 'title' => $articles->title,\Session::get('lang')]).'">Read More</a>';
                             }
                         @endphp
                     </div>
                 </div>
            @endforeach
        </div>
    </div>
@endsection

@section('modal')

@endsection

@section('footer')

@endsection
