@extends('frontend.layouts.default')

@section('title')
    {{$detail->title}}
@endsection

@section('keywords')
@endsection

@section('description')
@php
    $title = str_replace(' ','-',$detail->title);
@endphp
{{$title}} {{$content}}
@endsection

@section('author')
    Xenren
@endsection

@section('url')
    https://www.xenren.co/Blog/detail
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
    <style>
        .customStyle{
            font-size:17px;
            margin-top: 0px;
            word-wrap: break-word;
        }
    </style>
@endsection
@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper" style="margin-bottom: 15px;">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="container">
            <div class="row">
                <h1 style="cursor:pointer;" onclick="window.location.href='/'">{{$detail->title}}</h1>
            </div>
            <hr>
            <div class="row customStyle">
                @php
                    $detail = stripslashes($detail->article_description);
                @endphp
                <p>{!! $detail !!}</p>
            </div>
        </div>
    </div>
@endsection

@section('modal')

@endsection

@section('footer')

@endsection
