@extends('frontend.companyInfo.layouts.default')

@section('title') 
{{ trans('common.terms_service_title') }}
@endsection

@section('keywords')
{{ trans('common.terms_service_keyword') }}
@endsection

@section('description') 
{{ trans('common.terms_service_desc') }}
@endsection

@section('author') 
    Xenren
@endsection

@section('header')
    <link href="/custom/css/frontend/home-new-custom.css" rel="stylesheet" type="text/css" />

    <!-- END HEADER AND FOOTER PAGE STYLES -->
    <link href="/css/company-info.css" rel="stylesheet" type="text/css" />
    <link href="/css/privacy-policy.css" rel="stylesheet" type="text/css" />
    <link href="/css/term.css" rel="stylesheet" type="text/css" >
@endsection

@section('content')
<div class="page-container page-container-main">
    @include('frontend.companyInfo.layouts.menu')
    
    <div class="row banner banner-terms">
        <div class="col-md-12 banner-inner">
            <div class="banner-inner-text">
                <p>{{ trans('home.terms_service_banner_p') }}</p>
                <h1>{{ trans('home.terms_service_banner_h1') }}</h1>
                <span>{{ trans('home.terms_service_banner_span') }}: {{ trans('home.terms_service_banner_spanDate') }}</span>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row body-content">
            <div class="col-md-12">
                <h1>Legends Lair Website.</h1>
                <p>These Website Terms & Conditions ("T&Cs") apply to your access and use of <a href="https://www.xenren.co">https://www.xenren.co</a>  (the "Site"), including all software, data, reports, text, image, sounds, video. and content made available through any portion of the Site (collectively, the "Content"). Content includes all such elements as a whole, as well as individual elements and portions thereof.</p>
            </div>

            <div class="col-md-12">
                <h1>Acceptance of Terms.</h1>
                <p>Legends Lair permits you ("User" or you or "your") to access and use the Site and Content. subject to these T&Cs. By accessing or using any portion of the Site, you acknowledge that you have read, understood, and agree to be bound by these T&Cs. If you are entering into these T&Cs on behalf of a company or other legal entity ("User Entity"), you must have the legal authority to contractually bind such User Entity to these T&Cs, in which case the terms you or your or "User" will refer to such User Entity. If you lack such legal authority to contractually bind or you do not agree with these T&Cs, you must not accept these T&Cs or access or use the site or content.</p>
            </div>

            <div class="col-md-12">
                <h1>T&Cs Updates.</h1>
                <p>Legends Lair reserves the right, at its sole discretion, to change or modify portions of these T&Cs at any time. Legends Lair will post the changes to these T&Cs on the Site and will indicate at the top of this page the date these terms were last revised. It is your responsibility to check the T&Cs periodically for changes. Your continued use of the Site and Content after the date and such changes become effective constitutes your acceptance of the new or revised T&Cs. </p>
            </div>

            <div class="col-md-12">
                <h1>General Conditions/Access and Use.</h1>
                <ul>
                    <li>
                        <h3>Authorization to Access and Use Site and Content.</h3>
                        <p>Subject to your compliance with these T&Cs and the provisions hereof, you may access or use the Site and Content solely for the purpose of your evaluation of Legends Lair and Legends Lair's products and services. You may only link to the Site or Content, or any portion thereof. as expressly permitted by Legends Lair.</p>
                    </li>
                    <li>
                        <h3>Ownership and Restrictions.</h3>
                        <p>All rights, title and interest in and to the Site and Content will remain with and belong exclusively to Legends Lair. You will not (a) sublicense, resell, rent, lease, transfer. assign, time share or otherwise commercially exploit or make the Site and any Content available to any third party. (b) use the Site and Content in any unlawful manner (including without limitation in violation of any data, privacy or export control laws) or in any manner that interferes with or disrupts the integrity or performance of the Site and Content or their related components, or (c) modify, adapt or hack the Site and Content to, or try to, gain unauthorized access to the restricted portions of the Site and Content or related systems or networks (i.e., circumvent any encryption or other security measures, gain access to any source code or any other underlying form of technology or information and gain access to any part of the Site and Content, or any other products or services of Legends Lair that are not readily made available to the general public).
                            <br><br>    You are not permitted to copy, modify, frame, repost, publicly perform or display, sell, reproduce. distribute, or create derivative works of the Site and Content, except that you may download, display, and print one copy of the publicly available materials (i.e., the Content that does not require an Account name or password to access) on any single computer solely for your personal, non-commercial use, provided that you do not modify the material in any way and you keep intact all copyright trademark and other proprietary notices. You agree not to access the Site or Content by any means other than through the interface that is provided by Legends Lair to access the same. You may not use any "page-scrape: "deep-link: "spider" or "robot or other automatic program. device, algorithm or methodology, or any similar manual process to access, copy, acquire, or monitor any portion of the Site or any Content, or in any way reproduce or circumvent the presentation or navigational structure of the Site or any Content, to obtain or attempt to obtain any Content or other information through any means not made generally available through the Site by Legends Lair. Legends Lair reserves the right to take any lawful measures to prevent any such activity. You may not forge headers or otherwise manipulate identifiers in order to disguise the origin of any message or transmittal you send to Legends Lair on or through the Site or any service offered on or through the Site. You may not pretend that you are, or that you represent someone else, or impersonate any other individual or entity.</p>
                    </li>
                    <li>
                        <h3>Responsibility For Your Data.</h3>
                        <p>You are solely responsible for all data, information and other content, that you upload, post or otherwise provide or store hereafter posting in connection with or relating to the Site.</p>
                    </li>
                    <li>
                        <h3>Reservation of Rights.</h3>
                        <p>Legends Lair and its licensors each own and retain their respective rights in and to all logos, company names, marks, trademarks, copyrights, trade secrets, know-how patents and patent applications that are used or embodied in or otherwise related to the Site and Content. Legends Lair grants no rights or licenses (implied by estoppel, or otherwise) whatsoever to you under these T&Cs. </p>
                    </li>
                </ul>
            </div>

            <div class="col-md-12">
                <h1>Use of Intellectual Property.</h1>
                <ul>
                    <li>
                        <h3>Rights in User Content.</h3>
                        <p>By posting your information and other content ("User Content') on or through the Site and Content, you grant Legends Lair a worldwide, non-exclusive, perpetual, irrevocable, royalty-free, fully paid, sublicensable and transferable license to use, modify, reproduce, distribute, display, publish and perform User Content in connection with the Site and Content. Legends Lair has the right, but not the obligation, to monitor the Site and Content and User Content. Legends Lair may remove or disable any User Content at any time for any reason. or for no reason at all.</p>
                    </li>
                    <li>
                        <h3>Unsecured Transmission of User Content.</h3>
                        <p>You understand that the operation of the Site and Platform. including User Content may be unencrypted and involve transmission to Legends Lair's third party vendors and hosting partners to operate and maintain the Site and Content. Accordingly, you acknowledge that you bear sole responsibility for adequate security protection and backup of User Content. Legends Lair will have no liability to you for any unauthorized access or use of any of User Content. or any corruption, deletion, destruction or loss of any of User Content.</p>
                    </li>
                    <li>
                        <h3>Feedback</h3>
                        <p>You may submit ideas, suggestions. or comments ("Feedback") regarding the Site and Content or Legends Lair's business, products or services. By submitting any Feedback you acknowledge and agree that (a) your Feedback is provided by you voluntarily and Legends Lair may, without any obligations or limitation, use and exploit such Feedback in any manner and for any purpose (b) you will not seek and are not entitled to any money or other form of compensation, consideration, or attribution with respect to your Feedback regardless of whether Legends Lair considered or used your Feedback in any manner, and (c) your Feedback is not the confidential or proprietary information of you or any third party.</p>
                    </li>
                    <li>
                        <h3>Your Representations and Warranties.</h3>
                        <p>You represent and warrant to Legends Lair that your activity on the Site and Legends Lair's possession and use of User Content as contemplated in these T&Cs do not and will not violate. infringe, or misappropriate any third party's copyright, trademark, right of privacy or publicity or other personal or proprietary right nor does User Content contain any matter that is defamatory, obscene, unlawful, threatening, abusive, tortious, offensive or harassing.</p>
                    </li>
                    <li>
                        <h3>Termination of Access Due to Violations.</h3>
                        <p>Legends Lair may, in its sole discretion and without prior notice terminate your access to the Site and/or block your future access to the Site if we determine that you have violated these T&Cs or other agreements or guidelines which may be associated with your use of the Site. You also agree that any violation by you of these T&Cs will cause irreparable harm to Legends Lair for which monetary damages would be inadequate and you consent to Legends Lair obtaining any injunctive or equitable relief that Legends Lair deems necessary or appropriate in such circumstances without limiting Legends Lair's other available remedies. Further. Legends Lair may, in its sole discretion and without prior notice, terminate your access to the Site, for cause, which includes (but is not limited to) (i.) requests by law enforcement or other government agencies, (2) discontinuance or material modification of the Site or any service offered on or through the Site, or (3) unexpected technical issues or problems. </p>
                    </li>
                </ul>
            </div>

            <div class="col-md-12">
                <h1>NO WARRANTIES AND DISCLAIMER BY LEGENDS LAIR.</h1>
                <p>THE SITE AND CONTENT AND ALL SERVER AND NETWORK COMPONENTS, ARE PROVIDED ON AN "AS IS" AND "AS AVAILABLE" BASIS WITHOUT ANY WARRANTIES OF ANY KIND, AND LEGENDS LAIR EXPRESSLY DISCLAIMS ALL OTHER REPRESENTATIONS AND WARRANTIES. INCLUDING ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT, AND ANY REPRESENTATIONS OR WARRANTIES ARISING FROM COURSE OF DEALING COURSE OF PERFORMANCE OR USAGE OF TRADE. YOU ACKNOWLEDGE THAT LEGENDS LAIR DOES NOT WARRANT THAT YOUR ACCESS OR USE OR BOTH OF THE THE SITE AND CONTENT WILL BE UNINTERRUPTED, TIMELY, SECURE, ERROR-FREE OR VIRUS-FREE, AND LEGENDS LAIR DOES NOT MAKE ANY WARRANTY AS TO THE RESULTS THAT MAY BE OBTAINED FROM USE OF THE SITE AND CONTENT. AND NO INFORMATION, ADVICE OR SERVICES OBTAINED BY YOU FROM TOPTAL OR THROUGH THE SITE AND PROPERTY WILL CREATE ANY WARRANTY NOT EXPRESSLY STATED IN THESE T&Cs.</p>
                <p>Legends Lair reserves the right to do any of the following. at any time. without notice: (1) to modify, suspend or terminate operation of or access to the Site. or any portion of the Site, for any reason; (2) to modify or change the Site, or any portion of the Site, for any reason; and (3) to interrupt the operation of the Site, or any portion of the Site, as necessary to perform routine or non-routine maintenance, error correction, or other changes. </p>
            </div>

            <div class="col-md-12">
                <h1>LIMITED LIABILITY.</h1>
                <ul>
                    <li>
                        <h3>Exclusion of Damages and Limitation of Liability.</h3>
                        <p>Legends Lair does not charge fees for you to access and use the Site and Content pursuant to these T&Cs. As consideration for your free access and use of the Site and Content pursuant to these T&Cs, you further agree that LEGENDS LAIR WILL NOT BE LIABLE TO YOU FOR ANY INCIDENTAL, CONSEQUENTIAL, INDIRECT, SPECIAL. PUNITIVE OR EXEMPLARY DAMAGES (INCLUDING DAMAGES FOR LOSS OF BUSINESS, LOSS OF PROFITS OR THE LIKE) ARISING OUT OF OR RELATING TO THIS T&Cs, INCLUDING WITHOUT LIMITATION. YOUR USE OR INABILITY TO USE THE SITE, PLATFORM, MATCHING SERVICES, CONTENT. PROPRIETARY INFORMATION, OR ANY INTERRUPTION OR DISRUPTION OF SUCH USE, EVEN IF LEGENDS LAIR HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES AND REGARDLESS OF THE CAUSE OF ACTION (WHETHER IN CONTRACT, TORT. BREACH OF WARRANTY OR OTHERWISE). THE AGGREGATE LIABILITY OF LEGENDS LAIR WITH REGARD TO THIS T&Cs WILL IN NO EVENT EXCEED USD 100.</p>
                    </li>
                    <li>
                        <h3>Jurisdictional Limitations.</h3>
                        <p>Some states and other jurisdictions do not allow the exclusion of implied warranties or limitation of liability for incidental or consequential damages. which means that some of the above limitations may not apply to you. IN THESE STATES. LEGENDS LAIR'S LIABILITY WILL BE LIMITED TO THE GREATEST EXTENT PERMITTED BY LAW. </p>
                    </li>
                    <li>
                        <h3>Dispute Resolution: Jury Waiver.</h3>
                        <p>THESE T&Cs ARE MADE UNDER. AND WILL BE CONSTRUED AND ENFORCED IN ACCORDANCE WITH, THE LAWS OF DELAWARE APPLICABLE TO AGREEMENTS MADE AND TO BE PERFORMED SOLELY THEREIN, WITHOUT GIVING EFFECT TO PRINCIPLES OF CONFLICTS OF LAW. In any action between or among any of the parties, whether arising out of these T&Cs or otherwise, each of the parties irrevocably and unconditionally (a) consents and submits to the exclusive jurisdiction and venue of the state and federal courts located in Wilmington Delaware: and (b) WAIVES ANY AND ALL RIGHT TO TRIAL BY JURY IN ANY LEGAL PROCEEDING ARISING OUT OF OR RELATED TO THIS AGREEMENT OR ANY TRANSACTIONS CONTEMPLATED HEREBY. </p>
                    </li>
                    <li>
                        <h3>Miscellaneous.</h3>
                        <p>These T&Cs, and any additions, changes, edits and/or modifications made thereto by Legends Lair, together with Legends Lair's Privacy Policy, constitute the entire agreement between the parties with respect to the portions of the Site available without an account ID or password. Access to certain password-restricted portions of the Site is also subject to additional agreement(s). These T&Cs and any additions, changes, edits and/or modifications made thereto by Legends Lair cannot be amended except by Legends Lair as set forth above. The failure of Legends Lair to exercise or enforce any right or provision of these T&Cs will not be a waiver of that right. Any notices to Legends Lair in connection with this agreement will be made by email transmitted to support@xenren.co provided that you also send a copy of such notice via nationally recognized carrier to Flat AT/RM A30. 9/F Sii Vercorp International Tower. 707-713 Nathan Road. Mongkok Kowloon, Hong Kong., Attn: Contract Administration. In the event that any provision of these T&Cs will be determined to be illegal or unenforceable, that provision will be first revised to give the maximum permissible effect to its original intent or, if such revision is not permitted, that specific provision will be eliminated so that these T&Cs will otherwise remain in full force and effect and enforceable. </p>
                    </li>
                </ul>
            </div>

        </div>
    </div>
    
</div>    
@endsection

