@extends('frontend.companyInfo.layouts.default')

@section('title')
    {{ trans('common.privacy_title') }}
@endsection

@section('keywords')
    {{ trans('common.privacy_keyword') }}
@endsection

@section('description')
    {{ trans('common.privacy_desc') }}
@endsection

@section('author') 
    Xenren
@endsection

@section('header')
    <link href="/custom/css/frontend/home-new-custom.css" rel="stylesheet" type="text/css" />

    <!-- END HEADER AND FOOTER PAGE STYLES -->
    <link href="/css/company-info.css" rel="stylesheet" type="text/css" />
    <link href="/css/privacy-policy.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')


<div class="page-container page-container-main">
    @include('frontend.companyInfo.layouts.menu')
    
    <div class="row banner">
        <div class="col-md-12 banner-inner">
            <div class="banner-inner-text">
                <p>{{ trans('home.privacy_banner_p') }}</p>
                <h1>{{ trans('home.privacy_banner_h1') }}</h1>
                <span>{{ trans('home.privacy_banner_span') }}: {{ trans('home.privacy_banner_spanDate') }}</span>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row body-content">
            <div class="col-md-12">
                <h1>Our Policy:</h1>
                <p>Welcome to the Legends Lair website (the "Site"). Legends Lair LLC ("Legends Lair", °we", "us", and/or "our") operates this Site to provide our users ("you° and 'your) with information about Legends Lair and its platform, which is designed to find and connect professional freelancers with Legends Lair's clients who need their services (the "Services"). This Privacy Policy sets out how Legends Lair collects. retains, and uses information, including personally identifiable data ("Personal Data"), from you and other Site visitors, as well as users of the Services.</p>
            </div>

            <div class="col-md-12">
                <h1>Information We Collect:</h1>
                <p>When you interact with us through the Site or the Services. we may collect Personal Data and other information from you, as further described below:</p>

                <ul>
                    <li>
                        <h3>Personal Data That You Provide Through the Site:</h3>
                        <p>
                            We collect Personal Data from you when you voluntarily provide such information such as when you register for access to the Services (for example, your name and mailing address), use certain Services, contact us with inquiries or respond to one of our surveys. Wherever Legends Lair collects Personal Data we make an effort to provide a link to this Privacy Policy.
                            <br>    By voluntarily providing us with Personal Data, you are consenting to our use of it in accordance with this Privacy Policy. If you provide Personal Data, you acknowledge and agree that such Personal Data may be transferred from your current location to the offices and servers of Legends Lair and the authorized third parties referred to herein located in the United States.
                        </p>
                    </li>
                    <li>
                        <h3>Non-Identifiable or Aggregated Data:</h3>
                        <p>
                            When you interact with Legends Lair through the Site or Services, we receive and store certain personally non-identifiable information. Such information, which is collected passively using various technologies. cannot presently be used to specifically identify you. Legends Lair may store such information itself or such information may be included in databases owned and maintained by Legends Lair affiliates, agents or service providers. This Site may use such information and pool it with other information to track. for example, the total number of visitors to our Site, the number of visitors to each page of our Site, the domain names of our visitors' Internet service providers. and how our users use and interact with the Services. Also, in an ongoing effort to better understand and serve the users of the Services, Legends Lair often conducts research on its customer demographics, interests and behavior based on the Personal Data and other information provided to us. This research may be compiled and analyzed on an aggregate basis. Legends Lair may share this non-identifiable and aggregate data with its affiliates, agents and business partners, but this type of non-identifiable and aggregate information does not identify you personally. Legends Lair may also disclose aggregated user statistics in order to describe our Services to current and prospective business partners, and to other third parties for other lawful purposes. 
                            <br> In operating this Site, we may also use a technology called "cookies' A cookie is a piece of information that the computer that hosts our Site gives to your browser when you access the Site. Our cookies help provide additional functionality to the Site and help us analyze Site usage more accurately. For instance, our Site may set a cookie on your browser that allows you to access the Site without needing to remember and then enter a password more than once during a visit to the Site. In all cases in which we use cookies. we will not collect Personal Data except with your permission. On most web browsers, you will find a "help" section on the toolbar. Please refer to this section for information on how to receive notification when you are receiving a new cookie and how to turn cookies off. We recommend that you leave cookies turned on because they allow you to take advantage of some of the Site's features.
                        </p>
                    </li>
                </ul>
            </div>

            <div class="col-md-12">
                <h1>Our Disclosure of Your Personal Data and Other Information:</h1>
                <p>Legends Lair is not in the business of selling your information. We consider this information to be a vital part of our relationship with you. There are, "However," certain circumstances in which we may share your Personal Data with certain third parties without further notice to you, as set forth below:</p>

                <ul>
                    <li>
                        <h3>Business Transfers:</h3>
                        <p>As we develop our business, we might sell or buy businesses or assets. In the event of a corporate sale, merger, reorganization, dissolution or similar event, Personal Data may be part of the transferred assets.</p>
                    </li>
                    <li>
                        <h3>Related Companies:</h3>
                        <p>We may also share your Personal Data with our Related Companies for purposes consistent with this Privacy Policy.</p>
                    </li>
                    <li>
                        <h3>Agents, Consultants and Related Third Parties:</h3>
                        <p>Legends Lair, like many businesses, sometimes hires other companies to perform certain business-related functions. Examples of such functions include mailing information. maintaining databases, billing and processing payments. When we employ another company to perform a function of this nature, we only provide them with the information that they need to perform their specific function.</p>
                    </li>
                    <li>
                        <h3>Legal Requirements:</h3>
                        <p>Legends Lair may disclose your Personal Data if required to do so by law or in the good faith belief that such action is necessary to (i) comply with a legal obligation, (ii) protect and defend the rights or property of Legends Lair, (ill act in urgent circumstances to protect the personal safety of users of the Site or the public, or (iv) protect against legal liability.</p>
                    </li>
                </ul>
            </div>  

            <div class="col-md-12">
                <h1>Your Choices:</h1>
                <p>You can use the Site without providing any Personal Data. If you choose not to provide any Personal Data, you may not be able to use certain Services.</p>      
            </div>
            
            <div class="col-md-12">
                <h1>Exclusions:</h1>
                <p>This Privacy Policy does not apply to any Personal Data collected by Legends Lair other than Personal Data collected through the Site or Services. This Privacy Policy shall not apply to any unsolicited information you provide to Legends Lair through this Site or through any other means. This includes, but is not limited to, information posted to any public areas of the Site, such as forums (collectively, 'Public Areas"), any ideas for new products or modifications to existing products, and other unsolicited submissions (collectively. "Unsolicited Information"). All Unsolicited Information shall be deemed to be non-confidential and Legends Lair shall be free to reproduce, use, disclose. distribute and exploit such Unsolicited Information without limitation or attribution. </p>      
            </div>

            <div class="col-md-12">
                <h1>Children:</h1>
                <p>Legends Lair does not knowingly collect Personal Data from children under the age of 13. If you are under the age of 13. please do not submit any Personal Data through the Site. We encourage parents and legal guardians to monitor their children's Internet usage and to help enforce our Privacy Policy by instructing their children never to provide Personal Data on this Site without their permission. If you have reason to believe that a child under the age of 13 has provided Personal Data to Legends Lair through this Site. please contact us, and we will endeavor to delete that information from our databases. </p>      
            </div>

            <div class="col-md-12">
                <h1>Links to Other Web Sites:</h1>
                <p>This Privacy Policy applies only to the Site. This Site may contain links to other web sites not operated or controlled by Legends Lair (the "Third Party Sites"). The policies and procedures we described here do not apply to the Third Party Sites. The links from this Site do not imply that Legends Lair endorses or has reviewed the Third Party Sites. We suggest contacting those sites directly for information on their privacy policies. </p>      
            </div>

            <div class="col-md-12">
                <h1>Security:</h1>
                <p>Legends Lair takes reasonable steps to protect the Personal Data provided via the Site from loss, misuse, unauthorized access, disclosure, alteration, or destruction. However, no Internet, email or other electronic transmission is ever fully secure or error free, so you should take special care in deciding what information you send to us in this way. </p>      
            </div>
            
            <div class="col-md-12">
                <h1>Other Terms and Conditions:</h1>
                <p>Your access to and use of this Site is subject to our Website Terms & Conditions. </p>      
            </div>

            <div class="col-md-12">
                <h1>Changes to Toptal's Privacy Policy:</h1>
                <p>The Site and our business may change from time to time. As a result, at times it may be necessary for Legends Lair to make changes to this Privacy Policy. Legends Lair reserves the right to update or modify this Privacy Policy at any time and from time to time without prior notice. Please review this policy periodically, and especially before you provide any Personal Data. This Privacy Policy was last updated on the date indicated above. Your continued use of the Site after any changes or revisions to this Privacy Policy shall indicate your agreement with the terms of such revised Privacy Policy. </p>      
            </div>

            <div class="col-md-12">
                <h1>Access to Information; Contacting Toptal:</h1>
                <p>To keep your Personal Data accurate, current, and complete, please contact us as specified below. We will take reasonable steps to update or correct Personal Data in our possession that you have previously submitted via this Site or Services. </p>      
                <p>Please also feel free to contact us if you have any questions about Legends Lair's Privacy Policy or the information practices of this Site. </p>
                <p>You may contact us as follows: support@xenren.co .</p>
            </div>    

        </div>
    </div>
    
</div>    
@endsection

