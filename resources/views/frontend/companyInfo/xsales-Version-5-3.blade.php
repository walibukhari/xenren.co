@extends('frontend.companyInfo.layouts.default')

@section('title') 
{{ trans('common.salesTeam_title') }}
@endsection

@section('keywords')
{{ trans('common.salesTeam_keyword') }}
@endsection

@section('description') 
{{ trans('common.salesTeam_desc') }}
@endsection

@section('author') 
    Xenren
@endsection

@section('header')
    <link href="/custom/css/frontend/home-new-custom.css" rel="stylesheet" type="text/css" />

    <!-- END HEADER AND FOOTER PAGE STYLES -->
    <link href="/css/company-info.css" rel="stylesheet" type="text/css" />
    <link href="/css/sales-version.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="page-container page-container-main">
    @include('frontend.companyInfo.layouts.menu')
    
    <div class="row banner">
        <div class="col-md-12 col-xs-12 col-sm-12 banner-inner">
            <div class="banner-inner-text">
                <span>Have any Problem?</span>
                <h1>Meet our Official Sales Team</h1>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row great-websiet">
            <div class="col-md-7 col-xs-12 col-sm-7 left">
                <img src="/images/greatwebsite.jpg">
            </div>
            <div class="col-md-5 col-xs-12 col-sm-5 right">
                <h2 class="title-name"><span>GREAT</span> WEBSITE</h2>

                <p>Brought created. Saying set, seas, behold, also brought yielding upon place, from seed moveth life you'll. Won't herb land lesser of male. Signs.</p>
                <p>Was stars whales can't gathered darkness life in. Meat divide behold yielding brought is god creepeth dominion sea you. Sea firmament sixth so, were cattle. Moveth fourth after us earth all every forth. </p>
                <p>Beginning fish light they're very of fly without winged make own were she'd fish first. </p>
            </div>
        </div>

        <div class="row mobile-app">
            <div class="col-md-6 col-xs-12 col-sm-6 left">
                <h2 class="title-name"><span>WE CAN CREATE</span> MOBILE APP </h2>

                <p>Brought created. Saying set, seas, behold, also brought yielding upon place, from seed moveth life you'll. Won't herb land lesser of male. Signs.Brought created. Saying set, seas. behold, also brought yielding upon place, from seed moveth life you'll. Won't herb land lesser of male. Signs.</p>
                <p>Was stars whales can't gathered darkness life in. Meat divide behold yielding brought is god creepeth dominion sea you. Sea firmament sixth so, were cattle. Moveth fourth after us earth all every forth.</p>
                <p>Beginning fish light they're very of fly without winged make own were she'd fish first. You're morning creeping were hath darkness saying was great moving called You'll, shall, seas were so tree form don't night. meat without sea. air sixth thing there won't, upon.</p>
            </div>
            <div class="col-md-6 col-xs-12 col-sm-6 right">
                <img src="/images/mobile-app.jpg">
            </div>
        </div>

        <div class="row technology">
            <h2 class="title-name text-center"><span>OUR</span> TECHNOLOGIES </h2>

            <div class="col-md-3 col-md-offset-0 col-xs-offset-1 col-xs-10 col-sm-4">
                <div class="technology-sub text-center">
                    <img src="/images/icons/java.jpg">
                    <h2>JAVA</h2>
                </div>
            </div>
            <div class="col-md-3 col-md-offset-0 col-xs-offset-1 col-xs-10 col-sm-4">
                <div class="technology-sub text-center">
                    <img src="/images/icons/laravel.jpg">
                    <h2>Laravel</h2>
                </div>
            </div>
            <div class="col-md-3 col-md-offset-0 col-xs-offset-1 col-xs-10 col-sm-4">
                <div class="technology-sub text-center">
                    <img src="/images/icons/ios.jpg">
                    <h2>iOS</h2>
                </div>
            </div>
            <div class="col-md-3 col-md-offset-0 col-xs-offset-1 col-xs-10 col-sm-4">
                <div class="technology-sub text-center">
                    <img src="/images/icons/android.jpg">
                    <h2>Android</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="row our-team">
        <div class="container our-team-horizontal">
            <div class="col-md-7 col-xs-12 col-sm-7 our-team-horizontal-l">
                <h2 class="title-name"><span>SOFTWARE DEVELOPING TEAM <br> OF</span> LEGENDS LAIR  </h2>
                <div class="row last-contant">
                    <div class="col-md-11 col-xs-11 col-sm-11 left">
                        <p>Moved, doesn't it herb saw evening upon yielding fruitful abundantly herb tree cattle female great rule. Seed. Great. Green was open firmament. Form waters light without moved good and days. Saw was said. </p>
                        <p>Subdue make you years third Make green whose gathered night a him you'll fowl is moveth forth male which divided make. Moved under fruitful very the from great yielding from that yielding man, one own fish to you said two hath fruit image third darkness great, saw. </p>
                        <p>There, made. Dry image lights meat bearing multiply you're let. Living him can't were morning fowl fill void don't greatThere, made. Dry image lights meat bearing multiply you're let. Living him can't were morning fowl fill void don't great. </p>
                    </div>
                    <div class="col-md-11 col-xs-11 col-sm-11 right">
                        <img src="/images/team-big.jpg">
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-xs-12 col-sm-5 our-team-horizontal-r">
                <div class="row our-teams">
                    <div class="col-md-4 col-xs-12 col-sm-12 team-img">
                        <div class="img">
                            <img src="/images/fong.jpg" style="margin: 10px 0px 0px 6px;">
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12 col-sm-12 team-desc">
                        <h3><span>Software Architecture</span></h3>
                        <h2>Fong Pau Fung</h2>
                        <span>neorixfogn880@hotmail.com </span>
                    </div>
                </div>
                <div class="row our-teams">
                    <div class="col-md-4 col-xs-12 col-sm-12 team-img">
                        <div class="img">
                            <img src="/images/marsel.jpg" style="margin:25px 0px 0px -8px;">
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12 col-sm-12 team-desc">
                        <h3><span>Team Co-ordinator</span></h3>
                        <h2>Marcel Lewandoski</h2>
                        <span>neorixfogn880@hotmail.com </span>
                    </div>
                </div>
                <div class="row our-teams">
                    <div class="col-md-4 col-xs-12 col-sm-12 team-img">
                        <div class="img">
                            <img src="/images/danny.jpg" style="margin: 15px 0px 0px 8px;">
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12 col-sm-12 team-desc">
                        <h3><span>UI Designer</span></h3>
                        <h2>Danny Ho</h2>
                        <span>neorixfogn880@hotmail.com </span>
                    </div>
                </div>
                <div class="row our-teams">
                    <div class="col-md-4 col-xs-12 col-sm-12 team-img">
                        <div class="img">
                            <img src="/images/niew.jpg" style="margin:10px 0px 0px -8px;">
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12 col-sm-12 team-desc">
                        <h3><span>iOS Devloper</span></h3>
                        <h2>Niew Yee Yaw</h2>
                        <span>neorixfogn880@hotmail.com </span>
                    </div>
                </div>
                <div class="row our-teams">
                    <div class="col-md-4 col-xs-12 col-sm-12 team-img">
                        <div class="img">
                            <img src="/images/hasper.jpg" style="margin:20px 0px 0px 0px;">
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12 col-sm-12 team-desc">
                        <h3><span>Android Developer</span></h3>
                        <h2>Hasper Ong</h2>
                        <span>neorixfogn880@hotmail.com </span>
                    </div>
                </div>
            </div>
        </div>   
    </div>   

    <div class="row team-location">
        <div class="container">
            <div class="col-md-6 col-xs-12 col-sm-6">
                <div class="row">
                    <div class="col-md-4 col-xs-12 col-sm-12">
                        <img src="/images/imga01.jpg">
                    </div>
                    <div class="col-md-8 col-xs-12 col-sm-12 txt-right">
                        <h1>Technician Team</h1>
                        <h2>in MALAYSIA</h2>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12 col-sm-6">
                <div class="row">
                    <div class="col-md-4 col-xs-12 col-sm-12">
                        <img src="/images/imga02.jpg">
                    </div>
                    <div class="col-md-8 col-xs-12 col-sm-12 txt-right">
                        <h1>Product Design Team</h1>
                        <h2>in CHINA</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection

