@extends('frontend.companyInfo.layouts.default')

@section('title') 
    {!! trans('common.company_culture_title') !!}
@endsection

@section('keywords')
    {!! trans('common.company_culture_keyword') !!}
@endsection

@section('description') 
    {!! trans('common.company_culture_desc') !!}
@endsection

@section('author') 
    Xenren
@endsection
@section('style')
@endsection

@section('header')
    <link href="/custom/css/frontend/home-new-custom.css" rel="stylesheet" type="text/css" />

    <!-- END HEADER AND FOOTER PAGE STYLES -->
    <link href="/css/company-info.css" rel="stylesheet" type="text/css" />
    <link href="/css/company-culture.css" rel="stylesheet" type="text/css" />
    <link href="/css/chineseCompanyC.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<div class="page-container page-container-main">
    @include('frontend.companyInfo.layouts.menu')
    
    <div class="row banner">
        <div class="col-md-12 col-xs-12 col-sm-12 banner-inner">
            <div class="banner-inner-text">
                <span>我们的使命是什么？</span>
                <h1>神人企业文化</h1>
            </div>
        </div>
    </div>

    <div class="container" id="intro">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12 intro">
                <img src="/images/Company-Info/culture-intro-sm.png">

                <h2 class="title-name">缘起 </h2>
                
                <p>神人英文名是Legends Lair，比传奇多一个S（复数）Lair意为摇篮。寄意平台成为传奇的摇篮，让执意改变世界的人们在这里作为起点。</p>
                
                <p> <span class="black">2014年神人刚注册公司，</span> 由于不熟悉互联网行业，便启程到新加坡、香港、台湾、中国、越南、甚至印度等，多个国家接洽业务，同时寻找技术人才。</p>
                
                <p><span class="green">当我出差到印度邦加罗尔（印度硅谷），和当地友人一起路过Dhamapuri，遇见一位小女孩正无助地看着她奄奄一息躺在地上的父亲，那样的场景让我不忍和痛心。</span></p>
                
                <p>实在想为他们做些什么，却只能将身上仅有的2000卢比全掏给了她。我的印度朋友却拦住我说我疯了，不要这么做。</p>
                
                <p><span class="green">”You can’t save all people like him, there’s too many of them.” (你并没有办法拯救所有处在困境中的人，这世上实在太多了。)</span></p>
                
                <p>那一刻，我真切地感受到个人力量的渺小和无力。</p>
                
                <p>我一直知道世界仍有许多陷于困境中需要帮助的人。但新闻网页和报纸中的画面，始终不比亲身所见来的震撼。这是个繁荣富庶平和的年代，但世界依然有许多悲伤的事情正在发生——战乱，</p>
                
                <p>饥荒，灾害，难民，以及老人们老无所养，孩童们生无所依。反观上流社会一天的消费，却可能是普通人 一年的开支，甚至是几年的开支。</p>

                <p>另一方面，世上也有很多默默奉献的人，甚至有人牺牲自己去帮助和拯救他人。我们团队向这些人献上最崇高的敬意，同时也学着用商业模式和更系统的方法，将慈善事业永续经营。</p>
            </div>
        </div>
    </div>

    <div class="row" id="talent">
        <div class="col-md-12 col-xs-12 col-sm-12 talent">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12 text-center">
                        <h2 class="title-name"><span>集英天下，创新世界</span></h2>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <p><span class="black">神人是一个平台，</span> 召唤那些有才能的人，怀有使命感的人，聚集起来去做同一件事。它更是一种信念： <span class="green">神创世界若不尽完善，那就由人来改变。</span></p>
                        <p>还记得2014年，在北京中关村、吉隆坡、台湾都掀起了一阵创业热潮，当时大街上走的人们几乎都拿着CEO的名片。然而短短2年多的时间过去，这些CEO的项目大都早已不复存在。</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-xs-12 col-sm-6 talent-banners">
                    <img src="/images/Company-Info/Culture-sm.png">
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-xs-12 col-sm-offset-6 col-sm-6 pull-right">
                        <p><span class="black">我们公司成立3年，</span> 刚开始的时候几乎一无所有。甚至连技术积累都没有，仅仅是凭着一个让世界更好一点的信念，舍弃过往所有的知识，来到了互联网。</p>
                        <p>和所有的新创公司一样，我们也经历了许多的磨难和挑战， <span class="green"> 而我们熬了过来。 究其原因，我想是使命的力量，让我们在这条路上坚持到了今天，并且走得比别人更远。  </span></p>
                        
                        <hr class="sm-line setCCCPHR">

                        <h2 class="title-name text-center"><span>企业文化</span> </h2>

                        <p><span class="black">能力就是权力</span>， 贡献即所得。你的能力决定了，你能获得多少的决策权。你的贡献，也决定了你可以获得多少。</p>
                    </div>
                </div>
                <div class="row technology setCCCPAGET">
                    <div class="col-md-offset-0 col-md-4 col-xs-offset-1 col-xs-10 col-sm-4">
                        <div class="technology-sub text-center">
                            <img src="/images/icons/Team.png">
                            <div class="text">
                                <p><strong class="setCCPPAGESC">一</strong>, 公司没有上下之分，只有岗位之别。我从不觉得自己是领导者，而仅仅是公司一个负责决策的部门。之所以在这个岗位上，也只是因为迄今为止，我让企业发展的决定还算正确。</p>
                                <p>企业里程序员也是决策部门，每一行敲打的代码，都是权利，都是团队因其能力而给予信任的表现。</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-offset-0 col-md-4 col-xs-offset-1 col-xs-10 col-sm-4">
                        <div class="technology-sub text-center">
                            <img src="/images/icons/Share.png">
                            <div class="text">
                                <p><strong class="setCCPPAGESC">二</strong>, 你是就企业：公平、透明。这些都体现在我们股份制度上。从基层、创始人、投资人，每个人的股份分配都是同样的机制。全职，不要求固定薪资者，股份比率高30%。要求固定薪资者，可申请将部分薪资用于购买股份。投资获得的股份，一股一元。  </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-offset-0 col-md-4 col-xs-offset-1 col-xs-10 col-sm-4">
                        <div class="technology-sub text-center">
                            <img src="/images/icons/Target.png">
                            <div class="text">
                                <p><strong class="setCCPPAGESC">三</strong>, 成立企业公司是为了让造福人群的模式，永远的经营传承下去。企业的福利，我们从不保留给少数人，而是开放给团队的每一个人，以及回馈社会。</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container" id="founder">
        <div class="row founder">
            <div class="col-md-4 col-xs-12 col-sm-4 technology">
                <div class="technology-sub">
                     <p>公司成立初期，我最常被问的一个问题就是：</p>
                     <h2 class=" text-center">“这个项目的前景、盈利、市场有多大？“</h2>
                </div>
            </div>
            <div class="col-md-8 col-xs-12 col-sm-8 right-text">
                <h2 class="title-name"><span>创始人一番话：</span></h2>

                <p class="text-18">  我和合伙人说的都是同一番话：“我并不知道我们未来会赚多少钱，到底会是怎样的结果。但我可以承诺的是，如果我有饭吃，我的股东就一定有酒有肉。如果我有车子，你们就一定会有房子。我一定会让我的合伙人比我更富有。“作为使命的执行者，我在乎的是将事情完成。</p>
                <p class="text-18">在我们的蓝图中，神人是一家即使时间匆匆再过5000年，也依然有人说得出这家企业的名字，并且记得那些曾经改变世界的人们。这样的一家企业，将非金钱所能衡量</p>
            </div>
        </div>
    </div>    

    {{--<div class="row" id="founder-footer">--}}
        {{--<div class="col-md-12 col-xs-12 col-sm-12 founder-footer">--}}
            {{--<div class="container">--}}
                {{--<div class="col-md-9 col-xs-12 col-sm-7">--}}
                    {{--<img src="/images/Company-Info/Inverted-Comma.png">--}}
                    {{--<h1 class="fix-text"> 神创世界若未尽完善，就由人来改变。</h1>--}}
                {{--</div>--}}
                {{--<div class="col-md-8 col-xs-12 col-sm-6 text-right">--}}
                    {{--<h1>- 房保宏</h1>--}}
                    {{--<span>创始人 </span>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>    --}}
    
</div>               
@endsection

