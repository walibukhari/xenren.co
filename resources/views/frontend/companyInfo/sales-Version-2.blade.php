@extends('frontend.companyInfo.layouts.default')

@section('title') 
{{ trans('common.salesTeam_title') }}
@endsection

@section('keywords')
{{ trans('common.salesTeam_keyword') }}
@endsection

@section('description') 
{{ trans('common.salesTeam_desc') }}
@endsection

@section('author')
    Xenren
@endsection

@section('header')
    <link href="/custom/css/frontend/home-new-custom.css" rel="stylesheet" type="text/css"/>

    <!-- END HEADER AND FOOTER PAGE STYLES -->
    <link href="/css/company-info.css" rel="stylesheet" type="text/css"/>
    <link href="/css/sales-version.css" rel="stylesheet" type="text/css"/>
@endsection

@section('content')

    <style>

        @media (max-width:767px)
        {
            .page-container-main {
                text-align: -webkit-center;
            }
        }

        @media (max-width:420px)
        {
            .page-container-main {
                margin-top: 81px;
            }
        }
        .setHOTLINE {
            margin-right: 64px !important;
        }
    </style>

    <div class="page-container page-container-main">
        @include('frontend.companyInfo.layouts.menu')

        <div class="row banner">
            <div class="col-md-12 col-xs-12 col-sm-12 banner-inner">
                <div class="banner-inner-text">
                    <span>{!! trans('salesteam.banner_span') !!}</span>
                    <h1>{!! trans('salesteam.banner_h1') !!}</h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row great-websiet setROWMEET">
                <div class="col-md-7  col-xs-12 col-sm-7 left">
                    <img src="/images/greatwebsite.jpg">
                </div>
                <div class="col-md-5 col-xs-12 col-sm-5 right">
                    <h2 class="title-name">
                        {!! trans('salesteam.title1') !!}  
                    </h2>
                    {!! trans('salesteam.desc1') !!}  

                </div>
            </div>

            <div class="row mobile-app setROWMEET">
                <div class="col-md-6 col-xs-12 col-sm-6 left">
                    <h2 class="title-name">
                        {!! trans('salesteam.title2') !!}  
                    </h2>
                    {!! trans('salesteam.desc2') !!}                      
                </div>
                <div class="col-md-6 col-xs-12 col-sm-6 right">
                    <img src="/images/mobile-app.jpg">
                </div>
            </div>

            <div class="row technology setROWMEET">
                <h2 class="title-name text-center">
                    {!! trans('salesteam.title3') !!}                      
                </h2>

                <div class="col-md-3 col-md-offset-0 col-xs-offset-1 col-xs-10 col-sm-4">
                    <div class="technology-sub text-center">
                        <div class="img">
                            <img src="/images/icons/Java-10.png">
                        </div>
                        <h2>{!! trans('salesteam.java') !!}</h2>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-0 col-xs-offset-1 col-xs-10 col-sm-4">
                    <div class="technology-sub text-center">
                        <div class="img">
                            <img src="/images/icons/php-10.png">
                        </div>
                        <h2>{!! trans('salesteam.php') !!}</h2>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-0 col-xs-offset-1 col-xs-10 col-sm-4">
                    <div class="technology-sub text-center">
                        <div class="img">
                            <img src="/images/icons/Apple-Icon-01.png">
                        </div>
                        <h2>{!! trans('salesteam.ios') !!}</h2>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-0 col-xs-offset-1 col-xs-10 col-sm-4">
                    <div class="technology-sub text-center">
                        <div class="img">
                            <img src="/images/icons/Android-01.png">
                        </div>
                        <h2>{!! trans('salesteam.android') !!}</h2>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-0 col-xs-offset-1 col-xs-10 col-sm-4">
                    <div class="technology-sub text-center">
                        <div class="img">
                            <img src="/images/icons/SQL-10.png">
                        </div>
                        <h2>{!! trans('salesteam.my_sql') !!}</h2>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-0 col-xs-offset-1 col-xs-10 col-sm-4">
                    <div class="technology-sub text-center">
                        <div class="img">
                            <img src="/images/icons/Hadoop-10.png">
                        </div>
                        <h2>{!! trans('salesteam.hadoop') !!}</h2>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-0 col-xs-offset-1 col-xs-10 col-sm-4">
                    <div class="technology-sub text-center">
                        <div class="img">
                            <img src="/images/icons/GitHub-10.png">
                        </div>
                        <h2>{!! trans('salesteam.github') !!}</h2>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-0 col-xs-offset-1 col-xs-10 col-sm-4">
                    <div class="technology-sub text-center">
                        <div class="img">
                            <img src="/images/icons/HTML-10.png">
                        </div>
                        <h2>{!! trans('salesteam.html_css_js') !!}</h2>
                    </div>
                </div>

            </div>
        </div>

        <div class="row our-team">
            <div class="container our-team-horizontal setMSTCOTH">
                <div class="col-md-12 col-xs-12 col-sm-12 our-team-horizontal-2">
                    <h2 class="title-name">
                        {!! trans('salesteam.title4') !!}
                    </h2>
                </div>
                <div class="col-md-12 col-xs-12 col-sm-12 our-teams-2">
                    <div class="row our-teams">
                        <div class="col-md-2 col-xs-12 col-sm-4 team-img">
                            <div class="img">
                                <img src="/images/Fong.png" class="setSV2IMAGE">
                            </div>
                        </div>
                        <div class="col-md-10 col-xs-12 col-sm-8 team-desc-2">
                            <h3><span>{!! trans('salesteam.team1_position') !!}</span></h3>
                            <h2>{!! trans('salesteam.team1_name') !!}</h2>
                            <p class="detail"><i class="fa fa-play" aria-hidden="true"></i> {!! trans('salesteam.team1_email') !!}
                            </p>
                            <p class="detail"><i class="fa fa-play" aria-hidden="true"></i> {!! trans('salesteam.team1_number') !!} </p>
                            <p class="detail"><i class="fa fa-play" aria-hidden="true"></i> {!! trans('salesteam.team1_add') !!}</p>
                        </div>
                    </div>
                    <div class="row our-teams">
                        <div class="col-md-2 col-xs-12 col-sm-4 team-img">
                            <div class="img">
                                <img src="/images/lina.jpg" class="setSV2IMAGE">
                            </div>
                        </div>
                        <div class="col-md-10 col-xs-12 col-sm-8 team-desc-2">
                            <h3><span>{!! trans('salesteam.team2_position') !!}</span></h3>
                            <h2>{!! trans('salesteam.team2_name') !!}</h2>
                            <p class="detail"><i class="fa fa-play" aria-hidden="true"></i> {!! trans('salesteam.team2_email') !!}
                            </p>
                            <p class="detail"><i class="fa fa-play" aria-hidden="true"></i> {!! trans('salesteam.team2_number') !!} </p>
                            <p class="detail"><i class="fa fa-play" aria-hidden="true"></i> {!! trans('salesteam.team2_add') !!}</p>
                        </div>
                    </div>
                    <div class="row our-teams">
                        <div class="col-md-2 col-xs-12 col-sm-4 team-img">
                            <div class="img">
                                <img src="/images/Hee.png" class="setSV2IMG2">
                            </div>
                        </div>
                        <div class="col-md-10 col-xs-12 col-sm-8 team-desc-2">
                            <h3><span>{!! trans('salesteam.team3_position') !!}</span></h3>
                            <h2>{!! trans('salesteam.team3_name') !!}</h2>
                            <p class="detail"><i class="fa fa-play" aria-hidden="true"></i> {!! trans('salesteam.team3_email') !!}
                            </p>
                            <p class="detail"><i class="fa fa-play" aria-hidden="true"></i> {!! trans('salesteam.team3_number') !!} </p>
                            <p class="detail"><i class="fa fa-play" aria-hidden="true"></i> {!! trans('salesteam.team3_add') !!}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12  col-xs-12 col-sm-12 our-teams-contant">
                    <img src="/images/Company-Info/contact-2.png">

                    <h3><span>{!! trans('salesteam.point1_title') !!}</span></h3>
                    {!! trans('salesteam.point1_desc') !!}

                    <h3><span>{!! trans('salesteam.point2_title') !!}</span></h3>
                    {!! trans('salesteam.point2_desc') !!}

                    <h3><span>{!! trans('salesteam.point3_title') !!}</span></h3>
                    {!! trans('salesteam.point3_desc') !!}

                    <h3><span>{!! trans('salesteam.point4_title') !!}</span></h3>
                    {!! trans('salesteam.point4_desc') !!}
                    
                </div>
            </div>
        </div>

        <div class="row team-contact">
            <div class="col-md-6 col-xs-12 col-sm-6 left">
            </div>
            <div class="col-md-4 col-xs-12 col-sm-6 right text-right">
                <h2 class="title-name text-center">
                    {!! trans('salesteam.contactus') !!}
                </h2>
                <h1 class="text-center">
                    {!! trans('salesteam.contactus_n') !!}
                </h1>
                <span class="text-right setHOTLINE">{!! trans('salesteam.hotline') !!}</span>
            </div>
        </div>
    </div>
@endsection

