@extends('frontend.companyInfo.layouts.default')

@section('title') 
    {!! trans('common.aboutus_title') !!}
@endsection

@section('keywords') 
    {!! trans('common.aboutus_keyword') !!}
@endsection

@section('description') 
    {!! trans('common.aboutus_desc') !!}
@endsection

@section('author') 
    Xenren
@endsection

@section('header')

    <link href="{{asset('custom/css/frontend/home-new-custom.css')}}" rel="stylesheet" type="text/css" />

    <!-- END HEADER AND FOOTER PAGE STYLES -->
    <link href="{{asset('css/company-info.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/sales-version.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{asset('css/abut.css')}}" rel="stylesheet" type="text/css" >
@endsection

@section('style')
@endsection

@section('content')

<div class="page-container page-container-main">
    @include('frontend.companyInfo.layouts.menu')
    
    <div class="row banner">
        <div class="col-md-12 col-xs-12 col-sm-12 banner-inner">
            <div class="banner-inner-text">
                <h1>{!! trans('aboutus.banner') !!}</h1>
            </div>
        </div>
    </div>

    <div class="container container-p-15 bulb">
	    <div class="row">
            <div class="col-md-6 col-xs-12 col-sm-6 text-center">
				<img src="/images/Company-Info/bulb.jpg" width="80%">    		
	    	</div>
            <div class="col-md-6 col-xs-12 col-sm-6 setTHISCOL text-center">
	    		<h2 class="title-name">{!! trans('aboutus.f_title') !!}</h2>
	    		{!! trans('aboutus.f_desc') !!}
	    	</div>
	    </div>
    </div>

    <div class="container container-p-15 vision-mission">
	    <div class="row">
            <div class="col-md-6 col-xs-12 col-sm-6 text-center">
				<img src="/images/Company-Info/mission.jpg" width="135px">    		
	    		<h2 class="title-name">{!! trans('aboutus.mission') !!} </h2>
	    		<p>{!! trans('aboutus.mission_desc') !!}</p>
	    	</div>
            <div class="col-md-6 col-xs-12 col-sm-6 text-center">
				<img src="/images/Company-Info/vision.jpg" width="135px">    		
	    		<h2 class="title-name">{!! trans('aboutus.vision') !!}</h2>
	    		<p>{!! trans('aboutus.vision_desc') !!}</p>
	    	</div>
	    </div>	
	</div>

	<div class="row banner-middal">
        <div class="col-md-12 col-xs-12 col-sm-12">
			<img src="/images/Company-Info/untitled-banner.jpg">
		</div>
    </div>

    <div class="container container-p-15 things">
	    <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
	    		<h2 class="title-name">{!! trans('aboutus.5things') !!}</h2>
	    	</div>
	    </div>
	    <div class="row">
            <div class="col-md-7 col-xs-12 col-sm-7">
            	{!! trans('aboutus.description') !!}
	    		
				<img src="/images/Company-Info/things5.jpg">
	    	</div>
            <div class="col-md-5 col-xs-12 col-sm-5">
	    		<div class="points">
	    			<div class="left"><p>1</p></div>
	    			<div class="right">
	    				<h3>{!! trans('aboutus.5things_title_1') !!}</h3>
	    				<p>{!! trans('aboutus.5things_1') !!}</p>
	    			</div>
	    		</div>
	    		<div class="points">
	    			<div class="left"><p>2</p></div>
	    			<div class="right">
	    				<h3>{!! trans('aboutus.5things_title_2') !!}</h3>
	    				<p>{!! trans('aboutus.5things_2') !!}</p>
	    			</div>
	    		</div>
	    		<div class="points">
	    			<div class="left"><p>3</p></div>
	    			<div class="right">
	    				<h3>{!! trans('aboutus.5things_title_3') !!}</h3>
	    				<p>{!! trans('aboutus.5things_3') !!}</p>
	    			</div>
	    		</div>
	    		<div class="points">
	    			<div class="left"><p>4</p></div>
	    			<div class="right">
	    				<h3>{!! trans('aboutus.5things_title_4') !!}</h3>
	    				<p>{!! trans('aboutus.5things_4') !!}</p>
	    			</div>
	    		</div>
	    		<div class="points">
	    			<div class="left"><p>5</p></div>
	    			<div class="right">
	    				<h3>{!! trans('aboutus.5things_title_5') !!}</h3>
	    				<p>{!! trans('aboutus.5things_5') !!}</p>
	    			</div>
	    		</div>
	    	</div>
	    </div>
    </div>

    <div class="container container-p-15">
        <div class="row technology">
            <div class="col-md-3 col-md-offset-0 col-xs-offset-1 col-xs-10 col-sm-4">
                <div class="technology-sub frees-sub frees-sub text-center">
                    <img src="/images/Company-Info/reg-freelancer.jpg" width="130px">
                    <p>{!! trans('aboutus.footer1_title') !!}</p>
                    <h2>{!! trans('aboutus.footer1_desc') !!}</h2>
                </div>
            </div>
            <div class="col-md-3 col-md-offset-0 col-xs-offset-1 col-xs-10 col-sm-4">
                <div class="technology-sub frees-sub text-center">
                    <img src="/images/Company-Info/reg-client.jpg" width="130px">
                    <p>{!! trans('aboutus.footer2_title') !!}</p>
                    <h2>{!! trans('aboutus.footer2_desc') !!}</h2>
                </div>
            </div>
            <div class="col-md-3 col-md-offset-0 col-xs-offset-1 col-xs-10 col-sm-4">
                <div class="technology-sub frees-sub text-center">
                    <img src="/images/Company-Info/job-post.jpg" width="130px">
                    <p>{!! trans('aboutus.footer3_title') !!}</p>
                    <h2>{!! trans('aboutus.footer3_desc') !!}</h2>
                </div>
            </div>
            <div class="col-md-3 col-md-offset-0 col-xs-offset-1 col-xs-10 col-sm-4">
                <div class="technology-sub frees-sub text-center">
                    <img src="/images/Company-Info/worth-work.jpg" width="130px">
                    <p>{!! trans('aboutus.footer4_title') !!}</p>
                    <h2>{!! trans('aboutus.footer4_desc') !!}</h2>
                </div>
            </div>
        </div>
    </div>

</div>    
@endsection

