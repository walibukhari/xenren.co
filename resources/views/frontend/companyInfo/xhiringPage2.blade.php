@extends('frontend.companyInfo.layouts.default')

@section('title') 
    {!! trans('common.hiringpage_title') !!}
@endsection

@section('keywords') 
    {!! trans('common.hiringpage_keyword') !!}
@endsection

@section('description') 
    {!! trans('common.hiringpage_desc') !!}
@endsection

@section('author')
    Xenren
@endsection

@section('header')

    <link href="/custom/css/frontend/home-new-custom.css" rel="stylesheet" type="text/css"/>

    <!-- END HEADER AND FOOTER PAGE STYLES -->
    <link href="/css/hiring-page.css" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <div class="page-container page-container-main">
        @include('frontend.companyInfo.layouts.menu')

        <div class="row banner">
            <div class="container">
                <div class="col-md-12 banner-inner">
                    <div class="banner-inner-text">
                        <h1>We are <span style="color: #58AF2A;"> hiring</span> now!</h1>
                        <p>Working at BookMyShow is fun</p>
                        <a class="btn banner-btn-readmore">Read More</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="container section">
            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <h1 class="title">Join our <span>Team</span></h1>
                </div>
            </div>
            <div class="row sub-section">
                <div class="col-md-6 col-xs-12 col-sm-6">
                    <h2 class="sub-title">Sales representative</h2>

                    <div class="text-content">
                        We are looking for a talented sales manager to join <span>our Legends Lair Team</span> full-time
                        in Hong Kong. A highly skilled team with flat hierarchies, a creative environment and a lot of
                        fun makes <span>Legends Lair</span> a great place to work at. If you love developing
                        cutting-edge software for the web and are passionate about the latest web technologies, join our
                        team!
                    </div>

                    <div class="custom-selectbox">
                        <select class="form-control" name="slcSkill" id="slcSkill">
                            <option value="0">Read More</option>
                        </select>
                    </div>
                    <br>

                    <div class="list">
                        <h2 class="sub-title">Responsibilities</h2>
                        <ul>
                            <li>Lorem ipsum dolor sit amet, consectetur</li>
                            <li>Lorem ipsum dolor sit amet, consectetur</li>
                            <li>Lorem ipsum dolor sit amet, consectetur</li>
                        </ul>
                    </div>

                    <div class="list">
                        <h2 class="sub-title">Requirements:</h2>
                        <ul>
                            <li>Lorem ipsum dolor sit amet, consectetur</li>
                            <li>Lorem ipsum dolor sit amet, consectetur</li>
                            <li>Lorem ipsum dolor sit amet, consectetur</li>
                        </ul>
                    </div>

                    <div class="list">
                        <h2 class="sub-title pull-left">Apply for this position</h2>
                        <a class="btn btn-readmore pull-left">Apply</a>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12 col-sm-6 text-center">
                    <img src="/images/Company-Info/sales.png">
                </div>
            </div>

            <div class="row sub-section">
                <div class="col-md-6 col-xs-12 col-sm-6 text-center">
                    <img src="/images/Company-Info/programer.png">
                </div>
                <div class="col-md-6 col-xs-12 col-sm-6">
                    <h2 class="sub-title">Programmer</h2>

                    <div class="text-content">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nultam vet iaculis nibh. non egestas
                        quam. Morbi aliquet justo malesuada neque gravida tempor. Cras mollis justo erat, sit amet
                        bibendum ante tacinia a Curabitur a ligula nisi. Nam ultrices, eros maximus consequat lacinia,
                        augue nunc hendrerit null, quis luctus turpis culla placerat erat. Aenean vitae massa nulla.
                    </div>

                    <div class="custom-selectbox">
                        <select class="form-control" name="slcSkill" id="slcSkill">
                            <option value="0">Read More</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row sub-section">
                <div class="col-md-6 col-xs-12 col-sm-6">
                    <h2 class="sub-title">Product Manager</h2>

                    <div class="text-content">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nultam vet iaculis nibh. non egestas
                        quam. Morbi aliquet justo malesuada neque gravida tempor. Cras mollis justo erat, sit amet
                        bibendum ante tacinia a Curabitur a ligula nisi. Nam ultrices, eros maximus consequat lacinia,
                        augue nunc hendrerit null, quis luctus turpis culla placerat erat. Aenean vitae massa nulla.
                    </div>

                    <div class="custom-selectbox">
                        <select class="form-control" name="slcSkill" id="slcSkill">
                            <option value="0">Read More</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12 col-sm-6 text-center">
                    <img src="/images/Company-Info/product-manager.png">
                </div>
            </div>

            <div class="row sub-section">
                <div class="col-md-6 col-xs-12 col-sm-6 text-center">
                    <img src="/images/Company-Info/online-marketing.png">
                </div>
                <div class="col-md-6 col-xs-12 col-sm-6">
                    <h2 class="sub-title">Online Marketing</h2>

                    <div class="text-content">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nultam vet iaculis nibh. non egestas
                        quam. Morbi aliquet justo malesuada neque gravida tempor. Cras mollis justo erat, sit amet
                        bibendum ante tacinia a Curabitur a ligula nisi. Nam ultrices, eros maximus consequat lacinia,
                        augue nunc hendrerit null, quis luctus turpis culla placerat erat. Aenean vitae massa nulla.
                    </div>

                    <div class="custom-selectbox">
                        <select class="form-control" name="slcSkill" id="slcSkill">
                            <option value="0">Read More</option>
                        </select>
                    </div>
                </div>
            </div>

        </div>


    </div>
@endsection