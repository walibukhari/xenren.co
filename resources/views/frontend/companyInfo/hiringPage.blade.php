@extends('frontend.companyInfo.layouts.default')

@section('title')
    {!! trans('common.hiringpage_title') !!}
@endsection

@section('keywords')
    {!! trans('common.hiringpage_keyword') !!}
@endsection

@section('description')
    {!! trans('common.hiringpage_desc') !!}
@endsection

@section('author')
    Xenren
@endsection

@section('header')

    <link href="/custom/css/frontend/home-new-custom.css" rel="stylesheet" type="text/css" />

    <!-- END HEADER AND FOOTER PAGE STYLES -->

	<link href="/css/hiring-page.css" rel="stylesheet" type="text/css" />

    @if(App::isLocale('cn'))
    	<style type="text/css">
    		.section h1.title{
    			font-weight: bold;
    			font-family: "Microsoft YaHei" !important;
    			font-size: 31px;
    		}
    	</style>
    @endif
	<style>
		.lds-ring {
			display: inline-block;
			position: relative;
			width: 20px;
			height: 20px;
		}
		.lds-ring div {
			box-sizing: border-box;
			display: block;
			position: absolute;
			width: 20px;
			height: 20px;
			margin: 1px;
			border: 2px solid #fff;
			border-radius: 50%;
			animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
			border-color: #fff transparent transparent transparent;
		}
		.lds-ring div:nth-child(1) {
			animation-delay: -0.45s;
		}
		.lds-ring div:nth-child(2) {
			animation-delay: -0.3s;
		}
		.lds-ring div:nth-child(3) {
			animation-delay: -0.15s;
		}
		@keyframes lds-ring {
			0% {
				transform: rotate(0deg);
			}
			100% {
				transform: rotate(360deg);
			}
		}
	</style>
@endsection

@section('content')
<div class="page-container page-container-main">
	@include('frontend.companyInfo.layouts.menu')

	<div class="row banner setROW">
	    <div class="container">
            <div class="col-md-12 col-xs-12 col-sm-12 banner-inner">
		        <div class="banner-inner-text">
		            <h1> {!! trans('hiringpage.banner_h1') !!} </h1>
		            <p>{!! trans('hiringpage.banner_p') !!}</p>
		            <a class="btn banner-btn-readmore">{!! trans('hiringpage.read_more') !!}</a>
		        </div>
	        </div>
	    </div>
	</div>

	<div class="container section">
		<div class="row">
			<div class="col-md-12 col-xs-12 col-sm-12">
				<h1 class="title">{!! trans('hiringpage.title') !!}</h1>
			</div>
		</div>
		<div class="row sub-section">
			<div class="col-md-6 col-xs-12 col-sm-6">
				<h2 class="sub-title">
					{!! trans('hiringpage.section1_title') !!}
				</h2>

				<div class="text-content">
					{!! trans('hiringpage.section1_desc') !!}
				</div>

                <div class="list">
                	<h2 class="sub-title pull-left">{!! trans('hiringpage.apply_position') !!}</h2>
                	<a class="btn btn-readmore pull-left" data-toggle="modal" onclick="setPositions('Sales representative')" data-target="#myHiringModal">{!! trans('hiringpage.apply') !!}</a>
                </div>
                <br>
			</div>
			<div class="col-md-6 col-xs-12 col-sm-6 text-center">
				<img src="/images/Company-Info/our-team.png">
			</div>
		</div>

		<div class="row sub-section">
			<div class="col-md-6 col-xs-12 col-sm-6 text-center">
				<img src="/images/Company-Info/programmer.png">
			</div>
			<div class="col-md-6 col-xs-12 col-sm-6">
				<h2 class="sub-title">
					{!! trans('hiringpage.section2_title') !!}
				</h2>

				<div class="text-content">
					{!! trans('hiringpage.section2_desc') !!}
				</div>

                <div class="list">
					<h2 class="sub-title pull-left">{!! trans('hiringpage.apply_position') !!}</h2>
					<a class="btn btn-readmore pull-left" data-toggle="modal" onclick="setPositions('Software Engineer')" data-target="#myHiringModal" >{!! trans('hiringpage.apply') !!}</a>
				</div>
			</div>
		</div>

		<div class="row sub-section">
			<div class="col-md-6 col-xs-12 col-sm-6">
				<h2 class="sub-title">
					{!! trans('hiringpage.section3_title') !!}
				</h2>

				<div class="text-content">
					{!! trans('hiringpage.section3_desc') !!}
				</div>

                <div class="list">
                	<h2 class="sub-title pull-left">{!! trans('hiringpage.apply_position') !!}</h2>
                	<a class="btn btn-readmore pull-left" data-toggle="modal" onclick="setPositions('Product Manager')" data-target="#myHiringModal">{!! trans('hiringpage.apply') !!}</a>
                </div>
			</div>
			<div class="col-md-6 col-xs-12 col-sm-6 text-center">
				<img src="/images/Company-Info/Product Manager.png">
			</div>
		</div>

		<div class="row sub-section">
			<div class="col-md-6 col-xs-12 col-sm-6 text-center">
				<img src="/images/Company-Info/marketing.png">
			</div>
			<div class="col-md-6 col-xs-12 col-sm-6">
				<h2 class="sub-title">
					{!! trans('hiringpage.section4_title') !!}
				</h2>

				<div class="text-content">
					{!! trans('hiringpage.section4_desc') !!}
				</div>

                <div class="list">
                	<h2 class="sub-title pull-left">{!! trans('hiringpage.apply_position') !!}</h2>
                	<a class="btn btn-readmore pull-left" data-toggle="modal" onclick="setPositions('Online Marketer')" data-target="#myHiringModal" >{!! trans('hiringpage.apply') !!}</a>
                </div>
			</div>
		</div>

	</div>
</div>


<!-- Modal -->
<div id="myHiringModal" class="modal fade" role="dialog" >
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">{{trans('common.details')}}</h4>
			</div>
		<form id="hiringForm" method="post" enctype="multipart/form-data">
			<div class="modal-body">
			<div class="row">
				<div class="col-md-6">
					<label>{{trans('common.first_name')}}</label>
					<div class="form-group">
						<input type="text" name="first_name" id="first_name" class="form-control">
					</div>
				</div>
				<div class="col-md-6">
					<label>{{trans('common.last_name')}}</label>
					<div class="form-group">
						<input type="text" name="last_name" id="last_name" class="form-control">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<label>{{trans('common.email')}}</label>
					<div class="form-group">
						<input type="email" name="email" id="email" class="form-control">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<label>{{trans('hiringpage.select_position')}}</label>
					<div class="form-group">
						<select class="form-control" name="job_position" id="job_position">
							<option id="set-default"></option>
							@foreach($positions  as $pos)
								<option>{{$pos->job_positions}}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<label>{{trans('common.about_us')}}</label>
					<div class="form-group">
						<textarea type="text" placeholder="{{trans('common.about_us')}}" name="bio" id="bio" class="form-control"></textarea>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<label>{{trans('common.upload')}} {{trans('common.resume_cv')}}</label>
					<div class="form-group">
						<input type="file" name="resume" id="resume" class="form-control">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<label>{{trans('common.captcha')}} </label>
					<div class="form-group">
						<span id="refreshCaptcha">@captcha</span>
						<input type="text" id="captcha" name="captcha" autocomplete="off" class="form-control" style="margin-top: 10px;border: 1px solid #ddd;">
					</div>
				</div>
			</div>
			</div>
			<div class="modal-footer" style="display:flex;justify-content:flex-end;">
				<button type="button" style="width: 106px;height: 48px;display:flex;justify-content:center;align-items:center;" id="submitHiringForm" class="btn btn-default set-btn-btn-submit">
					<span class="idSpanSubmitHiring">{{trans('common.submit')}}</span>
					<div style="display:none;" class="lds-ring">
						<div></div><div></div><div></div><div></div>
					</div>
				</button>
			</div>
		</form>
		</div>

	</div>
</div>

@endsection

@section('footer')
	<script type="text/javascript">
		function setPositions(position)
		{
			$('#set-default').html(position);
		}

		$('#submitHiringForm').click(function (event){
			$('.idSpanSubmitHiring').hide();
			$('.lds-ring').show();
			event.preventDefault();
			var formData = new FormData($("#hiringForm")[0]);
			var first_name = $('#first_name').val();
			var last_name = $('#last_name').val();
			var email = $('#email').val();
			var job_position = $('#job_position').val();
			var bio = $('#bio').val();
			var resume = $('#resume')[0].files[0];
			var captcha = $('#captcha').val();
			var token = '{!! csrf_token() !!}';
			var link = '/submit-hiring-form';

			if(first_name === '')
			{
				$('.idSpanSubmitHiring').show();
				$('.lds-ring').hide();
				toastr.error('Please enter first name');
			} else if(last_name === '') {
				$('.idSpanSubmitHiring').show();
				$('.lds-ring').hide();
				toastr.error('Please enter last name');
			} else if(email === '') {
				$('.idSpanSubmitHiring').show();
				$('.lds-ring').hide();
				toastr.error('Please enter email');
			} else if(job_position === '') {
				$('.idSpanSubmitHiring').show();
				$('.lds-ring').hide();
				toastr.error('Please enter job position');
			} else if(bio === '') {
				$('.idSpanSubmitHiring').show();
				$('.lds-ring').hide();
				toastr.error('Please tell about us');
			} else if(resume === undefined) {
				$('.idSpanSubmitHiring').show();
				$('.lds-ring').hide();
				toastr.error('Please upload resume');
			} else if(captcha === '') {
				toastr.error('Please enter captcha');
				$('.idSpanSubmitHiring').show();
				$('.lds-ring').hide();
			} else {
				$.ajax({
					url:link,
					method: 'POST',
					data:formData,
					headers:{Authorization:token},
					processData: false,
					contentType: false,
					success: function(data)
					{
						if(data.error)
						{
							$('.idSpanSubmitHiring').show();
							$('.lds-ring').hide();
							toastr.error(data.error);
							document.getElementById('captcha').value = '' ;
						     var captcha = document.getElementById('refreshCaptcha').innerHTML ;
							document.getElementById('refreshCaptcha').innerHTML = captcha ;

						}
						if(data.message)
						{
							window.location.reload();
							toastr.error(data.message);
							$('#myHiringModal').modal("hide");
							$('#hiringForm')[0].reset();

						}
						if(data.success){
							window.location.reload();
							toastr.success("you have successfully apply for this position");
							$('#myHiringModal').modal("hide");
							$('#hiringForm')[0].reset();
						}
					}
				});
			}
		});
	</script>
@endsection
