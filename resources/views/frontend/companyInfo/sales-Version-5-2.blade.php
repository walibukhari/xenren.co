@extends('frontend.companyInfo.layouts.default')

@section('title') 
{{ trans('common.team_title') }}
@endsection

@section('keywords')
{{ trans('common.team_keyword') }}
@endsection

@section('description') 
{{ trans('common.team_desc') }}
@endsection

@section('author') 
    Xenren
@endsection

@section('header')
    <link href="/custom/css/frontend/home-new-custom.css" rel="stylesheet" type="text/css" />

    <!-- END HEADER AND FOOTER PAGE STYLES -->
    <link href="/css/company-info.css" rel="stylesheet" type="text/css" />
    <link href="/css/sales-version.css" rel="stylesheet" type="text/css" />
    <link href="/css/sales-version52.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
<div class="page-container page-container-main">
    @include('frontend.companyInfo.layouts.menu')
    
    <div class="row banner">
        <div class="col-md-12 col-xs-12 col-sm-12 banner-inner">
            <div class="banner-inner-text">
                <span>{!! trans('team.banner_span') !!}</span>
                <h1 class="set-banner-ineer-text-h1">{!! trans('team.banner_h1') !!}</h1>
            </div>
        </div>
    </div>

    <div class="container setTEAMCONTAINER">
        <div class="row great-websiet setROWMEET">
            <div class="col-md-7 col-xs-12 col-sm-7 left">
                <img src="/images/greatwebsite.jpg">
            </div>
            <div class="col-md-5 col-xs-12 col-sm-5 right">
                <h2 class="title-name">
                    {!! trans('team.title1') !!}  
                </h2>
                {!! trans('team.desc1') !!}  
            </div>
        </div>

        <div class="row mobile-app setROWMEET">
            <div class="col-md-6 col-xs-12 col-sm-6 left">
                <h2 class="title-name">
                    {!! trans('team.title2') !!}
                </h2>

                {!! trans('team.desc2') !!}  
            </div>
            <div class="col-md-6 col-xs-12 col-sm-6 right">
                <img src="/images/mobile-app.jpg">
            </div>
        </div>

        <div class="row technology setROWMEET">
            <h2 class="title-name text-center">
                {!! trans('team.title3') !!}
            </h2>

            <div class="col-md-3 col-md-offset-0 col-xs-offset-1 col-xs-10 col-sm-4">
                <div class="technology-sub text-center">
                    <div class="img">
                        <img src="/images/icons/Java-10.png">
                    </div>
                    <h2>{!! trans('team.java') !!}</h2>
                </div>
            </div>

            <div class="col-md-3 col-md-offset-0 col-xs-offset-1 col-xs-10 col-sm-4">
                <div class="technology-sub text-center">
                    <div class="img">
                        <img src="/images/icons/Laravel.png">
                    </div>
                    <h2>{!! trans('team.laravel') !!}</h2>
                </div>
            </div>

            <div class="col-md-3 col-md-offset-0 col-xs-offset-1 col-xs-10 col-sm-4">
                <div class="technology-sub text-center">
                    <div class="img">
                        <img src="/images/icons/Apple-Icon-01.png">
                    </div>
                    <h2>{!! trans('team.ios') !!}</h2>
                </div>
            </div>

            <div class="col-md-3 col-md-offset-0 col-xs-offset-1 col-xs-10 col-sm-4">
                <div class="technology-sub text-center">
                    <div class="img">
                        <img src="/images/icons/Android-01.png">
                    </div>
                    <h2>{!! trans('team.android') !!}</h2>
                </div>
            </div>
        </div>
    </div>

    {{--<div class="row our-team setOUTEAM">--}}
        {{--<h2 class="title-name text-center">--}}
            {{--{!! trans('team.title5') !!} --}}
        {{--</h2>--}}
        {{--<div class="container team-container">--}}
            {{--<div class="col-md-6 col-xs-12 col-sm-12">--}}
                {{--<div class="row our-teams">--}}
                    {{--<div class="col-md-4 col-xs-12 col-sm-4 team-img">--}}
                        {{--<div class="img">--}}
                            {{--<img src="/images/fong.jpg" class="setSV52IMG1">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-8 col-xs-12 col-sm-4 team-desc">--}}
                        {{--<h3><span>{!! trans('team.core_team_position1') !!}</span></h3>--}}
                        {{--<h2>{!! trans('team.core_team_name1_h2') !!}</h2>--}}
                        {{--<span>fong@xenren.co  </span>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-6 col-xs-12 col-sm-12">--}}
                {{--<div class="row our-teams">--}}
                    {{--<div class="col-md-4 col-xs-12 col-sm-4 team-img">--}}
                        {{--<div class="img">--}}
                            {{--<img src="/images/niew.jpg" class="setSV52IMG2">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-8 col-xs-12 col-sm-4 team-desc">--}}
                        {{--<h3><span>{!! trans('team.core_team_position2') !!}</span></h3>--}}
                        {{--<h2>Niew Yee Yaw</h2>--}}
                        {{--<span>nyy@xenren.co  </span>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-6 col-xs-12 col-sm-12">--}}
                {{--<div class="row our-teams">--}}
                    {{--<div class="col-md-4 col-xs-12 col-sm-4 team-img">--}}
                        {{--<div class="img">--}}
                            {{--<img src="/images/marsel.jpg" class="setSV52IMG3" >--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-8 col-xs-12 col-sm-4 team-desc">--}}
                        {{--<h3><span>{!! trans('team.core_team_position3') !!}</span></h3>--}}
                        {{--<h2>Marcel Lewandoski</h2>--}}
                        {{--<span>marcel@xenren.co </span>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-6 col-xs-12 col-sm-12">--}}
                {{--<div class="row our-teams">--}}
                    {{--<div class="col-md-4 col-xs-12 col-sm-4 team-img">--}}
                        {{--<div class="img">--}}
                            {{--<img src="/images/hasper.jpg" class="setSV52IMG4">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-8 col-xs-12 col-sm-4 team-desc">--}}
                        {{--<h3><span>{!! trans('team.core_team_position5') !!}</span></h3>--}}
                        {{--<h2>Hasper Ong</h2>--}}
                        {{--<span> hasper@xenren.co danny@xenren.co  </span>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-6 col-xs-12 col-sm-12">--}}
                {{--<div class="row our-teams">--}}
                    {{--<div class="col-md-4 col-xs-12 col-sm-4 team-img">--}}
                        {{--<div class="img">--}}
                            {{--<img src="/images/danny.jpg" class="setSV52IMG5">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-8 col-xs-12 col-sm-4 team-desc">--}}
                        {{--<h3><span>{!! trans('team.core_team_position4') !!}</span></h3>--}}
                        {{--<h2>{!! trans('team.core_team_name2_h2') !!}</h2>--}}
                        {{--<span>neorixfogn880@hotmail.com </span>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-6 col-xs-12 col-sm-12">--}}
                {{--<div class="row our-teams">--}}
                    {{--<div class="col-md-4 col-xs-12 col-sm-4 team-img">--}}
                        {{--<div class="img">--}}
                            {{--<img src="/images/lina.jpg">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-8 col-xs-12 col-sm-4 team-desc">--}}
                        {{--<h3><span>{!! trans('team.core_team_position6') !!}</span></h3>--}}
                        {{--<h2>Lina Chieng</h2>--}}
                        {{--<span>lina@xenren.co</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="container">--}}
            {{--<div class="row last-contant">--}}
                {{--<div class="col-md-5 col-xs-12 col-sm-6 left">--}}
                    {{--{!! trans('team.last_desc') !!}--}}
                {{--</div>--}}
                {{--<div class="col-md-7 col-xs-12 col-sm-6 right">--}}
                    {{--<img src="/images/imga8.jpg">--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>   --}}

    {{--<div class="row core-team">--}}
        {{--<h2 class="title-name text-center">--}}
            {{--{!! trans('team.title4') !!}--}}
        {{--</h2>--}}
        {{--<div class="container core-team-container setCORETEAMCONT">--}}
            {{--<div class="core-team-member">--}}
                {{--<div class="core-team-member1 a">--}}
                    {{--<p>{!! trans('team.core_team_position2') !!}</p>--}}
                    {{--<h2>Niew <br> YEE YAW</h2>--}}
                {{--</div>--}}
                {{--<img src="/images/Company-Info/Niew.png">--}}
            {{--</div>--}}

            {{--<div class="core-team-member">--}}
                {{--<div class="core-team-member2 a">--}}
                    {{--<p>{!! trans('team.core_team_position3') !!}</p>--}}
                    {{--<h2>Marcel <br> LEWANDOSKI</h2>--}}
                {{--</div>--}}
                {{--<img src="/images/Company-Info/Marcel.png">--}}
            {{--</div>--}}

            {{--<div class="core-team-member">--}}
                {{--<div class="core-team-member3 a">--}}
                    {{--<p>{!! trans('team.core_team_position1') !!}</p>--}}
                    {{--<h2>{!! trans('team.core_team_name1') !!}</h2>--}}
                {{--</div>--}}
                {{--<img src="/images/Company-Info/Fong1-.png">--}}
            {{--</div>--}}

            {{--<div class="core-team-member">--}}
                {{--<div class="core-team-member4 a">--}}
                    {{--<p>{!! trans('team.core_team_position4') !!}</p>--}}
                    {{--<h2>{!! trans('team.core_team_name2') !!}</h2>--}}
                {{--</div>--}}
                {{--<img src="/images/Company-Info/Danny.png">--}}
            {{--</div>--}}

            {{--<div class="core-team-member">--}}
                {{--<div class="core-team-member5 a">--}}
                    {{--<p>{!! trans('team.core_team_position5') !!}</p>--}}
                    {{--<h2>Hasper <br> ONG</h2>--}}
                {{--</div>--}}
                {{--<img src="/images/Company-Info/Hasper.png">--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>    --}}
    
</div>    
@endsection

