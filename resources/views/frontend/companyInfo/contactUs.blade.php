@extends('frontend.companyInfo.layouts.default')

@section('title') 
    {!! trans('common.contactus_title') !!}
@endsection

@section('keywords') 
    {!! trans('common.contactus_keyword') !!}
@endsection

@section('description') 
    {!! trans('common.contactus_desc') !!}
@endsection

@section('author') 
    Xenren
@endsection

@section('header')
    <link href="/custom/css/frontend/home-new-custom.css" rel="stylesheet" type="text/css" />

    <!-- END HEADER AND FOOTER PAGE STYLES -->
    <link href="/css/company-info.css" rel="stylesheet" type="text/css" />
    <link href="/css/sales-version.css" rel="stylesheet" type="text/css" />
    <link href="/css/contactUs.css" rel="stylesheet" type="text/css">
@endsection

@section('style')
@endsection

@section('content')

<div class="page-container page-container-main">
    @include('frontend.companyInfo.layouts.menu')
    
    <div class="row banner">
        <div class="col-md-12 col-xs-12 col-sm-12 banner-inner">
            <div class="banner-inner-text">
                <span>{!! trans('contactus.banner_span') !!}</span>
                <h1>{!! trans('contactus.banner_h1') !!}</h1>
            </div>
        </div>
    </div>

    <div class="container container-p-15 contant-hong-kong">
        <div class="row">
            <div class="col-md-6 col-xs-12 col-sm-6 text-center">
                <img class="contact-img" src="/images/Company-Info/contant-hong-kong.jpg" width="400px">
            </div>
            <div class="col-md-6 col-xs-12 col-sm-6 contact-address">
                <br>
                <h2 class="title-name"> {!! trans('contactus.hongkong') !!}</h2>
                <span class="sm-text">{!! trans('contactus.legends_lair_limited') !!}</span>

                <div class="row address-sub">
                    <div class="col-md-2 col-xs-4 col-sm-3 icon">
                        <img src="/images/Company-Info/icon-contact.jpg" width="60px">
                    </div>
                    <div class="col-md-10 col-xs-8 col-sm-9 add-text">
                        <p>{!! trans('contactus.service_line') !!}:</p>
                        <address>{!! trans('contactus.service_line_n') !!}</address>
                    </div>
                </div>
                <div class="row address-sub">
                    <div class="col-md-2 col-xs-4 col-sm-3 icon">
                        <img src="/images/Company-Info/icon-location.jpg" width="60px">
                    </div>
                    <div class="col-md-10 col-xs-8 col-sm-9 add-text">
                        <p>{!! trans('contactus.address') !!}:</p>
                        <address>
                            {!! trans('contactus.address_t') !!}
                        </address>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container container-p-15 contant-china">
        <div class="row">
            <div class="col-md-6 col-xs-12 col-sm-6 contact-address">
                <h2 class="title-name">{!! trans('contactus.china_shen_zhen') !!}</h2>
                <span class="sm-text">{!! trans('contactus.china_legends_lair_limited') !!} </span>

                <div class="row address-sub">
                    <div class="col-md-2 col-xs-4 col-sm-3 icon">
                        <img src="/images/Company-Info/icon-contact.jpg" width="60px">
                    </div>
                    <div class="col-md-10 col-xs-8 col-sm-9 add-text">
                        <p>{!! trans('contactus.service_line') !!}:</p>
                        <address>{!! trans('contactus.china_service_line_n') !!}</address>
                    </div>
                </div>
                <div class="row address-sub">
                    <div class="col-md-2 col-xs-4 col-sm-3 icon">
                        <img src="/images/Company-Info/icon-location.jpg" width="60px">
                    </div>
                    <div class="col-md-10 col-xs-8 col-sm-9 add-text">
                        <p>{!! trans('contactus.address') !!}:</p>
                        <address>
                            {!! trans('contactus.china_address_t') !!}
                        </address>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12 col-sm-6 text-center">
                <img class="contact-img" src="/images/Company-Info/contact-chaina.jpg" width="480px">
            </div>
        </div>
    </div>

    <div class="container container-p-15 contant-malasiya">
        <div class="row">
            <div class="col-md-6 col-xs-12 col-sm-6 text-center">
                <br>
                <img class="contact-img" src="/images/Company-Info/contact-malasiya.jpg" width="420px">
            </div>
            <div class="col-md-6 col-xs-12 col-sm-6 contact-address">
                <h2 class="title-name">{!! trans('contactus.malasyia') !!} </h2>
                <span class="sm-text">{!! trans('contactus.malasyia_legends') !!} </span>

                <div class="row address-sub">
                    <div class="col-md-2 col-xs-4 col-sm-3 icon">
                        <img src="/images/Company-Info/icon-contact.jpg" width="60px">
                    </div>
                    <div class="col-md-10 col-xs-8 col-sm-9 add-text">
                        <p>{!! trans('contactus.service_line') !!}:</p>
                        <address>{!! trans('contactus.malasyia_service_line_n') !!}</address>
                    </div>
                </div>
                <div class="row address-sub">
                    <div class="col-md-2 col-xs-4 col-sm-3 icon">
                        <img src="/images/Company-Info/icon-location.jpg" width="60px">
                    </div>
                    <div class="col-md-10 col-xs-8 col-sm-9 add-text">
                        <p>{!! trans('contactus.address') !!}:</p>
                        <address>
                            {!! trans('contactus.malasyia_address_t') !!}
                        </address>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
<footer class="footer-main-contact">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12 text-center">
                <h1>
                    <img src="/images/Company-Info/footer-mail.jpg">
                    KF@XENREN.CO
                </h1>
            </div>
        </div>
    </div>
</footer>            
@endsection

