<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta name="description" content="神人 (XENREN) 集英天下，创新世界 - 企业文化">
    <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <meta content="@yield('description')" name="description"/>
    <meta content="@yield('keywords')" name="keywords"/>
    <meta content="@yield('author')" name="author"/>
    <meta property="wb:webmaster" content="4bfa78a5221bb48a" />
    <meta property="qc:admins" content="2552431241756375" />
    <!-- FAVICON -->
    {{--<link href="{{$_G['xenren_path']}}images/logo/fav_new.png" rel="icon" type="image/x-icon" />--}}
    <link href="{{asset('images/Favicon.jpg')}}" rel="icon" type="image/x-icon" />
    <link href='https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,600' rel='stylesheet' type='text/css'>
    
    @include('frontend-xenren.includes.headscript')
    @include('frontend-xenren.includes.headscript-new')
    @yield('style')
    @section('header') @show
    <style>
        body {
            margin: 0;
            font-family: Arial, Helvetica, sans-serif;
        }

        .setalignment{
            border-right: 0px solid #c1c1c1 !important;
            font-weight: 600;
            margin-top: 20px;
            padding-right: 10px;
            text-transform: capitalize !important;
            color: #FFF !important;
            font-size: 12px !important;
        }

        .logo-default{
            margin-top:-6px;
        }

        .topnav {
            overflow: hidden;
            background-color: #5ec329;
            margin: 0px;
            color: white;
            padding:5px;
            padding-left: 70px;
        }
        .topnav a {
            color: #fff;
            font-size: 16px;
            font-weight: 500;
            padding: 13px;
            text-decoration: none;
        }


        .topnav a:hover {
            color: white;
            text-decoration:none;
        }

        .active {
            color: #fff;
            margin-top:10px;
            text-decoration:underline;
            text-decoration-style: solid;
            text-decoration-color: white;
            font-size: 20px;
        }

        .topnav .icon {
            display: none;
        }
        #menu {
            display:none;
        }

        @media (max-width:1199px)
        {
            .topnav {
                overflow: hidden;
                background-color: #5ec329;
                margin: 0px;
                color: white;
                padding:5px;
                padding-left: 0px;
            }
        }

        @media screen and (max-width: 1199px) {
            .topnav a:not(:first-child) {display: none;}
            .topnav a.icon {
                float: right;
                display: block;
                right: 8px !important;
                top: 5px !important;
            }
            #menu{
                display:block
            }
            .topnav a {
                float: left;
                display: block;
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
                font-size: 21px;
                font-weight:500;
            }
        }

        @media screen and (max-width: 1199px) {
            .topnav.responsive {
                position: relative;
                right: 0;
                top: 0;
            }
            .topnav.responsive .icon {
                float: right;
                position: absolute;
                display: block;
                right: 5px !important;
                top: 5px !important;
            }
            .topnav.responsive a {
                float: none;
                display: block;
                text-align: left;
            }
        }

        .center-menu{
            margin-top:34px;
        }


    </style>
</head>

<body>
    <!-- Navigation -->
    @include('frontend.companyInfo.layouts.header')
    <!--/#Navigation-->

    <!-- Content -->
    @yield('content')
    <!-- /Content -->

    <!-- Footer -->
    @include('frontend-xenren.includes.footer-new')

    <!-- Java Script -->
    <script type="text/javascript" src="{{asset('xenren-assets/js/jquery-2.1.4.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('xenren-assets/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.js')}}" ></script>
    @section('footer') @show

    <script type="text/javascript">
        // $(function() {
        //     var pull = $('#mobile-menu');
        //     menu = $('.mobile-nav');
        //     menuHeight = menu.height();
        //
        //     $(pull).on('click', function(e) {
        //         e.preventDefault();
        //         menu.slideToggle("slow");
        //     });
        //
        //     $(window).resize(function() {
        //         var w = $(window).width();
        //         if (w > 1199 && menu.is(':hidden')) {
        //             menu.removeAttr('style');
        //         }
        //     });
        // });

        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }

        function clickFunction() {
            var x = document.getElementById("Topnav");
            if (x.className === "navbar") {
                x.className += " toggler";
            } else {
                x.className = "navbar";
            }
        }

    </script>

</body>
</html>