<style>
   .ul li  a {
        text-decoration: none;
        color: #494949;
        font-size: 16px;
       text-transform: uppercase;
    }
    a:hover {
        color:black;
    }
    header {
        background: #fff;
        width: 100%;
        /*height: 100%;*/
        position: fixed;
        top: 0;
        left: 0;
        z-index: 100;
    }
    #logo{
        margin: 20px;
        float: left;
        width: 200px;
        height: 40px;
        display: block;
        margin-left:30%;
    }
    nav {
        float: right;
        padding: 12px;
    }
   #menu-icon {
       display: hidden;
       width: 30px;
       height: 23px;
       background-image: url(images/logo-menu/mennu.png);
       background-repeat: round;
   }
    .ul{

        list-style: none;
    }
    .ul li {

        display: inline-block;
        float: left;
        padding: 10px
    }
   .text-right{
       border-right: 1px solid #c1c1c1;
       color: #494949;
       font-weight: 600;
       margin-top: 20px;
       padding-right: 10px;
       text-transform: uppercase;
       font-family: 'Raleway', sans-serif;
       font-size: 30px;
   }



    @media only screen and (max-width : 1199px) {
        .ul {
            list-style: none;
            width: 138px;
            padding: 0px;
        }
        header {
            position: fixed;
        }
        #menu-icon {
            width: 22px;
            height: 17px;
            margin-top: 18px;
            margin-right: 10px;
            display:inline-block;
        }
        nav ul, nav:active ul {
            display: none;
            position: absolute;
            background: #fff;
            right: 20px;
        }
        nav li {
            text-align: center;
            width: 100%;
            padding: 10px 0;
            margin: 0;
        }
        nav:hover ul {
            display: block;
        }
        .text-right {
            border-right: 1px solid #c1c1c1;
            color: #494949;
            font-weight: 600;
            margin-top: 28px;
            padding-right: 10px;
            text-transform: uppercase;
            font-family: 'Raleway', sans-serif;
            font-size: 22px;
        }
        .center-menu a{
            font-size:15px;
        }
    }


    @media (max-width:991px)
    {
        .text-right {
            border-right: 1px solid #c1c1c1;
            color: #494949;
            font-weight: 600;
            /* padding-right: 72px; */
            text-transform: uppercase;
            font-family: 'Raleway', sans-serif;
            font-size: 15px;
            margin-right: 9%;
        }
        #logo {
            margin: 20px;
            float: left;
            display: block;
        }
    }


    @media (max-width:767px)
    {
        .center-menu a {
            font-size: 13px;
        }
        .text-right {
            border-right: 1px solid #c1c1c1;
            color: #494949;
            font-weight: 600;
            /* padding-right: 72px; */
            text-transform: uppercase;
            font-family: 'Raleway', sans-serif;
            font-size: 15px;
            margin-right: -17%;
        }
    }

   @media (max-width:676px)
   {
       .center-menu a {
           font-size: 11px;
       }
       .text-right {
           border-right: 1px solid #c1c1c1;
           color: #494949;
           font-weight: 600;
           /* padding-right: 72px; */
           text-transform: uppercase;
           font-family: 'Raleway', sans-serif;
           font-size: 12px;
           margin-right: -17%;
       }
   }

    @media (max-width:496px)
    {
        .center-menu a {
            font-size: 11px;
        }
        .text-right {
            border-right: 1px solid #c1c1c1;
            color: #494949;
            font-weight: 600;
            /* padding-right: 72px; */
            text-transform: uppercase;
            font-family: 'Raleway', sans-serif;
            font-size: 11px;
            margin-right: -46%;
        }
        #menu-icon {
            width: 22px;
            height: 17px;
            margin-top: 18px;
            margin-right: 10px;
        }
    }


    @media (max-width:398px)
    {
        .page-logo img {
            width: 35px;
            height: 35px;
        }
        .text-right {
            border-right: 1px solid #c1c1c1;
            color: #494949;
            font-weight: 600;
            /* padding-right: 72px; */
            text-transform: uppercase;
            font-family: 'Raleway', sans-serif;
            font-size: 9px;
            margin-right: -46%;
        }
    }

    @media (max-width:411px)
    {

        .text-right {
            border-right: 1px solid #c1c1c1;
            color: #494949;
            font-weight: 600;
            /* padding-right: 72px; */
            text-transform: uppercase;
            font-family: 'Raleway', sans-serif;
            margin-right: -46%;
        }

    }

    @media (max-width:320px){
        .page-logo img {
            width: 35px;
            height: 35px;
        }
        .text-right {
            border-right: 1px solid #c1c1c1;
            color: #494949;
            font-weight: 600;
            /* padding-right: 72px; */
            text-transform: uppercase;
            font-family: 'Raleway', sans-serif;
            font-size: 9px;
            margin-right: -89%;
        }
    }


</style>

<header>

    <div class="row">
        <div class="col-md-2 col-xs-2">
            <div class="page-logo">
                <a id="logo" href="{{ route('home') }}">
                    <img src="{{ asset('images/homelogo.jpg') }}" alt="logo" class="logo-default" height="45px"/>
                </a>
            </div>
        </div>
        <div class="col-md-3 col-xs-2">
            <h2 class="text-right">{{ trans('home.legends_lair') }}</h2>
        </div>
        <div class="col-md-4 col-xs-5">
            <h3 class="center-menu">
                <a href="#">
                    {{ trans('home.ci_company_info') }}
                    @section('title') @show
                </a>
            </h3>
        </div>
        <div class="col-md-3">
            <nav>
                @if(!Auth::check())
                    <a href="#" id="menu-icon"></a>
                    <ul class="ul">
                        <li><a href="{{ route('register') }}">{{ trans('home.ci_register') }}</a></li>
                        <li><a href="{{ route('login') }}">{{ trans('home.ci_login') }}</a></li>
                        {{--<li><a href="{{ route('home') }}">{{ trans('home.ci_home') }}</a></li>--}}
                    </ul>
                @endif
            </nav>
        </div>
    </div>
</header>
