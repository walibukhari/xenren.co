<div class="topnav" id="myTopnav">
    <a href="#" id="menu"> Menu</a>
                    @if(App::isLocale('en'))

                        <a href="{{ route('frontend.company-culture', \Session::get('lang')) }}"
                            class="{{ Route::current()->getName() == 'frontend.company-culture' ? 'active' : '' }}">
                            {{ trans('home.ci_company_culture') }}
                        </a>

                    @elseif(App::isLocale('cn'))

                        <a href="{{ route('frontend.chinese-company-culture', \Session::get('lang')) }}"
                            class="{{ Route::current()->getName() == 'frontend.chinese-company-culture' ? 'active' : '' }}">
                            {{ trans('home.ci_company_culture') }}
                        </a>
                    @endif

                        <a href="{{ route('frontend.about-us', \Session::get('lang')) }}"
                            class="{{ Route::current()->getName() == 'frontend.about-us' ? 'active' : '' }}{{ Route::current()->getName() == 'frontend.hiring-page' ? 'active' : '' }}">
                            {{ trans('home.ci_about_us') }}
                        </a>

                        <a href="{{ route('frontend.terms-service', \Session::get('lang')) }}"
                            class="{{ Route::current()->getName() == 'frontend.terms-service' ? 'active' : '' }}">
                            {{ trans('home.ci_terms_service') }}
                        </a>

                        <a href="{{ route('frontend.privacy-policy', \Session::get('lang')) }}"
                            class="{{ Route::current()->getName() == 'frontend.privacy-policy' ? 'active' : '' }}">
                            {{ trans('home.ci_privacy_policy') }}
                        </a>

                        <a href="javascript:void(0);">
                            {{ trans('home.ci_company_info') }}
                        </a>

                    @if(Route::current()->getName() == 'frontend.meet-the-team2')
                        <a href="{{ route('frontend.meet-the-team2', \Session::get('lang')) }}"
                            class="{{ Route::current()->getName() == 'frontend.meet-the-team2' ? 'active' : '' }}">
                            {{ trans('home.ci_meet_team') }}
                        </a>
                    @else
                        <a href="{{ route('frontend.meet-sales-team', \Session::get('lang')) }}"
                            class="{{ Route::current()->getName() == 'frontend.meet-sales-team' ? 'active' : '' }}">
                            {{ trans('home.ci_meet_sales_team') }}
                        </a>
                    @endif
                        <a href="{{ route('frontend.contact-us', \Session::get('lang')) }}"
                            class="{{ Route::current()->getName() == 'frontend.contact-us' ? 'active' : '' }}">
                            {{ trans('home.ci_contact_us') }}
                        </a>
    <a href="javascript:void(0);" class="icon" onclick="myFunction()">
        <i class="fa fa-bars"></i>
    </a>
</div>    