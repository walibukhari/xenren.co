@extends('frontend.companyInfo.layouts.default')

@section('title')
    Company Culture
@endsection

@section('description')
    Company Culture
@endsection

@section('author')
    Xenren
@endsection

@section('header')
    <link href="/custom/css/frontend/home-new-custom.css" rel="stylesheet" type="text/css"/>

    <!-- END HEADER AND FOOTER PAGE STYLES -->
    <link href="/css/company-info.css" rel="stylesheet" type="text/css"/>
    <link href="/css/company-culture.css" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <div class="page-container page-container-main">
        @include('frontend.companyInfo.layouts.menu')

        <div class="row banner">
            <div class="col-md-12 col-xs-12 col-sm-12 banner-inner">
                <div class="banner-inner-text">
                    <span>How we beahve?</span><span> Here is</span>
                    <h1>LEGENDS LAIR's Culture</h1>
                </div>
            </div>
        </div>

        <div class="container setHCCContainer" id="intro">
            <div class="row setCCHTHISCROWCol12">
                <div class="col-md-12 col-xs-12 col-sm-12 intro">
                    <img class="setIM" src="/images/Company-Info/culture-intro-sm.png">

                    <h2 class="title-name">INTRODUCTION </h2>

                    <p>Our platform, <span class="black">Legend Lairs</span>, is the legendary starting point for those who wants a change in the world.  The company’s journey from its inception to date is quite. In a country where exposure to programming and technology was limited,to get well experienced and quality technical personnel I traveled to countries like Singapore, Hong Kong, Taiwan, China, Vietnam and India. </p>

                    <p><span class="green">But one such trip to India’s Silicon Valley, Bangalore, changed my world and how i perceived it.</span></p>
                    <p>On my trip to Banglore, I and a few of my local friends had the pleasure to visit Dharmapuri. What I saw there totally shocked and saddened me a helpless little girl was desperately trying to save her dying father.  I could not bear and I really wanted to do something for them. The only thing I had at that time was my pocket money of 2000 rupees.  I gave it to her without a second thought.  My Indian friends seemed familiar and used to such scenarios. They looked unperturbed and in fact one of them  tried to stop me and said: <span class="green">"Don't be crazy. You cannot save all people like this. There are too many of them."</span></p>


                    <p>At that moment, I personally felt way too small and weak. I always knew that there are people around the world who are in death and need support. But what's shown in the newspapers is a far cry from actual reality. Though this era has been regarded as prosperous and peaceful, more than half of the world's population is in a misery. There are wars. famine, disasters, refugees, elders without any support, and children without care all around the World.</p>

                    <p>There are some people in the world which one day expenditure is more than a poor family's one year or even several years of income. I know the world is not fair, but it should not be so unfair. However, there are also people who are dedicated to sacrifice themselves to help and save others.</p>

                    <p>We have the highest respect for such great people. And that's why <span class="black">Legends Lair came</span> into existence and became a registered company in 2014. We are trying to use our business model for a more systematic approach <span class="green">to sustain philanthropy.</span></p>
                </div>
            </div>
        </div>

        <div class="row setHCCRCol12" id="talent">
            <div class="col-md-12 col-xs-12 col-sm-12 talent">
                <div class="container">
                    <div class="row setCCHPGRCol12">
                        <div class="col-md-12 col-xs-12 col-sm-12 text-center">
                            <h2 class="title-name"><span>HALL OF TALENT - </span> <br> UNITED TO CHANGE THE WORLD </h2>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12">
                            <p>Legends Lair is a platform that brings together all those talented people who are in mission to do something good for the world.  We are driven by the belief that although God didn’t create a perfect world, <span class="green">we are here so as to make it a better place for everyone.  </span>
                            </p>
                            <p>Do you remember how in 2004, Beijing, Zhong Guan Cun, Kuala Lumpur, and Taiwan caught on the entrepreneurial boom? Almost everybody walking on the streets had a CEO's card in their name. However, two years later, most of these CEO's projects ceased to exist. </p><br>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-12 col-sm-6 talent-banners setCol12TBanner">
                        <img src="/images/Company-Info/talent-banner-sm.png">
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-xs-12 col-sm-offset-6 col-sm-6 pull-right">
                            <p>Though at the beginning we almost had nothing, our goal to make the world a better place brought me to let go of the past, consolidate all the technical knowledge and set up an IT business.</p>
                            <p>And just like every new company, we too experienced a lot of hardships and challenges but we overcome all. Looking back on how we surpassed all of it, <span class="green">we believe that it was the power of the faith we had in our mission that let us adhere to this road even today, and will even take us further in the future.</span>
                            </p>

                            <hr class="sm-line">

                            <h2 class="title-name text-center"><span>COMPANY </span> CULTURE </h2>

                            <p><span class="black">In Legends Lair</span>, your ability is directly proportional to the
                                power you will have and your contribution to your income. Your ability will decide your
                                share in decision-making while your contribution will determine how much you get </p>
                        </div>
                    </div>
                    <div class="row technology setRowTCCCol12">
                        <div class="col-md-offset-0 col-md-4 col-xs-offset-1 col-xs-10 col-sm-4 setColXSSMOFS">
                            <div class="technology-sub text-center">
                                <img src="/images/icons/Team.png">
                                <div class="text">
                                    <p><strong class="setCCPSP1">Firstly</strong>, our company does not have any
                                        upper or lower management, only the designations we all have make the
                                        difference. I do not feel that I'm some kind of leader. I'm just our company's
                                        decision-making department. The only reason I'm in this post is that so far, I
                                        have made all enterprise-development decisions fairly correct.</p>
                                    <p>In other words, as a programmer, you, too, are the decision-makers. Every row of
                                        code you type is the trust accord of your ability and performance. </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-offset-0 col-md-4 col-xs-offset-1 col-xs-10 col-sm-4">
                            <div class="technology-sub text-center">
                                <img src="/images/icons/Share.png">
                                <div class="text">
                                    <p><strong class="setCCPSP1">Secondly</strong>, your share in the company is all
                                        fair and transparent. These are also reflected in our share system. From the
                                        grass root members to the founders and the investors, each person's share
                                        allocation is done using the same mechanism. Full-time members don't qualify for
                                        fixed salaries but will get a share ratio of 30%. For those who are seeking
                                        fixed salary should invest a part of their salary to purchase the company
                                        shares. The value per share will always be one dollar each, to investor or to
                                        anyone. </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-offset-0 col-md-4 col-xs-offset-1 col-xs-10 col-sm-4">
                            <div class="technology-sub text-center">
                                <img src="/images/icons/Target.png">
                                <div class="text">
                                    <p><strong class="setCCPSP1">Thirdly</strong>, the goal of our enterprise is to
                                        run a crowdfunding model that distribute philanthropy benefits eternally. It's
                                        not about how much we are earning, it's all about how much good we have done for
                                        our society. Therefore we have not reserved our company's benefits to a handful
                                        of people, but to every team member, as well as to our community. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container" id="founder">
            <div class="row founder setRCCPF">
                <div class="col-md-4 col-xs-12 col-sm-4 technology">
                    <div class="technology-sub setCCHTSUB">
                        <p>- In the early years of Legends Lair, I am always bombarded with this one question, </p>
                        <h2 class=" text-center">"How big is the prospect, profitability and market for this
                            project?" </h2>
                    </div>
                </div>
                <div class="col-md-8 col-xs-12 col-sm-8 right-text setCCHPCol8">
                    <h2 class="title-name"><span>FOUNDER </span> WORDS </h2>
                    <p>And I always said the same thing to my partners:1 do not know how much money we will make in the
                        future. or how big prospect we will achieve. But what I can promise is that even if I will just
                        have rice on my plate, my shareholders will certainly rejoice a full meal If I can have a car,
                        you can certainly buy a house. I will make sure being my shareholder you will always have more
                        money than me. As for the implementation of the mission, what I care about most is to accomplish
                        the things we are here to achieve" </p>
                    <p>In our minds, <span class="black">Legends Lair Ltd.</span> will certainly become the company.
                        that even after 5000 years. there will still be people recalling our enterprise, and will
                        remember all those people who have changed the world </p>
                </div>
            </div>
        </div>

        {{--<div class="row setROWFF" id="founder-footer">--}}
            {{--<div class="col-md-12 col-xs-12 col-sm-12 founder-footer">--}}
                {{--<div class="container setCCPFFC">--}}
                    {{--<div class="col-md-9 col-xs-12 col-sm-7">--}}
                        {{--<img src="/images/Company-Info/Inverted-Comma.png">--}}
                        {{--<h1 class="fix-text setTHISH1CCP"> God didn't create a perfect world, <br> We are here to make it a better--}}
                            {{--place. </h1>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-8 col-xs-12 col-sm-6 text-right setFPFFOLL">--}}
                        {{--<h1>- Fong Pau Fung</h1>--}}
                        {{--<span>Founder of Legends Lair </span>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

    </div>
@endsection

