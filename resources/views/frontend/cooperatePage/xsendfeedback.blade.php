
{{--Click here to reset your password: <a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> {{ $link }} </a> <br/>--}}
{{--<p>  Your password hint is     {{$hint}}@for($i=0; $i<$len; $i++) * @endfor</p>--}}



<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0;">
    <meta name="format-detection" content="telephone=no"/>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700" rel="stylesheet">
    <style>
        /* Reset styles */
        body { margin: 0; padding: 0; min-width: 100%; width: 100% !important; height: 100% !important;}
        body, table, td, div, p, a { -webkit-font-smoothing: antialiased; text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; line-height: 100%; }
        table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse !important; border-spacing: 0; }
        img { border: 0; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
        #outlook a { padding: 0; }
        .ReadMsgBody { width: 100%; } .ExternalClass { width: 100%; }
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }

        /* Rounded corners for advanced mail clients only */
        @media all and (min-width: 560px) {
            .container { border-radius: 8px; -webkit-border-radius: 8px; -moz-border-radius: 8px; -khtml-border-radius: 8px;}
        }

        /* Set color for auto links (addresses, dates, etc.) */
        a, a:hover {
            color: #127DB3;
        }
        .footer a, .footer a:hover {
            color: #999999;
        }

    </style>

    <!-- MESSAGE SUBJECT -->
    <title>User Feedback</title>

</head>

<!-- BODY -->
<!-- Set message background color (twice) and text color (twice) -->
<body topmargin="0" rightmargin="0" bottommargin="0" leftmargin="0" marginwidth="0" marginheight="0" width="100%" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%; height: 100%; -webkit-font-smoothing: antialiased; text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; line-height: 100%;
	background-color: #F8F8F8;
	color: #000000; font-family: 'Montserrat', sans-serif;
	padding: 120px 0px;
"
      bgcolor="#F8F8F8"
      text="#000000">

<!-- SECTION / BACKGROUND -->
<!-- Set message background color one again -->
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%;" class="background">
    <tr>
        <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;"
            bgcolor="#F8F8F8">

            <!-- WRAPPER -->
            <!-- Set wrapper width (twice) -->
            <table border="0" cellpadding="0" cellspacing="0" align="center"
                   width="560" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
	max-width: 580px;" class="wrapper">

                <tr>
                    <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%;
			padding-top: 20px;
			padding-bottom: 20px;">
                        <img src="https://www.xenren.co/images/fav.png">
                    </td>
                </tr>

                <!-- End of WRAPPER -->
            </table>

            <!-- WRAPPER / CONTEINER -->
            <!-- Set conteiner background color -->
            <table border="0" cellpadding="0" cellspacing="0" align="center"
                   bgcolor="#fff"
                   width="580" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
	max-width: 680px; width: 680px; margin-top: 30px;" class="container">

                <tr>
                    <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 580px; font-size: 24px; font-weight: bold; line-height: 130%;color: #000000;font-family: sans-serif;" class="header">
                <tr>
                    <td>
                        <div style="text-align: center;">
                            <h3 style="font-size:26px;margin-bottom:10px;margin-top:60px; margin-bottom: 60px; color:#3DAF3F; font-weight: 600;">User feedback</h3>
                        </div>
                        <div style="clear: both;"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="text-align: center;">
						<span style="color:#000; font-size: 14px; font-weight: 600;">
                            <h3>Username :</h3>
                            <br>
                            {{$request['name']}} <br>
						</span>
                        </div>
                        <div style="clear: both;"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="text-align: center;">
						<span style="color:#000; font-size: 14px; font-weight: 600;">
                            <h3>User Email :</h3>
                            <br>
                            {{$request['email']}} <br>
						</span>
                        </div>
                        <div style="clear: both;"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="text-align: center;">
						<span style="color:#000; font-size: 14px; font-weight: 600;">
                            <h3>User Feedback Message :</h3>
                            <br>
                            {{$request['text']}} <br>
						</span>
                        </div>
                        <div style="clear: both;"></div>
                    </td>
                </tr>

                <!-- End of WRAPPER -->
            </table>
            <div style="clear: both;"></div>
            <!-- WRAPPER -->
            <!-- Set wrapper width (twice) -->
            <table border="0" cellpadding="0" cellspacing="0" align="center"
                   width="580" style="margin-bottom:30px; margin-top: 10px; border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
	max-width: 580px; width: 100%; margin-bottom: 150px;" class="wrapper">
                <tr>
                    <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 25px;" class="social-icons">
                        <span style="color: #A5A5A5; font-size: 14px; font-weight: 600;">{{ trans('common.unsubscribe_from_xenren') }}</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr style="background: #A5A5A5; width: 100%; border:0px; height: 1px;" />

                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 20px;" class="social-icons">
                        <span style="color: #3DAF3F; font-size: 14px; font-weight: 600;">
                            {{ trans('common.Legends_Lair_Limited-©-2014-2017')}}</span>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 10px;" class="social-icons">
                        <span class="company-register">
                            {{ trans('common.company_register_no') }} :
                            {{ trans('common.company_register_noId') }}
                        </span>
                        | <br><span class="customer-hotlink">
                            {{ trans('home.customer_hotline')}} :
                            {{trans('common.HotlineNo')}}
                        </span>
                    </td>
                </tr>
            </table>
        </td></tr></table>
</body>
</html>