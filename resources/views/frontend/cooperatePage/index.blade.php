@extends('frontend.companyInfo.layouts.default')

@section('title') 
    Cooperate Page
@endsection

@section('description') 
    Cooperate Page
@endsection

@section('author') 
    Xenren
@endsection

@section('header')

    <link href="/custom/css/frontend/home-new-custom.css" rel="stylesheet" type="text/css" />

    <!-- END HEADER AND FOOTER PAGE STYLES -->
    <link href="/css/cooperate-page.css" rel="stylesheet" type="text/css" />


@endsection
@section('content')
<div class="page-container page-container-main">
	@include('frontend.companyInfo.layouts.menu')

	<div class="row banner setRow">
	    <div class="container">
		    <div class="col-md-12 banner-inner">
		        <div class="banner-inner-text">
		            <h1>{!! trans('cooperate.banner_h1') !!}</h1>
		            <p>{!! trans('cooperate.banner_p') !!}</p>
		            <a class="btn btn-readmore">{!! trans('cooperate.read_more') !!}</a>
		        </div>
	        </div>
	    </div>
	</div>

	<div class="container section">
		<div class="row">
			<div class="col-md-12 col-xs-12 col-sm-12">
				<h1 class="title">{!! trans('cooperate.title1') !!}</h1>
			</div>
		</div>	
		<div class="row lend-section">
			<div class="col-md-6 col-xs-12 col-sm-6">
				<!-- <h2 class="sub-title">Lend out builind for us to make cabinator</h2> -->

				<div class="text-content">
					{!! trans('cooperate.description1') !!}
				</div>

				<a class="btn btn-readmore pull-right setBTNCONTUS">{!! trans('cooperate.contact_us') !!}</a>
			</div>
			<div class="col-md-6 col-xs-12 col-sm-6 text-right">
				<img src="/images/Company-Info/building.png">
			</div>
		</div>
	</div>

	<div class="container section">
		<div class="row">
			<div class="col-md-12 col-xs-12 col-sm-12">
				<h1 class="title">{!! trans('cooperate.title2') !!}</h1>
			</div>
		</div>	
		<div class="row software-section">
			<div class="col-md-6 col-xs-12 col-sm-6 text-center">
				<img src="/images/Company-Info/software-icon.png">
			</div>
			<div class="col-md-6 col-xs-12 col-sm-6">
				<!-- <h2 class="sub-title">We need time tracker software and also other potention software project</h2> -->

				<div class="text-content">
					{!! trans('cooperate.description2') !!}
				</div>

				<a class="btn btn-readmore pull-right setBTNCONTUS">{!! trans('cooperate.contact_us') !!}</a>
			</div>
		</div>
	</div>

	<div class="container section">
		<div class="row">
			<div class="col-md-12 col-xs-12 col-sm-12">
				<h1 class="title">{!! trans('cooperate.title3') !!}</h1>
			</div>
		</div>	
		<div class="row media-section">
			<div class="col-md-6 col-xs-12 col-sm-6">
				<!-- <h2 class="sub-title">Share our news</h2> -->

				<div class="text-content">
					{!! trans('cooperate.description3') !!}
				</div>

				<a class="btn btn-readmore pull-right setBTNCONTUS">{!! trans('cooperate.contact_us') !!}</a>
			</div>
			<div class="col-md-6 col-xs-12 col-sm-6 text-right">
				<img src="/images/Company-Info/media-icon.png">
			</div>
		</div>
	</div>

	<div class="container section">
		<div class="row">
			<div class="col-md-12 col-xs-12 col-sm-12">
				<h1 class="title">{!! trans('cooperate.title4') !!}</h1>
			</div>
		</div>	
		<div class="row game-section">
			<div class="col-md-6  col-xs-12 col-sm-6 text-center">
				<img src="/images/Company-Info/game.png">
			</div>
			<div class="col-md-6 col-xs-12 col-sm-6">
				<!-- <h2 class="sub-title">
					We can make api and link with our membership, 
					log in with xenren account will have benefit 
					at the same time bring user to the game
				</h2> -->

				<div class="text-content">
					{!! trans('cooperate.description4') !!}
				</div>

				<a class="btn btn-readmore pull-right setBTNCONTUS">{!! trans('cooperate.contact_us') !!}</a>
			</div>
		</div>
	</div>

	<div class="container section">
		<div class="row">
			<div class="col-md-12 col-xs-12 col-sm-12">
				<h1 class="title">{!! trans('cooperate.more_cooperate_recommend') !!}</h1>
			</div>
			{{--its better to change to More Cooperative Recommendation or More Cooperate Recommendation--}}
		</div>	
		<div class="row">
			<div class="col-md-offset-3 col-md-6 col-xs-offset-2 col-xs-8 col-sm-offset-3 col-sm-6 text-center contact-form">
				{!! Form::open(array('method'=>'post')) !!}
					
					<div class="form-group">
						{!! Form::text('name', null, array('class'=>'form-control','placeholder'=>trans('cooperate.your_name'))) !!}
					</div>
					<div class="form-group">
						{!! Form::text('email', null, array('class'=>'form-control','placeholder'=>trans('cooperate.your_email'))) !!}
					</div>
					<div class="form-group">
						{!! Form::textarea('text', null, array('class'=>'form-control','placeholder'=>trans('cooperate.text'))) !!}
					</div>

					<button type="submit" class="btn btn-readmore">Submit</button>
				{!! Form::close() !!}
			</div>
		</div>
	</div>


</div>
@endsection