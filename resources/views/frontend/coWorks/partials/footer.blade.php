<section class="footer-btn-section">
    <div class="container set-footer-container">
        <div class="row">
            <div class="col-md-12 footer-btn-section-title text-center">
                <p style="margin-top:20px">{{trans('common.browse_co_working')}}</p>
            </div>
            @php
                $continents = \App\Models\WorldContinents::get();
            @endphp
                <div class="col-md-12 text-center setColFooter">
                    @foreach($continents as $k => $v)
                     <a href="{{'/'.\Session::get('lang').'/coworking-spaces/'.str_replace(' ','-',$v->name)}}" class="btn btn-defualt custom-btn-footer">{{$v->name}}</a>
                    @endforeach
                </div>
        </div>
        @php
            $lang = \Session::get('lang');
            $base = env('APP_URL');
        @endphp
        <div id="footer">
            <div class="container setFooterContainer">
                <div class="footer-bg text-center">
                    <p>&copy; 2019 - 2020 {{trans('home.all_reversed')}}</p>
                    <ul>
                        <li><a href="{{ url($base.$lang.'/aboutUs') }}">{{ trans('home.about_us') }}</a></li>
                        <li><a href="{{ url($base.$lang.'/contactUs') }}">{{ trans('home.contact_us') }}</a></li>
                        <li><a href="{{ url($base.$lang) }}">{{ trans('home.landing_page') }}</a></li>
                        <li><a href="{{ url($base.$lang.'/hiringPage') }}">{{ trans('home.hiring_notice') }}</a></li>
                        <li><a href="{{ url($base.$lang.'/terms-service') }}">{{ trans('home.tnc') }}</a></li>
                        <li><a href="{{ url($base.$lang.'/privacy-policy') }}">{{ trans('home.privacy_policy') }}</a></li>
                        <li><a href="{{route('faq.coWorkfaq.cowork.user',[\Session::get('lang')])}}">Space User FAQ</a></li>
                        <li><a href="{{route('faq.coWorkfaq.cowork.space',[\Session::get('lang')])}}">Space Manager FAQ</a></li>
                        <li><a href="http://ditu.amap.com/place/B0FFFA23H5">{{ trans('home.location') }}</a></li>
                    </ul>
                </div>
            </div>
               <img class="set-scroll-to-image-go-to-top" id="click-scroll-to-top" src="{{asset('images/scrolltopimage.png')}}">
        </div>
    </div>
</section>

<!-- mobile view section footer -->
<section class="mobile-view-section-footer">
    <div class="col-md-12 text-center setColFooter footer-btn-section-title">
        <div class="container set-container-by-reigion">
            <p>{{trans('common.browse_co_working')}}....</p>
            <a href="{{'/'.\Session::get('lang').'/coworking-spaces/co-work-spaces/?location=Asia'}}" class="btn btn-defualt custom-btn-footer set-asia">{{trans('common.asia')}}</a>
            <a href="{{'/'.\Session::get('lang').'/coworking-spaces/co-work-spaces/?location=Africa'}}" class="btn btn-defualt custom-btn-footer set-africa">{{trans('common.africa')}}</a>
            <a href="{{'/'.\Session::get('lang').'/coworking-spaces/co-work-spaces/?location=North America'}}" class="btn btn-defualt custom-btn-footer set-north-america">{{trans('common.north_america')}}</a>
            <a href="{{'/'.\Session::get('lang').'/coworking-spaces/co-work-spaces/?location=South America'}}" class="btn btn-defualt custom-btn-footer set-south-america">{{trans('common.south_america')}}</a>
            <a href="{{'/'.\Session::get('lang').'/coworking-spaces/co-work-spaces/?location=Antarctica'}}" class="btn btn-defualt custom-btn-footer set-antarctica">{{trans('common.antarctica')}}</a>
            <a href="{{'/'.\Session::get('lang').'/coworking-spaces/co-work-spaces/?location=Europe'}}" class="btn btn-defualt custom-btn-footer set-europe">{{trans('common.europe')}}</a>
            <a href="{{'/'.\Session::get('lang').'/coworking-spaces/co-work-spaces/?location=Australia'}}" class="btn btn-defualt custom-btn-footer set-australia">{{trans('common.australia')}}</a>
        </div>
    </div>
    <div class="container set-container-footer">
        <p class="set-mobile-view-footer">2017 - 2020 {{trans('home.all_reversed')}}</p>
        <ul class="set-un-order-list-mobile-view-footer">
            <li><a href="{{ route('frontend.about-us') }}">{{ trans('home.about_us') }}</a></li>
            <li><a href="{{ route('frontend.contact-us') }}">{{ trans('home.contact_us') }}</a></li>
            <li><a href="{{ route('home') }}">{{ trans('home.landing_page') }}</a></li>
            <li><a href="{{ route('frontend.hiring-page') }}">{{ trans('home.hiring_notice') }}</a></li>
            <li><a href="{{ route('frontend.terms-service') }}">{{ trans('home.tnc') }}</a></li>
            <li><a href="{{ route('frontend.privacy-policy') }}">{{ trans('home.privacy_policy') }}</a></li>
            <li><a href="{{ route('frontend.privacy-policy') }}">{{ trans('home.support') }}</a></li>
            <li> <a href="http://ditu.amap.com/place/B0FFFA23H5">{{ trans('home.location') }}</a></li>
        </ul>
    </div>
</section>
<!-- mobile view section footer -->
