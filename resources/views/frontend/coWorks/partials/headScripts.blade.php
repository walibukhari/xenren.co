<!-- Latest compiled and minified CSS -->
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Raleway:400,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<link rel="stylesheet" type="text/css" href="{{asset('custom/css/frontend/coWork/css/co-work.css')}}">
<link rel="stylesheet" href="{{asset('custom/css/frontend/searchCoWork/styleOfficedetails.css')}}">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<link href="/assets/global/plugins/select2/css/select2.css" rel="stylesheet" type="text/css"/>
<link href="/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('custom/css/frontend/coWork/bootstrap-datetimepicker.css')}}" rel="stylesheet" type="text/css">

<link href="/assets/global/plugins/magicsuggest/magicsuggest-min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->

<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="/assets/layouts/layout4/css/layout.css" rel="stylesheet" type="text/css" />
<link href="/assets/layouts/layout4/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color" />

<!-- END THEME LAYOUT STYLES -->
<!-- BEGIN HEADER AND FOOTER PAGE STYLES -->
<link href="/custom/css/frontend/headerOrFooter.css" rel="stylesheet" type="text/css" />
<!-- END HEADER AND FOOTER PAGE STYLES -->
<link href="/css/frontend.css" rel="stylesheet" type="text/css" />
<link href="/css/frontend_custom.css" rel="stylesheet" type="text/css" />
<link href="/css/themify-icons.css" rel="stylesheet" type="text/css" />


