<!-- header-banner start -->
<section class="main-header-banner-section" id="MainHeaderSection">
    <div class="container-fluid" style="padding-left:0px;padding-right:0px;">
        <div class="row">
            <div class="col-md-12 banner-img">
                <h1 class="banner-head-text">{{trans('common.start_search_h1')}}</h1>
                <h2 class="banner-head-text">{{trans('common.start_search')}}</h2>
            </div>
        </div>
    </div>
    <div class="container setContainerSearch">
        <div class="row header-banner-second-part">
            <div class="col-md-12">
                <div class="container container-transparent-box searchSection">
                    <div class="set-box-transparent">
                        <div class="add-on set-add-on-select col-md-3">
                            <select class="form-control" id="select-type" name="type" style="width:100%">
                                <option  value="all-spaces">{{trans('common.select_type')}}</option>
                                <option value="hot-desk">{{trans('common.hot_desk')}}</option>
                                <option value="dedicated-desk">{{trans('common.dedicated_desk')}}</option>
                                <option value="meeting-room">{{trans('common.meeting_room')}}</option>
                            </select>
                        </div>
                        <div class="add-on set-add-on col-md-9">
                            <input type="text" class="set-input-box-search searchTxt" placeholder="Search.." name="search2">
                            <button type="submit" class="set-btnbtn-search search">
                                <i class="fa fa-search sicon"></i>
                                <div class="lds-ellipsis sloader" style="display: none"><div></div><div></div><div></div><div></div></div>
                            </button>
                            <img id="searchIcLdr" src="{{asset('sl.gif')}}" alt="" style="width: 3%;position: absolute;top: 37px;right: 39px;display: none;">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- header-banner end -->

<!-- mobile view design -->
<section class="mobile-view-design-head-banner">
    <div class="container image-background-mobile-view-container">
        <h2>Start search for you next Coworking Space</h2>
        <div class="header-search-mobile">
            <img class="search-green-icon" src="{{asset('images/searchIconImageMobileView.png')}}">
            <input type="text" value="" class="form-control searchTxt" placeholder="{{trans('common.where_would_you_like_to_rent')}} ?" />
            <img class="search-filter-icon search" src="{{asset('images/filtericonTollbar.png')}}" data-toggle="modal" data-target="#filterModal">
        </div>
        <br>
        <div class="panel panel-default set-panel-deafult-mobile-view-responsive-head">
            <button class="tablinkHead allSpaces" v-model="type_filter" @click="filterRecord(1)" id="getIDopendefault">All</button>
            <button class="tablinkHead openspaces" v-model="type_filter" @click="filterRecord(2)" id="">Open Spaces</button>
            <button class="tablinkHead privatespaces" v-model="type_filter" @click="filterRecord(3)" id="">Private Offices</button>
            <button class="tablinkHead sharedworkspaces" v-model="type_filter" @click="filterRecord(4)" id="">Shared Workspaces</button>
            <button class="tablinkHead eventspaces" v-model="type_filter">Event Space</button>
        </div>
    </div>
</section>
<!-- mobile view design -->

        <!-- start filter modal -->
        <div id="filterModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header set-modal-header-headbanner">
                        <img data-dismiss="modal" class="set-back-image" src="{{asset('images/backImage.png')}}">
                        <h4 class="modal-title set-modal-head-title" style="color:#5CC22F;">Filter</h4>
                    </div>
                    <div class="modal-body set-modal-body-headbanner">
                        <div class="row">
                            <div class="col-md-12 set-col-md-12-headbanner">
                                <span class="set-span-text-headbanner">{{trans('common.search_country')}}</span>
                                <input type="text" id="" value="" class="searchBarTxt set-searchBar-box form-control" placeholder="{{trans('common.ex')}}" />
                                <img src="{{asset('images/searchBar.png')}}" class="set-searchBar-image">
                            </div>
                        </div>

                        <div class="row set-col-row-search-type">
                            <div class="col-md-12">
                                <span class="set-space-modal-section">{{trans('common.space')}}</span> &nbsp;<span class="set-type-modal-section">{{trans('common.type')}}</span>
                            </div>
                        </div>
                        <div class="row set-col-row-spaces">
                            <div class="col-md-12 set-col-12-spaces-1">
                                <span class="set-spaces-text">All Spaces</span>
                                <label class="set-container-checkbox">
                                    <input type="checkbox" id="1" onchange="selectOption('1',event)" class="checkboxVal">
                                    <span class="set-checkmark-checkbox"></span>
                                </label>
                            </div>
                            <div class="col-md-12 set-col-12-spaces-1">
                                <span class="set-spaces-text">Hot desk</span>
                                <label class="set-container-checkbox">
                                    <input type="checkbox" id="2" onchange="selectOption('2',event)" class="checkboxVal">
                                    <span class="set-checkmark-checkbox"></span>
                                </label>
                            </div>
                            <div class="col-md-12 set-col-12-spaces-1">
                                <span class="set-spaces-text">Dedicated desk</span>
                                <label class="set-container-checkbox">
                                    <input type="checkbox" id="3" onchange="selectOption('3',event)" class="checkboxVal">
                                    <span class="set-checkmark-checkbox"></span>
                                </label>
                            </div>
                            <div class="col-md-12 set-col-12-spaces-1">
                                <span class="set-spaces-text">Meeting Room</span>
                                <label class="set-container-checkbox">
                                    <input type="checkbox" id="4" onchange="selectOption('4',event)" class="checkboxVal">
                                    <span class="set-checkmark-checkbox"></span>
                                </label>
                            </div>
                        </div>

                        <div class="row set-row-col-rating">
                            <div class="col-md-12 set-col-md-12-rating-1">
                                <span class="set-col-rating">{{trans('common.rating')}}</span>
                            </div>
                        </div>

                        <div class="tab">
                            <div class="set-scroller-tabs">
                                <button class="tablinks" onclick="openCity('5',event)">
                                    <span class="set-span-text-all-rating">{{trans('common.all')}}</span>
                                </button>
                                <button class="tablinks set-tab-links" onclick="openCity('1',event)">
                                            <span><i class="fa fa-star set-fa-fa-stars-rating active-fa-stars" aria-hidden="true"></i><i class="fa fa-star set-fa-fa-stars-rating" aria-hidden="true"></i><i class="fa fa-star set-fa-fa-stars-rating" aria-hidden="true"></i><i class="fa fa-star set-fa-fa-stars-rating" aria-hidden="true"></i><i class="fa fa-star set-fa-fa-stars-rating" aria-hidden="true"></i><br>
                                                <span class="set-span-and-up-rating">{{trans('common.and_up')}}</span>
                                            </span>
                                    <div class="vl"></div>
                                </button>
                                <button class="tablinks set-tab-links" onclick="openCity('2',event)">
                                            <span><i class="fa fa-star set-fa-fa-stars-rating active-fa-stars" aria-hidden="true"></i><i class="fa fa-star set-fa-fa-stars-rating active-fa-stars" aria-hidden="true"></i><i class="fa fa-star set-fa-fa-stars-rating" aria-hidden="true"></i><i class="fa fa-star set-fa-fa-stars-rating" aria-hidden="true"></i><i class="fa fa-star set-fa-fa-stars-rating" aria-hidden="true"></i><br>
                                                <span class="set-span-and-up-rating">{{trans('common.and_up')}}</span>
                                            </span>
                                    <div class="vl1"></div>
                                </button>
                                <button class="tablinks set-tab-links" onclick="openCity('3',event)">
                                            <span><i class="fa fa-star set-fa-fa-stars-rating active-fa-stars" aria-hidden="true"></i><i class="fa fa-star set-fa-fa-stars-rating active-fa-stars" aria-hidden="true"></i><i class="fa fa-star set-fa-fa-stars-rating active-fa-stars" aria-hidden="true"></i><i class="fa fa-star set-fa-fa-stars-rating" aria-hidden="true"></i><i class="fa fa-star set-fa-fa-stars-rating" aria-hidden="true"></i><br>
                                                <span class="set-span-and-up-rating">{{trans('common.and_up')}}</span>
                                            </span>
                                    <div class="vl2"></div>
                                </button>
                                <button class="tablinks set-tab-links" onclick="openCity('4',event)">
                                            <span><i class="fa fa-star set-fa-fa-stars-rating active-fa-stars" aria-hidden="true"></i><i class="fa fa-star set-fa-fa-stars-rating active-fa-stars" aria-hidden="true"></i><i class="fa fa-star set-fa-fa-stars-rating active-fa-stars" aria-hidden="true"></i><i class="fa fa-star set-fa-fa-stars-rating active-fa-stars" aria-hidden="true"></i><i class="fa fa-star set-fa-fa-stars-rating" aria-hidden="true"></i><br>
                                                <span class="set-span-and-up-rating">{{trans('common.and_up')}}</span>
                                            </span>
                                    <div class="vl3"></div>
                                </button>
                                <button class="tablinks set-tab-links not-match-border" onclick="openCity('5',event)">
                                    <div class="vl4"></div>
                                    <span><i class="fa fa-star set-fa-fa-stars-rating active-fa-stars" aria-hidden="true"></i><i class="fa fa-star set-fa-fa-stars-rating active-fa-stars" aria-hidden="true"></i><i class="fa fa-star set-fa-fa-stars-rating active-fa-stars" aria-hidden="true"></i><i class="fa fa-star set-fa-fa-stars-rating active-fa-stars" aria-hidden="true"></i><i class="fa fa-star set-fa-fa-stars-rating active-fa-stars" aria-hidden="true"></i><br>
                                            <span class="set-span-and-up-rating">{{trans('common.and_up')}}</span>
                                        </span>
                                    <div class="vl5"></div>
                                </button>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="set-primary-title">{{trans('common.price_Range')}}</h2>
                                <div class="range-slider" id="range">
                                    <span>
                                        <input type="text" id="Value2"  value="$0" class="set-slider-number set-slider-number-1"/>
                                        <input type="text" id="Value3" value="$1000" class="set-slider-number set-slider-number-2" />
                                    </span>
                                    <input value="0" onchange="priceRange('min_val',event)" min="0" max="1000"  class="set-slider-range" type="range"/>
                                    <input value="1000" onchange="priceRange('max_val',event)" min="0" max="1000"  class="set-slider-range" type="range"/>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer set-modal-footer-headbanner">
                        <button class="btn btn-block set-apply-filter-button" id="applyFilterClick">{{trans('common.apply_filter')}}</button>
                    </div>
                </div>

            </div>
        </div>
        <!-- end filter modal -->


@push('js')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
<!-- range slider  -->
<script>
    function getID(val){
        alert(val);
    }
    $('#applyFilterClick').click(function(){
        let country_name;
        let country = $('.searchBarTxt').val();
        let min_price = $('#Value2').val();
        let max_price = $('#Value3').val();
        let star_rating = sessionStorage.getItem('rating_stars');
        let type = sessionStorage.getItem('type');
        let lang = "{{getLocale()}}";
        console.log('check all data comes or not');
        min_price = min_price.replace('$','');
        max_price = max_price.replace('$','');
        console.log(country);
        console.log(min_price);
        console.log(max_price);
        console.log(star_rating);
        console.log(type);
        let url = '/'+lang+'/coworking-spaces/'+country+'?filterType='+type;
        console.log(url);
        window.location.href=url;
    });
    function selectOption(val,event) {
        if(val == 1) {
            sessionStorage.setItem('type','all-spaces');
        } else if(val == 2) {
            sessionStorage.setItem('type','hot-desk');
        } else if(val == 3) {
            sessionStorage.setItem('type','dedicated-desk');
        } else {
            sessionStorage.setItem('type','meeting-room');
        }
        $('.checkboxVal').prop('checked', false);
        console.log(event);
        console.log(event.target.id);
        let id = event.target.id;
        $('#' + id).prop('checked', true);
    }
    function priceRange(val,event) {
        if(val == 'min_val') {
            $('#Value2').val('$'+event.target.value)
        } else {
            $('#Value3').val('$'+event.target.value);
        }
    }
</script>

<script>
	function openCity(stars,evt, cityName) {
	    sessionStorage.setItem('rating_stars',stars);
		var i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("tabcontent");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		// document.getElementById(cityName).style.display = "block";
		evt.currentTarget.className += " active";
	}
</script>
<style>
    .lds-ellipsis {
        display: inline-block;
        position: relative;
        width: 64px;
        height: 64px;
    }
    .lds-ellipsis div {
        position: absolute;
        top: 20px;
        width: 11px;
        height: 11px;
        border-radius: 50%;
        background: #fff;
        animation-timing-function: cubic-bezier(0, 1, 1, 0);
    }
    .lds-ellipsis div:nth-child(1) {
        left: 6px;
        animation: lds-ellipsis1 0.6s infinite;
    }
    .lds-ellipsis div:nth-child(2) {
        left: 6px;
        animation: lds-ellipsis2 0.6s infinite;
    }
    .lds-ellipsis div:nth-child(3) {
        left: 26px;
        animation: lds-ellipsis2 0.6s infinite;
    }
    .lds-ellipsis div:nth-child(4) {
        left: 45px;
        animation: lds-ellipsis3 0.6s infinite;
    }
    @keyframes lds-ellipsis1 {
        0% {
            transform: scale(0);
        }
        100% {
            transform: scale(1);
        }
    }
    @keyframes lds-ellipsis3 {
        0% {
            transform: scale(1);
        }
        100% {
            transform: scale(0);
        }
    }
    @keyframes lds-ellipsis2 {
        0% {
            transform: translate(0, 0);
        }
        100% {
            transform: translate(19px, 0);
        }
    }
    .ui-widget.ui-widget-content {
        top: 585px !important;
    }
</style>
<script>
	$(function () {
		$('.search').on('click', function(){
		    $('.sicon').hide()
		    $('#searchIcLdr').show()
            var link = '/'+window.session.value+'/coworking-spaces/co-work-spaces';
            // alert(link);
			var type = $('#select-type').val();
			var val = $('.searchTxt').val();
			if(val)
            {
                type = type.replace(' ','-');
                window.location.href = `${link}?text=${val}&type=${type}`;
            }
			else{
            }
		});

        $( ".searchTxt" ).on( "autocompleteresponse", function( event, ui ) {
            console.log('responseeeee');
            $('.sloader').hide();
            $('.sicon').show()
        } );
        $( ".searchBarTxt" ).on( "autocompleteresponse", function( event, ui ) {
            console.log('responseeeee');
            $('.sloader').hide();
            $('.sicon').show()
        } );

        function detectMob() {
            const toMatch = [
                /Android/i,
                /webOS/i,
                /iPhone/i,
                /iPad/i,
                /iPod/i,
                /BlackBerry/i,
                /Windows Phone/i
            ];

            return toMatch.some((toMatchItem) => {
                return navigator.userAgent.match(toMatchItem);
            });
        }

        function changeAllAutoComplete() {
            var listMenu = $('.ui-menu.ui-widget.ui-widget-content.ui-autocomplete.ui-front')
            if (detectMob()) {
                $.each(listMenu, function(indexMenu, menu) {
                    var getUlStyle = $(menu).attr('style');
                    if (getUlStyle != '') {
                        splitedUl = getUlStyle.split(';');
                        appendedCss = [];
                        $.each(splitedUl, (index, el) => {
                            if (el.length > 0 && el.indexOf('important') === -1) {
                                console.log(el)
                                appendedCss.push(el + ' !important');
                            }
                        });

                        $(menu).attr('style', appendedCss.join(';'));
                    }
                });
            }
        }

        function hideAllAutoComplete() {
            var listMenu = $('.ui-menu.ui-widget.ui-widget-content.ui-autocomplete.ui-front')
            if (detectMob()) {
                $.each(listMenu, function(indexMenu, menu) {
                    $(menu).hide();
                });
            }
        }

        $(".searchTxt").autocomplete({
			source: function (request, response) {
				jQuery.get("/api/search-shared-office/?lang={{\Session::get('lang')}}", {
					search: request.term
				}, function (data) {
					response(data['data']);
                    changeAllAutoComplete();
				});
			},
			minLength: 1,
            search: function(){
                console.log('search event');
                hideAllAutoComplete();
                $('.sloader').show();
                $('.sicon').hide()
            },
            response: function( event, ui ) {
                console.log('response recieved');
            },
            select: function(event, ui) {
                console.log('data');
                console.log(ui);
                console.log('data');
                if (ui.item.searchByLocation === true) {
                    sessionStorage.setItem("country", ui.item.label);
                    var n = ui.item.label.search(",");
                    if (n === -1) {
                        sessionStorage.setItem("setContent", "<div class='bredcum setMalaysiabredcrum'><a href='#'><p class='p-0 m-0' id='country'><br><span>123</span> <img class='setImagetopUp2 setArrowImage' src='{{asset('custom/css/frontend/coWork/image/arrow.png')}}'></p></a></div>");
                    } else {
                        sessionStorage.setItem("setContent", "<div class='bredcum setMalaysiabredcrum'><a href='#'><p class='p-0 m-0' id='country'><br><span>123</span> <img class='setImagetopUp2 setArrowImage' src='{{asset('custom/css/frontend/coWork/image/arrow.png')}}'></p></a></div><div class='bredcum setSepang'><a href='#'><p class='p-0 m-0'  id='city'><br><span>11</span></p></a></div>");
                    }
                    const route = ui.item.route+'?type='+$('#select-type').val();
                    window.open(route);
                } else {
                    window.open(ui.item.route);
                }
            }
		}).autocomplete("instance")._renderItem = function(ul, item) {
            var item = $('<div class="row">' +
              '<div class="col-md-1 '+item.icon+' "></div>' +
              '<div class="col-md-11 description">' + item.label +'</div>' +
              '</div>')
            return $("<li>").append(item).appendTo(ul);
        };


        $(".searchBarTxt").autocomplete({
			source: function (request, response) {
				jQuery.get("/api/search-shared-office/?lang={{\Session::get('lang')}}", {
					search: request.term
				}, function (data) {
					response(data['data']);
                    changeAllAutoComplete();
				});
			},
			minLength: 1,
            search: function(){
                console.log('search event');
                hideAllAutoComplete();
                $('.sloader').show();
                $('.sicon').hide()
            },
            response: function( event, ui ) {
                console.log('response recieved');
            },
            select: function(event, ui) {
                console.log('data');
                console.log(ui);
                console.log('data');
                if (ui.item.searchByLocation === true) {
                    sessionStorage.setItem("country", ui.item.label);
                    const route = ui.item.route+'?type='+$('#select-type').val();
                    console.log('route route route');
                    console.log(route);
                } else {
                    alert('ui.item.route ui.item.route ui.item.route');
                    alert(ui.item.route);
                }
            }
		}).autocomplete("instance")._renderItem = function(ul, item) {
            var item = $('<div class="row">' +
              '<div class="col-md-1 '+item.icon+' "></div>' +
              '<div class="col-md-11 description">' + item.label +'</div>' +
              '</div>')
            return $("<li>").append(item).appendTo(ul);
        };
	});
</script>
@endpush
