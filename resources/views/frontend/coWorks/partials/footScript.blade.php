
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
{{-- below files use resources thats why we use cdn accord to client 22-01-2020 --}}
{{--<script src="{{asset('js/vue.min.js')}}" type="text/javascript"></script>--}}
{{--<script src="{{asset('js/vue-resource.min.js')}}" type="text/javascript"></script>--}}
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.11"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.5.1/vue-resource.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="{{asset('bootstrapDatePicker/js/bootstrap-datepicker.js')}}" charset="UTF-8"></script>
<script src="/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/select2/js/i18n/zh-CN.js" type="text/javascript"></script>
{{--<script src="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.js')}}"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<!--To fix ajax token mismatch problem -->
<script src="{{asset('custom/css/frontend/coWork/bootstrap-datetimepicker.js')}}"></script>
<script src="/js/ajaxform.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/js/app.js" type="text/javascript"></script>
<script src="/js/global.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
{{--<script src="/assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>--}}
<script src="/assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
<script src="/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<!-- chat module -->
<script src="/custom/js/frontend/myChatPullRequest.js" type="text/javascript"></script>
<script src="/assets/global/plugins/magicsuggest/magicsuggest.js" type="text/javascript"></script>
@if(\Auth::guard('users')->check())
<script src="{{asset('js/pleaseCompleteInfo.js')}}"></script>
@endif
