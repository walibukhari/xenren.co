<div class="row">
    <div class="by-space setby-space1">
        <h2 class="primary-title">{{trans('common.by_space_type')}}</h2>
        <label class="ccontainer">{{trans('common.hot_desk')}}
            <input type="checkbox" id="hot_desk_filter" v-model="type_filter" value="{{\App\Models\SharedOfficeProducts::CATEGORY_HOT_DESK}}"  v-on:change="filerRecord">
            <span class="checkmark"></span>
        </label>
        <label class="ccontainer">{{trans('common.dedicated_desk')}}
            <input type="checkbox" id="dedicated_desk_filter"  v-model="type_filter" value="{{\App\Models\SharedOfficeProducts::CATEGORY_DEDICATED_DESK}}"  v-on:change="filerRecord">
            <span class="checkmark"></span>
        </label>
        <label class="ccontainer">{{trans('common.meeting_room')}}
            <input type="checkbox" id="meeting_room_filter"  v-model="type_filter" value="{{\App\Models\SharedOfficeProducts::CATEGORY_MEETING_ROOM}}"  v-on:change="filerRecord">
            <span class="checkmark"></span>
        </label>
    </div>
    <div class="by-category setCategory1">
        <h2 class="primary-title">{{trans('common.by_continent')}}</h2>
        @php
            $continents = \App\Models\WorldContinents::get();
        @endphp

        @foreach($continents as $k => $v)
            <label class="ccontainer" id="{{isset($location)&&$location==$v->name?str_replace(' ','_',$location):''}}">{{$v->name}}
                <input type="checkbox"  value="{{$v->id}}"  @if($location==$v->name) checked @endif v-model="continent_filter" v-on:change="filerRecord" />
{{--   <input type="checkbox"  value="{{$v->id}}"  @if($location==$v->name) checked @endif />--}}
                <span class="checkmark"></span>
            </label>
        @endforeach
    </div>
    <div class="by-category setCategory2">
        <h2 class="primary-title">{{trans('common.by_rating')}}</h2>
        <div class="first-rating" v-on:click="rate(5)">
            <i class="fa fa-star rated"></i>
            <i class="fa fa-star rated"></i>
            <i class="fa fa-star rated"></i>
            <i class="fa fa-star rated"></i>
            <i class="fa fa-star rated"></i>
        </div>
        <div class="second-rating" v-on:click="rate(4)">
            <i class="fa fa-star rated"></i>
            <i class="fa fa-star rated"></i>
            <i class="fa fa-star rated"></i>
            <i class="fa fa-star rated"></i>
            <i class="fa fa-star"></i>
            {{trans('common.and_up')}}
        </div>
        <div class="third-rating" v-on:click="rate(3)">
            <i class="fa fa-star rated"></i>
            <i class="fa fa-star rated"></i>
            <i class="fa fa-star rated"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            {{trans('common.and_up')}}
        </div>
        <div class="forth-rating" v-on:click="rate(2)">
            <i class="fa fa-star rated"></i>
            <i class="fa fa-star rated"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            {{trans('common.and_up')}}
        </div>
        <div class="fifth-rating" v-on:click="rate(1)">
            <i class="fa fa-star rated"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            {{trans('common.and_up')}}
        </div>
        <div class="fifth-rating" v-on:click="rate(0)">
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            {{trans('common.and_up')}}
        </div>
    </div>
    <div class="by-category setCategory3">
        <h2 class="primary-title">{{trans('common.by_price_range')}}</h2>
        <div class="range-slider" id="range">
            <span>
                <input type="text" v-model="input.min_price" value="$0" class="set-slider-number set-slider-number-1"/>
                <input type="text" v-model="input.max_price" value="$1000" class="set-slider-number set-slider-number-2" />
            </span>
            <input v-on:change="seekFilter($event, true)" value="0" min="0" max="1000"  class="set-slider-range" type="range"/>
            <input v-on:change="seekFilter($event, false)" value="1000" min="0" max="1000"  class="set-slider-range" type="range"/>
        </div>
    </div>
</div>
