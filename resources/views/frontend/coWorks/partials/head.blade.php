<!-- headersecond start -->
<section class="main-second-header">
    <div class="container set-head-main-container">
        <div class="row" style="margin-left:0px;margin-right:0px;">
			<div class="col-md-8 set-row-col-md-10 breadcrumb_inner">
				<ul>
					<li class="liBradCrum">
						<a href="{{url(\Session::get('lang').'/coworking-spaces')}}">
							<p class="p-0 m-0">WORLD <br><span>{{\App\Models\SharedOffice::count()}}</span> </p>
						</a>
					</li>
					@if(isset($continent))
					<li class="liBradCrum">
						<a href="javascript:;"><p class="p-0 m-0">
								<t id="country">{{ strtoupper($continent) }}</t>
								<br><span>{{ $countContinent  }}</span></p>
						</a>
					</li>
					@endif
                    @if(isset($country) && $country != '')
                        <li class="liBradCrum">
                            <a href="javascript:;"><p class="p-0 m-0">
                                    <t id="country">{{ strtoupper($country) }}</t>
                                    <br><span>{{ $countCountry  }}</span></p>
                            </a>
                        </li>
                    @endif
					@if(isset($city) && $city && $city != '')
						<li class="liBradCrum">
							<a href="javascript:;"><p class="p-0 m-0">
									<t id="city">{{ strtoupper($city) }}</t>
									<br><span>{{$countCity}}</span></p>
							</a>
						<li>
					@endif
					@if(isset($officeName))
						<li class="liBradCrum" style="background:none !important;">
							<a href="#"><p class="p-0 m-0">{{ strtoupper($officeName) }}<br><span>1</span></p>
							</a>
						</li>
					@endif
				</ul>
			</div>
			<div class="col-md-4 pull-right set-list-plus">
				<div class="plus-tag-header-right" style="display: flex;align-items: center;justify-content: flex-end;">
                    <a href="#" style="position:relative;right:37px;" data-toggle="modal" id="modals" data-target="#listSpaceModal"><i class="fa fa-plus"></i>{{trans('common.list_spaces')}}</a>
                    @if(\Auth::guard('users')->check())
                        <button onclick="window.location.href='/{{\Session::get('lang')}}/booking/requests'" style="background: transparent;border:0px;color:555;" class="btn btn-success btnSuccesBookingRequests">My Booking</button>
                    @else
                        <button style="background: transparent;border:0px;color:555;" onclick="window.location.href='/{{\Session::get('lang')}}/login'" class="btn btn-success btnSuccesBookingRequests">My Booking</button>
                    @endif
				</div>
        	</div>
		</div>
    </div>
</section>
<!-- headersecond end -->
<!-- list space Modal -->
<div id="listSpaceModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content set-modal-content-list-space">
				<div class="modal-header set-modal-header-list-space">
					<a href="#" class="set0close-icon" data-dismiss="modal"><img src="{{asset('images/cross-icon-image.png')}}"></a>
					<div class="row">
						<div class="col-md-12 text-center set-col-12-md-col-12-modal-list-space">
							<span class="list-you-space-today">{{trans('common.list_space_today')}}</span>
							<br>
							<span class="donot-be-late">{{trans('common.dont_be_late')}}.</span>
						</div>
					</div>
				</div>
			<form id="form-data" v-on:submit.prevent="uploadListForm()" enctype="multipart/form-data">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<input type="text"  name="companyName" v-model="companyName" id="companyName" class="form-control set-company-name-input" placeholder="{{trans('common.company_name')}}">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
{{--								<input type="text" name="location" id="pac-input"  v-model="userLocation" class="form-control set-company-name-input location-input" placeholder="{{trans('common.office_location')}} [{{trans('common.search_by_map')}}] ">		--}}
								<input type="text" name="location" id="pac-input"  class="form-control set-company-name-input location-input" placeholder="{{trans('common.office_location')}} [{{trans('common.search_by_map')}}] ">
								{{--<small id="smalltxt" class="small-txt-search">[{{trans('common.search_by_map')}}]</small>--}}
								<img class="icon-search-image" src="{{asset('images/search.png')}}" v-on:click.prevent="getLocation()">
								<div class="vldivider"></div>
								<img id="crossimages" class="set-cross-image-icon" src="{{asset('images/cross-icon-image.png')}}"> <br/>
								<div id="google-map">
									<div id="listMap"></div>
									{{--<img src="{{asset('images/1243-xxxxxxxxxxx.png')}}">--}}
								</div>
							</div>
						</div>
					</div>

					<br>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group set-form-group-list-space">
								<img class="graphicImage" src="{{asset('images/graphicImage.png')}}">
								<span class="dragdropimages">{{trans('common.upload_image')}}</span>
								<div class="file-input-wrapper">
									<button id="addImage" class="btn-file-input">{{trans('common.add_more')}}
									</button>
									<input type="file" class="newFiles" accept="image/*" id="files" multiple name="files" />
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<input type="text" name="full_name" id="full_name"  v-model="full_name" class="form-control set-company-name-input" placeholder="{{trans('common.full_name')}}">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<input type="text" name="phone_number" id="phone_number" v-model="phone_number"  class="form-control set-company-name-input" placeholder="{{trans('common.phone_number')}}">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<input type="text" name="email" id="email_address" v-model="email_address" class="form-control set-company-name-input" placeholder="{{trans('common.email')}}">
							</div>
						</div>
					</div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="password" name="password" id="password" v-model="password" class="form-control set-company-name-input" placeholder="{{trans('common.password')}}">
                            </div>
                        </div>
                    </div>

				</div>
				<div class="modal-footer set-modal-footer-list-space">
					<button type="submit" class="btn btn-default set-btnbtn-listspace-btn">
						<div v-if="listLoader == false">{{trans('common.list_now')}}</div>
						<div class="lds-ellipsis" v-if="listLoader == true"><div></div><div></div><div></div><div></div></div>
					</button>
				</div>
			</form>
		</div>
	</div>
</div>


<!-- mobile view section header mobile responsive -->
<section class="mobile-view-design-header">
	<div class="container mobile-responsive-container">
		<div class="row add-roww-class">
			<nav class="menu-opener">
				<i class="fa fa-bars" aria-hidden="true"></i>
			</nav>
			<span class="add-arrow-left" style="display: none;position: absolute;top: 9px;color: #fff;font-size: 20px;"></span>
			<nav class="menu">
				@if(\Auth::guard('users')->check())
					<ul class="menu-inner">
						<a href="{{ route('home') }}" class="menu-link">
							<li>{{ trans('common.home')}}</li>
						</a>
						<a href="{{ route('frontend.jobs.index') }}" class="menu-link">
							<li> {{ trans('common.jobs')}}</li>
						</a>
						<a href="{{ route('frontend.findexperts') }}" class="menu-link">
							<li> {{ trans('common.find_experts')}}</li>
						</a>
						<a href="{{ route('frontend.postproject') }}" class="menu-link">
							<li>{{ trans('common.post_project')}}</li>
						</a>
						<a href="{{ route('frontend.usercenter') }}" class="menu-link">
							<li>{{ trans('common.user_center') }}</li>
						</a>
						<a href="{{ route('logout') }}" class="menu-link">
							<li>{{trans('member.logout')}}</li>
						</a>
						<a href="https://www.xenren.co" class="menu-link">
							<li>{{ trans('common.xenren')}}</li>
						</a>
					</ul>
				@else
				<ul class="menu-inner">
					<a href="{{ route('register') }}" class="menu-link">
						<li>{{ trans('member.register') }}</li>
					</a>
					<a href="{{ route('login') }}" class="menu-link">
						<li>{{ trans('member.login') }}</li>
					</a>
					<a href="{{ route('setlang', ['lang' => 'en', 'redirect' => Request::url()]) }}" class="menu-link">
						<li>{{ trans('common.lang_en') }}</li>
					</a>
					<a href="{{ route('setlang', ['lang' => 'cn', 'redirect' => Request::url()]) }}" class="menu-link">
						<li>{{ trans('common.lang_cn') }}</li>
					</a>
					<a href="{{ route('home') }}" class="menu-link">
						<li>{{ trans('common.home')}}</li>
					</a>
					<a href="https://www.xenren.co" class="menu-link">
						<li>{{ trans('common.xenren')}}</li>
					</a>
				</ul>
				@endif
			</nav>

			<h2 class="mobile-view-h2-head">Xenren CO-Work Space</h2>
			<a href="https://xenren.app.link/100Coworkers" class="set0images" id="set0images" target="_blank">
				<img class="set-image-mobile-responsive-view" src="{{asset('images/bellIconImage.png')}}">
			</a>
		</div>
	</div>
</section>
<!-- mobile view section header mobile responsive -->
@push('js')
<script async src="https://maps.google.com/maps/api/js?key=AIzaSyCIasatwSA8nyOeN8Cobzil6yO1jsT13rE&callback=initMap&libraries=geometr‌​y,places,controls">
</script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCIasatwSA8nyOeN8Cobzil6yO1jsT13rE&callback=initListMap&libraries=geometr‌​y,places,controls"></script>
<script src="{{asset('custom/js/ListSpaceMap.js')}}"></script>
<script src="{{asset('custom/js/bs-modal-bootstrap.js')}}"></script>
<script>
    var file_num = 0;

	$(document).ready(function() {
        window.file_num = 0;
        $(document).on('click', 'span.imgRemove', function() {
            console.log('please decrement coutner now');
            window.file_num = window.file_num-1;
            if(window.file_num==0)
            {
                $('.dragdropimages').show();
                $('.graphicImage').show();
            }
            console.log(window.file_num);
        });

		if (window.File && window.FileList && window.FileReader) {
			$("#files").on("change", function(e) {
                var files = e.target.files;
				var filesLength = files.length;
                console.log('length of files uploaded');
                console.log(files.length);
                console.log('length of previous fiels uploaded');
                console.log(window.file_num);
                // let totalNumberOfFiles = window.file_num + files.length;
                console.log('length of all fiels uploaded now');
                console.log(window.file_num);

                if(window.file_num+filesLength <= 6 )  {
                    window.file_num = window.file_num+filesLength;
                    var a = [];
					for (var i = 0; i < filesLength; i++) {
						var f = files[i]
						var b = a.push(f)
						var fileReader = new FileReader();
						fileReader.onload = (function(e) {
							$('.graphicImage').hide();
							$('.dragdropimages').hide();
							var file = e.target;
							$("<span class=\"pip\">" +
									"<img class=\"imageThumb\" style=\"width:100px;height:70px;object-fit: cover;\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
									"<br/><span class=\"remove set-remove imgRemove \"><img src=\"/images/cross-image-list-space.png\"></span>" +
									"</span>").insertAfter("#files");
							$(".remove").click(function(){
								var length = $(this).length;
								$(this).parent(".pip").remove();
								if(length === null) {
									$('.graphicImage').show();
									$('.dragdropimages').show();
								}
							});
						});
						sessionStorage.setItem('length',a);
						fileReader.readAsDataURL(f);
					}
				}
                else {
                    toastr.error('{{trans('validation.images_limit')}}');
                }
			});
		} else {
			alert("Your browser doesn't support to File API")
		}
	});


	function getID(val) {
		if(val === '1')
		{
			$('allSpaces').addClass('SpaceActive');
		} else if(val === '2') {
			$('openspaces').addClass('SpaceActive');
		} else if(val === '3') {
			$('privatespaces').addClass('SpaceActive');
		} else if(val === '4') {
			$('sharedworkspaces').addClass('SpaceActive');
		} else if(val === '5') {
			$('eventspaces').addClass('SpaceActive');
		}
	}

	/* click got ot city search and country search and both */
		$('#country').click(function(){
			var session = window.session.value;
			var country = $(this).text().toLowerCase();
			console.log(country);
			console.log(session);
			console.log("/api/search-shared-office/?lang={{\Session::get('lang')}}&search="+country);
			var url = "/api/search-shared-office/?lang={{\Session::get('lang')}}&search="+country;
			if(country) {
				$.ajax({
					url: url,
					Type: 'GET',

					success: function (response) {
						console.log(response.data[0].route);
						window.location.href = response.data[0].route;
					}
				});
			}
		});

		$('#city').click(function() {
			var session = window.session.value;
			var city = $(this).text().toLowerCase();
			var country = $('#country').text().toLowerCase();
			console.log(city);
			console.log(country);
			console.log(session);
			console.log("/api/search-shared-office/?lang="+session+"&search="+city+','+country);
			var url = "/api/search-shared-office/?lang="+session+"&search="+city+','+country;
			console.log(url);
			if(city) {
				$.ajax({
					url: url,
					Type: 'GET',

					success: function (response) {
						console.log(response.data[0].route);
						window.location.href = response.data[0].route;
					}
				});
			}
		});
	/* click got ot city search and country search and both */
</script>
<script>
	$('#output').empty();
	$('#output').hide();
	var loadFile = function(event) {
		$('#output').show();
		var output = document.getElementById('output');
		$('.graphicImage').css('display','none');
		$('.dragdropimages').css('display','none');
		output.src = URL.createObjectURL(event.target.files[0]);
	};
	$(".menu-opener").click(function(){
		$(".menu-opener, .menu-opener-inner, .menu").toggleClass("active");
	});
	$('.modal').on('show.bs.modal', function () {
		$('.settopHeds').addClass('showZIndex');
	});
	$('.modal').on('hidden.bs.modal', function () {
		$('.settopHeds').removeClass('showZIndex');
	});

	$('#crossimages').click(function(){
		document.getElementById("pac-input").value = '';
	});
</script>
@endpush
