@extends("frontend.coWorks.layouts.default")
@section('title')
    {{trans('common.coworking_spaces_title')}}
@endsection
@section('header')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{asset('custom/css/frontend/coWork/login.css')}}" rel="stylesheet" type="text/css">
    <style>
        .spinner:focus{
            background: transparent;
            border: 0px !important;
        }
        .spinner {
            position: absolute;
            left: 0;
            top: 0px;
            width: 100%;
        }
        .lds-ring {
            display: inline-block;
            position: relative;
            width: 40px;
            height: 40px;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 28px;
            height: 28px;
            margin: 8px;
            border: 4px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

    </style>
@endsection
@section("content")
    <div class="row set-main-row-login-coWorkSpace">
        <div class=" multi-languages">
            <a class="defaultImage" href="javascript:;"><img id="setimg" src="{{ trans('common.flg')}}"></a>
            <ul class="select-languages" style="display: none;">
                <a href="{{ route('setlang', ['lang' => 'en', 'redirect' => url('')]) }}">
                    <li class="img1">
                        <img id="eng" style="height: 20px;" src="{{asset('/images/admin-english-language.png')}}"><span>{{ trans('common.lang_en') }}</span>
                    </li>
                </a>
                <a href="{{ route('setlang', ['lang' => 'cn', 'redirect' => url('')]) }}">
                    <li class="img2">
                        <img id="chn" style="height: 20px;" src="{{asset('/images/admin-chinese-language.png')}}"><span>{{ trans('common.lang_cn') }}</span>
                    </li>
                </a>
            </ul>
        </div>
        <div class="col-md-12 set-padding-login-col-md-6-2-left">
            <div class="col-md-6 set-col-md-6-paragraph-small-text">
                <p class="set-login-paragraph">{{trans('common.management')}}</p>
                <small class="set-small-text-image">{{trans('common.tech')}}</small>
            </div>
            <div class="col-md-6 pull-right set-padding-col-md-6-2login-cowork-space">
                <div class=" col-md-12">
                    <div class=" col-md-12 coWorkSpace-login-logo">
                    <img src="{{asset('custom/css/frontend/coWork/image/xenrenLogoLoginPageCoWork.png')}}">
                    <p class="set-logo-xenren-name">XenRen</p>
                    <small class="set-small-logo-xenren-name">{{trans('common.worker')}} & {{trans('common.space')}}</small>
                    <br>
                    </div>
                </div>
                <form class="set-form-login-coWorkspace" id="login-form" action="{{route('staff.login.post')}}" method="post">
                    <div class="row">
                        <div class="col-md-12 set-col-md-12-login-cowork-space">
                            <div class="col-md-6 pull-left set-col-md-6-login-coworkSpace">
                                @if(\Session::has('error'))
                                    <div class="alert alert-danger">
                                        <span> {{ \Session::get('error') }} </span>
                                    </div>
                                @endif
                            <div class="form-group set-form-group-email-margin-bottom">
                                <label class="set-login-email-label-coWork">{{trans('common.email')}}</label>
                                <input type="text" name="login" class="form-control set-login-email-input">
                            </div>
                        </div>
                        </div>

                        <div class="col-md-12 set-col-md-12-login-cowork-space">
                        <div class="col-md-6 pull-left set-col-md-6-login-coworkSpace">
                            <div class="form-group">
                                <label class="set-login-password-label-coWork">{{trans('common.password')}}</label>
                                <input type="password" name="password" class="form-control set-login-password-input">
                            </div>
                        </div>
                        </div>

                        <div class="col-md-12 set-col-md-12-login-cowork-space">
                        <div class="col-md-6 pull-left set-col-md-6-login-coworkSpace">
                            <div class="form-group set-fonts-rememberMe">
                                <label class="set-container-checkbox-login-cowork">
                                    <input type="checkbox" checked="checked" name="remember" value="1"> {{trans('common.remember_me')}}
                                    <span class="checkmark-login-coworkspace"></span>
                                </label>
                            </div>
                            <div class="form-group set-fonts-forgotPassword">
                                    <span onclick="window.location.href='{{url('/en/coworking-spaces/reset-password')}}'">
                                            {{trans('common.forget_pass')}}?
                                    </span>
                            </div>
                        </div>
                        </div>

                        <div class="col-md-12 set-col-md-12-login-cowork-space">
                        <div class="col-md-6 pull-left set-col-md-6-login-coworkSpace">
                            <div class="form-group set-margin-botton-singin-CowOrkSpace">
                                <img class="set-login-coWork-image" src="{{asset('custom/css/frontend/coWork/image/buttonimagesharedoffice.png')}}">
                                <button type="submit" id="submiForm" style="height:32px;padding-left:8px" class="set-sign-in-image-coWork btn green">
                                   <span id="textt">{{trans('common.sign_in')}}</span>
                                   <div class="spinner">
                                       <div class="lds-ring" style="display: none;"><div></div><div></div><div></div><div></div></div>
                                   </div>
                                </button>
                            </div>
                        </div>
                        </div>

                        <div class="col-md-12 set-col-md-12-login-cowork-space">
                        <div class="col-md-6 pull-left set-col-md-6-login-coworkSpace">
                            <div class="form-group set-width-form-group-sign-up">
                                <p class="set-coWork-login-donthave-account">{{trans('common.not_account')}}?</p><br>
                                <p class="set-coWork-login-sign-up-if-no-account" onclick="window.location.href='{{url('/en/coworking-spaces/sign-up')}}'">{{trans('common.sign_up')}}</p>
                            </div>
                        </div>
                        </div>


                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('#submiForm').click(function () {
            $('.lds-ring').show();
            $('#textt').hide();
        });
        $('.defaultImage').hover(function(){
            $('.select-languages').show();
        });
        $(document).click(function(){
            $('.select-languages').hide();
        });
        $('.img1').click(function(){
            var src = $('#eng').attr('src');
            var th = $('.defaultImage').html('');
            th.append('<img src="/images/admin-english-language.png">');
            $('.select-languages').hide();
        });
        $('.img2').click(function(){
            var src = $('#chn').attr('src');
            var th = $('.defaultImage').html('');
            th.append('<img src="/images/admin-chinese-language.png">');
            $('.select-languages').hide();
        });
    </script>
@endsection
