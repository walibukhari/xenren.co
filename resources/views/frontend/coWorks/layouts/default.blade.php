<!DOCTYPE html>
<!--[if IE 8]> <html lang="{{ trans('common.lang') }}" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="{{ trans('common.lang') }}" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="{{ trans('common.lang') }}">
@php
    $session = session()->get('lang');
    $url1 = url('/'.$session.'/coworking-spaces/sign-in');
    $url2 = url('/'.$session.'/coworking-spaces/sign-up');
    $url3 = url('/'.$session.'/coworking-spaces/reset-password');
    $url4 = url('/'.$session.'/coworking-spaces/set-password');
    $url5 = url('/'.$session.'/booking/requests');
    $url6 = url('/'.$session.'/booking/detail');
    $url41 = str_contains($url4,'set-password');
    $url = url()->current();
    $url = explode('/en/',$url);
    $url = explode('detail/',$url[1]);
    $url = $url[0].'detail';
@endphp
<!--<![endif]-->
<html>
<head>
    <title>@yield('title')</title>
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta property="og:title" content="OFFICE SPACE FOR RENT - XENREN.CO" />
    <meta name="description" content="@yield('searchResult') Coworking Spaces. Are you looking for best Coworking Spaces? Read real reviews, Real pictures of offices and find your perfect Coworking Spaces."/>
    <meta property="og:description" content="Coworking Spaces. Are you looking for best Coworking Space? Read real reviews, Real pictures of offices and find your perfect Coworking Spaces." />
    <meta name="msvalidate.01" content="DAC0A9F3A3973E8BE8A5EB7FA07899EE" />
    <meta property="wb:webmaster" content="4bfa78a5221bb48a" />
    <meta property="qc:admins" content="2552431241756375" />
    <meta property="og:url" content="@yield('url')" />
    <meta property="og:image" content="@yield('officeImage')">
    <meta property="og:image:type" content="image/jpg" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="630" />
    <meta property="og:image:alt" content="Office Space banner image" />
    <meta property="fb:app_id" content="2093040524080185" />
    @include('frontend.coWorks.partials.headScripts')
{{--    <link rel="canonical" href="https://coworkspace.xenren.co/en/coworking-spaces"/>--}}
    <link rel="alternate" href="https://coworkspace.xenren.co/cn/coworking-spaces" hreflang="zh" />
    <link rel="alternate" href="https://coworkspace.xenren.co/en/coworking-spaces" hreflang="en" />
    <link href="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('custom/css/frontend/coWork/sharedofficeHeader.css')}}" rel="stylesheet" type="text/css">
    <link href="/custom/css/frontend/homePagePopup.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
    <link href="{{asset('images/Favicon.jpg')}}" rel="icon" type="image/x-icon" />
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+SC&display=swap&subset=chinese-simplified" rel="stylesheet">
    {{--<link href="{{asset('xenren-assets/images/logo/fav_new.png')}}" rel="icon" type="image/x-icon">--}}
@section('header') @show

    <style>
        .newFiles {
            display: block;
        }
        .imageThumb {
            cursor: pointer;
            border-radius:3px !important;
        }
        .pip {
            display: inline-block;
            margin: 0px 0px 0 13px;
            float: left;
        }
        .set-remove {
            display: inline-block;
            text-align: center;
            cursor: pointer;
            position: relative;
            top: -68px;
            right: -38px;
        }
        #pac-input {
            background-color: #fff !important;
            font-size: 15px;
            font-weight: 300;
            /*margin-left: 12px;*/
            padding: 0 65px 0 13px !important;
            text-overflow: ellipsis !important;
        }
        #pac-input:focus {
            border-color: #4d90fe !important;
        }
        .showZIndex{
            z-index: -1 !important;
        }

        .set-box{
            width:100% !important;
        }

        .change-status-content input:checked + div .box {
            border-color: #41ae3d;
            color: #41ae3d !important;
            border-radius: 5px;
        }
        .change-status-content .box {
            border-radius: 3px;
            border: 1px solid #b5b5b5;
            height: 155px;
        }
        .set-reload-page-show-content{
            position: absolute;
            top: 144px;
            z-index: 999;
            width: 100%;
            padding-right: 20px;
            padding-left: 20px;
            margin: 0px !important;
            text-align: -webkit-center;
            text-align: -moz-center;
            display: none;
            background: #fff;
        }
        .set-row-margin-show-content-page-reload{
            margin:0px !important;
            background-color: #fbe1e3;
            border-color: #fbe1e3;
            color: #e73d4a;
            padding: 20px;
        }
        .set-row-margin-show-content-page-reload > a{
            cursor: pointer;
            text-decoration: none;
            padding-right: 10px;
        }
        /* loader css */
        .lds-ellipsis {
            display: inline-block;
            position: relative;
            width: 0px !important;
            bottom: 17px;
            right: 28px;
            height: 13px !important;
        }
        .lds-ellipsis div {
            position: absolute;
            top: 33px;
            width: 13px;
            height: 13px;
            border-radius: 50%;
            background: #fff;
            animation-timing-function: cubic-bezier(0, 1, 1, 0);
        }
        .lds-ellipsis div:nth-child(1) {
            left: 8px;
            animation: lds-ellipsis1 0.6s infinite;
        }
        .lds-ellipsis div:nth-child(2) {
            left: 8px;
            animation: lds-ellipsis2 0.6s infinite;
        }
        .lds-ellipsis div:nth-child(3) {
            left: 32px;
            animation: lds-ellipsis2 0.6s infinite;
        }
        .lds-ellipsis div:nth-child(4) {
            left: 56px;
            animation: lds-ellipsis3 0.6s infinite;
        }
        @keyframes lds-ellipsis1 {
            0% {
                transform: scale(0);
            }
            100% {
                transform: scale(1);
            }
        }
        @keyframes lds-ellipsis3 {
            0% {
                transform: scale(1);
            }
            100% {
                transform: scale(0);
            }
        }
        @keyframes lds-ellipsis2 {
            0% {
                transform: translate(0, 0);
            }
            100% {
                transform: translate(24px, 0);
            }
        }
        .btnSuccesBookingRequests{
            background-color: #57Af2A;
            border: 0px;
            color:#fff;
            border-radius:3px;
            right: 20px;
            font-weight: bold;
            position: relative;
            top: 10px;
            font-size: 10px;
        }
        /* loader css */
        @if(url()->current() == $url1 || url()->current() == $url2 || url()->current() == $url3 || url()->current() == $url4 || url()->current() == $url5 || $url == 'booking/detail' || $url41)
        @else
        [v-cloak] > * { display:none }
        [v-cloak]::before {
            content: " ";
            display: flex;
            width: 100px;
            height: 100px;
            background-size: cover;
            text-align: center;
            background-image: url('/images/searchResult.png.gif');
            margin: 0 auto;
            display: block;
        }
        @endif
    </style>
    <script src="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.js')}}"></script>
</head>
@if(\Auth::guard('users')->check())
    <body onload="checkCookie()">
@else
    <body>
@endif
<div class="web-view-header-include">
    @include('frontend.includes.header')
</div>

@if(\Auth::user() && (\Auth::user()->name == '' || is_null(\Auth::user()->name) || \Auth::user()->about_me == '' || is_null(\Auth::user()->about_me)))
<div class="container set-reload-page-show-content" id="set-reload-page-content">
    <div class="row set-row-margin-show-content-page-reload">
        <div class="col-md-12">
            please complete your profile information by clicking
            <a href="/personalInformation">here</a>
        </div>
    </div>
</div>
@endif

<div class="main-section" id="app" v-cloak style="margin-top: 75px; background-color: #fff;">
    @if(url()->current() == $url1 || url()->current() == $url2)
    @else
        @include('frontend.coWorks.partials.head')
    @endif
    <div id="modal-app" class="uk-container uk-container-center uk-margin-top">
        <modal class="showModal">
        @include('frontend.coWorks.partials.headBanner')
        </modal>
    </div>
    @yield("content")

</div>
@include('frontend.userCenter.modalChangeUserStatus')
@include('frontend.coWorks.partials.footer')
@include('frontend.coWorks.partials.footScript')
@yield('modal')
@include('frontend.includes.cookiesModal')

@if(\Auth::guard('users')->check())
    <script>
        localStorage.setItem('username',"{{\Auth::user()->id}}");
    </script>
    <script src="/js/cookies.js" type="text/javascript"></script >
@endif
<script>
    $('#click-scroll-to-top').click(function(e){
        $('html, body').animate({scrollTop:0}, '400');
    });
</script>
@yield('script')
@stack('js')
</body>
</html>
