@if($OwnerSettings != '' && !is_null($OwnerSettings) && $OwnerSettings->status_book_type == \App\Models\SharedOfficeSetting::BOOK_STATUS_PAY_NOW)
<!-- pay modal -->
<div id="paymentModal" class="modal fade" role="dialog">
    <div class="modal-dialog" id="modalDialoge">
        <!-- Modal content-->
        <div class="modal-content" id="modalContent">
            <div class="modal-header" id="modalHeader">
                <a data-dismiss="modal" class="set-book-space-cross-image"><img id="closemodal" src="http://127.0.0.1:8000/images/crossImage.png"></a>
                <h4 class="modal-title" id="modalTitle">Quick Checkout</h4>
            </div>
            <div class="modal-body" id="modalBody">
                <form action="{{route('paypal.topup-payment')}}" method="POST">
                    <input type="hidden" name="payment_method" id="selected_payment_method">
                    <input type="hidden" name="type" value="">
                    <p class="pp">Amount</p>
                    <input type="text" id="notStripe" name="amount" required class="roww form-control" placeholder="Topup Amount" />
                    <input type="text" id="stripeAmount" style="display: none;" v-model="topupamount" class="form-control" placeholder="{{trans('common.amount')}}" @keyup="calculatePrice" value="0">
                    <p class="pp">Available payment options</p>
                    <br>
                    @php
                        $user = \Auth::user();
                    @endphp
                    @if($user != '' && \App\Services\GeneralService::payPalAvailableCountries(strtolower(\Auth::user()->country_name)))
                        <div class="row roww paymentMethodOptions">
                            <div class="col-md-6">
                                <img style="width:20px;" src="{{asset('/images/paylogo/paypal.png')}}">
                                <span class="spanText">Paypal</span>
                            </div>
                            <div class="col-md-6">
                                <label class="containerPayCheckBox">
                                    <input type="checkbox" class="paymentMethod" @change="paymentMethod($event,'paypal')" value="Paypal" data-type="Paypal" id="pp">
                                    <span class="checkmarkPay"></span>
                                </label>
                            </div>
                        </div>
                        <br>
                    @endif
                    <div class="row roww">
                        <div class="col-md-6">
                            <img style="width:20px" src="{{asset('/images/paylogo/bank.png')}}">
                            <span class="spanText">Bank</span>
                        </div>
                        <div class="col-md-6">
                            <label class="containerPayCheckBox">
                                <input type="checkbox" class="paymentMethod" @change="paymentMethod($event,'bank')" value="Bank" data-type="Bank" id="bank">
                                <span class="checkmarkPay"></span>
                            </label>
                        </div>
                    </div>
                    <br>
                    <div class="row roww">
                        <div class="col-md-6">
                            <img style="width:20px" src="{{asset('/images/paylogo/ali.png')}}">
                            <span class="spanText">Ali Pay</span>
                        </div>
                        <div class="col-md-6">
                            <label class="containerPayCheckBox">
                                <input type="checkbox" class="paymentMethod" @change="paymentMethod($event,'ali_pay')" value="Ali Pay" data-type="Ali Pay" id="ap">
                                <span class="checkmarkPay"></span>
                            </label>
                        </div>
                    </div>
                    <br>
                    <div class="row roww">
                        <div class="col-md-6">
                            <img style="width:20px;" src="{{asset('/images/paylogo/wechat.png')}}">
                            <span class="spanText">Wechat Pay</span>
                        </div>
                        <div class="col-md-6">
                            <label class="containerPayCheckBox">
                                <input type="checkbox" class="paymentMethod" @change="paymentMethod($event,'wechat_pay')" value="Wechat Pay" data-type="Wechat Pay" id="wc">
                                <span class="checkmarkPay"></span>
                            </label>
                        </div>
                    </div>
                    <br>
                    @if($available_country['is_available_country'] == true)
                        <div class="row roww">
                            <div class="col-md-9">
                                <img style="width:20px;" src="{{asset('/images/rm.png')}}">
                                <span class="spanText">Grab Pay | Boost | Wechat Pay</span>
                            </div>
                            <div class="col-md-3">
                                <label class="containerPayCheckBox">
                                    <input type="checkbox" class="paymentMethod" @change="paymentMethod($event,'revenue_monster')" value="Revenue Monster" data-type="Revenue Monster" id="rm">
                                    <span class="checkmarkPay"></span>
                                </label>
                            </div>
                        </div>
                        <br>
                    @endif
                @if($user != '' && \App\Services\GeneralService::stripeAvailableCountries(ucfirst(\Auth::user()->country_name)))
                    <div class="row roww">
                        <div class="col-md-9">
                            <i class="fa fa-cc-stripe right-icon setFAFA" style="color:#6672E5;" aria-hidden="true"></i>
                            <span class="spanText">Credit card | Debit Card</span>
                        </div>
                        <div class="col-md-3">
                            <label class="containerPayCheckBox">
                                <input type="checkbox" class="paymentMethod" @change="paymentMethod($event,'stripe')" value="stripe" data-type="Stripe" id="st">
                                <span class="checkmarkPay"></span>
                            </label>
                        </div>
                    </div>
                                        @endif
                    <br>
                    <br>
                    <div class="row roww">
                        <div class="col-md-12">
                            <button id="btnCustom" type="submit" class="btn btn-default btn-Green" style="height: 50px;">top up</button>
                            <button id="customButtonStripe" @click.prevent="stripeTopUp" type="button" style="display: none;height: 50px;" class="btn btn-default btn-Green">top up stripe</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- pay modal -->
@endif

<!-- give review Modal -->
<div id="myReviewPlease" class="modal fade" role="dialog">
    <div class="modal-dialog set-my-review-pleas-modal-dailogue">
        <!-- Modal content-->
        <form id="form_review">
            <input type="hidden" :value="getOfficeDetails.id" id="officeId" name="office_id">
            <div class="modal-content set-review-modal-content">
                <div class="modal-header set-modal-header-my-review-please">
                    <a href="#" class="set0close-icon" data-dismiss="modal">
                        <i class="fa fa-times"></i>
                        {{--                                <img src="{{asset('/images/cross-icon-image.png')}}">--}}
                    </a>
                    <p class="modal-title set-write-a-review-title">{{trans('common.write_review')}}</p>
                    <small class="set-small-less-minutes">{{trans('common.it_takes_minute')}}.</small>
                </div>
                <div class="modal-body set-modal-body-review-please">
                    <p class="set-share-a-review">{{trans('common.hi')}} {{isset(\Auth::user()->name) ? \Auth::user()->name : ''}},{{trans('common.please_share')}}.
                    </p>
                    <!-- Rating Stars Box -->
                    <section class='rating-widget' v-if="sharedoffice.rate > 0">
                        <div class='rating-stars text-center' v-if="sharedoffice.rate == 1">
                            <ul id='stars' onclick="myStars('a')">
                                <li  class='star selected'  @click="myReview('1')" value="1" title='Poor' data-value='1'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star' @click="myReview('2')" title='Fair' data-value='2'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star' @click="myReview('3')" title='Good' data-value='3'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star' @click="myReview('4')" title='Excellent' data-value='4'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star' @click="myReview('5')" title='WOW!!!' data-value='5'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                            </ul>
                        </div>

                        <div class='rating-stars text-center' v-if="sharedoffice.rate == 2">
                            <ul id='stars' onclick="myStars('b')">
                                <li  class='star selected' @click="myReview('1')" value="2"  title='Poor' data-value='1'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star selected' @click="myReview('2')"  title='Fair' data-value='2'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star' @click="myReview('3')"  title='Good' data-value='3'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star' @click="myReview('4')"  title='Excellent' data-value='4'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star' @click="myReview('5')"  title='WOW!!!' data-value='5'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                            </ul>
                        </div>

                        <div class='rating-stars text-center' v-if="sharedoffice.rate == 3">
                            <ul id='stars' onclick="myStars('c')">
                                <li  class='star selected' @click="myReview('1')" value="3"  title='Poor' data-value='1'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star selected' @click="myReview('2')"  title='Fair' data-value='2'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star selected' @click="myReview('3')"  title='Good' data-value='3'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star' @click="myReview('4')"  title='Excellent' data-value='4'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star' @click="myReview('5')"  title='WOW!!!' data-value='5'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                            </ul>
                        </div>

                        <div class='rating-stars text-center'   v-if="sharedoffice.rate == 4">
                            <ul id='stars' onclick="myStars('d')">
                                <li  class='star selected' @click="myReview('1')" value="4" title='Poor' data-value='1'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star selected' @click="myReview('2')" title='Fair' data-value='2'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star selected' @click="myReview('3')" title='Good' data-value='3'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star selected' @click="myReview('4')" title='Excellent' data-value='4'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star' @click="myReview('5')"  title='WOW!!!' data-value='5'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                            </ul>
                        </div>

                        <div class='rating-stars text-center' v-if="sharedoffice.rate == 5">
                            <ul id='stars' onclick="myStars('e')">
                                <li  class='star selected' @click="myReview('1')" value="5" title='Poor' data-value='1'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star selected' @click="myReview('2')"  title='Fair' data-value='2'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star selected' @click="myReview('3')"  title='Good' data-value='3'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star selected' @click="myReview('4')"  title='Excellent' data-value='4'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star selected' @click="myReview('5')"  title='WOW!!!' data-value='5'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                            </ul>
                        </div>

                    </section>
                    <section v-else>
                        <div class='rating-stars text-center'>
                            <ul id='stars' onclick="myStars('f')">
                                <li  class='star' @click="myReview('1')" title='Poor' data-value='1'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star' @click="myReview('2')" title='Fair' data-value='2'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star' @click="myReview('3')" title='Good' data-value='3'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star' @click="myReview('4')" title='Excellent' data-value='4'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li  class='star' @click="myReview('5')" title='WOW!!!' data-value='5'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                            </ul>
                        </div>
                    </section>
                    <textarea class="set-text-area-review" id="commentReview" name="commentReview" placeholder="{{trans('common.you_review')}}">@{{ sharedoffice.comment }}</textarea>
                </div>
                <div class="modal-footer set-modal-footer-my-review-please">
                    <button type="button" @click="submitReviewForm()" class="btn btn-default set-btnbtn-default-review">{{trans('common.submit_review')}}</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- give review Modal -->

<div id="thanksPopup" class="modal fade" role="dialog">
    <div class="modal-dialog set-my-review-pleas-modal-dailogue">
        <div class="modal-content set-thanks-moda-content">
            <div class="modal-header set-modal-header-my-review-please">
                <a href="#" class="set0close-icon" data-dismiss="modal">
                    <i class="fa fa-times"></i>
                </a>
            </div>
            <div class="modal-body thanks-modal-body">
                <img class="thanks-image" src="{{asset('images/hand thanks.png')}}">
                <br>
                <p class="thanks-review">{{trans('common.thanks_review')}}</p>
                <small class="small-success">{{trans('common.success_story')}}</small>
            </div>
        </div>
    </div>
</div>

<div id="notUsedPlace" class="modal" role="dialog">
    <div class="modal-dialog set-my-review-pleas-modal-dailogue">
        <div class="modal-content set-notUsedPlace-moda-content">
            <div class="modal-header set-modal-header-my-review-please">
                <a href="#" class="set0close-icon" data-dismiss="modal">
                    <i class="fa fa-times"></i>
                </a>
            </div>
            <div class="modal-body notUsedPlace-modal-body">
                <img class="notUsedPlace-image" src="{{asset('images/signupPlease.png')}}">

                <p class="notUsedPlaceHaven">{{trans('common.not_used_place')}}</p>
                {{--                    <button class="btnbtn-SignUp">Sign Up</button>--}}
                {{--                    <h3 class="now">Now!</h3>--}}
            </div>
        </div>
    </div>
</div>

<div id="signInUser" class="modal fade" role="dialog">
    <div class="modal-dialog set-my-review-pleas-modal-dailogue">
        <div class="modal-content set-notUsedPlace-moda-content">
            <div class="modal-header set-modal-header-my-review-please">
                <h2 class="pleaseLoginIn">Please Login</h2>
                <a href="#" class="set0close-icon" data-dismiss="modal">
                    <i class="fa fa-times"></i>
                </a>
            </div>
            <div class="modal-body notUsedPlace-modal-body">
                <form method="POST" action="{{ route('login.post') }}">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group set-custom-width">

                                <label class="labelEmail">{{trans('common.email_address')}}</label>
                                {{--                                    <img class="setEUImage" src="{{asset('images/newUser.png')}}">--}}
                                <i class="fa fa-envelope setEUImage" aria-hidden="true"></i>
                                <input type="text" name="phone_or_email" id="p_email" placeholder="Enter Email Address" class="form-control setEmailBox" value="{{ old('phone_or_email') }}">
                            </div>
                        </div>

                        <div class="col-md-12 passwordBottom">
                            <div class="form-group set-custom-width">
                                <label class="labelEmail">{{trans('common.password')}}</label>
                                <img class="setPUImage" src="{{asset('images/newLock.png')}}">
                                <input type="password" name="password" placeholder="Enter Password" id="pas" class="form-control setPassBox" value="">
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="button" class="btnbtn-Login" id="submit-login">Login</button>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group set-form-group-hr">
                                <hr class="hr-set">Or<hr class="hr-set">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <button class="btnbtn-Loginfb">
                                    <a href="{{ route('facebook.login') }}">
                                        <i class="fa fa-facebook-f"></i>
                                    </a>
                                </button>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group" style="margin:0px">
                                <h2 class="sign-up">Sign Up</h2>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-body">
                <div class="col-md-12">
                    <h3><center>{{trans('common.send_us_message')}}</center></h3>
                    <div><center>{{trans('common.if_you_are')}}</center><br/></div>
                </div>
                <form action="#">
                    <div class="col-md-12">
                        <div class="col-md-6 set-col-md-6-modal-dp">
                            <div class="form-group">
                                @if(\Auth::guard('users')->check())
                                    <input type="text" class="form-control set-contact-name-details" value="{{\Auth::user()->name}}" id="name"  placeholder="{{trans('common.your_name')}}">
                                @else
                                    <input type="text" class="form-control set-contact-name-details" id="name"  placeholder="{{trans('common.your_name')}}">
                                @endif
                                <span class="fa fa-user form-control-feedback set-fafauser-dp" style="margin-right: 14px; margin-top: 10px "></span>
                            </div>
                        </div>
                        <div class="col-md-6 set-col-md-6-2-modal-dp">
                            <div class="form-group">
                                @if(\Auth::guard('users')->check())
                                    <input type="email" class="form-control" id="email" value="{{\Auth::user()->email}}"  placeholder="{{trans('common.your_email')}}">
                                @else
                                    <input type="email" class="form-control" id="email"  placeholder="{{trans('common.your_email')}}">
                                @endif
                                <span class="fa fa-envelope form-control-feedback set-fafaenvolope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" class="form-control" v-model="contact_subject" placeholder="{{trans('common.subject')}}">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" id="message" v-model="contact_message" placeholder="{{trans('common.messages')}}"></textarea>
                        </div>
                        <button type="button" @click="submitForm()" class="btn btn-success set-btn-succes-dp">{{trans('common.submit')}}</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer" style="border: none">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('common.close')}}</button>
            </div>
        </div>

    </div>
</div>
<!-- end message modal -->


<!-- book your space today modal -->
<div id="myBookSpaceModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content set-book-space-modal-content">
            <div class="modal-header set-modal-head-book-space-today">
                <a href="#" class="set-book-space-cross-image" data-dismiss="modal"><img id="closemodal" src="{{asset('images/crossImage.png')}}"></a>
                <center><div class="set-head-text-book-space-1">{{trans('common.book_your_space_today')}}</div></center>
                <center><div class="set-head-text-book-space-2">{{trans('common.be_smart')}}.</div></center>
            </div>
            <form class="form" id="book_form">
                <input type="hidden" :value="getOfficeDetails.contact_email" id="too" name="to">
                <input type="hidden" :value="getOfficeDetails.office_manager" id="office_manager" name="office_manager">
                <input type="hidden" value="{{$office_id}}" id="idd" name="office_id">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12 set-col-book-space-modal">
                                <input type="text" name="full_name" value="{{(auth()->user()!=null)?auth()->user()->name:''}}" id="f_name"  class="form-control set-box-border-radius set-input-box-1" placeholder="{{trans('common.full_name')}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12 set-col-book-space-modal">
                                <input type="text" name="phone_no"  value="{{(auth()->user()!=null)?auth()->user()->handphone_no:''}}" id="p_number" class="form-control set-box-border-radius set-input-box-2" placeholder="{{trans('common.phone_no')}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12 set-col-book-space-modal">
                                <img src="{{asset('images/dateTimePic.png')}}" class="set-date-time-bbo-space-modal-datetimeimg">
                                <input type="text"  placeholder="{{trans('common.check_in_date')}}" name="check_in_date" class="form-control set-box-border-radius set-box-select-2" id="datepicker"><img src="{{asset('images/arrowDownDD.png')}}" class="set-arrow-down-img">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12 set-col-book-space-modal">
                                <img src="{{asset('images/dateTimePic.png')}}" class="set-date-time-bbo-space-modal-datetimeimg">
                                <input type="text"  placeholder="{{trans('common.check_out_date')}}" name="check_out_date" class="form-control set-box-border-radius set-box-select-2" id="datepicker1"><img src="{{asset('images/arrowDownDD.png')}}" class="set-arrow-down-img">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12 set-col-book-space-modal">
                                <select v-model="category_id" name="category_id" id="category_id" class="form-control set-box-border-radius set-input-box-2">
                                    <option value="" disabled selected>Please Choose...</option>
                                    @if(count($facilities) > 0)
                                    @foreach ($facilities as $f)
                                        <option value="{{ $f['id'] }}">{{ $f['name'] }}</option>
                                    @endforeach
                                    @else
                                        <option>office against no products</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6 set-col-md-6-book-spaces-rooms-1">
                                <input type="text" v-model="no_of_rooms" placeholder="{{trans('common.no_of_rooms')}}" name="no_of_rooms" class="form-control set-box-border-radius set-select-rooms-1">
                            </div>
                            <div class="col-md-6 set-col-md-6-book-spaces-rooms-2">
                                <input type="text" v-model="people_strength" placeholder="{{trans('common.no_of_peoples')}}" name="no_of_peoples" class="form-control set-box-border-radius set-select-rooms-2">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12 set-col-book-space-modal">
                                <textarea placeholder="{{trans('common.remarks')}}" v-model="remarks_please" name="remarks" class="form-control set-box-border-radius set-text-area-book-now"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer set-modal-footer-book-spaces">
                    @if(count($facilities) > 0)
                        <button type="button" id="bookBtn" @click="bookNowMessage()" class="btn btn-default setBookNowBtn">
                            <div v-model="loaderBooking" v-if="loaderBooking" class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
                            <span v-if="!loaderBooking">{{trans('common.book_now')}}</span>
                        </button>
                    @else
                        <button type="button" disabled class="btn btn-default setBookNowBtn">
                            {{trans('common.book_now')}}
                        </button>
                    @endif
                </div>
            </form>
        </div>

    </div>
</div>


<!-- Reserve space modal -->
<div id="myReserveSpace" class="modal fade" role="dialog">
    <div class="modal-dialog">
        @{{ office_type }}
        <!-- Modal content-->
        <div class="modal-content set-book-space-modal-content">
            <div class="modal-header set-modal-head-book-space-today">
                <a href="#" class="set-book-space-cross-image" data-dismiss="modal"><img id="closemodal" src="{{asset('images/crossImage.png')}}"></a>
                <center><div class="set-head-text-book-space-1">{{trans('common.reserve_your_space_today')}}</div></center>
                <center><div class="set-head-text-book-space-2">{{trans('common.be_smart_reserve')}}.</div></center>
            </div>
            <form class="form" id="reserve-form">
                <input type="hidden" :value="getOfficeDetails.contact_email" id="to" name="to">
                <input type="hidden" :value="getOfficeDetails.office_manager" id="office_manager_email" name="office_manager">
                <input type="hidden" :value="getOfficeDetails.id" id="id" name="office_id">
                <input type="hidden" :value="office_type" name="office_type">
                <input type="hidden" :value="durationType" name="duration_type" id="duration_type">
                <input type="hidden" :value="price" name="price">

                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12 set-col-book-space-modal">
                                <input type="text" name="full_name" value="{{(auth()->user()!=null)?auth()->user()->name:''}}" id="first_name"  class="form-control set-box-border-radius set-input-box-1" placeholder="{{trans('common.full_name')}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12 set-col-book-space-modal">
                                <input type="text" name="phone_no"  value="{{(auth()->user()!=null)?auth()->user()->handphone_no:''}}" id="Phone_number" class="form-control set-box-border-radius set-input-box-2" placeholder="{{trans('common.phone_no')}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12 set-col-book-space-modal">
                                <img src="{{asset('images/dateTimePic.png')}}" class="set-date-time-bbo-space-modal-datetimeimg">
                                <input type="text"  placeholder="{{trans('common.check_in_date')}}" name="check_in_date" id="form_datetime" class="form-control set-box-border-radius set-box-select-2"><img src="{{asset('images/arrowDownDD.png')}}" class="set-arrow-down-img">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12 set-col-book-space-modal">
                                <img src="{{asset('images/dateTimePic.png')}}" class="set-date-time-bbo-space-modal-datetimeimg">
                                <input type="text"  placeholder="{{trans('common.check_out_date')}}" name="check_out_date" id="form_datetime2" class="form-control set-box-border-radius set-box-select-2"><img src="{{asset('images/arrowDownDD.png')}}" class="set-arrow-down-img">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6 set-col-md-6-book-spaces-rooms-1">
                                <input type="text" v-model="noRooms" placeholder="{{trans('common.no_of_seats')}}" name="no_of_rooms" class="form-control set-box-border-radius set-select-rooms-1">
                            </div>
                            <div class="col-md-6 set-col-md-6-book-spaces-rooms-2">
                                <input type="text" :value="peoples" id="noOfPeoples" placeholder="{{trans('common.no_of_peoples')}}" name="no_of_peoples" class="form-control set-box-border-radius set-select-rooms-2">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12 set-col-book-space-modal">
                                <textarea placeholder="{{trans('common.remarks')}}" v-model="remarksPlease" name="remarks" class="form-control set-box-border-radius set-text-area-book-now"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer set-modal-footer-book-spaces">
                    <button type="button" id="ReserveBtn" @click="ReserveRequest()" class="btn btn-default setBookNowBtn">{{trans('common.reserve_now')}}</button>
                </div>
            </form>
        </div>

    </div>
</div>
<!-- Reserve space modal -->
