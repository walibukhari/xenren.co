@extends('frontend.coWorks.layouts.default')
@section('header')
    <style>
        .newest-spaces-box{
            display: none;
        }
        .preload{
            position: relative;
            top:50%;
            text-align: center;
            background-color: #fff;
            height: 100px;
            width: 100%;
        }
        .preload > img{
            text-align: center;
            display: inline-block;
            text-align: -webkit-center;
            width: 50px;
        }

        .carousel-cell {
            width: 100%;
            height: 120px;
            margin-right: 10px;
            background: #8C8;
            border-radius: 5px;
            counter-increment: carousel-cell;
        }

        /* cell number */
        .carousel-cell:before {
            display: block;
            text-align: center;
            /*content: counter(carousel-cell);*/
            line-height: 120px;
            font-size: 80px;
            color: white;
        }
        .flickity-prev-next-button.next {
            right: 10px;
            display: none;
        }
        .flickity-prev-next-button.previous {
            left: 10px;
            display: none;
        }
        .flickity-page-dots {
            position: absolute;
            width: 100%;
            bottom: 0px !important;
            padding: 0;
            margin: 0;
            list-style: none;
            text-align: center;
            line-height: 1;
        }
        .flickity-page-dots .dot {
            display: inline-block;
            width: 7px !important;
            height: 7px !important;
            margin: 2px !important;
            background: #333;
            border-radius: 50%;
            opacity: .25;
            cursor: pointer;
        }

    </style>
@endsection
@section("content")
<section class="mobile-design-newest-spaces-available">
    <div class="preload">
        <img src="https://i.imgur.com/KUJoe.gif">
    </div>
    <div class="row m-0">
        <div class="col-xs-12">
            <div class="newest-spaces-box" v-for="(offices,index) in sharedOffices" @click="getSharedOfficeByID(offices, offices.id)">
                {{--<ul class="bxslider">--}}
                    {{--<li>--}}
                        {{--<img class="newest-spaces-image" :src="'/'+offices.image">--}}
                    {{--</li>--}}
                    {{--<li v-for="images in offices.office_images">--}}
                        {{--<img class="newest-spaces-image" :src="'/'+images.image">--}}
                    {{--</li>--}}
                {{--</ul>--}}
                <div class="main-carousel">
                    <div class="carousel-cell">
                        <img class="newest-spaces-image" :src="'/'+offices.image">
                    </div>
                    <div class="carousel-cell" v-for="images in offices.office_images">
                        <img class="newest-spaces-image" :src="'/'+images.image">
                    </div>
                </div>
                <div class="newest-spaces-inner-box">
                    <div class="left-side">
                        <h2 class="newest-space-office-name">@{{ getOfficeNameSTR(offices) }}</h2>
                        <p class="newest-space-office-location">
                            <img src="{{asset('images/location-image.png')}}">
                            @{{ getsharedOfficeLocationSTR(offices) }}
                        </p>
                        <p class="newest-space-office-time">@{{ getDate(offices) }}</p>
                    </div>
                    <div class="right-side">
                        <h3 class="newest-space-office-price">$@{{ offices.min_seat_price }}/Month</h3>
                        <div class="newest-space-office-rating">
                            <ul  v-if="getSharedOfficeRatingMV(offices) == 1">
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                <li>
                                    (@{{ getSharedOfficeRatingMV(offices) }}) reviews
                                </li>
                            </ul>
                            <ul  v-else-if="getSharedOfficeRatingMV(offices) == 2">
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                <li>
                                    (@{{ getSharedOfficeRatingMV(offices) }}) reviews
                                </li>
                            </ul>
                            <ul  v-else-if="getSharedOfficeRatingMV(offices) == 3">
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                <li>
                                    (@{{ getSharedOfficeRatingMV(offices) }}) reviews
                                </li>
                            </ul>
                            <ul  v-else-if="getSharedOfficeRatingMV(offices) == 4">
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                            </ul>
                            <ul  v-else-if="getSharedOfficeRatingMV(offices) == 5">
                                <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                <li>
                                    (@{{ getSharedOfficeRatingMV(offices) }}) reviews
                                </li>
                            </ul>
                            <ul  v-else>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li>
                                    (@{{ getSharedOfficeRatingMV(offices) }}) reviews
                                </li>
                            </ul>
                        </div>
                        <ul class="newest-space-office-facilities">
                            <li v-for="facilities in offices.shfacilities">
                                <img :src="facilities.image" />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@include('frontend.coWorks.scripts.newestCoworkSpaces')