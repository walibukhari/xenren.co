@extends('frontend.coWorks.layouts.default')
@section('title')
    Shared Office Booking Requests
@endsection
@section('url')
    {{url()->current()}}
@endsection

@section('header')
    <style>
        #MainHeaderSection{
            display: none;
        }
        .main-second-header{
            display: none;
        }
        html{
            background: #fff;
        }
        .footer-btn-section {
            background-color: #fff !important;
            position: absolute;
            bottom: 0px;
            width: 100%;
        }
        .footer-btn-section-title{
            display: none;
        }
        .setColFooter{
            display: none;
        }
        .setDDMLIST{
            overflow: scroll;
        }
        .imageSet{
            width: 100%;
            height: 200px;
        }
        .setRowMargin{
            margin: 0px !important;
        }
        .officeName{
            font-size: 20px;
            font-weight: bold;
        }
        .footer-btn-section {
            background-color: #fff !important;
            position:unset;
            bottom: 0px;
            width: 100%;
        }
        .officeDescription{

        }
        .addColorViewMap{
            color:#57a224;
            font-weight: bold;
            text-decoration: underline;
        }
        .addColorViewMap:hover{
            color:#57a224;
            font-weight: bold;
            text-decoration: underline;
        }
        .setStylePhone{
            width: 50px;
            height: 50px;
            background: #efefef;
            padding: 14px;
            border-radius: 23px;
        }
        .setStyleSeat{
            width: 50px;
            height: 50px;
            background: #efefef;
            padding: 14px;
            border-radius: 23px;
        }
        .setContactProperty{
            font-weight: bold;
            font-size: 20px;
            color:#57a224;
            padding-left: 20px;
        }
        .setDisplay1{
            display: flex;
            align-items: center;
            margin-bottom: 10px;
        }
        .setDisplay2{
            display: flex;
            align-items: center;
        }
        .setSeatNumber{
            font-weight: bold;
            font-size: 16px;
            color:#555;
            padding-left: 20px;
        }
        .setBgColor{
            position: relative;
            left:10px;
            background: #efefef;
            border-radius: 15px;
            padding: 5px 15px 5px 15px;
        }
        .displayRow0{
            padding-bottom: 15px;
        }
        .displayRow1{
            border-top: 1px solid #efefef;
        }
        .displayRow2{
            border-top: 1px solid #efefef;
        }
        .setDisplay3{
            display: flex;
            align-items: center;
        }
        .setDisplay31{
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .setDisplay4{
            display: flex;
            align-items: center;
            padding: 20px;
        }
        .calenderRow{
            width: 50px;
            height: 50px;
            background: #efefef;
            padding: 10px;
            border-radius: 23px;
        }
        .setCheckIn{
            display: flex;
            flex-direction: column;
            justify-content: center;
            padding-left: 12px;
        }
        .setRightArrow{
            width: 20px;
        }
        .setDisplayArrow{
            position: relative;
            top: 10px;
            height: 45px;
            display: flex;
            align-items: center;
        }
        .setDisplay2Main{
            padding: 20px;
            padding-left: 0px;
            border-right: 1px solid #efefef;
        }
        .setDisplayRowMain2{
            padding: 20px;
            border-right: 1px solid #efefef;
            padding-left: 0px;
        }
        .setRowMain1{
            padding: 20px;
        }
        .setRoomImage{
            width: 50px;
            height: 50px;
            background: #efefef;
            padding: 10px;
            border-radius: 23px;
        }
        .setRoomStyle{

        }
        .setDisplay41{
            display: flex;
            flex-direction: column;
            padding-left: 12px;
        }
        .setFreelancerStyle{
            width: 50px;
            height: 50px;
            background: #efefef;
            padding: 10px;
            border-radius: 23px;
        }
        .setDisplay5{
            display: flex;
            align-items: center;
        }
        .setDisplay51{
            display: flex;
            flex-direction: column;
            padding-left: 12px;
        }
        .setDisplay6{
            display: flex;
            align-items: center;
            padding: 20px;
        }
        .setStylefacility{
            font-size: 15px;
            padding-left:12px;
            font-weight: bold;
        }
        .setDisplay61{
            display: flex;
            flex-direction: column;
        }
        .setYes{
            padding-left:12px;
            text-align: center;
        }
        .setRowMain0{
            padding: 20px;
        }
        .image-background-mobile-view-container{
            display: none;
        }
        @media (max-width:991px) {
            .displayRow0{
                display: flex;
            }
            .setResCol8{
                width: 70%;
            }
            .setResCol4{
                width: 30%;
            }


            /* ------------------------ */
            .displayRow1{
                display: flex;
            }
            .setColM8displayRow1{
                width: 70%;
            }
            .setDisplay2Main{
                display: flex;
            }
            .setCol5Display2Main{
                width: 40%;
            }
            .setCol2Display21Main{
                width: 20%;
            }
            .displayRow2{
                display: flex;
            }
            .setCol8displayRow2Reso{
                width: 70%;
            }
        }

        @media (max-width: 767px){
            .setStylePhone {
                width: 30px;
                height: 30px;
                background: #efefef;
                padding: 6px;
                border-radius: 23px;
            }
            .calenderRow{
                width: 30px;
                height: 30px;
                background: #efefef;
                padding: 6px;
                border-radius: 23px;
            }
            .setRoomImage{
                width: 30px;
                height: 30px;
                background: #efefef;
                padding: 6px;
                border-radius: 23px;
            }
            .setFreelancerStyle{
                width: 30px;
                height: 30px;
                background: #efefef;
                padding: 6px;
                border-radius: 23px;
            }
            .setCheckIn {
                display: flex;
                flex-direction: column;
                justify-content: center;
                padding-left: 12px;
                font-size: 10px;
            }
            .setDisplayArrow {
                position: relative;
                top: 0px;
                height: 35px;
                display: flex;
                align-items: center;
            }
            .setDisplay41 {
                display: flex;
                flex-direction: column;
                padding-left: 12px;
                font-size: 10px;
            }
            .setDisplay51 {
                display: flex;
                flex-direction: column;
                padding-left: 12px;
                font-size: 10px;
            }
            .setDisplay61 {
                display: flex;
                flex-direction: column;
                font-size: 10px;
            }
            .setStylefacility {
                font-size: 10px;
                padding-left: 12px;
                font-weight: bold;
            }
            .setStyleSeat{
                width: 30px;
                height: 30px;
                background: #efefef;
                padding: 6px;
                border-radius: 23px;
            }
            .setSeatNumber {
                font-weight: bold;
                font-size: 10px;
                color: #555;
                padding-left: 20px;
            }
            .setBgColor {
                position: relative;
                left: 10px;
                background: #efefef;
                border-radius: 15px;
                padding: 5px 15px 5px 15px;
                font-size: 10px;
            }
            .officeDescription {
                font-size: 10px;
            }
            .officeName {
                font-size: 18px;
                font-weight: bold;
            }
            .setContactProperty {
                font-weight: bold;
                font-size: 10px;
                color: #57a224;
                padding-left: 20px;
            }
        }
        @media (max-width: 557px) {
            .setSeatNumber {
                font-weight: bold;
                font-size: 10px;
                color: #555;
                padding-left: 12px;
            }
            .setContactProperty {
                font-weight: bold;
                font-size: 10px;
                color: #57a224;
                padding-left: 12px;
            }
            .setResCol4 {
                width: 50%;
                display: flex;
                justify-content: flex-end;
            }
            .setResCol8 {
                width: 50%;
            }
            .setRightArrow {
                width: 13px;
            }
            .setDisplay2Main {
                padding: 20px;
                padding-left: 0px;
                border-right: 1px solid #efefef;
                padding-right: 0px;
            }
            .setCol8displayRow2Reso {
                width: 50%;
            }
            .setCol4displayRow2Reso{
                width: 50%;
            }
            .setColM4displayRow1{
                width: 30%;
            }
            .setDisplay4 {
                display: flex;
                align-items: center;
                padding: 20px;
                padding-left: 0px;
            }
            .setRowMain1{
                padding: 0px;
            }
            .setRowMain0{
                padding: 0px;
            }
            .setColM8displayRow1{
                padding: 0px;
            }
            .setCol5Display2Main{
                padding: 0px;
            }
            .setCol5Display22Main{
                padding: 0px;
            }
            .setResCol8 {
                width: 50%;
                padding: 0px;
            }
            .setResCol4 {
                width: 50%;
                display: flex;
                justify-content: flex-end;
                padding: 0px;
            }
            .setDisplay6 {
                display: flex;
                align-items: center;
                padding: 20px;
                padding-left: 0px;
            }
            .setCol8displayRow2Reso {
                width: 70%;
                padding: 0px;
            }
            .setDisplayRowMain12{
                padding-left: 0px;
            }
            .setCol4displayRow2Reso {
                width: 30%;
            }
            .setBgColor {
                position: relative;
                left: 4px;
                background: #efefef;
                border-radius: 15px;
                padding: 3px 11px 3px 11px;
                font-size: 10px;
            }
            .setCol2Display21Main {
                width: 15%;
                padding-right: 0px;
                padding-left: 10px;
            }
        }
        .alertSuccess{
            padding: 7px;
            text-align: center;
            margin-top: 10px;
            font-size: 12px;
            margin-bottom: 10px;
        }
        .displayRow4{
            padding-bottom: 12px;
            display: flex;
            justify-content: space-between;
        }
        .infoBtn{
            background: #54616c;
            color:#fff;
            font-size: 10px;
            width: 97%;
            text-shadow: none;
        }
        .successBtn{
            width: 100%;
            background: #57a224;
            color:#fff;
            font-size: 10px;
            text-shadow: none;
        }
        .btnSuccess{
            display: flex;
            justify-content: flex-start;
            width: 50%;
        }
        .btnInfo{
            display: flex;
            justify-content: flex-end;
            width: 50%;
        }
        .officeDescription2{
            background: #ffffff;
            box-shadow: 0px 1px 6px -3px;
            padding: 10px;
            font-size: 12px;
            padding-bottom: 60px;
        }
        .officeName2{
            padding: 10px;
            font-weight: bold;
            font-size: 18px;
            margin-bottom: 0px;
        }
    </style>
@endsection
@section("content")
    <div class="setMainContainer" id="bookingRequests">
        <div class="container setMainContainer">
            <h3 style="color:#57AF2A">Booking Details</h3>
        </div>
        <div class="row setRowMargin">
            <img src="/{{$booking_detail->office->image}}" class="imageSet" />
        </div>
        <br>
        <div class="row displayRow0">
            <div class="col-md-8 setResCol8">
               <div class="setRowMain0">
                   <p class="officeName">{{$booking_detail->office->office_name}}</p>
                   <p class="officeDescription">{{str_limit($booking_detail->office->description,'100','-')}}
                       <a href="" class="addColorViewMap">View on map</a></p>
               </div>
            </div>
            <div class="col-md-4 setResCol4">
                <div class="setRowMain1">
                    <div class="setDisplay1">
                        <img src="{{asset('images/phone.png')}}" class="setStylePhone" />
                        <span class="setContactProperty">Contact Property</span>
                    </div>
                    <div class="setDisplay2">
                        <img src="{{asset('images/seatNo.png')}}" class="setStyleSeat" />
                        <span class="setSeatNumber">Seat No .</span><span class="setBgColor">{{isset($booking_detail->products) ? $booking_detail->products->number : '--'}}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row displayRow1">
            <div class="col-md-8 setColM8displayRow1">
               <div class="row setDisplay2Main">
                   <div class="col-md-5 setCol5Display2Main">
                       <div class="setDisplay3">
                           <img src="{{asset('images/calender.png')}}" class="calenderRow">
                           <span class="setCheckIn">
                            <span>Check In</span>
                            <span>{{$booking_detail->check_in}}</span>
                        </span>
                       </div>
                   </div>
                   <div class="col-md-2 setCol2Display21Main">
                       <div class="setDisplayArrow">
                           <img class="setRightArrow" src="{{asset('images/rightArrow.png')}}" >
                       </div>
                   </div>
                   <div class="col-md-5 setCol5Display22Main">
                       <div class="setDisplay31">
                           <img src="{{asset('images/calender.png')}}" class="calenderRow">
                           <span class="setCheckIn">
                            <span>Check Out</span>
                            <span>{{$booking_detail->check_out}}</span>
                        </span>
                       </div>
                   </div>
               </div>
            </div>
            <div class="col-md-4 setColM4displayRow1">
                <div class="setDisplay4">
                    <img src="{{asset('images/room.png')}}" class="setRoomImage" />
                    <span class="setDisplay41">
                        <span class="setRoomStyle">
                            Room
                        </span>
                        <span>
                            @if(isset($booking_detail->products))
                                {{$booking_detail->getRoomData($booking_detail->products->category_id)}}
                            @endif
                        </span>
                    </span>
                </div>
            </div>
        </div>
        <div class="row displayRow2">
            <div class="col-md-8 setCol8displayRow2Reso">
                <div class="row setDisplayRowMain2">
                    <div class="col-md-12 setDisplayRowMain12">
                        <div class="setDisplay5">
                            <img src="{{asset('images/freelancer.png')}}" class="setFreelancerStyle" />
                            <span class="setDisplay51">
                                <span class="free1">
                                    Freelancer
                                </span>
                                <span class="free2">
                                    {{$booking_detail->getTotalFreelancer()}}
                                </span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 setCol4displayRow2Reso">
                <div class="setDisplay6">
                    <img src="{{asset('images/freelancer.png')}}" class="setFreelancerStyle" />
{{--                    @foreach($booking_detail->office->shfacilities as $facility)--}}
                        <span class="setDisplay61">
                            <span class="setStylefacility">Wifi</span>
                            <span class="setYes">Yes</span>
                        </span>
{{--                    @endforeach--}}
                </div>
            </div>
        </div>
        <div class="row displayRow3">
            <div class="alert alert-success alertSuccess">
                {{$booking_detail->getStatusName($booking_detail->status)}}
            </div>
        </div>
        <div class="row displayRow4">
            <div class="btnSuccess">
                <button class="btn btn-default successBtn">Review your Booking</button>
            </div>
            <div class="btnInfo">
                <button class="btn btn-default infoBtn">Get Booking Information</button>
            </div>
        </div>
        <div class="row displayRow5">
            <p class="officeName2">{{$booking_detail->office->office_name}}</p>
            <p class="officeDescription2">{{$booking_detail->office->description}}</p>
        </div>
    </div>

    <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.11"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.5.1/vue-resource.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script>
        new Vue({
            el: '#bookingRequests',
            data: {
                clientIp: '{{\Request::getClientIp(true)}}',
                seat_no:'',
                id:'',
                remarks:'',
                office_name:''
            },
            methods: {
                openCancelBox: function (id,seat_no) {
                    this.seat_no = seat_no;
                    this.id = id;
                },
                openDetailBox: function (id,office_name,remarks) {
                    this.office_name = office_name;
                    this.remarks = remarks;
                },
                confirmCancel: function () {
                    let link = '/booking/request/cancel/'+this.id;
                    this.$http.get(link).then((response) => {
                        console.log('response');
                        console.log(response.body.status);
                        if(response.body.status == 'success') {
                            toastr.success(response.body.message);
                            setTimeout(function () {
                                window.location.reload();
                            },2000);
                        }
                    }).catch((error) => {
                        console.log('error');
                        console.log(error);
                    })
                }
            },

            mounted(){
                console.log('booking requests details');
            }
        });
    </script>
    <script>
        $('.scroll-div').slimScroll({});
    </script>
@endsection

