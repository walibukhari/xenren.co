@extends('frontend.coWorks.layouts.default')
@section('title')
    Shared Office Booking Requests
@endsection
@section('url')
    {{url()->current()}}
@endsection

@section('header')
    <style>
        /*#footer{*/
        /*    position: absolute;*/
        /*    width: 100%;*/
        /*    bottom: 0;*/
        /*}*/
        #MainHeaderSection{
            display: none;
        }
        .main-second-header{
            display: none;
        }
        html{
            background: #fff;
        }
        .footer-btn-section {
            background-color: #fff !important;
            position: absolute;
            bottom: 0px;
            width: 100%;
        }
        .footer-btn-section-title{
            display: none;
        }
        .setColFooter{
            display: none;
        }
        .setDDMLIST{
            overflow: scroll;
        }
        .imageSettings{
            width: 140px;
            height: 180px;
            border-radius: 10px;
            border: 1px solid #EFEFEF;
        }
        .setCustomRowSet{
            border: 1px solid #EFEFEF;
            border-radius: 10px;
            height:210px;
            margin-top: 20px;
            display: flex;
            align-items: center;
            flex-direction: row;
        }
        .footer-btn-section {
            background-color: #fff !important;
            position:unset;
            bottom: 0px;
            width: 100%;
        }
        .setCustomRow3{
            width: 12%;
        }
        .setCustomRow7{
            width: 70%;
            height: 185px;
        }
        .officeName{
            font-size: 20px;
            font-weight: bold;
            padding-top: 10px;
            margin-bottom: 3px;
        }
        .seatImage{
            width: 20px;
            height: 18px;
        }
        .seatImageBg{
            background: #efefef;
            padding: 10px;
            width: 40px;
            border-radius: 40px;
            display: inline-block;
        }
        .setDisplay{
            display: flex;
            align-items: center;
            border-bottom: 1px solid #efefef;
            padding-bottom: 12px;
        }
        .seatNo{
            padding-left: 12px;
            font-size: 14px;
            font-weight: 600;
            color: #555;
        }
        .dates{
            font-size: 14px;
            font-weight: 600;
            color: #555;
        }
        .setPara{
            margin:0px;
        }
        .setNumber{
            background: #efefef;
            padding:  5px 8px 5px 8px;
            border-radius: 10px;
            font-size: 10px;
            margin-left: 10px;
        }
        .setDisplay2{
            display: flex;
            align-items: center;
            padding-top: 12px;
            padding-bottom: 12px;
        }
        .setCalender{

        }
        .setDisplayInside{
            padding-left: 12px;
            display: flex;
            width: 100%;
            align-items: center;
        }
        .section-1{
            width: 30%;
        }
        .section-2{
            width: 20%;
        }
        .section-3{
            width: 50%;
        }
        .setRightArrow{
            width: 20px;
        }
        .setCustomRow2{
            height: 185px;
            display: flex;
            flex-direction: column;
            justify-content: flex-end;
            border-left: 1px solid #efefef;
        }
        .btnGrayColor{
            background: #54616c;
            color: #fff !important;
            margin-bottom: 6px;
            font-weight: bold !important;
            text-shadow:unset !important;
            border-radius: 8px;
            border: 0px;
        }
        .btnGrayColor:hover{
            background: #54616c;
            color: #fff !important;
            margin-bottom: 6px;
            text-shadow:unset !important;
        }
        .btnGreenColor{
            background: #57a224;
            color: #fff;
            font-weight: bold !important;
            border-radius: 8px;
            border: 0px;
        }
        .btnGreenColor:hover{
            background: #57a224;
            color: #fff;
        }
        .setAlertMessage{
            padding: 7px;
            text-align: center;
            font-size: 11px;
        }

        @media (max-width: 1200px) {
            .setCustomRow3 {
                width: unset;
            }
        }

        @media(max-width: 676px) {
            .section-1 {
                width: 50%;
            }
            .dates {
                font-size: 12px;
                font-weight: 600;
                color: #555;
            }
            .setCalender {
                font-size: 12px;
            }
            .seatImageBg {
                background: #efefef;
                padding: 6px;
                width: 33px;
                border-radius: 40px;
                display: inline-block;
            }
            .seatImage {
                width: 17px;
                height: 17px;
            }
            .seatNo {
                padding-left: 12px;
                font-size: 12px;
                font-weight: 600;
                color: #555;
            }

            .setCustomRow3{
                width: unset;
            }
        }

        @media (max-width: 554px) {
            .setCustomRow7 {
                width: 70%;
                height: 185px;
                padding-left: 0px;
            }
            .imageSettings {
                width: 90px;
                height: 180px;
                border-radius: 10px;
            }
        }

        @media (max-width: 497px){
            .section-1 {
                width: 57%;
            }
            .setCustomRow2 {
                height: 185px;
                display: flex;
                flex-direction: column;
                align-items: flex-end;
                justify-content: flex-end;
                border-left: 1px solid #efefef;
                padding: 6px;
            }
            .setRightArrow {
                width: 13px;
            }
            .setCustomRow3 {
                padding-right: 8px;
            }
            .dates {
                font-size: 10px;
                font-weight: 600;
                color: #555;
            }
            .setCalender {
                font-size: 10px;
            }
            .setAlertMessage {
                padding: 7px;
                text-align: center;
                font-size: 10px;
            }
        }
        @media (max-width: 450px) {
            .imageSettings {
                width: 77px;
                height: 155px;
                border-radius: 10px;
            }
            .setDisplay {
                display: flex;
                align-items: center;
                border-bottom: 1px solid #efefef;
                padding-bottom: 7px;
            }
            .setDisplay2 {
                display: flex;
                align-items: center;
                padding-top: 7px;
                padding-bottom: 7px;
            }
            .setCustomRow2 {
                height: 155px;
                display: flex;
                flex-direction: column;
                align-items: flex-end;
                justify-content: flex-end;
                border-left: 1px solid #efefef;
                padding: 6px;
            }
            .setCustomRowSet {
                border: 1px solid #EFEFEF;
                border-radius: 10px;
                height: 185px;
                margin-top: 20px;
                display: flex;
                align-items: center;
                flex-direction: row;
            }
            .setMainContainer {
                width: 100%;
                padding-left: 0px;
                padding-right: 0px;
            }
        }

        @media (max-width: 403px) {
            .setCustomRow7 {
                width: 70%;
                height: 185px;
                padding-left: 0px;
                padding-right: 6px;
            }
        }

        .image-background-mobile-view-container{
            display: none;
        }
        .set-date-time-bbo-space-modal-datetimeimg {
            position: absolute;
            top: 7px;
            left:unset !important;
            right: 35px;
        }
        .setChangeBookingRequest{
            display: flex;
            align-items: center;
        }
        .set-box-select-2{
            height: 37px;
            color: #707070 !important;
            border: 1px solid lightgray;
            padding-left: 25px;
            border-radius: 6px !important;
        }
        .setModalTitleChangeBookingRequest{
            flex:1;
            font-weight: bold;
        }
        #modalFooter-changeBooking{
            display: flex;
            justify-content: space-around;
        }
        #btnbtnChangeBookinggray{
            background: #d5d5d5;
            color: #555;
            border: 0px;
            width: 35%;
            padding: 12px;
            border-radius: 20px;
            font-weight: bold;
        }
        #btnbtnChangeBookingGreen{
            background: #4eb800;
            border-radius: 20px;
            width: 35%;
            font-weight: bold;
            border: 0px;
        }
        .set-input-box-2 {
            height: 37px;
            color: #707070 !important;
            border: 1px solid lightgray;
            border-radius: 6px !important;
        }

        @media (max-width: 991px) {
            .set-col-book-space-modal{
                margin-bottom:10px;
            }
        }
    </style>
@endsection
@section("content")
    <div class="container setMainContainer" id="bookingRequests">
        <div class="container-fluid">
            <h3 style="color:#57AF2A">Booking Requests</h3>
        </div>
        @if(count($bookings) == 0)
            <h1>No Data found</h1>
        @endif
        @if(isset($bookings) && count($bookings) > 0)
            @foreach($bookings as $books)
                @php
                    $seatNumber = $books->getUserSeatNumber($books->category_id,$books->office_id);
                    $getTimePrice = $books->getTimePrice($books->category_id,$books->office_id);
                @endphp
                @if(isset($seatNumber) && isset($books->office))
                    @if(isset($books->office->officeSettings))
                    <div class="row setCustomRowSet">
                    <div class="setCustomRow3 col-md-3">
                        <img class="imageSettings" src="/{{$books->office->image}}" onerror="this.src='/images/MainLogoDefault.png'" />
                    </div>
                    <div @if($books->status == \App\Models\SharedOfficeBooking::STATUS_CANCELED || $books->status == \App\Models\SharedOfficeBooking::STATUS_CANCELED_CLIENT_SIDE || $books->office->officeSettings->allow != \App\Models\SharedOffice::MANAGE_BOOKING_ALLOW) style="width: 100%;" @endif class="setCustomRow7  col-md-7">
                        <p class="officeName">{{$books->office->office_name}}</p>
                        <div class="row setDisplay">
                            <div class="seatImageBg">
                                <img src="{{asset('images/seatNo.png')}}" class="seatImage">
                            </div>
                            <p class="setPara">
                                <span class="seatNo">Seat No . </span> <span class="setNumber">{{isset($books) ? $books->getUserSeatNumber($books->category_id,$books->office_id) : '--'}}</span>
                            </p>
                        </div>
                        <div class="row setDisplay2">
                            <div class="seatImageBg">
                                <img src="{{asset('images/calender.png')}}" class="seatImage">
                            </div>
                            <div class="row setDisplayInside">
                                <div class="section-1">
                                    <p class="setPara">
                                        <span class="dates">Check In . </span>
                                    </p>
                                    <p class="setPara">
                                        <span class="setCalender">{{$books->check_in}}</span>
                                    </p>
                                </div>
                                <div class="section-2">
                                    <img class="setRightArrow" src="{{asset('images/rightArrow.png')}}" >
                                </div>
                                <div class="section-3">
                                    <p class="setPara">
                                        <span class="dates">Check Out . </span>
                                    </p>
                                    <p class="setPara">
                                        <span class="setCalender">{{$books->check_out}}</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        @if($books->status == \App\Models\SharedOfficeBooking::STATUS_REQUEST)
                            <div class="setAlertMessage alert alert-info">
                                Pending
                            </div>
                        @elseif($books->status == \App\Models\SharedOfficeBooking::STATUS_CANCELED)
                            <div class="setAlertMessage alert alert-danger">
                                Canceled By Owner
                            </div>
                        @elseif($books->status == \App\Models\SharedOfficeBooking::STATUS_BOOKED)
                            <div class="setAlertMessage alert alert-success">
                                Booked
                            </div>
                        @elseif($books->status == \App\Models\SharedOfficeBooking::STATUS_CANCELED_CLIENT_SIDE)
                            <div class="setAlertMessage alert alert-danger">
                                Canceled By Client
                            </div>
                        @endif
                    </div>
                    @if($books->status == \App\Models\SharedOfficeBooking::STATUS_CANCELED || $books->status == \App\Models\SharedOfficeBooking::STATUS_CANCELED_CLIENT_SIDE)
                    @else
                    <div class="setCustomRow2 col-md-2">
                        @if($books->office->officeSettings->allow == \App\Models\SharedOffice::MANAGE_BOOKING_ALLOW)
                            @if($books->status != \App\Models\SharedOfficeBooking::STATUS_BOOKED)
                            <button class="btn btn-sm btn-default btnGreenColor" data-toggle="modal" data-target="#changeBookingModa" @click="changeBookingRequest('{{$books->office->id}}','{{$books->id}}')">Change booking</button>
                            @endif
                        <br>
                        @endif
                        @if($books->status != \App\Models\SharedOfficeBooking::STATUS_BOOKED)
                        <button class="btn btn-sm btn-default btnGrayColor" @click="cancelBooking('{{$books->office->id}}','{{$books->id}}','{{$books->category_id}}','{{$books->office->officeSettings->cancel_type}}','{{$getTimePrice}}')">Cancel</button>
                        @endif
                        @if($books->office->officeSettings->allow == \App\Models\SharedOffice::MANAGE_BOOKING_ALLOW)
                        <button class="btn btn-sm btn-success btnGreenColor" @click.prevent="goToDetailPage('{{$books->id}}')">Details</button>
                        @endif
                    </div>
                    @endif
                </div>
                @endif
            @endif
        @endforeach
    @endif
    <!-- The Modal -->
        <div class="modal fade" id="changeBookingModa">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header setChangeBookingRequest">
                        <h4 class="modal-title setModalTitleChangeBookingRequest">Change Your Booking</h4>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6 set-col-book-space-modal">
                                        <input type="text" id="checkIn"  placeholder="{{trans('common.check_in_date')}}" name="check_in_date" class="datepickerChangeBooking form-control set-box-border-radius set-box-select-2">
                                        <img src="{{asset('images/dateTimePic.png')}}" class="set-date-time-bbo-space-modal-datetimeimg">
                                    </div>
                                    <div class="col-md-6 set-col-book-space-modal">
                                        <img src="{{asset('images/dateTimePic.png')}}" class="set-date-time-bbo-space-modal-datetimeimg">
                                        <input type="text" id="checkout" placeholder="{{trans('common.check_out_date')}}" name="check_out_date" class="datepickerChangeBooking2 form-control set-box-border-radius set-box-select-2">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12 set-col-book-space-modal">
                                        <select v-model="product_id" name="product_id" id="category_id" class="form-control set-input-box-2">
                                            <option v-for="products in productList" :value="products.id">@{{ products.product_type_name }}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div id="modalFooter-changeBooking" class="modal-footer">
                        <button type="button" id="btnbtnChangeBookinggray" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        <button type="button" id="btnbtnChangeBookingGreen" class="btn btn-success" @click="sendChangeRequest">Submit</button>
                    </div>

                </div>
            </div>
        </div>
        <!-- Modal -->
        <div id="cancelConfirm" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Confirmation</h4>
                    </div>
                    <form>
                        <div class="modal-body">
                            <p>Are you sure you want to cancel booking request against seat no : <b>@{{  seat_no }}</b></p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" @click.prevent="confirmCancel">Confirm</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>

        <!-- Modal -->
        <div id="checkDetails" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Details</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" style="display: flex;">
                            <div class="col-md-6">
                                <p>Office Name : @{{ this.office_name }}</p>
                                <p>Office Owner Remarks : @{{ this.remarks }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>

            </div>
        </div>


        <div id="cancelModalBooking" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Cancel Booking</h4>
                    </div>
                    <div class="modal-body">
                        <b v-if="cancel_type == '{{\App\Models\SharedOfficeSetting::CANCEL_WITH_CHARGES}}'">If you cancel the seat it will charge you @{{ time_price }} $ .<br> Do you still want to cancel?</b>
                        <b v-else>Are you sure you want to cancel your booking ... !</b>
                    </div>
                    <div v-if="this.cancel_type == '{{\App\Models\SharedOfficeSetting::CANCEL_WITH_CHARGES}}'" class="modal-footer">
                        <button type="button" class="btn btn-default" style="color:#fff;border:0px;background:#54616c;" data-dismiss="modal">Wait</button>
                        <button type="button" class="btn btn-default" style="color:#fff;border:0px;background:#57a224;" @click="confirmCancelBooking" >Confirm</button>
                    </div>
                    <div v-if="this.cancel_type == '{{\App\Models\SharedOfficeSetting::CANCEL_WITH_OUT_CHARGES}}'" class="modal-footer">
                        <button type="button" class="btn btn-default" style="color:#fff;border:0px;background:#54616c;" data-dismiss="modal">No</button>
                        <button type="button" class="btn btn-default" style="color:#fff;border:0px;background:#57a224;" @click="confirmCancelBooking" >Yes</button>
                    </div>
                </div>

            </div>
        </div>

    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('.datepickerChangeBooking').datetimepicker({
                autoClose: true,
            }).on('changeDate', function(e){
                $('.datepickerChangeBooking').datetimepicker('hide');
            });

            $('.datepickerChangeBooking2').datetimepicker({
                autoClose: true,
            }).on('changeDate', function(e){
                $('.datepickerChangeBooking2').datetimepicker('hide');
            });
        });
    </script>
    <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.11"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.5.1/vue-resource.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script>
        new Vue({
            el: '#bookingRequests',
            data: {
                clientIp: '{{\Request::getClientIp(true)}}',
                seat_no:'',
                id:'',
                remarks:'',
                office_name:'',
                productList:[],
                product_id:'',
                checkIn:'',
                checkout:'',
                office_id:'',
                book_id:'',
                office_id_c:'',
                category_id:'',
                id_id:'',
                cancel_type:'',
                time_price:''
            },
            methods: {
                cancelBooking: function(office_id , id ,category_id, cancel_type, time_price) {
                    this.id_id = id;
                    this.category_id = category_id;
                    this.office_id_c = office_id;
                    this.cancel_type = cancel_type;
                    this.time_price = time_price;
                    $('#cancelModalBooking').modal('show');
                },
                confirmCancelBooking: function(){
                    let office_id = this.office_id_c;
                    let category_id = this.category_id;
                    let id = this.id_id;

                    let url = '/cancel/booking/'+office_id+'/'+id+'/'+category_id;
                    this.$http.get(url).then((response) => {
                        console.log('response');
                        console.log(response);
                        if(response.data.status == true) {
                            toastr.success(response.data.message);
                            setTimeout(function () {
                                window.location.reload();
                            },1500);
                        }
                    }).catch(() => {
                        console.log('error');
                        console.log(error);
                    });
                },
                sendChangeRequest: function(){
                    console.log('product_id');
                    let checkIn = $('#checkIn').val();
                    let checkOut = $('#checkout').val();
                    console.log(this.product_id);
                    console.log(checkIn);
                    console.log(checkOut);
                    console.log(this.office_id);
                    if(this.product_id == '') {
                        toastr.error('Please Select Seat');
                    } else if(this.office_id == ''){
                        toastr.error('Office id null');
                    } else {
                        let form = new FormData();
                        form.append('_token', '{{csrf_token()}}');
                        form.append('book_id', '{{csrf_token()}}');
                        form.append('product_id', this.product_id);
                        form.append('user_id', "{{\Auth::user()->id}}");
                        form.append('check_in', checkIn);
                        form.append('check_out', checkOut);
                        form.append('office_id', this.office_id);
                        form.append('book_id', this.book_id);
                        let url = "{{ route('api.sharedoffice.changeBookingPost') }}";
                        this.$http.post(url, form).then((response) => {
                            console.log('response');
                            console.log(response);
                            if(response.data.status == true) {
                                toastr.success(response.data.message);
                                setTimeout(function () {
                                    window.location.reload();
                                },1000);
                            }
                        }).catch((error) => {
                            console.log('error');
                            console.log(error);
                        });
                    }
                },
                changeBookingRequest: function(officeId,bookId){
                    this.office_id = officeId;
                    this.book_id = bookId;
                    let url = '/change/booking/request/'+officeId;
                    this.$http.get(url).then((response) => {
                        console.log('response');
                        console.log(response.data.data);
                        console.log(response.data.data.office.office_products);
                        this.productList = response.data.data.office.office_products;
                    }).catch((error) => {
                        console.log('error');
                        console.log(error);
                    })
                },
                goToDetailPage: function(id){
                    window.location.href='/{{\Session::get('lang')}}/booking/detail/'+id;
                },
                openCancelBox: function (id,seat_no) {
                    this.seat_no = seat_no;
                    this.id = id;
                },
                openDetailBox: function (id,office_name,remarks) {
                    this.office_name = office_name;
                    this.remarks = remarks;
                },
                confirmCancel: function () {
                    let link = '/booking/request/cancel/'+this.id;
                    this.$http.get(link).then((response) => {
                        console.log('response');
                        console.log(response.body.status);
                        if(response.body.status == 'success') {
                            toastr.success(response.body.message);
                            setTimeout(function () {
                                window.location.reload();
                            },2000);
                        }
                    }).catch((error) => {
                        console.log('error');
                        console.log(error);
                    })
                }
            },

            mounted(){
                console.log('booking requests data');
            }
        });
    </script>
    <script>
        $('.scroll-div').slimScroll({});
    </script>
@endsection

