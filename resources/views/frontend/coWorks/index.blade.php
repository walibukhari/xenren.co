@extends("frontend.coWorks.layouts.default")
@section('title')
    {{trans('common.coworking_spaces_title')}}
@endsection
@section('header')
    <link href="{{asset('custom/css/frontend/coWork/css/mobile-view.css')}}" rel="stylesheet" type="text/css">
    <style>
        .dot-loader img{
            width: 50px;
            padding-left: 20px;
        }
    </style>
@endsection
@section("content")

    <!-- web-view results -->
    <div class="website-resonsive-design">
        <section class="slider-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center title set-center-title-neweset">
                        <h2>{{trans('common.newest_spaces')}}</h2>
                    </div>
                    <div class="col-md-12 text-right nex-prev-btn-section">
                        <div class="control-box">
                            <a href="#" class="set-view-all-text"><span class="view-all-text-slider p-0 m-0">{{trans('common.view_all')}}</span></a>
                            <a data-slide="prev" href="#myCarousel" class="carousel-control left"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                            <a data-slide="next" href="#myCarousel" class="carousel-control right"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>

                    <div class="col-md-12 p-0 m-0">
                        <div id="myCarousel" class="row carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="item active">
                                    <ul class="thumbnails">

                                        <li class="col-sm-2 box" v-for="offices in sharedOfficesActive">
                                            <a href="Javascript:;" style="text-decoration:none !important;" @click="getSharedOfficeByID(offices, offices.id)">
                                                <img :src="offices.compressed_image" width="100%" alt="" class="setImageCMP"  onerror="this.src='/images/MainLogoDefault.png'">
                                                <div class="box-hidden"><p class="p-0 m-0">ENQUIRY NOW</p></div>
                                                <div class="caption-box">
                                                    <div class="dot-loader" v-model="dotsLoader" v-if="dotsLoader">
                                                        <img class="dotsloader" src="{{asset('images/loaders/dotsloader.gif')}}">
                                                    </div>
                                                    <span class="attachLoader" v-else>@{{ moment(offices.created_at).format('YYYY-MM-DD') }}</span>
                                                    <div class="dot-loader" v-model="dotsLoader" v-if="dotsLoader">
                                                        <img class="dotsloader" src="{{asset('images/loaders/dotsloader.gif')}}">
                                                    </div>
                                                    <h4 class="attachLoader" v-else>@{{ getOfficeName(offices) }}</h4>
                                                    <div class="dot-loader" v-model="dotsLoader" v-if="dotsLoader">
                                                        <img class="dotsloader" src="{{asset('images/loaders/dotsloader.gif')}}">
                                                    </div>
                                                    <p class="attachLoader" v-else><img src="{{asset('custom/css/frontend/coWork/image/icon.png')}}">
                                                            @{{ officeLocation(offices) }}
                                                    </p>
                                                </div>
                                            </a>
                                        </li>

                                    </ul>
                                </div>

                                <div class="item" v-for="i in Math.ceil(sharedOfficesInactive.length / 5)">
                                    <ul class="thumbnails">
                                        <li class="col-sm-2 box" v-for="offices in sharedOfficesInactive.slice((i - 1) * 5, i * 5)">
                                            <a href="Javascript:;" style="text-decoration:none !important;" @click="getSharedOfficeByID(offices, offices.id)">
                                                <img :src="offices.compressed_image" width="100%" alt="" class="setImageCMP"  onerror="this.src='/images/MainLogoDefault.png'">
                                                <div class="box-hidden"><p class="p-0 m-0">ENQUIRY NOW</p></div>
                                                <div class="caption-box">
                                                    <div class="dot-loader" v-model="dotsLoader" v-if="dotsLoader">
                                                        <img class="dotsloader" src="{{asset('images/loaders/dotsloader.gif')}}">
                                                    </div>
                                                    <span class="attachLoader" v-else>@{{ moment(offices.created_at).format('YYYY-MM-DD') }}</span>
                                                    <div class="dot-loader" v-model="dotsLoader" v-if="dotsLoader">
                                                        <img class="dotsloader" src="{{asset('images/loaders/dotsloader.gif')}}">
                                                    </div>
                                                    <h4 class="attachLoader" v-else>@{{ getOfficeName(offices) }}</h4>
                                                    <div class="dot-loader" v-model="dotsLoader" v-if="dotsLoader">
                                                        <img class="dotsloader" src="{{asset('images/loaders/dotsloader.gif')}}">
                                                    </div>
                                                    <p class="attachLoader" v-else><img src="{{asset('custom/css/frontend/coWork/image/icon.png')}}">
                                                            @{{ officeLocation(offices) }}
                                                    </p>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="map-secion">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center title">
                        <h2>{{trans('common.find_spaces_world')}}</h2>
                    </div>
                    <div class="col-md-12 map-secion-img text-center m-0 p-0">
                        <div id="map"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="slider-section slider-section-second">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center title">
                        <h2>{{trans('common.most_review_spaces')}}</h2>
                    </div>
                    <div class="col-md-12 text-right nex-prev-btn-section">
                        <div class="control-box">
                            <a href="#" class="set-view-all-text"><span class="view-all-text-slider p-0 m-0">{{trans('common.view_all')}}</span></a>
                            <a data-slide="prev" href="#myCarousel2" class="carousel-control left"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                            <a data-slide="next" href="#myCarousel2" class="carousel-control right"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>

                    <div class="col-md-12 p-0 m-0">
                        <div id="myCarousel2" class="row carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="item active">
                                    <ul class="thumbnails">

                                        <li class="col-sm-2 box" v-for="offices in sharedOfficesInactive">
                                            <a href="Javascript:;" style="text-decoration:none !important;" @click="getSharedOfficeByID(offices, offices.id)">
                                                <img :src="offices.compressed_image" width="100%" alt="" class="setImageCMP" onerror="this.src='/images/MainLogoDefault.png'">
                                                <div class="box-hidden"><p class="p-0 m-0">ENQUIRY NOW</p></div>
                                                <div class="caption-box">
                                                    <div class="dot-loader" v-model="dotsLoader" v-if="dotsLoader">
                                                        <img class="dotsloader" src="{{asset('images/loaders/dotsloader.gif')}}">
                                                    </div>
                                                    <span class="attachLoader" v-else>@{{ moment(offices.created_at).format('YYYY-MM-DD') }}</span>
                                                    <div class="dot-loader" v-model="dotsLoader" v-if="dotsLoader">
                                                        <img class="dotsloader" src="{{asset('images/loaders/dotsloader.gif')}}">
                                                    </div>
                                                    <h4 class="attachLoader" v-else>@{{ getOfficeName(offices) }}</h4>
                                                    <div class="dot-loader" v-model="dotsLoader" v-if="dotsLoader">
                                                        <img class="dotsloader" src="{{asset('images/loaders/dotsloader.gif')}}">
                                                    </div>
                                                    <p class="attachLoader" v-else><img src="{{asset('custom/css/frontend/coWork/image/icon.png')}}"> @{{ officeLocation(offices) }}</p>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="item" v-for="i in Math.ceil(sharedOfficesInactive.length / 5)">
                                    <ul class="thumbnails">
                                        <li class="col-sm-2 box" v-for="offices in sharedOfficesInactive.slice((i - 1) * 5, i * 5)">
                                            <a href="Javascript:;" style="text-decoration:none !important;" @click="getSharedOfficeByID(offices, offices.id)">
                                                <img :src="offices.compressed_image" width="100%" alt=""  class="setImageCMP" onerror="this.src='/images/MainLogoDefault.png'">
                                                <div class="box-hidden"><p class="p-0 m-0">ENQUIRY NOW</p></div>
                                                <div class="caption-box">
                                                    <span>@{{ moment(offices.created_at).format('YYYY-MM-DD') }}</span>
                                                    <h4>@{{ getOfficeName(offices)}}</h4>
                                                    <p><img src="{{asset('custom/css/frontend/coWork/image/icon.png')}}"> @{{ officeLocation(offices) }}</p>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
    <!-- web-view results -->

    @if($is_mobile)
        <!-- mobile web responsive design -->
    <div class="preload">
        <img src="{{ asset('images/searchResult.png.gif') }}" style="width:100px;">
    </div>
    <section class="mobile-web-responsive-devices">
        <div class="mobile-content-main">
            <div class="row m-0">
                <div class="col-sm-12">
                    <div class="co-worker-head">
                        <p>Newest Spaces Available</p>
                        <a href="javascript:;" @click="newestSpaces()">Show all</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 custom-padding" v-for="(offices , index) in randomList(this.sharedOffices)" v-if="index < 10">
                    <div class="co-worker-widget" @click="getSharedOfficeByID(offices, offices.id)">
                        <div class="co-worker-widget-image">
                            <img :src="'/'+offices.image"/>
                            <div class="ratting-star-co-worker">
                                <ul v-if="getSharedOfficeRatingMV(offices) == 1">
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                </ul>
                                <ul v-else-if="getSharedOfficeRatingMV(offices) == 2">
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                </ul>
                                <ul v-else-if="getSharedOfficeRatingMV(offices) == 3">
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                </ul>
                                <ul v-else-if="getSharedOfficeRatingMV(offices) == 4">
                                    <li><i class="fa fa-star " aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                </ul>
                                <ul v-else-if="getSharedOfficeRatingMV(offices) == 5">
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                </ul>
                                <ul v-else>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                </ul>
                                <p>@{{ getSharedOfficeRatingMV(offices) }} reviews</p>
                            </div>
                            <div class="price-co-worker">
                                <p>$@{{ offices.min_seat_price }}/month</p>
                            </div>
                            <div class="view-co-worker">
                                <p>@{{ offices.office_images.length }}</p>
                                <img class="camera-icon-view-co-worker" src="{{asset('images/photo-camera.png')}}">
                            </div>
                        </div>
                        <div class="co-worker-widget-detail">
                            <span>@{{ getDate(offices) }}</span>
                            <h2>@{{ getOfficeNameSTR(offices) }}</h2>
                            <p><img class="location-icon" src="{{asset('images/location-image.png')}}">
                                @{{ getsharedOfficeLocationSTR(offices) }}   </p>
                            <ul>
                                <li v-for="facilities in offices.shfacilities"><img :src="facilities.image" /></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="top-co-works-spaces">
            <div class="row m-0">
                <div class="col-xs-6 custom-padding-top-spaces-1" @click="gotoTopSpaces()">
                    <img src="{{asset('images/image-1331.png')}}"/>
                    <p>Top @{{ this.sharedOfficesMobileDevice.length }} Coworking Spaces in @{{ this.country_name[0] }}</p>
                </div>

                <div class="col-xs-6 custom-padding-top-spaces-2" @click="gotoTopSpaces()">
                    <img src="{{asset('images/image-1221.png')}}"/>
                    <p>Top @{{ this.sharedOfficesMobile && this.sharedOfficesMobile.length }} Coworking Spaces in @{{ this.countrycity_name[0] }}</p>
                </div>
            </div>
        </div>


        <div class="row m-0">
            <div class="col-sm-12">
                <div class="co-worker-head">
                    <p>Most Reviewed Coworking Spaces</p>
                </div>
            </div>
        </div>
        <ul class="set-ul-custom-slider">
            <li class="set-li-slider">
                <div class="col-xs-6 custom-padding-most-review-co-work-spaces" v-for="(offices , index) in this.sharedOffices">
                    <div class="co-worker-widget most-reviwed-cowork-spaces" @click="getSharedOfficeByID(offices, offices.id)">
                        <div class="co-worker-widget-image">
                            <img class="most-reviwed-cowork-spaces-shared-office-image" :src="'/'+offices.image"/>
                            <div class="price-co-worker">
                                <p>$@{{ offices.min_seat_price }}/month</p>
                            </div>
                        </div>
                        <div class="co-worker-widget-detail co-worker-widget-detail-most-reviewed">
                            <div class="ratting-star-co-worker-most-review">
                                <ul v-if="getSharedOfficeRatingMV(offices) == 1">
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                </ul>
                                <ul v-else-if="getSharedOfficeRatingMV(offices) == 2">
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                </ul>
                                <ul v-else-if="getSharedOfficeRatingMV(offices) == 3">
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                </ul>
                                <ul v-else-if="getSharedOfficeRatingMV(offices) == 4">
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star " aria-hidden="true"></i></li>
                                </ul>
                                <ul v-else-if="getSharedOfficeRatingMV(offices) == 5">
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star activeStars" aria-hidden="true"></i></li>
                                </ul>
                                <ul v-else>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                </ul>
                            </div>
                            <span class="likes-count-most-reviewed">
                                <span>@{{ getLikeCountSharedOffice(offices) }}</span>
                                <img src="{{asset('images/likeIcons.png')}}"></span>
                            <h2>@{{ getOfficeNameSTR(offices) }}</h2>
                            <p><img class="location-icon-most-review" src="{{asset('images/location-image.png')}}">
                                @{{ getsharedOfficeLocationSTR(offices) }}</p>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </section>
    <!-- mobile web responsive design -->
    @endif

@endsection
@include('frontend.coWorks.scripts.index')
