@extends("frontend.coWorks.layouts.default")
@section('title')
    {{trans('common.coworking_spaces_title')}}
@endsection
@section('header')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{asset('custom/css/frontend/coWork/login.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('custom/css/frontend/coWork/forgotPassword.css')}}" rel="stylesheet" type="text/css">
    <style>
        .coWorkSpace-login-logo {
            text-align: center;
            padding-top: 10%;
            padding-right: 48%;
            position: relative;
        }
        .lds-ring {
            position: absolute;
            width: 100%;
            height: 37px;
            bottom: 44px;
            display: flex;
            justify-content: center;
            right: 8px;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 28px;
            height: 28px;
            margin: 8px;
            border: 4px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
@endsection
@section("content")
    <div class="row set-main-row-login-coWorkSpace">
        <div class="col-md-12 set-padding-login-col-md-6-2-left">
            <div class="col-md-6 set-col-md-6-paragraph-small-text">
                <p class="set-login-paragraph">Cowork space management tools, for free!</p>
                <small class="set-small-text-image">We use technology to simplify online recruitment process and make your space manageable and efficient.</small>
            </div>
            <div class="col-md-6 pull-right set-padding-col-md-6-2login-cowork-space">
                <div class="coWorkSpace-login-logo">
                    <img src="{{asset('custom/css/frontend/coWork/image/xenrenLogoLoginPageCoWork.png')}}">
                    <p class="set-logo-xenren-name">XenRen</p>
                    <small class="set-small-logo-xenren-name">Coworker & Coworking space</small>
                    <br>
                </div>
                <form class="set-form-login-coWorkspace" id="reset-form" method="post">
                    <div class="row">

                        <div class="col-md-6 text-center set-col-md-6-forgot-password-col-md-6">
                            <p class="resetpassword-c">
                                Reset your password
                            </p>
                            <p class="emailaddress-c">
                                Enter new password.
                            </p>
                        </div>

                        <div class="col-md-6 pull-left set-col-md-6-login-coworkSpace">
                            <div class="form-group set-form-group-email-margin-bottom">
                                <label class="set-login-email-label-coWork">Email</label>
                                <input type="text" name="email" id="email" value="{{$email}}" class="form-control set-login-email-input">
                            </div>
                        </div>

                        <div class="col-md-6 pull-left set-col-md-6-login-coworkSpace">
                            <div class="form-group set-form-group-email-margin-bottom">
                                <label class="set-login-email-label-coWork">Password</label>
                                <input type="password" name="password" id="password123" class="form-control set-login-email-input">
                            </div>
                        </div>
                        <div class="col-md-6 pull-left set-col-md-6-login-coworkSpace">
                            <div class="form-group set-form-group-email-margin-bottom">
                                <label class="set-login-email-label-coWork">Confirm Password</label>
                                <input type="password" name="retype-password" id="retype-password" class="form-control set-login-email-input">
                            </div>
                        </div>


                        <div class="col-md-6 pull-left set-col-md-6-login-coworkSpace">
                            <div class="form-group set-margin-botton-singin-CowOrkSpace">
                                <img class="set-login-coWork-image" src="{{asset('custom/css/frontend/coWork/image/buttonimagesharedoffice.png')}}">
                                <button type="button" style="height:37px;" onclick="setPassword()" class="set-sign-in-image-coWork btn green pull-right">
                                    <span id="resetPassword">Reset Password</span>
                                </button>
                                <div class="lds-ring" style="display: none;"><div></div><div></div><div></div><div></div></div>
                            </div>
                        </div>
                    <input type="hidden" name="email" id="staff_id" value="{{$id}}">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function setPassword()
        {
            $('#resetPassword').hide();
            $('.lds-ring').show();
            var action =  $('#reset-form').attr('method');
            var password = $('#password123').val();
            var retypepassword = $('#retype-password').val();
            var session = '{{\Session::get('lang')}}';
            var url = '/'+session+'/coworking-spaces/set-password-post';
            var staff_id = $('#staff_id').val();
            console.log('password');
            console.log(action,password,retypepassword,staff_id);
            if(password === ''){
                toastr.error('please enter password');
                $('#resetPassword').show();
                $('.lds-ring').hide();
            } else if(password.length < 8){
                toastr.error('password length must be at least 8 characters');
                $('#resetPassword').show();
                $('.lds-ring').hide();
            } else if(password !== retypepassword){
                toastr.error('password and confirm password not match');
                $('#resetPassword').show();
                $('.lds-ring').hide();
            } else {
                $.ajax({
                    url:url,
                    type:action,
                    data:{
                        'password':password,
                        'retype_password':retypepassword,
                        'id' : staff_id,
                    },

                    success: function(response)
                    {
                        console.log(response);
                        if (response.success)
                        {
                            toastr.success(response.success);
                            $('#resetPassword').show();
                            $('.lds-ring').hide();
                            window.location.href = '/'+session+'/coworking-spaces/sign-in';
                        }
                    },
                    error: function(error)
                    {
                        toastr.error(error);
                        $('#resetPassword').show();
                        $('.lds-ring').hide();
                        console.log(error);
                    }
                });
            }
        }
    </script>
@endsection
