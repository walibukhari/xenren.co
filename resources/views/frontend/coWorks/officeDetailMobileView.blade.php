<section class="section-mobile-view-office-details">
    <!-- Slider main container -->
    <div class="swiper-container">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <!-- Slides -->
{{--            @foreach($officeImages as $image)--}}
                <div class="swiper-slide">
                    <img src="/{{$officeData->image}}" />
                </div>
{{--            @endforeach--}}

        </div>
        <!-- If we need pagination -->
        <div class="swiper-pagination"></div>

        <!-- If we need navigation buttons -->
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>

        <!-- If we need scrollbar -->
        <div class="swiper-scrollbar"></div>
    </div>
    <div class="preload">
        <img src="https://i.imgur.com/KUJoe.gif">
    </div>
    <div class="panel panel-default office-details-panel-default">
        <div class="panel-heading office-details-panel-heading">
            <div class="row">
                <div class="col-md-6 pull-left set-col-md-6-pull-left-mobile-view-2">
                    <h2 class="set-office-detail-office_name">@{{getOfficeDetails.office_name}}</h2>
                    <div class="office-details-div">
                        <img  class="office-details-location-image" src="{{asset('images/mobile-view-image-location.png')}}">
                        <div class="set-span-location-office-details">@{{ getOfficeDetails.location }}</div>
                    </div>
                </div>
                <div class="col-md-6 pull-right set-col-md-6-pull-right-mobile-view-2">
                    <div class="set-telephone-block">
                        <img src="{{asset('images/telephone.png')}}"><span class="set-telephone-number">@{{ getOfficeDetails.contact_phone }}</span>
                    </div>
                    <div class="set-envolope-block">
                        <img src="{{asset('images/close envelope.png')}}"><span class="set-email-cowork">@{{ getOfficeDetails.contact_email }}</span>
                    </div>
                    <div class="open-bloxk">
                        <span class="set-open-now-block">Open now</span>
                        <p class="set-open-now-time">@{{ formatTime(getOfficeDetails.monday_opening_time) }} - @{{ formatTime(getOfficeDetails.monday_closing_time) }}</p>
                    </div>
                </div>
            </div>
        </div>
        <hr class="office-details-hr">
        <div class="panel-body office-details-panel-body">
            <p class="set-office-description-office-details">@{{ getOfficeDetails.description }}</p>
            <h2 class="set-facilitites-office-details-mobile-view">Facilities</h2>
            <div class="row set-scrolller-office-detail-mobile-view-12">
                <div class="scroller-view-mobile">
                    <div class="col-md-3 col-sm-3 set-mobile-view-office-details-facilities  set-border-right-mobile-view" style="padding-left:0px;">
                        <h6>@{{ getOfficeDetails.office_space }}</h6>
                        <h5 class="set-border-right-h6">{{trans('common.office_space')}}</h5>
                    </div>
                    <div class="col-md-3 col-sm-3 set-mobile-view-office-details-facilities set-border-right-mobile-view ">
                        <h6>@{{ getOfficeDetails.total_dedicated_desks }}</h6>
                        <h5>{{trans('common.dedicated_desk')}}</h5>
                    </div>
                    <div class="col-md-3 col-sm-3 set-mobile-view-office-details-facilities set-border-right-mobile-view ">
                        <h6>@{{ getOfficeDetails.total_meeting_room }}</h6>
                        <h5>{{trans('common.meeting_room')}}</h5>
                    </div>
                    <div class="col-md-3 col-sm-3 set-mobile-view-office-details-facilities" style="padding-right: 0px;">
                        <h6>@{{ time_format(getOfficeDetails.monday_opening_time) }} TO @{{ time_format(getOfficeDetails.monday_closing_time) }}</h6>
                        <h5 class="set-h6-operate-hours-mobile-view">{{trans('common.operate_hourss')}}</h5>
                    </div>
                </div>
            </div>

            <div class="col-md-12 facilitie-bellow setFacilitie-mobile-view-office-details">
                <div class="row setFacilitieRowsUFF-mobile-view-office-details">
                    <div class="col-md-3 col-sm-3 col-xs-6 set-office-details-mobile-view-col-xs-6" v-for="cat in getOfficeDetails.categories">
                        <div class="row">
                            <h5><img :src="`/${cat.productcategory.image}`"/> @{{ getCategoryName(cat.productcategory.name) }}</h5>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6 set-office-details-mobile-view-col-xs-6" v-for="(shfacility,index) in getOfficeDetails.shfacilities">
                        <div class="row">
                            <h5><img :src="`${shfacility.image}`"/> @{{ shfacility.title }}</h5>
                        </div>
                    </div>
                </div>
            </div>


            <div class="set-book-now-mobile-view">
                    <span data-toggle="modal" data-target="#myBookSpaceModal" class="set-book-now-office-details-mobile-view">
                        <img src="{{asset('images/calenderMovileView.png')}}">{{trans('common.book_now')}}
                    </span>
                <img data-toggle="modal" data-target="#myModal" class="set-chat-message-image-mobile-view" src="{{asset('images/messageChatView.png')}}">
            </div>

            <div class="set-similar-spaces-mobile-view">
                <h2>Same State</h2>
                <ul class="set-ul-custom-slider">
                    <li class="set-li-slider">
                        <div class="set-sharedofficesss" v-for="offices in sharedOfficesData">
                            <div class="panel panel-default set-office-details-mobile-view-panel-2">
                                <div class="panel-heading panel-heading-mobile-view-2">
                                    <img class="set-mobile-view-panel-head-body-image-2" :src="'/'+offices.image">
                                </div>
                                <div class="panel panel-body-mobile-view-2">
                                    <span class="set-office-details-per-month-mobile-view-panel-2">$@{{ offices.min_seat_price }} / month</span>
                                    <p class="set-office-details-para-mobile-view-panel-2">@{{ getofficenamestr(offices) }}</p>
                                    <span class="set-office-details-location-mobile-view-panel-2"><img src="{{asset('images/location-image.png')}}">@{{ getsharedOfficeLocationstr(offices) }}</span>
                                    <p style="margin-bottom:0px;">
                                    <div v-if="getSharedOfficeRatingMVOD(offices) == 1">
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view activeStars" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view" aria-hidden="true"></i>
                                    </div>

                                    <div v-else-if="getSharedOfficeRatingMVOD(offices) == 2">
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view activeStars" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view activeStars" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view" aria-hidden="true"></i>
                                    </div>

                                    <div v-else-if="getSharedOfficeRatingMVOD(offices) == 3">
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view activeStars" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view activeStars" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view activeStars" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view" aria-hidden="true"></i>
                                    </div>

                                    <div v-else-if="getSharedOfficeRatingMVOD(offices) == 4">
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view activeStars" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view activeStars" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view activeStars" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view activeStars" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view" aria-hidden="true"></i>
                                    </div>

                                    <div v-else-if="getSharedOfficeRatingMVOD(offices) == 5">
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view activeStars" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view activeStars" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view activeStars" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view activeStars" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view activeStars" aria-hidden="true"></i>
                                    </div>
                                    <div v-else>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view" aria-hidden="true"></i>
                                        <i class="fa fa-star set-fa-fa-fastars-mobile-web-view" aria-hidden="true"></i>
                                    </div>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

        </div>
    </div>

</section>
