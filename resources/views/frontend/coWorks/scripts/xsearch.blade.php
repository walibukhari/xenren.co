@section('script')
	<script>
		$(function() {
			if (window.matchMedia("(max-width: 556px)").matches) {
				$(".preload").fadeOut(2000, function() {
					$(".set-container-tabs-girdandlist").fadeIn(1000);
					$("#btnContainer").fadeIn(1000);
				});
			}
		});
	</script>
	<script>
		$(function(){
            $(".searchbarMVR1").autocomplete({
                source: function (request, response) {
                    jQuery.get("/api/search-shared-office", {
                        search: request.term
                    }, function (data) {
                        // console.log(data['data']);
                        response(data['data']);
                    });
                },
                minLength: 3,
                select: function(event, ui) {
                    if (ui.item.searchByLocation === true) {
                        sessionStorage.setItem("country", ui.item.label);
                        var n = ui.item.label.search(",");
                        if (n === -1) {
                            sessionStorage.setItem("setContent", "<div class='bredcum setMalaysiabredcrum'><a href='#'><p class='p-0 m-0' id='country'><br><span>123</span> <img class='setImagetopUp2 setArrowImage' src='{{asset('custom/css/frontend/coWork/image/arrow.png')}}'></p></a></div>");
                        } else {
                            sessionStorage.setItem("setContent", "<div class='bredcum setMalaysiabredcrum'><a href='#'><p class='p-0 m-0' id='country'><br><span>123</span> <img class='setImagetopUp2 setArrowImage' src='{{asset('custom/css/frontend/coWork/image/arrow.png')}}'></p></a></div><div class='bredcum setSepang'><a href='#'><p class='p-0 m-0'  id='city'><br><span>11</span></p></a></div>");
                        }
                        window.location.href = ui.item.route
                    } else {
                        window.location.href = ui.item.route
                    }
                }
            }).autocomplete("instance")._renderItem = function(ul, item) {
                // console.log(item);
                var item = $('<div class="row">' +
                    '<div class="col-md-1 '+item.icon+' "></div>' +
                    '<div class="col-md-11 description">' + item.label +'</div>' +
                    '</div>')
                return $("<li>").append(item).appendTo(ul);
            };
		})
	</script>

	<script>
		/*get country and city while search*/
		var link2 = '{{asset('coworking-spaces/co-work-spaces?text=&type=undefined')}}';
		var link3 = '{{asset('coworking-spaces')}}';
		var link4 = '{{asset('coworking-spaces/co-work-spaces')}}';
		if (window.location.href === link3 || window.location.href === link2 || window.location.href === link4) {
			sessionStorage.removeItem("setContent");
			sessionStorage.removeItem("country");
		}

		var check = sessionStorage.getItem("setContent");
		var country = sessionStorage.getItem("country");
		var comma = country.split(',');

		// console.log("get-localstorage");
		// console.log(country);
		// console.log(comma);
		// console.log(comma[0]);
		// console.log(comma[1]);
		// console.log("get-localstorage");

		var city_url = '/api/count-shared-office/?city_name=' + comma[0] + '&type=city';
		$.ajax({
			url: city_url,
			type: 'get',

			success: function (data) {
				if (data.count){
					// console.log("get-data");
					// console.log(comma[0]);
					// console.log("get-data");
					var htmlCity = comma[0];
					// console.log(htmlCity);
					$('#setCity').append(htmlCity);
				}
			}
		});

		var country_url = '/api/count-shared-office/?country_name='+comma[1]+'&type=country';
		$.ajax({
			url: country_url,
			type: 'get',

			success: function (data) {
				if (data.countCountry){
					// console.log("get-data");
					// console.log(comma[1]);
					// console.log("get-data");
					var htmlCountry = comma[1];
					// console.log(htmlCountry);
					$('#setCountry').append(htmlCountry);
				}
			}
		});
		/*get country and city while search*/
	</script>


	<!-- grid view and list view tabs scripts -->
	<script>
		// Get the elements with class="column"
		var elements = document.getElementsByClassName("listviewcolumn");

		// Declare a loop variable
		var i;

		// List View
		function listView() {
			$('.fa-fa-fabars-list').addClass('activeViews');
			$('.fa-fa-fabars-grid').removeClass('activeViews');
			$('.listviewcolumn').css('display','block');
			$('.gridviewcolumn').css('display','none');
			for (i = 0; i < elements.length; i++) {
				elements[i].style.width = "99%";
			}
		}

		// Get the elements with class="column"
		var elementss = document.getElementsByClassName("gridviewcolumn");

		var c;
		// Grid View
		function gridView() {
			$('.fa-fa-fabars-grid').addClass('activeViews');
			$('.fa-fa-fabars-list').removeClass('activeViews');
			$('.gridviewcolumn').css('display','grid');
			$('.listviewcolumn').css('display','none');
			for (c = 0; c < elementss.length; c++) {
				elementss[c].style.width = "100%";
			}
		}

		setTimeout(function(){
			$('.fa-fa-fabars-list').addClass('activeViews');
			listView();
		},5);

	</script>
	<!-- grid view and list view tabs scripts -->

	<script src="{{asset('custom/js/bs-modal-bootstrap.js')}}"></script>
	<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCIasatwSA8nyOeN8Cobzil6yO1jsT13rE&callback=initMap&libraries=places,controls"></script>
	<script src="{{asset('custom/js/ListSpaceMap.js')}}"></script>
	<script>
	function initMap() {
			initListMap();;
		}
	</script>
@endsection

@push('js')
<script src="{{asset('js/moment.min.js')}}"></script>
<script>
	var token = '{{ session('api_token') }}';
	var setToken = `Bearer ${token}`;
	new Vue({
		el: '.searchSection',
		data: {
			continent_filter:[],
			type_filter:[],
			sharedOffices:[],
			sharedOfficesOriginalData:[],
			file_name:'',
			full_name:'',
			phone_number:'',
			email_address:'',
			companyName:'',
			location :'',
			filename:'',
		},
		methods: {

			filterRecord(type)
			{
				var arr = [];
				// // console.log("arrrrr");
				// // console.log(arr);
				// // console.log("arrrrr");
				if(type > 0)
				{
					this.sharedOfficesOriginalData.map(function(item){
						if(type == 2){
							// // console.log("hotindex");
							// // console.log(item.total_hot_desk && item.total_hot_desk > 0);
							// // console.log("hotindex");
							if(item.total_hot_desk && item.total_hot_desk)  {
								arr.push(item);
							}
						}
						if(type == 3){
							// // console.log("dedicateddeskindex");
							// // console.log(item.total_dedicated_desks && item.total_dedicated_desks > 0);
							// // console.log("dedicateddeskindex");
							if(item.total_dedicated_desks && item.total_dedicated_desks) {
								arr.push(item);
							}
						}
						if(type == 4){
							// // console.log("meetingindex");
							// // console.log(item.total_meeting_room && item.total_meeting_room);
							// // console.log("meetingindex");
							if(item.total_hot_desk && item.total_meeting_room) {
								arr.push(item);
							}
						}
					});
					// // console.log('final array');
					// // console.log(arr);
					this.sharedOffices = arr;
				} else {
					this.sharedOffices = this.sharedOfficesOriginalData;
				}
			},
			getOfficeNameMV(sharedOffice)
			{
				const locale = '{{\Config::get('app.locale')}}';

				if(locale === 'cn') {
					if(sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'true') {
						return sharedOffice.shared_office_language_data.office_name_cn.substring(0,15) + '...';
					} else if(sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'false') {
						return sharedOffice.shared_office_language_data.office_name_cn.substring(0,15) + '...';
					} else if(sharedOffice.version_chinese === 'false' && sharedOffice.version_english === 'true') {
						return sharedOffice.office_name.substring(0,15) + '...';
					}
				} else if(locale === 'en') {
					if(sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'true') {
						// console.log('case 1');
						return sharedOffice.office_name.substring(0,15) + '...';
					}
					else if(sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'false') {
						// console.log('case 2');
						return sharedOffice.shared_office_language_data && sharedOffice.shared_office_language_data.office_name_cn.substring(0,15) + '...';
					} else if(sharedOffice.version_chinese === 'false' && sharedOffice.version_english === 'true') {
						// console.log('case 3');
						return sharedOffice.office_name.substring(0,15) + '...';
					}
				}
				return sharedOffice.office_name ? sharedOffice.office_name.substring(0,15) + '...' : sharedOffice.shared_office_language_data.office_name_cn.substring(0,15) + '...';
			},


			getSharedOfficeRatingMVS(offices)
			{
				var stars = offices.shared_office_rating ? offices.shared_office_rating : 0;
				var getstars = '';
				stars.map(function(values){
					getstars = Math.round(values.avg);
				});
				return getstars ? getstars : 0;
			},

			getsharedOfficeLocation(sharedOffice)
			{
				if(sharedOffice.location && sharedOffice.location.length && sharedOffice.location.length > 9)
				{
					return sharedOffice.location.substring(0,15) + '...';
				}
				return sharedOffice.location;
			},

			getsharedOfficedesc(sharedOffice)
			{
				if(sharedOffice.description && sharedOffice.description.length && sharedOffice.description.length > 10)
				{
					return sharedOffice.description.substring(0,80) + '...';
				}
				return sharedOffice.description;
			},

			uploadListForm() {
				$('.imageloader').show();
				let tokencsrf = '{{ csrf_token() }}';
				let Listform = new FormData();
				var files  = $('#files')[0].files;
				let counter = 0;
				$.each(files , function(key, value) {
					Listform.append('image_'+key, value);
					counter++;
				});
				Listform.append('company_name',this.companyName);
				Listform.append('full_name',this.full_name);
				Listform.append('phone_number',this.phone_number);
				Listform.append('location',this.location);
				Listform.append('email',this.email_address);
				Listform.append('file_name',$('#files')[0].files[0]);
				Listform.append('counter',counter);
				this.filename = $('#files')[0].files[0];
				let link = '/shared-office-list-space-request';

				if(this.companyName == '')
				{
					toastr.error('Company name field is required');
				}
				else if(this.location == '')
				{
					toastr.error('Please enter location');

				}
				else if(this.filename == undefined || this.filename == '') {
					toastr.error('Please select image');

				}
				else if(this.full_name == '')
				{
					toastr.error('Name field is required');

				}
				else if(this.phone_number == ''){
					toastr.error('Phone number filed is required');

				}
				else if(this.email_address == ''){
					toastr.error('Email filed is required');

				}
				else if(counter > 7){
					toastr.error('you cannot upload more then 6 images');
				} else{
					// const  config = { headers: {'Content-Type': 'multipart/form-data'} };
					this.$http.post(link,Listform, {
						headers: {
							Authorization: tokencsrf,
							'Content-Type': 'multipart/form-data'
						}
					}).then(function(response){
						$('.imageloader').hide();
						this.companyName = '';
						this.full_name = '';
						this.phone_number = '';
						this.location = '';
						this.email_address = '';
						this.file_name = '';
						toastr.success('Your Space Listed Successfully');
						setTimeout(function(){
							window.location.reload();
						},100);
						$('#listSpaceModal').modal('hide');
					}, function(error){
					});
				}
			},

			getSharedOffices: function (uri) {
				let language = window.session.value;
				this.$http.get('/api/sharedoffice?language='+language+'&sort=desc&'+uri[1], {
					headers: {
						Authorization: setToken
					}
				}).then(function(response){
					this.sharedOffices = response.data;
					this.sharedOfficesOriginalData = response.data;
				}, function(error){
				});
			},
			getSharedOfficeByID(name, officeId){
				sessionStorage.removeItem("setContent");
				sessionStorage.removeItem("country");
				const office_name = name.replace(/[^A-Z0-9]/ig, "-").toLowerCase().replace('--', '-').replace('---', '-').replace(/-/g, '-');
				window.location.href= `/coworking-spaces/${office_name}/${officeId}`;
			},
			getOfficeName(sharedOffice){
				const locale = '{{\Config::get('app.locale')}}';
				if(locale === 'cn') {
					return sharedOffice.shared_office_language_data && sharedOffice.shared_office_language_data.office_name_cn ?sharedOffice.shared_office_language_data.office_name_cn : sharedOffice.office_name;
				} else {
					return sharedOffice.office_name;
				}
			},
			filerRecord(){
				// total_dedicated_desk
				// total_meeting_room
				// total_hot_desk
				var arr = [];
				if(this.continent_filter.length > 0 && this.type_filter > 0){
					let continents = this.continent_filter.join();
					let types = this.type_filter.join();
					this.sharedOfficesOriginalData.map(function(item){
						if(item.continent_id && continents.indexOf(item.continent_id) > -1) {
							arr.push(item);
						} else {
							if(this.type_filter == 1){
								if(item.total_hot_desk && item.total_hot_desk > 0 && types.indexOf(1) > -1)  {
									arr.push(item);
								}
							}
							if(this.type_filter == 2){
								if(item.total_hot_desk && item.total_dedicated_desk > 0 && types.indexOf(2) > -1) {
									arr.push(item);
								}
							}
							if(this.type_filter == 3){
								if(item.total_hot_desk && item.total_meeting_room > 0 && types.indexOf(3) > -1) {
									arr.push(item);
								}
							}
						}
					});
					this.sharedOffices = arr;
				} else if(this.type_filter.length > 0){
					let types = this.type_filter.join();
					// console.log('types');
					// console.log(types);
					// console.log(this.sharedOfficesOriginalData);
					// console.log('types');
					this.sharedOfficesOriginalData.map(function(item){
						if(types.indexOf(1) > -1){
							// console.log('indexOfOne');
							if(item.total_hot_desk && item.total_hot_desk > 0)  {
								arr.push(item);
							}
						}
						if(types.indexOf(2) > -1){
							// console.log('indexOfTwo');
							// console.log(item.total_dedicated_desks);
							// console.log('indexOfTwo');
							if(item.total_dedicated_desks > 0) {
								arr.push(item);
							}
						}
						if(types.indexOf(3) > -1){
							// console.log('indexOfThree');
							if(item.total_hot_desk && item.total_meeting_room > 0) {
								arr.push(item);
							}
						}
					});
					this.sharedOffices = arr;
				} else if(this.continent_filter.length > 0){
					let continents = this.continent_filter.join();
					this.sharedOfficesOriginalData.map(function(item){
						if(item.continent_id && continents.indexOf(item.continent_id) > -1) {
							arr.push(item);
						}
					});
					this.sharedOffices = arr;
				} else {
					this.sharedOffices = this.sharedOfficesOriginalData;
				}
			},

			rate(rate) {
				var arr = [];
				this.sharedOfficesOriginalData.map(function(item){
					// console.log(item.rating);
					if (item.rating == rate) {
						arr.push(item);
					}
				});
				this.sharedOffices = arr;
			},

			seekFilter(val) {
				var arr = [];
				this.sharedOfficesOriginalData.map(function(item){
					if (item.max_seat_price != null) {
						if (item.max_seat_price <= val.target.value) {
							arr.push(item);
						}
					} else {
						if (item.meeting_room_price != null) {
							if (item.meeting_room_price <= val.target.value) {
								arr.push(item);
							}
						}
					}
				});
				// console.log('arr');
				// console.log(arr);
				// console.log('arr');
				this.sharedOffices = arr;
			},
		},
		mounted(){
			var uri = window.location.href.split('?');
			this.getSharedOffices(uri);
			this.filterRecord();
		}
	});

	$('#crossimages').click(function(){
		document.getElementById("pac-input").value = '';
	});
</script>
@endpush
