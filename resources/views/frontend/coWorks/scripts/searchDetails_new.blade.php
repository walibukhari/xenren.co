@section('script')
    <script>
        $(function() {
            if (window.matchMedia("(max-width: 556px)").matches) {
                $(".preload").fadeOut(2000, function() {
                    $(".set-container-tabs-girdandlist").fadeIn(1000);
                    $("#btnContainer").fadeIn(1000);
                });
            }
        });

        // $(function() {
        //         $(".preload").fadeOut(2500, function() {
        //             $(".preload").css('display','none');
        //             $(".setPostBoxImage").fadeIn(1000);
        //         });
        //
        //         $(".dotsloader").fadeOut(2500, function() {
        //             $(".dotsloader").css('display','none');
        //             $(".attachLoader").fadeIn(1000);
        //         });
        // });
    </script>
    <!-- grid view and list view tabs scripts -->
    <script>
        // Get the elements with class="column"
        var elements = document.getElementsByClassName("listviewcolumn");

        // Declare a loop variable
        var i;

        // List View
        function listView() {
            $('.fa-fa-fabars-list').addClass('activeViews');
            $('.fa-fa-fabars-grid').removeClass('activeViews');
            $('.listviewcolumn').css('display','block');
            $('.gridviewcolumn').css('display','none');
            for (i = 0; i < elements.length; i++) {
                elements[i].style.width = "99%";
            }
        }

        // Get the elements with class="column"
        var elementss = document.getElementsByClassName("gridviewcolumn");

        var c;
        // Grid View
        function gridView() {
            $('.fa-fa-fabars-grid').addClass('activeViews');
            $('.fa-fa-fabars-list').removeClass('activeViews');
            $('.gridviewcolumn').css('display','grid');
            $('.listviewcolumn').css('display','none');
            for (c = 0; c < elementss.length; c++) {
                elementss[c].style.width = "100%";
            }
        }

        setTimeout(function(){
            $('.fa-fa-fabars-list').addClass('activeViews');
            listView();
        },5);

    </script>
    <!-- grid view and list view tabs scripts -->

    <script src="{{asset('custom/js/bs-modal-bootstrap.js')}}"></script>
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCIasatwSA8nyOeN8Cobzil6yO1jsT13rE&callback=initMap&libraries=places,controls"></script>
    <script src="{{asset('custom/js/ListSpaceMap.js')}}"></script>
    <script>
        function initMap() {
            initListMap();
        }
    </script>
@endsection

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script>
        var token = '{{ session('api_token') }}';
        var setToken = `Bearer ${token}`;
        var urlLocation = '{{$location}}';
        const urlType = '{{$filterType}}';
        new Vue({
            el: '#app',
            data: {
                continent_filter:[],
                type_filter:[],
                sharedOffices:[],
                sharedOfficesOriginalData:[],
                file_name:'',
                full_name:'',
                phone_number:'',
                email_address:'',
                companyName:'',
                location :'',
                filename:'',
                loader: true,
                listLoader:false,
                password:'',
                input: {
                    space_type: '',
                    continent: '',
                    rating: '',
                    max_price: '$1000',
                    min_price: '$0'
                },
                pagination: {
                    per_page: 10,
                    page: 0,
                    total_data: 0
                }
            },
            methods: {

                getLocation() {
                    console.log("click event");
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(showPosition,showError);
                    } else {
                        toastr.error('Geolocation is not supported by this browser.');
                    }

                    function showPosition(position) {
                        lat = position.coords.latitude;
                        long = position.coords.longitude;
                        displayLocation(lat,long);
                        showLocationMap(lat,long)
                    }
                    function showError(error) {
                        console.log("error");
                        console.log(error);
                        console.log('no location access');
                        $.getJSON('https://ipapi.co/json/', function (result) {
                            lat = result.latitude;
                            long = result.longitude;
                            showLocationMap(lat,long);
                            var city = result.city;
                            var country = result.country_name;
                            var fullName = city+','+country;
                            $('#pac-input').val(fullName);
                        });
                    }

                    function showLocationMap(latitude,longitude) {
                        //The location on the map.
                        var mapLocation = new google.maps.LatLng(latitude,longitude);
                        //Map options.
                        var options = {
                            center: mapLocation, //Set center.
                            zoom: 7, //The zoom value.
                            mapTypeId: 'roadmap'
                        };
                        //Create the map object.
                        var listmap = new google.maps.Map(document.getElementById('listMap'), options);
                        var latlng = {lat:latitude , lng:longitude};
                        //Create the marker.
                        marker = new google.maps.Marker({
                            position: latlng,
                            title: 'place',
                            map: listmap
                        });
                    }
                    function displayLocation(latitude,longitude){
                        let that = this;
                        const geocoder = new google.maps.Geocoder();
                        const latlng = new google.maps.LatLng(latitude, longitude);
                        geocoder.geocode(
                            {'latLng': latlng},
                            function(results, status) {
                                if (status == google.maps.GeocoderStatus.OK) {
                                    if (results[0]) {
                                        var add = results[0].formatted_address ;
                                        var  value = add.split(",");
                                        var names = value[2]+','+value[4];
                                        var cleanName = names.replace(/^\s+|\s+$/g, '');
                                        console.log(cleanName);
                                        // this.userLocation = cleanName;
                                        // // console.log("that.userLocation");
                                        // // console.log(that.userLocation);
                                        // // $('.location-input').val(cleanName);
                                        // console.log("this.userLocation");
                                        // console.log(this.userLocation);
                                        $('#pac-input').val(cleanName);
                                    }
                                    else  {
                                        toastr.error('address not found');
                                    }
                                }
                                else {
                                    toastr.error('Geocoder failed due to:'+ status);
                                }
                            }
                        );
                    }
                },

                // filterRecord(type)
                // {
                //     var arr = [];
                //     // // console.log("arrrrr");
                //     // // console.log(arr);
                //     // // console.log("arrrrr");
                //     if(type > 0)
                //     {
                //         this.sharedOfficesOriginalData.map(function(item){
                //             if(type == 2){
                //                 // // console.log("hotindex");
                //                 // // console.log(item.total_hot_desk && item.total_hot_desk > 0);
                //                 // // console.log("hotindex");
                //                 if(item.total_hot_desk && item.total_hot_desk)  {
                //                     arr.push(item);
                //                 }
                //             }
                //             if(type == 3){
                //                 // // console.log("dedicateddeskindex");
                //                 // // console.log(item.total_dedicated_desks && item.total_dedicated_desks > 0);
                //                 // // console.log("dedicateddeskindex");
                //                 if(item.total_dedicated_desks && item.total_dedicated_desks) {
                //                     arr.push(item);
                //                 }
                //             }
                //             if(type == 4){
                //                 // // console.log("meetingindex");
                //                 // // console.log(item.total_meeting_room && item.total_meeting_room);
                //                 // // console.log("meetingindex");
                //                 if(item.total_hot_desk && item.total_meeting_room) {
                //                     arr.push(item);
                //                 }
                //             }
                //         });
                //         // // console.log('final array');
                //         // // console.log(arr);
                //         this.sharedOffices = arr;
                //     } else {
                //         this.sharedOffices = this.sharedOfficesOriginalData;
                //     }
                // },

                getOfficeNameMV(sharedOffice)
                {
                    const locale = '{{\Config::get('app.locale')}}';

                    if(locale === 'cn') {
                        if(sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'true') {
                            return sharedOffice.shared_office_language_data.office_name_cn.substring(0,15) + '...';
                        } else if(sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'false') {
                            return sharedOffice.shared_office_language_data.office_name_cn.substring(0,15) + '...';
                        } else if(sharedOffice.version_chinese === 'false' && sharedOffice.version_english === 'true') {
                            return sharedOffice.office_name.substring(0,15) + '...';
                        }
                    } else if(locale === 'en') {
                        if(sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'true') {
                            // console.log('case 1');
                            return sharedOffice.office_name.substring(0,15) + '...';
                        }
                        else if(sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'false') {
                            // console.log('case 2');
                            return sharedOffice.shared_office_language_data && sharedOffice.shared_office_language_data.office_name_cn.substring(0,15) + '...';
                        } else if(sharedOffice.version_chinese === 'false' && sharedOffice.version_english === 'true') {
                            // console.log('case 3');
                            return sharedOffice.office_name.substring(0,15) + '...';
                        }
                    }
                    return sharedOffice.office_name ? sharedOffice.office_name.substring(0,15) + '...' : sharedOffice.shared_office_language_data.office_name_cn.substring(0,15) + '...';
                },


                getSharedOfficeRatingMVS(offices)
                {
                    var stars = offices.shared_office_rating ? offices.shared_office_rating : 0;
                    var getstars = '';
                    stars.map(function(values){
                        getstars = Math.round(values.avg);
                    });
                    return getstars ? getstars : 0;
                },

                getsharedOfficeLocation(sharedOffice)
                {
                    if(sharedOffice.location && sharedOffice.location.length && sharedOffice.location.length > 9)
                    {
                        return sharedOffice.location.substring(0,15) + '...';
                    }
                    return sharedOffice.location;
                },

                getsharedOfficedesc(sharedOffice)
                {
                    if(sharedOffice.description && sharedOffice.description.length && sharedOffice.description.length > 10)
                    {
                        return sharedOffice.description.substring(0,80) + '...';
                    }
                    return sharedOffice.description;
                },

                uploadListForm() {
                    this.listLoader = true;

                    var loc =  $('#pac-input').val();
                    console.log(loc);
                    console.log("loc");


                    let tokencsrf = '{{ csrf_token() }}';
                    let Listform = new FormData();
                    var files  = $('#files')[0].files;
                    let counter = 0;
                    $.each(files , function(key, value) {
                        Listform.append('image_'+key, value);
                        counter++;
                    });
                    Listform.append('company_name',this.companyName);
                    Listform.append('full_name',this.full_name);
                    Listform.append('phone_number',this.phone_number);
                    // Listform.append('location',this.location);
                    Listform.append('location',loc);
                    Listform.append('email',this.email_address);
                    Listform.append('file_name',$('#files')[0].files[0]);
                    Listform.append('password',this.password);
                    Listform.append('counter',counter);
                    this.filename = $('#files')[0].files[0];
                    let link = '/shared-office-list-space-request';

                    if(this.companyName == '')
                    {
                        toastr.error('{{trans('validation.company_required')}}');
                        this.listLoader = false;
                    }
                    else if(loc == '')
                    {
                        toastr.error('{{trans('validation.location_required')}}');
                        this.listLoader = false;
                    }
                    else if(this.filename == undefined || this.filename == '') {
                        toastr.error('{{trans('validation.image_required')}}');
                        this.listLoader = false;
                    }
                    else if(this.phone_number == ''){
                        toastr.error('Phone number filed is required');
                        this.listLoader = false;
                    }
                    else if(this.email_address == ''){
                        toastr.error('Email filed is required');
                        this.listLoader = false;
                    } else if(this.password == ''){
                        toastr.error('Password filed is required');
                        this.listLoader = false;
                    } else if(counter > 7){
                        toastr.error('you cannot upload more then 6 images');
                        this.listLoader = false;
                    } else{
                        this.$http.post(link,Listform, {
                            headers: {
                                Authorization: tokencsrf,
                                'Content-Type': 'multipart/form-data'
                            }
                        }).then(function(response){
                            console.log(response.data.status);
                            if(response.data.status === false) {
                                $('.imageloader').hide();
                                toastr.error(response.data.message);
                                this.listLoader = false;
                            } else {
                                $('.imageloader').hide();
                                this.listLoader = false;
                                this.companyName = '';
                                this.full_name = '';
                                this.phone_number = '';
                                // this.location = '';
                                loc = '';
                                this.email_address = '';
                                this.file_name = '';
                                toastr.success('Your Space Listed Successfully');
                                setTimeout(function(){
                                    window.location.reload();
                                },100);
                                $('#listSpaceModal').modal('hide');
                            }
                        }, function(error){
                        });
                    }
                },

                getSharedOffices: function (is_append = false) {
                    this.loader = true;
                    let language = window.session.value;
                    let self = this;
                    self.$http.get('/api/sharedoffice/pagination?language='+language+'&sort=desc&location='+urlLocation+'&filterType='+urlType+'&'+ jQuery.param(this.pagination) + '&' + jQuery.param(this.input), {
                        headers: {
                            Authorization: setToken
                        }
                    }).then(function(response){
                        self.loader = false;
                        if (is_append) {
                            self.sharedOffices = self.sharedOffices.concat(response.data.data);
                            console.log(self.sharedOffices);
                        } else {
                            self.sharedOffices = response.data.data;
                        }
                        self.pagination.total_data = response.data.total_data
                        self.sharedOfficesOriginalData = response.data.data;

                        this.scrollEnd();
                    }, function(error){
                    });
                },
                getSharedOfficeByID(name, officeId ,office){
                    sessionStorage.removeItem("setContent");
                    sessionStorage.removeItem("country");
                    var cityName = office.city.name;
                    var countryName = office.country && office.country.name ? office.country.name : office.city.country.name;
                    cityName = cityName.replace(/[^A-Z0-9]/ig, "-").toLowerCase().replace('--', '-').replace('---', '-').replace(/-/g, '-');
                    countryName = countryName.replace(/[^A-Z0-9]/ig, "-").toLowerCase().replace('--', '-').replace('---', '-').replace(/-/g, '-');
                    const office_name = name.replace(/[^A-Z0-9]/ig, "-").toLowerCase().replace('--', '-').replace('---', '-').replace(/-/g, '-');
                    window.location.href= '/'+window.session.value+`/coworking-spaces/${countryName}/${cityName}/${office_name}/${officeId}`;
                },
                getOfficeName(sharedOffice){
                    const locale = '{{\Config::get('app.locale')}}';
                    if(locale === 'cn') {
                        return sharedOffice.shared_office_language_data && sharedOffice.shared_office_language_data.office_name_cn ?sharedOffice.shared_office_language_data.office_name_cn : sharedOffice.office_name;
                    } else {
                        return sharedOffice.office_name;
                    }
                },
                filerRecord(){
                    this.input.space_type = this.type_filter;
                    this.input.continent = this.continent_filter;
                    this.sharedOffices = [];
                    this.pagination.page = 0;
                    this.getSharedOffices();
                },

                rate(rate) {
                    this.input.rating = rate;
                    this.sharedOffices = [];
                    this.pagination.page = 0;
                    this.getSharedOffices();
                    // var arr = [];
                    // this.sharedOfficesOriginalData.map(function(item){
                    //     // console.log(item.rating);
                    //     if (item.rating == rate) {
                    //         arr.push(item);
                    //     }
                    // });
                    // this.sharedOffices = arr;
                },

                seekFilter(event, is_min) {
                    console.log('i clicked seek filter');
                    console.log(event);
                    console.log('event.target.value');
                    console.log(event.target.value);
                    if (is_min) {
                        this.input.min_price = '$'+event.target.value;
                    } else {
                        this.input.max_price = '$'+event.target.value;
                    }

                    this.sharedOffices = [];
                    this.pagination.page = 0;
                    this.getSharedOffices();
                    // var arr = [];
                    // this.sharedOfficesOriginalData.map(function(item){
                    //     if (item.max_seat_price != null) {
                    //         if (item.max_seat_price <= val.target.value) {
                    //             arr.push(item);
                    //         }
                    //     } else {
                    //         if (item.meeting_room_price != null) {
                    //             if (item.meeting_room_price <= val.target.value) {
                    //                 arr.push(item);
                    //             }
                    //         }
                    //     }
                    // });
                    // console.log('arr');
                    // console.log(arr);
                    // console.log('arr');
                    // this.sharedOffices = arr;
                },
                scrollEnd: function() {
                    window.onscroll = () => {
                        if($(window).scrollTop() + $(window).height() >= $('.main-section').height()) {
                            if (this.sharedOffices.length > 0 && this.sharedOffices.length % 10 == 0 && this.loader == false) {
                                this.pagination.page++;
                                this.getSharedOffices(true);
                            }
                        }
                    }
                },
                calculatePrice: function(sharedoffice) {
                    var response = null;
                    var listPriority = ['month_price', 'weekly_price', 'daily_price', 'hourly_price'];
                    var listLabel = [' / Month', ' / Week', ' / Day', ' / Hour'];
                    $.each(listPriority, function(index_priority, el) {
                        $.each(sharedoffice.products, (index_product, product) => {
                            if (product[el] > 0) {
                                response = '$' + product[el] + listLabel[index_priority];
                                return false;
                            }
                        });
                
                        if (response != null) {
                            return false;
                        }
                    });

                    if (response == null) {
                        response = 'Free Plan';
                    }
                    return response;
                }
            },
            mounted(){
                var uri = window.location.href;
                var location = '{{$location}}';
                const type = '{{$filterType}}';
                console.log(location);
                console.log(type);
                this.getSharedOffices();
                // this.filterRecord();
            }
        });

        $('#crossimages').click(function(){
            document.getElementById("pac-input").value = '';
        });
    </script>
@endpush
