@section("script")
    <script>
        $(function() {
            if (window.matchMedia("(max-width: 556px)").matches) {
                $(".preload").fadeOut(2000, function() {
                    $(".newest-spaces-box").fadeIn(1000);
                });
            }
        });
    </script>

    <!-- CSS -->
    <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
    <!-- JavaScript -->
    <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
    <script>
        setTimeout(function(){
            $('.main-carousel').flickity();
        },3000);
    </script>


    <script>
        $('.menu-opener').hide();
        $('.mobile-view-h2-head').html('');
        $('.mobile-view-h2-head').html('Newest Spaces Available');
        $('.image-background-mobile-view-container').css('display','none');
        $('.add-arrow-left').css(
            'display','block',
        );
        $('.add-arrow-left').html('' +
            '<i class="fas fa-arrow-left"  onclick="goBack()"></i>' +
            '');
        $('.set0images').html('' +
            '<img src="/images/Filter Iconn.png" class="set-image-mobile-responsive-view">' +
            '');

        function goBack()
        {
            window.history.go(-1);
        }
    </script>

    <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCIasatwSA8nyOeN8Cobzil6yO1jsT13rE&callback=initMap&libraries=geometr‌​y,places,controls"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script>
        var token = '{{ session('api_token') }}';
        var setToken = `Bearer ${token}`;
        new Vue({
            el: '#app',
            data: {
                clientIp: '{{\Request::getClientIp(true)}}',
                sharedOffices:[],
                file_name:'',
                full_name:'',
                phone_number:'',
                email_address:'',
                companyName:'',
                location :'',
                filename:'',
                locationOfMap: '',
                fillstars:'',
                type_filter:'',
                multi_image:'',
                office_images:'',
                getimages:[],
                images:[],
                listLoader:false,
                password:'',
            },
            methods: {
                getOfficeImages(images,index)
                {
                    console.log("images");
                    console.log(index);
                    console.log(images.image);

                    return images.image ? images.image : '' ;
                },

                getOfficeNameSTR(sharedOffice)
                {
                    try {
                        const locale = '{{\Config::get('app.locale')}}';
                        let officeName = sharedOffice.office_name ? sharedOffice.office_name: sharedOffice.shared_office_language_data.office_name_cn
                        if (locale === 'cn') {
                            if (sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'true') {
                                officeName = sharedOffice.shared_office_language_data.office_name_cn;
                            } else if (sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'false') {
                                officeName = sharedOffice.shared_office_language_data.office_name_cn;
                            } else if (sharedOffice.version_chinese === 'false' && sharedOffice.version_english === 'true') {
                                officeName = sharedOffice.office_name;
                            }
                        } else if (locale === 'en') {
                            if (sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'true') {
                                officeName = sharedOffice.office_name;
                            }
                            else if (sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'false') {
                                officeName = sharedOffice.shared_office_language_data && sharedOffice.shared_office_language_data.office_name_cn;
                            } else if (sharedOffice.version_chinese === 'false' && sharedOffice.version_english === 'true') {
                                officeName = sharedOffice.office_name;
                            }
                        }
                        return (officeName.length) >= 25 ? officeName.substring(0, 25) + '...' : officeName;
                    } catch (e) {
                        return '';
                    }
                },

                getDate(offices)
                {
                    var date = moment(offices.created_at).fromNow();
                    return date;
                },

                getsharedOfficeLocationSTR(sharedOffice)
                {

                    if(sharedOffice.location && sharedOffice.location.length && sharedOffice.location.length > 9)
                    {
                        return sharedOffice.location.substring(0,15) + '...';
                    }
                    return sharedOffice.location;
                },

                getSharedOfficeRatingMV(offices)
                {
                    var stars = offices.shared_office_rating ? offices.shared_office_rating : 0;
                    var getstars = '';
                    stars.map(function(values){
                        getstars = Math.round(values.avg);
                    });
                    return getstars ? getstars : 0;
                },

                getStarsFillable(offices)
                {
                    let stars = offices.shared_office_rating;
                    var fillablestars = '';
                    stars.map(function(starsValue){
                        fillablestars = starsValue
                        this.fillstars = fillablestars;
                        return this.fillstars
                    });
                },

                getSharedOffices(){

                    this.$http.get('/api/sharedoffice?sort=desc&limit=20', {
                        headers: {
                            Authorization: setToken
                        }
                    }).then(function(response){
                        this.sharedOffices = response.data;
                    }, function(error){
                    });
                },

                getSharedOfficeByID(name, officeId){

                    const locale = '{{\Config::get('app.locale')}}';
                    // console.log(locale);
                    if(locale === 'cn') {
                        if(name.version_chinese === 'true' && name.version_english === 'true') {
                            const office_name = name.shared_office_language_data.office_name_cn;
                            window.location.href= `cn/coworking-spaces/${office_name}/${officeId}`;
                        } else if(name.version_chinese === 'true' && name.version_english === 'false') {
                            const office_name = name.shared_office_language_data.office_name_cn;
                            window.location.href= `cn/coworking-spaces/${office_name}/${officeId}`;
                        } else if(name.version_chinese === 'false' && name.version_english === 'true') {
                            const officeName = name.office_name;
                            const office_name = officeName.replace(/[^A-Z0-9]/ig, "-").toLowerCase().replace('--', '-').replace('---', '-').replace(/-/g, '-');
                            window.location.href= `cn/coworking-spaces/${office_name}/${officeId}`;
                        } else {
                            const officeName = name.office_name;
                            const office_name = officeName.replace(/[^A-Z0-9]/ig, "-").toLowerCase().replace('--', '-').replace('---', '-').replace(/-/g, '-');
                            window.location.href= `cn/coworking-spaces/${office_name}/${officeId}`;
                        }
                    } else if(locale === 'en') {
                        if(name.version_chinese === 'true' && name.version_english === 'true') {
                            const officeName = name.office_name;
                            const office_name = officeName.replace(/[^A-Z0-9]/ig, "-").toLowerCase().replace('--', '-').replace('---', '-').replace(/-/g, '-');
                            window.location.href= `/en/coworking-spaces/${office_name}/${officeId}`;
                        }
                        else if(name.version_chinese === 'true' && name.version_english === 'false') {
                            const  officeName = name.shared_office_language_data.office_name_cn;
                            const office_name = officeName.replace(/[^A-Z0-9]/ig, "-").toLowerCase().replace('--', '-').replace('---', '-').replace(/-/g, '-');
                            window.location.href= `/en/coworking-spaces/${office_name}/${officeId}`;
                        } else if(name.version_chinese === 'false' && name.version_english === 'true') {
                            const officeName = name.office_name;
                            const office_name = officeName.replace(/[^A-Z0-9]/ig, "-").toLowerCase().replace('--', '-').replace('---', '-').replace(/-/g, '-');
                            window.location.href= `/en/coworking-spaces/${office_name}/${officeId}`;
                        } else {
                            const office_name = name.office_name ? name.office_name : name.shared_office_language_data.office_name_cn;
                            window.location.href= `/en/coworking-spaces/${office_name}/${officeId}`;
                        }
                    }
                }
            },

            mounted(){
                this.getSharedOffices();
            }
        });

        $('#crossimages').click(function(){
            document.getElementById("pac-input").value = '';
        });
    </script>
@endsection
