@section('script')
<script>

    $('.mobile-view-h2-head').html('Search Result');
    $('.set0images').html('' +
        '<img src="/images/Filter Iconn.png" class="set-image-mobile-responsive-view">' +
        '');

    $(function() {
        if (window.matchMedia("(max-width: 556px)").matches) {
            $(".preload").fadeOut(2000, function() {
                $(".set-container-tabs-girdandlist").fadeIn(1000);
                $("#btnContainer").fadeIn(1000);
            });
            $(".preloadsmall").fadeOut(2000, function() {
                $(".set-box-container").fadeIn(1000);
            });
        }
    });
</script>
<!-- grid view and list view tabs scripts -->
<script>
    // Get the elements with class="column"
    var elements = document.getElementsByClassName("listviewcolumn");

    // Declare a loop variable
    var i;

    // List View
    function listView() {
        $('.fa-fa-fabars-list').addClass('activeViews');
        $('.fa-fa-fabars-grid').removeClass('activeViews');
        $('.listviewcolumn').css('display','block');
        $('.gridviewcolumn').css('display','none');
        for (i = 0; i < elements.length; i++) {
            elements[i].style.width = "99%";
        }
    }

    // Get the elements with class="column"
    var elementss = document.getElementsByClassName("gridviewcolumn");

    var c;
    // Grid View
    function gridView() {
        $('.fa-fa-fabars-grid').addClass('activeViews');
        $('.fa-fa-fabars-list').removeClass('activeViews');
        $('.gridviewcolumn').css('display','grid');
        $('.listviewcolumn').css('display','none');
        for (c = 0; c < elementss.length; c++) {
            elementss[c].style.width = "100%";
        }
    }

    setTimeout(function(){
        $('.fa-fa-fabars-list').addClass('activeViews');
        listView();
    },5);

</script>
<!-- grid view and list view tabs scripts -->
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCIasatwSA8nyOeN8Cobzil6yO1jsT13rE&callback=initMap&libraries=places,controls"></script>
<script src="{{asset('custom/js/ListSpaceMap.js')}}"></script>
<script>
    function initMap() {
        initListMap();
    }
</script>
@endsection

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script>
        var token = '{{ session('api_token') }}';
        var setToken = `Bearer ${token}`;
        new Vue({
            el: '#app',
            data: {
                continent_filter:[],
                type_filter:[],
                sharedOffices:[],
                sharedOfficesOriginalData:[],
                file_name:'',
                full_name:'',
                phone_number:'',
                email_address:'',
                companyName:'',
                location :'',
                filename:'',
                countrycity_name:'',
                country_name:'',
                listLoader:false,
                password:'',
            },
            methods: {

                getsharedofficeMobile()
                {
                    var th = this;
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(
                            function (position) {
                                //do work work here
                                var long = position.coords.longitude;
                                var lat = position.coords.latitude;
                                th.$http.get('/api/sharedoffice?sort=desc&limit=20&mobileApp=true&lat='+lat+'&lang='+long, {
                                    headers: {
                                        Authorization: setToken
                                    }
                                }).then(function(response){
                                    this.sharedOffices = response.data.data;
                                    var citycountryName = [];
                                    var countryName = [];
                                    this.sharedOffices.map(function(data){
                                        latitude = data.lat;
                                        longitude = data.lng;
                                        var geocoder;
                                        geocoder = new google.maps.Geocoder();
                                        var latlng = new google.maps.LatLng(latitude, longitude);
                                        geocoder.geocode(
                                            {'latLng': latlng},
                                            function(results, status) {
                                                if (status == google.maps.GeocoderStatus.OK) {
                                                    if (results[0]) {
                                                        var add = results[0].formatted_address;
                                                        var value = add.split(",");
                                                        var names = value[2] + ',' + value[4];
                                                        var country_name = value[4];
                                                        countryName.push(country_name);
                                                        var cleanName = names.replace(/^\s+|\s+$/g, '');
                                                        citycountryName.push(cleanName);
                                                    }
                                                }
                                            }
                                        );
                                    });
                                    this.countrycity_name = citycountryName;
                                    this.country_name = countryName;


                                }, function(error){
                                });
                            },
                            function (error) {
                                // console.log("error");
                                // console.log(error.message);
                                // console.log("error");
                                var ip = th.clientIp;
                                // console.log(ip);
                                th.$http.get('/api/sharedoffice?sort=desc&limit=20&mobileApp=true&ip='+ip, {
                                    headers: {
                                        Authorization: setToken
                                    }
                                }).then(function(response){
                                    this.sharedOffices = response.data.data;
                                    var citycountryName = [];
                                    var countryName = [];
                                    this.sharedOffices.map(function(data){
                                        latitude = data.lat;
                                        longitude = data.lng;
                                        var geocoder;
                                        geocoder = new google.maps.Geocoder();
                                        var latlng = new google.maps.LatLng(latitude, longitude);
                                        geocoder.geocode(
                                            {'latLng': latlng},
                                            function(results, status) {
                                                if (status == google.maps.GeocoderStatus.OK) {
                                                    if (results[0]) {
                                                        var add = results[0].formatted_address;
                                                        var value = add.split(",");
                                                        var names = value[2] + ',' + value[4];
                                                        var country_name = value[4];
                                                        countryName.push(country_name);
                                                        var cleanName = names.replace(/^\s+|\s+$/g, '');
                                                        citycountryName.push(cleanName);
                                                    }
                                                }
                                            }
                                        );
                                    });
                                    this.countrycity_name = citycountryName;
                                    this.country_name = countryName;
                                }, function(error){
                                });

                            },
                            {
                                enableHighAccuracy: true
                                , timeout: 5000
                            }
                        );
                    } else {
                        alert("Geolocation is not supported by this browser.");
                    }
                },


                getOfficeNameMV(sharedOffice)
                {
                    try {
                        const locale = '{{\Config::get('app.locale')}}';
                        let officeName = sharedOffice.office_name ? sharedOffice.office_name: sharedOffice.shared_office_language_data.office_name_cn
                        if (locale === 'cn') {
                            if (sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'true') {
                                officeName = sharedOffice.shared_office_language_data.office_name_cn;
                            } else if (sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'false') {
                                officeName = sharedOffice.shared_office_language_data.office_name_cn;
                            } else if (sharedOffice.version_chinese === 'false' && sharedOffice.version_english === 'true') {
                                officeName = sharedOffice.office_name;
                            }
                        } else if (locale === 'en') {
                            if (sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'true') {
                                officeName = sharedOffice.office_name;
                            }
                            else if (sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'false') {
                                officeName = sharedOffice.shared_office_language_data && sharedOffice.shared_office_language_data.office_name_cn;
                            } else if (sharedOffice.version_chinese === 'false' && sharedOffice.version_english === 'true') {
                                officeName = sharedOffice.office_name;
                            }
                        }
                        return (officeName.length) >= 18 ? officeName.substring(0, 18) + '...' : officeName;
                    } catch (e) {
                        return '';
                    }
                },


                getSharedOfficeRatingMVS(offices)
                {
                    var stars = offices.shared_office_rating ? offices.shared_office_rating : 0;
                    var getstars = '';
                    stars.map(function(values){
                        getstars = Math.round(values.avg);
                    });
                    return getstars ? getstars : 0;
                },

                getsharedOfficeLocation(sharedOffice)
                {
                    if(sharedOffice.location && sharedOffice.location.length && sharedOffice.location.length > 20)
                    {
                        return sharedOffice.location.substring(0,20) + '...';
                    }
                    return sharedOffice.location;
                },

                getsharedOfficedesc(sharedOffice)
                {
                    if(sharedOffice.description && sharedOffice.description.length && sharedOffice.description.length > 10)
                    {
                        return sharedOffice.description.substring(0,60);
                    }
                    return sharedOffice.description;
                },

                uploadListForm() {
                    this.listLoader = true;
                    let tokencsrf = '{{ csrf_token() }}';
                    let Listform = new FormData();
                    var files  = $('#files')[0].files;
                    let counter = 0;
                    $.each(files , function(key, value) {
                        Listform.append('image_'+key, value);
                        counter++;
                    });
                    Listform.append('company_name',this.companyName);
                    Listform.append('full_name',this.full_name);
                    Listform.append('phone_number',this.phone_number);
                    Listform.append('location',this.location);
                    Listform.append('email',this.email_address);
                    Listform.append('file_name',$('#files')[0].files[0]);
                    Listform.append('password',this.password);
                    Listform.append('counter',counter);
                    this.filename = $('#files')[0].files[0];
                    let link = '/shared-office-list-space-request';

                    if(this.companyName == '')
                    {
                        toastr.error('{{trans('validation.company_required')}}');
                        this.listLoader = false;
                    }
                    else if(this.location == '')
                    {
                        toastr.error('{{trans('validation.location_required')}}');
                        this.listLoader = false;
                    }
                    else if(this.filename == undefined || this.filename == '') {
                        toastr.error('{{trans('validation.image_required')}}');
                        this.listLoader = false;
                    }
                    else if(this.phone_number == ''){
                        toastr.error('Phone number filed is required');
                        this.listLoader = false;
                    }
                    else if(this.email_address == ''){
                        toastr.error('Email filed is required');
                        this.listLoader = false;
                    } else if(this.password == ''){
                        toastr.error('Password filed is required');
                        this.listLoader = false;
                    } else if(counter > 7){
                        toastr.error('you cannot upload more then 6 images');
                        this.listLoader = false;
                    } else{
                        this.$http.post(link,Listform, {
                            headers: {
                                Authorization: tokencsrf,
                                'Content-Type': 'multipart/form-data'
                            }
                        }).then(function(response){
                            console.log(response.data.status);
                            if(response.data.status === false) {
                                $('.imageloader').hide();
                                toastr.error(response.data.message);
                            } else {
                                $('.imageloader').hide();
                                this.listLoader = false;
                                this.companyName = '';
                                this.full_name = '';
                                this.phone_number = '';
                                this.location = '';
                                this.email_address = '';
                                this.file_name = '';
                                toastr.success('Your Space Listed Successfully');
                                setTimeout(function(){
                                    window.location.reload();
                                },100);
                                $('#listSpaceModal').modal('hide');
                            }
                        }, function(error){
                        });
                    }
                },

                getSharedOfficeByID(name, officeId){
                    sessionStorage.removeItem("setContent");
                    sessionStorage.removeItem("country");
                    const office_name = name.replace(/[^A-Z0-9]/ig, "-").toLowerCase().replace('--', '-').replace('---', '-').replace(/-/g, '-');
                    window.location.href= `/coworking-spaces/${office_name}/${officeId}`;
                },
                getOfficeName(sharedOffice){
                    const locale = '{{\Config::get('app.locale')}}';
                    if(locale === 'cn') {
                        return sharedOffice.shared_office_language_data && sharedOffice.shared_office_language_data.office_name_cn ?sharedOffice.shared_office_language_data.office_name_cn : sharedOffice.office_name;
                    } else {
                        return sharedOffice.office_name;
                    }
                },

            },
            mounted(){
                this.getsharedofficeMobile();
            }
        });

        $('#crossimages').click(function(){
            document.getElementById("pac-input").value = '';
        });
    </script>
@endpush
