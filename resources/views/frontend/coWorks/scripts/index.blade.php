@section("script")
    <script>
        $(function() {
            if (window.matchMedia("(max-width: 556px)").matches) {
                $(".preload").fadeOut(2000, function() {
                    $(".mobile-web-responsive-devices").fadeIn(1000);
                });
            }
        });
    </script>
{{--    <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCIasatwSA8nyOeN8Cobzil6yO1jsT13rE&callback=initMap&libraries=geometr‌​y,places,controls" async defer></script>--}}
{{--    <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCIasatwSA8nyOeN8Cobzil6yO1jsT13rE&callback=initListMap&libraries=geometr‌​y,places,controls" async defer></script>--}}
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
    <script>
        function getShareddOfficeById(officeId) {
            var officeName = sessionStorage.getItem('office_name').replace(' ','-').replace(' - ','-');
            var Country = sessionStorage.getItem('country').replace(' ','-');
            var City = sessionStorage.getItem('city').replace(' ','-');
            console.log(officeId);
            console.log(officeName);
            console.log(Country);
            console.log(City);
            console.log('check city country office_name office_id');
            console.log(check ,  city ,  country ,  officeName , officeId);
            const locale = '{{\Config::get('app.locale')}}';
            if (locale === 'cn') {
                const office_name = officeName.replace(/\s+/g, '');
                window.location.href = `/cn/coworking-spaces/${Country}/${City}/${office_name}/${officeId}`;
            } else if (locale === 'en') {
                const office_name = officeName.replace(/\s+/g, '');
                window.location.href = `/en/coworking-spaces/${Country}/${City}/${office_name}/${officeId}`;
            }
        }

        map = null;
        var getGoogleClusterInlineSvg = function (color) {
            var encoded = window.btoa('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="-100 -100 200 200"><defs><g id="a" transform="rotate(45)"></g></defs><g fill="' + color + '"><circle r="80"/><use xlink:href="#a"/><g transform="rotate(120)"><use xlink:href="#a"/></g><g transform="rotate(240)"><use xlink:href="#a"/></g></g></svg>');

            return ('data:image/svg+xml;base64,' + encoded);
        };
        var cluster_styles = [
            {
                width: 40,
                height: 40,
                url: getGoogleClusterInlineSvg('#5ec329'),
                textColor: 'white',
                textSize: 12,
            },
            {
                width: 50,
                height: 50,
                url: getGoogleClusterInlineSvg('#5ec329'),
                textColor: 'white',
                textSize: 14,
            },
            {
                width: 60,
                height: 60,
                url: getGoogleClusterInlineSvg('#5ec329'),
                textColor: 'white',
                textSize: 16,
            }
            //up to 5
        ];
        function setMarkerClick(marker, location, infoWindow) {
            infoWindow.setContent('hello how are you');
            infoWindow.open(map, marker);
        }

        function computedMapData(response) {
            infoWindow = new google.maps.InfoWindow({
                content: ''
            });
            response = Array.isArray(response) ? response : JSON.parse(response);
            let C_markers = [];
            var markers = response.map(function (data, i) {
                data.map(function (location) {
                let marker  = new google.maps.Marker({
                    position: new google.maps.LatLng( parseFloat(location.lat), parseFloat(location.lng) ),
                    // label: location.office_name,
                    icon: '{{asset('images/markerImage.png')}}',
                    scale: 10
                });

                C_markers.push(marker);

                let rating = location.avg;
                // var review = Math.round(rating);
                var rateRange = Math.round(rating);
                var price   =  '';
                if(location.min_seat_price) {
                    price = location.min_seat_price.month_price;
                }
                var monthPrice = price ? price : 0;
                var totalStars = 5;
                var range = totalStars - rateRange;
                var starsHtml = '';
                for ($i = 0; $i < rateRange; $i++) {
                    starsHtml += '<i class="fa fa-star set-fa-fa-active-color" id="set-fa-fa-color-1" aria-hidden="true"></i>\n'
                }
                for ($i = 0; $i < range; $i++) {
                    starsHtml += '<i class="fa fa-star" id="set-fa-fa-color-1" aria-hidden="true"></i>\n'
                }
                google.maps.event.addListener(marker, 'mouseover', function(evt) {
                        var iwContent =
                            '<div onclick="getShareddOfficeById('+location.id+')">' +
                            '<img class="set-shred-officess-images" src="/'+location.image+'">'
                            +'<br>'+'' +
                            '<div class="row rowM"><div class="col-md-12 set-col-md-12-office-name">' +
                            '<span class="set-shared-office-p-price">'+location.office_name+'</span>' +
                            '</div>' +
                            '</div>'+
                            '<br>'
                            +'<div class="row rowM"><div class="col-md-12 set-col-md-12-price-row">' +
                            '<span class="set-dollar-sign-2-dollar-sign">'+"$"+'</span>'+'' +
                            '<span class="set-shared-office-p-price">'+monthPrice+'</span>'+'' +
                            '<span class="set-monthly-area">'+'/'+'Monthly'+'</span>' +
                            '</div>' +
                            '</div>'
                            + '<div class="row rowM"><div class="col-md-12 set-col-md-12-popover-box">'+
                            '<div class="col-md-7 set-col-md-6-fa-fa-stars">' + starsHtml
                            +'</div>' +
                            '<div class="col-md-5 set-col-5-md-revirews">'
                            +'<span>'+rateRange+' '+'Reviews'
                            +'</span>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>';

                        infoWindow.setContent(iwContent);
                        infoWindow.open(map, marker);
                        sessionStorage.setItem('office_name',location.office_name);
                        sessionStorage.setItem('country',location.country_name);
                        sessionStorage.setItem('city',location.city_name);
                });
                google.maps.event.addListener(map, 'mouseout', function() {
                    infoWindow.close();
                });
                return marker;
                });
            });
            var clusterOptions = {
                styles: cluster_styles,
                gridSize:50,
                maxZoom:15,
                minimumClusterSize:1,
                animate:true
            };
            var markerCluster = new MarkerClusterer(map, C_markers, clusterOptions);
            setTimeout(function(){
                $('.gm-style-pbc ~ div > div > div:nth-child(3) div').each(function () {
                    console.log($(this));
                    $(this).prop('Counter',0).animate({
                        Counter: $(this).text()
                    }, {
                        duration: 3000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(Math.ceil(now));
                        }
                    });
                });
            },100)
        }

        function goForMapNow(lat, lng){
            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 8,
                center: {lat: lat, lng: lng}
            });

            var time = new Date().getTime()
            var mapData = localStorage.getItem('map_data') != null ?
                JSON.parse(localStorage.getItem('map_data')) : null
            if (mapData == null || mapData.expired <= time ) {
                let setToken = `Bearer ${token}`;
                let link = '/get-map-data';
                $.ajax({
                    url: link,
                    headers: {
                        '_token': '{{csrf_token()}}'
                    },
                    success: function (response) {
                        // set expired to 1 day
                        time = new Date().getTime() + (24*60*60*1000)
                        localStorage.setItem('map_data', JSON.stringify({data: response, expired: time}))
                        computedMapData(response)
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                computedMapData(mapData.data)
            }
        }

        function initMap() {

            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    goForMapNow(position.coords.latitude, position.coords.longitude)
                }, function() {
                    console.log('no location access');
                    $.getJSON('https://ipapi.co/json/', function (result) {
                        console.log(result);
                        goForMapNow(parseFloat(result.latitude), parseFloat(result.longitude))
                    });
                });
            } else {
                console.log('no location access');
                $.getJSON('https://ipapi.co/json/', function (result) {
                    console.log(result);
                    goForMapNow(parseFloat(result.latitude), parseFloat(result.longitude))
                });
                // $.getJSON('http://api.wipmania.com/jsonp?callback=?', function (data) {
                //     goForMapNow(parseFloat(data.latitude), parseFloat(data.longitude))
                // });
            }

        }

    </script>
{{--    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.11"></script>--}}
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.5.1/vue-resource.min.js"></script>--}}
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>--}}
    {{-- below files use resources thats why we use cdn accord to client 22-01-2020 --}}
    <script src="{{asset('js/vue.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/vue-resource.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('custom/js/ListSpaceMap.js')}}"></script>
    <script>
        var token = '{{ session('api_token') }}';
        var setToken = `Bearer ${token}`;
        new Vue({
            el: '#app',
            data: {
                clientIp: '{{\Request::getClientIp(true)}}',
                sharedOffices:[],
                sharedOfficesOriginalData:[],
                sharedOfficesActive:[],
                sharedOfficesInactive:[],
                file_name:'',
                full_name:'',
                phone_number:'',
                email_address:'',
                companyName:'',
                location :'',
                userLocation: '',
                filename:'',
                locationOfMap: '',
                fillstars:'',
                type_filter:'',
                sharedOfficesMobile:[],
                sharedOfficesMobileDevice:[],
                multi_image:'',
                countrycity_name:'',
                country_name:'',
                listLoader:false,
                password:'',
                latitude: '',
                longitude: '',
                dotsLoader:true,
            },
            methods: {

                randomList(offices)
                {
                    var arry = Object.entries(offices);
                    var shuffle = arry.sort(function(){return 0.5 - Math.random()});
                    var arr = [];
                    var obj = shuffle.map(function(data) {
                        arr.push(data[1]);
                    });
                    return arr;
                },

                newestSpaces()
                {
                    const locale = '{{\Config::get('app.locale')}}';

                    if(locale === 'cn') {
                        window.location.href=`/cn/coworking-spaces/newest-co-work-spaces`;

                    } else{
                        window.location.href=`/en/coworking-spaces/newest-co-work-spaces`;
                    }
                },

                filterRecord(type)
                {
                    var arr = [];
                    if(type > 0)
                    {
                        this.sharedOfficesOriginalData.map(function(item){
                            if(type == 1)
                            {
                                arr.push(item);
                            }
                            if(type == 2){
                                if(item.total_hot_desk && item.total_hot_desk)  {
                                    arr.push(item);
                                }
                            }
                            if(type == 3){
                                if(item.total_dedicated_desks && item.total_dedicated_desks) {
                                    arr.push(item);
                                }
                            }
                            if(type == 4){
                                if(item.total_hot_desk && item.total_meeting_room) {
                                    arr.push(item);
                                }
                            }
                        });
                        this.sharedOffices = arr;
                    } else {
                        this.sharedOffices = this.sharedOfficesOriginalData;
                    }
                },

                getOfficeNameSTR(sharedOffice)
                {
                    try {
                        const locale = '{{\Config::get('app.locale')}}';
                        let officeName = sharedOffice.office_name ? sharedOffice.office_name: sharedOffice.shared_office_language_data.office_name_cn
                        if (locale === 'cn') {
                            if (sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'true') {
                                officeName = sharedOffice.shared_office_language_data.office_name_cn;
                            } else if (sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'false') {
                                officeName = sharedOffice.shared_office_language_data.office_name_cn;
                            } else if (sharedOffice.version_chinese === 'false' && sharedOffice.version_english === 'true') {
                                officeName = sharedOffice.office_name;
                            }
                        } else if (locale === 'en') {
                            if (sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'true') {
                                officeName = sharedOffice.office_name;
                            }
                            else if (sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'false') {
                                officeName = sharedOffice.shared_office_language_data && sharedOffice.shared_office_language_data.office_name_cn;
                            } else if (sharedOffice.version_chinese === 'false' && sharedOffice.version_english === 'true') {
                                officeName = sharedOffice.office_name;
                            }
                        }
                        return (officeName.length) >= 18 ? officeName.substring(0, 10) + '...' : officeName;
                    } catch (e) {
                        return '';
                    }
                },

                getDate(offices)
                {
                    var date = moment(offices.created_at).fromNow();
                    return date;
                },

                uploadListForm() {
                    this.listLoader = true;
                    var loc =  $('#pac-input').val();
                    console.log("loc");
                    console.log(loc);

                    let tokencsrf = '{{ csrf_token() }}';
                    let Listform = new FormData();
                    var files  = $('#files')[0].files;
                    let counter = 0;
                    $.each(files , function(key, value) {
                        Listform.append('image_'+key, value);
                        counter++;
                    });
                Listform.append('company_name',this.companyName);
                Listform.append('full_name',this.full_name);
                Listform.append('phone_number',this.phone_number);
                Listform.append('location',loc);
                Listform.append('email',this.email_address);
                Listform.append('file_name',$('#files')[0].files[0]);
                Listform.append('password',this.password);
                Listform.append('counter',counter);
                this.filename = $('#files')[0].files[0];
                let link = '/shared-office-list-space-request';

                        if(this.companyName == '')
                        {
                                toastr.error('{{trans('validation.company_required')}}');
                                this.listLoader = false;
                        }
                        else if(loc == '')
                        {
                                toastr.error('{{trans('validation.location_required')}}');
                            this.listLoader = false;
                        }
                        else if(this.filename == undefined || this.filename == '') {
                            toastr.error('{{trans('validation.image_required')}}');
                            this.listLoader = false;
                        }
                        else if(this.phone_number == ''){
                            toastr.error('Phone number filed is required');
                            this.listLoader = false;
                        }
                        else if(this.email_address == ''){
                            toastr.error('Email filed is required');
                            this.listLoader = false;
                        } else if(this.password == ''){
                            toastr.error('Password filed is required');
                            this.listLoader = false;
                        } else if(counter > 7){
                            toastr.error('you cannot upload more then 6 images');
                            this.listLoader = false;
                        } else{
                        this.$http.post(link,Listform, {
                            headers: {
                                Authorization: tokencsrf,
                                'Content-Type': 'multipart/form-data'
                            }
                        }).then(function(response){
                            console.log(response.data.status);
                            if(response.data.status === false) {
                                $('.imageloader').hide();
                                toastr.error(response.data.message);
                                this.listLoader = false;
                            } else {
                                $('.imageloader').hide();
                                this.listLoader = false;
                                this.companyName = '';
                                this.full_name = '';
                                this.phone_number = '';
                                // this.location = '';
                                loc = '';
                                this.email_address = '';
                                this.file_name = '';
                                toastr.success('Your Space Listed Successfully');
                                setTimeout(function(){
                                    window.location.reload();
                                },100);
                                $('#listSpaceModal').modal('hide');
                            }
                        }, function(error){
                        });
                    }
                },

                getLocation() {
                            console.log("click event");
                            if (navigator.geolocation) {
                                navigator.geolocation.getCurrentPosition(showPosition,showError);
                            } else {
                                toastr.error('Geolocation is not supported by this browser.');
                            }

                            function showPosition(position) {
                                lat = position.coords.latitude;
                                long = position.coords.longitude;
                                displayLocation(lat,long);
                                showLocationMap(lat,long)
                            }
                            function showError(error) {
                                console.log("error");
                                console.log(error);
                                console.log('no location access');
                                $.getJSON('https://ipapi.co/json/', function (result) {
                                    lat = result.latitude;
                                    long = result.longitude;
                                    showLocationMap(lat,long);
                                    var city = result.city;
                                    var country = result.country_name;
                                    var fullName = city+','+country;
                                    $('#pac-input').val(fullName);
                                });
                            }

                            function showLocationMap(latitude,longitude) {
                                //The location on the map.
                                var mapLocation = new google.maps.LatLng(latitude,longitude);
                                //Map options.
                                var options = {
                                    center: mapLocation, //Set center.
                                    zoom: 7, //The zoom value.
                                    mapTypeId: 'roadmap'
                                };
                                //Create the map object.
                                var listmap = new google.maps.Map(document.getElementById('listMap'), options);
                                var latlng = {lat:latitude , lng:longitude};
                                //Create the marker.
                                marker = new google.maps.Marker({
                                    position: latlng,
                                    title: 'place',
                                    map: listmap
                                });
                            }
                            function displayLocation(latitude,longitude){
                                let that = this;
                                const geocoder = new google.maps.Geocoder();
                                const latlng = new google.maps.LatLng(latitude, longitude);
                                geocoder.geocode(
                                    {'latLng': latlng},
                                    function(results, status) {
                                        if (status == google.maps.GeocoderStatus.OK) {
                                            if (results[0]) {
                                                var add = results[0].formatted_address ;
                                                var  value = add.split(",");
                                                var names = value[2]+','+value[4];
                                                var cleanName = names.replace(/^\s+|\s+$/g, '');
                                                console.log(cleanName);
                                                // this.userLocation = cleanName;
                                                // // console.log("that.userLocation");
                                                // // console.log(that.userLocation);
                                                // // $('.location-input').val(cleanName);
                                                // console.log("this.userLocation");
                                                // console.log(this.userLocation);
                                                $('#pac-input').val(cleanName);
                                            }
                                            else  {
                                                toastr.error('address not found');
                                            }
                                        }
                                        else {
                                            toastr.error('Geocoder failed due to:'+ status);
                                        }
                                    }
                                );
                            }
                 },

                getsharedofficeMobile()
                {
                    var th = this;
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(
                            function (position) {
                                //do work work here
                                var language = window.session.value;
                                var long = position.coords.longitude;
                                var lat = position.coords.latitude;
                                th.$http.get('/api/sharedoffice?sort=desc&limit=20&mobileApp=true&lat='+lat+'&lang='+long+'&language='+language, {
                                    headers: {
                                        Authorization: setToken
                                    }
                                }).then(function(response){
                                    this.sharedOfficesMobileDevice = response.data.data;
                                    var citycountryName = [];
                                    var countryName = [];
                                    this.sharedOfficesMobileDevice.map(function(data){
                                        latitude = data.lat;
                                        longitude = data.lng;
                                        var geocoder;
                                        geocoder = new google.maps.Geocoder();
                                        var latlng = new google.maps.LatLng(latitude, longitude);
                                        geocoder.geocode(
                                            {'latLng': latlng},
                                            function(results, status) {
                                                if (status == google.maps.GeocoderStatus.OK) {
                                                    if (results[0]) {
                                                        var add = results[0].formatted_address;
                                                        var value = add.split(",");
                                                        var names = value[2] + ',' + value[4];
                                                        var country_name = value[4];
                                                        countryName.push(country_name);
                                                        var cleanName = names.replace(/^\s+|\s+$/g, '');
                                                        citycountryName.push(cleanName);
                                                    }
                                                }
                                            }
                                        );
                                    });
                                    this.countrycity_name = citycountryName;
                                    this.country_name = countryName;


                                }, function(error){
                                });
                            },
                            function (error) {
                                var language = window.session.value;
                                var ip = th.clientIp;
                                // console.log(ip);
                                th.$http.get('/api/sharedoffice?sort=desc&limit=20&mobileApp=true&ip='+ip+'&language='+language, {
                                    headers: {
                                        Authorization: setToken
                                    }
                                }).then(function(response){
                                    this.sharedOfficesMobile = response.data.data;
                                    var citycountryName = [];
                                    var countryName = [];
                                    this.sharedOfficesMobile.map(function(data){
                                        latitude = data.lat;
                                        longitude = data.lng;
                                        var geocoder;
                                        geocoder = new google.maps.Geocoder();
                                        var latlng = new google.maps.LatLng(latitude, longitude);
                                        geocoder.geocode(
                                            {'latLng': latlng},
                                            function(results, status) {
                                                if (status == google.maps.GeocoderStatus.OK) {
                                                    if (results[0]) {
                                                        var add = results[0].formatted_address;
                                                        var value = add.split(",");
                                                        var names = value[2] + ',' + value[4];
                                                        var country_name = value[4];
                                                        countryName.push(country_name);
                                                        var cleanName = names.replace(/^\s+|\s+$/g, '');
                                                        citycountryName.push(cleanName);
                                                    }
                                                }
                                            }
                                        );
                                    });
                                    this.countrycity_name = citycountryName;
                                    this.country_name = countryName;
                                }, function(error){
                                });

                            },
                            {
                                enableHighAccuracy: true
                                , timeout: 5000
                            }
                        );
                    } else {
                        alert("Geolocation is not supported by this browser.");
                    }
                },

                getSharedOfficeByID(name, officeId) {
                        var cityName = name.city.name;
                        var countryName = name.country && name.country.name ? name.country.name : name.city.country.name;
                        cityName = cityName.replace(/[^A-Z0-9]/ig, "-").toLowerCase().replace('--', '-').replace('---', '-').replace(/-/g, '-');
                        countryName = countryName.replace(/[^A-Z0-9]/ig, "-").toLowerCase().replace('--', '-').replace('---', '-').replace(/-/g, '-');
                        console.log(cityName);
                        console.log(countryName);
                        if (!name.version_chinese) {
                            name.version_chinese = 'false';
                        }
                        if (!name.version_english) {
                            name.version_english = 'false';
                        }
                        const locale = '{{\Config::get('app.locale')}}';

                        if (!name.office_name || name.office_name == '') {
                            name.office_name = '-';
                        }
                        if (locale === 'cn') {

                            if (name.version_chinese === 'true' && name.version_english === 'true') {
                                const officeName = name.shared_office_language_data.office_name_cn;
                                const office_name = officeName.replace(/\s+/g, '').replace(',', '-');
                                window.location.href = `/cn/coworking-spaces/${countryName}/${cityName}/${office_name}/${officeId}`;
                            } else if (name.version_chinese === 'true' && name.version_english === 'false') {
                                const officeName = name.shared_office_language_data.office_name_cn;
                                const office_name = officeName.replace(/\s+/g, '').replace(',', '-');
                                window.location.href = `/cn/coworking-spaces/${countryName}/${cityName}/${office_name}/${officeId}`;
                            } else if (name.version_chinese === 'false' && name.version_english === 'true') {
                                const officeName = name.office_name;
                                const office_Name = officeName.replace(/\s+/g, '').replace(',', '-');
                                const office_name = office_Name.replace(/[^A-Z0-9]/ig, "-").toLowerCase().replace('--', '-').replace('---', '-').replace(/-/g, '-');
                                window.location.href = `/cn/coworking-spaces/${countryName}/${cityName}/${office_name}/${officeId}`;
                            } else {
                                const officeName = name.office_name;
                                const office_Name = officeName.replace(/\s+/g, '').replace(',', '-');
                                const office_name = office_Name.replace(/[^A-Z0-9]/ig, "-").toLowerCase().replace('--', '-').replace('---', '-').replace(/-/g, '-');

                                window.location.href = `/cn/coworking-spaces/${countryName}/${cityName}/${office_name}/${officeId}`;
                            }
                        } else if (locale === 'en') {
                            if (!name.office_name || name.office_name == '') {
                                name.office_name = '-';
                            }
                            if (name.version_chinese === 'true' && name.version_english === 'true') {
                                const officeName = name.office_name;
                                const office_Name = officeName.replace(/\s+/g, '').replace(',', '-');
                                const office_name = office_Name.replace(/[^A-Z0-9]/ig, "-").toLowerCase().replace('--', '-').replace('---', '-').replace(/-/g, '-');
                                window.location.href = `/en/coworking-spaces/${countryName}/${cityName}/${office_name}/${officeId}`;
                            } else if (name.version_chinese === 'true' && name.version_english === 'false') {
                                const officeName = name.shared_office_language_data.office_name_cn;
                                const office_Name = officeName.replace(/\s+/g, '').replace(',', '-');
                                const office_name = office_Name.replace(/[^A-Z0-9]/ig, "-").toLowerCase().replace('--', '-').replace('---', '-').replace(/-/g, '-');
                                window.location.href = `/en/coworking-spaces/${countryName}/${cityName}/${office_name}/${officeId}`;
                            } else if (name.version_chinese === 'false' && name.version_english === 'true') {
                                const officeName = name.office_name;
                                const office_Name = officeName.replace(/\s+/g, '').replace(',', '-');
                                const office_name = office_Name.replace(/[^A-Z0-9]/ig, "-").toLowerCase().replace('--', '-').replace('---', '-').replace(/-/g, '-');
                                window.location.href = `/en/coworking-spaces/${countryName}/${cityName}/${office_name}/${officeId}`;
                            } else {
                                const officeName = name.office_name ? name.office_name : name.shared_office_language_data.office_name_cn;
                                const office_name = officeName.replace(/\s+/g, '').replace(',', '-');
                                window.location.href = `/en/coworking-spaces/${countryName}/${cityName}/${office_name}/${officeId}`;
                            }
                        }
                    },

                gotoTopSpaces(){
                    const locale = '{{\Config::get('app.locale')}}';

                    if(locale === 'cn') {
                        window.location.href=`/cn/coworking-spaces/top-co-work-spaces`;

                    } else{
                        window.location.href=`/en/coworking-spaces/top-co-work-spaces`;
                    }
                },

                getSharedOffices(){
                    var language = window.session.value;
                    var location = {
                        lat: this.latitude,
                        lang: this.longitude
                    };
                    this.$http.get('/api/sharedoffice?sort=desc&limit=20&language='+language+'&' + jQuery.param( location ), {
                        headers: {
                            Authorization: setToken
                        }
                    }).then(function(response){
                        this.sharedOffices = response.data;
                        this.sharedOfficesOriginalData = response.data;
                        this.sharedOfficesActive = this.sharedOffices.slice(0, 5);
                        this.sharedOfficesInactive = this.sharedOffices.slice(35, 40);
                        let that = this;
                        setTimeout(function () {
                            that.dotsLoader = false;
                        },1000);
                    }, function(error){
                    });
                },

                getOfficeName(sharedOffice){
                    const locale = '{{\Config::get('app.locale')}}';
                    if(!sharedOffice.version_chinese){
                        sharedOffice.version_chinese = 'false';
                    }
                    if(!sharedOffice.version_english){
                        sharedOffice.version_english= 'false';
                    }

                    let officeName = sharedOffice.office_name ? sharedOffice.office_name: sharedOffice.shared_office_language_data.office_name_cn;
                    if (locale === 'cn') {
                        if (sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'true') {
                            officeName = sharedOffice.shared_office_language_data.office_name_cn;
                        } else if (sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'false') {
                            officeName = sharedOffice.shared_office_language_data.office_name_cn;
                        } else if (sharedOffice.version_chinese === 'false' && sharedOffice.version_english === 'true') {
                            officeName = sharedOffice.office_name;
                        }
                    } else if (locale === 'en') {
                        if (sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'true') {
                            officeName = sharedOffice.office_name;
                        }
                        else if (sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'false') {
                            officeName = sharedOffice.shared_office_language_data && sharedOffice.shared_office_language_data.office_name_cn;
                        } else if (sharedOffice.version_chinese === 'false' && sharedOffice.version_english === 'true') {
                            officeName = sharedOffice.office_name;
                        }
                    }
                    return (officeName.length) >= 20 ? officeName.substring(0, 20) + '...' : officeName;
                },

                officeLocation(sharedOffice)
                {
                    if(sharedOffice.location && sharedOffice.location.length && sharedOffice.location.length > 60)
                    {
                        return sharedOffice.location.substring(0,60) + '...';
                    }
                    return sharedOffice.location;
                },

                getSharedOfficeRatingMV(offices)
                {
                    var stars = offices.shared_office_rating ? offices.shared_office_rating : 0;
                    var getstars = '';
                    stars.map(function(values){
                        getstars = Math.round(values.avg);
                    });
                    return getstars ? getstars : 0;
                },
                getStarsFillable(offices)
                {
                    let stars = offices.shared_office_rating;
                    var fillablestars = '';
                    stars.map(function(starsValue){
                        fillablestars = starsValue
                        this.fillstars = fillablestars;
                        return this.fillstars
                    });
                },

                getsharedOfficeLocationSTR(sharedOffice)
                {

                    if(sharedOffice.location && sharedOffice.location.length && sharedOffice.location.length > 9)
                    {
                        return sharedOffice.location.substring(0,15) + '...';
                    }
                    return sharedOffice.location;
                },
                getLikeCountSharedOffice(sharedoffice)
                {
                    return sharedoffice.shared_office_like_count.length ? sharedoffice.shared_office_like_count.length : '0';
                },
                getCurrentLocation() {
                    // Try HTML5 geolocation.
                    if (navigator.geolocation) {
                        console.log('location allowed')
                        navigator.geolocation.getCurrentPosition((position) => {
                            console.log(position)
                            this.latitude = position.coords.latitude
                            this.longitude = position.coords.longitude
                            this.getSharedOffices()
                            this.filterRecord()
                            if (window.matchMedia("(max-width: 556px)").matches) {
                                this.getsharedofficeMobile()
                            }
                        }, () => {
                            console.log('no location access');
                            $.getJSON('https://ipapi.co/json/', (result) => {
                                console.log(result)
                                this.latitude = parseFloat(result.latitude)
                                this.longitude = parseFloat(result.longitude)
                                this.getSharedOffices()
                                this.filterRecord()
                                if (window.matchMedia("(max-width: 556px)").matches) {
                                    this.getsharedofficeMobile()
                                }
                            });
                        });
                    } else {
                        console.log('no location access');
                        $.getJSON('https://ipapi.co/json/', (result) => {
                            console.log(result)
                            this.latitude = parseFloat(result.latitude)
                            this.longitude = parseFloat(result.longitude)
                            this.getSharedOffices()
                            this.filterRecord()
                            if (window.matchMedia("(max-width: 556px)").matches) {
                                this.getsharedofficeMobile()
                            }
                        });
                    }
                }

            },

            mounted(){
                this.getCurrentLocation();
            }
        });

        $('#crossimages').click(function(){
            document.getElementById("pac-input").value = '';
        });

        setTimeout(function () {
            jQuery('.PT_open_popup').append("<input type='button' class='yearly_pay' value='Pay yearly' />");
            console.log('parent now');
            console.log(jQuery('.PT_open_popup'));
            console.log('timeiut');
            $('body').on('click', '.yearly_pay', function() {
                alert(2);
                Paytabs(".PT_open_popup").expresscheckout({
                    settings: {
                        merchant_id: "10025649",
                        secret_key: "FtH7UX0HsKHw8Tpc1qQVrNEVjMtclzC12j8Wg8EJTlf1eFeg55cwTQscDVpYu1Zxk8uBh8lauBcIgrZlwbSp2lCFg1DL3DioLNZ3",
                        amount: "500",
                        currency: "AED",
                        title: "Packege",
                        product_names: "Basic Packege",
                        order_id: 1,
                        url_redirect: "https://malenaapp.com/paynow/",
                        display_customer_info: 1,
                        display_billing_fields: 1,
                        display_shipping_fields: 0,
                        language: "en",
                        redirect_on_reject: 0,



                    },
                    customer_info: {
                        first_name: "Name",
                        last_name: "Name",
                        phone_number: "109209234",
                        email_address: "info@malenaapp.com",
                        country_code: "971"
                    },
                });
            });
        }, 3000)
    </script>
@endsection
