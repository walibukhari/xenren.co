@section('script')
    <script src="https://checkout.stripe.com/checkout.js"></script>
    <script>
        var handler = StripeCheckout.configure({
            key: '{{config('services.stripe.key')}}',
            image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
            locale: 'auto',
            token: function(token) {
                $(".ajaxloader").show();
                console.log('in token now');
                console.log(token)
                var form = new FormData();
                form.append("data", JSON.stringify(token));
                form.append("amount", window.topupamount);

                var settings = {
                    "url": "/stripe/charge",
                    "method": "POST",
                    "data": form,
                    "processData": false,
                    "contentType": false,
                    "mimeType": "multipart/form-data",
                };

                $.ajax(settings).done(function (response) {
                    console.log(response);
                    $(".ajaxloader").hide();
                });
            }
        });
        // document.getElementById('customButtonStripe').addEventListener('click', function(e) {
        //     console.log(parseInt(window.topupamount));
        //     if(parseInt(window.topupamount) < 1 || window.topupamount == NaN) {
        //         alert('Please enter valid amount');
        //         e.preventDefault();
        //     } else {
        //         // Open Checkout with further options:
        //         handler.open({
        //             image: '/images/Favicon.jpg',
        //             name: 'Xenren',
        //             description: 'Topup',
        //             amount: window.topupamount * 100
        //         });
        //         e.preventDefault();
        //     }
        // });
        // Close Checkout on page navigation:
        window.addEventListener('popstate', function() {
            handler.close();
        });
        function calculatePrice() {
            console.log('hello');
            var tax = (2.75 * (window.topupamount))/100;
            $('#spanAmount').html((window.topupamount-tax))
        }
    </script>

    <script>
        $(function() {
			if (window.matchMedia("(max-width: 556px)").matches) {
				$(".preload").fadeOut(2000, function() {
					$(".section-mobile-view-office-details").fadeIn(1000);
					$(".office-details-panel-default").fadeIn(1000);
				});
			}
		});
	</script>
	<!-- Swiper JS -->
	<script src="{{asset('swiperdist/js/swiper.min.js')}}"></script>
{{--	<script src="https://unpkg.com/swiper/js/swiper.js"></script>--}}
{{--	<script src="https://unpkg.com/swiper/js/swiper.min.js"></script>--}}
	<!-- Initialize Swiper -->
	<script type="text/javascript">
		setTimeout(function () {
			var mySwiper = new Swiper ('.swiper-container', {
				// Optional parameters
				direction: 'horizontal',
				loop: true,
				// If we need pagination
				pagination: {
					el: '.swiper-pagination',
				},
			})
		},1000);
	</script>

	<script src="{{asset('custom/js/bs-modal-bootstrap.js')}}"></script>
	<script>

		function myStars(val)
		{
			if(val === 'a' || val === 'b' || val === 'c' || val === 'd' || val === 'e' || val === 'f') {

				$('#stars li').on('mouseover', function(){
					var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

					// Now highlight all the stars that's not after the current hovered star
					$(this).parent().children('li.star').each(function(e){
						if (e < onStar) {
							$(this).addClass('hover');
						}
						else {
							$(this).removeClass('hover');
						}
					});

				}).on('mouseout', function(){
					$(this).parent().children('li.star').each(function(e){
						$(this).removeClass('hover');
					});
				});

				$('#stars li').on('click', function () {
					var onStar = parseInt($(this).data('value'), 10); // The star currently selected
					var stars = $(this).parent().children('li.star');
					for ( i = 0; i < stars.length; i++) {
						$(stars[i]).removeClass('selected');
					}

					for ( i = 0; i < onStar; i++) {
						$(stars[i]).addClass('selected');
					}
				});

				// JUST RESPONSE (Not needed)
				var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
				var msg = "";
				if (ratingValue > 1) {
					msg = "Thanks! You rated this " + ratingValue + " stars.";
				}
				else {
					msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
				}
				responseMessage(msg);
			}

		}
	</script>

	{{--head-tabs-style--}}
	<script>

		function responseMessage(msg) {
			$('.success-box').fadeIn(200);
			$('.success-box div.text-message').html("<span>" + msg + "</span>");
		}

		function openHead(cityName,elmnt,color,whiteColor) {
			var i, tabcontent, tablinks;
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}
			tablinks = document.getElementsByClassName("tablink");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].style.backgroundColor = "";
				tablinks[i].style.color = "";
			}
			document.getElementById(cityName).style.display = "block";
			elmnt.style.backgroundColor = color;
			elmnt.style.color = whiteColor;
		}

		setTimeout(function(){
			$("#defaultOpen").css('background-color','#57af2b');
			$("#defaultOpen").css('color','#fff');
			$('#London').css('display','block');
		},3000);

	</script>


	{{--head-tabs-style--}}

	<script>
		var slideIndex = 1;
		showSlides(slideIndex);

		function plusSlides(n) {
			showSlides(slideIndex += n);
		}

		// function currentSlide(n) {
		// 	showSlides(slideIndex = n);
		// }

		function showSlides(n) {
			var i;
			var slides = document.getElementsByClassName("mySlidesArea");
			var dots = document.getElementsByClassName("dotArea");
			if (n > slides.length) {slideIndex = 1}
			if (n < 1) {slideIndex = slides.length}
			for (i = 0; i < slides.length; i++) {
				slides[i].style.display = "none";
			}
			for (i = 0; i < dots.length; i++) {
				dots[i].className = dots[i].className.replace(" activeClass", "");
			}
			slides[slideIndex-1].style.display = "block";
			dots[slideIndex-1].className += " activeClass";
		}


		$(document).ready(function(){
			/*smooth scroll on click section*/
			$('.nav-tabs > li > a').click( function (e) {
				var a =$(this).innerHeight();
				$('.set-col-md-12-scroll-view-area').removeClass('set-position');
				e.preventDefault();
				var section = this.href;
				var sectionClean = section.substring(section.indexOf("#"));
				$("html, body").animate({
					 scrollTop: $(sectionClean).addClass('set-position').offset().top
				},500,function () {
					window.location.hash = sectionClean;
				});
			});
		})
	</script>
	<script>
		$(function () {
			// console.log('making footer stickt');
			window.onscroll = function() {myFunction()};
			// window.header = document.getElementById("myHeader");
			var navbar = document.getElementById("navbar");
			window.sticky = navbar.offsetTop + 400;

			$('.stickyHeaderNavBarMenu').on('click', function(){
				$('.stickyHeaderNavBar > li').removeClass("active")
				$(this).addClass('active')
			})
		})

		function myFunction() {
			var sticky = window.sticky;

			if (window.pageYOffset >= sticky) {
				$('.stickyHeaderNavBar').addClass('sticky')
			} else {
                $('.stickyHeaderNavBar').removeClass('sticky')
			}
		}
	</script>
	<script>
			$( function() {
				$("#form_datetime").datetimepicker({
					format: "yyyy-mm-dd",
					// altFormat: 'dd-mm-yy',
					altField: "#hiddenField",
					autoClose: true,
					pickTime: false,
					minView: 2
				}).on('changeDate', function(e){
					$('#form_datetime').datetimepicker('hide');
				});

				$("#form_datetime2").datetimepicker({
					format: "yyyy-mm-dd",
					// altFormat: 'dd-mm-yy',
					altField: "#hiddenField",
					autoClose: true,
					pickTime: false,
					minView: 2
				}).on('changeDate', function(e){
					$('#form_datetime2').datetimepicker('hide');
				});

				$("#datepicker").datetimepicker({
					format: "yyyy-mm-dd",
					// altFormat: 'dd-mm-yy',
					altField: "#hiddenField",
					autoClose: true,
					pickTime: false,
					minView: 2
				}).on('changeDate', function(e){
					$('#datepicker').datetimepicker('hide');
				});

				$( "#datepicker1" ).datetimepicker({
					format: "yyyy-mm-dd",
					// altFormat: 'dd-mm-yy',
					altField: "#hiddenField",
					autoClose: true,
					pickTime: false,
					minView: 2
				}).on('changeDate', function(e){
					$('#datepicker1').datetimepicker('hide');
				});
			});
	</script>
	<script>
		+function () {
			$(document).ready(function () {
				$("#submit-login").click(function(e){
					e.preventDefault()
					ajaxProceed()
				});

				function ajaxProceed() {
					var _token = $("input[name='_token']").val()
					var phone_or_email = $('#p_email').val()
					var pass = $('#pas').val()
					var errField = $('.err-msg')

					$.ajax({
						url: "{{ route('login.post') }}",
						type:'POST',
						data: {_token:_token, phone_or_email:phone_or_email, pass:pass},
						success: function(data) {
							errField.remove()
							window.location.href='/en/coworking-spaces/'+'{{$officeName}}/'+'{{$office_id}}';
						},
						error: function(err) {
							errField.remove()
							printErrorMsg(err)
						}
					});
				}
			});
		}(jQuery);

		$('#pass').bind('keypress',function(e){
			if(e.keyCode === 13) {
				$('#submit-login').click();
			}
		});

	</script>
	<script>
		function openLoginPopup()
		{
			$('#signInUser').modal('show');
		}

		/* check user join cowork or not*/
		let uId = '{{ isset(\Auth::user()->id) ? \Auth::user()->id : '' }}';
		let offId = '{{ $office_id }}';
		let url = '/coworking-spaces/shared-office-coWork/'+uId+'/'+offId;
		$.ajax({
			url:url,
			Type:'GET',

			success: function(response)
			{
				console.log(response.status === 'true');
				if(response.status === 'true')
				{
					$('#notUsedPlace').modal('show');
				}
			}
		});
	</script>
    <script>
        $(document).ready(function() {
            $('.video').each(function(i, obj) {
                $(this).on("mouseover", function() { hoverVideo(i); });
                $(this).on("mouseout", function() { hideVideo(i); });
            });
        });

        function hoverVideo(i) {
            $('.thevideo')[i].play();
        }

        function hideVideo(i) {
            $('.thevideo')[i].currentTime = 0;
            $('.thevideo')[i].pause();
        }
    </script>
    <script>
        var handler = StripeCheckout.configure({
            key: '{{config('services.stripe.key')}}',
            image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
            locale: 'auto',
            token: function(token) {
                $(".ajaxloader").show();
                console.log('in token now');
                console.log(token);
                let amount = $('#stripeAmount').val();
                var form = new FormData();
                form.append("data", JSON.stringify(token));
                form.append("amount", amount);
                console.log('this.topupamount');
                console.log(amount);
                var settings = {
                    "url": "/stripe/charge",
                    "method": "POST",
                    "data": form,
                    "processData": false,
                    "contentType": false,
                    "mimeType": "multipart/form-data",
                };

                $.ajax(settings).done(function (response) {
                    console.log(response);
                    $(".ajaxloader").hide();
                });
            }
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
	<script>
			let token = '{{ session('api_token') }}';
			let officeId = '{{ $office_id }}';
			let setToken = `Bearer ${token}`;
			new Vue({
				el:'#app',
				props:['id'],
				data: {
                    topupamount:0,
					getOfficeDetails:[],
					officeId: officeId,
					getOfficeImages:[],
					getOfficeImagesLength:[],
					productId:'',
					contact_message: '',
					contact_subject: '',
					filerRecord: '',
					continent_filter:[],
					type_filter:[],
					sharedOffices:[],
					sharedOfficesOriginalData:[],
					file_name:'',
					full_name:'',
					phone_number:'',
					email_address:'',
					companyName:'',
					location :'',
					filename:'',
					remarks_please:'',
					people_strength:'',
					no_of_rooms:'',
					filled_stars: 0,
					unfilled_stars: 0,
					getRaters:'',
					showReviews:true,
					moreReviews:'',
					office_type:'',
					noRooms:'',
					peopleStrength:'',
					remarksPlease:'',
					price:'',
					peoples:'',
					durationType:'',
					ratingStars:'',
					lastscannedOffice:'',
					reviewStars:'',
					comment:'',
					sharedoffice:'',
					getReviewId:'',
					sharedOfficesData:[],
					sharedOfficesActiveData:[],
					sharedOfficesInactiveData:[],
					listLoader:false,
                    password:'',
                    loaderBooking: false
                },

				methods:{
                    stripeTopUp: function(e){
                            console.log(parseInt(this.topupamount));
                            if(parseInt(this.topupamount) < 1 || this.topupamount == NaN) {
                                alert('Please enter valid amount');
                                e.preventDefault();
                            } else {
                                // Open Checkout with further options:
                                handler.open({
                                    image: '/images/Favicon.jpg',
                                    name: 'Xenren',
                                    description: 'Topup',
                                    amount: this.topupamount * 100
                                });
                                e.preventDefault();
                            }
                            // Close Checkout on page navigation:
                            window.addEventListener('popstate', function() {
                                handler.close();
                            });
                    },
                    calculatePrice: function() {
                        console.log('hello');
                        var tax = (2.75 * (this.topupamount))/100;
                        $('#spanAmount').html((this.topupamount-tax))
                    },
                    closeModal: function(val){
                        $('#paymentModal').modal('hide');
                    },
                    paymentMethod: function(event,val){
                        $('.paymentMethod').prop("checked", false);
                        console.log(val);
                          console.log(event.target.value);
                          console.log(event.target.id);
                          let id =  event.target.id;
                          console.log(id);
                          $('#'+id).prop("checked", true);
                          $('#selected_payment_method').val(id);
                        if( id == 'st') {
                            $('#btnCustom').hide();
                            $('#customButtonStripe').show();
                            $('#stripeAmount').show();
                            $('#notStripe').hide();
                        } else {
                            $('#btnCustom').show();
                            $('#customButtonStripe').hide();
                            $('#stripeAmount').hide();
                            $('#notStripe').show();
                        }
                    },
					getLocation() {
						console.log("click event");
						if (navigator.geolocation) {
							navigator.geolocation.getCurrentPosition(showPosition,showError);
						} else {
							toastr.error('Geolocation is not supported by this browser.');
						}

						function showPosition(position) {
							lat = position.coords.latitude;
							long = position.coords.longitude;
							displayLocation(lat,long);
							showLocationMap(lat,long)
						}
						function showError(error) {
							console.log("error");
							console.log(error);
							console.log('no location access');
							$.getJSON('https://ipapi.co/json/', function (result) {
								lat = result.latitude;
								long = result.longitude;
								showLocationMap(lat,long);
								var city = result.city;
								var country = result.country_name;
								var fullName = city+','+country;
								$('#pac-input').val(fullName);
							});
						}

						function showLocationMap(latitude,longitude) {
							//The location on the map.
							var mapLocation = new google.maps.LatLng(latitude,longitude);
							//Map options.
							var options = {
								center: mapLocation, //Set center.
								zoom: 7, //The zoom value.
								mapTypeId: 'roadmap'
							};
							//Create the map object.
							var listmap = new google.maps.Map(document.getElementById('listMap'), options);
							var latlng = {lat:latitude , lng:longitude};
							//Create the marker.
							marker = new google.maps.Marker({
								position: latlng,
								title: 'place',
								map: listmap
							});
						}
						function displayLocation(latitude,longitude){
							let that = this;
							const geocoder = new google.maps.Geocoder();
							const latlng = new google.maps.LatLng(latitude, longitude);
							geocoder.geocode(
									{'latLng': latlng},
									function(results, status) {
										if (status == google.maps.GeocoderStatus.OK) {
											if (results[0]) {
												var add = results[0].formatted_address ;
												var  value = add.split(",");
												var names = value[2]+','+value[4];
												var cleanName = names.replace(/^\s+|\s+$/g, '');
												console.log(cleanName);
												// this.userLocation = cleanName;
												// // console.log("that.userLocation");
												// // console.log(that.userLocation);
												// // $('.location-input').val(cleanName);
												// console.log("this.userLocation");
												// console.log(this.userLocation);
												$('#pac-input').val(cleanName);
											}
											else  {
												toastr.error('address not found');
											}
										}
										else {
											toastr.error('Geocoder failed due to:'+ status);
										}
									}
							);
						}
					},


					getSharedOffices(){
						let language = window.session.value;
						this.$http.get('/api/sharedoffice?sort=desc&langauge='+language, {
							headers: {
								Authorization: setToken
							}
						}).then(function(response){
							this.sharedOfficesData = response.data;
							// console.log("get office details shardoffices");
							// console.log(this.sharedOfficesData);
							// console.log("get office details shardoffices");
						}, function(error){
						});
					},

					getofficename(sharedoffices)
					{
						const locale = '{{\Config::get('app.locale')}}';
						if(!sharedoffices.version_chinese){
							sharedoffices.version_chinese = 'false';
						}
						if(!sharedoffices.version_english){
							sharedoffices.version_english = 'false';
						}
						if(locale === 'cn') {
							if(sharedoffices.version_chinese === 'true' && sharedoffices.version_english === 'true') {
								return sharedoffices.shared_office_language_data.office_name_cn;
							} else if(sharedoffices.version_chinese === 'true' && sharedoffices.version_english === 'false') {
								return sharedoffices.shared_office_language_data.office_name_cn;
							} else if(sharedoffices.version_chinese === 'false' && sharedoffices.version_english === 'true') {
								return sharedoffices.office_name;
							}
						} else if(locale === 'en') {
							if(sharedoffices.version_chinese === 'true' && sharedoffices.version_english === 'true') {
								// console.log('case 1');
								return sharedoffices.office_name;
							}
							else if(sharedoffices.version_chinese === 'true' && sharedoffices.version_english === 'false') {
								// console.log('case 2');
								return sharedoffices.shared_office_language_data.office_name_cn;
							} else if(sharedoffices.version_chinese === 'false' && sharedoffices.version_english === 'true') {
								// console.log('case 3');
								return sharedoffices.office_name;
							}
						}
						return sharedoffices.office_name ? sharedoffices.office_name : sharedoffices.shared_office_language_data.office_name_cn;
					},

					getImagePath(img) {
					    console.log(img);
						if(!img || img == '') {
							return "{{asset('/images/noimage.jpeg')}}";
						} else {
							return img;
						}
					},

					officeRating(office_id)
					{
						let link = "/get-shared-office-rating/"+office_id;
						var token = '{!! csrf_token() !!}';
						this.$http.get(link,{
							headers:{
								Authorization:token,
							}
						}
						).then(function(response){
							// console.log(response.data);
                            this.sharedoffice = response.data;
						});
					},


                    myReview(val)
                    {
                      this.reviewStars = val;
				  	},

					submitReviewForm()
					{
						var office_id = $('#officeId').val();
						var id = '{!! isset(\Auth::user()->id) ? \Auth::user()->id : '' !!}';
						var comment = $('#commentReview').val();

						if(this.reviewStars === ''){
						    toastr.error('{{trans('validation.fill_stars')}}');
                        } else
						if(comment === ''){
                            toastr.error('{{trans('validation.comment_required')}}');
                        } else{
							let formData = new FormData();
							formData.append('comment', comment);
							formData.append('reviewsStars', this.reviewStars);
							formData.append('office_id', office_id);
							formData.append('user_id', id);
							var token = '{!! csrf_token() !!}';
							let link = '/coworking-spaces/shared-office-rating';
							this.$http.post(link, formData, {
								headers: {
									Authorization: token,
								}
							}).then(function (response) {
								if(response.data.status === 'success') {
								    // toastr.success("shared office rated successfully");
									$('#myReviewPlease').modal('hide');
									$('#thanksPopup').modal('show');
									setTimeout(function () {
                                        window.location.reload()
                                    }, 1000);
								} else {
                                    toastr.error(response.data.message);
								}
							});
						}

					},


					reserveModal(type,no_of_peoples,price,duration_type)
					{
						this.office_type = type;
						this.price = price;
						this.peoples = no_of_peoples;
						this.durationType = duration_type;
						// // console.log('office-fee-type');
						// // console.log(type);
						// // console.log(price);
						// // console.log(no_of_peoples);
						// // console.log(duration_type);
						// // console.log('office-fee-type');
					},

					ReserveRequest()
					{
						var form = $('#reserve-form').serialize();
						var check_in_Date = $('#form_datetime').val();
						var check_out_Date = $('#form_datetime2').val();
						var office_manager = $('#office_manager_email').val();
						var no_of_peoples = $('#noOfPeoples').val();
						var first_name = $('#first_name').val();
						var Phone_number = $('#Phone_number').val();
						var url = "/shared-office-reserve";
						var token = '{!! csrf_token() !!}';
						if(first_name === '')
						{
							toastr.error('Please Enter Full Name');
						} else if( check_in_Date === '')
						{
							toastr.error('Please select check in date');
						} else if(check_out_Date === '') {
							toastr.error('Please select check out date');
						} else if(Phone_number === '')
						{
							toastr.error('Please Enter Phone Number');
						} else if(this.noRooms === '')
						{
							toastr.error('Please Enter No of Rooms');
						} else if(no_of_peoples  === '')
						{
							toastr.error('Please Enter People Strength');
						} else if(this.remarksPlease === '')
						{
							toastr.error('Remarks Field is required');
						} else if(office_manager === '')
						{
							toastr.error('The office has not been setup completely');
						}
						else {

							$.ajax({
								method: 'post',
								url: url,
								headers: {
									Authorization: token,
								},
								data: form,

								success: function (data) {
									if (data.status == 'success') {
										alertSuccess(data.message);
										$("#reserve-form")[0].reset();
										$('#myReserveSpace').modal('hide');
										setTimeout(function(){window.location.reload();},200)
									}
								}
							});
						}
					},

					currentSlide(n) {
						// // console.log('n');
						// // console.log(n);
						showSlides(slideIndex = n+1);
					},

					setupRatingStars() {

						this.filled_stars = this.getOfficeDetails && this.getOfficeDetails.rating ? this.getOfficeDetails.rating : 0;
						this.unfilled_stars = this.getOfficeDetails && this.getOfficeDetails.rating ? 5 - this.getOfficeDetails.rating : 0;

					},

					getsharedOfficeLocationstr(sharedOffice)
					{
						if(sharedOffice.location && sharedOffice.location.length && sharedOffice.location.length > 9)
						{
							return sharedOffice.location.substring(0,15) + '...';
						}
						return sharedOffice.location;
					},

					getofficenamestr(sharedOffice)
					{
						const locale = '{{\Config::get('app.locale')}}';

						if(locale === 'cn') {
							if(sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'true') {
								return sharedOffice.shared_office_language_data.office_name_cn.substring(0,15) + '...';
							} else if(sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'false') {
								return sharedOffice.shared_office_language_data.office_name_cn.substring(0,15) + '...';
							} else if(sharedOffice.version_chinese === 'false' && sharedOffice.version_english === 'true') {
								return sharedOffice.office_name.substring(0,15) + '...';
							}
						} else if(locale === 'en') {
							if(sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'true') {
								// console.log('case 1');
								return sharedOffice.office_name.substring(0,15) + '...';
							}
							else if(sharedOffice.version_chinese === 'true' && sharedOffice.version_english === 'false') {
								// console.log('case 2');
								return sharedOffice.shared_office_language_data && sharedOffice.shared_office_language_data.office_name_cn.substring(0,15) + '...';
							} else if(sharedOffice.version_chinese === 'false' && sharedOffice.version_english === 'true') {
								// console.log('case 3');
								return sharedOffice.office_name.substring(0,15) + '...';
							}
						}
						return sharedOffice.office_name ? sharedOffice.office_name.substring(0,15) + '...' : sharedOffice.shared_office_language_data.office_name_cn.substring(0,15) + '...';
					},

					getSharedOfficeRatingMVOD(offices)
					{
						var stars = offices.shared_office_rating ? offices.shared_office_rating : 0;
						var getstars = '';
						stars.map(function(values){
							getstars = Math.round(values.avg);
						});
						return getstars ? getstars : 0;
					},


					uploadListForm() {
						this.listLoader = true;

						var loc =  $('#pac-input').val();
						console.log(loc);
						console.log("loc");

						let tokencsrf = '{{ csrf_token() }}';
						let Listform = new FormData();
						var files  = $('#files')[0].files;
						let counter = 0;
						$.each(files , function(key, value) {
							Listform.append('image_'+key, value);
							counter++;
						});
						Listform.append('company_name',this.companyName);
						Listform.append('full_name',this.full_name);
						Listform.append('phone_number',this.phone_number);
						// Listform.append('location',this.location);
						Listform.append('location',loc);
						Listform.append('email',this.email_address);
						Listform.append('file_name',$('#files')[0].files[0]);
                        Listform.append('password',this.password);
                        Listform.append('counter',counter);
						this.filename = $('#files')[0].files[0];
						let link = '/shared-office-list-space-request';

						if(this.companyName == '')
						{
							toastr.error('{{trans('validation.company_required')}}');
							this.listLoader = false;
						}
						else if(loc == '')
						{
							toastr.error('{{trans('validation.location_required')}}');
							this.listLoader = false;
						}
						else if(this.filename == undefined || this.filename == '') {
							toastr.error('{{trans('validation.image_required')}}');
							this.listLoader = false;
						}
						else if(this.phone_number == ''){
							toastr.error('Phone number filed is required');
							this.listLoader = false;
						}
						else if(this.email_address == ''){
							toastr.error('Email filed is required');
							this.listLoader = false;
						} else if(this.password == ''){
                            toastr.error('Password filed is required');
                            this.listLoader = false;
                        } else if(counter > 7){
							toastr.error('you cannot upload more then 6 images');
							this.listLoader = false;
						} else{
							this.$http.post(link,Listform, {
								headers: {
									Authorization: tokencsrf,
									'Content-Type': 'multipart/form-data'
								}
							}).then(function(response){
                                console.log(response.data.status);
                                if(response.data.status === false) {
                                    $('.imageloader').hide();
                                    toastr.error(response.data.message);
									this.listLoader = false;
								} else {
                                    $('.imageloader').hide();
                                    this.listLoader = false;
                                    this.companyName = '';
                                    this.full_name = '';
                                    this.phone_number = '';
                                    // this.location = '';
									loc = '';
                                    this.email_address = '';
                                    this.file_name = '';
                                    toastr.success('Your Space Listed Successfully');
                                    setTimeout(function(){
                                        window.location.reload();
                                    },100);
                                    $('#listSpaceModal').modal('hide');
                                }
							}, function(error){
							});
						}
					},

					countHelpful(reviewId){
						// alert(reviewId);
						$.ajax({
							type: 'post',
							url: `/api/sharedoffice/helpful-review/${reviewId}`,
							headers: {
								Authorization: setToken
							},
							success: function (data) {
								// console.log(data);
								if(data.status == 'error'){
									toastr.error(data.message);
									this.getSharedOfficeByID();
								} else {
									toastr.success(data.message);
								}
							},
						});
					},

					copyLinkToClipBoard() {
						var registerUrl = `https://www.xenren.co/`+window.session.value+`/coworking-spaces/${this.getOfficeDetails.office_name}/${this.getOfficeDetails.id}`;

						var $temp = $("<input>");
						$("body").append($temp);
						$temp.val(registerUrl).select();
						document.execCommand("copy");
						$temp.remove();

						alertSuccess('Link copied, you can share with anyone now...!')
					},

					likeOffice(){
						const officeId = this.getOfficeDetails.id;
						$.ajax({
							type: 'post',
							url: `/api/sharedoffice/like-shared-office/${officeId}`,
							headers: {
								Authorization: setToken
							},
							success: function (data) {
								// console.log(data);
								if(data.status == 'error'){
									toastr.error(data.message);
								} else {
									toastr.success(data.message);
								}
							},
						});
					},

					isReview() {
						const a = (this.getOfficeDetails.rating < 1 ? false : true);
						// console.log('a');
						// console.log(a);
						return a;
					},

					getReviewComment(key) {
						try {
							var review = (this.getOfficeDetails.shared_office_rating)
							let comment = '';
							let commenter = '';
							let date = '';
							let helpful_count = '';
							let id = '';
							for (var k in review) {
								commenter = review[k].rater.name;
								date = review[k].created_at;
								comment = (review[k].comment);
								helpful_count = (review[k].helpful_count);
								id = (review[k].id);
							}
							if(key == 'username'){
								return commenter
							} else if(key == 'date') {
								return date;
							} else if(key == 'helpful_count') {
								return helpful_count ? helpful_count : '0';
							} else if(key == 'id') {
								this.getReviewId =  id;
								return this.getReviewId;
							} else {
								return comment;
							}
						} catch (e) {
							return '';
						}
					},

                    getOnlyVideo(file) {
					    let videoFile = '';
                        file.map(function (file) {
                            if(file.video) {
                                videoFile = file.video;
                            }
                        });
                        console.log('videoFile');
                        console.log(videoFile);
                        console.log(window.location.origin);
                        return window.location.origin+'/'+videoFile;
                    },

					getSharedOfficeByID(){
						const url = '/api/officeDetailsBtn/'+this.officeId;
						this.$http.get(url, {
							headers: {
								Authorization: setToken
							}
						}).then(function(response){
							// console.log(response.data.shared_office_rating);
							initMap();
							this.getOfficeDetails = response.data;
							console.log('this.getOfficeDetails');
							console.log(this.getOfficeDetails);
							console.log('this.getOfficeDetails');
							this.setupRatingStars();
							let currentLocale = '{{\Config::get('app.locale')}}';
							if(currentLocale === 'cn') {
								if(this.getOfficeDetails.version_chinese == 'true' && this.getOfficeDetails.version_english == 'true') {
									this.getOfficeDetails.office_name = this.getOfficeDetails.shared_office_language_data.office_name_cn;
									this.getOfficeDetails.description = this.getOfficeDetails.shared_office_language_data.office_description_cn;
									this.getOfficeDetails.office_space = this.getOfficeDetails.shared_office_language_data.office_space_cn;
									this.getOfficeDetails.contact_name = this.getOfficeDetails.shared_office_language_data.contact_name_cn;
									this.getOfficeDetails.contact_phone = this.getOfficeDetails.shared_office_language_data.contact_phone_cn;
									this.getOfficeDetails.contact_email = this.getOfficeDetails.shared_office_language_data.contact_email_cn;
									this.getOfficeDetails.contact_address = this.getOfficeDetails.shared_office_language_data.contact_address_cn;
								} else if(this.getOfficeDetails.version_chinese == 'true' && this.getOfficeDetails.version_english == 'false') {
									this.getOfficeDetails.office_name = this.getOfficeDetails.shared_office_language_data.office_name_cn;
									this.getOfficeDetails.description = this.getOfficeDetails.shared_office_language_data.office_description_cn;
									this.getOfficeDetails.office_space = this.getOfficeDetails.shared_office_language_data.office_space_cn;
									this.getOfficeDetails.contact_name = this.getOfficeDetails.shared_office_language_data.contact_name_cn;
									this.getOfficeDetails.contact_phone = this.getOfficeDetails.shared_office_language_data.contact_phone_cn;
									this.getOfficeDetails.contact_email = this.getOfficeDetails.shared_office_language_data.contact_email_cn;
									this.getOfficeDetails.contact_address = this.getOfficeDetails.shared_office_language_data.contact_address_cn;
								} else if(this.getOfficeDetails.version_chinese == 'false' && this.getOfficeDetails.version_english == 'true') {}
							} else if(currentLocale === 'en') {
								if(this.getOfficeDetails.version_chinese == 'true' && this.getOfficeDetails.version_english == 'true') {}
								else if(this.getOfficeDetails.version_chinese && !this.getOfficeDetails.version_english) {
									this.getOfficeDetails.office_name = this.getOfficeDetails.shared_office_language_data.office_name_cn;
									this.getOfficeDetails.description = this.getOfficeDetails.shared_office_language_data.office_description_cn;
									this.getOfficeDetails.office_space = this.getOfficeDetails.shared_office_language_data.office_space_cn;
									this.getOfficeDetails.contact_name = this.getOfficeDetails.shared_office_language_data.contact_name_cn;
									this.getOfficeDetails.contact_phone = this.getOfficeDetails.shared_office_language_data.contact_phone_cn;
									this.getOfficeDetails.contact_email = this.getOfficeDetails.shared_office_language_data.contact_email_cn;
									this.getOfficeDetails.contact_address = this.getOfficeDetails.shared_office_language_data.contact_address_cn;
								} else if(this.getOfficeDetails.version_chinese == 'false' && this.getOfficeDetails.version_english == 'true') {}
							}

							this.getOfficeImages = response.data.office_images;
							if(this.getOfficeImages.length > 0) {
								this.getOfficeImagesLength = 0;
							}else {
								this.getOfficeImagesLength = 4;
							}
							geocodeAddress(window.geocoder, window.map, response.data);
						}, function(error) {
						});
					},

					bookNowMessage() {
						var form_data = $("#book_form").serialize();
						var category_id = $('#category_id').val();
                        var check_in_Date = $('#datepicker').val();
						var check_out_Date = $('#datepicker1').val();
						var office_manager = $('#office_manager').val();
						var p_number = $('#p_number').val();
						var f_name = $('#f_name').val();
						var token = '{{csrf_token()}}';
						var url = '/book/shared/office/send/email';
						if(f_name === '')
						{
							toastr.error('{{trans('validation.full_name_enter')}}');
						} else if( p_number === '')
						{
							toastr.error('{{trans('validation.phone_enter')}}');
						} else if( check_in_Date === '')
						{
							toastr.error('{{trans('validation.select_checkin')}}');
						} else if(category_id === null)
                        {
                            toastr.error('{{trans('validation.category_id')}}');
                        } else if(check_out_Date === '')
                        {
							toastr.error('{{trans('validation.select_checkout')}}');
						} else if(this.no_of_rooms === '')
						{
							toastr.error('{{trans('validation.no_of_rooms')}}');
						} else if(this.people_strength === '')
						{
							toastr.error('{{trans('validation.no_of_people')}}');
						} else if(this.remarks_please === '')
						{
							toastr.error('{{trans('validation.remark_required')}}');
						}
						{{--else if(office_manager === '')--}}
						{{--{--}}
						{{--	toastr.error('{{trans('validation.office_not_complete')}}');--}}
						{{--}--}}
						else
						 {
                             let that = this;
                             that.loaderBooking = true;
							$.ajax({
								type: 'post',
								url: url,
								data: form_data,
								headers: {
									Authorization: token
								},
								success: function (data) {
                                    that.loaderBooking = false;
									if (data.status == 'success') {
                                        that.loaderBooking = false;
										alertSuccess(data.message);
										$("#book_form")[0].reset();
										$('#myBookSpaceModal').modal('hide');
                                        @if($OwnerSettings !== '' && !is_null($OwnerSettings) && $OwnerSettings->status_book_type == \App\Models\SharedOfficeSetting::BOOK_STATUS_PAY_NOW)
                                            $('#paymentModal').modal('show');
                                        @else
                                            setTimeout(function () {
                                                window.location.reload();
                                            },2000);
                                        @endif
									} else if(data.status == 'info') {
                                        that.loaderBooking = false;
                                        toastr.info(data.message);
                                    } else {
                                        that.loaderBooking = false;
                                        alertError(data.message);
                                    }
								}
							});
						}
					},

					submitForm() {
						// console.log("=======");
						// console.log(this.getOfficeDetails.office_manager);
						// console.log("=======");
						let formData = new FormData();
						formData.append('email',$('#email').val());
						formData.append('message', this.contact_message);
						formData.append('to', this.getOfficeDetails.office_manager);
						formData.append('contact_name',$('#name').val());
						formData.append('contact_subject', this.contact_subject);
						formData.append('_token', '{{csrf_token()}}');

						let settings = {
							"async": true,
							"crossDomain": true,
							"url": "{{route('sharedoffice.contact', $office_id )}}",
							"method": "POST",
							"processData": false,
							"contentType": false,
							"mimeType": "multipart/form-data",
							"data": formData
						};
						var email = $('#email').val();
						var name = $('#name').val();
						if(name === '')
						{
							toastr.error('{{trans('validation.name_enter')}}');
						} else if(email === ''){
							toastr.error('{{trans('validation.enter_email')}}');
						} else if(this.contact_subject === '') {
							toastr.error('{{trans('validation.enter_subject')}}');
						} else if(this.contact_message === '') {
							toastr.error('{{trans('validation.enter_message')}}');
						} else {
							$.ajax(settings).done(function (response) {
								response = JSON.parse(response)
								if (response.status == 'success') {
									alertSuccess(response.message);
									$('#myModal').modal('hide');
								} else {
									alertError(response.message);
									$('#myModal').modal('hide');
								}
							});
						}
					},

					loadReviews()
					{
						this.moreReviews = true;
						this.showReviews = false;
					},

					getCategoryName(name) {
						return name.split('.')[1]
					},

					getBulletinText(item) {
						let currentLocale = '{{\Config::get('app.locale')}}';
						if(currentLocale == 'en') {
							return item.text;
						} else {
							return item.text_cn;
						}
					},

					getSharedOfficeCountry(){
						var url = '/api/officeDetailsBtn/'+this.officeId;
					},

					formatTime(time){
						if(time == 'OFF DAY' || !time || time == '' || time == null || time == undefined || time == "undefined") {
							return 'OFF DAY';
						}
						return moment(time, "HH:mm:ss").format("hh:mm A");
					},

					time_format(time)
					{
						if(time == 'OFF DAY' || !time || time == '' || time == null || time == undefined || time == "undefined") {
							return 'OFF DAY';
						}
						return moment(time, "HH:mm:ss").format("hA");
					}
				},


				mounted() {
					this.getSharedOfficeByID();
					if (window.matchMedia("(max-width: 556px)").matches) {
						this.getSharedOffices();
					}
					// this.getofficename();
				}

			});
	</script>
	<script>
			$('#modal-app').hide();
			var g_lang = {
				success : "{{ trans('common.success') }}",
				error : "{{ trans('common.error')  }}",
				notice : "{{ trans('common.notice')  }}",
				search : "{{ trans('common.search')  }}"
			};
	</script>
	<link href="/assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
	<script src="/assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
	<script src="/custom/js/global.js"></script>
	<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCIasatwSA8nyOeN8Cobzil6yO1jsT13rE&callback=initMap&libraries=places,controls"></script>
	<link href="{{asset('assets/global/plugins/lightbox2/dist/css/lightbox.min.css')}}" rel="stylesheet" type="text/css"/>
	<script src="{{asset('assets/global/plugins/lightbox2/dist/js/lightbox.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('custom/js/ListSpaceMap.js')}}"></script>
	<script>
			window.geocoder = '';
			window.map = '';
			function initMap() {
				initListMap();
				let map = new google.maps.Map(document.getElementById('map'), {
					zoom: 18,
					center: {lat: -34.397, lng: 150.644}
				});
				let geocoder = new google.maps.Geocoder();
				window.geocoder = geocoder;
				window.map = map;
			}
			function geocodeAddress(geocoder, resultsMap, address) {
				if(address.lat) {
					let marker = new google.maps.Marker({
						map: resultsMap,
						position: new google.maps.LatLng(address.lat , address.lng),
						visible: true,
						title: 'mare'
					});
					resultsMap.setCenter(new google.maps.LatLng(address.lat , address.lng))
				}
			}
	</script>
@endsection
