@extends('frontend.coWorks.layouts.default')
@section('title'){{trans('common.start_search_h1')}} | {{isset($location) ? str_replace(',',' | ',ucfirst($location)) : ''}}{{str_replace('Coworking Spaces','',trans('common.coworking_spaces_title'))}}@endsection

@section('searchResult'){{trans('common.found')}} @if(isset($country) && isset($city)){{isset($countCity) ? $countCity : '0'}}@else{{isset($countCountry) ? $countCountry : '0'}}@endif {{trans('common.item_found_in')}} {{isset($location) ? ucfirst($location) : ''}}@stop
@section('header')
    <link href="{{asset('custom/css/frontend/coWork/searchOfficeDetails.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('custom/css/frontend/coWork/searchResultsMobileView.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        /*.attachLoader{*/
        /*    display: none;*/
        /*}*/
        /*.preload>img{*/
        /*    display: block;*/
        /*}*/
        .preload {
            width: 100%;
            height: 239px;
            position: relative;
            top: 31%;
            text-align: -moz-center;
            text-align: -webkit-center;
            text-align-last: center;
            text-align: center;
            margin-bottom: 0px;
            /* background: #fff; */
        }

        .dot-loader img{
            width: 70px;
            padding-left: 20px;
        }
        .preload >img{
            position: relative;
            display: inline-block;
            text-align-last: center;
            width: 50px;
            align-items: center;
            margin-top:50px;
        }
        .setCustomImage{
            display: flex;
            justify-content: center;
            margin-top: 20px;
        }
        .setCustomImage img{
            width: 25%;
            height: 300px;
        }
    </style>
@endsection
@section("content")

<div class="section-web-view-search-results">
<section class="sidebar">
    <div class="container set-search-container">
        <div class="row set-rowmarginbothside">
            <div class="col-md-2 left-sidebar set-col-md-2-left-side-bar">
                @include('frontend.coWorks.partials.leftBarFilters')
            </div>

            <div class="col-md-10 col-sm-10 col-xs-12 right-section setRightSection">
                <div class="row">
                    <div class="col-md-6 right-section-hedding set-right-section-search-head">
                        <p class="p-0 m-0 set-m-0-p-0">{{trans('common.search_result')}}</p>
                        <div v-if="sharedOffices && sharedOffices.length == 0">
                        </div>
                        <div v-else>
                            <span class="attachLoader" v-if="!loader"> {{trans('common.found')}}   @{{sharedOffices.length}} {{trans('common.item_found_in')}} {{isset($location) ? ucfirst($location) : ''}}</span>
                        </div>
                    </div>
                </div>
                <div class="setCustomImage" v-if="sharedOffices && sharedOffices == 0">
                    <img src="{{ asset('images/searchResult.png.gif') }}">
                </div>
                <div class="row" v-else>
                    <div class="col-md-6 set-col-ms-12-search"  v-for="sharedOffice in sharedOffices">
                        <div class="container" style="width:100%;padding-left: 0px;
padding-right: 0px;">
                            <div class="panel panel-default post-box" @click="getSharedOfficeByID(sharedOffice.office_name, sharedOffice.id, sharedOffice)">
                                <div class="panel-body setPanelBody">
                                    <div class="row setPOSTIMGROW">
                                        <div class=" post-box-img">
                                            <div class="preload" v-if="loader">
                                                <img src="https://i.imgur.com/KUJoe.gif">
                                            </div>
                                            <img v-if="!loader" class="setPostBoxImage attachLoader" :src="'/'+sharedOffice.image">
                                        </div>
                                    </div>

                                    <div class="setCaptionBox">
                                        <div class="p-2 post-title">
                                            <div class="row">
                                                <div class="col-md-12 set-col-md-12-p-2-title">
                                                    <div class="dot-loader" v-if="loader">
                                                        <img class="dotsloader" src="{{asset('images/loaders/dotsloader.gif')}}">
                                                        <br>
                                                    </div>
                                                    <p class="m-0 attachLoader" v-if="!loader"> @{{ getOfficeName(sharedOffice) }} </p>
                                                    <div class="dot-loader" v-if="loader">
                                                        <img class="dotsloader" src="{{asset('images/loaders/dotsloader.gif')}}">
                                                    </div>
                                                    <span class="attachLoader" v-if="!loader"><img src="{{asset('custom/css/frontend/coWork/image/icon.png')}}">&nbsp; @{{ sharedOffice.location }}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="p-2">
                                            <div class="row">
                                                <div class="col-md-7 post-second-title">
                                                    <div class="post-border"></div>
                                                    <div class="dot-loader" v-if="loader">
                                                        <img class="dotsloader" src="{{asset('images/loaders/dotsloader.gif')}}">
                                                    </div>
                                                    <p class="m-0 attachLoader" v-if="!loader">@{{ sharedOffice.office_space }}</p>
                                                    <span>{{trans('common.office_space')}}</span>
                                                </div>
                                                <div class="col-md-5 post-second-title">
                                                    <div class="dot-loader" v-if="loader">
                                                        <img class="dotsloader" src="{{asset('images/loaders/dotsloader.gif')}}">
                                                    </div>
                                                    <p class="m-0 attachLoader" v-if="!loader">@{{ sharedOffice.total_dedicated_desks }}</p>
                                                    <span>{{trans('common.dedicated_desk')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="p-2">
                                            <div class="row">
                                                <div class="col-md-7 post-second-title">
                                                    <div class="post-border"></div>
                                                    <div class="dot-loader" v-if="loader">
                                                        <img class="dotsloader" src="{{asset('images/loaders/dotsloader.gif')}}">
                                                    </div>
                                                    <p class="m-0 attachLoader" v-if="!loader">@{{ sharedOffice.office_size_x * sharedOffice.office_size_y }}</p>
                                                    <span>{{trans('common.people_can_attented')}}</span><br>
                                                    <div class="dot-loader" v-if="loader">
                                                        <img class="dotsloader" v-if="!loader" src="{{asset('images/loaders/dotsloader.gif')}}">
                                                    </div>
                                                    <span style="position: relative;top:10px;" class="attachLoader" v-if="!loader">{{trans('common.phone_no')}}: @{{ sharedOffice.contact_phone}} <br> {{trans('common.meeting_room_price')}}: @{{ sharedOffice.meeting_room_price }}$ <br> {{trans('common.seat_price')}}: $@{{ sharedOffice.min_seat_price }}/h $@{{ sharedOffice.max_seat_price }}/h {{trans('common.month')}}</span>
                                                </div>
                                                <div class="col-md-5 post-second-title">
                                                    <div class="dot-loader" v-if="loader">
                                                        <img class="dotsloader" src="{{asset('images/loaders/dotsloader.gif')}}">
                                                    </div>
                                                    <p class="m-0 attachLoader" v-if="!loader">
                                                        <p v-if="sharedOffice && sharedOffice.monday_opening_time">
                                                        @{{ moment(sharedOffice.monday_opening_time,"HH:mm").format('hh:mm A') }} {{trans('common.to')}} @{{ moment(sharedOffice.monday_closing_time,"HH:mm").format('hh:mm A') }}
                                                        </p>
                                                        <p v-else>
                                                            00:00
                                                        </p>
                                                    </p>
                                                    <span>{{trans('common.operate_hours')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="dot-loader" v-if="loader">
                                    <img class="dotsloader" src="{{asset('images/loaders/dotsloader.gif')}}">
                                </div>
                                <div class="panel-footer setPanelFooter attachLoader" v-if="!loader">
                                    <a href="#" class="btn custom-btn btn-defualt" @click="getSharedOfficeByID(sharedOffice.office_name, sharedOffice.id,sharedOffice)">{{trans('common.enquiry_now')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--<div class="row">--}}
                    {{--<div class="col-md-12 text-right">--}}
                        {{--<div class="pagination">--}}
                            {{--<a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a>--}}
                            {{--<a class="active" href="#">1</a>--}}
                            {{--<a href="#">2</a>--}}
                            {{--<a href="#">3</a>--}}
                            {{--<a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
</section>
</div>

<!-- mobile-view search results -->
<section class="mobile-view-search-results">
    <div class="container set-box-container">
        <div v-if="sharedOffices.length && sharedOffices.length > 0">
            <img src="{{asset('images/locationnewsearch.png')}}" class="set-head2-images-location">
            <span class="set-location-name-search-result">

                <span style="font-family: Montserrat, Helvetica, sans-serif;" id="setCity"></span>,<span style="font-family: Montserrat, Helvetica, sans-serif;" id="setCountry"></span>

                <span v-if="sharedOffices.length == 1">
                    <span v-for="sharedOffice in sharedOffices">
                        @{{ sharedOffice.city_name }}
                    </span>,
                    <span v-for="sharedOffice in sharedOffices">
                            @{{ sharedOffice.country_name }}
                    </span>
                </span>
            </span>
        </div>
        <div v-else>
            <p>No record found</p>
        </div>
    </div>
    <div class="container search-container">
       {{--<img src="{{asset('images/searchbarrNew.png')}}" class="set-search-bar-image-mobile-responsive-tabs-grid-list">--}}
       {{--<input type="text" id="searchTxt" name="search" class="form-control set-search-input-results" placeholder="{{trans('common.where_would_you_like_to_rent')}} ?" >--}}

        <img class="set-search-bar-image-mobile-responsive-tabs-grid-list" src="{{asset('images/searchbarrNew.png')}}">
        <input type="text" id="searchbarMVR" class="form-control set-search-input-results searchbarMVR1" placeholder="{{trans('common.where_would_you_like_to_rent')}} ?" />
{{--        <img class="set-toolbar-mobile-responsive-view search" src="{{asset('images/toolbars12 copy.png')}}" style="width: 60px;left: 121px;top: -45px;">--}}
        <br>
   </div>
    <br>

    <div class="preload">
        <img src="https://i.imgur.com/KUJoe.gif">
    </div>
    <div id="btnContainer">
        <div class="found-items-results">
            @{{sharedOffices.length}} items founds
        </div>
        <i onclick="gridView()" class="fas fa-th fa-fa-fabars-grid"></i>
        <i onclick="listView()" class="fas fa-list  fa-fa-fabars-list"></i>
        <div class="vl"></div>
        <div class="set-sort-by-search-results">SORT BY <img style="width:12px;" src="{{asset('images/arrowdown copy.png')}}"></div>
    </div>
    <br>
    <div class="preload">
        <img src="https://i.imgur.com/KUJoe.gif">
    </div>
    <div class="container set-container-tabs-girdandlist">
        <div class="gridviewcolumn">
            <div class="row">
                <div class="col-sm-6 col-xs-6 set-col-sm-xs-6-grid-view" v-for="sharedOffice in sharedOffices" @click="getSharedOfficeByID(sharedOffice.office_name, sharedOffice.id,sharedOffice)">
                    <div class="panel panel-default set-panel-default-search-results-grid-view">
                        <div class="panel-heading set-panel-heading-search-results-grid-view">
                            <img :src="'/'+sharedOffice.image" class="set-gird-view-image">
                        </div>
                        <div class="panel-body set-panel-body-search-results-grid-view">
                            <p class="set-office-space-name-grid-view">@{{ getOfficeNameMV(sharedOffice) }}</p>
                            <p class="set-office-name-location-grid-view"><img class="set-office-location-grid-view" src="{{asset('images/locationnewsearch.png')}}">@{{ getsharedOfficeLocation(sharedOffice) }}</p>
                            <p class="grid-view-stars" v-if="getSharedOfficeRatingMVS(sharedOffice) == 1">
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view activeSearchStars" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view" aria-hidden="true"></i>
                                <span class="set-grid-view-review">(@{{ getSharedOfficeRatingMVS(sharedOffice) }})</span>
                            </p>
                            <p class="grid-view-stars" v-if="getSharedOfficeRatingMVS(sharedOffice) == 2">
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view activeSearchStars" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view activeSearchStars" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view" aria-hidden="true"></i>
                                <span class="set-grid-view-review">(@{{ getSharedOfficeRatingMVS(sharedOffice) }})</span>
                            </p>
                            <p class="grid-view-stars" v-if="getSharedOfficeRatingMVS(sharedOffice) == 3">
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view activeSearchStars" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view activeSearchStars" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view activeSearchStars" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view" aria-hidden="true"></i>
                                <span class="set-grid-view-review">(@{{ getSharedOfficeRatingMVS(sharedOffice) }})</span>
                            </p>
                            <p class="grid-view-stars" v-if="getSharedOfficeRatingMVS(sharedOffice) == 4">
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view activeSearchStars" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view activeSearchStars" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view activeSearchStars" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view activeSearchStars" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view" aria-hidden="true"></i>
                                <span class="set-grid-view-review">(@{{ getSharedOfficeRatingMVS(sharedOffice) }})</span>
                            </p>
                            <p class="grid-view-stars" v-if="getSharedOfficeRatingMVS(sharedOffice) == 5">
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view activeSearchStars" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view activeSearchStars" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view activeSearchStars" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view activeSearchStars" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view activeSearchStars" aria-hidden="true"></i>
                                <span class="set-grid-view-review">(@{{ getSharedOfficeRatingMVS(sharedOffice) }})</span>
                            </p>
                            <p class="grid-view-stars" v-if="getSharedOfficeRatingMVS(sharedOffice) == 0">
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view" aria-hidden="true"></i>
                                <i class="fa fa-star set-fa-fa-fastars-mobile-grid-view" aria-hidden="true"></i>
                                <span class="set-grid-view-review">(@{{ getSharedOfficeRatingMVS(sharedOffice) }})</span>
                            </p>
                            <div class="row set-grid-view-row-2">
                                    <div class="col-sm-6 col-xs-6 set-col-md-6feature-grid-view">
                                        <span class="set-features-grid-view" v-if="sharedOffice.shfacilities && sharedOffice.shfacilities.length <= 4">
                                            @{{ sharedOffice.shfacilities && sharedOffice.shfacilities.length }} features
                                        </span>
                                        <span class="set-features-grid-view" v-else>
                                            @{{ sharedOffice.shfacilities && sharedOffice.shfacilities.length }}+ features
                                        </span>
                                    </div>
                                    <div class="col-sm-6 col-xs-6 set-col-md-6price-grid-view">
                                        <span class="set-price-grid-view">
                                             $@{{ sharedOffice.min_seat_price }} / Monthly
                                        </span>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="listviewcolumn">
                <div class="panel panel-default set-panel-default-search-results-list-view" v-for="sharedOffice in sharedOffices" @click="getSharedOfficeByID(sharedOffice.office_name, sharedOffice.id,sharedOffice)">
                    <div class="panel-heading set-panel-heading-search-results-list-view">
                        <div class="row set-row-list-view">
                            <div class="col-md-3 set-col-md-3-list-view">
                                <img :src="'/'+sharedOffice.image" class="set-list-view-image">
                            </div>
                            <div class="col-md-9 set-col-md-9-list-view">
                                <p class="set-office-space-name-list-view">@{{ getOfficeNameMV(sharedOffice) }}</p>
                                <p class="set-office-name-location-list-view"><img class="set-office-location-list-view" src="{{asset('images/locationnewsearch.png')}}">@{{ getsharedOfficeLocation(sharedOffice) }}</p>
                                <p class="list-view-stars" v-if="getSharedOfficeRatingMVS(sharedOffice) == 1">
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view sctiveStarsListView" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view" aria-hidden="true"></i>
                                    <span class="set-list-view-review">(@{{ getSharedOfficeRatingMVS(sharedOffice) }})</span>
                                </p>
                                <p class="list-view-stars" v-if="getSharedOfficeRatingMVS(sharedOffice) == 2">
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view sctiveStarsListView" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view sctiveStarsListView" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view" aria-hidden="true"></i>
                                    <span class="set-list-view-review">(@{{ getSharedOfficeRatingMVS(sharedOffice) }})</span>
                                </p>
                                <p class="list-view-stars" v-if="getSharedOfficeRatingMVS(sharedOffice) == 3">
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view sctiveStarsListView" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view sctiveStarsListView" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view sctiveStarsListView" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view" aria-hidden="true"></i>
                                    <span class="set-list-view-review">(@{{ getSharedOfficeRatingMVS(sharedOffice) }})</span>
                                </p>
                                <p class="list-view-stars" v-if="getSharedOfficeRatingMVS(sharedOffice) == 4">
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view sctiveStarsListView" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view sctiveStarsListView" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view sctiveStarsListView" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view sctiveStarsListView" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view" aria-hidden="true"></i>
                                    <span class="set-list-view-review">(@{{ getSharedOfficeRatingMVS(sharedOffice) }})</span>
                                </p>
                                <p class="list-view-stars" v-if="getSharedOfficeRatingMVS(sharedOffice) == 5">
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view sctiveStarsListView" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view sctiveStarsListView" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view sctiveStarsListView" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view sctiveStarsListView" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view sctiveStarsListView" aria-hidden="true"></i>
                                    <span class="set-list-view-review">(@{{ getSharedOfficeRatingMVS(sharedOffice) }})</span>
                                </p>
                                <p class="list-view-stars" v-if="getSharedOfficeRatingMVS(sharedOffice) == 0">
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view" aria-hidden="true"></i>
                                    <i class="fa fa-star set-fa-fa-fastars-mobile-list-view" aria-hidden="true"></i>
                                    <span class="set-list-view-review">(@{{ getSharedOfficeRatingMVS(sharedOffice) }})</span>
                                </p>
                                <hr class="set-list-view-hr">
                                <p class="set-list-view-description">
                                    @{{ getsharedOfficedesc(sharedOffice) }}
                                </p>
                                <span class="set-features-list-view" v-if="sharedOffice.shfacilities && sharedOffice.shfacilities.length <= 4">
                                    @{{ sharedOffice.shfacilities && sharedOffice.shfacilities.length }} features
                                </span>
                                <span class="set-features-list-view" v-else>
                                    @{{ sharedOffice.shfacilities && sharedOffice.shfacilities.length }}+ features
                                </span>
                                <span class="set-price-list-view">
                                    $@{{ sharedOffice.min_seat_price }} / Monthly
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- mobile-view-search results -->
@include('frontend.coWorks.scripts.searchDetails')
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script>
// $(document).ready(function(){
//     setTimeout(() => {
//         var urlParams = new URLSearchParams(window.location.search);
// if(urlParams.has('location')){
//     let id = urlParams.get('location');
//     id = id.replace(' ','_');
//     $('#'+id).click();
//     console.log(id);
//
// }else{
//     console.log('f');
// }
//
//     }, 500);
//
// });
</script>

@endsection
