@extends("frontend.coWorks.layouts.default")
@section('title')
    {{trans('common.coworking_spaces_title')}}
@endsection
@section('header')
    <link href="{{asset('custom/css/frontend/coWork/register.css')}}" rel="stylesheet" type="text/css">
    <style>
        .lds-ring {
            position: absolute;
            width: 100%;
            height: 37px;
            bottom: 70px;
            display: flex;
            justify-content: center;
            right: 1px;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 28px;
            height: 28px;
            margin: 8px;
            border: 4px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
@endsection
@section("content")
<div class="row set-main-row-register-coWorkSpace">
    <div class="col-md-12" style="padding:0px">
        <div class="col-md-6 set-padding-right">
            <img class="set-image-with-coworker-register" src="{{asset('custom/css/frontend/coWork/image/sharedofficeProple.png')}}">
            <p class="set-paragraph">Share your space to global</p>
            <small class="set-small-text-image">Mandate is to use technology to make the world a better place.We utilize technology to simplify online recruitment processes.</small>
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="container set-register-container-2">
                <img src="{{asset('/custom/css/frontend/coWork/image/bubbles.png')}}" class="set-image-bubbles">
                <h2 class="set-xenren-welcome">Welcome to XenRen</h2>
                <h2 class="set-xenren-signup">SignUp by enter information below</h2>
                <form id="shared-office-sign-up">
                    <input type="hidden" value="{{csrf_token()}}">
                    {{--<div class="col-md-12 set-padding-left-12">--}}
                    <div class="row set-signUp-cowork-space-main-row">
                        <div class="col-md-6 set-padding-left-6">
                            <div class="form-group set-form-group">
                                <label class="set-label-cowork-space-register">Office Name </label>
                                <select class="form-control js-example-basic-single set-select-2" id="office_name" name="sharedoffices_id">
                                    <option v-for="offices in sharedOffices" :value="offices.id">@{{ getOfficeName(offices) }}</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 set-padding-left-6">
                            <div class="form-group set-form-group">
                                <label class="set-label-cowork-space-register">Username </label>
                                <input type="text" name="username" id="username" class="form-control set-email-input">
                            </div>
                        </div>

                        <div class="col-md-6 set-padding-left-6">
                            <div class="form-group set-form-group">
                                <label class="set-label-cowork-space-register">Email </label>
                                 <input type="text" name="email" id="email" class="form-control set-email-input">
                            </div>
                        </div>

                        <div class="col-md-6 set-padding-left-6">
                            <div class="form-group set-form-group">
                                <label class="set-label-cowork-space-register">Password </label>
                                <input type="password" name="password" id="password" class="form-control set-email-input">
                            </div>
                        </div>

                        <div class="col-md-6 set-padding-left-6">
                            <div class="form-group set-form-group">
                                <label class="set-label-cowork-space-register">Retype Password </label>
                                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control set-email-input">
                            </div>
                        </div>

                        <div class="col-md-6 set-padding-left-6">
                            <div class="form-group set-form-group set-align-center-text" id="createAccount">
                                <img class="set-back-image-button-sign-up" src="{{asset('custom/css/frontend/coWork/image/buttonimagesharedoffice.png')}}">
                                <button class="set-creat-account" type="button" name="submit">
                                    <span id="registerForm">create account</span>
                                </button>
                                <div class="lds-ring" style="display: none;"><div></div><div></div><div></div><div></div></div>
                            </div>
                        </div>

                        <div class="col-md-6 set-padding-left-6">
                            <div class="form-group set-form-group">
                                <span class="set-already-account">Already have an account?</span>
                            </div>
                        </div>

                        <div class="col-md-6 set-padding-left-6">
                            <div class="form-group set-form-group">
                                <span class="set-sign-in" onclick="window.location.href='{{url('/en/coworking-spaces/sign-in')}}'">Sign in</span>
                            </div>
                        </div>
                    </div>
                </form>
                <input id="cust-url" type="text" style="display: none;">
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $(function(){
            $('#createAccount').click(function (event) {
                event.preventDefault();
                $('#registerForm').hide();
                $('.lds-ring').show();
                var office_id = $('#office_name').val();
                var email = $('#email').val();
                var password = $('#password').val();
                var confirmPassword = $('#password_confirmation').val();
                var username = $('#username').val();
                var token = '{!! csrf_token() !!}';
                if(username === '') {
                    toastr.error('username field is required');
                    $('#registerForm').show();
                    $('.lds-ring').hide();
                } else if(email === '') {
                    toastr.error('email field is required');
                    $('#registerForm').show();
                    $('.lds-ring').hide();
                } else if(password === ''){
                    toastr.error('password field is required');
                    $('#registerForm').show();
                    $('.lds-ring').hide();
                } else if(password.length < 7){
                    toastr.error('password must be at least 8 characters');
                    $('#registerForm').show();
                    $('.lds-ring').hide();
                } else if(password !== confirmPassword) {
                    toastr.error('password and confirm password must match');
                    $('#registerForm').show();
                    $('.lds-ring').hide();
                } else {
                    var url = '/shared-office/create-owner';
                    $.ajax({
                        type:'POST',
                        url:url,
                        headers:{
                            Authorization: token,
                        },
                        data:{
                            'office_id':office_id,
                            'username':username,
                            'email':email,
                            'password':password,
                            'password_confirmation':confirmPassword,
                        },
                        success: function (response) {
                            toastr.success('Shared Office owner created successfully');
                            $('#registerForm').show();
                            $('.lds-ring').hide();
                            window.location.href='/{{\Session::get('lang')}}/coworking-spaces/sign-in';
                        },
                        error: function (error) {
                            toastr.error('Some thing went wrong');
                            $('#registerForm').show();
                            $('.lds-ring').hide();
                        }
                    });
                }
            });

            // GET AND AUTO SELECT IF PARAM EXIST
            setTimeout(function(){
                var searchParams = new URLSearchParams(location.search)
                if (searchParams.has('selected')) {
                    var selected = searchParams.get('selected')
                    $('#office_name').val(selected)
                }
            }, 1500);

            // GENERATE AUTO SELECTING URL
            $('.set-xenren-welcome').click(function () {
                var url = location.href,
                    key = 'selected',
                    val = $('#office_name').val();

                function setUrlParameter(url, key, val) {
                    var key = encodeURIComponent(key),
                        val = encodeURIComponent(val);

                    var baseUrl = url.split('?')[0],
                        newParam = key + '=' + val,
                        params = '?' + newParam;

                    if (url.split('?')[1] == undefined){ // if there are no query strings, make urlQueryString empty
                        urlQueryString = '';
                    } else {
                        urlQueryString = '?' + url.split('?')[1];
                    }

                    // If the "search" string exists, then build params from it
                    if (urlQueryString) {
                        var updateRegex = new RegExp('([\?&])' + key + '[^&]*');
                        var removeRegex = new RegExp('([\?&])' + key + '=[^&;]+[&;]?');

                        if (typeof val === 'undefined' || val === null || val === '') { // Remove param if val is empty
                            params = urlQueryString.replace(removeRegex, "$1");
                            params = params.replace(/[&;]$/, "");
                        } else if (urlQueryString.match(updateRegex) !== null) { // If param exists already, update it
                            params = urlQueryString.replace(updateRegex, "$1" + newParam);
                        } else if (urlQueryString==''){ // If there are no query strings
                            params = '?' + newParam;
                        } else { // Otherwise, add it to end of query string
                            params = urlQueryString + '&' + newParam;
                        }
                    }
                    // no parameter was set so, not need the question mark
                    params = params === '?' ? '' : params;

                    return baseUrl + params;
                }

                var urlTxt = $('#cust-url').val(setUrlParameter(url, key, val));
                copyUrl(urlTxt);
            });

            function copyUrl(url) {
                $('#cust-url').show()
                var copyText = document.getElementById("cust-url");
                copyText.select();
                copyText.setSelectionRange(0, 99999); /*For mobile devices*/
                document.execCommand("copy");
                $('#cust-url').hide();
            }
        });

        // $(document).ready(function(){

        // });
    </script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script>
        let token = '{{ session('api_token') }}';
        let setToken = `Bearer ${token}`;
        new Vue({
            el:'#app',
            props:['id'],
            data: {
                companyName:'',
                sharedOffices:[],
                full_name:'',
                phone_number:'',
                location:'',
                email_address:'',
                filename:'',
                file_name:'',
                type_filter:'',
                listLoader:false
            },
            methods: {

                getSharedOfficeName()
                {
                    this.$http.get('/api/sharedoffice?sort=desc',{
                        headers:{
                            Authorization: setToken
                        }
                    }).then(function(response){
                        this.sharedOffices = response.data;
                    }, function(error){
                        console.log(error.data);
                    });
                },

                getOfficeName(sharedoffices)
                {
                    const locale = '{{\Config::get('app.locale')}}';

                    if(locale === 'cn') {
                        if(sharedoffices.version_chinese === 'true' && sharedoffices.version_english === 'true') {
                            return sharedoffices.shared_office_language_data.office_name_cn;
                        } else if(sharedoffices.version_chinese === 'true' && sharedoffices.version_english === 'false') {
                            return sharedoffices.shared_office_language_data.office_name_cn;
                        } else if(sharedoffices.version_chinese === 'false' && sharedoffices.version_english === 'true') {
                            return sharedoffices.office_name;
                        }
                    } else if(locale === 'en') {
                        if(sharedoffices.version_chinese === 'true' && sharedoffices.version_english === 'true') {
                            return sharedoffices.office_name;
                        } else if(sharedoffices.version_chinese === 'true' && sharedoffices.version_english === 'false') {
                            return sharedoffices.shared_office_language_data.office_name_cn;
                        } else if(sharedoffices.version_chinese === 'false' && sharedoffices.version_english === 'true') {
                            return sharedoffices.office_name;
                        }
                    }
                    return sharedoffices ? sharedoffices.office_name : sharedoffices.shared_office_language_data.office_name_cn;
                },

            },
            mounted()
            {
                this.getSharedOfficeName();
            }
        });
    </script>
@endsection
