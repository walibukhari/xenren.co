@extends("frontend.coWorks.layouts.default")
@section('title')
    {{trans('common.coworking_spaces_title')}}
@endsection
@section('header')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{asset('custom/css/frontend/coWork/login.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('custom/css/frontend/coWork/forgotPassword.css')}}" rel="stylesheet" type="text/css">
    <style>
        .lds-ring {
            position: absolute;
            width: 100%;
            height: 37px;
            bottom: 70px;
            display: flex;
            justify-content: center;
            right: 8px;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 28px;
            height: 28px;
            margin: 8px;
            border: 4px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
@endsection
@section("content")
    <div class="row set-main-row-login-coWorkSpace">
        <div class="col-md-12 set-padding-login-col-md-6-2-left">
            <div class="col-md-6 set-col-md-6-paragraph-small-text">
                <p class="set-login-paragraph">Cowork space management tools, for free!</p>
                <small class="set-small-text-image">We use technology to simplify online recruitment process and make your space manageable and efficient.</small>
            </div>
            <div class="col-md-6 pull-right set-padding-col-md-6-2login-cowork-space">
                <div class="coWorkSpace-login-logo">
                    <img src="{{asset('custom/css/frontend/coWork/image/xenrenLogoLoginPageCoWork.png')}}">
                    <p class="set-logo-xenren-name">XenRen</p>
                    <small class="set-small-logo-xenren-name">Coworker & Coworking space</small>
                    <br>
                </div>
                <form class="set-form-login-coWorkspace" id="reset-form" method="post">
                    <div class="row">

                        <div class="col-md-6 text-center set-col-md-6-forgot-password-col-md-6">
                            <p class="resetpassword-c">
                                Reset your password
                            </p>
                            <p class="emailaddress-c">
                                Enter the email address associated with your account.
                            </p>
                        </div>

                        {{--<div class="col-md-12 set-col-md-12-login-cowork-space">--}}
                            <div class="col-md-6 pull-left set-col-md-6-login-coworkSpace">
                                <div class="form-group set-form-group-email-margin-bottom">
                                    <label class="set-login-email-label-coWork">Email</label>
                                    <input type="text" name="email" id="email" class="form-control set-login-email-input">
                                </div>
                            </div>
                        {{--</div>--}}


                        {{--<div class="col-md-12 set-col-md-12-login-cowork-space">--}}
                            <div class="col-md-6 pull-left set-col-md-6-login-coworkSpace">
                                <div class="form-group set-margin-botton-singin-CowOrkSpace">
                                    <img class="set-login-coWork-image" src="{{asset('custom/css/frontend/coWork/image/buttonimagesharedoffice.png')}}">
                                    <button type="button" style="height:37px;" onclick="resetPassword()" class="set-sign-in-image-coWork btn green pull-right">
                                        <span id="sendBtn">
                                        <i class="fa fa-paper-plane set-fa-fapaperplane" aria-hidden="true"></i>
                                        Send</span>
                                    </button>
                                    <div class="lds-ring" style="display: none;"><div></div><div></div><div></div><div></div></div>
                                </div>
                            </div>
                        {{--</div>--}}

                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function resetPassword()
        {
            $('#sendBtn').hide();
            $('.lds-ring').show();
            var action =  $('#reset-form').attr('method');
            var email = $('#email').val();
            var session = '{{\Session::get('lang')}}';
            var url = '/'+session+'/coworking-spaces/forgot-password';
            if(email === ''){
                $('#sendBtn').show();
                $('.lds-ring').hide();
                toastr.error('please enter you email address');
            } else {
                $.ajax({
                    url:url,
                    type:action,
                    data:{'email':email},

                    success: function(response)
                    {
                        if(response.error)
                        {
                            toastr.error(response.error);
                            $('#sendBtn').show();
                            $('.lds-ring').hide();
                        } else if(response.success)
                        {
                            toastr.success(response.success);
                            $('#sendBtn').show();
                            $('.lds-ring').hide();
                        }
                    },
                    error: function(error)
                    {
                        toastr.error(error);
                        $('#sendBtn').show();
                        $('.lds-ring').hide();
                    }
                });
            }
        }
    </script>
@endsection
