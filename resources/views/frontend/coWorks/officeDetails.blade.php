@extends('frontend.coWorks.layouts.default')
@section('title')
<?php
    echo 'Coworking Spaces'.' '.ucfirst($city).', '. ucfirst($country) .' ' . '|' .' '.'Xenren.co';
?>
@endsection
@section('url')
{{url()->current()}}
@endsection

@section('officeImage')
{{$officeImage}}
@endsection

@section('header')
    <link href="{{asset('custom/css/frontend/coWork/officeDetails.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('custom/css/frontend/coWork/css/mobile-view-office-details.css')}}" rel="stylesheet" type="text/css">
    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="{{asset('swiperdist/css/swiper.min.css')}}">
    <style>
        .swiper-slide{
        }
        .swiper-button-next, .swiper-button-prev{
            display: none !important;
        }
        .setImagetopUp2{
            margin-left: 93px !important;
        }
        #set-country > .setMalaysiabredcrum{

        }
        #set-city > .setMalaysiabredcrum{
            margin-left: 15px;
        }
        #set-officeName > .setMalaysiabredcrum{
            margin-left: 10px;
        }
        #closemodal {
            width:18px !important;
        }
        .lds-ellipsis {
            display: inline-block;
            position: relative;
            width: 80px;
            height: 80px;
        }
        .lds-ellipsis div {
            position: absolute;
            top: 33px;
            width: 13px;
            height: 13px;
            border-radius: 50%;
            background: #fff;
            animation-timing-function: cubic-bezier(0, 1, 1, 0);
        }
        .lds-ellipsis div:nth-child(1) {
            left: 8px;
            animation: lds-ellipsis1 0.6s infinite;
        }
        .lds-ellipsis div:nth-child(2) {
            left: 8px;
            animation: lds-ellipsis2 0.6s infinite;
        }
        .lds-ellipsis div:nth-child(3) {
            left: 32px;
            animation: lds-ellipsis2 0.6s infinite;
        }
        .lds-ellipsis div:nth-child(4) {
            left: 56px;
            animation: lds-ellipsis3 0.6s infinite;
        }
        @keyframes lds-ellipsis1 {
            0% {
                transform: scale(0);
            }
            100% {
                transform: scale(1);
            }
        }
        @keyframes lds-ellipsis3 {
            0% {
                transform: scale(1);
            }
            100% {
                transform: scale(0);
            }
        }
        @keyframes lds-ellipsis2 {
            0% {
                transform: translate(0, 0);
            }
            100% {
                transform: translate(24px, 0);
            }
        }
        /* The container */
        .containerPayCheckBox {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 22px;
            float: right;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        /* Hide the browser's default checkbox */
        .containerPayCheckBox input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }
        /* Create a custom checkbox */
        .checkmarkPay {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
            border-radius: 50%;
        }
        /* On mouse-over, add a grey background color */
        .containerPayCheckBox:hover input ~ .checkmark {
            background-color: #ccc;
            border: solid white;
        }
        /* When the checkbox is checked, add a blue background */
        .containerPayCheckBox input:checked ~ .checkmarkPay {
            background-color: #57a224;
            background: #57a224;
            border-radius: 50%;
        }
        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmarkPay:after {
            content: "";
            position: absolute;
            display: none;
            border: solid white;
        }
        /* Show the checkmark when checked */
        .containerPayCheckBox input:checked ~ .checkmarkPay:after {
            display: block;
        }
        .roww{
            padding-right: 10px;
            padding-left: 10px;
        }
        .btn-Green:hover {
            background: #fff;
            color:#57a224;
            width:100%;
            border: 1px solid #57a224 !important;
            padding: 16px;
        }
        .btn-Green{
            background: #57a224;
            color:#fff;
            width:100%;
            padding: 16px;
            border: 1px solid #57a224 !important;
        }
        .btn-Green2{
            background: #fff;
            color:#57a224;
            width:100%;
            border: 1px solid #57a224 !important;
            padding: 16px;
        }
        .btn-Green2:hover{
            background: #57a224;
            color:#fff;
            width:100%;
            padding: 16px;
        }
        /* Style the checkmark/indicator */
        .containerPayCheckBox .checkmarkPay:after {
            left: 10px;
            top: 7px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
        #modalHeader {
            border-bottom: 0px;
            padding-top: 25px;
        }
        #modalContent{
            border-radius: 2%;
        }
        #modalBody{
            padding-top: 0px;
        }
        .pp{
            color:gray;
            font-size: 16px;
            padding-left: 10px;
            margin-bottom: 6px;
        }
        #modalTitle{
            font-weight: 500;
            font-size: 22px;
            padding-left: 10px;
        }
        .spanText{
            font-size: 16px;
            padding-left: 10px;
        }
        #modalDialoge{
            position: relative;
            top:95px;
        }
        .swiper-container-horizontal>.swiper-scrollbar{
            display: none !important;
        }
    </style>
@endsection
@section("content")
    <div class="container setMainContainer">
        <div class="row">
            <div class="col-md-2 left-sidebar setCol3SearchBarP">
                <div class="panel panel-default set-pael-defaultcol-md-2-area">
                    <span class="set-text-odp">{{trans('common.how_works')}}</span><br>
                    <div class="row how-ite-wors-row">
                        <div class="col-md-12">
                            <span class="set-cirlces">1</span><span class="set-book-now-text-xenren">{{trans('common.simply_click')}}.</span>
                        </div>
                        <div class="col-md-12">
                            <span class="set-cirlces">2</span><span class="set-book-now-text-xenren">{{trans('common.submit_required')}}.</span>
                        </div>
                        <div class="col-md-12">
                            <span class="set-cirlces">3</span><span class="set-book-now-text-xenren">{{trans('common.your_in')}}...</span>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default set-col-md-2-panel-2-default">
                    <div class="col-md-12 set-col-md-2-panel-2-default-col-md-12">
                        @if(\Auth::guard('users')->check())
                            <button data-toggle="modal" id="m1" data-target="#myBookSpaceModal" class="modals btn btn-default set-panel-default-2-btn">{{trans('common.book_now')}}</button>
                        @else
                            <button onclick="openLoginPopup()" class="modals btn btn-default set-panel-default-2-btn">{{trans('common.book_now')}}</button>
                        @endif
                    </div>
                </div>

                <div class="panel panel-default set-col-md-2-panel-3-default">
                    <div class="col-md-12 set-col-md-2-panel-3-default-col-md-12">
                        <img class="set-binocularsaimage" src="{{asset('images/binoculars.png')}}">
                        <span class="set-image-text-span-default-3">@{{ this.getOfficeDetails.total_members_in_office && this.getOfficeDetails.total_members_in_office.length }} {{trans('common.people_looking_space')}}</span>
                    </div>
                </div>

                <div class="panel panel-default set-col-md-2-panel-4-default">
                    <div class="col-md-12 set-col-md-2-panel-4-default-col-md-12">
                        <span class="guest-loves">{{trans('common.guest_love')}}...</span>
                    </div>
                    <div class="col-md-12 set-col-md-2-panel-4-1-default-col-md-12">
                        <img src="{{asset('images/ratedstars.png')}}">
                        <span class="friendlystaffs">"{{trans('common.friendly_staff')}}"</span><br>
                        <small class="set-small">99 {{trans('common.rated_view')}}</small>
                    </div>
                    <div class="col-md-12 set-col-md-2-panel-4-1-default-col-md-12">
                        <img src="{{asset('images/ratedstars.png')}}">
                        <span class="friendlystaffs">"{{trans('common.best_space')}}"</span><br>
                        <small class="set-small">99 {{trans('common.rated_view')}}</small>
                    </div>
                    <div class="col-md-12 set-col-md-2-panel-4-1-default-col-md-12">
                        <img src="{{asset('images/sharedofficeheart.png')}}">
                        <span class="friendlystaffs">"{{trans('common.best_location')}}"</span><br>
                        <small class="set-small">55 {{trans('common.rated_view')}}</small>
                    </div>
                    <div class="col-md-12 set-col-md-2-panel-4-1-default-col-md-12">
                        <img src="{{asset('images/parkingsign.png')}}">
                        <span class="friendlystaffs">{{trans('common.free_park')}}</span>
                    </div>
                    <div class="col-md-12"  style="margin-top: 10px">
                        @if(\Auth::guard('users')->check())
                            <button data-toggle="modal" data-target="#myBookSpaceModal" class="modals btn btn-default set-panel-default-2-btn">{{trans('common.book_now')}}</button>
                        @else
                            <button onclick="openLoginPopup()" class="modals btn btn-default set-panel-default-2-btn">{{trans('common.book_now')}}</button>
                        @endif
                    </div>
                </div>


                <div class="panel panel-default set-col-md-2-panel-5-default" v-show="showReviews" v-for="(review, index) in this.getOfficeDetails.shared_office_rating" v-if="index < 4">
                    <div class="col-md-12 set-col-md-2-panel-5-default-col-md-12">
                        <span class="set-ver-beautiful-view">"@{{ review.comment }}"</span>
                    </div>
                    <div class="col-md-12 set-col-md-2-panel-5-1-default-col-md-12">
                        <img class="set-face-image-odp" onerror="this.src='/images/svster.png'" v-bind:src="review.rater.img_avatar">
                        <span class="set-name-odp">@{{ review.rater.name }}</span>
                        <div class="col-md-12" style="margin-top:24px;text-align: left;display:inline-block">
                            <img class="set-country-image" v-bind:src="'/images/Flags-Icon-Set/24x24/'+review.rater.country_flag+'.png'">
                            <small class="set-country-name-odp">@{{ review.rater.country_name }}</small>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default set-col-md-2-panel-5-default" v-show="moreReviews" v-for="(review , index) in this.getOfficeDetails.shared_office_rating" v-if="index < 30">
                    <div class="col-md-12 set-col-md-2-panel-5-default-col-md-12">
                        <span class="set-ver-beautiful-view">"@{{ review.comment }}"</span>
                    </div>
                    <div class="col-md-12 set-col-md-2-panel-5-1-default-col-md-12">
                        <img class="set-face-image-odp" onerror="this.src='/images/svster.png'" v-bind:src="review.rater.img_avatar">
                        <span class="set-name-odp">@{{ review.rater.name }}</span>
                        <div class="col-md-12" style="margin-top:24px;text-align: left;display:inline-block;">
                            <img class="set-country-image" v-bind:src="'/images/Flags-Icon-Set/24x24/'+review.rater.country_flag+'.png'">
                            <small class="set-country-name-odp">@{{ review.rater.country_name }}</small>
                        </div>
                    </div>
                </div>
                <div v-if="this.getOfficeDetails.shared_office_rating && this.getOfficeDetails.shared_office_rating.length >= 4">
                <div class="col-md-12">
                    <button class="btn btn-default set-panel-default-5-btn" @click="loadReviews()">{{trans('common.view_all_views')}}</button>
                </div>
                </div>
                <div v-else>
                </div>


            </div>
            <div class="col-md-10 col-sm-10 col-xs-12 setColMd10">
                <div class="col-md-12 set-col-md-12-location-map-office-name">
                    <div class="col-md-8 set-col-md-6-primary-title-1">
                        <h1 class="primary-title">
                            @{{getOfficeDetails.office_name}}
                            @if(isset($category_data) && $category_data != '')
                                <span style="font-size: 14px;">{{$category_data['name']}}<img style="width: 20px;" src="/{{$category_data->image}}" /></span>
                            @endif
                        </h1>

                        <h5><i class="fas fa-map-marker set-office-details-fa-marker"></i>@{{ getOfficeDetails.location }} - <a class="show-on-map-set" :href=`http://maps.google.com/?q=${getOfficeDetails.lat},${getOfficeDetails.lng}` target="_blank">{{trans('common.show_map')}}</a></h5>
                    </div>
                    <div class="col-md-4 set-col-md-6-primary-title-2">
                        <div class="col-md-12 text-right setBookNowDiv">
                            @if(\Auth::user())
                                {{--<i class="far fa-thumbs-up" @click="likeOffice()" style="cursor: pointer"></i>--}}
                                <img src="{{asset('images/goodhand.png')}}" @click="likeOffice()" style="vertical-align: sub; padding-right:6px;cursor: pointer">
                            @else
                                <img src="{{asset('images/goodhand.png')}}" onclick="openLoginPopup()" style="padding-right:6px;vertical-align: sub;cursor: pointer">
                                {{--<i class="far fa-thumbs-up" onclick="window.location.href='/login'" style="cursor: pointer"></i>--}}
                            @endif
                            {{--<i class="fas fa-share-alt" @click="copyLinkToClipBoard()" style="cursor: pointer"></i>--}}
                                <img src="{{asset('images/shareLink.png')}}" @click="copyLinkToClipBoard()" style="cursor: pointer">
                                @if(\Auth::guard('users')->check())
                                    <a href="#" data-toggle="modal" data-target="#myBookSpaceModal" class="modals btn btn-primary book-now">{{trans('common.book_now')}}</a>
                                @else
                                    <a href="#" onclick="openLoginPopup()" class="modals btn btn-primary book-now">{{trans('common.book_now')}}</a>
                                @endif
                        </div>
                    </div>
                </div>
                @if(isset($office_video) && $office_video != '')
                    <div class="col-md-6 col-sm-6 col-xs-12 green-valley set-office-main-images">
                        <div class="row video">
                            <video width="600" class="thevideo" controls style="height:258px;width:100%" data-play="hover">
                                <source src="/{{$office_video}}" type="video/mp4">
                            </video>
                        </div>
                    </div>
                @else
                   <div class="col-md-6 col-sm-6 col-xs-12 green-valley set-office-main-images">
                       <div class="row">
                            <a :href="'/'+getOfficeDetails.image" data-lightbox="roadtrip">
                                <img class="img-responsive image-modify set-main-image-shared-office" :src="'/'+getOfficeDetails.image" width="100%" alt=""  onerror="this.src='/images/noimage.png'"/>
                            </a>
                        </div>
                    </div>
                @endif
                <div class="col-md-6 col-sm-6 col-xs-12 setRowCol6ColXs6 set-col-md-6-2-images-2">
                    <div class="row">

                        <div>

                            <div v-for="index in getOfficeImagesLength">
                                <div class="col-md-6 col-sm-6 col-xs-12 set-col-md-6-2-office-details-page ahsan">
                                    <div class="row" style="display:block">
                                        <a href="" data-lightbox="roadtrip">
                                            <img data-lightbox="roadtrip"  class="img-responsive image-modify setImageModify" src="{{asset('/images/noimage.jpeg')}}" alt=""  onerror="this.src='{{asset('/images/noimage.jpeg')}}'"/>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div v-for="(images, index) in getOfficeImages">
                                <div class="col-md-6 col-sm-6 col-xs-12 set-col-md-6-2-office-details-page ahsan" v-if="index < 4">
                                    <div class="row" style="display:block">
                                        <a :href="images.priority_images" data-lightbox="roadtrip">
                                            <img data-lightbox="roadtrip"  class="img-responsive image-modify setImageModify" :src="getImagePath(images.priority_images)" alt=""  onerror="this.src='{{asset('/images/noimage.jpeg')}}'"/>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 set-col-md-6-2-office-details-page mughal" v-else>
                                    <div class="row" style="display:none">
                                        <a :href="images.priority_images" data-lightbox="roadtrip">
                                            <img data-lightbox="roadtrip"  class="img-responsive image-modify setImageModify" :src="getImagePath(images.priority_images)" alt=""  onerror="this.src='/images/noimage.jpeg'"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 set-padding-right-col-md-12">
                    <div class="col-md-9 col-sm-9 col-xs-12 nopadding-left tab-detail setColMd9OverView">
                        <div class="card">
                            <div class="set-scroller">
                                <ul class="nav nav-tabs stickyHeaderNavBar" role="tablist" id="navbar">
                                    <li role="presentation" class="anchor stickyHeaderNavBarMenu"><a id="" href="#overview" aria-controls="home" role="tab" >{{trans('common.overview')}}</a></li>
                                    <li role="presentation" class="anchor stickyHeaderNavBarMenu"><a id="" href="#facalities" aria-controls="facalities" role="tab"  >{{trans('common.facalities')}}</a></li>
                                    <li role="presentation" class="anchor stickyHeaderNavBarMenu"><a id="" href="#officefee" aria-controls="officefee" role="tab" >{{trans('common.office_fee')}}</a></li>
                                    <li role="presentation" class="anchor stickyHeaderNavBarMenu"><a id="" href="#location" aria-controls="location" role="tab" >{{trans('common.location')}}</a></li>
                                    <li role="presentation" class="anchor stickyHeaderNavBarMenu"><a id="" href="#reviews" aria-controls="reviews" role="tab"  >{{trans('common.reviews')}}</a></li>
                                </ul>
                            </div>
                            <!--main container-->
                            <div class="container set-section-container-scroll">
                                <div id="overview" class="col-md-12 set-col-md-12-scroll-view-area" style="padding-left:7px;">

                                            <p style="word-wrap:break-word;">@{{ getOfficeDetails.description }}</p>
                                </div>
                                <br>
                                <div id="facalities" class="col-md-12 set-col-md-12-scroll-view-area">
                                        <div class="row">
                                            <div class="col-md-12 set-facalities-col-md-12" style="padding-left:5px;">
                                                <h2 class="section-title"><img src="{{asset('custom/css/frontend/searchCoWork/images/facilities-1.png')}}"> {{trans('common.facalities')}}</h2>
                                            </div>
                                        </div>
                                        <div class="col-md-12 facilities set-padding-left-of-office-facilities">
                                            <div class="row setSectionupCol3colxs3">
                                                <div class="col-md-3 col-sm-3 col-xs-12 set-border-right">
                                                    <div class="row">
                                                        <h5>@{{ getOfficeDetails.office_space }}</h5>
                                                        <h6 class="set-border-right-h6">{{trans('common.office_space')}}</h6>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-12 set-border-right">
                                                    <h5>@{{ getOfficeDetails.total_dedicated_desks }}</h5>
                                                    <h6>{{trans('common.dedicated_desk')}}</h6>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-12 set-border-right">
                                                    <h5>@{{ getOfficeDetails.total_meeting_room }}</h5>
                                                    <h6>{{trans('common.meeting_room')}}</h6>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                    <h3>@{{ time_format(getOfficeDetails.monday_opening_time) }} TO @{{ time_format(getOfficeDetails.monday_closing_time) }}</h3>
                                                    <h6 class="set-h6-operate-hours">{{trans('common.operate_hourss')}}</h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 facilitie-bellow setFacilitie">
                                            <div class="row setFacilitieRowsUFF">
                                                <div class="col-md-3 col-sm-3 col-xs-6" v-for="cat in getOfficeDetails.categories">
                                                    <div class="row">
                                                        <h5><img :src="`/${cat.productcategory.image}`"/> @{{ getCategoryName(cat.productcategory.name) }}</h5>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-6" v-for="shfacility in getOfficeDetails.shfacilities">
                                                    <div class="row">
                                                        <h5><img :src="`${shfacility.image}`"/> @{{ shfacility.title }}</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <br>
                                <div v-if="this.getOfficeDetails.officeProducts && this.getOfficeDetails.office_products.length == 0"></div>
                                <div id="officefee" class="col-md-12 set-col-md-12-scroll-view-area" v-else>
                                        <div class="row">
                                            <div class="col-md-12 office-feee-col-md-12">
                                                <h2 class="section-title">
                                                    <img src="{{asset('images/coinimages.png')}}">
                                                    {{trans('common.office_fee')}}</h2>
                                            </div>
                                        </div>
                                        <div class="panel panel-default set-header-tab-panel" id="web-view">
                                            <a id="defaultOpen" class="tablink set-border-radius-top-left" data-toggle="tab" onclick="openHead('London', this, '#57af2b','#fff')" >{{trans('common.hot_desks')}}</a>
                                            <a class="tablink" data-toggle="tab" onclick="openHead('Paris', this, '#57af2b','#fff')">{{trans('common.dedicated_desks')}}</a>
                                            <a class="tablink set-border-radius-top-right" data-toggle="tab" onclick="openHead('Tokyo', this, '#57af2b','#fff')">{{trans('common.office_desks')}}</a>

                                            <div class="col-md-12 set-col-md-12-row-header-tabs">
                                                <div class="col-md-3 set-text-align-center-col-md-3">
                                                    No of peoples
                                                </div>
                                                <div class="col-md-3 set-text-align-center-col-md-3">
                                                    Duration Type
                                                </div>
                                                <div class="col-md-3 set-text-align-center-col-md-3">
                                                    Price
                                                </div>
                                                <div class="col-md-3 set-text-align-center-col-md-3">
                                                    Availablity
                                                </div>
                                            </div>
                                            <div class="container set-container-innner-header-tab">
                                                <div id="London" class="tabcontent" v-for="officeFee in this.getOfficeDetails.office_products" v-if="officeFee.type == '{!! App\Models\SharedOfficeFee::HOT_DESK_TYPE !!}'">
                                                    <div class="col-md-12 set-col-12-md-inner-part-head" v-if="officeFee.hourly_price !== 'n/a' && officeFee.hourly_price !== 0">
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            @{{ officeFee.no_of_peoples }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            1 hours
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            $@{{ Number((officeFee.hourly_price)) }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3">
                                                            <div data-toggle="modal" v-on:click="reserveModal(officeFee.type,officeFee.no_of_peoples,officeFee.hourly_price,'hourly')" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 set-col-12-md-inner-part-head" v-if="officeFee.daily_price !== 'n/a' && officeFee.daily_price !== 0">
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            @{{ officeFee.no_of_peoples }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            1 day
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            $@{{ Number((officeFee.daily_price)) }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3">
                                                            <div data-toggle="modal" v-on:click="reserveModal(officeFee.type,officeFee.no_of_peoples,officeFee.daily_price,'daily')" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 set-col-12-md-inner-part-head" v-if="officeFee.weekly_price !== 'n/a' && officeFee.weekly_price !== 0">
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            @{{ officeFee.no_of_peoples }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            1 week
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            $@{{ Number((officeFee.weekly_price)) }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3">
                                                            <div data-toggle="modal" v-on:click="reserveModal(officeFee.type,officeFee.no_of_peoples,officeFee.weekly_price,'weekly')" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 set-col-12-md-inner-part-head" v-if="officeFee.month_price !== 'n/a' && officeFee.month_price !== 0">
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            @{{ officeFee.no_of_peoples }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            1 month
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            $@{{ Number((officeFee.month_price)) }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3">
                                                            <div data-toggle="modal" v-on:click="reserveModal(officeFee.type,officeFee.no_of_peoples,officeFee.month_price,'monthly')" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="Paris" class="tabcontent set-border-radius-top-left" v-for="officeFee in this.getOfficeDetails.office_products" v-if="officeFee.type == '{!! App\Models\SharedOfficeFee::DEDICATED_DESK_TYPE !!}'">
                                                    <div class="col-md-12 set-col-12-md-inner-part-head" v-if="officeFee.hourly_price !== 'n/a' && officeFee.hourly_price !== 0">
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            @{{ officeFee.no_of_peoples }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            1 hours
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            $@{{ Number((officeFee.hourly_price)) }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3">
                                                            <div data-toggle="modal" v-on:click="reserveModal(officeFee.type,officeFee.no_of_peoples,officeFee.hourly_price,'hourly')" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 set-col-12-md-inner-part-head" v-if="officeFee.daily_price  !== 'n/a' && officeFee.daily_price  !== 0">
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            @{{ officeFee.no_of_peoples }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            1 day
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            $@{{ Number((officeFee.daily_price)) }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3">
                                                            <div data-toggle="modal" v-on:click="reserveModal(officeFee.type,officeFee.no_of_peoples,officeFee.daily_price,'daily')" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 set-col-12-md-inner-part-head" v-if="officeFee.weekly_price !== 'n/a' && officeFee.weekly_price !== 0">
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            @{{ officeFee.no_of_peoples }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            1 week
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            $@{{ Number((officeFee.weekly_price)) }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3">
                                                            <div data-toggle="modal" v-on:click="reserveModal(officeFee.type,officeFee.no_of_peoples,officeFee.weekly_price,'weekly')" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 set-col-12-md-inner-part-head" v-if="officeFee.month_price !== 'n/a' && officeFee.month_price !== 0">
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            @{{ officeFee.no_of_peoples }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            1 month
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            $@{{ Number((officeFee.month_price)) }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3">
                                                            <div data-toggle="modal" v-on:click="reserveModal(officeFee.type,officeFee.no_of_peoples,officeFee.month_price,'monthly')" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="Tokyo" class="tabcontent set-border-radius-top-right" v-for="officeFee in this.getOfficeDetails.office_products" v-if="officeFee.type == '{!! App\Models\SharedOfficeFee::OFFICE_DESK_TYPE !!}'">
                                                    <div class="col-md-12 set-col-12-md-inner-part-head" v-if="officeFee.hourly_price !== 'n/a' && officeFee.hourly_price !== 0">
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            @{{ officeFee.no_of_peoples }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            1 hours
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            $@{{ Number((officeFee.hourly_price)) }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3">
                                                            <div data-toggle="modal" v-on:click="reserveModal(officeFee.type,officeFee.no_of_peoples,officeFee.hourly_price,'hourly')" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 set-col-12-md-inner-part-head" v-if="officeFee.daily_price !== 'n/a' && officeFee.daily_price !== 0">
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            @{{ officeFee.no_of_peoples }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            1 day
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            $@{{ Number((officeFee.daily_price)) }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3">
                                                            <div data-toggle="modal" v-on:click="reserveModal(officeFee.type,officeFee.no_of_peoples,officeFee.daily_price,'daily')" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 set-col-12-md-inner-part-head" v-if="officeFee.weekly_price !== 'n/a' && officeFee.weekly_price !== 0">
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            @{{ officeFee.no_of_peoples }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            1 week
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            $@{{ Number((officeFee.weekly_price)) }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3">
                                                            <div data-toggle="modal" v-on:click="reserveModal(officeFee.type,officeFee.no_of_peoples,officeFee.weekly_price,'weekly')" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 set-col-12-md-inner-part-head" v-if="officeFee.month_price == 'n/a' && officeFee.month_price !== 0">
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            @{{ officeFee.no_of_peoples }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            1 month
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3-text">
                                                            $@{{ Number((officeFee.month_price)) }}
                                                        </div>
                                                        <div class="col-md-3 set-reserve-col-md-3">
                                                            <div data-toggle="modal" v-on:click="reserveModal(officeFee.type,officeFee.no_of_peoples,officeFee.month_price,'monthly')" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        {{--mobile view area--}}
                                    <div class="container set-mobile-view-container">
                                        <div class="panel panel-default set-header-tab-panel set-mobile-view-area">
                                            <div class="panel panel-heading set-mobile-view-area-color">{{trans('common.hot_desks')}}</div>
                                            <div class="panel-body">
                                                <div class="table-responsive set-scroller-table">
                                                <table class="table table-striped set-col-md-12-mobile-view">
                                                    <thead>
                                                    <tr>
                                                        <th>No of peoples</th>
                                                        <th>Duration Type</th>
                                                        <th>Price</th>
                                                        <th>Availablity</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr v-for="officeFee in this.getOfficeDetails.shared_office_fee" v-if="officeFee.type == '{{\App\Models\SharedOfficeFee::HOT_DESK_TYPE}}'">
                                                            <td>@{{ officeFee.no_of_peoples }}</td>
                                                            <td>1 hours</td>
                                                            <td>@{{ officeFee.hourly_price }}</td>
                                                            <td><div data-toggle="modal" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                            </td>
                                                        </tr>
                                                        <tr v-for="officeFee in this.getOfficeDetails.shared_office_fee" v-if="officeFee.type == '{{\App\Models\SharedOfficeFee::HOT_DESK_TYPE}}'">
                                                            <td>@{{ officeFee.no_of_peoples }}</td>
                                                            <td>1 day</td>
                                                            <td>@{{ officeFee.daily_price }}</td>
                                                            <td><div data-toggle="modal" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                            </td>
                                                        </tr>
                                                        <tr v-for="officeFee in this.getOfficeDetails.shared_office_fee" v-if="officeFee.type == '{{\App\Models\SharedOfficeFee::HOT_DESK_TYPE}}'">
                                                            <td>@{{ officeFee.no_of_peoples }}</td>
                                                            <td>1 day</td>
                                                            <td>@{{ officeFee.weekly_price }}</td>
                                                            <td><div data-toggle="modal" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                            </td>
                                                        </tr>
                                                        <tr v-for="officeFee in this.getOfficeDetails.shared_office_fee" v-if="officeFee.type == '{{\App\Models\SharedOfficeFee::HOT_DESK_TYPE}}'">
                                                            <td>@{{ officeFee.no_of_peoples }}</td>
                                                            <td>1 day</td>
                                                            <td>@{{ officeFee.month_price }}</td>
                                                            <td><div data-toggle="modal" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-default set-header-tab-panel set-mobile-view-area">
                                            <div class="panel panel-heading set-mobile-view-area-color">{{trans('common.dedicated_desks')}}</div>
                                            <div class="panel-body">
                                                <div class="table-responsive set-scroller-table">
                                                <table class="table table-striped set-col-md-12-mobile-view">
                                                    <thead>
                                                    <tr>
                                                        <th>No of peoples</th>
                                                        <th>Duration Type</th>
                                                        <th>Price</th>
                                                        <th> Availablity</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr v-for="officeFee in this.getOfficeDetails.shared_office_fee" v-if="officeFee.type == '{{\App\Models\SharedOfficeFee::DEDICATED_DESK_TYPE}}'">
                                                        <td>@{{ officeFee.no_of_peoples }}</td>
                                                        <td>1 hours</td>
                                                        <td>@{{ officeFee.hourly_price }}</td>
                                                        <td><div data-toggle="modal" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                        </td>
                                                    </tr>
                                                    <tr v-for="officeFee in this.getOfficeDetails.shared_office_fee" v-if="officeFee.type == '{{\App\Models\SharedOfficeFee::DEDICATED_DESK_TYPE}}'">
                                                        <td>@{{ officeFee.no_of_peoples }}</td>
                                                        <td>1 day</td>
                                                        <td>@{{ officeFee.daily_price }}</td>
                                                        <td><div data-toggle="modal" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                        </td>
                                                    </tr>
                                                    <tr v-for="officeFee in this.getOfficeDetails.shared_office_fee" v-if="officeFee.type == '{{\App\Models\SharedOfficeFee::DEDICATED_DESK_TYPE}}'">
                                                        <td>@{{ officeFee.no_of_peoples }}</td>
                                                        <td>1 day</td>
                                                        <td>@{{ officeFee.weekly_price }}</td>
                                                        <td><div data-toggle="modal" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                        </td>
                                                    </tr>
                                                    <tr v-for="officeFee in this.getOfficeDetails.shared_office_fee" v-if="officeFee.type == '{{\App\Models\SharedOfficeFee::DEDICATED_DESK_TYPE}}'">
                                                        <td>@{{ officeFee.no_of_peoples }}</td>
                                                        <td>1 day</td>
                                                        <td>@{{ officeFee.month_price }}</td>
                                                        <td><div data-toggle="modal" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-default set-header-tab-panel set-mobile-view-area">
                                            <div class="panel panel-heading set-mobile-view-area-color">{{trans('common.office_desks')}}</div>
                                            <div class="panel-body">
                                                <div class="table-responsive set-scroller-table">
                                                <table class="table table-striped set-col-md-12-mobile-view">
                                                    <thead>
                                                    <tr>
                                                        <th>No of peoples</th>
                                                        <th>Duration Type</th>
                                                        <th>Price</th>
                                                        <th> Availablity</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr v-for="officeFee in this.getOfficeDetails.shared_office_fee" v-if="officeFee.type == '{{\App\Models\SharedOfficeFee::OFFICE_DESK_TYPE}}'">
                                                        <td>@{{ officeFee.no_of_peoples }}</td>
                                                        <td>1 hours</td>
                                                        <td>@{{ officeFee.hourly_price }}</td>
                                                        <td><div data-toggle="modal" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                        </td>
                                                    </tr>
                                                    <tr v-for="officeFee in this.getOfficeDetails.shared_office_fee" v-if="officeFee.type == '{{\App\Models\SharedOfficeFee::OFFICE_DESK_TYPE}}'">
                                                        <td>@{{ officeFee.no_of_peoples }}</td>
                                                        <td>1 day</td>
                                                        <td>@{{ officeFee.daily_price }}</td>
                                                        <td><div data-toggle="modal" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                        </td>
                                                    </tr>
                                                    <tr v-for="officeFee in this.getOfficeDetails.shared_office_fee" v-if="officeFee.type == '{{\App\Models\SharedOfficeFee::OFFICE_DESK_TYPE}}'">
                                                        <td>@{{ officeFee.no_of_peoples }}</td>
                                                        <td>1 day</td>
                                                        <td>@{{ officeFee.weekly_price }}</td>
                                                        <td><div data-toggle="modal" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                        </td>
                                                    </tr>
                                                    <tr v-for="officeFee in this.getOfficeDetails.shared_office_fee" v-if="officeFee.type == '{{\App\Models\SharedOfficeFee::OFFICE_DESK_TYPE}}'">
                                                        <td>@{{ officeFee.no_of_peoples }}</td>
                                                        <td>1 day</td>
                                                        <td>@{{ officeFee.month_price }}</td>
                                                        <td><div data-toggle="modal" data-target="#myReserveSpace" class="set-reserve">Reserve</div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                        {{--mobile view area--}}
                                </div>
                                <br>
                                <div id="location" class="col-md-12 facilities set-col-md-12-scroll-view-area">
                                        <div class="row" style="margin-right:0px;margin-left:0px;">
                                            <h2 class="section-title">
                                                <img src="{{asset('images/location_sharedoffice.png')}}">
                                                {{trans('common.location')}}</h2>
                                            <h5>@{{getOfficeDetails.location}}</h5>
                                            <div id="map"></div>
                                        </div>
                                </div>
                                <br>
                                <div id="reviews" class="col-md-12 facilities reviews set-col-md-12-scroll-view-area">
                                        <div class="col-md-12 why-choose">
                                            <div class="row">
                                                <h2>{{trans('common.reason_choose')}} @{{ getOfficeDetails.office_name }} {{trans('common.work_space')}}</h2>

                                                <div class="reason-box col-md-12" v-if="getOfficeDetails.bulletins && getOfficeDetails.bulletins.length > 0">
                                                    <div class="col-md-6 col-sm-6 col-xs-12" v-for="bulletin in getOfficeDetails.bulletins">
                                                        <div class="row">
                                                            <h3><img src="{{asset('custom/css/frontend/searchCoWork/images/tick.png') }}"/>
                                                                @{{ getBulletinText(bulletin.bulletin_detail) }}
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="reason-box col-md-12" v-else>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <div class="row">
                                                            <h3 class="reason-box-dum-text"><img src="{{asset('custom/css/frontend/searchCoWork/images/tick.png') }}"/>
                                                                {{trans('common.dum_text')}}
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 set-reiviews-section-2-padding">
                                        <h2 class="section-title"><img src="{{asset('custom/css/frontend/searchCoWork/images/review.png') }}"> {{trans('common.reviews')}}</h2>
                                        <div v-if="getOfficeDetails.rating > 1">
                                            <div class="col-md-5 col-sm-5 col-xs-12" >
                                                <h3><span id="rating" style="font-size: 20px">@{{ getOfficeDetails.rating }} {{trans('common.out_of')}} 5</span></h3>
                                                <div class="pull-left setBarProgress" style="width: 250px;">
                                                    <div class="progress" style=" height: 19px; margin: 8px 0;border-radius: 30px;border: 1px solid #ccc;">
                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width: 80%">
                                                        </div>
                                                    </div>
                                                </div>
                                                <h2 class="write-review-if">{{trans('common.want_review')}}?</h2>
                                                <div class="text-center">
                                                    @if(\Auth::guard('users')->check())
                                                        <a class="write-review-btn" data-toggle="modal" @click="officeRating(getOfficeDetails.id)" data-target="#myReviewPlease" href="#">{{trans('common.write_review')}}</a>
                                                    @else
                                                        <a onclick="openLoginPopup()" class="write-review-btn"  href="#">{{trans('common.write_review')}}</a>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-7 col-sm-7 col-xs-12 b-left set-b-left-area">
                                                <div class="review-box">
                                                    <div class="slideshow-container">
                                                        <div class="mySlidesArea fadeArea">
                                                            <span class="float-right" v-for="starts in filled_stars"><i class="text-warning fa fa-star"></i></span>
                                                            <span class="float-right" v-for="starts in unfilled_stars"><i class="text-warning fa fa-star" style="color: grey"></i></span>

                                                            <span>@{{ getOfficeDetails.rating }} {{trans('common.out_of')}} 5 {{trans('common.stars')}}</span>
                                                            <h5 class="r-name">@{{ getReviewComment('username') }}<span class="r-date">- @{{ getReviewComment('date') }}</span></h5>
                                                            <p>@{{ getReviewComment('comment') }}</p>
                                                            <span>{{trans('common.helpful')}} ?</span>
                                                            @if(\Auth::user())
                                                                <span style="cursor: pointer" @click="countHelpful('{!! \Auth::user()->id !!}')">
                                                            <img src="{{asset('custom/css/frontend/searchCoWork/images/heart.png')}}"/>
                                                        </span>
                                                            @else
                                                                <span style="cursor: pointer" onclick="openLoginPopup()">
                                                            <img src="{{asset('custom/css/frontend/searchCoWork/images/heart.png')}}"/>
                                                        </span>
                                                            @endif
                                                        </div>
                                                        <div class="mySlidesArea fadeArea" v-for="rec in this.getOfficeDetails.shared_office_rating">
                                                            <span class="float-right" v-for="starts in filled_stars"><i class="text-warning fa fa-star"></i></span>
                                                            <span class="float-right" v-for="starts in unfilled_stars"><i class="text-warning fa fa-star" style="color: grey"></i></span>

                                                            <span>@{{ rec.rate }} {{trans('common.out_of')}} 5 {{trans('common.stars')}}</span>
                                                            <h5 class="r-name">@{{ rec.rater.name }}<span class="r-date">- @{{ rec.created_at }}</span></h5>
                                                            <p>@{{ rec.comment }}</p>
                                                            <span>{{trans('common.helpful')}} ?</span>
                                                            @if(\Auth::user())
                                                                <span style="cursor: pointer" @click="countHelpful(rec.id)">
                                                            <img src="{{asset('custom/css/frontend/searchCoWork/images/heart.png')}}"/>
                                                        </span>
                                                            @else
                                                                <span style="cursor: pointer" onclick="openLoginPopup()">
                                                            <img src="{{asset('custom/css/frontend/searchCoWork/images/heart.png')}}"/>
                                                        </span>
                                                            @endif
                                                            <span class="h-counter">@{{ rec.helpful_count }}</span>
                                                        </div>

                                                        <div class="set-doArea">
                                                            <span class="dotArea" v-for="(rex, index) in this.getOfficeDetails.shared_office_rating" v-on:click="currentSlide(index)"></span>
                                                        </div>
                                                    </div>
                                                    <div v-else>
                                                        {{trans('common.no_review_yet')}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div v-else>
                                            <div class="col-md-5 col-sm-5 col-xs-12 set-row-02-10" >
                                                <i class='fa fa-star fa-fw stars-no-review'></i>
                                                <i class='fa fa-star fa-fw stars-no-review'></i>
                                                <i class='fa fa-star fa-fw stars-no-review'></i>
                                                <i class='fa fa-star fa-fw stars-no-review'></i>
                                                <i class='fa fa-star fa-fw stars-no-review'></i>
                                                <p class="No-review">{{trans('common.no_review')}}</p>
                                                <hr class="divideHr">
                                                <h2 class="write-review">{{trans('common.want_review')}}?</h2>
                                                <div class="text-center">
                                                    @if(\Auth::guard('users')->check())
                                                        <a class="write-review-btn" data-toggle="modal" @click="officeRating(getOfficeDetails.id)" data-target="#myReviewPlease" href="#">{{trans('common.write_review')}}</a>
                                                    @else
                                                        <a onclick="openLoginPopup()" class="write-review-btn"  href="#">{{trans('common.write_review')}}</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>
                        </div>
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padding setCol3MdOH">
                        <h5 class="right-label"><img src="{{asset('custom/css/frontend/searchCoWork/images/clock-1.png')}}"/> {{trans('common.operating_hours')}}</h5>
                        <div class="oprtaing-hour">
                            <h6 class="set-operating-hours-monday">{{trans('common.monday_friday')}}</h6>
                            <span>@{{ formatTime(getOfficeDetails.monday_opening_time) }} - @{{ formatTime(getOfficeDetails.monday_closing_time) }} </span>
                            <h6>{{trans('common.sat')}}</h6>
                            <span>@{{ formatTime(getOfficeDetails.saturday_opening_time) }}  - @{{ formatTime(getOfficeDetails.saturday_closing_time) }} </span>
                            <h6>{{trans('common.sun')}}</h6>
                            <span>@{{ formatTime(getOfficeDetails.sunday_opening_time) }}  - @{{  formatTime(getOfficeDetails.sunday_closing_time)  }} </span>
                        </div>
                        <br>
                        <h5 class="right-label"><img src="{{asset('custom/css/frontend/searchCoWork/images/contact-1.png')}}">{{trans('common.contact_details')}}</h5>
                        <div class="oprtaing-hour" v-if="getOfficeDetails.contact_name == 0 || getOfficeDetails.contact_phone == 0 || getOfficeDetails.contact_email == 0 || getOfficeDetails.contact_address == 0">
                            <p class="comingSoon">Coming Soon !</p>
                            <div class="text-center" style="    margin-top: 15px;">
                                <a class="message-link" href="#" data-toggle="modal" data-target="#myModal">{{trans('common.messages')}}</a>
                            </div>
                            <h3>{{trans('common.share_friend')}}</h3>
                            <div class="text-center social-r">
                            <span>
                                <img style="cursor:pointer;" src="{{asset('images/facebooklogosharedoffice.png')}}" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u={{\Request::url()}}&display=popup')">
                            </span>
                                <span>
                                    <img style="cursor:pointer;" src="{{asset('images/twittersharedoffice.png')}}" onclick="window.open('https://twitter.com/intent/tweet?url={{\Request::url()}}')">
                                </span>
                            </div>
                        </div>
                        <div class="oprtaing-hour" v-else>
                            <h4>@{{ getOfficeDetails.contact_name }} <i class="fas fa-circle"></i></h4>
                            <span>@{{ getOfficeDetails.contact_phone }} </span><br>
                            <span id="contact_email">@{{ getOfficeDetails.contact_email }} </span><br/>
                            <span>@{{ getOfficeDetails.contact_address }} </span><br>
                            <div class="text-center" style="    margin-top: 15px;">
                                <a class="message-link" href="#" data-toggle="modal" data-target="#myModal">{{trans('common.messages')}}</a>
                            </div>
                            <h3>{{trans('common.share_friend')}}</h3>
                            <div class="text-center social-r">
                            <span>
                                <img style="cursor:pointer;" src="{{asset('images/facebooklogosharedoffice.png')}}" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u={{\Request::url()}}&display=popup')">
                            </span>
                                <span>
                                    <img style="cursor:pointer;" src="{{asset('images/twittersharedoffice.png')}}" onclick="window.open('https://twitter.com/intent/tweet?url={{\Request::url()}}')">
                                </span>
                            </div>
                        </div>

                    </div>
                </div>
                </div>
            </div>
        </div>
    <!-- Modal -->
    @include('frontend.coWorks.modal.modal')
    @if($is_mobile)
    <!-- mobile biew office details-->
    <div class="preload">
        <img src="https://i.imgur.com/KUJoe.gif">
    </div>
    @include('frontend.coWorks.officeDetailMobileView',[
        'officeData' => $officeData
    ])
    <!-- mobile biew office details-->
    @endif
@include('frontend.coWorks.scripts.officeDetail')
@endsection

