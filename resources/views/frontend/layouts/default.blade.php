<!DOCTYPE html>
<!--[if IE 8]> <html lang="{{ trans('common.lang') }}" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="{{ trans('common.lang') }}" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="{{ trans('common.lang') }}">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '522786281936287');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=522786281936287&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
    <?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start('ob_gzhandler'); else ob_start(); ?>
    @include('analytics')
    <meta charset="utf-8" />
    <title>@yield('title')</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="@yield('description')" name="description"/>
    <meta content="@yield('keywords')" name="keywords"/>
    <meta content="@yield('author')" name="author"/>
    <meta property="og:url" content="@yield('url')" />
    <meta property="og:image" content="@yield('images')">
    <meta property="og:type" content="article" />
    <meta property="og:title" content="@yield('ogtitle')" />
    <meta property="og:description" content="@yield('ogdescription')" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:locale:alternate" content="es_ES" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="630" />

    <meta property="wb:webmaster" content="4bfa78a5221bb48a" />
    <meta property="qc:admins" content="2552431241756375" />
    <meta name="msvalidate.01" content="DAC0A9F3A3973E8BE8A5EB7FA07899EE" />
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    @include('frontend.includes.headscript')
{{--    <link href="/custom/css/frontend/homePagePopup.css" rel="stylesheet" type="text/css"/>--}}
    <link href="{{asset('css/innerMinifiedHome.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">
    {{--<link rel="shortcut icon" href="favicon.ico" />--}}
    {{--<link href="{{asset('xenren-assets/images/logo/fav_new.png')}}" rel="icon" type="image/x-icon">--}}
    <link href="{{asset('images/Favicon.jpg')}}" rel="icon" type="image/x-icon" />
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+SC&display=swap&subset=chinese-simplified" rel="stylesheet">
        @section('header') @show

        <style>
            .setSpnBg {
                right: -2px !important;
                margin-top: 0px !important;
                padding: 3px 5px !important;
                margin-left: -9px !important;
                background-color: #C63031 !important;
                height: 18px !important;
                width: 18px !important;
            }
            #header_feedback span {
                border: 2px solid #57b029 !important;
                padding: 7px 13px !important;
                border-radius: 5px !important;
                color: #57b029 !important;
                font-weight: 200 !important;
                line-height: 20px !important;
                font-size: 14px !important;
            }
            .icon-bell:before {
                content: "\e027";
                color: #5ec329 !important;
                font-weight: bold !important;
            }
            .img-thumbnail {
                border-radius: 16% !important;
            }
            .newBellIcon2{
                color:#fff !important;
                background:#57a224;
                padding:10px 24px 10px 10px;
                position: relative;
                left: 23px;
                border-radius: 100%;
            }
            .newBellIcon1{
                color:#fff !important;
                background:#57a224;
                padding:10px 24px 10px 10px;
                position: relative;
                left: 37px;
                border-radius: 100%;
            }
            .settoplinks {
                padding: 27px 0px 0px 18px !important;
                display: flex !important;
                align-items: center !important;
                margin-bottom: 0px !important;
                justify-content: flex-end;
            }
            .settoplinks li {
                padding: 2px 8px 2px 0px !important;
            }
            .set-reload-page-show-content{
                padding:0px;
            }
            @media (max-width: 500px) {
                .set-row-margin-show-content-page-reload {
                    padding-left:0px;
                    padding-right:0px;
                    padding-top: 13px;
                }
            }
        </style>
    <script src="{{asset('js/pusher.js')}}"></script>

    <script type="text/javascript" defer="defer">
			window.searchSkills = [];
			var pusher = new Pusher("{{config('pusher.connections.main.auth_key')}}");
			var channel = pusher.subscribe('private_chat');
			var channel_1 = pusher.subscribe('system_chat');
    </script>
    <script src="https://sdk.pushy.me/web/1.0.5/pushy-sdk.js"></script>
    @if(isset($inboxEvents))
        @foreach($inboxEvents as $val)
            @if($val->category == 1)
                <script type="text/javascript" defer="defer">
									{{--console.log('norm: '+'{{$val->id}}');--}}
channel.bind('{{$val->id}}', function(data) {
										console.log('norm socket called');
										var user_id = '{{\Auth::user()->id}}';
										if(data.sender_id != user_id) {
											var name = '';
											if (data.name) {
												var name = ' from ' + data.name;
											}
											var txt = 'New Private Message';
											alertInfo(txt + name);
										}
									});
                </script>
            @else
                <script defer="defer">
									{{--                        console.log('sys: '+'{{$val->id}}');--}}
channel_1.bind('{{$val->id}}', function(data) {
										console.log('sys socket called');
										var user_id = '{{\Auth::user()->id}}';
										if(data.sender_id != user_id) {
											var name = '';
											if (data.name) {
												var name = ' from ' + data.name;
											}
											var txt = 'New System Message';
											alertInfo(txt + name);
										}
									});
                </script>
        @endif
    @endforeach
@endif
    <!-- END HEAD -->
</head>

<body onload="checkCookie()" class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md" style="{{ $bothSideMargin }}">
<!-- BEGIN HEADER -->
@if(\Auth::user() && (\Auth::user()->name == '' || is_null(\Auth::user()->name) || \Auth::user()->about_me == '' || is_null(\Auth::user()->about_me)))
<div class="container set-reload-page-show-content" style="width: 100%;" id="set-reload-page-content">
    <div class="row set-row-margin-show-content-page-reload">
        <div class="col-md-12">
            please complete your profile information by clicking
            <a href="/personalInformation">here</a>
        </div>
    </div>
</div>
@endif
@include('frontend.includes.header')
<!-- END HEADER -->
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
@if(isset($_G['route_name']) && $_G['route_name'] == 'home')
    <div class="setStyleMPGe"></div>
@endif
<!-- END HEADER & CONTENT DIVIDER -->
<!-- BEGIN CONTAINER -->
<div class="page-container" >

    <a class="text-uppercase raleway back-url-mobile" href="javascript:history.back()">
        <span class="fa fa-angle-left"></span> Back
    </a>

    @if (!isset($_G['disable_sidemenu']) || $_G['disable_sidemenu'] != true)
        @include('frontend.includes.sidemenu')
    @endif
    @if( isset($_G['route_name']) && $_G['route_name'] == 'frontend.tailororders' || isset($_G['route_name']) && $_G['route_name'] == 'frontend.jobs.index' )
    @endif
    @if (isset($_G['disable_sidemenu']) && $_G['disable_sidemenu'] == true)
        @include('frontend.flash')
    @endif
    @if (isset($_G['route_name']) && ( $_G['route_name'] == 'frontend.findexperts' || isset($_G['route_name']) && $_G['route_name'] == 'frontend.findexperts.post'))
        {{--@include('frontend.findExperts.searchOption')--}}
        @include('frontend.findExperts.searchOptionNew')
    @endif
    @yield('content')

</div>
<!-- END CONTAINER -->
@include('frontend.userCenter.modalChangeUserStatus')
@include('frontend.includes.footer')
@include('frontend.includes.footscript')
@section('footer') @show
@include('frontend.includes.modal')
@section('modal')@show
{!! Form::open(['id' => 'p-key-form']) !!}
{!! Form::close() !!}
<style>
    .myDiv {
        display: block !important;
    }
    .showHover {
        border: 3px solid #5DBA2D;
        box-shadow: 0px 1px 9px 3px #c7ecc5 !important;
    }
</style>
<!-- settings area -->
<script defer="defer">
    var user_ui_preference = '{{\Auth::user() ? \Auth::user()->uipreference: false }}';
    var user_ui_preference_both = '{{\App\Models\User::USER_UI_PREFERENCE_BOTH}}';
    var user_ui_preference_freelancer = '{{\App\Models\User::USER_UI_PREFERENCE_FREELANCER}}';
    var user_ui_preference_employer = '{{\App\Models\User::USER_UI_PREFERENCE_EMPLOYER}}';
    var user_email_notification = '{{\Auth::user() ? \Auth::user()->email_notification: false }}';
    var user_wechat_notification = '{{\Auth::user() ? \Auth::user()->wechat_notification: false }}';
    var user_push_notification = '{{\Auth::user() ? \Auth::user()->push_notification: false }}';
    var user_close_account = '{{\Auth::user() ? \Auth::user()->close_account: false }}';
    var push_notification = '{{\App\Models\User::APP_PUSH_NOTIFICATION}}';
    var close_account = '{{\App\Models\User::CLOSE_MY_ACCOUNT}}';
    var email_notification = '{{\App\Models\User::USER_EMAIL_NOTIFICATION}}';
    var wechat_notification = '{{\App\Models\User::USER_WECHAT_NOTIFICATION}}';
    var checked_in_space = '{{\App\Models\User::CHECKED_IN_CO_WORK_SPACE}}';
    var checked_in_COWORK = '{{\Auth::user() ? \Auth::user()->checked_in_cowork_space : false}}';
</script>

@if(\Auth::guard('users')->check())
<script src="{{asset('js/pleaseCompleteInfo.js')}}" defer="defer"></script>
@endif
<script type="text/javascript" src="/custom/js/frontend/settings.js" defer="defer"></script>
<!-- settings area -->
@if(Auth::user())
<script defer="defer">
    $(function(){
        // Register device for push notifications
        Pushy.register({ appId: '5cc990d8a3c809350df6bc01' }).then(function (deviceToken) {
            // Print device token to console
            console.log('Pushy device token: ' + deviceToken);
            var  curruntUserId = '{{\Auth::user()->id}}';
            console.log('currunt user');
            var url = '/save/device/token/'+curruntUserId+'/'+deviceToken;
            $.ajax({
                url:url,
                Type:'GET',

                success: function(response){
                    console.log('response');
                    console.log(response);
                    console.log('response');
                },
                error: function(error){
                    console.log('response');
                    console.log(error);
                    console.log('response');
                },
            });
            // Send the token to your backend server via an HTTP GET request
            //fetch('https://your.api.hostname/register/device?token=' + deviceToken);
            // Succeeded, optionally do something to alert the user
        }).catch(function (err) {
            // Handle registration errors
            // console.error(err);
            console.error('permission denay user cannot subscribed ');
        });

        // Check if the device is registered
        if (Pushy.isRegistered()) {
            // Subscribe the device to a topic
            Pushy.subscribe('news').catch(function (err) {
                // Handle subscription errors
                console.error('Subscribe failed:', err);
            });
        }
    });
</script>
@endif
<script defer="defer">
	$(function(){
		$('.find-expert-box').hover(function(){
			if($(this).children().children().closest('div.user-info.col-md-3').children().closest('div').children().eq(1).hasClass('myDiv')) {
				$(this).children().children().closest('div.user-info.col-md-3').children().closest('div').children().eq(1).removeClass('myDiv')
			} else {
				$(this).children().children().closest('div.user-info.col-md-3').children().closest('div').children().eq(1).addClass('myDiv')
			}
		});
		$('.find-expert-box').click(function(){
			$(this).toggleClass('showHover')
		});
		$('body').on('click', '.showHover', function(e) {
			console.log('click called');
			$(this).toggleClass('showHoverClick');
		})
		$('body').on('click', '.showHoverClick', function(e) {
			$(this).toggleClass('showHoverClick');
			var className = (e.target.className)
			if (className == "setFEPPBtn btn btn-success find-experts-success pull-right" ||
				className == "btn find-experts-offer pull-right setFEPPBtn" ||
				className == "setFEPPBtn btn find-experts-offer find-experts-check pull-right" ||
				className == "btn find-experts-offer find-experts-check pull-right collapsed" ||
				className == "fa fa-envelope" ||
				className == "btn btn-success pull-right invite-freelance-gray" ||
				className == "md-click-circle md-click-animate" ||
				className == "btn find-experts-offer find-experts-request send-request-for-contact-info pull-right" ||
				className == "fa fa-heart pull-right" ||
				className == "fa fa-heart pull-right heart-red" ||
				className == "fa fa-heart pull-right  heart-red" ||
				className == "btn-add-fav-freelancer setHeartactiveFree" ||
				className == "fa fa-heart pull-right " ||
				className == "btn-add-fav-freelancer setHeart"
			) {
				return;
			}
			var url = $(this).data('reurl');
			if(url)
				window.location.href = url;
		})
	});
	$(document).on('click', function(e){
		if (e.target.className !== 'col-md-12') {
			$('.page-content div').removeClass('showHover')
		}
	});
</script>
<script src="{{asset('js/vue.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/vue-resource.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/socket.io.js')}}"></script>
{{--<script src="https://npmcdn.com/vue-select@1.3.0"></script>--}}
<style>
    .img-thumbnail {
        border-radius: 16% !important;
    }
    a img {
        border-radius: 7%;
    }
</style>
@if(\Auth::guard('users')->check())
    <script defer="defer">
			var user = {!! ($_G['user']!=null || isset($_G['user'])) ? $_G['user'] : null  !!};
    </script>

    @stack('vue')
    @stack('js')


    <script defer="defer">

			$(".advance-filter-collapse").click(function() {
				$(this).find('.fa').toggleClass('fa-chevron-circle-up fa-chevron-circle-down');
			});

			Vue.http.headers.common['X-CSRF-TOKEN'] = $('input[name="_token"]').val();
			new Vue({
				el: "#ChangeUserStatus",
				data: {
					user: user,
					skills: [],
					languages: [],


				},
				methods: {
					change: function (val) {
						console.log('val');
						console.log(val);
						console.log('val');
						// e.preventDefault();
						var input = {
							'val': this.user[val],
							'column': 'status',
						};
						console.log('input');
						console.log(input);
						console.log('input');
						this.$http.post('/personalInformation/change', input).then((response)=> {
							console.log(response.body.status);
						if(response.body.status == 'success') {
							window.location.reload();
							toastr.success('status Changed..', response.body.status, {timeOut: 5000});
						}
					}, (response)=> {
							// error
						});
					},
				}
			});


    </script>
@endif
<script type="text/javascript" defer="defer">
	$(document).ready(function() {
		$('.leftSide-menu-list').click(function(){
			$('.page-sidebar').toggleClass('collapse');
		});
		window.topupamount = 0;
	});
</script>
<script defer="defer">
	function myFunction() {
		var x = document.getElementById("myTopnav");
		if (x.className === "topnav") {
			x.className += " responsive";
		} else {
			x.className = "topnav";
		}
	}

	function clickFunction() {
		var x = document.getElementById("Topnav");
		if (x.className === "navbar") {
			x.className += " toggler";
		} else {
			x.className = "navbar";
		}
	}
	var lang = {
		submit_successful: "{{ trans('common.submit_successful') }}",
		chat_room_is_empty: "{{ trans('common.chat_room_is_empty') }}",
		download: "{{ trans('common.download') }}",
		recommended_screenshot_tool_for_project: "{{ trans('common.recommended_screenshot_tool_for_project') }}",
		recommended_management_tool_for_project: "{{ trans('common.recommended_management_tool_for_project') }}",
	};

	$(function() {
        $('.chclick').on('click', function(){
            if(this.classList.toggle('btn-active'))
            {
                $('#myModalCH').modal('show');
            }
            else {
                $('#myModalCH2').modal('show');
            }
        });
			// Multiple images preview in browser
		var imagesPreview = function(input, placeToInsertImagePreview) {

			if (input.files) {
				var filesAmount = input.files.length;

				for (i = 0; i < filesAmount; i++) {
					var reader = new FileReader();

					reader.onload = function(event) {
						var html="<div class='col-sm-3 thumbImages'>" +
							"<img style='width: 108%; height: 80px; border-radius: 5px' src='"+event.target.result+"'/>" +
							"</div>";
						$($.parseHTML(html)).prependTo(placeToInsertImagePreview);
					}

					reader.readAsDataURL(input.files[i]);
				}
			}

		};

		$('#screenshot').on('change', function() {
			$('.thumbImages').remove();
			imagesPreview(this, 'div.gallery');
		});


		$("body").click(function(){
            $(".setCustomCollapse2").collapse('hide');
            $(".setCustomCollapse3").collapse('hide');
        });


	});
</script>
<script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<script defer="defer">
    $('.scroll-div').slimScroll({});
</script>
<!-- Modal -->
@include('frontend.includes.cookiesModal')
{{--<script src="http://lab.alexcican.com/set_cookies/cookie.js" type="text/javascript" ></script>--}}
@if(\Auth::guard('users')->check())
    <script>
        localStorage.setItem('username',"{{\Auth::user()->id}}");
    </script>
    <script src="/js/cookies.js" type="text/javascript"></script >
@endif
<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
@yield('javascript')
</body>
</html>
