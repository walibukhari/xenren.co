@extends('frontend.mobDev.layouts.default')

@section('content')
    <div class="col-md-12 user-detail">
        <div class="row">
            <div id="profile-section">
                <div class="col-md-6 col-xs-6 xol-sm-6">
                    <h3>Long Graphic Design</h3>
                    <p>Posted 293 days ago</p>
                    <div class="profile-image block">
                        <img src="{{asset('custom/css/frontend/mobDev/images/user.png')}}">
                    </div>
                    <div class="profile-intro block">
                        <h2>Jojo86511</h2>
                        <p>Wuxi | UI Designer</p>
                    </div>
                </div>
                <div class="col-md-6 col-xs-6 xol-sm-6 text-right h-rightside">
                    <span><img src="{{asset('custom/css/frontend/mobDev/images/eye.png')}}">231</span>
                    <span><img src="{{asset('custom/css/frontend/mobDev/images/message.png')}}">0</span>
                    <span><img src="{{asset('custom/css/frontend/mobDev/images/thumb.png')}}">1</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="">
            <div id="content-section">
                <h2>Long graphic design for walking activities</h2>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <img src="{{asset('custom/css/frontend/mobDev/images/top.png')}}" style="width:100%;">
        </div>
    </div>
    <div class="col-md-12 text-center btn-f">
        <a href="#" class="btn btn-info btn-xs"><i class="fas fa-level-up-alt"></i> Submit your own idea</a>
        <a href="#" class="btn btn-info btn-xs"><i class="fab fa-facebook-f"></i> Share on facebook</a>
    </div>
    <div class="col-md-12 slider-bellow">
        <h2>You Might Interested </h2>
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4">
                <img src="{{asset('custom/css/frontend/mobDev/images/first-img.png')}}" class="img-responsive">
                <h3>Title</h3>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <img src="{{asset('custom/css/frontend/mobDev/images/second-img.jpg')}}" class="img-responsive">
                <h3>Title</h3>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <img src="{{asset('custom/css/frontend/mobDev/images/third-img.jpg')}}" class="img-responsive">
                <h3>Title</h3>
            </div>
        </div>
    </div>
    <div class="col-md-12 comment">
        <span><img src="{{asset('custom/css/frontend/mobDev/images/message.png')}}">3 Comments</span>
        <div class="form-group">
            <textarea class="form-control" rows="3" id="comment" placeholder="Write something"></textarea>
        </div>
        <a href="#" class="btn btn-info pull-right">Comment</a>
    </div>
    <div class="col-md-12 commentlist row">
    <div class="comment-item">
        <div class="comment-wrap">
            <div class="photo">
                <div class="avatar setAvatar"></div>
            </div>
            <div class="comment-block">
                <h3>Person Name <span>2 days ago</span></h3>
                <div class="comment-contro">
                    <a href="#"><i class="fas fa-pen"></i></a>
                    <a href="#"><i class="fas fa-trash"></i></a>
                </div>
                <p class="comment-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus </p>
                <div class="react">
                    <span class="emoji emoji--happy"></span>
                    <span class="emoji emoji--sad"></span>
                    <span class="emoji emoji--crying"></span>
                    <span class="emoji emoji--grimacing"></span>
                </div>
                <a class="thumb-react" href="#"><i class="fas fa-thumbs-up"></i></a>
            </div>
        </div>
    </div>
    <div class="comment-item">
        <div class="comment-wrap">
            <div class="photo">
                <div class="avatar setAvatar2"></div>
            </div>
            <div class="comment-block">
                <h3>Person Name <span>2 days ago</span></h3>
                <div class="comment-contro">
                    <a href="#"><i class="fas fa-pen"></i></a>
                    <a href="#"><i class="fas fa-trash"></i></a>
                </div>
                <p class="comment-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus </p>
                <div class="react">
                    <span class="emoji emoji--happy"></span>
                    <span class="emoji emoji--sad"></span>
                    <span class="emoji emoji--crying"></span>
                    <span class="emoji emoji--grimacing"></span>
                </div>
                <a href="#"><i class="fas fa-thumbs-up"></i></a>
            </div>
        </div>
    </div>
    <div class="comment-item">
        <div class="comment-wrap">
            <div class="photo">
                <div class="avatar setAvatar3"></div>
            </div>
            <div class="comment-block">
                <h3>Person Name <span>2 days ago</span></h3>
                <div class="comment-contro">
                    <a href="#"><i class="fas fa-pen"></i></a>
                    <a href="#"><i class="fas fa-trash"></i></a>
                </div>
                <p class="comment-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus </p>
                <div class="react">
                    <span class="emoji emoji--happy"></span>
                    <span class="emoji emoji--sad"></span>
                    <span class="emoji emoji--crying"></span>
                    <span class="emoji emoji--grimacing"></span>
                </div>
                <a href="#"><i class="fas fa-thumbs-up"></i></a>
            </div>
        </div>
    </div>
    <div class="comment-item">
        <div class="comment-wrap">
            <div class="photo">
                <div class="avatar setAvatar4"></div>
            </div>
            <div class="comment-block">
                <h3>Person Name <span>2 days ago</span></h3>
                <div class="comment-contro">
                    <a href="#"><i class="fas fa-pen"></i></a>
                    <a href="#"><i class="fas fa-trash"></i></a>
                </div>
                <p class="comment-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus </p>
                <div class="react">
                    <span class="emoji emoji--happy"></span>
                    <span class="emoji emoji--sad"></span>
                    <span class="emoji emoji--crying"></span>
                    <span class="emoji emoji--grimacing"></span>
                </div>
                <a href="#"><i class="fas fa-thumbs-up"></i></a>
            </div>
        </div>
    </div>
</div>
@endsection

@section("script")
@endsection

