<!DOCTYPE html>
<html lang="en">
<head>
    <title>Tablet Design</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('frontend.mobDev.partials.headScripts')
</head>
<body>
    @include('frontend.mobDev.partials.head')

        @yield("content")

    @include('frontend.mobDev.partials.footer')
    @include("frontend.mobDev.partials.footerScripts")
    @yield('script')
</body>
</html>