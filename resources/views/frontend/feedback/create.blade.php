@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.feedback') }}
@endsection

@section('description')
    {{ trans('common.feedback') }}
@endsection

@section('author')
    Xenren
@endsection

@section('url')
    https://www.xenren.co/feedback
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
    <link href="/custom/css/frontend/feedback.css" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/js/dropzone.css')}}">
    <style>
        #myId{
            display:flex;
            justify-content: center;
            align-items: center;
            width:115px;
        }
        .lds-ring {
            display: inline-block;
            position: relative;
            width: 20px;
            height: 20px;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 20px;
            height: 20px;
            margin: 1px;
            border: 2px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
@endsection
@section('content')
    <div class="row page-contant">
        <div class="col-md-12 title">
            <div id="alert-container">
            </div>
            <h3>{{ trans('common.feedback') }}</h3>
            <h4>{{ trans('common.feedback_description') }}</h4>
        </div>

        {!! Form::open(['route' => 'frontend.feedback.create.post', 'id' => 'create-feedback-form', 'method' => 'post', 'class' => 'horizontal-form', 'files' => true]) !!}
            <div class="col-md-12 body-box">
                <div class="row">
                    <div class="col-md-6 form-field">
                        <div class="form-group">
                            <label>{{ trans('common.issue_priority') }}</label>
                            <div class="search-selectbox custom-days-selectbox">
                                <select class="form-control" name="priority">
                                    @foreach( $priorities as $key => $priority)
                                    <option value="{{ $key }}">{{ $priority }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>{{ trans('common.issue_title') }}</label>
                            <input type="text" name="title" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>{{ trans('common.issue_description') }}</label>
                            <textarea class="form-control" name="description"></textarea>
                        </div>
                    </div>
                    <div class="col-md-6 form-field">
                        <div class="form-group">
                            <label>{{ trans('common.screenshot_of_issue') }}</label>
                        </div>
                        <div class="row">
                            @for( $i=0; $i<12; $i++ )
                            <div id="div-upload-{{ $i }}" class="col-md-4 m-b-20" style="{{ $i == 0? 'display:block;': 'display:none;' }}">
                                <div id="div-img-{{ $i }}" class="screenshot setFBCKPAGESS">
                                    <img class="img-{{ $i }}" src="">
                                </div>
                                <label class="screenshot_add lbl-fileupload-{{ $i }}" for="fileUploadImage-{{ $i }}" >
                                    <i class="fa fa-plus"></i>
                                    <input type="file" class="" name="fileUploadImage[]" id="fileUploadImage-{{ $i }}">
                                </label>
                            </div>
                            @endfor
                        </div>

                    </div>
                    {{-------}}
                    {{--<div class='dropzone' id='dropzoneFileUpload'>--}}
                        {{--<label class="screenshot_add lbl-fileupload-0" for="fileUploadImage-0" >--}}
                            {{--<i class="fa fa-plus"></i>--}}
                        {{--</label>--}}
                    {{--</div>--}}
                    {{-------}}
                    {{--<div id="preview"></DIV>--}}
                    <div class="col-md-12">
                        {{--<button id="myId" class="btn btn-color-custom" type="button">--}}
                            {{--{{ trans('common.start_chat') }}--}}
                        {{--</button>--}}
                        <button id="myId" class="btn btn-color-custom" name="submit" type="submit">
                            <span class="idSpanSaveChange">{{ trans('common.start_chat') }}</span>
                            <div style="display: none;" class="lds-ring">
                                <div></div><div></div><div></div><div></div>
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('footer')
     <script type="text/javascript">

        var lang = {
            image_file_too_big: "{{ trans('common.image_file_too_big') }}",
            invalid_file_type_for_feedback: "{{ trans('common.invalid_file_type_for_feedback') }}",
        };

        var url = {
            feedbackList: "{{ route('frontend.feedback',['lang' => \Session::get('lang')]) }}"
        }
     </script>
    <script src="/custom/js/frontend/createFeedback.js" type="text/javascript"></script>
     <script src="{{asset('assets/js/dropzone.js')}}"></script>
     <script>
         {{--$(function () {--}}
	         {{--Dropzone.autoDiscover = false;--}}
	         {{--var token = "{!! csrf_token() !!}";--}}

	         {{--Dropzone.options.dropzoneFileUpload = {--}}
		         {{--url: "/project/uploadImage",--}}
		         {{--paramName: "file",--}}
{{--//                 previewsContainer: "<div class='dz-preview dz-file-preview'>" +--}}
{{--//                 "<div class='dz-details'>" +--}}
{{--//                    "<div class='dz-filename'>" +--}}
{{--//                      "<span data-dz-name></span>" +--}}
{{--//                     "</div>" +--}}
{{--//                     "<div class='dz-size' data-dz-size></div>" +--}}
{{--//                        "<img data-dz-thumbnail />" +--}}
{{--//                     "</div>" +--}}
{{--//                     "<div class='dz-progress'>" +--}}
{{--//                         "<span class='dz-upload' data-dz-uploadprogress></span>" +--}}
{{--//                     "</div>" +--}}
{{--//                     "<div class='dz-success-mark'>" +--}}
{{--//                         "<span>✔</span>" +--}}
{{--//                     "</div>" +--}}
{{--//                     "<div class='dz-error-mark'>" +--}}
{{--//                         "<span>✘</span>" +--}}
{{--//                     "</div>" +--}}
{{--//                     "<div class='dz-error-message'>" +--}}
{{--//                        "<span data-dz-errormessage></span>" +--}}
{{--//                     "</div>" +--}}
{{--//                 "</div>",--}}
		         {{--maxFilesize: 10,--}}
		         {{--params: {--}}
			         {{--_token: token--}}
		         {{--},--}}
		         {{--init: function() {--}}
			         {{--this.on("addedfile", function(file) {--}}
				         {{--alert("Added file.");--}}
			         {{--})--}}
                     {{--this.on("success", function(file, response) {--}}
                         {{--console.log(response);--}}
                     {{--})--}}
		         {{--}--}}
	         {{--}--}}

	         {{--$('#dropzoneFileUpload').dropzone();--}}

         {{--})--}}
     </script>
@endsection
