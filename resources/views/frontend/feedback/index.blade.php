@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.feedback') }}
@endsection

@section('description')
    {{ trans('common.feedback') }}
@endsection

@section('author')
    Xenren
@endsection

@section('url')
    https://www.xenren.co/feedback
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
    <link href="/custom/css/frontend/feedback.css" rel="stylesheet" type="text/css"/>
    <link href="/custom/css/frontend/chat-room.css" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/lightbox2/dist/css/lightbox.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')

    <div class="row page-contant setFBPC">
        <div class="col-md-12 title">
            <h3>{{ trans('common.feedback') }}</h3>

            <h4>{{ trans('common.feedback_description')}}</h4>
        </div>

        <div class="col-md-12 setFBPAGECol12">
            <div class="portlet box job box-shadow">
                <div class="portlet-body main-tabs-div set-Main-Div">
                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs uppercase">
                                <li class="active">
                                    <a href="#tab_default_1" class="setFeedback" data-toggle="tab">{{ trans('common.feedback') }}</a>
                                </li>
                                <li>
                                    <a href="#tab_default_2" class="setHistory" data-toggle="tab">{{ trans('common.feedback_record') }}</a>
                                </li>
                                <li class="setLiFBPCBTN">
                                    <a href="{{ route('frontend.feedback.create')}}">
                                        <button class="btn btn-green-bg-white-text setFBPBTN">{{ trans('common.create') }}</button>
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_default_1">
                                    @if( count($feedbacks) == 0 )
                                    <div class="no-feeback-sect text-center">
                                        <img src="/images/no-feedback-icon.jpg">
                                        <div class="textfeedbackPage">
                                            {{ trans('common.no_feedback_given_yet') }}
                                        </div>
                                    </div>
                                    @else
                                    <div class="row form-group chat-section setFEEDBACKPAGECS">
                                        @if( $feedback != null )
                                        <div class="col-md-9 chat-room-div">
                                            <h5 class="title setTitle">{{ trans('common.chat_room') }} - ( {{ $feedback->title }} )</h5>

                                            <div class="row conversation" data-url="{{ route('frontend.chat.pullchat',$chatRoom->id)}}">
                                                <div class="col-md-12 clearfix">
                                                    <div class="portlet box gray-color">
                                                        <div class="portlet-body form">
                                                            <div class="scroller text-center setPBFBPAGESC" id="content-chat">
                                                                <h3>{{ trans('common.chat_content') }} ...</h3>
                                                                <div class="ajaxloader preloader_bg cls setAJAXLOADER">
                                                                    <svg version="1" xmlns="http://www.w3.org/2000/svg"
                                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                         width="150px" height="150px" viewBox="0 0 28 28">
                                                                        <g class="qp-circular-loader">
                                                                            <path class="qp-circular-loader-path" fill="none"
                                                                                  d="M 14,1.5 A 12.5,12.5 0 1 1 1.5,14" stroke-width="3"
                                                                                  stroke-linecap="round"></path>
                                                                        </g>
                                                                    </svg>
                                                                </div>
                                                                {{--<ul class="chats">--}}

                                                                    {{--<li class="in">--}}
                                                                        {{--<img class="avatar" alt=""--}}
                                                                             {{--src="/images/people-avatar.png"/>--}}
                                                                        {{--<div class="message">--}}
                                                                            {{--<a href="javascript:void(0);" class="name">--}}
                                                                                {{--Adam Johnson </a>--}}
                                                                            {{--<div class="div-datetime">--}}
                                                                                {{--<span class="datetime">07 Nov, 2016 09:33</span>--}}
                                                                            {{--</div>--}}
                                                                            {{--<span class="body">--}}
                                                                                {{--Sed elelfend nonummy dlam. Praesent maurls ante, elementum et. blbendum at, posuere sit amet, nlbh. DuIs tIncldunt lectus quls dul vlverra vestlbulum. Suspendlsse vulputate allquam dul. NuIla elementum dul ut augue. Allquam vehlcula ml at maurls. Maecenas placerat, nisi at consequa--}}
                                                                            {{--</span>--}}
                                                                        {{--</div>--}}
                                                                    {{--</li>--}}

                                                                    {{--<li class="in">--}}
                                                                        {{--<img class="avatar" alt=""--}}
                                                                             {{--src="/images/people-avatar.png"/>--}}
                                                                        {{--<div class="message">--}}
                                                                            {{--<a href="javascript:void(0);" class="name">--}}
                                                                                {{--John Henry </a>--}}
                                                                            {{--<div class="div-datetime">--}}
                                                                                {{--<span class="datetime">3 min ago</span>--}}
                                                                            {{--</div>--}}
                                                                            {{--<span class="body">--}}
										                    				{{--DuIs tIncldunt lectus quls dui vlverra vestlbulum. Suspendlsse vulputate allquam dul. NuIla elementum dul ut augue. --}}
										                    			{{--</span>--}}
                                                                        {{--</div>--}}
                                                                    {{--</li>--}}

                                                                    {{--<li class="in">--}}
                                                                        {{--<img class="avatar" alt=""--}}
                                                                             {{--src="/images/avatar_xenren.png"/>--}}
                                                                        {{--<div class="message">--}}
                                                                            {{--<a href="javascript:void(0);" class="name">--}}
                                                                                {{--You </a>--}}
                                                                            {{--<div class="div-datetime">--}}
                                                                                {{--<span class="datetime">3 min ago</span>--}}
                                                                            {{--</div>--}}
                                                                            {{--<span class="body">--}}
										                    				{{--DuIs tIncldunt lectus quls dui vlverra vestlbulum. Suspendlsse vulputate allquam dul. NuIla elementum dul ut augue. --}}
										                    			{{--</span>--}}
                                                                        {{--</div>--}}
                                                                    {{--</li>--}}

                                                                {{--</ul>--}}
                                                            </div>
                                                            <div class="chat-form">
                                                                <form action="{{ route('frontend.chat.postchat') }}" method="post" class="">
                                                                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                                                    <input type="hidden" name="chat_room_id" id="chat_room_id" value="{{ $chatRoom->id}}">
                                                                    <div class="row chat-form-main">
                                                                        <div class="col-md-1 chat-form-l">
                                                                            <div class="upload-picture upload-btn-custom setUPLDBTNC">
                                                                                <a href="{{ route('frontend.chat.chatimage', ['chat_room_id' => $chatRoom->id] ) }}"
                                                                                    data-toggle="modal"
                                                                                    data-target="#modalAddFileChat">
                                                                                    <i class="fa fa-paperclip"
                                                                                       aria-hidden="true"></i>
                                                                                </a>
                                                                            </div>
                                                                            <div class="upload-btn-custom">
                                                                                <a href="javascript:;">
                                                                                    <i class="fa fa-cog"
                                                                                       aria-hidden="true"></i>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-11 chat-form-r">
                                                                            <div class="input-cont">
                                                                                <input class="form-control message"
                                                                                       name="message" type="text"
                                                                                       placeholder="{{ trans('member.leave_message') }}..."/>
                                                                                <button type="submit"
                                                                                        class="btn btn-submit setFBPAGEBTN">
                                                                                    <i class="fa fa-send"
                                                                                       aria-hidden="true"></i>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 set-col-md-3PJ">
                                            <h5 class="title setTitle">{{ trans('common.chat_room_members') }} ({{count($chatFollowers)}})</h5>
                                            <div class="portlet light bordered chat-room-memb-div">
                                                <div class="portlet-body">
                                                    <div class="tab-content settabcontents">
                                                        <div class="tab-pane active" id="tab_actions_pending">
                                                            @foreach( $chatFollowers as $key => $chatFollower )
                                                            <div class="mt-actions">
                                                                <div class="mt-action" style="background-color: white; border-bottom: 1px solid white">
                                                                    @if( $chatFollower->staff_id != null )
                                                                    <div class="mt-action-img">
                                                                        <img alt="" class="img-circle setIMAGEAVAFBPFF"
                                                                             src="{{ $chatFollower->staff->getAvatar() }}"
                                                                             >
                                                                    </div>
                                                                    <div class="mt-action-body">
                                                                        <div class="mt-action-row">
                                                                            <div class="mt-action-info ">
                                                                                <div class="mt-action-details setFBACKPAGEMTA"
                                                                                >

                                                                                <span class="mt-action-author">
                                                                                    {{ $chatFollower->staff->name }}
                                                                                    <span class="fa fa-check-circle"></span>
                                                                                </span>
                                                                                    <p class="mt-action-desc">{{ trans('common.support_executive') }}</p>
                                                                                </div>

                                                                            <div>
                                                                            <br/></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    @elseif( $chatFollower->user_id != null )
                                                                    <div class="mt-action-img">
                                                                        <img alt="" class="img-circle setIMAGEAVAFBPFF"
                                                                             src="{{ $chatFollower->user->getAvatar() }}"
                                                                        >
                                                                    </div>

                                                                    <div class="mt-action-body">
                                                                        <div class="mt-action-row">
                                                                            <div class="mt-action-info ">
                                                                                <div class="mt-action-details setFBACKPAGEMTA"
                                                                                     >
                                                                                <span class="mt-action-author">
                                                                                    {{ trans('common.you') }}
                                                                                    {{--@if( $chatFollower->user->getLatestIdentityStatus() == \App\Models\UserIdentity::STATUS_APPROVED )--}}
                                                                                    <span class="fa fa-check-circle"></span>
                                                                                    {{--@endif--}}
                                                                                </span>
                                                                                    <p class="mt-action-desc">{{ trans('common.creator') }}</p>
                                                                                </div>
                                                                                <div><br/></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            @endforeach

                                                            {{--<div class="mt-actions">--}}
                                                                {{--<div class="mt-action">--}}
                                                                    {{--<div class="mt-action-img">--}}
                                                                        {{--<img alt="" class="img-circle"--}}
                                                                             {{--src="/images/people-avatar.png"--}}
                                                                             {{--style="width: 55px; height: 55px;">--}}
                                                                    {{--</div>--}}
                                                                    {{--<div class="mt-action-body">--}}
                                                                        {{--<div class="mt-action-row">--}}
                                                                            {{--<div class="mt-action-info ">--}}
                                                                                {{--<div class="mt-action-details"--}}
                                                                                     {{--style="display: unset;">--}}
                                                                                {{--<span class="mt-action-author">--}}
                                                                                    {{--Adam Johnson--}}
                                                                                    {{--<span class="fa fa-check-circle"></span>--}}
                                                                                {{--</span>--}}
                                                                                    {{--<p class="mt-action-desc">Support--}}
                                                                                        {{--Excecutive</p>--}}
                                                                                {{--</div>--}}
                                                                                {{--<div><br/></div>--}}
                                                                            {{--</div>--}}
                                                                        {{--</div>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}

                                                            {{--<div class="mt-actions">--}}
                                                                {{--<div class="mt-action">--}}
                                                                    {{--<div class="mt-action-img">--}}
                                                                        {{--<img alt="" class="img-circle"--}}
                                                                             {{--src="/images/avatar_xenren.png"--}}
                                                                             {{--style="width: 55px; height: 55px;">--}}
                                                                    {{--</div>--}}

                                                                    {{--<div class="mt-action-body">--}}
                                                                        {{--<div class="mt-action-row">--}}
                                                                            {{--<div class="mt-action-info ">--}}
                                                                                {{--<div class="mt-action-details"--}}
                                                                                     {{--style="display: unset;">--}}
                                                                                {{--<span class="mt-action-author">--}}
                                                                                    {{--You--}}
                                                                                    {{--<span class="fa fa-check-circle"></span>--}}
                                                                                {{--</span>--}}
                                                                                    {{--<p class="mt-action-desc">Guest</p>--}}
                                                                                {{--</div>--}}
                                                                                {{--<div><br/></div>--}}
                                                                            {{--</div>--}}
                                                                        {{--</div>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                    @endif
                                </div>
                                <div class="tab-pane" id="tab_default_2">
                                    @if( count($feedbacks) == 0 )
                                    <div class="no-feeback-sect text-center">
                                        <img src="/images/no-feedback-icon.jpg">
                                        <div class="textfeedbackPage">
                                            {{ trans('common.no_feedback_given_yet') }}
                                        </div>
                                    </div>
                                    @else
                                    <div class="portlet box gray-color">
                                        <div class="portlet-body history setPBHTABLEFBP">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th width="100px">#</th>
                                                        <th class="uppercase">{{ trans('common.issue') }}</th>
                                                        <th class="uppercase">{{ trans('common.date') }}</th>
                                                        <th class="uppercase">{{ trans('common.status') }}</th>
                                                        <th class="uppercase">{{ trans('common.action') }}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach( $feedbacks as $feedback )
                                                    <tr id="feedback-{{ $feedback->id }}">
                                                        <td>{{ $feedback->id }}</td>
                                                        <td>{{ $feedback->title }}</td>
                                                        <td>{{ $feedback->created_at }}</td>
                                                        <td>
                                                            <span class="wait-admin-reply-{{ $feedback->id }}">
                                                                {{ $feedback->translateStatus() }}
                                                            </span>
                                                        </td>
                                                        <td class="setPBTABLETD5">
                                                            <a href="{{ route('frontend.feedback',[\Session::get('lang')]) }}?feedback_id={{ $feedback->id }}">
                                                                <button class="btn btn-make-Offer" data-toggle="tooltip" title="{{ trans('common.chat_room') }}">
                                                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                                                </button>
                                                            </a>

                                                            <button class="btn f-s-14 default btn-delete" data-url="{{ route('frontend.feedback.delete',[\Session::get('lang')]) }}" data-id="{{ $feedback->id }}" data-toggle="tooltip" title="{{ trans('common.delete') }}">
                                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                                            </button>

                                                            <button class="btn f-s-14 blue btn-wait-admin-reply" data-url="{{ route('frontend.feedback.waitadminreply',[\Session::get('lang')]) }}" data-id="{{ $feedback->id }}" data-toggle="tooltip" title="{{ trans('common.wait_admin_reply') }}" >
                                                                <i class="fa fa-mail-reply" aria-hidden="true"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    {{--<tr>--}}
                                                        {{--<td>03</td>--}}
                                                        {{--<td>Lorems issue occur while attaching files in post job page.</td>--}}
                                                        {{--<td>18 Jun 2017</td>--}}
                                                        {{--<td>Pending</td>--}}
                                                        {{--<td>--}}
                                                            {{--<button class="btn btn-make-Offer"><i class="fa fa-eye"--}}
                                                                                                  {{--aria-hidden="true"></i>--}}
                                                            {{--</button>--}}
                                                            {{--<button class="btn btn-make-Offer default"><i--}}
                                                                        {{--class="fa fa-trash" aria-hidden="true"></i></button>--}}
                                                        {{--</td>--}}
                                                    {{--</tr>--}}
                                                    {{--<tr>--}}
                                                        {{--<td>04</td>--}}
                                                        {{--<td>Lorems issue occur while attaching files in post job page.</td>--}}
                                                        {{--<td>18 Jun 2017</td>--}}
                                                        {{--<td>Running</td>--}}
                                                        {{--<td>--}}
                                                            {{--<button class="btn btn-make-Offer"><i class="fa fa-eye"--}}
                                                                                                  {{--aria-hidden="true"></i>--}}
                                                            {{--</button>--}}
                                                            {{--<button class="btn btn-make-Offer default"><i--}}
                                                                        {{--class="fa fa-trash" aria-hidden="true"></i></button>--}}
                                                        {{--</td>--}}
                                                    {{--</tr>--}}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <div id="modalAddFileChat" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{asset('assets/global/plugins/lightbox2/dist/js/lightbox.min.js')}}" type="text/javascript"></script>
    <script>
        var lang = {
            unable_to_request: "{{ trans('common.unable_to_request') }}",
            unknown_error: "{{ trans('common.unknown_error') }}",
            wait_admin_reply: "{{ trans('common.wait_admin_reply') }}"
        };
    </script>
    <script src="/custom/js/frontend/feedbackChat.js" type="text/javascript"></script>
    <script>
        +function () {
            $(document).ready(function () {
                $(".btn-delete").on('click', function(){
                    var url = $(this).data('url');
                    var id = $(this).data('id');
                    $.ajax({
                        url: url,
                        dataType: 'json',
                        data: {id: id},
                        method: 'POST',
                        error: function () {
                            alert(lang.unable_to_request);
                        },
                        success: function (result) {
                            if (result.status == 'OK') {
                                alertSuccess(result.message);
                                $('#feedback-' + id).hide();
                            } else if (result.status == 'Error') {
                                alertError(result.message);
                            }
                        }
                    });
                });

                var _self = this;

                //handlers
                document.addEventListener('paste', function (e) {
                    _self.paste_auto(e);
                }, false);

                //on paste
                this.paste_auto = function (e) {
                    if (e.clipboardData) {
                        var items = e.clipboardData.items;
                        if (!items) return;

                        //access data directly
                        for (var i = 0; i < items.length; i++) {
                            if (items[i].type.indexOf("image") !== -1) {
                                //image
                                var blob = items[i].getAsFile();

                                var URLObj = window.URL || window.webkitURL;
                                var source = URLObj.createObjectURL(blob);

                                var f = document.createElement("form");
                                f.setAttribute('id',"upload-image-form");
                                f.setAttribute('method',"post");
                                f.setAttribute('enctype',"multipart/form-data");

                                var fd = new FormData();
                                var attrs = $("#upload-image-form").serialize().split("&");
                                for (i = 0; i < attrs.length - 1; i++) {
                                    var temp = attrs[i].split('=');
                                    fd.append(temp[0], temp[1]);
                                }
                                fd.append("chat_room_id", "{{ $chatRoom != null? $chatRoom->id: 0 }}");
                                fd.append("message", "PrintScreen");
                                fd.append('file', blob);
                                $.ajax({
                                    type: 'POST',
                                    url: '{{ route('frontend.chat.chatimage.post')}}',
                                    data: fd,
                                    processData: false,
                                    contentType: false,
                                    error: function () {
                                        alert(lang.unable_to_request);
                                    },
                                    success: function (result) {
                                        if (result.status == 'OK') {
//                                            alertSuccess(result.message);
//                                            setTimeout(function () {
//                                                location.reload();
//                                            }, 3000);
                                            $('#content-chat').append(result.contents);

                                            //always scroll to bottom
                                            var scrollHeight =  $('#content-chat').prop('scrollHeight');
                                            $("#content-chat").slimScroll({ scrollTo: scrollHeight });
                                        } else if (result.status == 'Error') {
                                            alertError(result.message);
                                        }
                                    }
                                });
                            }
                        }
                        e.preventDefault();
                    }
                };
            });
        }(jQuery);
    </script>
@endsection