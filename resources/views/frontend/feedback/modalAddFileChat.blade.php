@extends('frontend.includes.ajaxmodal')

@section('title')
    {{ trans('member.add_file') }}
@endsection

@section('script')
    <script>
        +function () {
            $(document).ready(function () {
                $('#add-file-chat').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btn-submit',
                    alertContainer : '#model-alert-container',
                    afterSuccessFunction: function (response, $el, $this) {
                        if( response.status == 'OK'){
                            $('#modalAddFileChat').modal('toggle');
//                            alertSuccess(response.message);
//
//                            setTimeout(function () {
//                                location.reload();
//                            }, 3000);

                            $('#content-chat ul').append(response.contents);

                            //always scroll to bottom
                            var scrollHeight =  $('#content-chat').prop('scrollHeight');
                            $("#content-chat").slimScroll({ scrollTo: scrollHeight });
                        }else{
                            App.alert({
                                type: 'danger',
                                icon: 'warning',
                                message: response.message,
                                container: '#model-alert-container',
                                reset: true,
                                focus: true
                            });
                        }

                        App.unblockUI('#add-file-chat');
                    }
                });
            });
        }(jQuery);
    </script>
@endsection

@section('footer')
    <div style="margin-top:0 px;">
        <hr/>
    </div>
    <div>
        <button type="submit" class="btn btn-green-bg-white-text" id="btn-submit">
            {{ trans('member.confirm_add') }}
        </button>
        <button type="button" class="btn btn-white-bg-green-text" data-dismiss="modal">
            {{ trans('member.cancel') }}
        </button>
    </div>
@endsection

@section('content')
    {!! Form::open(['route' => 'frontend.chat.chatimage.post', 'id' => 'add-file-chat', 'method' => 'post', 'class' => 'form-horizontal', 'files' => true]) !!}
        <input type="hidden" name="chat_room_id" value="{{ $chat_room_id }}">
        <div class="modal-body">
            <div class="form-body">
                <div class="modal-alert-container"></div>
                <div class="form-group">
                    <label class="col-md-3 control-label">{{ trans('member.filename') }}</label>
                    <div class="col-md-9">
                        {!! Form::text('message', '', ['class' => 'form-control', 'placeholder' => 'Message']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">{{ trans('member.file') }}</label>
                    <div class="col-md-9">
                        {!! Form::file('file', ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@endsection


