@extends('frontend.layouts.default')

@section('title')
    <?php
        $link = asset('remote-jobs-online?project_type='.$projectType);
        if($link == asset('remote-jobs-online?project_type=1'))
        {
            echo trans('common.jobs_title');
        }
        else
        {
            echo trans('common.jobs_title_offical');
        }
    ?>
@endsection

@section('keywords')
    {{ trans('common.jobs_keyword') }}
@endsection

@section('description')
    <?php
    $link = asset('remote-jobs-online?project_type='.$projectType);
    if($link == asset('remote-jobs-online?project_type=1'))
    {
        echo trans('common.jobs_desc');
    }
    else
    {
        echo trans('common.jobs_desc_offical');
    }
    ?>
@endsection
@section('ogdescription')
    <?php
    $link = asset('remote-jobs-online?project_type='.$projectType);
    if($link == asset('remote-jobs-online?project_type=1'))
    {
        echo trans('common.jobs_desc');
    }
    else
    {
        echo trans('common.jobs_desc_offical');
    }
    ?>
@endsection

@section('author')
    Xenren
@endsection

@section('url')
    https://www.xenren.co/jobs
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
{{--    <link href="/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="/custom/css/frontend/jobs.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="/custom/css/frontend/modalMakeOffer.css" rel="stylesheet" type="text/css"/>--}}
    <link href="{{asset('css/minifiedJobs.css')}}" rel="stylesheet">
    <!-- <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script> -->
    <style>

        .user-imgs svg{
            position: relative !important;
            top: -15px !important;
            right: -40px !important;
        }
        @media (max-width:991px) and (min-width:412px)
        {
            .setJPHeaderContent{
                text-align:center;
            }
            .setJPBTNJD{
                width:100% !important;
            }
            .setJPCol10{
                margin-top:2%;
                text-align:center;
            }
            .setJPSSCol4 img{
                float:none !important;
            }
            .setJPSkillDiv {
                margin-top: 2%;
                display: grid;
            }
            .setJBtn1{
                width:100% !important;
            }
            .setJBtn2 button{
                margin-top:2%;
                width: 100% !important;
            }
            .setJBtn2beforeLogin{
                margin-top:2%;
                width: 100% !important;
            }
            .setJPSKILLDIVCol8{

            }
        }
        @media (max-width:411px) and (min-width:320px)
        {
            .setJBtn1{
                margin-left:0px !important;
                width:100% !important;
            }
            .setJBtn2 button{
                width: 100% !important;
            }
            .setJBtn2beforeLogin{
                margin-top:2%;
                width: 100% !important;
            }
        }
    </style>
    {{-- For new design --}}
    <style>
        .user-imgs svg{
            position: relative !important;
            top: -15px !important;
            right: -40px !important;
        }
        .award-dilog-contant{
            padding:15px;
            border-radius: 2px !important;
            width:101% !important;
        }
        .award-dilog-model-header{
            border:none !important;
        }
        .award-dilog-model-main{
            width:90% !important;
        }
        .award-dilog-model-body{
            margin:0px 15px;
        }

        .border-left{
            border-left:1px solid #c2c2c2;
        }
        .border-right{
            border-right:1px solid #c2c2c2;
        }
        .border-bottom{
            border-bottom:1px solid #c2c2c2;
        }
        .border-top{
            border-top:1px solid #c2c2c2;
        }
        .model-img-part{
            min-height:90px !important;
        }
        .award-dilog-box{
            min-height:250px;
            display: inline-block;
            display: table-cell;
        }
        .award-dilog-box img{
            margin-top:25px;
        }
        .award-dilog-box h2{
            display: flex;
            align-items: center;
            justify-content: center;
            min-height:60px;
            color:#5DB331;
            font-size:26px;
            margin:10px 0px 0px 0px;
        }
        .award-dilog-box p{
            margin-top:5px;
            font-size:17px;
            margin-bottom: -80px;
            color:000;
            font-weight:500;
        }

        @media (max-width:1100px)
        {
            .award-dilog-model-main{
                width:95% !important;
            }
            .setADDMD{
                padding:70px;
            }
            .setADDC{
                width:100% !important;
            }
            .setMIPPART{
                min-height:6rem !important;
            }
            .award-dilog-box h2{
                min-height:30px !important;
            }
        }

        @media (max-width:991px) and (min-width:878px)
        {
            .award-dilog-model-main{
                width:95% !important;
            }
            .setADDC {
                width: 100% !important;
            }
            .setAWARDB1{
                width:31% !important;
            }
            .setAWARDB1 img{
                width:40px;
            }
            .setAWARDB1 p{
                margin-bottom:0px !important;
                font-size: 15px;
            }
            .setAWARDB1 h2 {
                font-size:18px !important;
            }
        }

        @media (max-width:877px) and (min-width:320px)
        {
            .award-dilog-model-main{
                width:95% !important;
            }
            .setADDC {
                width: 100% !important;
            }
            .setAWARDB1{
                width:31% !important;
                display: contents;
            }
            .setAWARDB1 h2 {
                font-size:13px !important;
            }
            .setAWARDB1 img{
                width:33px;
            }
            .setAWARDB1 p{
                margin-bottom:0px !important;
                font-size: 13px;
            }
        }

        .title-section img{
            position: absolute;
            right:10px;
            top:-10px;
        }

        .per-text{
            position: absolute;
            right:20px;
            top: -3px;
            font-size: 28px;
            font-weight: bold;
            font-family: "Open Sans",sans-serif !important;
        }
        @media (min-width:320px) and (max-width:560px) {
            .per-text {
                position: absolute;
                right: -4px;
                top: -6px;
                font-size: 20px;
                font-weight: bold;
                font-family: "Open Sans",sans-serif !important;
            }
            .title-section img{
                position: absolute;
                right:-10px;
                top:-10px;
                width:50px;
            }
            .project-name{
                font-size: 14px !important;
                /*font-family: "Gotham Medium";*/
                cursor: pointer;
                color: #59AF28;
                text-decoration: none;
                font-weight:bolder;
            }

        }
        .per-text sub{
            font-weight:600;
            font-size:13px;
        }
        .fa-thumbs-down,.fa-heart{
            color:#808080;
            font-size: 25px;
            margin:0px 7px 0px 7px;
        }
        .award-yellow-per{
            color:#F7AD43;
        }
        .award-Red-per{
            color:#FE0101;
        }
        .active {
         background-color: white !important;
        }
        .setNAVNAVTABS{
            background-color:#FAFAFA;
        }
        .setJPMSHELPER{
            display:none;
        }
        .setJPSEARCHBARBOX{
            width: 492px;
        }
        .setJPDIVDIVSEARCH{
            display:none;
        }
        .setJPSPAN{
            cursor: pointer;
        }
        .setCheckCircle{
            color:#58AF2A !important
        }
        .fav-job .svg-inline--fa.fa-w-16 {
            width: 1em;
            color:#808080;
        }
        .set-job-div-cursor{
            cursor: pointer;
        }
    </style>
@endsection

@section('customStyle')
@endsection

@section('content')
    <div class="page-content">
        @include('frontend.includes.100-coworkers')
        <div class="row">
            <div class="col-md-12">
                @if($projectType == \App\Models\Project::PROJECT_TYPE_COMMON)
                    <h1 style="margin:0px;" class="find-experts-search-title text-uppercase">{{trans('common.listing')}}</h1>
                @else
                    <h1 style="margin:0px;" class="find-experts-search-title text-uppercase">{{trans('common.offical_listing')}}</h1>
                @endif
            </div>
        </div>

        <section class="main-well-custom">
            <div class="row">
                <div class="col-md-12">
                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs setNAVNAVTABS">
                                <li class="{{ $projectType == \App\Models\Project::PROJECT_TYPE_OFFICIAL? 'active': '' }}">
                                    <a href="{{ route('frontend.jobs.official', \Session::get('lang') ) }}?project_type={{ \App\Models\Project::PROJECT_TYPE_OFFICIAL }}" class="f-s-16 set-projects-color">
                                        {{ trans('common.official_project') }}
                                    </a>
                                </li>
                                <li class="{{ $projectType == \App\Models\Project::PROJECT_TYPE_COMMON? 'active': '' }}">
                                    <a href="{{ route('frontend.jobs.index') }}?project_type={{ \App\Models\Project::PROJECT_TYPE_COMMON }}" class="f-s-16 set-projects-color">
                                        {{ trans('common.common_project') }}
                                    </a>
                                </li>
                                <a href="javascript:;" class="filter set-class-filters">
                                    {{ trans('common.filter') }}
                                    <i class="fa fa-caret-down set-class-filters" aria-hidden="true"></i>
                                </a>
                            </ul>
                            <div class="tab-content">
                                <div id="tab_default_1" class="tab-pane active filter-option">
                                    <div class="row">
                                        <div class="col-md-12 tab-content-searchBox">
                                            <div id="msSkills">
                                                {{--load to slow, temporary design--}}
                                                <div class="ms-ctn form-control  ms-no-trigger ms-ctn-focus" id="msSearchBar">
                                                    <span class="ms-helper setJPMSHELPER"></span>
                                                    <div class="ms-sel-ctn">
                                                        <input class="search-box-home setJPSEARCHBARBOX" placeholder="{{ trans('common.search') }}" type="text">
                                                        <div class="setJPDIVDIVSEARCH"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <i id="btn-done" class="fa fa-search btn-search" data-base-url="{{ $advance_url }}" data-redirect-url="{{ $advance_url }}"></i>
                                        </div>
@php

$order_url = str_replace(URL::to('/'),URL::to('/').'/'.\Session::get('lang'),$order_url);

$type_url = str_replace(URL::to('/'),URL::to('/').'/'.\Session::get('lang'),$type_url);
@endphp
                                        <div class="col-md-3 setThisCol3JDP">
                                            <h4>{{ trans('member.show_option') }}</h4>
                                            <ul>
                                                <li class="{{ $filter['type'] == 2 ? 'active' : '' }}">
                                                    <a href="{{ $type_url }}type=2" >
                                                        {{ trans('common.recommend_jobs') }}<span title="Xenren system suggest jobs according to your information." class="setJPSPAN">  <i class="fas fa-question-circle"></i> </span>
                                                    </a>
                                                </li>
                                                <li class="{{ $filter['type'] == null || $filter['type'] == 1 ? 'active' : '' }}">
                                                    <a href="{{ $type_url }}type=1">
                                                        {{ trans('common.all_jobs') }}
                                                    </a>
                                                </li>

                                                @if($filter['project_type'] == \App\Models\Project::PROJECT_TYPE_COMMON)
                                                    <li class="{{ $filter['type'] == 3 ? 'active' : '' }}">
                                                        <a href="{{ $type_url }}type=3">
                                                            {{ trans('common.fixed_price_jobs') }}
                                                        </a>
                                                    </li>
                                                    <li class="{{ $filter['type'] == 4 ? 'active' : '' }}">
                                                        <a href="{{ $type_url }}type=4">
                                                            {{ trans('common.hourly_jobs') }}
                                                        </a>
                                                    </li>
                                                @endif
                                            </ul>
                                        </div>
                                        <div class="col-md-3 setThisCol3JDP">
                                            <h4>{{ trans('member.ordering_option') }}</h4>
                                            <ul>
                                                <li class="{{ $filter['order'] == null || $filter['order'] == 1 ? 'active' : '' }}">
                                                    <a href="{{ $order_url }}order=1">
                                                        {{ trans('common.latest_jobs') }}
                                                    </a>
                                                </li>
                                                <li class="{{ $filter['order'] == 2 ? 'active' : '' }}">
                                                    <a href="{{ $order_url }}order=2">
                                                        {{ trans('common.oldest_jobs') }}
                                                    </a>
                                                </li>

                                                @if($filter['project_type'] == \App\Models\Project::PROJECT_TYPE_OFFICIAL)
                                                    <li class="{{ $filter['order'] == 3 ? 'active' : '' }}">
                                                        <a href="{{ $order_url }}order=3">
                                                            {{ trans('common.project_scale_ascending') }}
                                                        </a>
                                                    </li>
                                                    <li class="{{ $filter['order'] == 4 ? 'active' : '' }}">
                                                        <a href="{{ $order_url }}order=4">
                                                            {{ trans('common.project_scale_descending') }}
                                                        </a>
                                                    </li>
                                                @endif

                                                @if($filter['project_type'] == \App\Models\Project::PROJECT_TYPE_COMMON)
                                                    <li class="{{ $filter['order'] == 5 ? 'active' : '' }}">
                                                        <a href="{{ $order_url }}order=5">
                                                            {{ trans('common.quoted_price_ascending') }}
                                                        </a>
                                                    </li>
                                                    <li class="{{ $filter['order'] == 6 ? 'active' : '' }}">
                                                        <a href="{{ $order_url }}order=6">
                                                            {{ trans('common.quoted_price_descending') }}
                                                        </a>
                                                    </li>
                                                @endif
                                            </ul>
                                        </div>

                                    <!--
	    								<div class="col-md-6">
	    								    <div class="row">
                                                <div class="col-md-6">
                                                    <h4>{{ trans('member.advance_search_option') }}</h4>
                                                    <div class="search-selectbox">
                                                        <select class="form-control" name="" id="slcKeyword">
                                                            <option value="0">{{ trans('member.filter_keyword') }}</option>
                                                            @foreach( $keywords as $key => $keyword )
                                        <option value="{{ $key }}">{{ $keyword }}</option>
                                                            @endforeach
                                            </select>
                                        </div>
                                        <div class="search-selectbox">
                                            <select class="form-control" name="" id="slcValue">
                                                <option value="0">{{ trans('member.filter_value') }}</option>
                                                        </select>
                                                    </div>
                                                    <div class="add-filter-con">
                                                        <a href="javascript:;">+ {{ trans('member.add_filter_condition') }}</a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="">
                                                        <a href="{{ $advance_url }}" class="done" id="btn-done2" data-base-url="{{ $advance_url }}">
                                                            {{ trans('common.done') }}
                                            </a>
                                        </div>
                                        <div>
                                            <div class="p-t-40">
                                                <div class="div-country">
                                                    <span class="value"></span>
                                                    <button id="btn-close-country" type="button" class="close pull-left" style="display:none;">×</button>
                                                </div>

                                                <input type="hidden" name="hidCountry" value="">

                                                <div class="div-language">
                                                    <span class="value"></span>
                                                    <button id="btn-close-language" type="button" class="close pull-left" style="display:none;">×</button>
                                                </div>
                                                <input type="hidden" name="hidLanguage" value="">

                                                <div class="div-hourly-range">
                                                    <span class="value"></span>
                                                    <button id="btn-close-hourly-range" type="button" class="close pull-left" style="display:none;">×</button>
                                                </div>
                                                <input type="hidden" name="hidHourlyRange" value="">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
-->


                                        <div class="col-md-6 setThisCol3JDP">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h4>{{ trans('member.advance_search_option') }}</h4>
                                                    {{--<div class="search-selectbox">--}}
                                                    {{--<select class="form-control" name="" id="slcLanguage">--}}
                                                    {{--<option value="0">{{ trans('common.language_2') }}</option>--}}
                                                    {{--@foreach( $languages as $key => $language )--}}
                                                    {{--<option value="{{ $key }}" {{ $key == $filter['language']? 'selected': '' }}>{{ $language }}</option>--}}
                                                    {{--@endforeach--}}
                                                    {{--</select>--}}
                                                    {{--</div>--}}
                                                    <div class="customize dropdown">
                                                        <a role="button" data-toggle="dropdown" class="btn custom-select-btn" data-target="#">
                                                            <label id="language_name" name="language_name" class="pull-left">
                                                                {{ isset($filter['language_name'])? $filter['language_name']: trans('common.language_2') }}
                                                            </label>
                                                        </a>
                                                        <ul class="language dropdown-menu">
                                                            <li>
                                                                <a class="item" data-id="0" data-name="{{ trans('common.language_2' ) }}">
                                                                    {{ trans('common.language_2' ) }}
                                                                </a>
                                                            </li>
                                                            @foreach ($languages as $key => $language )
                                                                <li>
                                                                    <a class="item" data-id="{{ $key }}" data-name="{{ $language }}">
                                                                        {{ $language }}
                                                                    </a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                    <input type="hidden" id="slcLanguage" name="slcLanguage" value="{{ isset($filter['language'])? $filter['language']: '0' }}" />

                                                    {{--<div class="search-selectbox">--}}
                                                    {{--<select class="form-control" name="" id="slcCountry">--}}
                                                    {{--<option value="0">{{ trans('common.country') }}</option>--}}
                                                    {{--@foreach( $countries as $key => $country )--}}
                                                    {{--<option value="{{ $key }}" {{ $key == $filter['country']? 'selected': '' }}>{{ $country }}</option>--}}
                                                    {{--@endforeach--}}
                                                    {{--</select>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="add-filter-con">--}}
                                                    {{--<a href="javascript:;">+ {{ trans('member.add_filter_condition') }}</a>--}}
                                                    {{--</div>--}}
                                                    <div class="customize dropdown">
                                                        <a role="button" data-toggle="dropdown" class="btn custom-select-btn" data-target="#">
                                                            <label id="country_name" name="country_name" class="pull-left">
                                                                {{ isset($filter['country_name'])? $filter['country_name']: trans('common.country') }}
                                                            </label>
                                                        </a>
                                                        <ul class="country dropdown-menu">
                                                            <li>
                                                                <a class="item" data-id="0" data-name="{{ trans('common.country' ) }}">
                                                                    {{ trans('common.country' ) }}
                                                                </a>
                                                            </li>
                                                            @foreach ($countries as $key => $country )
                                                                <li>
                                                                    <a class="item" data-id="{{ $key }}" data-name="{{ $country->name }}">
                                                                        {{ $country->name }}
                                                                    </a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                    <input type="hidden" id="slcCountry" name="slcCountry" value="{{ isset($filter['country'])? $filter['country']: '0' }}" />
                                                </div>
                                                <div class="col-md-6">
                                                    <h4>&nbsp;
                                                        <div class="">
                                                            {{--<a href="{{ $advance_url }}" class="done" id="btn-done2" data-base-url="{{ $advance_url }}">--}}
                                                            {{--{{ trans('common.done') }}--}}
                                                            {{--</a>--}}
                                                        </div>
                                                        <input type="hidden" name="hidCountry" value="">
                                                        <input type="hidden" name="hidLanguage" value="">
                                                        <input type="hidden" name="hidHourlyRange" value="">
                                                    </h4>
                                                    {{--<div class="search-selectbox">--}}
                                                    {{--<select class="form-control" name="" id="slcSkills">--}}
                                                    {{--<option value="0">{{ trans('common.skills') }}</option>--}}
                                                    {{--@foreach( $keywords as $key => $keyword )--}}
                                                    {{--<option value="{{ $key }}">{{ $keyword }}</option>--}}
                                                    {{--@endforeach--}}
                                                    {{--</select>--}}
                                                    {{--</div>--}}
                                                    @if($filter['project_type'] == \App\Models\Project::PROJECT_TYPE_COMMON)
                                                        {{--<div class="search-selectbox">--}}
                                                        {{--<select class="form-control" name="" id="slcBudget">--}}
                                                        {{--<option value="0">{{ trans('common.budget') }}</option>--}}
                                                        {{--@foreach( $hourlyRanges as $key => $hourlyRange )--}}
                                                        {{--<option value="{{ $key }}" {{ $key == $filter['hourly_range']? 'selected': '' }}>{{ $hourlyRange }}</option>--}}
                                                        {{--@endforeach--}}
                                                        {{--</select>--}}
                                                        {{--</div>--}}
                                                        <div class="customize dropdown">
                                                            <a role="button" data-toggle="dropdown" class="btn custom-select-btn" data-target="#">
                                                                <label id="hourly_range_name" name="hourly_range_name" class="pull-left">
                                                                    {{ isset($filter['hourly_range_name'])? $filter['hourly_range_name']: trans('common.hourly_range') }}
                                                                </label>
                                                            </a>
                                                            <ul class="hourly-range dropdown-menu">
                                                                <li>
                                                                    <a class="item" data-id="0" data-name="{{ trans('common.hourly_range' ) }}">
                                                                        {{ trans('common.hourly_range' ) }}
                                                                    </a>
                                                                </li>
                                                                @foreach ($hourlyRanges as $key => $hourlyRange )
                                                                    <li>
                                                                        <a class="item" data-id="{{ $key }}" data-name="{{ $hourlyRange }}">
                                                                            {{ $hourlyRange }}
                                                                        </a>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                        <input type="hidden" id="slcBudget" name="slcBudget" value="{{ isset($filter['hourly_range'])? $filter['hourly_range']: '0' }}" />
                                                    @endif

                                                    {{--<div id="msSkills">--}}
                                                    {{--</div>--}}
                                                    <input type="hidden" id="skill_id_list" name="skill_id_list" value="{{ isset($filter['skill_id_list'])? $filter['skill_id_list']: '' }}">
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div id="tab_default_2" class="tab-pane"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="row">
            <div class="col-md-12 found-count-top">
                <span class="number">{{ $totalCount }}</span> <span class="text"> {{ trans('common.jobs_found') }}</span>

                {{--                @if ( $officialProjects->lastPage() > 1)--}}
                <div class="find-experts-pagination pull-right">
                    @if( isset($officialProjects) )
                        {{ $officialProjects->appends($filter)->links() }}
                    @endif

                    @if( isset($commonProjects) )
                        {{ $commonProjects->appends($filter)->links() }}
                    @endif
                </div>
                {{--@endif--}}
            </div>
        </div>

        @if( isset($officialProjects) )
            @foreach($officialProjects as $officialProject)
                @include('frontend.includes.ucOfficialProjectItem')
            @endforeach
        @endif

        @if( isset($commonProjects) )
            @foreach($commonProjects as $commonProject)
                @if(isset($adminSetting) && $adminSetting->value == \App\Models\Project::PAY_TYPE_HOURLY_PAY)
                    @if($commonProject->pay_type != \App\Models\Project::PAY_TYPE_HOURLY_PAY)
                        @include('frontend.includes.ucCommonProjectItem')
                    @endif
                @else
                    @include('frontend.includes.ucCommonProjectItem')
                @endif
            @endforeach
        @endif


        <div class="row">
            <div class="col-md-12 found-count-top">
                <span class="number">{{ $totalCount }}</span> <span class="text"> {{ trans('common.jobs_found') }}</span>

                {{--                @if ( $officialProjects->lastPage() > 1)--}}
                <div class="find-experts-pagination pull-right">
                    @if( isset($officialProjects) )
                        {{ $officialProjects->appends($filter)->links() }}
                    @endif

                    @if( isset($commonProjects) )
                        {{ $commonProjects->appends($filter)->links() }}
                    @endif
                </div>
                {{--@endif--}}
            </div>
        </div>
    </div>
@endsection

@section('modal')

    <!-- award-dilog start-->
    <div id="award-dilog-model" class="modal fade" role="dialog">
        <div class="modal-dialog award-dilog-model-main setADDMD">
            <div class="modal-content award-dilog-contant setADDC">
                <div class="modal-header award-dilog-model-header text-right">
                    <a href="#" data-dismiss="modal"> <img src="{{ asset('/images/Delete-100.jpg') }}"> </a>
                </div>
                <div class="modal-body award-dilog-model-body text-center">
                    <div class="row">
                        <div class="col-md-4 award-dilog-box setAWARDB1 border-top border-left border-bottom">
                            <div class="row skill-fit">
                                <div class="col-md-12 model-img-part setMIPPART">
                                    <img src="{{ asset('/images/icons/skill-fit.png') }}">
                                </div>
                                <div class="col-md-12">
                                    <h2 class="skill-fit">Skill Fit</h2>
                                </div>
                                <div class="col-md-12">
                                    <p>Make sure your skills fit the current post and try to passed some skill verification test.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 award-dilog-box setAWARDB1 border-left border-top border-right">
                            <div class="row location-fit">
                                <div class="col-md-12 model-img-part setMIPPART">
                                    <img src="{{ asset('/images/icons/location-fit.png') }}">
                                </div>
                                <div class="col-md-12">
                                    <h2 class="location-fit">Location Fit</h2>
                                </div>
                                <div class="col-md-12">
                                    <p>If your location match with employer, your success rate will increase.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 award-dilog-box setAWARDB1 border-top border-right">
                            <div class="row budget-fit">
                                <div class="col-md-12 model-img-part setMIPPART">
                                    <img src="{{ asset('/images/icons/budget-fit.png') }}">
                                </div>
                                <div class="col-md-12">
                                    <h2 class="budget-fit">Budget fit</h2>
                                </div>
                                <div class="col-md-12">
                                    <p>Match your salary</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 award-dilog-box setAWARDB1 border-left border-right">
                            <div class="row veryfi-fit">
                                <div class="col-md-12 model-img-part setMIPPART">
                                    <img src="{{ asset('/images/icons/veryfi-fit.png') }}">
                                </div>
                                <div class="col-md-12">
                                    <h2 class="veryfi-fit">Verify</h2>
                                </div>
                                <div class="col-md-12">
                                    <p>Verify your info</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 award-dilog-box setAWARDB1  border-top border-right">
                            <div class="row hiring-experience">
                                <div class="col-md-12 model-img-part setMIPPART">
                                    <img src="{{ asset('/images/icons/hiring-experience-of-this-employer.png') }}">
                                </div>
                                <div class="col-md-12">
                                    <h2 class="hiring-experience">Hiring experience of this employer</h2>
                                </div>
                                <div class="col-md-12">
                                    <p>Employer hire experience is : 0</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 award-dilog-box setAWARDB1 border-top border-right">
                            <div class="row posted-for" >
                                <div class="col-md-12 model-img-part setMIPPART">
                                    <img src="{{ asset('/images/icons/posted-for-how-long.png') }}">
                                </div>
                                <div class="col-md-12">
                                    <h2 class="posted-for">Posted for how long?</h2>
                                </div>
                                <div class="col-md-12">
                                    <p>The Longer of the post exist, may lower the availability.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 award-dilog-box setAWARDB1 border-left border-top border-right border-bottom">
                            <div class="row avatar-award">
                                <div class="col-md-12 model-img-part setMIPPART">
                                    <img src="{{ asset('/images/icons/avtar-award-img.png') }}">
                                </div>
                                <div class="col-md-12">
                                    <h2 class="avatar-award">Avatar</h2>
                                </div>
                                <div class="col-md-12">
                                    <p>If you have uploaded your personal avatar you will increase the chance to be awarded.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 award-dilog-box setAWARDB1 border-top border-right border-right border-bottom">
                            <div class="row personal-information">
                                <div class="col-md-12 model-img-part setMIPPART">
                                    <img src="{{ asset('/images/icons/personal-information.png') }}">
                                </div>
                                <div class="col-md-12">
                                     <h2 class="personal-information">Personal informations</h2>
                                </div>
                                <div class="col-md-12">
                                    <p>Employer will more likely to hire someone uploaded his personal information</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 award-dilog-box setAWARDB1 border-right border-top border-bottom">
                            <div class="row application-contact">
                                <div class="col-md-12 model-img-part setMIPPART">
                                    <img src="{{ asset('/images/icons/application-contact.png') }}">
                                </div>
                                <div class="col-md-12">
                                    <h2 class="application-contact">Applicant's Contact</h2>
                                </div>
                                <div class="col-md-12">
                                    <p>Applicants' with complete info is more trustful.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- End new design --}}



        {{--@if( isset($officialProjects) )--}}
            {{--@foreach($officialProjects as $officialProject)--}}
                {{--@include('frontend.includes.ucOfficialProjectItem')--}}
            {{--@endforeach--}}
        {{--@endif--}}

        {{--@if( isset($commonProjects) )--}}
            {{--@foreach($commonProjects as $commonProject)--}}
                {{--@include('frontend.includes.ucCommonProjectItem')--}}
            {{--@endforeach--}}
        {{--@endif--}}
    </div>
    <!-- award-dilog end-->

    <div id="modalMakeOffer" class="modal fade apply-job-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>

    {{--<div id="modalThumbdownJob" class="modal fade apply-job-modal" role="dialog">--}}
        {{--<div class="modal-dialog">--}}
            {{--<div class="modal-content">--}}
                {{--<div class="modal-header">--}}
                    {{--<button type="button" class="btn pull-right" data-dismiss="modal">--}}
                        {{--<i class="fa fa-close" aria-hidden="true"></i>--}}
                    {{--</button>--}}
                    {{--<h4 class="modal-title"> Thumb Down Job </h4>--}}
                {{--</div>--}}
                {{--<div class="modal-body">--}}
                    {{--<p>Are you sure you want to thumb down this job?</p>--}}
                    {{--<button type="button" class="btn btn-thumb-down" data-dismiss="modal">OK</button>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
@endsection

@section('footer')
    <script>
			var lang = {
				unable_to_request : "{{ trans('common.unable_to_request') }}",
				country : "{{ trans('common.country') }}",
				language : "{{ trans('common.language_2') }}",
				hourly_range : "{{ trans('common.hourly_range') }}",
				make_selection : "{{ trans('common.make_selection') }}",
			};

			var url = {
				getCountries : "{{ route('frontend.jobs.getcountries') }}",
				getLanguages : "{{ route('frontend.jobs.getlanguage') }}",
				getHourlyRanges : "{{ route('frontend.jobs.gethourlyranges') }}",
				removeFavouriteJob : "{{ route('frontend.removefavouritejob.post') }}",
				addFavouriteJob : "{{ route('frontend.addfavouritejob.post') }}",
			};

			@if ($advance_filter_json && $advance_filter_json != '')
                @foreach ($advance_filter_json as $key => $var)
                    @if( $var['type'] == 1 )
$('.div-country .value').html("{!! $var['text'] !!}");
			$('input[name="hidCountry"]').val("{{ $var['value'] }}");
			$('#btn-close-country').show();
			@elseif( $var['type'] == 2 )
$('.div-language .value').html("{!! $var['text'] !!}");
			$('input[name="hidLanguage"]').val("{{ $var['value'] }}");
			$('#btn-close-language').show();
			@elseif( $var['type'] == 3 )
$('.div-hourly-range .value').html("{!! $var['text'] !!}");
			$('input[name="hidHourlyRange"]').val("{{ $var['value'] }}");
			$('#btn-close-hourly-range').show();
              @endif
              @endforeach
              @endif

			var skillList = {!! $skillList !!};
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });

			$('.openDialog').on('click',function(e) {
				datascore = $(this).data('scoredetail');
                console.log(datascore);
                if(datascore.skillScore < 1) {
					$('.skill-fit').css('color', 'red');
				}
				if(datascore.locationScore < 1) {
					$('.location-fit').css('color', 'red');
				}
				if(datascore.budgetScore < 1) {
					$('.budget-fit').css('color', 'red');
				}
				if(datascore.verifyScore < 1) {
					$('.veryfi-fit').css('color', 'red');
				}
				if(datascore.hiringScore < 1) {
					$('.hiring-experience').css('color', 'red');
				}
				if(datascore.howLongScore < 1) {
					$('.posted-for').css('color', 'red');
				}
				if(datascore.avatarScopre < 1) {
					$('.avatar-award').css('color', 'red');
				}
				if(datascore.personalInformationScore < 1) {
					$('.personal-information').css('color', 'red');
				}
				if(datascore.applicantScore < 1) {
					$('.application-contact').css('color', 'red');
				}

				$("#award-dilog-model").modal("show");
			});

            setTimeout(function(){
			   $(".slimScrollBar").fadeOut();
            },10000);

    </script>
    <script src="{{asset('js/minifiedJobs.js')}}"></script>
{{--    <script src="/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>--}}
{{--    <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>--}}
{{--    <script src="/custom/js/frontend/ucProjectItem.js" type="text/javascript"></script>--}}
{{--    <script src="/custom/js/frontend/jobs.js?lm=010920171534" type="text/javascript"></script>--}}
@endsection
