@extends('frontend.layouts.default')

@section('title')
{{ trans('common.jobs') }}
@endsection

@section('description')
{{ trans('common.jobs') }}
@endsection

@section('author')
Xenren
@endsection

@section('header')
    <link href="/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/jobs.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/modalMakeOffer.css" rel="stylesheet" type="text/css"/>
@endsection

@section('customStyle')
<style type="text/css">
    .page-sidebar{
        margin-top: 40px;
        position: absolute;
        box-shadow: 0px 0px !important;
    }
    
    .page-content-wrapper{
        margin-top: 15px;
    }
</style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3 search-title-container">
            <span class="find-experts-search-title text-uppercase">{{trans('common.filter')}}</span>
        </div>
        <div class="col-md-9">
            <span class="find-experts-search-title text-uppercase">{{trans('jobs.listing')}}</span>
        </div>
    </div>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            @include('frontend.flash')

            <div class="row">
                <div class="col-md-12">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs ">
                            <li>
                                <a href="#tab_default_1" data-toggle="tab">
                                    <i class="ti-medall"></i>
                                    {{ trans('common.official_project')}}
                                </a>
                            </li>
                            <li class="active">
                                <a href="#tab_default_2" data-toggle="tab">
                                    {{ trans('common.common_project')}}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane " id="tab_default_1">
                            <div class="row">
                                <div class="col-md-12 found-count-top">
                                    <span class="number">
                                        {{ $officialProjects->count() }}
                                    </span>
                                    <span class="text">{{ trans('common.jobs_found') }}</span>

                                    @if ( $officialProjects->lastPage() > 1)
                                    <div class="find-experts-pagination pull-right">
                                        {{ $officialProjects->count() }}
                                    </div>
                                    @endif
                                </div>
                            </div>

                            @foreach($officialProjects as $officialProject)
                            <div class="portlet box job div-job-main official-pro">
                                <div class="row" style="margin: 0px;">
                                    <div class="col-md-3 official-pro-left">
                                        <div class="row">
                                            <div class="col-md-12 user-img">
                                                <img src="{{ $officialProject->out_cover }}" width="150px" height="150px">
                                            </div>
                                            <div class="col-md-12 desc-div">
                                                <label>{{ trans('common.shares') }}:</label>
                                                <span class="price">{{ $officialProject->stock_total }}</span>
                                                <br>
                                                <span>{{ trans('common.issued') }}: {{ $officialProject->stock_share }}%</span>
                                            </div>
                                            <div class="col-md-12 btn-div">
                                                <a href="{{ route('frontend.joindiscussion.getproject', [\Session::get('lang'), $officialProject->name, $officialProject->project_id]) }}">
                                                    <button class="btn btn-color-custom">
                                                        {{ trans('common.join_discuss') }}
                                                    </button>
                                                </a>
                                            </div>
                                        </div>    
                                    </div>
                                    <div class="col-md-9 official-pro-right">
                                        <div class="portlet-title">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-span-12">
                                                        <div class="project-name pull-left">
                                                            <h4> <i class="ti-medall medall-icon-round"></i> {{ $officialProject->title }}</h4>
                                                        </div>

                                                        @if( \Auth::check())
                                                        <a class="btn-add-fav-job {{ $officialProject->project->isYourFavouriteJob()? 'active': '' }}" href="javascript:;" data-url="{{ route('frontend.addfavouritejob.post') }}" data-project-id="{{ $officialProject->project->id }}">
                                                            <span class="fa fa-star pull-right"></span>
                                                        </a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 price-div top-info">
                                                    <ul>
                                                        <li>
                                                            <span>{{ trans('common.project_type') }}: <span> {{ trans('common.official_project') }}</span></span>
                                                        </li>
                                                        <li>
                                                            <span>{{ trans('common.time_frame') }}: <span> {{ $officialProject->getTimeFrame() }} days</span></span>
                                                        </li>
                                                        <li>
                                                            <span>{{ trans('common.end_date') }}: <span> {{ $officialProject->getRecruitEndDate() }}</span></span>
                                                        </li>
                                                    </ul>
                                                    <ul style="padding-top:20px;">
                                                        @if( $officialProject->budget_switch == 1)
                                                        <li>
                                                            <span>{{ trans('common.budget') }}:
                                                                <span>
                                                                    ${{ $officialProject->budget_from }}
                                                                    - ${{ $officialProject->budget_to }}
                                                                </span>
                                                            </span>
                                                        </li>
                                                        @endif
                                                        <li>
                                                            <span>{{ trans('common.funding_stage') }}: <span> {{ \App\Constants::translateFundingStage($officialProject->funding_stage) }} </span></span>
                                                        </li>
                                                        @if( isset($officialProject->city) )
                                                        <li>
                                                            <span>{{ trans('common.location') }}:
                                                                <a href="{{ route('frontend.map.officialprojectlocation', [ 'official_project_id' => $officialProject->id ]) }}">
                                                                    <span> {{ $officialProject->city }}</span>
                                                                </a>
                                                            </span>
                                                        </li>
                                                       @endif
                                                    </ul>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="row">
                                                <div class="col-md-12 description">
                                                    {{ $officialProject->description }}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 difficulty">
                                                    <label>{{ trans('common.difficulty') }}:</label>
                                                    <div class="difficulty-star">
                                                        @for ($i = 0; $i < $officialProject->project_level; $i++)
                                                        <i class="fa fa-star"></i>
                                                        @endfor

                                                        @for ($i = 0; $i < ( 6 - $officialProject->project_level ); $i++)
                                                        <i class="fa fa-star-o"></i>
                                                        @endfor
                                                        {{--<i class="fa fa-star"></i>--}}
                                                        {{--<i class="fa fa-star"></i>--}}
                                                        {{--<i class="fa fa-star"></i>--}}
                                                        {{--<i class="fa fa-star-o"></i>--}}
                                                    </div>
                                                </div>
                                                <div class="col-md-12 possition">
                                                    <label>{{ trans('common.position_needed') }}:</label>
                                                    <div>
                                                        <span>
                                                            @foreach( $officialProject->projectPositions as $key => $projectPosition)
                                                                @if( $key != 0 )
                                                                {{ ", " }}
                                                                @endif

                                                                {{  $projectPosition->jobPosition->getName() }}
                                                            @endforeach
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 skill">
                                                    <label>{{ trans('common.skill_request') }}:</label>
                                                    <div class="skill-div" >
                                                        @foreach( $officialProject->project->projectSkills as $projectSkill)
                                                        <span class="item-skill">
                                                            <span>{{ $projectSkill->getSkillName( $projectSkill->skill_id ) }}</span>
                                                        </span>
                                                        @endforeach
                                                        {{--<span class="item-skill"><span>Photoshop</span></span>--}}
                                                        {{--<span class="item-skill"><span>CSS</span></span>--}}
                                                        {{--<span class="item-skill"><span>HTML</span></span>--}}
                                                    </div>
                                                </div>
                                            </div>    
                                        </div>  
                                    </div>
                                </div>
                            </div>
                            @endforeach

                            {{--<div class="portlet box job div-job-main official-pro">--}}
                                {{--<div class="row" style="margin: 0px;">--}}
                                    {{--<div class="col-md-3 official-pro-left">--}}
                                        {{--<div class="row">--}}
                                            {{--<div class="col-md-12 user-img">--}}
                                                {{--<img src="/images/people-avatar.png" width="150px" height="150px">--}}
                                            {{--</div>--}}
                                            {{--<div class="col-md-12 desc-div">--}}
                                                {{--<label>Price:</label>--}}
                                                {{--<span class="price">100,000</span>--}}
                                                {{--<br>--}}
                                                {{--<span>Share Amount: 10%</span>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-md-12 btn-div">--}}
                                                {{--<button class="btn btn-color-custom">--}}
                                                    {{--Join Discuss--}}
                                                {{--</button>--}}
                                            {{--</div>--}}
                                        {{--</div>    --}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-9 official-pro-right">--}}
                                        {{--<div class="portlet-title">--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-md-12">--}}
                                                    {{--<div class="col-span-12">--}}
                                                        {{--<div class="project-name pull-left">--}}
                                                            {{--<h4> <i class="ti-medall medall-icon-round"></i> Infographic For Dog Ear Cleaner Product</h4>--}}
                                                        {{--</div>--}}
                                                        {{--<span class="fa fa-star pull-right"></span>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-md-12 price-div top-info">--}}
                                                    {{--<ul>--}}
                                                        {{--<li>--}}
                                                            {{--<span>Project Type: <span> Oficial Project</span></span>    --}}
                                                        {{--</li>--}}
                                                        {{--<li>--}}
                                                            {{--<span>Time Frame: <span> 7 days</span></span>    --}}
                                                        {{--</li>--}}
                                                        {{--<li>--}}
                                                            {{--<span>End date: <span> 2016-10-10</span></span>    --}}
                                                        {{--</li>--}}
                                                    {{--</ul>--}}
                                                    {{--<ul>--}}
                                                        {{--<li>--}}
                                                            {{--<span>Price: <span> $5,000</span></span>    --}}
                                                        {{--</li>--}}
                                                        {{--<li>--}}
                                                            {{--<span>Project State: <span> Pre A</span></span>    --}}
                                                        {{--</li>--}}
                                                        {{--<li>--}}
                                                            {{--<span>Location: <span> Zhang Hai</span></span>    --}}
                                                        {{--</li>--}}
                                                    {{--</ul>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<hr>--}}
                                        {{--</div>--}}
                                        {{--<div class="portlet-body">--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-md-12 description">--}}
                                                    {{--Looking for someone who will make some banners for my jewelry site. I need 3 existing banners resized to fit mobile screen 300 X 250. Then i need 3 banners with placed on a woman (see attachment ) in the size 645X967. Also I need 1 side banner...--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-md-12 difficulty">--}}
                                                    {{--<label>Difficulty:</label>--}}
                                                    {{--<div class="difficulty-star">--}}
                                                        {{--<i class="fa fa-star"></i>--}}
                                                        {{--<i class="fa fa-star"></i>--}}
                                                        {{--<i class="fa fa-star"></i>--}}
                                                        {{--<i class="fa fa-star"></i>--}}
                                                        {{--<i class="fa fa-star-o"></i>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-md-12 possition">--}}
                                                    {{--<label>Possition Needed:</label>--}}
                                                    {{--<div>--}}
                                                        {{--<span>Graphic Designer, UI Designer</span>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-md-12 skill">--}}
                                                    {{--<label>Skill Request:</label>--}}
                                                    {{--<div class="skill-div" >--}}
                                                        {{--<span class="item-skill"><span>Adobe Ilusstrator</span></span>--}}
                                                        {{--<span class="item-skill"><span>Photoshop</span></span>--}}
                                                        {{--<span class="item-skill"><span>CSS</span></span>--}}
                                                        {{--<span class="item-skill"><span>HTML</span></span>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>    --}}
                                        {{--</div>  --}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="portlet box job div-job-main official-pro">--}}
                                {{--<div class="row" style="margin: 0px;">--}}
                                    {{--<div class="col-md-3 official-pro-left">--}}
                                        {{--<div class="row">--}}
                                            {{--<div class="col-md-12 user-img">--}}
                                                {{--<img src="/images/people-avatar.png" width="150px" height="150px">--}}
                                            {{--</div>--}}
                                            {{--<div class="col-md-12 desc-div">--}}
                                                {{--<label>Price:</label>--}}
                                                {{--<span class="price">100,000</span>--}}
                                                {{--<br>--}}
                                                {{--<span>Share Amount: 10%</span>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-md-12 btn-div">--}}
                                                {{--<button class="btn btn-color-custom">--}}
                                                    {{--Join Discuss--}}
                                                {{--</button>--}}
                                            {{--</div>--}}
                                        {{--</div>    --}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-9 official-pro-right">--}}
                                        {{--<div class="portlet-title">--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-md-12">--}}
                                                    {{--<div class="col-span-12">--}}
                                                        {{--<div class="project-name pull-left">--}}
                                                            {{--<h4> <i class="ti-medall medall-icon-round"></i> Infographic For Dog Ear Cleaner Product</h4>--}}
                                                        {{--</div>--}}
                                                        {{--<span class="fa fa-star pull-right"></span>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-md-12 price-div top-info">--}}
                                                    {{--<ul>--}}
                                                        {{--<li>--}}
                                                            {{--<span>Project Type: <span> Oficial Project</span></span>    --}}
                                                        {{--</li>--}}
                                                        {{--<li>--}}
                                                            {{--<span>Time Frame: <span> 7 days</span></span>    --}}
                                                        {{--</li>--}}
                                                        {{--<li>--}}
                                                            {{--<span>End date: <span> 2016-10-10</span></span>    --}}
                                                        {{--</li>--}}
                                                    {{--</ul>--}}
                                                    {{--<ul>--}}
                                                        {{--<li>--}}
                                                            {{--<span>Price: <span> $5,000</span></span>    --}}
                                                        {{--</li>--}}
                                                        {{--<li>--}}
                                                            {{--<span>Project State: <span> Pre A</span></span>    --}}
                                                        {{--</li>--}}
                                                        {{--<li>--}}
                                                            {{--<span>Location: <span> Zhang Hai</span></span>    --}}
                                                        {{--</li>--}}
                                                    {{--</ul>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<hr>--}}
                                        {{--</div>--}}
                                        {{--<div class="portlet-body">--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-md-12 description">--}}
                                                    {{--Looking for someone who will make some banners for my jewelry site. I need 3 existing banners resized to fit mobile screen 300 X 250. Then i need 3 banners with placed on a woman (see attachment ) in the size 645X967. Also I need 1 side banner...--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-md-12 difficulty">--}}
                                                    {{--<label>Difficulty:</label>--}}
                                                    {{--<div class="difficulty-star">--}}
                                                        {{--<i class="fa fa-star"></i>--}}
                                                        {{--<i class="fa fa-star"></i>--}}
                                                        {{--<i class="fa fa-star"></i>--}}
                                                        {{--<i class="fa fa-star"></i>--}}
                                                        {{--<i class="fa fa-star-o"></i>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-md-12 possition">--}}
                                                    {{--<label>Possition Needed:</label>--}}
                                                    {{--<div>--}}
                                                        {{--<span>Graphic Designer, UI Designer</span>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-md-12 skill">--}}
                                                    {{--<label>Skill Request:</label>--}}
                                                    {{--<div class="skill-div" >--}}
                                                        {{--<span class="item-skill"><span>Adobe Ilusstrator</span></span>--}}
                                                        {{--<span class="item-skill"><span>Photoshop</span></span>--}}
                                                        {{--<span class="item-skill"><span>CSS</span></span>--}}
                                                        {{--<span class="item-skill"><span>HTML</span></span>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>    --}}
                                        {{--</div>  --}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                        <div class="tab-pane active" id="tab_default_2">
                            <div class="row">
                                <div class="col-md-12 found-count-top">
                                    <span class="number">
                                        {{ $models->count() }}
                                    </span>
                                    <span class="text">{{ trans('common.jobs_found') }}</span>

                                    @if ( $models->lastPage() > 1)
                                    <div class="find-experts-pagination pull-right">
                                        {{ $models->links() }}
                                    </div>
                                    @endif
                                </div>
                            </div>

                            <!-- BEGIN PAGE BASE CONTENT -->
                            @foreach ($models as $key => $var)
                                <div class="portlet box job div-job-{{ $var->id }} div-job-main">
                                    <div class="portlet-title">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="span-info-{{ $var->id }} col-span-12">
                                                    <div class="project-name pull-left"><h4>{{ $var->name }}</h4> </div>

                                                    @if( \Auth::check() )
                                                    <a class="btn-add-fav-job {{ $var->isYourFavouriteJob()? 'active': '' }}" href="javascript:;" data-url="{{ route('frontend.addfavouritejob.post') }}" data-project-id="{{ $var->id }}">
                                                        <span class="fa fa-star pull-right"></span>
                                                    </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 price-div">
                                                <span class="top-smoll">
                                                    {{ $var->explainPayType() }}
                                                    @if( $var->pay_type == \App\Constants::PAY_TYPE_FIXED_PRICE || $var->pay_type == \App\Constants::PAY_TYPE_HOURLY_PAY )
                                                    :
                                                    <span class="price">
                                                        <i class="fa fa-usd" aria-hidden="true"></i>
                                                        {{ $var->reference_price }}.
                                                    </span>
                                                    @endif
                                                </span>
                                                <span class="top-smoll">{{ trans('common.project_type') }}: <span class="type">{{ trans('common.public') }}.</span></span>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-md-12 description">
                                                {{ $var->description }}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 skill">
                                                <span class="skill-div-title">{{ trans('common.skill_request') }}:</span>
                                                <div class="skill-div div-skill-{{ $var->id }}" >
                                                    @foreach( $var->projectSkills as $key => $projectSkill )
                                                        <span class="item-skill">
                                                            <span>
                                                                {{ $projectSkill->getSkillName( $projectSkill->skill_id ) }}
                                                            </span>
                                                        </span>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>    
                                        <div class="row">
                                            <div class="col-md-6">
                                                @if( \Auth::guard('users')->check())
                                                    <a href="{{ route('frontend.modal.makeoffer', [
                                                            'project_id' => $var->id,
                                                            'user_id' => \Auth::id(),
                                                            'inbox_id' => 0
                                                        ] ) }}"
                                                        data-toggle="modal"
                                                        data-target="#modalMakeOffer">
                                                        <button id="btn-make-offer-{{ $var->id }}" class="btn btn-make-Offer">
                                                            {{ trans('common.make_offer') }}
                                                        </button>
                                                    </a>
                                                    <button id="btn-join-discussion-{{ $var->id }}"  class="btn btn-join-discuss" data-redirect="{{ route('frontend.joindiscussion.getproject', [\Session::get('lang'), $var->name, $var->id]) }}">
                                                        {{ trans('common.join_discuss') }}
                                                    </button>
                                                    {{--{!! Form::hidden('info', $var->projectQuestions->toJson(), ['class'=>'question-'. $var->id ]) !!}--}}
                                                @else
                                                    <button class="btn btn-login btn-make-Offer"  data-redirect="{{ route('login') }}">
                                                        {{ trans('common.make_offer') }}
                                                    </button>
                                                    <button class="btn btn-login btn-join-discuss" data-redirect="{{ route('login') }}">
                                                        {{ trans('common.join_discuss') }}
                                                    </button>
                                                @endif
                                            </div>
                                            <div class="col-md-6 span-info-{{ $var->id }} posted-by">
                                                {{  $var->getProjectCreatedInfo() }}

                                                <img alt="" class="img-circle" src="{{ $var->creator->getAvatar() }}" width="35px" height="35px">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            {!! $models->render() !!}
                            <!-- END PAGE BASE CONTENT -->
                        </div>
                    </div>
                </div>
            </div>
            
            {{--@if( \Auth::guard('users')->check())--}}
                {{--@include('frontend.includes.modalMakeOffer', [--}}
                    {{--'user' => $user--}}
                {{--])--}}

                {{--{!! Form::hidden('qq-id', $user->qq_id, ['class'=>'qq-id' ]) !!}--}}
                {{--{!! Form::hidden('wechat-id', $user->wechat_id, ['class'=>'wechat-id' ]) !!}--}}
                {{--{!! Form::hidden('skype-id', $user->skype_id, ['class'=>'skype-id' ]) !!}--}}
                {{--{!! Form::hidden('handphone-no', $user->handphone_no, ['class'=>'handphone-no' ]) !!}--}}
            {{--@endif--}}
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection

@section('modal')
    <div id="modalMakeOffer" class="modal fade apply-job-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>

    <div id="modalSendOffer" class="modal fade apply-job-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jquery-countdown/jquery.countdown.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script>
        var lang = {
            unable_to_request : "{{ trans('common.unable_to_request') }}"
        };
    </script>
    <script src="/custom/js/frontend/jobs.js" type="text/javascript"></script>

    <script>
        +function () {
            $(document).ready(function() {
                $('span.counter').each (function (index, value) {
                    $(this).countdown($(this).data('countdown'), function(event) {
                        $(this).html(
                            event.strftime('%D {{ trans('member.days') }} %H {{ trans('member.hours') }} %M {{ trans('member.minutes') }} %S {{ trans('member.seconds') }}')
                        );
                    });
                });
                $('body').on('click', '.delete-apply', function () {
                    var applicant_id = $(this).data('applicantid');
                    var $this = $(this);
                    $.ajax({
                        url: '{{ route('frontend.order.applydelete') }}' + '/' + applicant_id,
                        dataType: 'json',
                        data: {'_token': '{!! csrf_token() !!}'},
                        method: 'post',
                        beforeSend: function () {
                            $this.prop('disabled', true);
                        },
                        error: function (response, statusText, xhr, formElm) {
                            if (typeof response !== 'undefined' && typeof response.status !== 'undefined' && typeof response.responseText !== 'undefined' && typeof response.responseJSON !== 'undefined') {
                                if (response.status == 422) {
                                    $.each(response.responseJSON, function(i) {
                                        $.each(response.responseJSON[i], function(key, value) {
                                            alertError(value, false);
                                        });
                                    });
                                } else {
                                    alertError('{{ trans('order.unable_to_delete_apply') }}', true);
                                }
                            } else {
                                alertError('{{ trans('order.unable_to_delete_apply') }}', true);
                            }
                            $this.prop('disabled', false);
                        },
                        success: function (resp) {
                            if (resp.status == 'success') {
                                var ct = $this.parent('.tailor-action');

                                //Append the delete apply job button
                                var btn = '<button type="button" class="btn default apply-order" data-orderid="' + resp.data.order_id + '">{{ trans('member.apply_for_order') }}</button>';

                                //Remove the delete apply button
                                ct.find('span.font-green01').remove();
                                ct.find('span.font-apply').remove();
                                ct.find('a.delete-apply').remove();

                                ct.prepend(btn);
                                alertSuccess('{{ trans('order.successfully_delete_apply') }}', true);
                            } else {
                                alertError('{{ trans('order.unable_to_delete_apply') }}', true);
                            }
                        }
                    });
                });
                $('body').on('click', '.apply-order', function () {
                    var order_id = $(this).data('orderid');
                    var $this = $(this);
                    $.ajax({
                        url: '{{ route('frontend.order.apply') }}' + '/' + order_id,
                        dataType: 'json',
                        data: {'_token': '{!! csrf_token() !!}'},
                        method: 'post',
                        beforeSend: function () {
                            $this.prop('disabled', true);
                        },
                        error: function (response, statusText, xhr, formElm) {
                            if (typeof response !== 'undefined' && typeof response.status !== 'undefined' && typeof response.responseText !== 'undefined' && typeof response.responseJSON !== 'undefined') {
                                if (response.status == 422) {
                                    $.each(response.responseJSON, function(i) {
                                        $.each(response.responseJSON[i], function(key, value) {
                                            alertError(value, false);
                                        });
                                    });
                                } else {
                                    alertError('{{ trans('order.unable_to_apply_order') }}', true);
                                }
                            } else {
                                alertError('{{ trans('order.unable_to_apply_order') }}', true);
                            }
                            $this.prop('disabled', false);
                        },
                        success: function (resp) {
                            if (resp.status == 'success') {
                                var ct = $this.parent('.tailor-action');

                                //Append the delete apply job button
                                var btn = '<span class="font-green01"><i class="fa fa-check-circle"></i></span>';
                                btn = btn + '<span class="font-apply">{{ trans('member.already_applied') }}, </span>';
                                btn = btn + '<a href="javascript:;" class="link delete-apply" style="padding-top: 10px;" data-applicantid="' + resp.data.applicant_id + '">{{ trans("member.cancel_apply") }}</a>';

                                //Remove the apply job button
                                $this.remove();
                                ct.prepend(btn);
                                alertSuccess('{{ trans('order.successfully_apply_order') }}', true);
                            } else {
                                alertError('{{ trans('order.unable_to_apply_order') }}', true);
                            }
                        }
                    });
                });
            });
        }(jQuery);
    </script>
    <script>
        +function () {
            $(document).ready(function () {

                var elt = $('#advance-filter-tag');
                elt.tagsinput({
                    itemValue: 'value',
                    itemText: 'text',
                });

                function resetFilter() {
                    $('#advance-filter-keyword').val('');
                    $('#advance-filter-keyword').selectpicker('refresh');
                    resetValueFilter();
                }

                function resetValueFilter() {
                    $('#advance-filter-value').find('option').remove();
                    $('#advance-filter-value').selectpicker('refresh');
                }

                function updateFilterValue(data) {
                    var container = $('#advance-filter-value');
                    resetValueFilter();

                    $.each(data, function (index, value) {
                        container.append($('<option></option>')
                                .attr('value', index)
                                .text(value).data('text', value));
                    });
                    container.selectpicker('refresh');
                }

                function addAdvanceFilter() {
                    var keyword_container = $('#advance-filter-keyword');
                    var value_container = $('#advance-filter-value');

                    var keyword = keyword_container.val();
                    var text = keyword_container.find(":selected").text();
                    var valueId = value_container.val();
                    var valueText = value_container.find(":selected").text();

                    if (typeof keyword !== 'undefined' && typeof text !== 'undefined' && typeof valueId !== 'undefined' && typeof valueText !== 'undefined'
                        && keyword != '' && text != '' && valueId != '' && valueText != '') {
                        var obj = [];
                        elt.tagsinput('add', {'value': keyword + '|' + valueId, 'text': text + ':' + valueText});
                        resetFilter();
                    } else {
                        resetFilter();
                    }
                }

                function submitAdvanceFilter() {
                    var base_url = '{!!  $advance_url !!}';
                    var goto_url = base_url + 'advance_filter=' + elt.val();
                    window.location.href = goto_url;
                }

                @if ($advance_filter_json && $advance_filter_json != '')
                    @foreach ($advance_filter_json as $key => $var)
                        elt.tagsinput('add', {'value': '{{ $var['value'] }}', 'text': '{{ $var['text'] }}'});
                    @endforeach
                @endif

                $('body').on('click', '#advance-filter-submit-btn', function () {
                    submitAdvanceFilter();
                });

                $('body').on('change', '#advance-filter-keyword', function () {
                    var id = $(this).val();
                    var text = this.options[this.selectedIndex].innerHTML;

                    if (id != '') {
                        $.ajax({
                            type: 'get',
                            url: '{{ route('frontend.getfiltervalue') }}/' + id,
                            beforeSend: function () {
                            },
                            dataType: 'json',
                            success: function (resp) {
                                if (resp.status === 'success') {
                                    updateFilterValue(resp.data.c);
                                } else {
                                    alertError('{{ trans('common.unknown_error') }}');
                                }
                            },
                            error: function (resp) {
                                alertError('{{ trans('common.unknown_error') }}');
                            }
                        });
                    }
                });

                $('body').on('click', '#advance-filter-add-btn', function () {
                    addAdvanceFilter();
                });
            });
        }(jQuery);
    </script>
@endsection




