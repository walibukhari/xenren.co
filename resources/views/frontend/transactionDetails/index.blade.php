@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.published_project_title') }}
@endsection

@section('keywords')
    {{ trans('common.published_project_keyword') }}
@endsection

@section('description')
    {{ trans('common.published_project_desc') }}
@endsection

@section('author')
Xenren
@endsection

@section('header')
    <link href="/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/bootstrap-star-rating/css/star-rating.css" media="all" rel="stylesheet" type="text/css"/>
    <link href="/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/transactionDetails.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row setCol12Row">
        <div class="col-md-3 search-title-container">
            <span class="find-experts-search-title text-uppercase setMAINMENU">{{ trans('common.main_action') }}</span>
        </div>
        <div class="col-md-9 right-top-main setCol9RowTD">
            <div class="row">
                <div class="col-md-12 top-tabs-menu">
                   
                    <div class="caption">
                        <span class="find-experts-search-title text-uppercase setTD">{{trans('common.transaction_details')}}</span>
                    </div>
                    <br>
                </div>
            </div>
          
        </div>
    </div>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            @include('frontend.flash')
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="portlet light bordered">
                        <div class="portlet-body setPBOFTD">
                    <div class="user-profile">
            <img class="avatar" src="https://www.signmyemails.com/images/avatar.png" alt="Ash" />
            <div class="name setNAMEOFTDPB">John Doe</div>
            <div class="info setINFO">
              <strong>
                <i class="fa fa-calendar">
                  <strong>2017-11-5</strong>
                </i> |
                <span class="setTDSPANC">ID: 034567</span>
                <br>
                <span class="money">25$</span>
                <br>
                <span class="setTDSPANC">Payment Status: </span>
                <span class="setTDPAGECCOLOR">Complete</span> |
                <span class="setCOLORHC">Hourly</span>
                <i class="fa fa-file-pdf-o setTDPAGEII" aria-hidden="true">
                  <strong>View Invoice</strong>
                </i>
                <i class="fa fa-calendar-check-o setTRANDPAGEFAC" aria-hidden="true">
                  <strong>View Timelog Details</strong>
                </i>
            </div>
            <br>
            <br>
            <h4 class="display-4">
              <strong>{{trans('common.dates_and_times_based_on_utc')}}</strong>
            </h4>
            <br>
            <ul class="list-group setListGroup">
              <li class="list-group-item d-flex justify-content-between align-items-center setList">
                Reference ID
                <span class="badge">123654</span>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center setList">
                Date
                <span class="badge">March 6 2017</span>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center setList">
                Type
                <span class="badge">Hourly</span>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center setList">
                Description
                <span class="badge">This is a description.</span>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center setList">
                Client
                <span class="badge">Legends Lair</span>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center setList">
                Freelancer
                <span class="badge">Dzejlana Karajic</span>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center setList">
                Amount
                <span class="badge">($9)</span>
              </li>
            </ul>
          </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
        @include('frontend.includes.modalCreateExtraSize')
        @include('frontend.includes.modalEvaluationOrder')
    </div>
    <!-- END CONTENT -->
@endsection

@section('footer')

@endsection




