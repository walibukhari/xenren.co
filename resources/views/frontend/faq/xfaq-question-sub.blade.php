<!DOCTYPE html>
<!--[if IE 8]> <html lang="{{ trans('common.language') }}" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="{{ trans('common.language') }}" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="{{ trans('common.language') }}">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>{{ trans('common.faqBuyer_title') }}</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="{{ trans('common.faqBuyer_desc') }}" name="description"/>
        <meta content="{{ trans('common.faqBuyer_keyword') }}" name="keywords"/>
        <meta content="@yield('author')" name="author"/>
        <meta property="wb:webmaster" content="4bfa78a5221bb48a" />
        <meta property="qc:admins" content="2552431241756375" />
        

                <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->

        <link href="/assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="/assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />

        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="/assets/layouts/layout4/css/layout.css" rel="stylesheet" type="text/css" />
        <link href="/assets/layouts/layout4/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="/assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <!-- BEGIN HEADER AND FOOTER PAGE STYLES -->
        <link href="/custom/css/frontend/headerOrFooter.css" rel="stylesheet" type="text/css" />
        <!-- END HEADER AND FOOTER PAGE STYLES -->
        <link href="/css/faq_custom.css" rel="stylesheet" type="text/css" />
        <link href="/css/themify-icons.css" rel="stylesheet" type="text/css" />

        <link rel="shortcut icon" href="favicon.ico" />
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
        
        <div class="page-header navbar navbar-fixed-top">
            <div class="page-header-inner">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="page-logo">
                                <a href="{{ route('home') }}">
                                    <img src="{{asset('images/homelogo.jpg')}}" alt="logo" class="logo-default" height="45px"/>
                                    <h2 class="text-right">legends lair</h2>
                                </a>
                                <div class="menu-toggler sidebar-toggler hide">
                                </div>
                            </div>
                            <h3 class="help-center-menu"><a href="#">Help Center</a></h3>
                        </div>
                        <div class="col-md-4">
                            <nav class="right-menu pull-right">
                                <ul>
                                    <li><a href="#">customer support</a></li>
                                    <li><a href="#">login</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-container">
            <div class="row banner">
                <div class="col-md-12 banner-inner">
                    <div class="form-group search-main-box">
                        <h1>How can we help you?</h1>
                        
                        <input type="text" name="search" class="form-control" placeholder="I have a question about ...." />
                        <button class="btn">Search</button>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-3 page-left-contant">
                        <div class="row">
                            <div class="col-md-12 page-title">
                                <span class="text-uppercase">quick access</span>
                            </div>

                            <div class="col-md-12 left-contant-box">
                                <ul>
                                    <li>
                                        <a href="#"><img src="/images/icons/group.png"> Follow our system status</a>
                                    </li>
                                    <li>
                                        <a href="#"><img src="/images/icons/question.png"> See community discussions and help</a>
                                    </li>
                                    <li>
                                        <a href="#"><img src="/images/icons/tv.png"> Read tips on how to be successful</a>
                                    </li>
                                    <li>
                                        <a href="#"><img src="/images/icons/massage.png"> Send a request</a>
                                    </li>
                                    <li>
                                        <a href="#"><img src="/images/icons/like.png"> Help make Legends Lair better</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-md-12 page-title">
                                <hr>
                                <span>Popular Questions</span>
                            </div>

                            <div class="col-md-12 left-contant-box popular-que">
                                <ul>
                                    <li><a href="#">What can't sign in?</a></li>
                                    <li><a href="#">What is an agreement?</a></li>
                                    <li><a href="#">Why Was my bank account changed automatically?</a></li>
                                    <li><a href="#">Does posting a job cost anyting?</a></li>
                                    <li><a href="#">What if my verification amount is not in USD?</a></li>
                                    <li><a href="#">Why is my payment "awaiting credit from bank"?</a></li>
                                </ul>
                            </div>
                        </div>    
                    </div>
                    <div class="col-md-9 right-page-contant">
                        <div class="row">
                            <div class="col-md-12 top-tabs-menu">
                                <div class="tabbable-line">
                                    <ul class="nav nav-tabs ">
                                        <li class="active">
                                            <a href="#tab_default_1" data-toggle="tab">I'm a Buyer</a>
                                        </li>
                                        <li>
                                            <a href="#tab_default_2" data-toggle="tab">I'm a Seller</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="caption">
                                    <span class="text-uppercase">Buyer faq</span>
                                    <span class="small-text">Payment</span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_default_1">
                                <div class="row">
                                    <div class="col-md-12 details-box-que que-sub">
                                		<h3>What are Milestone Payments? </h3>
                                		
                                		<p>Freelancer.com offers a special feature called Milestone Payments to ensure that payments to projects are made securely within the site. Milestone Payments can be created by the employer for their awarded freelancer. This shows the employer's wittingness to pay the freelancer which. in turn. encourages the freelancer to do the job well.</p>

                                		<p>Before they work on projects. freelancers can request to have a Milestone Payment created for them as assurance of payment It is the employer who will decide on whether to create a Milestone Payment.</p>	

                                		<p>Once a Milestone Payment is created (funded). the money will not be transferred to the freelancer just yet. It will remain pending until the employer releases it Employers are advised to only release the Milestone Payment if they are completely satisfied with the delivered work and all deliverables have been submitted and up-to-standard. When all tasks are satisfactory and the employer releases the Milestone Payment it wilt be transferred to the freelancer's account balance on the site.</p>

                                		<p>In cases wherein a pending Milestone Payment needs to be returned (created accidentally or duplicated). the freelancer can cancel it for the funds to return to the employer's account balance. </p>

                                		<p>Note that only the employer can create a Milestone Payment and make the release. Conversely. only the freelancer can cancel and return the Milestone Payment.</p>

                                		<span>For more details about Milestone Payments. please check here:</span>

                                		<ul>
                                			<li>How to Create a Milestone Payment</li>
                                			<li>How to Release a Milestone Payment</li>
                                			<li>How to Cancel a Milestone Payment</li>
                                		</ul>

                                		<span>Aside from facilitating payments on the site. the Milestone Payment System also:</span>

                                		<ul>
                                			<li>Offers protection for both users. Employers are recommended to release payments only if they're happy with the submitted work while freelancers are assured in knowing that their payment is available and ready to be released.</li>
                                			<li>Allows the use of our Dispute Resolution Service. If something goes wrong with a project. both the employer and freelancer can have access to our Dispute Resolution Service. This is. however, only available for projects with pending Milestone Payments.</li>
                                			<li>Helps build freelancers' reputation. Both users will be able to leave feedback and ratings for each other if the project is completed and paid with Milestone Payments. Earning more positive reviews from completed projects will give freelancers more chances of being hired for other projects.</li>
                                			<li>Provides better work results for employers. Setting up a Milestone Payment encourages freelancers to be productive by making them confident that they are going to be well-compensated for their hard work. Employers wilt then find more agreeable and committed freelancers. which means they can also get better results out of their projects.</li>
                                		</ul>

                                		<div class="row footer">
                                			<div class="col-md-4">
                                				<h4>What this article helpful?</h4>
                                				<small>80% found this helpful</small>
                                			</div>
                                			<div class="col-md-8">
                                				<button class="btn btn-footer-color">Yes</button>
                                				<button class="btn btn-footer-white">No</button>
                                			</div>
                                		</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_default_2">
                                <div class="row">
                                    <div class="col-md-12">
                                        I'm a seller
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="row sugges-art">
                            <div class="col-md-12 title">
                                <span class="text">Suggested Articles</span> <span class="number">(12)</span> 
                                <div class="find-experts-pagination pull-right">
                                    <span class="count badge">1</span>
                                    <span class="count">2</span>
                                    <span class="count">3</span>
                                    <span class="count">4</span> ... <span class="count">8</span>
                                </div>
                            </div>

                            <div class="col-md-12 suggested-art-list-body">
                                <div class="row suggested-art-main">
                                    <div class="col-md-3 suggested-art-left">
                                        <span>Nov 16, 2016 </span>
                                        <br>
                                        <small>3 day ago</small>
                                        <br>
                                        <button class="btn btn-read-more">Read More</button>
                                    </div>
                                    <div class="col-md-9 suggested-art-right">
                                        <h4>What are Milestone Payments?</h4>
                                        <small>
                                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridicules mus. Donec quam fells, ultricies nec. pellentesque eu. pretium quis. sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel......
                                        </small>
                                    </div>
                                </div>
                                <div class="row suggested-art-main">
                                    <div class="col-md-3 suggested-art-left">
                                        <span>Nov 16, 2016 </span>
                                        <br>
                                        <small>3 day ago</small>
                                        <br>
                                        <button class="btn btn-read-more">Read More</button>
                                    </div>
                                    <div class="col-md-9 suggested-art-right">
                                        <h4>The Billing Agreement</h4>
                                        <small>
                                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridicules mus. Donec quam fells, ultricies nec. pellentesque eu. pretium quis. sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel......
                                        </small>
                                    </div>
                                </div>
                                <div class="row suggested-art-main">
                                    <div class="col-md-3 suggested-art-left">
                                        <span>Nov 16, 2016 </span>
                                        <br>
                                        <small>3 day ago</small>
                                        <br>
                                        <button class="btn btn-read-more">Read More</button>
                                    </div>
                                    <div class="col-md-9 suggested-art-right">
                                        <h4>Milestone Payment for Employers</h4>
                                        <small>
                                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridicules mus. Donec quam fells, ultricies nec. pellentesque eu. pretium quis. sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel......
                                        </small>
                                    </div>
                                </div>
                                <div class="row suggested-art-main">
                                    <div class="col-md-3 suggested-art-left">
                                        <span>Nov 16, 2016 </span>
                                        <br>
                                        <small>3 day ago</small>
                                        <br>
                                        <button class="btn btn-read-more">Read More</button>
                                    </div>
                                    <div class="col-md-9 suggested-art-right">
                                        <h4>Milestone Payment for Freelancers</h4>
                                        <small>
                                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridicules mus. Donec quam fells, ultricies nec. pellentesque eu. pretium quis. sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel......
                                        </small>
                                    </div>
                                </div>
                                <div class="row suggested-art-main">
                                    <div class="col-md-3 suggested-art-left">
                                        <span>Nov 16, 2016 </span>
                                        <br>
                                        <small>3 day ago</small>
                                        <br>
                                        <button class="btn btn-read-more">Read More</button>
                                    </div>
                                    <div class="col-md-9 suggested-art-right">
                                        <h4>How do I Deliver a Project?</h4>
                                        <small>
                                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridicules mus. Donec quam fells, ultricies nec. pellentesque eu. pretium quis. sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel......
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>    
                    </div>
                </div>

            </div>
        </div>

        <!-- BEGIN FOOTER -->
        <div class="page-footer container">
            <div class="page-footer-inner col-md-12">
                <div class="row">
                     <div class="col-md-2">
                        <!-- <img src="/images/logo-dark.png" alt="logo" class="logo-default" /*width="150px"*/ height="45px"  /> -->
                    </div> 
                    <div class="col-md-10">
                        {{ trans('home.about_us') }} | {{ trans('home.contact_us') }} | {{ trans('home.cooperate_link') }} | {{ trans('home.hiring_notice') }} | {{ trans('home.tnc') }} | {{ trans('home.privacy_policy') }} | {{ trans('home.location') }}
                        <br/><br/>
                        {{ trans('home.company_name')}}　&copy;　2004 - <?php echo date('Y'); ?> &nbsp;&nbsp; {{ trans('home.register_id') }}
                         &nbsp;&nbsp;{{ trans('home.customer_hotline')}} : 0755-23320228
                    </div>
                </div>
            </div>
            @if (isset($_G['route_name']) && $_G['route_name'] == 'home')
            <div class="row col-md-12 div-btnCreateOrder">
                <a href="{{ route('frontend.postproject') }}" class="btn btn-lg btnOrange">
                    {{ trans('member.create_project_now') }}<i class="fa fa-plus"></i>
                </a>
            </div>
            @endif
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->

        <!-- BEGIN CORE PLUGINS -->
        <script src="/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
    </body>
</html>    