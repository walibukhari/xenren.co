<!DOCTYPE html>
<!--[if IE 8]> <html lang="{{ trans('common.language') }}" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="{{ trans('common.language') }}" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="{{ trans('common.language') }}">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>{{ trans('common.faqBuyer_title') }}</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="{{ trans('common.faqBuyer_desc') }}" name="description"/>
        <meta content="{{ trans('common.faqBuyer_keyword') }}" name="keywords"/>
        <meta content="@yield('author')" name="author"/>
        <meta property="wb:webmaster" content="4bfa78a5221bb48a" />
        <meta property="qc:admins" content="2552431241756375" />


                <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->

        <link href="/assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="/assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />

        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="/assets/layouts/layout4/css/layout.css" rel="stylesheet" type="text/css" />
        <link href="/assets/layouts/layout4/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="/assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <!-- BEGIN HEADER AND FOOTER PAGE STYLES -->
        <link href="/custom/css/frontend/headerOrFooter.css" rel="stylesheet" type="text/css" />
        <!-- END HEADER AND FOOTER PAGE STYLES -->
        <link href="/css/faq_custom.css" rel="stylesheet" type="text/css" />
        <link href="/css/themify-icons.css" rel="stylesheet" type="text/css" />

        <link rel="shortcut icon" href="favicon.ico" />
        <style>
            .pagination {
                padding-left: 0;
                /*margin: 20px 0;*/
                border-radius: 4px;
            }

            .pagination > .active > a,
            .pagination > .active > a:focus,
            .pagination > .active > a:hover,
            .pagination > .active > span,
            .pagination > .active > span:focus,
            .pagination > .active > span:hover {
                z-index: 3;
                color: #fff;
                background-color: #4CAF50;;
                border-color: #4CAF50;;
                cursor: default;
            }

            .pagination > li > a, .pagination > li > span {
                position: relative;
                float: left;
                padding: 6px 12px;
                line-height: 1.42857;
                text-decoration: none;
                color: #4CAF50;
                background-color: #fff;
                border: 1px solid #ddd;
                margin-left: -1px;
            }

            .center-menu{
                margin-top: 23px;
            }

            /*.pagination a.active {*/
            /*background-color: #4CAF50;*/
            /*color: white;*/
            /*}*/

            /*.pagination a:hover:not(.active) {background-color: #ddd;}*/

            .ul li  a {
                text-decoration: none;
                color: #494949;
                font-size: 17px;
                text-transform: uppercase;
            }
            a:hover {
                color:black;
            }
            header {
                background: #fff;
                width: 100%;
                /*height: 100%;*/
                position: fixed;
                top: 0;
                left: 0;
                z-index: 100;
            }
            #logo{
                margin: 20px;
                float: left;
                width: 200px;
                height: 40px;
                display: block;
                margin-left:30%;
            }
            nav {
                float: right;
                padding: 12px;
                margin-left:-49px;
            }
            #menu-icon {
                display: hidden;
                width: 30px;
                height: 23px;
                background-image: url(images/logo-menu/mennu.png);
                background-repeat: round;
            }
            .ul{

                list-style: none;
            }
            .ul li {

                display: inline-block;
                float: left;
                padding: 10px
            }
            .text-right{
                border-right: 1px solid #c1c1c1;
                color: #494949;
                font-weight: 600;
                margin-top: 20px;
                padding-right: 10px;
                text-transform: uppercase;
                font-family: 'Raleway', sans-serif;
                font-size: 30px;
            }

            .center-menu a{
                color:black;
                font-size:26px;
            }


            @media only screen and (max-width : 991px) {
                .ul li a {
                    text-decoration: none;
                    color: #494949;
                    font-size: 17px;
                    text-transform: uppercase;
                    margin-right: 22px;
                }
                .banner .banner-inner .search-main-box {
                    overflow: hidden;
                    width: 65%;
                    margin: auto;
                    margin-top: 0px;
                }
                .banner .banner-inner h1 {
                    color: #fff;
                    font-weight: 700;
                    font-size: 42px;
                    margin-bottom: 20px;
                    margin-top: 20%;
                }
                .banner {
                    background: url(/images/banner.jpg);
                    height: 400px;
                    background-size: 100% 100%;
                    width: 100%;
                    margin: 0px;
                    margin-top: 6%;
                }
                header {
                    position: fixed;
                }
                #menu-icon {
                    margin-right: 25px;
                    margin-top: 17px;
                    display: inline-block;
                    width: 24px;
                    height: 19px;
                }
                nav ul, nav:active ul {
                    display: none;
                    position: absolute;
                    background: #fff;
                    right: 20px;
                }
                nav li {
                    text-align: center;
                    width: 100%;
                    padding: 10px 0;
                    margin: 0;
                }
                nav:hover ul {
                    display: block;
                }
                .text-right {
                    border-right: 1px solid #c1c1c1;
                    color: #494949;
                    font-weight: 600;
                    margin-top: 28px;
                    padding-right: 10px;
                    text-transform: uppercase;
                    font-family: 'Raleway', sans-serif;
                    font-size: 22px;
                }
                .center-menu a {
                    font-size: 24px;
                    color: black;
                }
                .text-right {
                    border-right: 1px solid #c1c1c1;
                    color: #494949;
                    font-weight: 600;
                    /* padding-right: 72px; */
                    text-transform: uppercase;
                    font-family: 'Raleway', sans-serif;
                    font-size: 15px;
                    margin-right: 9%;
                }
                #logo {
                    margin: 20px;
                    float: left;
                    display: block;
                }
            }

            @media (max-width:767px)
            {
                .banner .banner-inner .btn {
                    margin-left: 0px;
                    background: #61AF2C;
                    color: #fff;
                    text-transform: none;
                    padding: 5px 25px;
                    font-size: 16px;
                    margin-top: 2px;
                }
                .banner .banner-inner .form-control {
                    width:100%;
                    float: left;
                    border-radius: 0px;
                    border: 2px solid #61AF2C;
                }
                .center-menu a {
                    font-size: 23px;
                }
                .text-right {
                    border-right: 1px solid #c1c1c1;
                    color: #494949;
                    font-weight: 600;
                    /* padding-right: 72px; */
                    text-transform: uppercase;
                    font-family: 'Raleway', sans-serif;
                    font-size: 15px;
                    margin-right: -17%;
                }
            }

            @media (max-width:676px)
            {
                .center-menu a {
                    font-size: 23px;
                }
                .text-right {
                    border-right: 1px solid #c1c1c1;
                    color: #494949;
                    font-weight: 600;
                    /* padding-right: 72px; */
                    text-transform: uppercase;
                    font-family: 'Raleway', sans-serif;
                    font-size: 12px;
                    margin-right: -17%;
                }
            }

            @media (max-width:496px)
            {
                .banner .banner-inner h1 {
                    color: #fff;
                    font-weight: 700;
                    font-size: 42px;
                    margin-bottom: 20px;
                    margin-top: 35%;
                }
                .center-menu a {
                    font-size: 20px;
                }
                .text-right {
                    border-right: 1px solid #c1c1c1;
                    color: #494949;
                    font-weight: 600;
                    /* padding-right: 72px; */
                    text-transform: uppercase;
                    font-family: 'Raleway', sans-serif;
                    font-size: 11px;
                    margin-right: -46%;
                }
                #menu-icon {
                    width: 19px;
                    height: 16px;
                    margin-top: 16px;
                }
            }


            @media (max-width:398px)
            {
                .page-logo img {
                    width: 35px;
                    height: 35px;
                }
                .text-right {
                    border-right: 1px solid #c1c1c1;
                    color: #494949;
                    font-weight: 600;
                    /* padding-right: 72px; */
                    text-transform: uppercase;
                    font-family: 'Raleway', sans-serif;
                    font-size: 9px;
                    margin-right: -46%;
                }
            }

            @media (max-width:411px)
            {

                .text-right {
                    border-right: 1px solid #c1c1c1;
                    color: #494949;
                    font-weight: 600;
                    /* padding-right: 72px; */
                    text-transform: uppercase;
                    font-family: 'Raleway', sans-serif;
                    margin-right: -46%;
                }

            }

            @media (max-width:320px){
                .page-logo img {
                    width: 35px;
                    height: 35px;
                }
                .text-right {
                    border-right: 1px solid #c1c1c1;
                    color: #494949;
                    font-weight: 600;
                    /* padding-right: 72px; */
                    text-transform: uppercase;
                    font-family: 'Raleway', sans-serif;
                    font-size: 9px;
                    margin-right: -89%;
                }
            }


        </style>
    </head>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">

        {{--<div class="page-header navbar navbar-fixed-top">--}}
            {{--<div class="page-header-inner">--}}
                {{--<div class="container">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-8">--}}
                            {{--<div class="page-logo">--}}
                                {{--<a href="{{ route('home') }}">--}}
                                    {{--<img src="/images/fav.png" alt="logo" class="logo-default" height="45px"/>--}}
                                    {{--<h2 class="text-right">{{ trans('home.legends_lair') }}</h2>--}}
                                {{--</a>--}}
                                {{--<div class="menu-toggler sidebar-toggler hide">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<h3 class="help-center-menu"><a href="#">{{ trans('home.help_center') }}</a></h3>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-4">--}}
                            {{--<nav class="right-menu pull-right">--}}
                                {{--<ul>--}}
                                    {{--<li><a href="#">{{ trans('home.customer_support') }}</a></li>--}}
                                    {{--@if(\Auth::guard('users')->user() == null)--}}
                                    {{--<li><a href="{{ route('login') }}">{{ trans('home.login') }}</a></li>--}}
                                        {{--@endif--}}
                                {{--</ul>--}}
                            {{--</nav>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        <header>

            <div class="row">
                <div class="col-md-2 col-xs-2">
                    <div class="page-logo">
                        <a id="logo" href="{{ route('home') }}">
                            <img src="{{asset('images/homelogo.jpg')}}" alt="logo" class="logo-default" style="margin-top:-6px;" height="45px"/>
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-xs-2">
                    <h2 class="text-right">{{ trans('home.legends_lair') }}</h2>
                </div>
                <div class="col-md-4 col-xs-5">
                    <h3 class="center-menu">
                        <a href="#">{{ trans('home.help_center') }}</a>
                    </h3>
                </div>
                <div class="col-md-3">
                    <nav>
                        <a href="#" id="menu-icon"></a>
                        <ul class="ul">
                            @if(!Auth::check())
                                <li><a href="{{ route('login') }}">{{ trans('home.ci_login') }}</a></li>
                            @endif
                            <li><a href="#">{{ trans('home.customer_support') }}</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>

        <div class="page-container">
            <div class="row banner">
                <div class="col-md-12 banner-inner">
                    <div class="form-group search-main-box">
                        <h1>{{ trans('home.how_can_we_help_you') }}</h1>

                        <input type="text" name="search" class="form-control" placeholder="{{ trans('home.i_have_a_question_about') }} ...." />
                        <button class="btn">{{ trans('home.search') }}</button>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-3 page-left-contant">
                        <div class="row">
                            <div class="col-md-12 page-title">
                                <span class="text-uppercase">{{ trans('home.quick_access') }}</span>
                            </div>

                            <div class="col-md-12 left-contant-box">
                                <ul>
                                    <li>
                                        <a href="#"><img src="/images/icons/group.png"> {{ trans('home.follow_our_system_status') }}</a>
                                    </li>
                                    <li>
                                        <a href="#"><img src="/images/icons/question.png"> {{ trans('home.See_community_discussions_and_help') }}</a>
                                    </li>
                                    <li>
                                        <a href="#"><img src="/images/icons/tv.png"> {{ trans('home.Read_tips_on_how_to_be_successful') }}</a>
                                    </li>
                                    <li>
                                        <a href="#"><img src="/images/icons/massage.png"> S{{trans('home.send_a_request')}}</a>
                                    </li>
                                    <li>
                                        <a href="#"><img src="/images/icons/like.png"> {{trans('home.help_make_legends_lair_better')}}</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-md-12 page-title">
                                <hr>
                                <span>{{trans('home.popular_questions')}}</span>
                            </div>

                            <div class="col-md-12 left-contant-box popular-que">
                                <ul>
                                    <li><a href="#">{{trans('home.what_cannot_sign_in?')}}</a></li>
                                    <li><a href="#">{{trans('home.agreement?')}}</a></li>
                                    <li><a href="#">{{trans('home.why_was_my_bank_account_changed_automatically')}}?</a></li>
                                    <li><a href="#">{{trans('home.does_posting_a_job_cost_anything')}}?</a></li>
                                    <li><a href="#">{{trans('home.what_if_my_verification_amount_is_not_in_usd')}}?</a></li>
                                    <li><a href="#">{{trans('home.awaiting_credit')}}?</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 right-page-contant">
                        <div class="row">
                            <div class="col-md-12 top-tabs-menu">
                                <div class="tabbable-line">
                                    <ul class="nav nav-tabs ">
{{--                                        <li class="{{($faq->getAncestors()->where('id',2)->count()!=0)?'active':''}}">--}}
{{--                                            <a href="#tab_default_1" data-toggle="tab">{{trans('home.buyer')}}</a>--}}
{{--                                        </li>--}}
{{--                                        <li class="{{($faq->getAncestors()->where('id',2)->count()==0)?'active':''}}">--}}
{{--                                            <a href="#tab_default_2" data-toggle="tab">{{trans('home.seller')}}</a>--}}
{{--                                        </li>--}}
                                    </ul>
                                </div>
                                <div class="caption">

                                    <span class="text-uppercase">Employer Faq</span>
                                    <span class="small-text">Employer</span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane {{($faq->getAncestors()->where('id',2)->count()!=0)?'active':''}}"
                                 id="tab_default_1">
                                <div class="row">
                                    <div class="col-md-12 details-box-que">
                                        <div class="row">
                                            <div class="col-md-9 left   -side">
                                                <h3>{{$faq->getTitle()}} </h3>
                                                {{($faq->getContent()!=null)?$faq->getContent():'content empty'}}

                                                <div class="row footer">
                                        			<div class="col-md-5">
                                        				<h4>{{trans('home.what_this_article_helpful')}}?</h4>
                                        				<small>80% {{trans('home.found_this_helpful')}}</small>
                                        			</div>
                                        			<div class="col-md-7">
                                        				<button class="btn btn-footer-color">Yes</button>
                                        				<button class="btn btn-footer-white">No</button>
                                        			</div>
                                        		</div>
                                        	</div>
                                        	<div class="col-md-3 right-side">
                                        		<ul>
                                                    @foreach($buyer->children as $buy)
                                                        <li class="active">
                                                            <label class="tree-toggler"> <a
                                                                        href="javascript:;"> {{$buy->getTitle()}}</a></label>
                                                            @if(!empty($buy->children))
                                                                <ul class="sub-li">
                                                                    @foreach($buy->children as $child)

                                                                        <li class="{{($child->id==$faq->id)? "active-sub":""}}">
                                                                            <a href="{{route('faq.faq-buyer.employer',$child->id)}}">
                                                                                <i class="fa fa-angle-right"></i>
                                                                                <p>{{$child->getTitle()}}</p></a>
                                                                        </li>
                                                                    @endforeach
                                                                </ul>
                                                            @endif

                                                        </li>
                                                    @endforeach
                                                <!--   <li class="active">
                                                    <a href="">Getting Started</a>
                                                    <ul class="sub-li">
                                                    <li><a href="#"><i class="fa fa-angle-right"></i> <p>Legends Lair Payment Protection</p></a></li>
                                                    <li><a href="#"><i class="fa fa-angle-right"></i> <p>Getting Started as a Freelancer</p></a></li>
                                                    <li class="active-sub"><a href="#"><i class="fa fa-caret-right"></i> <p>How do I get started as a client?</p></a></li>
                                                    <li><a href="#"><i class="fa fa-angle-right"></i> <p>What is a Job Success Score?</p></a></li>
                                                    </ul>
                                                    </li>
                                                    <li><a href="#">Freelancer Profile</a></li>
                                                    <li><a href="#">Getting Paid</a></li>
                                                    <li><a href="#">Work With Clients</a></li>
                                                    <li><a href="#">Account</a></li>
                                                    <li><a href="#">Apps & Massages</a></li> -->
                                        		</ul>
                                        	</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane {{($faq->getAncestors()->where('id',2)->count()==0)?'active':''}}"
                                 id="tab_default_2">
                                <div class="row">
                                    <div class="col-md-12 details-box-que">
                                        <div class="row">
                                            <div class="col-md-9 left   -side">
                                                <h3>{{$faq->getTitle()}} </h3>
                                                {{($faq->getContent()!=null)?$faq->getContent():'content empty'}}

                                                <div class="row footer">
                                                    <div class="col-md-5">
                                                        <h4>{{trans('home.what_this_article_helpful')}}?</h4>
                                                        <small>80% {{trans('home.found_this_helpful')}}</small>
                                                    </div>
                                                    <div class="col-md-7">
                                                        <button class="btn btn-footer-color">Yes</button>
                                                        <button class="btn btn-footer-white">No</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 right-side">
                                                <ul>
                                                    @foreach($seller->children as $buy)
                                                        <li class="active">
                                                            <label class="tree-toggler"> <a
                                                                        href="javascript:;"> {{$buy->getTitle()}}</a></label>
                                                            @if(!empty($buy->children))
                                                                <ul class="sub-li">
                                                                    @foreach($buy->children as $child)

                                                                        <li class="{{($child->id==$faq->id)? "active-sub":""}}">
                                                                            <a href="{{route('faq.faq-buyer.employer',$child->id)}}">
                                                                                <i class="fa fa-angle-right"></i>
                                                                                <p>{{$child->getTitle()}}</p></a>
                                                                        </li>
                                                                    @endforeach
                                                                </ul>
                                                            @endif

                                                        </li>
                                                    @endforeach

                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row sugges-art">
                            <div class="col-md-12 title">
                                <div class="pull-left">
                                    <span class="text">{{ trans('home.suggested_articles') }}</span> <span
                                            class="number">{{$pagination->total()}}</span>
                                </div>
                                <div class="pull-right">
                                    @include('frontend.includes.pagination', ['paginator' => $pagination])
                                </div>
                            </div>

                            <div class="col-md-12 suggested-art-list-body">
                                @foreach($suggested as $suggest)

                                    <div class="row suggested-art-main">
                                        <div class="col-md-3 suggested-art-left">
                                            <span>{{$suggest->created_at}}</span>
                                            <br>
                                            <small>3 day ago</small>
                                            <br>
                                            <a href="{{route('faq.faq-buyer.employer',$suggest->id)}}"
                                               class="btn btn-read-more">{{trans('home.read_more')}}</a>
                                        </div>
                                        <div class="col-md-9 suggested-art-right">
                                            @if(!empty($suggest->parent)||$suggest->parent !=null)
                                                @foreach($suggest->getAncestors() as $parent)


                                                    <span class="small-text">> {{$parent->getTitle()}}</span>
                                                @endforeach
                                            @endif
                                            <h4>{{$suggest->getTitle()}}</h4>
                                            <small>
                                                {!! str_limit($suggest->getContent(),150) !!}...
                                            </small>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- BEGIN FOOTER -->
        <div class="page-footer container">
            <div class="page-footer-inner col-md-12">
                <div class="row">
                     <div class="col-md-2">
                        <!-- <img src="/images/logo-dark.png" alt="logo" class="logo-default" /*width="150px"*/ height="45px"  /> -->
                    </div>
                    <div class="col-md-10">
                        <a href="{{ route('frontend.about-us') }}">{{ trans('home.about_us') }}</a> | <a href="{{ route('frontend.contact-us') }}">{{ trans('home.contact_us') }} </a>| <a href="{{ route('frontend.cooperate-page') }}">{{ trans('home.cooperate_link') }}</a> | <a href="{{ route('frontend.hiring-page') }}">{{ trans('home.hiring_notice') }}</a> | <a href="{{ route('frontend.terms-service') }}">{{ trans('home.tnc') }}</a> | <a href="">{{ trans('home.privacy_policy') }}</a> | <a href="http://f.amap.com/f7qK_0DE2dKb">{{ trans('home.location') }}</a>
                        <br/><br/>
                        {{ trans('home.company_name')}}　&copy;　2004 - <?php echo date('Y'); ?> &nbsp;&nbsp; {{ trans('home.register_id') }}
                        &nbsp;&nbsp;{{ trans('home.customer_hotline')}} : 0755-23320228
                    </div>
                </div>
            </div>
            @if (isset($_G['route_name']) && $_G['route_name'] == 'home')
            <div class="row col-md-12 div-btnCreateOrder">
                <a href="{{ route('frontend.postproject') }}" class="btn btn-lg btnOrange">
                    {{ trans('member.create_project_now') }}<i class="fa fa-plus"></i>
                </a>
            </div>
            @endif
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->

        <!-- BEGIN CORE PLUGINS -->
        <script src="/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->

        <script>
            $(document).ready(function () {
                $('label.tree-toggler').click(function () {
                    $(this).parent().children('ul.sub-li').toggle(300);
                });
            });
        </script>
    </body>
</html>
