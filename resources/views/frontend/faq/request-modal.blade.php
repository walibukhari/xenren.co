<!-- Modal -->
<div id="sendRequest" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    {{trans('faqs.sendrequest')}}
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-body">
                    <div class="form-group">
                        <div class="col-md-12" id="add-send-request-errors"></div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-9 control-label">
                            {!! trans('faqs.sendemail') !!}
                        </label>

                    </div>
                <!--
                    {!! Form::open(['route' => 'faq.send-request.post', 'id' => 'send-request-form', 'method' => 'post', 'class' => 'form-horizontal']) !!}

                    <div class="form-group">
                        <label class="col-md-3 control-label">
                            {{trans('member.email')}}
                        </label>

                        <div class="col-md-9">

                            {!! Form::text('email','',['class'=>'form-control']) !!}

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">
                            {{trans('member.account_name')}}
                        </label>

                        <div class="col-md-9">

                            {!! Form::text('name','',['class'=>'form-control']) !!}

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">
                            {{trans('faqs.message')}}
                        </label>

                        <div class="col-md-9">

                            {!! Form::textarea('messaage','',['class'=>'form-control']) !!}

                        </div>
                    </div>

                    {{--<div class="form-group">--}}
                    {{--<label class="col-md-3 control-label">--}}
                {{--{{trans('common.experience')}}--}}
                    {{--</label>--}}
                    {{--<div class="col-md-9">--}}

                    {{--</div>--}}
                    {{--</div>--}}
                        -->
                </div>
            </div>
            <div class="modal-footer">
                {{--<button id="btn-send-request-submit" type="submit" class="btn btn-submit"--}}
                {{-->--}}

                {{--{{ trans('common.save_change') }}--}}
                {{--</button>--}}
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    {{ trans('member.cancel') }}
                </button>
            </div>
            {{--{!! Form::close() !!}--}}
        </div>
    </div>
</div>

