@extends('frontend.layouts.default')

@section('header')
    <link href="/assets/global/plugins/jcrop/css/jquery.Jcrop.min.css" rel="stylesheet"/>
@stop

@section('footer')
    <script>
        var lang = {
            unable_to_request : '{{ trans('common.unable_to_request') }}'
        }

        var url = {
            {{--cropImage : '{{ route('frontend.cropimage.post') }}'--}}
        }
    </script>
    <script type="text/javascript" src="/assets/global/plugins/jcrop/js/jquery.Jcrop.min.js" ></script>
    {{--<script type="text/javascript" src="/custom/js/frontend/croppingImage.js" ></script>--}}
@stop

@section('content')
    <div class="row">
        <div>
            <img id="portfolio-img-17" src="/images/startupstockphotos-36.jpg" width="500px"/>
            <a href="{{ route('frontend.modal.cropImage', ['type' => "portfolio", 'id' => 17 ] ) }}"
                data-toggle="modal"
                data-target="#modalCropImage">
                <button id="btn-crop-image-modal" type="button">
                    Crop Image Modal
                </button>
            </a>
        </div>
    </div>
@stop

@section('modal')
    <div id="modalCropImage" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
            </div>
        </div>
    </div>
@endsection