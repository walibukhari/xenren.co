@extends('frontend.layouts.default')

@section('header')
    <link href="/assets/global/plugins/dropzone/dropzone.min.css" rel="stylesheet"/>
    <link href="/assets/global/plugins/dropzone/basic.min.css" rel="stylesheet"/>
    <link href="/custom/css/frontend/personalPortfolio.css" rel="stylesheet"/>
@stop

@section('footer')
    <script>
        var lang = {
            'remove' : "{{ trans('common.remove') }}",
            'image_is_bigger_than_5mb' : "{{ trans('common.image_is_bigger_than_5mb') }}",
            'you_can_not_upload_any_more_files' : "{{ trans('common.you_can_not_upload_any_more_files') }}",
            'drop_files_here_to_upload' : "{{ trans('common.drop_files_here_to_upload') }}",
            'cancel_upload' : "{{ trans('common.cancel_upload') }}"
        }
    </script>
    <script type="text/javascript" src="/assets/global/plugins/dropzone/dropzone.min.js" ></script>
    <script type="text/javascript" src="/custom/js/dropzone-config-portfolio.js" ></script>
@stop

@section('content')
    <div class="row">
        <div class="dropzone" id="portfolio-dropzone">
            <div class="fallback">
                <input name="file" type="file" multiple />
            </div>
        </div>
    </div>
@stop
