@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.published_project_title') }}
@endsection

@section('keywords')
    {{ trans('common.published_project_keyword') }}
@endsection

@section('description')
    {{ trans('common.published_project_desc') }}
@endsection

@section('author')
    Xenren
@endsection

@section('header')
    <link href="/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/bootstrap-star-rating/css/star-rating.css" media="all" rel="stylesheet" type="text/css"/>
    <link href="/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/myPublishOrder.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3 search-title-container">
            <span class="find-experts-search-title text-uppercase">{{ trans('common.main_action') }}</span>
        </div>
        <div class="col-md-9 right-top-main">
            <div class="row">
                <div class="col-md-12 top-tabs-menu">
                    <div class="order-status-menu pull-right">
                        <ul>
                            <li {{ isset($status) && $status == 'all' ? "class=active": ''}}>
                                <a href="{{ route('frontend.mypublishorder')}}?status=all ">
                                    {{ trans('common.all')  }}
                                </a>
                            </li>
                            <li {{ isset($status) && ( $status == 'published') ? "class=active": ''}}>
                                <a href="{{ route('frontend.mypublishorder')}}?status=published">
                                    {{ trans('common.published') }}
                                </a>
                            </li>
                            <li {{ isset($status) && $status == 'progressing' ? "class=active": ''}}>
                                <a href="{{ route('frontend.mypublishorder')}}?status=progressing">
                                    {{ trans('common.progressing') }}
                                </a>
                            </li>
                            <li {{ isset($status) && $status == 'completed' ? "class=active": ''}}>
                                <a href="{{ route('frontend.mypublishorder')}}?status=completed">
                                    {{ trans('common.completed') }}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="caption">
                        <span class="find-experts-search-title text-uppercase">{{ trans('common.published_project') }}</span>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12 found-count-top">
                    <span class="text">
                        @if( $status == 'all' )
                            {{ trans('common.all') }}
                        @elseif( $status == 'published' )
                            {{ trans('common.published') }}
                        @elseif( $status == 'progressing' )
                            {{ trans('common.in_progress') }}
                        @elseif( $status == 'completed' )
                            {{ trans('common.completed') }}
                        @endif
                    </span>
                    <span class="number">({{ count($projects) }})</span>
                    <div class="search-selectbox custom-days-selectbox">
                        <select class="form-control" id="lastNumberDays">
                            <option value="0"> {{ trans('common.all') }}</option>

                            @foreach ($lastNumberDayOptions as $key => $lastNumberDayOption)
                                @if($lastNumberDay == $key)
                                    <option value="{{ $key }}" selected> {{ $lastNumberDayOption }}</option>
                                @else
                                    <option value="{{ $key }}"> {{ $lastNumberDayOption }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
        @include('frontend.flash')
        <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="portlet light bordered">
                        <div class="portlet-body">
                            <div class="jobs-list">
                                @if(isset($projects) && count($projects) > 0)
                                    @foreach($projects as $projectInfo)
                                        {{--                                        @if($projectInfo->status == \App\Models\Project::STATUS_PUBLISHED)--}}
                                        <div class="row jobs-main-div">
                                            <div class="col-md-12 jobs-main-div-title">
                                                <div class="row">
                                                    <div class="col-md-8 project-name">
                                                        <h4>{{$projectInfo->name}}</h4>
                                                        <div class="project-type">
                                                            @if($projectInfo->pay_type == \App\Constants::PAY_TYPE_HOURLY_PAY)
                                                                {{ trans('common.pay_hourly') }}
                                                                <span> {{ $projectInfo->reference_price }}</span>
                                                            @elseif($projectInfo->pay_type == \App\Constants::PAY_TYPE_FIXED_PRICE)
                                                                {{ trans('common.fixed_price') }}
                                                                <span> {{ $projectInfo->reference_price }}</span>
                                                            @elseif($projectInfo->pay_type == \App\Constants::PAY_TYPE_MAKE_OFFER)
                                                                {{ trans('common.request_quotation') }}
                                                            @endif
                                                        </div>
                                                        <div class="project-type">
                                                            {{ trans('common.project_type') }}
                                                            <span>{{ trans('common.public') }}</span>
                                                        </div>
                                                        <div class="is_read">
                                                            @if( $projectInfo->is_read == 0 &&
                                                                $projectInfo->user_id ==\Auth::guard('users')->user()->id )
                                                                <i class="fa fa-circle"></i>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="project-count">
                                                            <div class="project-count-sub">
                                                                {{ trans('common.proposals') }} <br>
                                                                <span>
                                                                {{ count($projectInfo->applicants) }}
                                                            </span>
                                                            </div>
                                                            <div class="project-count-sub">
                                                                {{ trans('common.interview') }} <br>
                                                                <span>
                                                                {{ count($projectInfo->projectChatRoom->chatFollower) }}
                                                            </span>
                                                            </div>
                                                            <div class="project-count-sub">
                                                                {{ trans('common.hired') }} <br>
                                                                <span>
                                                                {{ count($projectInfo->awardedApplicant) }}
                                                            </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 jobs-main-div-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a href="{{ route('frontend.joindiscussion.getprojectapplicant', ['slug' => $projectInfo->name, 'id' => $projectInfo->id ]) }}#sec-chat">
                                                            <button class="btn btn-color-custom">
                                                                {{ trans('common.join_discuss_room')}}
                                                            </button>
                                                        </a>

                                                        @if( $projectInfo->status == \App\Models\Project::STATUS_PUBLISHED)
                                                            <a href="{{ route('frontend.mypublishorder.deleteproject', ['id' => $projectInfo->id ]) }}#sec-common-project">
                                                                <button class="btn btn-color-non-custom">
                                                                    {{ trans('common.remove_post')}}
                                                                </button>
                                                            </a>

                                                            <a href="{{ route('frontend.mypublishorder.editproject', ['id' => $projectInfo->id ]) }}#sec-common-project">
                                                                <button class="btn btn-color-non-custom">
                                                                    {{trans('common.edit_post')}}
                                                                </button>
                                                            </a>
                                                        @endif

                                                    orderDetail    @if(isset($projectInfo->awardedApplicant->status) && $projectInfo->awardedApplicant->status == 3 && $projectInfo->status != 4)

<a href="{{ route('frontend.mypublishorder.orderDetail', ['id' => $projectInfo->id, 'userId' => $projectInfo->id]) }}#sec-common-project">
                                                                <button class="btn btn-color-non-custom">
                                                                    {{trans('common.order_detail')}}
                                                                </button>
                                                            </a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{--@endif--}}
                                    @endforeach
                                @else
                                    <div class="text-center" style="padding:250px 0px">
                                        {{ trans('common.no_publish_project') }}
                                        <br/>
                                        <img src="\images\no-publish-project-icon.jpg">
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
        @include('frontend.includes.modalCreateExtraSize')
        @include('frontend.includes.modalEvaluationOrder')
    </div>
    <!-- END CONTENT -->
@endsection

@section('footer')
    <script src="/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-star-rating/js/star-rating.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js" type="text/javascript"></script>
    <script src="/custom/js/frontend/myPublishOrder.js" type="text/javascript"></script>
    <script>
        function () {
            $(document).ready(function () {
                $('#lastNumberDays').on('change', function (e) {
                    var optionSelected = $("option:selected", this);
                    var valueSelected = this.value;

                    var url = "{{ route('frontend.mypublishorder') }}" + "?status=" + "{{ $status }}" + "&lastNumberDays=" + valueSelected;
                    window.location.href = url;
                });
            });
        }(jQuery);
    </script>
@endsection




