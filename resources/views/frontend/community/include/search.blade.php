<div class="col-md-12">
    <div class="form-group-custom">
        <div class="set-input-search">
            <i class="setFasearch fa fa-search" aria-hidden="true"></i>
            <input type="text" name="search" placeholder="Search the community or ask a question" class="customSearch form-control searchTxtI" />
        </div>
    </div>
</div>


@push('js')
    <style>
        .ui-menu .ui-menu-item {
            margin: 0;
            cursor: pointer;
            list-style-image: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7);
        }
        .ui-widget.ui-widget-content {
            margin-top: 1px !important;
            border-bottom-left-radius: 15px;
            border-bottom-right-radius: 15px;
        }
        .ui-menu-item {
            margin-top: -3px !important;
        }
        .ui-state-active , .ui-button:active:hover
        {
            border:1px solid #5CC22F !important;
            background: #5CC22F !important;
            color:#fff !important;
        }
        .ui-menu .ui-menu-item-wrapper {
            position: relative;
            margin-right:0px !important;
            margin-left:0px !important;
            padding: 8px 0em 9px 0.4em !important;
        }
        .ui-menu .ui-menu-item-wrapper:hover{
            position: relative;
            margin-right: 0px !important;
            margin-left: 0px !important;
            padding: 8px 0em 9px 0.4em !important;
        }
        .setfacolor{
            color: #5CC22F !important;
        }
    </style>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
    <script>
        $(function () {

            $('.customStylecommunity').click(function () {
                $('.searchTxtI').css('border-bottom-left-radius','30px');
                $('.searchTxtI').css('border-bottom-right-radius','30px');
                $('.searchTxtI').css('border-bottom','1px solid #c2cad8');
            });

            $( ".searchTxtI" ).on( "autocompleteresponse", function( event, ui ) {
                console.log('responseeeee');
                $('.sloader').hide();
                $('.sicon').show()
                $('.searchTxtI').css('border-bottom-left-radius','0px');
                $('.searchTxtI').css('border-bottom-right-radius','0px');
                $('.searchTxtI').css('border-bottom','0px');
            } );

            function detectMob() {
                const toMatch = [
                    /Android/i,
                    /webOS/i,
                    /iPhone/i,
                    /iPad/i,
                    /iPod/i,
                    /BlackBerry/i,
                    /Windows Phone/i
                ];

                return toMatch.some((toMatchItem) => {
                    return navigator.userAgent.match(toMatchItem);
                });
            }

            function changeAllAutoComplete() {
                var listMenu = $('.ui-menu.ui-widget.ui-widget-content.ui-autocomplete.ui-front')
                if (detectMob()) {
                    $.each(listMenu, function(indexMenu, menu) {
                        var getUlStyle = $(menu).attr('style');
                        if (getUlStyle != '') {
                            splitedUl = getUlStyle.split(';');
                            appendedCss = [];
                            $.each(splitedUl, (index, el) => {
                                if (el.length > 0 && el.indexOf('important') === -1) {
                                    console.log(el)
                                    appendedCss.push(el + ' !important');
                                }
                            });

                            $(menu).attr('style', appendedCss.join(';'));
                        }
                    });
                }
            }

            function hideAllAutoComplete() {
                var listMenu = $('.ui-menu.ui-widget.ui-widget-content.ui-autocomplete.ui-front')
                if (detectMob()) {
                    $.each(listMenu, function(indexMenu, menu) {
                        $(menu).hide();
                    });
                }
            }

            $(".searchTxtI").autocomplete({
                source: function (request, response) {
                    jQuery.get("/api/search-community/", {
                        search: request.term
                    }, function (data) {
                        response(data['data']);
                        changeAllAutoComplete();
                    });
                },
                minLength: 1,
                search: function(){
                    console.log('search event');
                    hideAllAutoComplete();
                    $('.sloader').show();
                    $('.sicon').hide()
                },
                response: function( event, ui ) {
                    console.log('response recieved');
                },
                select: function(event, ui) {
                    console.log('data');
                    console.log(ui);
                    console.log('data');
                    window.open(ui.item.route);
                    $('.searchTxtI').css('border-bottom-left-radius','30px');
                    $('.searchTxtI').css('border-bottom-right-radius','30px');
                    $('.searchTxtI').css('border-bottom','1px solid #c2cad8');
                }
            }).autocomplete("instance")._renderItem = function(ul, item) {
                var item = $('<div class="row">' +
                    '<div class="col-md-1 '+item.icon+' "></div>' +
                    '<div class="col-md-11 description">' + item.label +'</div>' +
                    '</div>')
                return $("<li>").append(item).appendTo(ul);
            };
        });
    </script>
@endpush
