@extends('frontend.layouts.default')

@section('title')
    Xenren Community
@endsection

@section('keywords')
    Community Home
@endsection

@section('description')
    Xenren Community Home
@endsection

@section('author')
    Xenren Community
@endsection

@section('url')
    https://www.xenren.co/Blog
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
    <link href="{{asset('css/community.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .communiyt-Xenren {
            position: absolute;
            right: 20px;
            z-index: 0;
            max-width: 70%;
            bottom: 0;
            width: 32%;
        }
        .customStylecommunity{
            background-color: #e5fdd9;
            color: #fff;
            position: relative;
            z-index: 1;
            padding: 48px 0 70px;
            min-height: 350px;
            margin-bottom: 0 !important;
        }
        .inner-community-text{
            width: 100%;
            display: inline-block;
        }
        .setWelcomeText span{
            color:#010101 !important;
        }

        .setWelcomeText{
            padding-left:6px;
            font-size: 35px;
            line-height: 50px;
            color: #fff;
            font-weight: bold;
            margin-bottom: 20px;
        }
        .customSet-TextArea{
            width: 95%;
            margin: 0 auto;
        }
        .setXenRenText{
            color:#5ec329 !important
        }
        .setSearchBox{
            width: 95%;
            margin: 0 auto;
            display: flex;
            flex-direction: row-reverse;
            position: relative;
            align-items: center;
        }
        .setCustomSearch{
            z-index: 99999;
            position: relative;
            height: 45px;
            padding-left: 45px;
            border-radius: 30px;
        }
        .setFasearch{
            z-index: 999999;
            left: 10px;
            top: 7px;
            width: 30px;
            height: 30px;
            background: #5bbc2e;
            padding: 8px;
            border-radius: 25px;
            color: #fff;
            font-size: 13px;
        }
        .setLinewithText{
            position: absolute;
            left: 0;
            bottom: 30px;
            color: #555;
            height: 52px;
            border-radius: 0 4px 0 0;
            padding: 0px 0;
            z-index: 0;
        }
        .customUl{
            list-style: none;
            float: left;
            display: flex;
            padding-left: 12px;
        }
        .customUlLI{
            display: flex;
            flex-direction: column;
            padding:7px 30px 4px 40px;
            justify-content: left;
        }
        .customUlLISpan1{
            font-size: 14px;
            font-family: auto !important;
            color: #000000;
        }
        .customUlLISpan2{
            font-size: 14px;
            color: #000000;
        }
        .setCustomImage{
            width: 30px;
            height: 30px;
            position: relative;
            top: 10px;
            left: 26px;
            background: #29343a;
        }
        .setCustomImage1{
            border-radius: 20px;
            padding: 0px 2px 0px 0px;
        }
        .setCustomImage2{
            padding: 6px;
            border-radius: 20px;
        }
        .setCustomImage3{
            padding: 0px 0px 3px 3px;
            border-radius: 20px;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .new-ToCommunity{

        }
        .new-ToCommunity-p1{
            font-size: 14px;
        }
        .new-ToCommunity-p2{
            font-size: 14px;
        }
        .btnSuccessLC{
            border-radius: 6px;
            font-size: 13px;
            background: #5bbc2e;
            width: 150px;
            padding-bottom: 10px;
            padding-top: 10px;
            box-shadow: 1px 1px 6px 4px #efefef !important;
            margin-right: 15px;
        }
        .btnSuccessLC:hover{
            border-radius: 6px;
            font-size: 13px;
            background: #5bbc2e;
            width: 150px;
            padding-bottom: 10px;
            padding-top: 10px;
            box-shadow: 1px 1px 6px 4px #efefef !important;
            margin-right: 15px;
        }
        .btnSuccessRC{
            border-radius: 6px;
            font-size: 13px;
            background: #fff;
            color: #5bbc2e;
            width: 150px;
            padding-bottom: 10px;
            padding-top: 10px;
            box-shadow: 1px 1px 6px 4px #efefef !important;
        }
        .btnSuccessRC:hover{
            border-radius: 6px;
            font-size: 13px;
            background: #fff;
            color: #5bbc2e;
            padding-bottom: 10px;
            padding-top: 10px;
            width: 150px;
            box-shadow: 1px 1px 6px 4px #efefef !important;
        }
        .setNewText{
            font-size: 35px;
            font-weight: bold;
        }

        audio, canvas, progress, video{
            border-radius: 12px !important;
        }

        .set-start-converstion-text{
            font-size: 35px;
            line-height: 26px;
            color: #222222;
            margin-bottom: 10px;
            font-weight: bold;
        }

        .inside-board{

        }

        .start-converstion{

        }

        .xen-desc{
            font-size: 14px;
            line-height: 21px;
        }

        .xenren-board{
            box-shadow: 0 1px 6px rgba(57,73,76,0.35);
            border-top-left-radius: 12px;
            border-top-right-radius: 12px;
            padding:0px !important;
        }

        .xenren-head-f{
            font-size: 17px;
            font-weight: 500;
        }

        .customPadd{
            padding-right: 0px;
            padding-left: 0px;
        }

        .xen-board-body{
            padding: 15px;
            background: #fff;
        }

        .xen-board-head{
            background: #27313d;
            padding: 15px;
            color: #fff;
            border-top-right-radius: 12px;
            border-top-left-radius: 12px;
        }

        .ulLi-xenren{
            padding-left: 0px;
        }

        .li-xenren-community{
            list-style: none;
            border-bottom: 1px solid #efefef;
        }

        .h2-xenren {
            font-size: 20px;
            color: #27313d;
            cursor: pointer;
            font-weight: 500;
        }
        .centerVideo{
            display: flex;
            justify-content: center;
            margin-top: 20px;
            margin-bottom: 25px;
        }
        .setBgBG{
            background: #fff;
            padding: 40px;
            padding-top: 10px;
        }
        .why-we-use-it{
            font-size: 15px;
            color: #222222;
            font-weight: 600;
        }
        .ui-menu .ui-menu-item {
            margin: 0;
            cursor: pointer;
            list-style-image: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7);
        }
        .ui-widget.ui-widget-content {
            margin-top: 1px !important;
            border-bottom-left-radius: 15px;
            border-bottom-right-radius: 15px;
        }
        .ui-menu-item {
            margin-top: -3px !important;
        }
        .ui-state-active , .ui-button:active:hover
        {
            background: #5CC22F !important;
            color:#fff !important;
        }
        .setfacolor{
            color: #5CC22F !important;
        }
        .ui-menu .ui-menu-item-wrapper {
            position: relative;
            margin-right:0px !important;
            margin-left:0px !important;
            padding: 8px 0em 9px 0.4em !important;
        }
        .ui-menu .ui-menu-item-wrapper:hover{
            position: relative;
            margin-right: 0px !important;
            margin-left: 0px !important;
            padding: 8px 0em 9px 0.4em !important;
        }
        .pp23{
            display: none;
        }
        .pp12 {
            display: none;
        }
        .pp22 {
            display: none;
        }
        @media (max-width: 991px) {
            .setCustomImage{
                width: 32px;
                height: 32px;
                position: relative;
                top: 10px;
                left: 26px;
                background: #29343a;
            }
            .btnbtn-div{
                display: flex;
                justify-content: space-around;
                margin-bottom: 25px;
                margin-top: 25px;
            }
            #homePageVideo{
                width: 90% !important;
            }
        }

        @media (max-width: 698px) {
            .page-header-fixed .page-container {
                padding-top: 30px;
            }
            .customUl{
                display:none;
            }
            .set-span-flex-p{
                position: relative;
                left: 40px;
                display: flex;
                flex-direction: column;
            }
            .setCustomImage{
                top:0px;
            }
            .pp12{
                display: flex;
                align-items: center;
                height: 50px;
                margin-top: 26px;
                margin-bottom: 15px;
            }
            .pp22{
                margin-bottom: 15px;
                display: flex;
                align-items: center;
                height: 50px;
                margin-top: 0px;
            }
            .pp23{
                margin-bottom:15px;
                margin-top: 0px;
                display: flex;
                align-items: center;
                height: 50px;
            }
            .customStylecommunity {
                background-color: #e5fdd9;
                color: #fff;
                position: relative;
                z-index: 1;
                padding: 48px 0 70px;
                min-height: 440px;
                margin-bottom: 0 !important;
            }
            .communiyt-Xenren {
                position: absolute;
                right: 20px;
                z-index: 0;
                max-width: 70%;
                bottom: 0;
                width: 56%;
            }
            .setLinewithText{
                 position: relative;
                left: 0;
                bottom: 30px;
                color: #555;
                height: 52px;
                border-radius: 0 4px 0 0;
                padding: 0px 0;
                z-index: 0;
            }
        }
        @media (max-width: 585px) and (min-width: 331px) {
            .communiyt-Xenren {
                position: absolute;
                right: 20px;
                z-index: 0;
                max-width: 70%;
                bottom: 0;
                width: 58%;
            }
            .communiyt-Xenren {
                position: absolute;
                right: 5px;
                z-index: 0;
                max-width: 70%;
                bottom: 0;
                width: 61%;
            }
            #homePageVideo {
                width: 100% !important;
            }
            .setWelcomeText span {
                color: #010101 !important;
                font-size: 20px;
            }
            .setXenRenText {
                color: #5ec329 !important;
                font-size: 28px;
                position: relative;
                bottom: 10px;
            }
            .setWelcomeText {
                padding-left: 6px;
                font-size: 35px;
                line-height: 50px;
                color: #fff;
                font-weight: bold;
                margin-bottom: 8px;
                margin-top:0px;
            }
            .setNewText {
                font-size: 22px;
                font-weight: bold;
            }
            .set-start-converstion-text {
                font-size: 22px;
                line-height: 26px;
                color: #222222;
                margin-bottom: 10px;
                font-weight: bold;
            }
            .pp12 {
                display: flex;
                align-items: center;
                height: 50px;
                margin-top: 26px;
                margin-bottom: 0px;
            }
            .pp22 {
                margin-bottom: 0px;
                display: flex;
                align-items: center;
                height: 50px;
                margin-top: 0px;
            }
            .customStylecommunity {
                background-color: #e5fdd9;
                color: #fff;
                position: relative;
                z-index: 1;
                padding: 48px 0 70px;
                min-height: 365px;
                margin-bottom: 0 !important;
            }
            .setBgBG {
                background: #fff;
                padding: 40px;
                padding-top: 10px;
                padding-left: 15px;
                padding-right: 15px;
            }
            .xenren-board{
                width: 100%;
                padding: 0px;
            }
        }
        @media (max-width:330px) {
            .setWelcomeText {
                padding-left: 6px;
                font-size: 35px;
                line-height: 50px;
                color: #fff;
                font-weight: bold;
                margin-bottom: 12px;
                margin-top: 0px;
            }
            .communiyt-Xenren {
                position: absolute;
                right: 0px;
                z-index: 0;
                max-width: 70%;
                bottom: 0;
                width: 58%;
            }
            .setXenRenText {
                color: #5ec329 !important;
                font-size: 25px;
                position: relative;
                bottom: 20px;
            }
            .setWelcomeText span {
                color: #010101 !important;
                font-size: 20px;
            }
            .customStylecommunity {
                background-color: #e5fdd9;
                color: #fff;
                position: relative;
                z-index: 1;
                padding: 48px 0 70px;
                min-height: 330px;
                margin-bottom: 0 !important;
            }
            .xenren-board {
                box-shadow: 0 1px 6px rgba(57,73,76,0.35);
                border-top-left-radius: 12px;
                border-top-right-radius: 12px;
                padding: 0px;
            }
            .setBgBG {
                background: #fff;
                padding: 40px;
                padding-top: 10px;
                padding-left: 15px;
                padding-right: 15px;
            }
            .set-start-converstion-text {
                font-size: 20px;
                line-height: 26px;
                color: #222222;
                margin-bottom: 10px;
                font-weight: bold;
            }
            .setNewText {
                font-size: 20px;
                font-weight: bold;
            }
            .pp23{
                height: 40px;
            }
            .pp12 {
                display: flex;
                align-items: center;
                height: 40px;
                margin-top: 14px;
                margin-bottom: 5px;
            }
            .pp22 {
                margin-bottom: 5px;
                display: flex;
                align-items: center;
                height: 40px;
                margin-top: 0px;
            }
        }
        .fafaSearch {
            top: 33px;
        }
    </style>
{{--    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">--}}
@endsection

@section('content')
    <div class="row customStylecommunity">
        <div class="setSearchBox">
            <input type="search" placeholder="Search or type your question here" name="search_data" class="setCustomSearch form-control searchTxt" />
            <i class="setFasearch fa fa-search" aria-hidden="true"></i>
        </div>
        <div class="customSet-TextArea">
            <div class="inner-community-text">
                <h2 class="setWelcomeText"><span>Welcome @if(\Auth::user()) {{\Auth::user()->name }} @else guest @endif,to the </span><br><b class="setXenRenText">XenRen Community!</b></h2>
            </div>
        </div>
        <img class="communiyt-Xenren" src="{{asset('images/togehtor2.png')}}">
        <div class="setLinewithText">
            <ul class="customUl">
                <img class="setCustomImage1 setCustomImage"
                     src="{{asset('images/userss.png')}}" />
                <li class="customUlLI">
                    <span class="customUlLISpan1">569908</span>
                    <span class="customUlLISpan2">Members</span>
                </li>
                <img class="setCustomImage2 setCustomImage"
                     src="{{asset('images/worldIcon.png')}}" />
                <li class="customUlLI">
                    <span class="customUlLISpan1">355</span>
                    <span class="customUlLISpan2">Online</span>
                </li>
                <img class="setCustomImage3 setCustomImage"
                     src="{{asset('images/topicsC.png')}}" />
                <li class="customUlLI">
                    <span class="customUlLISpan1">109685</span>
                    <span class="customUlLISpan2">Topics</span>
                </li>
            </ul>
            <p class="pp12">
                <img class="setCustomImage1 setCustomImage"
                    src="{{asset('images/userss.png')}}" />
                <span class="set-span-flex-p">
                    <span class="customUlLISpan1">569908</span>
                    <span class="customUlLISpan2">Members</span>
                </span>
            </p>
            <p class="pp22">
                <img class="setCustomImage2 setCustomImage"
                     src="{{asset('images/worldIcon.png')}}" />
                <span class="set-span-flex-p">
                    <span class="customUlLISpan1">323</span>
                    <span class="customUlLISpan2">Online</span>
                </span>
            </p>
            <p class="pp23">
                <img class="setCustomImage3 setCustomImage"
                     src="{{asset('images/topicsC.png')}}" />
                <span class="set-span-flex-p">
                    <span class="customUlLISpan1">109685</span>
                    <span class="customUlLISpan2">Topics</span>
                </span>
            </p>
        </div>
    </div>
    <div class="row setBgBG">
        <div class="row">
            <div class="col-md-12">
                <div class="new-ToCommunity">
                    <h2 class="setNewText">New to our community?</h2>
                    <p class="new-ToCommunity-p1">As a global platform for collaboration and professionalism, the Xenren Community forums are where customers can engage with each other and Xenren representatives. In the forums professionals  can ask and answer questions, search topics, learn, socialize and get updates about Xenren. The Community is also where members will share input on existing features and suggestions for new features. If customers have any ideas on how we can improve their experience on Xenren, the Community is a great place to share that feedback.</p>
                    <p class="new-ToCommunity-p2">You can explore the community and search for solutions without joining. But if you want to post or receive email notifications, you'll need to be registered on Xenren.</p>
                    <div class="btnbtn-div">
                        @if(!\Auth::user())
                            <a class="btnSuccessLC btn btn-success" href="{{url(\Session::get('lang').'/login')}}">
                                <span>Login</span>
                            </a>
                            <a class="btnSuccessRC btn btn-success" href="{{url(\Session::get('lang').'/register')}}">
                                <span>Register</span>
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="centerVideo">
                    <video width="50%" height="100%" controls="" autoplay="" id="homePageVideo">
                        <source src="{{asset('video/xenren_home.mp4')}}" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                </div>
            </div>
        </div>
        <div class="start-converstion row">
            <div class="col-md-12">
                <div class="inside-board">
                    <h2 class="set-start-converstion-text">Start a Conversation</h2>
                    <p class="xen-desc">The Xenren Community is here as a place for interesting, thoughtful and helpful conversation. With active participation, the Community can be a great way to make friends, share information, and gain a more engaging experience on Xenren.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="inside-board">
                    <h2 class="why-we-use-it">Why do we use it?</h2>
                    <p class="xen-desc">The Xenren Community is here as a place for interesting, thoughtful and helpful conversation. With active participation, the Community can be a great way to make friends, share information, and gain a more engaging experience on Xenren.</p>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="xenren-board container">
        <div class="customPadd col-md-12">
            <div class="xen-board-head">
                <h5 class="xenren-head-f">Xenren Offical</h5>
            </div>
        </div>
        <div class="customPadd col-md-12">
            <div class="xen-board-body">
                <p>Stay up to date on all things Xenren by visiting our Announcements area. Check out the Content Corner for tips and best practices from other professionals. Visit Community Basics to learn how to use the forums.</p>
                <ul class="ulLi-xenren">
                    @foreach($topics as $topic)
                        @if($topic->topic_type == 1)
                        <li class="li-xenren-community">
                            <h2 class="h2-xenren" onclick="window.location.href='{{route('frontend.XenrenOffical',[\Session::get('lang'),cleanLink($topic->topic_name),$topic->id])}}'">{{$topic->topic_name}}</h2>
                            <p>{{$topic->topic_description}}</p>
                        </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <br>
    <div class="xenren-board container">
        <div class="customPadd col-md-12">
            <div class="xen-board-head">
                <h5 class="xenren-head-f">Community Discussions</h5>
            </div>
        </div>
        <div class="customPadd col-md-12">
            <div class="xen-board-body">
                <p>Start a discussion related to freelancing or hiring, Xenren features, policies and questions related to the Xenren platform. Or, have some fun in the Coffee Break area.</p>
                <ul class="ulLi-xenren">
                    @foreach($topics as $topic)
                        @if($topic->topic_type == 2)
                            <li class="li-xenren-community">
                                <h2 class="h2-xenren" onclick="window.location.href='{{route('frontend.CommunityDiscussion',[\Session::get('lang'),cleanLink($topic->topic_name),$topic->id])}}'" >{{$topic->topic_name}}</h2>
                                <p>{{$topic->topic_description}}</p>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <br>
    <br>
@endsection

@push('js')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
<script>
$(function () {

    $('.customStylecommunity').click(function () {
        $('.searchTxt').css('border-bottom-left-radius','30px');
        $('.searchTxt').css('border-bottom-right-radius','30px');
        $('.searchTxt').css('border-bottom','1px solid #c2cad8');
    });

    $( ".searchTxt" ).on( "autocompleteresponse", function( event, ui ) {
        console.log('responseeeee');
        $('.sloader').hide();
        $('.sicon').show()
    } );

    function detectMob() {
        const toMatch = [
            /Android/i,
            /webOS/i,
            /iPhone/i,
            /iPad/i,
            /iPod/i,
            /BlackBerry/i,
            /Windows Phone/i
        ];

        return toMatch.some((toMatchItem) => {
            return navigator.userAgent.match(toMatchItem);
        });
    }

    function changeAllAutoComplete() {
        var listMenu = $('.ui-menu.ui-widget.ui-widget-content.ui-autocomplete.ui-front')
        if (detectMob()) {
            $.each(listMenu, function(indexMenu, menu) {
                var getUlStyle = $(menu).attr('style');
                if (getUlStyle != '') {
                    splitedUl = getUlStyle.split(';');
                    appendedCss = [];
                    $.each(splitedUl, (index, el) => {
                        if (el.length > 0 && el.indexOf('important') === -1) {
                            console.log(el)
                            appendedCss.push(el + ' !important');
                        }
                    });

                    $(menu).attr('style', appendedCss.join(';'));
                }
            });
        }
    }

    function hideAllAutoComplete() {
        var listMenu = $('.ui-menu.ui-widget.ui-widget-content.ui-autocomplete.ui-front')
        if (detectMob()) {
            $.each(listMenu, function(indexMenu, menu) {
                $(menu).hide();
            });
        }
    }

    $(".searchTxt").autocomplete({
        source: function (request, response) {
            jQuery.get("/api/search-community/", {
                search: request.term
            }, function (data) {
                response(data['data']);
                changeAllAutoComplete();
            });
        },
        minLength: 1,
        search: function(){
            console.log('search event');
            hideAllAutoComplete();
            $('.sloader').show();
            $('.sicon').hide();
            $('.searchTxt').css('border-bottom-left-radius','0px');
            $('.searchTxt').css('border-bottom-right-radius','0px');
            $('.searchTxt').css('border-bottom','0px');
        },
        response: function( event, ui ) {
            console.log('response recieved');
        },
        select: function(event, ui) {
            console.log('data');
            console.log(ui);
            console.log('data');
            window.open(ui.item.route);
            $('.searchTxt').css('border-bottom-left-radius','30px');
            $('.searchTxt').css('border-bottom-right-radius','30px');
            $('.searchTxt').css('border-bottom','1px solid #c2cad8');
        }
    }).autocomplete("instance")._renderItem = function(ul, item) {
        console.log('item');
        console.log(item);
        var item = $('<div class="row">' +
            '<div class="col-md-1 '+ item.icon +'"></div>' +
            '<div class="col-md-11 description">' + item.label +'</div>' +
            '</div>')
        return $("<li>").append(item).appendTo(ul);
    };
});
</script>
@endpush
