@extends('frontend.layouts.default')

@section('title')
    Xenren Community Discussion
@endsection

@section('keywords')
    Community Discussion
@endsection

@section('description')
    Xenren Community Discussion
@endsection

@section('author')
    Xenren Community Discussion
@endsection

@section('url')
    https://www.xenren.co/{{$lang}}/community/Community-Discussion/{{$topic_name}}
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
    <style>
        .customStylecommunity{
            background-color: #fff;
            color: #fff;
            position: relative;
            z-index: 1;
            padding: 30px;
            min-height: 350px;
            margin-bottom: 0 !important;
            box-shadow: 0px 0px 10px -2px #555;
            border-radius: 5px;
        }
        .set-backslash{
            color: #b2b2b2;
            padding-right: 10px;
        }
        .bredcrums-xenren a {
            color: #b2b2b2;
            padding-right: 10px;
        }
        .customSearch{
            z-index: 99999;
            position: relative;
            height: 45px;
            padding-left: 45px;
            border-radius: 30px;
        }
        .set-input-search{
            position: relative;
        }
        .setFasearch{
            display: flex !important;
            align-items: center;
            position: absolute;
            z-index: 999999;
            top: 7px;
            left: 10px;
            width: 30px;
            height: 30px;
            background: #5bbc2e;
            padding: 8px;
            border-radius: 25px;
            color: #fff;
            font-size: 13px;
        }
        .category-pagination-xenren{
            padding-bottom: 10px;
            color: #5ec329;
            font-size: 13px;
            position: relative;
            float: right;
        }
        .xenren-pagination-link-1{
            color: #313e44;
            font-size: 13px;
            margin-right: 20px;
        }
        .xenren-pagination-link-1:before {
            display: inline-block;
            font: normal normal normal 13px/1 FontAwesome;
            font-size: 13px;
            font-size: inherit;
            -moz-osx-font-smoothing: grayscale;
            -webkit-font-smoothing: antialiased;
            text-rendering: auto;
            transform: translate(0, 0);
            content: "\f104";
            margin-right: 4px;
        }
        .xenren-pagination-link-1:hover{
            color: #5ec329;
            font-size: 13px;
        }
        .xenren-pagination-link:after{
            display: inline-block;
            font: normal normal normal 13px/1 FontAwesome;
            font-size: 13px;
            font-size: inherit;
            -moz-osx-font-smoothing: grayscale;
            -webkit-font-smoothing: antialiased;
            text-rendering: auto;
            transform: translate(0, 0);
            content: "\f105";
            margin-left: 4px;
        }
        .xenren-pagination-link{
            color: #fff;
            font-size: 13px;
            background: #27313d;
            padding: 9px 15px 9px 15px;
            border-radius: 12px;
        }
        .xenren-pagination-link:hover{
            color: #5ec329;
            font-size: 13px;
        }
        .form-group-custom{
            margin-top: 20px;
            margin-bottom: 0px;
        }
        .topic_name{
            color: #555;
            font-size: 26px;
            font-weight: bold;
        }
        .topic_name1{
            color: #555;
            font-size: 16px;
            font-weight: bold;
        }
        .customSetBtn{
            float: right;
        }
        .customBtnColor:hover{
            background: #27313d;
            color: #fff;
            width: 130px;
            height: 45px;
            border-radius: 10px;
        }
        .customBtnColor{
            background: #27313d;
            color: #fff;
            width: 130px;
            height: 45px;
            border-radius: 10px;
        }
        .xen-menu-option{
            background-color: transparent !important;
            border-color: transparent !important;
            font-size: 0;
            height: 30px;
            width: 30px;
            background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
            border: medium none;
            border-radius: 4px;
            color: #3e3e3e;
            cursor: pointer;
            display: inline-block;
            line-height: 26px;
            margin-bottom: 0;
            min-width: 0;
            padding: 6px 9px;
            text-align: center;
            touch-action: manipulation;
            vertical-align: middle;
            white-space: nowrap;
        }
        .xen-menu-option:before {
            background: rgba(0, 0, 0, 0) url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiPjxwYXRoIGQ9Ik0xMiAxNmMxLjY1NiAwIDMgMS4zNDQgMyAzcy0xLjM0NCAzLTMgMy0zLTEuMzQ0LTMtMyAxLjM0NC0zIDMtM3ptMCAxYzEuMTA0IDAgMiAuODk2IDIgMnMtLjg5NiAyLTIgMi0yLS44OTYtMi0yIC44OTYtMiAyLTJ6bTAtOGMxLjY1NiAwIDMgMS4zNDQgMyAzcy0xLjM0NCAzLTMgMy0zLTEuMzQ0LTMtMyAxLjM0NC0zIDMtM3ptMCAxYzEuMTA0IDAgMiAuODk2IDIgMnMtLjg5NiAyLTIgMi0yLS44OTYtMi0yIC44OTYtMiAyLTJ6bTAtOGMxLjY1NiAwIDMgMS4zNDQgMyAzcy0xLjM0NCAzLTMgMy0zLTEuMzQ0LTMtMyAxLjM0NC0zIDMtM3ptMCAxYzEuMTA0IDAgMiAuODk2IDIgMnMtLjg5NiAyLTIgMi0yLS44OTYtMi0yIC44OTYtMiAyLTJ6Ii8+PC9zdmc+) no-repeat scroll center center / 18px 18px !important;
            content: "";
            display: block;
            height: 100%;
            padding: 9px 6px !important;
            vertical-align: middle;
            width: 100%;
        }
        .customSetBorderBottom{
            border-bottom: 2px solid #efefef;
        }
        .customSetBorderBottom2{
            padding-bottom: 20px;
            border-bottom: 2px solid #efefef;
        }
        .pColorDiscussion{
            color: #555;
        }
        p{
            color: black;
            font-size: 14px;
            font-family: 'Myriad Pro', 'Raleway', sans-serif !important;
        }
        span{
            font-size: 14px !important;
            font-family: 'Myriad Pro', 'Raleway', sans-serif !important;
        }
        .imageAvatar{
            width: 60px;
            height: 60px;
            position: relative;
            top: 15px;
        }
        .image_avatar{
            padding-top: 20px;
            display: flex;
        }
        .set-flex-direaction{
            flex-direction: column;
            display: flex;
            width: 100%;
            padding-left: 20px;
        }
        .setUsernameColor{
            color: #5ec329;
            font-weight: bold;
            text-transform: capitalize;
        }
        .colorp222{
            color: gray;
        }
        .setTimeColor{
            color: gray;
            font-family: sans-serif !important;
        }
        .bgColorCustom{
            cursor: pointer;
            border-bottom: 1px solid #c0c0c0;
        }
        .bgColorCustom:nth-last-child(1){
            cursor: pointer;
            border-bottom: 0px solid #c0c0c0;
        }
        .setSpanColor{
            color:#555;
            font-weight: bold;
        }
        .setSpanColor:hover{
            color:#5ec329;
            font-weight: bold;
        }
        .set-box-drop-down{
            background: #fff;
            width: 160px;
            padding: 0px;
            border: 1px solid #555;
            position: absolute;
            margin-top: 20px;
            z-index: 9999;
        }
        .custom-set-ul{
            padding-left: 0px;
        }
        .custom-set-li:hover{
            color: #57a224;
        }
        .custom-set-li{
            list-style: none;
            color: #555;
            font-size: 12px;
            padding-top: 15px;
            padding: 12px;
            cursor: pointer;
            border-bottom: 1px solid #555;
        }
        .custom-set-li:nth-last-child(4){
            border-bottom: 0px solid #555;
            padding-bottom: 5px;
        }
        .custom-set-li:nth-last-child(6){
            border-bottom: 0px solid #555;
            padding-bottom: 5px;
        }
        .custom-set-li:nth-last-child(1){
            border-bottom: 0px solid #555;
            padding-bottom: 5px;
        }
        .setBookMarkIcon{
            position: absolute;
            right: 100px;
        }
        .spanTotalReplies{
            color:#808080;
            font-weight: 500;
            padding-left: 15px;
        }
        .total-replies{
            margin-bottom: 15px;
        }
    </style>
@endsection

@section('content')
    <div class="row customStylecommunity">
        <div class="col-md-12">
            <div class="bredcrums-xenren">
                <a href="{{route('frontend.Community',[$lang])}}">Xenren Community</a>
                <span class="set-backslash">/</span>
                <a href="">Community Discussion</a>
            </div>
        </div>
        @include('frontend.community.include.search')
        <div class="customSetBorderBottom2 col-md-12">
            <div class="form-group-custom">
                <div class="category-pagination-xenren">
                    <label><a class="xenren-pagination-link-1" href="">Previous</a></label>
                    <label><a class="xenren-pagination-link" href="">Next</a></label>
                </div>
            </div>
        </div>
        <div class="customSetBorderBottom2 col-md-12">
            <div class="form-group-custom">
                <div class="col-md-6">
                    <h2 class="topic_name">
                        {{cleanName($topic_name)}}
                    </h2>
                </div>
                <div class="col-md-6">
                    <div class="customSetBtn">
                        <h2>
                            @if(!\Auth::user())
                                <button disabled class="customBtnColor btn btn-success" onclick="window.location.href='{{ url('/'.$lang.'/login') }}'">New Topics</button>
                                <button type="button" class="menu-opener-xenren xen-menu-option" role="button" href="#">Option</button>
                            @else
                                 <button class="customBtnColor btn btn-success" onclick="window.location.href='{{route('frontend.CommunityForums',[\Session::get('lang'),$topic_name])}}'">New Topics</button>
                                @if(isset($getDiscussion) && count($getDiscussion) > 0)
                                <button type="button" class="menu-opener-xenren xen-menu-option" id="openBox" role="button" href="#">Option</button>
                                <div class="set-box-drop-down" id="opendailogeBox" style="display:none;">
                                    <ul class="custom-set-ul">
                                        <li class="custom-set-li" onclick="window.location.href='{{route('frontend.feature',[$topic_id,'0'])}}'">Mark all as New</li>
                                        <li class="custom-set-li" onclick="window.location.href='{{route('frontend.feature',[$topic_id,'3'])}}'">Mark all as Read</li>
                                        <li class="custom-set-li" onclick="window.location.href='{{route('frontend.feature',[$topic_id,'9'])}}'">Subscribe Rss Feed</li>
                                        <li class="custom-set-li" onclick="window.location.href='{{route('frontend.inviteEmailToFriend',[$lang,$topic_id])}}'">Invite a Friend</li>
                                    </ul>
                                </div>
                                @else
                                <button type="button" disabled class="menu-opener-xenren xen-menu-option" role="button" href="#">Option</button>
                                @endif
                            @endif
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        @foreach($getDiscussion as $discussion)
        @if($discussion->status == \App\Models\CommunityDiscussion::STATUS_START)
            <div class="bgColorCustom col-md-12"
                 @if($read != '' && $read->user_id == \Auth::user()->id &&  $read->read_all == \App\Models\CommunityDiscussion::MARK_AS_ALL_READ)
                 style="background: #efefef"
                 @php
                     $fontWeight = '500';
                 @endphp
                 @else
                 @php
                     $fontWeight = 'bold';
                 @endphp
                 @endif
                 @if($single != '' && $single->user_id == \Auth::user()->id && $discussion->id == $single->d_id && $single->read_as == \App\Models\CommunityRead::STATUS_READ_AS)
                 style="background: #efefef"
                 @php
                     $fontWeight = '500';
                 @endphp
                 @else
                 @php
                     $fontWeight = 'bold';
                 @endphp
                 @endif

            onclick="window.location.href='{{route('frontend.CommunityReply',[\Session::get('lang'),cleanLink($discussion->getTopicName($discussion->topic_id)),cleanLink($discussion->name),$discussion->topic_id,$discussion->id])}}'">
                @if($discussion->book_mark == \App\Models\CommunityDiscussion::BOOKMARK)
                    <img src="/images/book_mark.svg" class="setBookMarkIcon" />
                    <div class="image_avatar">
                    <img  src="/{{$discussion->user->img_avatar}}" class="imageAvatar" onerror="this.src='/images/image.png'" />
                    <div class="set-flex-direaction">
                        <div class="col-md-4">
                            <h2 style="font-weight: {{$fontWeight}}" class="topic_name1">{{$discussion->name}}</h2>
                            <span class="colorp222">Latest Post :
                                <span class="setUsernameColor">{{$discussion->user->name}}</span>
                                .
                                <span class="setTimeColor">{{\Carbon\Carbon::parse($discussion->created_at)->format('m d Y')}}</span>
                            </span>
                        </div>
                        <div class="col-md-8">
                            <p class="pColorDiscussion">
                                @php
                                    $article = $discussion->description;
                                    $html =  explode( "<img>", $article );
                                    $html = preg_replace('/<[^>]*>/', '', $html[0]);
                                    if(strlen($html) > 650) {
                                          $stringCut = substr($html, 0, 200);
                                          $endPoint = strrpos($stringCut, ' ');
                                          $html = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                         echo $html .= '.... <span class="setSpanColor">more</span>';
                                      }
                                @endphp
                            </p>
                        </div>
                        <div class="total-replies">
                            <span class="spanTotalReplies">Total Replies: {{$discussion->topicReplies->count()}}</span>
                            <span class="spanTotalReplies">Total Likes: {{$discussion->topicLikes->count()}}</span>
                        </div>
                    </div>
                </div>
                @else
                    <div class="image_avatar">
                        <img  src="/{{$discussion->user->img_avatar}}" class="imageAvatar" onerror="this.src='/images/image.png'" />
                        <div class="set-flex-direaction">
                            <div class="col-md-4">
                                <h2 style="font-weight: {{$fontWeight}}" class="topic_name1">{{$discussion->name}}</h2>
                                <span class="colorp222">Latest Post :
                                <span class="setUsernameColor">{{$discussion->user->name}}</span>
                                .
                                <span class="setTimeColor">{{\Carbon\Carbon::parse($discussion->created_at)->format('m d Y')}}</span>
                            </span>
                            </div>
                            <div class="col-md-8">
                                <p class="pColorDiscussion">
                                    @php
                                        $article = $discussion->description;
                                        $html =  explode( "<img>", $article );
                                        $html = preg_replace('/<[^>]*>/', '', $html[0]);
                                        if(strlen($html) > 650) {
                                              $stringCut = substr($html, 0, 200);
                                              $endPoint = strrpos($stringCut, ' ');
                                              $html = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                             echo $html .= '.... <span class="setSpanColor">more</span>';
                                          }
                                    @endphp
                                </p>
                            </div>
                            <div class="total-replies">
                                <span class="spanTotalReplies">Total Replies: {{$discussion->topicReplies->count()}}</span>
                                <span class="spanTotalReplies">Total Likes: {{$discussion->topicLikes->count()}}</span>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        @endif
        @endforeach
    </div>
    <br>
    <br>
@endsection



@section('modal')

@endsection

@section('footer')
    <script>
        $('#openBox').click(function () {
            $('#opendailogeBox').toggle();
        });
    </script>
@endsection
