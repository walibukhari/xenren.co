@extends('frontend.layouts.default')

@section('title')
    Ask a question - Xenren Community
@endsection

@section('keywords')
    Ask Question
@endsection

@section('description')
    Xenren Community Ask a question
@endsection

@section('author')
    Ask Question
@endsection

@section('url')
    https://www.xenren.co/{{$lang}}/{{$topic_name}}
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
    <style>
        .customStylecommunity{
            background-color: #fff;
            color: #fff;
            position: relative;
            z-index: 1;
            padding: 48px 0 70px;
            min-height: 350px;
            margin-bottom: 0 !important;
            box-shadow: 0px 0px 10px -2px #555;
            border-radius: 5px;
        }
        .bredcrums-xenren{
            border-bottom: 1px solid #efefef !important;
            padding-bottom: 20px;
        }
        .bredcrums-xenren a {
            color: #b2b2b2;
            padding-right: 10px;
        }
        .set-backslash{
            color: #b2b2b2;
            padding-right: 10px;
        }
        .form-group-custom{
            margin-top: 20px;
            margin-bottom: 0px;
        }
        .labul{
            font-weight: bold;
            color: #555;
        }
        .setContent{
            display: flex;
            align-items: center;
        }
        .customCheckbox{
            width: 15px;
        }
        .setEmailSec{
            color: #555;
            font-weight: bold;
            padding-left: 12px;
        }
        .customSBtn{
            width: 138px;
            height: 40px;
            background: #27313d;
            color: #fff;
            border-radius: 10px;
        }
        .customSBtn:hover{
            width: 138px;
            height: 40px;
            background: #27313d;
            color: #fff;
            border-radius: 10px;
        }
        .customSBtn:focus{
            width: 138px;
            height: 40px;
            background: #27313d;
            color: #fff;
            border-radius: 10px;
        }
        .customStyleForum{
            display: flex;
            padding: 0px;
            align-items: center;
        }
        .customvol62 {
            display: flex;
            justify-content: flex-end;
            top:12px;
        }


        /* custom input type file */


        .custom-file-input::-webkit-file-upload-button {
            visibility: hidden;
        }
        .custom-file-input::before {
            content: 'Attach file';
            display: inline-block;
            background: #2aca15;
            border-radius: 30px;
            padding: 12px 30px;
            outline: none;
            color: #fff !important;
            -webkit-user-select: none;
            cursor: pointer;
            font-size: 10pt;
        }
        .custom-file-input:hover::before {
            border-color: black;
        }
        .custom-file-input:active::before {
            background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9);
        }
        .customFaPaperClip {
            position: absolute;
            top: 35px;
            left: 24px;
        }
        .displayNone{
            display: none !important;
        }
        /* custom input type file */
    </style>
    <link href='https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/css/froala_editor.pkgd.min.css' rel='stylesheet' type='text/css' />
@endsection

@section('content')
    <div class="row customStylecommunity">
        <div class="col-md-12">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{session()->get('success')}}
                </div>
            @endif
            @if(session()->has('error_message'))
                <div class="alert alert-danger">
                    {{session()->get('error_message')}}
                </div>
            @endif
        </div>
        <div class="col-md-12">
            <div class="bredcrums-xenren">
                <a href="{{route('frontend.Community',[$lang])}}">Xenren Community</a>
                <span class="set-backslash">/</span>
                <a href="/{{$lang}}/community/{{cleanLink($topic_type)}}/{{$topic_name}}/{{$topic_id}}">{{$topic_type}}</a>
                <span class="set-backslash">/</span>
                <a href="">{{cleanName($topic_name)}}</a>
            </div>
        </div>
        <div class="col-md-12">
            <form class="form" id="formQuestion" action="{{route('frontend.submitQuestions')}}" method="POST">
                {!! csrf_token() !!}
                <input type="hidden" name="topic_type" value="{{$type}}" />
                <input type="hidden" name="topic_name" value="{{$topic_name}}" />
                <input type="hidden" name="topic_id" value="{{$topic_id}}" />
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group-custom">
                            <input type="text" class="customClassIn form-control" name="name" placeholder="Subject" value="" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group-custom">
                            <textarea type="text" id="flroa" cols="12" name="description" rows="12" class="form-control" placeholder="Type Something"></textarea>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="customStyleForum col-md-12">
                        <div class="customvol61 col-md-6">
                            <div class="form-group-custom">
                                <i class="customFaPaperClip fa fa-paperclip" aria-hidden="true"></i>
                                <input type="file" name="file" class="custom-file-input" />
                            </div>
                        </div>
                        <div class="customvol62 col-md-6">
                            <button type="button" onclick="submitform()" class="customSBtn btn btn-success">
                                <i id="faShow" style="display: none;" class="fa fa-refresh fa-spin"></i>
                                Submit
                            </button>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group-custom">
                            <div class="setContent">
                                <input type="checkbox" checked name="notify_me" class="customCheckbox form-control" />
                                <span class="setEmailSec">Email me when someone replies</span>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <br>
    <br>
@endsection



@section('modal')

@endsection

@section('footer')
    <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/js/froala_editor.pkgd.min.js'></script>
    <script>
        $(document).ready(function() {
            $('.fr-wrapper > div > a').addClass('displayNone');
            // var editor = new FroalaEditor('#flroa')
            new FroalaEditor('#flroa',{
                events: {
                    "image.beforeUpload": function(files) {
                        var editor = this;
                        if (files.length) {
                            // Create a File Reader.
                            var reader = new FileReader();
                            // Set the reader to insert images when they are loaded.
                            reader.onload = function(e) {
                                var result = e.target.result;
                                editor.image.insert(result, null, null, editor.image.get());
                            };
                            // Read image as base64.
                            reader.readAsDataURL(files[0]);
                        }
                        editor.popups.hideAll();
                        // Stop default upload chain.
                        return false;
                    }
                }
            })
        });

        function submitform() {
            $('#faShow').show();
            $('#formQuestion').submit()
        }
    </script>
@endsection
