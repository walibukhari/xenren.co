@extends('frontend.layouts.default')

@section('title')
    Reply to Message - Xenren Community
@endsection

@section('keywords')
    Ask Question
@endsection

@section('description')
    Reply A Message
@endsection

@section('author')
    Ask Question
@endsection

@section('url')
    https://www.xenren.co/{{$lang}}/{{$d_name}}
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
    <style>
        label{
            color: #555;
            font-weight: bold;
        }
        .customStylecommunity{
            background-color: #fff;
            color: #fff;
            position: relative;
            z-index: 1;
            padding: 48px 0 70px;
            min-height: 350px;
            margin-bottom: 0 !important;
            box-shadow: 0px 0px 10px -2px #555;
            border-radius: 5px;
        }
        .bredcrums-xenren{
            border-bottom: 1px solid #efefef !important;
            padding-bottom: 20px;
        }
        .bredcrums-xenren a {
            color: #b2b2b2;
            padding-right: 10px;
        }
        .set-backslash{
            color: #b2b2b2;
            padding-right: 10px;
        }
        .form-group-custom{
            margin-top: 20px;
            margin-bottom: 0px;
        }
        .labul{
            font-weight: bold;
            color: #555;
        }
        .setContent{
            display: flex;
            align-items: center;
        }
        .customCheckbox{
            width: 15px;
        }
        .setEmailSec{
            color: #555;
            font-weight: bold;
            padding-left: 12px;
        }
        .customSBtn{
            width: 138px;
            height: 40px;
            background: #27313d;
            color: #fff;
            border-radius: 10px;
        }
        .customSBtn:hover{
            width: 138px;
            height: 40px;
            background: #27313d;
            color: #fff;
            border-radius: 10px;
        }
        .customSBtn:focus{
            width: 138px;
            height: 40px;
            background: #27313d;
            color: #fff;
            border-radius: 10px;
        }
        .customStyleForum{
            display: flex;
            padding: 0px;
            align-items: center;
        }
        .customvol62 {
            display: flex;
            justify-content: flex-end;
            top:12px;
        }


        /* custom input type file */


        .custom-file-input::-webkit-file-upload-button {
            visibility: hidden;
        }
        .custom-file-input::before {
            content: 'Attach file';
            display: inline-block;
            background: #2aca15;
            border-radius: 30px;
            padding: 12px 30px;
            outline: none;
            color: #fff !important;
            -webkit-user-select: none;
            cursor: pointer;
            font-size: 10pt;
        }
        .custom-file-input:hover::before {
            border-color: black;
        }
        .custom-file-input:active::before {
            background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9);
        }
        .customFaPaperClip {
            position: absolute;
            top: 35px;
            left: 24px;
        }
        /* custom input type file */
        .boldcustomClassIn{
            font-weight: bold;
            color: #555;
        }


        .customStylecommunity-2{
            margin-top: 50px;
            background-color: #fff;
            color: #fff;
            position: relative;
            z-index: 1;
            padding: 30px;
            min-height: 350px;
            margin-bottom: 0 !important;
            box-shadow: 0px 0px 10px -2px #555;
            border-radius: 5px;
        }
        .user-image-avatar{
            display: flex;
            align-items: center;
            margin-top: 25px;
            padding-bottom: 15px;
            border-bottom: 1px solid #efefef;
        }
        .imageAvatar{
            width: 60px;
            height: 60px;
        }

        .set-remain-section{
            display: flex;
            flex-direction: column;
            padding-left: 20px;
        }
        .testCenter{
            display: flex;
            align-items: center;
        }
        .setColorLikes{
            color: #555;
        }
        .image-good{
            cursor: pointer;
        }
        p{
            color: black;
            font-size: 14px;
            font-family: 'Myriad Pro', 'Raleway', sans-serif !important;
        }
        span{
            font-size: 14px !important;
            font-family: 'Myriad Pro', 'Raleway', sans-serif !important;
        }
        .imageAvatar{
            width: 60px;
            height: 60px;
        }
        .setTimeColor{
            color: gray;
            font-family: sans-serif !important;
        }
        .setColorSS{
            color:#555;
            padding-top: 5px;
        }
        .setColor{
            color:#555;
            padding-top: 5px;
        }
        .nameM{
            color: #5ec329;
            font-weight: 500;
            text-transform: capitalize;
        }
        .customBtnColor-2{
            margin-top: 20px;
            background: #fff;
            color: #5ec329;
            width: 130px;
            height: 45px;
            border: 2px solid #efefef !important;;
            border-radius: 3px !important;;
        }
        .customBtnColor-2:hover{
            margin-top: 20px;
            background: #fff;
            color: #5ec329;
            width: 130px;
            height: 45px;
            border: 2px solid #efefef !important;
            border-radius: 3px !important;;
        }
        .setBoldB{
            margin-top: 20px;
            margin-bottom: 0px;
            font-weight: bold;
            font-size: 17px;
            color: #555;
        }
    </style>
    <link href='https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/css/froala_editor.pkgd.min.css' rel='stylesheet' type='text/css' />
@endsection

@section('content')
    <div class="row customStylecommunity">
        <div class="col-md-12">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{session()->get('success')}}
                </div>
            @endif
        </div>
        <div class="col-md-12">
            <div class="bredcrums-xenren">
                <a href="{{route('frontend.Community',[$lang])}}">Xenren Community</a>
                <span class="set-backslash">/</span>
            </div>
        </div>
        <div class="col-md-12">
            <form class="form" action="{{route('frontend.submitAnswer')}}" method="POST">
                {!! csrf_token() !!}
                <input type="hidden" name="d_name" value="{{$d_name}}" />
                <input type="hidden" name="topic_id" value="{{$topic_id}}" />
                <input type="hidden" name="d_id" value="{{$d_id}}" />
                <input type="hidden" name="r_id" value="{{$r_id}}" />
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group-custom">
                            <label>Subject:</label>
                            <input type="text" class="customClassIn boldcustomClassIn form-control" readonly value="Re : {{cleanName($d_name)}}" name="name" placeholder="Subject" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group-custom">
                            <label>Body:</label>
                            <textarea type="text" id="flroa" cols="12" name="description" rows="12" class="form-control" placeholder="Type Something"></textarea>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="customStyleForum col-md-12">
                        <div class="customvol61 col-md-6">
                            <div class="form-group-custom">
                                <i class="customFaPaperClip fa fa-paperclip" aria-hidden="true"></i>
                                <input type="file" name="file" class="custom-file-input" />
                            </div>
                        </div>
                        <div class="customvol62 col-md-6">
                            <button type="submit" class="customSBtn btn btn-success">Submit</button>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group-custom">
                            <div class="setContent">
                                <input type="checkbox" checked name="notify_me" class="customCheckbox form-control" />
                                <span class="setEmailSec">Email me when someone replies</span>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <h3 class="setBoldB">Replying to :</h3>
    </div>
    @if($replyTo != '')
        @if($type == 'd_comment')
            <div class="row customStylecommunity-2">
                <div class="row">
                    <div class="col-md-12">
                        <div class="user-image-avatar">
                            <img src="/images/image.png" class="imageAvatar" onerror="this.src='/images/image.png'">
                            <div class="set-remain-section">
                                <span class="setColor">Active Member</span>
                                <span class="setColor nameM">{{$replyTo->user->name}}</span>
                                <small class="setColorSS">Member Since {{$replyTo->user->created_at->format('M d Y')}}</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <p>{!! $replyTo->description !!}</p>
                    </div>
                    <div class="col-md-12 text-right testCenter">
                        <div class="col-md-6 text-left">
                            @if(isset($checkLike))
                                @if($checkLike->likes > 0)
                                    <img class="image-good" src="{{asset('images/green-good-like.svg')}}" />
                                @else
                                    <img class="image-good" onclick="window.location.href='{{route('frontend.topicLikes',[$replyTo->id])}}'" src="{{asset('images/good-icon.svg')}}" />
                                @endif
                            @else
                                <img class="image-good" onclick="window.location.href='{{route('frontend.topicLikes',[$replyTo->id])}}'" src="{{asset('images/good-icon.svg')}}" />
                            @endif
                            <span class="setColorLikes">{{$replyTo->getLikesCount($replyTo->topicLikes)}} Likes</span>
                        </div>
                    </div>
                </div>
            </div>
        @else
        <div class="row customStylecommunity-2">
            <div class="row">
                <div class="col-md-12">
                    <div class="user-image-avatar">
                        <img src="/images/image.png" class="imageAvatar" onerror="this.src='/images/image.png'">
                        <div class="set-remain-section">
                            <span class="setColor">Active Member</span>
                            <span class="setColor nameM">{{$replyTo->user->name}}</span>
                            <small class="setColorSS">Member Since {{$replyTo->user->created_at->format('M d Y')}}</small>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <p>{!! $replyTo->comment !!}</p>
                </div>
                <div class="col-md-12 text-right testCenter">
                    <div class="col-md-6 text-left">
                        @if(isset($checkLike))
                            @if($checkLike->likes > 0)
                                <img class="image-good" src="{{asset('images/green-good-like.svg')}}" />
                            @else
                                <img class="image-good" onclick="window.location.href='{{route('frontend.submitLikes',[$replyTo->id])}}'" src="{{asset('images/good-icon.svg')}}" />
                            @endif
                        @else
                            <img class="image-good" onclick="window.location.href='{{route('frontend.submitLikes',[$replyTo->id])}}'" src="{{asset('images/good-icon.svg')}}" />
                        @endif
                        <span class="setColorLikes">{{$replyTo->getLikesCount($replyTo->like)}} Likes</span>
                    </div>
                </div>
            </div>
        </div>
        @endif
    @endif
    <br>
    <br>
@endsection



@section('modal')

@endsection

@section('footer')
    <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/js/froala_editor.pkgd.min.js'></script>
    <script>
        $(document).ready(function() {
            // var editor = new FroalaEditor('#flroa')
            new FroalaEditor('#flroa',{
                events: {
                    "image.beforeUpload": function(files) {
                        var editor = this;
                        if (files.length) {
                            // Create a File Reader.
                            var reader = new FileReader();
                            // Set the reader to insert images when they are loaded.
                            reader.onload = function(e) {
                                var result = e.target.result;
                                editor.image.insert(result, null, null, editor.image.get());
                            };
                            // Read image as base64.
                            reader.readAsDataURL(files[0]);
                        }
                        editor.popups.hideAll();
                        // Stop default upload chain.
                        return false;
                    }
                }
            })
        });
    </script>
@endsection
