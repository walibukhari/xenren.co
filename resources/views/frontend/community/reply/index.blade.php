@extends('frontend.layouts.default')

@section('title')
    Xenren Community
@endsection

@section('keywords')
    Community Home
@endsection

@section('description')
    Xenren Offical
@endsection

@section('author')
    Xenren Offical
@endsection

@section('url')
    https://www.xenren.co/{{$lang}}/community/{{$topic_name}}/Xenren - Community !
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
    <style>
        .customStylecommunity{
            background-color: #fff;
            color: #fff;
            position: relative;
            z-index: 1;
            padding: 30px;
            min-height: 350px;
            margin-bottom: 0 !important;
            box-shadow: 0px 0px 10px -2px #555;
            border-radius: 5px;
        }
        .customStylecommunity-2{
            margin-top: 50px;
            background-color: #fff;
            color: #fff;
            position: relative;
            z-index: 1;
            padding: 30px;
            min-height: 350px;
            margin-bottom: 0 !important;
            box-shadow: 0px 0px 10px -2px #555;
            border-radius: 5px;
        }
        .set-backslash{
            color: #b2b2b2;
            padding-right: 10px;
        }
        .bredcrums-xenren a {
            color: #b2b2b2;
            padding-right: 10px;
        }
        .customSearch{
            z-index: 99999;
            position: relative;
            height: 45px;
            padding-left: 45px;
            border-radius: 30px;
        }
        .set-input-search{
            position: relative;
        }
        .setFasearch{
            display: flex !important;
            align-items: center;
            position: absolute;
            z-index: 999999;
            top: 7px;
            left: 10px;
            width: 30px;
            height: 30px;
            background: #5bbc2e;
            padding: 8px;
            border-radius: 25px;
            color: #fff;
            font-size: 13px;
        }
        .category-pagination-xenren{
            padding-bottom: 10px;
            color: #5ec329;
            font-size: 13px;
            position: relative;
            float: right;
        }
        .xenren-pagination-link-1{
            color: #313e44;
            font-size: 13px;
            margin-right: 20px;
        }
        .xenren-pagination-link-1:before {
            display: inline-block;
            font: normal normal normal 13px/1 FontAwesome;
            font-size: 13px;
            font-size: inherit;
            -moz-osx-font-smoothing: grayscale;
            -webkit-font-smoothing: antialiased;
            text-rendering: auto;
            transform: translate(0, 0);
            content: "\f104";
            margin-right: 4px;
        }
        .xenren-pagination-link-1:hover{
            color: #5ec329;
            font-size: 13px;
        }
        .xenren-pagination-link:after{
            display: inline-block;
            font: normal normal normal 13px/1 FontAwesome;
            font-size: 13px;
            font-size: inherit;
            -moz-osx-font-smoothing: grayscale;
            -webkit-font-smoothing: antialiased;
            text-rendering: auto;
            transform: translate(0, 0);
            content: "\f105";
            margin-left: 4px;
        }
        .xenren-pagination-link{
            color: #fff;
            font-size: 13px;
            background: #27313d;
            padding: 9px 15px 9px 15px;
            border-radius: 12px;
        }
        .xenren-pagination-link:hover{
            color: #5ec329;
            font-size: 13px;
        }
        .form-group-custom{
            margin-top: 20px;
            margin-bottom: 0px;
        }
        .topic_name{
            color: #555;
            font-size: 26px;
            font-weight: bold;
        }
        .topic_name1{
            color: #555;
            font-size: 16px;
            font-weight: bold;
        }
        .customSetBtn{
            float: right;
        }
        .customBtnColor:hover{
            margin-top: 20px;
            background: #5ec329;
            color: #fff;
            width: 130px;
            height: 45px;
        }
        .customBtnColor{
            margin-top: 20px;
            background: #5ec329;
            color: #fff;
            width: 130px;
            height: 45px;
        }
        .customBtnColor-2{
            margin-top: 20px;
            background: #fff;
            color: #5ec329;
            width: 130px;
            height: 45px;
            border: 2px solid #efefef !important;;
            border-radius: 3px !important;;
        }
        .customBtnColor-2:hover{
            margin-top: 20px;
            background: #fff;
            color: #5ec329;
            width: 130px;
            height: 45px;
            border: 2px solid #efefef !important;
            border-radius: 3px !important;;
        }
        .xen-menu-option{
            background-color: transparent !important;
            border-color: transparent !important;
            font-size: 0;
            height: 30px;
            width: 30px;
            background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
            border: medium none;
            border-radius: 4px;
            color: #3e3e3e;
            cursor: pointer;
            display: inline-block;
            line-height: 26px;
            margin-bottom: 0;
            min-width: 0;
            padding: 6px 9px;
            text-align: center;
            touch-action: manipulation;
            vertical-align: middle;
            white-space: nowrap;
        }
        .xen-menu-option:before {
            background: rgba(0, 0, 0, 0) url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiPjxwYXRoIGQ9Ik0xMiAxNmMxLjY1NiAwIDMgMS4zNDQgMyAzcy0xLjM0NCAzLTMgMy0zLTEuMzQ0LTMtMyAxLjM0NC0zIDMtM3ptMCAxYzEuMTA0IDAgMiAuODk2IDIgMnMtLjg5NiAyLTIgMi0yLS44OTYtMi0yIC44OTYtMiAyLTJ6bTAtOGMxLjY1NiAwIDMgMS4zNDQgMyAzcy0xLjM0NCAzLTMgMy0zLTEuMzQ0LTMtMyAxLjM0NC0zIDMtM3ptMCAxYzEuMTA0IDAgMiAuODk2IDIgMnMtLjg5NiAyLTIgMi0yLS44OTYtMi0yIC44OTYtMiAyLTJ6bTAtOGMxLjY1NiAwIDMgMS4zNDQgMyAzcy0xLjM0NCAzLTMgMy0zLTEuMzQ0LTMtMyAxLjM0NC0zIDMtM3ptMCAxYzEuMTA0IDAgMiAuODk2IDIgMnMtLjg5NiAyLTIgMi0yLS44OTYtMi0yIC44OTYtMiAyLTJ6Ii8+PC9zdmc+) no-repeat scroll center center / 18px 18px !important;
            content: "";
            display: block;
            height: 100%;
            padding: 9px 6px !important;
            vertical-align: middle;
            width: 100%;
        }
        .customSetBorderBottom2{
            padding-bottom: 20px;
            border-bottom: 2px solid #efefef;
        }
        p{
            color: black;
            font-size: 14px;
            font-family: 'Myriad Pro', 'Raleway', sans-serif !important;
        }
        span{
            font-size: 14px !important;
            font-family: 'Myriad Pro', 'Raleway', sans-serif !important;
        }
        .imageAvatar{
            width: 60px;
            height: 60px;
        }
        .setTimeColor{
            color: gray;
            font-family: sans-serif !important;
        }
        .setColorSS{
            color:#555;
            padding-top: 5px;
        }
        .setColor{
            color:#555;
            padding-top: 5px;
        }
        .user-image-avatar{
            display: flex;
            align-items: center;
            margin-top: 25px;
            padding-bottom: 15px;
            border-bottom: 1px solid #efefef;
        }
        .set-remain-section{
            display: flex;
            flex-direction: column;
            padding-left: 20px;
        }
        .nameM{
            color: #5ec329;
            font-weight: 500;
            text-transform: capitalize;
        }
        .testCenter{
            display: flex;
            align-items: center;
        }
        .setColorLikes{
            color: #555;
        }
        .image-good{
            cursor: pointer;
        }
        .set-box-drop-down{
            background: #fff;
            width: 160px;
            padding: 0px;
            border: 1px solid #555;
            position: absolute;
            margin-top: 20px;
            z-index: 9999;
            right: 20px;
        }
        .custom-set-ul{
            padding-left: 0px;
        }
        .custom-set-li:hover{
            color: #57a224;
        }
        .custom-set-li{
            list-style: none;
            color: #555;
            font-size: 12px;
            padding-top: 15px;
            padding: 12px;
            cursor: pointer;
            border-bottom: 1px solid #555;
        }
        .custom-set-li:nth-last-child(4){
            border-bottom: 0px solid #555;
            padding-bottom: 5px;
        }
        .custom-set-li:nth-last-child(6){
            border-bottom: 0px solid #555;
            padding-bottom: 5px;
        }
        .custom-set-li:nth-last-child(1){
            border-bottom: 0px solid #555;
            padding-bottom: 5px;
        }
        .foreach-menu{
            position: absolute;
            top: 20px;
            right: 20px;
        }

        .quotes {
            quotes: "“" "”" "‘" "’";
            color:#555;
            padding:12px;
            font: 15px Arial, sans-serif;
            font: italic bold 30px/30px Georgia, serif;
        }
        .disbaled{
            font-size: 16px;
            color:#555;
            font-weight: bold;
            pointer-events: none;
            /* for "disabled" effect */
            opacity: 0.5;
        }
        .quotes::before {
            content: open-quote;
        }
        .quotes::after {
            content: close-quote;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('success_message'))
                <div class="alert alert-success">
                    {{session()->get('success_message')}}
                </div>
            @endif
        </div>
    </div>
    <div class="row customStylecommunity">
        <div class="col-md-12">
            <div class="bredcrums-xenren">
                <a href="{{route('frontend.Community',[$lang])}}">Xenren Community</a>
                <span class="set-backslash">/</span>
                @if($topic_type == 'Xenren-Offical')
                    <a href="{{route('frontend.XenrenOffical',[$lang,cleanLink($topic_name),$topic_id])}}">{{cleanName($topic_type)}}</a>
                @else
                    <a href="{{route('frontend.CommunityDiscussion',[$lang,cleanLink($topic_name),$topic_id])}}">{{cleanName($topic_type)}}</a>
                @endif
            </div>
        </div>
        @include('frontend.community.include.search')
        <div class="customSetBorderBottom2 col-md-12">
            <div class="form-group-custom">
                <div class="category-pagination-xenren">
                    <label><a class="xenren-pagination-link-1" href="">Previous</a></label>
                    <label><a class="xenren-pagination-link" href="">Next</a></label>
                </div>
            </div>
        </div>
        <div class="customSetBorderBottom2 col-md-12">
            <div class="form-group-custom">
                <div class="col-md-6">
                    <h2 class="topic_name">
                        {{cleanName($topic_name)}}
                    </h2>
                    <span class="setTimeColor">
                        {{\Carbon\Carbon::parse($getDiscussion->created_at)->format('M d Y')}}
                        .
                        {{\Carbon\Carbon::parse($getDiscussion->created_at)->format('g:i A')}}
                        .
                        Edited
                    </span>
                </div>
                <div class="col-md-6">
                    <div class="customSetBtn">
                        <h2>
                            @if(!\Auth::user())
                                <button type="button" class="menu-opener-xenren xen-menu-option" role="button" href="#">Option</button>
                            @else
                                <button type="button" class="menu-opener-xenren xen-menu-option" id="openBox" role="button" href="#">Option</button>
                                <div class="set-box-drop-down" id="opendailogeBox" style="display:none;">
                                    <ul class="custom-set-ul">
                                        @if(isset($getDiscussion->subscribe) && $getDiscussion->subscribe->subscribe == \App\Models\CommunityDiscussion::SUBSCRIBE)
                                            <li class="custom-set-li" onclick="window.location.href='{{route('frontend.replyFeatureS',[$getDiscussion->id,'8'])}}'"> Un Subscribe</li>
                                        @elseif($getDiscussion->subscribe && $getDiscussion->subscribe->subscribe == \App\Models\CommunityDiscussion::UN_SUBSCRIBE)
                                            <li class="custom-set-li" onclick="window.location.href='{{route('frontend.replyFeatureS',[$getDiscussion->id,'4'])}}'">Subscribe</li>
                                        @else
                                            <li class="custom-set-li" onclick="window.location.href='{{route('frontend.replyFeatureS',[$getDiscussion->id,'4'])}}'"> Subscribe</li>
                                        @endif
                                        @if($getDiscussion->book_mark == \App\Models\CommunityDiscussion::UN_BOOKMARK)
                                        <li class="custom-set-li" onclick="window.location.href='{{route('frontend.replyFeatureS',[$getDiscussion->id,'5'])}}'">Bookmark</li>
                                        @else
                                        <li class="custom-set-li" onclick="window.location.href='{{route('frontend.replyFeatureS',[$getDiscussion->id,'7'])}}'">Un Bookmark</li>
                                        @endif
                                        <li class="custom-set-li" onclick="window.location.href='{{route('frontend.emailToFriend',[$lang,$getDiscussion->id])}}'">Email to Friend</li>
                                        <li class="custom-set-li" onclick="window.location.href='{{route('frontend.reportCommunityMessage',[$lang,$getDiscussion->id])}}'">Report</li>
                                    </ul>
                                </div>
                            @endif
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="user-image-avatar">
                    <img src="" class="imageAvatar" onerror="this.src='/images/image.png'" />
                    <div class="set-remain-section">
                        <span class="setColor">Active Member</span>
                        <span class="setColor nameM">{{$getDiscussion->user->name}}</span>
                        <small class="setColorSS">Member Since {{$getDiscussion->user->created_at->format('M d Y')}}</small>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <p>{!! $getDiscussion->description !!}</p>
            </div>
            <div class="col-md-12 text-right testCenter">
                <div class="col-md-6 text-left">
                    @if(isset($checkLike))
                        @if($checkLike->likes > 0)
                            <img class="image-good" src="{{asset('images/green-good-like.svg')}}" />
                        @else
                            <img class="image-good" onclick="window.location.href='{{route('frontend.topicLikes',[$getDiscussion->id])}}'" src="{{asset('images/good-icon.svg')}}" />
                        @endif
                    @else
                        <img class="image-good" onclick="window.location.href='{{route('frontend.topicLikes',[$getDiscussion->id])}}'" src="{{asset('images/good-icon.svg')}}" />
                    @endif
                    <span class="setColorLikes">{{$getDiscussion->getLikesCount($getDiscussion->topicLikes)}} Likes</span>
                </div>
                <div class="col-md-6">
                    <button class="customBtnColor btn btn-success" onclick="window.location.href='{{route('frontend.CommunityReplyForums',[$lang,$topic_name,$topic_id,$getDiscussion->id,'d_comment',$getDiscussion->id])}}'">Reply</button>
                </div>
            </div>
        </div>
    </div>

    @foreach($getReplies as $replies)
    <div class="row customStylecommunity-2">
        @if(isset($replies->reply_id))
        <div class="row">
             <blockquote class="quotes">
                 <div class="disbaled">
                     {!!  $replies->getQuotes($replies->reply_id) !!}
                 </div>
             </blockquote>
        </div>
        @endif

        <div class="row">
            <div class="col-md-12">
                    <div class="user-image-avatar">
                        <img src="/images/image.png" class="imageAvatar" onerror="this.src='/images/image.png'">
                        <div class="set-remain-section">
                            <span class="setColor">Active Member</span>
                            <span class="setColor nameM">{{$replies->user->name}}</span>
                            <small class="setColorSS">Member Since {{$getDiscussion->user->created_at->format('M d Y')}}</small>
                        </div>
                    </div>
                    <button type="button" class="foreach-menu menu-opener-xenren xen-menu-option" onclick="openModalBox('{{$replies->id}}')" role="button" href="#">Option</button>
                    <div class="set-box-drop-down" id="opendailogeBox-{{$replies->id}}" style="display:none;">
                        <ul class="custom-set-ul">
                            <li class="custom-set-li" onclick="window.location.href='{{route('frontend.reportCommunityReplyMessage',[$lang,$replies->id])}}'">Report Inappropriate Content</li>
                            <li class="custom-set-li" onclick="window.location.href='{{route('frontend.emailToFriend',[$lang,$replies->id])}}'">Email to Friend</li>
                        </ul>
                    </div>
            </div>
            <div class="col-md-12">
                <p>{!! $replies->comment !!}</p>
            </div>
            <div class="col-md-12 text-right testCenter">
                <div class="col-md-6 text-left">
                    @php
                        $getUser = $replies->getUser(\Auth::user()->id,$replies->id);
                    @endphp
                    @if(isset($getUser))
                        @if($getUser->likes > 0)
                            <img class="image-good" src="{{asset('images/green-good-like.svg')}}" />
                        @else
                            <img class="image-good" onclick="window.location.href='{{route('frontend.submitLikes',[$replies->id])}}'" src="{{asset('images/good-icon.svg')}}" />
                        @endif
                    @else
                        <img class="image-good" onclick="window.location.href='{{route('frontend.submitLikes',[$replies->id])}}'" src="{{asset('images/good-icon.svg')}}" />
                    @endif
                    <span class="setColorLikes">{{$replies->getLikesCount($replies->like)}} Likes</span>
                </div>
                <div class="col-md-6">
                    <button class="customBtnColor-2 btn btn-success" onclick="window.location.href='{{route('frontend.CommunityReplyForums',[$lang,$topic_name,$topic_id,$replies->discuss_id,'u_comment',$replies->id])}}'">Reply</button>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    <br>
    <br>
@endsection



@section('modal')

@endsection

@section('footer')
    <script>
        $('#openBox').click(function () {
            $('#opendailogeBox').toggle();
        });

        function openModalBox(id) {
            $('#opendailogeBox-'+id).toggle();
        }
    </script>
@endsection
