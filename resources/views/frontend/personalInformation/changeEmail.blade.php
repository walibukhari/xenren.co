@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.edit_email') }}
@endsection

@section('description')
    {{ trans('common.edit_email') }}
@endsection

@section('author')
Xenren
@endsection

@section('header')
    <link href="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/personalInformation.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/select2/css/select2.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3 search-title-container">
            <span class="find-experts-search-title text-uppercase">{{ trans('common.main_action') }}</span>
        </div>
        <div class="col-md-9 right-top-main">
            <div class="row">
                <div class="col-md-12 top-tabs-menu">
                    <div class="caption">
                        <span class="find-experts-search-title text-uppercase">{{ trans('common.edit_email') }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper" >
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12 col-sm-12" >
                    <div class="portlet light bordered" style="margin-top: 18px">
                        <div class="portlet-body personal-info">
                            @include('frontend.flash')
                            <div class="portlet-body form">
                                @if($user)
                                {!! Form::open(['route' => 'frontend.changeemail.post', 'class' => 'form-horizontal', 'id' => 'personal-form', 'role' => 'form', 'files' => true]) !!}
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                {{ trans('common.email') }}
                                            </label>
                                            <div class="col-md-8">
                                                {!! Form::text('email', $_G['user']->email, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                {{ trans('common.new_email') }}
                                            </label>
                                            <div class="col-md-8">
                                                {!! Form::text('newEmail', '', ['class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                    </div>

                                    <br/>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <hr>
                                                <button type="submit" class="btn blue" id="personal-form-submit-btn">{{ trans('member.save_modify') }}</button>
                                                <input type="hidden" name="userId" value="{{ $user->id }}" />
                                            </div>
                                        </div>
                                    </div>

                                    <div style="padding:140px 0"></div>
                                {!! Form::close() !!}
                                @else
                                    <div class="text-center" style="padding:280px 0">
                                        {{ trans('common.not_allow_to_change_email') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
        </div>
            <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@endsection


@section('footer')
    <script>
        var lang = {
        };

        var url ={
        }

        var userId = '{{ $_G['user']->id }}';
    </script>
    <script type="text/javascript">

    </script>
@endsection




