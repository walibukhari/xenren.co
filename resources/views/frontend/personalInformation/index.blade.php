@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.new_personal_info_title') }}
@endsection

@section('description')
    {{ trans('common.personal_information') }}
@endsection

@section('author')
Xenren
@endsection

@section('url')
    https://www.xenren.co/personalInformation
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
    <link href="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/personalInformation.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/select2/css/select2.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <style>
        .lds-ring {
            display: inline-block;
            position: relative;
            width: 20px;
            height: 20px;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 20px;
            height: 20px;
            margin: 1px;
            border: 2px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
        #personal-form-submit-btn{
            display: flex;
            justify-content: center;
            align-items: center;
        }
    </style>
@endsection

@section('content')
    <!-- BEGIN CONTENT -->
    @php
       $handPhone = ''; $code = '';
       if (Auth::user()->handphone_no != null) {
            $phone = Auth::user()->handphone_no;
            $data = Auth::user()->getCountryCode();
            $code = '+'.$data;
            $handPhone = str_replace($code, '', $phone);
       }
    @endphp
    <div class="page-content-wrapper">
        <div class="row">
            <div class="col-md-3 search-title-container setPIROE">
                <span class="find-experts-search-title text-uppercase setMAinMENU">{{ trans('common.main_action') }}</span>
            </div>
        </div>
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            @if(isset(Auth::user()->face_image) &&  Auth::user()->face_image == \App\Models\FaceDetect::STATUS_FACE_DETECT_FALSE)
                <div class="alert alert-danger setALPIP">{{trans('common.face_image')}}</div>
            @elseif(isset(Auth::user()->face_image) &&  Auth::user()->face_image == \App\Models\FaceDetect::STATUS_FACE_DETECT_TRUE)
                <div class="alert alert-success setALPIP">Face Detection Successfully</div>
            @else
                <div class="alert alert-danger setALPIP">{{trans('common.face_image')}}</div>
            @endif
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row setPageContent">
                <div class="col-md-12 col-sm-12">
                    <div class="portlet light bordered setPLBP">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-blue bold uppercase setColorGray">
                                    {{ trans('common.personal_information') }}
                                </span>
                            </div>
                            <span class="pull-right setBTUC">
                                <button type="button" class="btn blue setColorGreen" id="btn-back-user-center">
                                    {{ trans('common.back_to_user_center') }}
                                </button>
                            </span>
                        </div>
                        <div class="portlet-body personal-info setPBPIP">
                            @include('frontend.flash')
                            <div class="portlet-body form">
                                {!! Form::open(['route' => 'frontend.personalinformation.update', 'class' => 'form-horizontal', 'id' => 'personal-form', 'role' => 'form', 'files' => true]) !!}
                                    <div class="form-body">
                                        {{--<div id="personal-form-alert-container"></div>--}}
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{ trans('member.real_user_avatar') }}</label>
                                            <div class="col-md-8">
                                                <div class="col-md-3">
                                                    <div class="profile-userpic">
                                                        <img src="{{ $_G['user']->getAvatar() }}" class="img-avatar setCLASSIMAGEAVATARPIP" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <span class="btn blue btn-file setColorGreen">
                                                            <span class="fileinput-new"> {{ trans('member.modify_avatar') }} </span>
                                                            <span class="fileinput-exists"> {{ trans('member.change') }} </span>
                                                            <input class="uploadAvatar" type="file" name="avatar"> </span>
                                                        <span class="fileinput-filename"> </span> &nbsp;
                                                        <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
                                                    </div>
                                                    <br/>
                                                    <label class="control-label">{{ trans('member.avatar_image_type') }}</label>
                                                </div>
                                            </div>
                                        </div>
                                        {{--<div class="form-group">--}}
                                            {{--<label class="col-md-3 control-label">{{ trans('member.real_name') }}</label>--}}
                                            {{--<div class="col-md-8">--}}
                                                {{--{!! Form::text('real_name', $_G['user']->real_name, ['class' => 'form-control' ]) !!}--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><span aria-required="true" class="required"> * </span>
                                                {{ trans('member.first_name') }}
                                            </label>
                                            <div class="col-md-3">
                                                @php
                                                    $name = $_G['user']->name;
                                                    $getname = explode(" ",$name);
                                                    $first_name = isset($getname[0]) ? $getname[0] : '';
                                                    $last_name = isset($getname[1]) ? $getname[1] : '';
                                                @endphp
                                                {!! Form::text('nick_name', $first_name, ['class' => 'form-control', 'placeholder' => trans('member.string_not_include')]) !!}
                                            </div>
                                            <label class="col-md-2 control-label"><span aria-required="true" class="required"> * </span>
                                                {{ trans('member.last_name') }}
                                            </label>
                                            <div class="col-md-3">
                                                {!! Form::text('last_name', $last_name, ['class' => 'form-control', 'placeholder' => trans('member.string_not_include')]) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><span aria-required="true" class="required"> * </span>
                                                {{ trans('common.about_me') }}
                                            </label>
                                            <div class="col-md-8">
                                                <textarea class="form-control" name="about_me" cols="50" rows="10" spellcheck="false" style="position: relative; background: none !important;" maxlength="20000">{{ $_G['user']->about_me }}</textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                {{ trans('common.experience_year') }}
                                            </label>
                                            <div class="col-md-8">
                                                {!! Form::text('experience', $_G['user']->experience, ['class' => 'form-control']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                {{ trans('common.job_position') }}
                                            </label>
                                            <div class="col-md-3">
                                                <select class="form-control" id="job_position_id" name="job_position_id">
                                                    @foreach( $jobPositions as $key => $jobPosition)
                                                    <option value="{{ $jobPosition->id }}" {{ isset($_G['user']->job_position_id) && $jobPosition->id == $_G['user']->job_position_id?'selected': ''}} >{{ $jobPosition->getName() }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <label class="col-md-2 control-label">
                                                {{ trans('common.job_title') }}
                                            </label>
                                            <div class="col-md-3">
                                                {!! Form::text('title', $_G['user']->title, ['class' => 'form-control', 'placeholder' => trans('common.job_title_example')]) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                {{ trans('common.skill') }}
                                            </label>
                                            <div class="col-md-8">
                                                <div id="magicsuggest"></div>
                                                {{ Form::hidden('skill_id_list', $_G['user']->getUserSkillList(), ['id'=>'skill_id_list']) }}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                {{ trans('common.city') }}
                                            </label>
                                            <div class="col-md-8">
                                                <select class="form-control" name="city_id" id="city" onchange="countryCode();"></select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-8 col-md-offset-3">
                                                <div id="map" style="height: 300px"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                {{ trans('common.address') }}
                                            </label>
                                            <div class="col-md-8">
                                                {!! Form::text('address', $_G['user']->address, ['class' => 'form-control', 'id' => 'address']) !!}
                                                <input type="hidden" name="latitude" value="{{ $_G['user']->latitude }}">
                                                <input type="hidden" name="longitude" value="{{ $_G['user']->longitude }}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 control-label">{{ trans('common.meet_up_place') }}</label>
                                            <div class="col-md-8">
                                                <select id="meet_up_place" name="meet_up_place" class="form-control">
                                                    <!-- <option v-for="office in listSharedOffice" :value="office.id">@{{ office.office_name }}</option> -->
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                {{ trans('common.language_2') }}
                                            </label>
                                            <div class="col-md-8">
                                                <div id="languageInput"></div>
                                                {{--<input type="text" name="language" class="form-control" v-model="languages"/>--}}

                                                {{ Form::hidden('lang_id_list', $_G['user']->getUserLanguageList(), ['id'=>'lang_id_list']) }}

                                            </div>
                                        </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                {{ trans('common.pay_hourly') }}
                                            </label>
                                            <div class="col-md-6">
                                                {!! Form::text('hourly_pay', $_G['user']->hourly_pay, ['class' => 'form-control']) !!}
                                            </div>
                                            <div class="col-md-2">
                                                <select class="form-control" name="currency" id="currency">
                                                    @if($_G['user']->currency!=null && $_G['user']->currency =='1')
                                                        <option value="1" selected="selected">{{ trans('common.usd') }}</option>
                                                        <option value="2">{{ trans('common.cny') }}</option>
                                                    @elseif($_G['user']->currency!=null && $_G['user']->currency =='2')
                                                        <option value="1">{{ trans('common.usd') }}</option>
                                                        <option value="2" selected="selected">{{ trans('common.cny') }}</option>
                                                    @else
                                                        <option value="">{{ trans('common.select_currency') }}</option>
                                                        <option value="1">{{ trans('common.usd') }}</option>
                                                        <option value="2">{{ trans('common.cny') }}</option>
                                                    @endif
                                                </select></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                {{ trans('common.email') }}
                                            </label>
                                            @if( $_G['user']->email == "")
                                                <div class="col-md-8">
                                                    {!! Form::text('email', $_G['user']->email, ['class' => 'form-control']) !!}
                                                </div>
                                            @else
                                                <div class="col-md-6">
                                                    {!! Form::text('email', $_G['user']->email, ['class' => 'form-control', 'readonly'=>'readonly']) !!}
                                                </div>
                                                <div class="col-md-2">
                                                    <button id="btnEditEmail" class="btn-green-bg-white-text edit-button" type="button">
                                                        <span id="btnEditEmailTxt">{{ trans('common.edit_email') }}</span>
                                                    </button>
                                                    <img id="btnEditEmailLdr" src="http://localhost:8000/sl.gif" alt="" style="display:none; position: absolute;right: 100px;width: 26px;top: 4px;">
                                                </div>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                {{ trans('common.password') }}
                                            </label>
                                            <div class="col-md-2">
                                                <button id="btnEditPassword" class="btn-green-bg-white-text edit-button" type="button">
                                                    <span id="btnEditPasswordTxt">{{ trans('common.edit_password') }}</span>
                                                </button>
                                                <img id="btnEditPasswordLdr" src="http://localhost:8000/sl.gif" alt="" style="display:none; position: absolute;right: 100px;width: 26px;top: 4px;">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            {{--<label class="col-md-3 control-label">--}}
                                                {{--QQ--}}
                                            {{--</label>--}}
                                            {{--<div class="col-md-8">--}}
                                                {{--{!! Form::text('qq_id', $_G['user']->qq_id, ['class' => 'form-control' ]) !!}--}}
                                            {{--</div>--}}

                                            <label class="col-md-3 control-label">
                                                {{trans('common.line')}}
                                            </label>
                                            <div class="col-md-8">
                                                {!! Form::text('line_id', $_G['user']->line_id, ['class' => 'form-control' ]) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                {{ trans('common.wechat') }}
                                            </label>
                                            <div class="col-md-8">
                                                {!! Form::text('wechat_id', $_G['user']->wechat_id, ['class' => 'form-control' ]) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                {{trans('common.skype')}}
                                            </label>
                                            <div class="col-md-8">
                                                {!! Form::text('skype_id', $_G['user']->skype_id, ['class' => 'form-control']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">{{ trans('member.handphone') }}</label>
                                            <div class="col-md-2">
                                                {!! Form::text('callingCode', $code , ['class' => 'form-control', 'readonly', 'id' => 'callingCode', 'placeholder' => 'Country Code']) !!}
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::text('handphone_no', $handPhone, ['class' => 'form-control']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                {{trans('common.coin_address')}}
                                            </label>
                                            <div class="col-md-2">
                                                <button type="button" style="height:34px;width:100%;padding:0px !important;" id="metamask-btn" class="btn btn-sm btn-green-bg-white-text">
                                                    <span id="metamask-btn-txt" >{{ trans('common.get_from_metamask') }}</span>
                                                </button>
                                                <img id="metamask-btn-ldr" src="http://localhost:8000/sl.gif" alt="" style="display:none; position: absolute;right: 82px;width: 26px;top: 4px;">
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::text('moveer_address', $moveerAddress, ['class' => 'form-control', 'id' => 'moveer_address']) !!}
                                            </div>
                                        </div>

                                        <br/>
                                    </div>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <hr class="set-hr-personalInfo-hr12">
                                                <button type="button" class="btn blue set-colorGreen2" id="personal-form-submit-btn">
                                                    <span class="idSpanSaveChange">{{ trans('member.save_modify') }}</span>
                                                    <div style="display: none;" class="lds-ring">
                                                        <div></div><div></div><div></div><div></div>
                                                    </div>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection


@section('footer')
    <script src="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" defer="defer" type="text/javascript"></script>
    <script src="/assets/global/plugins/select2/js/select2.js" defer="defer" type="text/javascript"></script>
    <script src="https://unpkg.com/web3@latest/dist/web3.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    {{--<script src="/custom/js/frontend/personalInformation.js" type="text/javascript"></script>--}}
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDoEgFmAYQIMaZKdeH4waSofQBfF8Ln9yQ&libraries=places"></script>
    @if (app()->getLocale() == 'cn')
        <script src="/assets/global/plugins/select2/js/i18n/zh-CN.js" type="text/javascript"></script>
    @endif
    <script>
        var lang = {
            'select_country' : '{{ trans('common.select_country') }}',
            'unable_to_request' : "{{ trans('common.unable_to_request') }}",
            'select_city' : '{{ trans('common.select_city') }}',
            'select_language' : '{{ trans('common.select_language') }}',
            'select_skill' : '{{ trans('common.select_skill') }}',
        };

        var url ={
            'sendeditemailverification' : '{{ route('frontend.personalinformation.sendeditemailverification') }}',
            'sendeditpasswordverification' : '{{ route('frontend.personalinformation.sendeditpasswordverification') }}'
        }

        var userId = '{{ $_G['user']->id }}';
        function countryCode() {
            var val = $('#city').val();
            $.ajax({
                url: `api/v1/fetchCountryCode?city_id=${val}`,
                dataType: 'json',
                success: function (resp) {
                    $('#callingCode').val('+' + resp.callingCode);
                }
            });
        }

        var tokenAddress = "0xd64521ac0496c609454d4e9ca884096e43e0cec7"
        var tokenSymbol = "MVC"
        var tokenDecimals = 18
        var tokenImage = "{{ asset('images/moveer.png') }}"
    </script>
    <script type="text/javascript" defer="defer">
        +function () {
            $(document).ready(function () {
                $('#personal-form-submit-btn').click(function(){
                    $('.idSpanSaveChange').hide();
                    $('.lds-ring').show();
                });
	            console.log('{{\Auth::user()->city_id}}');
                $('#personal-form').makeAjaxForm({
                    submitBtn: '#personal-form-submit-btn',
                    alertContainer: '#body-alert-container',
                    errorFunction: function(response, statusText, xhr, formElm){
                        console.log('err');
                        console.log(response.responseJSON);
                        $('.err').html('');
                        $('.idSpanSaveChange').show();
                        $('.lds-ring').hide();
                        var html = "<div class='alert alert-danger err'>";
                        html += "<ul style='list-style-type: none;padding-left: 0px;'>";
                        var errors = response.responseJSON && response.responseJSON.errors ? response.responseJSON.errors : response.responseJSON;
                        $.each(errors, function(k, v) {
                            console.log(k);
                            console.log(v[0]);
                            html += `<li> ${v[0]} </li>`;
                        });
                        html += `</ul> </div>`;
                        $('.page-content').prepend(html);
                        $('.err').focus();
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                        $('.blockUI').hide()
                    },
                    afterSuccessFunction: function (response, $el, $this) {
                        console.log('success');
                        window.location.reload();
                    }
                });

                $.ajax({
                    url: '{{ route('backend.coin.getListCoin') }}',
                    success: function(response) {
                        $('#metamask-btn').click(async (event) => {
                            $("#metamask-btn-ldr").show();
                            $("#metamask-btn-txt").hide();
                            if (window.ethereum) {
                                window.web3 = new Web3(ethereum)
                                ethereum.autoRefreshOnNetworkChange = false
                                try {
                                    const accounts = await ethereum.enable()
                                    const account = accounts[0]
                                    $('#moveer_address').val(account)

                                    // add token
                                    const provider = window.web3.currentProvider
                                    $.each(response.data, (index, value) => {
                                        var params = {
                                            "type":"ERC20",
                                            "options":{
                                                "address": value.contract_address,
                                                "symbol": value.symbol,
                                                "decimals": value.decimal,
                                            },
                                        }

                                        if (value.img !== null) {
                                            params.options.image = value.img
                                        }

                                        provider.sendAsync({
                                            method: 'metamask_watchAsset',
                                            params: params,
                                            id: Math.round(Math.random() * 100000),
                                        }, (err, added) => {
                                            console.log('provider returned', err, added)
                                            if (err || 'error' in added) {
                                                swal("{{ trans('common.warning') }}", "{{ trans('common.token_error') }}", "warning")
                                                return
                                            }
                                            swal("{{ trans('common.success') }}", '{{ trans('common.token_added') }}', "success")
                                        })
                                    })
                                    $("#metamask-btn-ldr").hide();
                                    $("#metamask-btn-txt").show();
                                } catch(e) {
                                    swal("{{ trans('common.warning') }}", e.message, "warning")
                                    $("#metamask-btn-ldr").hide();
                                    $("#metamask-btn-txt").show();
                                }

                            } else if (window.web3) {
                                window.web3 = new Web3(web3.currentProvider)
                                ethereum.autoRefreshOnNetworkChange = false
                                try {
                                    const accounts = await web3.eth.getAccounts()
                                    const account = accounts[0]
                                    $('#moveer_address').val(account)

                                    // add token
                                    const provider = window.web3.currentProvider
                                    $.each(response.data, (index, value) => {
                                        var params = {
                                            "type":"ERC20",
                                            "options":{
                                                "address": value.contract_address,
                                                "symbol": value.symbol,
                                                "decimals": value.decimal,
                                            },
                                        }

                                        if (value.img !== null) {
                                            params.options.image = value.img
                                        }

                                        provider.sendAsync({
                                            method: 'metamask_watchAsset',
                                            params: params,
                                            id: Math.round(Math.random() * 100000),
                                        }, (err, added) => {
                                            console.log('provider returned', err, added)
                                            if (err || 'error' in added) {
                                                swal("{{ trans('common.warning') }}", "{{ trans('common.token_error') }}", "warning")
                                                return
                                            }
                                            swal("{{ trans('common.success') }}", '{{ trans('common.token_added') }}', "success")
                                        })
                                    })
                                    $("#metamask-btn-ldr").hide();
                                    $("#metamask-btn-txt").show();
                                } catch(e) {
                                    swal("{{ trans('common.warning') }}", e.message, "warning")
                                    $("#metamask-btn-ldr").hide();
                                    $("#metamask-btn-txt").show();
                                }

                            } else {
                                swal("{{ trans('common.warning') }}", "{{ trans("common.meta_mask_not_installed") }}", "warning")
                                $("#metamask-btn-ldr").hide();
                                $("#metamask-btn-txt").show();
                            }
                        });
                    }
                });

                // this.$http.get('{{ route('backend.coin.getListCoin') }}').then(response => {
                //     var data = response.data;
                //     if (data.status == 'success') {
                //         this.listCoin = data.data
                //     } else {
                //         swal("{{ trans('common.warning') }}", data.message, "warning")
                //     }
                // }).catch(error => {
                //     swal("{{ trans('common.warning') }}", error.message, "warning")
                // })

                //Skill List
                var skillList = {!! $skillList !!};
                var selectedSkillList = {!! $userSelectedSkillArr !!};

                var ms = $('#magicsuggest').magicSuggest({
                    placeholder: lang.select_skill,
                    allowFreeEntries: true,
                    allowDuplicates: false,
                    data: skillList,
                    value: selectedSkillList,
                    method: 'get',
                    selectionRenderer: function (data) {
                        if( data.is_gold_tag == 1 )
                        {
                            return '<span class="badge-gold"></span>' + data.name;
                        }
                        else if(data.is_green_tag == 1)
                        {
                            return '<span class="badge-green"></span>' + data.name;
                        }
                        else
                        {
                            return data.name;
                        }
                    }
                });

                //user skill with golden tag can not remove after finish page load
                jQuery(window).load(function() {
                    var selectedItem = $('.badge-gold').parent();
                    $(selectedItem).addClass('admin-verified');

                    var selectedItem = $('.badge-green').parent();
                    $(selectedItem).addClass('client-verified');
                });

                $('#magicsuggest').on('click', function(c){
                    var selectedItem = $('.badge-gold').parent();
                    $(selectedItem).addClass('admin-verified');

                    var selectedItem = $('.badge-green').parent();
                    $(selectedItem).addClass('client-verified');
                });

                $(ms).on('blur', function(c){
                    var selectedItem = $('.badge-gold').parent();
                    $(selectedItem).addClass('admin-verified');

                    var selectedItem = $('.badge-green').parent();
                    $(selectedItem).addClass('client-verified');
                });

                $(ms).on(
                    'selectionchange', function(e, cb, s){
                        $('#skill_id_list').val(cb.getValue());
                    }
                );

                var languages =
                        {!! $langList !!}


                var lang_id_list = '{{$_G['user']->getUserLanguageList()}}';
                var selectedlang = [];
                if (lang_id_list != "") {
                    selectedlang = lang_id_list.split(",")
                }
                var lms = $('#languageInput').magicSuggest({
                    placeholder: '{{trans('common.select_language')}}',
                    allowFreeEntries: false,
                    allowDuplicates: false,
                    data: languages,
                    value: selectedlang,
                    method: 'get',
                    renderer: function (data) {
                    	var c = data.code ? data.code.toUpperCase() : '';
                        return '<img src="/images/Flags-Icon-Set/24x24/' + c + '.png" />' + data.name;
                    },
                    selectionRenderer: function (data) {
	                    var c = data.code ? data.code.toUpperCase() : '';
                        return '<img src="/images/Flags-Icon-Set/24x24/' + c + '.png" />' + data.name;
                    }
                });

                $(lms).on(
                    'selectionchange', function (e, cb, s) {
                        $('#lang_id_list').val(cb.getValue());
                    }
                );

                $('.profile-userpic').click(function(){
                    $('.uploadAvatar').trigger('click');
                });

                $('#btn-back-user-center').click(function(){
                    var redirectUrl = '{{ route('frontend.usercenter') }}';
                    window.location.href = redirectUrl;
                });

                function formatRepo(state) {
                    if (!state.id) {
                        return state.text;
                    }
                    var $state = $(
                        '<span><img src="/images/Flags-Icon-Set/24x24/' + state.code.toUpperCase() + '.png" class="img-flag" /> ' + state.text + '</span>'
                    );
                    return $state;
                }

                function formatCity(city) {
                    if (!city.id) {
                        return city.text;
                    }
                    return city.text;
                }

                function formatRepoSelection(state) {
                    if (!state.id) {
                        return '<span><img src="/images/Flags-Icon-Set/24x24/' + state.code.toUpperCase() + '.png" class="img-flag" /> ' + state.text + '</span>';
                    }
                    var $state = $(
                            '<span><img src="/images/Flags-Icon-Set/24x24/' + state.code.toUpperCase() + '.png" class="img-flag" /> ' + state.text + '</span>'
                    );
                    return $state;
                }


                $("#country").select2({
                    placeholder: lang.select_country,
                    language: "zh-CN",
                    theme: "bootstrap",
                    ajax: {
                        delay: 500,
                        url: "{{url('api/countries')}}",
                        dataType: 'json',

                        data: function (params) {
                            return {
                                q: params.term,
                            };
                        },
                        processResults: function (data, params) {
                            // parse the results into the format expected by Select2
                            // since we are using custom formatting functions we do not need to
                            // alter the remote JSON data, except to indicate that infinite
                            // scrolling can be used

                            return {
                                results: data,

                            };
                        },
                        cache: true
                    },
//                    escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                    minimumInputLength: 1,
                    templateResult: formatRepo, // omitted for brevity, see the source of this page
//                    templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
                });
                $("#city").select2({
                    placeholder: '{{\Auth::user()->getCityWithCountry()}}',
                    language: "zh-CN",
                    theme: "bootstrap",
	                value: 240,
                    ajax: {
                        url: "{{url('api/cities')}}",
                        dataType: 'json',
                        data: function (params) {
                            return {
                                q: params.term,
                            };
                        },
                        processResults: function (data, params) {
                            // parse the results into the format expected by Select2
                            // since we are using custom formatting functions we do not need to
                            // alter the remote JSON data, except to indicate that infinite
                            // scrolling can be used

                            return {
                                results: data,

                            };
                        },
                        cache: true
                    },
                    minimumInputLength: 2,
                    templateResult: formatCity, // omitted for brevity, see the source of this page
                });

                $('#btnEditEmail').click(function(){
                    $('#btnEditEmailTxt').hide();
                    $('#btnEditEmailLdr').show();

                    $.ajax({
                        url: url.sendeditemailverification,
                        dataType: 'json',
                        data: {
                            userId: userId
                        },
                        method: 'post',
                        error: function () {
                            alert(lang.unable_to_request);
                            $('#btnEditEmailTxt').show();
                            $('#btnEditEmailLdr').hide();
                        },
                        success: function (result) {
                            if(result.status == 'success')
                            {
                                alertSuccess(result.data.msg);
                                $('#btnEditEmailTxt').show();
                                $('#btnEditEmailLdr').hide();
                            }
                        }
                    });
                });

                $('#btnEditPassword').click(function(){
                    $('#btnEditPasswordTxt').hide();
                    $('#btnEditPasswordLdr').show();

	                alertSuccess('Sending Email, Please don\'t refresh the page..!');
                    $.ajax({
                        url: url.sendeditpasswordverification,
                        dataType: 'json',
                        data: {
                            userId: userId
                        },
                        method: 'post',
                        error: function () {
                            $('#btnEditPasswordTxt').show();
                            $('#btnEditPasswordLdr').hide();
                            alert(lang.unable_to_request);
                        },
                        success: function (result) {
                            if(result.status == 'success')
                            {
                                $('#btnEditPasswordTxt').show();
                                $('#btnEditPasswordLdr').hide();
                                alertSuccess(result.data.msg);
                            }
                        }
                    });
                });

                // get shared office
                function getSharedOffice() {
                    $.ajax({
                        url: "{{ route('frontend.personalinformation.sharedoffice.get') }}"
                    })
                    .done(function(response) {
                        console.log("response")
                        console.log(response.data)
                        var data = response.data
                        if (data.length > 0) {
                            var options = ''
                            data.forEach(function(el, index) {
                                options += "<option value='"+el.id+"'>"+el.office_name+"</option>"
                            })

                            $('#meet_up_place').html(options)
                        }

                        getCurrentPosition()
                    })
                    .fail(function() {
                        console.log("error");
                    })
                }
                getSharedOffice()

                // init google map
                function initMap(coord) {
                    // init variable
                    var inputAdditional = {
                        latitude: '{{ $_G['user']->latitude }}',
                        longitude: '{{ $_G['user']->longitude }}',
                        country: '{{ $_G['user']->country_name }}',
                        city: '{{ !is_null($_G['user']->city) ? $_G['user']->city->name : '' }}',
                        address: '{{ $_G['user']->address }}',
                        meet_up_place: '{{ $_G['user']->meet_up_place_id }}'
                    }
                    console.log(inputAdditional)
                    $('#meet_up_place').val(inputAdditional.meet_up_place)

                    function setInputValue() {
                        $('#address').val(inputAdditional.address)
                        $('#latitude').val(inputAdditional.latitude)
                        $('#longitude').val(inputAdditional.longitude)
                    }

                    setInputValue()

                    var map = new google.maps.Map(document.getElementById('map'), {
                        center: coord,
                        zoom: 13
                    })

                    var input = document.getElementById('address')
                    var autocomplete = new google.maps.places.Autocomplete(input)
                    var geocoder = new google.maps.Geocoder();

                    // set marker
                    var marker = new google.maps.Marker({
                        position: coord,
                        map: map,
                        title: 'your address'
                    })

                    // change marker when lat or lng is not null
                    if (inputAdditional.latitude !== null && inputAdditional.longitude !== null) {
                        var coord = {
                            lat: parseFloat(inputAdditional.latitude),
                            lng: parseFloat(inputAdditional.longitude)
                        }
                        marker.setPosition(coord)
                    }

                    // on map click change marker position
                    map.addListener('click', (mapsMouseEvent) => {
                        marker.setPosition(mapsMouseEvent.latLng)
                        inputAdditional.latitude = mapsMouseEvent.latLng.lat()
                        inputAdditional.longitude = mapsMouseEvent.latLng.lng()
                        console.log(inputAdditional)
                        geocoder.geocode({ location: mapsMouseEvent.latLng }, (results, status) => {
                            if (status === "OK") {
                                if (results[0]) {
                                    // get country and city
                                    results[0].address_components.forEach( (element, index) => {
                                        if (element.types.indexOf("administrative_area_level_2") !== -1) {
                                            inputAdditional.city = element.long_name
                                        }

                                        if (element.types.indexOf("country") !== -1) {
                                            inputAdditional.country = element.long_name
                                        }
                                    })

                                    // set address
                                    inputAdditional.address = results[0].formatted_address

                                    setInputValue()
                                } else {
                                    // No results found
                                    swal('{{ trans('common.warning') }}', '{{ trans('common.no_result_found') }}', 'warning')
                                }
                            } else {
                                // "Geocoder failed due to: " + status
                                swal('{{ trans('common.warning') }}', '{{ trans('common.geocoder_failed_due_to') }} : ' + status, 'warning')
                            }
                        })
                    })

                    // autocomplete changed
                    google.maps.event.addListener(autocomplete, 'place_changed', () => {
                        var place = autocomplete.getPlace()

                        // get country and city
                        place.address_components.forEach( (element, index) => {
                            if (element.types.indexOf("administrative_area_level_2") !== -1) {
                                inputAdditional.city = element.long_name
                            }

                            if (element.types.indexOf("country") !== -1) {
                                inputAdditional.country = element.long_name
                            }
                        })

                        // set address
                        inputAdditional.address = place.formatted_address
                        console.log(inputAdditional.address)

                        setInputValue()
                    })
                }

                // get current location
                function getCurrentPosition() {
                    // Try HTML5 geolocation.
                    if (navigator.geolocation) {
                        console.log('location allowed')
                        navigator.geolocation.getCurrentPosition((position) => {
                            console.log(position)
                            coord = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude
                            }

                            initMap(coord)
                        }, () => {
                            console.log('no location access');
                            $.getJSON('https://ipapi.co/json/', (result) => {
                                console.log(result)
                                coord = {
                                    lat: parseFloat(result.latitude),
                                    lng: parseFloat(result.longitude)
                                }
                                initMap(coord)
                            })
                        })
                    } else {
                        console.log('no location access');
                        $.getJSON('https://ipapi.co/json/', (result) => {
                            console.log(result)
                            coord = {
                                lat: parseFloat(result.latitude),
                                lng: parseFloat(result.longitude)
                            }
                            initMap(coord)
                        })
                    }
                }
            });
        }(jQuery);
    </script>
@endsection




