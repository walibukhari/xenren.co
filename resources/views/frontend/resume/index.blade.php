@extends('frontend.layouts.default')

@section('title')
    {{trans('common.resume_title')}}
@endsection

@section('description')
    {{trans('common.resume')}}
@endsection

@section('author')
Xenren
@endsection

@section('url')
    https://www.xenren.co/resume
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
    <link href="/assets/global/plugins/bootstrap-star-rating/css/star-rating.css" media="all" rel="stylesheet" type="text/css"/>
    <link href="/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/global.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/userCenter.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/resume.css" rel="stylesheet" type="text/css" />
    <link href="http://allfont.net/allfont.css?fonts=montserrat-light" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/modalInviteToWork.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/modalSendOffer.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/personalPortfolio.css" rel="stylesheet" type="text/css">
    <link href="/custom/css/frontend/personalResume.css" rel="stylesheet" type="text/css">
    <link href="/custom/css/frontend/modalSetContactInfo.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN PAGE BASE CONTENT -->
            @include('frontend.resume.newUserDetail')
{{--            @include('frontend.resume.adminReview')--}}
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT -->
@endsection

@section('modal')
    <div id="modalInviteToWork" class="modal fade apply-job-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>


    <div id="modalSendOffer" class="modal fade apply-job-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn pull-right" data-dismiss="modal">
                        <i class="fa fa-close" aria-hidden="true"></i>
                    </button>
                    <h4 class="modal-title">
                        {{ trans('common.send_offer_to', [ 'who' => $user->getName() ]) }}
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="modal-alert-container"></div>
                    {!! Form::open(['route' => 'frontend.findexperts.sendoffer.post', 'method' => 'post', 'role' => 'form', 'id' => 'send-offer-form']) !!}
                    <div class="form-body">
                        <div class="form-group">
                            <div id="model-alert-container">
                            </div>
                        </div>

                        <input type="hidden" name="receiverId" value="{{ $user->id }}">

                        <div class="form-group">
                            <label class="bold m-b-20">{{ trans('common.job_type') }}</label>
                            <div class="m-b-20">
                                <div class="radio-item">
                                    <input type="radio" id="ritema" name="pay_type" value="1" checked>
                                    <label for="ritema" class="setRESUMEPAGERETIMA">
                                        <span class="radio-labe1">{{ trans('common.hourly') }}</span>
                                    </label>
                                </div>
                                <br/>
                                <span class="description">{{ trans('common.pay_by_hour_and_verify') }}</span>
                            </div>
                            <div class="m-b-30">
                                <div class="radio-item">
                                    <input type="radio" id="ritemb" name="pay_type" value="2">
                                    <label for="ritemb" class="setRESUMEPAGERETIMA">
                                        <span class="radio-labe1">{{ trans('common.fixed_price') }}</span>
                                    </label>
                                </div>
                                <br/>
                                <span class="description">{{ trans('common.pay_by_the_milestone') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="bold">{{ trans('common.amount') }} ( {{ app()->getLocale() == "cn"? trans('common.cny'): trans('common.usd') }} ) </label>
                            <input type="text" class="form-control amount" name="amount">
                        </div>

                        <div class="form-group m-b-20">
                            <label class="bold">{{ trans('common.link_offer_to_job_post') }}</label>
                            <div class="form-group search-selectbox">
                                @if( $projects != null )
                                    <select id="slcProjectId" class="bs-select form-control" name="projectId">
                                        <option value="0" >{{ trans('common.select_job_post')  }}</option>
                                        @foreach ( $projects as $key => $project)
                                            <option value="{{ $project->id }}" >{{ $project->name }}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <a href="{{ route('frontend.postproject') }}" target="_blank">
                                <span class="create-project">{{ trans('common.create_new_job_and_send_offer') }}</span>
                            </a>
                        </div>

                        <div class="form-group">
                            <label class="bold">{{ trans('common.write_message_to_freelancer') }}</label>
                            @php
                              $name = \Auth::user() ? \Auth::user()->name : '';
                            @endphp
                            <textarea name="senderMessage" class="form-control" cols="50" rows="10">{!! trans('common.example_message_to_freelancer', ['username' => $name ]) !!}</textarea>
                        </div>

                        <div class="form-group col-md-12 setCol12RowRP">
                            <hr>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <div class="pull-left">
                        <button id="btnSubmit" class="btn btn-green-bg-white-text btn-send-invitation">
                            {{ trans('common.send_offer') }}
                        </button>

                        <a data-dismiss="modal">
                            <span class="cancel">{{ trans('common.cancel') }}</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modalSetContactInfo" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content" style="height: auto;">
                <div class="modal-header">
                    <button type="button" class="btn pull-right" data-dismiss="modal">
                        <i class="fa fa-close" aria-hidden="true"></i>
                    </button>
                    <h4 class="modal-title">
                        {{trans('common.contact_info')}}
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">
                        <div class="form-group content-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group setInputGroupMSCI">
                                        <span class="input-group-addon">
                                            <img src="/images/skype-icon-5.png">
                                        </span>
                                        {!! Form::text('skype_id', $user->skype_id, ['class' => 'form-control text-center', 'id' => 'tbxSkypeId', 'placeholder' => 'Skype', 'readonly' => 'readonly']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group setInputGroupMSCI">
                                        <span class="input-group-addon">
                                            <img src="/images/wechat-logo.png">
                                        </span>
                                        {!! Form::text('wechat_id', $user->wechat_id, ['class' => 'form-control text-center', 'id' => 'tbxWechatId', 'placeholder' => trans('common.wechat'), 'readonly' => 'readonly']) !!}
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group setInputGroupMSCI">
                                        <span class="input-group-addon">
                                            <img src="/images/line.png">
                                        </span>
                                        {!! Form::text('line_id', $user->line_id, ['class' => 'form-control text-center', 'id' => 'tbxlineId', 'placeholder' => trans('common.line'), 'readonly' => 'readonly']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group setInputGroupMSCI">
                                        <span class="input-group-addon">
                                            <img src="/images/MetroUI_Phone.png">
                                        </span>
                                        {!! Form::text('handphone_no', $user->handphone_no, ['class' => 'form-control text-center', 'id' => 'tbxHandphoneNo', 'placeholder' => trans('common.phone'), 'readonly' => 'readonly']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script>
        var url = {
            api_isAllowToCheckContact: "{{ route('api.isallowtocheckcontact') }}",
            addFavorite: "{{ route('frontend.addfavouritefreelancer.post') }}",
            removeFavorite: "{{ route('frontend.removefavouritefreelancer.post') }}",
            login : '{{ route('login') }}'
        }

        var lang = {
            unable_to_request : "{{ trans('common.unable_to_request') }}",
        };
    </script>
    <script src="/assets/global/plugins/bootstrap-star-rating/js/star-rating.js" type="text/javascript"></script>
    <script src="/custom/js/frontend/userCenter.js" type="text/javascript"></script>
    <script src="/custom/js/frontend/resume.js" type="text/javascript"></script>

    <script>
        +function () {
            $(document).ready(function () {
                $('#send-offer-form').makeAjaxForm({
                    inModal: true,
                    closeModal: true,
                    submitBtn: '#btnSubmit',
                    alertContainer : '#model-alert-container',
                    afterSuccessFunction: function (response, $el, $this) {
	                    console.log(response);
	                    if( response.status == 'OK'){
                            App.alert({
                                type: 'success',
                                icon: 'check',
                                message: response.msg,
                                place: 'append',
                                container: '#model-alert-container',
                                reset: true,
                                focus: true
                            });
                            alertSuccess(response.msg)
                            $('#modalSendOffer').modal('toggle');
                        } else if(response.status == 'Error'){
                            App.alert({
                                type: 'danger',
                                icon: 'warning',
                                message: response.message,
                                container: '#model-alert-container',
                                reset: true,
                                focus: true
                            });
                            alertError(response.message)
                        } else{
                            App.alert({
                                type: 'danger',
                                icon: 'warning',
                                message: response.message,
                                container: '#model-alert-container',
                                reset: true,
                                focus: true
                            });
                            alertError(response.message)
                        }

                        App.unblockUI('#send-offer-form');
                    }
                });

                $('#add-favorite').click(function(event) {
                    $.ajax({
                        url: url.addFavorite,
                        type: 'POST',
                        data: {freelancerId: '{{ $user->id }}'},
                        success: function(response) {
                            alertSuccess(response.message);
                            setTimeout(function(){
                                window.location.reload();
                            }, 300);
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            alertError(errorThrown);
                            if (errorThrown == "Unauthorized") {
                                setTimeout(function(){
                                    window.location.href = url.login;
                                }, 300);
                            }
                        }
                    });
                });

                $('#remove-favorite').click(function(event) {
                    $.ajax({
                        url: url.removeFavorite,
                        type: 'POST',
                        data: {freelancerId: '{{ $user->id }}'},
                        success: function(response) {
                            alertSuccess(response.message);
                            setTimeout(function(){
                                window.location.reload();
                            }, 300);
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            alertError(errorThrown);
                            if (errorThrown == "Unauthorized") {
                                setTimeout(function(){
                                    window.location.href = url.login;
                                }, 300);
                            }
                        }
                    });
                });
            });
        }(jQuery);
    </script>
    <script>
    window.session == '{{\Session::get('lang')}}';
</script>

    <script src="/custom/js/frontend/findExperts.js" type="text/javascript"></script>
@endsection




