@foreach ($adminReviews as $key => $var)
    <div class="col-md-12 clearfix m-t-20 admin-review p-tb-15">
        <div class="rows col-md-12">
            <div class="col-md-8">
                <div class="col-md-2 p-lr-0">
                    <div class="profile-userpic p-lr-0">
                        <img src="/assets/pages/media/profile/photo2.jpg" class="img-responsive" alt="">
                    </div>
                </div>
                <div class="col-md-6">
                    {{ $var->staff->name }}<br/>
                    Posted on {{ $var->created_at }}
                </div>
            </div>

            <div class="col-md-4">
                @if ($var->location_address != '' && $var->location_address != null)
                    项目地点 ：{{ $var->location_address }}<br/>
                @endif
                <input type="text" class="rating-loading user-rated-val" value="{{ $var->score_overall }}" data-size="12" title="" data-show-clear="false" data-readonly="true">
            </div>
        </div>
        <div class="rows col-md-12 m-t-15">
            @if ($var->files)
                @foreach ($var->files as $k => $v)
                    <div class="col-md-3">
                        <img src="{{ asset($v->path) }}" width="200px" height="150px"/>
                    </div>
                @endforeach
            @endif
        </div>
        @if ($var->comment && $var->comment != '' && $var->comment != null)
            <div class="rows col-md-12 m-t-15">
                <div class="col-md-12">
                    {!! nl2br($var->comment) !!}
                </div>
            </div>
        @endif
        @if ($var->skills && $var->skills != '' && $var->skills != null)
            <div class="rows col-md-12 m-t-15">
                <div class="col-md-12">
                    <ul class="commenter p-l-0 p-t-10">
                        @foreach ($var->skills as $k => $v)
                            <li class="item-skill">{{ $v->skill->getName() }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
    </div>
@endforeach