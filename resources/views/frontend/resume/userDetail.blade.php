<div class="col-md-12 personal-info">
    <div class="row">
        <div class="col-md-6 pull-left p-t-15 f-b-16">
            Personal Info
        </div>
        <div class="col-md-6 pull-right text-right p-t-15">
            @if( \Auth::guard('users')->user() != null )
                @if( $user->id == \Auth::guard('users')->user()->id)
                <a href="{{ route('frontend.personalinformation') }}" class="f-c-blk">
                    <i class="fa fa-edit f-s-25 p-lr-10 p-t-5"></i>
                </a>
                @endif
            @endif
            <i class="fa fa-star f-s-25 p-lr-10 p-t-5 f-c-y"></i>
        </div>
    </div>
    <hr/>

    <div class="row personal-profile">
        <div class="profile-image col-md-2">
            <div class="profile-userpic">
                <img src="{{ $user->getAvatar() }}" class="img-responsive" alt="" style="width: 100px; height: 100px; "/>
            </div>
        </div>
        <div class="user-info col-md-7">
            <div class="basic-info">
                <span class="f-b">
                    {{ $user->getName() }}&emsp;
                </span>
                @if( $user->title != null )
                {{ $user->title }}
                @endif

                @if( $jobPosition != null )
                - <span class="job-remark" title="{{ $jobPosition->getRemark() }}">{{ $jobPosition->getName() }}</span>
                @endif
                <br/>

                {{ $user->age }}
                {{ trans('member.years_old') }}
                &emsp;

                Experience {{ $user->experience }} {{ trans('member.year') }}
                &emsp;&emsp;

                {{ $user->hourly_pay  }}/hr
                <br/>

                <span class="location">
                    {{ $user->address }}
                </span>
                <br/>
                @if ($user->is_validated == 1)
                <span>
                    Already Authenticate<img src="/images/auth-logo.png" class="img-auth" alt="" >
                </span>
                @endif

                @if ($user->status >= 1)
                    <br/>
                    Status : {{ $user->explainStatus() }}
                @endif

                @if ($user->address != '' )
                    <br/>
                    Map :
                    <a href="{{ route('frontend.map.user', $user->id ) }}">
                        <span class="f-s-20">
                            <i class="fa fa-map-marker"></i>
                        </span>
                    </a>
                @endif

                <ul>
                    @foreach ( $userSkills as $key => $userSkill)
                        <li class="item-skill">{{ $userSkill->skill->getName() }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="user-info col-md-3">
            <div class="text-center">
                <button type="submit" class="btn btn-success">
                    Choose
                </button>
                <br/>
                <img src="{{ $qrImage }}" class="img-auth p-t-10" alt="" width="100px" height="100px">
            </div>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-12 f-s-16">Summary</div>
        <div class="col-md-12">
            {{ $user->getAboutMe() }}
        </div>
    </div>

    <div class="row sect-choose p-b-0">
         <ul class="tab p-lr-0 m-b-0">
            <li class="active">
                Summary
            </li>
            <li class="">
                {{ trans('member.comment') }}（984,45）
            </li>
        </ul>
    </div>
    <hr class="hr-line">
    <div class="row">
        <div class="col-md-2 p-lr-0">
            <div class="rate-value">
                4.5
            </div>
        </div>
        <div class="col-md-2 sect-star01 p-lr-0">
            {{--//http://plugins.krajee.com/star-rating-demo-basic-usage--}}
            <input type="text" class="rating-loading personal-rated-val" value="2" data-size="16" title="" data-show-clear="false" data-readonly="true">
            <span class="ttl-eval">27,1321</span>&nbsp;&nbsp;{{ trans('member.how_many_comment') }}
        </div>
        <div class="col-md-5 sect-star02 p-lr-0">
            <div class="col-md-4 star">
                <span class="no-star">1</span>
                <i class="fa fa-star"></i> &nbsp;&nbsp;
                <span class="no-vote">5, 464</span>
            </div>
            <div class="col-md-4 star">
                <span class="no-star">2</span>
                <i class="fa fa-star"></i> &nbsp;&nbsp;
                <span class="no-vote">1, 461</span>
            </div>
            <div class="col-md-4 star">
                <span class="no-star">3</span>
                <i class="fa fa-star"></i> &nbsp;&nbsp;
                <span class="no-vote">5, 464</span>
            </div>
            <br/>
            <div class="col-md-4 star">
                <span class="no-star">4</span>
                <i class="fa fa-star"></i> &nbsp;&nbsp;
                <span class="no-vote">458</span>
            </div>
            <div class="col-md-4 star">
                <span class="no-star">5</span>
                <i class="fa fa-star"></i> &nbsp;&nbsp;
                <span class="no-vote">300</span>
            </div>
        </div>
        <div class="col-md-3 sect-rate-histogram p-lr-0">
            <div class="histo">
                <div class="one histo-rate">
                    <span class="histo-star">
                        1</span>
                    <span class="bar-block">
                        <span id="bar-one" class="bar" >
                        </span>
                    </span>
                </div>
                <div class="two histo-rate">
                    <span class="histo-star">
                        2</span>
                    <span class="bar-block">
                        <span id="bar-two" class="bar" >
                        </span>
                    </span>
                </div>
                <div class="three histo-rate">
                    <span class="histo-star">
                        3</span>
                    <span class="bar-block">
                        <span id="bar-three" class="bar" >
                        </span>
                    </span>
                </div>
                <div class="four histo-rate">
                    <span class="histo-star">
                        4</span>
                    <span class="bar-block">
                        <span id="bar-four" class="bar" >
                        </span>
                    </span>
                </div>
                <div class="five histo-rate">
                    <span class="histo-star">
                        5</span>
                    <span class="bar-block">
                        <span id="bar-five" class="bar" >
                        </span>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="row sect-user-comment">
        @include('frontend.includes.userComment')
        @include('frontend.includes.userComment')
        @include('frontend.includes.userComment')
        @include('frontend.includes.userComment')
    </div>
    <div class="col-md-12 text-center">
        <ul class="pagination">
            <li>
                <a href="javascript:;">
                    <i class="fa fa-angle-left"></i>
                </a>
            </li>
            <li>
                <a href="javascript:;"> 1 </a>
            </li>
            <li>
                <a href="javascript:;"> 2 </a>
            </li>
            <li class="active">
                <a href="javascript:;"> 3 </a>
            </li>
            <li>
                <a href="javascript:;"> 4 </a>
            </li>
            <li>
                <a href="javascript:;"> 5 </a>
            </li>
            <li>
                <a href="javascript:;"> 6 </a>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="fa fa-angle-right"></i>
                </a>
            </li>
        </ul>
    </div>
</div>