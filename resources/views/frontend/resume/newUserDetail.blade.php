<div class="col-md-12 usercenter link-div">
    <a href="{{ $redirectBackUrl }}" class="text-uppercase raleway">
        <span class="fa fa-angle-left set-fa-faangle-left-resume-page">
        </span>
        {!! $redirectBackName  !!}
    </a>
</div>
<!-- <div class="col-md-3 usercenter raleway text-uppercase sidebar-title-div set-usercenter-sidebar">
    <span>{{ trans('common.similar_profile') }}</span>
</div> -->
<div class="clearfix"></div>
<br>
{{--<div class="col-md-9 personal-info setRPICol9">--}}
{{--    <div class="row personal-profile">--}}
{{--        <div class="col-md-12 text-center personal-profile-left">--}}
{{--            <div class="row">--}}
{{--                <div class="col-md-3">--}}
{{--                    <div class="profile-userpic">--}}
{{--                        <img onerror="this.src='/images/MainLogoDefault.png'" src="{{ $user->getAvatar() }}" class="img-responsive setNUDPAVATAR" alt="" style="width:160px; height: 160px;"/>--}}
{{--                    </div>--}}
{{--                    <div class="user-info col-md-12 text-left setRNUStars">--}}
{{--                        <span class="fa fa-star pull-right set-fa-fa-stars-getDetails-page"></span>--}}
{{--                        <span class="fa fa-star pull-right set-fa-fa-stars-getDetails-page"></span>--}}
{{--                        <span class="fa fa-star pull-right set-fa-fa-stars-getDetails-page"></span>--}}
{{--                        <span class="fa fa-star pull-right set-fa-fa-stars-getDetails-page"></span>--}}
{{--                        <span class="fa fa-star pull-right set-fa-fa-stars-getDetails-page"></span>--}}
{{--                        <div class="clearfix"></div>--}}
{{--                        <br>--}}
{{--                    </div>--}}
{{--                    <span class="f-s-12 text-center f-c-gray-2 setNUDFSLT">{{ trans('common.last_login_time') }}</span>--}}
{{--                    <br>--}}
{{--                    <span class="f-s-14 f-c-gray-3 text-center" style="font-style: italic;">{{ $user->getLastLogin() }}</span>--}}

{{--                    <br><br>--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-md-12">--}}
{{--                            <div class="user-info user-info-barcode col-md-12 text-center">--}}
{{--                                <img src="{{ $qr_code }}" class="img-auth pull-right" alt="" width="100px" height="100px">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="user-info col-md-11">--}}
{{--                    <div id="contact-{{$user->id}}" class="contact collapse" data-user-id="{{ isset( $_G['user'] )? $_G['user']->id: ''  }}" data-ip-address="{{ $_SERVER['REMOTE_ADDR'] }}" data-target-user-id="{{$user->id}}">--}}
{{--                        <div class="col-md-12 contacts-info">--}}
{{--                            <div class="col-md-12 col-md-12">--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-md-3 col-md-3 contacts-info-sub">--}}
{{--                                        <img src="/images/skype-icon-5.png">--}}
{{--                                        <span>Skype</span>--}}
{{--                                        <p>{{$user->skype_id}}</p>--}}
{{--                                    </div>--}}

{{--                                    <div class="col-md-3 col-md-3 contacts-info-sub">--}}
{{--                                        <img src="/images/wechat-logo.png">--}}
{{--                                        <span>WeChat</span>--}}
{{--                                        <p>{{$user->wechat_id}}</p>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-3 col-md-3 contacts-info-sub">--}}
{{--                                        <img src="/images/line.png">--}}
{{--                                        <span>Line</span>--}}
{{--                                        <p>{{$user->line_id}}</p>--}}
{{--                                    </div>--}}

{{--                                    <div class="col-md-3 col-md-3 contacts-info-sub">--}}
{{--                                        <img src="/images/MetroUI_Phone.png">--}}
{{--                                        <span>Phone</span>--}}
{{--                                        <p>{{$user->handphone_no}}</p>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="user-info col-md-offset-3 col-md-6">--}}
{{--            <div class="basic-info">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-md-8 setRNUCol8" style="padding-right:0px;">--}}
{{--                        <span class="full-name">{{$user->getName()}}</span>--}}
{{--                        @if( isset($userIdentity) && $userIdentity->isApproved() )--}}
{{--                        <span class="fa fa-check-circle"></span>--}}
{{--                        @endif--}}
{{--                    </div>--}}

{{--                    --}}{{--<div class="col-md-4 m-t-10" style="padding-left:0px;">--}}
{{--                        --}}{{--@if(!empty(\Auth::user()->id))--}}
{{--                        --}}{{--@if( \Auth::user()->id != $user->id )--}}
{{--                        --}}{{--<span class="send-message-sect">--}}
{{--                            --}}{{--<a href="{{ route('frontend.sendprivatemessage', ['user_id' => $user->id ]) }}"--}}
{{--                                --}}{{--data-toggle="modal" data-target="#remote-modal" data-dismiss="modal">--}}
{{--                                --}}{{--<img src="/images/icons/ic_msg-copy.png" style="width: 23px;">--}}
{{--                            --}}{{--</a>--}}
{{--                        --}}{{--</span>--}}
{{--                        --}}{{--@endif--}}
{{--                        --}}{{--@endif--}}
{{--                    --}}{{--</div>--}}
{{--                </div>--}}
{{--                <br>--}}
{{--                --}}{{--Basic info--}}
{{--                <div class="row">--}}
{{--                    <div class="col-md-3">--}}
{{--                        <small>{{trans('common.status')}}:</small>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-9">--}}
{{--                        <span class="status">{{$user->explainStatus()}}</span>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <br>--}}
{{--                <div class="row">--}}
{{--                    <div class="col-md-3">--}}
{{--                        <small>{{trans('common.rate')}}:</small>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-9">--}}
{{--                        <span class="details">{{($user->currencySign)}} {{$user->hourly_pay}} /{{trans('common.hr')}}</span>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <br>--}}
{{--                <div class="row">--}}
{{--                    <div class="col-md-3">--}}
{{--                        <small>{{trans('common.age')}}:</small>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-9">--}}
{{--                        <span class="details">{{$user->age}}</span>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <br>--}}
{{--                <div class="row">--}}
{{--                    <div class="col-md-3">--}}
{{--                        <small>{{trans('common.experience')}}:</small>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-9">--}}
{{--                        <span class="details">{{$user->experience}} {{trans('member.year')}}</span>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                --}}{{--End basic info--}}
{{--                <div class="clearfix"></div>--}}
{{--                <br>--}}
{{--                <img src="/images/hands.png" alt="" class="setRUIImageHands"> &nbsp;<span class="subtitle"> {{trans('common.skill')}}</span>--}}
{{--                <div class="clearfix"></div>--}}
{{--                @foreach($userSkills as $userSkill)--}}
{{--                <div class="skill-div setRNUSkillDiv">--}}
{{--                    @if ( $userSkill->getSkillVerifyStatus() == \App\Models\UserSkill::SKILL_UNVERIFIED )--}}
{{--                    <span class="item-skill unverified">--}}
{{--                        <span>{{$userSkill->skill->getName()}}</span>--}}
{{--                    </span>--}}
{{--                    @elseif ( $userSkill->getSkillVerifyStatus() == \App\Models\UserSkill::SKILL_ADMIN_VERIFIED )--}}
{{--                    <span class="item-skill admin-verified">--}}
{{--                        <img src="/images/crown.png" class="abc" alt="" height="24px" width="24px">--}}
{{--                        <span>{{$userSkill->skill->getName()}}</span>--}}
{{--                    </span>--}}
{{--                    @elseif ( $userSkill->getSkillVerifyStatus() == \App\Models\UserSkill::SKILL_CLIENT_VERIFIED )--}}
{{--                    <span class="item-skill client-verified">--}}
{{--                        <img src="/images/checked.png" class="abc" alt="" height="24px" width="24px">--}}
{{--                        <span>{{$userSkill->skill->getName()}}</span>--}}
{{--                    </span>--}}
{{--                    @endif--}}
{{--               </div>--}}
{{--                @endforeach--}}
{{--                <div class="clearfix"></div>--}}
{{--                <br>--}}
{{--                <img src="/images/earth.png" alt="" class="setRUIImageHands">&nbsp;<span class="subtitle"> {{trans('common.language_2')}}</span>--}}
{{--                <div class="clearfix"></div>--}}
{{--                <div class="media similar-profiles">--}}
{{--                    @foreach($user->languages as $lang)--}}
{{--                        @if(isset($lang->language))--}}
{{--                        <div class="media-left media-middle item-languages">--}}
{{--                            <span>--}}
{{--                                <img class="media-object"--}}
{{--                                    src="{{asset('images/Flags-Icon-Set/24x24/'.strtoupper($lang->language->country->code).'.png')}}"--}}
{{--                                    alt="..."--}}
{{--                                    style="height: 40px; width: 40px;">--}}
{{--                            </span>--}}
{{--                            <span class="pull-left">{{$lang->language->language->name}}</span>--}}
{{--                        </div>--}}
{{--                        @endif--}}
{{--                    @endforeach--}}
{{--                    --}}{{--<div class="media-left media-middle item-languages">--}}
{{--                        --}}{{--<a href="#">--}}
{{--                            --}}{{--<img class="media-object" src="http://placehold.it/50x30" alt="...">--}}
{{--                        --}}{{--</a>--}}
{{--                        --}}{{--<span class="pull-left">English</span>--}}
{{--                    --}}{{--</div>--}}
{{--                    --}}{{--<div class="media-left media-middle item-languages">--}}
{{--                        --}}{{--<a href="#">--}}
{{--                            --}}{{--<img class="media-object" src="http://placehold.it/50x30" alt="...">--}}
{{--                        --}}{{--</a>--}}
{{--                        --}}{{--<span class="pull-left">Germany</span>--}}
{{--                    --}}{{--</div>--}}
{{--                </div>--}}
{{--                <div class="clearfix"></div>--}}
{{--                <br>--}}
{{--                <img src="/images/info.png" alt="" class="setRUIImageHands">&nbsp;<span class="subtitle">{{trans('common.info')}}</span>--}}
{{--                <p>{{ $user->getAboutMe() }}</p>--}}
{{--                <div class="clearfix"></div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="user-info user-info-barcode col-md-3 text-right">--}}
{{--            <div class="col-md-12">--}}
{{--                @if(\Auth::user())--}}
{{--                    <a href="{{ route('frontend.findexperts.invitetowork', ['user_id' => $user->id, 'invitation_project_id' => 0 ] ) }}"--}}
{{--                       data-toggle="modal"--}}
{{--                       data-target="#modalInviteToWork">--}}
{{--                @else--}}
{{--                    <a href="{{ route('login')}}" >--}}
{{--                @endif--}}
{{--                    <button type="button" class="btn btn-success btn-green-bg-white-text setRNUBtn" data-url="{{ route('login') }}">--}}
{{--                        {{ trans('common.invite_for_work') }}--}}
{{--                    </button>--}}
{{--                    </a>--}}
{{--            </div>--}}
{{--            <div class="col-md-12" style="margin-top:14px">--}}
{{--                <button type="button" data-toggle="modal" data-target="#modalSendOffer" class="btn btn-success btn-green-bg-white-text setRNUBtn" data-url="{{ route('login') }}">--}}
{{--                    {{ trans('common.award_job') }}--}}
{{--                </button>--}}
{{--            </div>--}}
{{--            <div class="col-md-12">--}}
{{--                @if( $user->is_contact_allow_view == \App\Models\User::CONTACT_INFO_ALLOW_TO_VIEW )--}}
{{--                    <button type="button" data-toggle="collapse" data-target="#contact-{{$user->id}}" class="btn btn-white-bg-green-text m-t-15 setRNUBtn" >--}}
{{--                        {{ trans('common.contact_info') }}--}}
{{--                    </button>--}}
{{--                @elseif( $user->is_contact_allow_view == \App\Models\User::CONTACT_INFO_NOT_ALLOW_TO_VIEW && \Auth::user())--}}
{{--                    <button type="button" class="btn btn-white-bg-yellow-text send-request-for-contact-info m-t-15 setRNUBtn changeColorSet"--}}
{{--                            data-receiver-id="{{ $user->id }}">--}}
{{--                        {{ trans('common.request_contact_info') }}--}}
{{--                    </button>--}}
{{--                @endif--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
<div class="col-md-9 personal-info setRPICol9">
    <div class="row">
        <div class="col-lg-9">
            <div class="row">
                <div class="col-lg-3">
                    <div class="profile-profile-pic">
                        <img onerror="this.src='/images/avatar_xenren.png'" src="{{ $user->getAvatar() }}" class="img-responsive" alt=""/>
                    </div>
                </div>
                <div class="col-lg-7 set-col-lg-7">
                    <div class="user-name">
                        {{$user->getName()}}
                        @if( isset($userIdentity) && $userIdentity->isApproved() )
                            <span class="fa fa-check-circle"></span>
                        @endif
                    </div>
                    <div class="last-login-time">
                        <span class="f-s-12 text-center f-c-gray-2 setNUDFSLT">{{ trans('common.last_login_time') }}</span>
                        <span class="f-s-14 f-c-gray-3 text-center" style="font-style: italic;">{{ $user->getLastLogin() }}</span>
                    </div>
                    <div class="stars">
                        @for($i=0; $i < floor($total_score); $i++)
                            <span class="fa fa-star pull-right set-fa-fa-stars-getDetails-page"></span>
                        @endfor
                        @if ($total_score > (floor($total_score) + 0.5))
                            <span class="fa fa-star-half-o pull-right set-fa-fa-stars-getDetails-page"></span>

                            @for($i=0; $i < (5 - floor($total_score)) - 1; $i++)
                            <span class="fa fa-star-o pull-right set-fa-fa-stars-getDetails-page"></span>
                            @endfor
                        @else
                            @for($i=0; $i < (5 - floor($total_score)); $i++)
                                <span class="fa fa-star-o pull-right set-fa-fa-stars-getDetails-page"></span>
                            @endfor
                        @endif
                        @if($total_score > 0)
                            &nbsp;<span class="rating-average">{{ $total_score }}</span>
                        @endif
                        &nbsp;&nbsp<span class="total-reviews">({{ count($reviews) > 1000 ? (count($reviews) / 1000).'K' : count($reviews) }} Reviews)</span>

                        <!-- Favorite -->
                        @if(is_null($favorite))
                            <i id="add-favorite" class="fa fa-heart-o" aria-hidden="true"></i>
                        @else
                            <i id="remove-favorite" style="float: right;cursor: pointer;color: #f4b00e" class="fa fa-heart" aria-hidden="true"></i>
                        @endif
                    </div>
                </div>
                <div class="col-lg-2 set-barcode-col-lg-2">
                    <div class="user-barcode-image">
                        <img src="{{ $qr_code }}" class="img-auth pull-right" alt="" width="100px" height="100px">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 desc-col-12-lg">
                    <p class="desc-resume-user-info">
                        {{$user->about_me}}
                    </p>
                </div>
                <br>
                <div class="col-md-12 skill-col-12">
                    <div class="skills-section">
                         <img src="/images/hands.png" alt="" class="setRUIImageHands"> &nbsp;<span class="subtitle"> {{trans('common.skill')}}</span>
                    </div>
                @foreach($userSkills as $userSkill)
                    <div class="skill-div setRNUSkillDiv">
                        @if ( $userSkill->getSkillVerifyStatus() == \App\Models\UserSkill::SKILL_UNVERIFIED )
                            <span class="item-skill unverified">
                                <span>{{$userSkill->skill->getName()}}</span>
                            </span>
                        @elseif ( $userSkill->getSkillVerifyStatus() == \App\Models\UserSkill::SKILL_ADMIN_VERIFIED )
                            <span class="item-skill admin-verified">
                                <img src="/images/crown.png" class="abc" alt="" height="24px" width="24px">
                                <span>{{$userSkill->skill->getName()}}</span>
                            </span>
                        @elseif ( $userSkill->getSkillVerifyStatus() == \App\Models\UserSkill::SKILL_CLIENT_VERIFIED )
                            <span class="item-skill client-verified">
                                <img src="/images/checked.png" class="abc" alt="" height="24px" width="24px">
                                <span>{{$userSkill->skill->getName()}}</span>
                            </span>
                        @endif
                    </div>
                @endforeach
                </div>
            </div>
        </div>
        <div class="col-lg-1 set-col-lg-1">
            <div class="hrVr"></div>
        </div>
        <div class="col-lg-2 set-col-lg-2">
                @if(\Auth::user())
                <a href="{{ route('frontend.findexperts.invitetowork', ['user_id' => $user->id, 'invitation_project_id' => 0 ] ) }}"
                   data-toggle="modal"
                   data-target="#modalInviteToWork">
                    @else
                <a href="{{ route('login')}}" >
                    @endif
                    <button type="button" class="btn btn-success btnInviteWork" data-url="{{ route('login') }}">
                        {{ trans('common.invite_for_work') }}
                    </button>
                </a>
                <br>
                @if(\Auth::user())
                <a href="{{ route('frontend.findexperts.invitetowork', ['user_id' => $user->id, 'invitation_project_id' => 0 ] ) }}"
                   data-toggle="modal"
                   data-target="#modalInviteToWork">
                    @else
                <a href="{{ route('login')}}" >
                    @endif
                    <button type="button" class="btn btn-success btnAwardWork" data-url="{{ route('login') }}">
                        {{ trans('common.award_job') }}
                    </button>
                </a>
                <br>
                <br>
                <b class="color-check">{{trans('common.status')}}</b>
                <span class="status">{!! $user->explainStatus() !!}</span>
                <br>
                <br>
                <b class="color-check">{{trans('common.rate')}}</b>
                <span class="details">{{($user->currencySign)}} {{$user->hourly_pay}} /{{trans('common.hr')}}</span>
                <br>
                <br>
                <b class="color-check">{{trans('common.experience')}}</b>
                <span class="details">{{$user->experience}} {{trans('member.year')}}</span>
                <br>
                <br>
                <b class="color-check">{{trans('common.age')}}</b>
                <span class="details">{{isset($user->date_of_birth) ? $user->date_of_birth : '--'}}</span>
                <br>
                <br>
                <b class="color-check">{{trans('common.languagee')}}</b>
                @foreach($user->languages as $lang)
                    @if(isset($lang->language))
                        <div class="languages-section">
                                    <span>
                                        <img class="image-flag"
                                             src="{{asset('images/Flags-Icon-Set/24x24/'.strtoupper($lang->language->country->code).'.png')}}"
                                             alt="..."
                                             style="height:23px;width:25px;">
                                    </span>
                            <span class="pull-left">{{$lang->language->language->name}}</span>
                        </div>
                @endif
                @endforeach
            <br>
            <br>
            <br>
            @if(!empty($user->skype_id) || !empty($user->wechat_id) || !empty($user->line_id) || !empty($user->handphone_no))
                <button type="button" data-toggle="modal" data-target="#modalSetContactInfo" class="btn btn-default btnContactInfo" >
                    {{ trans('common.contact_info') }}
                </button>
            @else
                <button type="button" class="btn btn-default send-request-for-contact-info m-t-15 setRNUBtn btnContactInfo"
                        data-receiver-id="{{ $user->id }}">
                    {{ trans('common.request_contact_info') }}
                </button>
            @endif
            <!-- TMP DISABLED -->
            {{-- @if( $user->is_contact_allow_view == \App\Models\User::CONTACT_INFO_ALLOW_TO_VIEW ) --}}
                <!-- <button type="button" data-toggle="collapse" data-target="#contact-{{$user->id}}" class="btn btn-default btnContactInfo" >
                    {{ trans('common.contact_info') }}
                </button> -->
            <!-- TMP DISABLED -->
            {{-- @elseif( $user->is_contact_allow_view == \App\Models\User::CONTACT_INFO_NOT_ALLOW_TO_VIEW && \Auth::user()) --}}
                <!-- <button type="button" class="btn btn-default send-request-for-contact-info m-t-15 setRNUBtn btnContactInfo"
                        data-receiver-id="{{ $user->id }}">
                    {{ trans('common.request_contact_info') }}
                </button> -->
            <!-- TMP DISABLED -->
            {{-- @endif --}}
        </div>
    </div>
</div>
<div class="col-md-3 similar-profiles-main set-similar-profiles">
    <div class="usercenter raleway text-uppercase sidebar-title-div set-usercenter-sidebar" style="margin-top: 10px">
        <span>{{ trans('common.similar_profile') }}</span>
    </div>
    <br>
    @foreach($similarUsers as $similarUser)
    <div class="media similar-profiles">
        <div class="media-left media-middle set-media-middle">
            <a href="#">
                <img class="media-object img-circle" src="{{ $similarUser->getAvatar() }}" onerror="this.src='/images/image.png'" alt="..." width="60px" height="60px">
            </a>
        </div>
        <div class="media-body">
            <h5 class="media-heading">
                {{$similarUser->getName()}}
                @if( $similarUser->getLatestIdentityStatus() == \App\Models\UserIdentity::STATUS_APPROVED )
                <span class="fa fa-check-circle"></span>
                @endif
            </h5>
            <span class="raleway">{{$similarUser->getJobPosition()}}</span>
            <br>
            <a href="{{ route('frontend.resume.getdetails', ['id'=>$similarUser->user_link, 'lang' => \Session::get('lang')]) }}" target="_blank">{{ trans('common.more') }}</a>
        </div>
    </div>
    <br>
    @endforeach
    {{--<div class="media similar-profiles">--}}
        {{--<div class="media-left media-middle">--}}
            {{--<a href="#">--}}
                {{--<img class="media-object img-circle" src="http://placehold.it/60x60" alt="...">--}}
            {{--</a>--}}
        {{--</div>--}}
        {{--<div class="media-body">--}}
            {{--<h5 class="media-heading">Adam Johnson <span class="fa fa-check-circle"></span></h5>--}}
            {{--<span>Senior developer</span>--}}
            {{--<br>--}}
            {{--<a href="">more</a>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<br>--}}
    {{--<div class="media similar-profiles">--}}
        {{--<div class="media-left media-middle">--}}
            {{--<a href="#">--}}
                {{--<img class="media-object img-circle" src="http://placehold.it/60x60" alt="...">--}}
            {{--</a>--}}
        {{--</div>--}}
        {{--<div class="media-body">--}}
            {{--<h5 class="media-heading">Adam Johnson <span class="fa fa-check-circle"></span></h5>--}}
            {{--<span>Senior developer</span>--}}
            {{--<br>--}}
            {{--<a href="">more</a>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<br>--}}
    {{--<div class="media similar-profiles">--}}
        {{--<div class="media-left media-middle">--}}
            {{--<a href="#">--}}
                {{--<img class="media-object img-circle" src="http://placehold.it/60x60" alt="...">--}}
            {{--</a>--}}
        {{--</div>--}}
        {{--<div class="media-body">--}}
            {{--<h5 class="media-heading">Adam Johnson <span class="fa fa-check-circle"></span></h5>--}}
            {{--<span>Senior developer</span>--}}
            {{--<br>--}}
            {{--<a href="">more</a>--}}
        {{--</div>--}}
    {{--</div>--}}
</div>
<br>
<div class="clearfix"></div>
<br>
<br>
<div class="col-md-9 review resume-detail-main">
    @include('frontend.userCenter.comment', [
        'is_allow_edit_info' => false,
        'reviews' => $reviews,
        'job_positions' => $job_positions,
        'user' => $user,
        'portfolios' => $portfolios
    ])
    {{--<div class="tabbable-panel">--}}
        {{--<div class="tabbable-line">--}}
            {{--<ul class="nav nav-tabs ">--}}
                {{--<li class="active">--}}
                    {{--<a href="#tab_default_1" class="f-s-16" data-toggle="tab">{{ trans('common.official_comment') }} ({{ $adminReviews->count() }})</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="#tab_default_2" class="f-s-16" data-toggle="tab">{{ trans('common.comment') }} (315)</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="#tab_default_3" class="f-s-16" data-toggle="tab">{{ trans('common.personal_portfolio') }} (0)</a>--}}
                {{--</li>--}}
            {{--</ul>--}}
            {{--<div class="tab-content">--}}
                {{--<div class="tab-pane active" id="tab_default_1">--}}
                    {{--@if(!empty($adminReviews) && $adminReviews->count())--}}
                        {{--@foreach( $adminReviews as $key => $adminReview )--}}
                        {{--<div class="review">--}}
                            {{--<div class="row review-main">--}}
                                {{--<div class="col-md-2 review-main-l text-left">--}}
                                    {{--<img src="/{{ $adminReview->staff->img_avatar }}" alt="..." class="img-circle" width="90px" height="90px">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-10 review-main-r">--}}
                                    {{--<div>--}}
                                        {{--<span class="full-name">{{ $adminReview->staff->username }}--}}
                                            {{--<span class="fa fa-check-circle"></span>--}}
                                            {{--<small class="date">{{ $adminReview->created_at }}</small>--}}
                                        {{--</span>--}}
                                    {{--</div>--}}
                                    {{--<span class="project-id">{{ trans('common.project_Id') }} : {{ $adminReview->official_project_id }}</span><br>--}}
                                    {{--<br>--}}

                                    {{--{!! \App\Constants::getScoreOverallHtml($adminReview->score_overall) !!}--}}

                                    {{--<span class="fa star fa-star"></span>--}}
                                    {{--<span class="fa star fa-star"></span>--}}
                                    {{--<span class="fa star fa-star"></span>--}}
                                    {{--<span class="fa star fa-star-o"></span>--}}
                                    {{--<blockquote>--}}
                                        {{--“{{ $adminReview->comment }}”--}}
                                    {{--</blockquote>--}}

                                    {{--<div class="skill-tag">--}}

                                    {{--</div>--}}

                                    {{--<div class="attachment">--}}

                                        {{--</div>--}}

                                    {{--<div class="attachment-sub">--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<hr>--}}
                        {{--@endforeach--}}
                    {{--@else--}}
                        {{--<div class="personal-portfolio-empty">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-12">--}}
                                    {{--<h5>USER HAVEN"T GOT ANY PORTFOLIO YET</h5>--}}
                                    {{--<img src="/images/icons/potfolio-emptys.png">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--@endif--}}
                {{--</div>--}}

                {{--<div class="tab-pane" id="tab_default_2">--}}
                    {{--<div class="review">--}}
                        {{--<div class="row review-main">--}}
                            {{--<div class="col-md-2 review-main-l text-left">--}}
                                {{--<img src="/images/people-avatar1.png" alt="..." class="img-circle" width="90px"--}}
                                     {{--height="90px">--}}
                            {{--</div>--}}
                            {{--<div class="col-md-10 review-main-r">--}}
                                {{--<div>--}}
                                    {{--<span class="full-name">Adam Rayerrr <span class="fa fa-check-circle"></span><small--}}
                                                {{--class="date">2016-10-10 14:30</small></span>--}}
                                {{--</div>--}}
                                {{--<span class="project-id">Project ID : NGC201354251456</span><br>--}}
                                {{--<br>--}}
                                {{--<span class="fa star fa-star"></span>--}}
                                {{--<span class="fa star fa-star"></span>--}}
                                {{--<span class="fa star fa-star"></span>--}}
                                {{--<span class="fa star fa-star-o"></span>--}}
                                {{--<blockquote>--}}
                                    {{--“It is a long established fact that a reader will be distracted--}}
                                    {{--by the readable content of a page when looking at its layout.--}}
                                    {{--The point of using Lorem Ipsum is that it has a more-or-less--}}
                                    {{--normal distribution of letters, as opposed to using 'Content--}}
                                    {{--here,--}}
                                    {{--content here', making it look like readable English.”--}}
                                {{--</blockquote>--}}

                                {{--<div class="skill-tag">--}}

                                {{--</div>--}}

                                {{--<div class="attachment">--}}

                                {{--</div>--}}

                                {{--<div class="attachment-sub">--}}

                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<hr>--}}
                {{--</div>--}}

                {{--<div class="tab-pane" id="tab_default_3">--}}
                    {{--<div class="personal-portfolio-empty">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-12">--}}
                                {{--<h5>USER HAVEN"T GOT ANY PORTFOLIO YET</h5>--}}
                                {{--<img src="/images/icons/potfolio-emptys.png">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
</div>






