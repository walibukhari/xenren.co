@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.post_project_title') }}
@endsection

@section('keywords')
    {{ trans('common.post_project_keyword') }}
@endsection

@section('description')
    {{ trans('common.post_project_desc') }}
@endsection

@section('author')
    Xenren
@endsection

@section('url')
    https://www.xenren.co/postProject
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection


@section('header')
	<!-- BEGIN PAGE LEVEL PLUGINS -->
{{--	<link href="/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />--}}
	<!-- END PAGE LEVEL PLUGINS -->
{{--    <link href="/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />--}}
{{--	<link href="/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />--}}
{{--	<link href="/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.css" rel="stylesheet" type="text/css" />--}}
{{--	<link href="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="/assets/global/plugins/dropzone-4.0.1/min/dropzone.min.css" rel="stylesheet" type="text/css"/>--}}
{{--    <link href="/assets/global/plugins/dropzone-4.0.1/min/basic.min.css" rel="stylesheet" type="text/css"/>--}}
    <link href="/assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css"/>
{{--    <link href="/custom/css/frontend/postProject.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="/custom/css/frontend/modalAddNewSkillKeywordv2.css" rel="stylesheet" type="text/css" />--}}
    <link href="{{asset('css/PostProject.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('modal')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script>
        $(".js-example-data-array").select2({
            minimumInputLength: 3,
            placeholder: '{{trans('common.select_location')}}',
            ajax: {
                url: '/fetchCity',
                dataType: 'json',
                processResults: function (data) {
                    // Tranforms the top-level key of the response object from 'items' to 'results'
                    return data;
                }
            }
        })
    </script>
    <style>
        a.btn.red.btn-sm.btn-block {
            display: none !important;
        }
        .dz-error-message {
            color: white !important;
         }
    </style>
@endsection

@section('content')

    {{--<div class="ajaxloader" style="display: none">--}}
        {{--<div class="center">--}}
            {{--<img alt="" src="/images/loading.gif" />--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="ajaxloader preloader_bg cls setAJAXLOADER">
        <svg version="1" xmlns="http://www.w3.org/2000/svg"
             xmlns:xlink="http://www.w3.org/1999/xlink"
             width="150px" height="150px" viewBox="0 0 28 28">
            <g class="qp-circular-loader">
                <path class="qp-circular-loader-path" fill="none"
                      d="M 14,1.5 A 12.5,12.5 0 1 1 1.5,14" stroke-width="3"
                      stroke-linecap="round"></path>
            </g>
        </svg>
    </div>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            @include('frontend.flash')
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
            	<div class="col-md-12">
            		<div class="portlet light bordered">
            			<div class="portlet-body form">
							{!! Form::open(['route' => 'frontend.postproject.create', 'id' => 'add-project-form', 'method' => 'post', 'class' => 'horizontal-form', 'files' => true]) !!}
            					<div class="form-wizard" >
            						<div class="form-body">
                                        <div id="project-alert-container"></div>
            							<h4 class="seth4PP">
                                                                    <strong><img style="margin-right:1%" src="{{URL::to('/')}}/images/icons/project_file_icon.png">{{ trans('common.new_project') }} </strong>
            							<hr>
                                        </h4>
            							<div class="tab-content">
            								<div class="alert alert-danger display-none">
            									<button class="close" data-dismiss="alert"></button> {{ trans('order.some_error') }} </div>
            								<div class="alert alert-success display-none">
            									<button class="close" data-dismiss="alert"></button> {{ trans('order.validate_success') }} </div>

                                            <div class="form-group">
                                                <div class="row setPPRow1">
                                                    <div class="col-md-4 text-right setCol4-PP">
                                                        <label class="control-label">{{ trans('common.reuse_previous_job') }}</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="category dropdown">
                                                            <a id="dLabel" role="button" data-toggle="dropdown" class="btn custom-select-btn" data-target="#" href="javascript:;">
                                                                {!! Form::label('prev_project_name', trans('common.please_select'),['class'=>'text-left', 'id'=>'prev_project_name']) !!}
                                                            </a>
                                                            <ul class="project dropdown-menu multi-level setprojectDropDownUl" role="menu" aria-labelledby="dropdownMenu">
                                                                @foreach ($projectList as $project )
                                                                <li projId="{{ $project->id }}" projName=" {{ $project->name }}" data-toggle="modal" data-target="#modalLoadPreviousProject" >
                                                                    <a href="javascript:;">
                                                                        {{ $project->name }}
                                                                    </a>
                                                                </li>
                                                                @endforeach
                                                            </ul>
                                                            {{--<button id="btn-reset-project" type="button" title="Reset selection" class="btn btn-link pull-left close fa fa-close setBTNBTNBTNPPP"></button>--}}
                                                        </div>
                                                            {{--<button id="btn-reset-project" type="button" title="Reset selection" style="display:none;" class="btn btn-link pull-left Setfa-close setBTNBTNBTNPPP"></button>--}}
                                                                <i id="btn-reset-project" style="display:none;" class="fa fa-times Setfa-close setBTNBTNBTNPPP" aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{ Form::hidden('pre_project_id', '0', ['id'=>'pre_project_id']) }}
                                                {{--{{ Form::hidden('pre_project_name', '', ['id'=>'pre_project_name']) }}--}}
                                           </div>

                                            <div class="form-group full-headding-div">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h4 class="seth4PP">
                                                            {{ trans('common.jobs_description') }}
                                                        </h4>
                                                    </div>
                                                </div>
                                           </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4 text-right setCol4-PP">
                                                        <label class="control-label">{{ trans('common.name_your_project_posting') }}</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        {!! Form::text('name', '', [
                                                            'class' => 'form-control custom-input-text',
                                                            'placeholder' => trans('common.project_name'),
                                                            'id'=>'tbxName'
                                                        ]) !!}
                                                        </br>
                                                        <span id="project-name-require" class="error-message"><i class="fa fa-exclamation-circle"></i> {{ trans('common.field_required') }}</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4 text-right setCol4-PP">
                                                        <label class="control-label">{{ trans('common.choose_category_and_subcategory') }}</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="category dropdown">
                                                            <a id="slcCategory" role="button" data-toggle="dropdown" class="btn custom-select-btn" data-target="#" href="/page.html">
                                                                {!! Form::label('category_name', trans('common.please_select'),['class'=>'pull-left', 'id'=>'category_name']) !!}
                                                            </a>
                                                            <ul class=" dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                                                                @foreach ($categories as $key => $category )
                                                                <li class="category dropdown-submenu">
                                                                    <a tabindex="{{ $category->id }}" href="javascript:;">
                                                                        {{ $category->getTitle() }}
                                                                    </a>
                                                                    <ul class="subcategory dropdown-menu">
                                                                        <?php
                                                                            $subcategories = $category->where('parent_id', $category->id )->get();
                                                                        ?>
                                                                        @foreach (  $subcategories as $key => $subCategory )
                                                                        <li catId="{{ $subCategory->id }}" catName=" {{ $category->getTitle() }} > {{ $subCategory->getTitle() }}">
                                                                            <a href="javascript:;">
                                                                                {{ $subCategory->getTitle() }}
                                                                            </a>
                                                                        </li>
                                                                        @endforeach
                                                                    </ul>
                                                                </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                        </br>
                                                        <span id="category-require" class="error-message"><i class="fa fa-exclamation-circle"></i> {{ trans('common.field_required') }}</span>
                                                    </div>

                                                </div>
                                                {{ Form::hidden('category_id', '0', ['id'=>'category_id']) }}
                                                {{ Form::hidden('category_full_name', '', ['id'=>'category_full_name']) }}
                                                {{ Form::hidden('category_menu_allow_close', '0', ['id'=>'category_menu_allow_close']) }}
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4 text-right setCol4-PP">
                                                        <label class="control-label">
                                                            {{ trans('common.describe_the_work_to_be_done') }}
                                                        </label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div id="snDescription"></div>
                                                        {{--{!!--}}
                                                            {{--Form::textarea('description', '', [--}}
                                                                {{--'class' => 'form-control custom-input-text',--}}
                                                                {{--'placeholder' => trans('common.example_job_description'),--}}
                                                                {{--'maxlength' => '5000',--}}
                                                                {{--'rows' => '5',--}}
                                                                {{--'id'=>'tbxDescription'--}}
                                                                {{--])--}}
                                                        {{--!!}--}}
                                                        {!!
                                                            Form::hidden('description', '', [
                                                                'class' => 'form-control custom-input-text',
                                                                'id'=>'tbxDescription'
                                                            ])
                                                        !!}
                                                        <span class="pull-right left-char-count"><span id="noOfCharDescription">5000</span> {{ trans('common.characters_left') }}</span>
                                                        </br>
                                                        <span id="description-require" class="error-message"><i class="fa fa-exclamation-circle"></i> {{ trans('common.field_required') }}</span>
                                                    </div>
                                                </div>
                                            <hr >
                                            </div>

                                            <div class="form-group">
                                                <div class="" data-ng-show="!isDefined(data.attachment)">
                                                    <div class="row">
                                                        <div class="col-md-4 text-right setCol4-PP">
                                                            <label class="control-label">{{ trans('common.add_attachments') }} </label>
                                                        </div>

                                                        <!--
                                                        <div id="attachments-sect" class="col-md-8">
                                                            {{--<label for="PostForm_attachment" class="attachment m-0">--}}
                                                                {{--<div class="fileinput fileinput-new" data-provides="fileinput">--}}
                                                                    {{--<div class="fileinput-exists fileinput-exists-custom" style="float: none">--}}
                                                                        {{--<img src="/images/png.jpg">--}}
                                                                        {{--<span class="fileinput-filename"></span>--}}
                                                                    {{--</div>--}}
                                                                    {{--<span class="btn btn-file btn-add-file-custom">--}}
                                                                        {{--<span class="fileinput-new p-l-1"><i class="fa fa-plus" aria-hidden="true"></i></span>--}}
                                                                        {{--<span class="fileinput-exists p-l-1"><i class="fa fa-plus" aria-hidden="true"></i></span>--}}
                                                                        {{--<input type="file" name="attachment">--}}
                                                                    {{--</span>--}}
                                                                    {{--<span class="btn btn-minus-file-custom" style="display:none">--}}
                                                                        {{--<span class="p-l-1"><i class="fa fa-close" aria-hidden="true"></i></span>--}}
                                                                    {{--</span>--}}
                                                                    {{--<!-- <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a> -->--}}
                                                                {{--</div>--}}
                                                            {{--</label>--}}
                                                            {{--<div class="attachment">--}}
                                                                {{--<span class="btn-minus-file-custom">--}}
                                                                    {{--<span class="p-l-1">--}}
                                                                        {{--<i class="fa fa-close" aria-hidden="true"></i>--}}
                                                                    {{--</span>--}}
                                                                {{--</span>--}}
                                                                {{--<div class="attachment-file">--}}
                                                                    {{--<a href="download?filename=uploads/skillcomment/2017/03/WXTPu2V2IwgTh7rn9amrkYuh5yoqRQpcH1V9ptPe.jpg" target="_blank">--}}
                                                                        {{--<img src="images/jpg.png">--}}
                                                                    {{--</a>--}}
                                                                {{--</div>--}}
                                                                {{--<div class="attachment-name">--}}
                                                                    {{--<a href="">WXTPu....jpg</a>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                            <div id="btn-add-new-file" class="attachment">
                                                                <div class="attachment-no-file">
                                                                    <span class="fileinput-new p-l-1">
                                                                        <span class="btn-file btn-add-file-custom">
                                                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="file-attachments">
                                                        </div>
                                                        -->
                                                        <div class="col-md-8">
                                                            <div class="dropzone" id="portfolio-dropzone">
                                                                <div class="fallback">
                                                                    <input name="file" type="file" multiple />
                                                                </div>
                                                            </div>
                                                            <div id="file-sect"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <hr>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4 text-right setCol4-PP">
                                                        <label class="control-label">
                                                            {{ trans('common.enter_skill_needed_optional') }} <br/>
                                                            <a href="javascript:;" class="f-s-10" data-toggle="modal" data-target="#modelAddNewSkillKeyword" >{{ trans('common.dont_found_the_keyword') }}</a>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div id="magicsuggest"></div>
                                                        {{ Form::hidden('skill_id_list', '', ['id'=>'skill_id_list']) }}

                                                        </br>
                                                        <span id="skill-require" class="error-message"><i class="fa fa-exclamation-circle"></i> {{ trans('common.field_required') }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-4 text-right setCol4-PP">
                                                        <label class="control-label">
                                                            {{ trans('common.enter_nearby_place_optional') }} <br/>
                                                            <a href="javascript:;" class="f-s-10">{{ trans('common.enter_nearby_place_small_optional') }}</a>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        {{--<input type="text" class="form-control" name="nearby_place" id="nearby_place"/>--}}
                                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} e-form" >
                                                            <div class="col-md-12">
                                                                <select name="nearby_place" id="js-example-data-array" class="js-example-data-array form-control">

                                                                </select>
                                                            </div>
                                                        </div>
                                                        </br>
                                                        {{--<span id="skill-require" class="error-message"><i class="fa fa-exclamation-circle"></i> {{ trans('common.field_required') }}</span>--}}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group full-headding-div">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h4 class="seth4PP">
                                                            {{ trans('common.rate_and_availability') }}
                                                        </h4>
                                                    </div>
                                                </div>
                                           </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12 seth4PP">
                                                        <label class="control-label">{{ trans('common.how_would_you_like_to_pay') }}</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    @if(isset($adminSetting) && $adminSetting->value != \App\Models\Project::PAY_TYPE_HOURLY_PAY)
                                                    <div class="col-md-3 pay-type-custom">
                                                        {!! Form::radio('pay_type', '1', false, ['class'=>'icheck pay_ty', 'id' => 'pay-type-1'] ) !!}
                                                        <label class="pay-type-label" for="pay-type-1">
                                                            <img src="/images/clock1.jpg">
                                                            <p>Pay by the hour</p>
                                                        </label>
                                                    </div>
                                                    @endif
                                                    <div class="col-md-3 pay-type-custom">
                                                        {!! Form::radio('pay_type', '2', false, ['class'=>'icheck pay_ty', 'id' => 'pay-type-2'] ) !!}
                                                        <label class="pay-type-label" for="pay-type-2">
                                                            <img src="/images/fixed_price.jpg">
                                                            <p>Pay a fixed price</p>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-3 pay-type-custom">
                                                        {!! Form::radio('pay_type', '3', false, ['class'=>'icheck pay_ty', 'id' => 'pay-type-3'] ) !!}
                                                        <label class="pay-type-label" for="pay-type-3">
                                                            <img src="/images/make_offer.jpg">
                                                            <p>Make Offer</p>
                                                        </label>
                                                    </div>

                                                    <!--@foreach( $pay_types as $key => $pay_type )
                                                    <div class="col-md-3 pay-type-custom">
                                                        {!! Form::radio('pay_type', $key, false, ['class'=>'icheck pay_ty', 'id' => 'pay-type-' . $key ] ) !!}
                                                        <label class="pay-type-label" for="pay-type-{!!$key!!}">
                                                            <img src="/images/clock1.jpg">
                                                            <p>{{ $pay_type }}</p>
                                                        </label>
                                                    </div>
                                                    @endforeach-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                    <span id="pay-require" class="error-message"><i class="fa fa-exclamation-circle"></i> {{ trans('common.field_required') }}</span>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group div-price setPPPAGEDIVP">
                                                <div class="col-md-12">
                                                    <label class="control-label setLABELPPAGE" id="label-div-price-fix">
                                                        @if( app()->getLocale() == "en" )
                                                            {{ trans('common.price_usd') }}
                                                        @elseif( app()->getLocale() == "cn" )
                                                            {{ trans('common.price_cny') }}
                                                        @endif
                                                    </label>
                                                    <label class="control-label setPPPAGEDIVPH" id="label-div-price-hour">
                                                        @if( app()->getLocale() == "en" )
                                                            {{ trans('common.price_usd_per_hour') }}
                                                        @elseif( app()->getLocale() == "cn" )
                                                            {{ trans('common.price_cny_per_hour') }}
                                                        @endif
                                                    </label>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div>
                                                            {!! Form::text('reference_price', '', ['class'=>'form-control', 'id' => 'reference_price' ] ) !!}
                                                            <label for="" id="reference_price_label"></label>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                    </br>
                                                    <span id="fixed-price-require" class="error-message"><i class="fa fa-exclamation-circle"></i> {{ trans('common.field_required') }}</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group setPPFORMG-2">
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-12 seth4PP">
                                                        <label class="control-label">{{ trans('common.what_time_commitment') }}</label>
                                                    </div>
                                                </div>
                                                <div class="rowm">
                                                    <div class="row">
                                                        <div class="col-md-3 pay-type-custom">
                                                            {!! Form::radio('time_commitment', '1', null, ['class'=>'icheck pay_ty','id'=>'time-commitment-1' ] ) !!}
                                                            <label class="pay-type-label" for="time-commitment-1">
                                                                <img src="/images/clock2.jpg">
                                                                <p>{{ trans('common.more_than_30_hrs_per_week') }}</p>
                                                            </label>
                                                        </div>

                                                        <div class="col-md-3 pay-type-custom">
                                                             {!! Form::radio('time_commitment', '2', null, ['class'=>'icheck pay_ty','id'=>'time-commitment-2' ] ) !!}
                                                            <label class="pay-type-label" for="time-commitment-2">
                                                                <img src="/images/clock3.jpg">
                                                                <p>{{ trans('common.less_than_30_hrs_per_week') }}</p>
                                                            </label>
                                                        </div>

                                                        <div class="col-md-3 pay-type-custom">
                                                            {!! Form::radio('time_commitment', '3', null, ['class'=>'icheck pay_ty','id'=>'time-commitment-3' ] ) !!}
                                                            <label class="pay-type-label" for="time-commitment-3">
                                                                <img src="/images/clock4.jpg">
                                                                <p>{{ trans('common.i_dont_know_yet') }}</p>
                                                            </label>
                                                        </div>

                                                        <div class="col-md-3 pay-type-custom">
                                                            {!! Form::radio('time_commitment', '4', null, ['class'=>'icheck pay_ty','id'=>'time-commitment-4' ] ) !!}
                                                            <label class="pay-type-label" for="time-commitment-4">
                                                                <img src="/images/clock5.jpg">
                                                                <p>{{ trans('common.full_time') }}</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                    <div class="col-md-12">
                                                    <span id="time-commitment-require" class="error-message"><i class="fa fa-exclamation-circle"></i> {{ trans('common.field_required') }}</span>
                                                    </div>
                                                </div>
                                                 </div>
                                            </div>

                                            <div class="form-group setPPFORMG-2">
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-12 seth4PP">
                                                        <label class="control-label">{{ trans('common.job_duration') }}</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="row">
                                                        <div class="col-md-3 pay-type-custom">
                                                            {!! Form::radio('time_duration', '4', null, ['class'=>'icheck pay_ty','id'=>'time_duration-4' ] ) !!}
                                                            <label class="pay-type-label" for="time_duration-4">
                                                                <img src="/images/post-project-modal/calender4.png">
                                                                <p>{{ trans('common.with_in_six_month') }}</p>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-3 pay-type-custom">
                                                            {!! Form::radio('time_duration', '3', null, ['class'=>'icheck pay_ty','id'=>'time_duration-3' ] ) !!}
                                                            <label class="pay-type-label" for="time_duration-3">
                                                                <img src="/images/post-project-modal/calender3.png">
                                                                <p>{{ trans('common.with_in_three_month') }}</p>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-3 pay-type-custom">
                                                            {!! Form::radio('time_duration', '2', null, ['class'=>'icheck pay_ty','id'=>'time_duration-2' ] ) !!}
                                                            <label class="pay-type-label" for="time_duration-2">
                                                                <img src="/images/post-project-modal/calender2.png">
                                                                <p>{{ trans('common.with_in_month') }}</p>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-3 pay-type-custom">
                                                            {!! Form::radio('time_duration', '1', null, ['class'=>'icheck pay_ty','id'=>'time_duration-1' ] ) !!}
                                                            <label class="pay-type-label" for="time_duration-1">
                                                                <img src="/images/post-project-modal/calender1.png">
                                                                <p>{{ trans('common.occasionally') }}</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3 pay-type-custom" style="float:left">
                                                            {!! Form::radio('time_duration', '5', null, ['class'=>'icheck pay_ty','id'=>'time_duration-5' ] ) !!}
                                                            <label class="pay-type-label" for="time_duration-5">
                                                                <img src="/images/post-project-modal/permanent.png">
                                                                <p>{{ trans('common.permanent') }}</p>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                    <div class="col-md-12">
                                                    <span id="time-commitment-require" class="error-message"><i class="fa fa-exclamation-circle"></i> {{ trans('common.field_required') }}</span>
                                                    </div>
                                                </div>
                                                 </div>
                                            </div>

                                            <div class="form-group full-headding-div">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h4 class="seth4PP">
                                                            {{ trans('common.freelancer_preferences') }}
                                                        </h4>
                                                    </div>
                                                </div>
                                           </div>

                                            <div class="form-group setPPFORMG-3">
                                                <div class="row">
                                                    <div class="col-md-12 seth4PP">
                                                        <label class="control-label">
                                                            {{ trans('common.do_you_want_freelancers_to_find_and_apply') }}
                                                        </label>
                                                    </div>
                                                    <div class="col-md-8" style="margin-left:20%">
                                                    <div class="col-md-12" name="rdFreelanceType">
                                                        <div class="col-md-12 p-t-10 freelance-type-custom">
                                                            {!! Form::radio('freelance_type', '1', false, ['class'=>'icheck greenradio','id'=>'freelance-type-1']) !!}
                                                            <label class="icheck-label"for="freelance-type-1">
                                                                {{ trans('common.anyone_can_find') }}</label>
                                                        </div>
                                                        <div class="col-md-12 p-t-10 freelance-type-custom">
                                                            {!! Form::radio('freelance_type', '2', false, ['class'=>'icheck greenradio','id'=>'freelance-type-2']) !!}
                                                            <label class="icheck-label"for="freelance-type-2">
                                                                {{ trans('common.only_xenren_users_can_find') }}</label>
                                                        </div>
                                                        <div class="col-md-12 p-t-10 freelance-type-custom">
                                                            {!! Form::radio('freelance_type', '3', false, ['class'=>'icheck greenradio','id'=>'freelance-type-3']) !!}
                                                            <label class="icheck-label"for="freelance-type-3">
                                                                {{ trans('common.only_freelancers_i_have_invited') }}</label>
                                                        </div>
                                                        </br>
                                                        <span id="freelance-type-require" class="error-message"><i class="fa fa-exclamation-circle"></i> {{ trans('common.field_required') }}</span>
                                                    </div>
                                                    </div>

                                                </div>

                                            </div>

                                            <div class="form-group setPPFORMG-4">
                                                <hr>
                                                    <div class="row">
                                                        <div class="col-md-12 seth4PP">
                                                            <label class="control-label"> {{ trans('common.screening_question') }} </label><br/>
                                                            <span class="setSPANPPPAGEAFEW"> {{ trans('common.add_a_few_question_for_candidates_answer') }}</span>
                                                        </div>
                                                        <div class="col-md-10" style="float: right">
                                                            <span class="section-question">
                                                            {{--<div class="col-md-12">--}}
                                                                {{--<div class="form-group" id="question-0"--}}
                                                                {{--name="div-question-box">--}}
                                                                {{--<div class="col-md-8 div-question">--}}
                                                                {{--<textarea class="form-control" placeholder=""--}}
                                                                {{--maxlength="256" rows="3"--}}
                                                                {{--id="tbxQuestion-0" name="question[]"--}}
                                                                {{--cols="50"></textarea>--}}
                                                                        {{--<span class="pull-right">--}}
                                                                        {{--<span id="noOfCharQuestion-">256</span>--}}
                                                                        {{--</span>--}}
                                                                {{--</div>--}}
                                                                {{--<div class="col-md-4 div-close-question">--}}
                                                                        {{--<button id="btn-close-question-0" type="button" class="close pull-left">--}}
                                                                        {{--&times;--}}
                                                                        {{--</button>--}}
                                                                {{--</div>--}}

                                                                {{--</div>--}}
                                                                {{--</div>--}}
                                                            </span>
                                                            <div class="text-right add-button-position">
                                                                <button class="btn btn-add-more-question btn-green-custom" type="button"> <i class="fa fa-plus" aria-hidden="true"></i> </button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="form-group setPPFORMG-5">
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-4 text-right setCol4-PP">
                                                        <label class="control-label">
                                                            {{ trans('common.whether_got_country_request') }} <br/>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div id="msCountries"></div>
                                                        {{ Form::hidden('country_id_list', '', ['id'=>'country_id_list']) }}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group setPPFORMG-6">
                                                <div class="row">
                                                    <div class="col-md-4 text-right setCol4-PP">
                                                        <label class="control-label ">
                                                            {{ trans('common.whether_got_language_request') }} <br/>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div id="msLanguages"></div>
                                                        {{ Form::hidden('language_id_list', '', ['id'=>'language_id_list']) }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group setPPFORMG-7">
                                                <hr>
                                               <div class="submit-position-adjust">
                                                <input type="hidden" name="continue_posting" id="continue_posting" value="0" />
                                                <input type="hidden" name="is_draft" id="is_draft" value="0" />
                                                <input type="hidden" name="is_draft_loaded" id="is_draft_loaded" value="0" />
                                                   <button type="button" id="modal_draft_project" class="btn button-submit btn-green-custom btn-width modal_draft_project" style="background-color: white;color: black;border: 1px solid green !important;">{{ trans('common.make_a_draft') }}</button>
                                                        &nbsp;&nbsp;
                                                   <button type="button" id="project-submit-btn"
                                                        class="btn button-submit btn-green-custom btn-width"> {{ trans('common.post_job') }}
                                                </button>
                                               </div>
                                            </div>

                                            <div class="setPPROJECTPAGEDIV"></div>
                                        </div>
                                    </div>
            					</div>
							{!! Form::close() !!}
            			</div>
            		</div>
            	</div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
    </div>
    <!-- END CONTENT -->
    @include('frontend.includes.modalLoadPreviousProject')
    @include('frontend.includes.modalPersonalInfo')
    @include('frontend.includes.modalAddNewSkillKeywordv2', [
        'tagColor' => \App\Models\Skill::TAG_COLOR_WHITE
    ])
@endsection


@section('footer')
    <script type="text/javascript">

        var lang = {
            type_or_click_here: "{{ trans('common.type_or_click_here') }}",
            characters_left: "{{ trans('common.characters_left') }}",
            please_select: "{{ trans('common.please_select') }}",
            unable_to_request : "{{ trans('common.unable_to_request') }}",
            max_upload_eight_documents : "{{ trans('common.max_upload_eight_documents') }}",
            please_input_project_name : "{{ trans('common.please_input_project_name') }}",
            please_choose_category : "{{ trans('common.please_choose_category') }}",
            please_choose_country : "{{ trans('common.please_choose_country') }}",
            please_choose_language : "{{ trans('common.please_choose_language') }}",
            please_input_description : "{{ trans('common.please_input_description') }}",
            please_choose_skill : "{{ trans('common.please_choose_skill') }}",
            please_choose_pay_type : "{{ trans('common.please_choose_pay_type') }}",
            please_choose_commitment_time : "{{ trans('common.please_choose_commitment_time') }}",
            please_choose_time_duration : "{{ trans('common.please_choose_commitment_time') }}",
            please_choose_freelance_type : "{{ trans('common.please_choose_freelance_type') }}",
            please_input_question : "{{ trans('common.please_input_question') }}",
            please_input_reference_price : "{{ trans('common.please_input_reference_price') }}",
            remove : "{{ trans('common.remove') }}",
            image_is_bigger_than_5mb : "{{ trans('common.image_is_bigger_than_5mb') }}",
            you_can_not_upload_any_more_files : "{{ trans('common.you_can_not_upload_any_more_files') }}",
            drop_files_here_to_upload : "{{ trans('common.drop_files_here_to_upload') }}",
            cancel_upload : "{{ trans('common.cancel_upload') }}",
            invalid_file_type_for_post_project : "{{ trans('common.invalid_file_type_for_post_project') }}",
            example_job_description : "{{ trans('common.example_job_description') }}",
            field_required : "{{ trans('common.field_required') }}",
            new_skill_keywords : "{{ trans('common.new_skill_keywords') }}",
            please_use_wiki_or_baidu_support_your_keyword : "{{ trans('common.please_use_wiki_or_baidu_support_your_keyword') }}",
            one_of_skill_input_empty : "{{ trans('common.one_of_skill_input_empty') }}",
            maximum_ten_skill_keywords_allow_to_submit : "{{ trans('common.maximum_ten_skill_keywords_allow_to_submit') }}",
        };

        var url = {
            getInputFile : "{{ route('frontend.postproject.getinputfile') }}",
            uploadDescriptionImage : "{{ route('frontend.postproject.uploaddescriptionimage') }}",
        };

        {{--var arrUploadFile = {!!$jsonUploadFile!!};--}}
        var fileUploadFeatureType = {{ \App\Models\UploadFile::TYPE_POST_PROJECT }};
    </script>

{{--    <script src="/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>--}}
{{--    <script src="/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>--}}
{{--    <script src="/assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>--}}
{{--    <script src="/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js" type="text/javascript"></script>--}}
{{--    <script src="/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>--}}
    <script src="/assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
{{--    <script src="/assets/global/plugins/bootstrap-summernote/lang/summernote-zh-CN.js" type="text/javascript"></script>--}}
{{--    <script src="/custom/js/frontend/postProject.js?lm=29082017" type="text/javascript"></script>--}}
{{--    <script src="/custom/js/frontend/modalAddNewSkillKeywordv2.js" type="text/javascript"  ></script>--}}
    <script src="/assets/global/plugins/dropzone-4.0.1/dropzone.js" type="text/javascript" ></script>
    <script src="/custom/js/dropzone-config-portfolio.js" type="text/javascript" ></script>
    <script src="{{asset('js/minifiedPostProject.js')}}"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#reference_price').blur(function(){
                let val = $(this).val();
                console.log(val);
                console.log(parseFloat(val).toFixed(2));
                $('#reference_price').val(parseFloat(val).toFixed(2))
            })
            $('.btn-add-more-question').click();

            $('#reference_price').on('keyup', function(){
	            console.log($(this).val());
	            if($(this).val() < 1) {
                    $('#reference_price_label').html('<span style="color:red">{{trans('common.lowest_rate_is')}} 1</span>');
                    $('#project-submit-btn').prop('disabled', true)
                } else if($(this).val() < 3) {
		            $('#reference_price_label').html('<span style="color:orange">{{trans('common.it_will_be')}}</span>');
			            $('#project-submit-btn').prop('disabled', false)
	            } else {
                    $('#reference_price_label').html('');
			            $('#project-submit-btn').prop('disabled', false)
                }
            })
        });
        $("input[name='time_commitment']").on('change', function () {
            if ($(this).val() == '1') {
                $("input[name='time_commitment'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
                $("input[name='time_commitment'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='time_commitment'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='time_commitment'][value='4']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');

            } else if ($(this).val() == '2') {
                $("input[name='time_commitment'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
                $("input[name='time_commitment'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='time_commitment'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='time_commitment'][value='4']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');

            }
            if ($(this).val() == '3') {
                $("input[name='time_commitment'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
                $("input[name='time_commitment'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='time_commitment'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='time_commitment'][value='4']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');

            }
            if ($(this).val() == '4') {
                $("input[name='time_commitment'][value='4']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
                $("input[name='time_commitment'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='time_commitment'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='time_commitment'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');

            }

        });
        $("input[name='time_duration']").on('change', function () {
            if ($(this).val() == '1') {
                $("input[name='time_duration'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
                $("input[name='time_duration'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='time_duration'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='time_duration'][value='4']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='time_duration'][value='5']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
            } else if ($(this).val() == '2') {
                $("input[name='time_duration'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
                $("input[name='time_duration'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='time_duration'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='time_duration'][value='4']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='time_duration'][value='5']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
            }
            if ($(this).val() == '3') {
                $("input[name='time_duration'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
                $("input[name='time_duration'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='time_duration'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='time_duration'][value='4']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='time_duration'][value='5']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
            }
            if ($(this).val() == '4') {
                $("input[name='time_duration'][value='4']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
                $("input[name='time_duration'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='time_duration'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='time_duration'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='time_duration'][value='5']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
            }

            if ($(this).val() == '5') {
                $("input[name='time_duration'][value='5']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
                $("input[name='time_duration'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='time_duration'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='time_duration'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='time_duration'][value='4']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
            }

        });

        $("input[name='pay_type']").on('change', function () {
            if ($(this).val() == '1') {
                $("input[name='pay_type'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
                $("input[name='pay_type'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='pay_type'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');

            } else if ($(this).val() == '2') {
                $("input[name='pay_type'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
                $("input[name='pay_type'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='pay_type'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');

            }
            if ($(this).val() == '3') {
                $("input[name='pay_type'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');
                $("input[name='pay_type'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $("input[name='pay_type'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');

            }

        });
        //                $('.pay_ty').on('focusout', function(){
        //                    $(this).parent('.pay-type-custom').find('.pay-type-label').css('border','1px solid #e1e1e1');
        //
        //                });
        $("input[name='freelance_type']").on('change', function () {
            if ($(this).val() == '1') {
                $("input[name='freelance_type'][value='1']").parent('.freelance-type-custom').find('.icheck-label').css('color', '#57b029');
                $("input[name='freelance_type'][value='2']").parent('.freelance-type-custom').find('.icheck-label').css('color', '#666666');
                $("input[name='freelance_type'][value='3']").parent('.freelance-type-custom').find('.icheck-label').css('color', '#666666');

            } else if ($(this).val() == '2') {
                $("input[name='freelance_type'][value='2']").parent('.freelance-type-custom').find('.icheck-label').css('color', '#57b029');
                $("input[name='freelance_type'][value='1']").parent('.freelance-type-custom').find('.icheck-label').css('color', '#666666');
                $("input[name='freelance_type'][value='3']").parent('.freelance-type-custom').find('.icheck-label').css('color', '#666666');

            }
            if ($(this).val() == '3') {
                $("input[name='freelance_type'][value='3']").parent('.freelance-type-custom').find('.icheck-label').css('color', '#57b029');
                $("input[name='freelance_type'][value='2']").parent('.freelance-type-custom').find('.icheck-label').css('color', '#666666');
                $("input[name='freelance_type'][value='1']").parent('.freelance-type-custom').find('.icheck-label').css('color', '#666666');

            }

        });

    </script>
    <script type="text/javascript">
        function getPreviousProject(id){
            $.ajax({
                url: '{{ route('frontend.getprojectinfo') }}',
                method: 'post',
                data: {'_token': '{{ csrf_token() }}', 'id': id},
                dataType: 'json',
                success: function (resp) {
                    App.unblockUI();
                    if (resp.status == 'success') {
                        var projectInfo = resp.data.project;
                        var skillIdList = resp.data.skillIdList;
                        var categoryName = resp.data.categoryName;
                        var countryIdList = resp.data.countryIdList;
                        var languageIdList = resp.data.languageIdList;

                        $('.Setfa-close').show();

                        //load all the previous project
                        //reused previous job
                        var pre_proj_name = $('#modal_project_name').html();
                        var pre_proj_id = $('#modal_project_id').html();
                        $('#prev_project_name').html(pre_proj_name);

                        //not allow to edit the existing project. only allow create new
                        //0 mean will create new project record
                        $('#pre_project_id').val(0);
//                            $('#pre_project_id').val(pre_proj_id);

                        //project name
                        $('#tbxName').val(projectInfo.name);

                        //category and subcategory
                        $('#category_name').html(categoryName);
                        $('#category_id').val(projectInfo.category_id);

                        //description
                        $('#tbxDescription').val( projectInfo.description );
                        var ttlCharDescription = 5000 - $('#tbxDescription').val().length;
                        $('#noOfCharDescription').html(ttlCharDescription);
                        $('#snDescription').summernote('code', projectInfo.description);

                        //attachment
                        $('#attachments-sect').html(resp.data.attachmentContent);

                        //skills
                        var skillIdList = skillIdList;
                        var ms = $('#magicsuggest').magicSuggest({});
                        ms.clear();
                        ms.setValue(skillIdList);

                        //pay type
                        var pay_type_value = projectInfo.pay_type;
                        $("input[name='pay_type'][value='1']").prop("checked", false);
                        $("input[name='pay_type'][value='2']").prop("checked", false);
                        $("input[name='pay_type'][value='3']").prop("checked", false);
                        $("input[name='pay_type'][value='" + pay_type_value + "']").prop("checked",true);
                        $("input[name='pay_type'][value='" + pay_type_value + "']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');

                        //reference price
                        if( pay_type_value == 1 || pay_type_value == 2 ){

                            $('#reference_price').val(projectInfo.reference_price);
                            $('.div-price').show();

                            if( pay_type_value == 1 )
                            {
                                $('#label-div-price-hour').show();
                            }
                            else if( pay_type_value == 2 )
                            {
                                $('#label-div-price-fix').show();
                            }

                        }else{
                            $('#reference_price').val('');
                            $('.div-price').hide();
                        }

                        //time commitment
                        var time_commitment_value = projectInfo.time_commitment;
                        $("input[name='time_commitment'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                        $("input[name='time_commitment'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                        $("input[name='time_commitment'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                        $("input[name='time_commitment'][value='4']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                        $("input[name='time_commitment'][value='" + time_commitment_value + "']").prop("checked",true);
                        $("input[name='time_commitment'][value='" + time_commitment_value + "']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');

                        //time commitment
                        var time_duration = projectInfo.time_duration;
                        $("input[name='time_duration'][value='1']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                        $("input[name='time_duration'][value='2']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                        $("input[name='time_duration'][value='3']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                        $("input[name='time_duration'][value='4']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                        $("input[name='time_duration'][value='5']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                        $("input[name='time_duration'][value='" + time_duration + "']").prop("checked",true);
                        $("input[name='time_duration'][value='" + time_duration + "']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #58AF2A');

                        //freelance type
                        var freelance_type_value = projectInfo.freelance_type;
                        $("input[name='freelance_type'][value='" + freelance_type_value + "']").prop("checked",true);

                        //reset question
                        $("textarea[id ^='tbxQuestion-']").each(function(){
                            var questionId = parseInt(this.id.replace("tbxQuestion-", ""));
                            $('#question-' + questionId).remove();
                        });

                        //load question
                        $questionList = projectInfo.project_questions;
                        for (i = 0; i < $questionList.length; i++) {

                            //random int
                            var item = Math.floor(1000 + Math.random() * 9000);

                            var result = '<div class="col-md-12">' +
                                '<div class="form-group" id="question-' + item + '" name="div-question-box">' +
                                '<div class="col-md-8 div-question">' +
                                '<textarea class="form-control" placeholder="" maxlength="256" rows="3" id="tbxQuestion-' + item + '" name="question[]" cols="50">' + $questionList[i].question + '</textarea>'+
                                '<span class="pull-right">' +
                                '<span id="noOfCharQuestion-' + item + '">256</span> ' +
                                lang.characters_left +
                                '</span>' +
                                '</div>' +
                                '<div class="col-md-4 div-close-question">' +
                                '<button id="btn-close-question-' + item + '" type="button" class="close pull-left">' +
                                '&times;' +
                                '</button>' +
                                '</div>' +
                                '</div>' +
                                '</div>';
                            $('.section-question').append(result);

                            var ttlCharQuestions = 256 - $('#tbxQuestion-' + item).val().length;
                            $('#noOfCharQuestion-' + item ).html(ttlCharQuestions);
                        }

                        //country
                        var countryIdList = countryIdList;
                        var msCountries = $('#msCountries').magicSuggest({});
                        msCountries.clear();
                        msCountries.setValue(countryIdList);

                        //language
                        var languageIdList = countryIdList;
                        var msLanguages = $('#msLanguages').magicSuggest({});
                        msLanguages.clear();
                        msLanguages.setValue(languageIdList);

                    } else {
                        alertError(resp.msg);
                    }
                },
                error: function (resp) {
                    alert( '{{trans('common.unknown_error') }}' );
                }
            });
        }
        +function () {
            try {
                var draft = JSON.parse('{!! $draft !!}');
                console.log(draft);
                getPreviousProject(draft.id)
                $('#is_draft_loaded').val(1);
            } catch (e) {

            }
            $('.error-message').hide();


            //skill
            var skillList = <?php echo $skillList; ?>;
            var skill_id_list = $('#skill_id_list').val();
            var selectedSkillList = [];
            if(skill_id_list != ""){
                selectedSkillList = skill_id_list.split(",")
            }
            var ms = $('#magicsuggest').magicSuggest({
                allowFreeEntries: false,
                allowDuplicates: false,
                placeholder: lang.type_or_click_here,
                data: skillList,
                value: selectedSkillList,
                method: 'get',
                renderer: function(data){
                    if( data.description == null )
                    {
                        return '<b>' + data.name + '</b>' ;
                    }
                    else
                    {
                        var name = data.name.split("|");

                        return '<b>' + name[0] + '</b> | <span class="search-desc">' + data.description + '</span>';
                    }

                },
                selectionRenderer: function(data){
                    result = data.name;
                    name = result.split("|")[0];
                    return name;
                }
            });
            $(ms).on(
                'selectionchange', function(e, cb, s){
                    $('#skill_id_list').val(cb.getValue());
                    if($('#magicsuggest').hasClass('error-border') && $('input[name="skill_id_list"]').val() != 0){
                        $('#magicsuggest').removeClass('error-border');
                        $("#magicsuggest").attr('style', '');
                        $("#skill-require").hide();
                    }
            });

            //country
            var countryList = <?php echo $countryList; ?>;
            var country_id_list = $('#country_id_list').val();
            var selectedCountryList = [];
            if(country_id_list != ""){
                selectedCountryList = country_id_list.split(",")
            }
            var msCountries = $('#msCountries').magicSuggest({
                allowFreeEntries: false,
                allowDuplicates: false,
                placeholder: lang.type_or_click_here,
                data: countryList,
                value: selectedCountryList,
                method: 'get'
            });
            $(msCountries).on(
                'selectionchange', function(e, cb, s){
                    $('#country_id_list').val(cb.getValue());
                }
            );

            //language
            var languageList = <?php echo $languageList; ?>;
            var language_id_list = $('#language_id_list').val();
            var selectedLanguageList = [];
            if(language_id_list != ""){
                selectedLanguageList = language_id_list.split(",")
            }
            var msLanguages = $('#msLanguages').magicSuggest({
                allowFreeEntries: false,
                allowDuplicates: false,
                placeholder: lang.type_or_click_here,
                data: languageList,
                value: selectedLanguageList,
                method: 'get'
            });
            $(msLanguages).on(
                'selectionchange', function(e, cb, s){
                    $('#language_id_list').val(cb.getValue());
                }
            );

            $('#btn_modal_load').click(function(e){
                $('#modalLoadPreviousProject').modal('toggle');
                $id = $('#modal_project_id').html();

                App.blockUI({
                    boxed: true
                });
                getPreviousProject($id);

            });

            $('#btn-reset-project').click(function(e){
                $('.error-message').hide();
                $('#prev_project_name').html(lang.please_select);
                $('.Setfa-close').hide();
                $('#prev_project_name').html("PLEASE SELECT...");
                //reset previous project

                //reused previous job
                $('#pre_project_id').val(0);

                //project name
                $('#tbxName').val("");

                //category and subcategory
                $('#category_name').html(lang.please_select);
                $('#category_id').val(0);

                //description
                $('#tbxDescription').val("");
                $('#noOfCharDescription').html(5000);
                $('#snDescription').summernote('code', '');

                //attachment
                var addNewFileButton = '<div id="btn-add-new-file" class="attachment">' +
                                            '<div class="attachment-no-file">' +
                                                '<span class="fileinput-new p-l-1">' +
                                                    '<span class="btn-file btn-add-file-custom">' +
                                                        '<i class="fa fa-plus" aria-hidden="true"></i>' +
                                                    '</span>' +
                                                '</span>' +
                                            '</div>' +
                                        '</div>';
                $('#attachments-sect').html(addNewFileButton);


                //skills
                var skillList = [];
                var ms = $('#magicsuggest').magicSuggest({});
                ms.clear();
                ms.setValue(skillList);

                //pay type
                $("input[name='pay_type'][value='1']").prop("checked", false);
                $("input[name='pay_type'][value='2']").prop("checked", false);
                $("input[name='pay_type'][value='3']").prop("checked", false);
                $("input[name='pay_type']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');
                $(".div-price").hide();

                //time commitment
                $("input[name='time_commitment'][value='1']").prop("checked",false);
                $("input[name='time_commitment'][value='2']").prop("checked",false);
                $("input[name='time_commitment'][value='3']").prop("checked",false);
                $("input[name='time_commitment'][value='4']").prop("checked",false);
                $("input[name='time_commitment']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');

                //job durations
                $("input[name='time_duration'][value='1']").prop("checked",false);
                $("input[name='time_duration'][value='2']").prop("checked",false);
                $("input[name='time_duration'][value='3']").prop("checked",false);
                $("input[name='time_duration'][value='4']").prop("checked",false);
                $("input[name='time_duration'][value='5']").prop("checked",false);
                $("input[name='time_duration']").parent('.pay-type-custom').find('.pay-type-label').css('border', '1px solid #e1e1e1');

                //freelance type
                $("input[name='freelance_type'][value='1']").prop("checked",false);
                $("input[name='freelance_type'][value='2']").prop("checked",false);
                $("input[name='freelance_type'][value='3']").prop("checked",false);

                //questions
                $("textarea[id ^='tbxQuestion-']").each(function(){
                    var questionId = parseInt(this.id.replace("tbxQuestion-", ""));
                    $('#question-' + questionId).remove();
                });

                //country
                var countryIdList = [];
                var msCountries = $('#msCountries').magicSuggest({});
                msCountries.clear();
                msCountries.setValue(countryIdList);

                //language
                var languageIdList = [];
                var msLanguages = $('#msLanguages').magicSuggest({});
                msLanguages.clear();
                msLanguages.setValue(languageIdList);
            });

        }(jQuery);
    </script>

@endsection




