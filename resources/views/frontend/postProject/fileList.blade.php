@foreach( $projectFiles as $projectFile)
<div class="attachment upload-file" id="fu-item-{{ $projectFile->id }}">
    <span class="btn-minus-file-custom" id="btn-remove-existing-{{ $projectFile->id }}">
        <span class="p-l-1">
            <i class="fa fa-close" aria-hidden="true"></i>
        </span>
    </span>
    <div class="attachment-file">
        <img src="{{ $projectFile->getFileTypeImage() }}">
    </div>
    <div class="attachment-name text-center">
        <a href="">{{ $projectFile->getShortFilename() }}</a>
    </div>
</div>
@endforeach

<div id="btn-add-new-file" class="attachment">
    <div class="attachment-no-file">
        <span class="fileinput-new p-l-1">
            <span class="btn-file btn-add-file-custom">
                <i class="fa fa-plus" aria-hidden="true"></i>
            </span>
        </span>
    </div>
</div>