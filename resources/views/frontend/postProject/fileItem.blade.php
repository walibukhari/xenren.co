<div class="attachment upload-file" id="fu-item-{{ $itemId }}">
    <span class="btn-minus-file-custom" id="btn-delete-{{ $itemId }}">
        <span class="p-l-1">
            <i class="fa fa-close" aria-hidden="true"></i>
        </span>
    </span>
    <div class="attachment-file">
        <img src="{{ $imageFileType }}">
    </div>
    <div class="attachment-name text-center">
        <a href="">{{ $shortFileName }}</a>
    </div>
</div>
<div id="btn-add-new-file" class="attachment">
    <div class="attachment-no-file">
        <span class="fileinput-new p-l-1">
            <span class="btn-file btn-add-file-custom">
                <i class="fa fa-plus" aria-hidden="true"></i>
            </span>
        </span>
    </div>
</div>