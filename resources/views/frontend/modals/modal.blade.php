<!-- Modal -->
<div class="modal fade" id="{{$id}}" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body confirm-box-model-contant">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <img src="/images/icons/confirmBox.png">
                    </div>
                    <div class="col-md-12 text-center">
                        <p>{{$message}}</p>
                    </div>
                    @if(!isset($reason))
                    <div class="col-md-12 released-milestone">
                        <form>
                            <div class="form-group">
                                <textarea class="form-control refundcomment" rows="5" id="{{isset($commentId) ? $commentId : 'comment'}}" placeholder="Write down all the reasons"></textarea>
                            </div>
                        </form>
                    </div>
                    @endif

                    <div class="col-md-12 confirm-box-model-btn-section">
                        <div class="row">
                            <div class="col-md-6">
                                <button class="btn btn-success btn-green-bg-white-text btn-block {{$successClass}} setMPUPBNTN1" type="button">Confirm</button>
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-success btn-white-bg-green-text btn-block setMPUPBNTN1" type="button"  data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- model end-->
