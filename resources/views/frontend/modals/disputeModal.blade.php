<!-- Modal -->
<style>

    .disputeModal{
        font-family: 'Raleway',Sans-Serif !important;
    }
    .uploadImage {
        background-color: lightgrey;
        height: 80px;
        border-radius: 4px;
        margin-top: 20px;
    }
    .plusIcon {
        display: block !important;
        margin: 0 auto;
        border: 1px solid;
        width: 20px;
        padding: 3px 3px 3px 3px;
        border-radius: 50%;
        color: #61AF2C;
        margin-top: 25px;
        cursor: pointer;
    }
    .confirmBtn {
        display: block;
        margin: 0 auto;
        width: 120px;
    }
    .textforpara {
        color: #61AF2C;
        font-size: 24px;
        margin-top:-6px;
    }
    .setSPNNDMP1{
        color:grey;
    }
    .setMDISPP{
        background-color: white;
    }
    .set-dispute-modal-content{
        width:571px;
        height:713px;
    }
    .setFilDispute{
        font-family: 'Raleway',Sans-Serif !important;
        font-size:36px;
    }
    .setcomment{
        width:517px;
        height:160px !important;
    }
    .setCFormGrp{
        text-align: -moz-center;
        text-align: -webkit-center;
    }
    .thumbImages{
        width: 27%;
        margin: 2px 0px 0px -13px;
    }

    .setGllery{
        margin-left:13px;
    }
</style>
<div class="modal fade disputeModal" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content set-dispute-modal-content">
            <div class="modal-body confirm-box-model-contant setMDISPP">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <img src="/images/icons/questionMarkIcon.png">
                    </div>
                    <div class="col-md-12 text-center">
                        <h1 class="setFilDispute">File a Dispute</h1>
                        <p class="textforpara">{{$message}}</p>
                    </div>
                    <div class="col-md-12 released-milestone">
                        <form>
                            <div class="form-group setCFormGrp">
                                <textarea class="form-control setcomment" rows="5" id="comment" placeholder="Write down all the reasons to file dispute"></textarea>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <span class="modal-label setSPNNDMP1">Screenshot of the reason to file a dispute</span>
                                </div>
                            </div>
                                <div class="form-group gallery setGllery">
                                </div>
                            <br>
                            <div class="col-md-12">
                                <div class="col-sm-3 uploadImage">
                                    <i class="fa fa-plus plusIcon" onclick="$('#screenshot').click()"></i>
                                </div>
                                <input type="file" name="screenshot" id="screenshot" style="display: none" multiple>

                            </div>
                        </form>
                    </div>
                    <div class="col-md-12 confirm-box-model-btn-section">
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-green-bg-white-text btn-block confirmBtn fileDisputeToAdmin" id="" type="button">Send</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade disputeModalEmployer" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body confirm-box-model-contant" style="background-color: white">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <img src="/images/icons/questionMarkIcon.png">
                    </div>
                    <div class="col-md-12 text-center">
                        <h1>File a Dispute</h1>
                        <p class="textforpara">{{$message}}</p>
                    </div>
                    <div class="col-md-12 released-milestone">
                        <form>
                            <div class="form-group">
                                <textarea class="form-control comment disputeComment" rows="5" id="comment" placeholder="Write down all the reasons to file dispute"></textarea>
                            </div>
                            <div class="form-group">
                                <span class="modal-label setSPNNDMP1">Screenshot of the reason to file a dispute</span>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3 uploadImage">
                                    <i class="fa fa-plus plusIcon" onclick="$('#screenshotEmployer').click()"></i>
                                    <input type="file" name="screenshot" id="screenshotEmployer" style="display: none" multiple>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-12 confirm-box-model-btn-section">
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-success btn-green-bg-white-text btn-block confirmBtn fileDisputeEmployerToAdmin" id="" type="button">Send</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- model end-->