<!-- Modal -->
<div class="modal fade confirm-alert confirmationMod" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body confirm-box-model-contant">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <img src="/images/icons/confirmBox.png">
                    </div>
                    <div class="col-md-12 text-center">
                        <p>{{$message}}</p>
                    </div>
                    <div class="col-md-12 confirm-box-model-btn-section">
                        <div class="row">
                            <div class="col-md-6">
                                <button class="btn btn-success btn-green-bg-white-text btn-block setMPUPBNTN1" id="confirmMileStone" type="button">Confirm</button>
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-success btn-white-bg-green-text btn-block setMPUPBNTN1" type="button"  data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- model end-->

<!-- password confirmation modal -->
<div class="modal fade password_show" id="passwordMod" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body confirm-box-model-contant">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <img src="/images/icons/confirmBox.png">
                    </div>
                    <div class="col-md-12 text-center">
                        <input type="password" autocomplete="off" class="form-control" id="password" name="password" placeholder="Please Confirm Your Password">
                    </div>
                    <div>
                        <p id="error" style="color:red"></p>
                    </div>
                    <div class="col-md-12 confirm-box-model-btn-section">
                        <div class="row">
                            <div class="col-md-6">
                                <button class="btn btn-success btn-green-bg-white-text btn-block setMPUPBNTN1" id="confirmMileStonepass" type="button">Confirm</button>
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-success btn-white-bg-green-text btn-block setMPUPBNTN1" type="button"  data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ends here -->