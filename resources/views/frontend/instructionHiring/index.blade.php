@extends('frontend.layouts.default')

@section('title')
{{ trans('common.instruction_title') }}
@endsection

@section('keywords')
{{ trans('common.instruction_keyword') }}
@endsection

@section('description')
{{ trans('common.instruction_desc') }}
@endsection

@section('author')
Xenren
@endsection

@section('header')    
	<link href="/custom/css/frontend/instructionHiring.css" rel="stylesheet" type="text/css" />
@endsection

@section('customStyle')
@endsection

@section('content')
	<div class="page-content">
	    <div class="row step-main-div">
	        <div class="col-md-12 stephedding">
           		<h2>{{ trans('instruction.instruction_title') }}</h2>
           		<span>{{ trans('instruction.instruction_sub_title') }}</span>
			</div>
	        <div class="col-md-12 stepbody">
		        <div class="wizard">
		            <div class="wizard-inner">
		                <div class="connecting-line"></div>
		                <ul class="nav nav-tabs" role="tablist">

		                    <li role="presentation" class="active step1-tab">
		                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab">
		                            <span class="round-tab">1</span>
		                        </a>
		                    </li>

		                    <li role="presentation" class="disabled step2-tab">
		                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab">
		                            <span class="round-tab">2</span>
		                        </a>
		                    </li>
		                    <li role="presentation" class="disabled step3-tab">
		                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab">
		                            <span class="round-tab">3</span>
		                        </a>
		                    </li>

		                    <li role="presentation" class="disabled step4-tab">
		                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab">
		                            <span class="round-tab">4</span>
		                        </a>
		                    </li>
		                </ul>
		            </div>
		            	
		            <div class="wizard-form">
			            <form role="form">
			                <div class="tab-content">
			                    <div class="tab-pane active" role="tabpanel" id="step1">
		                        	<div class="row detailes">
		                        		<div class="col-md-12">
		                        			<h1>{!! trans('instruction.title_st1') !!}</h1>
				                        	<span>
				                        		{!! trans('instruction.desc_st1') !!}
				                        	</span>
				                        	<ul class="textskill">
					                        	<li>
					                        		{!! trans('instruction.li1_b_st1') !!}
					                        		<span>{!! trans('instruction.li1_desc_st1') !!}</span>
					                        	</li>
					                        	<li>
					                        		{!! trans('instruction.li2_b_st1') !!}
					                        		<span>{!! trans('instruction.li2_desc_st1') !!}</span>
					                        	</li>
					                        	<li>
					                        		{!! trans('instruction.li3_b_st1') !!}
					                        		<span>{!! trans('instruction.li3_desc_st1') !!}</span>
					                        	</li>
				                        	</ul>
					                    </div>
					                </div>
		                        	
		                        	<div class="row post">
		                        		<div class="col-md-1 left">
		                        			<div class="img"></div>
		                        		</div>
		                        		<div class="col-md-11 right">
		                        			<div class="title"></div>
		                        			<div class="designation"></div>

		                        			<div class="description-skill">
		                        				<div class="skill-div">
                                                	<span class="item-skill official">
                                                    	<img src="/images/logo_2.png" alt="" height="24px" width="24px">
                                                    	<span>PHP</span>
                                                	</span>
                                            	</div>
                                            	<div class="skill-div">
                                                	<span class="item-skill success">
                                                    	<img src="/images/checked.png" alt="" height="24px" width="24px">
                                                    	<span>JavaScript</span>
                                                	</span>
                                            	</div>
                                            </div>

		                        			<div class="desc"></div>
		                        			<div class="desc"></div>
		                        			<div class="desc"></div>
		                        			<div class="desc-last"></div>
		                        		</div>
		                        	</div>	

		                        	<div class="row detaileslast">
		                        		<div class="col-md-12">
				                        	<span>
				                        		{!! trans('instruction.desc_line1_st1') !!}
				                        	</span> <br>
				                        	<span>
				                        		{!! trans('instruction.desc_line2_st1') !!}
				                        	</span> <br>
				                        	<span>
				                        		{!! trans('instruction.desc_line3_st1') !!}
				                        	</span>
					                    </div>
					                </div>
		                        	<div class="row">
		                        		<div class="col-md-12 text-center">
		                        			<button type="button" class="btn btn-success next-step"><i class="fa fa-arrow-right" aria-hidden="true"></i> {!! trans('instruction.next_stp') !!}</button></li>
		                        		</div>
		                        	</div>
			                    </div>
			                    <div class="tab-pane" role="tabpanel" id="step2">
			                        <div class="row detailes">
		                        		<div class="col-md-12">
		                        			<h1>{!! trans('instruction.title_st2') !!}</h1>
				                        	<ul class="textskill">
					                        	<li>
				                        			{!! trans('instruction.descLine1_st2') !!}
					                        	</li>
					                        	<li>
				                        			{!! trans('instruction.descLine2_st2') !!}
					                        	</li>
					                        	<li>
				                        			{!! trans('instruction.descLine3_st2') !!}
					                        	</li>
				                        	</ul>
					                    </div>
						            </div>
						            <div class="row">
		                        		<div class="col-md-12 screen-img">
		                        			<img src="{!! trans('instruction.img_st2') !!}" width="100%">
		                        		</div>
		                        	</div>	

		                        	<div class="row">
		                        		<div class="col-md-12 text-center">
			                        		<button type="button" class="btn btn-primary next-step nextstep2"><i class="fa fa-arrow-right" aria-hidden="true"></i> {!! trans('instruction.next_stp') !!}</button>
		                        		</div>
		                        	</div>	
			                    </div>
			                    <div class="tab-pane" role="tabpanel" id="step3">
			                        <div class="row detailes">
		                        		<div class="col-md-12">
		                        			<h1>{!! trans('instruction.title_st3') !!}</h1>
				                        	<ul class="textskill">
					                        	<li>
					                        		{!! trans('instruction.descLine1_st3') !!}
					                        	</li>
					                        	<li>
					                        		{!! trans('instruction.descLine2_st3') !!}
					                        	</li>
					                        	<li>
					                        		{!! trans('instruction.descLine3_st3') !!}
					                        	</li>
				                        	</ul>
					                    </div>
					                </div>
					                <div class="row">
		                        		<div class="col-md-12 text-center">
			                        		<button type="button" class="btn btn-default next-step nextstep2"><i class="fa fa-arrow-right" aria-hidden="true"></i> {!! trans('instruction.next_stp') !!}</button>
			                        	</div>
			                        </div>		
			                    </div>
			                    <div class="tab-pane" role="tabpanel" id="complete">
			                    	<div class="row detailes">
		                        		<div class="col-md-12">
		                        			<h1>{!! trans('instruction.title_st4') !!}</h1>
				                        	<ul class="textskill">
					                        	<li>
					                        		{!! trans('instruction.descLine1_st4') !!}
					                        	</li>
					                        	<li>
					                        		{!! trans('instruction.descLine2_st4') !!}
					                        	</li>
					                        	<li>
					                        		{!! trans('instruction.descLine3_st4') !!}
					                        	</li>
					                        	<li>
					                        		{!! trans('instruction.descLine4_st4') !!}
					                        	</li>
					                        	<li>
					                        		{!! trans('instruction.descLine5_st4') !!}					                        		
					                        	</li>
				                        	</ul>
					                    </div>
					                </div>
					                <div class="row">
		                        		<div class="col-md-12 text-center">
											<a href="{{ route('home') }}">
			                        		<button type="button" class="btn btn-success next-step nextstep2">{!! trans('instruction.finish') !!}</button>  </a>
			                        	</div>
			                        </div>		
			                    </div>
			                    <div class="clearfix"></div>
			                </div>
			            </form>
		            </div>
		        </div>
			</div>
		</div>
    </div>
@endsection
@section('footer')    
<script type="text/javascript">
	$(document).ready(function () {
	    //Initialize tooltips
	    $('.nav-tabs > li a[title]').tooltip();
	    
	    //Wizard
	    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

	        var $target = $(e.target);
	    
	        if ($target.parent().hasClass('disabled')) {
	            return false;
	        }
	    });

	    $(".next-step").click(function (e) {

	        var $active = $('.wizard .nav-tabs li.active');
	        $active.next().removeClass('disabled');
	        nextTab($active);

	    });
	    $(".prev-step").click(function (e) {

	        var $active = $('.wizard .nav-tabs li.active');
	        prevTab($active);

	    });
	});

	function nextTab(elem) {
	    $(elem).next().find('a[data-toggle="tab"]').click();
	}
	function prevTab(elem) {
	    $(elem).prev().find('a[data-toggle="tab"]').click();
	}
</script>
@endsection