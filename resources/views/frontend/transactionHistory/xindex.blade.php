@extends('frontend.layouts.default')

@section('title')
    Transaction History
@endsection

@section('keywords')
    Transaction History
@endsection

@section('description')
    Transaction History
@endsection

@section('author')
    Xenren
@endsection

@section('header')
    <link href="/custom/css/frontend/transactionHistory.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3 search-title-container">
            <span class="find-experts-search-title text-uppercase">{{ trans('common.main_action') }}</span>
        </div>
        <div class="col-md-9 right-top-main">
            <div class="row">
                <div class="col-md-12 top-tabs-menu">

                    <div class="caption">
                        <span class="find-experts-search-title text-uppercase">Transaction History</span>
                    </div>
                    <br>
                </div>
            </div>

        </div>
    </div>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
        @include('frontend.flash')
        <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="portlet light bordered transactionHistory-main">
                        <div class="portlet-body">
                            <div class="row filter-section">
                                <div class="col-md-1 col-sm-2 col-xs-2 filter-icon">
                                    <i class="fa fa-filter" aria-hidden="true"></i>
                                </div>
                                <div class="col-md-3 date-input">
                                    <div class="input-icon input-icon-sm">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        <input class="form-control input-sm" type="text" placeholder="March 6 2017">
                                    </div>
                                </div>
                                <div class="col-md-3 date-input">
                                    <div class="input-icon input-icon-sm">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        <input class="form-control input-sm" type="text" placeholder="March 6 2017">
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <button class="btn btn-success btn-green-custom filter-go">Go</button>
                                </div>
                                <div class="col-md-4 date-detail">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;&nbsp;
                                    <span>Last Week</span>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;&nbsp;
                                    <span>Last Month</span>
                                </div>
                            </div>
                            <div class="row balance-part">
                                <div class="col-md-6 balance-part-text">
                                    <span class="balance">BALANCE</span> &nbsp;
                                    <span>0.00<sup>$</sup></span>

                                    <p>Fixed Price Deposits (not included in balance): $0.00</p>
                                </div>
                                <div class="col-md-6 download-btn">
                                    <button class="btn btn-download-red">Download Invoice (zip)</button>
                                    <button class="btn btn-download-white">Download CSV</button>
                                </div>
                            </div>
                            <div class="row search-section">
                                <div class="col-md-4">
                                    <div class="input-icon input-icon-sm">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                        <input class="form-control input-sm" type="text" placeholder="Search...">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="category dropdown">
                                        <a id="dLabel" class="btn custom-select-btn" href="javascript:;" data-target="#" data-toggle="dropdown" role="button" aria-expanded="false">
                                            <button id="btn-reset-project" class="btn btn-link pull-left close fa fa-close setFAFACLOSE" title="Reset selection" type="button"></button>
                                            <label id="prev_project_name" class="pull-left" for="prev_project_name">Please select ...</label>
                                        </a>
                                        <ul class="project dropdown-menu multi-level" aria-labelledby="dropdownMenu" role="menu"> </ul>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="category dropdown">
                                        <a id="dLabel" class="btn custom-select-btn" href="javascript:;" data-target="#" data-toggle="dropdown" role="button" aria-expanded="false">
                                            <button id="btn-reset-project" class="btn btn-link pull-left close fa fa-close setFAFACLOSE" title="Reset selection" type="button"></button>
                                            <label id="prev_project_name" class="pull-left" for="prev_project_name">Please select ...</label>
                                        </a>
                                        <ul class="project dropdown-menu multi-level" aria-labelledby="dropdownMenu" role="menu"> </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row user-block-section">
                                <div class="col-md-6 user-block-mian">
                                    <div class="row user-block">
                                        <div class="col-md-3">
                                            <img src="/images/f-user-icon2.png">
                                        </div>
                                        <div class="col-md-9">
                                            <h4>John Snow</h4>
                                            <p class="red"><i class="fa fa-calendar" aria-hidden="true"></i> 2017 02 23  &nbsp; <span> ID:12345</span></p>
                                            <h5><i class="fa fa-jpy" aria-hidden="true"></i> 12</h5>
                                            <span>Payment Status: <span class="green">Complete</span></span> |
                                            <span class="blue">Horty</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 user-block-mian">
                                    <div class="row user-block">
                                        <div class="col-md-3">
                                            <img src="/images/favouriteFreelancers2.png">
                                        </div>
                                        <div class="col-md-9">
                                            <h4>Sisro</h4>
                                            <p class="red"><i class="fa fa-calendar" aria-hidden="true"></i> 2017 02 23  &nbsp; <span> ID:12345</span></p>
                                            <h5><i class="fa fa-jpy" aria-hidden="true"></i> 12</h5>
                                            <span>Payment Status: <span class="green">Complete</span></span> |
                                            <span class="blue">Horty</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 user-block-mian">
                                    <div class="row user-block">
                                        <div class="col-md-3">
                                            <img src="/images/favouriteFreelancers3.png">
                                        </div>
                                        <div class="col-md-9">
                                            <h4>Jenny</h4>
                                            <p class="red"><i class="fa fa-calendar" aria-hidden="true"></i> 2017 02 23  &nbsp; <span> ID:12345</span></p>
                                            <h5><i class="fa fa-jpy" aria-hidden="true"></i> 12</h5>
                                            <span>Payment Status: <span class="green">Complete</span></span> |
                                            <span class="blue">Horty</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 user-block-mian">
                                    <div class="row user-block">
                                        <div class="col-md-3">
                                            <img src="/images/favouriteFreelancers4.png">
                                        </div>
                                        <div class="col-md-9">
                                            <h4>Elika</h4>
                                            <p class="red"><i class="fa fa-calendar" aria-hidden="true"></i> 2017 02 23  &nbsp; <span> ID:12345</span></p>
                                            <h5><i class="fa fa-jpy" aria-hidden="true"></i> 12</h5>
                                            <span>Payment Status: <span class="green">Complete</span></span> |
                                            <span class="blue">Horty</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 user-block-mian">
                                    <div class="row user-block">
                                        <div class="col-md-3">
                                            <img src="/images/f-user-icon2.png">
                                        </div>
                                        <div class="col-md-9">
                                            <h4>John Snow</h4>
                                            <p class="red"><i class="fa fa-calendar" aria-hidden="true"></i> 2017 02 23  &nbsp; <span> ID:12345</span></p>
                                            <h5><i class="fa fa-jpy" aria-hidden="true"></i> 12</h5>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 user-block-mian">
                                    <div class="row user-block">
                                        <div class="col-md-3">
                                            <img src="/images/favouriteFreelancers2.png">
                                        </div>
                                        <div class="col-md-9">
                                            <h4>Sisro</h4>
                                            <p class="red"><i class="fa fa-calendar" aria-hidden="true"></i> 2017 02 23  &nbsp; <span> ID:12345</span></p>
                                            <h5><i class="fa fa-jpy" aria-hidden="true"></i> 12</h5>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 user-block-mian">
                                    <div class="row user-block">
                                        <div class="col-md-3">
                                            <img src="/images/favouriteFreelancers3.png">
                                        </div>
                                        <div class="col-md-9">
                                            <h4>Jenny</h4>
                                            <p class="red"><i class="fa fa-calendar" aria-hidden="true"></i> 2017 02 23  &nbsp; <span> ID:12345</span></p>
                                            <h5><i class="fa fa-jpy" aria-hidden="true"></i> 12</h5>
                                            <span>Payment Status: <span class="green">Complete</span></span> |
                                            <span class="blue">Horty</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 user-block-mian">
                                    <div class="row user-block">
                                        <div class="col-md-3">
                                            <img src="/images/favouriteFreelancers4.png">
                                        </div>
                                        <div class="col-md-9">
                                            <h4>Elika</h4>
                                            <p class="red"><i class="fa fa-calendar" aria-hidden="true"></i> 2017 02 23  &nbsp; <span> ID:12345</span></p>
                                            <h5><i class="fa fa-jpy" aria-hidden="true"></i> 12</h5>
                                            <span>Payment Status: <span class="green">Complete</span></span> |
                                            <span class="blue">Horty</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection

@section('footer')

@endsection




