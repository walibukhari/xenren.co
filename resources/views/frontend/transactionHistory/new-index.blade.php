@extends('frontend.layouts.default')

@section('title')
	{{trans('common.transaction_history_title')}}
@endsection

@section('keywords')
	{{trans('common.transaction_history')}}
@endsection

@section('description')
	{{trans('common.transaction_history')}}
@endsection

@section('author')
	Xenren
@endsection

@section('url')
	https://www.xenren.co/transactionHistory
@endsection

@section('images')
	https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
	<link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700" rel="stylesheet">
	<link href="/custom/css/frontend/new-transactionHistory.css" rel="stylesheet" type="text/css" />
	{{--<script src="/custom/js/frontend/typeahead.jquery.min.js"></script>--}}
	<link href="/assets/global/plugins/select2/css/select2.css" rel="stylesheet" type="text/css"/>
	<link href="/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
	<script src="/assets/global/plugins/jquery.min.js" type="text/javascript"></script>

@endsection

@section('content')
	<div class="row">
		<div class="col-md-12 right-top-main">
			<div class="row">
				<div class="col-md-12 top-tabs-menu setCol12RowDiv">
					<div class="caption">
						<span class="find-experts-search-title text-uppercase setTRHPH">{{trans('common.transaction_history')}}</span>
					</div>
					<br>
				</div>
			</div>
		</div>
	</div>

	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
		@include('frontend.flash')
		<!-- BEGIN PAGE BASE CONTENT -->
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="portlet light bordered">
						<div class="portlet-body setPLBODYTDPAGE">
							<div class="row invoice-main">
								<div class="col-md-12 col-sm-12 setCol12RowBTN">
									<div class="col-md-6 right-main-part setRowCol6THP">
										<span class="span">{{trans('common.balance')}} </span>
										<h1>{{number_format(round(((float)\Auth::user()->account_balance), 2),2,".","" )}}<sup class="setDOLLARTHP">@if(\Auth::user()->currency == \App\Models\User::CURRENCY_CNY)¥@else$@endif</sup></h1>
										{{--<p>Fixed Price Deposits (not included in balance): <span class="green">$0.00</span></p>--}}
									</div>
									<div class="col-md-6 header-right-btn setROWTHPCol12">
										<button class="btn btn-color-non-custom pull-right setBTNBTNTHP" onclick="window.open('/download-transaction-csv')">{{trans('common.download_CSV')}}</button>
										{{--<button class="btn btn-color-custom pull-right setBTNBTNTHP">Download Invoice (zip)</button>--}}
									</div>
								</div>
								<div class="col-md-12 col-sm-12 search-section">
									<div id="the-basics" class="input-icon input-icon-sm calender-section auto-search-part setSEARCHSECT">
										<i class="fa fa-search input-icon-part" aria-hidden="true"></i>
										<input class="typeahead" id="search" type="text" placeholder="Search...">
									</div>
									<div class="input-icon input-icon-sm calender-section setSMCSEC">
										<i class="fa fa-calendar-o input-icon-part" aria-hidden="true"></i>
										<input class="form-control input-sm search-box box" id="date" type="text" placeholder="01 January 19">
									</div>
									<select class="form-control search-text-box setCol12DIV" id="payment_status">
										<option value="">{{trans('common.payment_status')}}</option>
										<option value="{{\App\Models\Transaction::STATUS_IN_PROGRESS_MILESTONE}}">{{trans('common.in_progress')}}</option>
										<option value="{{\App\Models\Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM}}">{{trans('common.approved')}} - {{trans('common.Processing')}}</option>
										<option value="{{\App\Models\Transaction::STATUS_APPROVED}}">{{trans('common.approved')}}</option>
									</select>
									<select class="form-control search-text-box setCol12DIV" id="payment_category">
										<option value="">{{trans('common.Category')}}</option>
										<option value="{{\App\Models\Transaction::TYPE_PROJECT_PAYMENT}}">
											{{trans('common.project_payment')}}</option>
										<option value="{{\App\Models\Transaction::TYPE_MILESTONE_PAYMENT}}">
											{{trans('common.milestone_payment')}}
										</option>
										<option value="{{\App\Models\Transaction::TYPE_TOP_UP}}">{{trans('common.topup')}}</option>
										<option value="{{\App\Models\Transaction::TYPE_WITHDRAW}}">{{trans('common.Withdraw')}}</option>
										<option value="{{\App\Models\Transaction::TYPE_MILESTONE_PAYMENT_REFUND}}">{{trans('common.refund')}}</option>
									</select>

									<div class="text-input input-text setCol12DIV">
										<input type="text" name="" placeholder="Id" style="font-size: 13px;color: #A9A9A9;" id="transaction_id">
									</div>
									{{--<select class="form-control search-text-box setCol12DIV">--}}
									{{--<option>Currency</option>--}}
									{{--<option>currency 1</option>--}}
									{{--<option>currency 2</option>--}}
									{{--</select>--}}
									<a href="Javascript:;" id="go" class="btn btn-success select-btn setBTNSS">{{trans('common.go')}}</a>
								</div>
								<div class="col-md-12 table-section setTABLETHPAGE">
									<table class="table table-border table-curved">
										<thead class="table-dark">
										<tr class="bg-dark-table">
											<th class="text-left setTLEFTTHP" scope="col">{{trans('common.name')}}</th>
											<th scope="col" class="setTHPAGETH1" >{{trans('common.date')}}</th>
											<th scope="col" class="setTHPAGETH1" >{{trans('common.release')}}</th>
											<th scope="col" class="setTHPAGETH2" >{{trans('common.status')}}</th>
											<th scope="col" class="setTHPAGETH2" >{{trans('common.Category')}}</th>
											<th scope="col" class="setTHPAGETH3" >{{trans('common.ID')}}</th>
											<th scope="col">{{trans('common.amount')}}</th>
											{{--<th scope="col">Balance</th>--}}
										</tr>
										</thead>
										<tbody>
										@php
										$balance = 0;
										@endphp
										@if( count($data) == 0)
											<td class="settdtnh" colspan="7" align="center">
												<img class="setTransactionImage" src="{{asset('images/transcation.jpg')}}"><br>
												<span class="setTransactionspan">No transaction recorded yet</span>
											</td>
										@else
											@foreach($data as $k => $v)
												<tr @if(
										$v['status'] == \App\Models\Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM ||
										$v['type'] == \App\Models\Transaction::TYPE_MILESTONE_PAYMENT && $v['status'] != \App\Models\Transaction::STATUS_APPROVED
										) style="background: lightgray"  @endif>
													<td width="15%">
														<img src="{{$v['image']}}" onerror="this.src='/images/avatar_xenren.png'">
														{{$v['name']}}
													</td>
													<td class="red-text">{{\Carbon\Carbon::parse($v['created_at'])->format('d M Y - h:i')}}</td>
													<td class="red-text">{{is_null($v['release_at']) ? '--': \Carbon\Carbon::parse($v['release_at'])->format('d M Y - h:i') }}</td>
													<td class="green-text">
														{{$v['status_string']}}
													</td>
													<td>
														@if($v['type'] == \App\Models\Transaction::TYPE_MILESTONE_PAYMENT && ($v['status'] == \App\Models\Transaction::STATUS_IN_PROGRESS_MILESTONE || $v['status'] == \App\Models\Transaction::STATUS_APPROVED_BY_CLIENT_HOLD_BY_SYSTEM))
															{{trans('common.milestone_escrow')}}
														@elseif($v['type'] == \App\Models\Transaction::TYPE_SERVICE_FEE)
															2.75% Service Fee
														@elseif($v['type'] == \App\Models\Transaction::TYPE_MILESTONE_PAYMENT && $v['status'] == \App\Models\Transaction::STATUS_APPROVED)
															{{trans('common.milestone_payment')}}
														@elseif($v['type'] == \App\Models\Transaction::TYPE_MILESTONE_PAYMENT_REFUND && $v['status'] == \App\Models\Transaction::STATUS_APPROVED)
															{{trans('common.project_refunded_payment')}}
														@elseif($v['type'] == \App\Models\Transaction::TYPE_PROJECT_PAYMENT)
															{{trans('common.project_payment')}}
														@elseif($v['type'] == \App\Models\Transaction::TYPE_TOP_UP)
															{{trans('common.topup_amount')}}
														@elseif($v['type'] == \App\Models\Transaction::TYPE_WITHDRAW)
															{{trans('common.Withdraw')}}
														@elseif($v['type'] == \App\Models\Transaction::TYPE_BONUS)
															BONUS - $ {{$v['amount']}}
														@elseif($v['type'] == \App\Models\Transaction::TYPE_BOOKING_CHARGES)
															Booking Charges
														@else
															N/A
														@endif
													</td>
													<td>{{$v['id']}}</td>
													<td class="text-center @if($v['payment_type']== \App\Models\Transaction::PAYMENT_TYPE_CREDIT) green-text @else red-text @endif" >{{--<i class="fa fa-jpy" aria-hidden="true"></i>--}}
														<b>{{$v['currency_symbol']}}</b>
														{{number_format(round(((float)$v['amount']), 2),2,".","" )}}
													</td>
													{{--<td>--}}{{--<i class="fa fa-jpy" aria-hidden="true"></i>--}}
													{{--<b>{{$v['currency_symbol']}}</b>--}}
													{{--{{$v['balance']}}--}}
													{{--</td>--}}
												</tr>
											@endforeach
										@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE BASE CONTENT -->
		</div>
		<!-- END CONTENT BODY -->
	</div>
	<!-- END CONTENT -->
	<script src="/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
	<script src="/assets/global/plugins/select2/js/i18n/zh-CN.js" type="text/javascript"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
	<style>
		.select2-selection--single {
			height: 39px !important;
			border-radius: 0 !important;
			border: 2px solid lightgray !important;
		}
		.select2-selection__rendered {
			padding-left: 32px !important;
			padding-top: 4px !important;
		}
	</style>
	<script type="text/javascript">
			var substringMatcher = function(strs) {
				return function findMatches(q, cb) {
					var matches, substringRegex;
					matches = [];
					substrRegex = new RegExp(q, 'i');
					$.each(strs, function(i, str) {
						if (substrRegex.test(str)) {
							matches.push(str);
						}
					});

					cb(matches);
				};
			};
			var clients = '{!! $clients !!}';
			var clients = JSON.parse(clients);
			console.log(clients);
			$('#search').select2({
				data: clients,
				placeholder: '{{trans('common.search')}}...'
			});
			$("#date").datetimepicker({
				viewMode: 'years',
				format: "DD MMMM YY"
			}).on('changeDate', function(ev) {

			});

			$('#go').on('click', function () {
				var employer = $('#search').val();
				var date = $('#date').val();
				var status = $('#payment_status').val();
				var category = $('#payment_category').val();
				var id = $('#transaction_id').val();

				console.log(employer);
				console.log(date);
				console.log(status);
				console.log(category);
				console.log(id);
				window.location.href=`?id=${id}&employer=${employer}&date=${date}&status=${status}&category=${category}`;
			})
//			var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
//				'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
//				'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
//				'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
//				'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
//				'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
//				'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
//				'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
//				'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
//			];

//			$('#the-basics .typeahead').typeahead({
//					hint: true,
//					highlight: true,
//					minLength: 1
//				},
//				{
//					name: 'states',
//					source: substringMatcher(states),
//					templates: {
//						empty: [
//							'<div class="empty-message">',
//							'unable to find any Best Picture winners that match the current query',
//							'</div>'
//						].join('\n'),
//						suggestion: function (data) {
//							console.log();
//							return '<a href="" class="list-group-item"><img src="/images/favouriteFreelancers4.png" class="auto-img-section">'+data+'</a>';
//						}
//					}
//				});

	</script>
@endsection

