@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.my_bank_account_title') }}
@endsection

@section('keywords')
    {{ trans('common.my_bank_account_keyword') }}
@endsection

@section('description')
    {{ trans('common.my_bank_account_desc') }}
@endsection

@section('author')
    Xenren
@endsection

@section('url')
    https://www.xenren.co/myAccount
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection


@section('header')
    <link href="../assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
    <link href="../custom/css/frontend/myAccount.css" rel="stylesheet" type="text/css" />
    <script src="https://checkout.stripe.com/checkout.js"></script>
@endsection


@section('content')
<div class="row">
    <div class="col-12">
        <input type="text" name="amount" id="amount" class="form-control">
    </div>
    <div class="col-12">
        <button type="submit" class="btn btn-success" id="customButton">Submit</button>
    </div>
</div>
<script>
    var stripeHandler = StripeCheckout.configure({
        key: '{{config("services.stripe.key")}}',
        image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
        locale: 'auto',
        token: function(token) {
            let amount = $('#amount').val();
            // $(".ajaxloader").show();
            console.log('in token now',token);
            var form = new FormData();
            form.append("data", JSON.stringify(token));
            form.append("amount", amount);

            var settings = {
                "url": "/stripe/charge",
                "method": "POST",
                "data": form,
                "processData": false,
                "contentType": false,
                "mimeType": "multipart/form-data",
            };

            $.ajax(settings).done(function (response) {
                console.log('charge response',response);
            });
        }
    });
    document.getElementById('customButton').addEventListener('click', (e) => {
        let amount = $('#amount').val();
        console.log(stripeHandler);
        stripeHandler.open({
            image: '/Images/FavIcon.ico',
            description: 'TopUp',
            name: 'XenRen',
            amount: amount*100
        })
        e.preventDefault();
    })
    window.addEventListener('popstate',() => {
        stripeHandler.close();
    })
</script>
@endsection

@section('customStyle')
    <style type="text/css">
        .page-content-wrapper{
            margin-top: 15px;
        }
        /* The container */
        .containerPayCheckBox {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 22px;
            float: right;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        /* Hide the browser's default checkbox */
        .containerPayCheckBox input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }
        /* Create a custom checkbox */
        .checkmarkPay {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
            border-radius: 50%;
        }
        /* On mouse-over, add a grey background color */
        .containerPayCheckBox:hover input ~ .checkmark {
            background-color: #ccc;
            border: solid white;
        }
        /* When the checkbox is checked, add a blue background */
        .containerPayCheckBox input:checked ~ .checkmarkPay {
            background-color: #57a224;
            background: #57a224;
            border-radius: 50%;
        }
        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmarkPay:after {
            content: "";
            position: absolute;
            display: none;
            border: solid white;
        }
        /* Show the checkmark when checked */
        .containerPayCheckBox input:checked ~ .checkmarkPay:after {
            display: block;
        }
        /* Style the checkmark/indicator */
        .containerPayCheckBox .checkmarkPay:after {
            left: 10px;
            top: 7px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
        #modalDialoge{
            width:30%;
        }
        .roww{
            padding-right: 10px;
            padding-left: 10px;
        }
        .btn-Green:hover {
            background: #fff;
            color:#57a224;
            width:100%;
            border: 1px solid #57a224 !important;
            padding: 16px;
        }
        .btn-Green{
            background: #57a224;
            color:#fff;
            width:100%;
            padding: 16px;
            border: 1px solid #57a224 !important;
        }
        .btn-Green2{
            background: #fff;
            color:#57a224;
            width:100%;
            border: 1px solid #57a224 !important;
            padding: 16px;
        }
        .btn-Green2:hover{
            background: #57a224;
            color:#fff;
            width:100%;
            padding: 16px;
        }
        #modalHeader {
            border-bottom: 0px;
            padding-top: 25px;
        }
        #modalContent{
            border-radius: 2%;
        }
        #modalBody{
            padding-top: 0px;
        }
        .pp{
            color:gray;
            font-size: 16px;
            padding-left: 10px;
            margin-bottom: 6px;
        }
        #modalTitle{
            font-weight: 500;
            font-size: 22px;
            padding-left: 10px;
        }
        .spanText{
                font-size: 16px;
            padding-left: 10px;
            }
    </style>
@endsection