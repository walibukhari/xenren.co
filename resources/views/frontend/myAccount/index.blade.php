@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.my_bank_account_title') }}
@endsection

@section('keywords')
    {{ trans('common.my_bank_account_keyword') }}
@endsection

@section('description')
    {{ trans('common.my_bank_account_desc') }}
@endsection

@section('author')
    Xenren
@endsection

@section('url')
    https://www.xenren.co/myAccount
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection


@section('header')
    <link href="../assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
    <link href="../custom/css/frontend/myAccount.css" rel="stylesheet" type="text/css" />
    <script src="https://checkout.stripe.com/checkout.js"></script>
    <!-- <script src="https://checkout.stripe.com/checkout.js"></script> -->
@endsection

@section('customStyle')
    <style type="text/css">
        .page-content-wrapper{
            margin-top: 15px;
        }
        /* The container */
        .containerPayCheckBox {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 22px;
            float: right;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        /* Hide the browser's default checkbox */
        .containerPayCheckBox input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }
        /* Create a custom checkbox */
        .checkmarkPay {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
            border-radius: 50%;
        }
        /* On mouse-over, add a grey background color */
        .containerPayCheckBox:hover input ~ .checkmark {
            background-color: #ccc;
            border: solid white;
        }
        /* When the checkbox is checked, add a blue background */
        .containerPayCheckBox input:checked ~ .checkmarkPay {
            background-color: #57a224;
            background: #57a224;
            border-radius: 50%;
        }
        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmarkPay:after {
            content: "";
            position: absolute;
            display: none;
            border: solid white;
        }
        /* Show the checkmark when checked */
        .containerPayCheckBox input:checked ~ .checkmarkPay:after {
            display: block;
        }
        /* Style the checkmark/indicator */
        .containerPayCheckBox .checkmarkPay:after {
            left: 10px;
            top: 7px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
        #modalDialoge{
            width:30%;
        }
        .roww{
            padding-right: 10px;
            padding-left: 10px;
        }
        .btn-Green:hover {
            background: #fff;
            color:#57a224;
            width:100%;
            border: 1px solid #57a224 !important;
            padding: 16px;
        }
        .btn-Green{
            background: #57a224;
            color:#fff;
            width:100%;
            padding: 16px;
            border: 1px solid #57a224 !important;
        }
        .btn-Green2{
            background: #fff;
            color:#57a224;
            width:100%;
            border: 1px solid #57a224 !important;
            padding: 16px;
        }
        .btn-Green2:hover{
            background: #57a224;
            color:#fff;
            width:100%;
            padding: 16px;
        }
        #modalHeader {
            border-bottom: 0px;
            padding-top: 25px;
        }
        #modalContent{
            border-radius: 2%;
        }
        #modalBody{
            padding-top: 0px;
        }
        .pp{
            color:gray;
            font-size: 16px;
            padding-left: 10px;
            margin-bottom: 6px;
        }
        #modalTitle{
            font-weight: 500;
            font-size: 22px;
            padding-left: 10px;
        }
        .spanText{
                font-size: 16px;
            padding-left: 10px;
            }
    </style>
@endsection

@section('content')
<!-- pay modal -->
<div id="paymentModal" class="modal fade" role="dialog">
    <div class="modal-dialog" id="modalDialoge">
        <!-- Modal content-->
        <div class="modal-content" id="modalContent">
            <div class="modal-header" id="modalHeader">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modalTitle">Quick Checkout</h4>
            </div>
            <div class="modal-body" id="modalBody">
                <form action="{{route('paypal.topup-payment')}}" method="POST">
                <input type="hidden" name="payment_method" id="selected_payment_method">
                <input type="hidden" name="type" value="">
                <p class="pp">Amount</p>
                <input type="text" id="notStripe" name="amount" required class="roww form-control" placeholder="Topup Amount" />
                <input type="text" id="stripeAmount" style="display: none;" class="form-control" placeholder="{{trans('common.amount')}}" onkeyup="console.log($(this).val()); window.topupamount = $(this).val(); calculatePrice()" value="0">
                <p class="pp">Available payment options</p>
                <br>
                @if(\App\Services\GeneralService::payPalAvailableCountries(strtolower($userOriginalLocation)))
                <div class="row roww paymentMethodOptions">
                    <div class="col-md-6">
                        <img style="width:20px;" src="{{asset('/images/paylogo/paypal.png')}}">
                        <span class="spanText">Paypal</span>
                    </div>
                    <div class="col-md-6">
                        <label class="containerPayCheckBox">
                            <input type="checkbox" class="paymentMethod" value="Paypal" data-type="Paypal" id="pp">
                            <span class="checkmarkPay"></span>
                        </label>
                    </div>
                </div>
                <br>
                @endif
                <div class="row roww">
                    <div class="col-md-6">
                        <img style="width:20px" src="{{asset('/images/paylogo/bank.png')}}">
                        <span class="spanText">Bank</span>
                    </div>
                    <div class="col-md-6">
                        <label class="containerPayCheckBox">
                            <input type="checkbox" class="paymentMethod" value="Bank" data-type="Bank" id="bank">
                            <span class="checkmarkPay"></span>
                        </label>
                    </div>
                </div>
                <br>
                <div class="row roww">
                    <div class="col-md-6">
                        <img style="width:20px" src="{{asset('/images/paylogo/ali.png')}}">
                        <span class="spanText">Ali Pay</span>
                    </div>
                    <div class="col-md-6">
                        <label class="containerPayCheckBox">
                            <input type="checkbox" class="paymentMethod" value="Ali Pay" data-type="Ali Pay" id="ap">
                            <span class="checkmarkPay"></span>
                        </label>
                    </div>
                </div>
                <br>
                <div class="row roww">
                    <div class="col-md-6">
                        <img style="width:20px;" src="{{asset('/images/paylogo/wechat.png')}}">
                        <span class="spanText">Wechat Pay</span>
                    </div>
                    <div class="col-md-6">
                        <label class="containerPayCheckBox">
                            <input type="checkbox" class="paymentMethod" value="Wechat Pay" data-type="Wechat Pay" id="wc">
                            <span class="checkmarkPay"></span>
                        </label>
                    </div>
                </div>
                <br>
               @if($available_country['is_available_country'] == true)
                <div class="row roww">
                    <div class="col-md-9">
                        <img style="width:20px;" src="{{asset('/images/rm.png')}}">
                        <span class="spanText">Grab Pay | Boost | Wechat Pay</span>
                    </div>
                    <div class="col-md-3">
                        <label class="containerPayCheckBox">
                            <input type="checkbox" class="paymentMethod" value="Revenue Monster" data-type="Revenue Monster" id="rm">
                            <span class="checkmarkPay"></span>
                        </label>
                    </div>
                </div>
                <br>
                @endif
                @if(\App\Services\GeneralService::stripeAvailableCountries(ucfirst($userOriginalLocation)))
                <div class="row roww">
                    <div class="col-md-9">
                        <i class="fa fa-cc-stripe right-icon setFAFA" style="color:#6672E5;" aria-hidden="true"></i>
                        <span class="spanText">Credit card | Debit Card</span>
                    </div>
                    <div class="col-md-3">
                        <label class="containerPayCheckBox">
                            <input type="checkbox" class="paymentMethod" value="Revenue Monster" data-type="Stripe" id="st">
                            <span class="checkmarkPay"></span>
                        </label>
                    </div>
                </div>
                @endif
                <br>
                <br>
                <div class="row roww">
                    <div class="col-md-12">
                        <button id="btnCustom" type="submit" class="btn btn-default btn-Green" style="height: 50px;">top up</button>
                        <button id="customButtonStripe" type="button" style="display: none;height: 50px;" class="btn btn-default btn-Green">top up stripe</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- pay modal -->
    <div class="row setRowCol12">
        <div class="col-md-3 search-title-container">
            <span class="find-experts-search-title text-uppercase setMAinMENU">{{ trans('common.main_action') }}</span>
        </div>
        <div class="col-md-9 setCol9MAP">
            @if(!\App\Services\GeneralService::stripeAvailableCountries(ucfirst($userOriginalLocation)) && !\App\Services\GeneralService::payPalAvailableCountries(ucfirst($userOriginalLocation)))
                <div class="row">
                    <label for="" class="alert alert-danger" style="margin:0px;">No payment method available in your country...!</label>
                </div>
            @endif
            <span class="find-experts-search-title text-uppercase setRECEIVP">{{ trans('common.receive_payment_details') }}</span>
        </div>
    </div>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
        @include('frontend.flash')
        <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row setPageContent">
                <div class="col-md-12 col-sm-12">
                    @if(\Illuminate\Support\Facades\Session::has('status'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{\Illuminate\Support\Facades\Session::get('status')}}
                        </div>
                    @endif
                    <div class="portlet light bordered setPLB">
                        <div class="portlet-body">
                            <div class="row bank-info">
                                <div class="col-md-12 bank-info-top">
                                    @if(\Session::has('error'))
                                    <div class="alert alert-danger" role="alert">
                                        {{\Session::get('error')}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    @endif
                                    @if(\Session::has('message') && \Session::get('message') != 'Account Topup')
                                        <div class="alert alert-success" role="alert">
                                            {{\Session::get('message')}}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif
                                    <h4 class="title-custom">{{trans('common.available_account_balance')}}</h4>
                                    <!--  <span class="total">$ 5000</span> -->

                                    <span class="total">@if (isset(\Auth::user()->account_balance)) {{\Auth::user()->account_balance}} @else 0 @endif </span>
                                        @if(!\App\Services\GeneralService::stripeAvailableCountries(ucfirst($userOriginalLocation)) && !\App\Services\GeneralService::payPalAvailableCountries(ucfirst($userOriginalLocation)))
                                            <button type="submit"  disabled class="btn btn-color-non-custom pull-right setBTNIPOFMA" onclick="openPaymentModal()">
                                                {{ trans('member.topup') }} <br/><small></small>
                                            </button>

                                            <button type="submit" disabled class="btn btn-color-custom pull-right setBTNIPOFMA" onclick="openWithdrawModal()">
                                                {{ trans('member.withdraw') }} <br/><small></small>
                                            </button>
                                        @else
                                            <button type="submit" class="btn btn-color-non-custom pull-right setBTNIPOFMA" onclick="openPaymentModal()">
                                                {{ trans('member.topup') }} <br/><small></small>
                                            </button>

                                            <button type="submit" class="btn btn-color-custom pull-right setBTNIPOFMA" onclick="openWithdrawModal()">
                                                {{ trans('member.withdraw') }} <br/><small></small>
                                            </button>
                                        @endif
                                </div>
                                <div class="col-md-12 bank-info-body">
                                    <div class="count-info">
                                        <span>{{trans('common.work_in_progress')}}</span>
                                        <h4>${{ $amountProgress }}</h4>
                                    </div>
                                    <div class="count-info">
                                        <span>{{trans('common.earned')}}</span>
                                        <h4>${{ $amountEarned }}</h4>
                                    </div>
                                    <div class="count-info">
                                        <span>{{trans('common.pending_clearance')}}</span>
                                        <h4>${{ $amountPending }}</h4>
                                    </div>
                                    <div class="count-info">
                                        <span>{{trans('common.withdrawals')}}</span>
                                        <h4>${{ $amountWithdrawal }}</h4>
                                    </div>
                                    <div class="count-info">
                                        <span>{{trans('common.paid')}}</span>
                                        <h4>$ {{ $amountPaid }}</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row payment-options setROWPAYMENTBODY">
                        @if(\App\Services\GeneralService::stripeAvailableCountries(ucfirst($userOriginalLocation)) || \App\Services\GeneralService::payPalAvailableCountries(ucfirst($userOriginalLocation)))
                        <div class="col-md-12"><h4 class="title-custom">{{trans('common.payment_options')}}</h4></div>
                        @endif
                        @if(\App\Services\GeneralService::stripeAvailableCountries(ucfirst($userOriginalLocation)))
                        <div class="col-md-12 title-sm">
                            <i class="fa fa-cc-stripe" aria-hidden="true"></i>
                            <span>{{trans('common.stripe')}}</span>
                        </div>

                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="row payment-option-box payment-option-des setROWPAYMENTOPTION">
                                        <div class="col-md-12 top">
                                            <h4>{{trans('common.stripe')}}</h4>
                                        </div>
                                        <div class="col-md-12 body">
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" placeholder="{{trans('common.amount')}}" onkeyup="console.log($(this).val()); window.topupamount = $(this).val(); calculatePrice()" value="0"><br>{{trans('common.amount')}}:<span id="spanAmount">0</span>
                                            </div>

                                            <div class="col-md-3">
                                                <button id="customButton" class="btn btn-primary setBTNIOMA">{{trans('common.add')}}</button>
                                            </div>
                                        </div>
                                        <div class="col-md-12 footer">
                                            <i class="fa fa-cc-stripe right-icon setFAFA" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        @endif

                        @if(\App\Services\GeneralService::payPalAvailableCountries(strtolower($userOriginalLocation)))
                        <div class="col-md-12 title-sm">
                            <i class="fa fa-paypal" aria-hidden="true"></i>
                            <span>{{trans('common.paypal')}}</span>
                        </div>

                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="row payment-option-box payment-option-des setROWPAYMENTOPTION">
                                        <div class="col-md-12 top">
                                            <h4>{{trans('common.paypal')}}</h4>
                                        </div>
                                        @php
                                            $paypal = $paymentMethodDetails->where('payment_method', \App\Models\TransactionDetail::PAYMENT_METHOD_PAYPAL);
                                            $email = isset($paypal) && isset($paypal[0]) ? $paypal[0]['email'] : '';
                                            $name = isset($paypal) && isset($paypal[0]) ? $paypal[0]['name'] : '';
                                        @endphp
                                        <div class="col-md-12 body setCol12BSP">
                                            @if($email == '')
                                                <span><i>{{trans('common.no_account_connected')}}</i></span>
                                            @else
                                                <span>{{$name}}</span>
                                                <p class="setPP">{{$email}}</p>
                                            @endif
                                        </div>
                                        <div class="col-md-12 footer">
                                            {{--<i class="fa fa-check-circle verified" aria-hidden="true"></i>--}}
                                            {{--<span>Verified</span>--}}
                                            <i class="fa fa-paypal right-icon setFAFA" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="row payment-option-box payment-option-add setROWPAYMENTBOXOPTIONADD">
                                        <div class="col-md-12">
                                            <a href="#" data-toggle="modal" data-target="#linkPayPal">
                                                <span>{{trans('common.link_a')}}</span>
                                                <p>
                                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                                    {{trans('common.paypal')}}
                                                </p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                    <div class="row earning-list">
                        <div class="col-md-12 earning-list-top">
                            <span class="text">{{trans('common.invoices')}}</span> <span class="number">({{count($invoices)}})</span>
                        </div>
                        <div class="col-md-12 portlet earning-list-body">
                            @foreach($invoices  as $k => $invoice)
                                <div class="row earning-list-main">
                                    <div class="col-md-2 earning-list-left">
                                        <span>{{$invoice->name}} (#{{$invoice->id}})</span>
                                        <br>
                                    </div>
                                    <div class="col-md-2 earning-list-title">
                                        @if($invoice->type == \App\Models\Transaction::TYPE_WITHDRAW)
                                            <small>{{trans('common.Withdraw')}}</small>
                                        @elseif($invoice->type == \App\Models\Transaction::TYPE_TOP_UP)
                                            <small>{{trans('common.topup')}}</small>
                                        @endif
                                    </div>
                                    <div class="col-md-2 earning-list-title">
                                        {{$invoice->transactionDetail['payment_method']}}
                                    </div>
                                    <div class="col-md-2 earning-list-title">
                                        @php
                                            $color = 'green';
                                            if($invoice->status_string == 'In Progress'){
                                                $color = 'orange';
                                            }else if($invoice->status_string == 'Rejected') {
                                                $color = 'red';
                                            }
                                        @endphp
                                        <small style="color: {{$color}}">
                                            {{$invoice->status_string}}
                                        </small>
                                    </div>
                                    <div class="col-md-2 earning-list-title">
                                        <small>
                                            {{\Carbon\Carbon::parse($invoice->created_at)->diffForHumans()}}
                                        </small>
                                    </div>
                                    <div class="col-md-2 earning-list-right">
                                        <span>{{$invoice->amount}}</span>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                                {{ trans('member.bank_accounts_bidding') }}
                @foreach (Auth::guard('users')->user()->bankAccount as $key => $var)
                    @include('frontend.includes.bankcard', ['card' => $var])
                @endforeach
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
        {{--@include('frontend.includes.modalAddBankAccount', ['banks' => $banks])--}}
    </div>
    <!-- END CONTENT -->


    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog" style="margin-top: 10%;">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <form action="{{route('paypal.topup')}}" method="POST">
                    @csrf
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">{{trans('common.top_up_amount')}}</h4>
                    </div>
                    <div class="modal-body">
                        <input type="text" required class="form-control" placeholder="{{trans('common.amount')}}" name="amount">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('common.close')}}</button>
                        <button type="submit" class="btn btn-success">{{trans('common.save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="linkPayPal" class="modal fade" role="dialog" style="margin-top: 10%;">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <form action="{{route('add.paypal')}}" method="POST" style="padding: 50px">
                    @csrf
                    <div class="form-group">
                        <label for="Name">{{trans('common.name')}}</label>
                        <input type="text" class="form-control" id="Name" name="Name" aria-describedby="emailHelp" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <label for="Email">{{trans('common.email')}}</label>
                        <input type="text" class="form-control" id="Email" name="Email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="Phone">{{trans('common.phone')}}</label>
                        <input type="text" class="form-control" id="Phone" name="Phone" placeholder="Phone">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('common.close')}}</button>
                        <button type="submit" class="btn btn-success">{{trans('common.save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="withdrawPayPal" class="modal fade" role="dialog" style="margin-top: 10%;">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{trans('common.amount_to_withdraw')}}</h4>
                </div>
                <form action="{{route('add.withdrawpaypal')}}" method="POST" style="padding: 50px">
                    @csrf
                    <div class="form-group">
                        <label for="Name">{{trans('common.amount')}}: <span id="afw"> {{\Auth::user()->account_balance}}</span></label>
                    </div>
                    <div class="form-group">
                        <label for="amount">{{trans('common.withdraw_amount')}}</label>
                        <input type="text" class="form-control" id="withdrawAmount" name="amount" placeholder="{{trans('common.withdraw_amount')}}">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('common.close')}}</button>
                        <button type="submit" class="btn btn-success">{{trans('common.save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="successTopUp" class="modal fade" role="dialog">
    <div class="modal-dialog" id="modalDialoge">
        <!-- Modal content-->
        <div class="modal-content" id="modalContent">
            <div class="modal-body" id="modalBody" style="text-align: center;padding: 60px;">
                <img style="width: 100px;" src="{{asset('images/ic_success_check.png')}}" />
                <br>
                <br>
                <p class="alert alert-success" role="alert">
                    Account Top Up Successfully
                </p>
            </div>
        </div>
    </div>
    </div>

@if(!$userCurrencyExist)
<div id="userCurrencyModal" class="modal fade" role="dialog">
    <div class="modal-dialog" id="modalDialoge">
        <!-- Modal content-->
        <div class="modal-content" id="modalContent">
            <div class="modal-header">
                <h4>
                    Add Currency
                </h4>
            </div>
            <div class="modal-body">
               <form>
                   <div class="row">
                       <div class="col-md-12">
                           <div class="form-group">
                               <label>Currency Name : </label>
                               <select required class="form-control" name="currency_name" id="currency">
                                    <option selected disabled>Select Your Currency</option>
                                   @foreach($currencies as $currency)
                                       <option value="{{$currency->id}}">{{$currency->currency_name}}</option>
                                   @endforeach
                               </select>
                           </div>
                       </div>

                       <div class="form-group">
                           <div class="col-md-12">
                               <input type="submit" name="submit" value="Save" onclick="submitUserCurrency(event)" class="btn btn-success" />
                           </div>
                       </div>
                   </div>
               </form>
            </div>
        </div>
    </div>
</div>
@endif
@endsection


@section('footer')
    <script>
        var stripeHandler1 = StripeCheckout.configure({
            key: '{{config('services.stripe.key')}}',
            image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
            locale: 'auto',
            token: function(token) {
                $(".ajaxloader").show();
                console.log('in token now');
                console.log(token)
                var form = new FormData();
                form.append("data", JSON.stringify(token));
                form.append("amount", window.topupamount);

                var settings = {
                    "url": "/stripe/charge",
                    "method": "POST",
                    "data": form,
                    "processData": false,
                    "contentType": false,
                    "mimeType": "multipart/form-data",
                };

                $.ajax(settings).done(function (response) {
                    console.log(response);
                    $(".ajaxloader").hide();
                });
            }
        });

        document.getElementById('customButtonStripe').addEventListener('click', function(e) {
            console.log(parseInt(window.topupamount));
            if(parseInt(window.topupamount) < 1 || window.topupamount == NaN) {
                alert('Please enter valid amount');
                e.preventDefault();
            } else {
                // Open Checkout with further options:
                stripeHandler1.open({
                    image: '/images/Favicon.jpg',
                    name: 'XenRen',
                    description: 'Topup',
                    amount: window.topupamount * 100
                });
                e.preventDefault();
            }
        });
        // Close Checkout on page navigation:
        window.addEventListener('popstate', function() {
            stripeHandler1.close();
        });
    </script>
    <script>
    var stripeHandler2 = StripeCheckout.configure({
        key: '{{config('services.stripe.key')}}',
        image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
        locale: 'auto',
        token: function(token) {
            $(".ajaxloader").show();
            console.log('in token now');
            console.log(token)
            var form = new FormData();
            form.append("data", JSON.stringify(token));
            form.append("amount", window.topupamount);

            var settings = {
                "url": "/stripe/charge",
                "method": "POST",
                "data": form,
                "processData": false,
                "contentType": false,
                "mimeType": "multipart/form-data",
            };

            $.ajax(settings).done(function (response) {
                console.log(response);
                $(".ajaxloader").hide();
            });
        }
    });

    document.getElementById('customButton').addEventListener('click', function(e) {
        console.log(parseInt(window.topupamount));
        if(parseInt(window.topupamount) < 1 || window.topupamount == NaN) {
            alert('Please enter valid amount');
            e.preventDefault();
        } else {
            // Open Checkout with further options:                                                        
            stripeHandler2.open({
                image: '/images/Favicon.jpg',
                name: 'XenRen',
                description: 'Topup',
                amount: window.topupamount * 100
            });
            e.preventDefault();
        }
    });
    // Close Checkout on page navigation:
    window.addEventListener('popstate', function() {
        stripeHandler2.close();
    });
</script>
    <script>
        // $('#paymentModal').modal('show');
    </script>
    <script src="/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="/custom/js/frontend/myAccount.js" type="text/javascript"></script>
    <script>
        @if(\Session::get('message') == 'Account Topup')
            $('#successTopUp').modal('show')
        @endif
        $(function(){
            getLocation();
        });
        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition, showError);
            }
        }
        function showPosition(position) {
            console.log('position');
            console.log(position);
        }

        function showError(error) {
            console.log('error');
            console.log(error);
            switch(error.code) {
                case error.PERMISSION_DENIED:
//                    x.innerHTML = "User denied the request for Geolocation."
                    break;
                case error.POSITION_UNAVAILABLE:
//                    x.innerHTML = "Location information is unavailable."
                    break;
                case error.TIMEOUT:
//                    x.innerHTML = "The request to get user location timed out."
                    break;
                case error.UNKNOWN_ERROR:
//                    x.innerHTML = "An unknown error occurred."
                    break;
            }
        }
			+function () {
				$(document).ready(function () {
					$('#add-bank-account-form').makeAjaxForm({
						'inModal': true,
						'closeModal': true,
						'submitBtn': '#submit-report-form',
						'alertContainer': '#add-bank-errors',
						'successRefresh': true,
					});
					$('#bank-card-no-input').on('keyup', function () {
						var val = $(this).val();
						val = val.replace(/(.{4})/g, '$1 ').trim();
						$('#bank-card-no').html(val);
					});
				});
			}(jQuery);
    </script>
    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--}}
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--}}
    <script>
			$('#withdrawAmount').keyup(function(){
				console.log('12323');
				var amount = '{{\Auth::user()->account_balance}}' - $('#withdrawAmount').val()
				$('#afw').html(amount);
			})
            function calculatePrice() {
	            console.log('hello');
	            var tax = (2.75 * (window.topupamount))/100;
	            $('#spanAmount').html((window.topupamount-tax))
            }
    </script>
    <script>
        function openPaymentModal() {
            let checkData = checkUserCurrencyExist();
            if(checkData == true) {
                $('#paymentModal').modal('show');
            }
        }

        function checkUserCurrencyExist(){
            let userCurrency = '{{$userCurrencyExist}}';
            if(userCurrency) {
                return true;
            } else {
                $('#userCurrencyModal').modal('show');
            }
        }
        function submitUserCurrency(e){
            e.preventDefault();
            let currency = $('#currency').val();
            console.log('currency_name');
            console.log(currency);
            let form = new FormData();
            form.append('_token','{{csrf_token()}}');
            form.append('currency',currency);
            console.log(form);
            $.ajax({
                url:'/save/user/currency',
                method:'POST',
                data:form,
                processData: false,
                contentType: false,

               success: function (response) {
                   console.log(response);
                   if(response.status == true) {
                       toastr.success(response.message);
                       $('#userCurrencyModal').modal('hide');
                       setTimeout(function () {
                            window.location.reload();
                       },1000);
                   }
               },
               error: function (error) {
                    console.log(error);
               }
            });
        }

        function openWithdrawModal(){
            let checkData = checkUserCurrencyExist();
            if(checkData == true) {
                $('#withdrawPayPal').modal('show');
            }
        }

        $('.paymentMethod').on('click', function () {
            console.log($(this).data('type'));
            $('.paymentMethod').prop("checked", false);
            console.log($(this).attr('id'));;
            let id = $(this).attr('id');
            $('#'+id).prop("checked", true);
            $('#selected_payment_method').val(id);
            if( id == 'st') {
                $('#btnCustom').hide();
                $('#customButtonStripe').show();
                $('#stripeAmount').show();
                $('#notStripe').hide();
            } else {
                $('#btnCustom').show();
                $('#customButtonStripe').hide();
                $('#stripeAmount').hide();
                $('#notStripe').show();
            }
        })
    </script>
@endsection




