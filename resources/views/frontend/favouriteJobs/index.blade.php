@extends('frontend.layouts.default')

@section('title')
    {{ trans('common.favourite_jobs_title') }}
@endsection

@section('keywords')
    {{ trans('common.favourite_jobs_keyword') }}
@endsection

@section('description')
    {{ trans('common.favourite_jobs_desc') }}
@endsection

@section('author')
    Xenren
@endsection

@section('url')
    https://www.xenren.co/favouriteJobs
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
{{--    <link href="/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="/custom/css/frontend/favouriteJobs.css" rel="stylesheet" type="text/css"/>--}}
{{--    <link href="/custom/css/frontend/jobs.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="/custom/css/frontend/modalMakeOffer.css" rel="stylesheet" type="text/css"/>--}}
        <link href="{{asset('css/minifiedFavouriteJob.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <div class="row setCol12">
        <div class="col-md-3 search-title-container">
            <span class="find-experts-search-title text-uppercase setMAINMENU">{{ trans('common.main_action') }}</span>
        </div>
        <div class="col-md-9 right-top-main setCol9RPP">
            <div class="row">
                <div class="col-md-12 top-tabs-menu">
                    @if ($favouriteJobs->lastPage() > 1)
                        <div class="find-experts-pagination pull-right">
                            {{ $favouriteJobs->links() }}
                        </div>
                    @endif
                    <div class="caption">
                        <span class="find-experts-search-title text-uppercase setFJOBS">{{ trans('common.favourite_jobs') }}</span>
                        <span class="count-record">({{ $totalCount }})</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-content-wrapper setPCWFJ">
        <div class="page-content">

            @if( $totalCount == 0 )
                <div class="portlet light text-center setPageContent">
                    <div class="setFJPAGEFJY">
                        {{ trans('common.you_dont_have_favourite_job_yet') }}
                        <div class="setFPJOBIMAGE">
                            <img src="\images\icons\favourite_jobs.png" width="300px" height="300px;">
                        </div>
                    </div>
                </div>
            @endif

            @foreach( $favouriteJobs as $favouriteJob)
                @if( isset($favouriteJob->project) && $favouriteJob->project->type == \App\Models\Project::PROJECT_TYPE_COMMON )
                    <div id="project-id-{{ $favouriteJob->project->id }}" class="portlet box job div-job-main setPageContent">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-span-12">
                                                <div class="project-name pull-left">
                                                    <h4>{{ $favouriteJob->project->name }}</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 price-div setPriceDIV">
                                        <span class="col-md-4 top-smoll setFJPCol4Span">
                                            {{ $favouriteJob->project->explainPayType() }}
                                            @if( $favouriteJob->project->pay_type == \App\Constants::PAY_TYPE_FIXED_PRICE || $favouriteJob->project->pay_type == \App\Constants::PAY_TYPE_HOURLY_PAY )
                                                :
                                                <span class="price">
                                                <i class="fa fa-usd" aria-hidden="true"></i>{{ $favouriteJob->project->reference_price }}.
                                            </span>
                                            @endif
                                        </span>
                                            <span class="col-md-8 top-smoll">{{ trans('common.project_type') }}: <span class="type">{{ trans('common.public') }}</span></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 description scroll-div setSCROLLDIVDESC">
                                            {!! $favouriteJob->project->description !!}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 skill setCOL12DIVSKILL">
                                            <div class="skill-div" >
                                                @foreach( $favouriteJob->project->projectSkills as $key => $projectSkill )
                                                    <span class="item-skill">
                                                <span>
                                                    {{ $projectSkill->getSkillName( $projectSkill->skill_id ) }}
                                                </span>
                                            </span>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 user-content-footer setUSERCONTENTFOOTER">
                                            <img alt="" class="img-circle" src="{{ $favouriteJob->project->creator->getAvatar() }}" width="35px" height="35px">
                                            <p>
                                                {{ $favouriteJob->project->getCreatedAt() }}
                                                <br>
                                                {!! $favouriteJob->project->getByCreator() !!}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 right-content">
                                <div class="row">
                                    @if( \Auth::guard('users')->check())
                                        <div class="col-md-12 setCol12FAFAStar">
                                            <a class="btn-remove-fav-job {{ $favouriteJob->project->isYourFavouriteJob()? 'active': '' }}" href="javascript:;" data-url="{{ route('frontend.removefavouritejob.post') }}" data-project-id="{{ $favouriteJob->project->id }}">
                                                <span class="fa fa-star"></span>
                                            </a>
                                        </div>
                                        <div class="col-md-12 right-content-btn setBTNBTNBTN">

                                            <a href="{{ route('frontend.modal.makeoffer', [
                                                'project_id' => $favouriteJob->project->id,
                                                'user_id' => $favouriteJob->user->id,
                                                'inbox_id' => 0
                                            ] ) }}"
                                               data-toggle="modal"
                                               data-target="#modalMakeOffer">
                                                <button id="btn-make-offer-{{ $favouriteJob->project->id }}" class="btn btn-success-color setBTNFJOBS">
                                                    {{ trans('common.make_offer') }}
                                                </button>
                                            </a>

                                            <button class="btn btn-default-color btn-check-detail setBTNFJOBS" data-url="{{ route('frontend.joindiscussion.getproject', [\Session::get('lang'),$favouriteJob->project->name, $favouriteJob->project->id]) . "#sec-chat" }}">
                                                {{ trans('common.join_discuss') }}
                                            </button>
                                            <button class="btn btn-default-color bottom-btn btn-check-detail setBTNCD" data-url="{{ route('frontend.joindiscussion.getproject', [\Session::get('lang'),$favouriteJob->project->name, $favouriteJob->project->id]) }}">
                                                {{ trans('common.check_detail') }}
                                            </button>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @elseif(  isset($favouriteJob->project) && $favouriteJob->project->type == \App\Models\Project::PROJECT_TYPE_OFFICIAL )
                    <div id="project-id-{{ $favouriteJob->project->id }}" class="portlet box job div-job-main">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-12 user-content-img">
                                        <img src="{{ $favouriteJob->project->officialProject->out_cover }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 user-content-budget">
                                        <span class="uppercase">{{ trans('common.shares') }}</span>
                                        <h3>{{ $favouriteJob->project->officialProject->stock_total }}</h3>
                                        <span class="share">{{ trans('common.issued') }}: {{ $favouriteJob->project->officialProject->stock_share }}%</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 user-content-footer setColROWFJPAGECol12">
                                        <img alt="" class="img-circle" src="/images/user-default-img.png" width="35px" height="35px">
                                        <p>
                                            {{ $favouriteJob->project->getCreatedAt() }}
                                            <br>
                                            {!! $favouriteJob->project->getByCreator() !!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 middel-content">
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-span-12">
                                                <div class="project-name pull-left">
                                                    <img src="/images/Official-Icon.png">
                                                    <h4>{{ $favouriteJob->project->officialProject->title }}</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5 price-div-sm">
                                            <span class="top-smoll">{{ trans('common.time_frame') }} : <span class="type">{{  $favouriteJob->project->officialProject->getTimeFrame() }} Days</span></span>
                                        </div>
                                        <div class="col-md-7 price-div-sm">
                                            <span class="top-smoll">{{ trans('common.end_date') }} : <span class="type">{{  $favouriteJob->project->officialProject->getRecruitEndDate() }}</span></span>
                                        </div>
                                        <div class="col-md-5 price-div-sm">
                                            <span class="top-smoll">{{ trans('common.funding_stage') }} : <span class="type">{{ \App\Constants::translateFundingStage( $favouriteJob->project->officialProject->funding_stage) }} </span></span>
                                        </div>
                                        <div class="col-md-7 price-div-sm">
                                        <span class="top-smoll">{{ trans('common.location') }} :
                                            <a href="{{ route('frontend.map.officialprojectlocation', [ 'official_project_id' => $favouriteJob->project->officialProject->id ]) }}">
                                                <span class="type">{{ $favouriteJob->project->officialProject->city }}</span>
                                            </a>
                                        </span>
                                        </div>
                                        <div class="col-md-7 price-div-sm">
                                            <span class="top-smoll">{{ trans('common.project_type') }} : <span class="type">{{ trans('common.official_project') }}</span></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 description">
                                            {!! $favouriteJob->project->officialProject->description !!}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5 defficulty">
                                            <span class="top-smoll">{{ trans('common.difficulty') }}:</span>
                                            <br>
                                            @for ($i = 0; $i < $favouriteJob->project->officialProject->project_level; $i++)
                                                <i class="fa fa-star"></i>
                                            @endfor

                                            @for ($i = 0; $i < ( 6 - $favouriteJob->project->officialProject->project_level ); $i++)
                                                <i class="fa fa-star-o"></i>
                                            @endfor
                                        </div>
                                        <div class="col-md-7 possition">
                                            <span class="top-smoll">{{ trans('common.position_needed') }}:</span>
                                            <br>
                                            <span>
                                            @foreach( $favouriteJob->project->officialProject->projectPositions as $key => $projectPosition)
                                                    @if( $key != 0 )
                                                        {{ ", " }}
                                                    @endif

                                                    {{  $projectPosition->jobPosition->getName() }}
                                                @endforeach
                                        </span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 skill">
                                            <div class="skill-div" >
                                                @foreach( $favouriteJob->project->officialProject->project->projectSkills as $projectSkill)
                                                    <span class="item-skill">
                                                <span>{{ $projectSkill->getSkillName( $projectSkill->skill_id ) }}</span>
                                            </span>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 right-content">
                                <div class="row">
                                    @if( \Auth::guard('users')->check())
                                        <div class="col-md-12">
                                            <a class="btn-remove-fav-job {{ $favouriteJob->project->isYourFavouriteJob()? 'active': '' }}" href="javascript:;" data-url="{{ route('frontend.removefavouritejob.post') }}" data-project-id="{{ $favouriteJob->project->id }}">
                                                <span class="fa fa-star"></span>
                                            </a>
                                        </div>
                                        <div class="col-md-12 right-content-btn right-content-btn-big">
                                            <a href="{{ route('frontend.modal.makeoffer', [
                                            'project_id' => $favouriteJob->project->id,
                                            'user_id' => $favouriteJob->user->id,
                                            'inbox_id' => 0
                                        ] ) }}"
                                               data-toggle="modal"
                                               data-target="#modalMakeOffer">
                                                <button id="btn-make-offer-{{ $favouriteJob->project->id }}" class="btn btn-success-color">
                                                    {{ trans('common.make_offer') }}
                                                </button>
                                            </a>

                                            <button class="btn btn-default-color btn-check-detail" data-url="{{ route('frontend.joindiscussion.getproject', [\Session::get('lang'), $favouriteJob->project->name, $favouriteJob->project->id]) . "#sec-chat" }}">
                                                {{ trans('common.join_discuss') }}
                                            </button>

                                            <button class="btn btn-default-color bottom-btn btn-check-detail" data-url="{{ route('frontend.joindiscussion.getproject', [\Session::get('lang'), $favouriteJob->project->name, $favouriteJob->project->id]) . "#sec-official-project" }}">
                                                {{ trans('common.check_detail') }}
                                            </button>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach

        </div>
    </div>

@endsection

@section('modal')
    <div id="modalMakeOffer" class="modal fade apply-job-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
@endsection

@section('footer')
{{--    <script src="/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>--}}
    <script>
			var lang = {
				unable_to_request : "{{ trans('common.unable_to_request') }}"
			};
    </script>
<script src="{{asset('js/minifiedFavouriteJob.js')}}"></script>
{{--    <script src="/custom/js/frontend/favouriteJobs.js" type="text/javascript"></script>--}}
@endsection
