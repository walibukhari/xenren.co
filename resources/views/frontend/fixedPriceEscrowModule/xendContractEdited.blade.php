@extends('frontend.layouts.default')

@section('title')
    End Contract Edited
@endsection

@section('description')
    End Contract Edited
@endsection

@section('author')
Xenren
@endsection

@section('header')
    <link href="/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/userCenter.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/fixedPriceEscrowModule.css" rel="stylesheet">
@endsection

@section('content')
<div class="page-content-wrapper">
     <div class="col-md-12 usercenter link-div">
	    <a href="#" class="text-uppercase raleway">
	        <span class="fa fa-angle-left">
	        </span>
	        back to my orders
	    </a>
	</div>
	<div class="clearfix"></div>
	<br>

	<div class="col-md-12">
	    <div class="row main-div">
	        <div class="col-md-12 personal-profile">
	        	<div class="row">
	        		<div class="col-md-8">
	        			<div class="row user-info">
			        		<div class="col-md-3 text-center">
			                    <div class="profile-userpic">
			                        <img alt="" class="img-responsive" src="/images/people-avatar.png">
			                         <label class="btn-bs-file btn btn-xs">
						                <i class="fa fa-camera"></i>
						                <input type="file" />
						            </label>
			                    </div>
			                </div>
			        		<div class="col-md-9 basic-info">
				                <div class="row">
				                    <div class="col-md-12 user-full-name">
				                        <span class="full-name">Adam John</span>
				                        <img src="/images/icons/check.png">
				                    </div>
				                </div>
				                <br>
				                <div class="row">
				                    <div class="col-md-3">
				                        <small>Status:</small>
				                    </div>
				                    <div class="col-md-9">
				                        <span class="status">On Your Project</span>
				                        <button class="btn btn-success btn-green-bg-white-text" type="button">Chat</button>
				                    </div>
				                </div>
				                <br>
				                <div class="row">
				                    <div class="col-md-3">
				                        <small>Rate:</small>
				                    </div>
				                    <div class="col-md-9">
				                        <span class="details">$10.00 /hr</span>
				                    </div>
				                </div>
				                <br>
				                <div class="row">
				                    <div class="col-md-3">
				                        <small>Age:</small>
				                    </div>
				                    <div class="col-md-9">
				                        <span class="details">36</span>
				                    </div>
				                </div>
				                <br>
				                <div class="row">
				                    <div class="col-md-3">
				                        <small>Experience:</small>
				                    </div>
				                    <div class="col-md-9">
				                        <span class="details">3 Year</span>
				                    </div>
				                </div> 
					                                    
			                </div>
	        			</div>
	        			<div class="row">
	        				<div class="col-md-12 contacts-info">
    							<div class="row">
                                    <div class="col-md-3 col-md-3 contacts-info-sub">
                                        <img src="/images/skype-icon-5.png"> <span>Skype</span>
                                        <p style="display: block;">adamjohn982</p>
                                    </div>

                                    <div class="col-md-3 col-md-3 contacts-info-sub">
                                        <img src="/images/wechat-logo.png"> <span>Wechat</span>
                                        <p style="display: block;">adamjohnhs</p>
                                    </div>

                                    <div class="col-md-3 col-md-3 contacts-info-sub">
    									<img src="/images/Tencent_QQ.png"> <span>QQ</span> 
    									<p style="display: block;">1851215421</p> 
    								</div>


    								<div class="col-md-3 col-md-3 contacts-info-sub">
    									<img src="/images/MetroUI_Phone.png"> <span>Phone</span> 
    									<p style="display: block;">+00 1234 567 89</p> 
    								</div>
    							</div>
							</div>
	        			</div>
	        		</div>
	                <div class="col-md-4">
	                	<div class="row basic-info">
			                <div class="col-md-12 basic-info-title text-left">
			                	<img alt="" src="/images/hands.png"> &nbsp;<span class="subtitle"> Skill</span>
			                </div>
			                <div class="col-md-12">
		                		<div class="skill-div">
		                            <span class="item-skill admin-verified">
		                        		<img width="24px" height="24px" alt="" src="/images/logo_2.png">
		                        		<span>HTML</span>
		                    		</span>
		                        </div>
	                            <div class="skill-div">
	                                <span class="item-skill client-verified">
	                    				<img width="24px" height="24px" alt="" src="/images/checked.png">
	                    				<span>javascript</span>
	                				</span>
	                            </div>
	                            <div class="skill-div">
	                                <span class="item-skill unverified">
	                    				<span>HTML</span>
	                				</span>
	                            </div>
	                            <div class="skill-div">
	                                <span class="item-skill unverified">
	                    				<span>Adobe Photoshop</span>
	                				</span>
	                            </div>
	                            <div class="skill-div">
	                                <span class="item-skill unverified">
	                    				<span>Adobe ilusstrator</span>
	                				</span>
	                            </div>
				                                
				                <div class="clearfix"></div>
			                </div>

			                <div class="col-md-12 basic-info-title text-left">
			                	<br><img alt="" src="/images/earth.png"> &nbsp;<span class="subtitle"> Language</span>
			                </div>
			                
			                <div class="col-md-12 media similar-profiles text-left">
	                            <div class="media-left media-middle item-languages">
		                             <img class="pull-left" alt="..." src="http://xenrencc.dev/images/Flags-Icon-Set/24x24/HK.png" class="media-object">
		                            <span class="pull-left">English</span>
		                        </div>
	                            <div class="media-left media-middle item-languages">
		                            <img class="pull-left" alt="..." src="http://xenrencc.dev/images/Flags-Icon-Set/24x24/MY.png" class="media-object">
		                            <span class="pull-left">Malay</span>
		                        </div>
		                  	</div>
			            </div>
	                </div>
	        	</div>
	        </div>

			<div class="col-md-12 prising-section text-center">
				<div class="row">
					<div class="col-md-4 prising-section-sub">
						<img src="/images/icons/doller-icon.png"><br>
						<p>Budget for the Project</p>
						<span>$1500 USD</span>
					</div>
					<div class="col-md-4 prising-section-sub">
						<img src="/images/icons/time-icon.png"><br>
						<p>Current Milestone</p>
						<span>$700USD</span>
					</div>
					<div class="col-md-4 prising-section-sub">
						<img src="/images/icons/doller-icon.png"><br>
						<p>Fixed Price</p>
						<button class="btn btn-success btn-green-bg-white-text" type="button">Switch to Hourly</button>
					</div>
				</div>
			</div>

			<div class="col-md-12 description">
				<h4>ROCK and ROLL - UI Designer</h4>
				<h5>DESCRIPTION</h5>
				<p>
					We are looking to overhaul the design of our extension and assoclated pages on our site. Attached here is the design brief. Please go through this and respond with relevant work from your portfollio.  **This is important** Please write in your own words what the extension does and how you think the design should help highlight this.
					<br><br>
					The end-result design should be a fully specified PSD file with all the found and Image include. Not a flat PNG mocup.
				</p>
			</div>

			<div class="col-md-12 milestone-desc">
				<ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="text-center"><a href="#milestone" aria-controls="milestone" role="tab" data-toggle="tab">Milestone</a></li>
                    <li role="presentation" class="text-center"><a href="#dispute" aria-controls="dispute" role="tab" data-toggle="tab">Dispute</a></li>
                    <li role="presentation" class="text-center"><a href="#contract-control-panel" aria-controls="contract-control-panel" role="tab" data-toggle="tab">Contract Control Panel</a></li>
                    <li role="presentation" class="active text-center"><a href="#end-contract" aria-controls="end-contract" role="tab" data-toggle="tab">End Contract</a></li>
                </ul>

                <div class="tab-content tabpanel-custom">
                    <div role="tabpanel" class="tab-pane" id="milestone">
                    </div>
                    <div role="tabpanel" class="tab-pane" id="dispute">
                    	Dispute
                    </div>
                    <div role="tabpanel" class="tab-pane" id="contract-control-panel">
                    	Contract Control Panel
                    </div>
                    <div role="tabpanel" class="tab-pane active" id="end-contract">
                    	<div class="row end-contract">
                    		<div class="col-md-6">
                    			<label><strong>End Contract</strong> </label>
                    			<p>After click conform, will jump to Leave Feedback. Must complete feedback then able to end contract. </p>
                    			<form accept="">
			                    	<div class="form-group row">
									  	<div class="col-xs-12">
									    	<label for="ex1"><strong> Reason of end contract</strong></label>
									    	<textarea class="form-control" id="ex1"></textarea>
									  	</div>
									  	<div class="col-xs-12">
									  	<br>
									  		<button class="btn btn-success btn-green-bg-white-text" type="submit">Confirm</button>
									  	</div>
									</div> 
		                    	</form>	
                    		</div>
                    	</div>
                    </div>
                </div>
			</div>
	    </div>
	</div>
</div>
@endsection
 
@section('footer')
@endsection
