@if(isset($type) && $type && $type == 2)
    <div class="setRowPJMBPAGE">
        <table class="table custom-table">
        @foreach($milestones as $v)
            @if(isset($v->milestoneStatus) && $v->milestoneStatus->status == $status)
                <tr>
                    <td width="30px"><i class="fa fa-circle"></i></td>
                    <td>
                        <span class="title">Amount</span><br>
                        <span class="desc">${{$v->amount}}</span>
                    </td>
                    <td>
                        <span class="title">Milestone Name</span><br>
                        <span class="desc">{{$v->name}}</span>
                    </td>
                    <td>
                        <span class="title">Milestone Description</span><br>
                        <span class="desc">{{$v->description}}</span>
                    </td>
                    <td>
                        <span class="title">Due Date</span><br>
                        <span class="desc">{{$v->due_date}}</span>
                    </td>
                    @if($v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_REJECTED)
                        @if(isset($data) && $data && \Auth::user() && $data->user_id == \Auth::user()->id)
                            <td>
                                <label for="">Change Status</label>
                                <select name="" id="changeStatus" class="form-control changeStatus" data-milestone="{{$v->id}}">
                                    <option value="">Select Status</option>
                                    <option value="1">In Progress</option>
                                    <option value="2">Confirm</option>
                                </select>
                            </td>
                        @endif
                    @endif
                    @if($v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_REQUESTED)
                        @if(isset($data) && $data && \Auth::user() && $data->user_id == \Auth::user()->id)
                            <td>
                                <select name="" id="changeStatus" class="form-control changeStatus" data-milestone="{{$v->id}}">
                                    <option value="">Select Status</option>
                                    <option value="{{\App\Models\ProjectMilestones::STATUS_CONFIRMED}}">Confirm</option>
                                    <option value="{{\App\Models\ProjectMilestones::STATUS_REJECTED}}">Cancel</option>
                                </select>
                            </td>
                        @endif
                    @endif
                </tr>
            @endif
        @endforeach
    </table>
    </div>
@else
    <div class="setRowPJMBPAGE">
        <table class="table custom-table">
        @foreach($milestones as $v)
            @foreach($v->transactions as $trans)
                @if($trans->status == $status)
                    <tr>
                        <td width="30px"><i class="fa fa-circle"></i></td>
                        <td>
                            <span class="title">Amount</span><br>
                            <span class="desc">${{$trans->amount}}</span>
                        </td>
                        <td>
                            <span class="title">Milestone Name</span><br>
                            <span class="desc">{{$v->name}}</span>
                        </td>
                        <td>
                            <span class="title">Transaction Name</span><br>
                            <span class="desc">{{$trans->name}}</span>
                        </td>
                        {{--<td>--}}
                            {{--<span class="title">Due Date</span><br>--}}
                            {{--<span class="desc">{{$trans->due_date}}</span>--}}
                        {{--</td>--}}
                        @if(isset($data) && $data && \Auth::user() && $data->user_id == \Auth::user()->id)
                            @if($status == \App\Models\Transaction::STATUS_REJECTED)
                                @else
                            <td>
                                {{csrf_field()}}
                                <button class="btn btn-success btn-green-bg-white-text confirmMilestoneEmployer setCMPAGEBNTN" id="confirmMilestoneEmployer" data-transactionid="{{$trans->id}}" data-name="{{$trans->name}}" data-amount="{{$trans->amount}}" data-milestoneid="{{$trans->milestone_id}}" data-duedate="{{$trans->due_date}}" data-type="2" data-status="{{\App\Models\Transaction::STATUS_APPROVED}}" type="button" data-id="{{ $trans->id }}">Accept</button>
                                <button class="btn btn-success btn-white-bg-green-text rejectMilestoneEmployerBtn setCMPAGEBNTN" id="rejectMilestoneEmployerBtn" type="button" data-id="{{ $trans->id }}" data-transactionid="{{$trans->id}}" data-name="{{$trans->name}}" data-amount="{{$trans->amount}}" data-milestoneid="{{$trans->milestone_id}}" data-duedate="{{$trans->due_date}}" data-type="2" data-status="{{\App\Models\Transaction::STATUS_REJECTED}}">Reject</button>
                                {{--<button class="btn btn-success btn-white-bg-green-text rejectMilestoneEmployer" id="rejectMilestoneEmployer" type="button" data-id="{{ $trans->id }}" data-transactionid="{{$trans->id}}" data-name="{{$trans->name}}" data-amount="{{$trans->amount}}" data-milestoneid="{{$trans->milestone_id}}" data-duedate="{{$trans->due_date}}" data-type="2" data-status="{{\App\Models\Transaction::STATUS_REJECTED}}">Reject</button>--}}
                            </td>
                                @endif

                        @endif
                    </tr>
                @endif
            @endforeach
        @endforeach
    </table>
    </div>
@endif