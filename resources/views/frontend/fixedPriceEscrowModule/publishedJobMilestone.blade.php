@extends('frontend.layouts.default')

@section('title')
    Published Job Milestone
@endsection

@section('description')
    Published Job Milestone
@endsection

@section('author')
    Xenren
@endsection

@section('url')
    https://www.xenren.co/myPublishOrder/orderDetail
@endsection

@section('images')
    https://www.xenren.co/images/1200x800-1c.png
@endsection

@section('header')
    <link href="/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css"/>
    <link href="/custom/css/frontend/userCenter.css" rel="stylesheet" type="text/css"/>
    <link href="/custom/css/frontend/fixedPriceEscrowModule.css" rel="stylesheet">
    <link href="/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet"
          type="text/css"/>
    <style>
        span.desc:hover {
            color: green;
        }
        .current-milestone .btn {
            padding: 9px 10px;
        }

        /*for model start*/
        .confirm-box-model-contant {
            background-color: #F4F7F7;
            border-radius: 8px !important;
            padding-bottom: 30px;
        }

        .confirm-box .modal-content {
            border-radius: 8px !important;
        }

        .confirm-box-model-contant img {
            margin-top: 15px;
        }

        .confirm-box-model-contant p {
            color: #61AF2C;
            font-weight: bold;
            font-size: 18px;
        }

        .confirm-box .modal-dialog {
            /*width:450px;*/
        }

        .confirm-box .modal-label {
            color: #7E7E7E;
            font-size: 13px;
            font-weight: 550;
        }

        .confirm-box-model-contant .form-group {
            margin-bottom: 5px !important;
        }

        .confirm-box-model-btn-section {
            margin-top: 20px;
        }

        .radio-label {
            font-size: 13px;
        }

        /*model end*/

        /*thead, tbody { display: block; }*/

        /*tbody {*/
        /*max-height: 300px;*/
        /*overflow-y: auto; !* Trigger vertical scroll *!*/
        /*overflow-x: hidden; !* Hide the horizontal scroll *!*/
        /*}*/
    </style>
@endsection

@section('modal')
    <!-- model strat-->
    <!-- Modal -->
    <div class="modal fade confirm-box" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body confirm-box-model-contant">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <img src="/images/icons/confirmBox.png">
                        </div>
                        <div class="col-md-12 text-center">
                            <p>{{trans('common.are_you_sure_you_want_to_reject')}} ?</p>
                        </div>
                        <div class="col-md-12 released-milestone">
                            <form>
                                {{--<div class="form-group">--}}
                                {{--<label for="comment" class="modal-label">Explanation</label><br>--}}

                                {{--<div class="md-radio">--}}
                                {{--<input type="radio" class="md-radiobtn" name="radio3" id="radio3">--}}
                                {{--<label for="radio3" class="radio-label">Terms and condition--}}
                                {{--<span class="inc"></span>--}}
                                {{--<span class="check"></span>--}}
                                {{--<span class="box"></span>--}}
                                {{--</label>--}}
                                {{--</div>--}}

                                {{--<div class="md-radio">--}}
                                {{--<input type="radio" class="md-radiobtn" name="radio3" id="radio4">--}}
                                {{--<label for="radio4" class="radio-label">Explanation 2--}}
                                {{--<span class="inc"></span>--}}
                                {{--<span class="check"></span>--}}
                                {{--<span class="box"></span>--}}
                                {{--</label>--}}
                                {{--</div>--}}

                                {{--</div>--}}
                                <div class="form-group">
                                    <label for="comment" class="modal-label">{{trans('common.reason')}}</label>
                                    <textarea class="form-control comment" rows="5" id="comment"></textarea>
                                </div>
                                <div class="form-group">
                                    <a href="/terms-service" target="_blank" class="modal-label"
                                       style="color: #61AF2C">{{trans('common.terms_and_condition')}}</a>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-12 confirm-box-model-btn-section">
                            <div class="row">
                                <div class="col-md-6">
                                    <button
                                        class="btn btn-success btn-green-bg-white-text btn-block rejectMilestoneEmployer setMPUPBNTN1"
                                        type="button">{{trans('common.confirm')}}</button>
                                </div>
                                <div class="col-md-6">
                                    <button class="btn btn-success btn-white-bg-green-text btn-block setMPUPBNTN1"
                                            type="button" data-dismiss="modal">{{trans('common.Cancel')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- model end-->


    @include('frontend.modals.confirmModal',[
        'message' => 'Only release milestone when freelancer 100% complete the current milestone',
    ])

    @include('frontend.modals.disputeModal',[
        'message' => 'Tell us what\'s happening',
    ])

    @include('frontend.modals.modal',[
        'message' => 'Why do you want to make refund request?',
        'id' => 'refundRequestModal',
        'successClass' => 'confirmDisputeMilestoneBtn'
    ])

    @include('frontend.modals.modal',[
        'message' => 'Why do you want to cancel this milestone??',
        'id' => 'rejectMileStoneModal',
        'successClass' => 'submitRejectMileStone',
        'commentId' => 'cancelCurrentMilestone'
    ])

    @include('frontend.modals.modal',[
        'message' => 'Only release milestone when freelancer 100% complete the current milestone',
        'id' => 'acceptRequestingMilestone',
        'successClass' => 'confirmMilestoneEmployerSubmit',
        'commentId' => 'acceptRequestingMilestoneComment',
        'reason' => false
    ])

    @include('frontend.fixedPriceEscrowModule.giveBonus', [
        'project_id' => $data->id,
        'applicant_id' => $data->awardedApplicant->user_id,
    ])
@endsection

@section('content')
    <div class="page-content-wrapper">
        <div class="col-md-12 usercenter link-div">
            <a href="{{route('frontend.mypublishorder',\Session::get('lang'))}}" class="text-uppercase raleway">
	        <span class="fa fa-angle-left">
	        </span>
                {{trans('common.back_to_my_orders')}}
            </a>
        </div>
        <div class="clearfix"></div>
        <br>
        <?php
        $projectId = $data->id;
        $applicantId = $data->awardedApplicant->user_id;
        ?>
        <script>
            window.ProjectId = '{{$projectId}}';
            window.token = '{{csrf_token()}}';
        </script>

        <div class="col-md-12">
            <div class="row main-div">
                <div class="col-md-12 personal-profile setPPROFILECol12MSP">
                    <div class="row">
                        <div class="col-md-8 setSpn1JMPP">
                            <span class="set-mainRow-span1">{{trans('common.about_staff')}}</span>
                        </div>
                        <div class="col-md-8">
                            <div class="row user-info">
                                <div class="col-md-3 text-center">
                                    <div class="profile-userpic setProfileUserPICPJMP">
                                        <img alt="" class="img-responsive"
                                             src="{{asset($data->awardedApplicant->user->img_avatar)}}"
                                             onerror="this.src='/images/avatar_xenren.png'">
                                        {{--<label class="btn-bs-file btn btn-xs">--}}
                                        {{--<i class="fa fa-camera"></i>--}}
                                        {{--<input type="file" />--}}
                                        {{--</label>--}}
                                    </div>
                                </div>
                                <div class="col-md-9 basic-info">
                                    <div class="row">
                                        <div class="col-md-12 user-full-name">
                                            <span class="full-name">{{$data->awardedApplicant->user->name}}</span>
                                            <img src="/images/icons/check.png">
                                        </div>
                                    </div>

                                    <div class="row setColRowR4">
                                        <div class="col-md-3">
                                            <small class="setSmall">{{trans('common.status')}}:</small>
                                        </div>
                                        <div class="col-md-9">
                                            <span class="status">{{trans('common.on_your_project')}}</span>
                                            {{--<button class="btn btn-success btn-green-bg-white-text setBNTNPJMBP" type="button" onClick="window.location='/inbox'">{{trans('common.chat')}}</button>--}}
                                            <button class="btn btn-success btn-green-bg-white-text setBNTNPJMBP"
                                                    type="button" data-toggle="modal"
                                                    data-target="#sendPrivateMessage">{{trans('common.chat')}}</button>
                                        </div>
                                    </div>

                                    <!-- private message modal -->
                                    <!-- Modal -->
                                    <div id="sendPrivateMessage" class="modal fade" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <a type="button" class="set-cross-click-pjmp" data-dismiss="modal">&times;</a>
                                                    <h4 class="modal-title text-left">{{ trans('common.send_message') }}</h4>
                                                </div>
                                                {!! Form::open(['route' => ['frontend.sendprivatemessage.post', 'user_id' => $userId], 'method' => 'post', 'role' => 'form', 'id' => 'send-message-form']) !!}
                                                <div class="modal-body">
                                                    <div class="form-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label>{{ trans('common.message') }}</label>
                                                                <div class="input-group">
                                                                    {{ Form::textarea('message', null, ['class' => 'form-control', 'cols' => '100', 'rows' => '3', 'id' => 'msg']) }}
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <input type="hidden" name="project_id" value="{{$data->id}}">
                                                <input type="hidden" name="to_user_id" id="user_id" value="{{$userId}}">
                                                {!! Form::close() !!}
                                                <div class="modal-footer">
                                                    <button class="btn btn-success sendMEssagebtn" id="btnSubmit">Send
                                                        Message
                                                    </button>
                                                    <button type="button" class="btn btn-default btndefault-sendMessage"
                                                            data-dismiss="modal">Close
                                                    </button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- private message modal -->

                                    <div class="row setColRowRS3">
                                        <div class="col-md-3">
                                            <small class="setSmall">{{trans('common.rate')}}:</small>
                                        </div>
                                        <div class="col-md-9">
                                            <span
                                                class="details">${{$data->awardedApplicant->user->hourly_pay}} /{{trans('common.hr')}}</span>
                                        </div>
                                    </div>

                                    <div class="row setColRowRS2">
                                        <div class="col-md-3">
                                            <small class="setSmall">{{trans('common.age')}}:</small>
                                        </div>
                                        <div class="col-md-9">
                                            <?php
                                            $date = \Carbon\Carbon::parse($data->awardedApplicant->user->date_of_birth);
                                            $age = $date->diff(\Carbon\Carbon::now())->format('%y');
                                            ?>
                                            <span class="details">{{$age}}</span>
                                        </div>
                                    </div>
                                    <div class="row setColRowRS1">
                                        <div class="col-md-3">
                                            <small class="setSmall">{{trans('common.experience')}}:</small>
                                        </div>
                                        <div class="col-md-9">
                                            <span
                                                class="details">{{$data->awardedApplicant->user->experience}} {{trans('common.year')}}</span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 contacts-info setContactInfo">
                                    <div class="row">

                                        <div class="col-md-3 col-md-3 contacts-info-sub setPHMPPP">
                                            <img src="/images/skype-icon-5.png"> <span
                                                class="setSP0">{{trans('common.skype')}}</span>
                                            <p>{{$data->awardedApplicant->user->skype_id or '-'}}</p>
                                        </div>

                                        <div class="col-md-3 col-md-3 contacts-info-sub setPHMPPP">
                                            <img src="/images/wechat-logo.png"> <span
                                                class="setSP0">{{trans('common.wechat')}}</span>
                                            <p>{{$data->awardedApplicant->user->wechat_id or '-'}}</p>
                                        </div>

                                        <div class="col-md-3 col-md-3 contacts-info-sub setPHMPPP">
                                            <img src="/images/line.png"> <span
                                                class="setSP0">{{trans('common.line')}}</span>
                                            <p>{{$data->awardedApplicant->user->line_id or '-'}}</p>
                                        </div>

                                        <div class="col-md-3 col-md-3 contacts-info-sub setPHMPPP">
                                            <img src="/images/MetroUI_Phone.png">
                                            <span class="setSP0">{{trans('common.phone')}}</span>
                                            <p>{{$data->awardedApplicant->user->phone_no or '-'}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row basic-info setRO">
                                <div class="col-md-12 basic-info-title text-left setSkillRowCol12JPM">
                                    <img alt="" src="/images/hands.png"> &nbsp;<span
                                        class="subtitle"> {{trans('common.skill')}}</span>
                                </div>
                                <div class="col-md-12  scroll-div">
                                    @foreach($data->awardedApplicant->user->skills as $v)
                                        <div class="skill-div">
	                                <span class="item-skill unverified">
	                    				<span>{{$v->skill->name_cn}}</span>
	                				</span>
                                        </div>
                                    @endforeach
                                    <div class="clearfix"></div>
                                </div>

                                <div class="col-md-12 basic-info-title text-left setLANGDIV">
                                    <br><img alt="" src="/images/earth.png"> &nbsp;<span
                                        class="subtitle"> {{trans('common.languagee')}}</span>
                                </div>

                                @if(isset($data->awardedApplicant) && isset($data->awardedApplicant->user) && isset($data->awardedApplicant->languages))
                                    <div class="col-md-12 media similar-profiles text-left setLANGUAGEDIV">
                                        @foreach($data->awardedApplicant->user->languages as $v)
                                            <div class="media-left media-middle item-languages">
                                                <img class="pull-left" alt="..."
                                                     src="http://xenrencc.dev/images/Flags-Icon-Set/24x24/HK.png"
                                                     class="media-object">
                                                <span
                                                    class="pull-left">{{isset($v->languageDetail->name) ? isset($v->languageDetail->name): ''}}</span>
                                            </div>
                                        @endforeach

                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 prising-section text-center setPrisingSection">
                    <div class="row">
                        <div class="col-md-4 prising-section-sub">
                            <img src="/images/icons/doller-icon.png"><br>
                            <p class="setSP0">{{trans('common.budget_for_the_project')}}</p>
                            <span class="setSP">${{$data->awardedApplicant->quote_price}} USD</span>
                        </div>
                        <div class="col-md-4 prising-section-sub">
                            <img src="/images/icons/time-icon.png"><br>
                            <p class="setSP0">{{trans('common.current_milestone')}}</p>
                            <span class="setSP">$700USD</span>
                        </div>
                        @if($data->status != 4)
                            <div class="col-md-4 prising-section-sub">
                                <img src="/images/icons/doller-icon.png"><br>
                                <p class="setSP0">{{$data['awardedApplicant']['pay_type'] != 1 ? trans('common.hourly_price') : trans('common.fixed_price') }}</p>
                                @if($data['awardedApplicant']['pay_type'] == 1)
                                    <button class="btn btn-success btn-green-bg-white-text switchFixed setBTNBNTPJM"
                                            type="button">
                                        {{trans('common.switch_to_fixed')}}
                                    </button>
                                @else
                                    <button class="btn btn-success btn-green-bg-white-text switchHourly setBTNBNTPJM"
                                            type="button">
                                        {{trans('common.switch_to_hourly')}}
                                    </button>
                                @endif
                            </div>
                        @endif
                    </div>
                </div>

                <div class="col-md-12 description setDEscriptionJPJM">
                    <h4>{{$data->name}}</h4>
                <!-- <h5>{!!$data->description!!}</h5> -->
                    <div class="row setROewe">
                        <div class="col-md-12 scroll-div">
                            <p class="setPpP">
                                {!!$data->description!!}
                            </p>
                        </div>
                    </div>
                </div>
                @if($data->awardedApplicant->status != \App\Models\ProjectApplicant::STATUS_COMPLETED)
                    <div class="col-md-12 milestone-desc">
                        <div class="setUL">
                            <ul class="nav nav-tabs setULNAVVTBSA" role="tablist">
                                <li role="presentation" class="active text-center"><a href="#milestone" class="setLI"
                                                                                      aria-controls="milestone"
                                                                                      role="tab"
                                                                                      data-toggle="tab">{{trans('common.milestone')}}</a>
                                </li>
                                <li role="presentation" class="text-center"><a href="#dispute" class="setLI"
                                                                               aria-controls="dispute" role="tab"
                                                                               data-toggle="tab">{{trans('common.dispute')}}</a>
                                </li>
                                <li role="presentation" class="text-center"><a href="#contract-control-panel"
                                                                               class="setLI"
                                                                               aria-controls="contract-control-panel"
                                                                               role="tab"
                                                                               data-toggle="tab">{{trans('common.contract_control_panel')}}</a>
                                </li>
                                <li role="presentation" class="text-center setendContractLi"><a href="#end-contract"
                                                                                                class="setLI"
                                                                                                aria-controls="end-contract"
                                                                                                role="tab"
                                                                                                data-toggle="tab">{{trans('common.end_contract')}}</a>
                                </li>
                            </ul>
                        </div>

                        <div class="tab-content tabpanel-custom setTabsPANEL">
                            <div role="tabpanel" class="tab-pane active" id="milestone">

                                <div class="row new-milestone-add setROWNEWMILSTONEADD">
                                    <div class="col-md-12">
                                        <label
                                            class="milestone-subpart-title">{{trans('common.add_new_milestone')}}</label>
                                        <form accept="">
                                            <div class="form-group row">
                                                <input type="hidden" name="applicant_id" id="applicant_id"
                                                       value="{{$userId}}">
                                                <div class="col-xs-2 setColXs2">
                                                    <label for="ex1">{{trans('common.enter_amount')}}</label>
                                                    <input class="form-control" id="milestones_amount" type="text">
                                                </div>
                                                <div class="col-xs-2 setColXs2">
                                                    <label for="ex2">{{trans('common.milestone_name')}}</label>
                                                    <input class="form-control" id="milestones_name" type="text">
                                                </div>
                                                <div class="col-xs-5 setColxs5">
                                                    <label for="ex3">{{trans('common.milestone_description')}}</label>
                                                    <input class="form-control" id="milestones_description" type="text">
                                                </div>
                                                <div class="col-xs-3 setColxs3">
                                                    <label for="ex2">{{trans('common.select_due_date')}}</label>
                                                    <div class="input-group date form_datetime">
                                                        <input id="milestones_duedate" type="text" class="form-control"
                                                               readonly="" size="16">
                                                        <span class="input-group-btn">
															<button type="button" class="btn default date-set">
																<i class="fa fa-calendar"></i>
															</button>
                                                                                                                </span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <br>
                                                    <button class="btn btn-success btn-green-bg-white-text setBNTNNTN"
                                                            id="confirmMilestone"
                                                            type="submit">{{trans('common.confirm')}}</button>
                                                    <button class="btn btn-success btn-white-bg-green-text setBNTNNTN"
                                                            id="cancelMilestone"
                                                            type="button">{{trans('common.Cancel')}}</button>
                                                    <button class="btn btn-success btn-white-bg-green-text setBNTNNTN"
                                                            id="cancelMilestone" type="button" style="float: right"
                                                            data-toggle="modal" data-target="#giveBobusModal">Give Bonus
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-12 set-this-col-md-12">
                                    <label class="milestone-subpart-title">{{trans('common.current_milestone')}}</label>
                                </div>
                                <div class="row current-milestone setRowPJMBPAGE">
                                    <div class="col-md-9">
                                        <div id="currentmsdiv" class="">
                                            <table class="table custom-table">
                                                <?php
                                                $miles = $data->milestones->sortByDesc('created_at');
                                                ?>
                                                @foreach($miles as $v)
                                                    @if(isset($v->milestoneStatus) && ($v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_INPROGRESS ||  $v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_CANCEL_REQUEST_BY_EMPLOYER))
                                                        <tr>
                                                            <td width="30px"><i class="fa fa-circle"></i></td>
                                                            <td>
                                                                <span
                                                                    class="title">{{trans('common.amount')}}</span><br>
                                                                <span class="desc" data-toggle="tooltip" title="{{number_format($local_currency_rate * $v->amount,2).' '.$local_currency}}">${{number_format($v->amount,2)}}</span>
                                                            </td>
                                                            <td>
                                                                <span
                                                                    class="title">{{trans('common.milestone_name')}}</span><br>
                                                                <span class="desc">{{$v->name}}</span>
                                                            </td>
                                                            <td>
                                                                <span
                                                                    class="title">{{trans('common.milestone_description')}}</span><br>
                                                                <span class="desc">{{$v->description}}</span>
                                                            </td>
                                                            <td>
                                                                <span
                                                                    class="title">{{trans('common.due_date')}}</span><br>
                                                                <span class="desc">{{$v->due_date}}</span>
                                                            </td>
                                                            @if($v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_REJECTED)
                                                                @if(isset($data) && $data && \Auth::user() && $data->user_id == \Auth::user()->id)
                                                                    <td>
                                                                        <label for="">Change Status</label>
                                                                        <select name="" id="changeStatus"
                                                                                class="form-control changeStatus"
                                                                                data-milestone="{{$v->id}}">
                                                                            <option
                                                                                value="">{{trans('common.select_status')}}</option>
                                                                            <option
                                                                                value="1">{{trans('common.in_progress')}}</option>
                                                                            <option
                                                                                value="2">{{trnas('common.confirm')}}</option>
                                                                        </select>
                                                                    </td>
                                                                @endif
                                                            @endif
                                                            @if($v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_REQUESTED)
                                                                @if(isset($data) && $data && \Auth::user() && $data->user_id == \Auth::user()->id)
                                                                    <td>
                                                                        <select name="" id="changeStatus"
                                                                                class="form-control changeStatus"
                                                                                data-milestone="{{$v->id}}">
                                                                            <option value="">Select Status</option>
                                                                            <option
                                                                                value="{{\App\Models\ProjectMilestones::STATUS_REJECTED}}">{{trans('common.confirm')}}</option>
                                                                            <option
                                                                                value="{{\App\Models\ProjectMilestones::STATUS_INPROGRESS}}">{{trans('common.Cancel')}}</option>
                                                                        </select>
                                                                    </td>
                                                                @endif
                                                            @endif
                                                            @if($v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_CANCEL_REQUEST_BY_EMPLOYER)
                                                                <td>
                                                                    <p style="color: orangered">{{trans('common.cancel_request_sent')}}</p>
                                                                </td>
                                                            @endif
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                    @if(isset($data->milestones) && count($data->milestones) > 0 && isset($completed_milestones_count) && $completed_milestones_count > 0)
                                        @foreach($data->milestones as $v)
                                            @if(isset($v->milestoneStatus) && ($v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_INPROGRESS) && $v->applicant_id == $userId )
                                                <div class="col-md-3 setCol3PJMILE">
                                                    <label class="milestone-subpart-title reasonTextArea"
                                                           style="display:none;">{{trans('common.please_explain_reason')}}</label>

                                                    <form accept="">
                                                        <div class="form-group row setFormPJMP">
                                                            <div class="col-xs-12 reasonTextArea" style="display:none;">
                                                                <textarea class="form-control" id="reason"></textarea>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <br>
                                                                @php
                                                                    $transactionId = $v->transaction->id;
                                                                    $inDispute = \App\Models\ProjectDispute::where('project_id', '=', $projectId)->where('status', '=', \App\Models\ProjectDispute::STATUS_INPROGRESS)->first();
                                                                @endphp
                                                                @if(is_null($inDispute) || $inDispute->status != \App\Models\ProjectDispute::STATUS_INPROGRESS )
                                                                    <button
                                                                        class="btn btn-success btn-green-bg-white-text setRLEASEBTNMSP"
                                                                        id="confirmMileStonebtn" type="button"
                                                                        data-milsetone="{{$v->id}}">{{trans('common.release')}}</button>
                                                                    <button
                                                                        class="btn btn-success btn-white-bg-green-text setRLEASEBTNMSP"
                                                                        id="rejectMileStoneBtn"
                                                                        type="button">{{trans('common.Cancel')}}</button>
                                                                    <button
                                                                        class="btn btn-success btn-green-bg-white-text"
                                                                        id="submitRejectMileStone" type="button"
                                                                        style="display:none;">{{trans('common.submit')}}</button>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </form>
                                                    @endif
                                                    @endforeach
                                                </div>
                                            @endif
                                </div>

                                <div class="row release-milestone">
                                    <div class="col-md-12">
                                        <label
                                            class="milestone-subpart-title">{{trans('common.milestone_that_already_released')}}
                                            <span class="green" id="ttr"> $0</span></label>
                                        <div class="setRowPJMBPAGE">
                                            <table class="table custom-table">
                                                <?php
                                                $total = 0;
                                                $totalReleasedAmount = 0;
                                                $md = 0;
                                                $msd = 0;
                                                ?>

                                                @foreach($data->milestones as $v)
                                                    @if($v->milestoneStatus->status == \App\Models\Transaction::STATUS_APPROVED || $v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_DISPUTED || $v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_FREELANCER_REJECTED_REFUND)
                                                        <?php
                                                        $totalReleasedAmount = $totalReleasedAmount + $v->amount;
                                                        ?>
                                                        @if($v->amount > 0)
                                                            <tr>
                                                                <td width="30px"><i class="fa fa-circle"
                                                                                    style="color: green;"></i></td>
                                                                <td>
                                                                    <span
                                                                        class="title">{{trans('common.amount')}}</span><br>
                                                                    <span
                                                                        class="desc"  data-toggle="tooltip" title="{{number_format($local_currency_rate * $v->amount,2).' '.$local_currency}}">${{number_format($v->amount,2)}}</span>
                                                                </td>
                                                                <td>
                                                                    <span
                                                                        class="title">{{trans('common.milestone_name')}}</span><br>
                                                                    <span class="desc">{{$v->name}}</span>
                                                                </td>
                                                                <td>
                                                                    <span
                                                                        class="title">{{trans('common.due_date')}}</span><br>
                                                                    <span class="desc">{{$v->due_date}}</span>
                                                                </td>
                                                                <td>
                                                                    <span
                                                                        class="title">{{trans('common.release_date')}}</span><br>
                                                                    <span
                                                                        class="desc">{{isset($v->transaction->release_at) ? $v->transaction->release_at : 'N-A'}}</span>
                                                                </td>
                                                                <td width="300px">
                                                                    <span
                                                                        class="title">{{trans('common.description')}}</span><br>
                                                                    <span class="desc">{{$v->description}}</span>
                                                                </td>
                                                                <td>
                                                                    @if($v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_DISPUTED)
                                                                        <span style="color: #57b029" id="ttr">Refund Request Sent</span>
                                                                    @elseif($v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_FREELANCER_REJECTED_REFUND)
                                                                        @if(!is_null($v->filedDisputes))
                                                                            <b style="color: orangered" id="ttr">Filed
                                                                                Dispute</b>
                                                                            <a href="/project/disputeDetail/{{$v->filedDisputes->id}}">See
                                                                                Detail</a><br/>
                                                                            @if(isset($v->filedDisputes->status) && $v->filedDisputes->status == \App\Models\ProjectFileDisputes::STATUS_FILE_DISPUTE_IN_APPROVED)
                                                                                <span
                                                                                    style="color: green">{{trans('common.approved')}}</span>
                                                                            @endif
                                                                            @if(isset($v->filedDisputes->status) && $v->filedDisputes->status == \App\Models\ProjectFileDisputes::STATUS_FILE_DISPUTE_IN_REJECTED)
                                                                                <span
                                                                                    style="color: darkred">{{trans('common.rejected')}}</span>
                                                                            @endif
                                                                        @else
                                                                            <span style="color: red"
                                                                                  id="ttr">{{trans('common.refund_request_rejected')}}</span>
                                                                            <br/>
                                                                            <span style="color: red" id="ttr">{{trans('common.reason')}}:</span> {{$v->milestoneStatus->comment}}
                                                                            <br/>
                                                                            <input
                                                                                class="btn btn-success btn-green-bg-white-text fileDisputeEmployer"
                                                                                value="File Dispute"
                                                                                style="border-color: #ccc !important; background-color: #57b029 !important"
                                                                                data-transactionid="{{isset($v->transaction) ? $v->transaction->id: 0}}"
                                                                                data-milestoneid="{{$v->id}}"
                                                                                data-project="{{$data->id}}"
                                                                                type="button">
                                                                        @endif
                                                                    @else

                                                                        <?php
                                                                        $now = \Carbon\Carbon::now();
                                                                        $dateCreated = isset($v->transaction->release_at) ? \Carbon\Carbon::parse($v->transaction->release_at) : \Carbon\Carbon::now()->startOfMonth();
                                                                        ?>
                                                                        @if($dateCreated->diffInDays() < config('milestones.system_hold_days'))
                                                                            <button
                                                                                class="btn btn-success btn-green-bg-white-text disputeMilestoneBtn"
                                                                                id="dispute-milestone-btn-{{$v->id}}"
                                                                                type="button"
                                                                                data-milestone="{{$v->id}}">{{trans('common.refund_requests')}}</button>
                                                                        @endif
                                                                    @endif
                                                                    <div id="dispute-explain-{{$v->id}}"
                                                                         style="display: none;">
                                                                        <span class="title">Please Explain Reason</span><br>
                                                                        <textarea id="dispute-reason-{{$v->id}}"
                                                                                  rows="5" class="form-control"
                                                                                  data-milestone="{{$v->id}}"></textarea>
                                                                        <div class="btn-group">
                                                                            <button
                                                                                class="btn btn-success confirmDisputeMilestoneBtn setMPUPBNTN1"
                                                                                data-milestone="{{$v->id}}">{{trans('common.confirm')}}</button>
                                                                            <button
                                                                                class="btn btn-success rejectDisputeMilestoneBtn setMPUPBNTN1"
                                                                                data-milestone="{{$v->id}}">{{trans('common.reject')}}</button>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endif
                                                @endforeach

                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row release-milestone">
                                    <div class="col-md-12">
                                        <label
                                            class="milestone-subpart-title">{{trans('common.disputed_milestone')}}</label>
                                        @include('frontend.fixedPriceEscrowModule.currentMilestones',array(
                                            'milestones' => $data->milestones,
                                            'status' => \App\Models\ProjectMilestones::STATUS_DISPUTED,
                                            'data' => $data,
                                            'type' => 2
                                        ))
                                    </div>
                                </div>
                                <div class="row release-milestone">
                                    <div class="col-md-12">
                                        <label
                                            class="milestone-subpart-title">{{trans('common.requesting_milestones')}}</label>
                                        @include('frontend.fixedPriceEscrowModule.currentMilestones',array(
                                            'milestones' => $data->milestones,
                                            'status' => \App\Models\ProjectMilestones::STATUS_REQUESTED,
                                            'data' => $data,
                                            'type' => 1
                                        ))
                                    </div>
                                </div>
                                <div class="row release-milestone">
                                    <div class="col-md-12">
                                        <label
                                            class="milestone-subpart-title">{{trans('common.rejected_milestone')}}</label>
                                        <div class="setRowPJMBPAGE">
                                            <table class="table custom-table">
                                                @foreach($data->milestones as $v)
                                                    @foreach($v->transactions as $trans)
                                                        @if(
                                                        $trans->status == \App\Models\Transaction::STATUS_REJECTED ||
                                                        $trans->status == \App\Models\Transaction::STATUS_DISPUTE_REJECTED ||
                                                        $trans->status == \App\Models\Transaction::STATUS_DISPUTE_APPROVED ||
                                                        $trans->status == \App\Models\Transaction::STATUS_IN_PROGRESS_DISPUTE ||
                                                        $trans->status == \App\Models\Transaction::STATUS_DISPUTE_CANCELED_BY_FREELANCER
                                                        )
                                                            <tr>
                                                                <td width="30px"><i class="fa fa-circle"></i></td>
                                                                <td>
                                                                    <span
                                                                        class="title">{{trans('common.amount')}}</span><br>
                                                                    <span
                                                                        class="desc" data-toggle="tooltip" title="{{number_format($local_currency_rate * $trans->amount,2).' '.$local_currency}}">${{number_format($trans->amount,2)}}</span>
                                                                </td>
                                                                <td>
                                                                    <span
                                                                        class="title">{{trans('common.milestone_name')}}</span><br>
                                                                    <span class="desc">{{$v->name}}</span>
                                                                </td>
                                                                <td>
                                                                    <span class="title">Transaction Name</span><br>
                                                                    <span class="desc">{{$trans->name}}</span>
                                                                </td>
                                                                {{--<td>--}}
                                                                {{--<span class="title">{{trans('common.due_date')}}</span><br>--}}
                                                                {{--<span class="desc">{{$trans->due_date}}</span>--}}
                                                                {{--</td>--}}
                                                                <td>
                                                                    <span
                                                                        class="title">{{trans('common.status')}}</span><br>
                                                                    <span class="desc">
																	@if(isset($trans->filedTransactionDispute))
                                                                            <b style="color: orangered" id="ttr">{{trans('common.filed_dispute')}} <a
                                                                                    href="/project/disputeDetail/{{$trans->filedTransactionDispute->id}}"> See Detail</a></b>
                                                                            <br/>
                                                                            @if($trans->filedTransactionDispute->status == \App\Models\ProjectFileDisputes::STATUS_FILE_DISPUTE_IN_REJECTED)
                                                                                <b style="color: darkred">{{trans('common.rejected')}}</b>
                                                                            @endif
                                                                            @if($trans->filedTransactionDispute->status == \App\Models\ProjectFileDisputes::STATUS_FILE_DISPUTE_IN_APPROVED)
                                                                                <b style="color: darkgreen">{{trans('common.approved')}}</b>
                                                                            @endif
                                                                            @if($trans->filedTransactionDispute->status == \App\Models\ProjectFileDisputes::STATUS_CANCELED_BY_FREELANCER)
                                                                                <b style="color: darkred">{{trans('common.rejected')}} --</b>
                                                                            @endif
                                                                        @else
                                                                            {{trans('common.rejected')}}
                                                                        @endif
																</span>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row release-milestone">
                                    <div class="col-md-12">
                                        <label
                                            class="milestone-subpart-title">{{trans('common.refunded_milestones')}}</label>
                                        @include('frontend.fixedPriceEscrowModule.currentMilestones',array(
                                            'milestones' => $data->milestones,
                                            'status' => \App\Models\ProjectMilestones::STATUS_REFUNDED,
                                            'data' => $data,
                                            'type' => 2
                                        ))
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="dispute">
                                <div class="row client-request">
                                    <div class="col-md-12">
                                        <label class="milestone-subpart-title">{{trans('common.dispute')}}:</label>
                                        <label
                                            class="milestone-subpart-title">{{trans('common.total_amount_of_this_project_has_been_released')}}
                                            : <span class="green">$1700</span></label>

                                        <div class="col-md-6 setColMd6PJM">
                                            <div class="form-group col-xs-12">
                                                <label for="type">
                                                    <input type="radio" id="type" class="md-radiobtn" name="manual"
                                                           value="1">
                                                    <b>{{trans('common.manual_amount_to_be_dispute')}}</b>
                                                </label>
                                                {{--<label for="manual">--}}
                                                {{--<span class="inc"></span>--}}
                                                {{--<span class="check"></span>--}}
                                                {{--<span class="box"></span>--}}
                                                {{--</label>--}}
                                            </div>
                                            <div class="col-xs-6 setColxs6AMTODIS">
                                                <label for=""><b>{{trans('common.amount_to_dispute')}}</b></label>
                                                <input type="text" class="form-control" id="manualAmount">
                                            </div>
                                            <div class="col-xs-12 setColXS12DISPUTMPKJU set-ColXS-12-DISPUT-MPKJU">
                                                <br>
                                                <button class="btn btn-success btn-green-bg-white-text setMPDISPUTEBNT"
                                                        id="confirmManualMilestone"
                                                        type="submit">{{trans('common.dispute')}}</button>
                                                <button class="btn btn-success btn-white-bg-green-text setMPDISPUTEBNT"
                                                        id="cancelManualMilestone"
                                                        type="button">{{trans('common.Cancel')}}</button>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group col-xs-12 setMilStoneRColXs12">
                                                <label for="type">
                                                    <input type="radio" id="type" class="md-radiobtn" name="manual"
                                                           value="2">
                                                    <b>{{trans('common.milestone_that_already_released')}}</b>
                                                </label>

                                                {{--<label for="manual">--}}
                                                {{--<span class="inc"></span>--}}
                                                {{--<span class="check"></span>--}}
                                                {{--<span class="box"></span>--}}
                                                {{--</label>--}}
                                            </div>
                                            <div class="col-xs-12 setColxs12PJMP">
                                                <div class="setRowPJMBPAGE">
                                                    <table class="table custom-table">
                                                        <tbody>
                                                        @foreach($data->milestones as $v)
                                                            @if($v->milestoneStatus->status == \App\Models\ProjectMilestones::STATUS_CONFIRMED)
                                                                <?php
                                                                $total = number_format($total + $v->amount, 2);
                                                                ?>
                                                                <tr style="background: white; border-bottom: 1px solid lightgray;">
                                                                    <td width="30px"></td>
                                                                    <td>
                                                                        <span
                                                                            class="title">{{trans('common.amount')}}</span><br>
                                                                        <span class="desc" data-toggle="tooltip" title="{{number_format($local_currency_rate * $v->amount,2).' '.$local_currency}}">${{number_format($v->amount, 2)}}</span>
                                                                    </td>
                                                                    <td>
                                                                        <span
                                                                            class="title">{{trans('common.milestone_name')}}</span><br>
                                                                        <span class="desc">{{$v->name}}</span>
                                                                    </td>
                                                                    <td>
                                                                        <span
                                                                            class="title">{{trans('common.due_date')}}</span><br>
                                                                        <span class="desc">{{$v->due_date}}</span>
                                                                    </td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                            <input type="radio" class="md-radiobtn"
                                                                                   name="autoMilestone"
                                                                                   id="autoMilestone"
                                                                                   data-amount="{{$v->amount}}"
                                                                                   value="{{$v->id}}">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 setColXs12-Xs-xol">
                                                <br/>
                                                <label for="" class="milestone-subpart-title">
                                                    {{trans('common.total_amount_that_able_to_dispute')}}: <span
                                                        class="green">${{$total}}</span>
                                                </label>
                                                <label for="" class="milestone-subpart-title">
                                                    {{trans('common.total_amount_that_disputes')}}: <span class="green">$<span
                                                            id="milestoneAmount">0.00</span></span>
                                                </label>
                                            </div>
                                            <div class="col-xs-12 setColXS12DISPUTMPKJU">
                                                <br>
                                                <button class="btn btn-success btn-green-bg-white-text setMPDISPUTEBNT"
                                                        id="confirmAutoMilestone"
                                                        type="submit">{{trans('common.dispute')}}</button>
                                                <button class="btn btn-success btn-white-bg-green-text setMPDISPUTEBNT"
                                                        id="cancelMilestone"
                                                        type="button">{{trans('common.Cancel')}}</button>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="row">
                                        <div class="col-md-12 release-milestone setCol12MSTONEA"
                                             style="margin-top:20px">
                                            <label class="milestone-subpart-title">{{trans('common.manual_disputes')}}
                                                <span class="green" id="md">$0.00</span></label>
                                            <div class="setRowPJMBPAGE">
                                                <table class="table custom-table">
                                                    <tbody>
                                                    @foreach($data->disputes as$val)
                                                        @if($val->type == \App\Models\ProjectDispute::TYPE_MANUAL_DISPUTE)
                                                            <tr>
                                                                <td width="30px"></td>
                                                                <td>
                                                                    <span
                                                                        class="title">{{trans('common.amount')}}</span><br>
                                                                    <span class="desc">${{$val->amount}}</span>
                                                                </td>
                                                                <td>
                                                                    <span
                                                                        class="title">{{trans('common.dispute_date')}}</span><br>
                                                                    <span
                                                                        class="desc">{{\Carbon\Carbon::parse($val->created_at)->format('d M Y')}}</span>
                                                                </td>
                                                                <td width="300px">
                                                                    <?php
                                                                    $md = $md + $val->amount;
                                                                    $status = $val->status;
                                                                    $stat = '';
                                                                    if ($status == \App\Models\ProjectDispute::STATUS_INPROGRESS) {
                                                                        $stat = 'InProgress';
                                                                    }
                                                                    if ($status == \App\Models\ProjectDispute::STATUS_REJECTED) {
                                                                        $stat = 'Rejected';
                                                                    }
                                                                    if ($status == \App\Models\ProjectDispute::STATUS_CONFIRM) {
                                                                        $stat = 'Confirmed';
                                                                    }
                                                                    ?>
                                                                    <span
                                                                        class="title">{{trans('common.status')}}</span><br>
                                                                    <span class="desc">{{$stat}}</span>
                                                                </td>
                                                                <td>
                                                                    @if($stat == 'Rejected')
                                                                        <button
                                                                            class="btn btn-success btn-green-bg-white-text requestDisputeAgain"
                                                                            type="button" data-disputeId="{{$val->id}}">
                                                                            {{trans('common.request_dispute_again')}}
                                                                        </button>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12 release-milestone set-col-md-12-re-m-s-t"
                                             style="margin-top:20px">
                                            <label
                                                class="milestone-subpart-title">{{trans('common.milestone_disputes')}}
                                                <span class="green" id="msd">$0.00</span></label>
                                            <div class="setRowPJMBPAGE">
                                                <table class="table custom-table">
                                                    <tbody>
                                                    @foreach($data->disputes as$val)
                                                        @if($val->type == \App\Models\ProjectDispute::TYPE_MILESTONE_DISPUTE)
                                                            <tr>
                                                                <td width="30px"></td>
                                                                <td>
                                                                    <span
                                                                        class="title">{{trans('common.milestone_name')}}</span><br>
                                                                    <span
                                                                        class="desc">{{$val->milestone->name or ''}}</span>
                                                                </td>
                                                                <td>
                                                                    <span
                                                                        class="title">{{trans('common.dispute_date')}}</span><br>
                                                                    <span
                                                                        class="desc">{{\Carbon\Carbon::parse($val->created_at)->format('d M Y')}}</span>
                                                                </td>
                                                                <td width="300px">
                                                                    <?php
                                                                    try {
                                                                        $msd = $md + $val->milestone->amount;
                                                                        $status = $val->status;
                                                                        $stat = '';
                                                                        if ($status == \App\Models\ProjectDispute::STATUS_INPROGRESS) {
                                                                            $stat = 'InProgress';
                                                                        }
                                                                        if ($status == \App\Models\ProjectDispute::STATUS_REJECTED) {
                                                                            $stat = 'Rejected';
                                                                        }
                                                                        if ($status == \App\Models\ProjectDispute::STATUS_CONFIRM) {
                                                                            $stat = 'Confirmed';
                                                                        }
                                                                    } catch (\Exception $e) {
                                                                        $stat = '';
                                                                    }
                                                                    ?>
                                                                    <span
                                                                        class="title">{{trans('common.status')}}</span><br>
                                                                    <span
                                                                        class="desc">{{isset($stat) ? $stat : '' }}</span>
                                                                </td>
                                                                <td>

                                                                    @if($stat == 'Rejected')
                                                                        <button
                                                                            class="btn btn-success btn-green-bg-white-text requestDisputeAgain"
                                                                            type="button" data-disputeId="{{$val->id}}">
                                                                            {{trans('common.request_dispute_again')}}
                                                                        </button>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="contract-control-panel">
                                <br/>
                                <div class="col-md-12 form-group">
                                    <label>
                                        <?php
                                        $dailyUpdate = isset($data->awardedApplicant) ? $data->awardedApplicant->daily_update : 0;
                                        ?>
                                        <input type="checkbox" class="daily_update {{$dailyUpdate}}"
                                               @if($dailyUpdate > 0 || trim($dailyUpdate) == 'Yes') checked="checked"
                                               @endif id="daily_update" data-user="{{$userId}}">
                                        {{trans('common.daily_update')}}
                                    </label>
                                    <p for="">
                                        {{trans('common.employer_require_daily_update')}}
                                    </p>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="end-contract">
                                <div class="row end-contract">
                                    <div class="col-md-6">
                                        <label><strong>{{trans('common.end_contract')}}</strong> </label>
                                        <p>{{trans('common.after_click_confirm_will_jump_to_leave_feedback')}}</p>
                                        <form accept="">
                                            <div class="form-group row">
                                                <div class="col-xs-12">
                                                    <label
                                                        for="ex1"><strong>{{trans('common.reason_of_end_contract')}}</strong></label>
                                                    <textarea class="form-control" id="reason_to_end"></textarea>
                                                </div>
                                                <div class="col-xs-12">
                                                    <br>
                                                    <button class="btn btn-success btn-green-bg-white-text"
                                                            type="submit" id="endContract"
                                                            data-user="{{$userId}}">{{trans('common.confirm')}}</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script type="text/javascript">
        $('#manualAmount').keyup(function () {
            let val = $(this).val();
            console.log(val);
            console.log(parseFloat(val).toFixed(2));
            $('#manualAmount').val(parseFloat(val).toFixed(2))
        })
        $('#rejectMilestoneEmployerBtn').on('click', function () {
            window.name = $(this).data('name');
            window.amount = $(this).data('amount');
            window.milestoneid = $(this).data('milestoneid');
            window.duedate = $(this).data('duedate');
            window.type = $(this).data('type');
            window.status = $(this).data('status');
            window.transactionid = $(this).data('transactionid');
            console.log('window.transactionid' + window.transactionid);

            $('#myModal').modal('show');
        });
    </script>
    <script>
        $(function () {
            $('#confirmMileStonebtn').on('click', function () {
                window.milestoneId = $(this).data('milsetone');
                $('.confirm-alert').modal('show')
            })
            //confirmMileStone
        });
    </script>
    <script>
        {{--$('#btnSubmit').click(function(){--}}
        {{--var formMessage = $('#send-message-form').serialize();--}}
        {{--var user_id = $('#user_id').val();--}}
        {{--var url = "/sendPrivateMessage/"+user_id;--}}
        {{--var token = '{!! csrf_token()  !!}';--}}

        {{--$.ajax({--}}
        {{--type:'post',--}}
        {{--url:url,--}}
        {{--data:formMessage,--}}
        {{--headers:{--}}
        {{--Authorization: token--}}
        {{--},--}}

        {{--success:function(data){--}}
        {{--console.log(data);--}}
        {{--toastr.success('Message Sent');--}}
        {{--$('#sendPrivateMessage').modal('hide');--}}
        {{--// setInterval(function(){window.location.reload()},200);--}}
        {{--}, error: function(error){--}}
        {{--console.log(error);--}}
        {{--}--}}
        {{--});--}}
        {{--});--}}
    </script>
    <script src="{{asset('custom/js/frontend/sendMessages.js')}}"></script>
    <script src="/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"
            type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"
            type="text/javascript"></script>
    <script src="/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
    <script src="{{asset('js/global.js')}}" type="text/javascript"></script>
    <script>
        window.total = '{{isset($total) ? $total : 0}}'
        window.applicantId = '{{$applicantId}}';
    </script>
    <script src="{{asset('custom/js/frontend/orderDetail.js')}}" type="text/javascript"></script>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').hover(function (){
                $(this).next().addClass("animated shake");
            });
            $('#milestones_amount').blur(function () {
                let val = $(this).val();
                console.log(val);
                console.log(parseFloat(val).toFixed(2));
                $('#milestones_amount').val(parseFloat(val).toFixed(2))
            })
        })
        setTimeout(function () {
            $('#ttr').html('${{isset($totalReleasedAmount) ? $totalReleasedAmount : 0}}');
            $('#md').html('${{isset($md) ? $md : 0}}');
            $('#msd').html('${{isset($msd) ? $msd : 0}}');
        }, 100)
    </script>

    <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"
            type="text/javascript"></script>
    <script>
        $('.scroll-div').slimScroll({});
    </script>
@endsection
