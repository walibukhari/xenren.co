<!-- Modal -->
<div id="giveBobusModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Give Bonus</h4>
            </div>
            <div class="modal-body">
                <form action="/action_page.php">
                    <div class="col-md-12">
                        <div class="col-md-6" style="padding-left: 0px">
                            <div class="form-group">
                                <label for="amount">Amount</label>
                                <input type="number" class="form-control" id="amount" name="amount">
                            </div>
                        </div>
                        <div class="col-md-6" style="padding-right: 0px">
                            <div class="form-group">
                                <label for="reason">Reason</label>
                                <input type="text" class="form-control" id="reasonForBonus" name="reason">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="description">Description</label><br/>
                            <small>This description will be sent to freelancer along with your payment</small>
                            <input type="text" class="form-control" id="description" name="description">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="private_note">Private Note (optional)</label><br/>
                            <small>The note will be available in transaction details for personal references, this freelancer will not see thi note.</small>
                            <input type="text" class="form-control" id="private_note" name="private_note">
                        </div>
                    </div>

                    <button type="button" class="btn btn-success btn-green-bg-white-text setBNTNNTN giveBonus" style="float: right">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@push('js')
<script>
    $(function () {
        $('.giveBonus').on('click', function(){
            const formData = new FormData();
            formData.append('amount', $('#amount').val());
            formData.append('reason', $('#reasonForBonus').val());
            formData.append('description', $('#description').val());
            formData.append('private_note', $('#private_note').val());
            formData.append('applicant_id', '{{$applicant_id}}');
            formData.append('_toke', '{{csrf_token()}}');

            var settings = {
                "url": "/giveBonus/{{$project_id}}",
                "method": "POST",
                "processData": false,
                "contentType": false,
                "mimeType": "multipart/form-data",
                "data": formData
            };

            $.ajax(settings).done(function (response) {
                console.log(response);
                response = JSON.parse(response);
                if(response.status == 'success') {
                    alertSuccess('Bonus Sent...!');
                    $('#giveBobusModal').modal('hide')
                } else {
                    alertError(response.message);
                }
            });
        })
    })
</script>