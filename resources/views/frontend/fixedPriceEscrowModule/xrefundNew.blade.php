@extends('frontend.layouts.default')

@section('title')
    Refund New
@endsection

@section('description')
    Refund New
@endsection

@section('author')
Xenren
@endsection

@section('header')
    <link href="/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/userCenter.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/fixedPriceEscrowModule.css" rel="stylesheet">

    <style type="text/css">
    	html:lang(cn) body, html:lang(cn) h1, html:lang(cn) h2, html:lang(cn) h3, html:lang(cn) h4, html:lang(cn) h5, html:lang(cn) h6, html:lang(cn) p, small, span html:lang(cn) .sub-heading, html:lang(cn) .btn, html:lang(cn) .label{
    		font-family: "Helvetica Neue",Helvetica,Arial,sans-serif !important;
    	}
    	.release-milestone{
    		border-top: 0px;
    		margin-top: 60px;
    	}
    </style>
@endsection

@section('content')
<div class="page-content-wrapper">
     <div class="col-md-12 usercenter link-div">
	    <a href="#" class="text-uppercase raleway">
	        <span class="fa fa-angle-left">
	        </span>
	        back to my orders
	    </a>
	</div>
	<div class="clearfix"></div>
	<br>

	<div class="col-md-12">
	    <div class="row main-div">
	        <div class="col-md-12 personal-profile">
	        	<div class="row">
	        		<div class="col-md-8">
	        			<div class="row user-info">
			        		<div class="col-md-3 text-center">
			                    <div class="profile-userpic">
			                        <img alt="" class="img-responsive" src="/images/people-avatar.png">
			                         <label class="btn-bs-file btn btn-xs">
						                <i class="fa fa-camera"></i>
						                <input type="file" />
						            </label>
			                    </div>
			                </div>
			        		<div class="col-md-9 basic-info">
				                <div class="row">
				                    <div class="col-md-12 user-full-name">
				                        <span class="full-name">Adam John</span>
				                        <img src="/images/icons/check.png">
				                    </div>
				                </div>
				                <br>
				                <div class="row">
				                    <div class="col-md-3">
				                        <small>Status:</small>
				                    </div>
				                    <div class="col-md-9">
				                        <span class="status">On Your Project</span>
				                        <button class="btn btn-success btn-green-bg-white-text" type="button">Chat</button>
				                    </div>
				                </div>
				                <br>
				                <div class="row">
				                    <div class="col-md-3">
				                        <small>Age:</small>
				                    </div>
				                    <div class="col-md-9">
				                        <span class="details">36</span>
				                    </div>
				                </div>                        
			                </div>
	        			</div>
	        			<div class="row">
	        				<div class="col-md-12 contacts-info">
    							<div class="row">
                                    <div class="col-md-3 col-md-3 contacts-info-sub">
                                        <img src="/images/skype-icon-5.png"> <span>Skype</span>
                                        <p style="display: block;">adamjohn982</p>
                                    </div>

                                    <div class="col-md-3 col-md-3 contacts-info-sub">
                                        <img src="/images/wechat-logo.png"> <span>Wechat</span>
                                        <p style="display: block;">adamjohnhs</p>
                                    </div>

                                    <div class="col-md-3 col-md-3 contacts-info-sub">
    									<img src="/images/Tencent_QQ.png"> <span>QQ</span> 
    									<p style="display: block;">1851215421</p> 
    								</div>


    								<div class="col-md-3 col-md-3 contacts-info-sub">
    									<img src="/images/MetroUI_Phone.png"> <span>Phone</span> 
    									<p style="display: block;">+00 1234 567 89</p> 
    								</div>
    							</div>
							</div>
	        			</div>
	        		</div>
	                <div class="col-md-4">
	                	<div class="row basic-info">
			                <div class="col-md-12 basic-info-title text-left">
			                	<img alt="" src="/images/hands.png"> &nbsp;<span class="subtitle"> Skill</span>
			                </div>
			                <div class="col-md-12">
		                		<div class="skill-div">
		                            <span class="item-skill admin-verified">
		                        		<img width="24px" height="24px" alt="" src="/images/logo_2.png">
		                        		<span>HTML</span>
		                    		</span>
		                        </div>
	                            <div class="skill-div">
	                                <span class="item-skill client-verified">
	                    				<img width="24px" height="24px" alt="" src="/images/checked.png">
	                    				<span>javascript</span>
	                				</span>
	                            </div>
	                            <div class="skill-div">
	                                <span class="item-skill unverified">
	                    				<span>HTML</span>
	                				</span>
	                            </div>
	                            <div class="skill-div">
	                                <span class="item-skill unverified">
	                    				<span>Adobe Photoshop</span>
	                				</span>
	                            </div>
	                            <div class="skill-div">
	                                <span class="item-skill unverified">
	                    				<span>Adobe ilusstrator</span>
	                				</span>
	                            </div>
				                                
				                <div class="clearfix"></div>
			                </div>

			                <div class="col-md-12 basic-info-title text-left">
			                	<br><img alt="" src="/images/earth.png"> &nbsp;<span class="subtitle"> Language</span>
			                </div>
			                
			                <div class="col-md-12 media similar-profiles text-left">
	                            <div class="media-left media-middle item-languages">
		                             <img class="pull-left" alt="..." src="http://xenrencc.dev/images/Flags-Icon-Set/24x24/HK.png" class="media-object">
		                            <span class="pull-left">English</span>
		                        </div>
	                            <div class="media-left media-middle item-languages">
		                            <img class="pull-left" alt="..." src="http://xenrencc.dev/images/Flags-Icon-Set/24x24/MY.png" class="media-object">
		                            <span class="pull-left">Malay</span>
		                        </div>
		                  	</div>
			            </div>
	                </div>
	        	</div>
	        </div>

			<div class="col-md-12 prising-section text-center">
				<div class="row">
					<div class="col-md-4 prising-section-sub">
						<img src="/images/icons/doller-icon.png"><br>
						<p>Budget for the Project</p>
						<span>$1500 USD</span>
					</div>
					<div class="col-md-4 prising-section-sub">
						<img src="/images/icons/time-icon.png"><br>
						<p>Current Milestone</p>
						<span>$700USD</span>
					</div>
					<div class="col-md-4 prising-section-sub">
						<img src="/images/icons/doller-icon.png"><br>
						<p>Fixed Price</p>
					</div>
				</div>
			</div>

			<div class="col-md-12 description">
				<h4>ROCK and ROLL - UI Designer</h4>
				<h5>DESCRIPTION</h5>
				<p>
					We are looking to overhaul the design of our extension and assoclated pages on our site. Attached here is the design brief. Please go through this and respond with relevant work from your portfollio.  **This is important** Please write in your own words what the extension does and how you think the design should help highlight this.
					<br><br>
					The end-result design should be a fully specified PSD file with all the found and Image include. Not a flat PNG mocup.
				</p>
			</div>

			<div class="col-md-12 milestone-desc">
				<ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="text-center"><a href="#milestone" aria-controls="milestone" role="tab" data-toggle="tab">Milestone</a></li>
                    <li role="presentation" class="active text-center"><a href="#dispute" aria-controls="dispute" role="tab" data-toggle="tab">Refund</a></li>
                    <li role="presentation" class="text-center"><a href="#contract-control-panel" aria-controls="contract-control-panel" role="tab" data-toggle="tab">Contract Control Panel</a></li>
                    <li role="presentation" class="text-center"><a href="#end-contract" aria-controls="end-contract" role="tab" data-toggle="tab">End Contract</a></li>
                </ul>

                <div class="tab-content tabpanel-custom">
                    <div role="tabpanel" class="tab-pane" id="milestone">
                    	<div class="row new-milestone-add">
                    		<div class="col-md-12">
                    			<label class="milestone-subpart-title">Request Milestone</label>
                    			<form accept="">
			                    	<div class="form-group row">
									  	<div class="col-xs-2">
									    	<label for="ex1">Enter Amount</label>
									    	<input class="form-control" id="ex1" type="text">
									  	</div>
									  	<div class="col-xs-2">
									    	<label for="ex2">Milestone Name</label>
									    	<input class="form-control" id="ex2" type="text">
									  	</div>
									  	<div class="col-xs-3">
									    	<label for="ex2">Select Due Date</label>
									    	<div class="input-group date form_datetime">
                                                <input type="text" class="form-control" readonly="" size="16">
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn default date-set">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>
									  	</div>
									  	<div class="col-xs-5">
									  	<br>
									  		<button class="btn btn-success btn-green-bg-white-text" type="submit">Request</button> &nbsp;
									  		<button class="btn btn-success btn-white-bg-green-text" type="button">Cancel</button>
									  	</div>
									</div> 
		                    	</form>	
                    		</div>
                    	</div>
                    	<div class="row release-milestone">
                    		<div class="col-md-12">
                    			<label class="milestone-subpart-title">Milestone that already earned <span class="green"> $700</span></label>

                    			<table class="table custom-table">
                    				<tr>
                    					<td width="30px"></td>
                    					<td>
                    						<span class="title">Amount</span><br>
                    						<span class="desc">700$</span>
                    					</td>
                    					<td>
                    						<span class="title">Milestone Name</span><br>
                    						<span class="desc">Complete E-Commerce </span>
                    					</td>
                    					<td>
                    						<span class="title">Due Date</span><br>
                    						<span class="desc">23/08/17 | 53:24:12</span>
                    					</td>
                    					<td width="300px">
                    						<span class="title">Description</span><br>
                    						<span class="desc">This milestone is release for 3 app research & write document.</span>
                    					</td>
                    					<td>
                    						<button class="btn btn-success btn-green-bg-white-text" type="button">Refund</button>
                    					</td>
                    				</tr>
                    			</table>
                    		</div>
                    	</div>
                    </div>
                    <div role="tabpanel" class="tab-pane active" id="dispute">
                    	<div class="row client-request">
                    		<div class="col-md-12">
                    			<label class="milestone-subpart-title">Client Request Dispute</label>
                    			<div class="row">
                    				<div class="col-md-2">
                    					<span class="title">Amount</span>
                    					<p class="desc">15 USD</p>
                    				</div>
                    				<div class="col-md-2">
                    					<span class="title">Milestone Name</span>
                    					<div class="desc plus-tooltop">
                    						Login
            								<i class="fa fa-plus"></i>
                							<div class="tooltop">
                								<label>Description</label>
                								<span>
                									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                								</span>
                							</div>
                    					</div>
                    				</div>
                    				<div class="col-md-4">
                    					<span class="title">Reason</span>
                    					<p class="desc">Mistakenly Paid</p>
                    				</div>
                    				<div class="col-md-4">
                    					<div class="row">
                    						<div class="col-md-9 col-md-offset-3">
                    							<span class="title">Auto Release Counter</span>
                    							<p class="desc">56:34:23</p>		
                    						</div>
                    						<div class="col-md-12">
                    							<button class="btn btn-success btn-green-bg-white-text" type="submit">Agree</button>
									  			<button class="btn btn-success btn-white-bg-green-text" type="button">Reject</button>
                    						</div>
                    					</div>
                    				</div>
                    			</div>
                    		</div>
                    	</div>

                    	<div class="row released-milestone">
                    		<div class="col-md-8">
                    			<label class="milestone-subpart-title">Milestone that already released</label>

                    			<table class="table custom-table">
                    				<tr>
                    					<td width="30px"></td>
                    					<td>
                    						<span class="title">Released Amount</span><br>
                    						<span class="desc">700$</span>
                    					</td>
                    					<td>
                    						<span class="title">Milestone Name</span><br>
                    						<span class="desc">Complete E-Commerce </span>
                    					</td>
                    					<td>
                    						<span class="title">Due Date</span><br>
                    						<span class="desc">23/08/17</span>
                    					</td>
                    					<td>
                    						<div class="md-radio">
                                                <input type="radio" class="md-radiobtn" name="radio1" id="radio1">
                                                <label for="radio1">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                </label>
                                            </div>
                    					</td>
                    				</tr>

                    				<tr>
                    					<td width="30px"></td>
                    					<td>
                    						<span class="title">Released Amount</span><br>
                    						<span class="desc">700$</span>
                    					</td>
                    					<td>
                    						<span class="title">Milestone Name</span><br>
                    						<span class="desc">Complete E-Commerce </span>
                    					</td>
                    					<td>
                    						<span class="title">Due Date</span><br>
                    						<span class="desc">23/08/17</span>
                    					</td>
                    					<td>
                    						<div class="md-radio">
                                                <input type="radio" class="md-radiobtn" name="radio2" id="radio2" checked="checked">
                                                <label for="radio2">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                </label>
                                            </div>
                    					</td>
                    				</tr>
                    			</table>
                    		</div> 
                    	</div>

                    	<div class="row total-refund">
                    		<div class="col-md-12">
                    			<label class="milestone-subpart-title">Total amount able to Refund <span class="green">700 USD</span></label>
                    			<form accept="">
			                    	<div class="form-group row">
									  	<div class="col-xs-12">
									    	<label for="ex1">Amount you wish to Refund</label>
									    </div>
									  	<div class="col-xs-2">
									    	<input class="form-control" id="ex1" type="text">
									  	</div>
									</div>
									  	
			                    	<div class="form-group row">
									  	<div class="col-xs-12">
									    	<label for="ex2">Reason</label>
									  	</div>
									  	<div class="col-xs-4">
									    	<textarea class="form-control" id="ex2"></textarea>
									  	</div>
									</div>  	
			                    	<div class="form-group row">
									  	<div class="col-xs-4">
									  	<br>
									  		<button class="btn btn-success btn-green-bg-white-text" type="submit">Refund</button>
									  		<button class="btn btn-success btn-white-bg-green-text" type="button">Cancel</button>
									  	</div>
									</div> 
		                    	</form>	
                    		</div>
                    	</div>			
                    </div>
                    <div role="tabpanel" class="tab-pane" id="contract-control-panel">
                    	<div class="row end-contract">
                    		<div class="col-md-8">
                    			<label class="milestone-subpart-title">Contract Control Panel</label>
		                    	<div class="form-group row">
                    				<br>
								  	<div class="col-xs-2">
                                		<input type="checkbox" checked class="make-switch" data-size="small">
								  	</div>
								  	<div class="col-xs-10">
								    	<label for="ex1">Daily Update</label>
								    	<p class="desc-short">
								    		Employer require daily update, if freelancer didn't daily update on xenren.co platform within 48 hours, employer will able to end contract and without giving any payment. same as due date.
								    	</p>
								  	</div>
								</div>  
                    		</div>
                    	</div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="end-contract">
                    	<div class="row end-contract">
                    		<div class="col-md-6">
                    			<label class="milestone-subpart-title">End Contract</label>
                    			<p class="desc-short">After click confirm. will jump to Leave Feedback. Must complete feedback then able to end contract.</p>
                    			<form accept="">
			                    	<div class="form-group row">
									  	<div class="col-xs-12"><br>
									  		<button class="btn btn-success btn-green-bg-white-text" type="submit">Confirm</button> &nbsp;
									  		<button class="btn btn-success btn-white-bg-green-text" type="button">Cancel</button>
									  	</div>
									</div> 
		                    	</form>	
                    		</div>
                    	</div>
                    </div>
                </div>
			</div>
	    </div>
	</div>
</div>
@endsection
 
@section('footer')
@endsection
