@extends('frontend.layouts.default')

@section('title')
    Refund Edit
@endsection

@section('description')
    Refund Edit
@endsection

@section('author')
Xenren
@endsection

@section('header')
    <link href="/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/userCenter.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/fixedPriceEscrowModule.css" rel="stylesheet">
@endsection

@section('content')
<div class="page-content-wrapper">
     <div class="col-md-12 usercenter link-div">
	    <a href="#" class="text-uppercase raleway">
	        <span class="fa fa-angle-left">
	        </span>
	        back to my orders
	    </a>
	</div>
	<div class="clearfix"></div>
	<br>

	<div class="col-md-12">
	    <div class="row main-div">
	        <div class="col-md-12 personal-profile">
	        	<div class="row">
	        		<div class="col-md-8">
	        			<div class="row user-info">
			        		<div class="col-md-3 text-center">
			                    <div class="profile-userpic">
			                        <img alt="" class="img-responsive" src="/images/people-avatar.png">
			                         <label class="btn-bs-file btn btn-xs">
						                <i class="fa fa-camera"></i>
						                <input type="file" />
						            </label>
			                    </div>
			                </div>
			        		<div class="col-md-9 basic-info">
				                <div class="row">
				                    <div class="col-md-12 user-full-name">
				                        <span class="full-name">Adam John</span>
				                        <img src="/images/icons/check.png">
				                    </div>
				                </div>
				                <br>
				                <div class="row">
				                    <div class="col-md-3">
				                        <small>Status:</small>
				                    </div>
				                    <div class="col-md-9">
				                        <span class="status">On Your Project</span>
				                        <button class="btn btn-success btn-green-bg-white-text" type="button">Chat</button>
				                    </div>
				                </div>
				                <br>
				                <div class="row">
				                    <div class="col-md-3">
				                        <small>Age:</small>
				                    </div>
				                    <div class="col-md-9">
				                        <span class="details">36</span>
				                    </div>
				                </div>                        
			                </div>
	        			</div>
	        			<div class="row">
	        				<div class="col-md-12 contacts-info">
    							<div class="row">

                                    <div class="col-md-3 col-md-3 contacts-info-sub">
                                        <img src="/images/skype-icon-5.png"> <span>Skype</span>
                                        <p style="display: block;">adamjohn982</p>
                                    </div>

                                    <div class="col-md-3 col-md-3 contacts-info-sub">
                                        <img src="/images/wechat-logo.png"> <span>Wechat</span>
                                        <p style="display: block;">adamjohnhs</p>
                                    </div>

                                    <div class="col-md-3 col-md-3 contacts-info-sub">
    									<img src="/images/Tencent_QQ.png"> <span>QQ</span> 
    									<p style="display: block;">1851215421</p> 
    								</div>


    								<div class="col-md-3 col-md-3 contacts-info-sub">
    									<img src="/images/MetroUI_Phone.png"> <span>Phone</span> 
    									<p style="display: block;">+00 1234 567 89</p> 
    								</div>
    							</div>
							</div>
	        			</div>
	        		</div>
	                <div class="col-md-4">
	                	<div class="row basic-info">
			                <div class="col-md-12 basic-info-title text-left">
			                	<img alt="" src="/images/hands.png"> &nbsp;<span class="subtitle"> Skill</span>
			                </div>
			                <div class="col-md-12">
		                		<div class="skill-div">
		                            <span class="item-skill admin-verified">
		                        		<img width="24px" height="24px" alt="" src="/images/logo_2.png">
		                        		<span>HTML</span>
		                    		</span>
		                        </div>
	                            <div class="skill-div">
	                                <span class="item-skill client-verified">
	                    				<img width="24px" height="24px" alt="" src="/images/checked.png">
	                    				<span>javascript</span>
	                				</span>
	                            </div>
	                            <div class="skill-div">
	                                <span class="item-skill unverified">
	                    				<span>HTML</span>
	                				</span>
	                            </div>
	                            <div class="skill-div">
	                                <span class="item-skill unverified">
	                    				<span>Adobe Photoshop</span>
	                				</span>
	                            </div>
	                            <div class="skill-div">
	                                <span class="item-skill unverified">
	                    				<span>Adobe ilusstrator</span>
	                				</span>
	                            </div>
				                                
				                <div class="clearfix"></div>
			                </div>

			                <div class="col-md-12 basic-info-title text-left">
			                	<br><img alt="" src="/images/earth.png"> &nbsp;<span class="subtitle"> Language</span>
			                </div>
			                
			                <div class="col-md-12 media similar-profiles text-left">
	                            <div class="media-left media-middle item-languages">
		                             <img class="pull-left" alt="..." src="http://xenrencc.dev/images/Flags-Icon-Set/24x24/HK.png" class="media-object">
		                            <span class="pull-left">English</span>
		                        </div>
	                            <div class="media-left media-middle item-languages">
		                            <img class="pull-left" alt="..." src="http://xenrencc.dev/images/Flags-Icon-Set/24x24/MY.png" class="media-object">
		                            <span class="pull-left">Malay</span>
		                        </div>
		                  	</div>
			            </div>
	                </div>
	        	</div>
	        </div>

			<div class="col-md-12 prising-section text-center">
				<div class="row">
					<div class="col-md-4 prising-section-sub">
						<img src="/images/icons/doller-icon.png"><br>
						<p>Budget for the Project</p>
						<span>$1500 USD</span>
					</div>
					<div class="col-md-4 prising-section-sub">
						<img src="/images/icons/time-icon.png"><br>
						<p>Current Milestone</p>
						<span>$700USD</span>
					</div>
					<div class="col-md-4 prising-section-sub">
						<img src="/images/icons/doller-icon.png"><br>
						<p>Fixed Price</p>
					</div>
				</div>
			</div>

			<div class="col-md-12 description">
				<h4>ROCK and ROLL - UI Designer</h4>
				<h5>DESCRIPTION</h5>
				<p>
					We are looking to overhaul the design of our extension and assoclated pages on our site. Attached here is the design brief. Please go through this and respond with relevant work from your portfollio.  **This is important** Please write in your own words what the extension does and how you think the design should help highlight this.
					<br><br>
					The end-result design should be a fully specified PSD file with all the found and Image include. Not a flat PNG mocup.
				</p>
			</div>

			<div class="col-md-12 milestone-desc">
				<ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="text-center"><a href="#milestone" aria-controls="milestone" role="tab" data-toggle="tab">Milestone</a></li>
                    <li role="presentation" class="active text-center"><a href="#dispute" aria-controls="dispute" role="tab" data-toggle="tab">Refund</a></li>
                    <li role="presentation" class="text-center"><a href="#contract-control-panel" aria-controls="contract-control-panel" role="tab" data-toggle="tab">Contract Control Panel</a></li>
                    <li role="presentation" class="text-center"><a href="#end-contract" aria-controls="end-contract" role="tab" data-toggle="tab">End Contract</a></li>
                </ul>

                <div class="tab-content tabpanel-custom">
                    <div role="tabpanel" class="tab-pane" id="milestone">
                    </div>
                    <div role="tabpanel" class="tab-pane active" id="dispute">
                    	<div class="row client-request">
                    		<div class="col-md-12">
                    			<label class="milestone-subpart-title">Client Request Dispute</label>
                    			<div class="row">
                    				<div class="col-md-2">
								    	<label for="ex1">Amount</label>
								    	<input class="form-control" id="ex1" type="text" value="15 USD">
                    				</div>
                    				<div class="col-md-2">
                    					<label for="ex2">Milestone Name</label>
                    					<div class="input-group">
                                            <input class="form-control input-addon" id="ex2" type="text" value="Login">
                                            <span class="input-group-addon">
                                                <i class="fa fa-plus"></i>
                                            </span>
                                        </div>
                    				</div>
                    				<div class="col-md-5">
                    					<label for="ex3">Reason</label>
								    	<input class="form-control" id="ex3" type="text" value="Mistakenly Paid">
                    				</div>
                    				<div class="col-md-3">
                    					<label for="ex4">Auto Release Counter</label>
								    	<input class="form-control" width="90px" id="ex4" type="text" value="56:34:23">
                    				</div>
                    			</div><br>
                    			<div class="row">
            						<div class="col-md-12">
            							<button class="btn btn-success btn-green-bg-white-text" type="submit">Agree</button>
							  			<button class="btn btn-success btn-white-bg-green-text" type="button">Reject</button>
            						</div>
                    			</div>
                    		</div>
                    	</div>

                    	<div class="row released-milestone">
                    		<div class="col-md-8">
                    			<label class="milestone-subpart-title">Milestone that already released</label>

                    			<table class="table custom-table">
                    				<tr>
                    					<td width="30px"></td>
                    					<td>
                    						<span class="title">Released Amount</span><br>
                    						<span class="desc">700$</span>
                    					</td>
                    					<td>
                    						<span class="title">Milestone Name</span><br>
                    						<span class="desc">Complete E-Commerce </span>
                    					</td>
                    					<td>
                    						<span class="title">Due Date</span><br>
                    						<span class="desc">23/08/17</span>
                    					</td>
                    					<td>
                    						<div class="md-radio">
                                                <input type="radio" class="md-radiobtn" name="radio1" id="radio1">
                                                <label for="radio1">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                </label>
                                            </div>
                    					</td>
                    				</tr>

                    				<tr>
                    					<td width="30px"></td>
                    					<td>
                    						<span class="title">Released Amount</span><br>
                    						<span class="desc">700$</span>
                    					</td>
                    					<td>
                    						<span class="title">Milestone Name</span><br>
                    						<span class="desc">Complete E-Commerce </span>
                    					</td>
                    					<td>
                    						<span class="title">Due Date</span><br>
                    						<span class="desc">23/08/17</span>
                    					</td>
                    					<td>
                    						<div class="md-radio">
                                                <input type="radio" class="md-radiobtn" name="radio2" id="radio2" checked="checked">
                                                <label for="radio2">
                                                    <span class="inc"></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                </label>
                                            </div>
                    					</td>
                    				</tr>
                    			</table>
                    		</div> 
                    	</div>

                    	<div class="row total-refund">
                    		<div class="col-md-12">
                    			<label class="milestone-subpart-title">Total amount able to Refund <span class="green">700 USD</span></label>
                    			<form accept="">
			                    	<div class="form-group row">
									  	<div class="col-xs-12">
									    	<label for="ex1">Amount you wish to Refund</label>
									    </div>
									  	<div class="col-xs-2">
									    	<input class="form-control" id="ex1" type="text">
									  	</div>
									</div>
									  	
			                    	<div class="form-group row">
									  	<div class="col-xs-12">
									    	<label for="ex2">Reason</label>
									  	</div>
									  	<div class="col-xs-4">
									    	<textarea class="form-control" id="ex2"></textarea>
									  	</div>
									</div>  	
			                    	<div class="form-group row">
									  	<div class="col-xs-4">
									  	<br>
									  		<button class="btn btn-success btn-green-bg-white-text" type="submit">Refund</button>
									  		<button class="btn btn-success btn-white-bg-green-text" type="button">Cancel</button>
									  	</div>
									</div> 
		                    	</form>	
                    		</div>
                    	</div>			
                    </div>
                    <div role="tabpanel" class="tab-pane" id="contract-control-panel">
                    	Contract Control Panel
                    </div>
                    <div role="tabpanel" class="tab-pane" id="end-contract">
                    	End Contract
                    </div>
                </div>
			</div>
	    </div>
	</div>
</div>
@endsection
 
@section('footer')
@endsection
