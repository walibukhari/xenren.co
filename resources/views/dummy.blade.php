<?php session_start(); ?>
<?php require("lib/simple-botdetect.php"); ?>
<html>
<head>
    <title>BotDetect PHP CAPTCHA Validation: PHP Login Form CAPTCHA Code Example</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<form method="post" action="process-login.php" class="column" id="form1">

	<?php // authentication failed, show error message
        $error = '';
        if (isset($_REQUEST['error'])) {
            $error = $_REQUEST['error'];
        }
        if ('Format' == $error) { ?>
            <p class="incorrect">Invalid authentication info</p><?php
        } else if ('Auth' == $error) { ?>
            <p class="incorrect">Authentication failed</p><?php
        }
	?>

    <div class="input">
			<?php // Adding BotDetect Captcha to the page
			$LoginCaptcha = new SimpleCaptcha('LoginCaptcha');

			// only show the Captcha if it hasn't been already solved for the current message
			if(!isset($_SESSION['IsCaptchaSolved'])) { ?>
                  <label for="CaptchaCode">Retype the characters from the picture:</label>
			      <?php echo $LoginCaptcha->Html(); ?>
                  <input type="text" name="CaptchaCode" id="CaptchaCode" class="textbox" /><?php

			// CAPTCHA validation failed, show error message
                if ('Captcha' == $error) { ?>
                      <span class="incorrect">Incorrect code</span><?php
                }
			}
			?>
    </div>


</form>

</body>
</html>