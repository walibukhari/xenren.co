<html>
<head>
    <title>Google Map</title>
    <meta
        name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1"
    />
    <style>
        html, body {
            margin: 0px !important;
            padding: 0px !important;
        }

        #maps {
            height: 100%;
            width: 100%;
            margin: 0px;
        }

        .gmnoprint {
            display: none !important;
        }

        .gm-ui-hover-effect {
            opacity: .6;
            display: none !important;
        }

        .lds-spinner {
            position: absolute;
            width: 100%;
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            right: 30px;
        }

        .lds-spinner div {
            transform-origin: 40px 40px;
            animation: lds-spinner 1.2s linear infinite;
        }

        .lds-spinner div:after {
            content: " ";
            display: block;
            position: absolute;
            top: 3px;
            left: 37px;
            width: 6px;
            height: 18px;
            border-radius: 20%;
            background: #555;
        }

        .lds-spinner div:nth-child(1) {
            transform: rotate(0deg);
            animation-delay: -1.1s;
        }

        .lds-spinner div:nth-child(2) {
            transform: rotate(30deg);
            animation-delay: -1s;
        }

        .lds-spinner div:nth-child(3) {
            transform: rotate(60deg);
            animation-delay: -0.9s;
        }

        .lds-spinner div:nth-child(4) {
            transform: rotate(90deg);
            animation-delay: -0.8s;
        }

        .lds-spinner div:nth-child(5) {
            transform: rotate(120deg);
            animation-delay: -0.7s;
        }

        .lds-spinner div:nth-child(6) {
            transform: rotate(150deg);
            animation-delay: -0.6s;
        }

        .lds-spinner div:nth-child(7) {
            transform: rotate(180deg);
            animation-delay: -0.5s;
        }

        .lds-spinner div:nth-child(8) {
            transform: rotate(210deg);
            animation-delay: -0.4s;
        }

        .lds-spinner div:nth-child(9) {
            transform: rotate(240deg);
            animation-delay: -0.3s;
        }

        .lds-spinner div:nth-child(10) {
            transform: rotate(270deg);
            animation-delay: -0.2s;
        }

        .lds-spinner div:nth-child(11) {
            transform: rotate(300deg);
            animation-delay: -0.1s;
        }

        .lds-spinner div:nth-child(12) {
            transform: rotate(330deg);
            animation-delay: 0s;
        }

        @keyframes lds-spinner {
            0% {
                opacity: 1;
            }
            100% {
                opacity: 0;
            }
        }
    </style>
</head>
<body style="margin:0px !important; padding: 0px !important;">
<div id="maps">
</div>
<!-- IOS -->
{{--<form action="com.xenren://app.com/?screen=xxxxx" target="_blank">--}}
{{--    <input type="submit" value="Open App" />--}}
{{--</form>--}}
<!-- Android -->
{{--<form action="https://play.google.com/store/apps/details?id=com.xenren://app.com/?screen=xxxxx" target="_blank">--}}
{{--    <input type="submit" value="Open App" />--}}
{{--</form>--}}
<script>
    // window.onload = function() {
    <!-- Deep link URL for existing users with app already installed on their device -->
    // window.location = 'yourapp://app.com/?screen=xxxxx';
    <!-- Download URL (TUNE link) for new users to download the app -->
    // setTimeout("window.location = 'http://hastrk.com/serve?action=click&publisher_id=1&site_id=2';", 1000);
    // }
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="
https://maps.google.com/maps/api/js?key=AIzaSyCIasatwSA8nyOeN8Cobzil6yO1jsT13rE&callback=initMap&libraries=geometr‌​y,places,controls"
        async defer></script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"
        async defer></script>
<script src=
        "https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.9.1/underscore-min.js">
</script>
<script>
    var token = '{{ session('api_token') }}';
    map = null;

    function getShareddOfficeById(officeId) {
        var officeName = sessionStorage.getItem('office_name').replace(' ', '-').replace(' - ', '-');
        var Country = sessionStorage.getItem('country').replace(' ', '-');
        var City = sessionStorage.getItem('city').replace(' ', '-');
        console.log(officeId);
        console.log(officeName);
        console.log(Country);
        console.log(City);
        const locale = '{{\Config::get('app.locale')}}';
        if (locale === 'cn') {
            const office_name = officeName.replace(/\s+/g, '');
            window.location.href = `/cn/coworking-spaces/${Country}/${City}/${office_name}/${officeId}`;
        } else if (locale === 'en') {
            const office_name = officeName.replace(/\s+/g, '');
            window.location.href = `/en/coworking-spaces/${Country}/${City}/${office_name}/${officeId}`;
        }
    }

    var getGoogleClusterInlineSvg = function (color) {
        var encoded = window.btoa('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="-100 -100 200 200"><defs><g id="a" transform="rotate(45)"></g></defs><g fill="' + color + '"><circle r="80"/><use xlink:href="#a"/><g transform="rotate(120)"><use xlink:href="#a"/></g><g transform="rotate(240)"><use xlink:href="#a"/></g></g></svg>');

        return ('data:image/svg+xml;base64,' + encoded);
    };
    var cluster_styles = [
        {
            width: 40,
            height: 40,
            url: getGoogleClusterInlineSvg('#5ec329'),
            textColor: 'white',
            textSize: 12,
        },
        {
            width: 40,
            height: 40,
            url: getGoogleClusterInlineSvg('#5ec329'),
            textColor: 'white',
            textSize: 12,
        },
        {
            width: 40,
            height: 40,
            url: getGoogleClusterInlineSvg('#5ec329'),
            textColor: 'white',
            textSize: 12,
        }
        //up to 5
    ];

    function setMarkerClick(marker, location, infoWindow) {
        infoWindow.setContent('hello how are you');
        infoWindow.open(map, marker);
    }

    function goForMapNow(lat, lng) {
        console.log(lat, lng);
        var map_options = {
            zoom: 9,
            center: {lat: lat, lng: lng},
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            draggable: true,
            minZoom: 4,
        };
        var map_canvas = document.getElementById('maps');
        var map = new google.maps.Map(map_canvas, map_options);

        let setToken = `Bearer ${token}`;
        let link = '/get-app-map-data';
        $.ajax({
            url: link,
            headers: {
                '_token': '{{csrf_token()}}'
            },
            success: function (response) {
                infoWindow = new google.maps.InfoWindow({
                    content: ''
                });
                let C_markers = [];
                var markers = response.map(function (data, i) {
                    console.log(data);
                    data.map(function (location) {
                        let marker = new google.maps.Marker({
                            position: new google.maps.LatLng(parseFloat(location.lat), parseFloat(location.lng)),
                            // label: location.office_name,
                            icon: '{{asset('images/markerImage.png')}}',
                            scale: 100 / 10
                        });

                        C_markers.push(marker);
                        let rating = location.avg;
                        // var review = Math.round(rating);
                        var rateRange = Math.round(rating);
                        var price = '';
                        if (location.min_seat_price) {
                            price = location.min_seat_price.month_price;
                        }
                        var monthPrice = price ? price : 0;
                        var totalStars = 5;
                        var range = totalStars - rateRange;
                        var starsHtml = '';
                        for ($i = 0; $i < rateRange; $i++) {
                            starsHtml += '<i class="fa fa-star set-fa-fa-active-color" id="set-fa-fa-color-1" aria-hidden="true"></i>\n'
                        }
                        for ($i = 0; $i < range; $i++) {
                            starsHtml += '<i class="fa fa-star" id="set-fa-fa-color-1" aria-hidden="true"></i>\n'
                        }
                        google.maps.event.addListener(marker, 'click', function (evt) {
                            var iwContent =
                                '<div style="display: flex;' +
                                'flex-direction: column;" onclick="getShareddOfficeById(' + location.id + ')">' +
                                '<img class="set-shred-officess-images" style="width:150px;height:100px" src="/' + location.image + '">'
                                + '<br>' + '' +
                                '<div class="row rowM"><div class="col-md-12 set-col-md-12-office-name">' +
                                '<span class="set-shared-office-p-price">' + location.office_name + '</span>' +
                                '</div>' +
                                '</div>' +
                                '<br>'
                                + '<div class="row rowM"><div class="col-md-12 set-col-md-12-price-row">' +
                                '<span class="set-dollar-sign-2-dollar-sign">' + "$" + '</span>' + '' +
                                '<span class="set-shared-office-p-price">' + monthPrice + '</span>' + '' +
                                '<span class="set-monthly-area">' + '/' + 'Monthly' + '</span>' +
                                '</div>' +
                                '</div>'
                                + '<div class="row rowM"><div class="col-md-12 set-col-md-12-popover-box">' +
                                '<div class="col-md-7 set-col-md-6-fa-fa-stars">' + starsHtml
                                + '</div>' +
                                '<div class="col-md-5 set-col-5-md-revirews">'
                                + '<span>' + rateRange + ' ' + 'Reviews'
                                + '</span>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>';

                            infoWindow.setContent(iwContent);
                            infoWindow.open(map, marker);
                            sessionStorage.setItem('office_name', location.office_name);
                            sessionStorage.setItem('country', location.country_name);
                            sessionStorage.setItem('city', location.city_name);
                        });
                        google.maps.event.addListener(map, 'click', function () {
                            infoWindow.close();
                        });
                        return marker;
                    });
                });
                var clusterOptions = {
                    styles: cluster_styles,
                    gridSize: 50,
                    maxZoom: 15,
                    minimumClusterSize: 1,
                    animate: true
                };
                var markerCluster = new MarkerClusterer(map, C_markers, clusterOptions);
                setTimeout(function () {
                    $('.gm-style-pbc ~ div > div > div:nth-child(3) div').each(function () {
                        console.log($(this));
                        $(this).prop('Counter', 0).animate({
                            Counter: $(this).text()
                        }, {
                            duration: 3000,
                            easing: 'swing',
                            step: function (now) {
                                $(this).text(Math.ceil(now));
                            }
                        });
                    });
                }, 80)
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function initMap() {

        // Try HTML5 geolocation.
        // if (navigator.geolocation) {
        //     navigator.geolocation.getCurrentPosition(function(position) {
        //         goForMapNow(position.coords.latitude, position.coords.longitude)
        //     }, function() {
        //         console.log('no location access');
        //         $.getJSON('https://ipapi.co/json/', function (result) {
        //             console.log(result);
        //             goForMapNow(parseFloat(result.latitude), parseFloat(result.longitude))
        //         });
        //     });
        // } else {
        console.log('no location access');
        $.getJSON('https://ipapi.co/json/', function (result) {
            console.log(result);
            goForMapNow(parseFloat(result.latitude), parseFloat(result.longitude))
        });
        // $.getJSON('http://api.wipmania.com/jsonp?callback=?', function (data) {
        //     goForMapNow(parseFloat(data.latitude), parseFloat(data.longitude))
        // });
        // }

    }
</script>
</body>
</html>
