@extends('frontend.layouts.default')

@section('title')
    User Center
@endsection

@section('description')
    User Center
@endsection

@section('author')
    Xenren
@endsection

@section('header')
    <link href="/assets/global/plugins/bootstrap-star-rating/css/star-rating.css" media="all" rel="stylesheet"
          type="text/css"/>
    <link href="/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css"/>
    <link href="/custom/css/frontend/userCenter.css" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE BASE CONTENT -->
        @include('frontend.userCenter.userDetail')
        @include('frontend.userCenter.adminReview')
        <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection

@section('footer')
    <script src="/assets/global/plugins/bootstrap-star-rating/js/star-rating.js" type="text/javascript"></script>
    <script src="/custom/js/frontend/userCenter.js" type="text/javascript"></script>
@endsection