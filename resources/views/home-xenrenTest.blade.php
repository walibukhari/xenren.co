@extends('frontend-xenren.layouts.default')

@section('title') {{ trans('home.title') }} @endsection

@section('description') {{ trans('home.description') }} @endsection

@section('author') Xenren @endsection

@section('header')
    <link href="/custom/css/frontend/home.css" rel="stylesheet" type="text/css" />
    <link href="/custom/css/frontend/home-new.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .hero-caption.caption-height-center {
            top: 26%;
        }
        .mrg-top-15 {
            margin-top: 15px !important;
        }
        .secure {
            border-bottom: none !important;
            padding-bottom: 50px !important;
        }

        hr.services {
            border-top: 2px solid #fff !important;
        }

        section.home-services {
            background-image: linear-gradient( rgba(46, 64, 78, 0.8), rgba(46, 64, 78, 0.8) ), url('../images/bg-services.jpg') !important;

            /*background-color: tomato !important;*/
            /*height: 670px!important;*/
        }
    </style>
@endsection

@section('content')
    <!-- Revolution slider -->
    <section id="slider">
        <div class="tp-banner-container ">
            
            <div class="tp-banner rev-interactive">
                <div class="hero-caption caption-center caption-height-center container" style="z-index: 100;">
                    <img src="{{$_G['xenren_path']}}images/logo/main.png" alt="Logo" class="banner-logo"/><br>
                    <a class="btn btn-style-3 btn-lg mrg-top-15 scroll-to explore centering pagination-centered" href="#about">{{ trans('home.explore_now') }}</a><br>
                    <span><i class="explore-span ti-angle-down"></i></span>
                </div>
                <!-- /hero-caption -->

                <div id="particles-js"></div>

            </div>
            <!-- /tp-banner -->
        </div>
        <!-- /tp-banner-container -->
    </section>
    <!-- /#Revolution slider -->

    {{--<!-- News -->
    <section id="news" class="section">
        <div class="container">
            <div class="row" style="padding:0px 10px 0px 20px;">
                @include('frontend.includes.news', ['newsList' => $newsList])
            </div>
        </div>
    </section>
    <!-- /News -->--}}

    {{--<!-- About -->--}}
    {{--<section id="about" class="section mrg-top-30">--}}
        {{--<div class="container">--}}
            {{--<div class="col-sm-12">--}}
                {{--<h2 class="heading">About Us</h2>--}}
                {{--<p class="about-content mrg-top-30 center-text bold-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>--}}
                {{--<br>--}}
                {{--<a class="btn btn-style-3 btn-lg mrg-top-50 centering pagination-centered">More</a><br>--}}
            {{--</div>--}}
            {{--<!-- /column -->--}}
        {{--</div>--}}
        {{--<!-- /container -->--}}
    {{--</section>--}}
    {{--<!-- /About -->--}}

    {{--<!-- Portfolio -->--}}
    {{--<section id="portfolio" class="section">--}}
        {{--<div class="container">--}}
            {{--<h2 class="heading">Our Works</h2>--}}

            {{--<div class="portfolio-filter-group folio-fillter-2 mrg-btm-50 mrg-vertical-30 text-center">--}}
                {{--<a href="javascript:void(0);" data-filter="*" class="iso-active iso-button ">All</a>--}}
                {{--<a href="javascript:void(0);" data-filter=".development" class="iso-button">Development</a>--}}
                {{--<a href="javascript:void(0);" data-filter=".design" class="iso-button">Design</a>--}}
                {{--<a href="javascript:void(0);" data-filter=".writing" class="iso-button">Writing</a>--}}
                {{--<a href="javascript:void(0);" data-filter=".accounting" class="iso-button">Accounting</a>--}}
            {{--</div>--}}
            {{--<!-- portfolio-filter -->--}}
            {{--<div class="portfolio portfolio-isotope masonry col-4 row mrg-top-50">--}}
                {{----}}
                {{--<div class="folio-item development design">--}}
                    {{--<div class="folio-style-1">--}}
                        {{--<div class="folio-image">--}}
                            {{--<img class="img-responsive" src="{{$_G['xenren_path']}}images/portfolio/1.jpg" alt="">--}}
                        {{--</div>--}}
                        {{--<!-- folio-image -->--}}
                        {{--<div class="overlay">--}}
                            {{--<div class="overlay-caption">--}}
                                {{--<div class="overlay-content">--}}
                                    {{--<!-- /folio-info -->--}}
                                    {{--<div class="folio-links">--}}
                                        {{--<a href="javascript:void(0);"><i class="ti-link"></i></a>--}}
                                        {{--<a href="javascript:void(0);"><i class="ti-search"></i></a>--}}
                                    {{--</div>--}}
                                    {{--<!-- folio-links -->--}}
                                    {{----}}
                                    {{--<div class="folio-info">--}}
                                        {{--<h3 class="text-white">Creative Design</h3>--}}
                                        {{--<p>Lorem ipsum dolor sit amet</p>--}}
                                    {{--</div>--}}
                                    {{----}}
                                {{--</div>--}}
                                {{--<!-- /overlay-content -->--}}
                            {{--</div>--}}
                            {{--<!-- /overlay-caption -->--}}
                        {{--</div>--}}
                        {{--<!-- /overlay -->--}}
                    {{--</div>--}}
                    {{--<!-- /folio-style -->--}}
                {{--</div>--}}
                {{--<!-- /folio-item -->--}}
                {{----}}
                {{--<div class="folio-item development design writing">--}}
                    {{--<div class="folio-style-1">--}}
                        {{--<div class="folio-image">--}}
                            {{--<img class="img-responsive" src="{{$_G['xenren_path']}}images/portfolio/2.jpg" alt="">--}}
                        {{--</div>--}}
                        {{--<!-- folio-image -->--}}
                        {{--<div class="overlay">--}}
                            {{--<div class="overlay-caption">--}}
                                {{--<div class="overlay-content">--}}
                                    {{--<!-- /folio-info -->--}}
                                    {{--<div class="folio-links">--}}
                                        {{--<a href="javascript:void(0);"><i class="ti-link"></i></a>--}}
                                        {{--<a href="javascript:void(0);"><i class="ti-search"></i></a>--}}
                                    {{--</div>--}}
                                    {{--<!-- folio-links -->--}}
                                    {{----}}
                                    {{--<div class="folio-info">--}}
                                        {{--<h3 class="text-white">Creative Design</h3>--}}
                                        {{--<p>Lorem ipsum dolor sit amet</p>--}}
                                    {{--</div>--}}
                                    {{----}}
                                {{--</div>--}}
                                {{--<!-- /overlay-content -->--}}
                            {{--</div>--}}
                            {{--<!-- /overlay-caption -->--}}
                        {{--</div>--}}
                        {{--<!-- /overlay -->--}}
                    {{--</div>--}}
                    {{--<!-- /folio-style -->--}}
                {{--</div>--}}
                {{--<!-- /folio-item -->--}}

                {{--<div class="folio-item accounting development">--}}
                    {{--<div class="folio-style-1">--}}
                        {{--<div class="folio-image">--}}
                            {{--<img class="img-responsive" src="{{$_G['xenren_path']}}images/portfolio/3.jpg" alt="">--}}
                        {{--</div>--}}
                        {{--<!-- folio-image -->--}}
                        {{--<div class="overlay">--}}
                            {{--<div class="overlay-caption">--}}
                                {{--<div class="overlay-content">--}}
                                    {{--<!-- /folio-info -->--}}
                                    {{--<div class="folio-links">--}}
                                        {{--<a href="javascript:void(0);"><i class="ti-link"></i></a>--}}
                                        {{--<a href="javascript:void(0);"><i class="ti-search"></i></a>--}}
                                    {{--</div>--}}
                                    {{--<!-- folio-links -->--}}
                                    {{----}}
                                    {{--<div class="folio-info">--}}
                                        {{--<h3 class="text-white">Web Design</h3>--}}
                                        {{--<p>Lorem ipsum dolor sit amet</p>--}}
                                    {{--</div>--}}
                                    {{----}}
                                {{--</div>--}}
                                {{--<!-- /overlay-content -->--}}
                            {{--</div>--}}
                            {{--<!-- /overlay-caption -->--}}
                        {{--</div>--}}
                        {{--<!-- /overlay -->--}}
                    {{--</div>--}}
                    {{--<!-- /folio-style -->--}}
                {{--</div>--}}
                {{--<!-- /folio-item -->--}}

                {{--<div class="folio-item writing accounting">--}}
                    {{--<div class="folio-style-1">--}}
                        {{--<div class="folio-image">--}}
                            {{--<img class="img-responsive" src="{{$_G['xenren_path']}}images/portfolio/4.jpg" alt="">--}}
                        {{--</div>--}}
                        {{--<!-- folio-image -->--}}
                        {{--<div class="overlay">--}}
                            {{--<div class="overlay-caption">--}}
                                {{--<div class="overlay-content">--}}
                                    {{--<!-- /folio-info -->--}}
                                    {{--<div class="folio-links">--}}
                                        {{--<a href="javascript:void(0);"><i class="ti-link"></i></a>--}}
                                        {{--<a href="javascript:void(0);"><i class="ti-search"></i></a>--}}
                                    {{--</div>--}}
                                    {{--<!-- folio-links -->--}}
                                    {{----}}
                                    {{--<div class="folio-info">--}}
                                        {{--<h3 class="text-white">Creative Design</h3>--}}
                                        {{--<p>Lorem ipsum dolor sit amet</p>--}}
                                    {{--</div>--}}
                                    {{----}}
                                {{--</div>--}}
                                {{--<!-- /overlay-content -->--}}
                            {{--</div>--}}
                            {{--<!-- /overlay-caption -->--}}
                        {{--</div>--}}
                        {{--<!-- /overlay -->--}}
                    {{--</div>--}}
                    {{--<!-- /folio-style -->--}}
                {{--</div>--}}
                {{--<!-- /folio-item -->--}}

                {{--<div class="folio-item development design writing">--}}
                    {{--<div class="folio-style-1">--}}
                        {{--<div class="folio-image">--}}
                            {{--<img class="img-responsive" src="{{$_G['xenren_path']}}images/portfolio/5.jpg" alt="">--}}
                        {{--</div>--}}
                        {{--<!-- folio-image -->--}}
                        {{--<div class="overlay">--}}
                            {{--<div class="overlay-caption">--}}
                                {{--<div class="overlay-content">--}}
                                    {{--<!-- /folio-info -->--}}
                                    {{--<div class="folio-links">--}}
                                        {{--<a href="javascript:void(0);"><i class="ti-link"></i></a>--}}
                                        {{--<a href="javascript:void(0);"><i class="ti-search"></i></a>--}}
                                    {{--</div>--}}
                                    {{--<!-- folio-links -->--}}
                                    {{----}}
                                    {{--<div class="folio-info">--}}
                                        {{--<h3 class="text-white">Creative Design</h3>--}}
                                        {{--<p>Lorem ipsum dolor sit amet</p>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<!-- /overlay-content -->--}}
                            {{--</div>--}}
                            {{--<!-- /overlay-caption -->--}}
                        {{--</div>--}}
                        {{--<!-- /overlay -->--}}
                    {{--</div>--}}
                    {{--<!-- /folio-style -->--}}
                {{--</div>--}}
                {{--<!-- /folio-item -->--}}

                {{--<div class="folio-item development design">--}}
                    {{--<div class="folio-style-1">--}}
                        {{--<div class="folio-image">--}}
                            {{--<img class="img-responsive" src="{{$_G['xenren_path']}}images/portfolio/6.jpg" alt="">--}}
                        {{--</div>--}}
                        {{--<!-- folio-image -->--}}
                        {{--<div class="overlay">--}}
                            {{--<div class="overlay-caption">--}}
                                {{--<div class="overlay-content">--}}
                                    {{--<!-- /folio-info -->--}}
                                    {{--<div class="folio-links">--}}
                                        {{--<a href="javascript:void(0);"><i class="ti-link"></i></a>--}}
                                        {{--<a href="javascript:void(0);"><i class="ti-search"></i></a>--}}
                                    {{--</div>--}}
                                    {{--<!-- folio-links -->--}}
                                    {{----}}
                                    {{--<div class="folio-info">--}}
                                        {{--<h3 class="text-white">Web Design</h3>--}}
                                        {{--<p>Lorem ipsum dolor sit amet</p>--}}
                                    {{--</div>--}}
                                    {{----}}
                                {{--</div>--}}
                                {{--<!-- /overlay-content -->--}}
                            {{--</div>--}}
                            {{--<!-- /overlay-caption -->--}}
                        {{--</div>--}}
                        {{--<!-- /overlay -->--}}
                    {{--</div>--}}
                    {{--<!-- /folio-style -->--}}
                {{--</div>--}}
                {{--<!-- /folio-item -->--}}

                {{--<div class="folio-item accounting design">--}}
                    {{--<div class="folio-style-1">--}}
                        {{--<div class="folio-image">--}}
                            {{--<img class="img-responsive" src="{{$_G['xenren_path']}}images/portfolio/7.jpg" alt="">--}}
                        {{--</div>--}}
                        {{--<!-- folio-image -->--}}
                        {{--<div class="overlay">--}}
                            {{--<div class="overlay-caption">--}}
                                {{--<div class="overlay-content">--}}
                                    {{--<!-- /folio-info -->--}}
                                    {{--<div class="folio-links">--}}
                                        {{--<a href="javascript:void(0);"><i class="ti-link"></i></a>--}}
                                        {{--<a href="javascript:void(0);"><i class="ti-search"></i></a>--}}
                                    {{--</div>--}}
                                    {{--<!-- folio-links -->--}}
                                    {{----}}
                                    {{--<div class="folio-info">--}}
                                        {{--<h3 class="text-white">Creative Design</h3>--}}
                                        {{--<p>Lorem ipsum dolor sit amet</p>--}}
                                    {{--</div>--}}
                                    {{----}}
                                {{--</div>--}}
                                {{--<!-- /overlay-content -->--}}
                            {{--</div>--}}
                            {{--<!-- /overlay-caption -->--}}
                        {{--</div>--}}
                        {{--<!-- /overlay -->--}}
                    {{--</div>--}}
                    {{--<!-- /folio-style -->--}}
                {{--</div>--}}
                {{--<!-- /folio-item -->--}}

                {{--<div class="folio-item design">--}}
                    {{--<div class="folio-style-1">--}}
                        {{--<div class="folio-image">--}}
                            {{--<img class="img-responsive" src="{{$_G['xenren_path']}}images/portfolio/8.jpg" alt="">--}}
                        {{--</div>--}}
                        {{--<!-- folio-image -->--}}
                        {{--<div class="overlay">--}}
                            {{--<div class="overlay-caption">--}}
                                {{--<div class="overlay-content">--}}
                                    {{--<!-- /folio-info -->--}}
                                    {{--<div class="folio-links">--}}
                                        {{--<a href="javascript:void(0);"><i class="ti-link"></i></a>--}}
                                        {{--<a href="javascript:void(0);"><i class="ti-search"></i></a>--}}
                                    {{--</div>--}}
                                    {{--<!-- folio-links -->--}}
                                    {{----}}
                                    {{--<div class="folio-info">--}}
                                        {{--<h3 class="text-white">Creative Design</h3>--}}
                                        {{--<p>Lorem ipsum dolor sit amet</p>--}}
                                    {{--</div>--}}
                                    {{----}}
                                {{--</div>--}}
                                {{--<!-- /overlay-content -->--}}
                            {{--</div>--}}
                            {{--<!-- /overlay-caption -->--}}
                        {{--</div>--}}
                        {{--<!-- /overlay -->--}}
                    {{--</div>--}}
                    {{--<!-- /folio-style -->--}}
                {{--</div>--}}
                {{--<!-- /folio-item -->--}}

            {{--</div>--}}
            {{----}}
            {{--<a class="btn btn-style-3 btn-lg mrg-top-50 centering pagination-centered"><i class="ti-plus"></i> Load More</a><br>--}}
            {{--<!-- /portfolio -->--}}
        {{--</div>--}}
        {{--<!-- /container -->--}}
    {{--</section>--}}
    {{--<!-- /#Portfolio -->--}}

    <!-- Testimonial -->
    <section id="testimonial" class="section section-slider bg-dark-blue testimonial">

        <div class="oval-shape-top"></div>

        <div class="container">
            <h2 class="heading">{{ trans('home.people_say') }}</h2>
            
            <!-- <img src="{{$_G['xenren_path']}}images/icon/quote.png" alt="testimonial" class="quote-img centering mrg-top-50"/> -->
            <img src="{{$_G['xenren_path']}}images/icon/what-people-say-icon.png" alt="testimonial" class="centering title-icon" />

            <div class="testimonial-1 owl-carousel owl-single-all owl-pagi-1 owl-nav-1 pagi-center owl testimonial-1 mrg-top-30">
                <div class="item text-center">

                    <div class="col-md-3 col-sm-5 col-xs-12">
                        <div class="people-avatar" style="background-image: url('{{URL::to('/')}}/images/img1.jpg');"></div>
                    </div>
                    <div class="col-md-9 col-sm-7 col-xs-12 people-shortstory">
                        <h1>{{ trans('home.people_helen') }}</h1>
                        <h3>{{ trans('home.entrepreneur') }}</h3>
                        <p>“ {{ trans('home.people_helen_say') }} ”</p>
                    </div>

                    <!-- <p>
                        Xenren.com is an amazing platform to start your own freelance projects or to become a client getting done your work. It saves your time and energy. It has a huge connection throughout the world and you will never out of works here. In terms of security, modern technology is used and till today there is no claim of breaking the security. Privacy is totally maintained. Each type of people can find their work here and the way getting payments in time are praiseworthy.
                    </p> -->

                    <!-- <h5 class="person mrg-top-15">
                        <span class="person-name">Lorem Ipsum</span><br>
                        <span class="person-title">UX Designer</span>
                    </h5> -->

                    <!-- <br> -->

                    <!-- <div class="person mrg-vertical-15">
                        <img class="img-responsive" src="{{$_G['xenren_path']}}images/thumb/testi-1.jpg" alt="">
                    </div> -->
                    <!-- /person -->
                </div>
                <div class="item text-center">

                    <div class="col-sm-12">
                        <div class="col-sm-3">
                            <div class="people-avatar" style="background-image: url('{{URL::to('/')}}/images/img2.jpg');"></div>
                        </div>
                        <div class="col-md-9 col-sm-7 col-xs-12 people-shortstory">
                            <h1>{{ trans('home.people_xubing') }}</h1>
                            <h3>{{ trans('home.hr') }}</h3>
                            <p>“ {{ trans('home.people_xubing_say') }} ”</p>
                        </div>

                    </div>
                </div>
                <div class="item text-center">

                    <div class="col-sm-12">
                        <div class="col-sm-3">
                            <div class="people-avatar" style="background-image: url('{{URL::to('/')}}/images/img3.jpg');"></div>
                        </div>
                        <div class="col-md-9 col-sm-7 col-xs-12 people-shortstory">
                            <h1>{{ trans('home.people_khairul') }}</h1>
                            <h3>{{ trans('home.freelancer') }}</h3>
                            <p>“ {{ trans('home.people_khairul_say') }} ”</p>
                        </div>

                    </div>
                </div>
                <!-- /item -->

                <!-- /item -->

                <!-- <div class="item text-center">
                    <p>
                        The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.
                    </p>
                    <h5 class="person mrg-top-15">
                        <span class="person-name">Lorem Ipsum</span><br>
                        <span class="person-title">UX Designer</span>
                    </h5>
                    
                    <br>
                    
                    <div class="person mrg-vertical-15">
                        <img class="img-responsive" src="{{$_G['xenren_path']}}images/thumb/testi-2.jpg" alt="">
                    </div>
                </div> -->
                <!-- /item -->

                <!-- <div class="item text-center">
                    <p>
                        Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.
                    </p>
                    <h5 class="person mrg-top-15">
                        <span class="person-name">Lorem Ipsum</span><br>
                        <span class="person-title">UX Designer</span>
                    </h5>
                    
                    <br>
                    
                    <div class="person mrg-vertical-15">
                        <img class="img-responsive" src="{{$_G['xenren_path']}}images/thumb/testi-3.jpg" alt="">
                    </div>
                    
                </div> -->
                <!-- /item -->

            </div>
            <!-- /testimonial -->

        </div>
        <!-- /container -->

    </section>
    <!-- /testimonial -->

    <!-- Team 1 -->
    <section id="team" class="section amazing-team">
        <div class="container">
        	<div class="top-border-header"></div>
            <h2 class="heading">{{ trans('home.meet_team') }}</h2>

            <!-- /text-center -->

            <div class="team-style-1 owl-3 owl-carousel mrg-top-20">
                <div class="owl-item team-member">
                    <div class="member-img">
                        <img src="{{$_G['xenren_path']}}images/team/team-avatar1.jpg" alt="">
                        <div class="overlay">
                            <div class="overlay-caption">
                                <div class="overlay-content">
                                    <a href="javascript:void(0);">
                                        <img class="centering pagination-centered" src="{{$_G['xenren_path']}}images/icon/member-info.png" alt="member info"/>
                                    </a>
                                </div>
                            </div>
                        </div>
	                    <div class="member-info text-center">
	                        <h3>Adam Johnson</h3>
	                        <p class="member-title monserrat">{{ trans('home.designer') }}</p>
	                    </div>
                    </div>
                    <!-- /member-image -->
                    <!-- text-center-->
                </div>
                <!-- /team-member -->

                <div class="owl-item team-member">
                    <div class="member-img">
                        <img src="{{$_G['xenren_path']}}images/team/team-avatar2.jpg" alt="">
                        <div class="overlay">
                            <div class="overlay-caption">
                                <div class="overlay-content">
                                    <a href="javascript:void(0);">
                                        <img class="centering pagination-centered" src="{{$_G['xenren_path']}}images/icon/member-info.png" alt="member info"/>
                                    </a>
                                </div>
                                <!-- /overlay -content -->
                            </div>
                            <!-- /overlay-caption -->
                        </div>
                        <!-- /overlay -->
                    </div>
                    <!-- /member-image -->
                    <div class="member-info text-center">
                        <h3>B. Lucia</h3>
                        <p class="member-title monserrat">{{ trans('home.designer') }}</p>
                    </div>
                    <!-- text-center-->
                </div>
                <!-- /team-member -->

                <div class="owl-item team-member">
                    <div class="member-img">
                        <img src="{{$_G['xenren_path']}}images/team/team-avatar3.jpg" alt="">
                        <div class="overlay">
                            <div class="overlay-caption">
                                <div class="overlay-content">
                                    <a href="javascript:void(0);">
                                        <img class="centering pagination-centered" src="{{$_G['xenren_path']}}images/icon/member-info.png" alt="member info"/>
                                    </a>
                                </div>
                                <!-- /overlay -content -->
                            </div>
                            <!-- /overlay-caption -->
                        </div>
                        <!-- /overlay -->
                    </div>
                    <!-- /member-image -->
                    <div class="member-info text-center">
                        <h3>John Do</h3>
                        <p class="member-title monserrat">{{ trans('home.web_developer') }}</p>
                    </div>
                    <!-- text-center-->
                </div>
                <!-- /team-member-->

            </div>
            <!-- /team-style-1 -->
        </div>
        <!-- /container -->
    </section>
    <!-- /Team 1-->
    
    <!-- Service Secure -->
    <section id="secure" class="secure team-section section bg-dark-blue testimonial bg-dark-large">

        <div class="container our-team">
        	<div class="top-border-header"></div>
            <h2 class="heading">{{ trans('home.our_team') }}</h2>
            <!-- <img src="{{$_G['xenren_path']}}images/icon/our-team-icon.png" alt="testimonial" class="centering title-icon" /> -->

            <div class="col-sm-12">
                <div class="col-sm-3 center-text team-div">
                    <a href="javascript:void(0);">
                        <img src="{{$_G['xenren_path']}}images/team/team04.png" alt="" />
                    </a>
                    <div class="team-div-shadow"></div>
                    <h4>
                        {{ trans('home.team_fong') }}
                        <span><h5 style="font-size: 10px; color: #5EC329; margin-top:2px;">{{ trans('home.team_CEO') }}</h5></span>
                    </h4>
                </div>
                <div class="col-sm-3 center-text team-div">
                    <a href="javascript:void(0);">
                        <img src="{{$_G['xenren_path']}}images/team/team03.png" alt="" />
                    </a>
                    <div class="team-div-shadow"></div>
                    <h4>
                        {{ trans('home.team_Xenren') }}
                        <span><h5 style="font-size: 10px; color: #5EC329; margin-top:2px;">{{ trans('home.team_CTO') }}</h5></span>
                    </h4>
                </div>
                <div class="col-sm-3 center-text team-div">
                    <a href="javascript:void(0);">
                        <img src="{{$_G['xenren_path']}}images/team/team02.png" alt="" />
                    </a>
                    <div class="team-div-shadow"></div>
                    <h4>
                        {{ trans('home.team_chenlou') }}
                        <span><h5 style="font-size: 10px; color: #5EC329; margin-top:2px;">{{ trans('home.team_CPO') }}</h5></span>
                    </h4>
                </div>
                <div class="col-sm-3 center-text team-div">
                    <a href="javascript:void(0);">
                        <img src="{{$_G['xenren_path']}}images/team/team01.png" alt="" />
                    </a>
                    <div class="team-div-shadow"></div>
                    <h4>
                        {{ trans('home.team_chenkun') }}
                        <span><h5 style="font-size: 10px; color: #5EC329; margin-top:2px;">{{ trans('home.team_CCO') }}</h5></span>
                    </h4>
                </div>
            </div>

            <p class="sub-heading">{!! trans('home.team_desc') !!}</p>

        </div>
        <!-- /container -->
    </section>
    <section class="section secure bg-dark-blue testimonial bg-dark-large home-services">
        <div class="container">
            <div class="col-sm-6 heading">
            	<p class="border-custom"></p>
                <h3>{{ trans('home.services_security') }}</h3>
                <!-- <p class="sub-heading">
                    {!! trans('home.platform_desc') !!}
                </p> -->
                <div class="row home-services-main">
                	<div class="col-md-3 home-services-main-l">
                		<a href="javascript:void(0);">
	                        <img src="{{$_G['xenren_path']}}images/icon/money-hover.png" data-src="money" alt="money guarantee" width="100px" />
	                    </a>	
                	</div>
                	<div class="col-md-9 home-services-main-r">
                		<a href="javascript:void(0);">
                			{{ trans('home.money_guarantee') }}
                		</a>	
                		<p>
                			{{ trans('home.money_guarantee_text') }}	
                		</p>
                	</div>
                </div>

                <div class="row home-services-main">
                	<div class="col-md-3 home-services-main-l">
                		<a href="javascript:void(0);">
	                        <img src="{{$_G['xenren_path']}}images/icon/skill-verify-hover.png" data-src="skill-verify" alt="Official skill verify" width="100px" />
	                    </a>
                	</div>
                	<div class="col-md-9 home-services-main-r">
                		<a href="javascript:void(0);">
	                        {{ trans('home.skill_verify') }}
                		</a>	
                		<p>
                			{{ trans('home.skill_verify_text') }}
                		</p>
                	</div>
                </div>	

            </div>
            <!-- /column -->
            
            <div class="col-sm-6">
                <div class="row home-services-main">
                	<div class="col-md-3 home-services-main-l">
                		<a href="javascript:void(0);">
	                        <img src="{{$_G['xenren_path']}}images/icon/cs-hover.png" data-src="ts" alt="Experience technician support" width="100px" />
	                    </a>
                	</div>
                	<div class="col-md-9 home-services-main-r">
                		<a href="javascript:void(0);">
	                        {{ trans('home.tech_support') }}
                		</a>	
                		<p>
                			{{ trans('home.tech_support_text') }}
                		</p>
                	</div>
                </div>


				<div class="row home-services-main">
                	<div class="col-md-3 home-services-main-l">
                		<a href="javascript:void(0);">
	                        <img src="{{$_G['xenren_path']}}images/icon/skillful-team-hover.png" data-src="cs" alt="Customer service" width="100px" />
	                    </a>
                	</div>
                	<div class="col-md-9 home-services-main-r">
                		<a href="javascript:void(0);">
	                        {{ trans('home.customer_service') }}
                		</a>	
                		<p>
                			{{ trans('home.customer_service_text') }}
                		</p>
                	</div>
                </div>	                

                <div class="row home-services-main">
                	<div class="col-md-3 home-services-main-l">
                		<a href="javascript:void(0);">
	                        <img src="{{$_G['xenren_path']}}images/icon/ts-hover.png" data-src="skillful-team" alt="10000+ of skillful person" width="100px" />
	                    </a>
                	</div>
                	<div class="col-md-9 home-services-main-r">
                		<a href="javascript:void(0);">
	                        {{ trans('home.skillfull_person') }}
                		</a>	
                		<p>
                			{{ trans('home.skillfull_person_text') }}
                		</p>
                	</div>
                </div>	                
            </div>
        </div>
        <!-- /container -->
        <!-- order now -->
        <div class="container">
            <div class="col-sm-12">
                <hr class="services">
                <p class="sub-heading">{!! trans('home.team_desc') !!}</p>
                <a class="btn btn-style-3 btn-lg mrg-top-50 centering pagination-centered">{{ trans('home.order_now') }}</a><br>
            </div>
            <!-- /column -->
        </div>

        <!-- /order now -->
    </section>
    <!-- /Service Secure -->

    <input id="user-id" type="hidden" value="{{ isset($_G['user']->id)? $_G['user']->id : '' }}">
    <input id="user-status" type="hidden" value="{{ isset($_G['user']->status)? $_G['user']->id : '' }}">
{{--    @include('frontend.includes.modalAddUserStatus')--}}
@endsection

@section('footer')
    <script src="/custom/js/frontend/home.js" type="text/javascript"></script>
    <script type="text/javascript">
        +function () {
            $(document).ready(function () {
                $('#btn-add-status-submit').on('click', function () {
                    $status = $('#status').val();
                    var url = $(this).data('url');
                    $.ajax({
                        url: url,
                        method: 'post',
                        data: {'_token': '{!! csrf_token() !!}', 'status': $status},
                        dataType: 'json',
                        beforeSend: function () {
                        },
                        success: function (resp) {
                            if (resp.status == 'success') {
                                $('#user-status').val($status);
                                $('#modalAddUserStatus').modal('hide');
                            } else {
                                alertError(resp.msg);
                            }
                        },
                        error: function (resp) {
                            alert(trans('common.unknown_error'));
                        }
                    });
                });
            });
        }(jQuery);
    </script>
@endsection
