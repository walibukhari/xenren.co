<?php

return [
	'banner_span' => '神人资策室',
	'banner_h1' => '线下业务代表',
	'title1'=>'软件制作',
	'desc1'=>'<p>我们有丰富的软件开发经验，从商务网页、小型网页如微信HTML5 到大型软件的开发。结合时尚设计、符合市场的用户习惯。</p>
			  <p>神人主要以原创开发与定制为主，从项目开发之前给用户建议，到过程中不断确认需求，以确保项目在最高效的状态下完成。</p>
			  <p>我们按照企业规模建议适宜的框架、并提供完整的开发文档，代码注释、以便我们的客户进行二次开发。</p>',
	'title2'=>'定制手机应用',
	'desc2'=>'<p>科技主导效率的年代，手机应用配合线下业务，将为客户带来更好的用户体验。</p>
			  <p>除了商业软件，大型企业也定制专属的移动端应用，AR等内部管理系统、以提高企业运作效率。</p>
			  <p>我们了解用商业模式以及户数据的重要性，因此神人提供稳定的后续维护，同时对您的商业模式、代码等严格保密。</p>',		  
	'title3'=>'精益技术',
	'java' => 'Java',
	'php' => 'PHP',
	'ios' => '苹果',
	'android' => '安卓',		  
	'my_sql' => 'MySQL',
	'hadoop' => 'Hadoop',
	'github' => 'GitHub',
	'html_css_js' => 'HTML | CSS | JS',
	'point1_title' => '业务代表',
	'point1_desc' => '<p>2014年神人股份有限公司于香港成立、技术部门成立于马来西亚。团队拥有多语言背景，精通于中英文。</p>',
	'point2_title' => '专业产品研发团队',
	'point2_desc' => '<p>企业研发软件主要目的是为了获利，因此商业模式的可行性是关键。</p>
					<p>从商业模式决定软件的功能，从公司的发展规划决定我们应该什么技术。</p>	
					<p>前期的项目的评估工作，可以减低中途预算不足而项目终止的风险。同时协助客户查看整个商业模式否有需要加强的部分。</p>	
					<p>新创公司一般无法负担高拓展性的框架，如：Laravel 、Java J2EE等技术框架。针对小型企业，我们一般会推荐CMS或是开源代码等技术。</p>',
	'point3_title' => '为何选择我们',
	'point3_desc' => '<p>神人股份有限公司拥有自己技术团队、核心技术、管理文化、以及长期合作的各种领域专才，以覆盖我们客户的各种需求。</p>
					<p>多年的经验告诉我们，专业的人能在最短时间完成、并产出最好结果。</p>',
	'point4_title' => '打造产品，而非软件',
	'point4_desc' => '<p>我们注重用户体验、并根据市场的需求去定制软件。神人与世界各地的设计师都有合作、包括中国、欧洲、美国、印度东南亚等。合适的设计，会让产品的推广事半功倍。</p>
					<p>此外，我们也协助客户选择合适的境内外付款渠道，帮助新创公司建立股权，或托管股权等服务。</p>',
	'contactus' => '联系我们',				
	'contactus_n' => '0755-23320228',
	'hotline' => '询问热线 14 x 7',
	'team1_position' => '软件架构师',
	'team1_name' => '房保宏',
	'team1_email' => 'fong@xenren.co',
	'team1_number' => '+86 13059135801 +0128509699',
	'team1_add' => '深圳 宝安区 西乡步行街 宝金华大厦 1105',
	'team2_position' => '市场总监',
	'team2_name' => 'Lina Chieng',
	'team2_email' => 'lina@xenren.co',
	'team2_number' => '+60168710081',
	'team2_add' => 'Lot L2.09, 2nd Floor, Shaw Parade. Jalan Changkat Thambi 
					Dollah, Kuala Lumpur, 55100. Kuala Lumpur, WP Kuala Lumpur',
	'team3_position' => '东马市场总监',
	'team3_name' => '许维胜',
	'team3_email' => 'hws@xenren.co',
	'team3_number' => '+60168996337',
	'team3_add' => 'Lot L2.09, 2nd Floor, Shaw Parade. Jalan Changkat Thambi 
					Dollah, Kuala Lumpur, 55100. Kuala Lumpur, WP Kuala Lumpur',
	'title4' => '<span>神人</span> 业务团队'
];