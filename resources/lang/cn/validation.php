<?php

return array(
    'accepted' => ':attribute 必须接受。',
    'active_url' => ':attribute 不是一个有效的网址。',
    'after' => ':attribute 必须是一个在 :date 之后的日期。',
    'alpha' => ':attribute 只能由字母组成。',
    'alpha_dash' => ':attribute 只能由字母、数字和斜杠组成。',
    'alpha_space' => ':attribute 只能由字母、数字和空格组成。',
    'alpha_num' => ':attribute 只能由字母和数字组成。',
    'array' => ':attribute 必须是一个数组。',
    'before' => ':attribute 必须是一个在 :date 之前的日期。',
    'select_proper_location' => "请从给定的选项中选择适当的位置",
    'between' =>
        array(
            'numeric' => ':attribute 必须介于 :min - :max 之间。',
            'file' => ':attribute 必须介于 :min - :max kb 之间。',
            'string' => ':attribute 必须介于 :min - :max 个字符之间。',
            'array' => ':attribute 必须只有 :min - :max 个单元。',
        ),
    'boolean' => ':attribute 必须为布尔值。',
    'confirmed' => ':attribute 两次输入不一致。',
    'date' => ':attribute 不是一个有效的日期。',
    'date_format' => ':attribute 的格式必须为 :format。',
    'different' => ':attribute 和 :other 必须不同。',
    'digits' => ':attribute 必须是 :digits 位的数字。',
    'digits_between' => ':attribute 必须是介于 :min 和 :max 位的数字。',
    'email' => ':attribute 不是一个合法的邮箱。',
    'exists' => ':attribute 不存在。',
    'filled' => ':attribute 不能为空。',
    'image' => ':attribute 必须是图片。',
    'in' => '已选的属性 :attribute 非法。',
    'integer' => ':attribute 必须是整数。',
    'ip' => ':attribute 必须是有效的 IP 地址。',
    'json' => ':attribute 必须是正确的 JSON 格式。',
    'max' =>
        array(
            'numeric' => ':attribute 不能大于 :max。',
            'file' => ':attribute 不能大于 :max kb。',
            'string' => ':attribute 不能大于 :max 个字符。',
            'array' => ':attribute 最多只有 :max 个单元。',
        ),
    'mimes' => ':attribute 必须是一个 :values 类型的文件。',
    'min' =>
        array(
            'numeric' => ':attribute 必须大于等于 :min。',
            'file' => ':attribute 大小不能小于 :min kb。',
            'string' => ':attribute 至少为 :min 个字符。',
            'array' => ':attribute 至少有 :min 个单元。',
        ),
    'not_in' => '已选的属性 :attribute 非法。',
    'numeric' => ':attribute 必须是一个数字。',
    'regex' => ':attribute 格式不正确。',
    'required' => ':attribute 不能为空。',
    'required_if' => '当 :other 为 :value 时 :attribute 不能为空。',
    'required_unless' => '当 :other 不为 :value 时 :attribute 不能为空。',
    'required_with' => '当 :values 存在时 :attribute 不能为空。',
    'required_with_all' => '当 :values 存在时 :attribute 不能为空。',
    'required_without' => '当 :values 不存在时 :attribute 不能为空。',
    'required_without_all' => '当 :values 都不存在时 :attribute 不能为空。',
    'same' => ':attribute 和 :other 必须相同。',
    'size' =>
        array(
            'numeric' => ':attribute 大小必须为 :size。',
            'file' => ':attribute 大小必须为 :size kb。',
            'string' => ':attribute 必须是 :size 个字符。',
            'array' => ':attribute 必须为 :size 个单元。',
        ),
    'string' => ':attribute 必须是一个字符串。',
    'timezone' => ':attribute 必须是一个合法的时区值。',
    'unique' => ':attribute 已经存在。',
    'url' => ':attribute 格式不正确。',
    'custom' =>
        array(
            'attribute-name' =>
                array(
                    'rule-name' => 'custom-message',
                ),
        ),
    'is_permission_tag' => '错误的权限。',
    'not_email' => ':attribute 不可为电子邮箱地址。',
    'is_password' => ':attribute 不是合法的密码，必须至少6个字符',
    'is_number' => ':attribute 必须为纯数字组合',
    'is_fund' => ':attribute 为错误的金额',
    'is_project_level' => '错误的项目等级',
    'is_project_type' => '错误的项目类型',
    'is_lang' => '错误的语言',
    'is_skill_level' => '错误的技能等级',

    /*Coworker list space popup*/
    'company_required' => '共享空间名称不能为空',
    'location_required' => '请输入共享空间地址',
    'image_required' => '图像不能为空',
    'name_required' => '姓名不能为空',
    'phone_required'=> '电话号码不能为空',
    'email_required'=> '电子邮箱不能为空',
    'images_limit'=> '上传的图像不能超过7个',
    'success_listspace' => '您的共享空间已上传成功',

    /*Coworker space book now popup*/
    'full_name_enter' => '请输入全名',
    'phone_enter' => '请输入电话号码',
    'select_checkin' =>'请选择入住日期',
    'select_checkout' =>'请选择离开日期',
    'select_checkout' =>'请选择离开日期',
    'category_id' =>'请选择设施',
    'no_of_rooms' => '请输入房间数量',
    'no_of_people' => '请输入人数',
    'remark_required' => '备注不能为空',
    'office_not_complete' => '此办公室尚未完善',

    /*Coworker space contact message popup*/
    'name_enter' => '请输入您的名字',
    'enter_email' => '请输入您的电子邮箱',
    'enter_subject' => '请输入主题',
    'enter_message' => '请输入信息',

    /*Coworker space write a review popup*/
    'fill_stars' => '请打分',
    'comment_required' => '请发表评论',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' =>  array(
        'country_id' => '国家',
        'about_me' => '关于我',
        'reason' => '理由',
        'english_proficiency' => '英语水平',
        'title' => '标题',
        'description' => '描述',
        'receiver' => '接收者',
        'message' => '讯息',
        'chat_room_id' => '聊天室ID',
        'url' => '链接',
        'file_upload' => '上传文件',
        'job_position' => '工作职位',
        'hidUploadedFile' => '文件',
        'email' => '电子邮箱地址',
        'phone_or_email' => '电子邮箱地址或电话号码',
        'password' => '密码',
        'password_confirmation' => '确认密码',
        'CaptchaCode' => '验证码',
        'real_name' => '真实姓名',
        'id_card_no' => '身份证号码',
        'gender' => '性别',
        'address' => '所在地',
        'date_of_birth' => '出生日期',
        'handphone_no' => '手机号码',
        'id_image' => '身份证图 ',
    ),
);

