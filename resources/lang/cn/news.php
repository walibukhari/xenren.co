<?php
/**
 * Created by PhpStorm.
 * User: khegiw
 * Date: 03/12/2016
 * Time: 05:46
 */

return [
    'dashboard' => "Dashboard",
    'news' => "News",
    'create' => 'Create',
    'save' => 'Save',
    'slider' => 'Slider',
    'edit' => 'edit',
    'fill_in_the_form_to_update_a_news' => 'Fill in the form to update a news',
    'crud' => 'View News/Update News',
    'add_news' => 'Add News',
    'image' => 'Image',
    'title' => 'Title',
    'sub-title' => 'Sub Title',
    'action' => 'Action',
];