<?php

return array(
    'name_required' => '名字不能为空。',
    'lastname_required' => '姓氏不能为空。',
    'email_required' => '电子邮件地址不能为空。',
    'validate_email' => '电子邮件必须是有效的电子邮件地址。',
    'password_required' => '密码不能为空。',
    'password_confirmation_required' => '确认密码不能为空。',
    'remember_required' => '请签署协议。',
);

