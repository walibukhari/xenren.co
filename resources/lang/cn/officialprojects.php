<?php
/**
 * Created by PhpStorm.
 * User: khegiw
 * Date: 29/11/2016
 * Time: 16:40
 */

return [
    "新增官方项目" => "新增官方项目",
    "desc" => "新增 官方项目",
    "后台" => "后台",
    "官方项目" => "官方项目",
    "内容" => "内容",
    "图集" => "图集",
    "项目语言" => "项目语言",
    "项目标题" => "项目标题",
    "项目简介" => "项目简介",
    "招聘开始时间" => "招聘开始时间",
    "招聘结束时间" => "招聘结束时间",
    "完成时间" => "完成时间",
    "办公地点地址" => "办公地点地址",
    "北京市朝阳区望京街道方恒国际中心A座北京方恒假日酒店" => "北京市朝阳区望京街道方恒国际中心A座北京方恒假日酒店",
    "预算范围" => "预算范围",
    "不显示" => "不显示",
    "显示" => "显示",
    "开始" => "开始",
    "结束" => "结束",
    "股份" => "股份",
    "总股份" => "总股份",
    "发放" => "发放",
    "股份追加说明" => "股份追加说明",
    "合同" => "合同",
    "外主图" => "外主图",
    "内主图" => "内主图",
    "项目等级" => "项目等级",
    "项目类型" => "项目类型",
    "分享得分" => "分享得分",
    "无" => "无",
    "有" => "有",
    "功勋" => "功勋",
    "weekly_limit" => "每周限制",
    "技能" => "技能",
    "职位" => "职位",
    "数量" => "数量",
    "新增职位" => "新增职位",
    "新增图" => "新增图",
    "选图" => "选图",
    "更改" => "更改",
    "删除" => "删除",
    "请选择技能" => "请选择技能",

    "修改官方项目" => '修改官方项目',
    "修改-官方项目" => '修改 官方项目',

    "官方项目管理" => "官方项目管理",
    "crud" => "新增/编辑/删除 官方项目",
    "官方项目列表" => "官方项目列表",
    "新增时间" => "新增时间",
    "操作" => "操作",
    "项目名称" => "项目名称",
    "过滤" => "过滤",
    "重置" => "重置",
    "简介" => "简介",
    "提交" => "提交",

    "资金阶段" => "资金阶段",
    "种子投资" => "种子投资",
    "天使投资" => "天使投资",
    "A轮前" => "A轮前",
    "A轮" => "A轮",
    "B轮" => "B轮",
    "C轮" => "C轮",
];