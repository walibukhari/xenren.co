<?php

return [
	'banner_span' => '神人资策室',
	'banner_h1' => '联系我们',
	'hongkong' => '香港',
	'legends_lair_limited' => '神人股份有限公司',
	'service_line' => '服务热线',
	'service_line_n' => '+00852-81209538',
	'address' => '地址',
	'address_t' => '香港九龙旺角弥敦道707-713号银行高国际大厦9楼A30室',
	'china_shen_zhen' => '中国 深圳',
	'china_legends_lair_limited' => '神人网络信息（ 深圳）有限公司',
	'china_service_line_n' => '+0755-23320228',
	'china_address_t' => '深圳宝安区侨民新村26-28 宝金华大厦1105室',
	'malasyia' => '马来西亚',
	'malasyia_legends' => '神人科技有限公司',
	'malasyia_service_line_n' => '+6012859699',
	'malasyia_address_t' => 'Lot L2.09, 2nd Floor, Shaw Parade. Jalan Changkat Thambi <br> Dollah, 55100. WP Kuala Lumpur'
];
