<?php
/**
 * Created by PhpStorm.
 * User: khegiw
 * Date: 01/12/2016
 * Time: 18:33
 */

return [
    "新增项目" => "新增项目",
    "新增" => "新增",
    "项目名称-中文" => "项目名称（中文）",
    "项目名称-英文" => "项目名称-英文",
    "背景颜色" => "背景颜色",
    "字体颜色" => "字体颜色",
    "标志" => "标志",

    "项目" => "项目",
    "name" => "Name",
    "category" => "Category",
    "description" => "Description",
    "attachment" => "Attachment",
    "pay-type" => "Pay Type",
    "pay_by_the_hour" => "Pay by the hour",
    "pay_a_fixed_price" => "Pay by the hour",
    "more_than_30_hrs_week" => "More than 30 hrs/week",
    "less_than_30_hrs_week" => "Less than 30 hrs/week",
    "dont_know_yet" => "I don't know yet",

    "anyone_can_find_and_apply_to_this_job" => "Anyone can find and apply to this job",
    "only_upwork_users_can_find_this_job" => "Only Upwork users can find this job",
    "only_freelancers_i_have_invited_can_find_this_job" => "Only freelancers I have invited can find this job",

    "question" => "Questions",
    "crud" => "新增/编辑/删除 项目",
    "项目列表" => "项目列表",

    "项目名称" => "项目名称",
    "新增时间" => "新增时间",
    "操作" => "操作",
    "开始" => "开始",
    "结束" => "结束",
    "过滤" => "过滤",
    "重置" => "重置"


];