<?php

return array(
    'failed' => '用户名或密码错误。',
    'throttle' => '您尝试登录的次数过多. 请 :seconds 秒后再试。',
    'qq_open_id_exists' => '该QQ帐号已被绑定',
    'weibo_open_id_exists' => '该微博帐号已被绑定',
    'weixin_open_id_exists' => '该微信帐号已被绑定',
    'please_enter_your_new_password_below' => '请在下面输入您的新密码'
);
