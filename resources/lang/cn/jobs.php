<?php
/**
 * Created by PhpStorm.
 * User: khegiw
 * Date: 05/12/2016
 * Time: 07:36
 */

return [

    "you_already_apply_this_job_before" => "您之前已经申请这个工作",
    "you_are_not_allow_to_apply_because_you_are_project_creator" => "您不能申请，因为您是项目创建者。",
    "listing" => "工作列表",
    "apply_job" => "申请工作",
    "make_offer" => "报价",
];