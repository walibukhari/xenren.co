<?php

return [
	'banner_h1' => 'Cooperate with us',
	'banner_p' => 'Cooperate wins the match',
	'read_more' => 'Read More',
	'contact_us' => 'Contact Us',

	'title1' => '<span>Incubator</span> Franchising',
	'title2' => '<span>Project</span> Cooperate',
	'title3' => '<span>Media</span> Cooperate',
	'title4' => '<span>Technology</span> Cooperate',

	'description1' => '<p>We welcome Property Owners to join our incubator plan. If you have an extra office space, use it to earn more. We invite you to become our technology incubator partner and in return, we offer rental and percentage revenue share from our software project Period</p>						<p>We will develop the office space as our Project Development hub. We can use the place as a podium for our Platform Project Presentation Event, and can also utilize the office space to accommodate the freelancers on rent.</p>
						<p>We are looking for the potential office spaces in the following areas:</p>
						<ul>
							<li>Shen Zhen in China</li>
							<li>Hong Kong</li>
							<li>Kuala Lumpur in Malaysia</li>
							<li>Singapore</li>
							<li>Sydney in Australia</li>
						</ul>
						<p>Please contact us for more detail.</p>',
	'description2' => '<p>Do not settle for mediocrity for you deserve the best for your software experience. Choose a pro Software Cooperate to kindly assist and collaborate with you in your software problem and other wide variety of internet-related concerns. With the high-caliber manpower, we provide affordable cost with quality services that compare to market.</p>
						<p>Visit us now.</p>
						',
	'description3' => '<p>We are looking for media partners to help us promote the project news in the modern platform. We can work together and release quality news-related project. Contact us and find out the benefits.</p>',
	'description4' => '<p>At a next stage, XENREN Platform is looking for the “Screen Monitor” as the “Time Log Tool” and “VR Reality System” for Project Management. If your company has available reality technology, you are welcome to join us as a part of our project and help us to speed up project milestone.</p>
						<p>Please contact us for more details:</p>
						',

	'more_cooperate_recommend' => 'More Cooperative Recommendation',
	'your_name' => 'Your Name',
	'your_email' => 'Your email',
	'text' => 'How can we work together, More Cooperative Recommendation',
];