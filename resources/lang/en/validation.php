<?php

return array(
    'accepted' => 'The :attribute must be accepted.',
    'active_url' => 'The :attribute is not a valid URL.',
    'after' => 'The :attribute must be a date after :date.',
    'alpha' => 'The :attribute may only contain letters.',
    'alpha_dash' => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_space' => 'The :attribute may only contain letters, numbers, and spaces.',
    'alpha_num' => 'The :attribute may only contain letters and numbers.',
    'array' => 'The :attribute must be an array.',
    'before' => 'The :attribute must be a date before :date.',
    'select_proper_location' => "Please select proper location from given options",
    'between' =>
        array(
            'numeric' => 'The :attribute must be between :min and :max.',
            'file' => 'The :attribute must be between :min and :max kilobytes.',
            'string' => 'The :attribute must be between :min and :max characters.',
            'array' => 'The :attribute must have between :min and :max items.',
        ),
    'boolean' => 'The :attribute field must be true or false.',
    'confirmed' => 'The :attribute confirmation does not match.',
    'date' => 'The :attribute is not a valid date.',
    'date_format' => 'The :attribute does not match the format :format.',
    'different' => 'The :attribute and :other must be different.',
    'digits' => 'The :attribute must be :digits digits.',
    'digits_between' => 'The :attribute must be between :min and :max digits.',
    'email' => 'The :attribute must be a valid email address.',
    'exists' => 'The selected :attribute is invalid.',
    'filled' => 'The :attribute field is required.',
    'image' => 'The :attribute must be an image.',
    'in' => 'The selected :attribute is invalid.',
    'integer' => 'The :attribute must be an integer.',
    'ip' => 'The :attribute must be a valid IP address.',
    'json' => 'The :attribute must be a valid JSON string.',
    'max' =>
        array(
            'numeric' => 'The :attribute may not be greater than :max.',
            'file' => 'The :attribute may not be greater than :max kilobytes.',
            'string' => 'The :attribute may not be greater than :max characters.',
            'array' => 'The :attribute may not have more than :max items.',
        ),
    'mimes' => 'The :attribute must be a file of type: :values.',
    'min' =>
        array(
            'numeric' => 'The :attribute must be at least :min.',
            'file' => 'The :attribute must be at least :min kilobytes.',
            'string' => 'The :attribute must be at least :min characters.',
            'array' => 'The :attribute must have at least :min items.',
        ),
    'not_in' => 'The selected :attribute is invalid.',
    'numeric' => 'The :attribute must be a number.',
    'regex' => 'The :attribute format is invalid.',
    'required' => 'The :attribute field is required.',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values is present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'The :attribute and :other must match.',
    'size' =>
        array(
            'numeric' => 'The :attribute must be :size.',
            'file' => 'The :attribute must be :size kilobytes.',
            'string' => 'The :attribute must be :size characters.',
            'array' => 'The :attribute must contain :size items.',
        ),
    'string' => 'The :attribute must be a string.',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => 'The :attribute has already been taken.',
    'url' => 'The :attribute format is invalid.',
    'custom' =>
        array(
            'attribute-name' =>
                array(
                    'rule-name' => 'custom-message',
                ),
        ),
    'is_permission_tag' => 'Invalid permission selected',
    'not_email' => 'The :attribute must not be a valid email address',
    'is_password' => 'The :attribute must be a valid password(atleast 6 characters)',
    'is_number' => 'The :attribute must be number only',
    'is_fund' => 'The :attribute is not a valid fund',
    'is_project_level' => 'Invalid project level',
    'is_project_type' => 'Invalid project type',
    'is_lang' => 'Invalid Language',
    'is_skill_level' => 'Invalid Skill Level',

    /*Coworker list space*/
    'company_required' => 'Company name field is required',
    'location_required' => 'Please enter location',
    'image_required' => 'Please upload image',
    'name_required' => 'Name field is required',
    'phone_required'=> 'Phone number filed is required',
    'email_required'=> 'Email field is required',
    'images_limit'=> 'you cannot upload more than 6 images',
    'success_listspace' => 'Your Space Listed Successfully',

    /*Coworker space book now*/
    'full_name_enter' => 'Please enter your full name',
    'phone_enter' => 'Please enter phone number',
    'select_checkin' =>'Please select check in date',
    'select_checkout' =>'Please select check out date',
    'category_id' =>'Please select facility',
    'no_of_rooms' => 'Please enter number of rooms',
    'no_of_people' => 'Please enter number of people',
    'remark_required' => 'Remarks Field is required',
    'office_not_complete' => 'The office has not been setup completely',

    /*Coworker space contact message popup*/
    'name_enter' => 'Please enter your name',
    'enter_email' => 'Please enter your email',
    'enter_subject' => 'Please enter subject',
    'enter_message' => 'Please enter message',

    /*Coworker space write a review popup*/
    'fill_stars' => 'Please fill stars',
    'comment_required' => 'Please give your comment',

    // login custom error message
    'phone_or_email' => 'Phone or Email is not valid!',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => array(
        'country_id' => 'Country',
        'about_me' => 'About Me',
        'reason' => 'Reason',
        'english_proficiency' => 'English Proficiency',
        'title' => 'Title',
        'description' => 'Description',
        'receiver' => 'Receiver',
        'message' => 'Message',
        'chat_room_id' => 'Chat Room ID',
        'url' => 'Url',
        'file_upload' => 'File Upload',
        'job_position' => 'Job Position',
        'hidUploadedFile' => 'File',
        'email' => 'Email',
        'password' => 'Password',
        'password_confirmation' => 'Password Confirmation',
        'CaptchaCode' => 'Captcha Code',
        'real_name' => 'Real Name',
        'id_card_no' => 'ID Card No',
        'gender' => 'Gender',
        'address' => 'Address',
        'date_of_birth' => 'Date Of Birth',
        'handphone_no' => 'Handphone No',
        'id_image' => 'ID Image',
    ),
);

