<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Texts Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'text-block-1' => 'Xenren  platform  provide  best  freelancer  for  all  phase  of  development  with  having  a team of skillful developers.   We have core team to protect customer project will be develop in required quality. With official skill we help employer verify freelancer’s talent true or false. ',
    'next'     => 'Next &raquo;',

];
