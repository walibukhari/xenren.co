<?php

return array(
    'managecoin' => 'Managed Coin',
    'crud'=> 'Managment',
    'dashboard'=>'Dashboard',
    'add_coin'=>'Add Coin',
    'coins'=>'Coins',
    'coin'=>'Coin',
    'coin_type'=>'Coin Type',
    'amount'=>'amount',
    'action'=>'Action',
    'create_coin'=>'Create Coin',
    'msg1'=>'Fill in the form to add a coin amount',
    'msg2'=>'Fill in the form to edit data',
    'save'=>'Save new coin amount',
    'coinchange'=>'Coin Edit',
    'edit'=>'Save changes',
);
