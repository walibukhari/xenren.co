<?php

return array(
    'crud' => 'add/edit/delete raw material',
    'id' => '身份证',
    '修改原材料' => 'edit raw material',
    '原材料列表' => 'raw material list',
    '原材料名称' => 'raw material name',
    '原材料名称-中文' => 'raw material name (Chinese)',
    '原材料名称-英文' => 'Raw material name (English)',
    '原材料管理' => 'raw material management',
    '开始' => 'start',
    '操作' => 'operate',
    '新增' => 'add',
    '新增原材料' => 'add raw material',
    '新增时间' => 'add time',
    '结束' => 'end',
    '过滤' => 'filter',
    '重置' => 'reset',
    '银行名称' => 'bank name',
);
