<?php

return array(
    'sharedofficechat'=>'shared office chat',
    'mile_stone'=>'MileStone',
    'add_topics'=>'Add Topics',
    'manage_posts'=>'Manage Posts',
    'reports'=>'Reports',
    'users'=>'Users',
    'invoices'=>'Invoices',
    'change_booking_request'=>'Change Booking Request',
    'logout'=>'Logout',
    'coins'=>'Coins',
    'article'=>'Article',
    'finance'=>'Finance',
    'reset_user'=>'Reset User',
    'push_notification'=>'Push Notification',
    'user_activity'=>'User Activity',
    'cate'=>'Categories',
    'set_phone'=>'SetUp Phone',
    'pend_manage'=>'Pending Withdrawals',
    'iden_manage'=>'Identity Management',
    'management'=>'Management',
    'bad_filter'=>'Badwords Filter',
    'community'=>'Community',
    'change_release_date'=>'Change Release Date',
    '首页' => 'Homepage',
    'article' => 'Article',
    'dashboard' => 'Dashboard',
    'working_sheet' => 'Working Sheet',
    'withdraw_requests' => 'Withdraw Requests',
    '操作员' => 'Operator',
    'create_article' => 'Create',
    '权限组' => 'Permission group',
    '会员' => 'Member',
    'check_in_user_sharedOffice'=>'Shared Office Check In',
    'booking_request' => 'booking Requests',
    '类别管理' => 'Category management',
    '技能管理' => 'Skill management',
    '技能关键字管理' => 'SEO management',
    '主职管理' => 'Main Management',
    '官方项目管理' => 'Official event management',
    'themes_panel' => 'Themes Panel',
    '后台' => 'Background',
    '即将推出' => 'Coming soon',
    '后台首页' => 'Background page',
    '水平管理' => 'Level managemet',
    '看板' => 'Panel',
    'hiring_request'=>'Hiring Requests',
    '银行管理' => 'Bank management',
    '技能认证' => 'skill verified',
    '项目管理' => 'project management',
    '原材料管理' => 'Raw Material Management',
    '提交个人身份管理' => 'Submit ID Management',
    'feedback_management' => 'Feedback Management',
    'inbox' => 'Inbox',
    'language_management' => 'Language Management',
    'country_management' => 'Country Management',
    'country_language_management' => 'Country Language Management',
    'member_management' => 'Member Management',
    'shared_office'=> 'Shared Office',
    'coins_manage'=> 'Coins Management',
    'user_coins_manage'=> 'User Coins',
    'user_feedback'=> 'User Feedback',
    'dispute_requests'=> 'Dispute Requests',
    'milestone_confirmation'=> 'Milestone Confirmation',
    'shared_office_request' => 'Shared office request',
    'sharedofficebarcodes' => 'Shared Office Barcodes',
        'sharedofficeprofileupdate' => 'Owner Profile',
        'sharedofficeNotifications' => 'Notifications',
        'sharedofficeOwnerEdit' => 'Shared Office Edit',
    'sharedofficeOwnerProducts' => 'Shared Office Product',
    'sharedofficeOwnerAddImages' => 'Shared Add Images',
    'coin' => 'Coin',
    'give_coin' => 'Give Coin',
    'coin_list' => 'Coin List',
    'office_request_owner' => 'Shared Office Requests',
);
