<?php

return array(
    'name_required' => 'Name required.',
    'lastname_required' => 'Last name required.',
    'email_required' => 'Email required.',
    'validate_email' => 'Email must be a valid email address.',
    'password_required' => 'Password required.',
    'password_confirmation_required' => 'ReType password required.',
    'remember_required' => 'Sign the agreement before to continue.',
);

