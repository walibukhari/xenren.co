<?php

return array(
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'qq_open_id_exists' => 'The QQ account is registered',
    'weibo_open_id_exists' => 'The Weibo account is registered',
    'weixin_open_id_exists' => 'The Weixin account is registered',
    'please_enter_your_new_password_below' => 'Please enter your new password below'
);
