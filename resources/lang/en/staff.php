<?php

return array(
    "用户名" => "Username",
    "电子邮件地址" => "Email Address",
    "姓名" => "Name",
    "权限组" => "Permission Group",
    "密码" => "Password",
    "确认密码" => "Confirm Password",
    "新增操作员" => "Operator List",
    "新增" => "Add",
    "职称" => "Job Title",
    "修改操作员" => "Edit Operator",
    "修改" => "Edit",
    "如不修改密码请留空" => "Please leave empty if don't want it to change",
    "头像" => "Avatar",
);
