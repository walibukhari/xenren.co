<?php

return array(
    '新增银行' => 'Add bank',
    '新增' => 'Add',
    'badge' => 'Badge',
    'level' => 'Level',
    'desc' => 'Add/edit/delete bank',
    '修改' => 'Change',
    '修改银行' => 'Change bank',
    '后台' => 'Background',
    '操作' => 'Operation',
    '新增水平' => 'Add level',
    '水平管理' => 'Level management',
    '银行管理' => 'Bank management',
);
