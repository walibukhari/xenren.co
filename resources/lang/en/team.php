<?php

return [
	'banner_span' => 'Legends Lair',
	'banner_h1' => 'Meet Our Developement Team',
	'title1'=>'<span>Create</span> Website',
	'desc1'=>'<p>Legends Lair strive to develop websites that are secure, robust and fully capture the needs of our clients. Our team also plays a key role in enhancing web experiences by designing and building applications that help our clients benefit extensively from web technologies. We design numerous websites that include Wechat HTML 5 which is the most common application in China. In addition, we design commercial as well as e-commerce sites and open source websites such as WordPress among others. </p>
			  <p>All our customers benefit from professional advice related to technology and how it can benefit your business. In addition, we advise you on sustainable and profitable business models so long as you send us your requirements.</p>',
	'title2'=>'<span>We Create</span> MOBILE APPS',
	'desc2'=>'<p>The world has changed with new technologies taking shape in the market. In modern times, software plays a crucial role as it helps us improve on time efficiency both within our company as well that of our clients. Mobile technologies are the in-thing and at Legends Lair, we’ve not been left behind as we’re constantly helping our clients to harness this technology. </p>
			  <p>Mobile gadgets have numerous tools such as GPS and other cool applications that are highly beneficial and exciting for customers. Our mobile expertise is unmatched as our company owns Mobile apps, VR as well as AR technology. We have a defined software development life cycle process and methodologies that we adhere to ensure all our software is produced in a professional manner. Today, Mobile Apps are not only used for commercial purposes only, companies can use them to cut costs. </p>
			  <p>We charge a reasonable maintenance fee and ensure your user database is always safe and protected. Our customers are important to us so we offer quick and timely support to ensure any technical hiccups are quickly resolved. If you’re looking for software development experts, Legends Lair Team is your best choice; you can be assured of impeccable service. </p>',
	'title3'=>'<span>Our</span> Technology',
	'java' => 'Java',
	'laravel' => 'Laravel',
	'ios' => 'iOS',
	'android' => 'Android',
	'last_desc' => '<p>We are a Technology base enterprise that consists of a wide range of seasoned professionals. We have a dedicated team of different types of programmers drawn from Hong Kong, China and Malaysia. Our programmers are skilled with vast developer knowledge and experience in VR and AR technologies. We cooperate with talented freelancers，to provider more variety of technology . Our Marketing Support team brings together professionals with experience in UI designing to create cutting edge software that is relevant in today’s market.</p>',
	'title4'=>'<span>CORE TEAM OF </span> LEGENDS LAIR',
	'core_team_position1' => 'Software Architecture',
	'core_team_position2' => 'iOS Developer',
	'core_team_position3' => 'Team Co-founder',
	'core_team_position4' => 'UI Designer',
	'core_team_position5' => 'Android Developer',
	'core_team_position6' => 'Senior Sales excecutive',
	'core_team_name1' => 'FONG<br> PAU FUNG',
	'core_team_name2' => 'DANNY<br> Ho',
	'core_team_name1_h2' => 'FONG PAU FUNG',
	'core_team_name2_h2' => 'DANNY Ho',
	'title5'=>'<span>DEVELOPER TEAM OF </span> LEGENDS LAIR'
];