<?php

return [
    'banner_h1' => 'We are <span style="color: #58AF2A;"> Hiring</span> now!',
    'banner_p' => 'Welcome To Join Us',
    'read_more' => 'Read More',
    'apply_position' => 'Apply for this position',
    'apply' => 'Apply',
    'title' => 'Join our <span>Team</span>',
    'section1_title' => 'Sales representative',
    'section1_desc' => '<p>Are you up for challenging jobs? We welcome all fiery and talented sales people to join us in our exciting endeavour. We at <span>Legend Lairs</span> are looking for passionate young people who are willing to enhance their horizon of internet/software knowledge and eager to learn more.</p>
                        <p>If you are witty and know how to talk, if you are confident about your communication skill, if you are daring to sell IT products, we help to add thrill into your life with our exciting sales job. </p>
                        <p>You will learn the comparative analysis of our products in the Australia and the Chinese markets and able to provide professional help and valuable information to our clients. We will back up you to enhance the percentage of closing sales with our strong IT background, own products, and constant patronage.</p>',
    'section2_title' => 'Software Engineer',
    'section2_desc' => '<p>We welcome experienced and enthusiastic IT professional to join our technical drive. If you love to chase success, daring for new challenges, have a hunger to learn new skills and framework, we are waiting with open arms to incorporate you with us.</p> 
                        <p>We are looking for coding geniuses who are familiar with project management tools or GitHub. If you are a programmer and open to speak your mind during technological hitch and eager to help our clients with smart solutions, if you respect the importance of timely completion of the professional project, you are the one perfect for us.</p>
                        <p>Our product manager will communicate and help you to get familiar with the workflow and every coding. We help you enhance your work efficiency and handling challenging jobs with perfection and timely completion.</p>',
    'section3_title' => 'Product Manager',
    'section3_desc' => '<p>If you are an experienced project manager with the knowledge of the use of mockup tools and project structure, we welcome you to work with us. We are looking for the dynamic and innovative project managers who are passionate about technology field.</p>
                        <p>If you have foreseen an amazing software product and dying to create it to astound the global market, feel free to join us to convert your dream into reality. Our programmers and designers communicate and help you to streamline the designed work. You will learn the proper documentation and updating of the project</p>
                        <p>Apply for the position if challenges excite you and learning new things inspire you to perform excel.</p>',
    'section4_title' => 'Online Marketer',
    'section4_desc' => '<p>We are calling techno-savvy, ready to learn and experiment new things and self-motivated online marketer to join us to add thrill in their life. </p>
                        <p>If you have the understanding of product advantage if you are a wordsmith and smart to grasp the market psychology, if your hunger for knowledge does not let you sleep tight at night, feel free to join us to take up new challenges.</p>
                        <p>We help you to learn internet knowledge, new studies and market research in the technology field and you will sharpen the online promoting as well as optimize promoting skills under our arcade. </p>',
    'select_position' => 'Select Position'
    ];