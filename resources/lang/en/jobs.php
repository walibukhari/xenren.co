<?php
/**
 * Created by PhpStorm.
 * User: khegiw
 * Date: 05/12/2016
 * Time: 07:36
 */

return [

    "you_already_apply_this_job_before" => "You already apply this job before",
    "you_are_not_allow_to_apply_because_you_are_project_creator" => "You are not allow to apply because you are project creator.",
    "listing" => "Job Listing",

    "apply_job" => "Apply Job",
    "make_offer" => "Make Offer",
];