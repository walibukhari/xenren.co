<?php
/**
 * Created by PhpStorm.
 * User: khegiw
 * Date: 27/12/2016
 * Time: 19:23
 */

return [
    'title' => "Level Translations",
    'desc' => "Add/Remove/Delete Level Translations",
    'name_cn'=> 'Name CN',
    'name'=> 'Name',
    'name_en'=> 'Name EN',
    'code_name'=> 'Code Name',
    'experience'=> 'Experience',
];