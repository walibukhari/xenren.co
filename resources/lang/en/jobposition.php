<?php

return array(
    '操作' => 'Operation',
    '新增' => 'Add',
    '新增时间' => 'Add time',
    '结束' => 'Finish',
    '过滤' => 'Filter',
    '重置' => 'Reset',
    'desc' => 'Add/Edit/Delete Job',
    '主职列表' => 'Job list',
    '主职名称' => 'Job name',
    '主职名称-中文' => 'Job name ( Chinese)',
    '主职名称-英文' => 'Job name (English)',
    '主职管理' => 'Job management',
    '主职说明-中文' => 'Job description ( Chinese) ',
    '主职说明-英文' => 'Job description (English)',
    '修改' => 'Change',
    '修改主职' => 'Change job',
    '后台' => 'Background',
    '开始' => 'Start',
    '新增主职' => 'Add job',
);
