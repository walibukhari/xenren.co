<?php

return [
	'banner_span' => 'Have any Problem?',
	'banner_h1' => 'Contact Us',
	'hongkong' => '<span>HONG</span> KONG',
	'legends_lair_limited' => 'Legends Lair Limited',
	'service_line' => 'SERVICE LINE',
	'service_line_n' => '+00852-81209538',
	'address' => 'ADDRESS',
	'address_t' => 'FLAT/RM A30.9/F SILVERCORP INTERNATIONAL TOWER, <br> 707-713 NATHAN ROAD. MONGKOK, KOWLOON,HONG KONG',
	'china_shen_zhen' => '<span>CHINA</span> SHEN ZHEN',
	'china_legends_lair_limited' => 'Legends Technology Shen Zhen Pvt Ltd',
	'china_service_line_n' => '+0755-23320228',
	'china_address_t' => 'Shen Zhen Bao An Xiang street 1105 Jing Hua Building',
	'malasyia' => '<span>MALA</span>YSIA',
	'malasyia_legends' => 'Legends Technology',
	'malasyia_service_line_n' => '+6012859699',
	'malasyia_address_t' => 'Lot L2.09, 2nd Floor, Shaw Parade. Jalan Changkat Thambi <br> Dollah, 55100. WP Kuala Lumpur'
];
