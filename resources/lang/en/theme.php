<?php
/**
 * Created by PhpStorm.
 * User: khegiw
 * Date: 03/12/2016
 * Time: 07:28
 */

return [
    "crud" => "View Sliders/Update Sliders",
    "add_slider" => "Add Slider",
    "projects_panel" => "Projects Panel",
    "main_page_slider" => "Main Page Slider",
    "main_page_news" => "Main Page News",
    "fill_in_the_form_to_add_a_slider" => "Fill in the form to add a slider",
    "fill_in_the_form_to_edit_a_slider" => "Fill in the form to edit a slider",
];