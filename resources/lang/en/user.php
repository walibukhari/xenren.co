<?php
/**
 * Created by PhpStorm.
 * User: Arslan Shahid
 * Date: 06/09/2020
 * Time: 02:10 PM
 */

return [
    "确认密码" => "确认密码",
    "编辑会员" => "编辑会员",
    "密码" => "密码",
    "如不更改密码请留空" => "如不更改密码请留空",
    "crud" => "新增/编辑/删除 会员",
    "水平" => "水平",
    "会员" => "会员",
    "电子邮件地址" => "电子邮件地址",
    "personal_info" => "Personal Info",
    "sign_out" => "Sign Out",
    "取消" => "取消",
    "确定" => "确定"
];
