<?php

return [
	'banner_span' => 'Legends Lair',
	'banner_h1' => 'Meet Our Official Sales Team',
	'title1'=>'<span>Create</span> Website',
	'desc1'=>'<p>Legends Lair strive to develop websites that are secure, robust and fully capture the needs of our clients. Our team also plays a key role in enhancing web experiences by designing and building applications that help our clients benefit extensively from web technologies. We design numerous websites that include Wechat HTML 5 which is the most common application in China. In addition, we design commercial as well as e-commerce sites and open source websites such as WordPress among others. </p>
			  <p>All our customers benefit from professional advice related to technology and how it can benefit your business. In addition, we advise you on sustainable and profitable business models so long as you send us your requirements.</p>',
	'title2'=>'<span>We Create</span> MOBILE APPS',
	'desc2'=>'<p>The world has changed with new technologies taking shape in the market. In modern times, software plays a crucial role as it helps us improve on time efficiency both within our company as well that of our clients. Mobile technologies is the new trend, we’ve not been left behind as we’re constantly helping our clients to harness this technology. </p>
			  <p>Mobile gadgets have numerous tools such as GPS and other cool applications that are highly beneficial and exciting for customers. Our mobile expertise is unmatched as our company owns Mobile apps, VR as well as AR technology. We have a defined software development life cycle process and methodologies that we adhere to ensure all our software is produced in a professional manner. Today, Mobile Apps are not only used for commercial purposes only, companies can use them to cut costs. </p>
			  <p>We charge a reasonable maintenance fee and ensure your user database is always safe and protected. Our customers are important to us so we offer quick and timely support to ensure any technical hiccups are quickly resolved. If you’re looking for software development experts, Legends Lair Team is your best choice; you can be assured of impeccable service. </p>',
	'title3'=>'<span>Our</span> Technology',
	'java' => 'Java',
	'php' => 'PHP',
	'ios' => 'iOS',
	'android' => 'Android',
	'my_sql' => 'MySQL',
	'hadoop' => 'Hadoop',
	'github' => 'GitHub',
	'html_css_js' => 'HTML | CSS | JS',
	'point1_title' => 'Business Representative',
	'point1_desc' => '<p>Legends Lair limited was established in 2014 in Hong Kong, technology set up in Malaysia. The team has a multi-cultural background, proficient in English.</p>',
	'point2_title' => 'Software projects to undertake',
	'point2_desc' => '<p>The motivation behind software development is for profit, so the whole business logic of the software is critical.</p>
					<p>From the business model, the decision function. From the company planning, decision frame.</p>	
					<p>This is necessary to open the process to help customers detect whether the logic of the business model is ignored, and whether the budget to complete the functional functions can be built up.</p>	
					<p>From the company planning decision framework, the general start-up companies may not afford high-end framework, such as: Laravel, the original front-end, Java J2EE and so on. We can recommend CMS, or some open source code. </p>',
	'point3_title' => 'Why choose us?',
	'point3_desc' => '<p>Legends Lair team has its own core technology, technical and cultural management, coupled with the long-term accumulation of domestic and foreign professionals list, customer demand for comprehensive coverage of technology. </p>
					<p>Years of development experience tells us: professional people in the shortest possible time, to make the best results.</p>',
	'point4_title' => 'We have not only technology',
	'point4_desc' => '<p>In addition to software, we are also concerned about the landing of the user experience. In accordance with company planning, to develop a reasonable user experience. Both the Chinese market, the Southeast Asian market, and even Europe and the United States, India. We have the right designers to create the most suitable for your local market software</p>
					<p>Including domestic and foreign payment channels, enterprise architecture, equity planning. We have a wealth of experience.</p>',
	'contactus' => '<span>CONTACT</span> US',
	'contactus_n' => '0755-23320228',					
	'hotline' => 'Hotline 14 x 7',
	'team1_position' => 'Software Architecture',
	'team1_name' => 'Fong Pau Fung',
	'team1_email' => 'fong@xenren.co',
	'team1_number' => '+86 13059135801 +0128509699',
	'team1_add' => 'China Shen Zhen Bao An Xi Xiang Street Jing Hua Building 1105',
	'team2_position' => 'Senior Sales excecutive',
	'team2_name' => 'Lina Chieng',
	'team2_email' => 'lina@xenren.co',
	'team2_number' => '+60168710081',
	'team2_add' => 'Lot L2.09, 2nd Floor, Shaw Parade. Jalan Changkat Thambi 
					Dollah, Kuala Lumpur, 55100. Kuala Lumpur, WP Kuala Lumpur',
	'team3_position' => 'East malaysia Sales Manager',
	'team3_name' => 'Hee Wei Seng',
	'team3_email' => 'hws@xenren.co',
	'team3_number' => '+60168996337',
	'team3_add' => 'Lot L2.09, 2nd Floor, Shaw Parade. Jalan Changkat Thambi 
					Dollah, Kuala Lumpur, 55100. Kuala Lumpur, WP Kuala Lumpur',
	'title4' => '<span>SALES TEAM OF</span> LEGENDS LAIR'
];