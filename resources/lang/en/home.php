<?php

return [
    'all_reversed' => 'Legends Lair Limited. All Rights Reserved.',
    'agreement?' => 'Home agreement?',
    'title' => 'Xenren.co - Find the Coworkers & Coworking Spaces.',
    'description' => 'Find the Coworkers & Coworking Spaces. Find the talented people. Both Co-Work Space and your favourite jobs in XENREN platform.',
    'keywords' => 'XENREN-Feelance platform,entrepreuneur platform, Internet candidates (Page)',
    'xenren' => 'Xenren',
    'support' => 'Support',
    'co_woker' => 'Co-Worker',
    'company_name' => 'Legends Lair Limited',
    'company_register_id' => 'Legends Lair | Company Register No:2099572',
    'register_id' => 'Company Register No:2099572',
    'social_network' => 'Social Network',
    'company_email' => 'Customer Support：support@xenren.co',
    'company_tel' => 'Service line：+00852-81209538',
    'company_business_hour' => 'Working Hour：00:00-24:00',
    'company_adress' => 'Address：FLAT/RM A30,9/F SILVERCORP INTERNATIONAL TOWER, 707-713 NATHAN ROAD, MONGKOK, KOWLOON,HONG KONG',
    'copyright' => 'Legends Lair Limited © 2017 - 2020.',
    'about_us' => 'About us',
    'contact_us' => 'Contact us',
    'cooperate_link' => 'Cooperate Link',
    'landing_page' => 'Landing Page',
    'hiring_notice' => 'Hiring notice',
    'tnc' => 'Term and condition',
    'privacy_policy' => 'Privacy policy',
    'location' => 'Location',
    'customer_hotline' => 'Cutomer Hotline',
    'project' => 'Project',
    'explore_now' => 'Explore Now',
    'legends_lair' => 'Legends Lair',
    'hall_of_talent' => 'Hall of talent',
    'meet_team' => 'Meet Our Amazing Team',
    'meet_our_team' => 'Meet Our Team',
    'designer' => 'UX/UI designer ',
    'ui_designer' => 'UI Designer',
    'co_owner' => 'CEO / CO-Owner',
    'content_writer' => 'Content Writer',
    'web_developer' => 'Web Developer',
    'our_team' => 'Our Team',
    'team_fong' => 'Fong Pau Fung',
    'team_Xenren' => 'Xenren',
    'team_chenlou' => 'Chen Lou',
    'team_chenkun' => 'Chen Kun',
    'team_CEO' => 'Chief Executive Officer',
    'team_CTO' => 'Chief Technology Officer',
    'team_CPO' => 'Chief Product Officer',
    'team_CCO' => 'Chief Creative Officer ',
    'team_desc' => 'Xenren Platform was built to connect skillful freelancers to entrepreneurs’ projects. We help in verifying and selecting skilled and talented freelancers and connecting them with project developers for purposes of developing outstanding projects.',
    'entrepreneur' => 'Entrepreneur',
    'Technicalstaff' => 'Technical staff',
    'hr' => 'Human Resource Manager',
    'freelancer' => 'Freelancer',
    'people_helen' => 'Helen',
    'people_xubing' => 'Xu Bing',
    'people_khairul' => 'Khairul',
    'people_MrZhang' => 'Mr.Zhang',
    'people_MsLi' => 'Ms.Li',
    'people_MsWang' => '',
    'people_helen_say' => 'This platform helped me in finding the suitable co-founders.',
    'people_xubing_say' => 'It help me found the right worker',
    'people_khairul_say' => 'I have found my project from this platform',
    'people_MrZhang_say' => 'The platform not only gives young people opportunities to find jobs, but also gives them opportunities to learn. And the platform is indeed a platform that is relatively stable compared to today’s Internet. I support God’s people and hope that in the current Internet age God is getting better and better.',
    'people_MsLi_say' => 'The platform has given more opportunities to young people. It may be obvious to all of you. The biggest beneficiary is us. So for us, the opportunities presented by the platform to us are also quite large, and it gives us more exposure to practical projects. The chances are that the platform will be able to persevere and allow more young people to have access to practical projects.',
    'people_MsWang_say' => '',
    'platform_desc' => 'Xenren  platform  provide  best  freelancer  for  all  phase  of  development  with  having  a team of skillful developers.   We have core team to protect customer project will be develop in required quality. With official skill we help employer verify freelancer’s talent true or false.',
    'services_security' => 'Services and Security',
    'money_guarantee' => 'Money guarantee',
    'skill_verify' => 'Official Skill verifying',
    'tech_support' => 'Experience technical support',
    'customer_service' => 'Customer service',
    'skillfull_person' => '10000+ of Skillfully person',
    'money_guarantee_text' => 'We endeavor to guarantee your financial security in all your online dealings. We have put in the place convenient systems of payment favorable to both clients and freelancers.',
    'skill_verify_text' => 'We have put in place well structured methods of skill verification for purposes of ensuring that we only bring on board skilled and qualified freelancers.  All freelancers go through a number of tests to prove their skills and only those who pass are selected.  Mock tests are also available to help prepare freelancers for the real tasks.',
    'tech_support_text' => 'We have dedicated and round the clock technical personnel ready to offer any assistance.  We are always determined to solve any technical problem in the shortest time possible.',
    'customer_service_text' => 'We give our best effort to satisfy customer. Please contact us and we will give our best effort to satisfy them.',
    'skillfull_person_text' => '10000 plus skillful persons around the world. The number is growing day by day. These people have taken all the responsibility to help you, so join us and explore!!',
    'people_say' => 'What People Say About Us',
    'quick_start' => 'Quick Start',
    'create_post' => 'Create Post',
    'get_started' => 'Get <br> Started',
    'network' => 'Network',
    'home' => 'Home',
    'jobs' => 'Jobs',
    'project_post' => 'Project Post',
    'register' => 'Register',
    'login' => 'Login',
    'sitemap' => 'Sitemap',
    'services' => 'Services',
    'escrow' => 'Escrow',
    'about_xenren' => 'About XENREN',
    'careers' => 'Careers',
    'terms_of_service' => 'Terms of Service',
    'meet_the_team' => 'Meet the Team',
    'additional_service' => 'Additional Service',
    'legends_lair_pro' => 'Legends Lair Pro',
    'enterprise_solutions' => 'Enterprise Solutions',
    'offline_business_cooperation' => 'Offline Business Cooperation',
    'connect_with_us' => 'Connect with US',
    'contact_support' => 'Contact & Support',
    'api_center' => 'API Center',
    'mobile_app' => 'Mobile APP',
    'social_media' => 'Social Media',
    'mobile_apph' => 'Mobile App',
    'get_it' => 'GET IT ON',
    'google_play' => 'Google Play',
    'download_on_the' => 'Download on the',
    'app_store' => 'App Store',
    'customer_support' => 'Customer Support',
    'customer_support_e' => 'support@xenren.co',
    'service_line' => 'Service Line',
    'service_line_m' => '+0755-23320888',
    'working_hour' => 'Working Hour',
    'working_hour_t' => '09:00-20:00',
    'company_address' => 'Company Address',
    'company_address_s' => 'Flat AT/RM A30. 9/F Sii Vercorp International Tower. 707-713 Nathan Road. Mongkok Kowloon, Hong Kong',
    'ci_company_info' => 'Enterprise Collaboration',
    'ci_register' => 'register',
    'ci_login' => 'login',
    'ci_home' => 'home',
    'ci_about_us' => 'About Us',
    'ci_careers' => 'Careers',
    'ci_terms_service' => 'Terms of Service',
    'ci_privacy_policy' => 'Privacy Policy',
    'ci_meet_team' => 'Meet Our Team',
    'ci_meet_sales_team' => 'Meet Our Sales Team',
    'ci_contact_us' => 'Contact Us',
    'company' => 'Enterprise Collaboration',
    'ci_company_culture' => 'Company Culture',
    'terms_service_banner_p' => "LEGENDS LAIR'S",
    'terms_service_banner_h1' => 'TERMS OF SERVICE',
    'terms_service_banner_span' => 'Date of Last Revision',
    'terms_service_banner_spanDate' => 'February, 2, 2019',
    'privacy_banner_p' => "LEGENDS LAIR'S",
    'privacy_banner_h1' => 'PRIVACY POLICY',
    'privacy_banner_span' => 'Date of Last Revision',
    'privacy_banner_spanDate' => 'October 15, 2016',
    'menu' => 'Menu',
    'help_center' => 'Help Center',
    'welcome_to_customer_support' => 'Welcome to Customer Support',
    'search' => 'Search',
    'i_have_a_question_about' => 'I have a question',
    'how_can_we_help_you' => 'How can we help you?',
    'many_companies' => 'Many companies - both large and small - have faced challenges with finding',
    'elite_tech' => 'elite tech talent, including candidate qualifications, team dynamics, and',
    'bottom_line' => 'bottom-line financials. Our unique solution addresses these concerns.',
    'quick_access' => 'quick access',
    'follow_our_system_status' => 'Follow our system status',
    'See_community_discussions_and_help' => 'See community discussions and help',
    'Read_tips_on_how_to_be_successful' => 'Read tips on how to be successful',
    'send_a_request' => 'Send a Request',
    'help_make_legends_lair_better' => 'Help make Legends Lair better',
    'popular_questions' => 'Popular Questions',
    'what_cannot_sign_in?' => 'What can\'t sign in?',
    'why_was_my_bank_account_changed_automatically' => 'Why is my bank account changed automatically',
    'does_posting_a_job_cost_anything' => 'Does posting a job cost anything',
    'what_if_my_verification_amount_is_not_in_usd' => 'What if my verification amount is not in USD',
    'awaiting_credit' => 'Why is my payment "awaiting credit from bank"',
    'buyer' => 'I\'m a Buyer',
    'seller' => 'I\'m a Seller',
    'buyer_faq' => 'Buyer faq',
    'read_more' => 'Read More',
    'suggested_articles' => 'Suggested Articles',
    'what_this_article_helpful' => 'What this article helpful',
    'found_this_helpful' => 'found this helpful',
    'seller_faq' => 'Seller Faq',
    'bind_facebook' => 'Bind Facebook',
    'now_enjoy_access_in_one_click' => 'Now enjoy access in one click',
    'bind_your_account' => 'BIND YOUR ACCOUNT',
    'landing_page_description' => 'Find the Coworkers & Coworking Spaces. Find the talented people. Both Co-Work Space and your favourite jobs in XENREN platform.',
    'company_culture' => 'Company Culture',
    'searched_article' => 'Article you search.',
    'article_not_found' => 'Article not found',
];
