<?php

return [
	'banner' => '<span>Welcome to</span> LEGENDS LAIR',
    'about_us' => 'About Us',
	'f_title' => '<span>WHO</span> WE ARE',
	'f_desc' => '<p>Legends Lair Limited was established in 2014. We operate and have a market presence in Malaysia as well as China’s Hong Kong and Shen Zhen cities. Our key focus is to help talented candidates connect with employers. </p><p>In the recent times, modern technologies including the internet continue to play a key role in simplifying how recruiters around the globe connect with a skilled and reliable workforce. At Legends Lair Limited, we have laid the necessary online technical infrastructure and experts to enable employers find talented candidates via our platform.</p>',
	'mission' => '<span>OUR </span> MISSION',
	'mission_desc' => 'Mandate is to use technology to make the world a better place. We utilize technology to simplify online recruitment processes. Candidates on our platform are taken through vigorous screening and assessment processes by our recruiters to ensure employers hire only talented candidates.',
	'vision' => '<span>OUR </span> VISION ',
	'vision_desc' => 'We strive to help companies maximize internet potential to realize improved performance.  We have a genuine passion to deploy relevant solutions to help offline companies reap the full benefits of technology in this internet era.',
	'5things' => '<strong style="font-size: 38px">5</strong> THINGS <span>TO KNOW ABOUT US  </span>',
	'5things_title_1' => 'Global Enterprise',
	'5things_title_2' => 'Professional',
	'5things_title_3' => 'Rich Experience',
	'5things_title_4' => 'Revolution',
	'5things_title_5' => 'Great User Experience',
	'5things_1' => 'We operate in a global multicultural environment with our team members drawn from Malaysia, Singapore, Hong Kong, India, Vietnam and United States. ',
	'5things_2' => 'We’ve deployed superb technology platforms to facilitate quick service and improved efficiency ',
	'5things_3' => 'Our team consists of elite tech savvy professionals who have vast experience in various spheres of modern technology such as App development. Our programmers have previously worked in large reputable organisation',
	'5things_4' => 'Our in-depth and incisive understanding of technology and trends cuts us above the rest and our level of creativity and presentation of ideas is unmatched.',
	'5things_5' => 'Our marketing support is top-notch as we design intuitive and interactive interfaces that promote user experience, drive engagement and effectively market enterprises by leveraging on technology',
	'footer1_title' => 'Registered <br> Freelancers',
	'footer1_desc' => '30K',
	'footer2_title' => 'Registered <br> Clients',
	'footer2_desc' => '5K',
	'footer3_title' => 'Jobs <br> postedAnnually',
	'footer3_desc' => '$10M',
	'footer4_title' => 'Worth of Work <br> done Annually',
	'footer4_desc' => '$ 3M',
	'description' => '<p>Established in 2014 with synergy of great minds, Legends Lair Limited has three key points that make the company stand out.</p>
					<p>First, we focus on directing potential individual workers to reputable employers and sound opportunities. They believe that best workers deserve best employees.</p>
					<p>Second, we help the obsolete business styles transform into an advanced platform that effectively reaches the dynamic trends.</p>
					<p>Third, we are a highly trusted group that offers quality technology-related outsourcing and management consultation services;</p>
					<p>Xenren is a word derived from Japanese and Chinese terminology. which means “talented people”. We do not settle in mediocrity. Here, talented individuals, who are recognized to be the primary asset of a successful company operations, are greatly valued!</p>
					<p>Currently, we are a 12 member team with common mission of providing the best variety of talented workers to employers through our user-friendly platform. </p>
					<p>Indeed, change is the only thing that does not change. Our team is always ready to guide you to the brighter business and career destinations you deserve. Let’s all together take out the gaps in achieving triumphs!</p>'
];