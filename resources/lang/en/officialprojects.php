<?php
/**
 * Created by PhpStorm.
 * User: khegiw
 * Date: 29/11/2016
 * Time: 16:40
 */

return [
    "新增官方项目" => "Add Official Project",
    "desc" => "Add Official Project",
    "后台" => "Backend",
    "官方项目" => "Official Project",
    "内容" => "Content",
    "图集" => "Image",
    "项目语言" => "Project Language",
    "项目标题" => "Project Title",
    "项目简介" => "Project Description",
    "招聘开始时间" => "Recruitment Start Time",
    "招聘结束时间" => "Recruitment End Time",
    "完成时间" => "Complete Time",
    "办公地点地址" => "Office Address",
    "北京市朝阳区望京街道方恒国际中心A座北京方恒假日酒店" => "北京市朝阳区望京街道方恒国际中心A座北京方恒假日酒店",
    "预算范围" => "Budget Scope",
    "不显示" => "Don't Show",
    "显示" => "Show",
    "开始" => "Start",
		"weekly_limit" => "Weekly Limit",
    "结束" => "End",
    "股份" => "Shares",
    "总股份" => "Total Shares",
    "发放" => "Issued",
    "股份追加说明" => "Share Additional Description",
    "合同" => "Contract",
    "外主图" => "Frontend Image",
    "内主图" => "Backend Image",
    "项目等级" => "Project Level",
    "项目类型" => "Project Type",
    "分享得分" => "Share and Gain Point",
    "无" => "No",
    "有" => "Yes",
    "功勋" => "Contribution",
    "技能" => "Skill",
    "职位" => "Job Title",
    "数量" => "Quantity",
    "新增职位" => "Add Job Position",
    "新增图" => "Add Image",
    "选图" => "Select Image",
    "更改" => "Change",
    "删除" => "Delete",
    "请选择技能" => "Please Select The Skills",

    "修改官方项目" => 'Modify Official Project',
    "修改-官方项目" => 'Modify Official Project',

    "官方项目管理" => "Official Project Management",
    "crud" => "Add/Edit/Delete Official Project",
    "官方项目列表" => "Official Project List",
    "新增时间" => "Add Time",
    "操作" => "Action",
    "项目名称" => "Project Name",
    "过滤" => "Filter",
    "重置" => "Reset",
    "简介" => "Introduction",
    "提交" => "Submit",

    "资金阶段" => "Funding Stage",
    "种子投资" => "Seed",
    "天使投资" => "Angle",
    "A轮前" => "Pre-A",
    "A轮" => "A",
    "B轮" => "B",
    "C轮" => "C",
];