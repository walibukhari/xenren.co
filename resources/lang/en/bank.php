<?php

return array(
    '银行名称' => 'Bank name(Chinese)',
    '银行名称-英文' => 'Bank name (English)',
    '背景颜色' => 'background color',
    '字体颜色' => 'font color',
    '标志' => 'logo',
    '修改银行' => 'Change Bank',
    '后台' => 'Background',
    'desc' => 'added/edit/delete bank',
    '开始' => 'start',
    '操作' => 'operate',
    '新增时间' => 'Add time',
    '结束' => 'Finish',
    '过滤' => 'filter',
    '重置' => 'reset',
    '银行列表' => 'Bank list',
    '银行管理' => 'Bank management',
);
